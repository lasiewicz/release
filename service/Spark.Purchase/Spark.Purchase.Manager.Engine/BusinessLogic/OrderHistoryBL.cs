﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.Manager.Engine.DataAccess;
using Spark.Purchase.ValueObjects;

namespace Spark.Purchase.Manager.Engine.BusinessLogic
{
    internal class OrderHistoryBL
    {
        internal List<OrderInfo> GetMemberOrderHistoryByMemberID(int CustomerID, int CallingSystemID, int PageSize, int PageNumber)
        {
            OrderHistoryDAL dal = new OrderHistoryDAL();
            return dal.GetMemberOrderHistoryByMemberID(CustomerID, CallingSystemID, PageSize, PageNumber);
        }

        internal List<OrderInfo> GetMemberOrderHistoryByOrderID(int OrderID, int PageSize, int PageNumber)
        {
            OrderHistoryDAL dal = new OrderHistoryDAL();
            CustomerInfo memberData = dal.GetCustomerInfoByOrderID(OrderID);
            return dal.GetMemberOrderHistoryByMemberID(memberData.CustomerID, memberData.CallingSystemID, PageSize, PageNumber);
        }

        internal List<OrderInfo> GetMemberOrderHistoryByPaymentID(int PaymentID, int PageSize, int PageNumber)
        {
            OrderHistoryDAL dal = new OrderHistoryDAL();
            CustomerInfo memberData = dal.GetCustomerInfoByPaymentID(PaymentID);
            return dal.GetMemberOrderHistoryByMemberID(memberData.CustomerID, memberData.CallingSystemID, PageSize, PageNumber);
        }

        internal OrderInfo GetOrderInfo(int orderID)
        {
            OrderHistoryDAL dal = new OrderHistoryDAL();
            OrderInfo orderInfo = dal.GetOrderInfo(orderID);

            AddItemDescriptionToOrderInfo(orderInfo);

            return orderInfo;
        }

        internal List<OrderInfo> SearchOrderHistory(int memberID, int callingSystemID, int orderID, int paymentID)
        {
            OrderHistoryDAL dal = new OrderHistoryDAL();
            return dal.SearchOrderHistory(memberID, callingSystemID, orderID, paymentID);
        }

        internal OrderInfo AddItemDescriptionToOrderInfo(OrderInfo orderInfo)
        {
            if (orderInfo != null && orderInfo.OrderDetail != null)
            {
                Dictionary<int, Package> packageList = new Dictionary<int, Package>();
                CatalogDAL catalogDAL = new CatalogDAL();
                Package package;
                foreach (OrderDetailInfo detailInfo in orderInfo.OrderDetail)
                {
                    if (!packageList.ContainsKey(detailInfo.PackageID))
                    {
                        package = catalogDAL.GetPackage(detailInfo.PackageID);
                        packageList.Add(detailInfo.PackageID, package);
                    }
                    else
                    {
                        package = packageList[detailInfo.PackageID];
                    }

                    foreach (Item item in package.Items)
                    {
                        if (item.ID == detailInfo.ItemID)
                        {
                            detailInfo.ItemDescription = item.Description;
                            break;
                        }
                    }
                }
            }

            return orderInfo;
        }
    }
}
