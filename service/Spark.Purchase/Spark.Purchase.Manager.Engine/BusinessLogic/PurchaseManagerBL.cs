﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.Manager.Engine.DataAccess;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;

namespace Spark.Purchase.Manager.Engine.BusinessLogic
{
    public class PurchaseManagerBL
    {
        public PurchaseManagerBL()
        {

        }

        internal Cart CreateCart(int customerID, int systemID, SystemType callingSystemTypeID, int regionID, ref PurchaseResponse purchaseResponse, string orderAttributes)
        {
            try
            {
                return new Cart { UserID = customerID, SystemID = systemID, CallingSystemType = callingSystemTypeID, RegionID = regionID, OrderAttributes = orderAttributes };
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseManagerBL.CreateCart() error.", ex);
            }
        }

        internal void AddPackageToCart(ref Cart userCart, Package package)
        {
            try
            {
                userCart.AddPackage(package);
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseManagerBL.AddPackageToCart() error.", ex);
            }
        }

        internal void RemovePackageFromCart(ref Cart userCart, int packageID)
        {
            try
            {
                userCart.RemovePackage(packageID);
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseManagerBL.RemovePackageFromCart() error.", ex);
            }
        }

        public Package GetPackageDetails(int packageID)
        {
            try
            {
                CatalogDAL dal = new CatalogDAL();
                return dal.GetPackage(packageID);
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseManagerBL.GetPackageDetails() error.", ex);
            }
        }

        public string[] GetRequiredValidations(TransactionType transactionType, SystemType systemType, PaymentType paymentType, int systemID)
        {
            try
            {
                return RequiredValidations.GetRequiredValidations(transactionType, systemType, paymentType, systemID);
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseManagerBL.GetRequiredValidations() error.", ex);
            }
        }

        public int GetRegionID(decimal latitude, decimal longitude)
        {
            int regionID = Constants.NULL_INT;

            try
            {
                TraceLog.Enter("PurchaseManagerBL.GetRegionID");
                TraceLog.WriteLine("Begin PurchaseManagerBL.GetRegionID({0}, {1})", latitude, longitude);

                RegionDAL regionDAL = new RegionDAL();
                regionID = regionDAL.GetRegionID(latitude, longitude);
                TraceLog.WriteLine("Region ID [{0}] found from the database", regionID);

                return regionID;
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseManagerBL.GetRegionID, Error: " + ex.Message);
                throw new BLException("PurchaseManagerBL.GetRegionID error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PurchaseManagerBL.GetRegionID");
                TraceLog.Leave("PurchaseManagerBL.GetRegionID");
            }
        }
    }
}
