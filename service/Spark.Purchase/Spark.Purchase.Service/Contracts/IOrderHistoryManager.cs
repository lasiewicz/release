﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Spark.Purchase.ValueObjects;

namespace Spark.Purchase.Service
{
    [ServiceContract(Namespace = "http://spark")]
    public interface IOrderHistoryManager
    {
        [OperationContract]
        List<OrderInfo> GetMemberOrderHistoryByMemberID(int MemberID, int CallingSystemID, int PageSize, int PageNumber);
        
        [OperationContract]
        List<OrderInfo> GetMemberOrderHistoryByOrderID(int OrderID, int PageSize, int PageNumber);
        
        [OperationContract]
        List<OrderInfo> GetMemberOrderHistoryByPaymentID(int PaymentID, int PageSize, int PageNumber);

        [OperationContract]
        OrderInfo GetOrderInfo(int orderID);

        [OperationContract]
        List<OrderInfo> SearchOrderHistory(int memberID, int callingSystemID, int orderID, int paymentID);
    }
}
