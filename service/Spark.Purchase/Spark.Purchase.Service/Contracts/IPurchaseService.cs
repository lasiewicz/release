﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using Spark.Purchase.ValueObjects.Credit;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.Purchase.ValueObjects;

namespace Spark.Purchase.Service
{
    // NOTE: If you change the interface name "IPurchaseManagerService" here, you must also update the reference to "IPurchaseManagerService" in Web.config.
    [ServiceContract(Namespace = "http://spark")]
    public interface IPurchaseService
    {
        [OperationContract]
        string CreateCart(int customerID, int systemID, int callingSystemTypeID, int regionID, string orderAttributes);

        [OperationContract]
        string AddPackageToCart(string cartToken, int packageID);

        [OperationContract]
        string RemovePackageFromCart(string cartToken, int packageID);

        [OperationContract]
        string PriceCart(string cartToken);

        [OperationContract]
        string PurchaseOrder(string paymentProfileID, string cartToken, string transactionType, string paymentType);

        [OperationContract]
        string GetRequiredValidations(string transactionType, int systemType, string paymentType, int systemID);

        [OperationContract]
        CreditOrderResponse CreditOrder(ValueObjects.Order order, TransactionType transactionType, int paymentID, int paymentTypeID);

        [OperationContract]
        CreditOrderResponse CreditOrderFromOrderInfo(ValueObjects.OrderInfo orderInfo, TransactionType transactionType, int paymentID, int paymentTypeID);

        [OperationContract]
        bool IsCreditSetToVoid(int paymentID, int callingSystemID);

        [OperationContract]
        int GetRegionID(decimal latitude, decimal longitude);

        [OperationContract]
        DataContractHelper GetDataContractHelper();

        [OperationContract(IsOneWay=true)]
        void RenewPackage(int[] packageIDs, int customerID, string paymentProfileID, int systemID, int callingSystemTypeID, int regionID);
    }
}
