﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Spark.Purchase.ValueObjects;

namespace Spark.Purchase.Service
{
    [ServiceContract(Namespace = "http://spark")]
    public interface IPaymentProfileMapper
    {
        [OperationContract]
        void SetMemberPaymentProfile(int CustomerID, int CallingSystemID, string PaymentProfileID, string PaymentProfileIdType);

        [OperationContract]
        PaymentProfileInfo GetMemberDefaultPaymentProfile(int CustomrID, int CallingSystemID);

        [OperationContract]
        bool IsMemberPaymentProfileValid(int CustomerID, int CallingSystemID);
    }
}
