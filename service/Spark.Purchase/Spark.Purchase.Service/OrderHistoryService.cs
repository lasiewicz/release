﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

using Spark.Purchase.Manager.Engine.ServiceManagers;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.ValueObjects.Credit;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.CustomContext;

namespace Spark.Purchase.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ServiceRequestContextAttribute]
    public class OrderHistoryService : IOrderHistoryManager
    {
        #region IOrderHistoryManager Members

        public List<OrderInfo> GetMemberOrderHistoryByMemberID(int MemberID, int CallingSystemID, int PageSize, int PageNumber)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetMemberOrderHistoryByMemberID(MemberID, CallingSystemID, PageSize, PageNumber);
        }

        public List<OrderInfo> GetMemberOrderHistoryByOrderID(int OrderID, int PageSize, int PageNumber)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetMemberOrderHistoryByOrderID(OrderID, PageSize, PageNumber);
        }

        public List<OrderInfo> GetMemberOrderHistoryByPaymentID(int PaymentID, int PageSize, int PageNumber)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetMemberOrderHistoryByPaymentID(PaymentID, PageSize, PageNumber);
        }

        public OrderInfo GetOrderInfo(int orderID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetOrderInfo(orderID);
        }

        public List<OrderInfo> SearchOrderHistory(int memberID, int callingSystemID, int orderID, int paymentID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.SearchOrderHistory(memberID, callingSystemID, orderID, paymentID);
        }

        #endregion
    }
}
