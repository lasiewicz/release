﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

using Spark.Purchase.Manager.Engine.ServiceManagers;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.ValueObjects.Credit;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.CustomContext;

namespace Spark.Purchase.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ServiceRequestContextAttribute]
    public class PaymentProfileService : IPaymentProfileMapper
    {
        #region IPaymentProfileMapper Members

        public void SetMemberPaymentProfile(int CustomerID, int CallingSystemID, string PaymentProfileID, string PaymentProfileIdType)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            purchaseManagerSM.SetMemberPaymentProfile(CustomerID, CallingSystemID, PaymentProfileID, PaymentProfileIdType);
        }

        public PaymentProfileInfo GetMemberDefaultPaymentProfile(int CustomerID, int CallingSystemID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetMemberDefaultPaymentProfile(CustomerID, CallingSystemID);
        }

        public bool IsMemberPaymentProfileValid(int CustomerID, int CallingSystemID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.IsMemberPaymentProfileValid(CustomerID, CallingSystemID);
        }

        #endregion
    }
}
