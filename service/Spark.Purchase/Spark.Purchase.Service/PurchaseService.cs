﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

using Spark.Purchase.Manager.Engine.ServiceManagers;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.ValueObjects.Credit;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.CustomContext;

namespace Spark.Purchase.Service
{
    // NOTE: If you change the class name "PurchaseManagerService" here, you must also update the reference to "PurchaseManagerService" in Web.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ServiceRequestContextAttribute]
    public class PurchaseService : IPurchaseService
    {
        public string CreateCart(int customerID, int systemID, int callingSystemTypeID, int regionID, string orderAttributes)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.CreateCart(customerID, systemID, callingSystemTypeID, regionID, orderAttributes);
        }

        public string AddPackageToCart(string cartToken, int packageID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.AddPackageToCart(cartToken, packageID);
        }

        public string RemovePackageFromCart(string cartToken, int packageID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.RemovePackageFromCart(cartToken, packageID);
        }

        public string PriceCart(string cartToken)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.PriceCart(cartToken);
        }

        public string PurchaseOrder(string paymentProfileID, string cartToken, string transactionType, string paymentType)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.PurchaseOrder(paymentProfileID, cartToken, transactionType, paymentType);
        }

        public string GetRequiredValidations(string transactionType, int systemType, string paymentType, int systemID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetRequiredValidations(transactionType, systemType, paymentType, systemID);
        }

        public CreditOrderResponse CreditOrder(ValueObjects.Order order, TransactionType transactionType, int paymentID, int paymentTypeID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.CreditOrder(order, transactionType, paymentID, paymentTypeID);
        }

        public CreditOrderResponse CreditOrderFromOrderInfo(ValueObjects.OrderInfo orderInfo, TransactionType transactionType, int paymentID, int paymentTypeID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.CreditOrderFromOrderInfo(orderInfo, transactionType, paymentID, paymentTypeID);
        }

        public bool IsCreditSetToVoid(int paymentID, int callingSystemID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.IsCreditSetToVoid(paymentID, callingSystemID);
        }

        public int GetRegionID(decimal latitude, decimal longitude)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetRegionID(latitude, longitude);
        }

        public DataContractHelper GetDataContractHelper()
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.GetDataContractHelper();
        }

        public void RenewPackage(int[] packageIDs, int customerID, string paymentProfileID, int systemID, int callingSystemTypeID, int regionID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            purchaseManagerSM.RenewPackage(packageIDs, customerID, paymentProfileID, systemID, callingSystemTypeID, regionID);

        }

        

        
    }
}

