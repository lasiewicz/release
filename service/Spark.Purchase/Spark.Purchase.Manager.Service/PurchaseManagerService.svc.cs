﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Spark.Purchase.Manager.Engine.ServiceManagers;

namespace Spark.Purchase.Manager.Service
{
    // NOTE: If you change the class name "PurchaseManagerService" here, you must also update the reference to "PurchaseManagerService" in Web.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PurchaseManagerService : IPurchaseManagerService
    {
        public string CreateCart(int customerID, int systemID, int callingSystemTypeID, int regionID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.CreateCart(customerID, systemID, callingSystemTypeID, regionID);
        }

        public string AddPackageToCart(string cartToken, int packageID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.AddPackageToCart(cartToken, packageID);
        }

        public string RemovePackageFromCart(string cartToken, int packageID)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.RemovePackageFromCart(cartToken, packageID);
        }

        public string PriceCart(string cartToken)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.PriceCart(cartToken);
        }

        public string PurchaseOrder(string paymentProfileID, string cartToken, string transactionType)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.PurchaseOrder(paymentProfileID, cartToken, transactionType);
        }

        public string CreditOrder(int orderID, decimal amount, int transactionTypeID, int systemID, int callingSystemType)
        {
            PurchaseManagerSM purchaseManagerSM = new PurchaseManagerSM();
            return purchaseManagerSM.CreditOrder(orderID, amount, transactionTypeID, systemID, callingSystemType);
        }

    }
}
