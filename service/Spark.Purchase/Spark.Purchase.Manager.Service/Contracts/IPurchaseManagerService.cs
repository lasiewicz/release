﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Spark.Purchase.Manager.Service
{
    // NOTE: If you change the interface name "IPurchaseManagerService" here, you must also update the reference to "IPurchaseManagerService" in Web.config.
    [ServiceContract(Namespace = "http://spark")]
    public interface IPurchaseManagerService
    {
        [OperationContract]
        string CreateCart(int customerID, int systemID, int callingSystemTypeID, int regionID);

        [OperationContract]
        string AddPackageToCart(string cartToken, int packageID);

        [OperationContract]
        string RemovePackageFromCart(string cartToken, int packageID);

        [OperationContract]
        string PriceCart(string cartToken);

        [OperationContract]
        string PurchaseOrder(string paymentProfileID, string cartToken, string transactionType);

        [OperationContract]
        string CreditOrder(int orderId, decimal amount, int transactionTypeID, int systemID, int CallingSystemType);
    }
}
