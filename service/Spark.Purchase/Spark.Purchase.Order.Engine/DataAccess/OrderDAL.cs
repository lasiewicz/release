﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Spark.Purchase.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;

namespace Spark.Purchase.Order.Engine.DataAccess
{
    internal class OrderDAL
    {
        public OrderDAL()
        {

        }

        /// <summary>
        /// A customer can have only one pending order per calling system.
        /// An order can only remain in the pending state while it is being processed internally
        /// through both the purchase and payment service.
        /// If in any case that the payment cannot go through, than the order must be failed to
        /// allow a retry at a later time.
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="callingSystemID"></param>
        /// <returns></returns>                
        internal int GetOrderLockCount(int customerID, int callingSystemID)
        {
            int orderLockCount = Constants.NULL_INT;
            SqlDataReader dataReader = null;

            try
            {
                TraceLog.Enter("OrderDAL.GetOrderLockCount");
                TraceLog.WriteLine("Begin OrderDAL.GetOrderLockCount({0}, {1})", customerID, callingSystemID);

                Command command = new Command("epOrder", "dbo.up_OrderLock_Get", 0);
                command.AddParameter("@CustomerID", SqlDbType.Int, ParameterDirection.Input, customerID);
                command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, callingSystemID);
                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    orderLockCount = dataReader.GetInt32(dataReader.GetOrdinal("OrderLockCount"));
                    TraceLog.WriteLine("Customer ID [{0}] has [{1}] pending orders in system ID [{2}]", customerID, orderLockCount, callingSystemID);
                }

                return orderLockCount;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in OrderDAL.GetOrderLockCount(), Error: " + exception.Message);
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderDAL.GetOrderLockCount() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderDAL.GetOrderLockCount() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }

                TraceLog.WriteLine("End OrderDAL.GetOrderLockCount()");
                TraceLog.Leave("OrderDAL.GetOrderLockCount");
            }
        }

        //public int InsertOrder(ValueObjects.Order order)
        //{
        //    return InsertOrder(order.UserID, order.ReferenceOrderID, order.SystemID, order.CallingSystemType, order.TotalAmount, order.CurrencyType, order.RegionID, order.AdminID, order.AdminName, order.OrderReasonID, order.PaymentProfileID, order.OrderAtthibutes);
        //}

        public int InsertOrder(int customerID, int referenceOrderID, int callingSystemID, SystemType callingSystemType, decimal totalAmount, CurrencyType currencyType, int regionID, int AdminUserID, string AdminUserName, int OrderReasonID, string paymentProfileID, string orderAttributes)
        {
            Command command = null;

            try
            {
                TraceLog.Enter("OrderDAL.InsertOrder");
                TraceLog.WriteLine("Begin OrderDAL.InsertOrder({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10})", 
                    customerID, 
                    referenceOrderID, 
                    callingSystemID, 
                    callingSystemType, 
                    totalAmount, 
                    currencyType, 
                    regionID, 
                    AdminUserID, 
                    AdminUserName, 
                    OrderReasonID, 
                    paymentProfileID);

                command = new Command("epOrder", "dbo.up_Order_Insert", 0);
                command.AddParameter("@CustomerID", SqlDbType.Int, ParameterDirection.Input, customerID);
                if (referenceOrderID != Constants.NULL_INT)
                {
                    command.AddParameter("@ReferenceOrderID", SqlDbType.Int, ParameterDirection.Input, referenceOrderID);
                }
                //else
                //{
                //    command.AddParameter("@ReferenceOrderID", SqlDbType.Int, ParameterDirection.Input, DBNull.Value);
                //}
                command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, callingSystemID);
                command.AddParameter("@CallingSystemTypeID", SqlDbType.Int, ParameterDirection.Input, (int)callingSystemType);
                if (!String.IsNullOrEmpty(paymentProfileID))
                {
                    command.AddParameter("@PaymentProfileID", SqlDbType.NVarChar, ParameterDirection.Input, paymentProfileID);
                }
                command.AddParameter("@TotalAmount", SqlDbType.Decimal, ParameterDirection.Input, totalAmount);
                command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)currencyType);
                command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
                if (AdminUserID != Constants.NULL_INT)
                {
                    command.AddParameter("@AdminUserID", SqlDbType.Int, ParameterDirection.Input, AdminUserID);
                }
                if (!String.IsNullOrEmpty(AdminUserName))
                {
                    command.AddParameter("@AdminUserName", SqlDbType.NVarChar, ParameterDirection.Input, AdminUserName);
                }
                if (OrderReasonID != Constants.NULL_INT)
                {
                    command.AddParameter("@OrderReasonID", SqlDbType.Int, ParameterDirection.Input, OrderReasonID);
                }
                command.AddParameter("@NewOrderID", SqlDbType.Int, ParameterDirection.Output);
                if(!string.IsNullOrEmpty(orderAttributes))
                    command.AddParameter("@OrderAttributeXML", SqlDbType.Xml, ParameterDirection.Input, GetOrderAttributesXml(orderAttributes));

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    TraceLog.WriteErrorLine(exception, "Error in OrderDAL.InsertOrder(), Error: " + exception.Message);
                    throw exception;
                }

                int newOrderID = Constants.NULL_INT;
                if (command.Parameters["@NewOrderID"].ParameterValue != DBNull.Value)
                {
                    newOrderID = Convert.ToInt32(command.Parameters["@NewOrderID"].ParameterValue);
                    TraceLog.WriteLine("Order ID {0} was successfully inserted into the database", newOrderID);
                }

                return newOrderID;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in OrderDAL.InsertOrder(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderDAL.InsertCharge() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderDAL.InsertCharge() error.", exception, exception.Message);
                }
            }
            finally
            {
                TraceLog.WriteLine("End OrderDAL.InsertOrder()");
                TraceLog.Leave("OrderDAL.InsertOrder");
            }
        }

        private string GetOrderAttributesXml(string orderAttributes)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<OrderAttributes>");
            if (!string.IsNullOrEmpty(orderAttributes))
            {
                string[] attributes = orderAttributes.Split(',');
                foreach (string a in attributes)
                {
                    string[] pair = a.Split('=');
                    sb.AppendFormat("<Attribute attributeid=\"{0}\" value=\"{1}\" />", pair[0], pair[1]);
                }
            }
            sb.Append("</OrderAttributes>");
            return sb.ToString();
        }

        public int InsertOrderDetail(int orderID, int itemID, int packageID, TransactionType orderType, decimal amount, CurrencyType currencyType, int ReferenceOrderDetailID, int Duration, DurationType durationType)
        {
            try
            {
                TraceLog.Enter("OrderDAL.InsertOrderDetail");
                TraceLog.WriteLine("Begin OrderDAL.InsertOrderDetail({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})",
                    orderID,
                    itemID,
                    packageID,
                    orderType,
                    amount,
                    currencyType,
                    ReferenceOrderDetailID,
                    Duration,
                    durationType);

                Command command = new Command("epOrder", "dbo.up_OrderDetail_Insert", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                command.AddParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, itemID);
                command.AddParameter("@PackageID", SqlDbType.Int, ParameterDirection.Input, packageID);
                command.AddParameter("@OrderTypeID", SqlDbType.Int, ParameterDirection.Input, (int)orderType);
                command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, amount);
                command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)currencyType);
                if (ReferenceOrderDetailID != Constants.NULL_INT)
                {
                    command.AddParameter("@ReferenceOrderDetailID", SqlDbType.Int, ParameterDirection.Input, ReferenceOrderDetailID);
                }
                if (Duration != Constants.NULL_INT)
                {
                    command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, Duration);
                    command.AddParameter("@DurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)durationType);
                }

                command.AddParameter("@NewOrderDetailID", SqlDbType.Int, ParameterDirection.Output);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    TraceLog.WriteErrorLine(exception, "Error in OrderDAL.InsertOrderDetail(), Error: " + exception.Message);
                    throw exception;
                }

                int newOrderDetailID = Constants.NULL_INT;
                if (command.Parameters["@NewOrderDetailID"].ParameterValue != DBNull.Value)
                {
                    newOrderDetailID = Convert.ToInt32(command.Parameters["@NewOrderDetailID"].ParameterValue);
                    TraceLog.WriteLine("Order detail ID {0} was successfully inserted into the database", newOrderDetailID);
                }

                return newOrderDetailID;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in OrderDAL.InsertOrderDetail(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderDAL.InsertOrderDetail() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderDAL.InsertOrderDetail() error.", exception, exception.Message);
                }
            }
            finally
            {
                TraceLog.WriteLine("End OrderDAL.InsertOrderDetail()");
                TraceLog.Leave("OrderDAL.InsertOrderDetail");
            }
        }

        public int InsertOrderTax(int orderDetailID, decimal amount, CurrencyType currencyType, int authorityID, string authorityCode, int regionID, ItemTaxType itemTaxType)
        {
            try
            {
                TraceLog.Enter("OrderDAL.InsertOrderTax");
                TraceLog.WriteLine("Begin OrderDAL.InsertOrderTax({0}, {1}, {2}, {3}, {4}, {5}, {6})",
                    orderDetailID,
                    amount,
                    currencyType,
                    authorityID,
                    authorityCode,
                    regionID,
                    itemTaxType);

                Command command = new Command("epOrder", "dbo.up_OrderTax_Insert", 0);
                command.AddParameter("@OrderDetailID", SqlDbType.Int, ParameterDirection.Input, orderDetailID);
                command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, amount);
                command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)currencyType);
                command.AddParameter("@AuthorityID", SqlDbType.Int, ParameterDirection.Input, authorityID);
                command.AddParameter("@AuthorityCode", SqlDbType.Text, ParameterDirection.Input, authorityCode);
                command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
                command.AddParameter("@ItemTaxTypeID", SqlDbType.Int, ParameterDirection.Input, (int)itemTaxType);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    TraceLog.WriteErrorLine(exception, "Error in OrderDAL.InsertOrderTax(), Error: " + exception.Message);
                    throw exception;
                }

                TraceLog.WriteLine("Tax item for order detail ID {0} was successfully inserted into the database", orderDetailID);

                return returnValue;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in OrderDAL.InsertOrderTax(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderDAL.InsertOrderTax() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderDAL.InsertOrderTax() error.", exception, exception.Message);
                }
            }
            finally
            {
                TraceLog.WriteLine("End OrderDAL.InsertOrderTax()");
                TraceLog.Leave("OrderDAL.InsertOrderTax");
            }
        }

        public void UpdateOrderStatus(int orderID, OrderStatus orderStatus, int commonResponseCode)
        {
            try
            {
                TraceLog.Enter("OrderDAL.UpdateOrderStatus");
                TraceLog.WriteLine("Begin OrderDAL.UpdateOrderStatus({0}, {1})", orderID, orderStatus);

                Command command = new Command("epOrder", "dbo.up_OrderStatus_Update", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                command.AddParameter("@OrderStatusID", SqlDbType.Int, ParameterDirection.Input, (int)orderStatus);
                command.AddParameter("@CommonResponseCode", SqlDbType.Int, ParameterDirection.Input, commonResponseCode);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    TraceLog.WriteErrorLine(exception, "Error in OrderDAL.UpdateOrderStatus(), Error: " + exception.Message);
                    throw exception;
                }

                TraceLog.WriteLine("The status of Order ID [{0}] successfully updated with a status of [{1}] in the database", orderID, orderStatus);
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in OrderDAL.UpdateOrderStatus(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderDAL.UpdateOrderStatus() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderDAL.UpdateOrderStatus() error.", exception, exception.Message);
                }
            }
            finally
            {
                TraceLog.WriteLine("End OrderDAL.UpdateOrderStatus()");
                TraceLog.Leave("OrderDAL.UpdateOrderStatus");
            }
        }

        public void UpdateOrderType(int orderID, TransactionType orderType)
        {
            try
            {
                Command command = new Command("epOrder", "up_OrderType_Update", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                command.AddParameter("@OrderTypeID", SqlDbType.Int, ParameterDirection.Input, (int)orderType);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    throw exception;
                }
            }
            catch (Exception exception)
            {
                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("OrderDAL.UpdateOrderType() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("OrderDAL.UpdateOrderType() error.", exception, exception.Message);
                }
            }
        }
    }
}
