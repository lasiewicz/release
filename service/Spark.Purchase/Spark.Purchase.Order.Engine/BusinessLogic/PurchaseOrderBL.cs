﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

using Spark.Purchase;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.Order.Engine.DataAccess;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.UnifiedPurchaseSystem.Lib.Common.Utils;
//using Order = global::Spark.Purchase.ValueObjects.Order;

namespace Spark.Purchase.Order.Engine.BusinessLogic
{
    public class PurchaseOrderBL
    {
        public PurchaseOrderBL()
        {
            
        }

        public bool UserAllowedToPurchaseFromSite(int customerID, int callingSystemID)
        {
            try
            {
                TraceLog.Enter("PurchaseOrderBL.UserAllowedToPurchaseFromSite");
                TraceLog.WriteLine("Begin PurchasPurchaseOrderBL.UserAllowedToPurchaseFromSite({0}, {1})", customerID, callingSystemID);

                bool isUserAllowedToPurchaseFromSite = true;
                OrderDAL orderDAL = new OrderDAL();
                isUserAllowedToPurchaseFromSite = (orderDAL.GetOrderLockCount(customerID, callingSystemID) > 0 ? false : true);
                TraceLog.WriteLine("Is Customer ID [{0}] allowed to purchase in system ID [{1}]? {2}", customerID, callingSystemID, isUserAllowedToPurchaseFromSite);
                return isUserAllowedToPurchaseFromSite;
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseOrderBL.UserAllowedToPurchaseFromSite(), Error: " + ex.Message);
                throw new BLException("PurchaseOrderBL.UserAllowedToPurchaseFromSite() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PurchaseOrderBL.UserAllowedToPurchaseFromSite()");
                TraceLog.Leave("PurchaseOrderBL.UserAllowedToPurchaseFromSite");
            }
        }

        public ValueObjects.Order CreateOrderFromCart(Cart cart, string paymentProfileID, TransactionType tranType)
        {
            ValueObjects.Order order = null;

            try
            {
                //determine if transaction is renewal (amount vs renewal amount)
                bool isRenewal = PurchaseUtility.IsRenewalTransaction(tranType);

                order = new ValueObjects.Order(cart, paymentProfileID, isRenewal);
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseOrderBL.CreateOrderFromCart() error.", ex);
            }

            return order;
        }


        public void UpdateOrderStatus(int orderID, OrderStatus orderStatus, int commonResponseCode)
        {
            try
            {
                TraceLog.Enter("PurchaseOrderBL.UpdateOrderStatus");
                TraceLog.WriteLine("Begin PurchasPurchaseOrderBL.UpdateOrderStatus({0}, {1})", orderID, orderStatus);

                OrderDAL orderDAL = new OrderDAL();
                orderDAL.UpdateOrderStatus(orderID, orderStatus, commonResponseCode);
                TraceLog.WriteLine("Order ID {0} set with a status of {1}", orderID, orderStatus);
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseOrderBL.UpdateOrderStatus(), Error: " + ex.Message);
                throw new BLException("PurchaseOrderBL.UpdateOrderStatus() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PurchaseOrderBL.UpdateOrderStatus()");
                TraceLog.Leave("PurchaseOrderBL.UpdateOrderStatus");
            }
        }

        public void UpdateOrderType(int orderID, TransactionType orderType)
        {
            try
            {
                OrderDAL orderDAL = new OrderDAL();
                orderDAL.UpdateOrderType(orderID, orderType);
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseOrderBL.UpdateOrderStatus() error.", ex);
            }
        }

        /// <summary>
        /// This will process a new order (this should not have an OrderID), saving it to our database.
        /// </summary>
        /// <param name="newOrder">The Order to be processed</param>
        /// <returns>The processed Order which should now contain an OrderID</returns>
        public ValueObjects.Order ProcessOrder(ValueObjects.Order newOrder, TransactionType transactionType)
        {
            try
            {
                TraceLog.Enter("PurchaseOrderBL.ProcessOrder");
                TraceLog.WriteLine("Begin PurchaseOrderBL.ProcessOrder({0}, {1})", newOrder, transactionType);

                OrderDAL orderDAL = new OrderDAL();
                int newOrderID = Constants.NULL_INT;
                int newOrderDetailID = Constants.NULL_INT;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    decimal totalAmount = newOrder.TotalAmount;
                    if (transactionType == TransactionType.Credit || transactionType == TransactionType.Void)
                    {
                        //save amount as negative
                        totalAmount = (-Math.Abs(totalAmount));
                    }

                    newOrderID = orderDAL.InsertOrder(newOrder.UserID, newOrder.ReferenceOrderID, newOrder.SystemID, newOrder.CallingSystemType, totalAmount, newOrder.CurrencyType, newOrder.RegionID, newOrder.AdminID, newOrder.AdminName, newOrder.OrderReasonID, newOrder.PaymentProfileID, newOrder.OrderAtthibutes);
                    
                    TraceLog.WriteLine("Completed saving the new order into the database, Order ID: {0}", newOrderID);
                    foreach (Package package in newOrder.Packages)
                    {
                        foreach (Item item in package.Items)
                        {
                            //determine correct item amount (regular amount or renewal amount?)
                            decimal itemAmount = PurchaseUtility.IsRenewalTransaction(transactionType) ? item.RenewalAmount : item.Amount;
                            if (transactionType == TransactionType.Credit || transactionType == TransactionType.Void)
                            {
                                //save amount as negative
                                itemAmount = (-Math.Abs(itemAmount));
                            }

                            newOrderDetailID = orderDAL.InsertOrderDetail(newOrderID, item.ID, package.ID, transactionType, itemAmount, item.CurrencyType, item.ReferenceItemID, item.Duration, item.DurationType);
                            TraceLog.WriteLine("Completed saving the new order detail into the database, Order Detail ID: {0}", newOrderDetailID);
                            foreach (Tax tax in item.Taxes)
                            {
                                decimal taxAmount = tax.Amount;
                                if (transactionType == TransactionType.Credit || transactionType == TransactionType.Void)
                                {
                                    //save amount as negative
                                    taxAmount = (-Math.Abs(taxAmount));
                                }
                                orderDAL.InsertOrderTax(newOrderDetailID, taxAmount, newOrder.CurrencyType, tax.AuthorityID, tax.AuthorityCode, tax.RegionID, tax.ItemTaxType);
                                TraceLog.WriteLine("Completed saving the new tax item into the database");
                            }
                        }
                    }

                    scope.Complete();
                }

                newOrder.OrderID = newOrderID;
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseOrderBL.ProcessOrder(), Error: " + ex.Message);
                throw new BLException("PurchaseOrderBL.ProcessOrder() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PurchaseOrderBL.ProcessOrder()");
                TraceLog.Leave("PurchaseOrderBL.ProcessOrder");
            }

            return newOrder;
        }

        public ValueObjects.Order CreateOrderFromOrderInfo(OrderInfo orderInfo)
        {
            ValueObjects.Order order = null;

            try
            {
                order = new ValueObjects.Order();

                //Order level
                order.AdminID = orderInfo.AdminUserID;
                order.AdminName = orderInfo.AdminUserName;
                if (orderInfo.CallingSystemTypeID != Constants.NULL_INT)
                {
                    order.CallingSystemType = (SystemType)Enum.Parse(typeof(SystemType), orderInfo.CallingSystemTypeID.ToString());
                }
                order.CurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), orderInfo.CurrencyID.ToString());
                order.OrderID = orderInfo.OrderID;
                order.OrderReasonID = orderInfo.OrderReasonID;
                order.PromotionID = orderInfo.PromoID;
                order.ReferenceOrderID = orderInfo.ReferenceOrderID;
                order.RegionID = orderInfo.RegionID;
                order.SystemID = orderInfo.CallingSystemID;
                order.TotalAmount = orderInfo.TotalAmount;
                order.UserID = orderInfo.CustomerID;
                order.PaymentProfileID = orderInfo.UserPaymentGuid;

                //Package
                Dictionary<int, Package> packageTempList = new Dictionary<int, Package>();
                foreach (OrderDetailInfo odInfo in orderInfo.OrderDetail)
                {

                    //Items
                    Item itemTemp = new Item();
                    itemTemp.Amount = odInfo.Amount;
                    itemTemp.CurrencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), odInfo.CurrencyID.ToString());
                    itemTemp.Duration = odInfo.Duration;
                    if (odInfo.DurationTypeID != Constants.NULL_INT)
                    {
                        itemTemp.DurationType = (DurationType)Enum.Parse(typeof(DurationType), odInfo.DurationTypeID.ToString());
                    }
                    itemTemp.ID = odInfo.ItemID;
                    itemTemp.ReferenceItemID = odInfo.ReferenceOrderDetailID;

                    //Item Tax
                    foreach (OrderTaxInfo otInfo in odInfo.OrderTax)
                    {
                        Tax taxTemp = new Tax(otInfo.Amount, otInfo.AuthorityID, otInfo.AuthorityCode, otInfo.RegionID, (ItemTaxType)Enum.Parse(typeof(ItemTaxType), otInfo.ItemTaxTypeID.ToString()), Constants.NULL_DECIMAL);
                        itemTemp.Taxes.Add(taxTemp);
                    }


                    //Add Item to Package
                    if (!packageTempList.ContainsKey(odInfo.PackageID))
                    {
                        Package packageTemp = new Package();
                        packageTemp.ID = odInfo.PackageID;
                        packageTempList.Add(odInfo.PackageID, packageTemp);
                    }

                    packageTempList[odInfo.PackageID].AddItem(itemTemp);

                }

                foreach (Package p in packageTempList.Values)
                {
                    order.Packages.Add(p);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("PurchaseOrderBL.CreateOrderFromCart() error.", ex);
            }

            return order;
        }

    }
}
