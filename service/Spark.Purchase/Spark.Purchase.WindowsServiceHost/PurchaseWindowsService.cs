﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using Spark.Purchase.ValueObjects;

namespace Spark.Purchase.Service.Hosts
{
    public partial class PurchaseWindowsService : ServiceBase
    {
        ServiceHost host;

        public PurchaseWindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            host = new ServiceHost(typeof(PurchaseService));
            host.Open();

            HydraWriterWrapper.Instance.HydraWriterStart();
        }

        protected override void OnStop()
        {
            if (host != null)
            {
                host.Close();

                HydraWriterWrapper.Instance.HydraWriterStop();
            }
        }
    }
}
