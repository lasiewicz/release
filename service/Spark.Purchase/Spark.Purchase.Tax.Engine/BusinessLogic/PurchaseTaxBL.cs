﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Purchase.ValueObjects;
using Spark.Purchase.Tax.Engine.DataAccess;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;

namespace Spark.Purchase.Tax.Engine.BusinessLogic
{
    /// <summary>
    /// Represents Business Logic related to core Tax related functionalities
    /// </summary>
    public class PurchaseTaxBL
    {
        private TaxDAL _taxDAL = new TaxDAL();

        public PurchaseTaxBL()
        {
        }

        #region Public Methods
        /// <summary>
        /// Updates the cart with tax for each of the items it contains
        /// </summary>
        /// <param name="cart"></param>
        /// <returns></returns>
        public Cart PriceCart(Cart cart, bool isRenewal)
        {
            try
            {
                TraceLog.Enter("PurchaseTaxBL.PriceCart");
                TraceLog.WriteLine("Begin PurchaseTaxBL.PriceCart({0})", cart);

                //calculate tax for every cart item
                if (cart.RegionID != Constants.NULL_INT)
                {
                    foreach (Package package in cart.Packages)
                    {
                        foreach (Item item in package.Items)
                        {
                            if (item.ItemTaxTypes.Count > 0 && item.Taxes.Count <= 0)
                            {
                                //get tax
                                if (item.ItemTaxTypes.Count == 1)
                                {
                                    PopulateTax(cart.RegionID, item, item.ItemTaxTypes[0], isRenewal);
                                }
                                else
                                {
                                    foreach (ItemTaxType taxType in item.ItemTaxTypes)
                                    {
                                        //there is no need to set tax based on catch-all if it has specific tax types
                                        if (taxType != ItemTaxType.None)
                                            PopulateTax(cart.RegionID, item, taxType, isRenewal);
                                    }
                                }
                            }
                        }
                    }

                    TraceLog.WriteLine("Tax items added to the cart");
                }
                else
                {
                    throw new BLException("PurchaseTaxBL.PriceCart() error. Missing RegionID.");
                }
            }
            catch (BLException ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseTaxBL.PriceCart(), Error: " + ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseTaxBL.PriceCart(), Error: " + ex.Message);
                throw new BLException("PurchaseTaxBL.PriceCart() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PurchaseTaxBL.PriceCart()");
                TraceLog.Leave("PurchaseTaxBL.PriceCart");
            }

            return cart;
        }

        #endregion

        #region Private Methods
        private void PopulateTax(int regionID, Item item, ItemTaxType taxType, bool isRenewal)
        {
            try
            {
                TraceLog.Enter("PurchaseTaxBL.PopulateTax");
                TraceLog.WriteLine("Begin PurchaseTaxBL.PopulateTax({0}, {1}, {2})", regionID, item, taxType);

                //get tax authority codes
                List<TaxAuthorityCode> taxCodes = _taxDAL.GetTaxAuthorityCodes(taxType, regionID);

                //set tax
                foreach (TaxAuthorityCode taxCode in taxCodes)
                {
                    ValueObjects.Tax tax = new Spark.Purchase.ValueObjects.Tax(isRenewal ? item.RenewalAmount : item.Amount, taxCode);
                    item.Taxes.Add(tax);
                }

                TraceLog.WriteLine("Package item ID {0} populated with {1} taxes", item.ID, item.Taxes.Count);
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PurchaseTaxBL.PopulateTax(), Error: " + ex.Message);
                throw new BLException("PurchaseTaxBL.PopulateTax() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PurchaseTaxBL.PopulateTax()");
                TraceLog.Leave("PurchaseTaxBL.PopulateTax");
            }
        }

        #endregion
    }
}
