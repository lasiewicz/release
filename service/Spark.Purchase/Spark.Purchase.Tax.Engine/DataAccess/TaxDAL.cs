﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Spark.Purchase.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.Purchase.Tax.Engine.DataAccess
{
    internal class TaxDAL
    {
        private const string DB_REGION = "epRegion";
        private Dictionary<ItemTaxType, Dictionary<int, List<TaxAuthorityCode>>> _AuthorityCodeCache = new Dictionary<ItemTaxType, Dictionary<int, List<TaxAuthorityCode>>>();

        public TaxDAL()
        {

        }

        #region Public Methods
        /// <summary>
        /// Gets the parent regions for a given regionID
        /// </summary>
        /// <param name="regionID">RegionID, should be depth 4 (postalCode) if you want all hierarchy</param>
        /// <returns></returns>
        public RegionHierarchy GetRegionHierarchy(int regionID)
        {
            //TODO - RegionHiearchy info should be cached

            RegionHierarchy regionHierarchy = new RegionHierarchy();
            SqlDataReader dataReader = null;

            bool colLookupDone = false;
            int maxDepth = Constants.NULL_INT;
            int languageID = 2; //english

            Int32 ordinalRegionID = 0;
            Int32 ordinalDepth = 0;
            Int32 ordinalLatitude = 0;
            Int32 ordinalLongitude = 0;
            Int32 ordinalDescription = 0;
            Int32 ordinalAbbreviation = 0;
            Int32 ordinalMask = 0;
            Int32 ordinalChildrenDepth = 0;

            try
            {
                Command command = new Command(DB_REGION, "up_Region_ListHierarchy", 0);
                command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
                if (maxDepth != Constants.NULL_INT)
                {
                    command.AddParameter("@MaxDepth", SqlDbType.Int, ParameterDirection.Input, maxDepth);
                }
                command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                dataReader = Client.Instance.ExecuteReader(command);

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalRegionID = dataReader.GetOrdinal("RegionID");
                        ordinalDepth = dataReader.GetOrdinal("Depth");
                        ordinalLatitude = dataReader.GetOrdinal("Latitude");
                        ordinalLongitude = dataReader.GetOrdinal("Longitude");
                        ordinalDescription = dataReader.GetOrdinal("Description");
                        ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
                        ordinalMask = dataReader.GetOrdinal("Mask");
                        ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
                        colLookupDone = true;
                    }

                    Int32 regionIDCurrent = dataReader.GetInt32(ordinalRegionID);
                    int depth = (Int32)dataReader.GetByte(ordinalDepth);
                    decimal latitude = dataReader.GetDecimal(ordinalLatitude);
                    decimal longitude = dataReader.GetDecimal(ordinalLongitude);
                    string description = dataReader.GetString(ordinalDescription);
                    string abbreviation = null;
                    if (!dataReader.IsDBNull(ordinalAbbreviation))
                    {
                        abbreviation = dataReader.GetString(ordinalAbbreviation);
                    }
                    int mask = dataReader.GetInt32(ordinalMask);
                    int childrenDepth = Constants.NULL_INT;
                    if (!dataReader.IsDBNull(ordinalChildrenDepth))
                    {
                        childrenDepth = (Int32)dataReader.GetByte(ordinalChildrenDepth);
                    }

                    switch (depth)
                    {
                        case 4:
                            regionHierarchy.PostalCodeRegionID = regionID;
                            regionHierarchy.PostalCode = description;
                            regionHierarchy.Latitude = latitude;
                            regionHierarchy.Longitude = longitude;
                            break;
                        case 3:
                            regionHierarchy.CityRegionID = regionID;
                            regionHierarchy.CityName = description;
                            regionHierarchy.Latitude = latitude;
                            regionHierarchy.Longitude = longitude;
                            break;
                        case 2:
                            regionHierarchy.StateRegionID = regionID;
                            regionHierarchy.StateDescription = description;
                            regionHierarchy.StateAbbreviation = abbreviation;
                            break;
                        case 1:
                            regionHierarchy.CountryRegionID = regionID;
                            regionHierarchy.CountryName = description;
                            regionHierarchy.CountryAbbreviation = abbreviation;
                            regionHierarchy.Depth1RegionID = regionID;
                            break;
                    }

                    if (depth != Constants.NULL_INT)
                    {
                        if (depth > regionHierarchy.HighestDepthRegionTypeID)
                        {
                            regionHierarchy.HighestDepthRegionTypeID = depth;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SqlException sqlException = ex.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("TaxDAL.GetRegionHierarchy() error.", ex, ex.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("TaxDAL.GetRegionHierarchy() error.", ex, ex.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return regionHierarchy;

        }


        public List<TaxAuthorityCode> GetTaxAuthorityCodes(ItemTaxType taxType, int RegionID)
        {
            //TODO - Cache Tax Authority Code
            List<TaxAuthorityCode> taxCodes = null;

            //Temporary per-call cache until application wide caching is implemented
            if (_AuthorityCodeCache.ContainsKey(taxType))
            {
                if (_AuthorityCodeCache[taxType].ContainsKey(RegionID))
                    taxCodes = _AuthorityCodeCache[taxType][RegionID];
            }
            else
            {
                _AuthorityCodeCache.Add(taxType, new Dictionary<int, List<TaxAuthorityCode>>());
            }

            if (taxCodes == null)
            {
                taxCodes = new List<TaxAuthorityCode>();
                SqlDataReader dataReader = null;
                bool colLookupDone = false;

                Int32 ordinalAuthorityID = 0;
                Int32 ordinalAuthorityCode = 0;
                Int32 ordinalRate = 0;
                Int32 ordinalRegionID = 0;
                Int32 ordinalItemTaxTypeID = 0;
                Int32 ordinalStartDate = 0;
                Int32 ordinalEndDate = 0;

                try
                {
                    Command command = new Command(DB_REGION, "up_AuthorityCode_Get", 0);
                    command.AddParameter("@ItemTaxTypeID", SqlDbType.Int, ParameterDirection.Input, (int)taxType);
                    command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, RegionID);
                    dataReader = Client.Instance.ExecuteReader(command);

                    while (dataReader.Read())
                    {
                        if (!colLookupDone)
                        {
                            ordinalAuthorityID = dataReader.GetOrdinal("AuthorityID");
                            ordinalAuthorityCode = dataReader.GetOrdinal("AuthorityCode");
                            ordinalRate = dataReader.GetOrdinal("Rate");
                            ordinalRegionID = dataReader.GetOrdinal("RegionID");
                            ordinalItemTaxTypeID = dataReader.GetOrdinal("ItemTaxTypeID");
                            ordinalStartDate = dataReader.GetOrdinal("StartDate");
                            ordinalEndDate = dataReader.GetOrdinal("EndDate");
                            colLookupDone = true;
                        }

                        //create tax authority code objects
                        TaxAuthorityCode code = new TaxAuthorityCode();
                        code.AuthorityID = dataReader.GetInt32(ordinalAuthorityID);
                        code.AuthorityCode = dataReader.GetString(ordinalAuthorityCode);
                        code.ItemTaxType = (ItemTaxType)Enum.ToObject(typeof(ItemTaxType), dataReader.GetInt32(ordinalItemTaxTypeID));
                        //code.ItemTaxType = (ItemTaxType)dataReader.GetInt32(ordinalItemTaxTypeID);
                        code.RegionID = dataReader.GetInt32(ordinalRegionID);
                        code.TaxRate = Convert.ToDecimal(dataReader[ordinalRate]);
                        //code.TaxRate = dataReader.GetDecimal(ordinalRate);

                        taxCodes.Add(code);

                    }

                    //update instance cache
                    _AuthorityCodeCache[taxType].Add(RegionID, taxCodes);

                }
                catch (Exception ex)
                {
                    SqlException sqlException = ex.InnerException as SqlException;
                    if (sqlException != null)
                    {
                        throw new DALException("TaxDAL.GetTaxAuthorityCodes() error.", ex, ex.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                    }
                    else
                    {
                        throw new DALException("TaxDAL.GetTaxAuthorityCodes() error.", ex, ex.Message);
                    }
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Close();
                    }
                }
            }

            return taxCodes;
        }

        #endregion
    }
}
