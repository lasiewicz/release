using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Hydra;

using Spark.Purchase.BusinessLogic;
using Spark.Purchase.ValueObjects.Account;


namespace Spark.Purchase.Harness
{
    public partial class Form1 : Form
    {
        HydraWriter _hydraWriter = null;

        public Form1()
        {
            InitializeComponent();

            _hydraWriter = new HydraWriter("mnPurchase");
            _hydraWriter.Start();


            txt_Account_Save_DataCard.Text = "memberID:1\r\n" +
                "groupID:1\r\n" +
                "providerPaymentID:1\r\n" +
                "firstName:firstName\r\n" +
                "lastName:lastName\r\n" +
                "phoneNumber:phoneNumber\r\n" +
                "addressLine1:addressLine1\r\n" +
                "addressLine2:addressLine2\r\n" +
                "city:city\r\n" +
                "state:state\r\n" +
                "postalCode:postalCode\r\n" +
                "countryRegionID:223\r\n" +
                "creditCardExpirationMonth:1\r\n" +
                "creditCardExpirationYear:2008\r\n" +
                "creditCardType:Visa\r\n" +
                "accountNumber:1234\r\n" +
                "accountStatus:Active";

            txt_Account_Save_DataCheck.Text = "memberID:1\r\n" +
                "groupID:1\r\n" +
                "providerPaymentID:1\r\n" +
                "firstName:firstName\r\n" +
                "lastName:lastName\r\n" +
                "driversLicenseNumber:driversLicenseNumber\r\n" +
                "driversLicenseState:st\r\n" +
                "phoneNumber:phoneNumber\r\n" +
                "addressLine1:addressLine1\r\n" +
                "addressLine2:addressLine2\r\n" +
                "city:city\r\n" +
                "state:state\r\n" +
                "postalCode:postalCode\r\n" +
                "countryRegionID:223\r\n" +
                "accountNumber:accountNumber\r\n" +
                "routingNumber:routingNumber\r\n" +
                "accountStatus:Active";
        }

        private void btn_Account_Get_OK_Click(object sender, EventArgs e)
        {
            txt_Account_Get_Result.Text = "";

            AccountBase[] accountItems = AccountBL.Get(Convert.ToInt32(txt_Account_Get_MemberID.Text),
                Convert.ToInt32(txt_Account_Get_GroupID.Text));

            txt_Account_Get_Result.Text = "(" + accountItems.Length.ToString() + " items)\r\n";

            foreach (AccountBase accountBase in accountItems)
            {
                txt_Account_Get_Result.Text += accountBase.GetType().Name + ", " + accountBase.AccountID.ToString() + "\r\n";
            }
        }

        private void btn_Account_Save_OK_Click(object sender, EventArgs e)
        {
            string[] args;

            if (tabControl_Account_Save.SelectedIndex == 0)
            {
                args = txt_Account_Save_DataCard.Text.Replace("\r", "").Split('\n');
                txt_Account_Save_Result.Text = AccountBL.SaveCard(Convert.ToInt32(getArgValue(args, 0)),
                    Convert.ToInt32(getArgValue(args, 1)),
                    Convert.ToInt32(getArgValue(args, 2)),
                    getArgValue(args, 3),
                    getArgValue(args, 4),
                    getArgValue(args, 5),
                    getArgValue(args, 6),
                    getArgValue(args, 7),
                    getArgValue(args, 8),
                    getArgValue(args, 9),
                    getArgValue(args, 10),
                    Convert.ToInt32(getArgValue(args, 11)),
                    Convert.ToByte(getArgValue(args, 12)),
                    Convert.ToInt16(getArgValue(args, 13)),
                    (CreditCardType)Enum.Parse(typeof(CreditCardType), getArgValue(args, 14)),
                    getArgValue(args, 15),
                    (AccountStatusType)Enum.Parse(typeof(AccountStatusType), getArgValue(args, 16))).ToString();
            }
            else
            {
                args = txt_Account_Save_DataCheck.Text.Replace("\r", "").Split('\n');
                txt_Account_Save_Result.Text = AccountBL.SaveCheck(Convert.ToInt32(getArgValue(args, 0)),
                    Convert.ToInt32(getArgValue(args, 1)),
                    Convert.ToInt32(getArgValue(args, 2)),
                    getArgValue(args, 3),
                    getArgValue(args, 4),
                    getArgValue(args, 5),
                    getArgValue(args, 6),
                    getArgValue(args, 7),
                    getArgValue(args, 8),
                    getArgValue(args, 9),
                    getArgValue(args, 10),
                    getArgValue(args, 11),
                    getArgValue(args, 12),
                    Convert.ToInt32(getArgValue(args, 13)),
                    getArgValue(args, 14),
                    getArgValue(args, 15),
                    (AccountStatusType)Enum.Parse(typeof(AccountStatusType), getArgValue(args, 16))).ToString();
            }
        }

        private string getArgValue(string[] args,
            Int32 ordinal)
        {
            string val = args[ordinal];
            Int32 index = val.IndexOf(":");

            if (index > -1)
            {
                return val.Substring(index + 1);
            }

            return val;
        }
    }
}