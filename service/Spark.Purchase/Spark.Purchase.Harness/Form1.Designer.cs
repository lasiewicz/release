namespace Spark.Purchase.Harness
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.txt_Account_Save_Result = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Account_Get_OK = new System.Windows.Forms.Button();
            this.txt_Account_Get_GroupID = new System.Windows.Forms.TextBox();
            this.txt_Account_Get_MemberID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Account_Get_Result = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_Account_Save_OK = new System.Windows.Forms.Button();
            this.tabControl_Account_Save = new System.Windows.Forms.TabControl();
            this.tab_Account_Save_Card = new System.Windows.Forms.TabPage();
            this.tabtab_Account_Save_Check = new System.Windows.Forms.TabPage();
            this.txt_Account_Save_DataCard = new System.Windows.Forms.TextBox();
            this.txt_Account_Save_DataCheck = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.txt_Account_Save_Result.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl_Account_Save.SuspendLayout();
            this.tab_Account_Save_Card.SuspendLayout();
            this.tabtab_Account_Save_Check.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.txt_Account_Save_Result);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1155, 763);
            this.tabControl1.TabIndex = 0;
            // 
            // txt_Account_Save_Result
            // 
            this.txt_Account_Save_Result.Controls.Add(this.tabControl2);
            this.txt_Account_Save_Result.Location = new System.Drawing.Point(4, 22);
            this.txt_Account_Save_Result.Name = "txt_Account_Save_Result";
            this.txt_Account_Save_Result.Padding = new System.Windows.Forms.Padding(3);
            this.txt_Account_Save_Result.Size = new System.Drawing.Size(1147, 737);
            this.txt_Account_Save_Result.TabIndex = 0;
            this.txt_Account_Save_Result.Text = "BL";
            this.txt_Account_Save_Result.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1147, 737);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "SA";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(7, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1134, 723);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1126, 697);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Account";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_Account_Get_Result);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_Account_Get_MemberID);
            this.groupBox1.Controls.Add(this.txt_Account_Get_GroupID);
            this.groupBox1.Controls.Add(this.btn_Account_Get_OK);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(423, 420);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Get()";
            // 
            // btn_Account_Get_OK
            // 
            this.btn_Account_Get_OK.Location = new System.Drawing.Point(339, 13);
            this.btn_Account_Get_OK.Name = "btn_Account_Get_OK";
            this.btn_Account_Get_OK.Size = new System.Drawing.Size(75, 23);
            this.btn_Account_Get_OK.TabIndex = 0;
            this.btn_Account_Get_OK.Text = "ok";
            this.btn_Account_Get_OK.UseVisualStyleBackColor = true;
            this.btn_Account_Get_OK.Click += new System.EventHandler(this.btn_Account_Get_OK_Click);
            // 
            // txt_Account_Get_GroupID
            // 
            this.txt_Account_Get_GroupID.Location = new System.Drawing.Point(233, 16);
            this.txt_Account_Get_GroupID.Name = "txt_Account_Get_GroupID";
            this.txt_Account_Get_GroupID.Size = new System.Drawing.Size(100, 20);
            this.txt_Account_Get_GroupID.TabIndex = 1;
            // 
            // txt_Account_Get_MemberID
            // 
            this.txt_Account_Get_MemberID.Location = new System.Drawing.Point(71, 16);
            this.txt_Account_Get_MemberID.Name = "txt_Account_Get_MemberID";
            this.txt_Account_Get_MemberID.Size = new System.Drawing.Size(100, 20);
            this.txt_Account_Get_MemberID.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "MemberID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "GroupID:";
            // 
            // txt_Account_Get_Result
            // 
            this.txt_Account_Get_Result.Location = new System.Drawing.Point(9, 42);
            this.txt_Account_Get_Result.Multiline = true;
            this.txt_Account_Get_Result.Name = "txt_Account_Get_Result";
            this.txt_Account_Get_Result.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_Account_Get_Result.Size = new System.Drawing.Size(408, 372);
            this.txt_Account_Get_Result.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox1);
            this.groupBox2.Controls.Add(this.tabControl_Account_Save);
            this.groupBox2.Controls.Add(this.btn_Account_Save_OK);
            this.groupBox2.Location = new System.Drawing.Point(435, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(423, 420);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Save()";
            // 
            // btn_Account_Save_OK
            // 
            this.btn_Account_Save_OK.Location = new System.Drawing.Point(339, 13);
            this.btn_Account_Save_OK.Name = "btn_Account_Save_OK";
            this.btn_Account_Save_OK.Size = new System.Drawing.Size(75, 23);
            this.btn_Account_Save_OK.TabIndex = 0;
            this.btn_Account_Save_OK.Text = "ok";
            this.btn_Account_Save_OK.UseVisualStyleBackColor = true;
            this.btn_Account_Save_OK.Click += new System.EventHandler(this.btn_Account_Save_OK_Click);
            // 
            // tabControl_Account_Save
            // 
            this.tabControl_Account_Save.Controls.Add(this.tab_Account_Save_Card);
            this.tabControl_Account_Save.Controls.Add(this.tabtab_Account_Save_Check);
            this.tabControl_Account_Save.Location = new System.Drawing.Point(6, 42);
            this.tabControl_Account_Save.Name = "tabControl_Account_Save";
            this.tabControl_Account_Save.SelectedIndex = 0;
            this.tabControl_Account_Save.Size = new System.Drawing.Size(408, 346);
            this.tabControl_Account_Save.TabIndex = 1;
            // 
            // tab_Account_Save_Card
            // 
            this.tab_Account_Save_Card.Controls.Add(this.txt_Account_Save_DataCard);
            this.tab_Account_Save_Card.Location = new System.Drawing.Point(4, 22);
            this.tab_Account_Save_Card.Name = "tab_Account_Save_Card";
            this.tab_Account_Save_Card.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Account_Save_Card.Size = new System.Drawing.Size(400, 320);
            this.tab_Account_Save_Card.TabIndex = 0;
            this.tab_Account_Save_Card.Text = "card";
            this.tab_Account_Save_Card.UseVisualStyleBackColor = true;
            // 
            // tabtab_Account_Save_Check
            // 
            this.tabtab_Account_Save_Check.Controls.Add(this.txt_Account_Save_DataCheck);
            this.tabtab_Account_Save_Check.Location = new System.Drawing.Point(4, 22);
            this.tabtab_Account_Save_Check.Name = "tabtab_Account_Save_Check";
            this.tabtab_Account_Save_Check.Padding = new System.Windows.Forms.Padding(3);
            this.tabtab_Account_Save_Check.Size = new System.Drawing.Size(400, 320);
            this.tabtab_Account_Save_Check.TabIndex = 1;
            this.tabtab_Account_Save_Check.Text = "check";
            this.tabtab_Account_Save_Check.UseVisualStyleBackColor = true;
            // 
            // txt_Account_Save_DataCard
            // 
            this.txt_Account_Save_DataCard.Location = new System.Drawing.Point(6, 6);
            this.txt_Account_Save_DataCard.Multiline = true;
            this.txt_Account_Save_DataCard.Name = "txt_Account_Save_DataCard";
            this.txt_Account_Save_DataCard.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_Account_Save_DataCard.Size = new System.Drawing.Size(391, 308);
            this.txt_Account_Save_DataCard.TabIndex = 0;
            // 
            // txt_Account_Save_DataCheck
            // 
            this.txt_Account_Save_DataCheck.Location = new System.Drawing.Point(3, 3);
            this.txt_Account_Save_DataCheck.Multiline = true;
            this.txt_Account_Save_DataCheck.Name = "txt_Account_Save_DataCheck";
            this.txt_Account_Save_DataCheck.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_Account_Save_DataCheck.Size = new System.Drawing.Size(391, 302);
            this.txt_Account_Save_DataCheck.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 394);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1157, 765);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Spark.Purchase.Harness";
            this.tabControl1.ResumeLayout(false);
            this.txt_Account_Save_Result.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl_Account_Save.ResumeLayout(false);
            this.tab_Account_Save_Card.ResumeLayout(false);
            this.tab_Account_Save_Card.PerformLayout();
            this.tabtab_Account_Save_Check.ResumeLayout(false);
            this.tabtab_Account_Save_Check.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage txt_Account_Save_Result;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_Account_Get_Result;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Account_Get_MemberID;
        private System.Windows.Forms.TextBox txt_Account_Get_GroupID;
        private System.Windows.Forms.Button btn_Account_Get_OK;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_Account_Save_OK;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabControl tabControl_Account_Save;
        private System.Windows.Forms.TabPage tab_Account_Save_Card;
        private System.Windows.Forms.TextBox txt_Account_Save_DataCard;
        private System.Windows.Forms.TabPage tabtab_Account_Save_Check;
        private System.Windows.Forms.TextBox txt_Account_Save_DataCheck;
    }
}

