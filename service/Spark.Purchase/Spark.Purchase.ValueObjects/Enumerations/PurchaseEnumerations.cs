﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    #region Item Types

    /// <summary>
    /// The type of item
    /// </summary>
    [DataContract]
    public enum ItemType : int
    {
        None = 0,
        Services = 1,
        HardGoods = 2
    }

    #endregion

    #region Item Tax Types
    /// <summary>
    /// The tax category for an item
    /// </summary>
    [DataContract]
    public enum ItemTaxType : int
    {
        None = 0,
        Services = 1,
        HardGoods = 2
    }
    #endregion

    #region Currency Types

    /// <summary>
    /// The currency type used in the payment profile or charge  
    /// </summary>
    [DataContract]
    public enum CurrencyType : int
    {
        None = 0,
        USD = 1,
        EUR = 2,
        CAD = 3,
        GBP = 4,
        AUD = 5,
        ILS = 6
    }

    #endregion

    #region Package Status

    /// <summary>
    /// The status of the package  
    /// </summary>
    [DataContract]
    public enum PackageStatus : int
    {
        None = 0,
        Active = 1,
        Inactive = 2
    }

    #endregion

    /*
    #region System Types

    /// <summary>
    /// The type of the calling system    
    /// </summary>
    public enum SystemType : int
    {
        None = 0,
        System = 1,
        User = 2,
        VirtualTerminal = 3
    }

    #endregion
    */

    /*
    #region Order Types

    /// <summary>
    /// The type of order    
    /// </summary>
    public enum OrderType : int
    {
        None = 0
    }

    #endregion
    */


}
