﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    /// <summary>
    /// Represents a specific Tax Authority Code
    /// </summary>
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class TaxAuthorityCode
    {
        private int _AuthorityID = Constants.NULL_INT;
        private string _AuthorityCode = String.Empty;
        private ItemTaxType _ItemTaxType;
        private int _RegionID = Constants.NULL_INT;
        private decimal _TaxRate = Constants.NULL_DECIMAL;

        #region Constructor
        public TaxAuthorityCode()
        {
        }

        public TaxAuthorityCode(int AuthorityID, string AuthorityCode, ItemTaxType itemTaxType, int regionID, decimal taxRate)
        {
            _AuthorityID = AuthorityID;
            _AuthorityCode = AuthorityCode;
            _ItemTaxType = itemTaxType;
            _RegionID = regionID;
            _TaxRate = taxRate;
        }
        #endregion

        #region Properties
        /// <summary>
        /// This is ID representing the Authority that this tax is paid to.
        /// </summary>
        [DataMember]
        public int AuthorityID
        {
            get { return this._AuthorityID; }
            set { this._AuthorityID = value; }
        }

        /// <summary>
        /// This is the Code representing the Subunit of an Authority that this tax is paid for.
        /// </summary>
        [DataMember]
        public string AuthorityCode
        {
            get { return this._AuthorityCode; }
            set { this._AuthorityCode = value; }
        }

        /// <summary>
        /// This represents the Item's Tax Type (or category)
        /// </summary>
        [DataMember]
        public ItemTaxType ItemTaxType
        {
            get { return this._ItemTaxType; }
            set { this._ItemTaxType = value; }
        }

        /// <summary>
        /// The tax rate
        /// </summary>
        [DataMember]
        public decimal TaxRate
        {
            get { return this._TaxRate; }
            set { this._TaxRate = value; }
        }

        /// <summary>
        /// RegionID
        /// </summary>
        [DataMember]
        public int RegionID
        {
            get { return this._RegionID; }
            set { this._RegionID = value; }
        }
        #endregion
    }
}
