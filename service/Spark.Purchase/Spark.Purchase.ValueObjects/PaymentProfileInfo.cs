﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class PaymentProfileInfo
    {
        [DataMember]
        public string PaymentProfileID { get; set; }
        [DataMember]
        public string PaymentProfileType { get; set; }
        [DataMember]
        public bool IsDefault { get; set; }
        [DataMember]
        public int CallingsystemID { get; set; }
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public DateTime InsertDate { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
    }
}
