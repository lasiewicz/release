﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects.Responses
{
    public class OrderHistoryResponse : ResponseBase
    {
        public OrderHistoryResponse()
        {
            base.Version = 1;
            base.Code = 0;
            base.Message = string.Empty;
        }
        [DataMember]
        public List<OrderInfo> OrderHistory { get; set; }
    }
}
