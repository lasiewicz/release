﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects.Responses
{
    public abstract class ResponseBase
    {
        [DataMember]
        public int Version { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public String Message { get; set; }
    }
}
