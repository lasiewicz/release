﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects.Responses
{
    public class PaymentProfileMapperResponse : ResponseBase
    {
        public PaymentProfileMapperResponse()
        {
            base.Version = 1;
            base.Code = 0;
            base.Message = string.Empty;
        }
        [DataMember]
        public PaymentProfileInfo PaymentProfileInfo { get; set; }
        [DataMember]
        public bool IsProfileValid { get; set; }
    }
}
