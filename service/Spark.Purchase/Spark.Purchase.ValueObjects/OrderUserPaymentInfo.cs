﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class OrderUserPaymentInfo
    {
        [DataMember]
        public int OrderID { get; set; }
        [DataMember]
        public int ChargeID { get; set; }
        [DataMember]
        public DateTime InsertDate { get; set; }
        [DataMember]
        public int CallingSystemID { get; set; }
        [DataMember]
        public int CallingSystemTypeID { get; set; }
        [DataMember]
        public int ChargeTypeID { get; set; }
        [DataMember]
        public DateTime UpdateDate { get; set; }
        [DataMember]
        public int ChargeStatusID { get; set; }
    }
}
