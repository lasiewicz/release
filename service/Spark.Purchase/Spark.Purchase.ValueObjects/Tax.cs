﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    /// <summary>
    /// Represents Tax for an Item
    /// </summary>
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class Tax
    {
        #region Private Members
        private decimal _Amount = Constants.NULL_DECIMAL; //tax amount
        private decimal _ItemAmount = Constants.NULL_DECIMAL; //item amount

        //Unfortunately, data contracts can't expose properties that references a property of a composed object
        //So, we will only use TaxAuthorityCode as a data structure to populate the Tax object.
        //private TaxAuthorityCode _TaxAuthorityCode = null; 
        private int _AuthorityID = Constants.NULL_INT;
        private string _AuthorityCode = String.Empty;
        private ItemTaxType _ItemTaxType;
        private int _RegionID = Constants.NULL_INT;
        private decimal _TaxRate = Constants.NULL_DECIMAL;
        #endregion

        #region Constructors

        public Tax(decimal itemAmount, TaxAuthorityCode taxAuthorityCode)
        {
            this._AuthorityID = taxAuthorityCode.AuthorityID;
            this._AuthorityCode = taxAuthorityCode.AuthorityCode;
            this._ItemTaxType = taxAuthorityCode.ItemTaxType;
            this._RegionID = taxAuthorityCode.RegionID;
            this._TaxRate = taxAuthorityCode.TaxRate;
            this._ItemAmount = itemAmount;

            if (itemAmount != Constants.NULL_DECIMAL && _TaxRate != Constants.NULL_DECIMAL)
            {
                //calculate tax amount
                _Amount = Math.Round(itemAmount * _TaxRate, 2, MidpointRounding.ToEven);
            }
        }

        public Tax(decimal taxAmount, int authorityID, string authorityCode, int regionID, ItemTaxType itemTaxType, decimal taxRate)
        {
            this._Amount = taxAmount;
            this._AuthorityID = authorityID;
            this._AuthorityCode = authorityCode;
            this._RegionID = regionID;
            this._ItemTaxType = itemTaxType;
            this._TaxRate = taxRate;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Tax amount
        /// </summary>
        [DataMember]
        public decimal Amount
        {
            get { return this._Amount; }
            set { this._Amount = value; }
        }

        /// <summary>
        /// Item amount
        /// </summary>
        [DataMember]
        public decimal ItemAmount
        {
            get { return this._ItemAmount; }
            set { this._ItemAmount = value; }
        }

        /// <summary>
        /// Tax Rate
        /// </summary>
        [DataMember]
        public decimal TaxRate
        {
            get { return this._TaxRate; }
            set { this._TaxRate = value; }
        }

        /// <summary>
        /// This is ID representing the Authority that this tax is paid to.
        /// </summary>
        [DataMember]
        public int AuthorityID
        {
            get { return this._AuthorityID; }
            set { this._AuthorityID = value; }
        }

        /// <summary>
        /// This is the Code representing the Subunit of an Authority that this tax is paid for.
        /// </summary>
        [DataMember]
        public string AuthorityCode
        {
            get { return this._AuthorityCode; }
            set { this._AuthorityCode = value; }
        }

        /// <summary>
        /// Region ID
        /// </summary>
        [DataMember]
        public int RegionID
        {
            get { return this._RegionID; }
            set { this._RegionID = value; }
        }

        /// <summary>
        /// This represents the Item's Tax Type (or category)
        /// </summary>
        [DataMember]
        public ItemTaxType ItemTaxType
        {
            get { return this._ItemTaxType; }
            set { this._ItemTaxType = value; }
        }

        #endregion
    }
}
