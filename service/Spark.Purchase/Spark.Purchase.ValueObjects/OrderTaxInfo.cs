﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class OrderTaxInfo
    {
        [DataMember]
        public int OrderTaxID { get; set; }
        [DataMember]
        public int OrderDetailID { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public int CurrencyID { get; set; }
        [DataMember]
        public int AuthorityID { get; set; }
        [DataMember]
        public string AuthorityCode { get; set; }
        [DataMember]
        public DateTime InsertDate { get; set; }
        [DataMember]
        public int RegionID { get; set; }
        [DataMember]
        public int ItemTaxTypeID { get; set; }
    }
}
