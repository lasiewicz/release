﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class Package
    {
        #region Private Members

        private int _id = Constants.NULL_INT;
        private string _description = Constants.NULL_STRING;
        private int _systemID = Constants.NULL_INT;
        private PackageStatus _status = PackageStatus.None;
        private DateTime _startDate = DateTime.MinValue;
        private DateTime _expiredDate = DateTime.MinValue;
        private CurrencyType _currencyType = CurrencyType.None;
        private List<Item> _items = new List<Item>();

        #endregion

        #region Constructors

        public Package()
        {

        }

        #endregion

        #region Properties

        [DataMember]
        public int ID
        {
            get { return this._id; }
            set { this._id = value; }
        }
        [DataMember]
        public string Description
        {
            get { return this._description; }
            set { this._description = value; }
        }
        [DataMember]
        public int SystemID
        {
            get { return _systemID; }
            set { this._systemID = value; }
        }
        [DataMember]
        public PackageStatus Status
        {
            get { return this._status; }
            set { this._status = value; }
        }
        [DataMember]
        public DateTime StartDate
        {
            get { return this._startDate; }
            set { this._startDate = value; }
        }
        [DataMember]
        public DateTime ExpiredDate
        {
            get { return this._expiredDate; }
            set { this._expiredDate = value; }
        }
        [DataMember]
        public CurrencyType CurrencyType
        {
            get { return this._currencyType; }
            set { this._currencyType = value; }
        }
        [DataMember]
        public List<Item> Items
        {
            get { return this._items; }
            set { this._items = value; }
        }
       
        #endregion

        #region Public Methods

        public void AddItem(Item item)
        {
            if (!this._items.Contains(item))
            {
                this._items.Add(item);
            }
        }
        public void RemoveItem(Item item)
        {
            if (this._items.Contains(item))
            {
                this._items.Remove(item);
            }
        }

        public decimal GetSubTotalAmount(bool isRenewal)
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            decimal total = 0.0m;

            foreach (Item item in this._items)
            {
                total += item.GetSubTotalAmount(isRenewal);
            }

            return (total < 0.0m ? 0.0m : total);
        }

        public decimal GetTaxTotalAmount()
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            decimal total = 0.0m;

            foreach (Item item in this._items)
            {
                total += item.GetTaxTotalAmount();
            }

            return (total < 0.0m ? 0.0m : total);
        }

        public decimal GetTotalAmount(bool isRenewal)
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            decimal total = GetSubTotalAmount(isRenewal) + GetTaxTotalAmount();
            return (total < 0.0m ? 0.0m : total);
        }

        #endregion
    }
}
