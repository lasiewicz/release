﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class OrderDetailInfo
    {
        public OrderDetailInfo()
        {
            this.OrderTax = new List<OrderTaxInfo>();
        }

        [DataMember]
        public int OrderDetailID { get; set; }
        [DataMember]
        public int OrderId { get; set; }
        [DataMember]
        public int ReferenceOrderDetailID { get; set; }
        [DataMember]
        public int ItemID { get; set; }
        [DataMember]
        public int PackageID { get; set; }
        [DataMember]
        public int OrderTypeID { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public int CurrencyID { get; set; }
        [DataMember]
        public int DurationTypeID { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public int PromoID { get; set; }
        [DataMember]
        public DateTime InsertDate { get; set; }
        [DataMember]
        public DateTime UpdateDate { get; set; }
        [DataMember]
        public string ItemDescription { get; set; }
        [DataMember]
        public List<OrderTaxInfo> OrderTax { get; set; }
    }
}
