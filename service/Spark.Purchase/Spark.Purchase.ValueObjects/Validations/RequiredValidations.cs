﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.Purchase.ValueObjects
{
    public class RequiredValidations
    {
        public static string[] GetRequiredValidations(TransactionType transactionType, SystemType systemType, PaymentType paymentType, int systemID)
        {
            ValidationType[] arrValidationTypes = GetValidations(transactionType, systemType, paymentType, systemID);
            List<string> validationsToSend = new List<string>();

            // Required to maintain the order of the validations 
            foreach (ValidationType vt in arrValidationTypes)
            {
                validationsToSend.Add(Enum.GetName(typeof(ValidationType), vt));
            }

            return validationsToSend.ToArray();
        }

        /// <summary>
        /// This call returns the validations mapped for each of the transaction types supported by the engine
        /// </summary>
        /// <param name="transactionType">an enum that defines the trantype</param>
        /// <returns>array of validationtypes to run for the transaction</returns>
        public static ValidationType[] GetValidations(TransactionType transactionType, SystemType systemType, PaymentType paymentType, int systemID)
        {
            List<ValidationType> validations = new List<ValidationType>();

            switch (systemType)
            {
                case SystemType.VirtualTerminal:
                    switch (transactionType)
                    {
                        case TransactionType.InitialSubscriptionPurchase:
                            validations.Add(ValidationType.format);
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            validations.Add(ValidationType.maxSystemAmount);
                            break;
                        case TransactionType.Authorization:
                            validations.Add(ValidationType.format);
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                            }
                            break;
                        case TransactionType.PaymentProfileAuthorization:
                            validations.Add(ValidationType.format);
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.TrialTakenInitialSubscription:
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.Credit:
                        case TransactionType.Void:
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.GiftPurchase:
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.Renewal:
                        case TransactionType.AdditionalSubscriptionPurchase:
                        case TransactionType.AdditionalNonSubscriptionPurchase:
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        default:
                            throw new ApplicationException("The system does not support this type of transaction");
                    }
                    break;
                case SystemType.System:
                case SystemType.User:
                    switch (transactionType)
                    {
                        case TransactionType.InitialSubscriptionPurchase:
                            validations.Add(ValidationType.format);
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            validations.Add(ValidationType.maxSystemAmount);
                            break;
                        case TransactionType.Authorization:
                            validations.Add(ValidationType.format);
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                            }
                            break;
                        case TransactionType.PaymentProfileAuthorization:
                            validations.Add(ValidationType.format);
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.TrialTakenInitialSubscription:
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.Credit:
                        case TransactionType.Void:
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.GiftPurchase:
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                                validations.Add(ValidationType.binBlocking);
                                validations.Add(ValidationType.velocity);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        case TransactionType.Renewal:
                        case TransactionType.AdditionalNonSubscriptionPurchase:
                        case TransactionType.AdditionalSubscriptionPurchase:
                            if (paymentType == PaymentType.CreditCard)
                            {
                                validations.Add(ValidationType.whitelist);
                                validations.Add(ValidationType.blacklist);
                            }
                            validations.Add(ValidationType.dupTransaction);
                            break;
                        default:
                            throw new ApplicationException("The system does not support this type of transaction");
                    }
                    break;
                default:
                    throw new ApplicationException("The system does not support this type of system type");
            }

            return validations.ToArray<ValidationType>();
        }
    }
}
