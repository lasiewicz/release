﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class OrderInfo
    {
        public OrderInfo()
        {
            this.OrderDetail = new List<OrderDetailInfo>();
            this.OrderUserPayment = new List<OrderUserPaymentInfo>();
        }

        [DataMember]
        public int OrderID { get; set; }
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public int ReferenceOrderID { get; set; }
        [DataMember]
        public int CallingSystemID { get; set; }
        [DataMember]
        public int CallingSystemTypeID { get; set; }
        [DataMember]
        public int OrderStatusID { get; set; }
        [DataMember]
        public string UserPaymentGuid { get; set; }
        [DataMember]
        public decimal TotalAmount { get; set; }
        [DataMember]
        public int CurrencyID { get; set; }
        [DataMember]
        public DateTime InsertDate { get; set; }
        [DataMember]
        public DateTime UpdateDate { get; set; }
        [DataMember]
        public int InternalResponseStatusID { get; set; }
        [DataMember]
        public int OrderReasonID { get; set; }
        [DataMember]
        public int PmotionID { get; set; }
        [DataMember]
        public int PromoID { get; set; }
        [DataMember]
        public string CustomerIP { get; set; }
        [DataMember]
        public int AdminUserID { get; set; }
        [DataMember]
        public string AdminUserName { get; set; }
        [DataMember]
        public int RegionID { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public int DurationTypeID { get; set; }
        [DataMember]
        public List<OrderDetailInfo> OrderDetail { get; set; }
        [DataMember]
        public List<OrderUserPaymentInfo> OrderUserPayment { get; set; }
    }
}
