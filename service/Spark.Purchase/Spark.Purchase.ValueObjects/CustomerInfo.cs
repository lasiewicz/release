﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class CustomerInfo
    {
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public int CallingSystemID { get; set; }
    }
}
