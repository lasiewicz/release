﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    /// <summary>
    /// Represents a specific Region Vertical(e.g. PostalCode, City, State/Province, Country)
    /// </summary>
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class RegionHierarchy
    {
        private string _PostalCode;
        private int _StateRegionID;
        private string _StateAbbreviation;
        private string _StateDescription;
        private int _CityRegionID;
        private string _CityName;
        private int _PostalCodeRegionID;
        private int _CountryRegionID;
        private string _CountryAbbreviation;
        private string _CountryName;
        private int _HighestDepthRegionTypeID;
        private decimal _Longitude;
        private decimal _Latitude;
        private int _Depth1RegionID;

        #region Public Properties

        /// <summary>
        /// PostalCode
        /// </summary>
        [DataMember]
        public string PostalCode
        {
            get { return _PostalCode; }
            set { _PostalCode = value; }
        }

        /// <summary>
        /// StateRegionID
        /// </summary>
        [DataMember]
        public int StateRegionID
        {
            get { return _StateRegionID; }
            set { _StateRegionID = value; }
        }

        /// <summary>
        /// StateAbbreviation
        /// </summary>
        [DataMember]
        public string StateAbbreviation
        {
            get { return _StateAbbreviation; }
            set { _StateAbbreviation = value; }
        }

        /// <summary>
        /// StateDescription
        /// </summary>
        [DataMember]
        public string StateDescription
        {
            get { return _StateDescription; }
            set { _StateDescription = value; }
        }

        /// <summary>
        /// PostalCodeRegionID
        /// </summary>
        [DataMember]
        public int PostalCodeRegionID
        {
            get { return _PostalCodeRegionID; }
            set { _PostalCodeRegionID = value; }
        }

        /// <summary>
        /// CityRegionID
        /// </summary>
        [DataMember]
        public int CityRegionID
        {
            get { return _CityRegionID; }
            set { _CityRegionID = value; }
        }

        /// <summary>
        /// CityName
        /// </summary>
        [DataMember]
        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; }
        }

        /// <summary>
        /// CountryRegionID
        /// </summary>
        [DataMember]
        public int CountryRegionID
        {
            get { return _CountryRegionID; }
            set { _CountryRegionID = value; }
        }

        /// <summary>
        /// CountryAbbreviation
        /// </summary>
        [DataMember]
        public string CountryAbbreviation
        {
            get { return _CountryAbbreviation; }
            set { _CountryAbbreviation = value; }
        }

        /// <summary>
        /// CountryName
        /// </summary>
        [DataMember]
        public string CountryName
        {
            get { return _CountryName; }
            set { _CountryName = value; }
        }

        /// <summary>
        /// HighestDepthRegionTypeID
        /// </summary>
        [DataMember]
        public int HighestDepthRegionTypeID
        {
            get { return _HighestDepthRegionTypeID; }
            set { _HighestDepthRegionTypeID = value; }
        }

        /// <summary>
        /// Longitude
        /// </summary>
        [DataMember]
        public decimal Longitude
        {
            get { return _Longitude; }
            set { _Longitude = value; }
        }

        /// <summary>
        /// Latitude
        /// </summary>
        [DataMember]
        public decimal Latitude
        {
            get { return _Latitude; }
            set { _Latitude = value; }
        }

        /// <summary>
        /// Depth1RegionID
        /// </summary>
        [DataMember]
        public int Depth1RegionID
        {
            get { return _Depth1RegionID; }
            set { _Depth1RegionID = value; }
        }

        #endregion
    }
}
