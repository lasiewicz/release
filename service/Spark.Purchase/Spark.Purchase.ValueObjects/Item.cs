﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class Item
    {
        #region Private Members

        private int _id = Constants.NULL_INT;
        private string _description = Constants.NULL_STRING;
        private ItemType _itemType = ItemType.None;
        private DurationType _defaultDurationType = DurationType.None;
        private int _defaultDuration = Constants.NULL_INT;
        private decimal _defaultAmount = Constants.NULL_DECIMAL;
        private CurrencyType _defaultCurrencyType = CurrencyType.None;
        private bool _isRenewable = false;
        private DurationType _durationType = DurationType.None;
        private int _duration = Constants.NULL_INT;
        private decimal _amount = Constants.NULL_DECIMAL;
        private List<Tax> _taxes = new List<Tax>();
        private List<ItemTaxType> _itemTaxTypes = new List<ItemTaxType>(); //Listing of tax types for an item, if empty, it indicates the item is not taxable
        //private decimal _taxTotalAmount = Constants.NULL_DECIMAL;
        //private decimal _totalAmount = Constants.NULL_DECIMAL;
        private CurrencyType _currencyType = CurrencyType.None;
        private DurationType _renewalDurationType = DurationType.None;
        private int _renewalDuration = Constants.NULL_INT;
        private decimal _renewalAmount = Constants.NULL_DECIMAL;
        private CurrencyType _renewalCurrencyType = CurrencyType.None;
        private int _referenceItemID = Constants.NULL_INT;

        #endregion

        #region Constructors

        public Item()
        {

        }

        #endregion

        #region Properties
        [DataMember]
        public int ID
        {
            get { return this._id; }
            set { this._id = value; }
        }
        [DataMember]
        public string Description
        {
            get { return this._description; }
            set { this._description = value; }
        }
        [DataMember]
        public ItemType ItemType
        {
            get { return this._itemType; }
            set { this._itemType = value; }
        }
        [DataMember]
        public DurationType DefaultDurationType
        {
            get { return this._defaultDurationType; }
            set { this._defaultDurationType = value; }
        }
        [DataMember]
        public int DefaultDuration
        {
            get { return this._defaultDuration; }
            set { this._defaultDuration = value; }
        }
        [DataMember]
        public decimal DefaultAmount
        {
            get { return this._defaultAmount; }
            set
            {
                if (value >= 0.0m)
                {
                    this._defaultAmount = value;
                }
            }
        }
        [DataMember]
        public CurrencyType DefaultCurrencyType
        {
            get { return this._defaultCurrencyType; }
            set { this._defaultCurrencyType = value; }
        }
        [DataMember]
        public bool IsRenewable
        {
            get { return this._isRenewable; }
            set { this._isRenewable = value; }
        }
        [DataMember]
        public DurationType DurationType
        {
            get { return this._durationType; }
            set { this._durationType = value; }
        }
        [DataMember]
        public int Duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }
        [DataMember]
        public decimal Amount
        {
            get { return this._amount; }
            set
            {
                if (value >= 0.0m)
                {
                    this._amount = value;
                }
            }
        }
        [DataMember]
        public List<Tax> Taxes
        {
            get { return this._taxes; }
            set { this._taxes = value; }
        }
        [DataMember]
        public List<ItemTaxType> ItemTaxTypes
        {
            get { return this._itemTaxTypes; }
            set { this._itemTaxTypes = value; }
        }
        [DataMember]
        public CurrencyType CurrencyType
        {
            get { return this._currencyType; }
            set { this._currencyType = value; }
        }
        [DataMember]
        public DurationType RenewalDurationType
        {
            get { return this._renewalDurationType; }
            set { this._renewalDurationType = value; }
        }
        [DataMember]
        public int RenewalDuration
        {
            get { return this._renewalDuration; }
            set { this._renewalDuration = value; }
        }
        [DataMember]
        public decimal RenewalAmount
        {
            get { return this._renewalAmount; }
            set { this._renewalAmount = value; }
        }
        [DataMember]
        public CurrencyType RenewalCurrencyType
        {
            get { return this._renewalCurrencyType; }
            set { this._renewalCurrencyType = value; }
        }
       
        [DataMember]
        public int ReferenceItemID
        {
            get { return _referenceItemID; }
            set { _referenceItemID = value; }
        }


        #endregion

        #region Public Methods

        public void AddTax(Tax taxItem)
        {
            if (!this._taxes.Contains(taxItem))
            {
                this._taxes.Add(taxItem);
            }
        }
        public void RemoveTax(Tax taxItem)
        {
            if (this._taxes.Contains(taxItem))
            {
                this._taxes.Remove(taxItem);
            }
        }

        public decimal GetSubTotalAmount(bool isRenewal)
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            if (isRenewal)
            {
                return (this._renewalAmount == Constants.NULL_DECIMAL ? 0.0m : this._renewalAmount);
            }
            else
            {
                return (this._amount == Constants.NULL_DECIMAL ? 0.0m : this._amount);
            }
        }

        public decimal GetTaxTotalAmount()
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            decimal total = 0.0m;

            foreach (Tax taxItem in this._taxes)
            {
                total += taxItem.Amount;
            }

            return (total < 0.0m ? 0.0m : total);

        }

        public decimal GetTotalAmount(bool isRenewal)
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            decimal total = GetSubTotalAmount(isRenewal) + GetTaxTotalAmount();
            return (total < 0.0m ? 0.0m : total);

        }

        #endregion


    }
}
