﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Data.Hydra;

namespace Spark.Purchase.ValueObjects
{
    public class HydraWriterWrapper
    {
        public static readonly HydraWriterWrapper Instance = new HydraWriterWrapper();
        private static HydraWriter hydraWriter;
        private static bool isRunning = false;

        private HydraWriterWrapper()
        {
            hydraWriter = new HydraWriter(new string[] { "epOrder", "epProductService", "epRegion", "epValidation" });
            isRunning = false;
        }

        public void HydraWriterStart()
        {
            if (!isRunning)
            {
                //ExceptionUtility.LogException(new BLException("Hydrawriter started 1", new Exception("x"), Spark.UnifiedPurchaseSystem.Lib.Common.ExceptionLevel.Fatal), typeof(HydraWriterWrapper), "Hydrawriter started 2");
                hydraWriter.Start();
                isRunning = true;
            }
        }

        public void HydraWriterStop()
        {
            if (isRunning)
            {
                //ExceptionUtility.LogException(new BLException("Hydrawriter stopped 1", new Exception("x"), Spark.UnifiedPurchaseSystem.Lib.Common.ExceptionLevel.Fatal), typeof(HydraWriterWrapper), "Hydrawriter stopped 2");
                hydraWriter.Stop();
                isRunning = false;
            }
        }
    }
}
