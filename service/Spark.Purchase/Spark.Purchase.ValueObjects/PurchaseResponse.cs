﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;
using System.Reflection;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Name = "purchaseResponse", Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class PurchaseResponse
    {
        #region Private Members

        private InternalResponse _internalResponse = null;
        private Cart _userCart = null;
        private string[] _requiredValidations = null;
        private string _orderID = null;
        
        #endregion

        #region Constructors

        public PurchaseResponse()
        {

        }

        #endregion

        #region Properties

        [DataMember(EmitDefaultValue = false, Order = 1)]
        public InternalResponse InternalResponse
        {
            get { return this._internalResponse; }
            set { this._internalResponse = value; }
        }

        [DataMember(Name = "userCart", Order = 2)]
        public Cart UserCart 
        {
            get { return this._userCart; }
            set { this._userCart = value; }
        }

        [DataMember(Name = "requiredValidations", EmitDefaultValue = false, Order = 3)]
        public string[] RequiredValidations
        {
            get { return this._requiredValidations; }
            set { this._requiredValidations = value; }
        }

        [DataMember(Name = "orderID", EmitDefaultValue = false, Order = 4)]
        public string OrderID
        {
            get { return this._orderID; }
            set { this._orderID = value; }
        }

        #endregion
    }

    public class PurchaseResponseFactory
    {
        public static PurchaseResponse GetResponse(Cart memberCart, ResponseCode code)
        {
            return null;
            //return new PurchaseResponse { Code = (int)code, Message = "", MemberCart = memberCart };
        }

        private string GetCodeMessage(ResponseCode code)
        {
            switch (code)
            {
                case ResponseCode.Success:
                    return "The operation was successful.";
                default:
                    return "";
            }
        }
    }

    public enum ResponseCode
    {
        Success = 0,

    }


    [DataContract]
    public class DataContractHelper
    {
        [DataMember]
        public PurchaseResponse _purchaseResponse = null;

        [DataMember]
        public PaymentType _paymentType = PaymentType.None;

        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            List<Type> knownTypes = new List<Type>();

            knownTypes.Add(typeof(PurchaseResponse));
            knownTypes.Add(typeof(PaymentType));

            return knownTypes;
        }
    }
}
