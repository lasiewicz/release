﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;

namespace Spark.Purchase.ValueObjects.Credit
{
    /// <summary>
    /// This represents the response from Purchase for Credit or Void processing
    /// </summary>
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class CreditOrderResponse
    {
        private OrderInfo _OrderInfo = new OrderInfo();
        private InternalResponse _internalResponse = new InternalResponse();

        [DataMember]
        public OrderInfo OrderInfo
        {
            get { return _OrderInfo; }
            set { _OrderInfo = value; }
        }

        [DataMember(EmitDefaultValue = false)]
        public InternalResponse InternalResponse
        {
            get { return this._internalResponse; }
            set { this._internalResponse = value; }
        }
    }
}
