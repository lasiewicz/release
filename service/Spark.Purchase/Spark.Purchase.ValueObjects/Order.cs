﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class Order
    {
        #region Private Members

        private int _orderID = Constants.NULL_INT;
        private int _userID = Constants.NULL_INT;
        private int _systemID = Constants.NULL_INT;
        private SystemType _callingSystemType = SystemType.None;
        private List<Package> _packages = new List<Package>();
        private int _serviceID = Constants.NULL_INT;
        private int _promotionID = Constants.NULL_INT;
        private CurrencyType _currencyType = CurrencyType.None;
        private int _regionID = Constants.NULL_INT;
        // Set amount that does not change once the cart is saved as an order  
        private decimal _subTotalAmount = Constants.NULL_DECIMAL;
        // Set amount that does not change once the cart is saved as an order  
        private decimal _taxTotalAmount = Constants.NULL_DECIMAL;
        // Set amount that does not change once the cart is saved as an order  
        private decimal _totalAmount = Constants.NULL_DECIMAL;
        private int _referenceOrderID = Constants.NULL_INT;
        private int _adminID = Constants.NULL_INT;
        private string _adminName = String.Empty;
        private int _orderReasonID = Constants.NULL_INT;
        private string _paymentProfileID = Constants.NULL_STRING;
        private string _orderAttributes = Constants.NULL_STRING;

        #endregion

        #region Constructors

        public Order()
        {

        }

        public Order(Cart cart, string paymentProfileID, bool isRenewal)
        {
            //this._orderID = orderID;
            this._userID = cart.UserID;
            this._systemID = cart.SystemID;
            this._callingSystemType = cart.CallingSystemType;
            this._packages = cart.Packages;
            this._serviceID = cart.ServiceID;
            this._promotionID = cart.PromotionID;
            this._currencyType = cart.CurrencyType;
            this._regionID = cart.RegionID;
            this._subTotalAmount = cart.GetSubTotalAmount(isRenewal);
            this._taxTotalAmount = cart.GetTaxTotalAmount();
            this._totalAmount = cart.GetTotalAmount(isRenewal);
            this._adminID = cart.AdminID;
            this._adminName = cart.AdminName;
            this._orderReasonID = cart.OrderReasonID;
            this._paymentProfileID = paymentProfileID;
            this._orderAttributes = cart.OrderAttributes;
        }


        #endregion

        #region Properties
        [DataMember]
        public int OrderID
        {
            get { return this._orderID; }
            set { this._orderID = value; }
        }
        [DataMember]
        public int UserID
        {
            get { return this._userID; }
            set { this._userID = value; }
        }
        [DataMember]
        public int SystemID
        {
            get { return this._systemID; }
            set { this._systemID = value; }
        }
        [DataMember]
        public SystemType CallingSystemType
        {
            get { return this._callingSystemType; }
            set { this._callingSystemType = value; }
        }
        //public int PackageID
        //{
        //    get { return this._packageID; }
        //    set { this._packageID = value; }
        //}
        [DataMember]
        public List<Package> Packages
        {
            get { return this._packages; }
            set { this._packages = value; }
        }
        [DataMember]
        public int ServiceID
        {
            get { return this._serviceID; }
            set { this._serviceID = value; }
        }
        [DataMember]
        public int PromotionID
        {
            get { return this._promotionID; }
            set { this._promotionID = value; }
        }
        [DataMember]
        public CurrencyType CurrencyType
        {
            get { return this._currencyType; }
            set { this._currencyType = value; }
        }
        [DataMember]
        public int RegionID
        {
            get { return this._regionID; }
            set { this._regionID = value; }
        }
        [DataMember]
        public decimal SubTotalAmount
        {
            get { return this._subTotalAmount; }
            set { this._subTotalAmount = value; }
        }
        [DataMember]
        public decimal TaxTotalAmount
        {
            get { return this._taxTotalAmount; }
            set { this._taxTotalAmount = value; }
        }
        [DataMember]
        public decimal TotalAmount
        {
            get { return this._totalAmount; }
            set { this._totalAmount = value; }
        }
        [DataMember]
        public int ReferenceOrderID
        {
            get { return this._referenceOrderID; }
            set { this._referenceOrderID = value; }
        }
        [DataMember]
        public int AdminID
        {
            get { return this._adminID; }
            set { this._adminID = value; }
        }
        [DataMember]
        public string AdminName
        {
            get { return this._adminName; }
            set { this._adminName = value; }
        }
        [DataMember]
        public int OrderReasonID
        {
            get { return this._orderReasonID; }
            set { this._orderReasonID = value; }
        }
        [DataMember]
        public string PaymentProfileID
        {
            get { return this._paymentProfileID; }
            set { this._paymentProfileID = value; }
        }
        [DataMember]
        public string OrderAtthibutes
        {
            get { return this._orderAttributes; }
            set { this._orderAttributes = value; }
        }

        /*
        public decimal SubTotalAmount
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            get
            {
                decimal total = 0.0m;

                foreach (Package item in this._packages)
                {
                    total += item.SubTotalAmount;
                }

                return (total < 0.0m ? 0.0m : total);
            }
        }
        public decimal TaxTotalAmount
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            get
            {
                decimal total = 0.0m;

                foreach (Package item in this._packages)
                {
                    total += item.TaxTotalAmount;
                }

                return (total < 0.0m ? 0.0m : total);
            }
        }
        public decimal TotalAmount
        {
            // Precise amount
            // If displayed by the calling application than it will need to be rounded by the
            // calling application before displaying  
            get
            {
                decimal total = SubTotalAmount + TaxTotalAmount;
                return (total < 0.0m ? 0.0m : total);
            }
        }
        */

        #endregion

        #region Public Methods

        public void AddPackage(Package packageItem)
        {
            if (!this._packages.Contains(packageItem))
            {
                this._packages.Add(packageItem);
            }
        }
        public void RemovePackage(Package packageItem)
        {
            if (this._packages.Contains(packageItem))
            {
                this._packages.Remove(packageItem);
            }
        }

        #endregion


    }
}
