﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Purchase.ValueObjects
{
    public sealed class PurchaseConstants
    {
        private PurchaseConstants()
        {

        }

        public const string CONTRACT_NAMESPACE = "http://spark.net/";
    }
}
