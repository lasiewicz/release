﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Runtime.Serialization;

namespace Spark.Purchase.ValueObjects
{
    [DataContract(Namespace = PurchaseConstants.CONTRACT_NAMESPACE)]
    public class Cart
    {
        #region Private Members

        private int _userID = Constants.NULL_INT;
        private int _systemID = Constants.NULL_INT;
        private SystemType _callingSystemType = SystemType.None;
        //private int _packageID = Constants.NULL_INT;
        private List<Package> _packages = new List<Package>();
        private int _serviceID = Constants.NULL_INT;
        private int _promotionID = Constants.NULL_INT;
        private int _regionID = Constants.NULL_INT;
        private CurrencyType _currencyType = CurrencyType.None;
        private int _adminID = Constants.NULL_INT;
        private int _orderReasonID = Constants.NULL_INT;
        private string _adminName = String.Empty;
        private string _orderAttributes = String.Empty;
        #endregion

        #region Constructors

        public Cart()
        {

        }

        #endregion

        #region Properties
        [DataMember]
        public int UserID
        {
            get { return this._userID; }
            set { this._userID = value; }
        }
        [DataMember]
        public int SystemID
        {
            get { return this._systemID; }
            set { this._systemID = value; }
        }
        [DataMember]
        public SystemType CallingSystemType
        {
            get { return this._callingSystemType; }
            set { this._callingSystemType = value; }
        }
        //public int PackageID
        //{
        //    get { return this._packageID; }
        //    set { this._packageID = value; }
        //}
        [DataMember]
        public List<Package> Packages
        {
            get { return this._packages; }
            set { this._packages = value; }
        }
        [DataMember]
        public int ServiceID
        {
            get { return this._serviceID; }
            set { this._serviceID = value; }
        }
        [DataMember]
        public int PromotionID
        {
            get { return this._promotionID; }
            set { this._promotionID = value; }
        }
        [DataMember]
        public int RegionID
        {
            get { return this._regionID; }
            set { this._regionID = value; }
        }
        [DataMember]
        public CurrencyType CurrencyType
        {
            get
            {
                return this._currencyType;
            }
            set
            {
                // Required for setting the CurrencyType during deserialization 
                this._currencyType = value;
            }
        }
        [DataMember]
        public String OrderAttributes
        {
            get
            {
                return this._orderAttributes;
            }
            set
            {
                // Required for setting the CurrencyType during deserialization 
                this._orderAttributes = value;
            }
        }

        [DataMember]
        public int AdminID
        {
            get { return this._adminID; }
            set { this._adminID = value; }
        }
        [DataMember]
        public string AdminName
        {
            get { return this._adminName; }
            set { this._adminName = value; }
        }
        [DataMember]
        public int OrderReasonID
        {
            get { return this._orderReasonID; }
            set { this._orderReasonID = value; }
        }

        #endregion

        #region Public Methods

        public void AddPackage(Package packageItem)
        {
            if (this.CurrencyType == CurrencyType.None || this.CurrencyType == packageItem.CurrencyType)
            {
                if (!this._packages.Contains(packageItem))
                {
                    this._packages.Add(packageItem);
                    if (this._currencyType == CurrencyType.None)
                        this._currencyType = packageItem.CurrencyType;
                }
            }
        }
        public void RemovePackage(int packageItem)
        {
            int index = this.Packages.FindIndex(o => o.ID == packageItem);
            if (index > -1)
            {
                this.Packages.RemoveAt(index);
            }
            if (this.Packages.Count == 0)
            {
                this.CurrencyType = CurrencyType.None;
            }
        }

        public decimal GetSubTotalAmount(bool isRenewal)
        {
            // Total amounts of the items without taxes
            // This amount is already rounded to 2 decimal places since the item amounts 
            // were previously rounded to 2 decimal places
            decimal total = 0.0m;

            foreach (Package item in this._packages)
            {
                total += item.GetSubTotalAmount(isRenewal);
            }

            return (total < 0.0m ? 0.0m : total);
        }

        public decimal GetTaxTotalAmount()
        {
            // Total amounts of the item taxes without the items  
            // This amount is already rounded to 2 decimal places since the item taxes   
            // were previously rounded to 2 decimal places
            decimal total = 0.0m;

            foreach (Package item in this._packages)
            {
                total += item.GetTaxTotalAmount();
            }

            return (total < 0.0m ? 0.0m : total);
        }

        public decimal GetTotalAmount(bool isRenewal)
        {
            // Total amounts of all the items and the item taxes 
            // This amount is already rounded to 2 decimal places since all the items and    
            // item taxes were previously rounded to 2 decimal places
            decimal total = GetSubTotalAmount(isRenewal) + GetTaxTotalAmount();
            return (total < 0.0m ? 0.0m : total);
        }

        #endregion
    }
}
