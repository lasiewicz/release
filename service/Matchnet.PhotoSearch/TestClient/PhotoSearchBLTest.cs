using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Matchnet.Geo;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.BusinessLogic;
using Matchnet.PhotoSearch.ServiceAdapters;

namespace TestClient
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnSearch = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnSearch
			// 
			this.btnSearch.Location = new System.Drawing.Point(136, 24);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(128, 24);
			this.btnSearch.TabIndex = 0;
			this.btnSearch.Text = "Search By Country";
			this.btnSearch.Click += new System.EventHandler(this.button1_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(136, 72);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(128, 24);
			this.button1.TabIndex = 1;
			this.button1.Text = "Search By My Region";
			this.button1.Click += new System.EventHandler(this.button1_Click_1);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(128, 112);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(164, 24);
			this.button2.TabIndex = 2;
			this.button2.Text = "Search By My Region SA";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(122, 158);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(164, 24);
			this.button3.TabIndex = 3;
			this.button3.Text = "Threaded test";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(128, 208);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(164, 24);
			this.button4.TabIndex = 4;
			this.button4.Text = "Clean PhotoStore";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(408, 341);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btnSearch);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				PhotoGalleryQuery query=new PhotoGalleryQuery();
				query.AgeMax=40;
				query.AgeMin=20;
				query.CommunityID=3 ;
				query.CountryRegionID=223;
				query.RegionType=SearchRegionType.country;
				query.GenderMask=9;
				query.InsertDays=7;
				query.ShowHiddenPhotosFlag=true;
				query.GuestFlag=false;

				PhotoGalleryResultList list= PhotoGalleryBL.Instance.Search(query);

		
			}
			catch(Exception ex)
			{MessageBox.Show(ex.Source + ":" + ex.Message);}
			
		}

		private void button1_Click_1(object sender, System.EventArgs e)
		{
				try
		 {
					//.651254 -2.130077

			 double latitude=.651254;
			 double longitude=-2.130077;
			 double radius=50;
			 bool metric=false;

			 PhotoGalleryQuery query=new PhotoGalleryQuery();
			 query.AgeMax=40;
			 query.AgeMin=20;
			 query.CommunityID=3 ;
			 query.CountryRegionID=223;
			 query.RegionType=SearchRegionType.myregion;
			 query.InsertDays=7;
			 query.ShowHiddenPhotosFlag=true;
			 query.GuestFlag=false;

			 GeoBox b=new GeoBox(new Coordinates(latitude,longitude),0.1,radius,metric);
			 query.SearchBox=b;
			 PhotoGalleryResultList list= PhotoGalleryBL.Instance.Search(query);

		 }
		 catch(Exception ex)
		 {MessageBox.Show(ex.Source + ":" + ex.Message);}
			
		
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			try
			{
				//.651254 -2.130077

				//double latitude=.651254;
				//double longitude=-2.130077;
				double radius=50;
				bool metric=false;
				int count=0;
				PhotoGalleryQuery query=new PhotoGalleryQuery();
				query.AgeMax=40;
				query.AgeMin=20;
				query.CommunityID=3 ;
				query.CountryRegionID=223;
				query.RegionID=3444696;
				query.GenderMask=9;
				query.RegionType=SearchRegionType.myregion;
				query.InsertDays=7;
				query.ShowHiddenPhotosFlag=true;
				query.GuestFlag=false;
				query.Radius=50;
				
				//GeoBox b=new GeoBox(new Coordinates(latitude,longitude),0.1,radius,metric);
				//query.SearchBox=b;
				ArrayList list= PhotoGallerySA.Instance.Search(query, 100066873, 1, 1,16,out count);

			}
			catch(Exception ex)
			{MessageBox.Show(ex.Source + ":" + ex.Message);}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			ThreadedTest test=new ThreadedTest();
			test.Show();
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
