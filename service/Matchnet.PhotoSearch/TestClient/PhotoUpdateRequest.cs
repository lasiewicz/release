using System;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Member.ValueObjects;


namespace TestClient
{
	/// <summary>
	/// Summary description for PhotoUpdateRequest.
	/// </summary>
	[Serializable]
	public class PhotoUpdateRequest
	{
		public int communityid=Int32.MinValue;
		public int memberid=Int32.MinValue;
		public Matchnet.Member.ValueObjects.Photos.PhotoUpdate update;
		public PhotoUpdateRequest()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
