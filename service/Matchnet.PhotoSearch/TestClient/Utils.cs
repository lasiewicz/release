using System;
using System.Xml;
using System.Xml.Serialization;

namespace TestClient
{
	/// <summary>
	/// Summary description for Utils.
	/// </summary>
	public class Utils
	{
		public Utils()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static object Deserialize(XmlNode doc, System.Type objType)
		{	XmlNodeReader reader=null;
			XmlSerializer ser;
			try
			{
				 reader = new XmlNodeReader(doc);
				 ser = new XmlSerializer(objType);
				object obj = ser.Deserialize(reader);

				return obj;
			}
			catch(Exception ex)
			{throw(ex);}
			finally{reader.Close();
				ser=null;
			}
		}
	}
}
