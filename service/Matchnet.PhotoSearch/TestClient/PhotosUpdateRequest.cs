using System;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Member.ValueObjects;


namespace TestClient
{
	/// <summary>
	/// Summary description for PhotoUpdateRequest.
	/// </summary>
	[Serializable]
	public class PhotosUpdateRequest
	{
		public int communityid=Int32.MinValue;
		public int memberid=Int32.MinValue;
		public Matchnet.Member.ValueObjects.Photos.PhotoUpdate[] update;
		public PhotosUpdateRequest()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}