using System;
using Matchnet.PhotoSearch.ValueObjects;

namespace TestClient
{
	/// <summary>
	/// Summary description for SearchRequest.
	/// </summary>
	/// 
	[Serializable]
	public class SearchRequest
	{
		public int pageNum=Int32.MinValue;
		public int pageSize=Int32.MinValue;
		public PhotoGalleryQuery query;

	}
}
