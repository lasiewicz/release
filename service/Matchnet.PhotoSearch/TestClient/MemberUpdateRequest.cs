using System;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Member.ValueObjects;

namespace TestClient
{
	/// <summary>
	/// Summary description for MemberUpdateRequest.
	/// </summary>
	[Serializable]
	public class MemberUpdateRequest
	{
		public int communityid=Int32.MinValue;
		public int memberid=Int32.MinValue;
		public MemberUpdate update;
		public MemberUpdateRequest()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
