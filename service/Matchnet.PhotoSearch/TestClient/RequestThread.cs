using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.ServiceDefinitions;
using Matchnet.PhotoSearch.ServiceAdapters;
using System.Net;

namespace TestClient
{
	/// <summary>
	/// Summary description for RequestThread.
	/// </summary>
	public delegate void LogSearchException(string file,string uri,SearchRequest r, Exception ex );
	public delegate void LogSearchResults(string file,string uri,ArrayList res, SearchRequest r, DateTime start,DateTime end);
	public delegate void LogAllResults(string file, int iterations,DateTime start, DateTime end);


	public delegate void LogSearchHTMLResults(string file,string uri,string html, DateTime start,DateTime end);
	public delegate void LogSearchHTMLException(string file,string uri, Exception ex );
	public enum TestRequestType
	{
		search=0,
		memberupdate,
		photoupdate,
		photoupdatearray
	}
	public class RequestThread
	{
		public  event LogSearchResults EventSearchResults;
		public  event LogSearchException EventSearchException;
		public  event LogAllResults EventAllResults;

		public  event LogSearchHTMLResults EventSearchHTMLResults;
		public  event LogSearchHTMLException EventSearchHTMLException;


		public int Iterations = 1;
		public string URI = "";
		public bool useSA=true;
		public string Logfile = "";
		public bool LogResults;
		public ArrayList request;
		public bool ExpandResults=false;

		public RequestThread(ArrayList req,  string uri,int iter,  string logFile, bool expandResults )
		{
			URI=uri;
			request=req;
			Iterations=iter;
			Logfile=logFile;
			if (Logfile!=String.Empty)
			{
				ResultsHandler handler = new ResultsHandler();
				EventSearchResults += new LogSearchResults(handler.OnSearchResults);
				EventAllResults += new LogAllResults(handler.OnAllResults);
				EventSearchException+=new LogSearchException(handler.OnSearchException);
				LogResults=true;
				
			}
			ExpandResults=expandResults;
			if(uri!=null && uri != String.Empty)
				useSA=false;

			
		}


		public RequestThread(ArrayList req, int iter,  string logFile)
		{
			
			request=req;
			Iterations=iter;
			Logfile=logFile;
			if (Logfile!=String.Empty)
			{
				ResultsHandler handler = new ResultsHandler();
				EventSearchHTMLResults += new LogSearchHTMLResults(handler.OnSearchHTMLResults);
				EventAllResults += new LogAllResults(handler.OnAllResults);
				EventSearchHTMLException+=new LogSearchHTMLException(handler.OnSearchHTMLException);
				LogResults=true;
				
			}
			
			

			
		}
		public  void workCycleSearch()
		{
			int numcycles;
			string xml = "";
		
			
			numcycles = Iterations;
			DateTime testStart = DateTime.Now;
			for (int i = 0; i < numcycles; i++)
			{
				for(int k=0; k < request.Count ;k++)
				{
					SearchRequest r=(SearchRequest) request[k];
					ArrayList res=null;
					DateTime start = DateTime.Now;
					int count=0;
					if (!useSA)
					{
						//Matchnet.PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.TestURI=URI;
					}
					else
					{
					//	Matchnet.PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.TestURI="";
					}

					try{
							
							res=Matchnet.PhotoSearch.ServiceAdapters.PhotoGallerySA.Instance.Search(r.query, 100066873, 1, r.pageNum,r.pageSize,out count);
						}
						 catch(Exception ex)
						{EventSearchException(Logfile,URI,r,ex);}
					
					DateTime end = DateTime.Now;
					if (LogResults)
						EventSearchResults(Logfile, URI, res, r, start, end);
				}
			}
			DateTime testEnd = DateTime.Now;
			EventAllResults(Logfile, Iterations, testStart,testEnd);
		}



		public  void workCycleHTTPSearch()
		{
			int numcycles;
			string xml = "";
		 int count=0;
			
			numcycles = Iterations;
			DateTime testStart = DateTime.Now;
			for (int i = 0; i < numcycles; i++)
			{
				for(int k=0; k < request.Count ;k++)
				{
					count+=1;
				
					string res="";
					DateTime start = DateTime.Now;
					
					
					try
					{
							
						res=SendFormHttpRequest((string) request[k]);
					}
					catch(Exception ex)
					{EventSearchHTMLException(Logfile,URI,ex);}
					
					DateTime end = DateTime.Now;
					if (LogResults)
						EventSearchHTMLResults(Logfile, URI,res, start, end);
				}
			}
			DateTime testEnd = DateTime.Now;
			EventAllResults(Logfile, count, testStart,testEnd);
		}

		private void PrepareLogEvents(string logfile)
		{
			if (logfile != String.Empty && logfile !=null)
			{
				ResultsHandler handler = new ResultsHandler();
				EventSearchResults += new LogSearchResults(handler.OnSearchResults);
				EventAllResults += new LogAllResults(handler.OnAllResults);
				Logfile = logfile;
				LogResults = true;
			}
		}

		public string SendFormHttpRequest(string url)
		{
			string html = "";
			ASCIIEncoding encoding = new ASCIIEncoding();
			int pos=url.IndexOf("?");
			string postdata=url.Substring(pos + 1);

			 byte[] data = encoding.GetBytes(postdata);

			HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
			
			req.Method = "POST";
			req.ContentType = "application/x-www-form-urlencoded";
			  req.ContentLength = data.Length;
			Stream newStream = req.GetRequestStream();
			// Send the data.
			newStream.Write(data, 0, data.Length);
           
			HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
			
				Stream stream = resp.GetResponseStream();
				StreamReader reader = new StreamReader(stream);
				 html = reader.ReadToEnd(); 

				
			
			newStream.Close();
			reader.Close();
			stream.Close();
			
			return html;

		}
	}


	
	public class ResultsHandler
	{
		public void OnSearchException(string file,  string uri, SearchRequest r, Exception ex)
		{
				TextWriter writer=null;
			try
			{
				
				writer = new StreamWriter(file, true);
				
				
				writer.NewLine = "\r\n";
				
				writer.WriteLine(uri);
				
				writer.NewLine = "\r\n";
				writer.WriteLine("pagenum:" + r.pageNum);
				writer.WriteLine("pagesize:" + r.pageSize);
				writer.Write(r.query.ToString());
				
				writer.WriteLine("Exception:" + ex.ToString());
				writer.WriteLine("===============================================================");
				writer.WriteLine("");
				
			}
			catch (Exception e)
			{ }
			finally
			{writer.Close();}
		}

		public void OnSearchResults(string file, string uri, ArrayList res, SearchRequest r, DateTime start, DateTime end)
		{TextWriter writer=null;
			try
			{
				
				 writer = new StreamWriter(file, true);
				
				TimeSpan t = new TimeSpan(end.Ticks - start.Ticks);
				writer.NewLine = "\r\n";
				writer.WriteLine("pagenum:" + r.pageNum);
				writer.WriteLine("pagesize:" + r.pageSize);
				writer.Write(r.query.ToString());
				
				writer.WriteLine(uri);
				if(res != null)
				{
					for(int i=0;i< res.Count;i++)
					{
						PhotoResultItem item=(PhotoResultItem) res[i];
						writer.WriteLine(String.Format("Memberid:{0}, MemberPhotoID {1}", item.MemberID,item.MemberPhotoID));
					
					}
				}
				else
				{writer.WriteLine("Null results");}
				
				string interval = String.Format("Start:{0:t} - End:{1:t}, Duration: {2} (ms)", start, end, t.TotalMilliseconds);
				writer.WriteLine(interval);
				writer.WriteLine("");
				writer.WriteLine("===============================================================");
				writer.WriteLine("");
				
			}
			catch (Exception ex)
			{ }
			finally
			{writer.Close();}
		}

		public void OnAllResults(string file,int iter,DateTime start, DateTime end)
		{TextWriter writer =null;
			try
			{
				 writer = new StreamWriter(file, true);
				
					TimeSpan t = new TimeSpan(end.Ticks - start.Ticks);
					writer.NewLine = "\r\n";
					writer.WriteLine("");
					writer.WriteLine("");
					writer.WriteLine("Test Completed");
					writer.WriteLine(String.Format("Request count:{0}",iter));
					string interval = String.Format("Started:{0:t} - Ended:{1:t}, Duration: {2} (sec), Duration per req: {3}", start, end, t.TotalSeconds, t.TotalSeconds/iter);
					writer.WriteLine(String.Format(interval));
					writer.WriteLine("");
					writer.WriteLine("===============================================================");
					writer.WriteLine("");
			
			}
			catch (Exception ex)
			{ }
			finally
			{writer.Close();}
		}


		public void OnSearchHTMLException(string file,  string uri,  Exception ex)
		{
			TextWriter writer=null;
			try
			{
				
				writer = new StreamWriter(file, true);
				
				
				writer.NewLine = "\r\n";
				
				writer.WriteLine(uri);
				
				writer.NewLine = "\r\n";
								
				writer.WriteLine("Exception:" + ex.ToString());
				writer.WriteLine("===============================================================");
				writer.WriteLine("");
				
			}
			catch (Exception e)
			{ }
			finally
			{writer.Close();}
		}


		public void OnSearchHTMLResults(string file, string uri, string html, DateTime start, DateTime end)
		{
				TextWriter writer=null;
			try
			{
				
				writer = new StreamWriter(file, true);
				
				TimeSpan t = new TimeSpan(end.Ticks - start.Ticks);
				writer.NewLine = "\r\n";
			
				
				writer.WriteLine(uri);
				if(html != "" && html != null)
				{
					writer.WriteLine("received html");
				}
				else
				{writer.WriteLine("Null results");}
				
				string interval = String.Format("Start:{0:t} - End:{1:t}, Duration: {2} (ms)", start, end, t.TotalMilliseconds);
				writer.WriteLine(interval);
				writer.WriteLine("");
				writer.WriteLine("===============================================================");
				writer.WriteLine("");
				
			}
			catch (Exception ex)
			{ }
			finally
			{writer.Close();}
		}

		private bool CheckXML(string xml)
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xml);
				return true;

			}
			catch (Exception ex)
			{
				return false;
			}

		}
	}
}
