using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Matchnet.Data;

namespace TestClient
{
	/// <summary>
	/// Summary description for Photostore.
	/// </summary>
	public class Photostore : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtReqFile;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnClean;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Photostore()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtReqFile = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnClean = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(-96, 202);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 5;
			this.label1.Text = "Req. File";
			// 
			// txtReqFile
			// 
			this.txtReqFile.Location = new System.Drawing.Point(144, 40);
			this.txtReqFile.Name = "txtReqFile";
			this.txtReqFile.Size = new System.Drawing.Size(368, 20);
			this.txtReqFile.TabIndex = 3;
			this.txtReqFile.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(24, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 24);
			this.label2.TabIndex = 6;
			this.label2.Text = "PhotoShare";
			// 
			// btnClean
			// 
			this.btnClean.Location = new System.Drawing.Point(216, 112);
			this.btnClean.Name = "btnClean";
			this.btnClean.Size = new System.Drawing.Size(136, 64);
			this.btnClean.TabIndex = 7;
			this.btnClean.Text = "btnClean";
			this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
			// 
			// Photostore
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(568, 245);
			this.Controls.Add(this.btnClean);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtReqFile);
			this.Name = "Photostore";
			this.Text = "Photostore";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnClean_Click(object sender, System.EventArgs e)
		{
			try
			{
				Command comm=new Command();
				comm.LogicalDatabaseName="mnPhotoStore";
				//comm.

			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
	}
}
