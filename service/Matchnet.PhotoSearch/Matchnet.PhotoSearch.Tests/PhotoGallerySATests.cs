﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PhotoSearch.ServiceAdapters;
using NUnit.Framework;

namespace Matchnet.PhotoSearch.Tests
{
    public class PhotoGallerySATests
    {
        [Test]
        public void TestRandomize()
        {
            var list = new ArrayList {0,1,2,3,4,5,6,7,8,9};
            var randomizedList = PhotoGallerySA.Instance.RandomizeResults(list);
            var numbersInDifferentPosition = 0;
            
            for (int i = 0; i < list.Count; i++ )
            {
                if(list[i] != randomizedList[i])
                {
                    numbersInDifferentPosition++;
                }
            }

            Assert.IsTrue(numbersInDifferentPosition >= 3);
            Assert.IsTrue(randomizedList.Count == 10);
        }
    }
}
