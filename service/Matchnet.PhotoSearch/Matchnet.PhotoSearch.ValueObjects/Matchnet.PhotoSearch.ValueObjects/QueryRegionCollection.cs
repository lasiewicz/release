using System;
using System.Collections;
using Matchnet;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for QueryRegionCollection.
	/// </summary>
	[Serializable]
	public class QueryRegionCollection:System.Collections.CollectionBase,IValueObject,ICacheable
	{	
		private int _cacheTTLSeconds = 180;
		private const string CACHE_KEY_PREFIX = "PhotoQueryRegionCollection";
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
		
		int siteID;
		
		public QueryRegion this[int index]
		{
			get { return ((QueryRegion)base.InnerList[index]); }
		}
		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="memberPayment">banner to add to collection</param>
		/// <returns></returns>
		public int Add(QueryRegion region)
		{
			return base.InnerList.Add(region);
		}
		public int SiteID
		{
			get{return siteID;}
			set{siteID=value;}
		}
		

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{
			return CACHE_KEY_PREFIX + siteID.ToString() ;
		}
		#endregion

	}
}
