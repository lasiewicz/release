using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for QueryRegion.
	/// </summary>
	public class QueryRegion:IValueObject
	{
		int regionID;
		bool defaultFlag;
		public QueryRegion(int regionid, bool defaultflag)
		{
			regionID=regionid;
			defaultFlag=defaultflag;
		}

		
		public int RegionID
		{
			get{return regionID;}
			set{regionID=value;}
		}

		public bool DefaultFlag
		{
			get{return defaultFlag;}
			set{defaultFlag=value;}
		}

	}
}
