using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for ResultItem.
	/// </summary>
	[Serializable]
	public class PhotoResultItem:IValueObject
	{
		int memberID;
		int memberPhotoID;
		DateTime photoInsertDate;
		PhotoGalleryMember galleryMember;

		public PhotoResultItem(int memberid, int photoid, DateTime photoinsertdate)
		{
			memberID=memberid;
			memberPhotoID=photoid;
			photoInsertDate=photoinsertdate;
		}

		public int MemberID
		{
			get{return memberID;}
			set{memberID=value;}
		}

		public int MemberPhotoID
		{
			get{return memberPhotoID;}
			set{memberPhotoID=value;}
		}

		public DateTime PhotoInsertDate
		{
			get{return photoInsertDate;}
			set{photoInsertDate=value;}
		}

		public PhotoGalleryMember GalleryMember
		{
			get{return galleryMember;}
			set{galleryMember=value;}
		}
	}
}
