using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for PhotoStore.
	/// </summary>
	[Serializable]
	public class PhotoStore:IValueObject,ICacheable,IReplicable
	{
		private int _cacheTTLSeconds = 180;
		private const string CACHE_KEY_PREFIX = "PhotoStore";
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

		int memberID;
		int memberPhotoID;
		int fileID;
		int communityID;
		DateTime birthDate;
		int gender;
		DateTime insertDate;
		bool privateFlag;
		int regionID;
		bool displayToGuestFlag;
		double latitude;
		double longitude;
		int depth1RegionID;
		int depth2RegionID;
		int depth3RegionID;
		int depth4RegionID;
		int boxX;
		int boxY;

		public PhotoStore()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int MemberID
		{ 
			get { return memberID;}
			set {memberID = value; }
		}

		public int MemberPhotoID
		{ 
			get { return memberPhotoID;}
			set {memberPhotoID = value; }
		}

		public int FileID
		{ 
			get { return fileID;}
			set {fileID = value; }
		}
		public int CommunityID
		{ 
			get { return communityID;}
			set {communityID = value; }
		}

		public DateTime BirthDate
		{ 
			get { return birthDate;}
			set {birthDate = value; }
		}

		public int Gender
		{ 
			get { return gender;}
			set {gender = value; }
		}
		public DateTime InsertDate
		{ 
			get { return insertDate;}
			set {insertDate = value; }
		}

		public bool PrivateFlag
		{ 
			get { return privateFlag;}
			set {privateFlag = value; }
		}

		public bool DisplayToGuestFlag
		{ 
			get { return displayToGuestFlag;}
			set {displayToGuestFlag = value; }
		}

		public int RegionID
		{ 
			get { return regionID;}
			set {regionID = value; }
		}

		public int Depth1RegionID
		{ 
			get { return depth1RegionID;}
			set {depth1RegionID = value; }
		}

		public int Depth2RegionID
		{ 
			get { return depth2RegionID;}
			set {depth2RegionID = value; }
		}
		public int Depth3RegionID
		{ 
			get { return depth3RegionID;}
			set {depth3RegionID = value; }
		}

		public int Depth4RegionID
		{ 
			get { return depth4RegionID;}
			set {depth4RegionID = value; }
		}
		public double Latitude
		{ 
			get { return latitude;}
			set {latitude = value; }
		}

		public double Longitude
		{ 
			get { return longitude;}
			set {longitude = value; }
		}

		public int BoxX
		{ 
			get { return boxX;}
			set {boxX = value; }
		}

		public int BoxY
		{ 
			get { return boxY;}
			set {boxY = value; }
		}

		
		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{
			return String.Format( CACHE_KEY_PREFIX + "_{0}_{1}" , memberID.ToString()  , memberPhotoID.ToString());
		}
		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
