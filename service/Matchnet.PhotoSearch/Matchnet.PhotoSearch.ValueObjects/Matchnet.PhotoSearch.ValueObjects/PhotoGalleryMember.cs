using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for ResultItem.
	/// </summary>
	[Serializable]
	public class PhotoGalleryMember:IValueObject
	{
		int memberID;
		DateTime birthDate;
		string userName;
		string thumbWebPath;
		string fileWebPath;
		int photoCount;
	
		
		public int MemberID
		{
			get{return memberID;}
			set{memberID=value;}
		}

		public int PhotoCount
		{
			get{return photoCount;}
			set{photoCount=value;}
		}

		public DateTime BirthDate
		{
			get{return birthDate;}
			set{birthDate=value;}
		}

		public string UserName
		{
			get{return userName;}
			set{userName=value;}
		}

		public string ThumbWebPath
		{
			get{return thumbWebPath;}
			set{thumbWebPath=value;}
		}
		public string FileWebPath
		{
			get{return fileWebPath;}
			set{fileWebPath=value;}
		}
	}
}
