using System;
using System.Collections;

using Matchnet;
using Matchnet.PhotoSearch.ValueObjects;



namespace Matchnet.PhotoSearch.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IPhotoSearchService.
	/// </summary>
	public interface IPhotoSearchService
	{
		 PhotoGalleryResultList Search(PhotoGalleryQuery query);
		 ArrayList RefreshSearchResults(PhotoGalleryQuery query);
		 QueryOptionCollection GetQueryOptionCollection(int siteid, QueryOptionType optiontype);
	}
}
