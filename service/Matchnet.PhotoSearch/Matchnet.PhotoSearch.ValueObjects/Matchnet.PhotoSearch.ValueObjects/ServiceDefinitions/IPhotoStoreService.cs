using System;
using System.Collections;
using Matchnet.PhotoSearch.ValueObjects;


namespace Matchnet.PhotoSearch.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IPhotoStoreService.
	/// </summary>
	public interface IPhotoStoreService
	{
		 void UpdateMember(PhotoStoreUpdate[] item);
		// void InsertPhoto(PhotoQueueItem item);
		 void SavePhoto(PhotoStoreUpdate item);
		 void SavePhotos(ArrayList items);
		 //void DeletePhoto(PhotoQueueItem item);
		 //void DeleteMemberPhotos(PhotoQueueItem item);
	}
}
