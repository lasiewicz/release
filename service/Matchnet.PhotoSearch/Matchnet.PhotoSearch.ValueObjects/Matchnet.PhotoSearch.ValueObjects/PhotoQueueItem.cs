using System;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for PhotoQueueItem.
	/// </summary>
	///

	[Serializable]
	public abstract class QueueItemBase : IValueObject
	{
		int communityid;

		public int CommunityID
		{
			get{return communityid;}
			set{communityid=value;}
		}
	}
	[Serializable]
	public class PhotoQueueItem:QueueItemBase
	{
		
		public PhotoStoreAction Action;
		public int memberid;
		 

		public PhotoUpdate photoUpdate;

	}
	
	[Serializable]
	public class MemberQueueItem:QueueItemBase
	{
	
		public int communityid;
		public MemberUpdate memberupdate;

	}
	
}
