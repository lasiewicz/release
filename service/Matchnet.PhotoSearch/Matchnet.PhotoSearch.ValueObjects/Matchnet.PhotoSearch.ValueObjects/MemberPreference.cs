using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for MemberPreference.
	/// </summary>
	public class MemberPreference:IValueObject
	{
		int siteID;
		int memberID;
		PhotoGalleryQuery query;
		public MemberPreference(int siteid, int memberid, PhotoGalleryQuery q)
		{
			siteID=siteid;
			memberID=memberid;
			query=q;
		}

		public int SiteID
		{
			get{return siteID;}
			set{siteID=value;}
		}

		public int MemberID
		{
				get{return memberID;}
			set{memberID=value;}
		}

		public PhotoGalleryQuery Query
		{
			get{return query;}
			set{query=value;}
		}


	}
}
