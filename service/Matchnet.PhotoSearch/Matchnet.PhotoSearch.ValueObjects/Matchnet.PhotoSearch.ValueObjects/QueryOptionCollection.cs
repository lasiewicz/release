using System;
using System.Collections;
using Matchnet;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for QueryRegionCollection.
	/// </summary>
	/// 
	public enum QueryOptionType
	{
		region=1,
		insertdays=2
	}
	[Serializable]
	public class QueryOptionCollection:System.Collections.CollectionBase,IValueObject,ICacheable
	{	
		private int _cacheTTLSeconds = 180;
		public const string CACHE_KEY_PREFIX = "PhotoQueryRegionCollection";
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
		
		int siteID;
		QueryOptionType optionType;
		int defaultOption;

		public QueryOptionCollection(int siteid,QueryOptionType optiontype)
		{ 
			siteID=siteid;
			optionType=optiontype;
		}
		
		public QueryOption this[int index]
		{
			get { return ((QueryOption)base.InnerList[index]); }
		}
		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="memberPayment">banner to add to collection</param>
		/// <returns></returns>
		public int Add(QueryOption option)
		{
			return base.InnerList.Add(option);
		}
		public int SiteID
		{
			get{return siteID;}
			set{siteID=value;}
		}
		
		public QueryOptionType OptionType
		{
			get{return optionType;}
			set{optionType=value;}
		}

		public int DefaultOption
		{
			get{return defaultOption;}
			set{defaultOption=value;}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{
			return CACHE_KEY_PREFIX + siteID.ToString() + ":" + (int)optionType ;
		}

		public static string GetCacheKey(int siteid, QueryOptionType option)
		{
			return CACHE_KEY_PREFIX + siteid.ToString() + ":" + (int) option;
		}
		
		#endregion

	}
}
