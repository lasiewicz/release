using System;
using System.Text;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for PhotoStore.
	/// </summary>
	/// 
	public enum PhotoStoreAction
	{
		none=0,
		insert,
		update,
		delete
	

	}
	[Serializable]
	public class PhotoStoreUpdate:IValueObject,ICacheable,IReplicable
	{
		private int _cacheTTLSeconds = 180;
		private const string CACHE_KEY_PREFIX = "PhotoStore";
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

		PhotoStoreAction action=PhotoStoreAction.none;

		int memberID=Int32.MinValue;
		int memberPhotoID=Int32.MinValue;
		int fileID=Int32.MinValue;
		int communityID=Int32.MinValue;
		DateTime birthDate=DateTime.MinValue;
		int gender=Int32.MinValue;
		DateTime insertDate=DateTime.MinValue;
		int privateFlag=Int32.MinValue;
		int regionID=Int32.MinValue;
		int displayToGuestFlag=Int32.MinValue;
		double latitude=double.MinValue;
		double longitude=double.MinValue;
		int depth1RegionID=Int32.MinValue;
		int depth2RegionID=Int32.MinValue;
		int depth3RegionID=Int32.MinValue;
		int depth4RegionID=Int32.MinValue;
		int boxX=Int32.MinValue;
		int boxY=Int32.MinValue;

		public PhotoStoreUpdate()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public PhotoStoreAction Action
		{ 
			get { return action;}
			set {action = value; }
		}

		public int MemberID
		{ 
			get { return memberID;}
			set {memberID = value; }
		}

		public int MemberPhotoID
		{ 
			get { return memberPhotoID;}
			set {memberPhotoID = value; }
		}

		public int FileID
		{ 
			get { return fileID;}
			set {fileID = value; }
		}
		public int CommunityID
		{ 
			get { return communityID;}
			set {communityID = value; }
		}

		public DateTime BirthDate
		{ 
			get { return birthDate;}
			set {birthDate = value; }
		}

		public int Gender
		{ 
			get { return gender;}
			set {gender = value; }
		}
		public DateTime InsertDate
		{ 
			get { return insertDate;}
			set {insertDate = value; }
		}

		public int PrivateFlag
		{ 
			get { return privateFlag;}
			set {privateFlag = value; }
		}

		public int DisplayToGuestFlag
		{ 
			get { return displayToGuestFlag;}
			set {displayToGuestFlag = value; }
		}

		public int RegionID
		{ 
			get { return regionID;}
			set {regionID = value; }
		}

		public int Depth1RegionID
		{ 
			get { return depth1RegionID;}
			set {depth1RegionID = value; }
		}

		public int Depth2RegionID
		{ 
			get { return depth2RegionID;}
			set {depth2RegionID = value; }
		}
		public int Depth3RegionID
		{ 
			get { return depth3RegionID;}
			set {depth3RegionID = value; }
		}

		public int Depth4RegionID
		{ 
			get { return depth4RegionID;}
			set {depth4RegionID = value; }
		}
		public double Latitude
		{ 
			get { return latitude;}
			set {latitude = value; }
		}

		public double Longitude
		{ 
			get { return longitude;}
			set {longitude = value; }
		}

		public int BoxX
		{ 
			get { return boxX;}
			set {boxX = value; }
		}

		public int BoxY
		{ 
			get { return boxY;}
			set {boxY = value; }
		}

		public override string ToString()
		{
			StringBuilder bld=new StringBuilder();
			try
			{
				bld.Append("memberID:" + memberID + "\r\n");
				bld.Append("CommunityID:" + communityID  + "\r\n");
				bld.Append("BirthDate:" + birthDate  + "\r\n");
				bld.Append("memberPhotoID:" + memberPhotoID  + "\r\n");
				bld.Append("fileID:" + fileID  + "\r\n");
				bld.Append("Gender:" + gender  + "\r\n");
				bld.Append("RegionID:" + regionID  + "\r\n");
				bld.Append("Latitude:" + latitude  + "\r\n");
				bld.Append("Longitude:" + longitude  + "\r\n");
				bld.Append("Depth1RegionID:" + depth1RegionID  + "\r\n");
				bld.Append("Depth2RegionID:" + depth2RegionID  + "\r\n");				
				bld.Append("Depth3RegionID:" + depth3RegionID  + "\r\n");
				bld.Append("Depth4RegionID:" + depth4RegionID  + "\r\n");
				bld.Append("BoxX:" + boxX  + "\r\n");
				bld.Append("BoxY:" + boxY  + "\r\n");
				bld.Append("Action:" + action.ToString()  + "\r\n");
				bld.Append("Private:" + privateFlag.ToString()  + "\r\n");
				bld.Append("DisplayoGuest:" + displayToGuestFlag.ToString()  + "\r\n");

				return bld.ToString();
				
			}
			catch(Exception ex)
			{return bld.ToString();}
			finally{bld=null;}
			
		}
		
		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{
			return String.Format( CACHE_KEY_PREFIX + "_{0}_{1}" , memberID.ToString()  , memberPhotoID.ToString());
		}
		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
