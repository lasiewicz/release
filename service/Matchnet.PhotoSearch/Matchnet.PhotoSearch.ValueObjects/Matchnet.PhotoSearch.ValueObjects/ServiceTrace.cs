using System;
using System.Diagnostics;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for Trace.
	/// </summary>
	[Serializable]
	public class ServiceTrace:IValueObject
	{	private const string  FORMAT_STR="App Source:{0}\r\n============\r\n Ex.Source:{1} - Ex.Msg:{2}\r\n============";
		private const string  FORMAT_STR_TRACE="App Source:{0}\r\n============\r\nTrace:{1}\r\n============";
		//private const string  FORMAT_STR="App Source:{0}\r\nEx.Source:{1}\r\nEx.Msg:{2}";
		public static readonly ServiceTrace Instance = new ServiceTrace();
		private ServiceTrace()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public void Trace(string appSource,Exception ex, IValueObject obj)
		{
			try
			{
				System.Diagnostics.Trace.Write(getTrace(appSource,ex,obj));
			}
			catch (Exception e)
			{e=null;}
		}

		public void Trace(string appSource,string trace)
		{
			try
			{
				System.Diagnostics.Trace.Write(String.Format(FORMAT_STR_TRACE,appSource,trace));
			}
			catch (Exception e)
			{e=null;}
		}

		public void DebugTrace(string appSource,string trace)
		{
			try
			{
				Trace(appSource,trace);
			}
			catch (Exception e)
			{e=null;}
		}
		public void DebugTrace(string appSource,Exception ex, IValueObject obj)
		{
			try
			{

			#if DEBUG
				Trace(appSource,ex,obj);
						
			#endif

			}
			catch (Exception e)
			{e=null;}
		}
		public string getTrace(string appSource,Exception ex, IValueObject obj)
		{  string exStr="";
		   string innerexStr="";
		   

			try
			{
				exStr=String.Format(FORMAT_STR,appSource,ex.Source,ex.Message);
				if (ex.InnerException != null)
					innerexStr=String.Format(FORMAT_STR,appSource,ex.InnerException.Source,ex.InnerException.Message);
				exStr+=innerexStr;
				if(obj != null)
					exStr+="\r\nValue Obj:" + obj.ToString();
				
				return exStr;


			}
			catch (Exception e)
			{
				return exStr;
				e=null;
			}
		}
	}
}
