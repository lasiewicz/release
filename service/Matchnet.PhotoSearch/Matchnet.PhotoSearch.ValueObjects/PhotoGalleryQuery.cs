using System;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.Geo;

namespace Matchnet.PhotoSearch.ValueObjects
{
	public enum SearchRegionType
	{
		anywhere=0,
		country,
		myregion
	}
	public enum ReLoadFlags
	{
		none=0,
		load
	}
    public enum SortByType
    {
        newest = 0,
        random = 1
    }
	/// <summary>
	/// Summary description for Class1.
	/// /// </summary>
	[Serializable]
	public class PhotoGalleryQuery :IValueObject,ICacheable
       {
		public  const string PHOTOGALL_SITE_ID="PHOTOGALL_SITE_ID";
		public  const string PHOTOGALL_COMMUNITY_ID="PHOTOGALL_COMMUNITY_ID";
		public  const string PHOTOGALL_GENDER="PHOTOGALL_GENDER";
		public  const string PHOTOGALL_REGIONID="PHOTOGALL_REGIONID";
		public  const string PHOTOGALL_COUNTRY_REGIONID="PHOTOGALL_COUNTRY_REGIONID";
		public  const string PHOTOGALL_RADIUS="PHOTOGALL_RADIUS";
		public  const string PHOTOGALL_INSERTDAYS="PHOTOGALL_INSERTDAYS";
		public  const string PHOTOGALL_AGEMIN="PHOTOGALL_AGEMIN";
		public  const string PHOTOGALL_AGEMAX="PHOTOGALL_AGEMAX";
		public  const string PHOTOGALL_GUEST_FLAG="PHOTOGALL_GUEST_FLAG";
		public  const string PHOTOGALL_SEARCH_TYPE="PHOTOGALL_SEARCH_TYPE";
		public  const string PHOTOGALL_SHOW_HIDDEN="PHOTOGALL_SHOW_HIDDEN";
		public  const string PHOTOGALL_RELOADFLAG="PHOTOGALL_RELOADFLAG";

		private const string CACHE_KEY_PREFIX = "PHOTOQUERY";
		private int _cacheTTLSeconds = 360;
		
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
		
		int siteID;
		int communityID;
		int genderMask;
		int gender;
		int ageMin;
		int	ageMax;
		int regionID;
		int countryRegionID;
		double radius;
		SearchRegionType regionType;
		GeoBox searchBox=null;
		int insertDays;
		DateTime insertDateMin=new DateTime();
		bool guestFlag=false;
		bool showHiddenFlag=true;
		int page=0;
		int pagesize=0;
		ReLoadFlags reloadFlag=ReLoadFlags.load;
        SortByType sortBy = SortByType.newest;
		
		public PhotoGalleryQuery()
		{
		}

		public int GenderMask
		{
			get{return genderMask;}
		
			set 
			{
				genderMask=value;
				gender=GenderUtils.GetGenderMaskSelf(genderMask);
			}
		}

		public int CommunityID
		{get{return communityID;}
			set{communityID=value;}
		}

		public int SiteID
		{
				get{return siteID;}
			set{siteID=value;}
		}

		public int Gender
		{get{return gender;}
			set{gender=value;}
		}

		public int AgeMin
		{
			get{return ageMin; }
			set{ ageMin=value; }
		}

		public int AgeMax
		{
			get{return ageMax; }
			set{ ageMax=value; }
		}
		public int RegionID
		{
			get{return regionID; }
			set{ regionID=value; }
		}

		public int CountryRegionID
		{
			get{return countryRegionID; }
			set{ countryRegionID=value; }
		}


		public double Radius
		{
			get{return radius; }
			set{ radius=value; }
		}

		public SearchRegionType RegionType
		{
			get{return regionType;}
			set{ regionType=value;}
		}
		
		public int InsertDays
		{
			get{return insertDays; }
			set{ insertDays=value; }
		}

		public DateTime InsertDateMin
		{
			get{return insertDateMin; }
			set{ insertDateMin=value; }
		}

		public bool GuestFlag
		{
			get{return guestFlag; }
			set{ guestFlag=value; }
		}
		public bool ShowHiddenPhotosFlag
		{
			get{return showHiddenFlag; }
			set{ showHiddenFlag=value; }
		}

		public GeoBox SearchBox
		{
			get{return searchBox;}
			set{searchBox=value;}
		}

        public SortByType SortBy
        {
            get { return sortBy; }
            set { sortBy = value; }
        }

		public string GetQueryCacheKey()
		{
			string key="";
			try
			{
				

				key=String.Format("{0}_CID:{1}G:{2}AMIN:{3}AMAX:{4}:STYPE:{5}RID:{6}CRID:{7}R:{8}DAYS:{9}GUEST:{10}HIDDEN:{11}", CACHE_KEY_PREFIX, communityID, gender,ageMin,ageMax,(int)regionType, regionID,countryRegionID,radius,insertDays,guestFlag.ToString(),showHiddenFlag.ToString());
				
				return key;
			}
			catch(Exception ex)
			{ throw (new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException));}

		}
		public int Page
		{
			get{return page;} 
			set{page=value;} 
		}
		public int PageSize
		{
			get{return pagesize;} 
			set{pagesize=value;} 
		}
		public ReLoadFlags ReloadFlag
		{
			get{return reloadFlag;}
			set{ reloadFlag=value;}
		}
		public override string ToString()
		{
			StringBuilder bld=new StringBuilder();
			try
			{
				bld.Append("AgeMin:" + AgeMin + "\r\n");
				bld.Append("AgeMax:" + AgeMax  + "\r\n");
				bld.Append("GenderMask:" + GenderMask  + "\r\n");
				bld.Append("Gender:" + Gender  + "\r\n");
				bld.Append("CommunityID:" + CommunityID  + "\r\n");
				bld.Append("SearchRegionType:" + RegionType.ToString()  + "\r\n");
				bld.Append("CountryRegionID:" + countryRegionID  + "\r\n");
				bld.Append("RegionID:" + regionID  + "\r\n");
				bld.Append("Radius:" + Radius  + "\r\n");
				bld.Append("Insertdays:" + InsertDays  + "\r\n");
				bld.Append("ShowHiddenPhotosFlags:" + ShowHiddenPhotosFlag  + "\r\n");
				bld.Append("GuestFlag:" + GuestFlag  + "\r\n");
				bld.Append("Radius:" + Radius  + "\r\n");
				bld.Append("Page:" + page  + "\r\n");
				bld.Append("PageSize:" + pagesize  + "\r\n");
				bld.Append("ReloadFlag:" + reloadFlag  + "\r\n");

				return bld.ToString();
				
			}
			catch(Exception ex)
			{return bld.ToString();}
			finally{bld=null;}
			
		}
	
		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{	
			return  CACHE_KEY_PREFIX +  ":" + siteID;
		}

		public static string GetCacheKey(int siteid)
		{	
			return  CACHE_KEY_PREFIX +  ":" + siteid;
		}
		#endregion
	}
}
