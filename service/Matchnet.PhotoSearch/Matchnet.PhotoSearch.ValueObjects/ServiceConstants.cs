using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for ServiceConstants.
	/// </summary>
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "PHOTOSEARCH_SVC";
		public const string SERVICE_NAME = "Matchnet.PhotoSearch.Service";
		public const string SEARCH_SERVICE_MANAGER_NAME = "PhotoGallerySM";
		public const string STORE_SERVICE_MANAGER_NAME = "PhotoStoreSM";
		public const string SERVICE_SA_CONNECTION_LIMIT="PHOTOSEARCHSVC_SA_CONNECTION_LIMIT";
		public const string SERVICE_RESULTS_CACHE_TTL="PHOTOSEARCHSVC_RESULTS_CACHE_TTL";
		public const string SERVICE_SA_HOST_OVERRIDE="PHOTOSEARCHSVC_SA_HOST_OVERRIDE";

		public const int DEFAULT_REGION_LANGUAGE=2;
		public const double DEFAULT_PARTITION_ANGLE=0.1;
		public ServiceConstants()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
