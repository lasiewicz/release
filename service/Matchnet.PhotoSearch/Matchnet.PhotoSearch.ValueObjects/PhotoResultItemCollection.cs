using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for PhotoResultItemCollection.
	/// </summary>
	[Serializable]
	public class PhotoResultItemCollection : System.Collections.CollectionBase, IValueObject
	{
		public PhotoResultItemCollection()
		{}

		public PhotoResultItem this[int index]
		{
			get { return ((PhotoResultItem)base.InnerList[index]); }
		}

		public int Add(PhotoResultItem item)
		{
			return base.InnerList.Add(item);
		}

	}
}
