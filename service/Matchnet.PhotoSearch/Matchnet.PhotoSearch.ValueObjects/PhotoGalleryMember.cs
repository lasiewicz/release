using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for ResultItem.
	/// </summary>
	[Serializable]
	public class PhotoGalleryMember:IValueObject
	{
		int memberID;
		DateTime birthDate;
		string userName;
		string thumbWebPath;
		string fileWebPath;
        string largeWebPath;
		int photoCount;
        string photoMoreDisplayText;
        string caption;
        string profileURL;
        string imURL;
        string emailURL;
        DateTime postedDate;
        string postedDateDisplayText;
        int age;
        string ageDisplayText;
        string ageLocationDisplayText;
        string maritalSeekingDisplayText;
        bool isOnline;
        bool isNew;
        bool isUpdated;
		
		public int MemberID
		{
			get{return memberID;}
			set{memberID=value;}
		}

		public int PhotoCount
		{
			get{return photoCount;}
			set{photoCount=value;}
		}

		public DateTime BirthDate
		{
			get{return birthDate;}
			set{birthDate=value;}
		}

		public string UserName
		{
			get{return userName;}
			set{userName=value;}
		}

		public string ThumbWebPath
		{
			get{return thumbWebPath;}
			set{thumbWebPath=value;}
		}
		public string FileWebPath
		{
			get{return fileWebPath;}
			set{fileWebPath=value;}
		}
        public string LargeWebPath
        {
            get { return largeWebPath; }
            set { largeWebPath = value; }
        }
        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }
        public string ProfileURL
        {
            get { return profileURL; }
            set { profileURL = value; }
        }
        public DateTime PostedDate
        {
            get { return postedDate; }
            set { postedDate = value; }
        }
        public string PostedDateDisplayText
        {
            get { return postedDateDisplayText; }
            set { postedDateDisplayText = value; }
        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        public string AgeDisplayText
        {
            get { return ageDisplayText; }
            set { ageDisplayText = value; }
        }
        public string AgeLocationDisplayText
        {
            get { return ageLocationDisplayText; }
            set { ageLocationDisplayText = value; }
        }
        public string MaritalSeekingDisplayText
        {
            get { return maritalSeekingDisplayText; }
            set { maritalSeekingDisplayText = value; }
        }
        public bool IsOnline
        {
            get { return isOnline; }
            set { isOnline = value; }
        }
        public bool IsNew
        {
            get { return isNew; }
            set { isNew = value; }
        }
        public bool IsUpdated
        {
            get { return isUpdated; }
            set { isUpdated = value; }
        }
        public string IMURL
        {
            get { return imURL; }
            set { imURL = value; }
        }
        public string EmailURL
        {
            get { return emailURL; }
            set { emailURL = value; }
        }
        public string PhotoMoreDisplayText
        {
            get { return photoMoreDisplayText; }
            set { photoMoreDisplayText = value; }
        }

        public bool IsFavorited { get; set; }
	    public bool IsHighlighted { get; set; }
	    public string Location { get; set; }
        public string LocationShort { get; set; }
	    
	}
}
