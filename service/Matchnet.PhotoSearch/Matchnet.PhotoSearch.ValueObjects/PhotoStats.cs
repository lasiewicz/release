using System;
using System.Collections;
using System.Collections.Specialized;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for PhotoStats.
	/// </summary>
	[Serializable]
	public class PhotoStats:IValueObject,ICacheable
	{
		private int _cacheTTLSeconds = 180;
		private const string CACHE_KEY_PREFIX = "PhotoGalleryStats";
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

		Hashtable _stats=null;
		int _hours=24;
		public PhotoStats()
		{
			
		}
		public PhotoStats(int hours)
		{
			_hours=hours;
			_stats=new Hashtable();
		}

		public Hashtable Stats
		{
			get{return _stats;}
			set{_stats=value;}
		}

		public int Hours
		{
			get{return _hours;}
			set{_hours=value;}
		}

		public void Add(int communityid, int count)
		{
			if(_stats== null)
				_stats=new Hashtable();

			_stats.Add(communityid,count);

		}


		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{	
			return  CACHE_KEY_PREFIX + "_" + _hours.ToString()  ;
		}

		public static string GetCacheKey(int hours)
		{	
			return  CACHE_KEY_PREFIX + "_" + hours.ToString()  ;
		}
		#endregion
	}
}
