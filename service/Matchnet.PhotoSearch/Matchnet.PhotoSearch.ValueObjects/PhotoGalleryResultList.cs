using System;
using System.Collections;



namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for ResultItemList.
	/// </summary>
	[Serializable]
	public class PhotoGalleryResultList:IValueObject,ICacheable
	{
		private int _cacheTTLSeconds = 300;
		private const string CACHE_KEY_PREFIX = "PhotoGalleryResultList";
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        private int _MatchesFound;

		string cacheKey;
		ArrayList results;

		public PhotoGalleryResultList(string querycachekey)
		{cacheKey=querycachekey;}

		public ArrayList PhotoGalleryResults
		{
			set{results=value;}
			get{return results;}
		}

		public  DateTime FirstListDate
		{
		get 
		  {
			 DateTime date=new DateTime();
			if(results != null && results.Count > 0)
			{
				date=((PhotoResultItem)results[0]).PhotoInsertDate;
			}
			  return date;
		  }
		}

        public int MatchesFound
        {
            get
            {
                return _MatchesFound;
            }
            set
            {
                _MatchesFound = value;
            }
        }

		public ArrayList ToMemberArray()
		{
			ArrayList members=new ArrayList();
			try
			{

				if(results== null )
					return members;

				for(int i=0; i < results.Count;i++)
				{
					int mid=((PhotoResultItem)results[0]).MemberID;
					members.Add(mid);
				}
				return members;
			}
			catch(Exception ex)
			{
				System.Diagnostics.Trace.Write("PhotoGalleryResultList.ToMemberArray exception:" + ex.Message);
				return members;
			}
		}
		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get	{return _cacheTTLSeconds;}
			set	{_cacheTTLSeconds = value;}
			
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get{	return _cacheItemMode;}
			set{	 _cacheItemMode=value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get	{return _cachePriority;	}
			set	{_cachePriority = value;}
		}


		public string GetCacheKey()
		{	
			return  cacheKey  ;
		}
		#endregion
	}
}
