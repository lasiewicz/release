using System;

namespace Matchnet.PhotoSearch.ValueObjects
{
	/// <summary>
	/// Summary description for QueryRegion.
	/// </summary>
	[Serializable]
	public class QueryOption:IValueObject
	{
		int optionValue;
		int listOrder;
		bool defaultFlag;
		
		public QueryOption(int optionvalue, int listorder, bool defaultflag)
		{
			optionValue=optionvalue;
			listOrder=listorder;
			defaultFlag=defaultflag;
		}

		
		public int OptionValue
		{
			get{return optionValue;}
			set{optionValue=value;}
		}

		public int ListOrder
		{
			get{return listOrder;}
			set{listOrder=value;}
		}

		public bool DefaultFlag
		{
			get{return defaultFlag;}
			set{defaultFlag=value;}
		}


	}
}
