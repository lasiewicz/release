using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.ServiceManagers;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.PhotoSearch.Service
{
	public class PhotoSearchService : Matchnet.RemotingServices.RemotingServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private  PhotoGallerySM _photogallerySM;
		private  PhotoStoreSM _photostoreSM;
		public PhotoSearchService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
			try
			{
				System.ServiceProcess.ServiceBase[] ServicesToRun;
	
				// More than one user Service may run within the same process. To add
				// another service to this process, change the following line to
				// create a second service object. For example,
				//
				//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
				//
				ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PhotoSearchService() };

				System.ServiceProcess.ServiceBase.Run(ServicesToRun);
			}
			catch(Exception ex)
				{ ServiceBoundaryException e= new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);}
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			try
			{
				components = new System.ComponentModel.Container();
				this.ServiceName = ServiceConstants.SERVICE_NAME;
				
			}
		
		catch(Exception ex)
				{ServiceBoundaryException e= new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);}
		}

		protected override void RegisterServiceManagers()
		{
			try
			{
				_photogallerySM = new PhotoGallerySM();
				base.RegisterServiceManager(_photogallerySM);

				_photostoreSM = new PhotoStoreSM();
				base.RegisterServiceManager(_photostoreSM);

			
			
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{	components.Dispose();}
			}

			if (_photogallerySM != null) 
			{	_photogallerySM.Dispose();}

			if (_photostoreSM != null) 
			{	_photostoreSM.Dispose();}

			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			// TODO: Add code here to start your service.
			try
			{
				base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
				base.OnStart(args);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
			 base.OnStop();
		}
	}
}
