using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.ServiceDefinitions;

namespace Matchnet.PhotoSearch.ServiceAdapters
{
	/// <summary>
	/// Summary description for PhotoStoreSA.
	/// </summary>
	public class PhotoStoreSA:SABase
	{
		private const string MODULE_NAME="PhotoStoreSA";
		private const int GROUP_PERSONALS = 8383;
		private const string  ATTR_REGIONID="RegionID";
		private const string  ATTR_GENDERMASK="GenderMask";
		private const string  ATTR_HIDEMASK="HideMask";
		private const string  ATTR_SELFSUSPEND="SelfSuspendedFlag";
		private const string  ATTR_DISPLAYPHOTOSTOGUEST="DisplayPhotosToGuests";
		private const string  ATTR_BIRTHDATE="Birthdate";
		private const string  ATTR_SUSPEND_MASK="SuspendMask";
		private const string  ATTR_GLOBAL_STATUS_MASK="GlobalStatusMask";

		enum AttributeOptionHideMask : int
		{
			HideSearch = 1,
			HideHotLists = 2,
			HideMembersOnline = 4,
			HidePhotos = 8
		}
		public static readonly PhotoStoreSA Instance = new PhotoStoreSA();

		private Cache _cache;
		private Attributes _attributes;
        private ISettingsSA _settingsService = null;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

		private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attributes
		{
			get
			{
				if (_attributes == null)
				{
					_attributes = AttributeMetadataSA.Instance.GetAttributes();
				}

				return _attributes;
			}
		}
		private PhotoStoreSA()
		{
			_cache = Cache.Instance;
		}
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("PHOTOSEARCHSVC_SA_CONNECTION_LIMIT"));
		}


		#region photostoreupdate

		//approved photo
		public void InsertPhoto(PhotoUpdate photoupdate, int memberid,int communityid)
		{PhotoStoreUpdate store=null;
			try
			{
				Member.ServiceAdapters.Member member=null;
				Content.ValueObjects.BrandConfig.Brand brand=null;
				member=MemberSA.Instance.GetMember(memberid,MemberLoadFlags.None);
				if(!IsValidMember( member,memberid))
					return;
				if(!IsValidBrand(member,out brand,communityid))
					return;
				if (IsHiddenMember(brand,member))
					return;
				if (IsSuspended(brand,member))
					return;
				store=new PhotoStoreUpdate();
				store.Action=PhotoStoreAction.insert;
				GetPhotoStoreUpdate(photoupdate,member,brand, store);
				string uri=getServiceManagerUri();
				getService(uri).SavePhoto(store);
			
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".InsertPhoto",ex,store);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}
		

		public void SavePhoto(PhotoUpdate update, CachedMember cachedMember,  int memberid,int communityid)
		{	PhotoStoreUpdate store=null;
			try
			{		
				if(!update.IsApproved)
						return;
				Member.ServiceAdapters.Member member= Member.ServiceAdapters.MemberSA.Instance.PopulateMemberObj(cachedMember);
				Content.ValueObjects.BrandConfig.Brand brand=null;

				if(!IsValidMember( member,memberid))
					return;
				if(!IsValidBrand(member,out brand,communityid))
					return;
				store=new PhotoStoreUpdate();
				store.Action=PhotoStoreAction.update;
				GetPhotoStoreUpdate(update,member,brand, store);
							
				string uri=getServiceManagerUri();
				getService(uri).SavePhoto(store);
			
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".SavePhoto",ex,store);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}
		
		//update/delete photo
		public void SavePhotos(PhotoUpdate[] photoupdate, CachedMember cachedmember, int memberid,int communityid)
		{
			try
               {
				
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".SavePhotos","Enter method");
				ArrayList storeUpdates=new ArrayList();
				//CachedMember cachedMember=new CachedMember(cachedmember.ToByteArray());
				Member.ServiceAdapters.Member member=Member.ServiceAdapters.MemberSA.Instance.PopulateMemberObj(cachedmember);

				if(!IsValidMember( member,memberid))
				{	ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".SavePhotos","Not valid member (null)");
					return;
				}
				
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand=null;
				if(!IsValidBrand(member,out brand,communityid))
					return;
			
				for(int i=0; i<photoupdate.Length;i++)
				{
					PhotoUpdate update=photoupdate[i];
										
					PhotoStoreUpdate item=new PhotoStoreUpdate();
					
					if(update.Delete)
					{	item.Action=PhotoStoreAction.delete;}
					
					else
					{
						if(!update.IsApproved)
							continue;
						item.Action=PhotoStoreAction.update;
					}

					GetPhotoStoreUpdate(photoupdate[i],member,brand, item);
					
					storeUpdates.Add(item);
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".SavePhotos",item.ToString());

				}
				
				if(storeUpdates.Count >0)
				{
					string uri=getServiceManagerUri();
					getService(uri).SavePhotos(storeUpdates);
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".SavePhotos","Udates:" + storeUpdates.Count.ToString());
				}
				else
				{
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".SavePhotos","No updates");
				}
			ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".SavePhotos","Exit method");
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".SavePhotos",ex,null);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}
		
		//member update
		public void UpdateMember(MemberUpdate memberupdate, CachedMember cachedmember, ArrayList communityids)
		{PhotoStoreUpdate store=null;
			try
			{	ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Enter method");
				bool makeBLCall;
				ArrayList photoStores=new ArrayList();
				//CachedMember cachedMember=new CachedMember(cachedmember.ToByteArray());
				Member.ServiceAdapters.Member member= Member.ServiceAdapters.MemberSA.Instance.PopulateMemberObj(cachedmember);
				if(!IsValidMember( member,memberupdate.MemberID))
					return;
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Communities:" + communityids.Count);
				for(int i=0;i<communityids.Count;i++)
				{	
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Start Community:" + ((int)communityids[i]).ToString());
					Content.ValueObjects.BrandConfig.Brand brand=null;
					if(!IsValidBrand(member,out brand,(int)communityids[i]))
					{	ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Community:" + ((int)communityids[i]).ToString() + " - not valid brand");
						continue;
					}
//					if(!member.HasApprovedPhoto((int)communityids[i])) 
//					{	ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Community:" + ((int)communityids[i]).ToString() + " - no approved photos");
//						continue;
//					}

					store=new PhotoStoreUpdate();
					makeBLCall=GetPhotoStoreUpdate(memberupdate,(int)communityids[i], store) ;
					if(makeBLCall)
					{
						ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Community:" + ((int)communityids[i]).ToString() + " - adding photostore:" + store.ToString());
						photoStores.Add(store);
					}

				}
				
				
				if(photoStores.Count > 0)
				{	
					string uri=getServiceManagerUri();
					getService(uri).UpdateMember((PhotoStoreUpdate[]) photoStores.ToArray(photoStores[0].GetType()));	
				}
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".UpdateMember","Exit method");
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".UpdateMember",ex,store);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}

		public void GetPhotoStoreUpdate(PhotoUpdate photoupdate, Member.ServiceAdapters.Member member, Content.ValueObjects.BrandConfig.Brand brand, PhotoStoreUpdate store)
		{bool makeBLCall=false;
			try
			{
		
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".GetPhotoStoreUpdate","Enter method");
				store.CommunityID=brand.Site.Community.CommunityID;
				store.MemberID=member.MemberID;

				
				store.MemberPhotoID=photoupdate.MemberPhotoID;
				store.FileID=photoupdate.FileID;
				store.PrivateFlag=photoupdate.IsPrivate?1:0;

				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".GetPhotoStoreUpdate","Setting community and member:" + store.ToString());

				if(store.Action==PhotoStoreAction.update || store.Action==PhotoStoreAction.delete)
					return ;

				
				int regionid=member.GetAttributeInt(brand,ATTR_REGIONID,Int32.MinValue);
				if (regionid != Int32.MinValue)
				{	
					store.RegionID=regionid;
					PopulateRegionInfo(store);
				}
				else
				{	throw new Exception("Invalid RegionID for PhotoStore Insert: " + regionid);}
				
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".GetPhotoStoreUpdate","Setting region:" + store.ToString());
				int gender=Int32.MinValue;
				int genderMask=member.GetAttributeInt(brand,ATTR_GENDERMASK,Int32.MinValue);
				if(genderMask!=Int32.MinValue)
				{
					gender=GenderUtils.GetGenderMaskSelf(genderMask);
					store.Gender=gender;
					
				}
				else
				{	throw new Exception("Invalid GenderMask for PhotoStore Insert: " + genderMask);}

				store.DisplayToGuestFlag=member.GetAttributeInt(brand,ATTR_DISPLAYPHOTOSTOGUEST,1);
				store.BirthDate=member.GetAttributeDate(brand,ATTR_BIRTHDATE,DateTime.MinValue);
				if(store.BirthDate==DateTime.MinValue)
				{throw new Exception("Invalid BirthDate for PhotoStore Insert" );}
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".GetPhotoStoreUpdate","Setting attributes:" + store.ToString());

				
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + ".GetPhotoStoreUpdate",store.ToString());
			
				//SavePhoto(store);
			
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".GetPhotoStoreUpdate",ex,store);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}
		
	
		
		public bool GetPhotoStoreUpdate(MemberUpdate memberupdate, int communityid, PhotoStoreUpdate store)
		{
			try
			{
				bool makeBLCall=false;
				
								
				store.CommunityID=communityid;
				store.MemberID=memberupdate.MemberID;

				if (IsSuspended(communityid,memberupdate))
				{
					store.Action=PhotoStoreAction.delete;
					makeBLCall=true;
					return makeBLCall;
				}
				if(IsHiddenMember(communityid,memberupdate))
				{
					store.Action=PhotoStoreAction.delete;
					makeBLCall=true;
					return makeBLCall;
				}
				

				int regionid=GetAttrInt(memberupdate,communityid, ATTR_REGIONID,Int32.MinValue);
				if (regionid != Int32.MinValue)
				{	
					store.RegionID=regionid;
					PopulateRegionInfo(store);
					makeBLCall=true;
									
				}
				int gender=Int32.MinValue;
				int genderMask=GetAttrInt(memberupdate,communityid, ATTR_GENDERMASK,Int32.MinValue);
				if(genderMask!=Int32.MinValue)
				{
					gender=GenderUtils.GetGenderMaskSelf(genderMask);
					store.Gender=gender;
					makeBLCall=true;
				}
				store.DisplayToGuestFlag=GetAttrInt(memberupdate,communityid, ATTR_DISPLAYPHOTOSTOGUEST,Int32.MinValue);

				makeBLCall=makeBLCall || store.DisplayToGuestFlag != Int32.MinValue;
				
				store.BirthDate=GetAttrDate(memberupdate,communityid, ATTR_BIRTHDATE,DateTime.MinValue);
				makeBLCall=makeBLCall || store.BirthDate != DateTime.MinValue;
				
				
				return makeBLCall;
			
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ". MemberUpdate GetPhotoStoreUpdate",ex,store);
				System.Diagnostics.EventLog.WriteEntry("Matchnet.PhotoSearch.PhotoSearchSA",ex.Source + "\r\n" +  ex.Message, System.Diagnostics.EventLogEntryType.Warning);
				return false;
			}


		}
		


		private int GetAttributeGroupID(int communityid, string attrname)
		{	int attrgroupid=0;
			try
			{
				Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attrname);
				int attrid=attribute.ID;
			
				int groupid=0;
				if(attribute.Scope==Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.Personals)
				{
					groupid=GROUP_PERSONALS;
				
				}
				else if(attribute.Scope==Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.Community)
				{	
					groupid=communityid; 
				}
				attrgroupid=attributes.GetAttributeGroup(groupid, attrid).ID;
				return attrgroupid;
			}
			catch(Exception ex)
			{return attrgroupid;}

		}
		private int GetAttrInt(MemberUpdate memberupdate, int communityid, string attrname, int defaultval)
		{
			try
			{
				
				int attrgroupid=GetAttributeGroupID(communityid,attrname);
				object o=null;
				if(attrgroupid !=0)
				{
					 o=memberupdate.AttributesInt[attrgroupid];
					if(o==null)
						return defaultval;
				}

				return (int) o;

			}
			catch(Exception ex)
			{throw(ex);}

		}

		private DateTime GetAttrDate(MemberUpdate memberupdate, int communityid, string attrname, DateTime defaultval)
		{
			try
			{
				int attrgroupid=GetAttributeGroupID(communityid,attrname);
				object o=null;
				if(attrgroupid !=0)
				{
					 o=memberupdate.AttributesDate[attrgroupid];
					if(o==null)
						return defaultval;
				}
				return (DateTime) o;

			}
			catch(Exception ex)
			{throw(ex);}

		}


		private void PopulateRegionInfo(PhotoStoreUpdate store)
		{
			try
			{
				Matchnet.Content.ValueObjects.Region.RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(store.RegionID, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.DEFAULT_REGION_LANGUAGE);

				if(region==null)
				{throw new Exception("Invalid RegionID in  PhotoStore update: " + store.RegionID);}

				Geo.Coordinates regioncoordinates =new Geo.Coordinates((double)region.Latitude,(double)region.Longitude);
				Geo.GeoBox geo=new Geo.GeoBox(regioncoordinates, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.DEFAULT_PARTITION_ANGLE, 0,false);

				store.Latitude=(double)region.Latitude;
				store.Longitude=(double)region.Longitude;
				store.BoxX=geo.SearchBox.Key.X;
				store.BoxY=geo.SearchBox.Key.Y;
				store.Depth1RegionID= region.Depth1RegionID;
				store.Depth2RegionID= region.StateRegionID;
				store.Depth3RegionID= region.CityRegionID;
				store.Depth4RegionID= region.PostalCodeRegionID;
			
			}
			catch(Exception ex)
			{ServiceTrace.Instance.Trace(MODULE_NAME + ". PopulateRegionInfo",ex,store);}

		}

		private bool IsValidMember(Member.ServiceAdapters.Member member, int memberid)
		{
			//
			if(member==null)
				return false;

			else
				return true;
		}

		private bool IsHiddenMember(Content.ValueObjects.BrandConfig.Brand brand,Member.ServiceAdapters.Member member)
		{
			int hideMask=member.GetAttributeInt(brand, ATTR_HIDEMASK, Int32.MinValue);
			if((hideMask & (int) AttributeOptionHideMask.HidePhotos)== (int) AttributeOptionHideMask.HidePhotos || (hideMask & (int) AttributeOptionHideMask.HideSearch)==(int) AttributeOptionHideMask.HideSearch)
				return true;
			return false;	
		}

		private bool IsHiddenMember(int communityid,MemberUpdate member)
		{
			int hideMask=GetAttrInt( member, communityid, ATTR_HIDEMASK, Int32.MinValue);
			if((hideMask & (int) AttributeOptionHideMask.HidePhotos)== (int) AttributeOptionHideMask.HidePhotos || (hideMask & (int) AttributeOptionHideMask.HideSearch)==(int) AttributeOptionHideMask.HideSearch)
				return true;
			return false;
				
		}

		private bool IsValidBrand(Member.ServiceAdapters.Member member, out Content.ValueObjects.BrandConfig.Brand brand, int communityid)
		{
			int brandid=0;
			member.GetLastLogonDate(communityid,out brandid);
			brand=BrandConfigSA.Instance.GetBrandByID(brandid);
			if(brand==null)
				return false;
			else
				return true;

		}

		private bool IsSuspended(int communityid ,MemberUpdate memberupdate)
		{
			int selfsuspendflag=GetAttrInt(memberupdate,communityid, ATTR_SELFSUSPEND,Int32.MinValue);
			if(selfsuspendflag==1)
			{
				return true;
			}

			Member.ValueObjects.GlobalStatusMask globalstatusmask=(Member.ValueObjects.GlobalStatusMask)GetAttrInt(memberupdate,communityid, ATTR_GLOBAL_STATUS_MASK,Int32.MinValue);
			if((globalstatusmask & Member.ValueObjects.GlobalStatusMask.AdminSuspended)==Member.ValueObjects.GlobalStatusMask.AdminSuspended)
			{
				return true;
			}
			return false;
		}


		private bool IsSuspended(Content.ValueObjects.BrandConfig.Brand brand,Member.ServiceAdapters.Member member)
		{
			
			int selfsuspendflag=member.GetAttributeInt(brand, ATTR_SELFSUSPEND,Int32.MinValue);
			if(selfsuspendflag==1)
			{
				return true;
			}

			Member.ValueObjects.GlobalStatusMask globalstatusmask=(Member.ValueObjects.GlobalStatusMask)member.GetAttributeInt(brand, ATTR_GLOBAL_STATUS_MASK,Int32.MinValue);
			if((globalstatusmask & Member.ValueObjects.GlobalStatusMask.AdminSuspended)==Member.ValueObjects.GlobalStatusMask.AdminSuspended)
			{
				return true;
			}
			return false;
		}
		#endregion

		#region service proxy

		private IPhotoStoreService getService(string uri)
		{
			try
			{
				return (IPhotoStoreService)Activator.GetObject(typeof(IPhotoStoreService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(PhotoSearch.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(PhotoSearch.ValueObjects.ServiceConstants.STORE_SERVICE_MANAGER_NAME);

                string overrideHostName = SettingsService.GetSettingFromSingleton(PhotoSearch.ValueObjects.ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		#endregion
	}
}

