﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Matchnet.PhotoSearch.ValueObjects;

namespace Matchnet.PhotoSearch.ServiceAdapters.Interfaces
{
    public interface IPhotoGallery
    {
        string TestURI { set; }
        ArrayList Search(PhotoGalleryQuery queryIn, int memberID, int communityID, int page, int pagesize, out int count);
        PhotoGalleryResultList Search(PhotoGalleryQuery queryIn, int memberID, int communityID, int page, int pagesize, bool randomize);
        int GetCommunityPhotoCount(int communityid, int hours);
        PhotoGalleryQuery GetDefaultQuery(int brandid);
        bool IsValidQuery(PhotoGalleryQuery query);
        QueryOptionCollection GetQueryRegionOptions(int siteid);
        QueryOptionCollection GetQueryDaysOptions(int siteid);
        QueryOptionCollection GetOptionCollection(int siteid, QueryOptionType optiontype);
        int GetDefaultOption(int siteid, QueryOptionType qtype);
    }
}
