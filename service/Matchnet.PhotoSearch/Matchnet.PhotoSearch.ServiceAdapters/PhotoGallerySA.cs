using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.ServiceDefinitions;
using Matchnet.PhotoSearch.ServiceAdapters.Interfaces;

namespace Matchnet.PhotoSearch.ServiceAdapters
{
	
	/// <summary>
	/// Summary description for PhotoGallerySA.
	/// </summary>
	public class PhotoGallerySA : SABase, IPhotoGallery
	{
		//we will be using english because we don't need literal translations only coordinates.

		private const string MODULE_NAME="PhotoGallerySA";
		private const string SEARCH_RESULTS_NAMED_CACHE = "searchresults";
		public static readonly PhotoGallerySA Instance = new PhotoGallerySA();
		private string testURI="";
		private Cache _cache;
        private ISettingsSA _settingsService = null;

		private PhotoGallerySA()
		{
			_cache = Cache.Instance;
		}

        public string TestURI
        {
            set { testURI = value; }
        }

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        #region IPhotoGallery
        public ArrayList Search(PhotoGalleryQuery queryIn, int memberID, int communityID, int page, int pagesize, out int count)
        {
            ArrayList results = null;
            count = 0;
            PhotoGalleryResultList resultList = Search(queryIn, memberID, communityID, page, pagesize, false);
            if (resultList != null)
            {
                count = resultList.MatchesFound;
                results = resultList.PhotoGalleryResults;
            }

            return results;
        }

		public PhotoGalleryResultList Search(PhotoGalleryQuery queryIn, int memberID, int communityID, int page, int pagesize, bool randomize)
		{
			try
			{
                PhotoGalleryResultList results = null;
				if(queryIn==null)
				{
                    return results;
				}
				PhotoGalleryQuery query=queryIn;
				string cachekey=query.GetQueryCacheKey();
				string uri="";
				uri=getServiceManagerUri();

				bool isProfileBlockingEnabled = false;
				isProfileBlockingEnabled = (memberID != Constants.NULL_INT &&
                    Convert.ToBoolean(SettingsService.GetSettingFromSingleton("PROFILE_BLOCK_ENABLED", communityID)));
				
				PhotoResultItemCollection photoResItems = null;

				if(isProfileBlockingEnabled)
				{
					// check the distributed cache
					string jsonString = Matchnet.DistributedCaching11.ServiceAdapters.DistributedCachingSA.Instance.GetString(GetMemberSpecificCacheKey(cachekey, memberID, randomize), SEARCH_RESULTS_NAMED_CACHE);

					if(jsonString != null && jsonString != string.Empty)
					{
						photoResItems = (PhotoResultItemCollection)AjaxPro.JavaScriptDeserializer.DeserializeFromJson(jsonString, typeof(PhotoResultItemCollection));
					}
				}

				// not found in distributed cache or it's not enabled.  go through the old process
				if(photoResItems == null)
				{			
					PhotoGalleryResultList resultlist=(PhotoGalleryResultList)_cache.Get(cachekey);
					query.Page=page;
					query.PageSize=pagesize;
			
					if(resultlist==null || resultlist.PhotoGalleryResults.Count==0)
					{
						//query.InsertDateMin=new DateTime();
						PrepareRegionQuery(query);
						resultlist=Search(query);
						_cache.Add(resultlist);
					
					}

					if((randomize || isProfileBlockingEnabled) && resultlist != null)
					{
                        /*These paths will save member specific cache results*/
                        ArrayList tempResultList = resultlist.PhotoGalleryResults;
                        if (randomize)
                        {
                            tempResultList = RandomizeResults(tempResultList);
                        }

                        if (isProfileBlockingEnabled && memberID > 0)
                        {
                            // if profile blocking is enabled further processing is required and caching afterwards
                            photoResItems = RemoveExcludedMembers(tempResultList, communityID, memberID);
                        }
                        else
                        {
                            //convert arraylist to PhotoResultItemCollection
                            photoResItems = new PhotoResultItemCollection();
                            foreach (PhotoResultItem item in tempResultList)
                            {
                                photoResItems.Add(item);
                            }
                        }

						// cache this in string format in our distributed cache
						string jsonString = AjaxPro.JavaScriptSerializer.Serialize(photoResItems);
						Matchnet.DistributedCaching11.ServiceAdapters.DistributedCachingSA.Instance.Put(GetMemberSpecificCacheKey(cachekey, memberID, randomize), jsonString, SEARCH_RESULTS_NAMED_CACHE);
					}
					else
					{
                        results = new PhotoGalleryResultList("");
                        results.PhotoGalleryResults = new ArrayList();
						if(resultlist != null)
						{
							int startIndex=(page  - 1) * pagesize;
							int endIndex=Math.Min(resultlist.PhotoGalleryResults.Count - 1, startIndex + pagesize - 1);
					
							for(int i=startIndex; i<= endIndex;i++)
							{
								results.PhotoGalleryResults.Add(resultlist.PhotoGalleryResults[i]);
							}
							results.MatchesFound = resultlist.PhotoGalleryResults.Count;
						}
				
						return results;
					}
				}
				
				// pagination and prepare the object to be returned
                results = new PhotoGalleryResultList("");
                results.PhotoGalleryResults = new ArrayList();
				if(photoResItems != null)
				{
					int startIndex=(page  - 1) * pagesize;
					int endIndex=Math.Min(photoResItems.Count - 1, startIndex + pagesize - 1);
					
					for(int i=startIndex; i<= endIndex;i++)
					{
						results.PhotoGalleryResults.Add(photoResItems[i]);
					}
					results.MatchesFound = photoResItems.Count;
				}
				
				return results;

			}
			catch(Exception ex)
			{
				ServiceTrace.Instance.Trace(MODULE_NAME + ".ArrayList Search",ex,queryIn);
				throw new SAException(ex.Message,ex.InnerException,queryIn);
			}


		}
		
		public int GetCommunityPhotoCount(int communityid, int hours)
		{
			int count=Constants.NULL_INT;
			try
			{PhotoStats stats=(PhotoStats)_cache.Get(PhotoStats.GetCacheKey(hours));
				
				if(stats== null || stats.Stats == null)
				{
					stats=getService().GetPhotoCommunityCounts(hours);
					_cache.Insert(stats);
				}
				count=(int)stats.Stats[communityid];
				
				return count;
			}
			catch(Exception ex)
			{
				ServiceTrace.Instance.Trace(MODULE_NAME + ".GetCommunityPhotoCount",ex,null);
				//throw new SAException(ex.Message,ex.InnerException,null);
				return count;
			}


		}		
		
		public PhotoGalleryQuery GetDefaultQuery(int brandid)
		{
			try
			{
				PhotoGalleryQuery query=null;
				
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand=BrandConfigSA.Instance.GetBrandByID(brandid);

				if(brand==null)
					{throw new Exception("Invalid BrandID for PhotoGallery search query: " + brandid);}

				query=(PhotoGalleryQuery)_cache.Get(PhotoGalleryQuery.GetCacheKey(brand.Site.SiteID));

				if(query != null)
					return query;

				query=new PhotoGalleryQuery();

				query.CommunityID=brand.Site.Community.CommunityID;
				query.SiteID=brand.Site.SiteID;
				
				query.CountryRegionID= brand.Site.DefaultRegionID;
				query.RegionType=SearchRegionType.country;
				query.AgeMax=brand.DefaultAgeMax;
				query.AgeMin=brand.DefaultAgeMin;
				query.Radius=brand.DefaultSearchRadius;
				query.InsertDays=GetDefaultOption(brand.Site.SiteID,QueryOptionType.insertdays);
				
				
				int siteid=brand.Site.SiteID;
				query.InsertDays=GetQueryDaysOptions(siteid).DefaultOption;
				_cache.Add(query);

				return query;

			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".GetDefaultQuery",ex,null);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}

		public bool IsValidQuery(PhotoGalleryQuery query)
		{
			bool valid=false;
			try
			{
				

				if(query == null)
					return valid;
				if (!ValidateInt(query.CommunityID))
					return valid;

				if (!ValidateInt(query.SiteID))
					return valid;

				if (!ValidateInt(query.AgeMax))
					return valid;

				if (!ValidateInt(query.AgeMin))
					return valid;

				if (!ValidateInt(query.InsertDays))
					return valid;
			
								
				valid=true;
				return valid;

			}
			catch(Exception ex)
			{ServiceTrace.Instance.Trace(MODULE_NAME + ".IsValidQuery",ex,query);
				return valid;
			}


		}

		public QueryOptionCollection GetQueryRegionOptions(int siteid)
		{
			QueryOptionCollection options=null;
			try
			{
				options=GetOptionCollection(siteid,QueryOptionType.region);
				return options;

			}
			catch(Exception ex)
			{ 
				ServiceTrace.Instance.Trace(MODULE_NAME + ".GetQueryRegionOptions",ex,null);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}

		public QueryOptionCollection GetQueryDaysOptions(int siteid)
		{
			QueryOptionCollection insertdays=null;
			try
			{
				
				insertdays=GetOptionCollection(siteid,QueryOptionType.insertdays);
				return insertdays;

			}
			catch(Exception ex)
			{ServiceTrace.Instance.Trace(MODULE_NAME + ".GetQueryDaysOptions",ex,null);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}

		public QueryOptionCollection GetOptionCollection(int siteid, QueryOptionType optiontype)
		{
			string uri  = "";
			QueryOptionCollection options;
			try
			{
				options=(QueryOptionCollection)_cache.Get(QueryOptionCollection.GetCacheKey(siteid, optiontype));
				if(options!=null)
					{return options;}
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					options= getService(uri).GetQueryOptionCollection(siteid,optiontype);
				}
				finally
				{
					base.Checkin(uri);
				}
				_cache.Add(options);
				return options;
			}
			catch(Exception ex)
			{ServiceTrace.Instance.Trace(MODULE_NAME + ".GetOptionCollection",ex,null);
				throw(new SAException("Error occurred while retrieving query options. (uri: " + uri + ")", ex));
			} 
		}

		public int GetDefaultOption( int siteid,QueryOptionType qtype)
		{
			QueryOptionCollection coll=null;
			int val=0;
			try
			{
				coll=GetOptionCollection(siteid,qtype);
				if(coll==null)
					return val;

				for (int i=0; i< coll.Count;i++)
				{
					if(coll[i].DefaultFlag)
					{
						val=coll[i].OptionValue;
						break;
					}
				}
				return val;

			}
			catch(Exception ex)
			{ServiceTrace.Instance.Trace(MODULE_NAME + ".GetDefaultOption",ex,null);
				throw new SAException(ex.Message,ex.InnerException);
			}


		}

		#endregion

        public PhotoResultItemCollection RemoveExcludedMembers(ArrayList photoResultItems, int communityID, int memberID)
        {
            PhotoResultItemCollection filteredList = new PhotoResultItemCollection();
            Matchnet.List.ServiceAdapters.List list = Matchnet.List.ServiceAdapters.ListSA.Instance.GetList(memberID);

            if (photoResultItems != null && list != null)
            {
                for (int i = 0; i < photoResultItems.Count; i++)
                {
                    PhotoResultItem item = (PhotoResultItem)photoResultItems[i];
                    if (!list.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeListInternal, communityID, item.MemberID))
                    {
                        filteredList.Add(item);
                    }
                }
            }

            return filteredList;
        }

        public ArrayList RandomizeResults(ArrayList photoResultItems)
        {
            ArrayList randomizedList = new ArrayList();
            Random random = new Random();
            int randomNumber = 0;
            if (photoResultItems != null && photoResultItems.Count > 0)
            {
                //create a temp so it does not effect original list passed in
                ArrayList tempPhotoResultItems = new ArrayList(photoResultItems);

                while (tempPhotoResultItems.Count > 0)
                {
                    if (tempPhotoResultItems.Count == 1)
                    {
                        randomizedList.Add(tempPhotoResultItems[0]);
                        break;
                    }
                    else
                    {
                        randomNumber = random.Next(tempPhotoResultItems.Count);
                        randomizedList.Add(tempPhotoResultItems[randomNumber]);
                        tempPhotoResultItems.RemoveAt(randomNumber);
                    }

                }
            }

            return randomizedList;
        }

        public string GetMemberSpecificCacheKey(string origionalCacheKey, int memberID, bool isRandomized)
        {
            if (!isRandomized)
            {
                return "EXCL_PHOTO_" + memberID.ToString() + "_" + origionalCacheKey;
            }
            else
            {
                return "EXCL_PHOTO_RANDOMIZED_" + memberID.ToString() + "_" + origionalCacheKey;
            }
        }

        #region Private methods
        private IPhotoSearchService getService()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + ".PhotoGalleryResultList Search", ex, null);
                throw (new SAException("Error occurred while attempting to execute search. (uri: " + uri + ")", ex));
            }
        }

        private PhotoGalleryResultList Search(PhotoGalleryQuery query)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri).Search(query);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + ".PhotoGalleryResultList Search", ex, query);
                throw (new SAException("Error occurred while attempting to execute search. (uri: " + uri + ")", ex));
            }
        }

        private ArrayList RefreshResults(PhotoGalleryQuery query)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri).RefreshSearchResults(query);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + ".RefreshResults", ex, query);
                throw (new SAException("Error occurred while attempting to execute search. (uri: " + uri + ")", ex));
            }
        }

        private void PrepareRegionQuery(PhotoGalleryQuery query)
        {
            try
            {
                if (query.RegionType != SearchRegionType.myregion)
                    return;

                int regionid = query.RegionID;
                if (regionid <= 0)
                { throw new Exception("RegionID is requred for My Region PhotoGallery search"); }

                Matchnet.Content.ValueObjects.Region.RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(regionid, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.DEFAULT_REGION_LANGUAGE);

                if (region == null)
                { throw new Exception("Invalid RegionID in My Region PhotoGallery search: " + regionid); }

                Geo.Coordinates regioncoordinates = new Geo.Coordinates((double)region.Latitude, (double)region.Longitude);
                Geo.GeoBox searchBox = new Geo.GeoBox(regioncoordinates, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.DEFAULT_PARTITION_ANGLE, query.Radius, false);
                query.SearchBox = searchBox;
                query.CountryRegionID = region.CountryRegionID;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + ".PrepareRegionQuery", ex, query);
                throw new SAException(ex.Message, ex.InnerException, query);
            }
        }

        private bool ValidateInt(int val)
        {
            bool valid = true;
            if (val == 0 || val == Matchnet.Constants.NULL_INT)
                valid = false;

            return valid;

        }
        #endregion

		#region service proxy

		private IPhotoSearchService getService(string uri)
		{
			try
			{
				return (IPhotoSearchService)Activator.GetObject(typeof(IPhotoSearchService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(PhotoSearch.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(PhotoSearch.ValueObjects.ServiceConstants.SEARCH_SERVICE_MANAGER_NAME);

                string overrideHostName = SettingsService.GetSettingFromSingleton(PhotoSearch.ValueObjects.ServiceConstants.SERVICE_SA_HOST_OVERRIDE);
				if(testURI!="")
				
					return testURI;
				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri= "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}
				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("PHOTOSEARCHSVC_SA_CONNECTION_LIMIT"));
        }

		#endregion
	}
}
