using System;
using System.Diagnostics;
using System.Messaging;
using System.Threading;

using System.IO;

using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

using Matchnet.PhotoSearch.ValueObjects;

namespace Matchnet.PhotoSearch.BusinessLogic
{
	/// <summary>
	/// Summary description for QueueProcessor.
	/// </summary>
	public class QueueProcessor
	{
		public const string DEFAULT_QUEUE_PATH = @".\private$\PhotoStoreUpdate";
		public const string DEFAULT_QUEUE_EXC_PATH = @".\private$\PhotoStoreUpdate_Exception";

		private const int SLEEP_TIME = 500;
		private const int SLEEP_TIME_ERROR = 15000;
		
		#region Private Variables
		private HydraWriter _hydraWriter = null;
		private bool _runnable;
		private Thread[] _PhotoStoreThreads;
		private MessageQueue _PhotoStoreQueue;
		#endregion
		
		public QueueProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Start()
		{
			_runnable = true;

			_hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnPhotoStore"});
			_hydraWriter.Start();

			startPhotoStoreThreads();
			EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Started processing QueuePath:" + DEFAULT_QUEUE_PATH, EventLogEntryType.Information);
		}


		public void Stop()
		{
			_runnable = false;
			_hydraWriter.Stop();
			stopPhotoStoreThreads();
			
		}


		private void startPhotoStoreThreads()
		{
			
			_PhotoStoreQueue = new MessageQueue(DEFAULT_QUEUE_PATH);
			_PhotoStoreQueue.Formatter = new XmlMessageFormatter();

			Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOSEARCHSVC_THREAD_COUNT"));
			_PhotoStoreThreads = new Thread[threadCount];

			for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
			{
				Thread t = new Thread(new ThreadStart(processPhotoStoreUpdateCycle));
				t.Name = "SoapProcessThread" + threadNum.ToString();
				t.Start();
				_PhotoStoreThreads[threadNum] = t;
			}
		}


		
		private void stopPhotoStoreThreads()
		{
			Int16 threadCount = (Int16)_PhotoStoreThreads.Length;

			for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
			{
				_PhotoStoreThreads[threadNum].Join(10000);
			}

		}
		private void processPhotoStoreUpdateCycle()
		{
			try
			{
				TimeSpan ts = new TimeSpan(0, 0, 10);

				while (_runnable)
				{
					MessageQueueTransaction tran = new MessageQueueTransaction();

					try
					{
						QueueItemBase queueitem = null;

						tran.Begin();

						#region Receive photostoreupdate
						try
						{
							queueitem = _PhotoStoreQueue.Receive(ts, tran).Body as QueueItemBase;
						}
						catch (MessageQueueException mex)
						{
							if (mex.Message != "Timeout for the requested operation has expired.")
							{
								attemptRollback(tran);
								throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
							}
						}
						#endregion

						#region Process 
						if (queueitem != null)
						{
							try
							{	
								if(queueitem is PhotoQueueItem)
								{ 
									if (((PhotoQueueItem)queueitem).Action==PhotoStoreAction.insert)
									    PhotoStoreBL.Instance.InsertPhoto((PhotoQueueItem)queueitem);
									else
										PhotoStoreBL.Instance.UpdatePhoto((PhotoQueueItem)queueitem);
								}
								
								else if(queueitem is MemberQueueItem)
									{PhotoStoreBL.Instance.UpdateMember((MemberQueueItem)queueitem);;}
								
								tran.Commit();
							
							}
							catch(Exception ex)
							{
								attemptRollback(tran);
								throw new BLException("Error importing/sending PhotoStoreUpdate  message. [queuePath: " + DEFAULT_QUEUE_PATH + "/" + queueitem.ToString() + "].\r\nInner Ex: " + ex.StackTrace, ex);
							}
						}
						else
						{
							tran.Commit();
							Thread.Sleep(SLEEP_TIME);
						}
						#endregion
					}
					catch(Exception ex)
					{
						EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "PhotoStoreUpdate  message. [QueuePath: " + DEFAULT_QUEUE_PATH + "]" + ex.ToString(), EventLogEntryType.Error);
						Thread.Sleep(SLEEP_TIME_ERROR);
					}
					finally
					{
						if (tran != null)
						{tran.Dispose();}
					}
				}
			}
			catch(Exception ex)
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processPhotoStoreUpdateCycle(): " + ex.ToString(), EventLogEntryType.Error);
				Thread.Sleep(SLEEP_TIME_ERROR);
			}
		}

		private void attemptRollback(MessageQueueTransaction tran)
		{
			try
			{
				if (tran.Status == MessageQueueTransactionStatus.Pending)
				{	tran.Abort();}
			} 
			catch (Exception ex)
			{
				string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
			}
		}
		
	}
}
