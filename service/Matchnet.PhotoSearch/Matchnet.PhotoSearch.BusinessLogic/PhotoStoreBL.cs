using System;
using System.Data;
using System.Messaging;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Spark.PhotoSearchEngine.ValueObjects;


namespace Matchnet.PhotoSearch.BusinessLogic
{
	/// <summary>
	/// Summary description for PhotoStoreBL.
	/// </summary>
	public class PhotoStoreBL
	{
		const string MODULE_NAME="PhotoStoreBL";
		public delegate void PhotoStoreUpdateEventHandler(PhotoStoreAction action);
		public event PhotoStoreUpdateEventHandler PhotoStoreUpdateRequested ;

		public delegate void PhotoStoreMemberUpdateEventHandler(PhotoStoreAction action);
		public event PhotoStoreMemberUpdateEventHandler PhotoStoreMemberUpdateRequested ;
//		public delegate void PhotoStoreMemberDeleteEventHandler();
//		public event PhotoStoreMemberDeleteEventHandler PhotoStoreMemberDeleteRequested ;
		
		const string PHOTOSTORE_LDB="mnPhotoStore";
		const string PHOTOSTORE_TABLE="PhotoStore";

		const string PHOTOSTORE_SP_SAVE="up_PhotoStore_Save";
		const string PHOTOSTORE_SP_SAVE_MEMBER="up_PhotoStore_SaveAll";
		//singleton implementation
	    public readonly static PhotoStoreBL Instance = new PhotoStoreBL();
        private ISettingsSA _settingsService = null;
        private Attributes _attributes;

		private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attributes
		{
			get
			{
				if (_attributes == null)
				{
					_attributes = AttributeMetadataSA.Instance.GetAttributes();
				}

				return _attributes;
			}
		}

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

		//update/delete photo



//		public void Enqueue(PhotoStoreUpdate item)
//		{
//			string queuePath = QueueProcessor.DEFAULT_QUEUE_PATH;
//
//			try
//			{
//				if (!MessageQueue.Exists(queuePath))
//				{	MessageQueue.Create(queuePath, true);}
//
//				MessageQueue queue = new MessageQueue(queuePath);
//				queue.Formatter = new XmlMessageFormatter();
//				MessageQueueTransaction trans = new MessageQueueTransaction();
//
//				try
//				{
//					trans.Begin();
//					queue.Send(item, trans);
//					trans.Commit();
//				}
//				finally
//				{
//					trans.Dispose();
//				}
//			}
//			catch (Exception ex)
//			{
//				string errorMessage = "Error enqueueing item object.";
//				if(queuePath != null)
//				{
//					errorMessage = errorMessage + " QueuePath:" + queuePath.ToString();
//				}
//				throw new BLException(errorMessage, ex);
//			}
//		}

//		public void SavePhotos(PhotoStoreUpdate[] photoStore)
//		{
//			try
//			{	for(int i=0; i<photoStore.Length -1 ;i++)
//				{
//					try
//					{
//						if (photoStore[i].Action==PhotoStoreAction.update)
//							SavePhoto(photoStore[i]);
//						else if (photoStore[i].Action==PhotoStoreAction.delete)
//							DeletePhoto(photoStore[i]);
//					}
//					catch(Exception ex){}
//
//				}
//			}
//			catch(Exception ex)
//			{throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);}
//
//		}


		public void SavePhoto(PhotoStoreUpdate photoStore)
		{
			string functionName=".SavePhoto";
			try
			{
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Enter method");
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,photoStore.ToString());
				Command comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName=PHOTOSTORE_SP_SAVE;
				

				comm.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, photoStore.CommunityID);
				comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, photoStore.MemberID);
				comm.AddParameter("@memberphotoid", SqlDbType.Int, ParameterDirection.Input, photoStore.MemberPhotoID);
				comm.AddParameter("@fileid", SqlDbType.Int, ParameterDirection.Input, photoStore.FileID);
				if(photoStore.Action==PhotoStoreAction.insert)
				{	AddBitParameter(comm,"@insertflag", 1);	}
				
					AddIntParameter(comm,"@gender", photoStore.Gender);
					AddDateParameter(comm,"@birthdate",  photoStore.BirthDate);
					AddBitParameter(comm,"@privateflag",  photoStore.PrivateFlag);
					AddBitParameter(comm,"@displaytoguestflag",photoStore.DisplayToGuestFlag);
					AddDateParameter(comm,"@photoinsertdate",  photoStore.InsertDate);
					AddIntParameter(comm,"@regionid", photoStore.RegionID);
					AdddoubleParameter(comm,"@latitude",   photoStore.Latitude);
					AdddoubleParameter(comm,"@longitude",   photoStore.Longitude);
					AddIntParameter(comm,"@depth1regionid", photoStore.Depth1RegionID);
					AddIntParameter(comm,"@depth2regionid", photoStore.Depth2RegionID);
					AddIntParameter(comm,"@depth3regionid", photoStore.Depth3RegionID);
					AddIntParameter(comm,"@depth4regionid", photoStore.Depth4RegionID);
					AddIntParameter(comm,"@boxx", photoStore.BoxX);
					AddIntParameter(comm,"@boxy",  photoStore.BoxY);
				
				

				Client.Instance.ExecuteAsyncWrite(comm);
				PhotoStoreUpdateRequested(photoStore.Action);
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Exit method");
                UpdateE2PhotoEngine(photoStore);
            }
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".SavePhoto",ex,photoStore);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException,photoStore,photoStore.MemberID);}

		}

	    private void UpdateE2PhotoEngine(PhotoStoreUpdate photoStore)
	    {
	        if (UseE2PhotoEngine(0, photoStore.CommunityID))
	        {
	            PhotoSearchUpdate photoSearchUpdate = new PhotoSearchUpdate();
	            photoSearchUpdate.CommunityID = photoStore.CommunityID;
	            photoSearchUpdate.MemberID = photoStore.MemberID;
	            photoSearchUpdate.MemberPhotoID = photoStore.MemberPhotoID;
	            photoSearchUpdate.UpdateMode = (photoStore.Action == PhotoStoreAction.insert) ? UpdateModeEnum.add : UpdateModeEnum.update;
	            photoSearchUpdate.UpdateReason = UpdateReasonEnum.none;

	            Spark.PhotoSearchEngine.ValueObjects.PhotoSearch photoSearch =
	                new Spark.PhotoSearchEngine.ValueObjects.PhotoSearch();
	            photoSearch.BirthDate = photoStore.BirthDate;
	            photoSearch.CommunityId = photoStore.CommunityID;
	            photoSearch.Depth1RegionId = photoStore.Depth1RegionID;
	            photoSearch.Depth2RegionId = photoStore.Depth2RegionID;
	            photoSearch.Depth3RegionId = photoStore.Depth3RegionID;
	            photoSearch.Depth4RegionId = photoStore.Depth4RegionID;
	            photoSearch.DisplayToGuestFlag = photoStore.DisplayToGuestFlag;
	            photoSearch.FileId = photoStore.FileID;
	            photoSearch.Gender = photoStore.Gender;
	            photoSearch.InsertDate = photoStore.InsertDate;
	            photoSearch.Latitude = photoStore.Latitude;
	            photoSearch.Longitude = photoStore.Longitude;
	            photoSearch.MemberId = photoStore.MemberID;
	            photoSearch.MemberPhotoId = photoStore.MemberPhotoID;
	            photoSearch.PhotoStoreInsertDate = photoStore.InsertDate;
	            photoSearch.PhotoStoreUpdateDate = DateTime.Now;
	            photoSearch.PrivateFlag = photoStore.PrivateFlag;
	            Spark.PhotoSearchEngine.ServiceAdaptersNRT.SearcherNRTSA.Instance.NRTUpdateMemberPhoto(photoSearchUpdate,
	                                                                                                   photoSearch);
	        }
	    }

	    public void DeletePhoto(PhotoStoreUpdate photoStore)
		{
			string functionName=".DeletePhoto";
			try
			{
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Enter method");
				Command comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName=PHOTOSTORE_SP_SAVE;

				comm.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, photoStore.CommunityID);
				comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, photoStore.MemberID);
				comm.AddParameter("@memberphotoid", SqlDbType.Int, ParameterDirection.Input, photoStore.MemberPhotoID);
				comm.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, 1);
				
				PhotoStoreUpdateRequested(PhotoStoreAction.delete);

				Client.Instance.ExecuteAsyncWrite(comm);
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Exit method");
                if (UseE2PhotoEngine(0,photoStore.CommunityID))
                {
                    PhotoSearchUpdate photoSearchUpdate = new PhotoSearchUpdate();
                    photoSearchUpdate.CommunityID = photoStore.CommunityID;
                    photoSearchUpdate.MemberID = photoStore.MemberID;
                    photoSearchUpdate.MemberPhotoID = photoStore.MemberPhotoID;
                    photoSearchUpdate.UpdateMode = UpdateModeEnum.remove;
                    photoSearchUpdate.UpdateReason = UpdateReasonEnum.none;
                    Spark.PhotoSearchEngine.ServiceAdaptersNRT.SearcherNRTSA.Instance.NRTRemoveMemberPhoto(photoSearchUpdate);
                }
			}
			catch(Exception ex)
			{		ServiceTrace.Instance.Trace(MODULE_NAME + ".DeletePhoto",ex,photoStore);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException,photoStore,photoStore.MemberID);
			}

		}


		public void UpdateMember(PhotoStoreUpdate[] photoStore)
		{
			try
			{
				if(photoStore ==null)
					return;

				for(int i=0;i<photoStore.Length;i++)
				{
					UpdateCommunityMember(photoStore[i]);
					
				}

			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".UpdateMember",ex,null);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);
			}
		}
		public void UpdateCommunityMember(PhotoStoreUpdate photoStore)
		{
			string functionName=".UpdateCommunityMember";
			try
			{
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Enter method");
				Command comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName=PHOTOSTORE_SP_SAVE_MEMBER;
				
				
				comm.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, photoStore.CommunityID);
				comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, photoStore.MemberID);

				if(photoStore.Action==PhotoStoreAction.delete)
				{comm.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, 1);}
				else
				{
					//possibly optional parameters
					AddIntParameter(comm, "@gender", photoStore.Gender);
					AddDateParameter(comm,"@birthdate",  photoStore.BirthDate);
					AddBitParameter(comm,"@displaytoguestflag", photoStore.DisplayToGuestFlag);
					AddIntParameter(comm,"@regionid", photoStore.RegionID);
					AdddoubleParameter(comm,"@latitude",   photoStore.Latitude);
					AdddoubleParameter(comm,"@longitude",  photoStore.Longitude);
					AddIntParameter(comm,"@depth1regionid", photoStore.Depth1RegionID);
					AddIntParameter(comm,"@depth2regionid", photoStore.Depth2RegionID);
					AddIntParameter(comm,"@depth3regionid", photoStore.Depth3RegionID);
					AddIntParameter(comm,"@depth4regionid", photoStore.Depth4RegionID);
					AddIntParameter(comm,"@boxx", photoStore.BoxX);
					AddIntParameter(comm,"@boxy", photoStore.BoxY);
				}

				Client.Instance.ExecuteAsyncWrite(comm);
				PhotoStoreMemberUpdateRequested(photoStore.Action);
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Exit method");
                UpdateE2PhotoEngine(photoStore);
            }
			catch(Exception ex)
			{ServiceTrace.Instance.Trace(MODULE_NAME + ".UpdateCommunityMember",ex,photoStore);}
		}

		private void DeleteMember(PhotoStoreUpdate photoStore)
		{
			string functionName=".DeleteMember";
			try
			{
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Enter method");
				Command comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName=PHOTOSTORE_SP_SAVE_MEMBER;

				comm.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, photoStore.CommunityID);
				comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, photoStore.MemberID);
				comm.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, 1);
			
				Client.Instance.ExecuteAsyncWrite(comm);
				PhotoStoreMemberUpdateRequested(PhotoStoreAction.delete);
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Exit method");
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".DeleteMember",ex,photoStore);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException,photoStore,photoStore.MemberID);
			}

		}

		
		private void AddIntParameter(Command comm,string paramname,int val)
		{
			if(val != Int32.MinValue)
				comm.AddParameter(paramname,SqlDbType.Int,ParameterDirection.Input,val);
		}

		private void AddBitParameter(Command comm,string paramname,int val)
		{
			if(val != Int32.MinValue)
				if(val==0 || val==1)
					comm.AddParameter(paramname,SqlDbType.Bit,ParameterDirection.Input,val);
		}


		private void AddDateParameter(Command comm,string paramname,DateTime val)
		{
			if(val != DateTime.MinValue)
				comm.AddParameter(paramname,SqlDbType.DateTime,ParameterDirection.Input,val);
		}

		private void AdddoubleParameter(Command comm,string paramname,double val)
		{
			if(val != double.MinValue)
				comm.AddParameter(paramname,SqlDbType.Decimal,ParameterDirection.Input,(decimal)val);
			
		}

        public bool UseE2PhotoEngine(int siteId, int communityId)
        {
            bool useE2PhotoEngine = false;
            try
            {
                string settingFromSingleton = SettingsService.GetSettingFromSingleton("USE_E2_PHOTO_ENGINE", communityId, siteId);
                if (null != settingFromSingleton)
                {
                    useE2PhotoEngine = bool.TrueString.ToLower().Equals(settingFromSingleton);
                }
            }
            catch (Exception e)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + ".UseE2PhotoEngine", e, null);
            }
            return useE2PhotoEngine;
        }
	
	}
}
