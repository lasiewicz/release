using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.Geo;

namespace Matchnet.PhotoSearch.BusinessLogic
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	/// 

	public class PhotoGalleryBL
	{
		const string MODULE_NAME="PhotoGalleryBL";
		const string PHOTOSTORE_LDB="mnPhotoStore";
		const string PHOTOSTORE_TABLE="mnPhotoStore";

		const string PHOTOSTORE_SP_SEARCHBYCOUNTRY="up_PhotoStoreSearch_ByCountry";
		const string PHOTOSTORE_SP_SEARCHBYREGION="up_PhotoStoreSearch_ByRegion";
		const string PHOTOSTORE_SP_SEARCHBYREGIONANY="up_PhotoStoreSearch_NotByRegion";

		const string PHOTOSTORE_SP_OPTIONS="up_PhotoStoreSearch_Options_List";
		private Matchnet.Caching.Cache _cache;
		public delegate void SearchRequestEventHandler(bool cacheHit, int count);
		public event SearchRequestEventHandler SearchRequested;

		public delegate void SearchRefreshEventHandler();
		public event SearchRefreshEventHandler SearchRefreshRequested;
		//singleton implementation
		public readonly static PhotoGalleryBL Instance = new PhotoGalleryBL();
        private ISettingsSA _settingsService = null;
        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

		private PhotoGalleryBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
			
		}
		#region search methods
		public PhotoGalleryResultList Search(PhotoGalleryQuery query)
		{
            System.Diagnostics.Trace.WriteLine("PhotoGalleryBL.Search - Entered");
			string functionName="Search";
			PhotoGalleryResultList resultsList=null;
			SqlDataReader reader=null;
			ArrayList results=null;
			try
			{
				if(query==null)
					{throw new Exception("Query cannot be null");}

				string key=query.GetQueryCacheKey();
				resultsList =(PhotoGalleryResultList)_cache.Get(key);
				if(resultsList==null)
				{
                    results = new ArrayList();
				    bool useE2PhotoEngine = UseE2PhotoEngine(query.SiteID, query.CommunityID);

                    if (useE2PhotoEngine)
                    {
                        System.Diagnostics.Trace.WriteLine("PhotoGalleryBL.Search - using E2 Photo Engine");

                        //convert query
                        Spark.PhotoSearchEngine.ValueObjects.SearchParameterCollection searchParameterCollection = new Spark.PhotoSearchEngine.ValueObjects.SearchParameterCollection()
                            .Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("maxResults", 1000, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.MaxResults))
                            .Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("birthdate", query.AgeMin + "|" + query.AgeMax, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.AgeRangeFilter))
                            .Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("communityid", query.CommunityID, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.Int))
                            .Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("displaytoguestflag", (query.GuestFlag) ? 1 : 0, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.Int));

                        DateTime insertMin = DateTime.Now.AddDays(-query.InsertDays);
                        DateTime insertMax = DateTime.Now.AddDays(1);
                        searchParameterCollection.Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("insertdate",insertMin+"|"+insertMax,Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.DateRangeFilter));

                        if (query.Gender > 0)
                        {
                            searchParameterCollection.Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("gender", query.Gender, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.Int));
                        }

                        if (!query.ShowHiddenPhotosFlag)
                        {
                            searchParameterCollection.Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("privateflag", 0, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.Int));
                        }

                        switch(query.RegionType)
                        {
                            case SearchRegionType.myregion:
                                searchParameterCollection.Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("location", query.RegionID + "|" + query.Radius, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.LocationRegion));
                                break;
                            case SearchRegionType.country:
                                searchParameterCollection.Add(new Spark.PhotoSearchEngine.ValueObjects.SearchParameter("depth1regionid", query.CountryRegionID, Spark.PhotoSearchEngine.ValueObjects.SearchParameterType.Int));
                                break;
                            default:
                                break;
                        }

                        searchParameterCollection.Sort = Spark.PhotoSearchEngine.ValueObjects.SearchSorting.InsertDate;
                        searchParameterCollection.ResultType = Spark.PhotoSearchEngine.ValueObjects.SearchResultType.TerseResult;
                        //run query
                        ArrayList photos = Spark.PhotoSearchEngine.ServiceAdapters.SearcherSA.Instance.RunQuery(searchParameterCollection, query.CommunityID);
                        //convert to Photo Gallery Result Items
                        if (photos.Count > 0)
                        {
                            foreach (Hashtable h in photos)
                            {
                                results.Add(new PhotoResultItem((int)h["memberid"], (int)h["memberphotoid"], (DateTime)h["insertdate"]));
                            }
                        }
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("PhotoGalleryBL.Search - using regular search");
                        reader = RunQuery(query);
                        results = PopulateResults(reader, query);
                    }

				    if (results.Count > 0)
					{
						resultsList=new PhotoGalleryResultList(key);
						resultsList.PhotoGalleryResults=results;
						_cache.Add(resultsList);
						SearchRequested(false, results.Count);
					}
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Search not from cache:" + query.ToString());
				}
				else
				{
					//refresh first 10 pages
					if(query.Page > 0)
					{
						int startPoint=0;
						if(resultsList.PhotoGalleryResults.Count >= query.PageSize * query.Page + 1)
							startPoint=query.PageSize * query.Page ;
						else
							startPoint=resultsList.PhotoGalleryResults.Count-1;

						query.InsertDateMin=((PhotoResultItem)resultsList.PhotoGalleryResults[startPoint]).PhotoInsertDate;
						ArrayList list=RefreshSearchResults(query);
						resultsList.PhotoGalleryResults.RemoveRange(0,startPoint);
						resultsList.PhotoGalleryResults.InsertRange(0,list);
						
					}
					ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Search from cache:" + query.ToString());
					SearchRequested(true,resultsList.PhotoGalleryResults.Count);
				}

				return resultsList;
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".Search",ex,query);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException,query,0);}
			finally
			{
				if(reader!= null)
					reader.Close();
			}
		}

	    public bool UseE2PhotoEngine(int siteId, int communityId)
	    {
	        bool useE2PhotoEngine=false;
            try
            {
                string settingFromSingleton = SettingsService.GetSettingFromSingleton("USE_E2_PHOTO_ENGINE", communityId, siteId);
                if (null != settingFromSingleton)
                {
                    useE2PhotoEngine = bool.TrueString.ToLower().Equals(settingFromSingleton);
                }
            }
            catch (Exception e)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + ".UseE2PhotoEngine", e, null);
            }
	        return useE2PhotoEngine;
	    }


	    public ArrayList RefreshSearchResults(PhotoGalleryQuery query)
		{
			string functionName="RefreshSearchResults";
			SqlDataReader reader=null;
			ArrayList results=null;
			try
			{
				if(query==null)
					{throw new Exception("Query cannot be null");}

				if(query.InsertDateMin == new DateTime())
					{throw new Exception("Queries InsertDateMin cannot be null");}
				SearchRefreshRequested();				
				reader=RunQuery(query);
				results=PopulateResults(reader,query);
				ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,"Refresh:" + query.ToString());
				return results;
				
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".RefreshSearchResults",ex,query);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException,query,0);
			}
			finally
			{
				if(reader!= null)
					reader.Close();
			}
		}


		public SqlDataReader RunQuery(PhotoGalleryQuery query)
		{
			Command comm=null;
			SqlDataReader reader=null;
			bool regionAnyFlag=false;
			try
			{
				string spName="";
				if(query==null )
					{throw new Exception("Query cannot be null");}

				if(query.RegionType==SearchRegionType.myregion )
				{
					if(query.SearchBox ==null)
					{throw new Exception("Query Search box cannot be null for SearchByRegion");}
					spName=PHOTOSTORE_SP_SEARCHBYREGION;
				}
				else if (query.RegionType==SearchRegionType.country )
				{
					spName=PHOTOSTORE_SP_SEARCHBYCOUNTRY;
				}
				else
				{	
					spName=PHOTOSTORE_SP_SEARCHBYREGIONANY;
					regionAnyFlag=true;
				}

				comm=CreateSearchBaseCommand(query,spName);
				if(!regionAnyFlag)
					AddRegionToCommand(comm,query);
			
				reader=Client.Instance.ExecuteReader(comm);
				return reader;
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".RunQuery",ex,query);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException,query,0);
			}
			
		}
		

		public  PhotoStats GetPhotoCommunityCounts(int hours)
		{
			
			Command comm=null;
			SqlDataReader rs=null;
			PhotoStats stats=null;
			try
			{
				 stats=(PhotoStats)_cache.Get(PhotoStats.GetCacheKey(hours));
				if(stats != null && stats.Stats != null)
					return stats;
				comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName="up_GetPhotoCounts";

					
				comm.AddParameter("@inserthours", SqlDbType.Int, ParameterDirection.Input, hours);
					
				rs=Client.Instance.ExecuteReader(comm);
				stats=new PhotoStats(hours);
				while(rs.Read())
				{
					int communityid;
					int count;
						
					communityid=(int) (rs["groupid"]!=Convert.DBNull ? rs["groupid"] : Constants.NULL_INT);
					count=(int) (rs["photocount"]!=Convert.DBNull ? rs["photocount"] : Constants.NULL_INT);
					if(communityid > 0 && count >=0)
					{
						stats.Add(communityid,count);
					}

				}
				_cache.Insert(stats);
				return stats;
			}
			
			catch(Exception ex)
			{
				ServiceTrace.Instance.Trace(MODULE_NAME + ".GetPhotoCommunityCounts",ex,null);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);
			}
			finally
			{
				if(rs!= null  && !rs.IsClosed)
				{
					rs.Close();
			
				}
				
			}

		}
		private Command CreateSearchBaseCommand(PhotoGalleryQuery query, string spName)
		{
			Command comm=null;
			try
			{
				if (query==null)
				{throw new Exception("Photo Gallery Query cannot be null");}

				comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName=spName;

				comm.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, query.CommunityID);
				comm.AddParameter("@insertdays", SqlDbType.Int, ParameterDirection.Input, query.InsertDays);
				
				if(query.InsertDateMin != new DateTime())
					comm.AddParameter("@insertdate", SqlDbType.DateTime, ParameterDirection.Input, query.InsertDateMin);
				
				comm.AddParameter("@agemin", SqlDbType.Int, ParameterDirection.Input, query.AgeMin);
				comm.AddParameter("@agemax", SqlDbType.Int, ParameterDirection.Input, query.AgeMax);
				comm.AddParameter("@gender", SqlDbType.Int, ParameterDirection.Input, query.Gender);
				comm.AddParameter("@guestflag", SqlDbType.Bit, ParameterDirection.Input, query.GuestFlag);
				comm.AddParameter("@displayprivateflag", SqlDbType.Bit, ParameterDirection.Input, query.ShowHiddenPhotosFlag);

				return comm;
			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".CreateSearchBaseCommand",ex,query);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);
			}

		}


		private void AddRegionToCommand(Command comm,PhotoGalleryQuery query)
		{
			try
			{
				if (query==null)
				{throw new Exception("Photo Gallery Query cannot be null");}

				if (comm==null)
					{throw new Exception("Command for Photo Gallery Query cannot be null");}

				comm.AddParameter("@countryid", SqlDbType.Int, ParameterDirection.Input, query.CountryRegionID);
				
				if (query.SearchBox !=null)
				{
					comm.AddParameter("@boxxmin", SqlDbType.Int, ParameterDirection.Input, query.SearchBox.SearchBox.Area.BoxXMin);
					comm.AddParameter("@boxxmax", SqlDbType.Int, ParameterDirection.Input, query.SearchBox.SearchBox.Area.BoxXMax);
					comm.AddParameter("@boxymin", SqlDbType.Int, ParameterDirection.Input, query.SearchBox.SearchBox.Area.BoxYMin);
					comm.AddParameter("@boxymax", SqlDbType.Int, ParameterDirection.Input, query.SearchBox.SearchBox.Area.BoxYMax);
				}

			}
			catch(Exception ex)
			{	ServiceTrace.Instance.Trace(MODULE_NAME + ".AddRegionToCommand",ex,query);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);
			}

		}


		private ArrayList PopulateResults(SqlDataReader rs, PhotoGalleryQuery query)
		{
			try
				
				{ArrayList results=new ArrayList();
				
				while(rs.Read())
				{
					int memberid;
					int memberphotoid;
					DateTime insertdate;
					decimal latitude;
					decimal longitude;


					memberid=(int) (rs["memberid"]!=Convert.DBNull ? rs["memberid"] : 0);
					memberphotoid=(int) (rs["memberid"]!=Convert.DBNull ? rs["memberphotoid"] : 0);
					insertdate = (DateTime) (rs["insertdate"]!=Convert.DBNull ? rs["insertdate"] : new DateTime());
					

					if(query.RegionType==SearchRegionType.myregion)
					{
						latitude = (decimal) (rs["latitude"]!=Convert.DBNull ? rs["latitude"] : 0.0);
						longitude = (decimal) (rs["longitude"]!=Convert.DBNull ? rs["longitude"] : 0.0);
						Coordinates coord2=new Coordinates((double)latitude,(double)longitude);
						double dist=Geo.GeoUtil.CalculateDistance(query.SearchBox.BoxCoordinates,coord2,query.SearchBox.MetricFlag);
						if (dist <=query.Radius)
						{
							PhotoResultItem item=new PhotoResultItem(memberid,memberphotoid,insertdate);
							results.Add(item);
						}
					}
					else
					{
						PhotoResultItem item=new PhotoResultItem(memberid,memberphotoid,insertdate);
						results.Add(item);
					}
					
					

				}

				return results;
			}
			catch(Exception ex)
			{		ServiceTrace.Instance.Trace(MODULE_NAME + ".PopulateResults",ex,query);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);
			}

		}
		
		#endregion

		#region search filter methods

		public QueryOptionCollection GetQueryOptionCollection(int siteid, QueryOptionType optiontype)
		{

            System.Diagnostics.Trace.WriteLine("GetQueryOptionCollection - Entered");
			SqlDataReader reader=null;
			
			Command comm=null;
			QueryOptionCollection options=null;
			try
             {
				string key=QueryOptionCollection.GetCacheKey(siteid,optiontype);
				options=(QueryOptionCollection)_cache.Get(key);
				if(options!=null)
					{return options;}

				comm=new Command();
				comm.LogicalDatabaseName=PHOTOSTORE_LDB;
				comm.StoredProcedureName=PHOTOSTORE_SP_OPTIONS;
				comm.AddParameter("@siteid", SqlDbType.Int, ParameterDirection.Input, siteid);
				comm.AddParameter("@optiontypeid", SqlDbType.Int, ParameterDirection.Input, (int)optiontype);
				
				options=new QueryOptionCollection(siteid,optiontype);
				reader=Client.Instance.ExecuteReader(comm);
				while(reader.Read())
				{
					int optionvalue=(int) (reader["OptionValue"]!=Convert.DBNull ? reader["OptionValue"] : 0);
					int listorder=(int) (reader["listorder"]!=Convert.DBNull ? reader["listorder"] : 0);
					bool defaultflag=(bool) (reader["defaultflag"]!=Convert.DBNull ? reader["defaultflag"] : false);
					QueryOption option=new QueryOption(optionvalue,listorder,defaultflag);
					if (defaultflag)
						options.DefaultOption=optionvalue;

					options.Add(option);
				}
				_cache.Add(options);

                System.Diagnostics.Trace.WriteLine("GetQueryOptionCollection - Leaving");
				return options;
			}
			catch(Exception ex)
			{
                System.Diagnostics.Trace.WriteLine("GetQueryOptionCollection - Error: " + ex.Message);
                ServiceTrace.Instance.Trace(MODULE_NAME + ".GetQueryOptionCollection",ex,null);
				throw new ServiceBoundaryException(ex.Source,ex.Message,ex.InnerException);
			}
			finally
			{
				if(reader!= null)
					reader.Close();
			}
		}


		#endregion
	}
}
