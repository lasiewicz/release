using System;
using System.Collections;
using System.Diagnostics;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.PhotoSearch.ServiceDefinitions;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.BusinessLogic;

namespace Matchnet.PhotoSearch.ServiceManagers
{
	/// <summary>
	/// Summary description for PhotoGallerySM.
	/// </summary>
	public class PhotoGallerySM: MarshalByRefObject, IPhotoSearchService, IServiceManager, IDisposable
	{
		//member perf
		private PerformanceCounter _perfPhotoGalleryRequestsSec;
		private PerformanceCounter _perfPhotoGalleryRefreshSec;
		private PerformanceCounter _perfPhotoGalleryHitRate;
		private PerformanceCounter _perfPhotoGalleryHitRate_Base;
		private PerformanceCounter _perfPhotoGalleryRequestItemsSec;
		public PhotoGallerySM()
		{
			try
			{
				System.Diagnostics.Trace.WriteLine("In PhotoGallerySM constructor");
				initPerfCounters();
				PhotoGalleryBL.Instance.SearchRequested += new PhotoGalleryBL.SearchRequestEventHandler(PhotoGalleryBL_SearchRequested);
				PhotoGalleryBL.Instance.SearchRefreshRequested += new PhotoGalleryBL.SearchRefreshEventHandler(PhotoGalleryBL_SearchRefreshRequested);
			}
			catch(Exception ex)
			{
			System.Diagnostics.Trace.WriteLine("In PhotoGallerySM exception block:" + ex.Message);
			}
		}
		
		public override object InitializeLifetimeService()
		{return null;}

		public void PrePopulateCache()
		{}
		#region PhotoGallery service implementation
		public PhotoGalleryResultList Search(PhotoGalleryQuery query)
		{
			return PhotoGalleryBL.Instance.Search(query);
		}
		public ArrayList RefreshSearchResults(PhotoGalleryQuery query)
		{
			return PhotoGalleryBL.Instance.RefreshSearchResults(query);
		}
		public QueryOptionCollection GetQueryOptionCollection(int siteid, QueryOptionType optiontype)
		{
			return PhotoGalleryBL.Instance.GetQueryOptionCollection(siteid,optiontype);
		}
		public PhotoStats GetPhotoCommunityCounts(int hours)
		{
			return PhotoGalleryBL.Instance.GetPhotoCommunityCounts(hours);
		}
		#endregion

		#region performance counters
		public static void PerfCounterInstall()
		{
			try
			{
				ServiceTrace.Instance.DebugTrace("PhotoGallerySM","Start PerfCounterInstall");
				CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
				ccdc.AddRange(new CounterCreationData [] {	
															 new CounterCreationData("PhotoGallery Search Requests/second", "PhotoGallery Search Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("PhotoGallery Search CacheHit Rate", "PhotoGallery Search CacheHit Rate", PerformanceCounterType.RawFraction),
															 new CounterCreationData("PhotoGallery Search CacheHit Rate_Base","PhotoGallery Search CacheHit Rate Base", PerformanceCounterType.RawBase),
															 new CounterCreationData("PhotoGallery Search Refresh/second", "PhotoGallery Search Refresh/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("PhotoGallery Search Request Items/second", "PhotoGallery Search Request Items/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 
														 
														 });
				ccdc.AddRange(PhotoStoreSM.GetCounters());
				PerformanceCounterCategory.Create(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
				ServiceTrace.Instance.DebugTrace("PhotoGallerySM","Finish PerfCounterInstall");
			}
			catch(Exception ex)
			{ ServiceTrace.Instance.Trace("PhotoGallerySM",ex,null);}
		}


		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME);
		}
		private void initPerfCounters()
		{
		try
		 {
			 ServiceTrace.Instance.DebugTrace("PhotoGallerySM","Start initPerfCounters");
			 _perfPhotoGalleryRequestsSec = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoGallery Search Requests/second", false);
			 _perfPhotoGalleryHitRate = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoGallery Search_CacheHit Rate", false);
			 _perfPhotoGalleryHitRate_Base = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoGallery Search CacheHit Rate Base", false);
			 _perfPhotoGalleryRefreshSec=new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoGallery Search Refresh/second", false);
			 _perfPhotoGalleryRequestItemsSec = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoGallery Search Request Items/second", false);
			 resetPerfCounters();
			 ServiceTrace.Instance.DebugTrace("PhotoGallerySM","Finish initPerfCounters");
		 }
		 catch(Exception ex)
		 { ServiceTrace.Instance.Trace("PhotoGallerySM" + ".initPerfCounters",ex,null);}
		}


		private void resetPerfCounters()
		{
			_perfPhotoGalleryRequestsSec.RawValue = 0;
			_perfPhotoGalleryHitRate.RawValue = 0;
			_perfPhotoGalleryHitRate_Base.RawValue=0;
			_perfPhotoGalleryRefreshSec.RawValue=0;
			_perfPhotoGalleryRequestItemsSec.RawValue=0;
		
		}
		private void PhotoGalleryBL_SearchRequested(bool cacheHit, int count)
		{
			_perfPhotoGalleryRequestsSec.Increment();
			_perfPhotoGalleryRequestItemsSec.IncrementBy(count);
			_perfPhotoGalleryHitRate_Base.Increment();
			if (cacheHit)
			{
				_perfPhotoGalleryHitRate.Increment();
			}
		}

		private void PhotoGalleryBL_SearchRefreshRequested()
		{
			_perfPhotoGalleryRequestsSec.Increment();		}
#endregion


		
		#region IDisposable Members
		public void Dispose()
		{
					}
		#endregion
	}
}
