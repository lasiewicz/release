using System;
using System.Collections;
using System.Diagnostics;

using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.PhotoSearch.ServiceDefinitions;
using Matchnet.PhotoSearch.ValueObjects;
using Matchnet.PhotoSearch.BusinessLogic;


namespace Matchnet.PhotoSearch.ServiceManagers
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class PhotoStoreSM: MarshalByRefObject, IPhotoStoreService, IServiceManager, IDisposable
	{
		private PerformanceCounter _perfHitRateSec;
		private PerformanceCounter _perfInsertPhotoRequestsSec;
		private PerformanceCounter _perfDeletePhotoRequestsSec;
		private PerformanceCounter _perfUpdatePhotoRequestsSec;
		private PerformanceCounter _perfUpdateMemberRequestsSec;
		private PerformanceCounter _perfDeleteMemberRequestsSec;
		private HydraWriter _hydraWriter = null;
		public PhotoStoreSM()
		{
			try{

			System.Diagnostics.Trace.WriteLine("In PhotoStoreSM constructor");
			initPerfCounters();
			_hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnPhotoStore"});
			_hydraWriter.Start();
			PhotoStoreBL.Instance.PhotoStoreUpdateRequested += new PhotoStoreBL.PhotoStoreUpdateEventHandler(PhotoStoreBL_PhotoUpdateRequested);
			PhotoStoreBL.Instance.PhotoStoreMemberUpdateRequested += new PhotoStoreBL.PhotoStoreMemberUpdateEventHandler(PhotoStoreBL_MemberUpdateRequested);
		}
		catch(Exception ex)
		{
		System.Diagnostics.Trace.WriteLine("In PhotoGallerySM exception block:" + ex.Message);
		}
	}

		public override object InitializeLifetimeService()
		{return null;}

		public void PrePopulateCache()
		{}

		#region service implementation

		public void UpdateMember(PhotoStoreUpdate[] item)
		{
			PhotoStoreBL.Instance.UpdateMember(item);
		}
		
		public void SavePhoto(PhotoStoreUpdate item)
		{
			if(item.Action!=PhotoStoreAction.delete)
				PhotoStoreBL.Instance.SavePhoto(item);
			else  
				PhotoStoreBL.Instance.DeletePhoto(item);
		}

		public void SavePhotos(ArrayList item)
		{
			for(int i=0; i < item.Count;i++)
			{
				PhotoStoreUpdate store=(PhotoStoreUpdate) item[i];
				SavePhoto(store);

			}
		}
	#endregion

		public void StartWriter()
		{
			_hydraWriter.Start();
		}


		public void StopWriter()
		{
			_hydraWriter.Stop();
		}
		#region performance counters
		public static CounterCreationData[] GetCounters()
		{	
			  return new CounterCreationData [] {	
				new CounterCreationData("PhotoStore Insert Requests/second", "PhotoStore Insert Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("PhotoStore Update Requests/second", "PhotoStore Update Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("PhotoStore Delete Requests/second", "PhotoStore Delete Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("PhotoStore MemberUpdate Requests/second", "PhotoStore MemberUpdate Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("PhotoStore MemberDelete Requests/second", "PhotoStore MemberDelete Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("PhotoStore Hit Rate/second", "PhotoStore  Hit Rate/second", PerformanceCounterType.RateOfCountsPerSecond32)
				};
			
			//PerformanceCounterCategory.Create(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
		}


//		public static void PerfCounterUninstall()
//		{	
//			PerformanceCounterCategory.Delete(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME);
//		}
		private void initPerfCounters()
		{
			_perfInsertPhotoRequestsSec = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoStore Insert Requests/second", false);
			_perfUpdatePhotoRequestsSec = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoStore Update Requests/second", false);
			_perfDeletePhotoRequestsSec = new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoStore Delete Requests/second", false);
			_perfUpdateMemberRequestsSec= new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoStore UpdateMember Requests/second", false);
			_perfDeleteMemberRequestsSec= new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoStore DeleteMember Requests/second", false);
			_perfHitRateSec= new PerformanceCounter(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, "PhotoStore Hit Rate/second", false);
		}


		private void resetPerfCounters()
		{
			_perfInsertPhotoRequestsSec.RawValue = 0;
			_perfUpdatePhotoRequestsSec.RawValue = 0;
			_perfDeletePhotoRequestsSec.RawValue=0;
			_perfUpdateMemberRequestsSec.RawValue=0;
			_perfDeleteMemberRequestsSec.RawValue=0;
			_perfHitRateSec.RawValue=0;

		
		}
		private void PhotoStoreBL_PhotoUpdateRequested(PhotoStoreAction action)
		{
			switch(action)
			{
				case PhotoStoreAction.update:
				case PhotoStoreAction.none:
				{
					_perfUpdatePhotoRequestsSec.Increment();
					break;

				}
				case PhotoStoreAction.insert:
			
				{
					_perfInsertPhotoRequestsSec.Increment();
					break;

				}
				case PhotoStoreAction.delete:
				{
					_perfDeletePhotoRequestsSec.Increment();
					break;

				}
				default:
				{break;}

			}
			_perfHitRateSec.Increment();
		}

		private void PhotoStoreBL_MemberUpdateRequested(PhotoStoreAction action)
		{
			switch(action)
			{
				case PhotoStoreAction.update:
				case PhotoStoreAction.none:
				{
					_perfUpdateMemberRequestsSec.Increment();
					break;

				}
				
				case PhotoStoreAction.delete:
				{
					_perfDeleteMemberRequestsSec.Increment();
					break;

				}
				default:
				{break;}

			}
			_perfHitRateSec.Increment();
		}
		#endregion

		
		#region IDisposable Members
		public void Dispose()
		{
			if (_hydraWriter != null)
			{
				_hydraWriter.Stop();
			}

			resetPerfCounters();
		}
		#endregion
	}
}
