using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Data;

using Matchnet.Search.Interfaces;

namespace Matchnet.e1.ValueObjects
{
    [Serializable]
    public class E1Member : ISerializable
    {
        #region Private Variables
        private Int32 _memberID;
        private Int32 _communityID;
		private Int32 _genderMask;
        #region Location
        private float _latitude;
        private float _longitude;
        private string _country = string.Empty;
        private string _state = string.Empty;
        private string _city = string.Empty;
        private string _zip = string.Empty;
        private string _areaCode = string.Empty;
        #endregion
        private DateTime _birthDate = DateTime.MinValue;
        private DateTime _registerDate = DateTime.MinValue;
        private DateTime _activeDate = DateTime.MinValue;
		private Int32 _height;
		private Int32 _weight;
		private Int32 _childrenCount;

        private UpdateFlag _hasPhoto = UpdateFlag.NoChange;
        private UpdateFlag _isOnline = UpdateFlag.NoChange;
        private Int32 _relocateFlag;
		private Int32 _emailCount;
        private Dictionary<string, string> _tags = new Dictionary<string,string>();
        #endregion

        #region Constructor
        public E1Member()
        {
        }


        public E1Member(SerializationInfo info, StreamingContext context)
        {
            MemoryStream ms = new MemoryStream((byte[]) info.GetValue("bytes", typeof(byte[])));
            BinaryReader br = new BinaryReader(ms);

            FromBytes(br);
        }
        #endregion

        #region Public Properties
        public Int32 MemberID
		{
            get
            {
                return _memberID;
            }
            set
            {
                _memberID = value;
            }
		}

		public Int32 CommunityID
		{
            get
            {
                return _communityID;
            }
            set
            {
                _communityID = value;
            }
		}

		public Int32 GenderMask
		{
            get
            {
                return _genderMask;
            }
            set
            {
                _genderMask = value;
            }
        }

        #region Location
        public float Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
            }
        }

        public float Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
            }
        }

        public string Country
        {
            get
            {
                return _country;
            }
            set
            {
                _country = value != null ? value : string.Empty;
            }
        }

        public string State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value != null ? value : string.Empty;
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value != null ? value : string.Empty;
            }
        }

        public string Zip
        {
            get
            {
                return _zip;
            }
            set
            {
                _zip = value != null ? value : string.Empty;
            }
        }

        public string AreaCode
        {
            get
            {
                return _areaCode;
            }
            set
            {
                _areaCode = value != null ? value : string.Empty;
            }
        }
        #endregion

        public DateTime BirthDate
		{
            get
            {
                return _birthDate;
            }
            set
            {
                _birthDate = value;
            }
		}

		public DateTime RegisterDate
		{
            get
            {
                return _registerDate;
            }
            set
            {
                _registerDate = value;
            }
        }

		public DateTime ActiveDate
		{
            get
            {
                return _activeDate;
            }
            set
            {
                _activeDate = value;
            }
        }

		public Int32 Height
		{
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

		public Int32 Weight
		{
            get
            {
                return _weight;
            }
            set
            {
                _weight = value;
            }
        }

		public Int32 ChildrenCount
		{
            get
            {
                return _childrenCount;
            }
            set
            {
                _childrenCount = value;
            }
        }

        public UpdateFlag HasPhoto
		{
            get
            {
                return _hasPhoto;
            }
            set
            {
                _hasPhoto = value;
            }
        }

        public UpdateFlag IsOnline 
		{
            get
            {
                return _isOnline;
            }
            set
            {
                _isOnline = value;
            }
        }

        public Int32 RelocateFlag
        {
            get
            {
                return _relocateFlag;
            }
            set
            {
                _relocateFlag = value;
            }
        }

		public Int32 EmailCount
		{
            get
            {
                return _emailCount;
            }
            set
            {
                _emailCount = value;
            }
        }

        public Dictionary<string, string> Tags
        {
            get
            {
                return _tags;
            }
        }
        #endregion

        #region ISerializable Members

        public void FromBytes(BinaryReader br)
        {
            _memberID = br.ReadInt32();
            _communityID = br.ReadInt32();
            _genderMask = br.ReadInt32();
            _latitude = br.ReadSingle();
            _longitude = br.ReadSingle();
            _country = br.ReadString();
            _state = br.ReadString();
            _city = br.ReadString();
            _zip = br.ReadString();
            _areaCode = br.ReadString();
            _birthDate = new DateTime(br.ReadInt64());
            _registerDate = new DateTime(br.ReadInt64());
            _activeDate = new DateTime(br.ReadInt64());
            _height = br.ReadInt32();
            _weight = br.ReadInt32();
            _childrenCount = br.ReadInt32();
            _hasPhoto = (UpdateFlag) br.ReadByte();
            _isOnline = (UpdateFlag) br.ReadByte();
            _relocateFlag = br.ReadInt32();
            _emailCount = br.ReadInt32();

            Int32 count = br.ReadInt32();

            for (Int32 i = 0; i < count; i++)
            {
                _tags.Add(br.ReadString(), br.ReadString());
            }
        }

        public void ToBytes(BinaryWriter bw)
        {
            bw.Write(_memberID);
            bw.Write(_communityID);
            bw.Write(_genderMask);
            bw.Write(_latitude);
            bw.Write(_longitude);
            writeString(bw, _country);
            writeString(bw, _state);
            writeString(bw, _city);
            writeString(bw, _zip);
            writeString(bw, _areaCode);
            bw.Write(_birthDate.Ticks);
            bw.Write(_registerDate.Ticks);
            bw.Write(_activeDate.Ticks);
            bw.Write(_height);
            bw.Write(_weight);
            bw.Write(_childrenCount);
            bw.Write((byte) _hasPhoto);
            bw.Write((byte) _isOnline);
            bw.Write(_relocateFlag);
            bw.Write(_emailCount);

            bw.Write(_tags.Count);

            foreach (string name in _tags.Keys)
            {
                writeString(bw, name);
                writeString(bw, _tags[name]);
            }
        }

        private void writeString(BinaryWriter bw, string s)
        {
            if (s == null)
            {
                bw.Write(string.Empty);
            }
            else
            {
                bw.Write(s);
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            MemoryStream ms = new MemoryStream(256); //approx byte count
            BinaryWriter bw = new BinaryWriter(ms);

            ToBytes(bw);

            info.AddValue("bytes", ms.ToArray());
        }

        #endregion

        /// <summary>
        /// List of columns that do not correspond to specific fields in the MemberSpec object
        /// and therefore are loaded as tags.
        /// </summary>
        public static SearchMemberColumn[] TagNames = new SearchMemberColumn[] { SearchMemberColumn.EducationLevel,
	        SearchMemberColumn.Religion,
	        SearchMemberColumn.LanguageMask,
	        SearchMemberColumn.Ethnicity,
	        SearchMemberColumn.SmokingHabits,
	        SearchMemberColumn.DrinkingHabits,
	        SearchMemberColumn.MaritalStatus,
	        SearchMemberColumn.JDateReligion,
	        SearchMemberColumn.JDateEthnicity,
	        SearchMemberColumn.SynagogueAttendance,
	        SearchMemberColumn.KeepKosher,
	        SearchMemberColumn.SexualIdentityType,
	        SearchMemberColumn.RelationshipMask,
	        SearchMemberColumn.RelationshipStatus,
	        SearchMemberColumn.BodyType,
	        SearchMemberColumn.Zodiac,
	        SearchMemberColumn.MajorType,
	        SearchMemberColumn.SchoolID,
            SearchMemberColumn.SubscriptionExpirationDate,
            SearchMemberColumn.ActivityLevel,
            SearchMemberColumn.Custody,
            SearchMemberColumn.MoreChildrenFlag,
            SearchMemberColumn.RelocateFlag,
            SearchMemberColumn.ColorCode
        };

        private static Dictionary<SearchMemberColumn, SqlDbType> _tagTypes;
        private static System.Threading.ReaderWriterLock _tagTypesLock = new System.Threading.ReaderWriterLock();

        public static Dictionary<SearchMemberColumn, SqlDbType> TagTypes
        {
            get
            {
                if (_tagTypes == null)
                {
                    //lock (_tagTypesLock)
                    //{
                    _tagTypesLock.AcquireWriterLock(-1);
                        if (_tagTypes == null)
                        {
                            _tagTypes = new Dictionary<SearchMemberColumn, SqlDbType>();
                            _tagTypes.Add(SearchMemberColumn.EducationLevel, SqlDbType.SmallInt);
                            _tagTypes.Add(SearchMemberColumn.Religion, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.LanguageMask, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.Ethnicity, SqlDbType.SmallInt);
                            _tagTypes.Add(SearchMemberColumn.SmokingHabits, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.DrinkingHabits, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.MaritalStatus, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.JDateReligion, SqlDbType.SmallInt);
                            _tagTypes.Add(SearchMemberColumn.JDateEthnicity, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.SynagogueAttendance, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.KeepKosher, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.SexualIdentityType, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.RelationshipMask, SqlDbType.SmallInt);
                            _tagTypes.Add(SearchMemberColumn.RelationshipStatus, SqlDbType.TinyInt);
                            _tagTypes.Add(SearchMemberColumn.BodyType, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.Zodiac, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.MajorType, SqlDbType.SmallInt);
                            _tagTypes.Add(SearchMemberColumn.SchoolID, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.RelocateFlag, SqlDbType.SmallInt);
                            _tagTypes.Add(SearchMemberColumn.SubscriptionExpirationDate, SqlDbType.SmallDateTime);
                            _tagTypes.Add(SearchMemberColumn.ActivityLevel, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.Custody, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.MoreChildrenFlag, SqlDbType.Int);
                            _tagTypes.Add(SearchMemberColumn.ColorCode, SqlDbType.Int);
                        }
                    _tagTypesLock.ReleaseLock();
                    
                }

                return _tagTypes;
            }
        }
    }

    /// <summary>
    /// List of all columns in SearchMember table
    /// </summary>
    public enum SearchMemberColumn
    {
        MemberID,
        CommunityID,
        GenderMask,
        BirthDate,
        CommunityInsertDate,
        LastActiveDate,
        HasPhotoFlag,
        EducationLevel,
        Religion,
        LanguageMask,
        Ethnicity,
        SmokingHabits,
        DrinkingHabits,
        Height,
        MaritalStatus,
        JDateReligion,
        JDateEthnicity,
        SynagogueAttendance,
        KeepKosher,
        SexualIdentityType,
        RelationshipMask,
        RelationshipStatus,
        BodyType,
        Zodiac,
        MajorType,
        Longitude,
        Latitude,
        Depth1RegionID,
        Depth2RegionID,
        Depth3RegionID,
        Depth4RegionID,
        SchoolID,
        AreaCode,
        EmailCount,
        Weight,
        RelocateFlag,
        ChildrenCount,
        SubscriptionExpirationDate,
        ActivityLevel,
        Custody,
        MoreChildrenFlag,
        ColorCode
    }
}