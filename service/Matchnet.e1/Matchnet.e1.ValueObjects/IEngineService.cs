using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Matchnet.Search.Interfaces;

namespace Matchnet.e1.ValueObjects
{
    public interface IEngineService
    {
        CommandResult EvaluateCommand(String command);
        IMatchnetQueryResults RunQuery(IMatchnetQuery query);
        ArrayList RunDetailedQuery(IMatchnetQuery query);
        void LoadMembers(E1UpdateCollection e1UpdateCollection);
        EngineStatus GetEngineStatus();
        void ChangeMaintenance(bool enabled);
    }
}
