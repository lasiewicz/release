using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.e1.ValueObjects
{
    [Serializable]
    public class CommandResult : IValueObject
    {
        private String _xml;
        private Int32 _errorCode;

        public CommandResult(String xml, Int32 errorCode)
        {
            _xml = xml;
            _errorCode = errorCode;
        }

        public String Xml
        {
            get
            {
                return _xml;
            }
        }

        public Int32 ErrorCode
        {
            get
            {
                return _errorCode;
            }
        }
    }
}
