using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace Matchnet.e1.ValueObjects
{
    [Serializable]
    public class E1MemberCollection : IEnumerable<E1Member>, ISerializable
    {
        //private List<E1Member> _e1Members;
        private MemoryStream _ms = new MemoryStream();
        private BinaryWriter _bw;
        private Int32 _count = 0;

        public E1MemberCollection(Int32 capacity)
        {
            _bw = new BinaryWriter(_ms);
            _bw.Write(_count);
            //_e1Members = new List<E1Member>(capacity);
        }

        public E1MemberCollection(SerializationInfo info, StreamingContext context)
        {
            _ms = new MemoryStream((byte[])info.GetValue("bytes", typeof(byte[])));
            BinaryReader br = new BinaryReader(_ms);
            _count = br.ReadInt32();

            //MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytes", typeof(byte[])));
            //BinaryReader br = new BinaryReader(ms);

            //FromBytes(br);
        }

        public void Add(E1Member e1Member)
        {
            _bw.Seek(0, SeekOrigin.End);
            e1Member.ToBytes(_bw);
            _count++;
            _bw.Seek(0, SeekOrigin.Begin);
            _bw.Write(_count);
            //_e1Members.Add(e1Member);
        }

        public void Clear()
        {
            _count = 0;
            //_e1Members.Clear();
        }

        public Int32 Count
        {
            get
            {
                return _count;
                //return _e1Members.Count;
            }
        }

        #region IEnumerable<E1Member> Members

        public IEnumerator<E1Member> GetEnumerator()
        {
            BinaryReader br = new BinaryReader(_ms);
            _ms.Seek(0, SeekOrigin.Begin);
            br.ReadInt32(); //count

            for (Int32 i = 0; i < _count; i++)
            {
                E1Member e1Member = new E1Member();
                e1Member.FromBytes(br);
                yield return e1Member;
            }
            //return _e1Members.GetEnumerator();
        }

        #endregion


        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
            //return _e1Members.GetEnumerator();
        }

        #endregion

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("bytes", _ms.ToArray());
        }

        #endregion
    }
}
