using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Member.ValueObjects;

namespace Matchnet.e1.ValueObjects
{
    [Serializable]
    public class E1Update
    {
        private E1Member _e1Member;
        private SearchUpdate _searchUpdate;

        public E1Update(E1Member e1Member, SearchUpdate searchUpdate)
        {
            _e1Member = e1Member;
            _searchUpdate = searchUpdate;
        }

        public E1Member E1Member
        {
            get
            {
                return _e1Member;
            }
            set
            {
                _e1Member = value;
            }
        }

        public SearchUpdate SearchUpdate
        {
            get
            {
                return _searchUpdate;
            }
            set
            {
                _searchUpdate = value;
            }
        }
    }
}
