using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.e1.BusinessLogic;
using Matchnet.e1.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.Interfaces;

namespace Matchnet.e1.ServiceManagers
{
    public class EngineSM : MarshalByRefObject, IServiceManager, IEngineService, IDisposable
    {
        private const string COMMAND_PREFIX = "/command/";

        private const string CATEGORY_NAME = Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME;
        private const string UPDATE_CATEGORY_NAME = CATEGORY_NAME + " Updates";

        private PerformanceCounter _membersTotal;
        private PerformanceCounter _searchPermitsTotal;
        private PerformanceCounter _queryRate;
        private PerformanceCounter _queriesTotal;
        private PerformanceCounter _scanRate;
        private PerformanceCounter _scansTotal;
        private PerformanceCounter _averageScanned;
        private PerformanceCounter _averageScannedBase;
        private PerformanceCounter _averageReturned;
        private PerformanceCounter _averageReturnedBase;
        private PerformanceCounter _averageQueryTimeQueryMarshal;
        private PerformanceCounter _averageQueryTimeQueryMarshalBase;
        private PerformanceCounter _averageQueryTimeEngine;
        private PerformanceCounter _averageQueryTimeEngineBase;
        private PerformanceCounter _averageQueryTimeResultMarshal;
        private PerformanceCounter _averageQueryTimeResultMarshalBase;
        private PerformanceCounter _averageQueryTimeTotal;
        private PerformanceCounter _averageQueryTimeTotalBase;
        private Dictionary<SearchUpdateType, PerformanceCounter> _memberUpdateRates;
        private Dictionary<SearchUpdateType, PerformanceCounter> _loadQueueCounts;
        private PerformanceCounter _memberUpdateRateTotal;
        private PerformanceCounter _loadQueueCountTotal;

        private HttpListener _httpListener;
        private bool _isEnabled;

        public delegate void HealthCheckChangedEventHandler();
        public event HealthCheckChangedEventHandler HealthCheckChanged;

        public EngineSM()
        {
            initPerfCounters();
            initializeHttpListener();
        }

        private void initializeHttpListener()
        {
            _httpListener = new HttpListener();
            _httpListener.Prefixes.Add("http://localhost:6969/");
            _httpListener.Prefixes.Add("http://" + Environment.MachineName + ":6969/");
            _httpListener.Start();
            _httpListener.BeginGetContext(new AsyncCallback(handleHttpRequest), _httpListener);
        }

        private void handleHttpRequest(IAsyncResult result)
        {
            System.IO.Stream outputStream = null;

            try
            {
                if (!_httpListener.IsListening)
                {
                    return;
                }

                HttpListenerContext context = _httpListener.EndGetContext(result);

                _httpListener.BeginGetContext(new AsyncCallback(handleHttpRequest), _httpListener);

                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;
                response.Headers.Add(HttpResponseHeader.CacheControl, "no-cache");

                outputStream = response.OutputStream;
                string responseString;
                string url = request.RawUrl;

                bool isCommand = url.StartsWith(COMMAND_PREFIX);

                if (isCommand)
                {
                    string command = url.Substring(COMMAND_PREFIX.Length);

                    try
                    {
                        responseString = EvaluateCommand(command).Xml;
                    }
                    catch (Exception ex)
                    {
                        responseString = "An exception was thrown when evaluating command: " + command + Environment.NewLine + ex.ToString();
                        EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, responseString, EventLogEntryType.Error);
                    }
                }
                else
                {
                    System.IO.StreamReader sr = null;

                    try
                    {
                        sr = new System.IO.StreamReader(WebPath + url);
                        responseString = sr.ReadToEnd();
                    }
                    catch (Exception ex)
                    {
                        responseString = "Error loading xsl file: " + ex.ToString();
                        EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, responseString, EventLogEntryType.Error);
                    }
                    finally
                    {
                        if (sr != null)
                        {
                            sr.Close();
                        }
                    }
                }

                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                response.ContentLength64 = buffer.Length;
                outputStream.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error handling http request: " + ex.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                if (outputStream != null)
                {
                    outputStream.Close();
                }
            }
        }

        public void PrePopulateCache()
        {
            // no implementation at this time
        }

        #region IEngineService Implementation
        public CommandResult EvaluateCommand(string command)
        {
            try
            {
                return EngineBL.Instance.EvaluateCommand(command);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error evaluating command.", ex);
            }
        }

        public IMatchnetQueryResults RunQuery(IMatchnetQuery query)
        {
            try
            {
                return EngineBL.Instance.RunQuery(query);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error performing search.", ex);
            }
        }

        public ArrayList RunDetailedQuery(IMatchnetQuery query)
        {
            try
            {
                return EngineBL.Instance.RunDetailedQuery(query);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error performing detailed search.", ex);
            }
        }

        public void LoadMembers(E1UpdateCollection e1UpdateCollection)
        {
            try
            {
                EngineBL.Instance.LoadMembers(e1UpdateCollection);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error loading member.", ex);
            }
        }

        public EngineStatus GetEngineStatus()
        {
            try
            {
                return EngineBL.Instance.GetEngineStatus();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error retrieving engine status.", ex);
            }
        }

        public void ChangeMaintenance(bool enabled)
        {
            try
            {
                EngineBL.Instance.ChangeMaintenance(enabled);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error changing maintenance status.", ex);
            }
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (_httpListener != null)
            {
                _httpListener.Abort();
            }
        }
        #endregion

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                HealthCheckChanged();
            }
        }

        public void DisconnectFromLoader()
        {
            EngineBL.Instance.DisconnectFromLoader();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }


        #region Configuration
        private String WebPath
        {
            get
            {
                try
                {
                    return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENGINESVC_WEB_PATH");
                }
                catch
                {
                    return @"C:\matchnet\data\e1\web";
                }
            }
        }
        #endregion

        #region Instrumentation

        private void initPerfCounters()
        {
            //Single-instance counters
            _membersTotal = new PerformanceCounter(CATEGORY_NAME, "Members in Memory", false);
            _membersTotal.RawValue = 0;

            _searchPermitsTotal = new PerformanceCounter(CATEGORY_NAME, "Available Search Permits", false);
            _searchPermitsTotal.RawValue = 0;

            _queryRate = new PerformanceCounter(CATEGORY_NAME, "Queries/sec", false);
            _queryRate.RawValue = 0;

            _queriesTotal = new PerformanceCounter(CATEGORY_NAME, "Total Queries", false);
            _queriesTotal.RawValue = 0;

            _scanRate = new PerformanceCounter(CATEGORY_NAME, "Members Scanned/sec", false);
            _scanRate.RawValue = 0;

            _scansTotal = new PerformanceCounter(CATEGORY_NAME, "Total Members Scanned", false);
            _scansTotal.RawValue = 0;

            _averageScanned = new PerformanceCounter(CATEGORY_NAME, "Average Members Scanned", false);
            _averageScanned.RawValue = 0;

            _averageScannedBase = new PerformanceCounter(CATEGORY_NAME, "Average Members Scanned_base", false);
            _averageScannedBase.RawValue = 0;

            _averageReturned = new PerformanceCounter(CATEGORY_NAME, "Average Members Returned", false);
            _averageReturned.RawValue = 0;

            _averageReturnedBase = new PerformanceCounter(CATEGORY_NAME, "Average Members Returned_base", false);
            _averageReturnedBase.RawValue = 0;

            //Query timing
            _averageQueryTimeQueryMarshal = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Query Marshaling)", false);
            _averageQueryTimeQueryMarshal.RawValue = 0;

            _averageQueryTimeQueryMarshalBase = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Query Marshaling)_base", false);
            _averageQueryTimeQueryMarshalBase.RawValue = 0;

            _averageQueryTimeEngine = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Engine)", false);
            _averageQueryTimeEngine.RawValue = 0;

            _averageQueryTimeEngineBase = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Engine)_base", false);
            _averageQueryTimeEngineBase.RawValue = 0;

            _averageQueryTimeResultMarshal = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Result Marhsaling)", false);
            _averageQueryTimeResultMarshal.RawValue = 0;

            _averageQueryTimeResultMarshalBase = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Result Marhsaling)_base", false);
            _averageQueryTimeResultMarshalBase.RawValue = 0;

            _averageQueryTimeTotal = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Total)", false);
            _averageQueryTimeTotal.RawValue = 0;

            _averageQueryTimeTotalBase = new PerformanceCounter(CATEGORY_NAME, "Average Query Processing Time (Total)_base", false);
            _averageQueryTimeTotalBase.RawValue = 0;

            //Multi-instance counters by update type

            Array updateTypes = Enum.GetValues(typeof(SearchUpdateType));

            _memberUpdateRates = new Dictionary<SearchUpdateType, PerformanceCounter>(updateTypes.Length);
            _loadQueueCounts = new Dictionary<SearchUpdateType, PerformanceCounter>(updateTypes.Length);

            foreach (SearchUpdateType updateType in updateTypes)
            {
                string instanceName = updateType.ToString();

                _memberUpdateRates.Add(updateType, new PerformanceCounter(UPDATE_CATEGORY_NAME, "Member Updates/sec", instanceName, false));
                _loadQueueCounts.Add(updateType, new PerformanceCounter(UPDATE_CATEGORY_NAME, "Loader Queue Count", instanceName, false));
            }

            foreach (PerformanceCounter pc in _memberUpdateRates.Values)
            {
                pc.RawValue = 0;
            }

            foreach (PerformanceCounter pc in _loadQueueCounts.Values)
            {
                pc.RawValue = 0;
            }

            _memberUpdateRateTotal = new PerformanceCounter(UPDATE_CATEGORY_NAME, "Member Updates/sec", "_total", false);
            _memberUpdateRateTotal.RawValue = 0;

            _loadQueueCountTotal = new PerformanceCounter(UPDATE_CATEGORY_NAME, "Loader Queue Count", "_total", false);
            _loadQueueCountTotal.RawValue = 0;

            EngineBL.Instance.UpdateQueueChanged += new EngineBL.UpdateQueueChangedEventHandler(EngineBL_UpdateQueueChanged);
            EngineBL.Instance.MemberUpdated += new EngineBL.MemberUpdatedEventHandler(EngineBL_MemberUpdated);
            EngineBL.Instance.Queried += new EngineBL.QueriedEventHandler(EngineBL_Queried);
            EngineBL.Instance.Maintenance += new EngineBL.MaintenanceEventHandler(EngineBL_Maintenance);
        }

        void EngineBL_UpdateQueueChanged(Int32 delta, SearchUpdateType updateType)
        {
            _loadQueueCounts[updateType].IncrementBy(delta);
            _loadQueueCountTotal.IncrementBy(delta);
        }

        void EngineBL_MemberUpdated(Int32 count, SearchUpdateType updateType)
        {
            _memberUpdateRates[updateType].IncrementBy(count);
            _memberUpdateRateTotal.IncrementBy(count);
        }

        void EngineBL_Queried(Int32 numScanned, Int32 numReturned, Int64 ticksElapsedQueryMarshal, Int64 ticksElapsedEngine, Int64 ticksElapsedResultMarshal, Int64 ticksElapsedTotal)
        {
            _queryRate.Increment();
            _queriesTotal.Increment();

            _scanRate.IncrementBy(numScanned);
            _scansTotal.IncrementBy(numScanned);
            
            _averageScanned.IncrementBy(numScanned);
            _averageScannedBase.Increment();

            _averageReturned.IncrementBy(numReturned);
            _averageReturnedBase.Increment();

            _averageQueryTimeQueryMarshal.IncrementBy(ticksElapsedQueryMarshal);
            _averageQueryTimeQueryMarshalBase.Increment();

            _averageQueryTimeEngine.IncrementBy(ticksElapsedEngine);
            _averageQueryTimeEngineBase.Increment();

            _averageQueryTimeResultMarshal.IncrementBy(ticksElapsedResultMarshal);
            _averageQueryTimeResultMarshalBase.Increment();

            _averageQueryTimeTotal.IncrementBy(ticksElapsedTotal);
            _averageQueryTimeTotalBase.Increment();
        }

        void EngineBL_Maintenance(Int32 numMembers, Int32 numSearchPermits)
        {
            _membersTotal.RawValue = numMembers;
            _searchPermitsTotal.RawValue = numSearchPermits;
        }

        public static void PerfCounterInstall()
        {
            if (PerformanceCounterCategory.Exists(CATEGORY_NAME))
            {
                PerformanceCounterCategory.Delete(CATEGORY_NAME);
            }

            if (PerformanceCounterCategory.Exists(UPDATE_CATEGORY_NAME))
            {
                PerformanceCounterCategory.Delete(UPDATE_CATEGORY_NAME);
            }
            
            CounterCreationDataCollection CCDC = new CounterCreationDataCollection();

            CCDC.Add(new CounterCreationData("Members in Memory", "Total number of members in memory", PerformanceCounterType.NumberOfItems32));

            CCDC.Add(new CounterCreationData("Available Search Permits", "Number of search permits remaining available.", PerformanceCounterType.NumberOfItems32));

            CCDC.Add(new CounterCreationData("Queries/sec", "Rate of queries/sec", PerformanceCounterType.RateOfCountsPerSecond32));
            CCDC.Add(new CounterCreationData("Total Queries", "Total number of queries run since engine started.", PerformanceCounterType.NumberOfItems32));

            CCDC.Add(new CounterCreationData("Members Scanned/sec", "Rate of queries/sec", PerformanceCounterType.RateOfCountsPerSecond32));
            CCDC.Add(new CounterCreationData("Total Members Scanned", "Total number of members scanned since engine started.", PerformanceCounterType.NumberOfItems32));

            CCDC.Add(new CounterCreationData("Average Members Scanned", "Average number of members scanned per query.", PerformanceCounterType.AverageCount64));
            CCDC.Add(new CounterCreationData("Average Members Scanned_base", "Average number of members scanned per query._base", PerformanceCounterType.AverageBase));

            CCDC.Add(new CounterCreationData("Average Members Returned", "Average number of members returned per query.", PerformanceCounterType.AverageCount64));
            CCDC.Add(new CounterCreationData("Average Members Returned_base", "Average number of members returned per query._base", PerformanceCounterType.AverageBase));

            CCDC.Add(new CounterCreationData("Average Query Processing Time (Query Marshaling)", "Average time to marshal an IMatchnetQuery into an e1 Query.", PerformanceCounterType.AverageTimer32));
            CCDC.Add(new CounterCreationData("Average Query Processing Time (Query Marshaling)_base", "Average time to marshal an IMatchnetQuery into an e1 Query._base", PerformanceCounterType.AverageBase));

            CCDC.Add(new CounterCreationData("Average Query Processing Time (Engine)", "Average time spent on a query by the engine.", PerformanceCounterType.AverageTimer32));
            CCDC.Add(new CounterCreationData("Average Query Processing Time (Engine)_base", "Average time spent on a query by the engine._base", PerformanceCounterType.AverageBase));

            CCDC.Add(new CounterCreationData("Average Query Processing Time (Result Marhsaling)", "Average time to marshal e1 results into managed results structure.", PerformanceCounterType.AverageTimer32));
            CCDC.Add(new CounterCreationData("Average Query Processing Time (Result Marhsaling)_base", "Average time to marshal e1 results into managed results structure._base", PerformanceCounterType.AverageBase));

            CCDC.Add(new CounterCreationData("Average Query Processing Time (Total)", "Average time to process a query.", PerformanceCounterType.AverageTimer32));
            CCDC.Add(new CounterCreationData("Average Query Processing Time (Total)_base", "Average time to process a query._base", PerformanceCounterType.AverageBase));

            PerformanceCounterCategory.Create(CATEGORY_NAME, string.Empty, PerformanceCounterCategoryType.SingleInstance, CCDC);

            CCDC.Clear();
            CCDC.Add(new CounterCreationData("Member Updates/sec", "Rate of updates/sec", PerformanceCounterType.RateOfCountsPerSecond32));
            CCDC.Add(new CounterCreationData("Update Queue Count", "Number of updates waiting to be sent to e1.", PerformanceCounterType.NumberOfItems32));

            PerformanceCounterCategory.Create(UPDATE_CATEGORY_NAME, "Counters broken down by update type.", PerformanceCounterCategoryType.MultiInstance, CCDC);
        }

        public static void PerfCounterUninstall()
        {
            if (PerformanceCounterCategory.Exists(CATEGORY_NAME))
            {
                PerformanceCounterCategory.Delete(CATEGORY_NAME);
            }

            if (PerformanceCounterCategory.Exists(UPDATE_CATEGORY_NAME))
            {
                PerformanceCounterCategory.Delete(UPDATE_CATEGORY_NAME);
            }
        }

        #endregion
    }
}
