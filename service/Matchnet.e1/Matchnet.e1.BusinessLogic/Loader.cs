using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Matchnet.e1.Engine.ManagedAdapter;

namespace Matchnet.e1.BusinessLogic
{
    /// <summary>
    /// Loads members into e1 using a single thread.
    /// </summary>
    class Loader
    {
        private Thread _loaderThread;
        private bool _isRunning = false;
        private Queue<MemberSpec> _loaderQueue = new Queue<MemberSpec>();

        public static Loader Instance = new Loader();

        private Loader()
        {
        }

        public void Start()
        {
            if (_loaderThread == null)
            {
                _loaderThread = new Thread(new ThreadStart(workCycle));
                _loaderThread.Start();
            }
            _isRunning = true;
        }

        public void Stop()
        {
            _isRunning = false;
        }

        /// <summary>
        /// Enqueue a MemberSpec to be loaded into e1.
        /// </summary>
        /// <param name="memberSpec"></param>
        public void Enqueue(MemberSpec memberSpec)
        {
            _loaderQueue.Enqueue(memberSpec);
        }

        private void workCycle()
        {
            while (_isRunning)
            {
                if (_loaderQueue.Count != 0)
                {
                    MemberSpec memberSpec = _loaderQueue.Dequeue();
                    memberSpec.Load();
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
