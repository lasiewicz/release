/*using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;

using Matchnet.Data;
using Matchnet.e1.ValueObjects;

namespace Matchnet.e1.BusinessLogic
{
    class BulkLoaderDB : BulkLoaderBase
    {
        private const string LOGICALDB_SEARCHSTORE = "mnSearchStore";
        private const Int32 PARTITION_COUNT = 101;
        private const Int32 THREAD_COUNT = 4;

        public BulkLoaderDB(E1MemberQueue e1MemberQueue) : base(e1MemberQueue)
        {
        }

        protected override void bulkLoad()
        {
            for (Int32 t = 0; t < THREAD_COUNT; t++)
            {
                Thread thread = new Thread(new ParameterizedThreadStart(bulkLoadThread));
                thread.Start(t);
            }
        }

        private void bulkLoadThread(object threadIndex)
        {
            Int32 i = Convert.ToInt32(threadIndex);
            System.Diagnostics.Trace.WriteLine("bulkLoadThread " + i);

            while (i < PARTITION_COUNT)
            {
                bulkLoadTable(i);
                i += THREAD_COUNT;
            }
        }

        private void bulkLoadTable(Int32 partition)
        {
            System.Diagnostics.Trace.WriteLine("bulkLoadTable " + partition);
            //Command command = new Command(LOGICALDB_SEARCHSTORE, "up_SearchStore_Get_ByPartition", 0);
            //command.AddParameter("@Partition", SqlDbType.Int, ParameterDirection.Input, partition);
            SqlConnection connection = new SqlConnection("Server=sql50;Database=mnSearchStore;Integrated Security=SSPI;");
            connection.Open();
            SqlCommand command = new SqlCommand("up_SearchStore_Get_ByPartition", connection);
            command.Parameters.Add("@Partition", SqlDbType.Int);
            command.Parameters["@Partition"].Value = partition;
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader(); //Client.Instance.ExecuteReader(command);

            Array columns = Enum.GetValues(typeof(SearchMemberColumn));
            Dictionary<SearchMemberColumn, Int32> ordinals = new Dictionary<SearchMemberColumn, int>(columns.Length);

            foreach (SearchMemberColumn column in columns)
            {
                ordinals[column] = reader.GetOrdinal(column.ToString());
            }

            while (reader.Read())
            {
                E1Member e1Member = getE1MemberFromDataRow(reader, ordinals);
                onMemberLoaded(1);
                _e1MemberQueue.WaitEnqueue(e1Member);
            }
            connection.Close();
        }

        private static E1Member getE1MemberFromDataRow(SqlDataReader reader, Dictionary<SearchMemberColumn, Int32> ordinals)
        {
            
            E1Member e1Member = new E1Member();

            e1Member.MemberID = reader.GetInt32(ordinals[SearchMemberColumn.MemberID]);
            e1Member.CommunityID = reader.GetByte(ordinals[SearchMemberColumn.CommunityID]);

            if (!reader.IsDBNull(ordinals[SearchMemberColumn.GenderMask]))
            {
                e1Member.GenderMask = reader.GetInt16(ordinals[SearchMemberColumn.GenderMask]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.BirthDate]))
            {
                e1Member.BirthDate = reader.GetDateTime(ordinals[SearchMemberColumn.BirthDate]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.CommunityInsertDate]))
            {
                e1Member.RegisterDate = reader.GetDateTime(ordinals[SearchMemberColumn.CommunityInsertDate]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.LastActiveDate]))
            {
                e1Member.ActiveDate = reader.GetDateTime(ordinals[SearchMemberColumn.LastActiveDate]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.HasPhotoFlag]))
            {
                e1Member.HasPhoto = reader.GetBoolean(ordinals[SearchMemberColumn.HasPhotoFlag]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.Height]))
            {
                e1Member.Height = reader.GetInt32(ordinals[SearchMemberColumn.Height]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.Latitude]))
            {
                e1Member.Latitude = (float) reader.GetDecimal(ordinals[SearchMemberColumn.Latitude]);
            }
            if (!reader.IsDBNull(ordinals[SearchMemberColumn.Longitude]))
            {
                e1Member.Longitude = (float) reader.GetDecimal(ordinals[SearchMemberColumn.Longitude]);
            }
            //if (!reader.IsDBNull(ordinals[SearchMemberColumn.EmailCount]))
            //{
            //    e1Member.Popularity = (Byte)reader.GetInt32(ordinals[SearchMemberColumn.EmailCount]);
            //}
            //TODO: These columns need to be added to SearchMember:
            //memberSpec.Weight
            //memberSpec.Activity
            //memberSpec.ChildrenCount
            //memberSpec.IsOnline
            //memberSpec.MemberLocation
            //memberSpec.RelocateFlag

            foreach (SearchMemberColumn column in Tags)
            {
                if (!reader.IsDBNull(ordinals[column]))
                {
                    e1Member.Tags.Add(column.ToString(), reader.GetValue(ordinals[column]).ToString());
                }
            }

            return e1Member;
        }
    }
}
*/