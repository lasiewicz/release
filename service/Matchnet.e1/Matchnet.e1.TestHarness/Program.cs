using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.e1.BusinessLogic;
using Matchnet.e1.ServiceAdapters;
using Matchnet.e1.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;

namespace Matchnet.e1.TestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            /*while (true)
            {
                string command = Console.ReadLine();

                CommandResult result = EngineSA.Instance.EvaluateCommand(command);

                Console.WriteLine(result.Xml);

                if (result.ErrorCode < 0)
                {
                    break;
                }
            }*/
            
            MatchnetQuery query = new Matchnet.Search.ValueObjects.MatchnetQuery();

            double lbs_100 = 45359.237038037829803270366517422;
            double lbs_200 = 90718.47407607565960654073303484;
            double lbs_126 = 57152.638667927665552120661811949;

            addParameter(query, QuerySearchParam.DomainID, 1);
            addParameter(query, QuerySearchParam.RegionID, 3443818);
            addParameter(query, QuerySearchParam.GeoDistance, 1);
            addParameter(query, QuerySearchParam.GenderMask, 9);
            addParameter(query, QuerySearchParam.AgeMin, 23);
            addParameter(query, QuerySearchParam.AgeMax, 23);
            addParameter(query, QuerySearchParam.WeightMin, lbs_100);
            addParameter(query, QuerySearchParam.WeightMax, lbs_200);
            addParameter(query, QuerySearchParam.ChildrenCount, "-1");
            
            query.SearchType = SearchType.WebSearch;

            MatchnetQueryResults results = (MatchnetQueryResults) EngineSA.Instance.RunQuery(query);

            /*E1UpdateQueue queue = new E1UpdateQueue(10000, "Matchnet.e1.TestHarness");
            queue.MemberDequeued += new E1UpdateQueue.MemberDeQueuedEventHandler(queue_MemberDequeued);
            queue.Start();
            
            for (Int32 i = 0; i < 1000; i++)
            {
                queue.Enqueue(new E1Update(null, new SearchUpdate(988272102, 1, true)));
                Console.WriteLine(queue.Count);
            }*/

            Console.WriteLine("Results: " + results.MatchesFound);

            foreach (MatchnetResultItem item in results.Items.List)
            {
                Int32 memberID = item.MemberID;
                if (memberID == 988272102 || memberID == 100064770) Console.Write("--->");
                Console.WriteLine(memberID);
            }

            Console.ReadLine();
        }

        private static void addParameter(MatchnetQuery query, QuerySearchParam param, object value)
        {
            MatchnetQueryParameter parameter = new MatchnetQueryParameter(param, value);
            query.AddParameter(parameter);
        }

        static void queue_MemberDequeued(E1UpdateQueueEventArgs args)
        {
            EngineSA.Instance.LoadMember(args.E1Update);
        }
    }
}
