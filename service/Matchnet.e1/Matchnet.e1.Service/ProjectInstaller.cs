using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

using Matchnet.e1.ValueObjects;
using Matchnet.e1.ServiceManagers;

namespace Matchnet.e1.Service
{
    /// <summary>
    /// Summary description for ProjectInstaller.
    /// </summary>
    [RunInstaller(true)]
    public class ProjectInstaller : System.Configuration.Install.Installer
    {
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public ProjectInstaller()
        {
            // This call is required by the Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;
            // 
            // serviceInstaller1
            // 
            this.serviceInstaller1.ServiceName = ServiceConstants.SERVICE_NAME;
            this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.serviceInstaller1.ServicesDependedOn = new string[] { "MSMQ" };
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller1,
																					  this.serviceInstaller1});

            this.AfterInstall += new InstallEventHandler(ProjectInstaller_AfterInstall);
            this.BeforeUninstall += new InstallEventHandler(ProjectInstaller_BeforeUninstall);
        }

        void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            EngineSM.PerfCounterUninstall();
        }

        void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            EngineSM.PerfCounterInstall();
            try
            {
                System.IO.Directory.CreateDirectory(@"c:\Matchnet\Logs\e1");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }
        }
        #endregion
    }
}
