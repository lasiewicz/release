using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.e1.ServiceManagers;
using Matchnet.e1.ValueObjects;

namespace Matchnet.e1.Service
{
    public partial class EngineService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private EngineSM _engineSM;
        private System.ComponentModel.Container components;

        public EngineService()
        {
            InitializeComponent();
            initServiceName();
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        private void initServiceName()
        {
            this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _engineSM = new EngineSM();
                base.RegisterServiceManager(_engineSM);
                _engineSM.HealthCheckChanged += new EngineSM.HealthCheckChangedEventHandler(_engineSM_HealthCheckChanged);
   			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
        }

        void _engineSM_HealthCheckChanged()
        {
            base.IsEnabled = _engineSM.IsEnabled;
        }

        protected override void Dispose(bool disposing)
        {
            if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			if (_engineSM != null) 
			{
				_engineSM.Dispose();
			}

            base.Dispose(disposing);
        }

        protected override void OnStop()
        {
            base.OnStop();

            _engineSM.DisconnectFromLoader();
        }
    }
}