#include "stdafx.h"
#include "GSVStatistics.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

GSVStatistics::GSVStatistics()
{
	_stats = new GsvStatsRec();
}

GSVStatistics::~GSVStatistics()
{
	delete _stats;
}

Int32 GSVStatistics::NumMembers::get()
{
	return _stats->mems;
}

Int32 GSVStatistics::SearchPermits::get()
{
	return _stats->searchPermits;
}

GsvStats GSVStatistics::NativeStats::get()
{
	return _stats;
}

String^ GSVStatistics::GetDebugTrace()
{
	return _stats->debugCount.ToString();
}