#include "stdafx.h"
#include "Search.h"
#include "string.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

Result^ Search::DoSearch(Query ^query)
{
	//Query checking
	S1Query nativeQuery = query->NativeQuery;

	//StringConverter::CheckString(nativeQuery->location.regAreaCode, "regAreaCode");
	StringConverter::CheckString(nativeQuery->location.regCity, "regCity");
	StringConverter::CheckString(nativeQuery->location.regCountry, "regCountry");
	StringConverter::CheckString(nativeQuery->location.regState, "regState");
	StringConverter::CheckString(nativeQuery->location.regZip, "regZip");
	//StringConverter::CheckString(nativeQuery->rankTuneName, "rankTuneName");

	if (!nativeQuery->tags)
	{
		System::Diagnostics::Trace::WriteLine("tags was null");
	}

	int i = 0;

	while (nativeQuery->tags[i].name || nativeQuery->tags[i].value)
	{
		StringConverter::CheckString(nativeQuery->tags[i].name, "tag " + i + " name");
		StringConverter::CheckString(nativeQuery->tags[i].value, "tag " + i + " value");
		i++;
	}

	//End query checking

	try
	{
		//Take a search permit
		if (PermitSearchTake(&query->NativeQuery->permit))
		{
			throw gcnew Exception("Unable to take search permit.");
		}

		S1Result result = S1Search(query->NativeQuery);

		if (result)
		{
			return gcnew Result(result);
		}

		return nullptr;
	}
	finally
	{
		if (query->NativeQuery->permit)
		{
			// Give up the permit
			PermitSearchGive(query->NativeQuery->permit);
		}
	}
}