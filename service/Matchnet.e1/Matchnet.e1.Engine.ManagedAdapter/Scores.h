using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				[Serializable]
				public ref class Scores
				{
				private:
					S1Scores _s1Scores;
				public:
					Scores();
					Scores(S1Scores s1Scores);
					~Scores();
					property Byte Match
					{
						Byte get();
						void set(Byte);
					}
					property Byte Age
					{
						Byte get();
						void set(Byte);
					}
					property Byte LDG
					{
						Byte get();
						void set(Byte);
					}
					property Byte Proximity
					{
						Byte get();
						void set(Byte);
					}
					property Byte Activity
					{
						Byte get();
						void set(Byte);
					}
					property Byte Popularity
					{
						Byte get();
						void set(Byte);
					}
					property Byte Height
					{
						Byte get();
						void set(Byte);
					}
					property Byte NewReg
					{
						Byte get();
						void set(Byte);
					}
					property S1Scores NativeScores
					{
						S1Scores get();
					}
				};
			};
		};
	};
};