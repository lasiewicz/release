// This is the main DLL file.
#include "stdafx.h"
#include "XML.h"
#include "string.h"

using namespace Matchnet::e1::Engine;
using namespace Matchnet::e1::Engine::ManagedAdapter;

XML::XML()
{
	_e1Xml = new E1Xml();
}

XML::XML(S4 dataMax)
{
	_e1Xml = new E1Xml(dataMax);
}

XML::~XML()
{
	delete _e1Xml;
}

Int32 XML::CommandEval(System::String ^command)
{
	if (command->StartsWith("@"))
	{
		return DoCommandFile(command->Substring(1));
	}

	StringWrapper^ sw = nullptr;

	try
	{
		sw = gcnew StringWrapper(command);
		
		return _e1Xml->cmd(sw->NativeSTR);
	}
	finally
	{
		delete sw;
	}
}

Int32 XML::DoCommandFile(System::String ^filename)
{
    Int32 result = 0;

	if (!System::IO::File::Exists(filename))
    {
		System::Diagnostics::Trace::WriteLine("File not found: " + filename);
        return -1;
    }
    
	System::Diagnostics::Trace::WriteLine("## Processing command file: " + filename);

	System::IO::StreamReader^ sr = gcnew System::IO::StreamReader(filename);
    while (!sr->EndOfStream && result >= 0)
    {
        result = CommandEval(sr->ReadLine());
    }
    return result;
}

String^ XML::CommandResult::get()
{
	return StringConverter::STRToString(_e1Xml->dataBuf());
}

void XML::Reset()
{
	_e1Xml->reset();
}

E1Xml* XML::NativeE1Xml::get()
{
	return _e1Xml;
}

GSV::GSV()
{
	e1Gsv = new E1Gsv("");
}