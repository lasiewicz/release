#include "stdafx.h"
#include "Query.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;
using namespace System;
using namespace System::Collections;
using namespace System::Runtime::InteropServices;

Query::Query()
{
	_query = new S1QueryRec();
	_tagCount = 0;
	_tags = gcnew ArrayList();
	_areaCodeCount = 0;
	_areaCodes = gcnew ArrayList();
}

Query::~Query()
{
	if (_searchLocation)
	{
		delete _searchLocation;
	}

	delete _query;

	for(int i = 0; i < _tags->Count; i++)
	{
		delete _tags[i];
	}

	delete _tags;
	delete _areaCodes;
}

QuerySorting Query::SortKey::get()
{
	return (QuerySorting) _query->sortKey;
}
void Query::SortKey::set(QuerySorting value)
{
	_query->sortKey = (S1SortKey) value;
}

Int32 Query::MaxResults::get()
{
	return _query->resMax;
}
void Query::MaxResults::set(Int32 value)
{
	_query->resMax = value;
}

Int32 Query::CommunityID::get()
{
	return _query->domId;
}
void Query::CommunityID::set(Int32 value)
{
	_query->domId = value;
}

Int32 Query::GenderMask::get()
{
	return _query->genderMask;
}
void Query::GenderMask::set(Int32 value)
{
	_query->genderMask = value;
}

Location^ Query::SearchLocation::get()
{
	return _searchLocation;
}
void Query::SearchLocation::set(Location^ location)
{
	_searchLocation = location;
	_query->location = *_searchLocation->NativeLoc;
}

Int16 Query::Radius::get()
{
	return _query->distance;
}
void Query::Radius::set(Int16 value)
{
	_query->distance = value;
}

QueryFlag Query::HasPhoto::get()
{
	return (QueryFlag) _query->hasPhoto;
}
void Query::HasPhoto::set(QueryFlag value)
{
	_query->hasPhoto = (S1Flag) value;
}

QueryFlag Query::IsOnline::get()
{
	return (QueryFlag) _query->isOnline;
}
void Query::IsOnline::set(QueryFlag value)
{
	_query->isOnline = (S1Flag) value;
}

Int16 Query::AgeMinDesired::get()
{
	return _query->ageMinDesired;
}
void Query::AgeMinDesired::set(Int16 value)
{
	_query->ageMinDesired = value;
}

Int16 Query::AgeMaxDesired::get()
{
	return _query->ageMaxDesired;
}
void Query::AgeMaxDesired::set(Int16 value)
{
	_query->ageMaxDesired = value;
}

Int16 Query::HeightMinDesired::get()
{
	return _query->hgtMinDesired;
}
void Query::HeightMinDesired::set(Int16 value)
{
	_query->hgtMinDesired = value;
}

Int16 Query::HeightMaxDesired::get()
{
	return _query->hgtMaxDesired;
}
void Query::HeightMaxDesired::set(Int16 value)
{
	_query->hgtMaxDesired = value;
}

Int32 Query::WeightMinDesired::get()
{
	return _query->wgtMinDesired;
}
void Query::WeightMinDesired::set(Int32 value)
{
	_query->wgtMinDesired = value;
}

Int32 Query::WeightMaxDesired::get()
{
	return _query->wgtMaxDesired;
}
void Query::WeightMaxDesired::set(Int32 value)
{
	_query->wgtMaxDesired = value;
}

Scores^ Query::Blend::get()
{
	return _scores;
}

void Query::Blend::set(Scores^ value)
{
	_scores = value;
	_query->rankTuneBlend = *(_scores->NativeScores);
}

void Query::SchoolID::set(Int16 value)
{
	_query->schoolId = value;
}

Int16 Query::SchoolID::get()
{
	return _query->schoolId;
}

void Query::ChildrenCount::set(Int32 value)
{
	_query->childrenCount = value;
}

Int32 Query::ChildrenCount::get()
{
	return _query->childrenCount;
}

void Query::MoreChildrenFlag::set(Int32 value)
{
	_query->moreChildrenFlag = value;
}

Int32 Query::MoreChildrenFlag::get()
{
	return _query->moreChildrenFlag;
}

void Query::ColorCode::set(Int32 value)
{
	_query->colorCode = value;
}

Int32 Query::ColorCode::get()
{
	return _query->colorCode;
}

void Query::AddParameter(String^ name, String^ value)
{
	if (!name)
	{
		System::Diagnostics::Trace::WriteLine("Tag was added to query with null name.");
		return;
	}

	if (!value)
	{
		System::Diagnostics::Trace::WriteLine("Tag was added to query with null value.  Tag name: " + name);
		return;
	}

	if (!(name->Length) || !(value->Length))
	{
		System::Diagnostics::Trace::WriteLine("Parameter name or value was empty.");
		return;
	}

	if (_tagCount > kS1Search_TagMax)
	{
		System::Diagnostics::Trace::WriteLine("Exceeded maximum number of tags in query.  Tag name: " + name);
		return;
	}

	NameValue^ nv = gcnew NameValue(name, value);
	_query->tags[_tagCount] = *(nv->NativeNameValue);
	_tagCount++;
	_tags->Add(nv);
}

void Query::AddAreaCode(String^ areaCode)
{
	areaCode = areaCode->Trim();
	if (areaCode != String::Empty && !_areaCodes->Contains(areaCode))
	{
		_query->areaCodes[_areaCodeCount] = Convert::ToInt16(areaCode);
		_areaCodeCount++;
		_areaCodes->Add(areaCode);
	}
}

S1Query Query::NativeQuery::get()
{
	return _query;
}

System::String^ Query::GetDebugTrace()
{
	/*System::Text::StringBuilder^ sb = gcnew System::Text::StringBuilder();

	for (Int32 i = 0; i < _query->debugCount; i++)
	{
		sb->Append(StringConverter::CSTRToString(_query->debug[i]) + System::Environment::NewLine);
	}

	return sb->ToString();*/
	return _query->debugCount.ToString();
}

System::String^ Query::GetString()
{
	System::Text::StringBuilder^ sb = gcnew System::Text::StringBuilder();
	sb->Append("AgeMax: " + _query->ageMaxDesired + Environment::NewLine);
	sb->Append("AgeMin: " + _query->ageMinDesired + Environment::NewLine);

	int i = 0;
	while (_query->areaCodes[i])
	{
		sb->Append("AreaCode: " + _query->areaCodes[i]);
		i++;
	}

	sb->Append("Distance: " + _query->distance + Environment::NewLine);
	sb->Append("DomID: " + _query->domId + Environment::NewLine);
	sb->Append("GenderMask: " + _query->genderMask + Environment::NewLine);
	sb->Append("HasPhoto: " + Convert::ToString(_query->hasPhoto) + Environment::NewLine);
	sb->Append("HgtMaxDesired: " + _query->hgtMaxDesired + Environment::NewLine);
	sb->Append("HgtMinDesired: " + _query->hgtMinDesired + Environment::NewLine);
	sb->Append("IsOnline: " + Convert::ToString(_query->isOnline) + Environment::NewLine);
	sb->Append("RegAreaCode: " + StringConverter::CSTRToString(_query->location.regAreaCode) + Environment::NewLine);
	sb->Append("RegCity: " + StringConverter::CSTRToString(_query->location.regCity) + Environment::NewLine);
	sb->Append("RegCountry: " + StringConverter::CSTRToString(_query->location.regCountry) + Environment::NewLine);
	sb->Append("RegLat: " + _query->location.regLat + Environment::NewLine);
	sb->Append("RegLng: " + _query->location.regLng + Environment::NewLine);
	sb->Append("RegState: " + StringConverter::CSTRToString(_query->location.regState) + Environment::NewLine);
	sb->Append("RegZip: " + StringConverter::CSTRToString(_query->location.regZip) + Environment::NewLine);
	sb->Append("ResMax: " + _query->resMax + Environment::NewLine);
	sb->Append("SchoolID: " + _query->schoolId + Environment::NewLine);
	sb->Append("SortKey: " + _query->sortKey + Environment::NewLine);

	i = 0;
	while (_query->tags[i].name)
	{
		sb->Append("Tag: (" + StringConverter::CSTRToString(_query->tags[i].name) + ", ");
		sb->Append(StringConverter::CSTRToString(_query->tags[i].value) + ")" + Environment::NewLine);
		i++;
	}

	sb->Append("WgtMaxDesired: " + _query->wgtMaxDesired + Environment::NewLine);
	sb->Append("WgtMinDesired: " + _query->wgtMinDesired + Environment::NewLine);

	return sb->ToString();
}