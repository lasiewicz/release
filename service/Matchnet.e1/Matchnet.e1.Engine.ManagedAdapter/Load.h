#include "XML.h"
#include "GSVStatistics.h"
#include "stdio.h"

using namespace System::Collections;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class Load
				{
				public:
					static void LoadFile(String^ fileName, String^ header, int mode, XML^ xml);
					static void MemberDelete(Int32 memberID, Int32 communityID);
					static GSVStatistics^ Maintain();
					static GSVStatistics^ GetStats();
				};
			};
		};
	};
};