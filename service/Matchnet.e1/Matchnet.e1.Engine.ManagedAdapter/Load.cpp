#include "StdAfx.h"
#include "Load.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

void Load::LoadFile(String^ fileName, String^ header, int mode, XML^ xml)
{
	StringWrapper^ swFileName = gcnew StringWrapper(fileName);
	StringWrapper^ swHeader = gcnew StringWrapper(header);

	FILE* file = NULL;//fopen(swFileName->NativeSTR, "r");
	
	LoadFileAux(swFileName->NativeSTR, file, xml->NativeE1Xml->pxObj, mode, swHeader->NativeSTR);

	//fclose(file);
}

void Load::MemberDelete(Int32 memberID, Int32 communityID)
{
	MemDeleteSpec memDeleteSpec;

	try	
	{
		memDeleteSpec = new MemDeleteSpecRec();

		memDeleteSpec->memId = memberID;
		memDeleteSpec->domId = communityID;

		MemDelete(memDeleteSpec);
	}
	catch (Exception^ ex)
	{
		throw gcnew Exception("Failure deleting member from engine.  MemberID: " + memberID, ex);
	}
	finally
	{
		delete memDeleteSpec;
	}
}

GSVStatistics^ Load::Maintain()
{
	GSVStatistics^ gsvStatistics = nullptr;

	try
	{
		GSVStatistics^ gsvStatistics = gcnew GSVStatistics();

		GsvMaintain(gsvStatistics->NativeStats);

		return gsvStatistics;
	}
	catch (Exception^ ex)
	{
		String^ debug = String::Empty;

		if (gsvStatistics)
		{
			debug = gsvStatistics->GetDebugTrace();
		}

		throw gcnew Exception("Error performing maintenance.  Debug trace: " + debug, ex);
	}
}

GSVStatistics^ Load::GetStats()
{
	GSVStatistics^ gsvStatistics = nullptr;

	try
	{
		GSVStatistics^ gsvStatistics = gcnew GSVStatistics();

		GsvStatsLoad(gsvStatistics->NativeStats);

		return gsvStatistics;
	}
	catch (Exception^ ex)
	{
		String^ debug = String::Empty;

		if (gsvStatistics)
		{
			debug = gsvStatistics->GetDebugTrace();
		}

		throw gcnew Exception("Error performing maintenance.  Debug trace: " + debug, ex);
	}
}