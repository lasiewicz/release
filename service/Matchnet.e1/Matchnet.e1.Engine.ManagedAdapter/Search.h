#include "Query.h"
#include "Result.h"

using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class Search
				{
				public:
					static Result^ DoSearch(Query^ query);
				};
			};
		};
	};
};