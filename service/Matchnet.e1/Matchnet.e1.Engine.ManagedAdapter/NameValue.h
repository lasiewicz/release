using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class NameValue
				{
				private:
					MemNV _memNV;
					StringWrapper^ _name;
					StringWrapper^ _value;
				public:
					NameValue();
					NameValue(MemNV memNV);
					NameValue(String^ name, String^ value);
					~NameValue();

					property String^ Name
					{
						String^ get();
						void set(String^);
					}

					property String^ Value
					{
						String^ get();
						void set(String^);
					}

					property MemNV NativeNameValue
					{
						MemNV get();
					}
				};
			};
		};
	};
};