<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<!-- XSL Transformation for e1 output
 *
 * History:
 *   v1.0 |  12/13/05 | Philip Nelson
 *
-->

<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" >

	<xsl:import href="e1MSFT.xsl"/>

	<xsl:output method="html" indent="yes" encoding="utf-8" />
	<xsl:preserve-space  elements="*"/>

	<!-- Pass through html markup in XML nodes when mode is "content" -->
	<xsl:template match="html:*" mode="content">
		<xsl:element name="{local-name()}">
			<xsl:for-each select="@html:*">
				<xsl:attribute name="{local-name()}">
					<xsl:value-of select="." />
				</xsl:attribute>
			</xsl:for-each>
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>


  <!-- The outer structure of the page -->

<xsl:template match="/">
	<html >
	<head>
		<title>E1 Results</title>
		<link rel="STYLESHEET" type="text/css" href="e1MSFT.css" />
		<script language="javascript" src="e1MSFT.js" > </script>
	</head>
	<body>
    <h1>Contents of XML</h1>
    <xsl:apply-templates select="e1"/>
	</body>
	</html>
</xsl:template>

</xsl:stylesheet>
