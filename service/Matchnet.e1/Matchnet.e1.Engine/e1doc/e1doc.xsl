<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="html" indent="yes" encoding="utf-8"/>
	<xsl:preserve-space elements="*"/>

	<xsl:template match="/">
		<xsl:apply-templates select="e1doc" />
	</xsl:template> 

	<xsl:template match="e1doc">
		<html>
			<head>
				<title>
					<xsl:value-of select="head/title"/>
				</title>
				<script type="text/javascript">
					var tabberNoOnLoad = true;
					function makeTabs() { 
					    var nm = 'tabifyMe';
						var nd = document.getElementById(nm);
						if (!nd) nd = document.all[nm];
						if (nd) nd.style.display = "none";
						tabberAutomatic(); 
					}
				</script>
				<script type="text/javascript" src="tabber.js"></script>
				<link rel="stylesheet" href="e1doc.css" TYPE="text/css" />
				<link rel="stylesheet" href="tabber.css" TYPE="text/css" MEDIA="screen"/>
				<link rel="stylesheet" href="tabber-print.css" TYPE="text/css" MEDIA="print"/>
			</head>
			<body>
			<h1>e1 Engine Documentation</h1>
			<div class="tabber" id="tabouter">
				<xsl:apply-templates select="head"/>
				<xsl:apply-templates select="contents" mode="TOC" />
				<xsl:apply-templates select="contents"/>
			</div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="head">
		<div class="tabbertab" id="docinfo" title="Document Info">
		<a id="tabifyMe" href="javascript:makeTabs()">[Click to Tabify]</a>
		<h2>Document Info</h2>
		<table>
		<tr><td>Title:      </td><td><xsl:value-of select="title"/></td></tr>
		<tr><td>Last Update:</td><td><xsl:value-of select="lastupdate"/></td></tr>
		<tr><td>Company:    </td><td><xsl:value-of select="company"/></td></tr>
		<tr><td>Overview:   </td><td><xsl:value-of select="overview"/></td></tr>
		<tr><td>History:    </td><td>
			<table>
			<tr>
				<th>Version</th>
				<th>Date</th>
				<th>Author</th>
				<th>Description</th>
			</tr>
			<xsl:for-each select="history/rev">
				<tr>
					<td><xsl:value-of select="@id"/></td>
					<td><xsl:value-of select="@date"/></td>
					<td><xsl:value-of select="@author"/></td>
					<td><xsl:value-of select="."/></td>
				</tr>
			</xsl:for-each>
			</table>
			</td>
		</tr>
		</table>
		</div>
	</xsl:template>

	<xsl:template match="contents" mode="TOC">
		<div class="tabbertab" id="TOC" title="TOC">
		<h2>Table of Contents</h2>
		<ol style="list-style-type: none;">
			<xsl:for-each select="chapter"> 
				<xsl:variable name="chapter_num" select="1+ count(preceding::chapter)"/>
				<li>[<xsl:value-of select="$chapter_num"/>] 
				    <a href="#{@id}"><xsl:value-of select="@title"/></a>
					<ol style="list-style-type: none;">
					<!-- Maybe have sections -->
					<xsl:for-each select="section">
						<xsl:variable name="section_num" select="position()"/>
						<li>[<xsl:value-of select="$chapter_num"/>.<xsl:value-of select="$section_num"/>] 
						    <a href="#{@id}"><xsl:value-of select="@title"/></a></li>
					</xsl:for-each>
					<!-- Maybe have commands -->
					<xsl:for-each select="cmds/cmd">
						<xsl:variable name="cmd_num" select="position()"/>
						<li>[<xsl:value-of select="$chapter_num"/>.<xsl:value-of select="$cmd_num"/>] 
						    <a href="#REST{@name}{@type}">
						<xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@type"/>
							</a>
						</li>
					</xsl:for-each>

				</ol>
				</li>
		</xsl:for-each>
		</ol>
		<hr/>
		</div>
	</xsl:template>

	<xsl:template match="contents">
		<xsl:apply-templates select="chapter" />
	</xsl:template>

	<xsl:template match="chapter">
		<xsl:variable name="chapter_num" select="1+ count(preceding::chapter)"/>
		<div class="tabbertab" id="{@id}" title="[{$chapter_num}]  {@title}">
			<a name="{@id}"><h2>[<xsl:value-of select="$chapter_num"/>] <xsl:value-of select="@title"/></h2></a>
			<!-- Sections of chapter <xsl:value-of select="@title"/>: -->
			<!-- Set up tabber for each section -->
			<div class="tabber" id="{@id}sections">
				<xsl:apply-templates select="section">
					<xsl:with-param name="chapter_num" select="$chapter_num" />
				</xsl:apply-templates>
				<xsl:apply-templates select="cmds"/>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="section">
		<xsl:param name="chapter_num" />
		<xsl:variable name="section_num" select="position()"/>
		<div class="tabbertab" id="{@id}" title="[{$chapter_num}.{$section_num}] {@title}">
			<a name="{@id}"><h3>[<xsl:value-of select="$chapter_num"/>.<xsl:value-of select="$section_num"/>] <xsl:value-of select="@title"/></h3></a>
			<xsl:apply-templates select="*"/>
		</div>
	</xsl:template>

	<xsl:template match="obj">
		<i>
			<xsl:value-of select="@id"/>
		</i>
	</xsl:template>

	<xsl:template match="arrow">
		<img border="0" alt=">>" src="rightarrow.gif" style="height:17px;"/>
	</xsl:template>

	<xsl:template match="code">
		<pre>
			<xsl:value-of select="."/>
		</pre>
	</xsl:template>

	<xsl:template match="cmds">
		<xsl:apply-templates select="cmd"/>
	</xsl:template>

	<xsl:template match="cmd">
		<div class="tabbertab" id="REST{@name}{@type}" title="{@name} {@type}">
		<h3>Command: <xsl:value-of select="@name"/> <xsl:value-of select="@type"/></h3>
		Help: <xsl:value-of select="help" /><br/>
		<xsl:if test="arg">
		<table>
		<tr><th>Name</th><th>Type</th><th>Count</th><th>Description</th></tr>
			<xsl:for-each select="arg">
				<tr>
					<td><xsl:value-of select="@name"/></td>
					<td><xsl:value-of select="@type"/></td>
					<td><xsl:value-of select="@upto"/></td>
					<td><xsl:value-of select="."/></td>
				</tr>
			</xsl:for-each>
		</table>
		</xsl:if>
		</div>
	</xsl:template>

	<!-- Pass through other nodes (esp embedded html) w/o the outer namespace -->
	<xsl:template match="*">
		<xsl:element name="{name()}" namespace="{namespace-uri()}">
			<xsl:copy-of select="@*"/>
    		<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<!-- Pass attributes -->
	<xsl:template match="@*">
		<xsl:copy>
		  <xsl:apply-templates select="@*"/>
	   </xsl:copy>
	</xsl:template>

</xsl:stylesheet>
