// mempack.c
// pnelson 12/3/2005
// Pack member data into concise forms

#define MEM_FRIEND
#include "peng.h"

#define PKGNAME PkgName_Mem


/* Gender mask bits
 * 1	Male
 * 2	Female
 * 4	Seeking Male
 * 8	Seeking Female
 * 16	Male turned female
 * 32	Female turned male
 * 64	Seeking male turned female
 * 128	Seeking female turned male
 */
// Return 0/GenderMale/GenderFemale
//
int MemPackGender(int gm, int *sameSexp)
{
  int ima, ssx; 

  if      (gm & 1)  ima = GenderMale;
  else if (gm & 2)  ima = GenderFemale;
  else if (gm & 16) ima = GenderFemale;
  else if (gm & 32) ima = GenderMale;
  else goto error;

  if (sameSexp) {
	if      ((gm&1) && (gm&4)) ssx = 1;
	else if ((gm&2) && (gm&8)) ssx = 1;
	else if (gm & (16|32|64|128)) ssx = 1;
	else ssx = 0;
	*sameSexp = ssx;
  }

  return ima;

 error:
  if (sameSexp) *sameSexp = 0;
  return 0;
}

STR MemPackDateStr(cSTR name, US2 pack, char *buf, int asAttr)
{
  MemDateRec dd;
  dd.pack = pack;
  if (asAttr) {
	sprintf(buf, "%s='%0.4d-%0.2d-%0.2d'", name, 
			(!dd.v.year ? 0 : dd.v.year + kMemPack_YearMin), dd.v.month, dd.v.day);
  } else if (pack == 0) {
	sprintf(buf, "%s code='0' ymd='0000-00-00'", name);
  } else {
	sprintf(buf, "%s code='%d' ymd='%d-%0.2d-%0.2d'", name, pack,
			dd.v.year + kMemPack_YearMin, dd.v.month, dd.v.day);
  }
  return buf;
}

STR MemPackDateTimeStr(cSTR name, US4 pack, char *buf, int asAttr)
{
  MemDateTimeRec dd;
  dd.pack = pack;
  if (asAttr) {
	  sprintf(buf, "%s='%0.4d-%0.2d-%0.2d %0.2d:%0.2d'", name, 
			(!dd.v.year ? 0 : dd.v.year + kMemPack_YearMin), dd.v.month, dd.v.day, dd.v.hour, dd.v.minute);
  } else if (pack == 0) {
	sprintf(buf, "%s code='0' ymd='0000-00-00'", name);
  } else {
	sprintf(buf, "%s code='%u' ymd='%d-%0.2d-%0.2d %0.2d:%0.2d'", name, pack,
			dd.v.year + kMemPack_YearMin, dd.v.month, dd.v.day, dd.v.hour, dd.v.minute);
  }
  return buf;
}

US2 MemPackDate(cSTR dateStr)
{
  int year, month, day;
  US2 pack;
  year = month = day = 0;
  (void)sscanf(dateStr, "%d-%d-%d", &year, &month, &day);
  if (year > kMemPack_YearMin)
	year -= kMemPack_YearMin;
  if (year == 0 && month == 0 && day == 0)
	return 0;
  else {
	year  = minmax(year,  1, kMemPack_YearRng);
	month = minmax(month, 1, 12);
	day   = minmax(day,   1, 31);
	pack = (year << 9) | (month << 5) | day;
	return pack;
  }
}

US4 MemPackDateTime(cSTR dateStr)
{
  US4 year, month, day, hour, minute;
  US4 pack;
  year = month = day = hour = minute = 0;
  (void)sscanf(dateStr, "%d-%d-%d %d:%d", &year, &month, &day, &hour, &minute);
  if (year > kMemPack_YearMin)
	year -= kMemPack_YearMin;
  if (year == 0 && month == 0 && day == 0)
	return 0;
  else {
	year   = minmax(year,   1, kMemPack_YearRng);
	month  = minmax(month,  1, 12);
	day    = minmax(day,    1, 31);
	hour   = minmax(hour,   0, 23);
	minute = minmax(minute, 0, 59);
	pack = (year << 25) | (month << 21) | (day << 16) | (hour << 11) | minute;
	return pack;
  }
}

///Returns a 16-bit packed date from a 32-bit packed date/time
US2 MemPackDateTimeAsDate(US4 pack)
{
	return (pack >> 16);
}

US2 MemPackDateAsDays(US2 pack)
{
  MemDateRec dd;
  dd.pack = pack;
  return (US2)((((S4)dd.v.year)*12 + ((S4)dd.v.month-1)) * 31 + (S4)dd.v.day);
}

US2 MemPackDateTimeAsDays(US4 pack)
{
	return MemPackDateAsDays(MemPackDateTimeAsDate(pack));
}

US4 MemPackDateTimeAsMinutes(US4 pack)
{
  MemDateTimeRec dd;
  dd.pack = pack;
  return (US4)((((((US4)dd.v.year)*12 + ((US4)dd.v.month-1)) * 31 + (US4)dd.v.day) * 24 + (US4)dd.v.hour) * 60 + (US4)dd.v.minute);
}

US2 MemPackMilesAsBoxes(int miles)
{
	// Calculate this distance in boxes
	S4 ew = kLoc_EWBoxes * (S4)miles / 24900;      // Full circum of earth
	S4 ns = kLoc_NSBoxes * (S4)miles / (24900/3);  // Between arctic circles

	// Select the longer distance
	S4 mx = (ew > ns ? ew : ns);
	
	// Add 1 more box band as padding
	return (US2)mx + 1;
}  

void MemPackLoc(Loc loc, MemLoc memLoc, int precise)
{
	float fltLng = memLoc->regLng, pctLng;
	float fltLat = memLoc->regLat, pctLat;

	US2 ewBoxes = kLoc_EWBoxes * (!precise ? 1 : kLoc_EWPrecise);
	US2 nsBoxes = kLoc_NSBoxes * (!precise ? 1 : kLoc_NSPrecise);

	fltLng = minmax(fltLng, kLoc_fLngMin, kLoc_fLngMax);
	fltLat = minmax(fltLat, kLoc_fLatMin, kLoc_fLatMax);

	pctLng = (fltLng - kLoc_fLngMin) / (kLoc_fLngMax - kLoc_fLngMin);
	if      (pctLng <= 0.0) loc->lng = 0;
	else if (pctLng >= 1.0) loc->lng = ewBoxes - 1;
	else    loc->lng = (US2)(pctLng * (float)ewBoxes);

	pctLat = (fltLat - kLoc_fLatMin) / (kLoc_fLatMax - kLoc_fLatMin);
	if      (pctLat <= 0.0) loc->lat = 0;
	else if (pctLat >= 1.0) loc->lat = nsBoxes - 1;
	else    loc->lat = (US2)(pctLat * (float)nsBoxes);
}

void MemPackVitals(RankTune rt, MemVitals vitals, MemSpec xspec)
{
  int sameSex;

  vitals->height   = RankPackHgt(xspec->height);
  vitals->weight   = RankPackWgt(xspec->weight);
  vitals->saPop    = (xspec->saPop ? minmax(xspec->saPop, 1, 15) : RankPopExec(rt, xspec->emailCount, 1)); 
  vitals->saAct    = (xspec->saAct ? minmax(xspec->saAct, 1, 15) : RankActExec(rt, xspec->activePack, 1));  
  vitals->saNew    = (xspec->saNew ? minmax(xspec->saNew, 1, 15) : RankNewExec(rt, xspec->registerPack, 1));
  if (xspec->hasPhoto != E1UpdateFlag_NoChange)
	vitals->hasPhoto = (xspec->hasPhoto == E1UpdateFlag_SetYes ? 1 : 0);
  if (xspec->isOnline != E1UpdateFlag_NoChange)
	vitals->isOnline = (xspec->isOnline == E1UpdateFlag_SetYes ? 1 : 0);
  vitals->childCnt  = (xspec->childrenCount > 3 ? 3 : xspec->childrenCount);

  (void)MemPackGender(xspec->genderMask, &sameSex);
  if (sameSex)
	vitals->sameSex = 1;
}


static S4 csczLookup(cSTR typeName, StxDb db, cSTR name, int bitsMax, int mod) 
{
  char msgBuf[128];
  Stx sx;

  // Lookup or create a numeric Stx for each string, Stx fnc guarantees size in range
  if (!name) 
	return 0;
  else if (atol(name) == 0) {
	snprintf(msgBuf, sizeof(msgBuf), "Region string was empty.");
    PEngError(PKGNAME, typeName, msgBuf);
	return 0;
  }
  else if (!(sx = StxDbFind(db, name))) {
	sx = StxDbFindOrNew(db, name);
	if (sx >= (1 << bitsMax)) {
	  snprintf(msgBuf, sizeof(msgBuf), "TooMany %ld=%s", sx, name);
	  PEngError(PKGNAME, typeName, msgBuf);
	  sx = 0;
	} else if ((sx % mod) == 0) {
	  snprintf(msgBuf, sizeof(msgBuf), "Auto %s: %ld=%s", typeName, sx, name);
	  PEngLog(PKGNAME, msgBuf, NULL, 0);
	}
  }
  return sx;
}

int MemPackCSCZMap(MemCSCZMap map, MemLoc ml)
{
  LocRec locPrec;
  cSTR zip     = ml->regZip;
  cSTR area    = ml->regAreaCode;

  // Lookup or create a numeric Stx for each string, Stx fnc guarantees size in range
  map->pkCountry = (int)csczLookup("Country", gsv->pkCountry, ml->regCountry, kStx_CountryBits, 10);
  map->pkState   = (int)csczLookup("State",   gsv->pkState,   ml->regState,   kStx_StateBits, 50);
  map->pkCity    = (int)csczLookup("City",    gsv->pkCity,    ml->regCity,    kStx_CityBits, 1000);
  map->pkZip     = (int)csczLookup("Zip",     gsv->pkZip,     ml->regZip,     kStx_ZipBits,  1000);

  // All regions should have at least valid Country and City region ids.
  if (!map->pkCountry || !map->pkCity)
  {
	return -1;
  }

  // Store area code as integer, make sure it fits in its bit range
  if (area && area[0]) {
	S4 iarea = atol(area);
	map->pkArea = minmax(iarea, 1, 999);
  }

  // Get the high precision box, then store the extra bits for lat and lng
  MemPackLoc(&locPrec, ml, 1);
  map->latPrecise = locPrec.lat % kLoc_NSPrecise; 
  map->lngPrecise = locPrec.lng % kLoc_EWPrecise; 

  return 0;
}

void MemPackMemToLoc(Mem xmem, Loc outLoc, int precise)
{
  Ldg xldg;
  if (xmem && (xldg = xmem->ldg)) {
	outLoc->lat = xldg->ldgKey.loc.lat;
	outLoc->lng = xldg->ldgKey.loc.lng;
	if (precise) {
	  outLoc->lat = outLoc->lat * kLoc_NSPrecise + xmem->cscz.latPrecise;
	  outLoc->lng = outLoc->lng * kLoc_EWPrecise + xmem->cscz.lngPrecise;
	}
  } else {
	outLoc->lat = 0;
	outLoc->lng = 0;
  }
}

/****************************************************************/

/// <summary>Fill a MemSpec with what we know about that member</summary>
/// <returns>Populates contents of outSpec, using xxBuf for scratch strings</returns>
/// <param name="xmem">The member we're describing</param>
/// <param name="outSpec">The memspec that will contain description</param>
/// <param name="xxBuf">A preallocated scratch buffer to hold string descriptions</param>
/// <param name="xxLen">Length of scratch buffer</param>
///
int MemToMemSpec(Mem xmem, MemSpec outSpec, char *xxBuf, int xxBufLen)
{
  LocRec lp;
  Ldg ldg = xmem->ldg;
  Grp grp = xmem->grp;
  int i, len;
  MemDateRec dd;
  MemDateTimeRec dateTime;

  memset(outSpec, 0, sizeof(*outSpec));
  outSpec->memId = xmem->memId;

  outSpec->domId      = ldg->ldgKey.domId;
  outSpec->genderMask = ldg->ldgKey.gender;

  // Get the precise location
  MemPackMemToLoc(xmem, &lp, 1);
  outSpec->memLoc.regLat = (float)lp.lat;
  outSpec->memLoc.regLng = (float)lp.lng;

  outSpec->memLoc.regCountry = StxDbById(gsv->pkCountry, xmem->cscz.pkCountry);
  outSpec->memLoc.regState   = StxDbById(gsv->pkState,   xmem->cscz.pkState);
  outSpec->memLoc.regCity    = StxDbById(gsv->pkCity,    xmem->cscz.pkCity);
  outSpec->memLoc.regZip     = StxDbById(gsv->pkZip,     xmem->cscz.pkZip);

  if (xxBufLen > 5) {
	snprintf(xxBuf, xxBufLen, "%0.3d", xmem->cscz.pkArea);
	xxBuf[xxBufLen-1] = 0;
	outSpec->memLoc.regAreaCode = xxBuf;
	len = strlen(xxBuf)+1; xxBuf += len; xxBufLen -= len;
  }

  if (xxBufLen > 12) {
	dd.pack = xmem->birthPack.pack;
	snprintf(xxBuf, xxBufLen, "%d-%0.2d-%0.2d", dd.v.year + kMemPack_YearMin, dd.v.month, dd.v.day);
	xxBuf[xxBufLen-1] = 0;
	outSpec->birthDate = xxBuf;
	len = strlen(xxBuf)+1; xxBuf += len; xxBufLen -= len;
  }

  if (xxBufLen > 12) {
	dd.pack = xmem->registerPack.pack;
	snprintf(xxBuf, xxBufLen, "%d-%0.2d-%0.2d", dd.v.year + kMemPack_YearMin, dd.v.month, dd.v.day);
	xxBuf[xxBufLen-1] = 0;
	outSpec->registerDate = xxBuf;
	len = strlen(xxBuf)+1; xxBuf += len; xxBufLen -= len;
  }

  if (xxBufLen > 12) {
	dateTime.pack = xmem->activePack.pack;
	snprintf(xxBuf, xxBufLen, "%d-%0.2d-%0.2d %0.2d:%0.2d", dateTime.v.year + kMemPack_YearMin, dateTime.v.month, dateTime.v.day, dateTime.v.hour, dateTime.v.minute);
	xxBuf[xxBufLen-1] = 0;
	outSpec->activeDate = xxBuf;
	len = strlen(xxBuf)+1; xxBuf += len; xxBufLen -= len;
  }

  outSpec->emailCount = xmem->vitals.saPop;

  if (xmem->vitals.height)
	outSpec->height = (int)xmem->vitals.height + kMemPack_HeightMin;
  if (xmem->vitals.weight)
	outSpec->weight = (int)xmem->vitals.weight + kMemPack_WeightMin;
  outSpec->childrenCount = xmem->vitals.childCnt;
  outSpec->hasPhoto   = xmem->vitals.hasPhoto;
  outSpec->isOnline   = xmem->vitals.isOnline;
  outSpec->saAct = xmem->vitals.saAct;
  outSpec->saNew = xmem->vitals.saNew;
  outSpec->saPop = xmem->vitals.saPop;
  outSpec->colorcode=xmem->colorcode;

  for (i=0; i<grp->tagCount && i<kMemSpec_TagsMax; i++) {
	Tag tag = grp->tagList[i];
	outSpec->tags[i].name  = StxDbById(gsv->pkTags, tag->nxId);
	outSpec->tags[i].value = StxDbById(gsv->pkTags, tag->vxId);
  }
  
  return 0;
}

