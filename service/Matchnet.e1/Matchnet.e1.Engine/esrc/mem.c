// mem.c
// pnelson 11/15/2005
// In Memory rep of members

#include "stdlib.h"
#define MEM_FRIEND
#include "peng.h"
#include "kw.h"

#define PKGNAME PkgName_Mem

/**********************/
/** Hashable methods **/
/**********************/

// Hash index is XOR of member id and domain
//
static PHashIdx xMemKeyToIdx(MemKey mk)
{
  US4 idx = (mk->memId ^ mk->domId) % kMem_HashSize;
  return (PHashIdx)idx;
}

// Compare by memId, then domId 
//
static int xMemKeyCmp(MemKey mk, Mem xmem)
{
  MemId xmemId;
  DomId xdomId;
  
  xmemId = xMemMemId(xmem);
  if (mk->memId != xmemId)
	return (mk->memId < xmemId) ? -1 : 1;
  
  xdomId = xMemDomId(xmem);
  if (mk->domId != xdomId)
	return (mk->domId < xdomId) ? -1 : 1;
  
  return 0;
}

/***************/
/** Utilities **/
/***************/

void MemInitialize()
{
  if (!gsv->mems) {
	gsv->mems = PHashNew(PKGNAME, kMem_HashSize, 
						 (PHashKeyToIdxFnc*)xMemKeyToIdx, 
						 (PHashKeyCmpFnc  *)xMemKeyCmp);
	gsv->memObjMgr = ObjMgrCreate(PKGNAME, sizeof(MemRec), kMem_MaxMems);
	// Build mapping tables that give strings a number
	gsv->pkCountry = StxDbNew(kStx_CountryBits, "Country");
	gsv->pkState   = StxDbNew(kStx_StateBits,   "State");  
	gsv->pkCity    = StxDbNew(kStx_CityBits,    "City");   
	gsv->pkZip     = StxDbNew(kStx_ZipBits,     "Zip");   
	if (!gsv->mems) 
	  PEngError(PKGNAME, NULL, "Init");
  }
}

// Cut a member from all other links
//
static int xMemLinkOut(Mem xmem)
{
  MemKeyRec mk;
  Mem *memRoot, memScan;
  
  if (!xmem) 
	  return -1;
  
  StructNull(mk);
  mk.memId = xMemMemId(xmem);
  mk.domId = xMemDomId(xmem);
  
  // Cut it from the ldg chain
  if (xmem->ldg) {
	xmem->ldg->memCount -= 1;
	for (memRoot = LdgChainMemRoot(xmem->ldg); 
		 (memScan = *memRoot); 
		 memRoot = &(memScan->ldgChain)) {
	  if (memScan == xmem)
		{ *memRoot = xmem->ldgChain; break; }
	}
	if (!memScan)  // Never found it
	   { E1ErrorConsistency("xMemLinkOut: Find in LdgChain"); goto error; }
  }
  
  // Cut it from the parent list of the tag grp
  if (xmem->grp) {
  	GrpCountUpdate(xmem->grp, -1);
	for (memRoot = GrpChainMemRoot(xmem->grp);
		 (memScan = *memRoot); 
		 memRoot = &(memScan->grpChain)) {
	  if (memScan == xmem)
		{ *memRoot = memScan->grpChain; break; }
	}
	if (!memScan) // Never found it
	{ E1ErrorConsistency("xMemLinkOut: Find in GrpChain"); goto error; }
  }
  
  // Pop it from the hash table
  if (!(PHashPop(gsv->mems, (PHashKey)&mk, (PHashObj)xmem)))
  { E1ErrorConsistency("xMemLinkOut: Find in Hash"); goto error; }
  
  return 0;
  
 error:
  return -1;
}
	
// Add a member to all other links
//
static Mem xMemLinkIn(Mem xmem, MemKey mk)
{
  Mem *memRoot;
  
  // Add it to the head of the ldg chain ??? insert in some order ???
  memRoot = LdgChainMemRoot(xmem->ldg);
  xmem->ldgChain = *memRoot;
  *memRoot = xmem;
  xmem->ldg->memCount += 1;
  
  // Add it to parent list of the tag grp
  if (xmem->grp) {
	memRoot = GrpChainMemRoot(xmem->grp);
	xmem->grpChain = *memRoot;
	*memRoot = xmem;
	GrpCountUpdate(xmem->grp, 1);
  }
  
  // Put it into the hash table
  return (Mem)PHashPush(gsv->mems, (PHashKey)mk, (PHashObj)xmem);
}

/**********************/
/** External Methods **/
/**********************/

S4 MemSequence(Mem xmem)
{
  return ObjMgrPtrToSeq(gsv->memObjMgr, (ObjPtr)xmem);
}

Mem MemBySequence(S4 seq)
{
  return (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, seq);
}

Mem MemFind(MemKey mk)
{
  if (!mk) return NULL;
  return (Mem)PHashFind(gsv->mems, (PHashKey)mk);
}

Mem MemCreateOrUpdate(MemSpec memSpec)
{
  return MemUpsert(memSpec);
}

// Create a new member, or update an existing one
// *Need a write permit
//
Mem MemUpsert(MemSpec memSpec)
{
  MemKeyRec mk;
  LdgKeyRec lk;
  Ldg xldg = NULL;
  Mem xmem = NULL;
  Mem memOld = NULL;
  Grp xgrp = NULL;
  Grp grpOld = NULL;
  RankTune rt = NULL;
  cSTR err = NULL;
  int  errCode = 0;
  PermitWrite myPermit = (PermitWrite)0;
  int gender, sameSex, reuseOld;
  S4  seq;
  US1 tagLoadMask=0;
  char memBuf[64];

  // Prep the mk first because it's used in error reporting
  StructNull(mk);
  if (memSpec) {
  	mk.memId = memSpec->memId;
	mk.domId = memSpec->domId;
  }

  memSpec->debugCount = 1;

  // Must have memId and domId
  if (!memSpec || (!memSpec->debugSeq && (!memSpec->memId || !memSpec->domId)))
	return NULL;
  // Must have gender
  if (!(gender = MemPackGender(memSpec->genderMask, &sameSex)))
	return NULL;

  memSpec->debugCount = 2;

  if (memSpec->permit) {
	// If permit passed in, make sure it's good
	if ((errCode = PermitWriteConfirm((PermitWrite)memSpec->permit)))
	  { err = "PermitWriteConfirm"; goto error; }
  } else {
	// Get the write permit
	if ((errCode = PermitWriteTake("MemUpsert", &myPermit, memSpec->memId)))
	  { err = "PermitWriteTake"; goto error; }
  }

  memSpec->debugCount = 3;

  gsv->statsUpserts += 1;

  memSpec->debugCount = 4;

  // Find the tuning structure for pre-processing this member's data
  rt = RankTuneToUse(memSpec->domId);

  memSpec->debugCount = 5;

  // See if this person already exists
  if (memSpec->debugSeq) {
	// If caller asked for it by sequence, sequence must already exist
	if (!(memOld = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, memSpec->debugSeq)))
	  { err = "ObjMgrSeqToPtr"; mk.memId = memSpec->debugSeq; goto error; }
  } else {
	if ((memOld = (Mem)PHashFind(gsv->mems, (PHashKey)&mk)))
	  memBuf[0] = 0; // Just a place holder to stop in debugger		
  }

  memSpec->debugCount = 6;

  if (memOld)
	grpOld = memOld->grp;

  memSpec->debugCount = 7;

  // Find or create this location
  StructNull(lk);
  if (memSpec->debugLocLat || memSpec->debugLocLng) {
	lk.loc.lat = memSpec->debugLocLat;
	lk.loc.lng = memSpec->debugLocLng;
  } else {
	MemPackLoc(&(lk.loc), &(memSpec->memLoc), 0);
  }

  memSpec->debugCount = 8;

  lk.domId  = memSpec->domId;
  lk.gender = gender;

  memSpec->debugCount = 9;

  if (!(xldg = LdgFindOrNew(&lk)))
	{ err = "LdgFindOrNew"; goto error; }

  {
	// Process any remaining NV pairs into tags
	int i; cSTR acVal;
	int memTagIdx = memSpec->tobjCnt;
	int memTagMax = ArrayCount(memSpec->tobj) - 32;
	Tag *memTagDat = (Tag*)memSpec->tobj;

	memSpec->debugCount = 11;

	// If an areacode was specified, create a tag for it too
	if ((acVal = memSpec->memLoc.regAreaCode) && acVal[0]) {
		cSTR acNam = KWLookupStr(KW_areacode);
		Tag xtag = TagFindOrNewStr(acNam, acVal);
		if (xtag)
		  memTagDat[memTagIdx++] = xtag;

		memSpec->debugCount = 12;
	}
	// Find, create the other tags
	for (i = 0; i < ArrayCount(memSpec->tags) && memSpec->tags[i].name && memTagIdx < memTagMax; i++) {
	  cSTR tnam = memSpec->tags[i].name;
	  cSTR tval = memSpec->tags[i].value;
	  TagDbId tdx = TagDbFind(tnam);

	  memSpec->debugCount = 15;
	 
	  if (tdx) {
		S4 code = E1atol(tval);
		memTagIdx += TagDbByCode(tdx, code, memTagDat+memTagIdx);
		memSpec->debugCount = 16;
	  } else if (tnam && tnam[0]) {
		Tag xtag = TagFindOrNewStr(tnam, tval);
		memSpec->debugCount = 17;
		if (xtag)
		  memTagDat[memTagIdx++] = xtag;
	  }
	}
	memSpec->debugCount = 18;

	memSpec->tobjCnt = memTagIdx;

	memSpec->debugCount = 19;
	// Find or create this Grp
	if (memTagIdx > 0) {
	  if (!(xgrp = GrpFindOrNewFromTags(memTagDat, memTagIdx)))
		{ err = "GrpFindOrNewFromTags"; goto error; }
	}
  }

  memSpec->debugCount = 20;

  // Check to see if nothing linked has changed, and on ly writeable fields are specified
  reuseOld = (memOld && (xldg == memOld->ldg) && (!xgrp || xgrp == memOld->grp));

  memSpec->debugCount = 21;
				 
  if (reuseOld) {
	xmem = memOld; 
  } else {
	// Create a new one -- can't write over objects in use !!!
	xmem = (Mem)ObjMgrNew(gsv->memObjMgr, &seq);

   memSpec->debugCount = 22;

#ifdef PENGDEBUG
	if ((seq & 0xFFFF) == 0)
	  memBuf[0] = 0; 
#endif
	// Link it to supporting objects 
	xmem->memId    = memSpec->memId;
	xmem->ldg      = xldg;
	xmem->grp      = xgrp;
	xmem->colorcode=memSpec->colorcode;
  }

  memSpec->debugCount = 23;

  // Load with accurate rep of it's location
  if (MemPackCSCZMap(&(xmem->cscz), &(memSpec->memLoc)))
  {
	  err = "Error performing member upsert - bad region.";
	  goto error;
  }

  memSpec->debugCount = 24;

  // Load dates from passed in strings or pre-packed values
  if (memSpec->birthDate)      
  {
	  xmem->birthPack.pack = MemPackDate(memSpec->birthDate);
	  memSpec->birthPack =  xmem->birthPack.pack;
  }
  else if (memSpec->birthPack) xmem->birthPack.pack = memSpec->birthPack;

  memSpec->debugCount = 25;

  if (memSpec->registerDate)      
  {
	  xmem->registerPack.pack = MemPackDate(memSpec->registerDate);
	  memSpec->registerPack = xmem->registerPack.pack;
  }
  else if (memSpec->registerPack) xmem->registerPack.pack = memSpec->registerPack;

  memSpec->debugCount = 26;

  if (memSpec->activeDate)
  {
	  xmem->activePack.pack = MemPackDateTime(memSpec->activeDate);
	  memSpec->activePack = xmem->activePack.pack;
  }
  else if (memSpec->activePack) xmem->activePack.pack = memSpec->activePack;

  memSpec->debugCount = 27;

  // Load in the rest of the vitals
	MemPackVitals(rt, &(xmem->vitals), memSpec);

	//Load in moreChildrenFlag
	xmem->moreChildrenFlag = memSpec->moreChildrenFlag;

  memSpec->debugCount = 28;

  if (!reuseOld) {
	// Disconnect old member instance from other chains
	if (memOld)
	  if ((errCode = xMemLinkOut(memOld)))
	    { err = "xMemLinkOut"; goto error; }
	// Now link me in
	if (!xMemLinkIn(xmem, &mk))
	  { err = "xMemLinkIn"; goto error; }
	// Set old memory to expire when all active threads have passed
	if (memOld)
	  PermitExpireObj(gsv->memObjMgr, (ObjPtr)memOld);
  }

  memSpec->debugCount = 29;

#if 0 
  // ??? Having trouble with this, and it's too slow. 
  // If we don't dispose, unused Grp structures will slowly accumulate
  if (grpOld && !grpOld->memChain) {
	PermitExpireCB((PermitExpireCBFnc*)GrpDispose, (ObjPtr)grpOld);
  }
#endif

  if (myPermit)
    PermitWriteGive(myPermit);

  memSpec->debugCount = 30;

  return xmem;

 error:
  if (myPermit)
    PermitWriteGive(myPermit);
  snprintf(memBuf, sizeof(memBuf), "MemUpsert(%d): %ld", errCode, mk.memId);
  PEngError(PKGNAME, memBuf, err);
  return NULL;
}

// Update dynamic attributes of a member that don't impact linkage
// *Need a search permit
//
Mem MemUpdate(MemUpdateSpec xspec)
{
  Mem xmem = NULL;
  PermitSearch myPermit = (PermitSearch)NULL;

  if (!gsv->mems || !xspec) 
	return NULL;

  // Take a search permit so no one else messes with our Mem
  if (PermitSearchTake(&myPermit))
	return NULL;

  // This is not thread safe, but this value only used for gross analysis
  gsv->statsUpdates += 1;

  if (xspec->debugSeq) { 
	// lookup by sequence
	xmem = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, xspec->debugSeq);
  } else { 
	// lookup by domid/memid
	MemKeyRec mk;
	StructNull(mk);
	mk.memId = xspec->memId;
	mk.domId = xspec->domId;
	xmem = (Mem)PHashFind(gsv->mems, (PHashKey)&mk);
  }

  if (xmem) {
	cSTR sval; US2 ival;

    // Find the tuning structure for pre-processing this member's data
    RankTune rt = RankTuneToUse(xspec->domId);

    if (xspec->hasPhoto != (US1)E1UpdateFlag_NoChange)
		xmem->vitals.hasPhoto = (xspec->hasPhoto == (US1)E1UpdateFlag_SetYes);
	if (xspec->isOnline != (US1)E1UpdateFlag_NoChange)
		xmem->vitals.isOnline = (xspec->isOnline == (US1)E1UpdateFlag_SetYes);

	// Set (or NULL) the last active date
	if ((sval = xspec->activeDate)) {
		// First precedence is string date
		if (sval[0] == 0) {
			xmem->activePack.pack = 0;
			xmem->vitals.saAct = 0;
		} else {
			xmem->activePack.pack = MemPackDateTime(sval);
			xmem->vitals.saAct = RankActExec(rt, xmem->activePack.pack, 1);
		}
	} else if ((ival = xspec->activePack)) {
		// Then check for a packed date
		if (ival == (US2)-1) {
			xmem->activePack.pack = 0;
			xmem->vitals.saAct = 0;
		} else {
			xmem->activePack.pack = ival;
			xmem->vitals.saAct = RankActExec(rt, xmem->activePack.pack, 1);
		}
	} else if (xspec->saAct) {
		// Check for explicit saAct
		xmem->vitals.saAct = minmax(xspec->saAct, 1, 15);
	}

	// Set (or NULL) popularity
	if (xspec->emailCount != E1_NULL_INT) {
			xmem->vitals.saPop = RankPopExec(rt, xspec->emailCount, 1); 
	} else if (xspec->saPop) {
		// Check for explicit saAct
		xmem->vitals.saPop = minmax(xspec->saPop, 1, 15);
	}
  }
  // Give back our permit
  PermitSearchGive(myPermit);
  return xmem;
}

// Delete a member
// *Need a WritePermit
//
int MemDelete(MemDeleteSpec xspec)
{
  Mem xmem;
  Grp xgrp;
  cSTR err = NULL; int errCode = 0;
  char memBuf[64];
  PermitWrite myPermit = (PermitWrite)0;

  if (!gsv->mems || !xspec) 
	{ err = "NULL"; goto error; }

  if (errCode = PermitWriteTake("MemDelete", &myPermit, xspec->memId))
	{ err = "PermitWriteTake"; goto error; }

  gsv->statsDeletes += 1;

  if (xspec->debugSeq) { 
	// lookup by sequence
	xmem = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, xspec->debugSeq);
  } else { 
	// lookup by domid/memid
	MemKeyRec mk;
	StructNull(mk);
	mk.memId = xspec->memId;
	mk.domId = xspec->domId;
	xmem = (Mem)PHashFind(gsv->mems, (PHashKey)&mk);
  }
  
  if (!xmem)
	{ err = "NotFound"; goto error; }
  xgrp = xmem->grp;

  if ((errCode = xMemLinkOut(xmem)))
  { err =" xMemLinkOut"; goto error; }

#if 0
  // ??? Having trouble with this, and it's too slow. 
  // If we don't dispose, unused Grp structures will slowly accumulate
  if (xgrp && !xgrp->memChain) {
	PermitExpireCB((PermitExpireCBFnc*)GrpDispose, (ObjPtr)xgrp);
  }
#endif
  PermitExpireObj(gsv->memObjMgr, (ObjPtr)xmem);

  PermitWriteGive(myPermit);
  return 0;

 error: 
  if (myPermit)
	PermitWriteGive(myPermit);
  if (!xspec) strcpy(memBuf, "MemDelete: NoSpec");
  else snprintf(memBuf, sizeof(memBuf), "MemDelete(%d): %ld", errCode,
		        (xspec->debugSeq ? xspec->debugSeq : xspec->memId));
  PEngError(PKGNAME, memBuf, err);
  return -1;
}

#define kMaxById 16

/// <summary>Refresh Active/New scores for all members while holding SearchPermit</summary>
/// <returns>Number of members whose Activity Score changed</returns>
/// Call whenever day flips over to the next day
///
int MemMaintain()
{
	int saAct, saNew;
	S4 seq, memSeq, memDelta = 0;
	RankTune rt, rtByDomId[kMaxById];

	// Set up cache for RankTunes by DomId for the first block of DomIds
	StructNull(rtByDomId);

	// Don't need a permit since we're going directly to the block manager
	//  No risk of link corruption, but small chance a writer simultaneously writing same member 
	//  In nearly all cases, both will be setting same value, except
	//  ??? Very small risk: this thread sees zero date, writer writes date then score, then this thread writes zero score
	//  Effect will be that that member would have wrong Act or New score
	//  Preventing this means holding a write permit for entire duration
	//
	memSeq = ObjMgrSequence(gsv->memObjMgr);
	for (seq = 1; seq < memSeq; seq++) {
		Mem   xmem  = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, seq);
		DomId domId = xmem->ldg->ldgKey.domId;
	
		// Try to cache the RankTune for the DomId
		if (domId >= kMaxById) {
			rt = RankTuneToUse(domId);
		} else if (!(rt = rtByDomId[domId])) {
			rt = rtByDomId[domId] = RankTuneToUse(domId);
		}

		// Reset the scores
		saAct = RankActExec(rt, xmem->activePack.pack, 1);
		saNew = RankNewExec(rt, xmem->registerPack.pack, 1);

		if (xmem->vitals.saAct != saAct) 
			memDelta += 1;

		xmem->vitals.saAct = saAct;
		xmem->vitals.saNew = saNew;
	}
	return memDelta;
}
