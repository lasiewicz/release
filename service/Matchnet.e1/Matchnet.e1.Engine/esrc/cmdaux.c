// cmdaux.c
// pnelson 11/22/2005
// Helpers for URL/CmdLine dispatcher

#include "stdlib.h"
#include "string.h"
#include "peng.h"
#include "cmd.h"

#define PKGNAME PkgName_Cmd

#define isWhite(ch) ((ch) == ' ' || (ch) == '\t' || (ch) == '\r' || (ch) == '\n')

/******************************/
/** Keywords in our language **/
/******************************/

static STR xCmdTrim(STR str)
{
	int len;
	while (*str && isWhite(*str)) str++;
	len = strlen(str);
	while (len > 0 && isWhite(str[len-1])) str[--len] = 0;
	return str;
}

// Turn a command line into a list of typed, parsed, trimmed pairs
//
typedef struct _CmdParsePairs { 
	KWEnum tag; STR val;
} CmdParsePairsRec, *CmdParsePairs;

static void parsePairsFree(CmdParsePairs pairs, S4 allocSize)
{
	if (pairs)
		PEngFree(PKGNAME, pairs, allocSize);
}

static CmdParsePairs parsePairsNew(STR str, S4 *allocSizep)
{
	CmdParsePairs pairs = NULL;
	int idx, maxNum, allocSize = 0;
	int len = (str ? strlen(str) : 0);
	if (!len) 
		return NULL;

	maxNum = 10 + len/5;
	*allocSizep = allocSize = maxNum * sizeof(CmdParsePairsRec);
	if (!(pairs = PEngAlloc(PKGNAME, allocSize)))
		return NULL;

	// First one is command followed by ?
	pairs[0].tag = KW_cmd;
	pairs[0].val = str;
	while (*str && (*str != '?' && *str != ' ')) str++;
	if (*str) *str++ = 0;
	pairs[0].val = xCmdTrim(pairs[0].val);

	for (idx = 1; *str && idx < maxNum-2; idx++) {
		STR val, tag = str;
		while (*str && *str != '=') str++;
		if (!*str) goto error;
		*str++ = 0;
		pairs[idx].tag = KWLookupKey(xCmdTrim(tag));

		val = str;
		while (*str && *str != '&') {
		  if (str[0] == '\\' && str[1]) str++;
		  str++;
		}
		if (*str) *str++ = 0;
		pairs[idx].val = xCmdTrim(val);
	}
	pairs[idx].tag = 0;
	pairs[idx].val = 0;
	return pairs;
 error:
	parsePairsFree(pairs, allocSize);
	return NULL;
}

/****************************************************************/

static void parseSpecFree(CmdSpec spec)
{
	if (spec)
		PEngFree(PKGNAME, spec, spec->specSize);
}

// Create a properly typed and populated cmdSpec from a list of pairs
//
static CmdSpec parseSpec(CmdParsePairs pairs, const CmdSpec *grammar)
{
	int i, j;
	KWEnum cmd, typ, ptag, atag;
	CmdSpec base = NULL;
	CmdSpec spec = NULL;

	if (!pairs)	return NULL;

	cmd = (pairs[0].tag != KW_cmd  ? KW_null : KWLookupKey(pairs[0].val));
	typ = (pairs[1].tag != KW_type ? KW_null : KWLookupKey(pairs[1].val));

	if (cmd == KW_null)
		return NULL;

	for (i = 0; (base = grammar[i]); i++) {
		if (base->cmd == cmd &&	(base->typ == KW_null || base->typ == typ))
			break;
	}
	if (!base)
		return NULL;

	spec = PEngAlloc(PKGNAME, base->specSize);
	*spec = *base;
	spec->typ = typ;

	// Start after cmd and type
	i = (pairs[1].tag == KW_type ? 2 : 1);
	for ( ; (ptag = pairs[i].tag) != KW_null || pairs[i].val; i++) {
		switch (ptag) {
		case 0: // Skip unknown arguments
			continue;
		case KW_xsl:
			spec->xsl = pairs[i].val;
			break;
		case KW_mode:
			spec->mode = (S2)atol(pairs[i].val);
			break;
		default:
		  // Find the description for this attr in the command spec
		  for (j=0; (atag = spec->args[j].tag) != KW_null; j++) {
			if (ptag == atag) {
				CmdSpecX aa = &(spec->args[j]);
				STR     val = pairs[i].val;
				int     ax  = aa->curIdx++;
				switch (aa->argType) {
				case Arg_S4:    *(S4*)((US1*)spec + aa->soff + ax*sizeof(S4)) = (S4)atol(val); break;
				case Arg_S2:    *(S2*)((US1*)spec + aa->soff + ax*sizeof(S2)) = (S2)atol(val); break;
				case Arg_S1:    *(S1*)((US1*)spec + aa->soff + ax*sizeof(S1)) = (S1)atol(val); break;
				case Arg_CH:    *(S1*)((US1*)spec + aa->soff + ax*sizeof(S1)) = (S1)val[0]; break;
				case Arg_Str:   *(STR*)((US1*)spec + aa->soff + ax*sizeof(STR)) = val; break;
				case Arg_Buf16: str0ncpy(((US1*)spec + aa->soff + ax*16), val, 16); break;
				case Arg_Buf32: str0ncpy(((US1*)spec + aa->soff + ax*32), val, 32); break;
				case Arg_Float: *(float*)((US1*)spec + aa->soff + ax*sizeof(float)) = (float)atof(val); break;
				default: 
					break; // Don't know what it is
				}
				break; 
			}
		  }
		  // If we don't find this tag, complain
		  if (atag == KW_null)
			goto error;
		}
		// Work on the next one
	}

	// We now have our populted cmd spec
	return spec;

 error:
	parseSpecFree(spec);
	return NULL;
}

/****************/

CmdSpec CmdSpecNew(STR cmdLine, const CmdSpec *grammar)
{
	S4 allocSize = 0;
	CmdParsePairs pairs = parsePairsNew(cmdLine, &allocSize);
	CmdSpec       inst  = parseSpec(pairs, grammar);

	if (inst) { 
		inst->pairs     = pairs;
		inst->pairsSize = allocSize;
		return inst;
	} else {
		parsePairsFree(pairs, allocSize);
		return NULL;
	}
}

void CmdSpecFree(CmdSpec spec)
{
	if (spec) {
		if (spec->pairs)
			parsePairsFree(spec->pairs, spec->pairsSize);
		parseSpecFree(spec);
	}
}

/****************************************************************/

// Must follow exact order of enums
static char *ArgNames[] = {
	"Unknown", "S4", "S2", "S1", "CH", "Str", "Buf16", "Buf32",	"Float"
};

static int showOne(CmdSpec spec, PXml px, int mode)
{
	int j, err;
	char msgBuf[512], typBuf[64];

	if (spec->typ == KW_null) typBuf[0] = 0;
	else snprintf(typBuf, sizeof(typBuf), " type='%s'", KWLookupStr(spec->typ));
	snprintf(msgBuf, sizeof(msgBuf), "cmd name='%s'%s", KWLookupStr(spec->cmd), typBuf);

	if (!spec->help && spec->args[0].tag == KW_null) {
		err = PXmlPush0(px, msgBuf, 0);
	} else if (mode == 0) {
		err = PXmlSetStr(px, msgBuf, spec->help);
	} else {
		err = PXmlPush(px, msgBuf, 0);
		if (spec->help) 
			err |= PXmlSetStr(px, "help", spec->help);
		if (spec->typ) {
			snprintf(msgBuf, sizeof(msgBuf), "arg name='%s' type='%s'", 
					 KWLookupStr(KW_type), KWLookupStr(spec->typ));
			err |= PXmlSetStr(px, msgBuf, "Required type for this command");
		}
		snprintf(msgBuf, sizeof(msgBuf), "arg name='%s' type='S2'", KWLookupStr(KW_mode));
		err |= PXmlSetStr(px, msgBuf, "Bit mask to control display mode");

		snprintf(msgBuf, sizeof(msgBuf), "arg name='%s' type='URL'", KWLookupStr(KW_xsl));
		err |= PXmlSetStr(px, msgBuf, "URL to XSL file to post-process XML output for display");

		for (j=0; spec->args[j].tag != KW_null; j++) {
			CmdSpecX aa = &(spec->args[j]);
			KWEnum     tagType = aa->tag;
			cSTR       tagName = KWLookupStr(tagType);
			CmdArgType argType = aa->argType;
			STR        argName = ArgNames[(int)argType];
			cSTR       argHelp = CmdKWHelp[(int)tagType];
			if (aa->howMany <= 1) typBuf[0] = 0;
			else snprintf(typBuf, sizeof(typBuf), " upto='%d'", aa->howMany);
			snprintf(msgBuf, sizeof(msgBuf), "arg name='%s' type='%s'%s", 
					 tagName, argName, typBuf);
			err |= PXmlSetStr(px, msgBuf, argHelp);
		}
		err |= PXmlPop(px);
	}
	return err;
}

int CmdGrammarShowHelp(const CmdSpec *grammar, KWEnum justOne, PXml px)
{
	int i, mode, err;
	CmdSpec spec;
	
	err = PXmlPush(px, "cmds", 0);

	if (justOne == KW_help) {
		// asking "help type=help" means just give overview w/o detailed args
		mode = 0; justOne = KW_null;
	} else {
		mode = 1;
	}

	for (i = 0; (spec = grammar[i]); i++) {
		if (justOne == KW_null || spec->cmd == justOne)
			err |= showOne(spec, px, mode);
	}
	err |= PXmlPop(px);
	return err;
}
