// memcheck.c
// pnelson 4/3/2006
// Walk the network checking for consistency

/// This is a first pass at consistency checker -- 
///    walks master hash table for each type to find objects in play, 
///    and then checks that their first level pointers are valid pointers
/// Does not do network consistency checks yet (like making sure children point back to parents, and that there are no loops)

#define LDG_FRIEND
#define MEM_FRIEND
#define GRP_FRIEND
#define TAG_FRIEND
#include "peng.h"

/****************/

static int checkLdg(Ldg xldg, E1CheckErr err, int depth);
static int checkMem(Mem xmem, E1CheckErr err, int depth);
static int checkGrp(Grp xgrp, E1CheckErr err, int depth);
static int checkTag(Tag xtag, E1CheckErr err, int depth);
static int checkStx(cSTR text, S4 seq, E1CheckErr err);
static int checkStxRegion(cSTR text, S4 seq, E1CheckErr err);

/****************/

static int checkDbgStop(E1CheckErr err, const void *ptr)
{
	err->ptr = ptr;
	return (err->errCode = -1);
}

static int checkLdg(Ldg xldg, E1CheckErr err, int depth)
{
	S4 seq;
	if (!xldg)
		{ err->msg = "Ldg: NULL"; goto abort; }

	if (!(seq = ObjMgrPtrToSeq(gsv->ldgObjMgr, (ObjPtr)xldg)))
		{ err->msg = "Ldg: InvalidPtr"; goto abort; }

	if (depth == 0) {
		// Check validity of head of mem chain
		if (xldg->memChain && (checkMem(xldg->memChain, err, depth+1)))
			goto abort;
	}

	// Do other checks ???
	return 0;
 abort:	 
	return checkDbgStop(err, xldg); 
}

static int checkMem(Mem xmem, E1CheckErr err, int depth)
{
	S4 seq;
	if (!xmem)
		{ err->msg = "Mem: NULL"; goto abort; }

	if (!(seq = ObjMgrPtrToSeq(gsv->memObjMgr, (ObjPtr)xmem)))
		{ err->msg = "Mem: InvalidPtr"; goto abort; }

	if (!xmem->memId)
		{ err->msg = "Mem: NoMemId"; goto abort; }

	if (depth == 0) {
		// Check validity of Grp,GrpChain, Ldg,LdgChain ptrs
		if (xmem->ldg      && (checkLdg(xmem->ldg, err, depth+1)))
			goto abort;
		if (xmem->ldgChain && (checkMem(xmem->ldgChain, err, depth+1)))
			goto abort;
		if (xmem->grp	   && (checkGrp(xmem->grp, err, depth+1)))
			goto abort;
		if (xmem->grpChain && (checkMem(xmem->grpChain, err, depth+1)))
			goto abort;
	}
	// Do other checks ???
	return 0;
 abort:	 
	return checkDbgStop(err, xmem); 
}

static int checkGrp(Grp xgrp, E1CheckErr err, int depth)
{
	Grp tgrp; int i;

	if (!xgrp)
		{ err->msg = "Grp: NULL"; goto abort; }
	if (!xgrp->sequence)
		{ err->msg = "Grp: NoSequence"; goto abort; }

	tgrp = (Grp)SeqMgrLookup(gsv->grpSeqMgr, xgrp->sequence);
	if (tgrp != xgrp)
		{ err->msg = "Grp: SequenceMatch"; goto abort; }

	if (depth == 0) {
		if (xgrp->memChain && (checkMem(xgrp->memChain, err, depth+1)))
			goto abort;

		// Check validity of all tags
		for (i=0; i < xgrp->tagCount; i++) {
			// Must have a tag
			if (checkTag(xgrp->tagList[i], err, depth+1))
				goto abort;
			// If tag has next parent, must be valid
			if (xgrp->grpNext[i] && checkGrp(xgrp->grpNext[i], err, depth+1))
				goto abort;
		}
	}
	return 0;
 abort:	 
	return checkDbgStop(err, xgrp); 
}

static int checkTag(Tag xtag, E1CheckErr err, int depth)
{
	S4 tseq; Tag ttag;

	if (!xtag)
		{ err->msg = "Tag: NULL"; goto abort; }

	if (!(tseq = ObjMgrPtrToSeq(gsv->tagObjMgr, (ObjPtr)xtag)))
		{ err->msg = "Tag: InvalidPtr"; goto abort; }

	ttag = (Tag)ObjMgrSeqToPtr(gsv->tagObjMgr, xtag->sequence);

	if (ttag != xtag || tseq != xtag->sequence) 
		{ err->msg = "Tag: InvalidSeq"; goto abort; }

	if (depth == 0) {
		if (xtag->grpChain && (checkGrp(xtag->grpChain, err, depth+1)))
			goto abort;
	}

	if (xtag->nxId) {
		// Get the string and strlen it to touch all bytes -- better be valid 
		cSTR nstr  = StxDbById(gsv->pkTags, xtag->nxId);
		int  nlen  = (nstr ? strlen(nstr) : 0);
	}
	if (xtag->vxId) {
		// Get the string and strlen it to touch all bytes -- better be valid 
		cSTR vstr = StxDbById(gsv->pkTags, xtag->vxId);
		int vlen  = (vstr ? strlen(vstr) : 0);
	}

	// Do other checks ???
	return 0;
 abort:	 
	return checkDbgStop(err, xtag); 
}

static int checkStx(cSTR text, S4 seq, E1CheckErr err)
{
	int len = strlen(text);
	if (len < 0)
		{ err->msg = "Stx: InvalidValue"; goto abort; }

	if (seq <= 0)
		{ err->msg = "Stx: InvalidSeq"; goto abort; }

	return 0;
 abort:
	err->val = seq;
	return checkDbgStop(err, text); 
}

static int checkStxRegion(cSTR text, S4 seq, E1CheckErr err)
{
	char nbBuf[32]; 
	S4 nbNum;
	int len, nbLen;

	if (!text)
		{ err->msg = "Stx: RegionNull"; goto abort; }
	if (!(len = strlen(text)))
		{ err->msg = "Stx: RegionEmpty"; goto abort; }

	if (seq <= 0)
		{ err->msg = "Stx: RegionSeq"; goto abort; }

	nbNum = E1atol(text);
	snprintf(nbBuf, sizeof(nbBuf), "%ld", nbNum); nbBuf[sizeof(nbBuf)-1] = 0;
	nbLen = strlen(nbBuf);

	if (nbLen != len || strcmp(text, nbBuf) != 0)
		{ err->msg = "Stx: RegionAsNumber"; goto abort; }
	
	return 0;
 abort:
	err->val = seq;
	return checkDbgStop(err, text); 
}

/****************/

int doCheck(E1CheckErr err)
{
	err->pkg = "Ldg";
	if (PHashCheck(gsv->ldgs, (PHashCheckFnc*)checkLdg, err))
		goto abort;

	err->pkg = "Mem";
	if (PHashCheck(gsv->mems, (PHashCheckFnc*)checkMem, err))
		goto abort;

	err->pkg = "Grp";
	if (PHashCheck(gsv->grps, (PHashCheckFnc*)checkGrp, err))
		goto abort;

	err->pkg = "Tag";
	if (PHashCheck(gsv->tags, (PHashCheckFnc*)checkTag, err))
		goto abort;

	err->pkg = "Tags";
	if (StxCheck(gsv->pkTags, checkStx, err))
		goto abort;

	err->pkg = "Country";
	if (StxCheck(gsv->pkCountry, checkStxRegion, err))
		goto abort;

	err->pkg = "State";
	if (StxCheck(gsv->pkState, checkStxRegion, err))
		goto abort;

	err->pkg = "City";
	if (StxCheck(gsv->pkCity, checkStxRegion, err))
		goto abort;

	err->pkg = "Zip";
	if (StxCheck(gsv->pkZip, checkStxRegion, err))
		goto abort;

	err->pkg = NULL;
	return 0;
 abort:
	return -1;
}

int E1Check(E1CheckErr err)
{
	int retVal = 0;
	PermitSearch myPermit;

	memset(err, 0, sizeof(*err));
	if (!gsv->initialized)
		return 0;

	err->pkg = "Permit take";
	if (PermitSearchTake(&myPermit))
		return -1;

	if(!PermitSearchBeg(myPermit))
	{
		retVal = doCheck(err);

		PermitSearchEnd(myPermit, 0);
	}

	PermitSearchGive(myPermit);

	return retVal;
}
