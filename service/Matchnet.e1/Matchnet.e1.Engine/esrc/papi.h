// papi.h
// pnelson, 11/13/2005
// Defintions needed by callers to the engine

#define INCLUDE_PAPI

#include <windows.h>

/*******************/
/** Generic Types **/
/*******************/

typedef   signed char   S1;
typedef unsigned char  US1;

typedef   signed short  S2;
typedef unsigned short US2;

typedef   signed long   S4;
typedef unsigned long  US4;

typedef       char  *STR;
typedef const char  *cSTR;

/*****************************/
/** Startup and Maintenance **/
/*****************************/

extern void GsvInitialize();
extern void GsvDestroy();

typedef struct _GsvStats {
  int todayYear, todayMonth, todayDay, todayHour, todayMinute;  // Today's date as known to engine

  // How many of each object type
  S4 ldgs, mems, grps, tags, stxs;
  S4 memoryMem, memoryGrp, memoryAll;   // How much memory
  S4 expires;
  
  S4 upserts;  // How many upserts
  S4 updates;  // How many updates
  S4 deletes;  // How many deletes
  S4 searchNews;   // How many calls to S1SearchNew
  S4 resultFrees;  // How many calls to S1ResultFree

  US4 queries;  // How many queries
  US4 scansHi, scansLo; // How many scans
  S4 ticks;    // Average ticks per search
  int threads; // Max simultaneous search threads
  int searchPermits; //How many search permits are currently available

  int debugCount; //For tracking line number where errors occur

} GsvStatsRec, *GsvStats;

extern int GsvMaintain(GsvStats stats);
extern int GsvStatsLoad(GsvStats stats);

/*********************/
/** URL Interpreter **/
/*********************/

typedef struct _PXml *PXml;
extern PXml PXmlNew  (S4 dataMax, int enumMax, cSTR xslName);
extern STR  PXmlData (PXml px, S4 *lenp);
extern void PXmlShow (PXml px, void *file, int reset);
extern void PXmlReset(PXml px);
extern void PXmlResetXsl(PXml px, cSTR xsl);
extern void PXmlFree (PXml px);

extern int CmdEval(STR cmdLine, PXml px, int readOnly);

extern int         LoadFile(cSTR fname, void *file, PXml px, int mode);
extern int         LoadFileAux(cSTR fname, void *file, PXml px, int mode, cSTR header);

/****************/
/** Base Types **/
/****************/

typedef US4 MemId;
typedef US1 DomId;
#define GenderMale   0x1
#define GenderFemale 0x2

/********************/
/** Adding Members **/
/********************/

#define kMemSpec_TagsMax  128
#define E1_NULL_INT -1

// Opaque handle to a Member
//
typedef struct _Mem   *Mem;

// Full spec of a location
//
typedef struct _MemLoc {
  float  regLat;
  float  regLng;
  cSTR   regCountry;
  cSTR   regState;
  cSTR   regCity;
  cSTR   regZip;
  cSTR   regAreaCode;
} MemLocRec, *MemLoc;

typedef struct _MemNV { 
  cSTR name;
  cSTR value; 
} MemNVRec, *MemNV;

/// Enum for updating flag values (like hasPhoto or isOnline)
typedef enum {
  E1UpdateFlag_NoChange	= 0,
  E1UpdateFlag_SetYes	= 1,
  E1UpdateFlag_SetNo	= 2
} E1UpdateFlag;

// Describe a member to load
//
typedef struct _MemSpec {
  // memId, domId, gender
  MemId   memId;
  DomId   domId;
  US1     genderMask;

  /** Remaining fields can also be passed as NV pairs, vals in struct take precendence **/

  // Location
  MemLocRec  memLoc;

  // Key vitals stored in Mem structure (everything else is a tag) 
  STR    birthDate;      // YYYY-MM-DD
  STR    registerDate;   // YYYY-MM-DD
  STR    activeDate;     // YYYY-MM-DD
  S4     emailCount;     // For popularity score

  US2    height;         // In inches
  US4    weight;         // In grams
  US1    childrenCount;
  US1	moreChildrenFlag;//0 = no, 1 = yes, 2 = not sure	
  E1UpdateFlag	hasPhoto;
  E1UpdateFlag	isOnline;  
  US1    saAct;
  US1    saNew;
  US1    saPop;
  
  // Other attributes as name-value pairs
  MemNVRec tags[kMemSpec_TagsMax];

  // Internal fields -- don't change
  void *tobj[kMemSpec_TagsMax*2];
  S2    tobjCnt;
  S4    debugSeq;
  US2   debugLocLat, debugLocLng;
  US2   birthPack;      // YYYY-MM-DD
  US2   registerPack;   // YYYY-MM-DD
  US4   activePack;     // YYYY-MM-DD

  S4    permit;         // Permit to write
  US2   colorcode;
  US2	debugCount;		//For tracking line number where errors occur
} MemSpecRec, *MemSpec;

// Create or update this member, may require a new object to replace old
//
extern Mem MemCreateOrUpdate(MemSpec memSpec);
extern Mem MemUpsert(MemSpec memSpec);

// Describe fields that can be easily updated
//
typedef struct _MemUpdateSpec {
  // Locate the member with memId, domId
  MemId   memId;
  DomId   domId;

  // Update active and populare scores w/ explicit value, or by calculation
  STR    activeDate;     // YYYY-MM-DD
  S4     emailCount;
  US1    saPop;
  US1    saAct;

  // Update key flags
  E1UpdateFlag    hasPhoto;
  E1UpdateFlag    isOnline;  

  // Internal fields -- don't change
  S4    debugSeq;
  US2   activePack;     // YYYY-MM-DD

} MemUpdateSpecRec, *MemUpdateSpec;

// Update the rapidly changing fields on an existing member 
//
extern Mem MemUpdate(MemUpdateSpec memSpec);

// Describe member to delete
//
typedef struct _MemDeleteSpec {
  MemId   memId;
  DomId   domId;
  S4    debugSeq;
} MemDeleteSpecRec, *MemDeleteSpec;

// Delete a member 
//
extern int MemDelete(MemDeleteSpec memSpec);


/****************************************************************/

/*************/
/** Permits **/
/*************/

typedef __int64 E1Ticks;

// The one write permit is implemented with a Mutex
//

// Search Permits are implemented with an interlocked list
//   max num of simultaneous searches set on init() and that many permits created and pushed to list
//   Everyone trying to search must get one of those permits
//   While the search is running, the start time (in ticks) is stored in the permit
//   No memory will actually be free() until all searches that started before that memory expired are done
//   http://msdn.microsoft.com/library/en-us/dllproc/base/interlocked_singly_linked_lists.asp
//
typedef struct _PermitSearch {
  SLIST_ENTRY    sList; // Internal interlocked list structure
  int            seq;   // Sequence number of this permit
  E1Ticks        now;   // Time that search holding this permit started (or 0)

  S4       cumQuery;   // Cumulated number of searches
  E1Ticks  cumTick;    // Cumulated ticks for running searches
  E1Ticks  cumScan;     // Cumulated scans for those searches

  void *forceAlign;     // Make sure structure is aligned (SLIST requirement) 
  
} PermitSearchRec, *PermitSearch;

extern int PermitSearchTake(PermitSearch *permitp);
extern int PermitSearchGive(PermitSearch permit);
extern int PermitSearchBeg(PermitSearch permit);
extern int PermitSearchEnd(PermitSearch permit, S4 scans);


/************/
/** Search **/
/************/

#define kS1Search_TagMax      100  // Most tags that can be requested
#define kS1Search_ResultMax   999  // Most memebers that can be returned
#define kS1SearchColorCode_ResultMax   360  // Most memebers that can be returned for colorcode search

#define kS1Search_DistanceMax 160  // Farthest we'll scan from center
#define kS1Search_ExtendMin   10   // Minimum number of results, else we go past requested range

typedef signed char S1Score;

// ???NB: The following enums are defined separately in managed C# code.  
//   If any of the underlying values are changed here,
//     they must be changed in the corresponding enum in Matchnet.Search.Interfaces.

/// Enums for different sorting options (which choose differnt blends) 
typedef enum {
  S1Sort_Unknown = 0,
  S1Sort_Newest = 1,
  S1Sort_Active = 2,
  S1Sort_Near = 3,
  S1Sort_Popular = 4,
  S1Sort_Online = 5
} S1SortKey;

#define kS1Sort_Max 6

/// Enum for testing flag values (like hasPhoto or isOnline)
typedef enum {
  S1Flag_NoTest  = 0,
  S1Flag_MustYes = 1,
  S1Flag_MustNo  = 2
} S1Flag;

#define S1Blend_Require (-101)
#define S1Blend_ForceNull (-102)


/// <summary>
///  How to blend individual scores into a single number for ranking
///   value == 0               ==> ignore score completely
///   value == S1Blend_Require ==> must be non-zero to consider member, score not blended
///   value between +(1:100)   ==> how much to blend this score into total
///   value between -(1:100)   ==> must be non-zero to consider member, abs(value) is blend
/// </summary>
///
typedef struct _S1Scores {
  /// overall match of attribute tags, if <0 *all* attrs must match
  S1Score match;
  /// ldg distance from center of query
  S1Score ldg;
  /// overlap of c/s/c/z
  S1Score cscz;
  /// match to requested age
  S1Score age;
  /// match to requested height
  S1Score hgt;
  /// member's newness rating
  S1Score newreg;
  /// member's activity rating
  S1Score act;
  /// member's popularity rating
  S1Score pop;
  /// member's online status
  S1Score mol;
  /// reciprocal match
  S1Score recip;
 
} S1ScoresRec, *S1Scores;

#define S1ScoresNullRec(b) b.match=b.ldg=b.cscz=b.age=b.hgt=b.newreg=b.act=b.pop=b.mol=b.recip=0
#define S1ScoresNull(b) b->match=b->ldg=b->cscz=b->age=b->hgt=b->newreg=b->act=b->pop=b->mol=b->recip=0

/// Specification of a query to the engine
typedef struct _S1Query {
  S2        sortKey;     /// Defaults in RankTune (currently most popular)
  S4        resMax;      /// Defaults in RankTune (currently 250)
  DomId     domId;       /// Required: domain to search
  US1       genderMask;  /// Required: gender to search

  MemLocRec location;      /// Location (1/3) by focus of search (need lat/lng in radians)
  US2       areaCodes[10]; /// Location (2/3) by list of area codes
  S2        schoolId;	   /// Location (3/3) by schoolId

  S2        distance;    /// Defaults in RankTune (currently 50 miles)
  S1Flag    hasPhoto;
  S1Flag    isOnline; 
  US2       ageMinDesired;  /// age in years
  US2       ageMaxDesired;
  US2       hgtMinDesired;  /// hgt in cm
  US2       hgtMaxDesired;
  US4       wgtMinDesired;  /// wgt in grams
  US4       wgtMaxDesired;
  US4		childrenCount;
  US4		moreChildrenFlag;  //0 = no, 1 = yes, 2 = either
  US4		colorCode;  //0 = no, 1 = white, 2 = blue, 4 = yellow, 8 = red
  MemNVRec tags[kS1Search_TagMax];  // Tags treated as And/Or group based on attribute name

  cSTR         rankTuneName;   /// NULL means use default for domain
  S1ScoresRec  rankTuneBlend;  // If any non-zero, overrides what's in rankTune

  PermitSearch permit;

  // Debug params
  US2 debugLocLat, debugLocLng;
  US2 debugCount;		 //For tracking line number where errors occur

} S1QueryRec, *S1Query;

// Opaque handle to compiled query
typedef struct _S1Scan *S1Scan;
typedef struct _PRes   *PRes;

// Results includes stats, and list of tags and members
//
typedef struct _S1Result {
  S4        sequence; // An arbitrary id for this search
  S1SortKey sortKey;  // The requested sort
  US2       memMax;   // The requested max results

  US2       memNum;   // The actual number returned
  S4        memScan;  // How many members were scanned
  S4        memLive;  // How many showed a non-zero score
  S4        ldgScan;  // How many LDGs were scanned

  MemId     memids[kS1Search_ResultMax+1];  // Ranked members
  US2       scores[kS1Search_ResultMax+1];  // Their  scores

  // Internal handles, don't touch
  S4      allocSize;
  S1Scan  xscan;
  void    *pres;

} S1ResultRec, *S1Result;

extern S1Result S1Search(S1Query);
extern float    S1ResultDetailsIdx(S1Result xres, S4 idx, MemSpec outMem, S1Scores outScores);
extern int      S1ResultXml(S1Result xres, PXml px, int mode);
extern void     S1ResultFree(S1Result);

extern int RankTuneSortGet(DomId domid, S1SortKey sort, S1Scores blend);
extern int RankTuneSortSet(DomId domid, S1SortKey sort, S1Scores blend);