// phash.c
// pnelson 11/15/2005
// Core hash structure to manage object lookups

#include "peng.h"

#define PKGNAME PkgName_PHash

typedef struct _PHash {
	PkgName  pkg;
	S4       allocSize;
	S4       maxHashIdx;
	PHashKeyToIdxFnc *keyToIdxFnc;
	PHashKeyCmpFnc   *keyCmpFnc;
	PHashObj *buckets;
} PHashRec;

PHash PHashNew(PkgName pkg, US4 maxHashIdx, 
							 PHashKeyToIdxFnc *keyToIdxFnc, 
							 PHashKeyCmpFnc   *keyCmpFnc)
{
	US4 alignSize = StructSizeAlign(PHashRec);
	US4 totSize   = alignSize + maxHashIdx*sizeof(PHashObj);

	US1 *data      = PEngAlloc(pkg, totSize);
	PHash phash    = (PHash)data;
	phash->buckets = (PHashObj*)(data + alignSize);
	phash->pkg        = pkg;
	phash->allocSize  = totSize;
	phash->maxHashIdx = maxHashIdx;
	phash->keyToIdxFnc = keyToIdxFnc;
	phash->keyCmpFnc   = keyCmpFnc;
	return phash;
}

void PHashFree(PHash phash)
{
	if (phash) {
		PEngFree(phash->pkg, phash, phash->allocSize);
	}
}

PHashObj PHashFind(PHash phash, PHashKey xkey)
{
	PHashIdx idx;
	PHashObj scan;

	if (!phash || !xkey) return NULL;
	idx = phash->keyToIdxFnc(xkey);
	for (scan = phash->buckets[idx]; scan; scan = scan->hashNext) {
		int cmp = phash->keyCmpFnc(xkey, scan);
		if (cmp < 0)
			continue;
		if (cmp == 0)
			return scan;
		if (cmp > 0)
			break;
	}
	return NULL;
}

PHashObj PHashPush(PHash phash, PHashKey xkey, PHashObj xobj)
{
	PHashObj *root, scan;
	PHashIdx idx  = phash->keyToIdxFnc(xkey);

	for (root = &(phash->buckets[idx]); (scan = *root); root = &(scan->hashNext)) {
		int cmp = phash->keyCmpFnc(xkey, scan);
		if (cmp >= 0) break;
	}
	if (scan == xobj) {
		PEngError(PKGNAME, GsvPkgNameAsStr(phash->pkg), "Push, Duplicate");
		return NULL;
	}
	xobj->hashNext = scan;
	/* Plug it in to net, we know this is safe because only one writer */
	*root = xobj;
	return xobj;
}

/* xobj can be null 
 */
PHashObj PHashPop(PHash phash, PHashKey xkey, PHashObj xobj)
{
  PHashIdx idx  = phash->keyToIdxFnc(xkey);
  PHashObj *root, scan;

  for (root = &(phash->buckets[idx]); (scan = *root); root = &(scan->hashNext)) {
	if (xobj) {  // Testing for actual object
	  if (scan == xobj) 
		goto cutit;
	} else {    // Testing by matching key
	  int cmp = phash->keyCmpFnc(xkey, scan);
	  if (cmp == 0) 
		goto cutit;
	  if (cmp > 0)
		break;
	}
  }
  return (PHashObj)NULL;

 cutit:
  // Cut it out of table, safe because only one writer, caller must expire it
  *root = scan->hashNext; 
  return scan;
}

#define MAXHISTO 256
int PHashXml(PXml px, PHash phash, US4 sequence, int mode)
{
  int err;
  S4 i, j, tooLong, lenLong, histo[MAXHISTO];
  char buf[32]; 
  
  if (!phash) 
	return -1;
  
  StructNull(histo);
  tooLong = lenLong = 0;
  for (i = 0; i < phash->maxHashIdx; i++) {
	PHashObj scan = phash->buckets[i];
	for (j=0; scan; scan = scan->hashNext) j++;
	if (j < MAXHISTO) {
	  histo[j] += 1; 
	} else {
	  tooLong += 1;
	  if (j > lenLong) lenLong = j;
	}
  }
  
  snprintf(buf, sizeof(buf), "phash name='%s'", GsvPkgNameAsStr(phash->pkg));
  if ((err = PXmlPush(px, buf, sequence)) < 0)
	return err;
  
  err |= PXmlSetNum(px, "maxhash", phash->maxHashIdx);
  err |= PXmlSetNum(px, "longlists",   tooLong);
  err |= PXmlSetNum(px, "longlongest", lenLong);
  
  {
	err |= PXmlPush(px, "histochains", MAXHISTO);
	for (i = 0; i < MAXHISTO; i++) {
	  if (histo[i]) {
		snprintf(buf, sizeof(buf), "chain len='%ld' cnt='%ld'", i, histo[i]);
		err |= PXmlPush0(px, buf, 0);
	  }
	}
	err |= PXmlPop(px);
  }
  err |= PXmlPop(px);
  return err;
}

int PHashCheck(PHash px, PHashCheckFnc cFnc, E1CheckErr err)
{
	S4 idx; PHashObj obj;
	if (px) {
		for (idx=0; idx < px->maxHashIdx; idx++)
			for (obj = px->buckets[idx]; obj; obj = obj->hashNext)
				if (err->errCode = (*cFnc)(obj, err, 0))
					return err->errCode;
	}
	return 0;
}
