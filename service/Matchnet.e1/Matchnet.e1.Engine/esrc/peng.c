// peng.c
// pnelson 11/15/2005
// Utilities for the engine

#include <malloc.h>
#include <time.h>
#include "peng.h"

#define PKGNAME PkgName_Eng

// Define the global state holder
static GsvRec gsvRec;
Gsv gsv = &gsvRec;

static void gsvPkgNameInit()
{
  gsv->pkgNames[0] = "PkgUnknown";
  gsv->pkgNames[PkgName_Eng]   = "PkgEng";
  gsv->pkgNames[PkgName_Cmd]   = "PkgCmd";
  gsv->pkgNames[PkgName_PHash] = "PkgPHash";
  gsv->pkgNames[PkgName_Stx]   = "PkgStx";
  gsv->pkgNames[PkgName_PMap]  = "PkgPMap";
  gsv->pkgNames[PkgName_Tag]   = "PkgTag";
  gsv->pkgNames[PkgName_TagDb] = "PkgTagDb";
  gsv->pkgNames[PkgName_Grp]   = "PkgGrp";
  gsv->pkgNames[PkgName_Ldg]   = "PkgLdg";
  gsv->pkgNames[PkgName_Mem]   = "PkgMem";
  gsv->pkgNames[PkgName_PXml]  = "PkgPXml";
  gsv->pkgNames[PkgName_Rank]  = "PkgRank";
  gsv->pkgNames[PkgName_S1]    = "PkgS1Search";
  gsv->pkgNames[PkgName_PRes]  = "PkgPRes";
  gsv->pkgNames[PkgName_Order] = "PkgOrder";
  gsv->pkgNames[PkgName_Load]  = "PkgLoad";
}

typedef struct _GsvToday {
  MemDateTimeRec	currentTime;		// Current date/time as of last maintain() call
  US2				days;				// Number of days from beginning to today
  US4				mintues;			// Number of minutes from beginning to currentTime
} GsvTodayRec, *GsvToday;

static GsvTodayRec gsvToday;

/// <summary>Set the cache of today's date, returning 1 if date has changed (trigger for daily maintenance)</summary>
/// <returns>0 if day is same, 1 if day has changed</returns>
///
int GsvTodaySet()
{
  US4 oldDays = gsvToday.days;
  struct tm *xtm;
  time_t timer = 0;
  timer = time(&timer);
  xtm = localtime(&timer);
  gsvToday.currentTime.v.year   = xtm->tm_year;  // also based on 1900
  gsvToday.currentTime.v.month  = xtm->tm_mon+1;
  gsvToday.currentTime.v.day    = xtm->tm_mday;
  gsvToday.currentTime.v.hour   = xtm->tm_hour;
  gsvToday.currentTime.v.minute = xtm->tm_min;
  gsvToday.days = MemPackDateTimeAsDays(gsvToday.currentTime.pack);
  gsvToday.mintues = MemPackDateTimeAsMinutes(gsvToday.currentTime.pack);
  return (oldDays != gsvToday.days);
}

US2 GsvGetTodayPack(US2 *days)
{
	if (days) *days = gsvToday.days;
	return MemPackDateTimeAsDate(gsvToday.currentTime.pack);
}

US4 GsvGetMinutesPack(US4 *minutes)
{
	if (minutes) *minutes = gsvToday.mintues;
	return gsvToday.currentTime.pack;
}

void GsvInitialize()
{
  int fatal = 0;
  
  if (gsv->initialized)
	return;
  gsv->initialized = clock();
  gsv->dbgOutput   = stdout;
  
  (void)GsvTodaySet();

  // Get our config data
  E1CfgInitialize(NULL);

  // Initialize our packages
  gsvPkgNameInit();
  MemInitialize();
  LoadInitialize();

  if (PermitInitialize(0)) {
	PEngError(PKGNAME, "Permit Initialize", "");
	fatal = 1;
  }

  // Make sure compiler is packing DOB properly
  { 
	MemDateTime currentTime = &(gsvToday.currentTime); 
	US4 pack;

	// Look at sizes in debugger
	int sCscz = sizeof(MemCSCZMapRec);
	int sDate = sizeof(MemDateRec);
	int sVital = sizeof(MemVitalsRec);
	int sMem = sizeof(MemRec);
	int sGrp = sizeof(GrpRec);
	int sTag = sizeof(TagRec);
	int sLdg = sizeof(LdgRec);
	int sLdgK = sizeof(LdgKeyRec);

	pack = ((US2)currentTime->v.year << 25) | ((US2)currentTime->v.month << 21) | ((US2)currentTime->v.day << 16) | ((US2)currentTime->v.hour << 11) | ((US2)currentTime->v.minute);
	if (pack != currentTime->pack) {
	  PEngError(PKGNAME, "COMPILER ALIGNMENT", "Packing DOB");
	  fatal = 1;
	}
  }
  
  if (fatal)
	exit(fatal);
  
  PEngLog(PkgName_Eng, "\nStartup", NULL, 0);
}

STR GsvPkgNameAsStr(PkgName pkg)
{
  int ipkg = minmax((int)pkg, 0, kPkg_NameMax);
  return gsv->pkgNames[ipkg];
}

void GsvDestroy()
{
  if (gsv->initialized) {

  PEngLog(PkgName_Eng, "Shutdown", NULL, 0);

  PHashFree(gsv->mems);
  ObjMgrDestroy(gsv->memObjMgr);
  
  PHashFree(gsv->ldgs);
  ObjMgrDestroy(gsv->ldgObjMgr);
  
  PHashFree(gsv->grps);
  // Free all the grps ???
  
  PHashFree(gsv->tags);
  ObjMgrDestroy(gsv->tagObjMgr);
  StxDbFree(gsv->pkTags);
  
  StxDbFree(gsv->pkCountry);
  StxDbFree(gsv->pkState);
  StxDbFree(gsv->pkCity);
  StxDbFree(gsv->pkZip);
  
  gsv->initialized = 0;
  }
  // PMapDestroy(gsv->pmap)
}

/// <summary>Do periodic maintenance on the engine</summary>
/// <returns>0 for success</returns>
/// <param name="stats">Optional stats structure that will be filled in for the caller</param>
///
int GsvMaintain(GsvStats stats)
{
	int flip = GsvTodaySet(), expired;

	stats->debugCount = 1;

	// It's tomorrow, update all New and Active scores
	if (flip)
		MemMaintain();

	stats->debugCount = 2;

	// Expire memory if permit available  ??? Transmit this value in the stats
	expired = PermitMaintain();

	stats->debugCount = 3;

	if (stats)
		GsvStatsLoad(stats);

	stats->debugCount = 4;

	return 0;
}

int GsvStatsLoad(GsvStats stats)
{
	int i;
	// Calculate stats
	if (stats) {
	  stats->todayYear		= gsvToday.currentTime.v.year  + kMemPack_YearMin;
	  stats->todayMonth		= gsvToday.currentTime.v.month;
	  stats->todayDay		= gsvToday.currentTime.v.day;
	  stats->todayHour		= gsvToday.currentTime.v.hour;
	  stats->todayMinute	= gsvToday.currentTime.v.minute;

	  stats->ldgs = ObjMgrSequence(gsv->ldgObjMgr);
	  stats->mems = ObjMgrSequence(gsv->memObjMgr);
	  stats->grps = SeqMgrSequence(gsv->grpSeqMgr);
	  stats->tags = ObjMgrSequence(gsv->tagObjMgr);
	  stats->stxs = -1;

	  stats->expires = gsv->statsExpires;
	  stats->upserts = gsv->statsUpserts;
	  stats->updates = gsv->statsUpdates;
	  stats->deletes = gsv->statsDeletes;

	  stats->searchNews = gsv->statsSearchNew;
	  stats->resultFrees = gsv->statsResultFree;

	  stats->memoryMem = gsv->pkgMemory[PkgName_Mem];
	  stats->memoryGrp = gsv->pkgMemory[PkgName_Grp];
	  stats->memoryAll = 0;
	  for (i=0; i<kPkg_NameMax; i++)
		if (gsv->pkgAllocs[i])
		  stats->memoryAll += gsv->pkgMemory[i];
	  stats->memoryAll -= (stats->memoryMem + stats->memoryGrp);

	  // This will gather search stats from all the permits
	  (void)PermitSearchStats(stats);
	}
	return 0;
}

/****************/

char *str0ncpy(char *dst, const char *src, int dstSize)
{
	if (!dst || dstSize <= 0) return dst;
	strncpy(dst, src, dstSize-1);
	dst[dstSize-1] = 0;
	return dst;
}

/****************/

static char *timeText(time_t timer, char *buf, int bufSize)
{
  int len;
  char *asc = ctime(&timer); // timeBuf ??? NOT THREAD SAFE
  str0ncpy(buf, asc, bufSize);
  if ((len = strlen(buf)) > 0)
	if (buf[len-1] == '\n') buf[len-1] = 0;
  return buf+4;  // Skip day of the week in time output
}

static char *timeFile(time_t timer, cSTR prefix, char *buf, int bufSize)
{
  struct tm *xtm;
  xtm = localtime(&timer); // timeBuf ??? NOT THREAD SAFE
  snprintf(buf, bufSize, "%s-%0.2d%0.2d%0.2d-%0.2d.log", 
		   prefix, xtm->tm_year-100, xtm->tm_mon+1, xtm->tm_mday, xtm->tm_hour);
  return buf;
}

/****/

void PEngError(PkgName pkg, cSTR obj, cSTR err)
{
  STR pstr = GsvPkgNameAsStr(pkg);
  if (!obj) obj = "~";
  if (!err) err = "!";
  snprintf(gsv->lastErr, sizeof(gsv->lastErr), "Error (%s) [%s]: %s", pstr, obj, err);
  gsv->lastErr[sizeof(gsv->lastErr)-1] = 0;
  fputs(gsv->lastErr, gsv->dbgOutput);
  PEngLog(pkg, gsv->lastErr, NULL, 0);
  // exit(1);
}

void PEngLog(PkgName pkg, cSTR fmt, cSTR strarg, S4 intarg)
{
  char msgBuf[2048], timeBuf[32], *timeTxt;
  int msgLen;
  FILE *out; 
  time_t timer = 0;

  static time_t lastReopen = 0;
  static time_t lastFlush = 0;

  timer = time(&timer);

  // reopen every ten minutes
  if (!gsv->logOutput || timer > (lastReopen+600)) {
	  char *fileName = timeFile(timer, "C:\\Matchnet\\Logs\\e1\\_peng", msgBuf, sizeof(msgBuf));
	if (gsv->logOutput) 
	  { fclose(gsv->logOutput); gsv->logOutput = NULL; }
 	gsv->logOutput = fopen(fileName, "a");
	lastReopen = timer;
  }

  if (!fmt)	fmt = (strarg ? "%s %d" : "%d");

  if (strarg) snprintf(msgBuf, sizeof(msgBuf), fmt, strarg, intarg);
  else        snprintf(msgBuf, sizeof(msgBuf), fmt, intarg);
  msgBuf[sizeof(msgBuf)-1] = 0;
  if ((msgLen = strlen(msgBuf)) > 0)
	if (msgBuf[msgLen-1] == '\n') msgBuf[msgLen-1] = 0;
 
  out = (gsv->logOutput ? gsv->logOutput : stderr);

  timeTxt = timeText(timer, timeBuf, sizeof(timeBuf));
  fprintf(out, "%s %s: %s\n", timeTxt, GsvPkgNameAsStr(pkg), msgBuf);
  if (timer >= lastFlush+10)
	{ fflush(out); lastFlush = timer; }
}

void PEngLoadError(PkgName pkg, cSTR file, S4 lineNum, MemId memid, cSTR errName, cSTR data)
{
  time_t timer = 0;
  static time_t lastReopen = 0;

  timer = time(&timer);

  // reopen every ten minues
  if (!gsv->loadOutput || timer > (lastReopen+600)) {
	char fileBuf[128];
	char *fileName = timeFile(timer, "C:\\Matchnet\\Logs\\e1\\_load", fileBuf, sizeof(fileBuf));
	if (gsv->loadOutput) 
	  { fclose(gsv->loadOutput); gsv->loadOutput = NULL; }
 	gsv->loadOutput = fopen(fileName, "a");
	lastReopen = timer;
  }
  if (gsv->loadOutput) {
	cSTR fname = (file ? file : "");
	int  flen  = strlen(fname);
	if (flen > 24) fname += (flen-24);
	fprintf(gsv->loadOutput, "%s:%ld err:%s memid:%ld csv:%s\n", 
			fname, lineNum, (errName ? errName : "Invalid"), memid, data);
  }
}

int E1ErrorConsistency(cSTR msg)
{
	// This is only called if there is an inconsistency in the Ldg/Mem/Grp/Tag mesh
	// Meant as convenient debugger stop, just push an error
	PEngError(PkgName_Eng, msg, NULL);
	return -1;
}

int E1ErrorPermit(int err)
{
	// Only called if there is some error w/ permits 
	// Meant as convenient debugger stop
	return err;
}

/****************/

#ifdef PENGDEBUG
static PkgName debugPkg = 0;
#endif

void *PEngAlloc(PkgName pkg, US4 size)
{
	int ipkg = minmax((int)pkg, 0, kPkg_NameMax);
	void *ptr = calloc(size, 1);
#ifdef PENGDEBUG
	if (debugPkg == pkg)
		debugPkg = pkg;  // Pt to stop in debugger
#endif
	if (!ptr) {
		char msgBuf[64];
		snprintf(msgBuf, sizeof(msgBuf), "Allocating %ld bytes", size);
		PEngError(pkg, NULL, msgBuf);
	}
	E1InterlockedInc(&(gsv->pkgAllocs[ipkg]));
	gsv->pkgMemory[ipkg] += size;
	return ptr;
}

void PEngFree(PkgName pkg, void *ptr, US4 size)
{
#ifdef PENGDEBUG
	if (debugPkg == pkg)
		debugPkg = pkg;  // Pt to stop in debugger
#endif
	if (size > 0) {
		int ipkg = minmax((int)pkg, 0, kPkg_NameMax-1);
		E1InterlockedDec(&(gsv->pkgAllocs[ipkg]));
		gsv->pkgMemory[ipkg] -= size;
	}
	if (ptr)
		free(ptr);
}

/****************/

/// <summary>Removes duplicate and NULL pointers from a list</summary>
/// <returns>The number of non-NULL, non-duplicated ptrs in the modified list</returns>
/// <param name="ptrs">the list of pointers to check, may be modified if there are dups or NULLs</param>
/// <param name="cnt">the original count of ptrs</param>
///
int e1UtlTrimDups(void **ptrs, int cnt)
{
  int i, j;
  // (Inefficiently) Double scan the list null'ing out values that duplicate something earlier
  for (i=0; i<cnt-1; i++)
	if (ptrs[i])
	  for (j=i+1; j<cnt; j++)
		if (ptrs[i] == ptrs[j])
		  ptrs[j] = NULL;

  // Now walk the list squeezing out the nulls 
  for (i=j=0; i < cnt; i++)
	if (ptrs[i])
	  ptrs[j++] = ptrs[i];

  // return updated count;
  return j;
}

/****************/

int GsvStatsXml(PXml px, GsvStats stats)
{
	char msgBuf[256];

 // Push the root node
  snprintf(msgBuf, sizeof(msgBuf), "stats today='%d-%0.2d-%0.2d %0.2d:%0.2d'", 
		   stats->todayYear, stats->todayMonth, stats->todayDay, stats->todayHour, stats->todayMinute);
  PXmlPush(px, msgBuf, 0);

  snprintf(msgBuf, sizeof(msgBuf), "counts ldgs='%d' mems='%d' grps='%d' tags='%d' stxs='%d'",
		   stats->ldgs, stats->mems, stats->grps, stats->tags, stats->stxs);
  PXmlPush0(px, msgBuf, 0);

  snprintf(msgBuf, sizeof(msgBuf), "members upserts='%d' updates='%d' deletes='%d'",
		   stats->upserts, stats->updates, stats->deletes);
  PXmlPush0(px, msgBuf, 0);

  snprintf(msgBuf, sizeof(msgBuf), "search queries='%d' scansHi='0x%X' scansLo='0x%X' avgtick='%d' maxthread='%d' searchNew='%d' resultFree='%d'",
		   stats->queries, stats->scansHi, stats->scansLo, stats->ticks, stats->threads, stats->searchNews, stats->resultFrees);
  PXmlPush0(px, msgBuf, 0);

  snprintf(msgBuf, sizeof(msgBuf), "memory mem='%d' grp='%d' other='%d' expire='%d'",
		   stats->memoryMem, stats->memoryGrp, stats->memoryAll, stats->expires);
  PXmlPush0(px, msgBuf, 0);

  PXmlPop(px);
  return 0;
}

int GsvXml(PXml px, int mode)
{
  int i, err;
  GsvStatsRec stats;

  S4 tagSeq = ObjMgrSequence(gsv->tagObjMgr);
  S4 ldgSeq = ObjMgrSequence(gsv->ldgObjMgr);
  S4 grpSeq = SeqMgrSequence(gsv->grpSeqMgr);
  S4 memSeq = ObjMgrSequence(gsv->memObjMgr);
  
  if ((err = PXmlPush(px, "Gsv", 0)) < 0)
	return err;
  
  StructNull(stats);
  GsvStatsLoad(&stats);
  err |= GsvStatsXml(px, &stats);

  err |= PXmlPush(px, "Sequences", 0);
  {
	err |= PXmlSetNum(px, "tagSequence", tagSeq);
	err |= PXmlSetNum(px, "grpSequence", grpSeq);
	err |= PXmlSetNum(px, "ldgSequence", ldgSeq);
	err |= PXmlSetNum(px, "memSequence", memSeq);
  }
  err |= PXmlPop(px);
  
  err |= PXmlPush(px, "Stxs", 0);
  {
	err |= PXmlSetNum(px, "Stx name='tags'",    StxDbCount(gsv->pkTags));
	err |= PXmlSetNum(px, "Stx name='country'", StxDbCount(gsv->pkCountry));
	err |= PXmlSetNum(px, "Stx name='state'",   StxDbCount(gsv->pkState));
	err |= PXmlSetNum(px, "Stx name='city'",    StxDbCount(gsv->pkCity));
	err |= PXmlSetNum(px, "Stx name='zip'",     StxDbCount(gsv->pkZip));
  }
  err |= PXmlPop(px);
  
  err |= PXmlPush(px, "MemAlloced", 0);
  for (i=0; i<kPkg_NameMax; i++)
	if (gsv->pkgAllocs[i]) {
	  char msgBuf[64]; 
	  snprintf(msgBuf, sizeof(msgBuf), "Memory name='%s' num='%ld' size='%ld'", 
			   gsv->pkgNames[i], gsv->pkgAllocs[i], gsv->pkgMemory[i]);
	  err |= PXmlPush0(px, msgBuf, 0);
	}
  err |= PXmlPop(px);
  
  if ((mode & 1) && !err) {
	// Show stats on our STR<>ID tables
	err = PXmlPush(px, "StxDbStats", 0);
	if (!err) err |= StxDbXml(px, gsv->pkTags,    mode);
	if (!err) err |= StxDbXml(px, gsv->pkCountry, mode);
	if (!err) err |= StxDbXml(px, gsv->pkState,   mode);
	if (!err) err |= StxDbXml(px, gsv->pkCity,    mode);
	if (!err) err |= StxDbXml(px, gsv->pkZip,     mode);
	PXmlPop(px);
  }
  
  if ((mode & 2) && !err) {
	// Show stats on our Hash tables
	err = PXmlPush(px, "PHashStats", 0);
	if (!err) err = PHashXml(px, gsv->tags, tagSeq, 0);
	if (!err) err = PHashXml(px, gsv->grps, grpSeq, 0);
	if (!err) err = PHashXml(px, gsv->ldgs, ldgSeq, 0);
	if (!err) err = PHashXml(px, gsv->mems, memSeq, 0);
	if (!err) err = GrpPkgXml(px, mode);
	PXmlPop(px);
  }
  
  if ((mode & 4) && !err) {
	// Show stats on our Object Managers
	err = PXmlPush(px, "ObjManagers", 0);
	if (!err) err |= ObjMgrXml(px, gsv->tagObjMgr, 0);
	if (!err) err |= SeqMgrXml(px, gsv->grpSeqMgr, 0);
	if (!err) err |= ObjMgrXml(px, gsv->ldgObjMgr, 0);
	if (!err) err |= ObjMgrXml(px, gsv->memObjMgr, 0);
	PXmlPop(px);
  }
  
  err |= PXmlPop(px);
  return err;
}

long E1atol(cSTR value)
{
	return value ? atol(value) : 0;
}
