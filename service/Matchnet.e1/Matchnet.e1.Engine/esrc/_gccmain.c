// C command line console
//

// Only use this when compiling a command console with GCC
#ifdef GCC

int main(int argc, char **argv)
{
  extern int e1main(int argc, char **argv);
  return e1main(argc, argv);
}

#endif
