// ldg.c
// pnelson 11/15/2005
// Organize members in chains of same Location/Domain/Gender

#include "stdlib.h"
#include "peng.h"

#define PKGNAME PkgName_Ldg

// Generate a distributed index from a location/domain/gender key
static PHashIdx xLdgKeyToIdx(LdgKey lk)
{
  US4 idx = (lk->loc.lng*997 + lk->loc.lat*7) ^ (5*(US4)lk->domId) ^ lk->gender;
  idx %= kLdg_HashSize;
  return (PHashIdx)idx;
}

// When ordering Ldgs, favor lower lng, then lat, then gender, then domain
static int xLdgKeyCmp(LdgKey lk, Ldg xldg)
{
	LdgKey xk = &(xldg->ldgKey);
	if (lk->loc.lng != xk->loc.lng)
		return (lk->loc.lng < xk->loc.lng ? -1 : 1);
	if (lk->loc.lat != xk->loc.lat)
		return (lk->loc.lat < xk->loc.lat ? -1 : 1);
	if (lk->gender != xk->gender)
		return (lk->gender < xk->gender ? -1 : 1);
	if (lk->domId != xk->domId)
		return (lk->domId < xk->domId ? -1 : 1);
	return 0;
}
	
/****************/

static void xLdgInitialize()
{
	if (!gsv->ldgs) {
		gsv->ldgs = PHashNew(PKGNAME, kLdg_HashSize, 
							         (PHashKeyToIdxFnc*)xLdgKeyToIdx, 
							         (PHashKeyCmpFnc  *)xLdgKeyCmp);
		gsv->ldgObjMgr = ObjMgrCreate(PKGNAME, sizeof(LdgRec), kLdg_MaxLdgs);
		if (!gsv->ldgs) 
			PEngError(PKGNAME, NULL, "Init");
	}
}

/****************/

S4 LdgSequence(Ldg xldg)
{
	return ObjMgrPtrToSeq(gsv->ldgObjMgr, (ObjPtr)xldg);
}

Ldg LdgBySequence(S4 seq)
{
	return (Ldg)ObjMgrSeqToPtr(gsv->ldgObjMgr, seq);
}

Ldg LdgFind(LdgKey lk)
{
	if (!gsv->ldgs) xLdgInitialize();
	if (!lk) return NULL;
	return (Ldg)PHashFind(gsv->ldgs, (PHashKey)lk);
}

Ldg LdgFindOrNew(LdgKey lk)
{
	Ldg xldg;
	if (!gsv->ldgs) xLdgInitialize();
	if (!lk) return NULL;
	if ((xldg = (Ldg)PHashFind(gsv->ldgs, (PHashKey)lk)))
		return xldg;
	xldg = (Ldg)ObjMgrNew(gsv->ldgObjMgr, NULL);
	xldg->ldgKey = *lk;
	return (Ldg)PHashPush(gsv->ldgs, (PHashKey)&(xldg->ldgKey), (PHashObj)xldg);
}

LdgKey LdgKeyGet(Ldg xldg)
{
	if (!xldg) return 0;
	return &(xldg->ldgKey);
}

Mem *LdgChainMemRoot(Ldg xldg)
{
	if (!xldg) return 0;
	return &(xldg->memChain);
}

// ****************************************************************

int LdgXml(PXml px, Ldg xldg, int mode)
{
  char msgBuf[128];
  int err;
  S4 seq = LdgSequence(xldg);
  LdgKey lk;

  lk = &(xldg->ldgKey);

  snprintf(msgBuf, sizeof(msgBuf), "ldg seq='%ld' domid='%d' gender='%d' lng='%d' lat='%d' memcnt='%ld'",
		   seq, lk->domId, lk->gender, lk->loc.lng, lk->loc.lat, xldg->memCount);
  if (mode == 0) { 
	err = PXmlPush0(px, msgBuf, 0);
  } else {
	err = PXmlPush(px, msgBuf, 0);
	err |= MemXmlLdgChain(px, xldg->memChain, 0);
	err |= PXmlPop(px);
  }
  return err;
}
