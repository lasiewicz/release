// C++ interface to overall engine state
//

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "e1cpp.h"

using namespace Matchnet::e1::Engine;

extern "C" { 
	extern int e1main(int argc, char **argv);
}

int E1Gsv::main(int argc, char **argv)
{
	return e1main(argc, argv);
}
