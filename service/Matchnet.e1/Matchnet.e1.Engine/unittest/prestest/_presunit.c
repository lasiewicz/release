#include "peng.h"

int PResTestMain(int argc, char *argv[])
{
  int i, j;
  PRes xres;
  S1ResultRec sres;

  GsvInitialize();
  xres = PResNew(500);
  StructNull(sres);

#if 0
  for (i=0; i < 50000; i++) {
	PResInsert(xres, 100000+i, (US2)i);
	if ((i % 1000) == 0)
	  i = i;
  }
#endif
  for (i=0; i < 25000; i++) {
	j = 50000-i;
	PResInsert(xres, 100000+i, (US2)i);
	PResInsert(xres, 100000+j, (US2)j);
	if ((i % 1000) == 0)
	  i = i;
  }

  PResLoad(xres, &sres);

  for (i = 0; i < sres.memNum; i++) {
	printf(" %d:[%d,%x]", i, sres.memids[i], sres.scores[i]);
	if ((i % 10) == 9) 
	  puts("\n");
  }
  puts("\n");

 return 0;
}

