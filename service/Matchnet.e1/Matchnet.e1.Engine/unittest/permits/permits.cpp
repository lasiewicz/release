// permits.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

extern "C" {
typedef struct _PermitSearch *PermitSearch;
extern int  PermitInitialize(int count);
extern void PermitDestroy();
extern int PermitSearchTake(PermitSearch *permit);
extern int PermitSearchGive(PermitSearch permit);
extern int PermitSearchBeg(PermitSearch permit);
extern int PermitSearchEnd(PermitSearch permit);
}

int _tmain(int argc, _TCHAR* argv[])
{
	PermitSearch p0, p1, p2, p3 , p4;
	int ex, e0, e1, e2, e3, e4;
	
	p0 = p1 = p2 = p3 = p4 = NULL;
	e0 = e1 = e2 = e3 = e4 = 0;

	ex = PermitInitialize(4);
	e0 = PermitSearchTake(&p0);
	PermitSearchBeg(p0);

	e1 = PermitSearchTake(&p1);
	PermitSearchBeg(p1);

	e2 = PermitSearchTake(&p2);
	e3 = PermitSearchTake(&p3);
	e4 = PermitSearchTake(&p4);

	PermitSearchEnd(p1);
	PermitSearchEnd(p0);
	ex = PermitSearchEnd(p1);

	PermitDestroy();
	return 0;
}

