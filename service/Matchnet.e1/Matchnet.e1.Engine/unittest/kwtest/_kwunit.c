#include "kw.h"
#include "stdio.h"
#include "string.h"

static char *testWords[] = {
"null", "memid", "memberid", "domid", "domainid", "gender", "gendermask", "blat", "boxlat", 
"blng", "boxlng", "flat", "fltlat", "latitude", "flng", "fltlng", "longitude", "distance", 
"country", "state", "city", "zip", "areacode", "birthdate", "activedate", "registerdate", 
"n", "name", "v", "value", "c", "code", "tag", "tagdb", "coding", "noauto", "cmd", "help", 
"quit", "time", "show", "type", "load", "file", "gsv", "mem", "grp", "ldg", "seq", "mode", 
"update", "search", "resmax", "sortkey", "hasphoto", "isonline", "saact", "sapop", 
"age", "agemax", "agemin", "maxage", "minage", "hgt", "height", "hgtmax", "hgtmin", 
"maxhgt", "minhgt", "heightmax", "heightmin", "maxheight", "minheight", "wgt", "weight", 
"wgtmax", "wgtmin", "maxwgt", "minwgt", "weightmax", "weightmin", "maxweight", "minweight", 
"appearanceimportance", "bodyart", "bodytype", "childcnt", "childrencount", "cuisine", "custody", 
"desireddrinkinghabits", "desirededucationlevel", "desiredjdatereligion", "desiredmaritalstatus", 
"desiredsmokinghabits", "drinkinghabits", "educationlevel", "entertainmentlocation", 
"ethnicity", "eyecolor", "favoritetimeofday", "habitattype", 
"haircolor", "havechildren", "incomelevel", "industrytype", 
"intelligenceimportance", "israelareacode", "israelimmigrationdate", "israelipersonalitytrait", 
"isrealimmigrationyear", "jdateethnicity", "jdatereligion", "keepkosher", 
"languagemask", "leisureactivity", "majortype", "maritalstatus", 
"militaryservicetype", "morechildrenflag", "moviegenremask", "music", 
"personalitytrait", "pets", "physicalactivity", "planets", 
"politicalorientation", "reading", "relmask", "relationshipmask", 
"relstatus", "relationshipstatus", "religion", "religionactivitylevel", 
"relocate", "relocateflag", "synagogueattendance", "smokinghabits", 
"zodiac", "four", "score", "and", "seven" "years", "ago",
NULL };


static void show(const char *tw, const E1KwRec *kw)
{
	printf("%0.2d[%s:%s]\n", 
			(kw ? (int)kw->kw : -1), tw,
			(kw ? kw->name : "NULL"));
}


int KWTestMain(int argc, char **argv)
{
  int i, len;
  char buf[128], *tw;
  const E1KwRec * kw;

  for (i=0; testWords[i]; i++) {
	tw  = testWords[i];
	len = (int)strlen(tw);
	kw  = KWLookupAux(tw, len);
	show(tw, kw);
  }
  while (gets_s(buf, sizeof(buf))) {
	len = (int)strlen(buf);
	if (len > 0 && buf[len-1] == '\n') 
	  buf[--len] = 0;
	if (len <= 0)
	  break;
	strlwr(buf);
	kw = KWLookupAux(buf, len);
	show(buf, kw); 
  }
  return 0;
}



  
