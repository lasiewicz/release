// kwtest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

extern "C" { 
extern int KWTestMain(int argc, char **argv);
}

int _tmain(int argc, char* argv[])
{
	KWTestMain(argc, argv);
}

