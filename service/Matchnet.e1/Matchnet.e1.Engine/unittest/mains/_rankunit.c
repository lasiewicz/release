
#include "peng.h"

static struct { US1 amin, amax; } ageTests[] = {
  { 20, 30 },
  { 30, 40 },
  { 40, 50 },
  { 20, 37 },
  { 35, 60 },
  { 20, 80 },
  { 35, 35 },
  { 35, 36 },
  { 35, 37 },
  { 35, 38 },
  { 30, 0 },
  { 0, 38 },
  { 40, 30 },
  { 0, 0 }
};

#define AMIN 18
#define AMAX 85

S4 MemPackDateAsDays(US2 pack) { return 0; }

main()
{
  char msgBuf[1024]; 
  int i, j, msgLen;
  US1 byAge[kRank_AgeVctMax];

  strcpy(msgBuf, "AgeMin,AgeMax");
  msgLen = strlen(msgBuf);
  for (j=AMIN; j < AMAX; j++) {
	sprintf(msgBuf+msgLen, ",x%d", j);
	msgLen += strlen(msgBuf+msgLen);
  }
  puts(msgBuf);

  for (i = 0; i < ArrayCount(ageTests); i++) {
	US1 amin = ageTests[i].amin;
	US1 amax = ageTests[i].amax;
	RankAgeInit(byAge, amin, amax, gsvRank.age);

	sprintf(msgBuf, "%d,%d", amin, amax);
	msgLen = strlen(msgBuf);
	for (j=AMIN; j < AMAX; j++) {
	  sprintf(msgBuf+msgLen, ",%d", byAge[j]);
	  msgLen += strlen(msgBuf+msgLen);
	}
	puts(msgBuf);
  }
}
