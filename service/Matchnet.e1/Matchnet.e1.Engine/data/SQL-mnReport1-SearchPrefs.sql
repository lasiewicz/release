select
r.Longitude as fltLng, 
r.Latitude  as fltLat, 
s.MemberId  as memId, 
s.Gendermask as genderMask, 
s.minage    as ageMin, 
s.maxage    as ageMax, 
s.minHeight as hgtMin,
s.maxHeight as hgtMax,
s.regionid  as City, 
s.countryregionid  as Country,
s.areacode1 as AreaCode,
s.distance  as Distance,
s.hasPhotoFlag as hasPhoto,
s.Educationlevel,
s.Religion,
s.LanguageMask,
s.Ethnicity,
s.SmokingHabits,
s.DrinkingHabits,
s.maritalstatus,
s.jdatereligion,
s.jdateethnicity,
s.synagogueattendance,
s.keepkosher,
s.relationshipmask,
s.relationshipstatus,
s.bodytype,
s.zodiac,
s.majortype,
s.searchorderby,
s.searchtypeid
from dbo.CurrentSearchPreference s (nolock)
left join RegionFlat r (nolock) on s.RegionID = r.Depth4RegionID
where	Domainid > 0 
and Longitude is not null
and distance is not null
and memberid is not null
and minage is not null
and regionid is not null
