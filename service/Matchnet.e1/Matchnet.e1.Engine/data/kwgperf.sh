# pnelson 12/16/2005
# tr is to remove cariage returns
# gperf options are:
#   -N KWLookupAux  override name of auto-generated lookup function
#   -a  generate ANSI standard C code
#   -C  make contents of all lookup tables "const"
#   -G  generate keyword list as static global
#   -t  use the E1Kw struct rather then char * for each KW defintion
#   -p  return the ptr ot the struct on lookup rather then yes/no
#   -T  do not generate type declaration -- that is handled manually in kw.h
#   --switch=1  generate code as big switch statement -- faster then table
#   -k * test all character positions in the word to generate best hash function
#   -r  random seeding helps gperf find best solution
#   -D  handle cases where multiple probes necessary to differenatiate similar words
#
#
cat ./data/kw.src | tr -d '\015' | gperf -a -C -G -t -T -N KWLookupAux -r -p --switch=1 -k '*' -D > ./esrc/kw.c

