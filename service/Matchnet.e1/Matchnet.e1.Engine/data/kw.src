%{
// kw.src & kw.c == use gperf to quickly match E1 keywords
// Build kw.c from kw.src with the following:
//    cat kw.src | tr -d '\015' | gperf -a -C -G -t -T -N KWLookupAux -r -p --switch=1 -k '*' -D > kw.c
// Use _kwunit.c to test 

#include "string.h"
#ifndef strlwr
#define strlwr   _strlwr
#endif
#include "kw.h"

%}
E1KwRec
%%
null,                           KW_null
# Member Keys
memid,                          KW_memid
memberid,                       KW_memid
domid,                          KW_domid
domainid,                       KW_domid
communityid,                    KW_domid
gender,                         KW_gender
gendermask,                     KW_gendermask
# Location and distance
blat,                           KW_boxlat
boxlat,                         KW_boxlat
blng,                           KW_boxlng
boxlng,                         KW_boxlng
flat,                           KW_fltlat
fltlat,                         KW_fltlat
reglat,                         KW_fltlat
latitude,                       KW_fltlat
flng,                           KW_fltlng
fltlng,                         KW_fltlng
reglng,                         KW_fltlng
longitude,                      KW_fltlng
distance,                       KW_distance
country,                        KW_country
depth1regionid,                 KW_country
state,                          KW_state
depth2regionid,                 KW_state
city,                           KW_city
depth3regionid,                 KW_city
zip,                            KW_zip
depth4regionid,                 KW_zip
areacode,                       KW_areacode
# Dates
birthdate,                      KW_birthdate
activedate,                     KW_activedate
lastactivedate,                 KW_activedate
registerdate,                   KW_registerdate
communityinsertdate,            KW_registerdate
updatedate,                     KW_updatedate
# Names, Values, Tags
n,                              KW_name
name,                           KW_name
v,                              KW_value
value,                          KW_value
c,                              KW_code
code,                           KW_code
tag,                            KW_tag
tagdb,                          KW_tagdb
coding,                         KW_coding
noauto,                         KW_noauto
# URL commands
cmd,                            KW_cmd
help,                           KW_help
quit,                           KW_quit
time,                           KW_time
reset,                          KW_reset
maintain,                       KW_maintain
check,                          KW_check
show,                           KW_show
type,                           KW_type
load,                           KW_load
file,                           KW_file
header,							KW_header
gsv,                            KW_gsv
mem,                            KW_mem
grp,                            KW_grp
ldg,                            KW_ldg
seq,                            KW_seq
mode,                           KW_mode
xsl,                            KW_xsl
update,                         KW_update
delete,                         KW_delete
search,                         KW_search
resmax,                         KW_resmax
sortkey,                        KW_sortkey
# Flags, dynamic attributes
hasphoto,                       KW_hasphoto
hasphotoflag,                   KW_hasphoto
isonline,                       KW_isonline
saact,                          KW_saact
sapop,                          KW_sapop
emailcount,                     KW_emailcount
# Age, Height, Weight
age,                            KW_age
agemax,                         KW_agemax
agemin,                         KW_agemin
maxage,                         KW_agemax
minage,                         KW_agemin
hgt,                            KW_hgt
height,                         KW_hgt
hgtmax,                         KW_hgtmax
hgtmin,                         KW_hgtmin
maxhgt,                         KW_hgtmax
minhgt,                         KW_hgtmin
heightmax,                      KW_hgtmax
heightmin,                      KW_hgtmin
maxheight,                      KW_hgtmax
minheight,                      KW_hgtmin
wgt,                            KW_wgt
weight,                         KW_wgt
wgtmax,                         KW_wgtmax
wgtmin,                         KW_wgtmin
maxwgt,                         KW_wgtmax
minwgt,                         KW_wgtmin
weightmax,                      KW_wgtmax
weightmin,                      KW_wgtmin
maxweight,                      KW_wgtmax
minweight,                      KW_wgtmin
# Other attributes
appearanceimportance,           KW_appearanceimportance
bodyart,                        KW_bodyart
bodytype,                       KW_bodytype
childcnt,                       KW_childcnt
childrencount,                  KW_childcnt
cuisine,                        KW_cuisine
custody,                        KW_custody
desireddrinkinghabits,          KW_desireddrinkinghabits
desirededucationlevel,          KW_desirededucationlevel
desiredjdatereligion,           KW_desiredjdatereligion
desiredmaritalstatus,           KW_desiredmaritalstatus
desiredsmokinghabits,           KW_desiredsmokinghabits
drinkinghabits,                 KW_drinkinghabits
educationlevel,                 KW_educationlevel
entertainmentlocation,          KW_entertainmentlocation
ethnicity,                      KW_ethnicity
eyecolor,                       KW_eyecolor
favoritetimeofday,              KW_favoritetimeofday
habitattype,                    KW_habitattype
haircolor,                      KW_haircolor
havechildren,                   KW_havechildren
incomelevel,                    KW_incomelevel
industrytype,                   KW_industrytype
intelligenceimportance,         KW_intelligenceimportance
israelareacode,                 KW_israelareacode
israelimmigrationdate,          KW_israelimmigrationdate
israelipersonalitytrait,        KW_israelipersonalitytrait
isrealimmigrationyear,          KW_isrealimmigrationyear
jdateethnicity,                 KW_jdateethnicity
jdatereligion,                  KW_jdatereligion
keepkosher,                     KW_keepkosher
languagemask,                   KW_languagemask
leisureactivity,                KW_leisureactivity
majortype,                      KW_majortype
maritalstatus,                  KW_maritalstatus
militaryservicetype,            KW_militaryservicetype
morechildrenflag,               KW_morechildrenflag
moviegenremask,                 KW_moviegenremask
music,                          KW_music
personalitytrait,               KW_personalitytrait
pets,                           KW_pets
physicalactivity,               KW_physicalactivity
planets,                        KW_planets
politicalorientation,           KW_politicalorientation
reading,                        KW_reading
relationshipmask,               KW_relationshipmask
relationshipstatus,             KW_relationshipstatus
religion,                       KW_religion
religionactivitylevel,          KW_religionactivitylevel
relocate,                       KW_relocate
relocateflag,                   KW_relocate
schoolid,                       KW_schoolid
synagogueattendance,            KW_synagogueattendance
smokinghabits,                  KW_smokinghabits
zodiac,                         KW_zodiac
# Tuning
ranktune,            KW_ranktune
def.sort,            KW_rt_defsort
def.max,             KW_rt_defmax
def.dist,            KW_rt_defdist
rt.act,              KW_rt_act
rt.new,              KW_rt_new
rt.pop,              KW_rt_pop
rt.ldg,              KW_rt_ldg
cscz.reg1,           KW_rt_cscz_reg1
cscz.reg2,           KW_rt_cscz_reg2
cscz.reg3,           KW_rt_cscz_reg3
cscz.reg4,           KW_rt_cscz_reg4
age.inmax,           KW_rt_age_inmax
age.inmin,           KW_rt_age_inmin
age.outmax,          KW_rt_age_outmax
age.algo,            KW_rt_age_algo
age.pct,             KW_rt_age_pct
age.peryr,           KW_rt_age_peryr
hgt.inmax,           KW_rt_hgt_inmax
hgt.inmin,           KW_rt_hgt_inmin
hgt.outmax,          KW_rt_hgt_outmax
hgt.percm,           KW_rt_hgt_percm
rtb.sort,            KW_rtb_sort
rtb.cscz,            KW_rtb_cscz
rtb.ldg,             KW_rtb_ldg
rtb.age,             KW_rtb_age
rtb.hgt,             KW_rtb_hgt
rtb.match,           KW_rtb_match
rtb.act,             KW_rtb_act
rtb.new,             KW_rtb_new
rtb.pop,             KW_rtb_pop
rtb.mol,             KW_rtb_mol
%%
const char *KWLookupStr(KWEnum key)
{
	int i, cnt = sizeof(wordlist)/sizeof(wordlist[0]);
	for (i=0; i < cnt; i++)
		if (wordlist[i].kw == key)
			return wordlist[i].name;
	return NULL;
}

#define kKWBufSiz 128

KWEnum KWLookupKey(const char *str)
{
    const E1KwRec * kw;
	char buf[kKWBufSiz+2]; 
	int len;

	if (!str || !*str)
		return KW_null;

	len = strlen(str);
	if (len >= kKWBufSiz)
	  len = kKWBufSiz;
	memcpy(buf, str, len);
	buf[len] = 0;
	strlwr(buf);

	kw = KWLookupAux(buf, len);
	if (kw)
	  return kw->kw;
	else
	  return KW_null;
}
