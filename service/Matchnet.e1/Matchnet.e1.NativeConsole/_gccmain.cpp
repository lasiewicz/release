#include "e1cpp.h"

using namespace Matchnet::e1::Engine;

int main(int argc, char **argv)
{
	E1Gsv e1("Console");
	int err = e1.main(argc, argv);
	return err;
}
