@ECHO ON
@ECHO ****************** Deploy to devapp01?
pause
psexec \\devapp01 net stop Matchnet.Search.Service
psexec \\devapp01 net stop FASTQuery
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Query.Service\ \\devapp01\c$\matchnet\bin\Service\Matchnet.FAST.Query.Service * /s /xo /r:999 /w:0

psexec \\devapp01 net start FASTQuery
psexec \\devapp01 net start Matchnet.Search.Service

@ECHO Deploy to devapp02?
pause
psexec \\devapp02 net stop Matchnet.Search.Service
psexec \\devapp02 net stop FASTQuery
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Query.Service\ \\devapp02\c$\matchnet\bin\Service\Matchnet.FAST.Query.Service * /s /xo /r:999 /w:0

psexec \\devapp02 net start FASTQuery
psexec \\devapp02 net start Matchnet.Search.Service


sc \\devapp01 query FASTQuery
sc \\devapp02 query FASTQuery
