using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Messaging;
using System.Threading;

using Matchnet.FAST.Push.BusinessLogic;
using Matchnet.FAST.Push.ValueObjects;
using Matchnet.FAST.Push.Interfaces;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.FAST.Push.TestHarness {
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class PushTestHarness : System.Windows.Forms.Form {

		private QueueConfiguration _QueueConf;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.TabControl tcMain;
		private System.Windows.Forms.TabPage tpQueue;
		private System.Windows.Forms.TabPage tpMetrics;
		private System.Windows.Forms.Button btnMetricsInstallCounters;
		private System.Windows.Forms.Button btnMetricsUninstallCounters;
		private System.Windows.Forms.Button btnQueueEnqueue;
		private System.Windows.Forms.NumericUpDown numQueueItterCount;
		private System.Windows.Forms.Button btnQueuePushSingle;
		private System.Windows.Forms.TextBox txtQueueMemberID;
		private System.Windows.Forms.TextBox txtQueueDomainID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabPage tpPush;
		private System.Windows.Forms.Button btnPushSingle;
		private System.Windows.Forms.TextBox txtPushMemberID;
		private System.Windows.Forms.NumericUpDown numPushDomainID;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnPushNBatches;
		private System.Windows.Forms.Button btnPushLoopStart;
		private System.Windows.Forms.Button btnPushLoopEnd;
		private System.Windows.Forms.NumericUpDown numPushItterCount;
		private System.Windows.Forms.TrackBar trkPushThreadCount;
		private System.Windows.Forms.Label lblPushThreadCount;
		private System.Windows.Forms.Button btnQueueRequeuErrors;
		private System.Windows.Forms.TabPage tpGeo;
		private System.Windows.Forms.Button btnGeoMakeBoxes;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtLatInput;
		private System.Windows.Forms.TextBox txtLngInput;
		private System.Windows.Forms.Button btnGeoGetRegion;
		private System.Windows.Forms.Button btnMetricsInstallQueues;
		private System.Windows.Forms.Button btnMetricsUninstallQueues;
		private System.Windows.Forms.Button btnDeleteSingle;
		private System.Windows.Forms.CheckBox chkQueueDeleteFlag;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btnShowThreadConfig;
		private System.Windows.Forms.TextBox txtBulkLoadConnectionString;
		private System.Windows.Forms.TextBox txtSqlStoredProc;

		private static int _BatchSize = Convert.ToInt32(RuntimeSettings.GetSetting("FASTPUSHSVC_BATCHSIZE"));
		private System.Windows.Forms.NumericUpDown numBatchSize;
		private System.Windows.Forms.TabPage tpErrorQueue;
		private System.Windows.Forms.TextBox txtRequeueErorrBatchSize;
		private System.Windows.Forms.Button btnErrorQueueShowContent;
		private System.Windows.Forms.TextBox txtErrorQueuePath;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button btnQueueInfinite;
		private static string _BulkLoadConnectionString = RuntimeSettings.GetSetting("FASTPUSHSVC_BULKIMPORT_CONNECTION_MEMBERBATCHES");

		public PushTestHarness() {
			_BatchSize = Convert.ToInt32(RuntimeSettings.GetSetting("FASTPUSHSVC_BATCHSIZE"));
			_BulkLoadConnectionString = RuntimeSettings.GetSetting("FASTPUSHSVC_BULKIMPORT_CONNECTION_MEMBERBATCHES");

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.tcMain = new System.Windows.Forms.TabControl();
			this.tpQueue = new System.Windows.Forms.TabPage();
			this.btnQueueInfinite = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.numBatchSize = new System.Windows.Forms.NumericUpDown();
			this.txtSqlStoredProc = new System.Windows.Forms.TextBox();
			this.txtBulkLoadConnectionString = new System.Windows.Forms.TextBox();
			this.chkQueueDeleteFlag = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtQueueDomainID = new System.Windows.Forms.TextBox();
			this.txtQueueMemberID = new System.Windows.Forms.TextBox();
			this.btnQueuePushSingle = new System.Windows.Forms.Button();
			this.numQueueItterCount = new System.Windows.Forms.NumericUpDown();
			this.btnQueueEnqueue = new System.Windows.Forms.Button();
			this.tpPush = new System.Windows.Forms.TabPage();
			this.btnDeleteSingle = new System.Windows.Forms.Button();
			this.lblPushThreadCount = new System.Windows.Forms.Label();
			this.trkPushThreadCount = new System.Windows.Forms.TrackBar();
			this.numPushItterCount = new System.Windows.Forms.NumericUpDown();
			this.btnPushLoopEnd = new System.Windows.Forms.Button();
			this.btnPushLoopStart = new System.Windows.Forms.Button();
			this.btnPushNBatches = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.numPushDomainID = new System.Windows.Forms.NumericUpDown();
			this.txtPushMemberID = new System.Windows.Forms.TextBox();
			this.btnPushSingle = new System.Windows.Forms.Button();
			this.tpMetrics = new System.Windows.Forms.TabPage();
			this.btnMetricsUninstallQueues = new System.Windows.Forms.Button();
			this.btnMetricsInstallQueues = new System.Windows.Forms.Button();
			this.btnMetricsUninstallCounters = new System.Windows.Forms.Button();
			this.btnMetricsInstallCounters = new System.Windows.Forms.Button();
			this.btnShowThreadConfig = new System.Windows.Forms.Button();
			this.tpGeo = new System.Windows.Forms.TabPage();
			this.btnGeoGetRegion = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtLngInput = new System.Windows.Forms.TextBox();
			this.txtLatInput = new System.Windows.Forms.TextBox();
			this.btnGeoMakeBoxes = new System.Windows.Forms.Button();
			this.tpErrorQueue = new System.Windows.Forms.TabPage();
			this.txtErrorQueuePath = new System.Windows.Forms.TextBox();
			this.btnErrorQueueShowContent = new System.Windows.Forms.Button();
			this.txtRequeueErorrBatchSize = new System.Windows.Forms.TextBox();
			this.btnQueueRequeuErrors = new System.Windows.Forms.Button();
			this.tcMain.SuspendLayout();
			this.tpQueue.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numBatchSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numQueueItterCount)).BeginInit();
			this.tpPush.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trkPushThreadCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPushItterCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPushDomainID)).BeginInit();
			this.tpMetrics.SuspendLayout();
			this.tpGeo.SuspendLayout();
			this.tpErrorQueue.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtOutput
			// 
			this.txtOutput.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtOutput.Location = new System.Drawing.Point(0, 165);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(704, 320);
			this.txtOutput.TabIndex = 0;
			this.txtOutput.Text = "";
			// 
			// tcMain
			// 
			this.tcMain.Controls.Add(this.tpQueue);
			this.tcMain.Controls.Add(this.tpPush);
			this.tcMain.Controls.Add(this.tpMetrics);
			this.tcMain.Controls.Add(this.tpGeo);
			this.tcMain.Controls.Add(this.tpErrorQueue);
			this.tcMain.Dock = System.Windows.Forms.DockStyle.Top;
			this.tcMain.Location = new System.Drawing.Point(0, 0);
			this.tcMain.Name = "tcMain";
			this.tcMain.SelectedIndex = 0;
			this.tcMain.Size = new System.Drawing.Size(704, 160);
			this.tcMain.TabIndex = 1;
			// 
			// tpQueue
			// 
			this.tpQueue.Controls.Add(this.btnQueueInfinite);
			this.tpQueue.Controls.Add(this.label5);
			this.tpQueue.Controls.Add(this.numBatchSize);
			this.tpQueue.Controls.Add(this.txtSqlStoredProc);
			this.tpQueue.Controls.Add(this.txtBulkLoadConnectionString);
			this.tpQueue.Controls.Add(this.chkQueueDeleteFlag);
			this.tpQueue.Controls.Add(this.label1);
			this.tpQueue.Controls.Add(this.txtQueueDomainID);
			this.tpQueue.Controls.Add(this.txtQueueMemberID);
			this.tpQueue.Controls.Add(this.btnQueuePushSingle);
			this.tpQueue.Controls.Add(this.numQueueItterCount);
			this.tpQueue.Controls.Add(this.btnQueueEnqueue);
			this.tpQueue.Location = new System.Drawing.Point(4, 22);
			this.tpQueue.Name = "tpQueue";
			this.tpQueue.Size = new System.Drawing.Size(696, 134);
			this.tpQueue.TabIndex = 0;
			this.tpQueue.Text = "Queue";
			// 
			// btnQueueInfinite
			// 
			this.btnQueueInfinite.Location = new System.Drawing.Point(576, 88);
			this.btnQueueInfinite.Name = "btnQueueInfinite";
			this.btnQueueInfinite.Size = new System.Drawing.Size(104, 40);
			this.btnQueueInfinite.TabIndex = 12;
			this.btnQueueInfinite.Text = "Continuos (OFF)";
			this.btnQueueInfinite.Click += new System.EventHandler(this.btnQueueInfinite_Click);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(304, 8);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(56, 23);
			this.label5.TabIndex = 11;
			this.label5.Text = "per batch";
			// 
			// numBatchSize
			// 
			this.numBatchSize.Increment = new System.Decimal(new int[] {
																		   10,
																		   0,
																		   0,
																		   0});
			this.numBatchSize.Location = new System.Drawing.Point(248, 8);
			this.numBatchSize.Maximum = new System.Decimal(new int[] {
																		 2048,
																		 0,
																		 0,
																		 0});
			this.numBatchSize.Minimum = new System.Decimal(new int[] {
																		 1,
																		 0,
																		 0,
																		 0});
			this.numBatchSize.Name = "numBatchSize";
			this.numBatchSize.Size = new System.Drawing.Size(56, 20);
			this.numBatchSize.TabIndex = 10;
			this.numBatchSize.Value = new System.Decimal(new int[] {
																	   250,
																	   0,
																	   0,
																	   0});
			// 
			// txtSqlStoredProc
			// 
			this.txtSqlStoredProc.Location = new System.Drawing.Point(8, 64);
			this.txtSqlStoredProc.Name = "txtSqlStoredProc";
			this.txtSqlStoredProc.Size = new System.Drawing.Size(680, 20);
			this.txtSqlStoredProc.TabIndex = 9;
			this.txtSqlStoredProc.Text = "up_UpdateMember_GetBatch";
			// 
			// txtBulkLoadConnectionString
			// 
			this.txtBulkLoadConnectionString.Location = new System.Drawing.Point(8, 40);
			this.txtBulkLoadConnectionString.Name = "txtBulkLoadConnectionString";
			this.txtBulkLoadConnectionString.Size = new System.Drawing.Size(680, 20);
			this.txtBulkLoadConnectionString.TabIndex = 8;
			this.txtBulkLoadConnectionString.Text = "Integrated Security=SSPI;Persist Security Info=True;Initial Catalog=brFastSearch_BulkLoader;Data Source=clsqltemp02";
			// 
			// chkQueueDeleteFlag
			// 
			this.chkQueueDeleteFlag.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.chkQueueDeleteFlag.Location = new System.Drawing.Point(8, 104);
			this.chkQueueDeleteFlag.Name = "chkQueueDeleteFlag";
			this.chkQueueDeleteFlag.Size = new System.Drawing.Size(368, 32);
			this.chkQueueDeleteFlag.TabIndex = 7;
			this.chkQueueDeleteFlag.Text = "Delete?  To test Delete, set FASTPUSHSVC_BULKIMPORT_NOREMOVEFLAG = \"false\" in DB";
			this.chkQueueDeleteFlag.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(176, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 20);
			this.label1.TabIndex = 5;
			this.label1.Text = "Itterations of";
			// 
			// txtQueueDomainID
			// 
			this.txtQueueDomainID.Location = new System.Drawing.Point(608, 8);
			this.txtQueueDomainID.Name = "txtQueueDomainID";
			this.txtQueueDomainID.Size = new System.Drawing.Size(80, 20);
			this.txtQueueDomainID.TabIndex = 4;
			this.txtQueueDomainID.Text = "3";
			// 
			// txtQueueMemberID
			// 
			this.txtQueueMemberID.Location = new System.Drawing.Point(488, 8);
			this.txtQueueMemberID.Name = "txtQueueMemberID";
			this.txtQueueMemberID.Size = new System.Drawing.Size(112, 20);
			this.txtQueueMemberID.TabIndex = 3;
			this.txtQueueMemberID.Text = "{MemberID}";
			this.txtQueueMemberID.Enter += new System.EventHandler(this.txt_Enter_BlankOutValue);
			// 
			// btnQueuePushSingle
			// 
			this.btnQueuePushSingle.Location = new System.Drawing.Point(368, 8);
			this.btnQueuePushSingle.Name = "btnQueuePushSingle";
			this.btnQueuePushSingle.Size = new System.Drawing.Size(112, 20);
			this.btnQueuePushSingle.TabIndex = 2;
			this.btnQueuePushSingle.Text = "Enqueue 1 Member";
			this.btnQueuePushSingle.Click += new System.EventHandler(this.btnQueuePushSingle_Click);
			// 
			// numQueueItterCount
			// 
			this.numQueueItterCount.Location = new System.Drawing.Point(96, 8);
			this.numQueueItterCount.Maximum = new System.Decimal(new int[] {
																			   999999,
																			   0,
																			   0,
																			   0});
			this.numQueueItterCount.Minimum = new System.Decimal(new int[] {
																			   1,
																			   0,
																			   0,
																			   0});
			this.numQueueItterCount.Name = "numQueueItterCount";
			this.numQueueItterCount.Size = new System.Drawing.Size(80, 20);
			this.numQueueItterCount.TabIndex = 1;
			this.numQueueItterCount.Value = new System.Decimal(new int[] {
																			 1,
																			 0,
																			 0,
																			 0});
			// 
			// btnQueueEnqueue
			// 
			this.btnQueueEnqueue.Location = new System.Drawing.Point(8, 8);
			this.btnQueueEnqueue.Name = "btnQueueEnqueue";
			this.btnQueueEnqueue.Size = new System.Drawing.Size(80, 23);
			this.btnQueueEnqueue.TabIndex = 0;
			this.btnQueueEnqueue.Text = "Enqueue";
			this.btnQueueEnqueue.Click += new System.EventHandler(this.btnQueueEnqueue_Click);
			// 
			// tpPush
			// 
			this.tpPush.Controls.Add(this.btnDeleteSingle);
			this.tpPush.Controls.Add(this.lblPushThreadCount);
			this.tpPush.Controls.Add(this.trkPushThreadCount);
			this.tpPush.Controls.Add(this.numPushItterCount);
			this.tpPush.Controls.Add(this.btnPushLoopEnd);
			this.tpPush.Controls.Add(this.btnPushLoopStart);
			this.tpPush.Controls.Add(this.btnPushNBatches);
			this.tpPush.Controls.Add(this.label2);
			this.tpPush.Controls.Add(this.numPushDomainID);
			this.tpPush.Controls.Add(this.txtPushMemberID);
			this.tpPush.Controls.Add(this.btnPushSingle);
			this.tpPush.Location = new System.Drawing.Point(4, 22);
			this.tpPush.Name = "tpPush";
			this.tpPush.Size = new System.Drawing.Size(696, 134);
			this.tpPush.TabIndex = 2;
			this.tpPush.Text = "Push";
			// 
			// btnDeleteSingle
			// 
			this.btnDeleteSingle.Location = new System.Drawing.Point(8, 40);
			this.btnDeleteSingle.Name = "btnDeleteSingle";
			this.btnDeleteSingle.Size = new System.Drawing.Size(88, 23);
			this.btnDeleteSingle.TabIndex = 10;
			this.btnDeleteSingle.Text = "Delete Single";
			this.btnDeleteSingle.Click += new System.EventHandler(this.btnDeleteSingle_Click);
			// 
			// lblPushThreadCount
			// 
			this.lblPushThreadCount.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblPushThreadCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPushThreadCount.Location = new System.Drawing.Point(448, 16);
			this.lblPushThreadCount.Name = "lblPushThreadCount";
			this.lblPushThreadCount.Size = new System.Drawing.Size(104, 23);
			this.lblPushThreadCount.TabIndex = 9;
			this.lblPushThreadCount.Text = "1 Thread(s)";
			this.lblPushThreadCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// trkPushThreadCount
			// 
			this.trkPushThreadCount.Location = new System.Drawing.Point(560, 8);
			this.trkPushThreadCount.Minimum = 1;
			this.trkPushThreadCount.Name = "trkPushThreadCount";
			this.trkPushThreadCount.Orientation = System.Windows.Forms.Orientation.Vertical;
			this.trkPushThreadCount.Size = new System.Drawing.Size(34, 120);
			this.trkPushThreadCount.TabIndex = 8;
			this.trkPushThreadCount.Value = 1;
			this.trkPushThreadCount.ValueChanged += new System.EventHandler(this.trkPushThreadCount_ValueChanged);
			// 
			// numPushItterCount
			// 
			this.numPushItterCount.Location = new System.Drawing.Point(208, 72);
			this.numPushItterCount.Maximum = new System.Decimal(new int[] {
																			  10000,
																			  0,
																			  0,
																			  0});
			this.numPushItterCount.Name = "numPushItterCount";
			this.numPushItterCount.Size = new System.Drawing.Size(104, 20);
			this.numPushItterCount.TabIndex = 7;
			this.numPushItterCount.Value = new System.Decimal(new int[] {
																			1,
																			0,
																			0,
																			0});
			// 
			// btnPushLoopEnd
			// 
			this.btnPushLoopEnd.Location = new System.Drawing.Point(448, 40);
			this.btnPushLoopEnd.Name = "btnPushLoopEnd";
			this.btnPushLoopEnd.Size = new System.Drawing.Size(104, 23);
			this.btnPushLoopEnd.TabIndex = 6;
			this.btnPushLoopEnd.Text = "Push Loop End";
			this.btnPushLoopEnd.Click += new System.EventHandler(this.btnPushLoopEnd_Click);
			// 
			// btnPushLoopStart
			// 
			this.btnPushLoopStart.Location = new System.Drawing.Point(448, 64);
			this.btnPushLoopStart.Name = "btnPushLoopStart";
			this.btnPushLoopStart.Size = new System.Drawing.Size(104, 23);
			this.btnPushLoopStart.TabIndex = 5;
			this.btnPushLoopStart.Text = "Push Loop Start";
			this.btnPushLoopStart.Click += new System.EventHandler(this.btnPushLoopStart_Click);
			// 
			// btnPushNBatches
			// 
			this.btnPushNBatches.Location = new System.Drawing.Point(208, 40);
			this.btnPushNBatches.Name = "btnPushNBatches";
			this.btnPushNBatches.Size = new System.Drawing.Size(104, 24);
			this.btnPushNBatches.TabIndex = 4;
			this.btnPushNBatches.Text = "Push [N] Batches";
			this.btnPushNBatches.Click += new System.EventHandler(this.btnPushNBatches_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(256, 8);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			this.label2.Text = "DomainID";
			// 
			// numPushDomainID
			// 
			this.numPushDomainID.Location = new System.Drawing.Point(208, 8);
			this.numPushDomainID.Maximum = new System.Decimal(new int[] {
																			999,
																			0,
																			0,
																			0});
			this.numPushDomainID.Minimum = new System.Decimal(new int[] {
																			1,
																			0,
																			0,
																			0});
			this.numPushDomainID.Name = "numPushDomainID";
			this.numPushDomainID.Size = new System.Drawing.Size(48, 20);
			this.numPushDomainID.TabIndex = 2;
			this.numPushDomainID.Value = new System.Decimal(new int[] {
																		  3,
																		  0,
																		  0,
																		  0});
			// 
			// txtPushMemberID
			// 
			this.txtPushMemberID.Location = new System.Drawing.Point(104, 8);
			this.txtPushMemberID.Name = "txtPushMemberID";
			this.txtPushMemberID.TabIndex = 1;
			this.txtPushMemberID.Text = "{memberid}";
			this.txtPushMemberID.Enter += new System.EventHandler(this.txt_Enter_BlankOutValue);
			// 
			// btnPushSingle
			// 
			this.btnPushSingle.Location = new System.Drawing.Point(8, 8);
			this.btnPushSingle.Name = "btnPushSingle";
			this.btnPushSingle.Size = new System.Drawing.Size(88, 23);
			this.btnPushSingle.TabIndex = 0;
			this.btnPushSingle.Text = "Push Single";
			this.btnPushSingle.Click += new System.EventHandler(this.btnPushSingle_Click);
			// 
			// tpMetrics
			// 
			this.tpMetrics.Controls.Add(this.btnMetricsUninstallQueues);
			this.tpMetrics.Controls.Add(this.btnMetricsInstallQueues);
			this.tpMetrics.Controls.Add(this.btnMetricsUninstallCounters);
			this.tpMetrics.Controls.Add(this.btnMetricsInstallCounters);
			this.tpMetrics.Controls.Add(this.btnShowThreadConfig);
			this.tpMetrics.Location = new System.Drawing.Point(4, 22);
			this.tpMetrics.Name = "tpMetrics";
			this.tpMetrics.Size = new System.Drawing.Size(696, 134);
			this.tpMetrics.TabIndex = 1;
			this.tpMetrics.Text = "Metrics";
			// 
			// btnMetricsUninstallQueues
			// 
			this.btnMetricsUninstallQueues.Location = new System.Drawing.Point(40, 56);
			this.btnMetricsUninstallQueues.Name = "btnMetricsUninstallQueues";
			this.btnMetricsUninstallQueues.Size = new System.Drawing.Size(104, 23);
			this.btnMetricsUninstallQueues.TabIndex = 3;
			this.btnMetricsUninstallQueues.Text = "Uninstall Queues";
			this.btnMetricsUninstallQueues.Click += new System.EventHandler(this.btnMetricsUninstallQueues_Click);
			// 
			// btnMetricsInstallQueues
			// 
			this.btnMetricsInstallQueues.Location = new System.Drawing.Point(40, 24);
			this.btnMetricsInstallQueues.Name = "btnMetricsInstallQueues";
			this.btnMetricsInstallQueues.Size = new System.Drawing.Size(104, 23);
			this.btnMetricsInstallQueues.TabIndex = 2;
			this.btnMetricsInstallQueues.Text = "Install Queues";
			this.btnMetricsInstallQueues.Click += new System.EventHandler(this.btnMetricsInstallQueues_Click);
			// 
			// btnMetricsUninstallCounters
			// 
			this.btnMetricsUninstallCounters.Location = new System.Drawing.Point(336, 64);
			this.btnMetricsUninstallCounters.Name = "btnMetricsUninstallCounters";
			this.btnMetricsUninstallCounters.Size = new System.Drawing.Size(112, 23);
			this.btnMetricsUninstallCounters.TabIndex = 1;
			this.btnMetricsUninstallCounters.Text = "Uninstall Counters";
			this.btnMetricsUninstallCounters.Click += new System.EventHandler(this.btnMetricsUninstallCounters_Click);
			// 
			// btnMetricsInstallCounters
			// 
			this.btnMetricsInstallCounters.Location = new System.Drawing.Point(336, 32);
			this.btnMetricsInstallCounters.Name = "btnMetricsInstallCounters";
			this.btnMetricsInstallCounters.Size = new System.Drawing.Size(112, 23);
			this.btnMetricsInstallCounters.TabIndex = 0;
			this.btnMetricsInstallCounters.Text = "Install Counters";
			this.btnMetricsInstallCounters.Click += new System.EventHandler(this.btnMetricsInstallCounters_Click);
			// 
			// btnShowThreadConfig
			// 
			this.btnShowThreadConfig.Location = new System.Drawing.Point(480, 24);
			this.btnShowThreadConfig.Name = "btnShowThreadConfig";
			this.btnShowThreadConfig.Size = new System.Drawing.Size(120, 23);
			this.btnShowThreadConfig.TabIndex = 2;
			this.btnShowThreadConfig.Text = "Show Thread Config";
			this.btnShowThreadConfig.Click += new System.EventHandler(this.btnShowThreadConfig_Click);
			// 
			// tpGeo
			// 
			this.tpGeo.Controls.Add(this.btnGeoGetRegion);
			this.tpGeo.Controls.Add(this.label4);
			this.tpGeo.Controls.Add(this.label3);
			this.tpGeo.Controls.Add(this.txtLngInput);
			this.tpGeo.Controls.Add(this.txtLatInput);
			this.tpGeo.Controls.Add(this.btnGeoMakeBoxes);
			this.tpGeo.Location = new System.Drawing.Point(4, 22);
			this.tpGeo.Name = "tpGeo";
			this.tpGeo.Size = new System.Drawing.Size(696, 134);
			this.tpGeo.TabIndex = 3;
			this.tpGeo.Text = "Geo";
			// 
			// btnGeoGetRegion
			// 
			this.btnGeoGetRegion.Location = new System.Drawing.Point(456, 8);
			this.btnGeoGetRegion.Name = "btnGeoGetRegion";
			this.btnGeoGetRegion.Size = new System.Drawing.Size(144, 23);
			this.btnGeoGetRegion.TabIndex = 5;
			this.btnGeoGetRegion.Text = "Get Region";
			this.btnGeoGetRegion.Click += new System.EventHandler(this.btnGeoGetRegion_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 32);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(56, 16);
			this.label4.TabIndex = 4;
			this.label4.Text = "Longitude";
			this.label4.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 8);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 16);
			this.label3.TabIndex = 3;
			this.label3.Text = "Latitude";
			this.label3.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// txtLngInput
			// 
			this.txtLngInput.Location = new System.Drawing.Point(64, 32);
			this.txtLngInput.Name = "txtLngInput";
			this.txtLngInput.Size = new System.Drawing.Size(88, 20);
			this.txtLngInput.TabIndex = 2;
			this.txtLngInput.Text = "0.0";
			// 
			// txtLatInput
			// 
			this.txtLatInput.Location = new System.Drawing.Point(64, 8);
			this.txtLatInput.Name = "txtLatInput";
			this.txtLatInput.Size = new System.Drawing.Size(88, 20);
			this.txtLatInput.TabIndex = 1;
			this.txtLatInput.Text = "0.0";
			// 
			// btnGeoMakeBoxes
			// 
			this.btnGeoMakeBoxes.Location = new System.Drawing.Point(8, 56);
			this.btnGeoMakeBoxes.Name = "btnGeoMakeBoxes";
			this.btnGeoMakeBoxes.Size = new System.Drawing.Size(144, 23);
			this.btnGeoMakeBoxes.TabIndex = 0;
			this.btnGeoMakeBoxes.Text = "Make Boxes";
			this.btnGeoMakeBoxes.Click += new System.EventHandler(this.btnGeoMakeBoxes_Click);
			// 
			// tpErrorQueue
			// 
			this.tpErrorQueue.Controls.Add(this.txtErrorQueuePath);
			this.tpErrorQueue.Controls.Add(this.btnErrorQueueShowContent);
			this.tpErrorQueue.Controls.Add(this.txtRequeueErorrBatchSize);
			this.tpErrorQueue.Controls.Add(this.btnQueueRequeuErrors);
			this.tpErrorQueue.Location = new System.Drawing.Point(4, 22);
			this.tpErrorQueue.Name = "tpErrorQueue";
			this.tpErrorQueue.Size = new System.Drawing.Size(696, 134);
			this.tpErrorQueue.TabIndex = 4;
			this.tpErrorQueue.Text = "Error Queue";
			// 
			// txtErrorQueuePath
			// 
			this.txtErrorQueuePath.Location = new System.Drawing.Point(168, 8);
			this.txtErrorQueuePath.Name = "txtErrorQueuePath";
			this.txtErrorQueuePath.Size = new System.Drawing.Size(512, 20);
			this.txtErrorQueuePath.TabIndex = 9;
			this.txtErrorQueuePath.Text = "FormatName:DIRECT=OS:.\\private$\\fastpush_error";
			this.txtErrorQueuePath.TextChanged += new System.EventHandler(this.txtErrorQueuePath_TextChanged);
			// 
			// btnErrorQueueShowContent
			// 
			this.btnErrorQueueShowContent.Location = new System.Drawing.Point(8, 8);
			this.btnErrorQueueShowContent.Name = "btnErrorQueueShowContent";
			this.btnErrorQueueShowContent.Size = new System.Drawing.Size(152, 20);
			this.btnErrorQueueShowContent.TabIndex = 8;
			this.btnErrorQueueShowContent.Text = "Show Error Queue Content";
			this.btnErrorQueueShowContent.Click += new System.EventHandler(this.btnErrorQueueShowContent_Click);
			// 
			// txtRequeueErorrBatchSize
			// 
			this.txtRequeueErorrBatchSize.Location = new System.Drawing.Point(648, 64);
			this.txtRequeueErorrBatchSize.Name = "txtRequeueErorrBatchSize";
			this.txtRequeueErorrBatchSize.Size = new System.Drawing.Size(40, 20);
			this.txtRequeueErorrBatchSize.TabIndex = 7;
			this.txtRequeueErorrBatchSize.Text = "100";
			// 
			// btnQueueRequeuErrors
			// 
			this.btnQueueRequeuErrors.Location = new System.Drawing.Point(504, 64);
			this.btnQueueRequeuErrors.Name = "btnQueueRequeuErrors";
			this.btnQueueRequeuErrors.Size = new System.Drawing.Size(136, 23);
			this.btnQueueRequeuErrors.TabIndex = 6;
			this.btnQueueRequeuErrors.Text = "Requeue Error Q";
			this.btnQueueRequeuErrors.Click += new System.EventHandler(this.btnQueueRequeuErrors_Click);
			// 
			// PushTestHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(704, 485);
			this.Controls.Add(this.tcMain);
			this.Controls.Add(this.txtOutput);
			this.Name = "PushTestHarness";
			this.Text = "Test Harness";
			this.tcMain.ResumeLayout(false);
			this.tpQueue.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numBatchSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numQueueItterCount)).EndInit();
			this.tpPush.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.trkPushThreadCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPushItterCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPushDomainID)).EndInit();
			this.tpMetrics.ResumeLayout(false);
			this.tpGeo.ResumeLayout(false);
			this.tpErrorQueue.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.Run(new PushTestHarness());
		}

		private void AppendMessage(string message) {
			txtOutput.AppendText(message + "\r\n");
		}

		private void AppendError(Exception ex) {
			AppendMessage(ex.ToString());
		}


		private void btnMetricsInstallCounters_Click(object sender, System.EventArgs e) {
			MetricsInstaller.Install();
		}

		private void btnMetricsUninstallCounters_Click(object sender, System.EventArgs e) {
			MetricsInstaller.Uninstall();
		}

		private void btnQueueEnqueue_Click(object sender, System.EventArgs e) {
			txtOutput.Clear();
			QueueConfiguration qconf = GetQueueConfig();
//			int CommunityID = Int32.Parse(txtQueueDomainID.Text);
//			BusinessLogic.Queue queue = qconf.GetQueue(CommunityID);

			for (int i = 0 ; i < (int)numQueueItterCount.Value; i++)
			{
				Debug.WriteLine("Form1.btnQueueEnqueue_Click():  _BatchSize[" + _BatchSize.ToString() 
					+ "] _BulkLoadConnectionString[" + _BulkLoadConnectionString.ToString() 
					+ "] chkQueueDeleteFlag.Checked[" + chkQueueDeleteFlag.Checked.ToString() + "]");

				QueueUtility.EnqueueMembersFromDB(
									(int)numBatchSize.Value, 
									txtBulkLoadConnectionString.Text, 
									txtSqlStoredProc.Text,
									chkQueueDeleteFlag.Checked ? IndexAction.Delete : IndexAction.Insert);
				AppendMessage(string.Format("Added batch {0} to queue",i));
			}

//			queue = null;

		}

		private void btnQueuePushSingle_Click(object sender, System.EventArgs e) {
			try {
				txtOutput.Clear();
				int CommunityID = Int32.Parse(txtQueueDomainID.Text);
				BusinessLogic.Queue queue = GetQueueConfig().GetQueue(CommunityID);

				int memberID = Convert.ToInt32(txtQueueMemberID.Text);

				AppendMessage(string.Format("Queueing Member {0} to {1}\n", memberID,CommunityID));
				IFastPushPointer pointer = new FastPushPointer(memberID,CommunityID,0,IndexAction.Insert);
				
				queue.EnqeueProfilePointer(pointer);

				AppendMessage("Member Queued");

				queue = null;
			} 
			catch (Exception ex) {
				AppendError(ex);
			}

		}

		private void txt_Enter_BlankOutValue(object sender, EventArgs e) {
			(sender as TextBox).Text = "";
		}

		private void btnPushSingle_Click(object sender, System.EventArgs e) {
			int CommunityID = (int)numPushDomainID.Value;
			BusinessLogic.Queue queue = GetQueueConfig().GetQueue(CommunityID);
			PushBL pusher = new PushBL(queue,CommunityID);
			IFastPushPointer pointer = new FastPushPointer(Int32.Parse(txtPushMemberID.Text),(int)numPushDomainID.Value,0,IndexAction.Insert);
			pusher.PushDocument(pointer);
			AppendMessage(string.Format("Pushed Test Profile {0} (DomainID {1})",pointer.MemberID,pointer.CommunityID ));
			pusher = null;
			queue = null;
		}

		private void trkPushThreadCount_ValueChanged(object sender, System.EventArgs e) {
			lblPushThreadCount.Text = trkPushThreadCount.Value.ToString() + " Thread(s)";
		}

		private void btnPushNBatches_Click(object sender, System.EventArgs e) {
			TimeSpan ts1, ts2;
			int CommunityID = (int)numPushDomainID.Value;
			BusinessLogic.Queue queue = GetQueueConfig().GetQueue(CommunityID);
			PushBL pusher = new PushBL(queue,CommunityID);


			for (int n = 0 ; n < (int)numPushItterCount.Value; n++){
			
				ts1 = new TimeSpan(DateTime.Now.Ticks);
				
				pusher.PushDocumentsFromQueue();

				ts2 = new TimeSpan(DateTime.Now.Ticks);
				AppendMessage(string.Format("[{0,-6}] Pushed batch from queue in {1} ms",n,ts2.Subtract(ts1).TotalMilliseconds ));
			}

			pusher = null;
			queue = null;
		}

		private void btnPushLoopEnd_Click(object sender, System.EventArgs e) {
		
		}

		private void btnPushLoopStart_Click(object sender, System.EventArgs e) {
		
		}

		private void btnQueueRequeuErrors_Click(object sender, System.EventArgs e) {
			QueueUtility.ReQueueMembersFromErrorQueue(Int32.Parse(txtRequeueErorrBatchSize.Text));
		}

		private void btnGeoMakeBoxes_Click(object sender, System.EventArgs e) {
			double xlat = Double.Parse(txtLatInput.Text);
			double xlng = Double.Parse(txtLngInput.Text);
			ValueObjects.GeoBox.Location loc = GeoBox.CreateLocation(xlng,xlat);
			string [] bestPhases = GeoBox.BestPhases(loc);
			
			txtOutput.AppendText(string.Format("\n*** Location *** Lat {0} [{1}], Lng {2} [{3}]\n",loc.lat,  loc.NormalizedLat, loc.lng  , loc.NormalizedLng,loc ));
			foreach (GeoBox.PhaseBoxes band in loc.Grids) {
				txtOutput.AppendText("Band: ");
				foreach (string boxName in band.BoxNames) {
					txtOutput.AppendText(boxName + " \t");
				}
				txtOutput.AppendText("\n");
			}
			foreach (string phaseName in bestPhases) {
				txtOutput.AppendText("Best: " + phaseName + "\n");
			}
		}

		private void btnGeoGetRegion_Click(object sender, System.EventArgs e) {
			BusinessLogic.Region r = RegionBL.Instance.GetRegion(2639053);

		}

		private void btnMetricsInstallQueues_Click(object sender, System.EventArgs e) {
			QueueUtility.InstallQueues();
		}

		private void btnMetricsUninstallQueues_Click(object sender, System.EventArgs e) {
			QueueUtility.UninstallQueues();
		}

		private void btnDeleteSingle_Click(object sender, System.EventArgs e) {
			// Retrieve values.
			int communityID = Convert.ToInt32(numPushDomainID.Value);
			int memberID = Convert.ToInt32(txtPushMemberID.Text);

			BusinessLogic.Queue queue = GetQueueConfig().GetQueue(communityID);
			
			PushBL pusher = new PushBL(queue, communityID);

			IFastPushPointer pointer = new FastPushPointer(memberID, communityID, 0, IndexAction.Delete);

			pusher.PushDocument(pointer);
			
			AppendMessage(string.Format("Pushed Test Profile {0} (DomainID {1}) for deletion.", memberID, communityID));
			
			// Cleanup.
			pusher = null;
			queue = null;
		}

		private void btnShowThreadConfig_Click(object sender, System.EventArgs e) {
			QueueConfiguration qconf = GetQueueConfig();

			int [] Communities = qconf.CommunityList;
			for (int i = 0; i < Communities.Length; i++){
				txtOutput.AppendText(string.Format(" {0})Community {1} using {2} threads.\n",i,Communities[i],qconf.ThreadsPerQueue[i]));
			}   
		}
		
		private QueueConfiguration GetQueueConfig(){
			if (_QueueConf == null){
				_QueueConf = new QueueConfiguration();
			}
			return _QueueConf;
		}

		private void btnErrorQueueShowContent_Click(object sender, System.EventArgs e)
		{
			try 
			{
				MessageQueue mq = new MessageQueue(txtErrorQueuePath.Text);
				mq.Formatter = new BinaryMessageFormatter();
				System.Messaging.Message [] messages = mq.GetAllMessages();

				for (int i = 0; i < messages.Length; i++)
				{
					try 
					{
						IFastPushPointer ptr = messages[i].Body as IFastPushPointer;
						txtOutput.AppendText(String.Format("mq{0}\t[{1}]>MID:{2}\tCID:{3}\tSID:{4}\tAct:{5}\n", messages[i].Id,i,ptr.MemberID, ptr.CommunityID, ptr.SiteID,ptr.ActionID));
					}
					catch (Exception ex) 
					{
						txtOutput.AppendText("msg[" + i.ToString() + "] not good." + ex.Message + "\n");
					}
				}

			}
			catch (Exception ex) 
			{
				txtOutput.AppendText(ex.ToString());
			}


		}

		private void txtErrorQueuePath_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		Thread _pushThread;
		QueueUtility _queueUtility;
		private void btnQueueInfinite_Click(object sender, System.EventArgs e)
		{
			if (_queueUtility == null || _queueUtility.ContinuousEnabledFlag == false )
			{
				btnQueueInfinite.Enabled = false;

				_queueUtility = new QueueUtility();
				_queueUtility.ContinuousEnabledFlag = true;
				_queueUtility.BulkImportConnectionString = txtBulkLoadConnectionString.Text ;
				_queueUtility.BulkImportSQLStoredProc = txtSqlStoredProc.Text;
				_pushThread = new Thread(new ThreadStart(_queueUtility.ContinuousEnqueue));
				_pushThread.Start();
				btnQueueInfinite.Text = "Continuos (is ON)";
				btnQueueInfinite.Enabled = true;


			}
			else {
				btnQueueInfinite.Enabled = false;
				_queueUtility.ContinuousEnabledFlag = false;
				_pushThread.Join();
				btnQueueInfinite.Text = "Continuos (is OFF)";
				btnQueueInfinite.Enabled = true;

			}
			
		}
	}
}
