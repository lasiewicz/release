@ECHO ***************************
@ECHO *** Deploy to devapp01?
pause

psexec \\devapp01 net stop FASTPush
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Push.Service\ \\devapp01\c$\matchnet\bin\Service\Matchnet.FAST.Push.Service * /s /xo /r:999 /w:0
psexec \\devapp01 net start FASTPush


@ECHO ***************************
@ECHO *** Deploy to devapp02?
pause

psexec \\devapp02 net stop FASTPush
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Push.Service\ \\devapp02\c$\matchnet\bin\Service\Matchnet.FAST.Push.Service * /s /xo /r:999 /w:0
psexec \\devapp02 net start FASTPush

pause


