using System;
using System.Text;

namespace Matchnet.FAST.Push.ValueObjects
{
	/// <summary>
	/// Summary description for EpochUtil.
	/// </summary>
	public class EpochUtil
	{

		/// <summary>
		/// Epoch Resolutions. The divisor list to generate toe epochs.
		/// Epoch base resolution is days, so an epoch resolution of 7 will yield a week epoch, 30 a month epoch etc.
		/// </summary>
		public static int [] EPOCH_RESOLUTIONS = {1,7,30,60,90,180,360,720};

		private const string EPOCH_TOKEN_PREFIX = "EPCH";
		
		/// <summary>
		/// This is the reference date for epoch calculations.
		/// </summary>
		private static readonly DateTime _ReferenceDate = new DateTime(1900,1,1);

		/// <summary>
		/// Given a date, returns string containing prefiexed toekns, each token representing the 
		/// epoch for the given epoch resolution , measured in day quantities.
		/// EPCH1x2600 is epoch # 2600 for resolution 1.
		/// EPCH7x32 is epoch # 32 for resulution 7 (32nd week..)
		/// etc.
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		public static string GetEpochDateTokens(DateTime date)
		{
			if (date > _ReferenceDate)
			{
				int totalDays = GetEpoch(date);
				StringBuilder sb = new StringBuilder(100);
				foreach ( int i in EPOCH_RESOLUTIONS){
					sb.Append(EPOCH_TOKEN_PREFIX);
					sb.Append(i.ToString());
					sb.Append("x");
					sb.Append(totalDays / i);
					sb.Append(" ");
				}

				return sb.ToString();
			}

			return string.Empty;
		}


		/// <summary>
		/// Returns the epoch, in day resolution, based on the date given and the internal reference date.
		/// 
		/// </summary>
		/// <param name="date">The date to calculate epoch for</param>
		/// <returns>The epoch in days. Technically: the datediff (days,_ReferenceDate,date)
		/// if date is earlier than the reference date, Int32.MaxValue is returned!
		/// </returns>
		public static int GetEpoch(DateTime date){
			if (date > _ReferenceDate)
			{
				return (int)(new TimeSpan(date.Subtract(_ReferenceDate).Ticks)).TotalDays;
			}
			else 
			{
				return Int32.MaxValue;
			}
		}

		/// <summary>
		/// Gets the appropriate string representation for the epoch in the given resolution
		/// </summary>
		/// <param name="Epoch">The epoch relative to the internal reference date, expressed in days</param>
		/// <param name="EpochResolution">The epoch resolution (should be one of EPOCH_RESOLUTIONS[i] ! </param>
		/// <returns>A token representing this epoch</returns>
		public static string GetEpochToken(int Epoch, int EpochResolution)
		{
			return	EPOCH_TOKEN_PREFIX + EpochResolution.ToString() + 'x' + (Epoch / EpochResolution).ToString();
		}
	}
}
