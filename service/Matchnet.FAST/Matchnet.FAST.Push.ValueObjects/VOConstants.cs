using System;

namespace Matchnet.FAST.Push.ValueObjects
{
	/// <summary>
	/// Summary description for VOConstants.
	/// </summary>
	public class VOConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string BOOL_TRUE = "1";
		/// <summary>
		/// 
		/// </summary>
		public const string BOOL_FALSE = "0";

		/// <summary>
		/// 
		/// </summary>
		public const int GLOBALSTATUSMASK_VAL_ADMINSUSPEND = 1 ;
		/// <summary>
		/// 
		/// </summary>
		public const int GLOBALSTATUSMASK_VAL_BADEMAIL = 2 ;
		/// <summary>
		/// 
		/// </summary>
		public const int HIDEMASK_VAL_HIDEFROMSEARCH = 1;
		/// <summary>
		/// 
		/// </summary>
		public const int SELFSUSPENDEDFLAG_VAL = 1;

		#region AttributeName Constants
		// Special FAST attributes.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HWBOOST = "HWBoost";

		// AttributeNames that are used specifically for ExclusionRules.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GLOBALSTATUSMASK = "GlobalStatusMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HIDEMASK = "HideMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_SELFSUSPENDEDFLAG = "SelfSuspendedFlag";

		// AttributeNames that are for existing Attributes that are not stored in a profile typically.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MEMBERID = "MemberID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_COMMUNITYID = "CommunityID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CITY = "City";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_STATE = "State";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_COUNTRY = "Country";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_POSTALCODE = "PostalCode";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS = "DisplayPhotosToGuests";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_REGIONID = "RegionID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_SMSCAPABLE = "SMSCapable";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CANNOTDOWITHOUT = "CannotDoWithout";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_WHENIHAVETIME = "WhenIHaveTime";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FACEPAGEINTRODUCTION = "FacePageIntroduction";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_BIRTHCOUNTRYREGIONID = "BirthCountryRegionID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_PRIVATELABELID = "PrivateLabelID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ISRAELAREACODE = "IsraelAreaCode";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_APPEARINGONTHEMEDIA = "AppearingOnTheMedia";

		// AttributeNames that don't exist as Attributes in mnShared at all and are synthetic with a base of 100,000.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LATITUDE = "Latitude";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LONGITUDE = "Longitude";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_AREACODE = "AreaCode";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MEMBERREGISTRATIONDATE = "MemberRegistrationDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LASTLOGONDATE = "LastLogonDate";
		/// <summary>
		/// 2005-07-05: The value of assigned to ATTRIBUTENAME_LASTLOGONDATE_EPOCH will go into the FAST index field weight1combo.
		/// </summary>
		public const string ATTRIBUTENAME_LASTLOGONDATE_EPOCH = "LastLogonDateEpoch"; 
		// TODO:  Add data compilation for these.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ACTIVENESSRANK = "ActivenessRank";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ACTIVENESSRANKDATE = "ActivenessRankDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CATALYSTRANK = "CatalystRank";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CATALYSTRANKDATE = "CatalystRankDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FEATUREUSERANK = "FeatureuseRank";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FEATUREUSERANKDATE = "FeatureuseRankDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_POPULARITYRANK = "CommunityEmailCount";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_POPULARITYRANKDATE = "LastEmailReceiveDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RESPONSIVENESSRANK = "ResponsivenessRank";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RESPONSIVENESSRANKDATE = "ResponsivenessRankDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_USEDRANK = "UsedRank";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_USEDRANKDATE = "UsedRankDate";

		/// <summary> </summary>
		public const string ATTRIBUTENAME_GEO1 = "Geo1";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GEO2 = "Geo2";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GEO3 = "Geo3";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GEO4 = "Geo4";
		/// <summary> </summary>
        public const string ATTRIBUTENAME_GEO5 = "Geo5";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GEO6 = "Geo6";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GEO7 = "Geo7";

		/// <summary> </summary>
		public const string ATTRIBUTENAME_MATCHNETMETA = "MatchnetMeta";

		// AttributeNames of standard Attributes.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ABOUTME = "AboutMe";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY = "AboutMyAppearanceEssay";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ACTIVITYLEVEL = "ActivityLevel";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_APPEARANCEIMPORTANCE = "AppearanceImportance";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_BIRTHDATE = "Birthdate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_BODYART = "BodyArt";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_BODYTYPE = "BodyType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CHILDRENCOUNT = "ChildrenCount";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CUISINE = "Cuisine";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_CUSTODY = "Custody";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDDRINKINGHABITS = "DesiredDrinkingHabits";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDEDUCATIONLEVEL = "DesiredEducationLevel";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDJDATERELIGION = "DesiredJDateReligion";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDMARITALSTATUS = "DesiredMaritalStatus";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDMAXAGE = "DesiredMaxAge";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDMINAGE = "DesiredMinAge";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DESIREDSMOKINGHABITS = "DesiredSmokingHabits";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_DRINKINGHABITS = "DrinkingHabits";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_EDUCATIONLEVEL = "EducationLevel";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ENTERTAINMENTLOCATION = "EntertainmentLocation";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ESSAYDESCRIBEHUMOR = "EssayDescribeHumor";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ESSAYFIRSTTHINGNOTICEABOUTYOU = "EssayFirstThingNoticeAboutYou";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ESSAYFRIENDSDESCRIPTION = "EssayFriendsDescription";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ESSAYORGANIZATIONS = "EssayOrganizations";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ETHNICITY = "Ethnicity";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_EYECOLOR = "EyeColor";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FAVORITEALBUMSONGARTIST = "FavoriteAlbumSongArtist";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FAVORITEBOOKAUTHOR = "FavoriteBookAuthor";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR = "FavoriteMovieDirectorActor";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_FAVORITETIMEOFDAY = "FavoriteTimeOfDay";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GAYSCENEMASK = "GaySceneMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GENDERMASK = "GenderMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_GREWUPIN = "GrewUpIn";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HABITATTYPE = "HabitatType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HAIRCOLOR = "HairColor";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HASPHOTOFLAG = "HasPhotoFlag";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HAVECHILDREN = "HaveChildren";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HEADLINE = "Headline";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HEIGHT = "Height";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_HIVSTATUSTYPE = "HIVStatusType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_IDEALRELATIONSHIPESSAY = "IdealRelationshipEssay";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_IDEALVACATION = "IdealVacation";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_INCOMELEVEL = "IncomeLevel";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_INDUSTRYTYPE = "IndustryType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_INTELLIGENCEIMPORTANCE = "IntelligenceImportance";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ISRAELIMMIGRATIONDATE = "IsraelImmigrationDate";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ISRAELIPERSONALITYTRAIT = "IsraeliPersonalityTrait";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_JDATEETHNICITY = "JDateEthnicity";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_JDATERELIGION = "JDateReligion";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_KEEPKOSHER = "KeepKosher";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LANGUAGEMASK = "LanguageMask";
		// DEPRICATED:  Same as LASTBRANDID but used for Bulk Adding from legacy system.
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LASTPRIVATELABELID = "LastPrivateLabelID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LASTBRANDID = "LastBrandID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LASTUPDATED = "LastUpdated";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LEARNFROMTHEPASTESSAY = "LearnFromThePastEssay";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_LEISUREACTIVITY = "LeisureActivity";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MAJORTYPE = "MajorType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MARITALSTATUS = "MaritalStatus";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MILITARYSERVICETYPE = "MilitaryServiceType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MORECHILDRENFLAG = "MoreChildrenFlag";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MOVIEGENREMASK = "MovieGenreMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_MUSIC = "Music";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_OCCUPATIONDESCRIPTION = "OccupationDescription";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_OUTNESSTYPE = "OutnessType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_PERFECTFIRSTDATEESSAY = "PerfectFirstDateEssay";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_PERFECTMATCHESSAY = "PerfectMatchEssay";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_PERSONALITYTRAIT = "PersonalityTrait";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_PETS = "Pets";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_PHYSICALACTIVITY = "PhysicalActivity";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_POLITICALORIENTATION = "PoliticalOrientation";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_READING = "Reading";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RELATIONSHIPMASK = "RelationshipMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RELATIONSHIPSTATUS = "RelationshipStatus";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RELIGION = "Religion";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RELIGIONACTIVITYLEVEL = "ReligionActivityLevel";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_RELOCATEFLAG = "RelocateFlag";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_SCHOOLID = "SchoolID";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_SEXUALIDENTITYTYPE = "SexualIdentityType";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_SMOKINGHABITS = "SmokingHabits";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_STUDIESEMPHASIS = "StudiesEmphasis";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_SYNAGOGUEATTENDANCE = "SynagogueAttendance";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_TRAVELMASK = "TravelMask";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_USERNAME = "Username";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_WEIGHT = "Weight";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_ZODIAC = "Zodiac";
		/// <summary> </summary>
		public const string ATTRIBUTENAME_THUMBPATH = "ThumbnailPath";
		#endregion
	}
}
