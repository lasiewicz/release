using System;
using System.Collections;

using Matchnet.FAST.Push.Interfaces;

namespace Matchnet.FAST.Push.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	public class ExclusionRule : IFastExclusionRule
	{		
		private int _communityID;
		private bool _isExcluded = false;	// Assume innocent until proven otherwise.
		private bool _hasValidRegion = false;
		private bool _hasValidGenderMask = false;
		private bool _hasValidBirthDate = false;
		private bool _hasPhoto = false;
		private bool _hasEssay = false;
		private string _exclusionReason = string.Empty;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="CommunityID"></param>
		public ExclusionRule(int CommunityID)
		{
			// These values are needed for Exclusing Rule processing, later.
			_communityID = CommunityID;
		}

		/// <summary>
		/// Gets or sets property whether this instance is considered exluded or not.
		/// Short circuit evaluation is used for immediate exclusion if any evidence so far has excluded this instance.
		/// No memory of exidence is stored, so IsExcluded = false is equivalent to new DefaultExclusionRule.
		/// </summary>
		public bool IsExcluded
		{
			get
			{
#if DEBUG
				System.Diagnostics.Trace.WriteLineIf(!_hasValidRegion, "FAILED ON EXCLUSION RULE:  hasValidRegion");
				System.Diagnostics.Trace.WriteLineIf(!_hasValidGenderMask, "FAILED ON EXCLUSION RULE:  hasValidGenderMask");
				System.Diagnostics.Trace.WriteLineIf(!_hasValidBirthDate, "FAILED ON EXCLUSION RULE:  hasValidBirthDate");
				System.Diagnostics.Trace.WriteLineIf(!Rule_MemberHasPhotoOrEssay(), "FAILED ON EXCLUSION RULE:  Rule_MemberHasPhotoOrEssay()");
#endif
				if (!_hasValidRegion)
					_exclusionReason = "FAILED ON EXCLUSION RULE:  hasValidRegion";
				if (!_hasValidGenderMask)
					_exclusionReason = "FAILED ON EXCLUSION RULE:  hasValidGenderMask";
				if (!_hasValidBirthDate)
					_exclusionReason = "FAILED ON EXCLUSION RULE:  hasValidBirthDate";
				if (!Rule_MemberHasPhotoOrEssay())
					_exclusionReason = "FAILED ON EXCLUSION RULE:  Rule_MemberHasPhotoOrEssay()";

				return _isExcluded || !_hasValidRegion || !_hasValidGenderMask || !_hasValidBirthDate || (!Rule_MemberHasPhotoOrEssay());
			}
			set
			{
				_isExcluded = value;
			}
		}

		/// <summary>
		/// Adds evidence to support / refute whether this instance is excluded.
		/// </summary>
		/// <param name="Evidence">The evidence object to inspect.</param>
		/// <returns>IsExcluded (true/false) based on current instant logic.</returns>
		/// <remarks >
		/// YOU MUST CALL IsExcluded property at the end of all evidence to see whether accumulated 
		/// evidence amounts to exclusion.
		/// The return value is provided for short circuit evaluation purposes only 
		/// so that exclusion will be triggered on the first independed evidence encountered
		/// </remarks>
		public bool AddEvidence(IFastExclusionEvidence Evidence)
		{
			if (_isExcluded)
				return _isExcluded;
			else
			{
				//TODO: Add accumulator markings for non independent / must haves.
				switch (Evidence.EvidenceName)
				{
					case VOConstants.ATTRIBUTENAME_USERNAME:
						if (Evidence.Evidence == null || Evidence.ToString() == string.Empty)
							_isExcluded = true;

						break;
					case VOConstants.ATTRIBUTENAME_GLOBALSTATUSMASK: 
						if ((Convert.ToInt32(Evidence.Evidence) & (VOConstants.GLOBALSTATUSMASK_VAL_ADMINSUSPEND)) != 0)
							_isExcluded = true;

						break;

					case VOConstants.ATTRIBUTENAME_HIDEMASK: 
						if ((Convert.ToInt32(Evidence.Evidence) & VOConstants.HIDEMASK_VAL_HIDEFROMSEARCH) != 0)
							_isExcluded = true;

						break;

					case VOConstants.ATTRIBUTENAME_SELFSUSPENDEDFLAG: 
						if (Convert.ToString(Evidence.Evidence) == VOConstants.BOOL_TRUE)
							_isExcluded = true;

						break;

					case VOConstants.ATTRIBUTENAME_REGIONID:
						// The RegionID must be greater than 0.
						if (Convert.ToInt32(Evidence.Evidence) > 0)
							_hasValidRegion = true; 
						else
						{
							_isExcluded = true;
							_hasValidRegion = false;
						}

						break;

					case VOConstants.ATTRIBUTENAME_GENDERMASK: 
						_hasValidGenderMask = true;
 
						break;

					case VOConstants.ATTRIBUTENAME_BIRTHDATE:
						try
						{
							DateTime birthdate = Convert.ToDateTime(Evidence.Evidence);

							if (DateTime.Compare(birthdate, DateTime.Today.AddYears(-18)) < 0 && DateTime.Compare(birthdate, DateTime.Now.AddYears(-100)) > 0)
								_hasValidBirthDate = true;
						}
						catch
						{
							_isExcluded = true;
						}
						break;

					case VOConstants.ATTRIBUTENAME_HASPHOTOFLAG:
						if (Convert.ToString(Evidence.Evidence) == VOConstants.BOOL_TRUE)
							_hasPhoto = true;

						break;

					case VOConstants.ATTRIBUTENAME_HEADLINE:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_IDEALVACATION:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_GREWUPIN:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_ESSAYDESCRIBEHUMOR:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_ESSAYFIRSTTHINGNOTICEABOUTYOU:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_ESSAYFRIENDSDESCRIPTION:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_ESSAYORGANIZATIONS:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_FAVORITEBOOKAUTHOR:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_ABOUTME:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_WHENIHAVETIME:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_FACEPAGEINTRODUCTION:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;

					case VOConstants.ATTRIBUTENAME_CANNOTDOWITHOUT:
						if (Evidence.Evidence != null && Convert.ToString(Evidence.Evidence) != string.Empty)
							_hasEssay = true;

						break;
				}
#if DEBUG
				System.Diagnostics.Trace.WriteLineIf(_isExcluded, "FAILED ON EXCLUSION RULE: (NAME: " + Evidence.EvidenceName + ") (VALUE: " + Convert.ToString(Evidence.Evidence) + ")");
#endif
				if (_isExcluded)
					_exclusionReason = "FAILED ON EXCLUSION RULE: (NAME: " + Evidence.EvidenceName + ") (VALUE: " + Convert.ToString(Evidence.Evidence) + ")";

				return _isExcluded;
			}
		}

		public string ExclusionReason
		{
			get
			{
				return _exclusionReason;
			}
		}

		#region Composite Rules
		private bool Rule_MemberHasPhotoOrEssay()
		{
			// Only for AS.
			if (_communityID == 1 && !_hasPhoto && !_hasEssay)
				return false;
			else
				return true;
		}
		#endregion

		/// <summary>
		/// Implements an evidence object for use with exlusion rules.
		/// </summary>
		public class ExclusionEvidence : IFastExclusionEvidence
		{
			private string _evidenceName;
			private object _evidence;

			/// <summary>
			/// Constructor.
			/// Creates an Exlusion Evidence object instance.
			/// </summary>
			/// <param name="EvidenceName">An Evidence name.</param>
			/// <param name="Evidence">The content of this evidence instance.</param>
			public ExclusionEvidence (string EvidenceName, object Evidence)
			{
				_evidenceName = EvidenceName;
				_evidence = Evidence;
			}

			/// <summary>
			/// Gets or sets this instance's Evidence name.
			/// </summary>
			public string EvidenceName
			{
				get
				{
					return _evidenceName;
				}
				set
				{
					_evidenceName = value;
				}
			}

			/// <summary>
			/// Gets or sets this instance's evidence content.
			/// </summary>
			public object Evidence
			{
				get
				{
					return _evidence;
				}
				set
				{
					_evidence = value;
				}
			}
		}
	}
}

