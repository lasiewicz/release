using System;

using Matchnet.FAST.Push.Interfaces;
namespace Matchnet.FAST.Push.ValueObjects {
	/// <summary>
	/// Represents a reference to a member in a domain. 
	/// This class mainly used to signal and queue up pointers so that a member's profile can be fetched.
	/// </summary>
	[Serializable]
	public class FastPushPointer :IFastPushPointer {
		private int _MemberID;
		private int _CommunityID;
		private int _SiteID;
		private IndexAction _IndexAction;

		/// <summary>
		/// Constructor
		/// </summary>
		public FastPushPointer(){
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="MemberID">The Member ID of the member</param>
		/// <param name="CommunityID">The CommunityID for which this profile is intended</param>
		/// <param name="SiteID">The SiteID for which this profile is intended</param>
		/// <param name="IndexAction">The intented action to perform on this pointer</param>
		public FastPushPointer(	int MemberID, int CommunityID, int SiteID, IndexAction IndexAction){
			_MemberID = MemberID;
			_CommunityID = CommunityID;
			_SiteID = SiteID;
			_IndexAction = IndexAction;
		}

		#region IFastPushPointer Members

		/// <summary>
		/// The Member ID of the member
		/// </summary>
		public int MemberID {
			get { 
				return _MemberID; 
			}
			set {
				_MemberID = value;
			}
		}

		/// <summary>
		/// SiteID (Private LableID in legacy)
		/// </summary>
		public int SiteID {
			get {
				return _SiteID;
			}
			set {
				_SiteID = value;
			}

		}

		/// <summary>
		/// The indexer action requested to be performed on this member
		/// </summary>
		public IndexAction ActionID {
			get {
				return _IndexAction;
			}
			set {
				_IndexAction = value;
			}
		}

		/// <summary>
		/// CommunityID (DomainID in legacy)
		/// </summary>
		public int CommunityID {
			get {
				return _CommunityID;
			}
			set {
				_CommunityID = value;
			}

		}

		#endregion
	}
}
