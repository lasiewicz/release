@ECHO ***************************
@ECHO *** Deploy to fspush01?
pause

psexec \\fspush01 net stop FASTPush
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Push.Service\ \\fspush01\c$\matchnet\bin\Service\Matchnet.FAST.Push.Service * /s /xo /r:999 /w:0
psexec \\fspush01 net start FASTPush


@ECHO ***************************
@ECHO *** Deploy to fspush02?
pause

psexec \\fspush02 net stop FASTPush
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Push.Service\ \\fspush02\c$\matchnet\bin\Service\Matchnet.FAST.Push.Service * /s /xo /r:999 /w:0
psexec \\fspush02 net start FASTPush

pause


