@ECHO ON
@ECHO ***********************************************
@ECHO Matchmail.Search.Service depends on FASTQuery. Must stop former first!
@ECHO ***********************************************

@ECHO ***************************
@ECHO *** Deploy to query01?
pause

psexec \\query01 net stop Matchnet.Search.Service
psexec \\query01 net stop FASTQuery
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Query.Service\ \\query01\c$\matchnet\bin\Service\Matchnet.FAST.Query.Service * /s /xo /r:999 /w:0
psexec \\query01 net start FASTQuery
psexec \\query01 net start Matchnet.Search.Service

@ECHO ***************************
@ECHO *** Deploy to query02?
pause
psexec \\query02 net stop Matchnet.Search.Service
psexec \\query02 net stop FASTQuery
pause
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.FAST.Query.Service\ \\query02\c$\matchnet\bin\Service\Matchnet.FAST.Query.Service * /s /xo /r:999 /w:0
psexec \\query02 net start FASTQuery
psexec \\query02 net start Matchnet.Search.Service

@ECHO ***************************
@ECHO *** DONE
@ECHO ***************************
sc \\query01 query FASTQuery
sc \\query01 query Matchnet.Search.Service
sc \\query02 query FASTQuery
sc \\query02 query Matchnet.Search.Service

pause


