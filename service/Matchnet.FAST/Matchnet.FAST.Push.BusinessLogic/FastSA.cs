using System;
using System.Diagnostics;

using Matchnet.FAST.Push.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;

using no.fast.ds.search;
using no.fast.ds.common;


namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Summary description for FastSA.
	/// </summary>
	public class FastSA {
		
//		private static IFastSearchEngineFactory _FastEngineFactory;
		private static string _FASTNameServiceLocation;
		private static FastFarm [] _Farms;

		public static readonly FastSA Instance = new FastSA();
		
		private FastSA()
		{
			_FASTNameServiceLocation = RuntimeSettings.GetSetting(Constants.CFG_FAST_NAME_SERVICE_LOCATION);
			Init();
		}

		private void Init(){
			string [] farms = _FASTNameServiceLocation.Split(new char [] {',','='});
			if (farms.Length == 1)
			{
				_Farms = new FastFarm [] {new FastFarm("SingleFarm",farms[0])};
				string str = _Farms[0].FarmName + "=" + _Farms[0].FarmLocation;

				showInitInfo();
				no.fast.ds.common.Initializer.Init(str);
			}
			else if ( farms.Length % 2 != 0){
				throw new Exception("FastSA.Init() can't parse [" + _FASTNameServiceLocation + "]. Make sure it's well formed: <name1>=<location1>,<n2>=<location2>...");
			}
			else
			{
				_Farms = new FastFarm[farms.Length / 2];
				for (int i =0; i < _Farms.Length; i++)
				{
					_Farms[i] = new FastFarm( farms[i*2],farms[i*2 + 1]);
				}

				showInitInfo();
				no.fast.ds.common.Initializer.Init(_FASTNameServiceLocation);
			}
		}

		/// <summary>
		/// The FAST name service location configured
		/// </summary>
		/// <returns>An address for the FAST farm name service server</returns>
		public string GetNameServiceLocation(){
			return _FASTNameServiceLocation;
		}


		/// <summary>
		/// FAST Collection name is "domainid" + communityID.ToString()
		/// </summary>
		/// <returns></returns>
		public string GetCollectionName(int id)
		{	
			return RuntimeSettings.GetSetting(Constants.CFG_FAST_COLLECTION_NAME_BASE) + id.ToString();
		}

		/// <summary>
		/// The list of fast farms that were initialized.
		/// </summary>
		public FastFarm [] FastFarms {
			get { return _Farms ; }
		}

		/// <summary>
		/// Can only be called once.
		/// </summary>
		public bool Initialize()
		{
			return no.fast.ds.common.Initializer.IsInitialized();
		}


		private void showInitInfo(){
			string farmInfo = string.Empty;
			foreach( FastFarm ff in _Farms){
				farmInfo += "[ " + ff.FarmName + " -> " + ff.FarmLocation + " ]";
			}
			Metrics.Instance.WriteInformation("Initializing with " + farmInfo);
		}

		public class FastFarm {
			public string FarmName;
			public string FarmLocation;
			public FastFarm(string farmName, string farmLocation)
			{
				FarmName = farmName;
				FarmLocation = farmLocation;
			}
		}
	}
	
}


