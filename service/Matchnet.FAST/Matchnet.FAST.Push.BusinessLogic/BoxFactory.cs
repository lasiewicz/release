using System;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Summary description for BoxFactory.
	/// </summary>
	public class BoxFactory {

		/* Four phases: 
		 *  0:	0,0 
		 *  1:	+ 1/2 box lat
		 *  2:	+ 1/2 box lng
		 *  3:	+ 1/2 box lat and + 1/2 box lng
		 */
		public const int MAXPHASES = 4;
		/* Max number of bands */
		public const int MAXBANDS  = 8;

		private static Config _Cfg = new Config(
			// If earth is 24k miles around, boxes in miles are (3/4 2.4 8 24 80 480)
			(double)-2.55, (double)2.55,
			(double)-3.15, (double)3.15,
			6, 
			new int [] { 32000, 10000, 3000, 1000, 300, 50 }
			);

		#region Structurs
		/* Configuration Data */
		public struct Config {		
			// min and max latitude as found in data (+/- 2.55)
			public double minLat, maxLat;
			// min and max longitude as found in data (+/- 3.15)
			public double minLng, maxLng;
			// how many bands of different size boxes 
			public int numBands;
			// # boxes to gird the earth (more means smaller boxes) in each band
			public int [] BandBoxCount;

			public Config(double MinLat, double MaxLat, double MinLng, double MaxLng,int numBands, int [] BandBoxCount){
				this.minLat = MinLat;
				this.maxLat = MaxLat;
				this.minLng = MinLng;
				this.maxLng = MaxLng;
				this.numBands = numBands;
				this.BandBoxCount = BandBoxCount;
			}
		}



		/*********************************************/
		/* Describe a box in a phase in a band		 */
		public struct Box {
			public int latBoxID;
			public int lngBoxID;
			public string name;

			public Box(int latBoxID, int lngBoxID, string name){
				this.latBoxID = latBoxID;
				this.lngBoxID = lngBoxID;
				this.name = name;
			}
		}


		/* Define a band of several phases
			 * ??? "Best Phase" (where test loc is closest to center of box not calc'd yet		 */
		public struct Band {
			public int    bestPhase;
			public double bestDistance;
			public Box [] byPhase;

			public Band( int bsetPhase, double bestDistance) {
				this.bestPhase = bsetPhase;
				this.bestDistance = bestDistance;
				this.byPhase = new Box[BoxFactory.MAXPHASES];
			}
		} 


		/* Define a location as boxes in multiple phases in mutiple bands  */
		public struct  Location {
			// Absolute lat/lng of this location
			public double lat;
			public double lng;
			// This lat/lng expressed as pct of known range of all lats/lngs
			public double NormalizedLat;
			public double NormalizedLng;
			// Info by band then phase about the boxes at this location
			public Band [] byBand;// = new RbBandRec[MAXBANDS] ;


			public Location(double lat, double lng, double NormalizedLat, double NormalizedLng){
				this.lat = lat;
				this.lng = lng;
				this.NormalizedLat = NormalizedLat;
				this.NormalizedLng = NormalizedLng;
				this.byBand = new Band[_Cfg.BandBoxCount.Length];
			}
		}


		#endregion Structures


		#region Methods

		private static void MakeLocationBand(Location loc, int BandIndex) {
			int bandSize = _Cfg.BandBoxCount[BandIndex];
			Band band = new Band(0,0);			

			for (int PhaseIndex = 0; PhaseIndex < MAXPHASES; PhaseIndex++){
				Box  box = new Box();

				// Are we phased?
				bool IsInPhaseLat = ((PhaseIndex & 1) == 1) ? true : false;
				bool IsInPhaseLng = ((PhaseIndex & 2) == 2) ? true : false;

				// Adjust our percent around the world by half a box for proper phase 
				// Since origins of each phase are shifted up, loc must be shifted down
				double latAdj = loc.NormalizedLat;
				if (IsInPhaseLat) { 
					latAdj -= ((double)0.5 / (double)bandSize);
					if (latAdj < 0.0) latAdj += 1.0;
				}
				double lngAdj = loc.NormalizedLng;
				if (IsInPhaseLng) {
					lngAdj -= ((double)0.5 / (double)bandSize);
					if (lngAdj < 0.0) lngAdj += 1.0;
				}

				// Which box are we in
				box.latBoxID = (int)(latAdj * (double)bandSize) % bandSize;
				box.lngBoxID = (int)(lngAdj * (double)bandSize) % bandSize;
				// Generate its name - can use fewer digits on bands w/ fewer boxes
				box.name = string.Format("b{0}p{1}t{2}g{3}", BandIndex, PhaseIndex, box.latBoxID, box.lngBoxID);

				band.byPhase[PhaseIndex] = box;
				loc.byBand[BandIndex] = band;

			}

		}

		/* Populate an already allocated RbLoc object at this lat/lng based on cfg
		 * ??? Most accurate phase per band not calculated yet */
		public static Location rbCalculate(double xlat, double xlng) {
			Location loc = new Location(0,0,0,0);

			int BandIndex;
			// Relative position on the grid of the world
			double NormalizedLat = (xlat - _Cfg.minLat) / (_Cfg.maxLat - _Cfg.minLat);
			double NormalizedLng = (xlng - _Cfg.minLng) / (_Cfg.maxLng - _Cfg.minLng);
			// Catch any bad data
			if (NormalizedLat < 0.0 || NormalizedLat >= 1.0) NormalizedLat = 0.0;
			if (NormalizedLng < 0.0 || NormalizedLng >= 1.0) NormalizedLng = 0.0;
			// Load the loc structure with our lat/lng info 
			loc.lat = xlat; loc.NormalizedLat = NormalizedLat;
			loc.lng = xlng; loc.NormalizedLng = NormalizedLng;
			// Do every band, every phase
			for (BandIndex = 0; BandIndex < _Cfg.numBands; BandIndex++){
				MakeLocationBand(loc, BandIndex);
				//				for (PhaseIndex = 0; PhaseIndex < MAXPHASES; PhaseIndex++){
				//					rbDoBox(loc, BandIndex, PhaseIndex);
				//				}
			}
			return loc;
		}

		#endregion Methods

		#region tests
		/* Dump the contents of an RbLoc with printf
		 */
		//		int rbLocShow(RbCfgRec cfg, Location loc) {
		//			int BandIndex, PhaseIndex;
		//			printf("* Location) Lat: %1.4f(%1.4f) Lng: %1.4f(%1.4f)\n",
		//				loc.lat, loc.NormalizedLat, loc.lng, loc.NormalizedLng);
		//			for (BandIndex = 0; BandIndex < cfg.numBands; BandIndex++) {
		//				int bsz = cfg.bandSizes[BandIndex];
		//				printf("Band%d (%d): \n", BandIndex, bsz);
		//				for (PhaseIndex = 0; PhaseIndex < MAXPHASES; PhaseIndex++) {
		//					RbBox box = &(loc.byBand[BandIndex].byPhase[PhaseIndex]);
		//					printf("     [%d %s\t%d\t%d]\n", PhaseIndex, box.name, box.lat, box.lng);
		//				}
		//			}
		//			return 0;
		//		}

		//		/*******************/
		//		/***** Testing *****/
		//		/*******************/
		//
		//		// test config
		//		static RbCfgRec testCfg = {
		//									  (double)0.0, (double)2.0,
		//									  (double)0.0, (double)3.0,
		//									  2, { 2000, 300 }
		//								  };
		//
		// test data
		//		static struct { double lat, lng; } testData[] = { {
		//		0.0, 0.0 }, {
		//		1.0, 0.0 }, {
		//		1.001, 2.003 }, {
		//		(2.0-.0007), (3.0-0.0007) }, {
		//		1.9999, 2.9999 }
		//	};
		//
		//		// test main
		//		int xmain() {
		//			int i;
		//			Location location;
		//
		//			for (i=0 ; i<(sizeof(testData)/sizeof(testData[0])); i++) {
		//																		  rbCalculate(&testCfg, &location, testData[i].lat, testData[i].lng);
		//																		  rbLocShow(&testCfg, &location);
		//																	  }
		//			return 1;
		//		}

		#endregion
	}
}
