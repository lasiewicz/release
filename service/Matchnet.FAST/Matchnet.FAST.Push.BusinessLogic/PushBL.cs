using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;

using Matchnet.FAST.Push.Interfaces;
using Matchnet.FAST.Push.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.FAST.Push.BusinessLogic 
{
	/// <summary>
	/// Business Logic for the Bulk import.
	/// This includes both 1 at a time queue injection of document pointers and batch oriented operations
	/// </summary>
	public class PushBL 
	{
		#region member variables	
		/// <summary>
		/// The amount of documents to in a single batch
		/// </summary>
		public int BulkImportBatchSize = Convert.ToInt32(RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_BATCH_SIZE));

		/// <summary>
		/// Flag whether to log action - save to the bulk load DB every memberid this instance processed and if it got pushed or deleted.
		/// True = log the actions 
		/// False = don't log the actions
		/// </summary>
		public static bool _bulkImport_LogActionFlag = Boolean.Parse(RuntimeSettings.GetSetting(Constants.CFC_BULK_IMPORT_MODE_LOGACTION));

		/// <summary>
		/// connection string to where batches of member ID's are to be gotten
		/// </summary>
		public string _bulkImport_BatchConnection = RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_BATCH_CONNECTION);

		private DocumentManager _DocumentManager;
		private Queue _Queue;
		private int _CommunityID;

		private bool _IsRunnable = true;


		#endregion

		#region methods

		/// <summary>
		/// Constructor
		/// </summary>
		public PushBL(Queue queue, int CommunityID) 
		{
			_Queue = queue;
			_CommunityID = CommunityID;
			_DocumentManager = new DocumentManager(CommunityID);
		}


		/// <summary>
		/// Run this instance until Stop is called.
		/// This will loop the Push Documents From Queue method.
		/// </summary>
		public void Run()
		{
			_IsRunnable = true;
			int SleepTime = 1;
			Metrics.pcPushThreadsRunning.Increment();
			while (_IsRunnable)
			{
				if (!PushDocumentsFromQueue())
				{
					if (SleepTime < 512) // about 8 minutes
					{
						SleepTime *= 2;
					}
					Thread.Sleep(SleepTime * 1000);
				}
				else
				{
					if (SleepTime > 1)
					{
						SleepTime /= 2;
					}
				}

			}	
			Metrics.pcPushThreadsRunning.Decrement();
			Metrics.Instance.WriteInformation(string.Format("Run has exited the after last operation complete on thread [{0}]",Thread.CurrentThread.Name));
		}


		/// <summary>
		/// Signals the object to stop. 
		/// Actual stop will occure next round after the current round if any is complete.
		/// </summary>
		public void Stop()
		{
			_IsRunnable = false;
			Trace.WriteLine(string.Format("Thread [{0}] got the stop signal", Thread.CurrentThread.Name));
		}


		/// <summary>
		/// Pushes a single document to FAST, using the pointer to retreive all data and populate
		/// </summary>
		/// <param name="pointer">A pointer to a member to populate document from</param>
		/// <returns>True: pushed successfully, False: failed push for any reason.</returns>
		public bool PushDocument(IFastPushPointer pointer)
		{
			bool SuccessFlag = true;
			FastDocument fd = new FastDocument(pointer);	
			//TODO: select appropriate document manager instance
			_DocumentManager.PushDocument(fd);
			if (_bulkImport_LogActionFlag) 
			{
				SaveMemberPushStatus( new FastDocument [] {fd});
			}

			if (_DocumentManager.PushError)
			{
				SuccessFlag = false;
				_Queue.EnqeueErrorProfilePointer(pointer);
			}
#if DEBUG
			Trace.WriteLine(string.Format("Pushed Test Profile {0} ({1}) PushError= {2} ", fd.MemberID, fd.CommunityID, _DocumentManager.PushError ));
#endif
			return SuccessFlag;
		}
		

		
		/// <summary>
		/// Pushes a bunch of documents at once.
		/// </summary>
		/// <param name="UsePushQueue">Whether to use the push queue or an error queue</param>
		/// <returns>True: processed at least one item successfully, False: no items to process or failed push for any reason.</returns>
		public bool PushDocumentsFromQueue()
		{
			IFastPushPointer [] MemberPointers;
			FastDocument [] profiles;
			TimeSpan tsStart,tsGotBatch,tsEndPush;
			string [] PushResults ;


			tsStart = new TimeSpan(DateTime.Now.Ticks);
				
			try 
			{
				MemberPointers = _Queue.DequeueProfilePointers(BulkImportBatchSize);
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("PushDocumentsFromQueue() -> DequeueProfilePointers() failed.\n\n" + ex.Message);
				return false;
			}

			if (MemberPointers == null || MemberPointers.Length == 0)
			{
				Trace.WriteLine("PushDocumentsFromQueue() got nothing from GetMemberListFromQueue().");
				return false;
			}

			try 
			{
				profiles = new FastDocument[MemberPointers.Length];
				for (int i = 0; i < MemberPointers.Length; i++) 
				{
					profiles[i] = new FastDocument( MemberPointers[i]);
				}
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("PushDocumentsFromQueue() -> build collection of documents failed.\n\n" + ex.ToString());
				SaveErrorProfilePointers(MemberPointers);
				return false;
			}

			tsGotBatch = new TimeSpan(DateTime.Now.Ticks);
		
			if (profiles.Length > 0)
			{
				try 
				{
					PushResults = _DocumentManager.PushDocuments(profiles);
				}
				catch (Exception ex)
				{
					SaveErrorProfilePointers(MemberPointers);
					Metrics.Instance.WriteError("PushDocumentsFromQueue() -> PushDocuments() failed.\n\n" + ex.ToString());
					return false;
				}
				finally
				{
					if (_bulkImport_LogActionFlag) 
					{
						SaveMemberPushStatus(profiles);
					}
					tsEndPush = new TimeSpan(DateTime.Now.Ticks);
#if DEBUG
					Trace.WriteLine(string.Format("push {0} docs #{1} ms Batch from Queue#{2} ms push FAST#{3} ms elapsed total", profiles.Length, tsGotBatch.Subtract(tsStart).TotalMilliseconds, tsEndPush.Subtract(tsGotBatch).TotalMilliseconds, tsEndPush.Subtract(tsStart).TotalMilliseconds ));
#endif
				}

				if (_DocumentManager.PushError)
				{
					SaveErrorProfilePointers(MemberPointers);
					return false;	
				}
				else 
				{
					return true;
				}
			}
			else 
			{
				return false;
			}
				
		} 
	

		private void SaveErrorProfilePointers( IFastPushPointer [] MemberPointers) 
		{
			try 
			{
				foreach( IFastPushPointer ptr in MemberPointers)
				{
					_Queue.EnqeueErrorProfilePointer(ptr);
				}
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("SaveErrorProfilePointers() failed. Unsafe to continue.\n\n" + ex.ToString());
				// catastrophic failure, items will be lost.
				System.Threading.Thread.CurrentThread.Abort();
			}
		}


		private void SaveMemberPushStatus(FastDocument [] profiles)
		{
			SqlCommand cmd = new SqlCommand("up_MemberPushStatus_Save");
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("@CommunityID",SqlDbType.Int);
			cmd.Parameters.Add("@MemberID", SqlDbType.Int);
			cmd.Parameters.Add("@PushStatusID", SqlDbType.TinyInt);

			cmd.Connection = new SqlConnection(_bulkImport_BatchConnection);

			try 
			{
				cmd.Connection.Open();
				for (int i = 0; i < profiles.Length; i++)
				{
					cmd.Parameters["@CommunityID"].Value = profiles[i].CommunityID;
					cmd.Parameters["@MemberID"].Value = profiles[i].MemberID;
					cmd.Parameters["@PushStatusID"].Value = (profiles[i].IsExcluded) ? MemberPushStatus.DeletedFromIndex  : MemberPushStatus.PushedToIndex;
					
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("SaveMemberPushStatus() failed.\n" + ex.ToString());
			}
			finally
			{
				cmd.Connection.Close();
			}			
		}


		#endregion

	}
}