using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;

//using Matching.Fast.Exceptions;
using Matchnet.FAST.Push.ValueObjects;

namespace Matchnet.FAST.Push.BusinessLogic {

	/// <summary>
	/// Represents a configuration object with alleged dynamic refresh to allow multiple queue with changing locations
	/// (This should be internal?? NH.)
	/// </summary>
	public class QueueConfiguration {
		/// <summary>
		/// The single instance of the QueueConfiguration object
		/// </summary>
		public static readonly QueueConfiguration Instance = new QueueConfiguration();

		private string [] _PushQueuePaths;
		private string _ErrorQueuePath;

		private QueueConfiguration() {
			// HACK: this needs to be multiple paths allowed.
			_PushQueuePaths = new string [] { ConfigurationSettings.AppSettings[Constants.CFG_QUEUE_UpdateQueueAddress] };
			_ErrorQueuePath = ConfigurationSettings.AppSettings[Constants.CFG_QUEUE_ErrorQueueAddress];

		}

		/// <summary>
		/// Returns a physical path for a queue with Push itmes given a key.
		/// </summary>
		/// <param name="key">some number to base queue selection on (think MOD)</param>
		/// <returns>a queue path given a key</returns>
		public string PushQueuePath(int key) {
			try {
				lock (this) {
					// if the key is zero, balance between queues
					int partition = 0;

					if (key > 0) {
						partition = (key % _PushQueuePaths.Length);
					} 
					else {
						partition = (new Random((int)DateTime.Now.Ticks)).Next( _PushQueuePaths.Length);
					}

					return (string)_PushQueuePaths[partition];
				}
			} 
			catch (Exception ex) {
				Metrics.Instance.WriteError("Failure to retrieve Queue Configuration " + ex.ToString());
				throw new Exception("Failure to retrieve Queue Configuration", ex);
			}
		}

		public string ErrorQueuePath{
			get {
				return _ErrorQueuePath;
			}
		}

		
		
	}
}
