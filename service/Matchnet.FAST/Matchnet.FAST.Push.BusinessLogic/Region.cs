using System;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// A geographic location object
	/// </summary>
	public class Region : ICacheable {
		private float _Longitude;
		private float _Latitude;	
		private string _Country;
		private string _City;
		private string _State;
		private string _Zip;
		private int _RegionID;
		private int _Depth1RegionID;
		private int _Depth2RegionID;
		private int _Depth3RegionID;
		private int _Depth4RegionID;
		private int _areaCode;

		private static CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
		private static CacheItemMode _CacheItemMode = CacheItemMode.Sliding;
		private static int _CacheTTLSeconds = 60 * 20;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="RegionID"></param>
		/// <param name="Longitude"></param>
		/// <param name="Latitude"></param>
		/// <param name="Country"></param>
		/// <param name="City"></param>
		/// <param name="State"></param>
		/// <param name="Zip"></param>
		/// <param name="Depth1RegionID"></param>
		/// <param name="Depth2RegionID"></param>
		/// <param name="Depth3RegionID"></param>
		/// <param name="Depth4RegionID"></param>
		public Region(int RegionID, float Longitude, float Latitude, string Country, string City, string State, string Zip , int Depth1RegionID, int Depth2RegionID, int Depth3RegionID, int Depth4RegionID, int AreaCode) {
			_Longitude = Longitude ;
			_Latitude = Latitude ;	
			_Country = Country ;
			_City = City;
			_State = State;
			_Zip = Zip;
			_RegionID = RegionID;
			_Depth1RegionID = Depth1RegionID;
			_Depth2RegionID = Depth2RegionID;
			_Depth3RegionID = Depth3RegionID;
			_Depth4RegionID = Depth4RegionID;
			_areaCode = AreaCode;
		}


		/// <summary>
		/// RegionID
		/// </summary>
		public int RegionID{
			get {
				return _RegionID;
			}
		}

		/// <summary>
		/// Longitude (Radians)
		/// </summary>
		public float Longitude {
			get {
				return _Longitude;
			}
			set {
				_Longitude = value;
			}
		}

		/// <summary>
		/// Latitude (Radians)
		/// </summary>
		public float Latitude {
			get {
				return _Latitude;
			}
			set {
				_Latitude = value;
			}
		}

		/// <summary>
		/// City name 
		/// </summary>
		public string City {
			get {
				return _City;
			}
			set {
				_City = value;
			}
		}

		/// <summary>
		/// Postal Code or Zip Code string
		/// </summary>
		public string Zip {
			get {
				return _Zip;
			}
			set {
				_Zip = value;
			}
		}

		/// <summary>
		/// Country name (full)
		/// </summary>
		public string Country{
			get {
				return _Country;
			}
			set {
				_Country = value;
			}
		}

		/// <summary>
		/// State or Province name (or whatever is below Country for this local)
		/// </summary>
		public string State{
			get {
				return _State;
			}
			set {
				_State = value;
			}
		}


		/// <summary>
		/// Country regionid
		/// </summary>
		public int Depth1RegionID { 
			get { 
				return _Depth1RegionID; 
			} 
			set { 
				_Depth1RegionID = value; 
			} 
		}

		/// <summary>
		/// State level regionid
		/// </summary>
		public int Depth2RegionID { 
			get { 
				return _Depth2RegionID; 
			} 
			set { 
				_Depth2RegionID = value; 
			} 
		}

		/// <summary>
		/// City level regionid
		/// </summary>
		public int Depth3RegionID { 
			get { 
				return _Depth3RegionID; 
			} 
			set { 
				_Depth3RegionID = value; 
			} 
		}

		/// <summary>
		/// Zip level regionid
		/// </summary>
		public int Depth4RegionID { 
			get { 
				return _Depth4RegionID; 
			} 
			set { 
				_Depth4RegionID = value; 
			}
		}

		public int AreaCode 
		{ 
			get 
			{ 
				return _areaCode; 
			} 
			set 
			{ 
				_areaCode = value; 
			}
		}


		/// <summary>
		/// The string representation of object
		/// </summary>
		/// <returns>a string representation of this instance values</returns>
		public override string ToString() {

			return string.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}",_RegionID,_Longitude,_Latitude,_Country,_State,_City,_Zip);
		}
		#region ICacheable Members

		public int CacheTTLSeconds {
			get {
				return _CacheTTLSeconds;
			}
			set {
				_CacheTTLSeconds = value;

			}
		}

		public Matchnet.CacheItemMode CacheMode {
			get {
				return _CacheItemMode;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority {
			get {
				return _CacheItemPriorityLevel;
			}
			set {
				_CacheItemPriorityLevel = value;
			}
		}


		public string GetCacheKey() {
			return Constants.PUSH_REGION_CACHE_PREFIX + RegionID.ToString();
		}


		#endregion
	}
}