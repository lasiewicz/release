using System;
using System.Collections;
using System.Diagnostics;

using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Provides Queue names for the application.
	/// Each community has a queue name, retreived by using the indexer.
	/// A single error queue path is available via the property ErrorQueuePath. 
	/// </summary>
	public class QueueConfiguration {
		private Hashtable _CommunityQueues;
		private string _ErrorQueue;

		private int [] _CommunityIDs;
		private int [] _ThreadsPerQueue;

		/// <summary>
		/// Constructor.
		/// When called, this will use the Matchnet.Configuration.ServiceAdapter object to get settings and initialize queue paths.
		/// </summary>
		public QueueConfiguration()	{
			try {
				string [] QNames = QueueUtility.GetQueueNames();
				string [] CommunityID = QueueUtility.GetCommunityIDs();

				_CommunityIDs = new int [QNames.Length]; 
				_CommunityQueues = new Hashtable(QNames.Length);

				for( int i = 0; i < QNames.Length; i++){
					int cid = Int32.Parse(CommunityID[i]);
					_CommunityQueues.Add(cid, @"FormatName:Direct=OS:" + QNames[i]);
					_CommunityIDs[i] = cid;
					Trace.WriteLine("\t-->" + _CommunityQueues[cid] + " idx [" + i.ToString() + "] => " + cid);
				}
			
				_ErrorQueue = @"FormatName:Direct=OS:" + QueueUtility.GetErrorQueueName();
				Trace.WriteLine("\t* -->" + _ErrorQueue);

				_ThreadsPerQueue = new int [CommunityID.Length]; // alloc as many as there are communities
				QueueUtility.GetThreadsConfig().CopyTo(_ThreadsPerQueue,0); // assign as many as configured
			}
			catch (Exception ex){
				throw new Exception("Queue Configuration Constructor failed loading", ex);
			}
		}

		/// <summary>
		/// Gets a full queue path for a given community id
		/// </summary>
		public string this[ int CommunityID]{
			get {
				return _CommunityQueues[CommunityID] as string;
			}
		}

		public Queue GetQueue(int CommunityID) {
			return new Queue(_CommunityQueues[CommunityID] as string,_ErrorQueue);
		}

		/// <summary>
		/// Gets the full queue path for the error items queue
		/// </summary>
		public string ErrorQueuePath{
			get{
				return _ErrorQueue;
			}
		}

		/// <summary>
		/// The list of the community ID's for which there is a queue configuration
		/// </summary>
		public int [] CommunityList{
			get {
				return _CommunityIDs;
			}
		}

		/// <summary>
		///  The number of community queues available ( not including the error queue!)
		/// </summary>
		public int Count{
			get{
				return _CommunityQueues.Count;
			}
		}

		/// <summary>
		/// The number of threads per community / queue to use. Ordinal community 1 is getting threads in ordinal threads 1..
		/// </summary>
		public int [] ThreadsPerQueue{
			get {
				return _ThreadsPerQueue;
			}
		}


	}
}
