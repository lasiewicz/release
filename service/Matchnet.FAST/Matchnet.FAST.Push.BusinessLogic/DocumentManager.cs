using System;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Configuration;

using Matchnet.FAST.Push.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;

using no.fast.ds.content;

namespace Matchnet.FAST.Push.BusinessLogic 
{
	/// <summary>
	/// Represents a FAST document manager, thinly vailed and properly initialized.
	/// The document manager is created as an instance for each domainid / community ID.
	/// This is because the FAST initialization requires a target collection upon initialization.
	/// 
	/// The class exposes methods to push a single or multiple documents at a time to the set collection.
	/// The push methods are blocking calls. The thread gets released based on a callback that sets the signal on the wait handles.
	/// 
	/// The public property PushError is set on a per call basis. So a calling app should check the error status if it wants to avoid consecutive push errors.
	/// </summary>
	public class DocumentManager : IContentCallback 
	{
		private IContentFactory _contentFactory;
		private bool _PushError = false;
		private TimeSpan tsPushCall, tsFinalCallback;
		private bool _BulkImport_NoRemoveFlag;
		private int  _FarmCount = FastSA.Instance.FastFarms.Length;

		private IContentManagerFactory [] _contentManagerFactories;
		private IContentManager [] _contentManagers;



		// Signals from FAST
		private ManualResetEvent _Signal_CompletedProcessing = new ManualResetEvent(false);
		private ManualResetEvent _Signal_ReceivedByIndex = new ManualResetEvent(false);
		private ManualResetEvent _Signal_PersistedByIndex = new ManualResetEvent(false);

		// tracking of callback returns
		private string [] _FASTAddBatchIDs;
		private string [] _FASTRemoveBatchIDs;

		private bool [] _CompletedProcessingFlags;
		private bool [] _ReceivedByIndexFlags;
		private bool [] _PersistedByIndexFlags;
		


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="SearchCollectionID">The id of the collection this instance will be responsible for.
		/// This id is concatenated with the configured FASTPUSHSVC_COLLECTIONNAMEBASE to form one of the FAST collection names.</param>
		public DocumentManager(int SearchCollectionID) 
		{
			try 
			{
				
				_contentManagerFactories = new IContentManagerFactory[_FarmCount];
				_contentManagers = new IContentManager[_FarmCount];

				_Signal_CompletedProcessing	= new ManualResetEvent(false);
				_Signal_ReceivedByIndex 	= new ManualResetEvent(false);
				_Signal_PersistedByIndex	= new ManualResetEvent(false);


				_FASTAddBatchIDs	= new string [_FarmCount];
				_FASTRemoveBatchIDs = new string [_FarmCount];

				_CompletedProcessingFlags	= new bool [_FarmCount];
				_ReceivedByIndexFlags		= new bool [_FarmCount];
				_PersistedByIndexFlags		= new bool [_FarmCount];

				for (int i = 0; i < _FarmCount; i++) 
				{
					string farmName = FastSA.Instance.FastFarms[i].FarmName;
					_contentManagerFactories[i] = Factory.CreateContentManagerFactory("fds/contentdistributor", farmName);
				}

				_contentFactory = Factory.CreateContentFactory();

				for (int i =0; i < _FarmCount; i++)
				{
					_contentManagers[i] = _contentManagerFactories[i].Create(FastSA.Instance.GetCollectionName(SearchCollectionID), this);
				}

				try 
				{
					_BulkImport_NoRemoveFlag = Boolean.Parse(RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_MODE_NOREMOVE));
				}
				catch
				{
					_BulkImport_NoRemoveFlag = false;
				}
			} 
			catch (Exception ex) 
			{
				Metrics.Instance.WriteError("Failure Initializing Content API\n\t" + ex.ToString());
				throw ex;
			}
		}
	

		/// <summary>
		/// Gets whether last operation resulted in an error
		/// </summary>
		public bool PushError
		{
			get 
			{ 
				return _PushError;
			}
		}


		/// <summary>
		/// Pushes a single document to the indexing process
		/// </summary>
		/// <param name="document">the matchnet document to push</param>
		public string [] PushDocument(FastDocument document) 
		{
			try
			{
				Metrics.pcProfilesTotal.Increment();
				_PushError = false;

				if (document.IsExcluded)
				{
					Metrics.pcExcludedTotal.Increment();
					for (int i = 0; i < _FarmCount; i++)
					{
						_contentManagers[i].RemoveContent(document.DocumentID);
						if (PersistedByIndexingCallbackEnabled)
						{
							_Signal_PersistedByIndex.WaitOne();
						}
					}
					return null;
				}
				else
				{
					no.fast.ds.content.Document doc = document.GetDocument(_contentFactory);
					tsPushCall = new TimeSpan(DateTime.Now.Ticks);

					for (int i = 0; i < _FarmCount; i++)
					{
						_contentManagers[i].AddContent(doc);
						if (CompletedByProcessingCallbackEnabled)
						{
							_Signal_CompletedProcessing.WaitOne();
						}
						if (!_PushError && PersistedByIndexingCallbackEnabled)
						{
							_Signal_PersistedByIndex.WaitOne();
						}
					}
					return null;
				}
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("PushDocument() failed.\n\n" + ex.ToString()); 
				_PushError = true;

			}
			return null;
		}

		/// <summary>
		/// clear out all the batch tracking structures.
		/// </summary>
		private void resetBatchTracking()
		{
			Array.Clear(_FASTAddBatchIDs,0,_FarmCount);
			Array.Clear(_FASTRemoveBatchIDs,0,_FarmCount);

			Array.Clear(_CompletedProcessingFlags,0,_FarmCount);
			Array.Clear(_ReceivedByIndexFlags,0,_FarmCount);
			Array.Clear(_PersistedByIndexFlags,0,_FarmCount);
		}

		/// <summary>
		/// Pushes a collection of documents as a batch to the indexing process
		/// </summary>
		/// <param name="documents">The collection of documents to push</param>
		public string [] PushDocuments(FastDocument [] documents) 
		{

			try 
			{
				ArrayList FastPushDocuments = new ArrayList();
				StringCollection FastRemoveDocuments = new StringCollection();

				Metrics.pcBatches.Increment();
				_PushError = false;

				foreach (FastDocument fd in documents) 
				{
					// TODO: Does this really work? does the remove notification somhow get included in the batch?
					if (fd.IsExcluded) 
					{
						FastRemoveDocuments.Add(fd.DocumentID);
					} 
					else 
					{
						FastPushDocuments.Add(fd.GetDocument(_contentFactory));						
					}
				}

				if (FastPushDocuments.Count > 0)
				{
					for (int i = 0; i < _FarmCount; i++)
					{
						_FASTAddBatchIDs[i] = _contentManagers[i].AddContents(FastPushDocuments);
						Metrics.pcProfilesInBatch.RawValue = (long)FastPushDocuments.Count;
						Metrics.pcProfilesTotal.IncrementBy((long)FastPushDocuments.Count);
						tsPushCall = new TimeSpan(DateTime.Now.Ticks);
						if (CompletedByProcessingCallbackEnabled)
						{
							_Signal_CompletedProcessing.WaitOne();
						}
						if (!_PushError && PersistedByIndexingCallbackEnabled)
						{		
							_Signal_PersistedByIndex.WaitOne();
						}
					}
				}

				if (FastRemoveDocuments.Count > 0 && !_BulkImport_NoRemoveFlag)
				{
					for (int i = 0; i < _FarmCount; i++) 
					{
						_FASTRemoveBatchIDs[i] = _contentManagers[i].RemoveContents(FastRemoveDocuments);
						Metrics.pcExcludedInBatch.RawValue = (long)FastRemoveDocuments.Count;
						Metrics.pcExcludedTotal.IncrementBy((long)FastRemoveDocuments.Count);
						if (CompletedByProcessingCallbackEnabled)
						{
							_Signal_CompletedProcessing.WaitOne();
						}
						if (!_PushError && PersistedByIndexingCallbackEnabled)
						{
							_Signal_PersistedByIndex.WaitOne();
						}
					}
				}

				if (_FASTRemoveBatchIDs[0] != "") 
				{
					string [] result = new string[_FASTAddBatchIDs.Length + _FASTRemoveBatchIDs.Length];
					_FASTAddBatchIDs.CopyTo(result,0);
					_FASTRemoveBatchIDs.CopyTo(result,_FASTAddBatchIDs.Length);
					return result;
				}
				else 
				{
					return _FASTAddBatchIDs;
				}
			} 
			catch (Exception ex) 
			{
				Metrics.Instance.WriteError("PushDocuments() " + ex.ToString());
				_PushError = true;
			}
			return null;
		}

		#region Content Callback 

		/// <summary>
		/// Called when FAST processing engine completed processing this batch
		/// </summary>
		/// <param name="batchId"></param>
		/// <param name="successful"></param>
		/// <param name="batchError"></param>
		/// <param name="errors"></param>
		public void CompletedByProcessingEngine(string batchId, bool successful, string batchError, DocumentErrors errors)
		{
#if DEBUG
			Trace.WriteLine(string.Format("***CB-Completed Proc Eng B:{0}\n\t\tSuccess:{1},\n\t\tErr:{2}",batchId,successful,batchError));
#endif
			_PushError = (!successful || batchError.Length > 0);

			if (_PushError)
			{
				// Ignore errors that are returned when we try to delete an indexed profile that does not exist in the
				// search engine.
				if (batchError.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST  ) > 0 || 
					batchError.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST2 ) > 0 )
					_PushError = false;
				else
				{
					StringBuilder errorText = new StringBuilder(512);
					int ignorableErr = 0;
					int totalErrs = 0;
					errorText.Append(successful.ToString());
					errorText.Append(" Proc Engine Callback ");
					errorText.Append(batchError);

					foreach (DocumentError err in errors)
					{
						totalErrs++;					
						if (err.Error.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST  ) > 0 || 
							err.Error.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST2 ) > 0 )
							ignorableErr++;
						else
						{
							errorText.Append(Environment.NewLine);
							errorText.Append(err.Error);
						}
					}

					if (totalErrs == ignorableErr)
						_PushError = false;
					else
						Metrics.Instance.WriteError(errorText.ToString());
				}
			}

			_Signal_CompletedProcessing.Set();
		}


	
		/// <summary>
		/// Called when FAST Indexing engine received batch
		/// </summary>
		/// <param name="batchId"></param>
		public void ReceivedByIndexingEngine(string batchId)
		{
#if DEBUG
			Trace.WriteLine(string.Format("***CB-Recv Idx Eng B:{0}",batchId));
#endif
			_Signal_ReceivedByIndex.Set();
		}


		/// <summary>
		/// Called when FAST Indexing engine persisted data (final step)
		/// </summary>
		/// <param name="batchId"></param>
		/// <param name="successful"></param>
		/// <param name="batchError"></param>
		/// <param name="errors"></param>
		public void PersistedByIndexingEngine(string batchId, bool successful, string batchError, DocumentErrors errors)
		{
#if DEBUG
			Trace.WriteLine(string.Format("***CB-Persisted Idx EngB:{0}\n\t\tSuccess:{1},\n\t\tErr:{2}",batchId,successful,batchError));
#endif

			tsFinalCallback = new TimeSpan(DateTime.Now.Ticks);
			Metrics.pcPushToIndexTime.RawValue = (long)(tsFinalCallback.Subtract(tsPushCall).TotalMilliseconds);

			_PushError = (!successful || batchError.Length > 0);
			
			if (_PushError)
			{
				// Ignore errors that are returned when we try to delete an indexed profile that does not exist in the
				// search engine.
				if (batchError.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST  ) > 0 || 
					batchError.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST2 ) > 0 )
					_PushError = false;
				else
				{
					StringBuilder errorText = new StringBuilder(512);
					int ignorableErr = 0;
					int totalErrs = 0;
					errorText.Append(successful.ToString());
					errorText.Append(" Persist Callback ");
					errorText.Append(batchError);

					foreach (DocumentError err in errors)
					{
						totalErrs++;					
						if (err.Error.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST  ) > 0 || 
							err.Error.IndexOf(Constants.ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST2 ) > 0 )
							ignorableErr++;
						else
						{
							errorText.Append(Environment.NewLine);
							errorText.Append(err.Error);
						}
					}

					if (totalErrs == ignorableErr)
						_PushError = false;
					else
						Metrics.Instance.WriteError(errorText.ToString());
				}
			}

			_Signal_PersistedByIndex.Set();			
		}

		#region callback on off per callback type	
		/// <summary>
		/// Signals whether or not to enable this callback
		/// </summary>
		public bool ReceivedByIndexingCallbackEnabled
		{
			get 
			{
				try
				{
					return bool.Parse(ConfigurationSettings.AppSettings[Constants.CFG_CONTENT_CALLBACK_RECEIVEDINDEXING_FLAG]);
				}
				catch
				{
					return true;
				}
			}
		}


		/// <summary>
		/// Signals whether or not to enable this callback
		/// </summary>
		public bool PersistedByIndexingCallbackEnabled
		{
			get	
			{
				try
				{
					return bool.Parse(ConfigurationSettings.AppSettings[Constants.CFG_CONTENT_CALLBACK_PERSISTEDINDEXING_FLAG]);
				}
				catch
				{
					return true;
				}
			}
		}


		/// <summary>
		/// Signals whether or not to enable this callback
		/// </summary>
		public bool CompletedByProcessingCallbackEnabled
		{
			get 
			{
				try
				{
					return bool.Parse(ConfigurationSettings.AppSettings[Constants.CFG_CONTENT_CALLBACK_COMPLETEDPROCESSING_FLAG]);
				}
				catch
				{
					return true;
				}
			}
		}

		#endregion callback on off


		#endregion


	}
}