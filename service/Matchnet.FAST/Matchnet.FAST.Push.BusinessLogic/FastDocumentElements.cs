using System;
using System.Collections;

using Matchnet.FAST.Push.ValueObjects;

using no.fast.ds.content;

namespace Matchnet.FAST.Push.BusinessLogic
{
	
	/// <summary>
	/// Represents a collection of FastDocumentElement objects
	/// </summary>
	public class FastDocumentElements: CollectionBase , IEnumerable 
	{
		
		/// <summary>
		/// Returns the element at position [index] of the elements collection
		/// </summary>
		public FastDocumentElement this[int index]{
			get {
				return (FastDocumentElement)base.InnerList[index]; 
			}
		}

		/// <summary>
		/// Adds an element to the list of elements this document is comprised of
		/// </summary>
		/// <param name="element">The element to be added</param>
		/// <returns>the position in the collection where this element was added</returns>
		public int Add(FastDocumentElement element){
			return base.InnerList.Add(element);
		}

		/// <summary>
		/// Returns an enumerator that can iterate through this collection of elements
		/// </summary>
		/// <returns></returns>
		public new IEnumerator GetEnumerator(){
			return  base.GetEnumerator();
		}

	}
}

