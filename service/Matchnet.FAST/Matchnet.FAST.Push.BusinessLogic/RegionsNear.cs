using System;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Summary description for RegionsNear.
	/// </summary>
	public class RegionsNear: ICacheable {
		private RegionNear [] _Bands;
		private int _RegionID ;
		
		private CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
		private CacheItemMode _CacheItemMode = CacheItemMode.Sliding;
		private int _CacheTTLSeconds = 60 * 20;

		private const string REGIONS_NEAR_CACHE_PREFIX = "RegionsNear:";

		public RegionsNear(int RegionID){
			_RegionID = RegionID ;
		}

		public RegionsNear(int RegionID,ref  RegionNear [] Bands){
			_RegionID = RegionID ;
			_Bands = Bands;
		}


		public RegionNear [] Bands{
			set {
				_Bands = value;
			}
			get {
				return _Bands;
			}
		}


		public static string GetCacheKey(int RegionID) {	
			return REGIONS_NEAR_CACHE_PREFIX + RegionID.ToString();
		}

		#region ICacheable Members

		public int CacheTTLSeconds {
			get {
				return _CacheTTLSeconds;
			}
			set {
				_CacheTTLSeconds = value;
			}
		}


		public Matchnet.CacheItemMode CacheMode {
			get {
				return _CacheItemMode;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority {
			get {
				return _CacheItemPriorityLevel;
			}
			set {
				_CacheItemPriorityLevel = value;
			}
		}


		public string GetCacheKey() {	
			return REGIONS_NEAR_CACHE_PREFIX + _RegionID.ToString();
		}


		#endregion
	}
}
