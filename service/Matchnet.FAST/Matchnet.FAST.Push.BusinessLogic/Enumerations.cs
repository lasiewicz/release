using System;

namespace Matchnet.FAST.Push.BusinessLogic {

	
	public enum LanguageID : int {
		English	= 2,
		French	=8,
		German	=32,
		Portugese	=64,
		Spanish	=128,
		Hebrew	=262144
	}

	public enum FastDocumentElementType : int {
		/// <summary>
		/// A single string
		/// </summary>
		String = 1,
		/// <summary>
		/// A string collection 
		/// </summary>
		StringCol = 2,
		/// <summary>
		/// An Int32
		/// </summary>
		Int32 = 3,
		/// <summary>
		/// A float
		/// </summary>
		Float = 4,
		/// <summary>
		/// A double
		/// </summary>
		Double = 5,
		/// <summary>
		/// A datetime object
		/// </summary>
		DateTime = 6
	}



	public enum DocumentManagerStatus: int {
		/// <summary>
		/// Not ready for operation yet
		/// </summary>
		Uninitialized = 1,
		/// <summary>
		/// Available for operations
		/// </summary>
		Ready = 2,
		/// <summary>
		/// In middle of operation, or awaiting callbacks
		/// </summary>
		Pushing = 3
	}

	internal enum AttributeDataType {
				Integer,
				DateTime,
				Boolean,
				String
	}

	public enum MemberPushStatus: int{
		PushedToIndex = 1,
		DeletedFromIndex = 2
	}

}
