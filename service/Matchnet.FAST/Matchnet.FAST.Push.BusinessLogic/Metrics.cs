using System;
using System.Diagnostics;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Summary description for Metrics.
	/// </summary>
	public class Metrics {


		#region counter instances
		
		public static PerformanceCounter pcBatches;
		public static PerformanceCounter pcProfilesInBatch;
		public static PerformanceCounter pcProfilesTotal;
		public static PerformanceCounter pcExcludedInBatch;
		public static PerformanceCounter pcExcludedTotal;
		public static PerformanceCounter pcProfileLoadTime;
		public static PerformanceCounter pcProfileTransformTime;
		public static PerformanceCounter pcPushToIndexTime;
		public static PerformanceCounter pcPushThreadsRunning;

		#endregion


		/// <summary>
		/// The single instance of this singleteon object
		/// </summary>
		public static readonly Metrics Instance = new Metrics();


		private Metrics() {
			if (PerformanceCounterCategory.Exists(Constants.COUNTER_CATEGORY_PUSH)){
				pcBatches = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Batches",false);
				pcProfilesInBatch = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Profiles In Batch",false);
				pcProfilesTotal = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Profiles Total",false);
				pcExcludedInBatch = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Excluded In Batch",false);
				pcExcludedTotal = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Excluded Total",false);
				pcProfileLoadTime = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Profile Load Time",false);
				pcProfileTransformTime = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Profile Transform Time",false);
				pcPushToIndexTime = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Push To Index Time",false);
				pcPushThreadsRunning = new PerformanceCounter(Constants.COUNTER_CATEGORY_PUSH,"Push Threads Running",false);
			}
			else {
				throw new Exception("Unable to instantiate counters. PerformanceCounterCategory does not exist");
			}
			
		}

		/// <summary>
		/// Adds an information type event log entry
		/// </summary>
		/// <param name="message">message content</param>
		public void WriteInformation(string message) {
			EventLog.WriteEntry(Constants.EVENT_LOG_SOURCE_PUSH, message, EventLogEntryType.Information);
		}


		/// <summary>
		/// Adds an error type event log entry
		/// </summary>
		/// <param name="message">message content</param>
		public void WriteError(string message) {
			EventLog.WriteEntry(Constants.EVENT_LOG_SOURCE_PUSH, message, EventLogEntryType.Error);
		}

		/// <summary>
		/// Reset all performance counters.
		/// </summary>
		public void ResetCounters(){
			// initialize values
			pcBatches.RawValue = 0;
			pcProfilesInBatch.RawValue = 0;
			pcProfilesTotal.RawValue = 0;
			pcExcludedInBatch.RawValue = 0;
			pcExcludedTotal.RawValue = 0;
			pcProfileLoadTime.RawValue = 0;
			pcProfileTransformTime.RawValue = 0;
			pcPushToIndexTime.RawValue = 0;
			pcPushThreadsRunning.RawValue = 0;
		}

	}
	public class MetricsInstaller{

		public static void Install(){
			Trace.WriteLine(Constants.COUNTER_CATEGORY_PUSH);
			if (!PerformanceCounterCategory.Exists(Constants.COUNTER_CATEGORY_PUSH)){
				CounterCreationDataCollection CountersToCreate = new CounterCreationDataCollection();
				CountersToCreate.Add(new CounterCreationData("Batches","# of batches processed",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Profiles In Batch","# of profiles processed in current batch prior to push",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Profiles Total","# of profiles processed total",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Excluded In Batch","# of profiles excluded in this batch",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Excluded Total","# of profiles processed total",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Profile Load Time","Time it took to load profile from data store",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Profile Transform Time","Time it took to transform data into FAST compatible document elements",PerformanceCounterType.NumberOfItems32));

				CountersToCreate.Add(new CounterCreationData("Push To Index Time","Time it took push batch or item from API call to time it was persisted to FAST",PerformanceCounterType.NumberOfItems32));

				CountersToCreate.Add(new CounterCreationData("Push Threads Running","Current # of Push threads that are in runing. A stop command does not stop a pusher immediatly, so only when this counter is at 0 you know all instances are done.",PerformanceCounterType.NumberOfItems32));

				// Create the category and pass the collection to it.
				PerformanceCounterCategory.Create(Constants.COUNTER_CATEGORY_PUSH, "Counters for the service that gets profiles and pushes them to FAST for indexing", CountersToCreate);
			}

			// Event log prep
			if(!EventLog.SourceExists(Constants.EVENT_LOG_SOURCE_PUSH)){
				EventLog.CreateEventSource( Constants.EVENT_LOG_SOURCE_PUSH ,"Application");
				Trace.WriteLine("Created Event Source -> " + Constants.EVENT_LOG_SOURCE_PUSH );
			}

		}

		public static void Uninstall(){
			if (PerformanceCounterCategory.Exists(Constants.COUNTER_CATEGORY_PUSH)){
				PerformanceCounterCategory.Delete(Constants.COUNTER_CATEGORY_PUSH);
			}

			if(EventLog.SourceExists(Constants.EVENT_LOG_SOURCE_PUSH)){
				EventLog.DeleteEventSource(Constants.EVENT_LOG_SOURCE_PUSH);
			}
		}
	}
}
