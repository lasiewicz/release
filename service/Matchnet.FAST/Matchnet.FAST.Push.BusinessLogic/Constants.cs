using System;
using Matchnet.FAST.Push.ValueObjects;

namespace Matchnet.FAST.Push.BusinessLogic
{
	/// <summary>
	/// Some notion of public constants, not currently in use.
	/// </summary>
	public class Constants
	{
		// FAST Callback errors which are ignorable.
		public const string ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST  = "probably because the documents do not exist in the indexer";
		public const string ERRORMSG_FASTCALLBACK_DOCUMENT_DOES_NOT_EXIST2 = "probably because the document does not exist in the indexer";

		/*** Configuration key name constants ***/
		//public const string CFG_ = "";

		// Content Callback Configs
		public const string CFG_CONTENT_CALLBACK_RECEIVEDINDEXING_FLAG = "ContentCallback_ReceivedByIndexing_Enable";
		public const string CFG_CONTENT_CALLBACK_PERSISTEDINDEXING_FLAG = "ContentCallback_PersistedByIndexing_Enable";
		public const string CFG_CONTENT_CALLBACK_COMPLETEDPROCESSING_FLAG = "ContentCallback_CompletedProcessing_Enable";
		

		//public const string CFG_ = "";
		public const string CFG_FAST_NAME_SERVICE_LOCATION	= "FASTPUSHSVC_NAMESERVICELOC";
		public const string CFG_FAST_COLLECTION_NAME_BASE	= "FASTPUSHSVC_COLLECTIONNAMEBASE";
		public const string CFG_SEARCH_CLUSTER_NAME			= "FASTPUSHSVC_CLUSTERNAME";


		// BULK IMPORT BL
		public const string CFG_BULK_IMPORT_BATCH_SIZE					= "FASTPUSHSVC_BATCHSIZE";
		public const string CFG_BULK_IMPORT_BATCH_CONNECTION			= "FASTPUSHSVC_BULKIMPORT_CONNECTION_MEMBERBATCHES";
		public const string CFG_BULK_IMPORT_MEMBER_CONNECTION			= "FASTPUSHSVC_BULKIMPORT_CONNECTION_MEMBERATTRIBUTES";
		public const string CFG_BULK_IMPORT_REGION_SOURCE_CONNECTION	= "FASTPUSHSVC_CONNECTION_REGIONSOURCE";

		public const string CFG_BULK_IMPORT_MODE_NOREMOVE				= "FASTPUSHSVC_BULKIMPORT_NOREMOVEFLAG";
		public const string CFG_BULK_IMPORT_MODE_LOADFROMDB				= "FASTPUSHSVC_BULKIMPORT_LOADFROMDBFLAG";
		public const string CFC_BULK_IMPORT_MODE_LOGACTION				= "FASTPUSHSVC_BULKIMPORT_LOGACTIONFLAG";
		
		public const string CFG_BULK_IMPORT_THREAD_COUNT_LIST			= "FASTPUSHSVC_THREAD_COUNT_LIST";

		//MSMQ Queues
		public const string CFG_QUEUE_NAME_SETTING = "FASTPUSHSVC_QUEUE_NAME";
		public const string CFG_QUEUE_COMMUNITY_LIST_SETTING = "FASTPUSHSVC_COMMUNITY_LIST";
		public const string CFG_QUEUE_ERROR_QUEUE_NAME_SETTING = "FASTPUSHSVC_ERRORQUEUENAME";
		public const string CFG_QUEUE_RECOVERABLEFLAG_SETTING = "FASTPUSHSVC_QUEUERECOVERABLEFLAG";


		// REGION
		public const string PUSH_REGION_CACHE_PREFIX = "RegionBL:RegionID";


		// the event source used for event log writing
		public const string EVENT_LOG_SOURCE_PUSH = "Matchnet.FAST.Push";
		public const string COUNTER_CATEGORY_PUSH = "Matchnet.FAST.Push";


		#region MEMBERATTRIBUTES_TO_INDEX
		// This is a list of MemberAttributes that we will need to retrieve for FAST.
		// Global.  This includes MemberAttributes that need to be checked for ExclusionRules but will not
		// necessarily be indexed.

		internal struct AttributeMapElement 
		{
			internal string AttributeName;
			internal AttributeDataType DataType;
			internal AttributeMapElement(string AttributeName, AttributeDataType DataType)
			{
				this.AttributeName = AttributeName;
				this.DataType = DataType;
			}
		}

		/// <summary>
		/// Mapping table from attribute name to FAST attribute type
		/// </summary>
		internal static readonly AttributeMapElement[] MEMBERATTRIBUTES_TO_INDEX_GLOBAL = new AttributeMapElement[] {
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ABOUTME, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ACTIVITYLEVEL, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_APPEARANCEIMPORTANCE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_APPEARINGONTHEMEDIA, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_BIRTHCOUNTRYREGIONID, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_BIRTHDATE, AttributeDataType.DateTime),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_BODYART, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_BODYTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_CANNOTDOWITHOUT, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_CHILDRENCOUNT, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_CUISINE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_CUSTODY, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDDRINKINGHABITS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDEDUCATIONLEVEL, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDJDATERELIGION, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDMARITALSTATUS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDMAXAGE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDMINAGE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DESIREDSMOKINGHABITS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS, AttributeDataType.Boolean),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_DRINKINGHABITS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_EDUCATIONLEVEL, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ENTERTAINMENTLOCATION, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ESSAYDESCRIBEHUMOR, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ESSAYFIRSTTHINGNOTICEABOUTYOU, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ESSAYFRIENDSDESCRIPTION, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ESSAYORGANIZATIONS, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ETHNICITY, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_EYECOLOR, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_FACEPAGEINTRODUCTION, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_FAVORITEBOOKAUTHOR, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_FAVORITETIMEOFDAY, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_GAYSCENEMASK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_GENDERMASK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_GREWUPIN, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HABITATTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HAIRCOLOR, AttributeDataType.Integer),
																														// NOTE: We are now appending this when we scan for thumbnail path. No need to itterate separately.
																														//new AttributeMapElement(VOConstants.ATTRIBUTENAME_HASPHOTOFLAG, AttributeDataType.Boolean),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HAVECHILDREN, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HEADLINE, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HEIGHT, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HIVSTATUSTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_IDEALVACATION, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_INCOMELEVEL, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_INDUSTRYTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_INTELLIGENCEIMPORTANCE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ISRAELAREACODE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ISRAELIMMIGRATIONDATE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ISRAELIPERSONALITYTRAIT, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_JDATEETHNICITY, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_JDATERELIGION, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_KEEPKOSHER, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_LANGUAGEMASK, AttributeDataType.Integer),
																														// NOTE:  NURI SAYS DO NOT WORRY ABOUT THIS FOR NOW.
																														//{VOConstants.ATTRIBUTENAME_LASTBRANDID, AttributeDataType.Integer},
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_LASTUPDATED, AttributeDataType.DateTime),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_LEISUREACTIVITY, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MAJORTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MARITALSTATUS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MILITARYSERVICETYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MORECHILDRENFLAG, AttributeDataType.Boolean),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MOVIEGENREMASK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MUSIC, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_OUTNESSTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_PERSONALITYTRAIT, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_PETS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_PHYSICALACTIVITY, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_POLITICALORIENTATION, AttributeDataType.Integer),
																														// NOTE:  NURI SAYS DO NOT WORRY ABOUT THIS FOR NOW.
																														//{VOConstants.ATTRIBUTENAME_PRIVATELABELID, AttributeDataType.Integer},
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_READING, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_REGIONID, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_RELATIONSHIPMASK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_RELATIONSHIPSTATUS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_RELIGION, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_RELIGIONACTIVITYLEVEL, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_RELOCATEFLAG, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_SCHOOLID, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_SEXUALIDENTITYTYPE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_SMOKINGHABITS, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_SMSCAPABLE, AttributeDataType.Boolean),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_SYNAGOGUEATTENDANCE, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_TRAVELMASK, AttributeDataType.Integer),
																														// NOTE: username is saved as a memberattributeText but always has languageID 2.
																														// The member service has this as a property, the preferred non language specific of getting the person's username.
																														//new AttributeMapElement(VOConstants.ATTRIBUTENAME_USERNAME, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_WEIGHT, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_WHENIHAVETIME, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_ZODIAC, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_GLOBALSTATUSMASK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_HIDEMASK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_SELFSUSPENDEDFLAG, AttributeDataType.Boolean),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_POPULARITYRANK, AttributeDataType.Integer),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_MEMBERREGISTRATIONDATE, AttributeDataType.DateTime),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_POPULARITYRANKDATE, AttributeDataType.DateTime),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_THUMBPATH, AttributeDataType.String),
																														new AttributeMapElement(VOConstants.ATTRIBUTENAME_LASTLOGONDATE, AttributeDataType.DateTime)};

		#region MEMBERATTRIBUTES_TO_INDEX - Currently Not Used
		//		// NOTE:  The following arrays were going to be used to make the "AddAttributes" call more efficient.
		//		// Instead of checking for all the Attributes, we could use the following Arrays to only check for Attributes
		//		// if they are being used by the CommunityID that we are processing (We would run through all the values in
		//		// the _GLOBAL and then all the values in the Community-specific array).  However, due to questions in the
		//		// data integrity of the mnShared.dbo.AttributeDomain table, we use the comprehensive array above.  Every
		//		// possible attribute will be checked for every possible CommunityID.  The following arrays are left in
		//		// in case we choose to use them in the future.
		//		private static readonly string[] MEMBERATTRIBUTES_TO_INDEX_GLOBAL = new string[] {VOConstants.ATTRIBUTENAME_ACTIVITYLEVEL,
		//																							 VOConstants.ATTRIBUTENAME_APPEARANCEIMPORTANCE,
		//																							 VOConstants.ATTRIBUTENAME_APPEARINGONTHEMEDIA,
		//																							 VOConstants.ATTRIBUTENAME_BIRTHCOUNTRYREGIONID,
		//																							 VOConstants.ATTRIBUTENAME_BIRTHDATE,
		//																							 VOConstants.ATTRIBUTENAME_BODYART,
		//																							 VOConstants.ATTRIBUTENAME_BODYTYPE,
		//																							 VOConstants.ATTRIBUTENAME_CANNOTDOWITHOUT,
		//																							 VOConstants.ATTRIBUTENAME_CHILDRENCOUNT,
		//																							 VOConstants.ATTRIBUTENAME_CUISINE,
		//																							 VOConstants.ATTRIBUTENAME_CUSTODY,
		//																							 VOConstants.ATTRIBUTENAME_DESIREDDRINKINGHABITS,
		//																							 VOConstants.ATTRIBUTENAME_DESIREDEDUCATIONLEVEL,
		//																							 VOConstants.ATTRIBUTENAME_DESIREDJDATERELIGION,
		//																							 VOConstants.ATTRIBUTENAME_DESIREDMARITALSTATUS,
		//																							 VOConstants.ATTRIBUTENAME_DESIREDSMOKINGHABITS,
		//																							 VOConstants.ATTRIBUTENAME_COMMUNITYID,
		//																							 VOConstants.ATTRIBUTENAME_ENTERTAINMENTLOCATION,
		//																							 VOConstants.ATTRIBUTENAME_ESSAYDESCRIBEHUMOR,
		//																							 VOConstants.ATTRIBUTENAME_ESSAYFIRSTTHINGNOTICEABOUTYOU,
		//																							 VOConstants.ATTRIBUTENAME_ESSAYFRIENDSDESCRIPTION,
		//																							 VOConstants.ATTRIBUTENAME_ESSAYORGANIZATIONS,
		//																							 VOConstants.ATTRIBUTENAME_ETHNICITY,
		//																							 VOConstants.ATTRIBUTENAME_EYECOLOR,
		//																							 VOConstants.ATTRIBUTENAME_FAVORITETIMEOFDAY,
		//																							 VOConstants.ATTRIBUTENAME_GAYSCENEMASK,
		//																							 VOConstants.ATTRIBUTENAME_HABITATTYPE,
		//																							 VOConstants.ATTRIBUTENAME_HAIRCOLOR,
		//																							 VOConstants.ATTRIBUTENAME_HAVECHILDREN,
		//																							 VOConstants.ATTRIBUTENAME_HEIGHT,
		//																							 VOConstants.ATTRIBUTENAME_HIVSTATUSTYPE,
		//																							 VOConstants.ATTRIBUTENAME_INDUSTRYTYPE,
		//																							 VOConstants.ATTRIBUTENAME_INTELLIGENCEIMPORTANCE,
		//																							 VOConstants.ATTRIBUTENAME_ISRAELAREACODE,
		//																							 VOConstants.ATTRIBUTENAME_ISRAELIMMIGRATIONDATE,
		//																							 VOConstants.ATTRIBUTENAME_ISRAELIPERSONALITYTRAIT,
		//																							 VOConstants.ATTRIBUTENAME_JDATEETHNICITY,
		//																							 VOConstants.ATTRIBUTENAME_JDATERELIGION,
		//																							 VOConstants.ATTRIBUTENAME_KEEPKOSHER,
		//																							 VOConstants.ATTRIBUTENAME_LEISUREACTIVITY,
		//																							 VOConstants.ATTRIBUTENAME_MARITALSTATUS,
		//																							 VOConstants.ATTRIBUTENAME_MILITARYSERVICETYPE,
		//																							 VOConstants.ATTRIBUTENAME_MORECHILDRENFLAG,
		//																							 VOConstants.ATTRIBUTENAME_MOVIEGENREMASK,
		//																							 VOConstants.ATTRIBUTENAME_MUSIC,
		//																							 VOConstants.ATTRIBUTENAME_OUTNESSTYPE,
		//																							 VOConstants.ATTRIBUTENAME_PERSONALITYTRAIT,
		//																							 VOConstants.ATTRIBUTENAME_PETS,
		//																							 VOConstants.ATTRIBUTENAME_PHYSICALACTIVITY,
		//																							 VOConstants.ATTRIBUTENAME_PRIVATELABELID,
		//																							 VOConstants.ATTRIBUTENAME_READING,
		//																							 VOConstants.ATTRIBUTENAME_REGIONID,
		//																							 VOConstants.ATTRIBUTENAME_RELATIONSHIPSTATUS,
		//																							 VOConstants.ATTRIBUTENAME_RELIGION,
		//																							 VOConstants.ATTRIBUTENAME_RELIGIONACTIVITYLEVEL,
		//																							 VOConstants.ATTRIBUTENAME_RELOCATEFLAG,
		//																							 VOConstants.ATTRIBUTENAME_SCHOOLID,
		//																							 VOConstants.ATTRIBUTENAME_SEXUALIDENTITYTYPE,
		//																							 VOConstants.ATTRIBUTENAME_SMOKINGHABITS,
		//																							 VOConstants.ATTRIBUTENAME_SMSCAPABLE,
		//																							 VOConstants.ATTRIBUTENAME_SYNAGOGUEATTENDANCE,
		//																							 VOConstants.ATTRIBUTENAME_TRAVELMASK,
		//																							 VOConstants.ATTRIBUTENAME_USERNAME,
		//																							 VOConstants.ATTRIBUTENAME_WEIGHT,
		//																							 VOConstants.ATTRIBUTENAME_WHENIHAVETIME,
		//																							 VOConstants.ATTRIBUTENAME_ZODIAC,
		//																							 VOConstants.ATTRIBUTENAME_GLOBALSTATUSMASK,
		//																							 VOConstants.ATTRIBUTENAME_HIDEMASK,
		//																							 VOConstants.ATTRIBUTENAME_SELFSUSPENDEDFLAG};
		//
		//		// Domain 1.
		//		private static readonly string[] MEMBERATTRIBUTES_TO_INDEX_DOMAIN1 = new string[] {VOConstants.ATTRIBUTENAME_ABOUTME,
		//																							  VOConstants.ATTRIBUTENAME_DESIREDMAXAGE,
		//																							  VOConstants.ATTRIBUTENAME_DESIREDMINAGE,
		//																							  VOConstants.ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS,
		//																							  VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST,
		//																							  VOConstants.ATTRIBUTENAME_GENDERMASK,
		//																							  VOConstants.ATTRIBUTENAME_GREWUPIN,
		//																							  VOConstants.ATTRIBUTENAME_HASPHOTOFLAG,
		//																							  VOConstants.ATTRIBUTENAME_HEADLINE,
		//																							  VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY,
		//																							  VOConstants.ATTRIBUTENAME_IDEALVACATION,
		//																							  VOConstants.ATTRIBUTENAME_INCOMELEVEL,
		//																							  VOConstants.ATTRIBUTENAME_LASTPRIVATELABELID,
		//																							  VOConstants.ATTRIBUTENAME_LASTUPDATED,
		//																							  VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY,
		//																							  VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION,
		//																							  VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY,
		//																							  VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY,
		//																							  VOConstants.ATTRIBUTENAME_RELATIONSHIPMASK,
		//																							  VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS};
		//
		//		// Domain 2.
		//		private static readonly string[] MEMBERATTRIBUTES_TO_INDEX_DOMAIN2 = new string[] {VOConstants.ATTRIBUTENAME_ABOUTME,
		//																							  VOConstants.ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY,
		//																							  VOConstants.ATTRIBUTENAME_DESIREDMAXAGE,
		//																							  VOConstants.ATTRIBUTENAME_DESIREDMINAGE,
		//																							  VOConstants.ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS,
		//																							  VOConstants.ATTRIBUTENAME_DRINKINGHABITS,
		//																							  VOConstants.ATTRIBUTENAME_EDUCATIONLEVEL,
		//																							  VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST,
		//																							  VOConstants.ATTRIBUTENAME_FAVORITEBOOKAUTHOR,
		//																							  VOConstants.ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR,
		//																							  VOConstants.ATTRIBUTENAME_GENDERMASK,
		//																							  VOConstants.ATTRIBUTENAME_GREWUPIN,
		//																							  VOConstants.ATTRIBUTENAME_HASPHOTOFLAG,
		//																							  VOConstants.ATTRIBUTENAME_HEADLINE,
		//																							  VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY,
		//																							  VOConstants.ATTRIBUTENAME_IDEALVACATION,
		//																							  VOConstants.ATTRIBUTENAME_INCOMELEVEL,
		//																							  VOConstants.ATTRIBUTENAME_LASTPRIVATELABELID,
		//																							  VOConstants.ATTRIBUTENAME_LASTUPDATED,
		//																							  VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY,
		//																							  VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION,
		//																							  VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY,
		//																							  VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY,
		//																							  VOConstants.ATTRIBUTENAME_POLITICALORIENTATION,
		//																							  VOConstants.ATTRIBUTENAME_RELATIONSHIPMASK,
		//																							  VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS};
		//
		//		// Domain 3.
		//		private static readonly string[] MEMBERATTRIBUTES_TO_INDEX_DOMAIN3 = new string[] {VOConstants.ATTRIBUTENAME_ABOUTME,
		//																							  VOConstants.ATTRIBUTENAME_DESIREDMAXAGE,
		//																							  VOConstants.ATTRIBUTENAME_DESIREDMINAGE,
		//																							  VOConstants.ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS,
		//																							  VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST,
		//																							  VOConstants.ATTRIBUTENAME_GENDERMASK,
		//																							  VOConstants.ATTRIBUTENAME_GREWUPIN,
		//																							  VOConstants.ATTRIBUTENAME_HASPHOTOFLAG,
		//																							  VOConstants.ATTRIBUTENAME_HEADLINE,
		//																							  VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY,
		//																							  VOConstants.ATTRIBUTENAME_IDEALVACATION,
		//																							  VOConstants.ATTRIBUTENAME_INCOMELEVEL,
		//																							  VOConstants.ATTRIBUTENAME_LASTPRIVATELABELID,
		//																							  VOConstants.ATTRIBUTENAME_LASTUPDATED,
		//																							  VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY,
		//																							  VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION,
		//																							  VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY,
		//																							  VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY,
		//																							  VOConstants.ATTRIBUTENAME_RELATIONSHIPMASK,
		//																							  VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS};
		//
		//		// Domain 10.
		//		private static readonly string[] MEMBERATTRIBUTES_TO_INDEX_DOMAIN10 = new string[] {VOConstants.ATTRIBUTENAME_LANGUAGEMASK,
		//																							   VOConstants.ATTRIBUTENAME_LASTPRIVATELABELID};
		//
		//		// Domain 12.
		//		private static readonly string[] MEMBERATTRIBUTES_TO_INDEX_DOMAIN12 = new string[] {VOConstants.ATTRIBUTENAME_ABOUTME,
		//																							   VOConstants.ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY,
		//																							   VOConstants.ATTRIBUTENAME_DESIREDMAXAGE,
		//																							   VOConstants.ATTRIBUTENAME_DESIREDMINAGE,
		//																							   VOConstants.ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS,
		//																							   VOConstants.ATTRIBUTENAME_DRINKINGHABITS,
		//																							   VOConstants.ATTRIBUTENAME_EDUCATIONLEVEL,
		//																							   VOConstants.ATTRIBUTENAME_FACEPAGEINTRODUCTION,
		//																							   VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST,
		//																							   VOConstants.ATTRIBUTENAME_FAVORITEBOOKAUTHOR,
		//																							   VOConstants.ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR,
		//																							   VOConstants.ATTRIBUTENAME_GENDERMASK,
		//																							   VOConstants.ATTRIBUTENAME_GREWUPIN,
		//																							   VOConstants.ATTRIBUTENAME_HASPHOTOFLAG,
		//																							   VOConstants.ATTRIBUTENAME_HEADLINE,
		//																							   VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY,
		//																							   VOConstants.ATTRIBUTENAME_IDEALVACATION,
		//																							   VOConstants.ATTRIBUTENAME_INCOMELEVEL,
		//																							   VOConstants.ATTRIBUTENAME_LASTPRIVATELABELID,
		//																							   VOConstants.ATTRIBUTENAME_LASTUPDATED,
		//																							   VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY,
		//																							   VOConstants.ATTRIBUTENAME_MAJORTYPE,
		//																							   VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION,
		//																							   VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY,
		//																							   VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY,
		//																							   VOConstants.ATTRIBUTENAME_POLITICALORIENTATION,
		//																							   VOConstants.ATTRIBUTENAME_RELATIONSHIPMASK,
		//																							   VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS};
		#endregion
		#endregion
		

	}
}
