using System;
using System.Data;

using Matchnet.FAST.Push.ValueObjects;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Summary description for RegionNear.
	/// </summary>
	public class RegionNear {

		private int _RegionID;
		private int _BandID;
		private string _RegionsNear;
		
//		private const string REGION_PREFIX = " RID";
		private const string REGION_CACHE_KEY_PREFIX = "RID:NEAR:";

//		public RegionNear(DataRow row) {
//			_RegionID = (int)row["RegionID"];
//			_BandID = (int)row["BandID"];
//			string [] splits = (row["RegionsNear"] as String).Split(',');
//			_RegionsNear = REGION_PREFIX + string.Join(REGION_PREFIX,splits,0,(splits.Length > 6) ? 6:splits.Length);
//		}

		public RegionNear(int RegionID, int BandID, ref string RegionsNear){
			_RegionID = RegionID;
			_BandID = BandID;
			_RegionsNear = RegionsNear;
		}

		public RegionNear(int RegionID, int BandID, string RegionsNear){
			_RegionID = RegionID;
			_BandID = BandID;
			_RegionsNear = RegionsNear;
		}


		public int RegionID{
			get { 
				return _RegionID;
			}
		}

		public int BandID{
			get {
				return _BandID;
			}
		}

		public string RegionsNear{
			get{
				return _RegionsNear;
			}
		}
	}

}
