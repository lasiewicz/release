using System;
using System.Collections;

using Matchnet.FAST.Push.ValueObjects;

using no.fast.ds.content;


namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Represents a document element in the fast document Summary description for FastDocumentElement.
	/// </summary>
	public class FastDocumentElement {
		private FastDocumentElementType _Type;
		private string _Name;
		private object _Value;
		private LanguageID _Language;
		private string _TokenPrefix;
		 

		/// <summary>
		/// Constructor
		/// </summary>
		public FastDocumentElement(){
			// Empty 
		}


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="name"></param>
		/// <param name="type"></param>
		public FastDocumentElement(string name, FastDocumentElementType type){
			_Name = name;
			_Type = type;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="name">element name</param>
		/// <param name="type">element type</param>
		/// <param name="Value">element value</param>
		public FastDocumentElement(string name, FastDocumentElementType type, object Value){
			_Name = name;
			_Type = type;
			_Value = Value;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="name">element name</param>
		/// <param name="type">element type</param>
		/// <param name="Value">element value</param>
		/// <param name="Language">Language (valid for string types only)</param>
		public FastDocumentElement(string name, FastDocumentElementType type, object Value, LanguageID Language){
			_Name = name;
			_Type = type;
			_Value = Value;
			_Language = Language;
		}

		/// <summary>
		/// The type of this object
		/// </summary>
		public FastDocumentElementType Type{
			get{
				return _Type;
			}
			set {
				_Type = value;
			}
		}

		/// <summary>
		/// The name of the Fast Document Element, as corolates to the index profile.
		/// </summary>
		public string Name{
			get{
				return _Name;
			}
			set {
				_Name = value;
			}
		}

		/// <summary>
		/// The value of this object
		/// </summary>
		public object Value{
			get {
				return _Value;
			}
			set{
				_Value = value;
			}
		}

		/// <summary>
		/// The language of the element. intended for for strings.
		/// </summary>
		public LanguageID Language{
			get { 
				return _Language;
			}
			set {
				_Language = value;
			}
		}

		/// <summary>
		/// A prefix to the value of this object. Tokens with non empty prefix 
		/// are going to be exported as {Prefix}_{Value}
		/// </summary>
		public string TokenPrefix{
			get {
				return _TokenPrefix;
			}
			set {
				_TokenPrefix = value;
			}
		}

		/// <summary>
		/// Gets a prefixed element value
		/// </summary>
		/// <returns>{Prefix}_{Value} if a prefix is defined, {value} otherwise</returns>
		public string GetPrefixedValue(){
			if (_TokenPrefix == null || _TokenPrefix == string.Empty){
				return _Value.ToString();
			}
			else {
				return _TokenPrefix + "Q" + _Value.ToString();
			}
		}

	}

	
}
