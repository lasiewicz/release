using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;


using Matchnet.Configuration.ServiceAdapters;

using Matchnet.FAST.Push.ValueObjects;
using Matchnet.FAST.Push.Interfaces;

namespace Matchnet.FAST.Push.BusinessLogic 
{

	public class QueueUtility
	{
		private static QueueConfiguration _QueueConfig;

		public bool ContinuousEnabledFlag = false;
		public string BulkImportConnectionString;
		public string BulkImportSQLStoredProc; 

		public QueueUtility(){}
		
		/// <summary>
		/// Move items from error queue to push queues (retry?)
		/// </summary>
		public static void ReQueueMembersFromErrorQueue(int BatchSize)
		{
			try 
			{
				TimeSpan tsStart,tsGotBatch,tsEnQueueComplete;
				
				tsStart = new TimeSpan(DateTime.Now.Ticks);
				if (_QueueConfig == null) _QueueConfig = new QueueConfiguration();
				
				BusinessLogic.Queue queue = _QueueConfig.GetQueue(_QueueConfig.CommunityList[0]);
						
				// Error queue shared, so pick any queue instance to get to it.
				IFastPushPointer [] members = queue.DequeueErrorProfilePointers(BatchSize);

				tsGotBatch = new TimeSpan(DateTime.Now.Ticks);
				int curCID = 0;
				int lastCID = 0;
				if (members != null && members.Length > 0) 
				{
					try 
					{
						foreach (IFastPushPointer ptr in members) 
						{
							curCID = ptr.CommunityID;
							if (lastCID != curCID)
							{
								queue = null;
								queue = _QueueConfig.GetQueue(curCID);
							}
							queue.EnqeueProfilePointer(ptr);					
							lastCID = curCID;
						}
						queue = null;
					}
					catch (Exception ex)
					{
						Metrics.Instance.WriteError("ReQueueMembersFromErrorQueue() enqueuing pointers to queues\n\n" + ex.ToString());
						throw new Exception("ReQueueMembersFromErrorQueue() enqueuing pointers to queues\n\n" + ex.Message);
					}

					tsEnQueueComplete = new TimeSpan(DateTime.Now.Ticks);
		
					Metrics.Instance.WriteInformation(
								string.Format("Error Queue Items Requeue: {0} items #{1} ms Batch from DB#{2} ms EnQueue#{3} ms elapsed total", 
									members.Length,
									tsGotBatch.Subtract(tsStart).TotalMilliseconds ,
									tsEnQueueComplete.Subtract(tsGotBatch).TotalMilliseconds ,
									tsEnQueueComplete.Subtract(tsStart).TotalMilliseconds ));
				}
				else 
				{
					Trace.WriteLine("ReQueueMembersFromErrorQueue got nothing from DequeueErrorProfilePointers.");
				}
				
			} 
			catch (Exception ex) 
			{
				Metrics.Instance.WriteError("ReQueueMembersFromErrorQueue() failed.\n\n" + ex.ToString());
				throw new Exception("ReQueueMembersFromErrorQueue() failed.\n\n" + ex.Message);
			}
		}



		/// <summary>
		/// Add a bunch of member pointers to the queue
		/// </summary>
		public static bool EnqueueMembersFromDB(int BatchSize, 
			string BulkImportConnectionString, 
			string SQLStoredProc, IndexAction IndexActionVal)
		{
			try 
			{
				Trace.WriteLine("QueueUtility.EnqueueMembersFromDB():  BatchSize[" + BatchSize.ToString() + "] BulkImportConnectionString[" + BulkImportConnectionString.ToString() + "] IndexActionVal[" + IndexActionVal.ToString() + "]");

				TimeSpan tsStart,tsGotBatch,tsEnQueueComplete;
				
				tsStart = new TimeSpan(DateTime.Now.Ticks);
				if (_QueueConfig == null) _QueueConfig = new QueueConfiguration();
				BusinessLogic.Queue queue = null;
						
				DataTable members = GetMemberListFromDB(BatchSize, BulkImportConnectionString, SQLStoredProc);

				tsGotBatch = new TimeSpan(DateTime.Now.Ticks);
				int curCID = 0;
				int lastCID = 0;
				if (members != null) 
				{
					try 
					{
						foreach (DataRow row in members.Rows) 
						{
							Trace.WriteLine("QueueUtility.EnqueueMembersFromDB():  MemberID[" + row["MemberID"].ToString() + "] DomainID[" + row["DomainID"].ToString() + "] IndexActionVal[" + IndexActionVal.ToString() + "]");
							
							IFastPushPointer ptr = new FastPushPointer((int)row["MemberID"],(int)row["DomainID"], 0, IndexActionVal);
							curCID = ptr.CommunityID;
							if (lastCID != curCID)
							{
								queue = null;
								queue = _QueueConfig.GetQueue(curCID);
							}
							queue.EnqeueProfilePointer(ptr);					
							lastCID = curCID;
						}
						queue = null;
					}
					catch (Exception ex)
					{
						Metrics.Instance.WriteError("EnqueueMembersFromDB() queueing up pointers" + ex.ToString());
						throw new Exception("EnqueueMembersFromDB() queueing up pointers",ex);
					}

					tsEnQueueComplete = new TimeSpan(DateTime.Now.Ticks);
		
					Trace.WriteLine(string.Format("EnQueue {0} pointers #{1} ms Batch from DB#{2} ms EnQueue#{3} ms elapsed total", 
						members.Rows.Count,
						tsGotBatch.Subtract(tsStart).TotalMilliseconds ,
						tsEnQueueComplete.Subtract(tsGotBatch).TotalMilliseconds ,
						tsEnQueueComplete.Subtract(tsStart).TotalMilliseconds ));
					
					return true;
				}
				else 
				{
					Trace.WriteLine("EnqueueMembers got nothing from GetMemberListFromDB().");
					return false;
				}
				
			} 
			catch (Exception ex) 
			{
				Metrics.Instance.WriteError("EnqueueMembers() Failed getting batch from DB " + ex.ToString());
				throw new Exception("Failed getting batch from DB\n\n" + ex.Message);
			}
		}


		public void ContinuousEnqueue()
		{
			while (ContinuousEnabledFlag) 
			{
				try 
				{
					Trace.WriteLine("QueueUtility.ContinuousEnqueue() BulkImportConnectionString: " + BulkImportConnectionString + "\n SQLStoredProc: " + BulkImportSQLStoredProc);
					TimeSpan tsStart,tsGotBatch,tsEnQueueComplete;
				
					tsStart = new TimeSpan(DateTime.Now.Ticks);
					if (_QueueConfig == null) _QueueConfig = new QueueConfiguration();
					BusinessLogic.Queue queue = null;
						
					ArrayList members = GetMemberListFromDB();

					tsGotBatch = new TimeSpan(DateTime.Now.Ticks);
					int curCID = 0;
					int lastCID = 0;
					if (members.Count > 0) 
					{
						for ( int i = 0; i < members.Count; i++)
						{
							IFastPushPointer ptr = (FastPushPointer)members[i];					
							curCID = ptr.CommunityID;
							if (lastCID != curCID)
							{
								queue = null;
								queue = _QueueConfig.GetQueue(curCID);
							}
							queue.EnqeueProfilePointer(ptr);					
							lastCID = curCID;
						}
						queue = null;					
						tsEnQueueComplete = new TimeSpan(DateTime.Now.Ticks);
		
						Trace.WriteLine(string.Format("EnQueue {0} pointers #{1} ms Batch from DB#{2} ms EnQueue#{3} ms elapsed total", 
										members.Count,
										tsGotBatch.Subtract(tsStart).TotalMilliseconds ,
										tsEnQueueComplete.Subtract(tsGotBatch).TotalMilliseconds ,
										tsEnQueueComplete.Subtract(tsStart).TotalMilliseconds ));
					}
					else 
					{
						Trace.WriteLine("ContinuousEnqueue got nothing from GetMemberListFromDB().");
						System.Threading.Thread.Sleep(20000);
					}
				} 
				catch (Exception ex) 
				{
					Trace.WriteLine("ContinuousEnqueue() Failed getting batch from DB " + ex.ToString());
					Metrics.Instance.WriteError("ContinuousEnqueue() Failed getting batch from DB " + ex.ToString());
					System.Threading.Thread.Sleep(20000);
				}
			}
		
		}

		/// <summary>
		/// Consume a batch of members to be sent in a batch
		/// </summary>
		/// <returns>A datatable of document pointers</returns>
		private static DataTable GetMemberListFromDB(int BulkImportBatchSize, 
			string BulkImportConnectionString, 
			string SQLStoredProc)
		{
			try 
			{
				SqlCommand cmd = new SqlCommand(SQLStoredProc);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Parameters.Add("@BatchSize", BulkImportBatchSize);
				cmd.Connection = new SqlConnection(BulkImportConnectionString);

				SqlDataAdapter da = new SqlDataAdapter(cmd);
				
				DataTable table = new DataTable();
				da.Fill(table);

				if (table != null && table.Rows.Count > 0) 
				{
					return table;
				} 
				else 
				{
					return null;
				}
			} 
			catch (Exception ex) 
			{
				Metrics.Instance.WriteError("Failed retreiving batch from database " + ex.ToString());
				throw new Exception("Failed retreiving batch from database", ex);
			}
		}


		/// <summary>
		/// Consume a batch of members to be queued
		/// </summary>
		/// <returns>An array of document pointers</returns>
		private ArrayList GetMemberListFromDB()
		{
			SqlDataReader dr = null;
			SqlCommand cmd = null;
			try 
			{
				ArrayList result = new ArrayList();
				cmd = new SqlCommand(BulkImportSQLStoredProc);
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.Connection = new SqlConnection(BulkImportConnectionString);

				cmd.Connection.Open();
				dr =  cmd.ExecuteReader();
				int ptrMemberID = dr.GetOrdinal("MemberID");
				int ptrCommunityID = dr.GetOrdinal("CommunityID");
				while (dr.Read())
				{
					IFastPushPointer ptr = new FastPushPointer(dr.GetInt32(ptrMemberID),dr.GetInt32(ptrCommunityID), 0, IndexAction.Insert);
					result.Add(ptr);
				}
				return result;
			} 
			catch (Exception ex) 
			{
				Trace.WriteLine("GetMemberListFromDB() Failed listing memebrs from database " + ex.ToString());
				Metrics.Instance.WriteError("GetMemberListFromDB() Failed listing memebrs from database " + ex.ToString());
				throw new Exception("GetMemberListFromDB() Failed listing memebrs from database", ex);
			}
			finally 
			{
				dr.Close();
				cmd.Connection.Close();
			}

		}

		/// <summary>
		/// Creates a new set of MSMQ queues on the local machine.
		/// The queue names are ./private$/{QueueName}, where QueueName comes form mnSystem setting FASTPUSHSVC_QUEUE_NAME.
		/// Each community specified in the setting FASTPUSHSVC_COMMUNITY_LIST will get a queue created for it. This setting is a comman separated list of community id's.
		/// There must be a CommunitySetting entery for each community specified in the cummunity list which contains the queue name.
		/// </summary>
		/// <returns>True = success, False = failure.</returns>
		public static bool InstallQueues()
		{
			bool SuccessFlag = false;
			
			try
			{
				// Community specific queues
				foreach(string QueueName in GetQueueNames())
				{
					if (!MessageQueue.Exists(QueueName))
					{
						MessageQueue.Create(QueueName,false); // create a non transactional queue
						Trace.WriteLine("Created " + QueueName);
					}
				}
				
				// a shared error items queue
				string ErrorQueueName = GetErrorQueueName();
				if (!MessageQueue.Exists(ErrorQueueName))
				{
					MessageQueue.Create(ErrorQueueName,false); // create a non transactional queue
					Trace.WriteLine("Created " + ErrorQueueName);
				}

				SuccessFlag = true;
			}
			catch (Exception ex)
			{
				throw new Exception("QueueInstaller.InstallQueues failed.",ex);
			}

			return SuccessFlag;
		}

		/// <summary>
		/// Deletes the set of MSMQ queues from the local machine.
		/// The queue names are ./private$/{QueueName}, where QueueName comes form mnSystem setting FASTPUSHSVC_QUEUE_NAME.
		/// Each community specified in the setting FASTPUSHSVC_COMMUNITY_LIST will get it's queue deleted. This setting is a comman separated list of community id's.
		/// There must be a CommunitySetting entery for each community specified in the cummunity list which contains the queue name.
		/// </summary>
		/// <returns>True = success, False = failure.</returns>
		public static bool UninstallQueues()
		{
			bool SuccessFlag = false;
			
			try
			{
				foreach(string QueueName in GetQueueNames())
				{
					if (MessageQueue.Exists(QueueName))
					{
						MessageQueue.Delete(QueueName);
						Trace.WriteLine("Deleted " + QueueName);
					}
				}

				string ErrorQueueName = GetErrorQueueName();
				if (MessageQueue.Exists(ErrorQueueName))
				{
					MessageQueue.Delete(ErrorQueueName);
					Trace.WriteLine("Deleted " + ErrorQueueName);
				}

				SuccessFlag = true;
			}
			catch (Exception ex)
			{
				throw new Exception("QueueInstaller.InstallQueues failed.",ex);
			}

			return SuccessFlag;
		}


		/// <summary>
		/// The list of the configured Community ID's
		/// </summary>
		/// <returns>A list of community ID's</returns>
		public static string [] GetCommunityIDs()
		{
			string CommunityListSetting = RuntimeSettings.GetSetting(Constants.CFG_QUEUE_COMMUNITY_LIST_SETTING);
			CommunityListSetting.Replace(" ","");
			Trace.WriteLine("Community List [" + CommunityListSetting + "]");
			return CommunityListSetting.Split(',');
		}


		/// <summary>
		/// The list of MSMQ paths for the configured Community ID's
		/// </summary>
		/// <returns>A list of local MSMQ names</returns>
		public static string [] GetQueueNames()
		{
			string [] CommunityList = GetCommunityIDs();
			string [] QueueList = new string [CommunityList.Length];

			for (int i = 0; i < CommunityList.Length; i++)
			{
				QueueList[i] = LocalQueueName(RuntimeSettings.GetSetting(Constants.CFG_QUEUE_NAME_SETTING,Int32.Parse(CommunityList[i])));
			}

			return QueueList;
		}


		/// <summary>
		/// The error queue name
		/// </summary>
		/// <returns>The local MSMQ error queue name</returns>
		public static string GetErrorQueueName()
		{
			return LocalQueueName(RuntimeSettings.GetSetting(Constants.CFG_QUEUE_ERROR_QUEUE_NAME_SETTING));			
		}


		private static string LocalQueueName(string QueueName)
		{
			return @".\private$\" + QueueName;
		}


		public static int [] GetThreadsConfig()
		{
			try 
			{
				string ThreadListSetting = RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_THREAD_COUNT_LIST);
				ThreadListSetting.Replace(" ","");
				Trace.WriteLine("Thread List [" + ThreadListSetting + "]");
				string [] ThreadList = ThreadListSetting.Split(',');
				int [] Threads = new int[ ThreadList.Length];
				for (int i = 0; i < Threads.Length; i++)
				{
					try 
					{
						Threads[i] = Int32.Parse(ThreadList[i]);
					}
					catch
					{
						Trace.WriteLine("\t--> GetThreadsConfig couldn't parse " + ThreadList[i].ToString());
						Threads[i] = 0;
					}
				}
				return Threads;
			}
			catch (Exception ex)
			{
				Trace.WriteLine("GetThreadsConfig exception " + ex.ToString());
				return new int [] {0};
			}
		}

	}
}
