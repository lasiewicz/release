using System;
using System.Collections;
using System.Messaging;

using Matchnet.FAST.Push.Interfaces;

using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Provides methods to enqueue and dequeue member pointers from underlying MSMQ
	/// </summary>
	public class Queue {

		private MessageQueue _Queue;
		private MessageQueue _ErrorQueue;

		/// <summary>
		/// Constructor.
		/// Creates a new object and opens underlying MSMQ queues
		/// </summary>
		/// <param name="QueuePath">The MSMQ path for the member items</param>
		/// <param name="ErrorQueuePath">The MSMQ path for error items</param>
		public Queue(string QueuePath, string ErrorQueuePath){
			_Queue = new MessageQueue(QueuePath);
			_Queue.Formatter = new BinaryMessageFormatter();
			_ErrorQueue = new MessageQueue(ErrorQueuePath);
			_ErrorQueue.Formatter = new BinaryMessageFormatter();
			// It was suggested that setting the messages' Recoverable property to "true" would allow us to store as many
			// messages in the queue as there was space available for on disk.  Rather than do it at them message level,
			// we set the property for every message entering the queue.
			bool IsRecoverableQueue;
			try 
			{
				IsRecoverableQueue = bool.Parse(RuntimeSettings.GetSetting(Constants.CFG_QUEUE_RECOVERABLEFLAG_SETTING));
			}
			catch (Exception ex){
				Metrics.Instance.WriteInformation("new Queue() failed to load FASTPUSHSVC_QUEUERECOVERABLEFLAG setting. Assuming false\n\n" + ex.ToString()); 
				IsRecoverableQueue = false;
			}
			_Queue.DefaultPropertiesToSend.Recoverable = IsRecoverableQueue;

		}

		/// <summary>
		/// Destructor
		/// </summary>
		~Queue(){
			if (_Queue != null){
				_Queue.Close();
				_Queue.Dispose();
			}

			if (_ErrorQueue != null){
				_ErrorQueue.Close();
				_ErrorQueue.Dispose();
			}
		}

		/// <summary>
		/// Adds a given profile pointer to the queue.
		/// </summary>
		/// <param name="pointer">The pointer to the profile to add to the queue.</param>
		public void EnqeueProfilePointer(IFastPushPointer pointer) {	
			try {
				_Queue.Send(pointer, pointer.MemberID.ToString());
			} 
			catch (Exception ex) {
				Metrics.Instance.WriteError("Unable to enqueue Profile [" + pointer.MemberID + "] " + ex.ToString());
				throw new Exception("Unable to enqueue Profile [" + pointer.MemberID + "] ", ex);
			}
		}

		/// <summary>
		/// Adds a given profile pointer to the queue.
		/// </summary>
		/// <param name="pointer">The pointer to the profile to add to the queue.</param>
		public void EnqeueErrorProfilePointer(IFastPushPointer pointer) {	
			try {
				_ErrorQueue.Send(pointer, pointer.MemberID.ToString());
			} 
			catch (Exception ex) {
				Metrics.Instance.WriteError("Unable to enqueue error pointer [" + pointer.MemberID + "] " + ex.ToString());
				throw new Exception("Unable to enqueue error pointer [" + pointer.MemberID + "] ", ex);
			}
		}

		/// <summary>
		/// Retreives a list of pointers from the queue
		/// </summary>
		/// <param name="BatchSize">Number of items to return (if available)</param>
		/// <remarks>This method will attempt to get the specified number of items. If they 
		/// don't exist, after a timeout period the method will return the available items 
		/// it could dequeue.
		/// </remarks>
		public IFastPushPointer [] DequeueProfilePointers(int BatchSize){
			ArrayList results = new ArrayList(BatchSize);
			TimeSpan tsTimeOut = new TimeSpan(0,0,10);

			try {
				IFastPushPointer ptr;
				for (int i = 0; i < BatchSize; i++){
					Message msg = _Queue.Receive(tsTimeOut); 
					ptr = msg.Body as IFastPushPointer;
					results.Add(ptr);
				}
			}
			catch (MessageQueueException e) {
				// A timeout is expected every now and then when not much to index.
				if (e.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout){
					Metrics.Instance.WriteError(e.ToString());
				}
			}

			return results.ToArray(typeof(IFastPushPointer)) as IFastPushPointer [];
		}


		/// <summary>
		/// Retreives a list of pointers from the error queue
		/// </summary>
		/// <param name="BatchSize">Number of items to return (if available)</param>
		/// <returns>A list of IFastPushPointer compliant items from the queueu</returns>
		/// <remarks>This method will attempt to get the specified number of items. If they 
		/// don't exist, after a timeout period the method will return the available items 
		/// it could dequeue.
		/// </remarks>
		public IFastPushPointer [] DequeueErrorProfilePointers(int BatchSize){
			ArrayList results = new ArrayList(BatchSize);
			TimeSpan tsTimeOut = new TimeSpan(0,0,10);

			try {
				IFastPushPointer ptr;
				for (int i = 0; i < BatchSize; i++){
					Message msg = _ErrorQueue.Receive(tsTimeOut); 
					ptr = msg.Body as IFastPushPointer;
					results.Add(ptr);
				}
			}
			catch (MessageQueueException e) {
				// A timeout is expected every now and then when not much to index.
				if (e.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout){
					Metrics.Instance.WriteError(e.ToString());
				}
			}

			return results.ToArray(typeof(IFastPushPointer)) as IFastPushPointer [];
		}
	}
}
