using System;
using System.Messaging;
using System.Collections;
using System.Threading;


using Matchnet.FAST.Push.Interfaces;
using Matchnet.FAST.Push.ValueObjects;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Represents a queue where notifications of new updates are dropped by external systems.
	/// This instance might mask multiple underlying queues.
	/// </summary>
	public class UpdateQueue {
		
		private static MessageQueue _PushQueue;
		private static MessageQueue _ErrorQueue;
		
		public UpdateQueue(){
			if (_PushQueue == null){
				_PushQueue = new MessageQueue(QueueConfiguration.Instance.PushQueuePath(1));
				_PushQueue.Formatter = new BinaryMessageFormatter();
			}
			
			if (_ErrorQueue == null){
				_ErrorQueue = new MessageQueue(QueueConfiguration.Instance.ErrorQueuePath);
				_ErrorQueue.Formatter = new BinaryMessageFormatter();
			}
		}

		/// <summary>
		/// destructor
		/// </summary>
		~UpdateQueue(){
			if (_PushQueue != null){
				_PushQueue.Close();
			}
			if (_ErrorQueue != null){
				_ErrorQueue.Close();
			}
		}

		
		/// <summary>
		/// Adds a given profile pointer to the queue.
		/// </summary>
		/// <param name="pointer">The pointer to the profile to add to the queue.</param>
		public void EnqeueProfilePointer(IFastPushPointer pointer, bool IsErrorItem) {
			
			MessageQueue _Queue = IsErrorItem ? _ErrorQueue: _PushQueue;

			try {
				_Queue.Send(pointer, pointer.MemberID.ToString());
			} 
			catch (Exception ex) {
				Metrics.Instance.WriteError("Unable to Queue Profile [" + pointer.MemberID + "] " + ex.ToString());
				throw new Exception("Unable to Queue Profile [" + pointer.MemberID + "]", ex);
			}
		}

		/// <summary>
		/// Retreives a list of pointers from the queue
		/// </summary>
		/// <param name="BatchSize">Number of items to return (if available)</param>
		/// <returns></returns>
		public ArrayList DequeueProfilePointers(int BatchSize, bool UsePushQueue){
			ArrayList results = new ArrayList(BatchSize);
			TimeSpan tsTimeOut = new TimeSpan(0,0,30);

			MessageQueue _Queue = UsePushQueue? _PushQueue: _ErrorQueue;

			int ThisCommunityID = 0;

			try {
				for (int i = 0; i < BatchSize; i++){
					Message msg = _Queue.Receive(tsTimeOut); 
					IFastPushPointer ptr = (IFastPushPointer)msg.Body;

					if (ThisCommunityID == 0){ // first time around, first item choses domain.
						ThisCommunityID = ptr.CommunityID;
					}

					if (ptr.CommunityID != ThisCommunityID){ // batch established for DOmainID, but this item doesn't belong. Throw it back.
						EnqeueProfilePointer(ptr,true);
					}
					results.Add(ptr);
				}
			}
			catch (MessageQueueException e) {
				// A timeout is expected every now and then when not much to index.
				if (e.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout){
					Metrics.Instance.WriteError(e.ToString());
				}
			}

			return results;
		}
	}
}