using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

using Matchnet.Caching;
using Matchnet.FAST.Push.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.FAST.Push.BusinessLogic
{
	/// <summary>
	/// Provides access to Region information
	/// </summary>
	public class RegionBL	
	{
		/// <summary>
		/// The one instance of this object
		/// </summary>
		public static readonly RegionBL Instance = new RegionBL();

		/// <summary>
		/// The connection string to the DB supporting this region lookup
		/// </summary>
		public static string RegionSourceConnection = RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_REGION_SOURCE_CONNECTION);
		public static string BulkImportBatchConnection = RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_BATCH_CONNECTION);

		private RegionBL(){
			// hidden empty constructor
		}

		/// <summary>
		/// Retreives a Region object from DB or cache
		/// </summary>
		/// <param name="RegionID">The RegionID of the region to retreive</param>
		/// <returns>The region object if found, null otherwise.</returns>
		public Region GetRegion(int RegionID) 
		{
			try {
				object obj = Matchnet.Caching.Cache.Instance.Get(Constants.PUSH_REGION_CACHE_PREFIX + RegionID.ToString());

				if (obj == null) {
					SqlConnection conn = new SqlConnection(RegionSourceConnection);
					SqlCommand command = new SqlCommand("up_Region_Get", conn);
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.Add("@RegionID", SqlDbType.Int).Value = RegionID;

					DataTable table = new DataTable();
					SqlDataAdapter da = new SqlDataAdapter(command);

					try
					{
						conn.Open();
						da.Fill(table);
					}
					finally
					{
						conn.Close();
					}
					/*
					Command command = new Command("mnRegion", "up_Region_Get", 0);
					command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, RegionID);
					DataTable table = Client.Instance.ExecuteDataTable(command);
					*/

					if (table != null && table.Rows.Count > 0) {
						Region region = new Region(RegionID,
											Convert.ToSingle(table.Rows[0]["Longitude"]),
											Convert.ToSingle(table.Rows[0]["Latitude"]),
											Convert.ToString(table.Rows[0]["Country"]),
											Convert.ToString(table.Rows[0]["City"]),
											Convert.ToString(table.Rows[0]["State"]),
											Convert.ToString(table.Rows[0]["Zip"]),
											Convert.ToInt32(table.Rows[0]["Depth1RegionID"]),
											Convert.ToInt32(table.Rows[0]["Depth2RegionID"]),
											Convert.ToInt32(table.Rows[0]["Depth3RegionID"]),
											Convert.ToInt32(table.Rows[0]["Depth4RegionID"]),
											Convert.ToInt32(table.Rows[0]["AreaCode"]));
							
						Matchnet.Caching.Cache.Instance.Insert(region);
						return region;
					} 
					else {
						return null;
					}
				} 
				else {
					return (Region)obj;
				}
			} 
			catch (Exception ex) {
				Metrics.Instance.WriteError("Unable to get Region [" + RegionID + "] " + ex.ToString());
				throw new Exception("Unable to get Region [" + RegionID + "]", ex);
			}
		}

		/// <summary>
		/// Gets a list of regions which are near the given region specified
		/// </summary>
		/// <param name="RegionID">The regionid around which to return the near regions</param>
		/// <param name="Longitude">Longitude in radian degrees</param>
		/// <param name="Latitude">Latitude in radian degrees</param>
		/// <returns>RegionsNear structure populated with nearby reagions to the one specified</returns>
		public RegionsNear GetRegionsNear(int RegionID, double Longitude, double Latitude){

			try {
				object obj = Matchnet.Caching.Cache.Instance.Get(RegionsNear.GetCacheKey(RegionID));

				if (obj == null) {
					RegionsNear result;
					GeoBox.Location  loc = GeoBox.CreateLocation(Longitude, Latitude );		

					RegionNear [] ary = new RegionNear[loc.Grids.Length];
					for (int i = 0; i < loc.Grids.Length; i++){
						ary[i] = new RegionNear(RegionID,i, loc.Grids[i].ToString());
					}

					result = new RegionsNear(RegionID,ref ary);
					Matchnet.Caching.Cache.Instance.Insert(result);
					return result;
				} 
				else {
					return obj as RegionsNear ;
				}
			} 
			catch (Exception ex) {
				Metrics.Instance.WriteError("Unable to get Region [" + RegionID + "] " + ex.ToString());
				throw new Exception("Unable to get Region [" + RegionID + "]", ex);
			}

		}

	}

}
