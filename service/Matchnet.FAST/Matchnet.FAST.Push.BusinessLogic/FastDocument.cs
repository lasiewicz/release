using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using Matchnet.FAST.Push.ValueObjects;
using Matchnet.FAST.Push.Interfaces;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;

using no.fast.ds.content;

namespace Matchnet.FAST.Push.BusinessLogic 
{
	/// <summary>
	/// Summary description for FastDocument.
	/// </summary>
	public class FastDocument 
	{
		private IFastExclusionRule _rule;
		private FastDocumentElements _fastDocElements = new FastDocumentElements();
		private int _memberID;
		private int _communityID;
		private bool _isExcluded = false;

		/// <summary>
		/// connection string to DB of Member attributes data
		/// </summary>
		public static string _bulkImport_Connection_MemberAttributes = RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_MEMBER_CONNECTION);
		public static bool _bulkImport_LoadFromDBFlag = Boolean.Parse(RuntimeSettings.GetSetting(Constants.CFG_BULK_IMPORT_MODE_LOADFROMDB));

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="pointer">a member pointer to populate from</param>
		public FastDocument(IFastPushPointer pointer)
		{
			_memberID = pointer.MemberID;
			_communityID = pointer.CommunityID;
			_rule = new ExclusionRule(_communityID);

			if (pointer.ActionID == IndexAction.Delete)	// Delete from index.
			{
				// Simply set this value and it will be deleted later.  We do not need to load
				// Member Attributes for a delete.
				_isExcluded = true;
				Trace.WriteLine("__FASTPUSH m " + pointer.MemberID.ToString() + " / c " + pointer.CommunityID.ToString() + " / action? " + pointer.ActionID.ToString() );
			}
			else	// Add/Update to index.
			{
				// Load the Member Attributes.
				if (_bulkImport_LoadFromDBFlag)
				{
					Trace.WriteLine("!!! LoadFromDB() deprecated !!!");
					//LoadProfileFromDB();
				}
				else
				{
					LoadProfileFromService();
				}
			}
		}

		/// <summary>
		/// TODO:
		/// NOTE 1:  Currently, we assume that the languageID to use for a Community is the LanguageID from the
		/// Site table for the "default"/most popular Site for that Community.
		/// NOTE 2:  We will eventually want this to be data-driven (the CommunityIDs can already be found in the dbase).
		/// </summary>
		private void LoadProfileFromService()
		{
			try
			{
				// Get Member from Member service.
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None); // MemberLoadFlags.ForceLoad deprecated per Garry.

				// Map attributes that are not real "Attributes" but will be needed to be added to the index, nonetheless.
				AddFakeAttributes();

				// Interate through all MemberAttributes.
				switch (_communityID)
				{
					case 1:
						AddAttributesFromService(member, 1, LanguageID.English, Constants.MEMBERATTRIBUTES_TO_INDEX_GLOBAL);
						break;
					case 2:
						AddAttributesFromService(member, 2, LanguageID.English, Constants.MEMBERATTRIBUTES_TO_INDEX_GLOBAL);
						break;
					case 3:
						AddAttributesFromService(member, 3, LanguageID.English, Constants.MEMBERATTRIBUTES_TO_INDEX_GLOBAL);
						break;
					case 10:
						AddAttributesFromService(member, 10, LanguageID.Hebrew, Constants.MEMBERATTRIBUTES_TO_INDEX_GLOBAL);
						break;
					case 12:
						AddAttributesFromService(member, 12, LanguageID.English, Constants.MEMBERATTRIBUTES_TO_INDEX_GLOBAL);
						break;
					case 17:
						AddAttributesFromService(member, 17, LanguageID.English, Constants.MEMBERATTRIBUTES_TO_INDEX_GLOBAL);
						break;
				}
			}
			catch (Exception ex) 
			{
				throw new Exception("Failure creating FAST document for Member [" + _memberID + "]", ex);
			}
		}


		private void AddAttributesFromService(Matchnet.Member.ServiceAdapters.Member Member, int CommunityID, LanguageID LanguageIDVal, Constants.AttributeMapElement [] MemberAttributesToIndex)
		{
			// everyone should have one of these..
			try 
			{
                Matchnet.Content.ValueObjects.BrandConfig.Brands brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsByCommunity(CommunityID);
                Matchnet.Content.ValueObjects.BrandConfig.Brand theBrand = null;
                foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
                {
                    theBrand = brand;
                }

				AddAttribute(new ProfileAttribute(VOConstants.ATTRIBUTENAME_USERNAME, CommunityID, Member.GetUserName(theBrand)));
			}
			catch (Exception ex)
			{
				throw new Exception("Add AttributeFromService failed adding username for " + MemberID.ToString(), ex);
			}


			for (int i = 0; i < MemberAttributesToIndex.Length; i++)
			{
				try 
				{
					string attribVal = string.Empty;

					// Retrieve the value.  Be sure to make the right call according to DataType.
					switch (MemberAttributesToIndex[i].DataType)
					{
						case AttributeDataType.Integer:
							int iVal = Member.GetAttributeInt(CommunityID, 0, 0, MemberAttributesToIndex[i].AttributeName, Matchnet.Constants.NULL_INT);

							if (iVal == Matchnet.Constants.NULL_INT)
							{
								attribVal = string.Empty;
							}
							else
							{
								if (MemberAttributesToIndex[i].AttributeName == VOConstants.ATTRIBUTENAME_POPULARITYRANK){
									iVal = (iVal == 0)? iVal: (int)Math.Log((double)(iVal + 1),2);
								}
								attribVal = Convert.ToString(iVal);
							}
							break;
						case AttributeDataType.String:
							// SPECIAL CASE:  ThumbPath.
							if (MemberAttributesToIndex[i].AttributeName == VOConstants.ATTRIBUTENAME_THUMBPATH)
							{
								Matchnet.Member.ValueObjects.Photos.PhotoCommunity photos = Member.GetPhotos(CommunityID);
								if (photos.Count > 0 && photos[0].IsApproved)
								{	
									if (photos[0].IsPrivate)
									{ 
										/// HACK: 2005-05-30
										/// only Cupid currently has these
										/// Since eventual "PhotoIsPrivate.jpg" logic requires brand, and there is not brand 
										/// in this scope, only community, I'll shove a fixed marker here.
										/// MatchMail would need to sense that it's Cupid, and use the brand there - which is available - 
										/// to replace full thumbpath to the appropriate one.
										attribVal = "private.jpg";
									}
									else 
									{
										attribVal = photos[0].ThumbFileWebPath;
									}
									AddAttribute(new ProfileAttribute(VOConstants.ATTRIBUTENAME_HASPHOTOFLAG ,CommunityID,"1"));
								}
								else
								{
									AddAttribute(new ProfileAttribute(VOConstants.ATTRIBUTENAME_HASPHOTOFLAG ,CommunityID,"0"));
								}
							}
							else
							{
								attribVal = Member.GetAttributeText(CommunityID, 0, 0, Convert.ToInt32(LanguageIDVal), MemberAttributesToIndex[i].AttributeName);
							}
							break;
						case AttributeDataType.Boolean:
							if (Member.GetAttributeBool(CommunityID, 0, 0, MemberAttributesToIndex[i].AttributeName))
							{
								attribVal = VOConstants.BOOL_TRUE;
							}
							else
							{
								attribVal = VOConstants.BOOL_FALSE;
							}
							break;
						case AttributeDataType.DateTime: // SPECIAL CASES:  LastLogonDate, MemberRegistrationDate.
							DateTime dtVal;
							switch (MemberAttributesToIndex[i].AttributeName) 
							{
								case VOConstants.ATTRIBUTENAME_LASTLOGONDATE:
									dtVal = Member.GetLastLogonDate(CommunityID);
									attribVal = Convert.ToString(dtVal);
									if (dtVal > DateTime.MinValue) 
									{
										AddAttribute(new ProfileAttribute(VOConstants.ATTRIBUTENAME_LASTLOGONDATE_EPOCH,CommunityID,EpochUtil.GetEpochDateTokens(dtVal)));
									}
									break;
								case VOConstants.ATTRIBUTENAME_MEMBERREGISTRATIONDATE:
									attribVal = Convert.ToString(Member.GetInsertDate(CommunityID));
									break;
								default:
									dtVal = Member.GetAttributeDate(CommunityID, 0, 0, MemberAttributesToIndex[i].AttributeName, DateTime.MinValue);
									if (dtVal == DateTime.MinValue)
									{
										attribVal = string.Empty;
									}
									else
									{
										attribVal = Convert.ToString(dtVal);
									}
									break;
							}
							break;
					}

					if (attribVal != string.Empty  && attribVal != null)
					{	// Short circuit the loop if the AddAttribute broke an Exclusion rule.
						if (!AddAttribute(new ProfileAttribute( MemberAttributesToIndex[i].AttributeName , CommunityID, attribVal, LanguageIDVal)))
							break;
					}
				}
				catch (Exception ex)
				{
					throw new Exception("Failure Getting Attibute from Service " + Member.MemberID.ToString() + "(" + i.ToString() + ") " + MemberAttributesToIndex[i].AttributeName ,ex);
				}
			}

			_isExcluded = _rule.IsExcluded;
		}

		/// <summary>
		/// These attributes are not real "Attributes" from the dbase but will be needed to be added to the index, nonetheless.
		/// </summary>
		private void AddFakeAttributes()
		{
			_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_MEMBERID, _memberID)));
			_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_COMMUNITYID, _communityID)));
			// The following attribute is used simply to see that the profile was reindexed.  For TESTING.
			_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_MATCHNETMETA, DateTime.Now.ToString("g"))));
		}


		private bool AddAttribute(ProfileAttribute ProfileAttrib)
		{
			// RegionID is handled differently.
			switch (ProfileAttrib.AttributeName)
			{
				case VOConstants.ATTRIBUTENAME_REGIONID:
					// If the Region passed the Exclusion rule and a valid Region was retrieved then return true.
					if ((!_rule.AddEvidence(new ExclusionRule.ExclusionEvidence(ProfileAttrib.AttributeName, ProfileAttrib.Value))) && (PopulateRegionAttributes(Convert.ToInt32(ProfileAttrib.Value), _communityID)))
						return true;
					else
					{
						_isExcluded = true;
						return false;
					}

					// HWBOOST CURRENTLY NOT USED.  THIS CASE WAS CREATED SPECIFICALLY FOR HWBOOST.
					// LEFT HERE IN CASE WE USE IT IN THE FUTURE.
					//				case VOConstants.ATTRIBUTENAME_LASTLOGONDATE:
					//					PopulateLastLogonDateAttributes(ProfileAttrib.Value);
					//
					//					return true;

				default:
					if (!_rule.AddEvidence(new ExclusionRule.ExclusionEvidence(ProfileAttrib.AttributeName, ProfileAttrib.Value))) 
					{
						FastDocumentElement fastDocElement = MapElement(ProfileAttrib);

						if (fastDocElement != null)
						{
							_fastDocElements.Add(fastDocElement);
						}
						else 
						{
#if DEBUG
							Trace.WriteLine("Un-mappable Attribute " + ProfileAttrib.AttributeName + " encountered.");
#endif
						}

						return true;
					}
					else 
					{
						_isExcluded = true;
						return false;
					}
			}
		}


		/// <summary>
		/// Transforms this instance to a FAST API content document and returns it.
		/// </summary>
		/// <param name="ContentFactory">the content factory object to use</param>
		/// <returns>A pupulated no.fast.ds.content.Document</returns>
		public no.fast.ds.content.Document GetDocument(no.fast.ds.content.IContentFactory ContentFactory)
		{
			no.fast.ds.content.Document FastDocumentResult = ContentFactory.CreateEmptyDocument(this.DocumentID);
			TimeSpan ts1, ts2;

#if DEBUG
			Trace.WriteLine("=========== MemberID " + _memberID + "(" + _communityID + ") ===============");
#endif
			ts1 = new TimeSpan(DateTime.Now.Ticks);

			//TODO: Add matchnetmeta mapping / injection.
			foreach (FastDocumentElement fde in _fastDocElements) 
			{
				try 
				{
					switch (fde.Type) 
					{
						case FastDocumentElementType.DateTime :
							FastDocumentResult.AddElement(new DateTimeElement(fde.Name, Convert.ToDateTime(fde.Value)));
							break;
						case FastDocumentElementType.Double  :
							FastDocumentResult.AddElement(new DoubleElement(fde.Name, Convert.ToDouble(fde.Value)));
							break;
						case FastDocumentElementType.Float  :
							FastDocumentResult.AddElement(new FloatElement(fde.Name,(float)fde.Value));
							break;
						case FastDocumentElementType.Int32  :
							FastDocumentResult.AddElement(new IntegerElement(fde.Name, Convert.ToInt32(fde.Value)));
							break;
						case FastDocumentElementType.String  :
							if ((fde.Value.ToString()).Length > 0)
							{
								FastDocumentResult.AddElement(new StringElement(fde.Name,fde.Value.ToString()));
							}
							else
							{
								Trace.WriteLine("String of length 0 " + fde.Name);
							}
							break;
						case FastDocumentElementType.StringCol :
							if ( FastDocumentResult.HasElement(fde.Name))
							{
								StringElement existing = FastDocumentResult.GetElement(fde.Name) as StringElement  ;
								existing.Data += " " + fde.GetPrefixedValue();
								FastDocumentResult.AddElement(existing);
							}
							else 
							{
								FastDocumentResult.AddElement(new StringElement(fde.Name,fde.GetPrefixedValue()));
							}
							break;	
						default:
							Metrics.Instance.WriteError("Unknown FastDocumentElement type " + fde.ToString());
							break;
					}
				}
				catch (Exception ex)
				{
					Metrics.Instance.WriteError(string.Format("Failed Add Element {0} {1} {2}\n\n{3}",fde.Name,fde.Type,fde.Value,ex.Message));
					return null;
				}
			}
			
#if DEBUG
			ts2 = new TimeSpan(DateTime.Now.Ticks);
			Trace.WriteLine(string.Format("*** Manufactured FAST API doc of {0} attributes in {1} ms",_fastDocElements.Count ,ts2.Subtract(ts1).TotalMilliseconds));
#endif
			Trace.WriteLine("__FASTPUSH m " + this.DocumentID.ToString() + " / c " + this.CommunityID.ToString() + " / exclude? " + this.IsExcluded.ToString());
			return FastDocumentResult;
		}


		/// <summary>
		/// Deprecated. Helper for direct load profile from DB.
		/// </summary>
		/// <param name="Row"></param>
		/// <returns></returns>
		private ProfileAttribute ProfileAttributeFromDataRow(DataRow Row)
		{
			try
			{
				if (Row["Value"] != DBNull.Value)
				{	
					if (Row["DomainID"] != DBNull.Value)
						return new ProfileAttribute(Row["AttributeName"].ToString(), (int)Row["DomainID"], Row["Value"], (LanguageID)Row["LanguageID"]); 
					else
						return new ProfileAttribute(Row["AttributeName"].ToString(), Row["Value"]); 
				}
				else
					return null;
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex);
				return null;
			}
		}


		/// <summary>
		/// Helper to expand the region attributes from a single RegionID from the profile.
		/// </summary>
		/// <param name="RegionID"></param>
		private bool PopulateRegionAttributes(int RegionID, int CommunityID)
		{
			try
			{
				Region region = RegionBL.Instance.GetRegion(RegionID);

				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_REGIONID, region.RegionID)));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_LATITUDE, region.Latitude)));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_LONGITUDE, region.Longitude)));
            
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_COUNTRY, region.Country)));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_STATE, region.State)));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_CITY, region.City)));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_POSTALCODE, "ZIP" + region.Zip)));

				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_COUNTRY, "RID" + region.Depth1RegionID.ToString())));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_STATE, "RID" + region.Depth2RegionID.ToString())));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_CITY, "RID" + region.Depth3RegionID.ToString())));
				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_POSTALCODE, "RID" + region.Depth4RegionID.ToString())));

				_fastDocElements.Add(MapElement(new ProfileAttribute(VOConstants.ATTRIBUTENAME_AREACODE, CommunityID, region.AreaCode.ToString())));

				RegionsNear regionsNear = RegionBL.Instance.GetRegionsNear(RegionID, region.Longitude, region.Latitude);

				if (regionsNear.Bands != null)
				{
					foreach(RegionNear band in regionsNear.Bands)
					{
						string attributeName = string.Empty;

						switch (band.BandID)
						{
							case 0: attributeName = VOConstants.ATTRIBUTENAME_GEO1; break;
							case 1: attributeName = VOConstants.ATTRIBUTENAME_GEO2; break;
							case 2: attributeName = VOConstants.ATTRIBUTENAME_GEO3; break;
							case 3: attributeName = VOConstants.ATTRIBUTENAME_GEO4; break;
							case 4: attributeName = VOConstants.ATTRIBUTENAME_GEO5; break;
							case 5: attributeName = VOConstants.ATTRIBUTENAME_GEO6; break;
							case 6: attributeName = VOConstants.ATTRIBUTENAME_GEO7; break;
						}
						if (attributeName != string.Empty)
						{
							_fastDocElements.Add(MapElement(new ProfileAttribute(attributeName, band.RegionsNear))); 
						}
					}
				}

				return true;
			}
			catch (Exception ex)
			{
				Trace.WriteLine(ex);
				return false;
			}
		}


		/// <summary>
		/// Maps a Matchnet ProfileAttribute to a FastDocumentElement.
		/// </summary>
		/// <param name="ProfileAttrib">A Matchnet ProfileAttribute from mnShared.dbo.Attribute table.</param>
		/// <returns>FastDocumentElement with the name and type expected by the profile index and pipeline.</returns>
		private FastDocumentElement MapElement(ProfileAttribute ProfileAttrib)
		{
			FastDocumentElement fastDocElement = new FastDocumentElement();
			
			switch (ProfileAttrib.AttributeName)
			{
				case VOConstants.ATTRIBUTENAME_MEMBERID: fastDocElement.Name = "memberid"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_CITY: fastDocElement.Name = "geoencity"; fastDocElement.Type = FastDocumentElementType.StringCol; break;
				case VOConstants.ATTRIBUTENAME_STATE: fastDocElement.Name = "geoenstate"; fastDocElement.Type = FastDocumentElementType.StringCol; break;
				case VOConstants.ATTRIBUTENAME_COUNTRY: fastDocElement.Name = "geoencountry"; fastDocElement.Type = FastDocumentElementType.StringCol; break;
				case VOConstants.ATTRIBUTENAME_POSTALCODE: fastDocElement.Name = "geopostalcode"; fastDocElement.Type = FastDocumentElementType.StringCol; break;
				case VOConstants.ATTRIBUTENAME_MARITALSTATUS: fastDocElement.Name = "maritalstatus"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_COMMUNITYID: fastDocElement.Name = "domainid"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GENDERMASK: fastDocElement.Name = "gendermask"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_BIRTHDATE: fastDocElement.Name = "birthdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_DISPLAYPHOTOSTOGUESTS: fastDocElement.Name = "displayphotostoguests"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_MUSIC: fastDocElement.Name = "music"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_HEADLINE: fastDocElement.Name = "headline"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_HEIGHT: fastDocElement.Name = "height"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_WEIGHT: fastDocElement.Name = "weight"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_HAIRCOLOR: fastDocElement.Name = "haircolor"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_EYECOLOR: fastDocElement.Name = "eyecolor"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_BODYTYPE: fastDocElement.Name = "bodytype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ETHNICITY: fastDocElement.Name = "ethnicity"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_EDUCATIONLEVEL: fastDocElement.Name = "educationlevel"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_OUTNESSTYPE: fastDocElement.Name = "outnesstype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_SMOKINGHABITS: fastDocElement.Name = "smokinghabits"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DRINKINGHABITS: fastDocElement.Name = "drinkinghabits"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ZODIAC: fastDocElement.Name = "zodiac"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_RELIGION: fastDocElement.Name = "religion"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_PERSONALITYTRAIT: fastDocElement.Name = "personalitytrait"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_APPEARANCEIMPORTANCE: fastDocElement.Name = "appearanceimportance"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_INTELLIGENCEIMPORTANCE: fastDocElement.Name = "intelligenceimportance"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_FAVORITETIMEOFDAY: fastDocElement.Name = "favoritetimeofday"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_POLITICALORIENTATION: fastDocElement.Name = "politicalorientation"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_CUSTODY: fastDocElement.Name = "custody"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_CUISINE: fastDocElement.Name = "cuisine"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_LEISUREACTIVITY: fastDocElement.Name = "leisureactivity"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_PHYSICALACTIVITY: fastDocElement.Name = "physicalactivity"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ENTERTAINMENTLOCATION: fastDocElement.Name = "entertainmentlocation"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_LASTUPDATED: fastDocElement.Name = "lastupdated"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_REGIONID: fastDocElement.Name = "regionid"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_ABOUTME: fastDocElement.Name = "aboutme"; fastDocElement.Type = FastDocumentElementType.String; break; 
				case VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY: fastDocElement.Name = "perfectmatchessay"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY: fastDocElement.Name = "perfectfirstdateessay"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY: fastDocElement.Name = "idealrelationshipessay"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY: fastDocElement.Name = "learnfromthepastessay"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION: fastDocElement.Name = "occupationdescription"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_INCOMELEVEL: fastDocElement.Name = "incomelevel"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ACTIVITYLEVEL: fastDocElement.Name = "activitylevel"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_PRIVATELABELID: fastDocElement.Name = "privatelabelid"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_ISRAELAREACODE: fastDocElement.Name = "geoareacode"; fastDocElement.Type = FastDocumentElementType.StringCol; 
					fastDocElement.TokenPrefix = "AC" + ProfileAttrib.CommunityID.ToString(); 
					break;
				case VOConstants.ATTRIBUTENAME_SMSCAPABLE: fastDocElement.Name = "smscapable"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_HASPHOTOFLAG: fastDocElement.Name = "hasphotoflag"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_ISRAELIPERSONALITYTRAIT: fastDocElement.Name = "israelipersonalitytrait"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_CANNOTDOWITHOUT: fastDocElement.Name = "cannotdowithout"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_WHENIHAVETIME: fastDocElement.Name = "whenihavetime"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_READING: fastDocElement.Name = "reading"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_APPEARINGONTHEMEDIA: fastDocElement.Name = "appearingonthemedia"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_FACEPAGEINTRODUCTION: fastDocElement.Name = "facepageintroduction"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_HAVECHILDREN: fastDocElement.Name = "havechildren"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_PETS: fastDocElement.Name = "pets"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_RELOCATEFLAG: fastDocElement.Name = "relocateflag"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_GREWUPIN: fastDocElement.Name = "grewupin"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_MORECHILDRENFLAG: fastDocElement.Name = "morechildrenflag"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS: fastDocElement.Name = "studiesemphasis"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_RELATIONSHIPMASK: fastDocElement.Name = "relationshipmask"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_USERNAME: fastDocElement.Name = "username"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_HABITATTYPE: fastDocElement.Name = "habitattype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST: fastDocElement.Name = "favoritealbumsongartist"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_FAVORITEBOOKAUTHOR: fastDocElement.Name = "favoritebookauthor"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR: fastDocElement.Name = "favoritemoviedirectoractor"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_IDEALVACATION: fastDocElement.Name = "idealvacation"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY: fastDocElement.Name = "aboutmyappearanceessay"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_SEXUALIDENTITYTYPE: fastDocElement.Name = "sexualidentitytype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_JDATEETHNICITY: fastDocElement.Name = "jdateethnicity"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_JDATERELIGION: fastDocElement.Name = "jdatereligion"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_SYNAGOGUEATTENDANCE: fastDocElement.Name = "synagogueattendance"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_KEEPKOSHER: fastDocElement.Name = "keepkosher"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_CHILDRENCOUNT: fastDocElement.Name = "childrencount"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_LANGUAGEMASK: fastDocElement.Name = "languagemask"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDMINAGE: fastDocElement.Name = "desiredminage"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDMAXAGE: fastDocElement.Name = "desiredmaxage"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDMARITALSTATUS: fastDocElement.Name = "desiredmaritalstatus"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDJDATERELIGION: fastDocElement.Name = "desiredjdatereligion"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDEDUCATIONLEVEL: fastDocElement.Name = "desirededucationlevel"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDDRINKINGHABITS: fastDocElement.Name = "desireddrinkinghabits"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_DESIREDSMOKINGHABITS: fastDocElement.Name = "desiredsmokinghabits"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_INDUSTRYTYPE: fastDocElement.Name = "industrytype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_TRAVELMASK: fastDocElement.Name = "travelmask"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_BIRTHCOUNTRYREGIONID: fastDocElement.Name = "birthcountryregionid"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_ISRAELIMMIGRATIONDATE : fastDocElement.Name = "israelimmigrationdate"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_MILITARYSERVICETYPE: fastDocElement.Name = "militaryservicetype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_RELATIONSHIPSTATUS: fastDocElement.Name = "relationshipstatus"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_RELIGIONACTIVITYLEVEL: fastDocElement.Name = "religionactivitylevel"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_MOVIEGENREMASK: fastDocElement.Name = "moviegenremask"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ESSAYFRIENDSDESCRIPTION: fastDocElement.Name = "essayfriendsdescription"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ESSAYDESCRIBEHUMOR: fastDocElement.Name = "essaydescribehumor"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ESSAYFIRSTTHINGNOTICEABOUTYOU: fastDocElement.Name = "essayfirstthingnoticeaboutyou"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_ESSAYORGANIZATIONS: fastDocElement.Name = "essayorganizations"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_MAJORTYPE: fastDocElement.Name = "majortype"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_BODYART: fastDocElement.Name = "bodyart"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_SCHOOLID: fastDocElement.Name = "schoolid"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GAYSCENEMASK: fastDocElement.Name = "gayscenemask"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_HIVSTATUSTYPE: fastDocElement.Name = "hivstatustype"; fastDocElement.Type = FastDocumentElementType.String; break;
					// DEPRECATED:  Same as LASTBRANDID but used for Bulk Adding from legacy system.
				case VOConstants.ATTRIBUTENAME_LASTPRIVATELABELID: fastDocElement.Name = "lastprivatelabelid"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_LASTBRANDID: fastDocElement.Name = "lastprivatelabelid"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_LATITUDE: fastDocElement.Name = "latitude"; fastDocElement.Type = FastDocumentElementType.Float; break;
				case VOConstants.ATTRIBUTENAME_LONGITUDE: fastDocElement.Name = "longitude"; fastDocElement.Type = FastDocumentElementType.Float; break;
				case VOConstants.ATTRIBUTENAME_AREACODE: fastDocElement.Name = "geoareacode"; fastDocElement.Type = FastDocumentElementType.StringCol; 
					fastDocElement.TokenPrefix = "AC" + ProfileAttrib.CommunityID.ToString(); 
					break;
				case VOConstants.ATTRIBUTENAME_MEMBERREGISTRATIONDATE: fastDocElement.Name = "memberregistrationdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_LASTLOGONDATE: fastDocElement.Name = "lastlogondate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				// 2005-07-05: tokens added to ATTRIBUTENAME_LASTLOGONDATE_EPOCH meant to participate in ranking only.
				case VOConstants.ATTRIBUTENAME_LASTLOGONDATE_EPOCH: fastDocElement.Name = "weight1combo"; fastDocElement.Type = FastDocumentElementType.StringCol; break;
				case VOConstants.ATTRIBUTENAME_ACTIVENESSRANK: fastDocElement.Name = "activenessrank"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_ACTIVENESSRANKDATE: fastDocElement.Name = "activenessrankdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_CATALYSTRANK: fastDocElement.Name = "catalystrank"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_CATALYSTRANKDATE: fastDocElement.Name = "catalystrankdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_FEATUREUSERANK: fastDocElement.Name = "featureuserank"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_FEATUREUSERANKDATE: fastDocElement.Name = "featureuserankdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_GEO1: fastDocElement.Name = "geo1"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GEO2: fastDocElement.Name = "geo2"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GEO3: fastDocElement.Name = "geo3"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GEO4: fastDocElement.Name = "geo4"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GEO5: fastDocElement.Name = "geo5"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GEO6: fastDocElement.Name = "geo6"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_GEO7: fastDocElement.Name = "geo7"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_MATCHNETMETA: fastDocElement.Name = "matchnetmeta"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_POPULARITYRANK: fastDocElement.Name = "popularityrank"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_POPULARITYRANKDATE: fastDocElement.Name = "popularityrankdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_RESPONSIVENESSRANK: fastDocElement.Name = "responsivenessrank"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_RESPONSIVENESSRANKDATE: fastDocElement.Name = "responsivenessrankdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_USEDRANK: fastDocElement.Name = "usedrank"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				case VOConstants.ATTRIBUTENAME_USEDRANKDATE: fastDocElement.Name = "usedrankdate"; fastDocElement.Type = FastDocumentElementType.DateTime; break;
				case VOConstants.ATTRIBUTENAME_THUMBPATH: fastDocElement.Name = "thumbpath"; fastDocElement.Type = FastDocumentElementType.String; break;
				case VOConstants.ATTRIBUTENAME_HWBOOST: fastDocElement.Name = "hwboost"; fastDocElement.Type = FastDocumentElementType.Int32; break;
				default: return null;
			}

			fastDocElement.Value = ProfileAttrib.Value; 
			fastDocElement.Language = ProfileAttrib.Language;

			return fastDocElement;
		}

		#region Properties
		/// <summary>
		/// Gets or sets whether this document should go to indexes or should be removed.
		/// </summary>
		public bool IsExcluded
		{
			get
			{
				return _isExcluded;
			}
			set
			{
				_isExcluded = value;
			}
		}

		/// <summary>
		/// Document ID (unique within collection, not globaly!).
		/// </summary>
		public string DocumentID
		{
			get
			{
				return _memberID.ToString();
			}
		}


		/// <summary>
		/// MemberID
		/// </summary>
		public int MemberID
		{
			get
			{ 
				return _memberID; 
			}
		}

		/// <summary>
		/// CommunityID.
		/// </summary>
		public int CommunityID
		{
			get
			{
				return _communityID;
			}
		}
		#endregion
	
	}
}



