using System;

namespace Matchnet.FAST.Push.BusinessLogic {
	/// <summary>
	/// Represents a single Profile Attribute (a.k.a MemberAttribute).
	/// </summary>
	[Serializable]
	public class ProfileAttribute
	{
		private string _attributeName;
		private int _communityID;
		private object _value;
		private LanguageID _language;

		#region Constructors
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="AttributeName">The DB mnShared Attribute name.</param>
		/// <param name="CommunityID">CommunityID.</param>
		/// <param name="Value">Value represented as string.</param>
		public ProfileAttribute(string AttributeName, int CommunityID, object Value)
		{
			_attributeName = AttributeName;
			_communityID = CommunityID;
			_value = Value;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="AttributeName">The DB mnShared Attribute name.</param>
		/// <param name="CommunityID">CommunityID.</param>
		/// <param name="Value">Value represented as string.</param>
		public ProfileAttribute(string AttributeName, int CommunityID, object Value, LanguageID Language)
		{
			_attributeName = AttributeName;
			_communityID = CommunityID;
			_value = Value;
			_language = Language;
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="AttributeName">The DB mnShared Attribute name.</param>
		/// <param name="Value">Value represented as string.</param>
		public ProfileAttribute(string AttributeName, object Value)
		{
			_attributeName = AttributeName;
			_value = Value;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets AttributeName.
		/// </summary>
		public string AttributeName 
		{
			get
			{ 
				return _attributeName; 
			}
			set
			{ 
				_attributeName = value; 
			}
		}

		/// <summary>
		/// Gets or sets CommunityID.
		/// </summary>
		public int CommunityID
		{
			get
			{ 
				return _communityID; 
			}
			set
			{ 
				_communityID = value; 
			}
		}

		//TODO: Investigate why this is all string instead of the native type.
		/// <summary>
		/// Gets or sets the attribute value.
		/// </summary>
		public object Value
		{
			get
			{ 
				return _value; 
			}
			set
			{ 
				_value = value; 
			}
		}

		public LanguageID Language
		{
			get
			{ 
				return _language ;
			}
			set 
			{ 
				_language = value;
			}
		}
		#endregion
	}
}
