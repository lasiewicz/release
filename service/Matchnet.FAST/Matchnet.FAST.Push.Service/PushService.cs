using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.FAST.Push.ValueObjects;
using Matchnet.FAST.Push.BusinessLogic;
using Matchnet.FAST.Push.ServiceManager;

using Matchnet.CacheSynchronization.Context;
using Matchnet.RemotingServices.ServiceManagers;


namespace Matchnet.FAST.Push.Service {
	public class FASTPushService : System.ServiceProcess.ServiceBase {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private const Int32 CACHE_SYNC_PORT = 48879;

		private Thread [] _Threads = new Thread [] {null};
		private PushBL [] _Pushers = new PushBL [] {null};
		private System.Timers.Timer _ErrorRequeueTimer = new System.Timers.Timer();

		public FASTPushService() {
			InitializeComponent();
		}

		// The main entry point for the process
		static void Main() {
			// Add a 20 second sleep time so that we can attach for debugging.
			Thread.Sleep(20000);

			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new FASTPushService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			components = new System.ComponentModel.Container();
			this.ServiceName = SMConstants.SERVICE_NAME; //"FASTPush";
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		protected override void OnStart(string[] args) {
			Metrics.Instance.ResetCounters();

			try 
			{
				//setup cache synchronization stuff
				Evaluator.Instance.SetRemotingPort(CACHE_SYNC_PORT);
				ChannelServices.RegisterChannel(new TcpServerChannel("", CACHE_SYNC_PORT));
				CacheManagerSM cacheManager = new CacheManagerSM(this.ServiceName);
				System.Runtime.Remoting.RemotingServices.Marshal(cacheManager, cacheManager.GetType().Name + ".rem");

				QueueConfiguration QConf = new QueueConfiguration();
				int ThreadCount = 0;
				int idx = 0;
				foreach (int n in QConf.ThreadsPerQueue) {
					ThreadCount += n;
				}
				
				_Threads = new Thread [ThreadCount];
				_Pushers = new PushBL [ThreadCount];
					
				int RunningCounter = 0;
				for (int n = 0; n < ThreadCount; n++){
					int CommunityID = QConf.CommunityList[idx];
					int MaxThreads = QConf.ThreadsPerQueue[idx];
					
					_Pushers[n] = new PushBL(QConf.GetQueue(CommunityID),CommunityID);
					ThreadStart ts = new ThreadStart(_Pushers[n].Run);
					_Threads[n] = new Thread(ts);
					_Threads[n].Name = "Thread " + n.ToString();
					

					if (++RunningCounter ==  MaxThreads){
						idx++;
						RunningCounter = 0;
					}
				}

				for ( int i = 0; i < _Threads.Length; i++){
					_Threads[i].Start();
				}

				_ErrorRequeueTimer = new System.Timers.Timer();
				_ErrorRequeueTimer.Elapsed +=new ElapsedEventHandler(_ErrorRequeueTimer_Elapsed);
				_ErrorRequeueTimer.Interval = 30 *		(60 * 1000D); // min * (sec * millisec) = 30 min
				_ErrorRequeueTimer.AutoReset = true;
				_ErrorRequeueTimer.Enabled = true;
		
			}
			catch (Exception ex){
				Metrics.Instance.WriteError("Failed OnStart service start: " + ex.ToString());
				throw new Exception("Failed OnStart service call\n\n" + ex.ToString());
			}
		}
 

		protected override void OnStop() {
			_ErrorRequeueTimer.Enabled = false;

			foreach(PushBL pusher in _Pushers){
				pusher.Stop();
			}

			foreach (Thread t in _Threads){
				try {
					if (t.IsAlive){
						t.Join(15);
					}
					if (t.IsAlive){
						t.Abort();
					}
				}
				catch (Exception ex){
					Metrics.Instance.WriteError("Exception while shutting down thread: " + ex.ToString());
				}
			}
			
		}

		private void _ErrorRequeueTimer_Elapsed(object sender, ElapsedEventArgs e)
		{
			QueueUtility.ReQueueMembersFromErrorQueue(240 * 3); // about 3 failed batches worth
		}
	}
}
