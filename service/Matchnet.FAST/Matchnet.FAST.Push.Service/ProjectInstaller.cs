using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

using Matchnet.FAST.Push.ServiceManager;
using Matchnet.FAST.Push.BusinessLogic ;

namespace Matchnet.FAST.Push.Service
{
	/// <summary>
	/// Summary description for ProjectInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
		private System.ServiceProcess.ServiceInstaller serviceInstaller1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ProjectInstaller()
		{
			// This call is required by the Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnAfterInstall(IDictionary savedState) {

			MetricsInstaller.Install();
			QueueUtility.InstallQueues();
			base.OnAfterInstall (savedState);
		}

		protected override void OnAfterUninstall(IDictionary savedState) {
			MetricsInstaller.Uninstall();
			// NOTE:  The following line has been commented out because we need to uninstall the service
			// to update it and we do not necessarily want to delete the queues when uninstalling (there may
			// be queue items waiting).
			//QueueUtility.UninstallQueues();
			base.OnAfterUninstall (savedState);
		}



		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
			this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
			// 
			// serviceProcessInstaller1
			// 
			this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.User;
			this.serviceProcessInstaller1.Password = "";
			this.serviceProcessInstaller1.Username = "";
			// 
			// serviceInstaller1
			// 
			this.serviceInstaller1.ServiceName = SMConstants.SERVICE_NAME;// "FASTPush";
			this.serviceInstaller1.DisplayName = SMConstants.SERVICE_DISPLAY_NAME;
			this.serviceInstaller1.ServicesDependedOn = new string[] {
																		 "MSMQ"};
			this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller1,
																					  this.serviceInstaller1});

		}
		#endregion
	}
}
