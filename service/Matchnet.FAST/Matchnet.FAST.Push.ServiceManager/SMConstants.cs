using System;

namespace Matchnet.FAST.Push.ServiceManager
{
	/// <summary>
	/// Summary description for SMConstants.
	/// </summary>
	public class SMConstants
	{
		public const string SERVICE_NAME = "FASTPush";
		public const string SERVICE_DISPLAY_NAME = "FAST Push Service";
		public const string SERVICE_CONSTANT = "FASTPUSH_SVC";
	}
}
