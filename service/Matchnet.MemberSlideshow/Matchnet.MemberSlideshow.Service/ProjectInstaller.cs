﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace Matchnet.MemberSlideshow.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            Matchnet.MemberSlideshow.ServiceManagers.MemberSlideshowSM.PerfCounterInstall();
        }


        private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            Matchnet.MemberSlideshow.ServiceManagers.MemberSlideshowSM.PerfCounterUninstall();
        }
    }
}
