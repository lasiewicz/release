﻿#region

using System;
using System.ComponentModel;
using System.ServiceProcess;
using log4net.Config;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.InitialConfiguration;
using Matchnet.MemberSlideshow.ServiceManagers;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.RemotingServices;

#endregion

namespace Matchnet.MemberSlideshow.Service
{
    public partial class MemberSlideshowService : RemotingServiceBase
    {
        private MemberSlideshowSM _slideshowSM;

        public MemberSlideshowService()
        {
            InitializeComponent();
            XmlConfigurator.Configure();
        }

        public MemberSlideshowService(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        private static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] {new MemberSlideshowService()};
            Run(ServicesToRun);
        }

        private void InitializeComponent()
        {
            components = new Container();
            ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT,
                                                                                  InitializationSettings.MachineName);
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _slideshowSM = new MemberSlideshowSM();
                base.RegisterServiceManager(_slideshowSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                                   "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            if (_slideshowSM != null)
            {
                _slideshowSM.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}