﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Matchnet;
using Matchnet.Replication;
using Matchnet.Exceptions;
using Matchnet.MemberSlideshow.BusinessLogic;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.MemberSlideshow.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.ValueObjects;

namespace Matchnet.MemberSlideshow.ServiceManagers
{
    public class MemberSlideshowSM : MarshalByRefObject, IMemberSlideshowService, IServiceManager, IReplicationRecipient    
    {
        private Replicator _replicator = null;
        private const string SERVICE_MANAGER_NAME = "MemberSlideshowSM";

        //member perf
        private PerformanceCounter _perfCountSlideshow;
        private PerformanceCounter _perfRequestsSecSlideshow;
        private PerformanceCounter _perfHitRateSlideshow;
        private PerformanceCounter _perfHitRateSlideshow_Base;
        
        public MemberSlideshowSM()
        {
            MemberSlideshowBL.Instance.ReplicationRequested += new MemberSlideshowBL.ReplicationEventHandler(MemberSlideshowBL_ReplicationRequested);
            MemberSlideshowBL.Instance.SlideshowAdded += new MemberSlideshowBL.SlideshowAddEventHandler(MemberSlideshowBL_SlideshowAdded);
            MemberSlideshowBL.Instance.SlideshowRemoved += new MemberSlideshowBL.SlideshowRemoveEventHandler(MemberSlideshowBL_SlideshowRemoved);
            MemberSlideshowBL.Instance.SlideshowRequested += new MemberSlideshowBL.SlideshowRequestEventHandler(MemberSlideshowBL_SlideshowRequested);

            string machineName = System.Environment.MachineName;
            string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }

            string replicationURI = string.Empty;

            try
            {
                replicationURI = RuntimeSettings.GetSetting("SLIDESHOWSVC_REPLICATION_OVERRIDE");
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving replication partner URI.", ex);
            }

            if (replicationURI.Length == 0)
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
            }

            Trace.WriteLine("Replicating to: " + replicationURI);

            _replicator = new Replicator(SERVICE_MANAGER_NAME);
            _replicator.SetDestinationUri(replicationURI);
            _replicator.Start();
            initPerfCounters();
        }

        public void MemberSlideshowBL_SlideshowAdded()
        {
            _perfCountSlideshow.Increment();
        }

        public void MemberSlideshowBL_SlideshowRemoved()
        {
            if (_perfCountSlideshow.RawValue > 0)
            {
                _perfCountSlideshow.Decrement();
            }
        }

        public void MemberSlideshowBL_SlideshowRequested(bool cacheHit)
        {
            _perfRequestsSecSlideshow.Increment();
            
            if (cacheHit)
            {
                _perfHitRateSlideshow.Increment();
            }
        }

        private void initPerfCounters()
        {
            _perfCountSlideshow = new PerformanceCounter(Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME, "Slideshow Items", false);
            _perfRequestsSecSlideshow = new PerformanceCounter(Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME, "Slideshow Requests/second", false);
            _perfHitRateSlideshow = new PerformanceCounter(Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME, "Slideshow Hit Rate", false);
            _perfHitRateSlideshow_Base = new PerformanceCounter(Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME, "Slideshow Hit Rate_Base", false);
            
            resetPerfCounters();
        }

        private void resetPerfCounters()
        {
            _perfCountSlideshow.RawValue = 0;
            _perfRequestsSecSlideshow.RawValue = 0;
            _perfHitRateSlideshow.RawValue = 0;
            _perfHitRateSlideshow_Base.RawValue = 0;
        }

        public static void PerfCounterInstall()
        {
            CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
            ccdc.AddRange(new CounterCreationData[] {	
														 new CounterCreationData("Slideshow Items", "Member Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("Slideshow Requests/second", "Slideshow Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Slideshow Hit Rate", "Slideshow Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Slideshow Hit Rate_Base","Slideshow Hit Rate_Base", PerformanceCounterType.RawBase),
														 
													 });
            PerformanceCounterCategory.Create(Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
        }


        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(Matchnet.MemberSlideshow.ValueObjects.ServiceConstants.SERVICE_NAME);
        }

        public Slideshow GetSlideshow(int memberID, Brand brand)
        {
            try
            {
                return MemberSlideshowBL.Instance.GetSlideshow(memberID, brand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting slideshow entries.", ex);
            }
        }

        public Slideshow GetSlideshow(int memberID, Brand brand, SearchPreferenceCollection preferences, bool prioritizeExpiringMembers)
        {
            try
            {
                return MemberSlideshowBL.Instance.GetSlideshow(memberID, brand, preferences, prioritizeExpiringMembers);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting slideshow entries.", ex);
            }
        }

        public void UpdateSlideshowPosition(Slideshow slideshow, int newPosition)
        {
            try
            {
                MemberSlideshowBL.Instance.UpdateSildeshowPosition(slideshow, newPosition);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure updating slideshow position.", ex);
            }
        }

        public void RemoveFromCache(int memberID, int communityID)
        {
            try
            {
                MemberSlideshowBL.Instance.RemoveFromCache(memberID, communityID, true);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure removing from cache.", ex);
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void Dispose()
        {
        }

        public void PrePopulateCache()
        {
            // no implementation at this time
        }

        #region IReplicationRecipient Members
        /// <summary>
        /// 
        /// </summary>
        public void Receive(IReplicable replicableObject)
        {
            if (replicableObject != null)
            {
                MemberSlideshowBL.Instance.CacheReplicatedObject(replicableObject);
            }
        }

        // Replication event handler
        private void MemberSlideshowBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (replicableObject != null)
            {
                _replicator.Enqueue(replicableObject);
            }
        }

        #endregion
    }
}
