﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.MemberSlideshow.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingClient;
using Matchnet.Search.ValueObjects;
using log4net;

#endregion

namespace Matchnet.MemberSlideshow.ServiceAdapters
{
    public class MemberSlideshowSA : SABase, IMemberSlideshowSA
    {
        private const string SERVICE_MANAGER_NAME = "MemberSlideshowSM";
        private const int SUSPENDED_FLAG = 1;
        public static readonly MemberSlideshowSA Instance = new MemberSlideshowSA();
        private static readonly ILog Log = LogManager.GetLogger(typeof (MemberSlideshowSA));
        private readonly Cache _cache;

        private MemberSlideshowSA()
        {
            _cache = Cache.Instance;
        }

        #region IMemberSlideshowSA Members

        public Slideshow GetSlideshow(int memberID, Brand brand)
        {
            var uri = "";
            Slideshow slideshow;

            try
            {
                slideshow =
                    _cache.Get(SlideshowCacheKey.GetCacheKey(memberID, brand.Site.Community.CommunityID)) as Slideshow;

                if (slideshow == null)
                {
                    uri = getServiceManagerUri(memberID);
                    base.Checkout(uri);
                    try
                    {
                        slideshow = getService(uri).GetSlideshow(memberID, brand);
                        cacheSlideshow(slideshow);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return slideshow;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot perform GetSlideshow (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        ///   Call RemoveFromCache() when preference values change.
        /// </summary>
        /// <param name="memberID"> </param>
        /// <param name="brand"> </param>
        /// <param name="preferences"> </param>
        /// <param name="prioritizeExpiringMembers"> </param>
        /// <returns> </returns>
        public Slideshow GetSlideshow(int memberID, Brand brand, SearchPreferenceCollection preferences,
                                      bool prioritizeExpiringMembers)
        {
            var uri = "";
            Slideshow slideshow;

            try
            {
                slideshow =
                    _cache.Get(SlideshowCacheKey.GetCacheKey(memberID, brand.Site.Community.CommunityID)) as Slideshow;

                if (slideshow == null)
                {
                    uri = getServiceManagerUri(memberID);
                    base.Checkout(uri);
                    try
                    {
                        slideshow = getService(uri).GetSlideshow(memberID, brand, preferences, prioritizeExpiringMembers);
                        cacheSlideshow(slideshow);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return slideshow;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot perform GetSlideshow (uri: " + uri + ")", ex));
            }
        }

        public void RemoveFromCache(int memberID, int communityID)
        {
            var uri = "";

            try
            {
                _cache.Remove(SlideshowCacheKey.GetCacheKey(memberID, communityID));
                uri = getServiceManagerUri(memberID);
                base.Checkout(uri);
                try
                {
                    getService(uri).RemoveFromCache(memberID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot perform UpdateSildeshowPosition (uri: " + uri + ")", ex));
            }
        }

        public void UpdateSildeshowPosition(Slideshow slideshow, int newPosition)
        {
            var uri = "";

            try
            {
                slideshow.CurrentPosition = newPosition;
                cacheSlideshow(slideshow);

                uri = getServiceManagerUri(slideshow.MemberID);
                base.Checkout(uri);
                try
                {
                    getService(uri).UpdateSlideshowPosition(slideshow, newPosition);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot perform UpdateSildeshowPosition (uri: " + uri + ")", ex));
            }
        }

        public IMember GetValidMemberFromSlideshow(Slideshow slideshow, Brand brand, bool updatePosition)
        {
            var validMemberFound = false;
            var removedInvalidMember = false;
            Matchnet.Member.ServiceAdapters.Member member = null;
            int slideshowMemberID;
            var currentPosition = slideshow.CurrentPosition;
            var originalPosition = currentPosition;

            if (updatePosition)
            {
                currentPosition++;
            }

            var canLoad = !(slideshow.AtEnd) && slideshow.Entries.Count > 0 && currentPosition < slideshow.Entries.Count;

            while (canLoad && !validMemberFound)
            {
                slideshowMemberID = slideshow.Entries[currentPosition].MemberID;
                member = MemberSA.Instance.GetMember(slideshowMemberID, MemberLoadFlags.None);
                if (IsMemberEligibleForSlideshow(member, brand))
                {
                    validMemberFound = true;
                }
                else
                {
                    removedInvalidMember = true;
                    currentPosition++;
                    canLoad = currentPosition < slideshow.Entries.Count;
                }
            }

            if ((validMemberFound && updatePosition) || (removedInvalidMember))
            {
                MemberSlideshowSA.Instance.UpdateSildeshowPosition(slideshow, currentPosition);
            }

            return member;
        }

        #endregion

        public List<IMember> GetValidMembersFromSlideshow(Slideshow slideshow, Brand brand, bool updatePosition,
                                                          short batchSize)
        {
            var stopWatch = Stopwatch.StartNew();
            var members = new List<IMember>();
            for (var i = 0; i < batchSize; i++)
            {
                var member = GetValidMemberFromSlideshow(slideshow, brand, updatePosition);
                if (member != null) members.Add(member);
            }
            stopWatch.Stop();
            Log.DebugFormat("Batchsize:{0} Took:{1}", batchSize, stopWatch.Elapsed.Milliseconds);
            return members;
        }

        private bool IsMemberEligibleForSlideshow(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            if (member == null)
            {
                return false;
            }

            var isEligible = (member.GetAttributeInt(brand, "GlobalStatusMask") & SUSPENDED_FLAG) != SUSPENDED_FLAG;

            if ((member.GetAttributeInt(brand, "SelfSuspendedFlag") & SUSPENDED_FLAG) == SUSPENDED_FLAG)
            {
                isEligible = false;
            }

            if (!member.HasApprovedPhoto(brand.Site.Community.CommunityID))
            {
                isEligible = false;
            }

            return isEligible;
        }

        private string getServiceManagerUri(int memberId)
        {
            try
            {
                return getServiceManagerUri(RuntimeSettings.GetSetting("SLIDESHOWSVC_SA_HOST_OVERRIDE"), memberId);
            }
            catch
            {
            }

            return getServiceManagerUri(string.Empty, memberId);
        }

        private IMemberSlideshowService getService(string uri)
        {
            try
            {
                return (IMemberSlideshowService) Activator.GetObject(typeof (IMemberSlideshowService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri(string overrideHostName, int memberId)
        {
            try
            {
                var uri =
                    AdapterConfigurationSA.GetServicePartition(
                        ServiceConstants.SERVICE_CONSTANT,
                        PartitionMode.Calculated, memberId).ToUri(
                            SERVICE_MANAGER_NAME);

                if (overrideHostName.Length > 0)
                {
                    var uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = 10;
        }

        private void cacheSlideshow(Slideshow slideshow)
        {
            slideshow.CachePriority = CacheItemPriorityLevel.Normal;
            slideshow.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SLIDESHOWSVC_CACHE_TTL_SA"));
            _cache.Insert(slideshow);
        }
    }
}