﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberSlideshow.ValueObjects
{
    public class SlideshowCacheKey
    {
		private const string CACHE_KEY	= "~SLIDESHOW^{0}^{1}";
        private SlideshowCacheKey()
		{}

		public static string GetCacheKey(int pMemberID, int pCommunityID)
		{
            return string.Format(CACHE_KEY, pMemberID, pCommunityID);
		}

    }
}
