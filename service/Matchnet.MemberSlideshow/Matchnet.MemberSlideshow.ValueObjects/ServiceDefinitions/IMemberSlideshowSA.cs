﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.MemberSlideshow.ValueObjects.ServiceDefinitions
{
    public interface IMemberSlideshowSA
    {
        Slideshow GetSlideshow(int memberID, Brand brand);
        Slideshow GetSlideshow(int memberID, Brand brand, SearchPreferenceCollection preferences,
                               bool prioritizeExpiringMembers);

        void RemoveFromCache(int memberID, int communityID);
        void UpdateSildeshowPosition(Slideshow slideshow, int newPosition);
        IMember GetValidMemberFromSlideshow(Slideshow slideshow, Brand brand, bool updatePosition);
    }
}
