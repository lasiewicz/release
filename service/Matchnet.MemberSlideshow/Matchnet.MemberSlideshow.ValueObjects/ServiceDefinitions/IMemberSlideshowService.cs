﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.ValueObjects;

namespace Matchnet.MemberSlideshow.ValueObjects.ServiceDefinitions
{
    public interface IMemberSlideshowService
    {
        Slideshow GetSlideshow(int memberID, Brand brand);
        Slideshow GetSlideshow(int memberID, Brand brand, SearchPreferenceCollection preferences, bool prioritizeExpiringMembers);
        void UpdateSlideshowPosition(Slideshow slideshow, int newPosition);
        void RemoveFromCache(int memberID, int communityID);
    }
}
