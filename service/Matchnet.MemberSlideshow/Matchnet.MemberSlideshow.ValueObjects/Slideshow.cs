﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.MemberSlideshow.ValueObjects
{
    [Serializable]
    public class Slideshow : ICacheable, IValueObject, IReplicable
    {
        private List<SlideshowEntry> _entries;
        private int _memberID;
        private int _communityID;

        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }

        public int CurrentPosition { get; set; }

        public bool AtEnd
        {
            get
            {
                return CurrentPosition >= Entries.Count;
            }
            
        }

        public List<SlideshowEntry> Entries
        {
            get
            {
                return _entries;
            }
        }

        public int MemberID
        {
            get
            {
                return _memberID;
            }
        }

        public int CommunityID
        {
            get
            {
                return _communityID;
            }
        }

        public Slideshow(int memberID, int communityID, List<SlideshowEntry> entries)
        {
            _entries = entries;
            _memberID = memberID;
            _communityID = communityID;
            _CacheKey = SlideshowCacheKey.GetCacheKey(memberID, communityID);
        }

        #region ICacheable Members

        private int _CacheTTLSeconds;
        private CacheItemPriorityLevel _CachePriority;
        private string _CacheKey;

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Sliding;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return _CacheKey;
        }

        #endregion

        #region IReplicable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion

    }
}
