﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberSlideshow.ValueObjects
{
    [Serializable]
    public class SlideshowEntry
    {
        public int MemberID {get;set;}

        public SlideshowEntry(int memberID)
        {
            MemberID = memberID;
        }
 
    }
}
