﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberSlideshow.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "SLIDESHOW_SVC";
        public const string SERVICE_NAME = "Matchnet.MemberSlideshow.Service";

        private ServiceConstants()
        {
        }
    }
}
