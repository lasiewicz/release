﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;
using log4net;
using Matchnet.List.ValueObjects.ServiceDefinitions;
using Matchnet.MemberSlideshow.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.CacheSynchronization.Tracking;
using Matchnet.Exceptions;
using Matchnet.List.ServiceAdapters.Interfaces;
using Matchnet.Search.ValueObjects.ServiceDefinitions;

namespace Matchnet.MemberSlideshow.BusinessLogic
{
    public class MemberSlideshowBL
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MemberSlideshowBL));

        public readonly static MemberSlideshowBL Instance = new MemberSlideshowBL();

        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SlideshowAddEventHandler();
        public event SlideshowAddEventHandler SlideshowAdded;

        public delegate void SlideshowRemoveEventHandler();
        public event SlideshowRemoveEventHandler SlideshowRemoved;

        public delegate void SlideshowRequestEventHandler(bool cacheHit);
        public event SlideshowRequestEventHandler SlideshowRequested;

        private Matchnet.Caching.Cache _cache;
        private CacheItemRemovedCallback _expireSideshow;
        private string _expiredCountLock = "";
        private int _expiredCount = 0;
        private Int32 _ttl = 1200;
        private const Int32 GC_THRESHOLD = 10000;
        private int _daysForExpiringMembers;
        private IListSA _listService = null;
        private IMemberSearchSA _searchService = null;
        private ISettingsSA _settingsService = null;
        private ISearchPreferencesSA _searchPreferencesService = null;

        public IListSA ListService
        {
            get
            {
                if(_listService == null)
                {
                    return Matchnet.List.ServiceAdapters.ListSA.Instance;
                }
                else
                {
                    return _listService;
                }
            }
            set { _listService = value; }
        }

        public IMemberSearchSA SearchService
        {
            get
            {
                if (_listService == null)
                {
                    return Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance;
                }
                else
                {
                    return _searchService;
                }
            }
            set { _searchService = value; }
        }

        public ISettingsSA SettingsService
        {
            get
            {
                if (_listService == null)
                {
                    return RuntimeSettings.Instance;
                }
                else
                {
                    return _settingsService;
                }
            }
            set { _settingsService = value; }
        }

        public ISearchPreferencesSA SearchPreferencesService
        {
            get
            {
                if(_searchPreferencesService == null)
                {
                    _searchPreferencesService = (ISearchPreferencesSA)Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance;
                }

                return _searchPreferencesService;

            }
            set { _searchPreferencesService = value; }
        }

        private MemberSlideshowBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
            _expireSideshow = new CacheItemRemovedCallback(this.ExpireSlideshowCallback);
            _ttl = Convert.ToInt32(SettingsService.GetSettingFromSingleton("SLIDESHOWSVC_CACHE_TTL_SVC"));
            _daysForExpiringMembers = Convert.ToInt32(SettingsService.GetSettingFromSingleton("SLIDESHOW_DAYS_FOR_EXPIRING_MEMBERS"));
        }

        private void IncrementExpirationCounter()
        {
            lock (_expiredCountLock)
            {
                _expiredCount++;

                if (_expiredCount > GC_THRESHOLD)
                {
                    _expiredCount = 0;
                    System.Diagnostics.Trace.WriteLine("___GC");
                    GC.Collect();
                }
            }
        }

        private void ExpireSlideshowCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            CachedSlideshow cachedSlideshow = value as CachedSlideshow;
            IncrementExpirationCounter();
            if(SlideshowRemoved != null)
                SlideshowRemoved();
        }

        /// <summary>
        /// Accepts an IReplicable object and updates the local service cache.
        /// </summary>
        /// <param name="pReplicableObject"></param>
        public void CacheReplicatedObject(IReplicable replicableObject)
        {
            switch (replicableObject.GetType().Name)
            {
                case "Slideshow":
                    Slideshow slideshow = (Slideshow)replicableObject;
                    if (slideshow.Entries != null)
                    {
                        CacheSlideshow((Slideshow)replicableObject);
                    }
                    else
                    {
                        RemoveFromCache(slideshow.MemberID, slideshow.CommunityID, false);
                    }
                    break;
                default:
                    throw new BLException("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
            }
        }

        private void CacheSlideshow(Slideshow slideshow)
        {
            slideshow.CachePriority = CacheItemPriorityLevel.Normal;
            slideshow.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SLIDESHOWSVC_CACHE_TTL_SVC"));
            _cache.Insert(slideshow, _expireSideshow);
            if (SlideshowAdded != null)
                SlideshowAdded();
        }

        public void RemoveFromCache(int memberID, int communityID, bool replicate)
        {
            try
            {
                _cache.Remove(SlideshowCacheKey.GetCacheKey(memberID, communityID));
                if (replicate)
                {
                    Slideshow forreplication = new Slideshow(memberID, communityID, null);
                    ReplicationRequested(forreplication);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("RemoveFromCache() error (memberID: " + memberID.ToString() + " communityID: " + communityID.ToString() + ").", ex));
            }
        }

        private void CacheSlideshow(List<SlideshowEntry> entries, int memberID, int communityID)
        {
            Slideshow cachedslideShow = new Slideshow(memberID, communityID, entries);
            CacheSlideshow(cachedslideShow);
        }

        public Slideshow GetSlideshow(int memberID, Brand brand)
        {
            
            try
            {
                Slideshow slideshow = _cache.Get(SlideshowCacheKey.GetCacheKey(memberID, brand.Site.Community.CommunityID)) as Slideshow;

                if (slideshow != null)
                {
                    SlideshowRequested(true);
                    return slideshow;
                }
                else
                { 
                    int count;

                    List<int> slideshowMembers = new List<int>();
                    IHotList list = (IHotList)ListService.GetList(memberID); 

                  
                    //get search results
                    SearchPreferenceCollection memberPreferences = SearchPreferencesService.GetSearchPreferences(memberID, brand.Site.Community.CommunityID);
                    SearchPreferenceCollection laxPreferences = GetLaxSearchPreference(memberPreferences, brand);

                    laxPreferences["SearchType"] = ((int)Matchnet.Search.Interfaces.SearchType.SecretAdmirerWebSearch).ToString();
                    laxPreferences["MemberId"] = memberID.ToString();
                    
                    ArrayList mems = SearchService.SearchDetailed(laxPreferences, brand.Site.Community.CommunityID, brand.Site.SiteID);

                    IEnumerable<int> searchMembers = (from DetailedQueryResult result in mems select result.MemberID);

                    List<int> myYeses = GetMemberIdsOfWhoClickedYesToMe(searchMembers, list, brand);

                    //get expiring members
                    List<int> expiringMembers = getExpiringMembers(mems);
                    slideshowMembers.AddRange(expiringMembers.AsEnumerable());
                    slideshowMembers.AddRange(searchMembers.Except(slideshowMembers));
                    slideshowMembers = slideshowMembers.Except(GetYesNoMembers(list, brand.Site.Community.CommunityID, brand.Site.SiteID)).ToList();

                   
                    if (myYeses.Count > 0)
                            UpdatedListforYess(slideshowMembers, myYeses);

                    
                    List<SlideshowEntry> entrylist = memberIDListToSlideshowEntryList(slideshowMembers);
                    slideshow = new Slideshow(memberID, brand.Site.Community.CommunityID, entrylist);
                    CacheSlideshow(slideshow);
                    ReplicationRequested(slideshow);
                    SlideshowRequested(false);
                }

                return slideshow;
            }
            catch (Exception ex)
            {
                throw (new BLException("GetSlideshow() error (memberID: " + memberID.ToString() + ").", ex));
            }
        }

        public Slideshow GetSlideshow(int memberID, Brand brand, SearchPreferenceCollection preferences, bool prioritizeExpiringMembers)
        {
            Log.Debug("GetSlideshow  memberID slide show with pref " + memberID);
            Slideshow slideshow = _cache.Get(SlideshowCacheKey.GetCacheKey(memberID, brand.Site.Community.CommunityID)) as Slideshow;
            
            try
            {
                if (slideshow != null)
                {
                    SlideshowRequested(true);
                    return slideshow;
                }
                else
                {
                    
                    List<int> slideshowMembers = new List<int>();
                    IHotList list = (IHotList)ListService.GetList(memberID);
                    
                   
                    preferences["SearchType"] = ((int)Matchnet.Search.Interfaces.SearchType.SecretAdmirerWebSearch).ToString();
                    preferences["MemberId"] = memberID.ToString();
                    var preferencesText = new StringBuilder(preferences.Count);
                    foreach (DictionaryEntry preference  in preferences)
                    {
                        preferencesText.Append(String.Format("Name:{0} Value:{1}", preference.Key, preference.Value));
                    }
                    Log.DebugFormat("Preferences {0}", preferencesText);

                    ArrayList mems = SearchService.SearchDetailed(preferences, brand.Site.Community.CommunityID, brand.Site.SiteID);

                    Log.Debug("total # of members returned: " + mems.Count + " /n/n");

                    IEnumerable<int> searchMembers = (from DetailedQueryResult result in mems select result.MemberID);
                    List<int> myYeses = GetMemberIdsOfWhoClickedYesToMe(searchMembers, list, brand);


                    if (prioritizeExpiringMembers)
                    {
                        //get expiring members
                        List<int> expiringMembers = getExpiringMembers(mems);
                        slideshowMembers.AddRange(expiringMembers.AsEnumerable());
                    }
                    slideshowMembers.AddRange(searchMembers.Except(slideshowMembers));
                    slideshowMembers = slideshowMembers.Except(GetYesNoMembers(list,brand.Site.Community.CommunityID,brand.Site.SiteID)).ToList();

                    if (myYeses.Count > 0)
                        UpdatedListforYess(slideshowMembers, myYeses);

                    List<SlideshowEntry> entrylist = memberIDListToSlideshowEntryList(slideshowMembers);
                    slideshow = new Slideshow(memberID, brand.Site.Community.CommunityID, entrylist);
                    CacheSlideshow(slideshow);
                    ReplicationRequested(slideshow);
                    SlideshowRequested(false);
                }
            }
            catch (Exception ex)
            {
                Log.DebugFormat("ERROR: " + ex.Message + "    " + ex.StackTrace + " " + ex.InnerException);
                throw (new BLException("GetSlideshow() error (memberID: " + memberID.ToString() + ").", ex));
            }

            return slideshow;
        }

        /// <summary>
        /// returns a union of all the members that the user has already clicked yes or no for. 
        /// </summary>
        /// <param name="list"></param>
        /// <param name="comunityID"></param>
        /// <param name="siteID"></param>
        /// <returns></returns>
        private IEnumerable<int> GetYesNoMembers(IHotList list, int comunityID, int siteID)
        {
            int count;
            //get yes members
            List<int> yes = convertToGeneric(list.GetListMembers(HotListCategory.MembersClickedYes, comunityID, siteID, 1, 1000, out count));
            //get no members
            List<int> no = convertToGeneric(list.GetListMembers(HotListCategory.MembersClickedNo, comunityID, siteID, 1, 1000, out count));

            IEnumerable<int> yesno = yes.Union(no.AsEnumerable());

            return yesno;
        }


        private void UpdatedListforYess(List<int> slideshowMembers, List<int> myYess)  
        { 
            
            //first remove the yess people from the list 
            slideshowMembers = slideshowMembers.Except(myYess).ToList();

            if (myYess.Count > 3 && slideshowMembers.Count > 20)
            {
                
                var slideshowMembersMini = slideshowMembers.Take(20).ToList();
                slideshowMembers = slideshowMembers.Except(slideshowMembersMini).ToList(); // remove the mini list from the slide show member list.     
                    
                myYess.AddRange(slideshowMembersMini); // appened the smaller list to the myYes list.  
                Log.DebugFormat("slideshowMembersMini: " + slideshowMembersMini.Count);
                Log.DebugFormat("new myyest count with slide show: " + myYess.Count);

                myYess = myYess.OrderBy(a => Guid.NewGuid()).ToList(); // Shuffle(myYess).ToList();// randomize the new list. 

                
                slideshowMembers.InsertRange(0, myYess); // add the randomised list to the top of the original list. 
            }
            else
            {
                // add them to the top. 
                slideshowMembers.InsertRange(0, myYess);
            }

             
        }


        /// <summary>
        /// returns memberID's of members who have said clicked "Yes" for the user in the secrest admirer game. 
        /// </summary>
        /// <param name="searchMembers"></param>
        /// <param name="list"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        private List<int> GetMemberIdsOfWhoClickedYesToMe(IEnumerable<int> searchMembers, IHotList list, Brand brand)
        {

            // create a new list of peeps that are YES for the Member. 
            var peopleWhoYesedMe = new List<int>();

            Log.Debug("GetMemberIdsOfWhoClickedYesToMe:start");

            if (list != null)
            {
                foreach (var searchMember in searchMembers)
                {
                    ClickMask clickMask = list.GetClickMask(brand.Site.Community.CommunityID, brand.Site.SiteID,
                                                            searchMember);
                    if ((clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
                    {
                        Log.Debug("peopleWhoYesedMe.Add " + searchMember);
                        peopleWhoYesedMe.Add(searchMember);
                    }

                }    
            }

            if (peopleWhoYesedMe.Count > 2)
            {
                Log.Debug("GetMemberIdsOfWhoClickedYesToMe:peopleWhoYesedMe.Count > 2");
                return peopleWhoYesedMe.OrderBy(a => Guid.NewGuid()).ToList();
            }
            else
            {
                Log.Debug("GetMemberIdsOfWhoClickedYesToMe:peopleWhoYesedMe.Count < 2");
                return peopleWhoYesedMe;    
            }
        }


        public void UpdateSildeshowPosition(Slideshow slideshow, int newPosition)
        {
            try
            {
                slideshow.CurrentPosition = newPosition;
                CacheSlideshow(slideshow);
                ReplicationRequested(slideshow);
            }
            catch (Exception ex)
            {
                throw (new BLException("UpdateSildeshowPosition() error.", ex));
            }
        }

        private SearchPreferenceCollection GetLaxSearchPreference(SearchPreferenceCollection strictPreferences, Brand brand)
        {
            SearchPreferenceCollection laxPrefs = new SearchPreferenceCollection();

            laxPrefs.Add("GenderMask", strictPreferences.Value("GenderMask"));
            laxPrefs.Add("RegionID", strictPreferences.Value("RegionID"));
            laxPrefs.Add("DomainID", brand.Site.Community.CommunityID.ToString());
            laxPrefs.Add("SearchTypeID", SearchTypeID.Region.ToString("d")); //Region
            laxPrefs.Add("SearchOrderBy", QuerySorting.Proximity.ToString("d")); 
            laxPrefs.Add("Radius", "160"); 
            laxPrefs.Add("HasPhotoFlag", "1");
            laxPrefs.Add("MinAge", strictPreferences.Value("MinAge"));
            laxPrefs.Add("MaxAge", strictPreferences.Value("MaxAge"));

            return laxPrefs;
        }


        private List<int> getExpiringMembers(ArrayList mems)
        {
            List<DetailedQueryResult> e = (from DetailedQueryResult r in mems where r.Tags.ContainsKey("subscriptionexpirationdate") select r).ToList();
            List<DetailedQueryResult> i = (from r in e where Convert.ToDateTime(r.Tags["subscriptionexpirationdate"]) > DateTime.Now.AddDays(-_daysForExpiringMembers) select r).ToList();

            i.Sort(delegate(DetailedQueryResult mem1, DetailedQueryResult mem2)
            {
                return Convert.ToDateTime(mem1.Tags["subscriptionexpirationdate"]).CompareTo(Convert.ToDateTime(mem2.Tags["subscriptionexpirationdate"]));
            });

            return (from DetailedQueryResult r in i select r.MemberID).ToList();
        }

        private List<int> convertToGeneric(ArrayList list)
        {
            List<int> converted = new List<int>();
            foreach (int i in list)
            {
                converted.Add(i);
            }

            return converted;
        }

        private List<SlideshowEntry> memberIDListToSlideshowEntryList(List<int> memberIDList)
        {
            List<SlideshowEntry> entries = new List<SlideshowEntry>();
            foreach (int i in memberIDList)
            {
                entries.Add(new SlideshowEntry(i));
            }

            return entries;
        }


        public static IList<T> Shuffle<T>(IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

    }
}
