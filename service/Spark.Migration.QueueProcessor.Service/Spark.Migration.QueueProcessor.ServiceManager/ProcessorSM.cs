﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet;
using Spark.Migration.QueueProcessor.BusinessLogic;

namespace Spark.Migration.QueueProcessor.ServiceManager
{
    public class ProcessorSM : MarshalByRefObject, IServiceManager, IBackgroundProcessor
    {
        public ProcessorSM()
        {

        }

        public void PrePopulateCache()
        {

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public void Start()
        {
            ProcessorBL.Instance.Start();
        }

        public void Stop()
        {
            ProcessorBL.Instance.Stop();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
