﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.Migration.QueueProcessor.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "MIGRATIONPROCESSOR_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Spark.Migration.Processor.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "ProcessorSM";
    }
}
