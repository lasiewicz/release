﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data;
using Spark.Managers.Managers;
using Spark.RabbitMQ.Client;
using Spark.MingleAPIAdapter.ExternalModels;
using Spark.Logging;
using MVO=Spark.Migration.QueueProcessor.ValueObjects;

namespace Spark.Migration.QueueProcessor.BusinessLogic
{
    public class ProcessorBL
    {
        public readonly static ProcessorBL Instance = new ProcessorBL();
        private Subscriber _subscriber;
        private string _rabbitMQUserName = string.Empty;
        private string _rabbitMQPassword = string.Empty;
        private string _rabbitMQServer = string.Empty;
        private int _prefetchCount = 0;

        private ProcessorBL()
        {
            
        }

        public void Start()
        {
            RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", "Started", null);
            GetSettings();
            _subscriber = new Subscriber(_rabbitMQServer, _rabbitMQUserName, _rabbitMQPassword, _prefetchCount);
            _subscriber.Subscribe<ExternalMigrationPhotoApprovalRequest>("ExternalMigrationPhotoApprovalRequest", DoPhotoapprovalCallback);
            _subscriber.Subscribe<ExternalMinglePasswordSynchRequest>("ExternalMinglePasswordSynchRequest", DoMinglePasswordSynch);
            RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", "Subscribed: " + _rabbitMQServer, null);
        }

        public void Stop()
        {
            if (_subscriber != null)
            {
                RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", "Stopped", null);
                _subscriber.Dispose();
            }
        }

        private void GetSettings()
        {
            var settingsManager = new SettingsManager();
            _rabbitMQUserName = settingsManager.GetSettingString(SettingConstants.RABBITMQ_USER_ACCOUNT, null);
            _rabbitMQPassword = settingsManager.GetSettingString(SettingConstants.RABBITMQ_PASSWORD, null);
            _rabbitMQServer = settingsManager.GetSettingString(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER, null);
            _prefetchCount = settingsManager.GetSettingInt(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_PREFETCH, null);
        }

        /// <summary>
        ///     Items are generated by Matchnet.PhotoApprove.Service
        /// </summary>
        /// <param name="approvalRequest"></param>
        private void DoPhotoapprovalCallback(ExternalMigrationPhotoApprovalRequest approvalRequest)
        {
          var success = ProcessPhotoApprovalCallback(approvalRequest);

          if (!success)
          {
              throw new Exception(
                  string.Format(
                      "Unsucessful DoPhotoApprovalCalback response from MingleAPI. MemberID: {0} MemberPhotoID: {1}",
                      approvalRequest.MemberId, approvalRequest.MemberPhotoId));
          }
        }

        public bool ProcessPhotoApprovalCallback(ExternalMigrationPhotoApprovalRequest approvalRequest)
        {
            try
            {
                RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL",
                    "Entered DoPhotoApprovalCalback " + approvalRequest, null);
                var mingleApiAdapter = new MingleAPIAdapter.Adapter();
                var brand = BrandConfigSA.Instance.GetBrandByID(approvalRequest.BrandId);
                var success = mingleApiAdapter.ApprovePhoto(brand, approvalRequest);

                RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL",
                    string.Format("DoPhotoApprovalCalback Result: {0} MemberID: {1} MemberPhotoID: {2}", success,
                        approvalRequest.MemberId, approvalRequest.MemberPhotoId), null);

                return success;
            }
            catch (Exception exception)
            {
                RollingFileLogger.Instance.LogException(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", exception,
                    string.Format("MemberID: {0} MemberPhotoID: {1}", approvalRequest.MemberId,
                        approvalRequest.MemberPhotoId));
                throw;
            }
        }

        private void DoMinglePasswordSynch(ExternalMinglePasswordSynchRequest synchRequest)
        {
            try
            {
                RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", "Entered DoMinglePasswordSynch " + synchRequest.ToString(), null);
                var mingleAPIAdapter = new MingleAPIAdapter.Adapter();
                Brand brand = BrandConfigSA.Instance.GetBrandByID(synchRequest.BrandId);
                var success = mingleAPIAdapter.MingleSynchPassword(synchRequest.MemberId,
                    brand,
                    synchRequest.EmailAddress,
                    synchRequest.P1,
                    synchRequest.P2);

                RollingFileLogger.Instance.LogInfoMessage(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", string.Format("DoMinglePasswordSynch Result: {0} MemberID: {1} EmailAddress: {2}", success, synchRequest.MemberId, synchRequest.EmailAddress), null);

                if(!success)
                {
                    throw new Exception(string.Format("Unsucessful DoMinglePasswordSynch response from MingleAPI. MemberID: {0} EmailAddress: {1}", synchRequest.MemberId, synchRequest.EmailAddress));
                }
            }
            catch(Exception ex)
            {
                RollingFileLogger.Instance.LogException(MVO.ServiceConstants.SERVICE_CONSTANT, "ProcessorBL", ex, string.Format("MemberID: {0} EmailAddress: {1}", synchRequest.MemberId, synchRequest.EmailAddress));
                throw;
            }
        }
    }
}
