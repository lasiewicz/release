﻿#region

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.Migration.QueueProcessor.BusinessLogic;
using Spark.MingleAPIAdapter.ExternalModels;

#endregion

namespace Spark.Migration.QueueProcessor.Test
{
    /// <summary>
    ///     Summary description for PhotoApprovalTest
    /// </summary>
    [TestClass]
    public class PhotoApprovalTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void DoPhotoapprovalCallbackTest()
        {
            // Setup
            var request = new ExternalMigrationPhotoApprovalRequest
            {
                BrandId = 90710,
                CloudFullPath = "mm2-dev-catholic/961f-2015/07/17/16/222008810.jpg",
                CloudPreviewPath = "mm2-dev-catholic/961f-2015/07/17/16/222008810.jpg",
                CloudThumbPath = "mm2-dev-catholic/961f-2015/07/17/16/222008810.jpg",
                LaFullPath = "/Photo9000/2015/07/17/16/222008810.jpg",
                LaPreviewPath = "/Photo9000/2015/07/17/16/222008810.jpg",
                LaThumbPath = "/Photo9000/2015/07/17/16/222008810.jpg",
                MemberId = 1127800166,
                MemberPhotoId = 17002596,
                NineSixtyPath = "mm2-dev-catholic/961f-2015/07/17/16/222008810.jpg",
                OriginalPath = "mm2-dev-catholic/961f-2015/07/17/16/222008810.jpg",
                AdminName = "gmena",
                ApprovedForMain = true,
                FullHeight = 100,
                FullWidth = 90,
                PreviewHeight = 80,
                PreviewWidth = 70,
                ThumbHeight = 60,
                ThumbWidth = 50
            };

            // Execute
            var success = ProcessorBL.Instance.ProcessPhotoApprovalCallback(request);

            // Assert
            Assert.IsTrue(success);
        }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion
    }
}