#region

using System.ComponentModel;
using System.Configuration.Install;
using Matchnet.PhotoApprove.BusinessLogic;

#endregion

namespace Matchnet.PhotoApprove.Service
{
    [RunInstaller(true)]
    public class Installer : System.Configuration.Install.Installer
    {
        /// <summary>
        ///     Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.Container components = null;

        private System.ServiceProcess.ServiceInstaller serviceInstaller;
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;

        public Installer()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void Installer_AfterInstall(object sender, InstallEventArgs e)
        {
            Metrics.MetricsInstaller.Install();
        }

        private void Installer_AfterUninstall(object sender, InstallEventArgs e)
        {
            Metrics.MetricsInstaller.Uninstall();
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // serviceProcessInstaller
            // 
            this.serviceProcessInstaller.Password = null;
            this.serviceProcessInstaller.Username = null;
            this.serviceInstaller.ServiceName = "Matchnet.PhotoApprove.Service";
            this.serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.serviceInstaller.ServicesDependedOn = new string[] {"MSMQ"};
            // 
            // Installer
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[]
            {
                this.serviceInstaller,
                this.serviceProcessInstaller
            });
            this.AfterInstall += new InstallEventHandler(Installer_AfterInstall);
            this.AfterUninstall += new InstallEventHandler(Installer_AfterUninstall);
        }

        #endregion
    }
}