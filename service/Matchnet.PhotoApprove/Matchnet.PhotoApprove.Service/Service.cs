#region

using System.Threading;
using Matchnet.PhotoApprove.BusinessLogic;

#endregion

namespace Matchnet.PhotoApprove.Service
{
    public class Service : System.ServiceProcess.ServiceBase
    {
        private Thread _runningThread;
        private System.ComponentModel.Container components;

        public Service()
        {
            InitializeComponent();
        }

        private static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service()};
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "Matchnet.PhotoApprove.Service";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        protected override void OnStart(string[] args)
        {
            _runningThread = new Thread(PhotoApproveBL.Instance.Run);
            _runningThread.Start();
        }

        protected override void OnStop()
        {
            if (_runningThread.ThreadState == System.Threading.ThreadState.Running)
            {
                _runningThread.Abort();
            }

            _runningThread = null;
        }
    }
}