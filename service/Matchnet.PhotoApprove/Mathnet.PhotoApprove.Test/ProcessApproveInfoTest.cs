﻿using System;
using Matchnet;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.PhotoApprove.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mathnet.PhotoApprove.Test
{
    [TestClass]
    public class ProcessApproveInfoTest
    {
        [TestMethod]
        public void Dequeue()
        {
            var approveInfoCollection = PhotoApproveBL.Instance.GetNextApproveInfoCollection();
            PhotoApproveBL.Instance.ProcessApproveInfo(approveInfoCollection[0]);
        }

        [TestMethod]
        public void MinglePhotoProcessApproveInfoTest()
        {
            // Setup
            var approveInfo = new ApproveInfo(
                memberID:1127796968,
                communityID:30, // CatholicMingle Community
                memberPhotoID:0,
                fileID:0,
                cropWidth:100,
                cropHeight:100,
                cropX:0,
                cropY:0,
                cropHeightThumb:50,
                cropWidthThumb:50,
                cropXThumb:20,
                cropYThumb:20,
                approveStatus:ApproveStatusType.Approved, 
                captionDelete: false,
                siteID:9071,
                captionOnly:false,
                fileIDThumb:Constants.NULL_INT,
                rotateAngle:0,
                rotateAngleThumb:0,
                adminMemberID:27029711
                );

            // Execute
            PhotoApproveBL.Instance.ProcessApproveInfo(approveInfo);

            // Assert
        }
    }
}
