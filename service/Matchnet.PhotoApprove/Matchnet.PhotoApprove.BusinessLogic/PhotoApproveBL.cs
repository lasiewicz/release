#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net.Mail;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Threading;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.CacheSynchronization.Context;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.File.ServiceAdapters;
using Matchnet.File.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.RemotingServices.ServiceManagers;
using Spark.Logging;
using Spark.PushNotifications.Processor.ServiceAdapter;
using Spark.RabbitMQ.Client;
using DBQ = Matchnet.ApproveQueue.ValueObjects.DBQueue;

#endregion

namespace Matchnet.PhotoApprove.BusinessLogic
{
    /// <summary>
    ///     Processes photo queue items sent from admin photo approval front end application
    /// </summary>
// ReSharper disable InconsistentNaming
    public class PhotoApproveBL
// ReSharper restore InconsistentNaming
    {
        #region Public Variables

        public static readonly PhotoApproveBL Instance = new PhotoApproveBL();

        #endregion

        #region Private Variables

        private const string ServiceConstant = "PHOTOAPPROVAL_SVC";
        private static int _sleepTime; // sleep between batches in seconds
        private static bool _isRunnable;

        // rabbitMq 
        private readonly Publisher _publisher;

        private readonly string _rabbitMqServer = RuntimeSettings.Instance.GetSettingFromSingleton(
            SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER);

        private readonly string _rabbitMqUserName =
            RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_USER_ACCOUNT);

        private readonly string _rabbitMqPassword =
            RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_PASSWORD); 

        #endregion

        #region Constructors

        private PhotoApproveBL()
        {
            //setup cache synchronization stuff
            const Int32 cacheSyncPort = 48880;
            Evaluator.Instance.SetRemotingPort(cacheSyncPort);
// ReSharper disable CSharpWarnings::CS0618
            ChannelServices.RegisterChannel(new TcpServerChannel("", cacheSyncPort));
// ReSharper restore CSharpWarnings::CS0618
            var cacheManager = new CacheManagerSM("Matchnet.PhotoApprove.Service");
            System.Runtime.Remoting.RemotingServices.Marshal(cacheManager, cacheManager.GetType().Name + ".rem");

            _isRunnable = true;
            _sleepTime = 1;

            _publisher = new Publisher(_rabbitMqServer, _rabbitMqUserName, _rabbitMqPassword);
        }

        #endregion

        #region Public Methods

        public void Stop()
        {
            _isRunnable = false;
        }

        public void Run()
        {
            while (_isRunnable)
            {
                try
                {
                    if (ProcessApproveInfoCollection())
                    {
                        _sleepTime = 1;
                    }
                    else
                    {
                        // 5 second maximum wait. CS is going to check for results frequently 
                        // and I wouldn't want them waiting for minutes.
                        if (_sleepTime > 5)
                        {
                            _sleepTime = 1;
                        }
                        else
                        {
                            _sleepTime++;
                        }

                        Thread.Sleep(_sleepTime*1000);
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    Metrics.Instance.WriteError(ex.ToString());
                }
            }
        }

        #endregion

        #region Private Methods

        private bool ProcessApproveInfoCollection()
        {
            ApproveInfoCollection approveInfoCollection;
            var start = DateTime.Now.Ticks;

            try
            {
                approveInfoCollection = GetNextApproveInfoCollection();
            }
            catch (Exception ex)
            {
                throw new BLException("Error receiving batch", ex);
            }
            finally
            {
                Metrics.BatchesTotal.Increment();
            }

            if (approveInfoCollection == null)
            {
                return false;
            }

            foreach (ApproveInfo approveInfo in approveInfoCollection)
            {
                try
                {
                    ProcessApproveInfo(approveInfo);
                    Metrics.SuccessTotal.Increment();
                }
                catch (Exception ex)
                {
                    Metrics.ErrorsTotal.Increment();
                    Metrics.Instance.WriteError(ex.ToString());
                }
            }

            Metrics.AvgApprovalSecs.IncrementBy(new TimeSpan(DateTime.Now.Ticks - start).Seconds);
            Metrics.AvgApprovalSecsBase.Increment();

            return true;
        }

        public ApproveInfoCollection GetNextApproveInfoCollection()
        {
            var timeout = new TimeSpan(0, 0, 0, 0, 10);

            var queue = new MessageQueue(PhotoUtil.MsmqPath) {Formatter = new BinaryMessageFormatter()};
            Message message;

            try
            {
                message = queue.Receive(timeout);
            }
            catch (MessageQueueException mqEx)
            {
                if (!mqEx.Message.Equals("Timeout for the requested operation has expired."))
                {
                    throw;
                }

                return null;
            }

            if (message == null)
            {
                return null;
            }

            var approveInfoCollection = message.Body as ApproveInfoCollection;

            return approveInfoCollection;
        }

/*
        /// <summary>
        /// </summary>
        /// <param name="fromFilePath"></param>
        /// <returns>Returns copied to file path</returns>
        private string MakeBackupCopy(string fromFilePath, bool forceOverWrite)
        {
            try
            {
                var backupServer = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOBACKUPSERVER");

                //dev use
                if (backupServer.Length == 0)
                {
                    return fromFilePath;
                }

                if (!System.IO.File.Exists(fromFilePath))
                {
                    fromFilePath = fromFilePath.Replace(@".jpg", @".bmp");

                    if (!System.IO.File.Exists(fromFilePath))
                    {
                        throw new BLException("Error. File does not exist. [FilePath:" + fromFilePath + "]");
                    }
                }

                var backUpServer = @"\\" + backupServer;

                var indexServerPathEnd = fromFilePath.IndexOf(@"\", 3);
                    // \\FILE05\FileRootC\2005\05\24\12\blah.jpg -> get the third one

                var localFilePath = fromFilePath.Substring(indexServerPathEnd);
                    // \FileRootC\2005\05\24\12\blah.jpg -> got this path

                var toFilePath = backUpServer + localFilePath;
                    // \\64.16.66.151\FileRootC\2005\05\24\12\blah.jpg -> build this path

                if (forceOverWrite == false)
                {
                    if (System.IO.File.Exists(toFilePath))
                    {
                        return toFilePath;
                    }
                }

                try
                {
                    var fileInfo = new FileInfo(toFilePath);
                    Directory.CreateDirectory(fileInfo.Directory.FullName);
                    System.IO.File.Copy(fromFilePath, toFilePath);
                    System.Diagnostics.Trace.WriteLine("FA:Copied back up From:" + fromFilePath + " To:" + toFilePath);
                }
                catch (Exception ex)
                {
                    throw new BLException("Error copying file. [From:" + fromFilePath +
                                          ", To:" + toFilePath + "index:" + indexServerPathEnd + "]", ex);
                }

                return toFilePath;
            }
            catch (Exception ex)
            {
                throw new BLException("Error making backup copy. [FromFilePath:" + fromFilePath + "]", ex);
            }
        }
*/

/*
        private void validateApproveInfo(ApproveInfo approveInfo)
        {
            if (!(isValidIntValue(approveInfo.MemberID) &&
                  isValidIntValue(approveInfo.MemberPhotoID) &&
                  isValidIntValue(approveInfo.FileID) &&
                  isValidIntValue(approveInfo.CommunityID) &&
                  isValidIntValue(approveInfo.AdminMemberID)))
            {
                throw new BLException("Invalid approve info member/file data. [MemberID:" + approveInfo.MemberID +
                                      ", MemberPhotoID:" + approveInfo.MemberPhotoID + ", FileID:" + approveInfo.FileID);
            }

            if (approveInfo.CropHeight == 0 ||
                approveInfo.CropWidth == 0 ||
                approveInfo.CropHeightThumb == 0 ||
                approveInfo.CropWidthThumb == 0)
            {
                throw new BLException("Crop height/width value cannot be zero. [MemberID:" + approveInfo.MemberID +
                                      ", AdminMemberID:" + approveInfo.AdminMemberID +
                                      ", MemberPhotoID:" + approveInfo.MemberPhotoID +
                                      ", FileID:" + approveInfo.FileID +
                                      ", CropHeight:" + approveInfo.CropHeight +
                                      ", CropWidth:" + approveInfo.CropWidth +
                                      ", CropHeightThumb:" + approveInfo.CropHeightThumb +
                                      ", CropWidthThumb:" + approveInfo.CropWidthThumb);
            }
        }
*/

/*
        private bool isValidIntValue(int testValue)
        {
            if (testValue == Constants.NULL_INT || testValue == 0)
            {
                return false;
            }
            return true;
        }
*/

        /// <summary>
        ///     Processes each photo as indicated by the admin approval tool and saves to db and aws
        ///     TODO: refactor ApproveMemberPhoto and approveMemberPhotoBasic into one
        /// </summary>
        /// <param name="approveInfo"></param>
        public void ProcessApproveInfo(ApproveInfo approveInfo)
        {
            try
            {
                // if set to true, photo approval service will call approval/rejection methods that don't update member or write to approval queue. mainly set in SiteSetting for Mingle.
                var useBasicProcessing = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.PHOTO_APPROVAL_USE_BASIC_MODE,
                        approveInfo.CommunityID, approveInfo.SiteID));

                if (approveInfo.CaptionOnlyApproval ||
                    approveInfo.ApproveStatus == ApproveStatusType.Approved ||
                    approveInfo.ApproveStatus == ApproveStatusType.Approved_ButSuspend ||
                    approveInfo.ApproveStatus == ApproveStatusType.Approved_PossibleFraud)
                {
                    if (!approveInfo.CaptionOnlyApproval)
                        ProcessNewFile(approveInfo);

                    if (!useBasicProcessing)
                    {
                        ApproveMemberPhoto(approveInfo);
                    }
                    else
                    {
                        approveMemberPhotoBasic(approveInfo);
                    }
                }
                else
                {
                    if (!useBasicProcessing)
                    {
                        RejectMemberPhoto(approveInfo);
                    }
                    else
                    {
                        RejectMemberPhotoBasic(approveInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                // approveinfo has been validated so it should be safe to use here
                var extraInfo = "CommunityID:" + approveInfo.CommunityID +
                                ", MemberID:" + approveInfo.MemberID +
                                ", MemberPhotoID:" + approveInfo.MemberPhotoID +
                                ", FileID:" + approveInfo.FileID +
                                ", AdminMemberID:" + approveInfo.AdminMemberID;

                System.Diagnostics.Trace.WriteLine("FA: Error: " + ex + ", " + extraInfo);

                throw new BLException("Error processing photo approval." + extraInfo, ex);
            }
        }

        private CreateNewFileResult saveNewImageFile(Image image, int communityId, int siteId, int memberId)
        {
            var stream = new MemoryStream();
            image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            var imageData = stream.ToArray();
            return FileSA.Instance.SaveNewFile(imageData, "jpg", communityId, siteId, memberId, false);
        }

        private void ProcessNewFile(ApproveInfo approveInfo)
        {
            var thumbFilePath = String.Empty;

            Image fullSize = null;
            Image thumbNail = null;
            var dynamicImageFiles = new List<DynamicImageFile>();

            try
            {
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstant,
                    "PhotoApproveBL",
                    string.Format("ProcessNewFile MemberID: {0} MemberPhotoID: {1}", approveInfo.MemberID,
                        approveInfo.MemberPhotoID), null);

                // calls mnSystem..up_PhotoFileType_List
                var photoFileTypeRecords = PhotoMetadataSA.Instance.GetPhotoFileTypeRecords(approveInfo.SiteID);

                #region Get Full Size Info from DB

                string fullSizeFileWebPath;
                string fullSizeFilePath;
                FileSA.Instance.GetFilePath(approveInfo.FileID, out fullSizeFilePath, out fullSizeFileWebPath);

                if (string.IsNullOrEmpty(fullSizeFilePath))
                {
                    throw new Exception("No file information found for FileID:" + approveInfo.FileID);
                }

                System.Diagnostics.Trace.WriteLine("FA: Found filepath:" + fullSizeFilePath + ", webpath:" +
                                                   fullSizeFileWebPath + " for FileID:" + approveInfo.FileID);

                fullSize = getImageFromPath(fullSizeFilePath, approveInfo);

                #endregion

                #region Photo Manipulation

                fullSize = PhotoUtil.RemoveMetadata(fullSize);

                // Rotate
                fullSize = PhotoUtil.Rotate(fullSize, approveInfo.RotateAngle);

                foreach (var photoFileTypeRecord in photoFileTypeRecords)
                {
                    // FWS and thumbnail images are handled separately, so don't reprocess them by adding them to the dynamic image collection even if they're marked "active"
                    if (photoFileTypeRecord.Active &&
                        photoFileTypeRecord.ID != (int) BasicPhotoFileType.FWS &&
                        photoFileTypeRecord.ID != (int) BasicPhotoFileType.Thumbnail)
                    {
                        dynamicImageFiles.Add(new DynamicImageFile
                        {
                            FileTypeRecord = photoFileTypeRecord,
                            Image = (Image) fullSize.Clone()
                        });
                    }
                }

                // Crop Thumbnail
                thumbNail = PhotoUtil.Crop((Image) fullSize.Clone(), approveInfo.CropXThumb,
                    approveInfo.CropYThumb,
                    approveInfo.CropWidthThumb, approveInfo.CropHeightThumb, true);

                // Crop Full Size
                fullSize = PhotoUtil.Crop((Image) fullSize.Clone(), approveInfo.CropX, approveInfo.CropY,
                    approveInfo.CropWidth, approveInfo.CropHeight, false);

                var maxPhotoWidth = PhotoUtil.GetMaxPhotoWidth(photoFileTypeRecords);
                var maxPhotoHeight = PhotoUtil.GetMaxPhotoHeight(photoFileTypeRecords);
                var maxThumbnailWidth = PhotoUtil.GetMaxThumbnailWidth(photoFileTypeRecords);
                var maxThumbnailHeight = PhotoUtil.GetMaxThumbnailHeight(photoFileTypeRecords);

                // Resize Full Photo
                fullSize = PhotoUtil.Resize(fullSize, maxPhotoWidth, maxPhotoHeight, false);

                // Resize Thumbnail
                thumbNail = PhotoUtil.Resize(thumbNail, maxThumbnailWidth, maxThumbnailHeight, true);

                // Compress
                int fileBytesSize;
                fullSize = PhotoUtil.CompressJpeg(fullSize, out fileBytesSize);
                int thumbFileBytesSize;
                thumbNail = PhotoUtil.CompressJpeg(thumbNail, out thumbFileBytesSize);

                foreach (var dynamicImageFile in dynamicImageFiles)
                {
                    dynamicImageFile.Image = PhotoUtil.Crop(dynamicImageFile.Image, approveInfo.CropX, approveInfo.CropY,
                        dynamicImageFile.Image.Width - approveInfo.CropX,
                        dynamicImageFile.Image.Height - approveInfo.CropY, false);

                    dynamicImageFile.Image = PhotoUtil.Resize(dynamicImageFile.Image,
                        dynamicImageFile.FileTypeRecord.MaxWidth, dynamicImageFile.FileTypeRecord.MaxHeight, true);

                    int fileSize;
                    dynamicImageFile.Image = PhotoUtil.CompressJpeg(dynamicImageFile.Image, out fileSize);
                    dynamicImageFile.FileSize = fileSize;
                }

                #endregion

                // Delete current file since new one will be generated
                if (approveInfo.FileID > 0)
                {
                    FileSA.Instance.DeleteFile(approveInfo.FileID, approveInfo.CommunityID, approveInfo.SiteID);
                }
                if (approveInfo.FileIDThumb > 0)
                {
                    FileSA.Instance.DeleteFile(approveInfo.FileIDThumb, approveInfo.CommunityID, approveInfo.SiteID);
                }

                //Since we now have multiple file types, we need to delete all of them except for the original when this is a re-approval
                var member = MemberSA.Instance.GetMember(approveInfo.MemberID, MemberLoadFlags.IngoreSACache);
                var photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);

                if (photo != null)
                {
                    foreach (
                        var photoFileToDelete in
                            photo.Files.Where(
                                photoFileToDelete =>
                                    photoFileToDelete.PhotoFileType != PhotoFileType.Original &&
                                    photoFileToDelete.FileID > 0))
                    {
                        FileSA.Instance.DeleteFile(photoFileToDelete.FileID, approveInfo.CommunityID, approveInfo.SiteID);
                    }
                }

                // Save full - creates a new file every time
                var fullResult = saveNewImageFile(fullSize, approveInfo.CommunityID, approveInfo.SiteID,
                    approveInfo.MemberID);

                // Save Thumb - creates a new file every time
                var thumbResult = saveNewImageFile(thumbNail, approveInfo.CommunityID, approveInfo.SiteID,
                    approveInfo.MemberID);

                foreach (var dynamicImageFile in dynamicImageFiles)
                {
                    var newImageFileResult = saveNewImageFile(dynamicImageFile.Image, approveInfo.CommunityID,
                        approveInfo.SiteID, approveInfo.MemberID);

                    var photoFile =
                        new Member.ValueObjects.Photos.PhotoFile((PhotoFileType) dynamicImageFile.FileTypeRecord.ID,
                            dynamicImageFile.Image.Height,
                            dynamicImageFile.Image.Width,
                            dynamicImageFile.FileSize,
                            newImageFileResult.FileID,
                            newImageFileResult.WebPath,
                            newImageFileResult.CloudPath);

                    approveInfo.Files.Add(photoFile);
                }

                approveInfo.FileID = fullResult.FileID;
                approveInfo.FilePath = fullSizeFilePath;
                approveInfo.FileWebPath = fullResult.WebPath;
                approveInfo.FileCloudPath = fullResult.CloudPath;
                approveInfo.ThumbFilePath = thumbFilePath;
                approveInfo.ThumbWebPath = thumbResult.WebPath;
                approveInfo.ThumbFileCloudPath = thumbResult.CloudPath;
                approveInfo.FileIDThumb = thumbResult.FileID;

                approveInfo.FileHeight = fullSize.Height;
                approveInfo.FileWidth = fullSize.Width;
                approveInfo.FileSize = fileBytesSize;
                approveInfo.ThumbFileHeight = thumbNail.Height;
                approveInfo.ThumbFileWidth = thumbNail.Width;
                approveInfo.ThumbFileSize = thumbFileBytesSize;

                var mingleCallbackEnabled =
                    Convert.ToBoolean(RuntimeSettings.GetSetting(
                        SettingConstants.MINGLE_PHOTO_APPROVAL_CALLBACK_ENABLED, approveInfo.CommunityID,
                        approveInfo.SiteID));

                if (mingleCallbackEnabled)
                {
                    EnqueueMingleCallback(approveInfo, fullResult, thumbResult, photo,
                        fullSize, thumbNail);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstant,
                    "PhotoApproveBL",
                    ex,
                    string.Format("MemberID: {0} MemberPhotoID: {1}", approveInfo.MemberID, approveInfo.MemberPhotoID));

                throw;
            }
            finally
            {
                if (fullSize != null)
                {
                    fullSize.Dispose();
                }

                if (thumbNail != null)
                {
                    thumbNail.Dispose();
                }
            }
        }

        private string GetAdminMemberUserNameForMingle(int memberId)
        {
            var member = MemberSA.Instance.GetMember(memberId);
            var mailAddress = new MailAddress(member.EmailAddress);
            return mailAddress.User;
        }

        /// <summary>
        ///     Enqueues a new Mingle callback to RabbitMQ for Spark.Migration.QueueProcessor to pickup
        /// </summary>
        /// <param name="approveInfo"></param>
        /// <param name="fullResult"></param>
        /// <param name="thumbResult"></param>
        /// <param name="photo"></param>
        /// <param name="fullSize"></param>
        /// <param name="thumbNail"></param>
        private void EnqueueMingleCallback(ApproveInfo approveInfo, CreateNewFileResult fullResult, CreateNewFileResult thumbResult, Photo photo, Image fullSize,
            Image thumbNail)
        {
            var bucketName = RuntimeSettings.GetSetting(SettingConstants.AWS_MEMBER_PHOTO_BUCKET_NAME,
                approveInfo.CommunityID, approveInfo.SiteID);
            var originalBucketName =
                RuntimeSettings.GetSetting(SettingConstants.AWS_ORIGINAL_MEMBER_PHOTO_BUCKET_NAME,
                    approveInfo.CommunityID, approveInfo.SiteID);

            var apiRequest = new Spark.MingleAPIAdapter.ExternalModels.ExternalMigrationPhotoApprovalRequest
            {
                MemberId = approveInfo.MemberID,
                MemberPhotoId = approveInfo.MemberPhotoID,
                CloudFullPath = bucketName + fullResult.CloudPath,
                LaFullPath = fullResult.WebPath,
                CloudThumbPath = bucketName + thumbResult.CloudPath,
                LaThumbPath = thumbResult.WebPath,
                AdminName = GetAdminMemberUserNameForMingle(approveInfo.AdminMemberID),
                FullWidth = fullSize.Width,
                FullHeight = fullSize.Height,
                ThumbHeight = thumbNail.Height,
                ThumbWidth = thumbNail.Width,
            };

            // 96size refers to mobile device resolution?
            var nineSixtySize =
                (from pf in approveInfo.Files where pf.PhotoFileType == PhotoFileType.iPhone4S select pf).FirstOrDefault();

            if (nineSixtySize != null)
                apiRequest.NineSixtyPath = bucketName + nineSixtySize.FileCloudPath;
            else
                throw new Exception("NineSixtySize not found in photofiles collection.");

            var previewSize =
                (from pf in approveInfo.Files where pf.PhotoFileType == PhotoFileType.MinglePreview select pf)
                    .FirstOrDefault();
            if (previewSize != null)
            {
                apiRequest.CloudPreviewPath = bucketName + previewSize.FileCloudPath;
                apiRequest.LaPreviewPath = previewSize.FileWebPath;
                apiRequest.PreviewHeight = previewSize.FileHeight;
                apiRequest.PreviewWidth = previewSize.FileWidth;
            }
            else
                throw new Exception("PreviewSize not found in photofiles collection.");

            if (photo != null && photo.Files != null)
            {
                var originalPhoto =
                    (from pf in photo.Files where pf.PhotoFileType == PhotoFileType.Original select pf)
                        .FirstOrDefault();
                if (originalPhoto == null)
                {
                    throw new Exception("OriginalPhoto not found in photofiles collection.");
                }
                apiRequest.OriginalPath = originalBucketName + originalPhoto.FileCloudPath;
            }
            else
            {
                throw new Exception("Photo Not Found");
            }

            foreach (Brand b in BrandConfigSA.Instance.GetBrandsBySite(approveInfo.SiteID))
            {
                apiRequest.BrandId = b.BrandID;
                break;
            }

            _publisher.Publish(apiRequest);
        }

        /// <summary>
        ///     Works with either UNC path or HTTP web path
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="approveInfo"></param>
        /// <returns></returns>
        private Image getImageFromPath(String filePath, ApproveInfo approveInfo)
        {
            try
            {
                var image = Image.FromFile(filePath);

                var stream = new MemoryStream();
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

                var fileData = stream.ToArray();
                long fileSize = fileData.Length;

                System.Diagnostics.Trace.WriteLine("FA:Loaded photo. " +
                                                   "FilePath:" + filePath +
                                                   ",  MemberID:" + approveInfo.MemberID +
                                                   ", MemberPhotoID:" + approveInfo.MemberPhotoID +
                                                   ", AdminMemberID:" + approveInfo.AdminMemberID +
                                                   ", Size:" + fileSize +
                                                   ", Width:" + image.Width +
                                                   ", Height:" + image.Height +
                                                   ", CropHeight:" + approveInfo.CropHeight +
                                                   ", CropWidth:" + approveInfo.CropWidth +
                                                   ", CropX:" + approveInfo.CropX +
                                                   ", CropY:" + approveInfo.CropY +
                                                   ", CropHeightThumb:" + approveInfo.CropHeightThumb +
                                                   ", CropWidthThumb:" + approveInfo.CropWidthThumb +
                                                   ", CropXThumb:" + approveInfo.CropXThumb +
                                                   ", CropYThumb:" + approveInfo.CropYThumb +
                                                   ", RotateAngle:" + approveInfo.RotateAngle);

                image.Dispose();

                return Image.FromStream(stream);
            }
            catch (Exception ex)
            {
                throw new BLException("Error retrieving photo file. [" +
                                      "FilePath: " + filePath + ", FileID: " + approveInfo.FileID +
                                      ", MemberPhotoID: " + approveInfo.MemberPhotoID + ", MemberID: " +
                                      approveInfo.MemberID + "]",
                    ex);
            }
        }

        private void RejectMemberPhoto(ApproveInfo approveInfo)
        {
            var member = MemberSA.Instance.GetMember(approveInfo.MemberID, MemberLoadFlags.IngoreSACache);
            int lastBrandId;

            member.GetLastLogonDate(approveInfo.CommunityID, out lastBrandId);
            var photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);

            var brand = BrandConfigSA.Instance.GetBrandByID(lastBrandId);

            if (photo == null) return;
            var photoUpdate = new PhotoUpdate(null,
                true,
                approveInfo.MemberPhotoID,
                Constants.NULL_INT,
                string.Empty,
                Constants.NULL_INT,
                string.Empty,
                photo.ListOrder,
                false,
                false,
                Constants.NULL_INT,
                Constants.NULL_INT);

            MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID,
                approveInfo.MemberID, new[] {photoUpdate});

            ExternalMailSA.Instance.SendPhotoRejectedNotification
                (approveInfo.MemberID,
                    approveInfo.MemberPhotoID,
                    lastBrandId,
                    approveInfo.ApproveStatus.ToString());

            // Send push notification
            PushNotificationsProcessorAdapter.Instance.SendSystemEvent(new PhotoRejectionEvent
            {
                BrandId = brand.BrandID,
                EventCategory = SystemEventCategory.MemberGenerated,
                EventType = SystemEventType.PhotoRejected,
                MemberId = approveInfo.MemberID,
                NotificationTypeId = PushNotificationTypeID.PhotoRejected,
                AppGroupId = AppGroupID.JDateAppGroup
            });

            AdminSA.Instance.AdminActionLogInsert(approveInfo.MemberID,
                approveInfo.CommunityID,
                (int) AdminAction.RejectPhoto,
                approveInfo.AdminMemberID,
                AdminMemberIDType.AdminProfileMemberID,
                ApprovalStatusToAdminActionReason(approveInfo.ApproveStatus));

            DBApproveQueueSA.Instance.CompletePhotoApproval(approveInfo.MemberPhotoID,
                DBQ.ApprovalStatus.Completed, approveInfo.AdminMemberID, (int) AdminAction.RejectPhoto);
        }

        private void RejectMemberPhotoBasic(ApproveInfo approveInfo)
        {
            var member = MemberSA.Instance.GetMember(approveInfo.MemberID, MemberLoadFlags.IngoreSACache);
            var brand =
                (from Brand b in BrandConfigSA.Instance.GetBrandsBySite(approveInfo.SiteID) select b).FirstOrDefault();
            var photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);

            if (photo != null && brand != null)
            {
                var photoUpdate = new PhotoUpdate(null,
                    true,
                    approveInfo.MemberPhotoID,
                    Constants.NULL_INT,
                    string.Empty,
                    Constants.NULL_INT,
                    string.Empty,
                    photo.ListOrder,
                    false,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT);

                MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID,
                    approveInfo.MemberID, new[] {photoUpdate});

                if (approveInfo.AdminMemberID > 0)
                {
                    AdminSA.Instance.AdminActionLogInsert(approveInfo.MemberID,
                        approveInfo.CommunityID,
                        (int) AdminAction.RejectPhoto,
                        approveInfo.AdminMemberID,
                        AdminMemberIDType.AdminProfileMemberID,
                        ApprovalStatusToAdminActionReason(approveInfo.ApproveStatus));
                }
            }
        }

        /// <summary>
        ///     Does *not* do the following. todo:Use this info to refactor with the other main method.
        ///     - Send photo approval notification email
        ///     - Send to Push Notification
        ///     - Insert to Photo Store
        ///     - Send approval completion back to DBApproveQueue
        /// 
        ///     For photo approval tool consolidation, 
        /// </summary>
        /// <param name="approveInfo"></param>
        private void approveMemberPhotoBasic(ApproveInfo approveInfo)
        {
            var isCaptionApproved = false;
            System.Diagnostics.Debug.WriteLine("Before getting member from cache for memberid=" + approveInfo.MemberID);
            var member = MemberSA.Instance.GetMember(approveInfo.MemberID,
                !approveInfo.CaptionOnlyApproval ? MemberLoadFlags.None : MemberLoadFlags.IngoreSACache);

            var photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);
            System.Diagnostics.Debug.WriteLine("Got photos for photoid=" + approveInfo.MemberPhotoID);

            if (photo == null)
            {
                System.Diagnostics.Debug.WriteLine("Got null for photoid=" + approveInfo.MemberPhotoID);
                //try load not from the cache and only then throw an exception
                member = MemberSA.Instance.GetMember(approveInfo.MemberID, MemberLoadFlags.IngoreSACache);
                System.Diagnostics.Debug.WriteLine("Before getting member ignoring cache for memberid=" +
                                                   approveInfo.MemberID);
                photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);
                if (photo == null)
                    throw new BLException("Member photo not found after re-loading member obj. [MemberID: " +
                                          approveInfo.MemberID + ", MemberPhotoID: " + approveInfo.MemberPhotoID +
                                          ", CommunityID:" + approveInfo.CommunityID + "]");
            }

            System.Diagnostics.Trace.WriteLine("FA:ApproveStatus:" + approveInfo.ApproveStatus);
            System.Diagnostics.Trace.WriteLine("FA:ApproveStatus Caption:" + approveInfo.CaptionDeleteFlag);
            System.Diagnostics.Trace.WriteLine("FA: Photo Caption=" + photo.Caption + ", photoid=" + photo.MemberPhotoID);
            if (approveInfo.CaptionOnlyApproval)
            {
                approveInfo.FileID = photo.FileID;
                approveInfo.FilePath = photo.FilePath;
                approveInfo.FileWebPath = photo.FileWebPath;
                approveInfo.FileCloudPath = photo.FileCloudPath;
                approveInfo.ThumbFilePath = photo.ThumbFilePath;
                approveInfo.ThumbWebPath = photo.ThumbFileWebPath;
                approveInfo.ThumbFileCloudPath = photo.ThumbFileCloudPath;
                approveInfo.FileIDThumb = photo.ThumbFileID;
                approveInfo.FileHeight = photo.FileHeight;
                approveInfo.FileWidth = photo.FileWidth;
                approveInfo.FileSize = photo.FileSize;
                approveInfo.ThumbFileHeight = photo.ThumbFileHeight;
                approveInfo.ThumbFileWidth = photo.ThumbFileWidth;
                approveInfo.ThumbFileSize = photo.ThumbFileSize;
            }

            if (approveInfo.CaptionDeleteFlag)
            {
                photo.Caption = "";
            }
            else
            {
                if (!string.IsNullOrEmpty(photo.Caption))
                    isCaptionApproved = true;
            }

            var photoUpdate = new PhotoUpdate(null,
                false,
                approveInfo.MemberPhotoID,
                approveInfo.FileID,
                approveInfo.FileWebPath,
                approveInfo.FileIDThumb,
                approveInfo.ThumbWebPath,
                photo.ListOrder,
                true,
                photo.IsPrivate,
                photo.AlbumID,
                approveInfo.AdminMemberID, photo.Caption, isCaptionApproved,
                approveInfo.FileCloudPath, approveInfo.ThumbFileCloudPath,
                approveInfo.IsApprovedForMain,
                false, false,
                approveInfo.FileHeight,
                approveInfo.FileWidth,
                approveInfo.FileSize,
                approveInfo.ThumbFileHeight,
                approveInfo.ThumbFileWidth,
                approveInfo.ThumbFileSize) {Files = approveInfo.Files};

            MemberSA.Instance.SavePhotos(Constants.NULL_INT, approveInfo.SiteID, approveInfo.CommunityID,
                approveInfo.MemberID, new[] {photoUpdate});

            var brand =
                (from Brand b in BrandConfigSA.Instance.GetBrandsBySite(approveInfo.SiteID) select b).FirstOrDefault();

            if (brand != null && approveInfo.AdminMemberID > 0)
            {
                AdminSA.Instance.AdminActionLogInsert(approveInfo.MemberID,
                    approveInfo.CommunityID,
                    (int) AdminAction.ApprovePhoto,
                    approveInfo.AdminMemberID,
                    brand.BrandID);
            }
        }

        private void ApproveMemberPhoto(ApproveInfo approveInfo)
        {
            var isCaptionApproved = false;
            System.Diagnostics.Debug.WriteLine("Before getting member from cache for memberid=" + approveInfo.MemberID);
            var member = MemberSA.Instance.GetMember(approveInfo.MemberID,
                !approveInfo.CaptionOnlyApproval ? MemberLoadFlags.None : MemberLoadFlags.IngoreSACache);

            var photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);
            System.Diagnostics.Debug.WriteLine("Got photos for photoid=" + approveInfo.MemberPhotoID);
            var sendCaptionRejectionEmail = false;
            if (photo == null)
            {
                System.Diagnostics.Debug.WriteLine("Got null for photoid=" + approveInfo.MemberPhotoID);
                //try load not from the cache and only then throw an exception
                member = MemberSA.Instance.GetMember(approveInfo.MemberID, MemberLoadFlags.IngoreSACache);
                System.Diagnostics.Debug.WriteLine("Before getting member ignoring cache for memberid=" +
                                                   approveInfo.MemberID);
                photo = member.GetPhotos(approveInfo.CommunityID).Find(approveInfo.MemberPhotoID);
                if (photo == null)
                    throw new BLException("Member photo not found after re-loading member obj. [MemberID: " +
                                          approveInfo.MemberID + ", MemberPhotoID: " + approveInfo.MemberPhotoID +
                                          ", CommunityID:" + approveInfo.CommunityID + "]");
            }

            System.Diagnostics.Trace.WriteLine("FA:ApproveStatus:" + approveInfo.ApproveStatus);
            System.Diagnostics.Trace.WriteLine("FA:ApproveStatus Caption:" + approveInfo.CaptionDeleteFlag);
            System.Diagnostics.Trace.WriteLine("FA: Photo Caption=" + photo.Caption + ", photoid=" + photo.MemberPhotoID);
            if (approveInfo.CaptionOnlyApproval)
            {
                approveInfo.FileID = photo.FileID;
                approveInfo.FilePath = photo.FilePath;
                approveInfo.FileWebPath = photo.FileWebPath;
                approveInfo.FileCloudPath = photo.FileCloudPath;
                approveInfo.ThumbFilePath = photo.ThumbFilePath;
                approveInfo.ThumbWebPath = photo.ThumbFileWebPath;
                approveInfo.ThumbFileCloudPath = photo.ThumbFileCloudPath;
                approveInfo.FileIDThumb = photo.ThumbFileID;
                approveInfo.FileHeight = photo.FileHeight;
                approveInfo.FileWidth = photo.FileWidth;
                approveInfo.FileSize = photo.FileSize;
                approveInfo.ThumbFileHeight = photo.ThumbFileHeight;
                approveInfo.ThumbFileWidth = photo.ThumbFileWidth;
                approveInfo.ThumbFileSize = photo.ThumbFileSize;
            }

            if (approveInfo.CaptionDeleteFlag)
            {
                photo.Caption = "";
                sendCaptionRejectionEmail = true;
            }
            else
            {
                if (!string.IsNullOrEmpty(photo.Caption))
                    isCaptionApproved = true;
            }

            var photoUpdate = new PhotoUpdate(null,
                false,
                approveInfo.MemberPhotoID,
                approveInfo.FileID,
                approveInfo.FileWebPath,
                approveInfo.FileIDThumb,
                approveInfo.ThumbWebPath,
                photo.ListOrder,
                true,
                photo.IsPrivate,
                photo.AlbumID,
                approveInfo.AdminMemberID, photo.Caption, isCaptionApproved,
                approveInfo.FileCloudPath, approveInfo.ThumbFileCloudPath,
                approveInfo.IsApprovedForMain,
                false, false,
                approveInfo.FileHeight,
                approveInfo.FileWidth,
                approveInfo.FileSize,
                approveInfo.ThumbFileHeight,
                approveInfo.ThumbFileWidth,
                approveInfo.ThumbFileSize);

            photoUpdate.Files = approveInfo.Files;
            MemberSA.Instance.SavePhotos(Constants.NULL_INT, approveInfo.SiteID, approveInfo.CommunityID,
                approveInfo.MemberID, new[] {photoUpdate});

            int lastBrandId;

            member.GetLastLogonDate(approveInfo.CommunityID, out lastBrandId);

            if (lastBrandId == Constants.NULL_INT)
            {
                throw new BLException("Could not retrieve member's last brand ID. " +
                                      "[MemberID: " + approveInfo.MemberID + ", CommunityID: " +
                                      approveInfo.CommunityID + "]");
            }

            if (!approveInfo.CaptionOnlyApproval)
            {
                ExternalMailSA.Instance.SendPhotoApprovedNotification
                    (approveInfo.MemberID,
                        approveInfo.MemberPhotoID,
                        lastBrandId,
                        approveInfo.ApproveStatus.ToString(), sendCaptionRejectionEmail);
                InsertPhotoStore(photoUpdate, approveInfo.MemberID, approveInfo.CommunityID);

                // Send push notification
                PushNotificationsProcessorAdapter.Instance.SendSystemEvent(new PhotoApprovalEvent
                {
                    BrandId = lastBrandId,
                    EventCategory = SystemEventCategory.MemberGenerated,
                    EventType = SystemEventType.PhotoApproved,
                    MemberId = approveInfo.MemberID,
                    NotificationTypeId = PushNotificationTypeID.PhotoApproved,
                    AppGroupId = AppGroupID.JDateAppGroup
                });
            }
            else if (sendCaptionRejectionEmail)
                ExternalMailSA.Instance.SendPhotoCaptionRejectNotification(approveInfo.MemberID,
                    approveInfo.MemberPhotoID, lastBrandId);

            AdminSA.Instance.AdminActionLogInsert(approveInfo.MemberID,
                approveInfo.CommunityID,
                (int) AdminAction.ApprovePhoto,
                approveInfo.AdminMemberID,
                lastBrandId);

            DBQ.ApprovalStatus photoApproveStatus;

            switch (approveInfo.ApproveStatus)
            {
                case ApproveStatusType.Approved_ButSuspend:
                    photoApproveStatus = DBQ.ApprovalStatus.CompletedButSuspended;
                    break;
                case ApproveStatusType.Approved_PossibleFraud:
                    photoApproveStatus = DBQ.ApprovalStatus.CompletedPossibleFraud;
                    break;
                default:
                    photoApproveStatus = DBQ.ApprovalStatus.Completed;
                    break;
            }

            DBApproveQueueSA.Instance.CompletePhotoApproval(approveInfo.MemberPhotoID, photoApproveStatus,
                approveInfo.AdminMemberID, (int) AdminAction.ApprovePhoto);
        }

        private AdminActionReasonID ApprovalStatusToAdminActionReason(ApproveStatusType approvalStatus)
        {
            var reason = AdminActionReasonID.PhotoBlurry;

            switch (approvalStatus)
            {
                case ApproveStatusType.Rejected_Blurry:
                    reason = AdminActionReasonID.PhotoBlurry;
                    break;
                case ApproveStatusType.Rejected_Copyright:
                    reason = AdminActionReasonID.PhotoCopyright;
                    break;
                case ApproveStatusType.Rejected_FileSize:
                    reason = AdminActionReasonID.PhotoFilesize;
                    break;
                case ApproveStatusType.Rejected_General:
                    reason = AdminActionReasonID.PhotoGeneral;
                    break;
                case ApproveStatusType.Rejected_Suggestive:
                    reason = AdminActionReasonID.PhotoSuggestive;
                    break;
                case ApproveStatusType.Rejected_UnknownMember:
                    reason = AdminActionReasonID.PhotoUnknownMember;
                    break;
                case ApproveStatusType.Rejected_BadFileFormat:
                    reason = AdminActionReasonID.PhotoBadFileFormat;
                    break;
            }

            return reason;
        }

        #endregion

        #region PhotoStore

        private void InsertPhotoStore(PhotoUpdate photoUpdate, int memberid, int communityid)
        {
            try
            {
                var updateStore =
                    Conversion.CBool(RuntimeSettings.GetSetting(
                        "ENABLE_PHOTOSTORE_UPDATE", communityid));
                if (updateStore)
                    PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.InsertPhoto(photoUpdate, memberid,
                        communityid);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstant, "PhotoApproveBL", ex);
            }
        }

        #endregion
    }
}