#region

using System.Diagnostics;

#endregion

namespace Matchnet.PhotoApprove.BusinessLogic
{
    public class Metrics
    {
        public const string ServiceName = "Matchnet.PhotoApprove.Service";
        public static readonly Metrics Instance = new Metrics();

        public static PerformanceCounter BatchesTotal;
        public static PerformanceCounter ErrorsTotal;
        public static PerformanceCounter SuccessTotal;
        public static PerformanceCounter AvgApprovalSecs;
        public static PerformanceCounter AvgApprovalSecsBase;

        private Metrics()
        {
            if (!PerformanceCounterCategory.Exists(ServiceName)) return;
            BatchesTotal = new PerformanceCounter(ServiceName, "Batches Total", false);
            ErrorsTotal = new PerformanceCounter(ServiceName, "Errors Total", false);
            SuccessTotal = new PerformanceCounter(ServiceName, "Success Total", false);
            AvgApprovalSecs = new PerformanceCounter(ServiceName, "Avg Batch Time", false);
            AvgApprovalSecsBase = new PerformanceCounter(ServiceName, "Avg Batch Time_base", false);

            ResetCounters();
        }

        public void ResetCounters()
        {
            BatchesTotal.RawValue = 0;
            ErrorsTotal.RawValue = 0;
            SuccessTotal.RawValue = 0;
            AvgApprovalSecs.RawValue = 0;
            AvgApprovalSecsBase.RawValue = 0;
        }

        public void WriteInformation(string message)
        {
            EventLog.WriteEntry(ServiceName, message, EventLogEntryType.Information);
        }

        public void WriteError(string message)
        {
            EventLog.WriteEntry(ServiceName, message, EventLogEntryType.Error);
        }

        public class MetricsInstaller
        {
            public static void Install()
            {
                if (!PerformanceCounterCategory.Exists(ServiceName))
                {
                    var counters = new CounterCreationDataCollection
                    {
                        new CounterCreationData("Batches Total", "# items processed total",
                            PerformanceCounterType.NumberOfItems32),
                        new CounterCreationData("Errors Total", "# items processed total",
                            PerformanceCounterType.NumberOfItems32),
                        new CounterCreationData("Success Total", "# items processed total",
                            PerformanceCounterType.NumberOfItems32),
                        new CounterCreationData("Avg Batch Time", "# seconds for a batch",
                            PerformanceCounterType.AverageCount64),
                        new CounterCreationData("Avg Batch Time_base", "# seconds for a batch base",
                            PerformanceCounterType.AverageBase)
                    };

                    PerformanceCounterCategory.Create(ServiceName,
                        "Counters for the service that runs Matchnet Photo Approval Service",
                        PerformanceCounterCategoryType.SingleInstance, counters);
                }

                if (!EventLog.SourceExists(ServiceName))
                {
                    EventLog.CreateEventSource(ServiceName, "Application");
                }
            }

            public static void Uninstall()
            {
                if (PerformanceCounterCategory.Exists(ServiceName))
                {
                    PerformanceCounterCategory.Delete(ServiceName);
                }

                if (EventLog.SourceExists(ServiceName))
                {
                    EventLog.DeleteEventSource(ServiceName);
                }
            }
        }
    }
}