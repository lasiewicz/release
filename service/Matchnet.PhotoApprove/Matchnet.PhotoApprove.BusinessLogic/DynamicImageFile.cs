﻿#region

using System.Drawing;
using Matchnet.Content.ValueObjects.Photos;

#endregion

namespace Matchnet.PhotoApprove.BusinessLogic
{
    public class DynamicImageFile
    {
        public Image Image { get; set; }
        public int FileSize { get; set; }
        public PhotoFileTypeRecord FileTypeRecord { get; set; }
    }
}