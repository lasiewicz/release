#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.Photos;
using Matchnet.Exceptions;

#endregion

namespace Matchnet.PhotoApprove.BusinessLogic
{
    /// <summary>
    ///     Provides photo manipulation methods
    /// </summary>
    internal static class PhotoUtil
    {
        public static string MsmqPath
        {
            get { return RuntimeSettings.GetSetting("PHOTO_APPROVE_MSMQ_PATH"); }
        }

        public static int MaxPhotoWidth
        {
            get { return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_XMAX")); }
        }

        public static int MaxPhotoHeight
        {
            get { return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_YMAX")); }
        }

        public static int MaxPhotoSizeBytes
        {
            get { return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_MAXBYTES")); }
        }

        public static int ThumbnailWidth
        {
            get { return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTOTHUMB_XMAX")); }
        }

        public static int ThumbnailHeight
        {
            get { return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTOTHUMB_YMAX")); }
        }

        public static int DefaultPhotoSizeBytes
        {
            get { return 19456; }
        }

        public static int GetMaxPhotoWidth(List<PhotoFileTypeRecord> fileTypeRecords)
        {
            var mainPhotoRecord =
                (from pht in fileTypeRecords where pht.ID == (int) BasicPhotoFileType.FWS && pht.Active select pht)
                    .FirstOrDefault();
            if (mainPhotoRecord != null)
            {
                return mainPhotoRecord.MaxWidth;
            }
            return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_XMAX"));
        }

        public static int GetMaxPhotoHeight(List<PhotoFileTypeRecord> fileTypeRecords)
        {
            var mainPhotoRecord =
                (from pht in fileTypeRecords where pht.ID == (int) BasicPhotoFileType.FWS && pht.Active select pht)
                    .FirstOrDefault();
            if (mainPhotoRecord != null)
            {
                return mainPhotoRecord.MaxHeight;
            }
            return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_YMAX"));
        }

        public static int GetMaxThumbnailWidth(List<PhotoFileTypeRecord> fileTypeRecords)
        {
            var mainPhotoRecord =
                (from pht in fileTypeRecords where pht.ID == (int) BasicPhotoFileType.Thumbnail && pht.Active select pht)
                    .FirstOrDefault();
            if (mainPhotoRecord != null)
            {
                return mainPhotoRecord.MaxWidth;
            }
            return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTOTHUMB_XMAX"));
        }

        public static int GetMaxThumbnailHeight(List<PhotoFileTypeRecord> fileTypeRecords)
        {
            var mainPhotoRecord =
                (from pht in fileTypeRecords where pht.ID == (int) BasicPhotoFileType.Thumbnail && pht.Active select pht)
                    .FirstOrDefault();
            if (mainPhotoRecord != null)
            {
                return mainPhotoRecord.MaxHeight;
            }
            return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTOTHUMB_YMAX"));
        }

        /// <summary>
        ///     Compresses a jpg image file
        ///     Start off with jpeg 90 quality factor and decrements by 2 in loop
        ///     When not used in junction with resize it could come out with very
        ///     poor quality.
        ///     0 - Lowest Quality, 100 - Highest Quality
        /// </summary>
        /// <param name="image">Jpg file</param>
        /// <param name="fileBytesSize"></param>
        public static Image CompressJpeg(Image image, out int fileBytesSize)
        {
            var compression = 90;

            var eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
            var ici = GetEncoderInfo("image/jpeg");

            var stream = new MemoryStream();
            image.Save(stream, ici, eps);
            var length = stream.Length;

            while ((length > MaxPhotoSizeBytes) & (compression > 0))
            {
                eps = new EncoderParameters(1);
                eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
                stream = new MemoryStream();
                image.Save(stream, ici, eps);
                length = stream.Length;
                compression -= 2;
                //stream.Close(); //http://support.microsoft.com/?id=814675
            }

            fileBytesSize = (int) stream.Length;
            image = Image.FromStream(stream);
            return image;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="cropX"></param>
        /// <param name="cropY"></param>
        /// <param name="cropWidth"></param>
        /// <param name="cropHeight"></param>
        /// <param name="isThumb"></param>
        /// <returns></returns>
        public static Image Crop(Image sourceImage, int cropX, int cropY, int cropWidth,
            int cropHeight, bool isThumb)
        {
            #region Validate
            if (cropX < 0 || cropY < 0 || cropWidth <= 0 || cropHeight <= 0)
            {
                throw new Exception("Crop() Negative param values are passed in. [cropX:" + cropX + ", cropY:" + cropY +
                                    ", targetwidth:" +
                                    cropWidth + ", targetheight:" + cropHeight + "]");
            }

            if (sourceImage.Width < cropWidth || sourceImage.Height < cropHeight)
            {
                var exceptionMessage = "Crop() Invalid param values are passed in. [isthumb:" + isThumb +
                                       ", sourcewidth:" + sourceImage.Width +
                                       ", sourceHeight:" + sourceImage.Height + ", targetwidth:" +
                                       cropWidth + ", targetheight:" + cropHeight + "]";

                System.Diagnostics.Trace.WriteLine(exceptionMessage);

                throw new Exception(exceptionMessage);
            }
            #endregion

            // all valid sizes are passed in
            if (cropX == 0 && cropY == 0 && sourceImage.Height == cropHeight && sourceImage.Width == cropWidth)
            {
                return sourceImage;
            }

            Graphics graphics = null;

            try
            {
                var cropped = new Bitmap(cropWidth, cropHeight);

                graphics = Graphics.FromImage(cropped);

                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                var rectangle = new Rectangle(0, 0, cropWidth, cropHeight);

                graphics.DrawImage(sourceImage, rectangle, cropX, cropY, cropWidth, cropHeight, GraphicsUnit.Pixel);

                return cropped;
            }
            finally
            {
                if (graphics != null)
                {
                    graphics.Dispose();
                }

                sourceImage.Dispose();
            }
        }

        public static Image GetImageFromWeb(string webPath)
        {
            try
            {
                var wc = new System.Net.WebClient();
                var stream = new MemoryStream(wc.DownloadData(webPath));
                var image = Image.FromStream(stream);
                //stream.Close(); //http://support.microsoft.com/?id=814675
                return image;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get image from webserver " +
                                      "[WebPath: " + webPath + "]", ex);
            }
        }

        public static Image Resize(Image photo, int maxWidth, int maxHeight, bool isThumb)
        {
            var originalWidth = photo.Width;
            var origianlHeight = photo.Height;

            const int sourceX = 0;
            const int sourceY = 0;

            // If smaller than spec than return it back untouched.
            if ((originalWidth <= maxWidth) & (origianlHeight <= maxHeight))
                return photo;

            // percent value of the new width based on the original width
            var widthPercent = (maxWidth / (float)originalWidth);
            // percent value of the new height based on the original width
            var heightPercent = (maxHeight / (float)origianlHeight);

            // determine which percent to use to keep aspect ratio
            var finalPercent = heightPercent < widthPercent ? heightPercent : widthPercent;

            // figure out the final width and height based on the final percent
            var finalWidth = (int) (originalWidth*finalPercent);
            var finalHeight = (int) (origianlHeight*finalPercent);

            // create a new final photo bitmap
            var finalPhoto = new Bitmap(finalWidth, finalHeight, PixelFormat.Format32bppRgb);
            finalPhoto.SetResolution(photo.HorizontalResolution, photo.VerticalResolution);

            // use the graphics library to fill out the final photo
            var grPhoto = Graphics.FromImage(finalPhoto);
            try
            {
                // transparent may be a better option
                grPhoto.Clear(Color.White);
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

                grPhoto.DrawImage(photo,
                    new Rectangle(sourceX, sourceY, finalWidth, finalHeight),
                    new Rectangle(sourceX, sourceY, originalWidth, origianlHeight),
                    GraphicsUnit.Pixel);
            }
            finally
            {
                // gurantees disposal of the images in the finally block
                grPhoto.Dispose();
                photo.Dispose();
            }

            return finalPhoto;
        }

        public static Image Rotate(Image photo, int angle)
        {
            EncoderValue angleValue;

            switch (angle)
            {
                case 0:
                    return photo;
                case 90:
                    angleValue = EncoderValue.TransformRotate90;
                    break;
                case 180:
                    angleValue = EncoderValue.TransformRotate180;
                    break;
                case 270:
                    angleValue = EncoderValue.TransformRotate270;
                    break;
                default:
                    throw new Exception("Angle must be 0, 90, 180, or 270. [Angle: "
                                        + angle + "]");
            }

            var enc = Encoder.Transformation;
            var encParms = new EncoderParameters(1);
            var codecInfo = GetEncoderInfo("image/jpeg");

            // for rewriting without recompression we must rotate the image 90 degrees
            var encParm = new EncoderParameter(enc, (long) angleValue);
            encParms.Param[0] = encParm;

            var stream = new MemoryStream();
            photo.Save(stream, codecInfo, encParms);

            return Image.FromStream(stream);
        }

        /// <summary>
        ///     Removes all metadata properties. Initially requested due to GPS info being shown to members.
        /// </summary>
        /// <param name="photo"></param>
        /// <returns></returns>
        public static Image RemoveMetadata(Image photo)
        {
            var newStream = new MemoryStream();
            var encoderQuality = new EncoderParameter(Encoder.Quality, 100L);

            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = encoderQuality;

            var imageCodecInfo = GetEncoderInfo("image/jpeg");

            try
            {
                foreach (var item in photo.PropertyItems)
                {
                    photo.RemovePropertyItem(item.Id);
                    Console.WriteLine("Removed " + item.Id);
                }

                photo.RotateFlip(RotateFlipType.Rotate180FlipNone);
                photo.RotateFlip(RotateFlipType.Rotate180FlipNone);
                photo.Save(newStream, imageCodecInfo, encoderParams);
                var removedPhoto = Image.FromStream(new MemoryStream(newStream.ToArray()));
                return removedPhoto;
            }
            finally
            {
                photo.Dispose();
                newStream.Close();
            }
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            var encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
    }
}