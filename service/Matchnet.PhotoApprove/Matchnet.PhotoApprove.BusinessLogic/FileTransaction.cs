using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.EnterpriseServices;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Messaging;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Data.Configuration;

namespace Matchnet.PhotoApprove.BusinessLogic
{
	/// <summary>
	/// This is a slightly modified version of Matchnet.Member.BusinessLogic.FileTransaction class
	/// It just doesn't use FileRootID as a param.
	/// This needs to move out to File's own MT in the future.
	/// </summary>
		[Transaction(TransactionOption.Required)]
			public class FileTransaction : ServicedComponent
		{
			public FileTransaction()
			{
			}

			public void SaveFile(Int32 fileID,
				string fileExtension,
				out Exception exception)
			{
				bool written = false;
				const string LOGICAL_DB = "mnFile";
				Partition partition = ConnectionDispenser.Instance.GetLogicalDatabase(LOGICAL_DB).GetPartition(fileID);
				PhysicalDatabases physicalDatabases = partition.PhysicalDatabases;
				Int32 pdbCount = physicalDatabases.Count;
				SqlConnection[] connections = new SqlConnection[pdbCount];
				string connectionString = "";

				exception = null;

				try
				{
					for (Int32 pdbNum = 0; pdbNum < pdbCount; pdbNum++)
					{
						PhysicalDatabase pdb = physicalDatabases[pdbNum];

						SqlCommand cmd = new SqlCommand();
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.CommandText = "dbo.up_File_Insert";
						cmd.Parameters.Add("@FileID", SqlDbType.Int).Value = fileID;
						cmd.Parameters.Add("@Extension", SqlDbType.VarChar).Value = fileExtension;

						if (pdb.IsActive)
						{
							connectionString = pdb.ConnectionString;
							SqlConnection conn = new SqlConnection(pdb.ConnectionString);
							connections[pdbNum] = conn;

							cmd.Connection = conn;
							conn.Open();
							cmd.ExecuteNonQuery();
							connectionString = "";
							written = true;
						}
						else
						{
							MessageQueue queue = HydraUtility.GetQueueRecovery(HydraUtility.GetKey(LOGICAL_DB, partition.Offset), pdb.ServerName);
							queue.Send(Command.Create(LOGICAL_DB, fileID, pdb.ConnectionString, cmd), MessageQueueTransactionType.Single);
						}
					}

					if (written == false)
					{
						throw new Exception("No active mnFile databases are active for offset " + partition.Offset.ToString());
					}

					ContextUtil.SetComplete();
				}
				catch (Exception ex)
				{
					exception = new Exception("File insert error (connectionString: " + connectionString + ").", ex);
					ContextUtil.SetAbort();
				}
				finally
				{
					foreach (SqlConnection conn in connections)
					{
						if (conn != null)
						{
							if (conn.State == ConnectionState.Open)
							{
								conn.Close();
							}
						}
					}
				}
			}
		}
	}
