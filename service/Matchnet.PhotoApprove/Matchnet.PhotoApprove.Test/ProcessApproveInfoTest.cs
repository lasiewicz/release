﻿#region

using Matchnet.Member.ValueObjects.Photos;
using Matchnet.PhotoApprove.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace Matchnet.PhotoApprove.Test
{
    [TestClass]
    public class ProcessApproveInfoTest
    {
        private ApproveInfo _approveInfo;

        [TestInitialize]
        public void Initialize()
        {
            // get memberPhotoId and fileId from
            // select * from mnMember15..MemberPhoto where MemberId = 1127800166
            _approveInfo = new ApproveInfo(
               memberID: 1127800166, // stage member, wlee@spark.net
               communityID: 30, // CatholicMingle Community
               memberPhotoID: 17014736,
               fileID: 222012695,
               cropWidth: 100,
               cropHeight: 100,
               cropX: 0,
               cropY: 0,
               cropHeightThumb: 50,
               cropWidthThumb: 50,
               cropXThumb: 20,
               cropYThumb: 20,
               approveStatus: ApproveStatusType.Approved,
               captionDelete: false,
               siteID: 9071,
               captionOnly: false,
               fileIDThumb: Constants.NULL_INT,
               rotateAngle: 0,
               rotateAngleThumb: 0,
               adminMemberID: 27029711
               );
        }

        [TestMethod]
        public void MinglePhotoProcessApproveInfoTest()
        {
            // Setup
           

            // Execute
            PhotoApproveBL.Instance.ProcessApproveInfo(_approveInfo);

            // Assert
        }
    }
}