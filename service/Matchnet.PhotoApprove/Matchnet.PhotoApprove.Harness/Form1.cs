#region

using System;
using System.Messaging;
using System.Windows.Forms;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.PhotoApprove.BusinessLogic;

#endregion

namespace Matchnet.PhotoApprove.Harness
{
    /// <summary>
    ///     Summary description for Form1.
    /// </summary>
    public class Form1 : System.Windows.Forms.Form
    {
        /// <summary>
        ///     Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.Container components = null;

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;

        public Form1()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.Run(new Form1());
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            PhotoApproveBL.Instance.Run();

            //send queue
            // won's jdate photo
            var approveInfo = new ApproveInfo(3000447,
                ApproveStatusType.Approved,
                120000613,
                Matchnet.Constants.NULL_INT,
                0,
                0,
                0,
                100,
                100,
                0,
                0,
                0,
                50,
                50,
                16000206,
                3,
                16000206);

            var approveInfoCollection = new ApproveInfoCollection();

            approveInfoCollection.Add(approveInfo);

            var queue =
                new MessageQueue(RuntimeSettings.GetSetting("PHOTO_APPROVE_MSMQ_PATH"));

            var message = new System.Messaging.Message(approveInfoCollection, new BinaryMessageFormatter());

            queue.Send(message);
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            PhotoApproveBL.Instance.Stop();
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(72, 32);
            this.button1.Name = "button1";
            this.button1.TabIndex = 0;
            this.button1.Text = "start";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(72, 152);
            this.button2.Name = "button2";
            this.button2.TabIndex = 1;
            this.button2.Text = "stop";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
        }

        #endregion
    }
}