using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Replication;
using Matchnet.Session.BusinessLogic;
using Matchnet.Session.ValueObjects;
using Matchnet.Session.ServiceDefinitions;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Data.Hydra;

using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.Session.ServiceManagers
{
	public class SessionSM : MarshalByRefObject, IServiceManager, ISessionService, IReplicationRecipient
	{
		private const string SERVICE_MANAGER_NAME = "SessionSM";
		public const string SERVICE_CONSTANT = "SESSION_SVC";
		public const string SERVICE_NAME = "Matchnet.Session.Service";

		private PerformanceCounter _perfCount;
		private PerformanceCounter _perfRequestsSec;
		private PerformanceCounter _perfHitRate;
		private PerformanceCounter _perfHitRate_Base;
		private PerformanceCounter _perfSavesSec;
		private Hashtable _communityCounters;
		private ReaderWriterLock _communityCounterLock;

		private Replicator _replicator = null;
		private Synchronizer _snychronizer = null;

		private string _machineName = Constants.NULL_STRING;
		private string _replicationURI = Constants.NULL_STRING;
		private HydraWriter _hydraWriter;
		private ServiceInstanceConfig _serviceInstanceConfig = null;
		private Thread threadUpdateSessionCount = null;

		public SessionSM()
		{
			//perf
			_communityCounters = new Hashtable();
			_communityCounterLock = new ReaderWriterLock();
			initPerfCounters();
			SessionBL.Instance.SessionRequested += new Matchnet.Session.BusinessLogic.SessionBL.SessionRequestEventHandler(sessionBL_SessionRequested);
			SessionBL.Instance.SessionAdded += new Matchnet.Session.BusinessLogic.SessionBL.SessionAddEventHandler(sessionBL_SessionAdded);
			SessionBL.Instance.SessionRemoved += new Matchnet.Session.BusinessLogic.SessionBL.SessionRemoveEventHandler(sessionBL_SessionRemoved);
			SessionBL.Instance.SessionSaved += new Matchnet.Session.BusinessLogic.SessionBL.SessionSaveEventHandler(sessionBL_SessionSaved);

			//replication
			SessionBL.Instance.ReplicationRequested += new Matchnet.Session.BusinessLogic.SessionBL.ReplicationEventHandler(sessionBL_ReplicationRequested);

			SessionBL.Instance.SynchronizationRequested += new Matchnet.Session.BusinessLogic.SessionBL.SynchronizationEventHandler(sessionBL_SynchronizationRequested);

			_machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

			if(overrideMachineName != null)
			{
				_machineName = overrideMachineName;
			}

			_replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_REPLICATION_OVERRIDE");
			if (_replicationURI.Length == 0)
			{
				_replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(SERVICE_CONSTANT, SERVICE_MANAGER_NAME, _machineName);
			}

			_replicator = new Replicator(SERVICE_NAME);
			_replicator.SetDestinationUri(_replicationURI);

			EventLog.WriteEntry(SERVICE_NAME,
				"Replicating to " + _replicationURI,
				EventLogEntryType.Information);

			_replicator.Start();

			_snychronizer = new Synchronizer(SERVICE_NAME);
			_snychronizer.Start();

			// Put this in to prevent partner server from blowing up this service from network timeout.
			// http://support.microsoft.com/kb/923614/
			//make sure the build machine and host server has System.RunTime.Remoting dll version 1.1.4322.2371
			IDictionary t = new Hashtable();
			t.Add("timeout", 1);
			t.Add("name", "sessionclient");
			TcpClientChannel clientChannel = new TcpClientChannel(t, null);
			ChannelServices.RegisterChannel(clientChannel);

			//Hydra writer
			_hydraWriter = new HydraWriter(new string[]{"mnSessionCounter", "mnActivityRecording"});
			_hydraWriter.Start();

			//Instance info
			_serviceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(SessionSM.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);

			//Updating Session Count
			threadUpdateSessionCount = new Thread(new ThreadStart(PersistSessionCount));
			threadUpdateSessionCount.Start();
		}

        public void Start()
        {
            SessionBL.Instance.Start();
        }

        public void Stop()
        {
            SessionBL.Instance.Stop();
        }

		public override object InitializeLifetimeService()
		{
			return null;
		}
		

		public void PrePopulateCache()
		{
		}


		public UserSession GetSession(string clientHostName,
			CacheReference cacheReference,
			Guid sessionKey)
		{
			try 
			{
				return SessionBL.Instance.GetSession(clientHostName,
					cacheReference,
					sessionKey);
			} 
			catch (Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure getting Session.", ex);
			}
		}

		
		public void SaveSession(string clientHostName,
			UserSession session)
		{
			SaveSession(clientHostName,
				null,
				session);
		}

			
		public void SaveSession(string clientHostName,
			CacheReference cacheReference,
			UserSession session)
		{
			try 
			{
				SessionBL.Instance.SaveSession(clientHostName,
					cacheReference,
					session);
			} 
			catch (Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving Session.", ex);
			}
		}

		
		private ISessionService getService(string uri)
		{
			try
			{
				return (ISessionService)Activator.GetObject(typeof(ISessionService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		/// <summary>
		/// Return the session count from which ever has more.
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public Int32 GetSessionCount(Int32 communityID)
		{
			Int32 thisCount = GetLocalSessionCount(communityID);
			Int32 partnerCount = Constants.NULL_INT;

			string checkCurrentReplicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(SERVICE_CONSTANT, SERVICE_MANAGER_NAME, _machineName);
			if (checkCurrentReplicationURI == null)
			{
				return thisCount;
			}
			
			// It's possible for this value to be null. Disabled service instance. No value for replication override.
			if (_replicationURI.Length == 0
				|| checkCurrentReplicationURI.Length == 0)
			{
				return thisCount;
			}
				
			// If session SA override is set to itself(local machine) then it's going to be infinite loop!. Avoid it.
			if (_replicationURI.IndexOf(_machineName) > -1 || !SessionReplicationFlag)
			{
				return thisCount;
			}
 
			#region Code get retrive SessionCount from partner
			bool isSessionCountUsingDB = IsSessionCountUsingDB();
			try
			{
				if (isSessionCountUsingDB)
				{
					//10272009 TL - retrieve partner count from database
					int cacheIntervalSeconds = GetSessionCountUsingDBInterval() / 1000;
					partnerCount = SessionBL.Instance.GetPartnerSessionCount(_replicationURI, communityID, Convert.ToInt32(_serviceInstanceConfig.PartitionOffset), cacheIntervalSeconds);
				}
				else
				{
					//kb923614
					partnerCount = getService(_replicationURI).GetLocalSessionCount(communityID);	
				}
				
				System.Diagnostics.Trace.WriteLine("__SessionSM.GetSessionCount: PartnerSessionCount: " + partnerCount + ", isSessionCountUsingDB: " + isSessionCountUsingDB.ToString());
			}
			catch(Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry(SERVICE_NAME, "GetSessionCount(): Query for Replication partner, " + _replicationURI + " is not available for Session Count. isSessionCountUsingDB: " + isSessionCountUsingDB.ToString() + ". " + ex.ToString(),
					System.Diagnostics.EventLogEntryType.Warning);
			}

/*
			endDateTime = DateTime.Now;
			duration = endDateTime - startDateTime;

			System.Diagnostics.EventLog.WriteEntry(SERVICE_NAME, "Time took " + duration.Seconds.ToString() + " seconds.");
*/
			#endregion
			
			if (thisCount > partnerCount)
			{
				return thisCount;
			}
			else
			{
				return partnerCount;
			}
		}

		public Int32 GetLocalSessionCount(Int32 communityID)
		{
			Int32 count = 0;

			_communityCounterLock.AcquireReaderLock(-1);
			try
			{
				PerformanceCounter counter = _communityCounters[communityID] as PerformanceCounter;
				if (counter != null)
				{
					count = (Int32)counter.RawValue;
				}
			}
			catch (Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure getting session count.", ex);
			}	
			finally
			{
				_communityCounterLock.ReleaseLock();
			}

			return count;
		}

        public List<SessionTracking> GetLastFewSessionTrackingEntries(int memberId, int siteId)
        {
            try
            {
                return SessionBL.Instance.GetLastFewSessionTrackingEntries(memberId, siteId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Error getting last few session tracking entries", ex);
            }
        }

	    public void SaveSessionDetails(UserSession session, int siteID, SessionType sessionType)
		{
			try 
			{
				if (sessionType == SessionType.Visitor)
				{
					SessionBL.Instance.SaveVisitorSessionDetails(session, siteID);
				}
				else
				{
					SessionBL.Instance.SaveMemberSessionDetails(session);
				}
			} 
			catch (Exception ex) 
			{
				System.Diagnostics.EventLog.WriteEntry(SERVICE_NAME, "Error saving the session details for tracking " + ex.ToString(),
					System.Diagnostics.EventLogEntryType.Warning);
				//throw new ServiceBoundaryException(SERVICE_NAME, "Error saving the session details for tracking", ex);
			}
		}

		public Int32 GetNextSessionTrackingID()
		{
			try 
			{
				return SessionBL.Instance.GetNextSessionTrackingID();
			} 
			catch (Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Error getting the next SessionTrackingID", ex);
			}
		}

		private bool IsSessionCountUsingDB()
		{
			string isSessionCountUsingDB = "false";
			try
			{
				isSessionCountUsingDB = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SESSION_COUNT_USING_DB");
			}
			catch(Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry(SERVICE_NAME, "IsSessionCountUsingDB(): Error retrieving ENABLE_SESSION_COUNT_USING_DB setting. " + ex.ToString(),
				System.Diagnostics.EventLogEntryType.Warning);
			}

			if (isSessionCountUsingDB.ToLower().Trim() == "true")
				return true;
			else
				return false;
		}

		private int GetSessionCountUsingDBInterval()
		{
			int intervalMilliseconds = 300000; //default to 5 minutes

			try
			{
				intervalMilliseconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INTERVAL_SESSION_COUNT_USING_DB"));
			}
			catch (Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry(SERVICE_NAME, "GetSessionCountUsingDBInterval(): Error retrieving INTERVAL_SESSION_COUNT_USING_DB setting. " + ex.ToString(),
					System.Diagnostics.EventLogEntryType.Warning);
			}

			return intervalMilliseconds;
		}

		private void PersistSessionCount()
		{
			int communityID = 0;
			int sessionCount = 0;
			PerformanceCounter counter = null;
			bool initialRun = true;
			bool isSessionCountUsingDB = IsSessionCountUsingDB();
			string communitiesList=Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSION_COMMUNITY_LIST");
			string[] communities= communitiesList.Split(new char[]{','});
			
			while (true)
			{
				if (isSessionCountUsingDB)
				{
					if (initialRun)
					{
						Thread.Sleep(10000); //wait 10 seconds before we start updating count to db
					}
				
					try
					{
						_communityCounterLock.AcquireReaderLock(-1);
						foreach (string cID in communities)
						{
							communityID = Convert.ToInt32(cID);
							counter = _communityCounters[communityID] as PerformanceCounter;
							sessionCount = 0;
							if (counter != null)
							{
								sessionCount = (Int32)counter.RawValue;
							}

							SessionBL.Instance.UpdateSessionCount(_machineName, Convert.ToInt32(_serviceInstanceConfig.PartitionOffset), _serviceInstanceConfig.Port, communityID, sessionCount);
							System.Diagnostics.Trace.WriteLine("__SessionSM.PersistSessionCount: machineName: " + _machineName + ", community: " + communityID.ToString() + ", sessionCount: " + sessionCount.ToString());
						}
					}
					catch (Exception ex)
					{
						System.Diagnostics.EventLog.WriteEntry(SERVICE_NAME, "SessionSM.PersistSessionCount error. communityID: " + communityID.ToString() + ", sessionCount: " + sessionCount.ToString() + ". " + ex.ToString(),
							System.Diagnostics.EventLogEntryType.Warning);
					}
					finally
					{
						_communityCounterLock.ReleaseReaderLock();
					}

					initialRun = false;
				}

				isSessionCountUsingDB = IsSessionCountUsingDB();
				Thread.Sleep(GetSessionCountUsingDBInterval());
			}
			
		}

		private void sessionBL_ReplicationRequested(IReplicable replicableObject)
		{
			_replicator.Enqueue(replicableObject);
		}


		private void sessionBL_SynchronizationRequested(string key, Hashtable cacheReferences)
		{
			_snychronizer.Enqueue(key, cacheReferences);
		}

		
		public void Receive(IReplicable replicableObject)
		{
			try
			{
				SessionBL.Instance.CacheReplicatedObject(replicableObject);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure processing replication object.", ex);
			}
		}


		//temporary hack, counts seem to be off for undetermined reason
		public void FixCommunityCounts()
		{
			Int32[] counts = SessionBL.Instance.GetCommunityCounts();
			Int32 total = 0;

			for (Int32 i = 0; i < counts.Length; i++)
			{
				if (counts[i] > 0)
				{
					(_communityCounters[i] as PerformanceCounter).RawValue = counts[i];
					total = total + counts[i];
				}
			}

			_perfCount.RawValue = total;
		}

		public bool SessionReplicationFlag
		{
			get
			{
				try
				{
					return Conversion.CBool(RuntimeSettings.GetSetting("SESSION_REPLICATION_FLAG"));
				}
				catch(Exception ex)
				{
					return true;
				}
			}
		}
		#region instrumentation
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("Sessions", "Sessions", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("Session Requests/second", "Session Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Session Hit Rate", "Session Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Session Hit Rate_Base","Session Hit Rate_Base", PerformanceCounterType.RawBase),
														 new CounterCreationData("Session Saves/second", "Session Saves/second", PerformanceCounterType.RateOfCountsPerSecond32),
			});
			PerformanceCounterCategory.Create(SERVICE_NAME, SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(SERVICE_NAME);
		}


		private void initPerfCounters()
		{
			_perfCount = new PerformanceCounter(SERVICE_NAME, "Sessions", "_Total", false);
			_perfRequestsSec = new PerformanceCounter(SERVICE_NAME, "Session Requests/second", "_Total", false);
			_perfHitRate = new PerformanceCounter(SERVICE_NAME, "Session Hit Rate", "_Total", false);
			_perfHitRate_Base = new PerformanceCounter(SERVICE_NAME, "Session Hit Rate_Base", "_Total", false);
			_perfSavesSec = new PerformanceCounter(SERVICE_NAME, "Session Saves/second", "_Total", false);

			string communitiesList=Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSION_COMMUNITY_LIST");
			System.Diagnostics.Debug.WriteLine("SESSION_COMMUNITY_LIST: " + communitiesList);
			string[] communities= communitiesList.Split(new char[]{','});
			//byte[] communities = new byte[]{1,2,3,8,9,10,12,17};
			System.Diagnostics.Debug.WriteLine("SESSION_COMMUNITY_LIST split: " + communities.Length.ToString());
			foreach (string communityID in communities)
			{System.Diagnostics.Debug.WriteLine("SESSION_COMMUNITY_LIST adding: " + communityID);
				PerformanceCounter counter = new PerformanceCounter(SERVICE_NAME, "Sessions", "Community" + communityID.ToString(), false);
				_communityCounters.Add(communityID, counter);
			}

			resetPerfCounters();
		}


		private void resetPerfCounters()
		{
			_perfCount.RawValue = 0;
			_perfRequestsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
			_perfSavesSec.RawValue = 0;

			_communityCounterLock.AcquireWriterLock(-1);
			try
			{
				IDictionaryEnumerator de = _communityCounters.GetEnumerator();

				while (de.MoveNext())
				{
					((PerformanceCounter)de.Value).RawValue = 0;
				}
			}
			finally
			{
				_communityCounterLock.ReleaseLock();
			}
		}


		private void sessionBL_SessionRequested(bool cacheHit)
		{
			_perfRequestsSec.Increment();
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
		}


		private void sessionBL_SessionAdded(Int32 communityID)
		{
			updateSessionCount(communityID, true);
		}

		
		private void sessionBL_SessionRemoved(Int32 communityID)
		{
			updateSessionCount(communityID, false);
		}


		private void updateSessionCount(Int32 communityID,
			bool increment)
		{
			PerformanceCounter counter;

			if (increment)
			{
				_perfCount.Increment();
			}
			else
			{
				_perfCount.Decrement();
			}
	
			if (communityID != Constants.NULL_INT)
			{
				_communityCounterLock.AcquireReaderLock(-1);
				try
				{
					counter = _communityCounters[communityID] as PerformanceCounter;
					if (counter == null)
					{
						LockCookie cookie = _communityCounterLock.UpgradeToWriterLock(-1);
						counter = _communityCounters[communityID] as PerformanceCounter;
						if (counter == null)
						{
							counter = new PerformanceCounter(SERVICE_NAME, "Sessions", "Community" + communityID.ToString(), false);
							_communityCounters.Add(communityID, counter);
						}
						_communityCounterLock.DowngradeFromWriterLock(ref cookie);
					}
				}
				finally
				{
					_communityCounterLock.ReleaseLock();
				}

				if (increment)
				{
					counter.Increment();
				}
				else
				{
					counter.Decrement();
				}
			}
		}


		private void sessionBL_SessionSaved()
		{
			_perfSavesSec.Increment();
		}
		#endregion

		public void Dispose()
		{
			if (_hydraWriter != null)
			{
				_hydraWriter.Stop();
			}

			if (_replicator != null)
			{
				_replicator.Stop();
			}

			if (_snychronizer != null)
			{
				_snychronizer.Stop();
			}

			resetPerfCounters();
		}
	}
}
