using System;

using Matchnet;


namespace Matchnet.Session.ServiceAdapters
{
	internal class SessionCount : ICacheable
	{
		private Int32 _communityID;
		private Int32 _count;

		public SessionCount(Int32 communityID,
			Int32 count)
		{
			_communityID = communityID;
			_count = count;
		}


		public Int32 Count
		{
			get
			{
				return _count;
			}
		}


		public int CacheTTLSeconds
		{
			get
			{
				return 10;
			}
			set
			{
			}
		}


		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return Matchnet.CacheItemPriorityLevel.Normal;
			}
			set
			{
			}
		}


		public string GetCacheKey()
		{
			return SessionCount.GetCacheKey(_communityID);
		}


		public static string GetCacheKey(Int32 communityID)
		{
			return "SessionCount" + communityID.ToString();
		}

	}
}
