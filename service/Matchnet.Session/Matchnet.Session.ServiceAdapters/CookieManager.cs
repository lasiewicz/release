using System;
using System.Web;

using Matchnet.Exceptions;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Session.ServiceAdapters
{
	internal class CookieManager
	{
		public static readonly CookieManager Instance = new CookieManager();

		// encoded with a version so that we can change
		// the cookie format and not have side affects
		// when deploying.
		public const string MATCHNET_VALUE_COOKIE = "mnc5";
		public const string MATCHNET_COOKIE_GUID_NAME = "sid";

		private CookieManager()
		{
		}

		/// <summary>
		/// Obtains a value by name that exists in the Matchnet cookie.
		/// If the provided name does not exist in the cookie collection,
		/// string.Empty is returned.
		/// </summary>
		public string this[string name]
		{
			get 
			{
				return this[MATCHNET_VALUE_COOKIE, name];
			}
		}

		/// <summary>
		/// Obtains a value by name in the cookie collection specified
		/// by the provided cookie name. If the provided name does not
		/// exist in the cookie colelction, string.Empty is returned.
		/// </summary>
		public string this[string cookieName, string name] 
		{
			get 
			{
				if (cookieName == string.Empty || name == string.Empty) 
				{
					return string.Empty;
				}

				HttpCookie cookie = HttpContext.Current.Request.Cookies[cookieName];

				if (cookie != null) 
				{
					string val = cookie.Values[name];

					if (val != string.Empty) 
					{
						return HttpUtility.UrlDecode(val, System.Text.Encoding.ASCII);
					}
				}
				return string.Empty;
			}
		}

		/// <summary>
		/// The session key obtained from the Matchnet cookie. If this is
		/// not present, Guid.Empty is returned.
		/// </summary>
		public Guid SessionKey 
		{
			get 
			{
				string key = this[MATCHNET_COOKIE_GUID_NAME] as string;
				
				if (key.Length > 0)
				{
					return new Guid(key);
				}

				return Guid.Empty;
			} 
		}

		/// <summary>
		/// Adds a cookie value to the Matchnet cookie.
		/// </summary>
		/// <param name="name">The name of the cookie value.</param>
		/// <param name="val">The value of the cookie.</param>
		/// <param name="expiration">When the cookie should expire</param>
		public void Add(string name, string val, DateTime expiration) 
		{
			Add(name, val, expiration, false);
		}

		/// <summary>
		/// Adds a cookie value to the Matchnet cookie.
		/// </summary>
		/// <param name="name">The name of the cookie value.</param>
		/// <param name="val">The value of the cookie.</param>
		/// <param name="expiration">When the cookie should expire.</param>
		/// <param name="clear">Whether or not to clear all the cookies values before setting this value.</param>
		public void Add(string name, string val, DateTime expiration, bool clear) 
		{
			Add(MATCHNET_VALUE_COOKIE, name, val, expiration, clear);
		}

		/// <summary>
		/// Adds a cookie value to the specified cookie.
		/// </summary>
		/// <param name="cookieName">The name of the cookie.</param>
		/// <param name="name">The name of the cookie value.</param>
		/// <param name="val">The value of the cookie.</param>
		/// <param name="expiration">When the cookie should expire.</param>
		public void Add(string cookieName, string name, string val, DateTime expiration) 
		{
			Add(cookieName, name, val, expiration, false);
		}

		/// <summary>
		/// Adds a cookie value to the specified cookie.
		/// </summary>
		/// <param name="cookieName">The name of the cookie.</param>
		/// <param name="name">The name of the cookie value.</param>
		/// <param name="val">The value of the cookie.</param>
		/// <param name="expiration">When the cookie should expire.</param>
		/// <param name="clear">Whether or not to clear all the cookies value before setting this value.</param>
		public void Add(string cookieName, string name, string val, DateTime expiration, bool clear) 
		{
			try 
			{
				HttpCookie cookie = HttpContext.Current.Response.Cookies[cookieName];

				if (clear) 
				{
					cookie.Values.Clear();
				}
				cookie.Values.Add(name, val);
				cookie.Expires = expiration;
			} 
			catch (Exception ex) 
			{
				throw new SAException("Unable to clear cookies.", ex);
			}
		}

		/// <summary>
		/// Clears all the values of the Matchnet cookie.
		/// </summary>
		public void Clear() 
		{
			Clear(MATCHNET_VALUE_COOKIE);
		}

		/// <summary>
		/// Clears all the values of the specified cookie.
		/// </summary>
		/// <param name="cookieName">The name of the cookie to clear values for.</param>
		public void Clear(string cookieName) 
		{
			try 
			{
				HttpContext.Current.Request.Cookies.Remove(cookieName);
			} 
			catch (Exception ex) 
			{
				throw new SAException("Unable to clear cookie.", ex);
			}
		}
	}
}