using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Web;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Helpers;
using Matchnet.RemotingClient;
using Matchnet.Configuration;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Session.ServiceDefinitions;
using Matchnet.Session.ValueObjects;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Session.ValueObjects.Queue;
using Matchnet.Session.ValueObjects.ServiceDefinitions;
using Spark.RabbitMQ.Client;

namespace Matchnet.Session.ServiceAdapters
{
	/// <summary>
	/// Session service adapter
	/// </summary>
    public class SessionSA : SABase, IDisposable, ISessionSA
	{
		private const string SERVICE_MANAGER_NAME = "SessionSM";
		private const string SERVICE_CONSTANT = "SESSION_SVC";
		private Cache _cache;
		private const string MATCHNET_COOKIE_GUID_NAME = "sid";
		private Thread _saveThread;
		private const string PERSISTENTEXPLICIT_NAME = "objname";
		private const string PERSISTENTEXPLICIT_VALUE = "val";
		private const string PERSISTENTEXPLICIT_EXPIRATION_DAYS = "days";
		private const string PERSISTENTEXPLICIT_EXPIRATION_DATE = "dateExp";
		private const string PERSISTENTEXPLICIT_IS_SLIDING = "sliding";
		private const string PERSISTENTEXPLICIT_HASH = "hash";
	    private static Publisher _publisher = new Publisher();
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly SessionSA Instance = new SessionSA();
        private static IHttpContextHelper _httpContextHelper = null;

        public static IHttpContextHelper HttpContextHelper
        {
            get { return _httpContextHelper ?? (_httpContextHelper = new HttpContextHelper()); }
            set { _httpContextHelper = value; }
        }

		private SessionSA()
		{
			_cache = Cache.Instance;
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_SA_CONNECTION_LIMIT"));
		}


        /// <summary>
        /// Used only by the REST API for mobile sites
        /// </summary>
        /// <param name="brandID">Required for keeping community counts</param>
        /// <returns></returns>
		public UserSession RESTCreateSession(int brandID, int memberID, string emailAddress, int genderMask, string sessionId)
        {
            //EventLog.WriteEntry("REST", "RESTCreateSession called. " + brandID, EventLogEntryType.Information);
            try
            {
                if(string.IsNullOrEmpty(sessionId))
                {
                    sessionId = Guid.NewGuid().ToString();
                }

                //create a new session
                var session = new UserSession(new Guid(sessionId));
                session.Add("BrandId", brandID.ToString(), SessionPropertyLifetime.Persistent);
                session.Add("MemberID", memberID.ToString(), SessionPropertyLifetime.Temporary);
                session.Add("EmailAddress", emailAddress, SessionPropertyLifetime.Persistent);
                session.Add("GenderMask", Convert.ToString(genderMask), SessionPropertyLifetime.Temporary);
                session.Add("HideMask", "0", SessionPropertyLifetime.Temporary);
                session.Add("SelfSuspendedFlag", "0", SessionPropertyLifetime.Temporary);
                session.Add("IsPayingMemberFlag", "0", SessionPropertyLifetime.Temporary);

                session.CacheTTLSeconds = Convert.ToInt32(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_CACHE_TTL"));

                saveSession(session);

                return session;
            }
            catch (Exception ex)
            {
                throw new SAException("Error GettingSessionServiceOnly", ex);
            }
        }
        
        /// <summary>
        /// Used only by the REST API for mobile sites
        /// </summary>
        /// <param name="brandID">Required for keeping community counts</param>
        /// <returns></returns>
		public UserSession RESTCreateSession(int brandID, int memberID, string emailAddress, int genderMask)
        {
            return RESTCreateSession(brandID, memberID, emailAddress, genderMask, string.Empty);
        }

        public UserSession RESTGetSession(String key)
        {
           // EventLog.WriteEntry("REST", "RESTGetSession called. " + key, EventLogEntryType.Information);
            Guid lookupKey;
            UserSession session = null;

            try
            {
                lookupKey = new Guid(key);

                session = getSessionBackup(lookupKey);

                return session;
            }
            catch (Exception ex)
            {
                throw new SAException("Unable to get session from key:" + key, ex);
            }
        }

        /// <summary>
        /// This call is implemented for Spark.REST for mobile sites ONLY.
        /// TODO: Add other logics found in other calls like async, > than 5 minutes.
        /// </summary>
        public void RESTSaveSession(UserSession session)
        {
            try
            {
             //   EventLog.WriteEntry("REST", "RESTSaveSession called. " + session.Properties.Count,EventLogEntryType.Information);

                saveSession(session);
            }
            catch (Exception ex)
            {
                throw new SAException("REST error saving session", ex);
            }
        }


	    /// <summary>
		/// Retreives user session based on cookie.
		/// </summary>
		/// <param name="isDevMode">Alternate cookie name is used when true</param>
		/// <returns>UserSession object</returns>
		public UserSession GetSession(bool isDevMode)
		{
			// Going to set the CultureInfo for en-US for the reading and writing to session cookie
			// in order to avoid datetime culture issues
			// CultureInfo will be set back to its original value in the end of this method
			System.Globalization.CultureInfo tmpCI =  Thread.CurrentThread.CurrentCulture;
			try
			{
				Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

				//HttpCookie cookie = getCookie(isDevMode); 
				HttpCookie cookie = this.getPrimarySessionCookie(isDevMode);//primary session cookie that contains SID
				Guid key = Guid.Empty;
				UserSession session = null;

				if (cookie != null)
				{
					string keyVal = cookie[MATCHNET_COOKIE_GUID_NAME];
					if (keyVal != string.Empty && keyVal != null)
					{
						key = new Guid(keyVal);
					}
				}

				//look for existing session
				if (key != Guid.Empty)
				{
					//retreive session from local cache
					session = _cache.Get(UserSession.GetCacheKey(key)) as UserSession;

					//retreive session backup from service
                    if (session == null)
                    {
                        session = getSessionBackup(key);
                    }
                    else
                    {
                        //System.Diagnostics.Trace.WriteLine("SessionTest: Getting cached[" + key + "]");
                    }
				}

				if (session == null)
				{
					//create a new session
					session = new UserSession(Guid.NewGuid());
					session.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_CACHE_TTL"));
				}

				HttpContextBase context = getContext();

				

				if (!(context.Items.Contains(UserSession.COOKIE_UPDATED_KEY)) && cookie != null)
				{
					//add primary cookie values
					foreach (string name in cookie.Values)
					{
						if (name != MATCHNET_COOKIE_GUID_NAME && name != null)
						{
							session.Add(name, HttpUtility.UrlDecode(cookie.Values[name]), SessionPropertyLifetime.Persistent);
						}
					}

					//Get secondary cookies
					HttpCookie[] secondaryCookies = this.getSecondarySessionCookies(isDevMode);
					if (secondaryCookies != null)
					{
						//add secondary cookie values
						foreach (HttpCookie sCookie in secondaryCookies)
						{
							try
							{
								bool isSliding = Convert.ToBoolean(sCookie.Values[SessionSA.PERSISTENTEXPLICIT_IS_SLIDING]);
								int expirationDays = Convert.ToInt32(sCookie.Values[SessionSA.PERSISTENTEXPLICIT_EXPIRATION_DAYS]);
								DateTime expirationDate = (isSliding) ? DateTime.Now.AddDays(expirationDays) : Convert.ToDateTime(HttpUtility.UrlDecode(sCookie.Values[SessionSA.PERSISTENTEXPLICIT_EXPIRATION_DATE]));

								session.Add(sCookie.Values[SessionSA.PERSISTENTEXPLICIT_NAME], HttpUtility.UrlDecode(sCookie.Values[SessionSA.PERSISTENTEXPLICIT_VALUE]), expirationDays, expirationDate, isSliding, HttpUtility.UrlDecode(sCookie.Values[SessionSA.PERSISTENTEXPLICIT_HASH]));
							}
							catch(Exception ex)
							{
								System.Diagnostics.Trace.WriteLine(ex.Message);
							}
						}
					}

				}

				_cache.Insert(session);

				return session;
			}
			catch (Exception ex) 
			{
				throw new SAException("Unable to get session.", ex);
			}
			finally
			{
				Thread.CurrentThread.CurrentCulture = tmpCI;
			}
		}


		
		public UserSession GetSession(String key)
		{
			Guid lookupKey;
			UserSession session=null;
			try
			{
				lookupKey = new Guid(key);
			

				session = _cache.Get(UserSession.GetCacheKey(lookupKey)) as UserSession;
                if (session == null)
                {
                    session = getSessionBackup(lookupKey);
                }
                else
                {
                    //System.Diagnostics.Trace.WriteLine("SessionTest: Getting cached[" + key + "]");
                }

                _cache.Insert(session);

				return session;
			}
			catch(Exception ex)
			{
				throw new SAException("Unable to get session from key:" + key, ex);
			}
		}
		/// <summary>
		/// Returns the MemberID for a given session if one exists.
		/// </summary>
		/// <param name="pSessionGUID"></param>
		/// <returns></returns>
		public int GetMemberID(string pSessionGUID)
		{
			int memberID = Constants.NULL_INT;
			Guid lookupKey;
			UserSession session = null;

			try
			{
				lookupKey = new Guid(pSessionGUID);
			

				session = _cache.Get(UserSession.GetCacheKey(lookupKey)) as UserSession;
				if (session == null)
				{
					session = getSessionBackup(lookupKey);
				}

				if (session != null)
				{
					memberID = Convert.ToInt32(session.GetInt("MemberID"));
				}
				return memberID;
			}
			catch (Exception ex)
			{
				throw new SAException("Unable to get MemberID from sesion", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public Int32 GetSessionCount(Int32 communityID)
		{
			string uri = "";
			Int32 count = 0;
			ArrayList offsets = new ArrayList();

			try
			{
				SessionCount sessionCount = _cache[SessionCount.GetCacheKey(communityID)] as SessionCount;

				if (sessionCount == null)
				{
					ServicePartition[] servicePartitions = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetAll(SERVICE_CONSTANT);
					string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_SA_HOST_OVERRIDE");

					for (Int32 partitionNum = 0; partitionNum < servicePartitions.Length; partitionNum++)
					{
						Int32 offset = servicePartitions[partitionNum].PartitionOffset;
						if (!offsets.Contains(offset))
						{
							offsets.Add(offset);

							uri = servicePartitions[partitionNum].ToUri(SERVICE_MANAGER_NAME);

							if (overrideHostName.Length > 0)
							{
								UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
								uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
							}

							base.Checkout(uri);
							try
							{
								count = count + getService(uri).GetSessionCount(communityID);

								//do not double-count if override is set
								if (overrideHostName.Length > 0)
								{
									break;
								}
							}
							finally
							{
								base.Checkin(uri);
							}
						}
					}

					sessionCount = new SessionCount(communityID, count);
					_cache.Add(sessionCount);
				}

				return sessionCount.Count;
			}
			catch (Exception ex) 
			{
				throw new SAException("Unable to get session count (uri = " + uri.ToString() + ").", ex);
			}
		}

		
		/// <summary>
		/// Saves user session and writes cookie.
		/// </summary>
		/// <param name="session">UserSession</param>
		/// <param name="domainName">Top-level domain name that cookie is accesible from (i.e. jdate.com)</param>
		/// <param name="isDevMode">Alternate cookie name is used when true</param>
		public void SaveSession(UserSession session, string domainName, bool isDevMode, bool async)
		{
			// Going to set the CultureInfo for en-US for the reading and writing to session cookie
			// in order to avoid datetime culture issues
			// CultureInfo will be set back to its original value in the end of this method
			System.Globalization.CultureInfo tmpCI =  Thread.CurrentThread.CurrentCulture;
			try
			{
				Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
			
                Hashtable properties = new Hashtable(session.Properties);

                //Hashtable properties = session.Properties;
				HttpContextBase context = getContext();

				//clear primary cookie
				string cookieName = GetCookieName(isDevMode);
				context.Response.Cookies[cookieName].Values.Clear();
				context.Response.Cookies[cookieName][MATCHNET_COOKIE_GUID_NAME] = session.Key.ToString();

				//expire secondary cookies
				foreach (string cookieKey in context.Response.Cookies.Keys)
				{
					if (cookieKey.StartsWith(cookieName + "_"))
						context.Response.Cookies[cookieKey].Expires = DateTime.Now.AddDays(-2);
				}

				// expire next year - primary cookie
				context.Response.Cookies[cookieName].Expires = DateTime.Now.AddYears(1);

				// set the domain of the cookie - primary cookie
				if (domainName != Constants.NULL_STRING) 
				{
					if (!domainName.StartsWith(".")) 
					{
						domainName = "." + domainName;
					}
					context.Response.Cookies[cookieName].Domain = domainName;
				}

				//write to cookie
				ArrayList expiredSessionItems = new ArrayList();
				IDictionaryEnumerator e = properties.GetEnumerator();
				while (e.MoveNext())
				{
					SessionProperty property = (SessionProperty)e.Value;

					//write primary cookie
					if (property.Lifetime == SessionPropertyLifetime.Persistent)
					{
						context.Response.Cookies[cookieName][property.Name] = HttpUtility.UrlEncode(property.Val.ToString());
					}//write secondary cookies
					else if (property.Lifetime == SessionPropertyLifetime.PersistentExplicit)
					{
						string secondaryCookieName = cookieName + "_" + property.Name;
						context.Response.Cookies[secondaryCookieName].Name = secondaryCookieName;
						context.Response.Cookies[secondaryCookieName].Expires = property.ExpirationDate;
						context.Response.Cookies[secondaryCookieName][PERSISTENTEXPLICIT_NAME] = property.Name;
						context.Response.Cookies[secondaryCookieName][PERSISTENTEXPLICIT_IS_SLIDING] = property.isSlidingExpiration.ToString();
						context.Response.Cookies[secondaryCookieName][PERSISTENTEXPLICIT_VALUE] = HttpUtility.UrlEncode(property.Val.ToString());
						context.Response.Cookies[secondaryCookieName][PERSISTENTEXPLICIT_EXPIRATION_DAYS] = property.ExpirationDays.ToString();
						context.Response.Cookies[secondaryCookieName][PERSISTENTEXPLICIT_EXPIRATION_DATE] = HttpUtility.UrlEncode(property.ExpirationDate.ToString());
						context.Response.Cookies[secondaryCookieName][PERSISTENTEXPLICIT_HASH] = HttpUtility.UrlEncode(property.Hash);

						if (domainName != Constants.NULL_STRING) 
						{
							context.Response.Cookies[secondaryCookieName].Domain = domainName;
						}

						if (property.ExpirationDate < DateTime.Now)
							expiredSessionItems.Add(property.Name);
					}

				}

				//remove expired session items, if any, for PersistentExplicit
				foreach (string sExpItem in expiredSessionItems)
				{
					session.Remove(sExpItem);
				}

				//save session to service
			    if (!session.IsDirty && !(DateTime.Now.Subtract(session.LastSaveTime).TotalMinutes >= 5)) return;
			    session.IsDirty = false;
			    session.SetLastSaveTime();

			    if (async)
			    {
			        // Use RabbitMQ to save asnchronously. This replaces the old mechanism of having a long running thread
			        // with an in memory queue
			        var sessionSave = new SessionSave
			        {
			            ClientHostName = Environment.MachineName,
			            CacheReference = Evaluator.Instance.GetReference(DateTime.MaxValue),
			            UserSession = session
			        };
			        _publisher.Publish(sessionSave);
			    }
			    else
			    {
			        saveSession(session);
			    }
			}
			catch (Exception ex) 
			{
				throw new SAException("Unable to get session.", ex);
			}
			finally
			{
				Thread.CurrentThread.CurrentCulture = tmpCI;
			}
		}

	    public void SaveSession(UserSession session, string domainName, bool isDevMode)
		{
			SaveSession(session, domainName, isDevMode, true);
		}

        public List<SessionTracking> GetLastFewSessionTrackingEntries(int memberId, int siteId)
        {
            string uri = getServiceManagerUri();
            base.Checkout(uri);
            try
            {
                return getService(uri).GetLastFewSessionTrackingEntries(memberId, siteId);
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Unable to get last few session tracking entries (uri = " + uri.ToString() + ").", ex);
            }
            finally
            {
                base.Checkin(uri);
            }
        }

	    public void SaveSessionDetails(UserSession session, int siteID, SessionType sessionType)
		{
			string uri = getServiceManagerUri(session.Key);
			base.Checkout(uri);
			try
			{
				getService(uri).SaveSessionDetails(session, siteID, sessionType);
			}
			finally
			{
				base.Checkin(uri);
			}
		}

		public Int32 GetNextSessionTrackingID(UserSession session)
		{
			string uri = getServiceManagerUri(session.Key);
			base.Checkout(uri);
			try
			{
				return getService(uri).GetNextSessionTrackingID();
			}
			finally
			{
				base.Checkin(uri);
			}
		}

		private HttpContextBase getContext()
		{
		    HttpContextBase currentHttpContextBase = HttpContextHelper.CurrentHttpContextBase;
            if (currentHttpContextBase == null)
			{
				throw new Exception("HttpContext unavailable, SessionSA must run under a web context.");
			}

            return currentHttpContextBase;
		}


		public string GetCookieName(bool isDevMode)
		{
			// encoded with a version so that we can change
			// the cookie format and not have side effects
			// when deploying.
			string cookieName = "mnc5";

			// Dev version cookie - This should use the same setting as stg, but don't want to break any existing functionality.
			if (isDevMode)
			{
				cookieName = "dev" + cookieName;
				return cookieName;
			}

			// Stage version cookie
			try
			{
				if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENVIRONMENT_TYPE").IndexOf("stg") > -1)
				{
					return "stg" + cookieName;
				}
			}
			catch(Exception)
			{
				// If the setting is not found, return the default value, "mnc5"
				return cookieName;
			}

			return cookieName;
		}

		/// <summary>
		/// Gets the Primary Session Cookie, which contains the SID
		/// </summary>
		/// <param name="isDevMode"></param>
		/// <returns></returns>
		private HttpCookie getPrimarySessionCookie(bool isDevMode)
		{
			return getContext().Request.Cookies[GetCookieName(isDevMode)];
		}


		/// <summary>
		/// Gets all Secondary Session Cookies, used for PersistentExplicit session objects
		/// </summary>
		/// <param name="isDevMode"></param>
		/// <returns></returns>
		private HttpCookie[] getSecondarySessionCookies(bool isDevMode)
		{
			ArrayList cookieList = new ArrayList();

			//secondary session cookie names
			string cookieName = GetCookieName(isDevMode) + "_";

			//get secondary session cookies
			foreach (string cookieKey in getContext().Request.Cookies.Keys)
			{
				if (cookieKey.StartsWith(cookieName))
					cookieList.Add(getContext().Request.Cookies[cookieKey]);
			}

			//convert list to array of HttpCookie items
			HttpCookie[] cookieArray;
			if (cookieList.Count > 0)
			{
				cookieArray = new HttpCookie[cookieList.Count];
				int cookieCount = 0;
				foreach (HttpCookie cookie in cookieList)
				{
					cookieArray[cookieCount] = cookie;
					cookieCount++;
				}
			}
			else
				cookieArray = null;

			return cookieArray;
		}


		private UserSession getSessionBackup(Guid key)
		{
			string uri = getServiceManagerUri(key);
			base.Checkout(uri);
			try
			{
                //System.Diagnostics.Trace.WriteLine("SessionTest: Getting new [" + key + "] from " + uri);

				return getService(uri).GetSession(System.Environment.MachineName,
					Evaluator.Instance.GetReference(DateTime.MaxValue),
					key);
			}
			finally
			{
				base.Checkin(uri);
			}
		}


		private void saveSession(UserSession session)
		{
			string uri = getServiceManagerUri(session.Key);
			base.Checkout(uri);
			try
			{
				getService(uri).SaveSession(System.Environment.MachineName,
					Evaluator.Instance.GetReference(DateTime.MaxValue),
					session);
			}
			finally
			{
				base.Checkin(uri);
			}
		}

		private ISessionService getService(string uri)
		{
			try
			{
				return (ISessionService)Activator.GetObject(typeof(ISessionService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}


		private string getServiceManagerUri(Guid sessionKey)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, sessionKey).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SESSIONSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}


		#region IDisposable Members

		/// <summary>
		/// 
		/// </summary>
		public void Dispose()
		{
			// TODO:  Add SessionSA.Dispose implementation
		}

		#endregion
	}
}
