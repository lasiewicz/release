using System;
using System.Collections;

using Matchnet;

namespace Matchnet.Session.ValueObjects
{
	/// <summary>
	/// A strongly type collection for holding SessionProperties.
	/// </summary>
	[Serializable]
	public class SessionPropertyCollection : CollectionBase, IValueObject
	{
		/// <summary>
		/// Retrieves a SessionProperty by index.
		/// </summary>
		public SessionProperty this[int index]
		{
			get{ return (SessionProperty)base.InnerList[index]; }
		}

		/// <summary>
		/// <p>
		/// Adds a SessionProperty to the Collection of
		/// SessionProperties.
		/// </p>
		/// <p>
		/// If a SessionProperty with the same name already
		/// exists, this operation will overwrite it.
		/// </p>
		/// </summary>
		/// <param name="sessionProperty"></param>
		/// <returns></returns>
		public int Add(SessionProperty sessionProperty)
		{
			if (base.InnerList.Contains(sessionProperty)) 
			{
				base.InnerList.Remove(sessionProperty);
			}
			return base.InnerList.Add(sessionProperty);
		}

		/// <summary>
		/// Removes a SessionProperty from the Collection
		/// of SessionProperties.
		/// </summary>
		/// <param name="sessionProperty">The SessionProperty to remove.</param>
		public void Remove(SessionProperty sessionProperty) 
		{
			base.InnerList.Remove(sessionProperty);
		}

		/// <summary>
		/// <p>
		/// Returns the SessionProperty Value for the SessionProperty
		/// with the provided name.
		/// </p>
		/// <p>
		/// If no SessionProperty exists with the given name, 
		/// null is returned
		/// </p>
		/// </summary>
		public object this[string name] 
		{
			get 
			{
				foreach (SessionProperty property in this) 
				{
					if (property.Name.ToLower() == name.ToLower()) 
					{
						return property.Value;
					}
				}
				return null;
			}
		}
	}
}
