using System;

namespace Matchnet.Session.ValueObjects
{
	/// <summary>
	/// Summary description for PartnerCount.
	/// </summary>
	public class PartnerCount : ICacheable, IValueObject
	{
		private Int32 _communityID;
		private Int32 _count;
		private int _CacheTTLSeconds = 300; //defaults to 5 minutes; may be override in BL based on setting

		public PartnerCount(Int32 communityID,
			Int32 count)
		{
			_communityID = communityID;
			_count = count;
		}


		public Int32 Count
		{
			get
			{
				return _count;
			}
		}


		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}


		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return Matchnet.CacheItemPriorityLevel.Normal;
			}
			set
			{
			}
		}


		public string GetCacheKey()
		{
			return PartnerCount.GetCacheKey(_communityID);
		}


		public static string GetCacheKey(Int32 communityID)
		{
			return "PartnerCount" + communityID.ToString();
		}
	}
}
