﻿#region

using System;
using Matchnet.CacheSynchronization.ValueObjects;

#endregion

namespace Matchnet.Session.ValueObjects.Queue
{
    /// <summary>
    ///     Use for saving session. Created for use with AMQP to be able to pass around a single request object.
    ///     Saving session the old way requires passing in three parameter to the BL.
    /// </summary>
    [Serializable]
    public class SessionSave : Message
    {
        /// <summary>
        /// </summary>
        public UserSession UserSession { get; set; }

        /// <summary>
        /// </summary>
        public string ClientHostName { get; set; }

        /// <summary>
        /// </summary>
        public CacheReference CacheReference { get; set; }
    }
}