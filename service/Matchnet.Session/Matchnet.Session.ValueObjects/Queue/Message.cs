﻿#region

#endregion

namespace Matchnet.Session.ValueObjects.Queue
{
    /// <summary>
    ///     Base class for handling any type of AMQP queue session messages.
    ///     This limits the amount of queues that need to be created on the server.
    /// </summary>
    public abstract class Message
    {
    }
}