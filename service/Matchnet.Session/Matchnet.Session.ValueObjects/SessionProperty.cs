using System;
using System.Security.Cryptography;
using System.Text;

namespace Matchnet.Session.ValueObjects
{
	/// <summary>
	/// Represents the lifetime options of session properties
	/// </summary>
	[Serializable]
	public enum SessionPropertyLifetime
	{
		/// <summary>
		/// Value is lost when session times out or is explicitly cleared
		/// </summary>
		Temporary,

		/// <summary>
		/// Value is persisted in user's cookie, must be of type string
		/// </summary>
		Persistent,

		/// <summary>
		/// Value is lost when session times out but will survive and explicit session clear
		/// </summary>
		TemporaryDurable,

		/// <summary>
		/// Value is persisted in user's cookie for an explicit amount of time, must be of type string
		/// </summary>
		PersistentExplicit
	}


	/// <summary>
	/// Represents a user session name-value pair
	/// </summary>
	[Serializable]
	public class SessionProperty
	{
		private SessionPropertyLifetime _lifetime;
		private string _name;
		private object _val;
		private DateTime _expirationDate; 				//used for PersistentExplicit
		private int _expirationDays;					//used for PersistentExplicit
		private bool _isSlidingExpiration;				//used for PersistentExplicit

        /// <summary>
        ///     Default constructor for supporting JSON serialization. Added specifically for RabbitMQ.
        /// </summary>
	    public SessionProperty()
	    {
	        
	    }

		/// <summary>
		/// SessionProperty constructor
		/// </summary>
		/// <param name="lifetime"></param>
		/// <param name="name"></param>
		/// <param name="val"></param>
		public SessionProperty(SessionPropertyLifetime lifetime,
			string name,
			object val)
		{
			_lifetime = lifetime;
			_name = name;
			_val = val;
			_expirationDate = new DateTime(1900, 1, 1);
			_expirationDays = -1;
			_isSlidingExpiration = true;

		}

		/// <summary>
		/// SessionProperty constructor
		/// </summary>
		/// <param name="lifetime"></param>
		/// <param name="name"></param>
		/// <param name="val"></param>
		/// <param name="expirationDays"></param>
		/// <param name="expirationDate"></param>
		/// <param name="isSlidingExpiration"></param>
		public SessionProperty(SessionPropertyLifetime lifetime,
			string name,
			object val,
			int expirationDays,
			DateTime expirationDate,
			bool isSlidingExpiration)
		{
			_lifetime = lifetime;
			_name = name;
			_val = val;
			_expirationDays = expirationDays;
			_isSlidingExpiration = isSlidingExpiration;
			_expirationDate = expirationDate;
		}


		/// <summary>
		/// SessionPropertyLifetime defines the type of session mode
		/// </summary>
		public SessionPropertyLifetime Lifetime
		{
			get{ return _lifetime;}
			set{ _lifetime = value;}
		}


		/// <summary>
		/// SessionProperty name
		/// </summary>
		public string Name
		{
			get{ return _name;}
			set{ _name = value;}
		}


		/// <summary>
		/// SessionProperty value
		/// </summary>
		public object Val
		{
			get { return _val;}
			set { _val = value;}
		}

		/// <summary>
		/// SessionProperty Days to persist (only valid for PersistentExplicit lifetime)
		/// </summary>
		public int ExpirationDays
		{
			get { return _expirationDays;}
			set { _expirationDays = value;}
		}

		/// <summary>
		/// SessionProperty Expiration Date (only valid for PersistentExplicit lifetime)
		/// </summary>
		public DateTime ExpirationDate
		{
			get { return _expirationDate;}
			set { _expirationDate = value;}
		}

		/// <summary>
		/// SessionProperty to determine whether expiration is fixed or sliding (only valid for PersistentExplicit lifetime)
		/// </summary>
		public bool isSlidingExpiration
		{
			get { return _isSlidingExpiration;}
			set { _isSlidingExpiration = value;}
		}

		/// <summary>
		/// Returns a string representing a hash code of the session property;
		/// Included in session cookies for PersistentExplicity lifetime to prevent tampering.
		/// </summary>
		public string Hash
		{
			get 
			{
				string saltValue = "twsjkmto";
				StringBuilder sessionPropertyText = new StringBuilder();
				sessionPropertyText.Append(this.ExpirationDays.ToString() + this.Val.ToString() + this.ExpirationDate.ToShortDateString() + this.isSlidingExpiration.ToString());
				sessionPropertyText.Append(saltValue);

				//Convert text into a byte array.
				byte[] sessionPropertyTextBytes = Encoding.ASCII.GetBytes(sessionPropertyText.ToString());

				//Generate Hash
				HashAlgorithm hash = new MD5CryptoServiceProvider();
				byte[] hashBytes = hash.ComputeHash(sessionPropertyTextBytes);
				
				return Convert.ToBase64String(hashBytes);
				
			}
		}
	}
}
