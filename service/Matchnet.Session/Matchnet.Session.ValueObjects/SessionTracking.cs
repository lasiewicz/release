﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Session.ValueObjects
{
    [Serializable]
    public class SessionTracking
    {
        public DateTime SessionStart { get; set; }
        public string ClientIP { get; set; }
    }
}
