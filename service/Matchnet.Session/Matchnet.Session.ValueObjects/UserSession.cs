using System;
using System.Collections;
using System.Threading;

using Matchnet;
using Matchnet.CacheSynchronization.Tracking;


namespace Matchnet.Session.ValueObjects
{
	/// <summary>
	/// Member or visitor session type
	/// </summary>
	[Serializable]
	public enum SessionType
	{
		/// <summary>
		/// The visitor has not registered or logged in
		/// </summary>
		Visitor,

		/// <summary>
		/// The user has already registered
		/// </summary>
		Member
	}

	/// <summary>
	/// Represents a users's session
	/// </summary>
	[Serializable]
	public class UserSession : IReplicable, IValueObject
	{
		private int _cacheTTLSeconds = 1200; // 20 minutes
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Default;
		private Guid _key;
		private bool _isDirty = true;
		private Hashtable _properties;
		public static string COOKIE_UPDATED_KEY = "COOKIE_UPDATED_KEY";

		[NonSerialized]
		private ReaderWriterLock _lock = new ReaderWriterLock();
		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		[NonSerialized]
		private DateTime _lastSaveTime = DateTime.Now;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="key">Session key</param>
		public UserSession(Guid key)
		{
			_key = key;
			_properties = new Hashtable();
		}


		/// <summary>
		/// Clears all session properties that are of lifetime Temporary.
		/// </summary>
		public void Clear()
		{
			ArrayList removeList = new ArrayList();	

			Lock.AcquireWriterLock(-1);
			try
			{
				IDictionaryEnumerator e = _properties.GetEnumerator();
				while (e.MoveNext())
				{
					SessionProperty property = (SessionProperty)e.Value;
					if (property.Lifetime == SessionPropertyLifetime.Temporary)
					{
						removeList.Add(e.Key);
					}
				}

				foreach (string key in removeList)
				{
					Remove(key);
				}
			}
			finally
			{
				Lock.ReleaseLock();
			}
		}

		/// <summary>
		/// Adds a name-value pair to the session
		/// </summary>
		/// <param name="lifetime"></param>
		/// <param name="name"></param>
		/// <param name="val"></param>
		public void Add(string name, object val, SessionPropertyLifetime lifetime) 
		{
			if (lifetime == SessionPropertyLifetime.Persistent && val.GetType() != typeof(string))
			{
				throw new Exception("Session properties with lifetime = Persistent must be of type string. (property name: " + name + ")");
			}
			else if (lifetime == SessionPropertyLifetime.PersistentExplicit)
			{
				throw new Exception("Session properties with lifetime = PersistenExplicity must use the overloaded function that specifies expiration days");
			}

			Lock.AcquireWriterLock(-1);
			try
			{
				SessionProperty property = _properties[name] as SessionProperty;

				if (property != null)
				{
					if (property.Lifetime != lifetime || !property.Val.Equals(val))
					{
						_isDirty = true;
						property.Lifetime = lifetime;
						property.Val = val;
					}
				}
				else
				{
					_isDirty = true;
					_properties.Add(name, new SessionProperty(lifetime, name, val));
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message);
				throw ex;
			}
			finally
			{
				Lock.ReleaseLock();
			}
		}

		/// <summary>
		/// Adds a name-value pair to the session with an explicit expiration in terms of days.
		/// SessionPropertyLifetime will automatically be set to PersistentExplicit.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="val"></param>
		/// <param name="ExpirationDays"></param>
		/// <param name="isSlidingExpiration"></param>
		public void Add(string name, string val, int ExpirationDays, bool isSlidingExpiration) 
		{
			SessionPropertyLifetime lifetime = SessionPropertyLifetime.PersistentExplicit;
			
			try
			{
				SessionProperty property = _properties[name] as SessionProperty;

				if (property != null)
				{
					if (property.Lifetime != lifetime || !property.Val.Equals(val) || property.ExpirationDays != ExpirationDays || property.isSlidingExpiration != isSlidingExpiration)
					{
						//update session
						_isDirty = true;
						property.Lifetime = lifetime;
						property.Name = name;
						property.Val = val;
						property.ExpirationDays = ExpirationDays;
						property.ExpirationDate = DateTime.Now.AddDays(ExpirationDays);
						property.isSlidingExpiration = isSlidingExpiration;
					}
//					else if (property.Lifetime == SessionPropertyLifetime.PersistentExplicit)
//					{
//						//this session is likely coming from UI, always update date
//						_isDirty=true;
//						property.ExpirationDate = DateTime.Now.AddDays(ExpirationDays);
//					}
				}
				else
				{
					_isDirty = true;
					_properties.Add(name, new SessionProperty(lifetime, name, val, ExpirationDays, DateTime.Now.AddDays(ExpirationDays), isSlidingExpiration));
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message);
				throw ex;
			}

		}

		/// <summary>
		/// Adds a name-value pair to the session with an explicit expiration in terms of days and the expiration date.
		/// NOTE: SessionPropertyLifetime will automatically be set to PersistentExplicit.
		/// NOTE: This version of Add is only used when reading values from cookies.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="val"></param>
		/// <param name="ExpirationDays"></param>
		/// <param name="ExpirationDate"></param>
		/// <param name="isSlidingExpiration"></param>
		/// <param name="hash">Hash of SessionProperty used to validate, optional</param>
		public void Add(string name, string val, int ExpirationDays, DateTime ExpirationDate, bool isSlidingExpiration, string hash) 
		{
			SessionPropertyLifetime lifetime = SessionPropertyLifetime.PersistentExplicit;
			
			try
			{
				bool isValidated = true;
				//check hash of SessionProperty
				SessionProperty hashcheckSessionProperty = new SessionProperty(lifetime, name, val, ExpirationDays, ExpirationDate, isSlidingExpiration);
				if (hashcheckSessionProperty.Hash != hash)
					isValidated = false;

				if (isValidated)
				{
					SessionProperty property = _properties[name] as SessionProperty;

					if (property != null)
					{
						//MPR-819, Check to ensure that session Lifetime in memory is still persistentexplicit, otherwise we should not update/add the value
						if (property.Lifetime == SessionPropertyLifetime.PersistentExplicit)
						{
							if (!property.Val.Equals(val) || property.ExpirationDays != ExpirationDays || property.isSlidingExpiration != isSlidingExpiration || property.ExpirationDate != ExpirationDate)
							{
								//update session
								_isDirty = true;
								property.Lifetime = lifetime;
								property.Name = name;
								property.Val = val;
								property.ExpirationDays = ExpirationDays;
								property.ExpirationDate = ExpirationDate;
								property.isSlidingExpiration = isSlidingExpiration;
							}
							else if (property.isSlidingExpiration)
							{
								//update expiration date for sliding session
								_isDirty=true;
								property.ExpirationDate = DateTime.Now.AddDays(ExpirationDays);
							}
						}
					}
					else
					{
						_isDirty = true;
						_properties.Add(name, hashcheckSessionProperty);
					}
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message);
				throw ex;
			}
		}


		/// <summary>
		/// Removes a name-value pair from the session for a given name
		/// </summary>
		/// <param name="name">Value name</param>
		public void Remove(string name)
		{
			bool isLocked = Lock.IsWriterLockHeld;

			if (!isLocked)
			{
				Lock.AcquireWriterLock(-1);
			}
			try
			{
				_isDirty = true;
				_properties.Remove(name);
			}
			finally
			{
				if (!isLocked)
				{
					Lock.ReleaseLock();
				}
			}
		}


		/// <summary>
		/// Retreives a value (as type object) from the session for a given name
		/// </summary>
		/// <param name="name">Value name</param>
		/// <returns>Session value</returns>
		public object Get(string name)
		{
			object val = null;
			
			Lock.AcquireReaderLock(-1);
			try
			{
				SessionProperty property = _properties[name] as SessionProperty;

				if (property != null)
				{
					//determine if object has expired
					if (property.Lifetime != SessionPropertyLifetime.PersistentExplicit || (property.Lifetime == SessionPropertyLifetime.PersistentExplicit && property.ExpirationDate >= DateTime.Now))
					{
						val = property.Val;
					}
				}
			}
			finally
			{
				Lock.ReleaseLock();
			}

			return val;
		}


		/// <summary>
		/// Retreives a value (as type string) from the session for a given name
		/// </summary>
		/// <param name="name">Value name</param>
		/// <returns>Session value</returns>
		public string GetString(string name)
		{
			return GetString(name, Constants.NULL_STRING);
		}

		
		/// <summary>
		/// Retreives a value (as type string) from the session for a given name.
		/// If the value does not exist defaultValue is returned.
		/// </summary>
		/// <param name="name">Value name</param>
		/// <param name="defaultValue">Default value</param>
		/// <returns>Session value</returns>
		public string GetString(string name, string defaultValue)
		{
			object o = Get(name);

			if (o != null)
			{
				return o.ToString();
			}
			else
			{
				return defaultValue;
			}
		}


		/// <summary>
		/// Retreives a value (as type int) from the session for a given name
		/// </summary>
		/// <param name="name">Value name</param>
		/// <returns>Session value</returns>
		public int GetInt(string name)
		{
			return GetInt(name, 0);
		}

		
		/// <summary>
		/// Retreives a value (as type int) from the session for a given name<br />
		/// If the value does not exist defaultValue is returned.
		/// </summary>
		/// <param name="name">Value name</param>
		/// <param name="defaultValue">Default value</param>
		/// <returns>Session value</returns>
		public int GetInt(string name, int defaultValue)
		{
			object o = Get(name);
			int val = defaultValue;

			if (o != null)
			{
				double valDouble;

				if (Double.TryParse(o.ToString(), System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out valDouble))
				{
					val = (int)valDouble;
				}
			}

			return val;
		}


		/// <summary>
		/// Session key
		/// </summary>
		public Guid Key 
		{
			get {
				return _key;
			}
		}


		/// <summary>
		/// Collection of all session name-value pairs
		/// </summary>
		public Hashtable Properties
		{
			get
			{
				return _properties;
			}
		}


		/// <summary>
		/// Indicates if data has been modified and not yet saved
		/// </summary>
		public bool IsDirty
		{
			get
			{
				return _isDirty;
			}
			set
			{
				_isDirty = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime LastSaveTime
		{
			get
			{
				return _lastSaveTime;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public void SetLastSaveTime()
		{
			_lastSaveTime = DateTime.Now;
		}


		internal ReaderWriterLock Lock
		{
			get
			{
				if (_lock == null)
				{
					_lock = new ReaderWriterLock();
				}
				return _lock;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string this[string name]
		{
			get
			{
				string retval;
				try
				{
					retval = this.GetString(name);
				}
				catch
				{
					return "";
				}
				if (retval != null)
					return retval;
				else
					return "";
			}
		}
	
		/// <summary>
		/// TODO: Needs to be eventually removed
		/// </summary>
		public string DalKey
		{
			get
			{
				return "0";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		#region ICacheable
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return UserSession.GetCacheKey(_key);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static string GetCacheKey(Guid key)
		{
			return "~SESSION^" + key.ToString();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());				
		}
		#endregion
		
		#region Temporary Stuff for SessionSA integration
		
		//This will be removed when we can compile the web project without it.

		/// <summary>
		/// Temporary.  
		/// </summary>
		[Obsolete("Use Key as unique identifier.")]
		public string SessionID
		{
			get
			{
				return "";
			}
		}


		#endregion
	}
}
