﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Session.ValueObjects.ServiceDefinitions
{
    public interface ISessionSA
    {
        UserSession RESTCreateSession(int brandID, int memberID, string emailAddress, int genderMask);
        UserSession RESTGetSession(String key);
        void RESTSaveSession(UserSession session);
        UserSession GetSession(bool isDevMode);
        UserSession GetSession(String key);
        int GetMemberID(string pSessionGUID);
        Int32 GetSessionCount(Int32 communityID);
        void SaveSession(UserSession session, string domainName, bool isDevMode, bool async);
        void SaveSession(UserSession session, string domainName, bool isDevMode);
        void SaveSessionDetails(UserSession session, int siteID, SessionType sessionType);
        Int32 GetNextSessionTrackingID(UserSession session);
        string GetCookieName(bool isDevMode);
    }
}
