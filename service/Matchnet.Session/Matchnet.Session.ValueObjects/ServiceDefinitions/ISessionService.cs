using System;
using System.Collections.Generic;
using Matchnet.Session.ValueObjects;
using Matchnet.CacheSynchronization.ValueObjects;

namespace Matchnet.Session.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface ISessionService
	{
		void FixCommunityCounts();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="sessionKey"></param>
		/// <returns></returns>
		UserSession GetSession(string clientHostName,
			CacheReference cacheReference,
			Guid sessionKey);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="session"></param>		
		void SaveSession(string clientHostName,
			UserSession session);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="session"></param>
		void SaveSession(string clientHostName,
			CacheReference cacheReference,
			UserSession session);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		Int32 GetSessionCount(Int32 communityID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		Int32 GetLocalSessionCount(Int32 communityID);

        /// <summary>
        /// Retrieves last few session tracking entries (5 for now)
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
	    List<SessionTracking> GetLastFewSessionTrackingEntries(int memberId, int siteId);

		/// <summary>
		/// Save user session details for tracking
		/// </summary>
		/// <param name="session"></param>		
		/// <param name="sessionType"></param>		
		void SaveSessionDetails(UserSession session, int siteID, SessionType sessionType);

		/// <summary>
		/// Get the next user session ID
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		Int32 GetNextSessionTrackingID();
	}
}
