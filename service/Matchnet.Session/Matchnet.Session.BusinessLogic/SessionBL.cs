using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Caching;
using System.Data.SqlClient;
using System.Data;
using log4net;
using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.Session.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.CacheSynchronization.Tracking;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
// Wonder if adding this namespace will resolve PromoVO missing from deployments?
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Session.ValueObjects.Queue;
using Spark.ActivityRecording.Processor.ServiceAdapter;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.RabbitMQ.Client;

namespace Matchnet.Session.BusinessLogic
{
	public class SessionBL
	{
		public static readonly SessionBL Instance = new SessionBL();

		private const int GC_THRESHOLD = 10000;

		private const string VISITOR_SESSIONDETAILS_SAVED = "VisitorSessionDetailsSaved";
		private const string MEMBER_SESSIONDETAILS_SAVED = "MemberSessionDetailsSaved";
		private const string SESSION_MEMBERID = "MemberID";

		private Matchnet.Caching.Cache _cache;
		private CacheItemRemovedCallback _expireSession;
		private string _expiredCountLock = "";
		private int _expiredCount = 0;

		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;

		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;

		public delegate void SessionRequestEventHandler(bool cacheHit);
		public event SessionRequestEventHandler SessionRequested;

		public delegate void SessionAddEventHandler(Int32 communityID);
		public event SessionAddEventHandler SessionAdded;

		public delegate void SessionRemoveEventHandler(Int32 communityID);
		public event SessionRemoveEventHandler SessionRemoved;

		public delegate void SessionSaveEventHandler();
		public event SessionSaveEventHandler SessionSaved;

        /// <summary>
        ///     RabbitMQ subscriber
        /// </summary>
	    private Subscriber _subscriber;

        private static readonly ILog Log = LogManager.GetLogger(typeof(SessionBL));

		private SessionBL()
		{
			_cache = Caching.Cache.Instance;
			_expireSession = expireCallback;
		}

        /// <summary>
        ///     Triggered when the service starts
        /// </summary>
        public void Start()
        {
            // Create a new bus to the RabbitMQ queue
            _subscriber = new Subscriber();

            // Start subscribing with a lambda expression
            _subscriber.Subscribe<SessionSave>("SessionSave",
                message => SaveSession(message.ClientHostName, message.CacheReference, message.UserSession));

            Log.InfoFormat("Subscribing, {0}, to RabbitMq Host, {1}.", "SessionSave", _subscriber.HostName);
        }

        /// <summary>
        ///     Triggered when the service stops
        /// </summary>
        public void Stop()
        {
            if (_subscriber != null)
            {
                // Must properly dispose per EasyNetQ's author
                _subscriber.Dispose();
            }
        }

		public UserSession GetSession(string clientHostName,
			CacheReference cacheReference,
			Guid key) 
		{
			try 
			{
				UserSession session = _cache.Get(UserSession.GetCacheKey(key)) as UserSession;
				if (session != null)
				{
					if (clientHostName != null && cacheReference != null)
					{
						//System.Diagnostics.Trace.WriteLine("__AddReference(" + clientHostName + ", " + session.Key + ", " + cacheReference.Address + ")");
						session.ReferenceTracker.Add(clientHostName, cacheReference);
					}
					else
					{
						//System.Diagnostics.Trace.WriteLine("__NoReference(" + clientHostName + ", " + session.Key + ")");
					}
					
					SessionRequested(true);
				}
				else
				{
					SessionRequested(false);
				}
				return session;
			} 
			catch (Exception ex) 
			{
				throw new BLException("Unable to retrieve Session.", ex);
			}
		}

	    /// <summary>
	    /// </summary>
	    /// <param name="clientHostName"></param>
	    /// <param name="cacheReference"></param>
	    /// <param name="session"></param>
	    public void SaveSession(string clientHostName,
	        CacheReference cacheReference,
	        UserSession session)
	    {
	        try
	        {
	            if (cacheReference != null)
	            {
	                session.ReferenceTracker.Add(clientHostName, cacheReference);
	            }

	            var existingSession = _cache.Get(session.GetCacheKey()) as UserSession;
	            if (existingSession != null)
	            {
	                existingSession.ReferenceTracker.Remove(clientHostName);
	            }

	            insertSession(session);

	            SessionSaved();
	        }
	        catch (Exception ex)
	        {
	            throw new BLException("Unable to save Session.", ex);
	        }
	    }

		private void insertSession(UserSession session)
		{
			insertSession(session, true);
		}

		private void insertSession(UserSession session,
			bool replicate)
		{
			_cache.Insert(session, _expireSession);

			if (replicate)
			{
				ReplicationRequested(session);
			}

			SessionAdded(getCommunityID(session));
		}

        public List<SessionTracking> GetLastFewSessionTrackingEntries(int memberId, int siteId)
        {
            Command command = null;
            SqlDataReader reader = null;
            try
            {
                command = new Command("mnActivityRecording", "dbo.up_SessionTracking_Get_LastFew", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteId);

                reader = Client.Instance.ExecuteReader(command);
                var ret = new List<SessionTracking>();
                while(reader.Read())
                {
                    ret.Add(new SessionTracking()
                    {
                        SessionStart = reader["SessionStart"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["SessionStart"]),
                        ClientIP = reader["ClientIP"] == DBNull.Value ? string.Empty : reader["ClientIP"].ToString()
                    });
                }

                return ret;
            }
            catch (Exception ex)
            {
                throw new BLException("Exception in GetLastFewSessionTrackingEntries ExMessage: " + ex.Message, ex);
            }
            finally
            {
                if(reader != null)
                    reader.Close();

                command = null;
            }
   
        }

		public void SaveVisitorSessionDetails(UserSession session, int siteID)
		{
			try
			{
			    var activity = new UserSessionActivity()
			    {
                    SessionActivityType = SessionActivityType.ForInsert,

                    SessionTrackingId = session.GetInt("SessionTrackingID"),
                    SessionKey =  session.Key.ToString(),
                    SiteId = siteID,
                    SessionStart = DateTime.Now,
                    PromotionId = session.GetOptionInt("PromotionID"),
                    LuggageId = session.GetStringOrDefault("Luggage"),
                    BannerId = session.GetOptionInt("BannerID_Tracking"),
                    ClientIp = session.GetString("ClientIP"),
                    ReferrerUrl = session.GetStringOrDefault("ReferralURL"),
                    LandingPageId = session.GetOptionInt("LandingPageID"),
                    LandingPageTestId = session.GetOptionInt("LandingPageTestID")

			    };
                ActivityRecordingProcessorAdapter.Instance.RecordActivity(activity);                
			}
			catch(Exception ex)
			{
				throw new BLException("Unable to save visitor session details for tracking, SessionKey:" + session.Key.ToString() + ", SessionTrackingID:" + Convert.ToString(((SessionProperty)session.Properties["SessionTrackingID"]).Val) + ". " + ex.Message, ex);
			}
		}

		public void SaveMemberSessionDetails(UserSession session)
		{
			try
			{
			    var activity = new UserSessionActivity()
			    {
                    SessionActivityType = SessionActivityType.ForUpdate,

                    SessionTrackingId = session.GetInt("SessionTrackingID"),
                    MemberId = session.GetInt("MemberID"),
                    RegistrationDate = session.GetOptionDateTime("RegistrationDate"),
                    UpdateDate = DateTime.Now
			    };
                ActivityRecordingProcessorAdapter.Instance.RecordActivity(activity);                
			}
			catch(Exception ex)
			{
				if (session.Properties["RegistrationDate"] != null)
				{
					throw new BLException("Unable to udpate member session details for tracking, SessionKey:" + session.Key.ToString() + ", SessionTrackingID:" + Convert.ToString(((SessionProperty)session.Properties["SessionTrackingID"]).Val) + ", RegistrationDate:" + Convert.ToString(((SessionProperty)session.Properties["RegistrationDate"]).Val) + ". " + ex.Message, ex);
				}
				else
				{
					throw new BLException("Unable to udpate member session details for tracking, SessionKey:" + session.Key.ToString() + ", SessionTrackingID:" + Convert.ToString(((SessionProperty)session.Properties["SessionTrackingID"]).Val) + ". " + ex.Message, ex);
				}
			}
		}

		public int GetNextSessionTrackingID()
		{
			int primarykey = Constants.NULL_INT;
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnKey", "dbo.up_PrimaryKey_Select", 0);
				command.AddParameter("@KeyName", SqlDbType.VarChar, ParameterDirection.Input, "SessionTrackingID");
				dataReader = Client.Instance.ExecuteReader(command);

				if (dataReader.Read())
				{
					primarykey = dataReader.GetInt32(dataReader.GetOrdinal("PKID"));
				}

				return primarykey;
			}
			catch (Exception exception)
			{
				throw new BLException("SessionBL.GetSessionTrackingID error. Unable to get the next SessionTrackingID. " + exception.Message, exception);
			}
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
		}

		private void expireCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			if (value.GetType().Name == "UserSession")
			{
				UserSession userSession = value as UserSession;
				SessionRemoved(getCommunityID(userSession));

				SynchronizationRequested(userSession.GetCacheKey(),
					userSession.ReferenceTracker.Purge(Constants.NULL_STRING));
			}
			incrementExpirationCounter();
		}


		public Int32[] GetCommunityCounts()
		{
			Matchnet.Caching.Cache cache = Matchnet.Caching.Cache.Instance;
			Int32[] counts = new Int32[20];

            IDictionaryEnumerator de = cache.GetEnumerator() as IDictionaryEnumerator;

			while (de.MoveNext())
			{
				UserSession session = de.Value as UserSession;

				if (session != null)
				{
					Int32 communityID = getCommunityID(session);
					if (communityID != Constants.NULL_INT)
					{
						counts[communityID]++;
					}
				}
			}

			return counts;
		}

		public int GetPartnerSessionCount(string partnerURI, int communityID, int partitionOffset, int cacheTTLSeconds)
		{
			
			int partnerCount = 0;
			DateTime partnerCountLastUpdatedDate = DateTime.MinValue;
			SqlDataReader dataReader = null;
			try
			{
				PartnerCount partnerSessionCount = _cache[PartnerCount.GetCacheKey(communityID)] as PartnerCount;

				if (partnerSessionCount == null)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(partnerURI));

					Command command = new Command("mnSessionCounter", "dbo.up_SessionCount_Get", 0);
					command.AddParameter("@ServerHostName", SqlDbType.NVarChar, ParameterDirection.Input, uriBuilder.Host);
					command.AddParameter("@PartitionOffset", SqlDbType.Int, ParameterDirection.Input, partitionOffset);
					command.AddParameter("@PartitionPort", SqlDbType.Int, ParameterDirection.Input, uriBuilder.Port);
					command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
					dataReader = Client.Instance.ExecuteReader(command);

					while (dataReader.Read())
					{
						partnerCount = dataReader.IsDBNull(dataReader.GetOrdinal("SessionCount")) ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("SessionCount"));
						partnerCountLastUpdatedDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
						TimeSpan differenceLastUpdate = DateTime.Now.Subtract(partnerCountLastUpdatedDate);
						if (partnerCount < 0 || (Math.Abs(differenceLastUpdate.TotalMinutes) > 60))
						{
							partnerCount = 0;
						}
						break;
					}

					if (partnerCount > 0)
					{
						partnerSessionCount = new PartnerCount(communityID, partnerCount);
						partnerSessionCount.CacheTTLSeconds = cacheTTLSeconds;
						_cache.Add(partnerSessionCount);
					}
				}
				else
				{
					partnerCount = partnerSessionCount.Count;
				}

			}
			catch(Exception ex)
			{
				throw new BLException("SessionBL.GetPartnerSessionCount error. Unable to retrieve SessionCount. CommunityID:" + communityID.ToString() + ". " + ex.Message, ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
			return partnerCount;
		}

		public int UpdateSessionCount(string serverHostName, int partitionOffset, int partitionPort, int communityID, int sessionCount)
		{
			int partnerCount = 0;
			Command command = null;
			try
			{
				command = new Command("mnSessionCounter", "dbo.up_SessionCount_Update", 0);
				command.AddParameter("@ServerHostName", SqlDbType.NVarChar, ParameterDirection.Input, serverHostName);
				command.AddParameter("@PartitionOffset", SqlDbType.Int, ParameterDirection.Input, partitionOffset);
				command.AddParameter("@PartitionPort", SqlDbType.Int, ParameterDirection.Input, partitionPort);
				command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
				command.AddParameter("@SessionCount", SqlDbType.Int, ParameterDirection.Input, sessionCount);
				
				Client.Instance.ExecuteAsyncWrite(command);

			}
			catch(Exception ex)
			{
				throw new BLException("Unable to update SessionCount. CommunityID:" + communityID.ToString() + ", serverHostName:" + serverHostName + ", partitionOffset: " + partitionOffset.ToString() + ", partitionPort: " + partitionPort.ToString() + ", SessionCount: " + sessionCount + ". " + ex.Message, ex);
			}
			finally
			{
				command = null;
			}

			return partnerCount;
		}

		
		private Int32 getCommunityID(UserSession session)
		{
			Int32 communityID = Constants.NULL_INT;
			Brand brand = BrandConfigSA.Instance.GetBrandByID(session.GetInt("BrandID", Constants.NULL_INT));

			if (brand != null)
			{
				communityID = brand.Site.Community.CommunityID;
			}

			return communityID;
		}

		
		private void incrementExpirationCounter()
		{
			lock (_expiredCountLock)
			{
				_expiredCount++;

				if (_expiredCount > GC_THRESHOLD)
				{
					_expiredCount = 0;
					GC.Collect();
				}
			}
		}

		
		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "UserSession":
					//System.Diagnostics.Trace.WriteLine("__CacheReplicatedObject(" + ((UserSession)replicableObject).Key.ToString() + ")");
					insertSession((UserSession)replicableObject, false);
					break;

				default:
					throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
			}
		}

	}
}
