﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Session.ValueObjects;

namespace Matchnet.Session.BusinessLogic
{
    public static class UserSessionExtensions
    {
        public static int GetInt(this UserSession session, string key)
        {
            var val = session.Properties[key];
            return Convert.ToInt32(((SessionProperty)val).Val);
        }

        public static int? GetOptionInt(this UserSession session, string key)
        {
            var val = session.Properties[key];
            if (val == null)
                return null;
            return Convert.ToInt32(((SessionProperty)val).Val);
        }

        public static DateTime? GetOptionDateTime(this UserSession session, string key)
        {
            var val = session.Properties["RegistrationDate"];
            if (val == null)
                return null;

            string registrationDate = String.Format("{0:u}", ((SessionProperty)val).Val);
            return Convert.ToDateTime(registrationDate);
        }


        public static string GetString(this UserSession session, string key)
        {
            var val = session.Properties[key];
            return Convert.ToString(((SessionProperty)val).Val);
        }

        public static string GetStringOrDefault(this UserSession session, string key)
        {
            var val = session.Properties[key];
            if (val == null)
                return null;
            return Convert.ToString(((SessionProperty)val).Val);
        }
    }
}
