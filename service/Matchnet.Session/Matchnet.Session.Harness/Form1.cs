using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Session.ValueObjects;
using Matchnet.Session.ServiceDefinitions;


namespace Matchnet.Session.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button btnGetSessionCounts;
		private System.Windows.Forms.TextBox txtURI;
		private System.Windows.Forms.Label lblCount;
		private System.Windows.Forms.TextBox txtCommunityID;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
			txtURI.Text="tcp://172.16.1.101:44000/SessionSM.rem";
			txtCommunityID.Text="3";
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.btnGetSessionCounts = new System.Windows.Forms.Button();
			this.txtURI = new System.Windows.Forms.TextBox();
			this.lblCount = new System.Windows.Forms.Label();
			this.txtCommunityID = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(240, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Fix Community Counts";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnGetSessionCounts
			// 
			this.btnGetSessionCounts.Location = new System.Drawing.Point(24, 208);
			this.btnGetSessionCounts.Name = "btnGetSessionCounts";
			this.btnGetSessionCounts.Size = new System.Drawing.Size(232, 23);
			this.btnGetSessionCounts.TabIndex = 1;
			this.btnGetSessionCounts.Text = "Get Sessions Count";
			this.btnGetSessionCounts.Click += new System.EventHandler(this.btnGetSessionCounts_Click);
			// 
			// txtURI
			// 
			this.txtURI.Location = new System.Drawing.Point(24, 112);
			this.txtURI.Name = "txtURI";
			this.txtURI.Size = new System.Drawing.Size(392, 20);
			this.txtURI.TabIndex = 2;
			this.txtURI.Text = "";
			// 
			// lblCount
			// 
			this.lblCount.Location = new System.Drawing.Point(24, 184);
			this.lblCount.Name = "lblCount";
			this.lblCount.Size = new System.Drawing.Size(136, 16);
			this.lblCount.TabIndex = 3;
			// 
			// txtCommunityID
			// 
			this.txtCommunityID.Location = new System.Drawing.Point(24, 144);
			this.txtCommunityID.Name = "txtCommunityID";
			this.txtCommunityID.Size = new System.Drawing.Size(72, 20);
			this.txtCommunityID.TabIndex = 4;
			this.txtCommunityID.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(432, 273);
			this.Controls.Add(this.txtCommunityID);
			this.Controls.Add(this.lblCount);
			this.Controls.Add(this.txtURI);
			this.Controls.Add(this.btnGetSessionCounts);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			ISessionService sessionService = (ISessionService)Activator.GetObject(typeof(ISessionService), "tcp://172.16.1.101:44000/SessionSM.rem");
			//ISessionService sessionService = (ISessionService)Activator.GetObject(typeof(ISessionService), "tcp://devapp01:44000/SessionSM.rem");
			sessionService.FixCommunityCounts();
			sessionService = (ISessionService)Activator.GetObject(typeof(ISessionService), "tcp://172.16.1.102:44000/SessionSM.rem");
			sessionService.FixCommunityCounts();
			sessionService = (ISessionService)Activator.GetObject(typeof(ISessionService), "tcp://172.16.1.103:44000/SessionSM.rem");
			sessionService.FixCommunityCounts();
			sessionService = (ISessionService)Activator.GetObject(typeof(ISessionService), "tcp://172.16.1.104:44000/SessionSM.rem");
			sessionService.FixCommunityCounts();
			Console.WriteLine("done");
		}

		private void btnGetSessionCounts_Click(object sender, System.EventArgs e)
		{
				try
		 {
			 int communityid=Int32.Parse(txtCommunityID.Text);
			 ISessionService sessionService = (ISessionService)Activator.GetObject(typeof(ISessionService), txtURI.Text);
			
			 lblCount.Text=sessionService.GetLocalSessionCount(communityid).ToString();
		 }catch(Exception ex)
				{MessageBox.Show(ex.ToString());}
		
		}
	}
}
