using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Session.ServiceManagers;


namespace Matchnet.Session.Service
{
	public class SessionService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private SessionSM _SessionSM;

		public SessionService()
		{
			InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
		}

		static void Main()
		{
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);

			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new SessionService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

        private static void OnUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            if (ex != null)
            {
                System.Diagnostics.EventLog.WriteEntry(SessionSM.SERVICE_NAME, 
                    "Unhandled exception:" + ex.ToString(),
                    System.Diagnostics.EventLogEntryType.Error);
            }
        }

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = SessionSM.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(SessionSM.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		protected override void RegisterServiceManagers()
		{
			try
			{
				_SessionSM = new SessionSM();

				base.RegisterServiceManager(_SessionSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(SessionSM.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}

				if (_SessionSM != null)
				{
					_SessionSM.Dispose();
				}
			}
			base.Dispose( disposing );
		}

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            _SessionSM.Start();
        }

        protected override void OnStop()
        {
            base.OnStop();
            _SessionSM.Stop();
        }
	}
}
