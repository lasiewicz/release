using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.ApproveQueue.ServiceManagers;


namespace Matchnet.ApproveQueue.Service
{
	public class ApproveQueueService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private DBApproveQueueSM _dbApproveQueueSM;

		private System.ComponentModel.Container components = null;

		public ApproveQueueService()
		{
			InitializeComponent();
		}

		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ApproveQueueService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		protected override void RegisterServiceManagers()
		{
			try
			{
                _dbApproveQueueSM = new DBApproveQueueSM();
                base.RegisterServiceManager(_dbApproveQueueSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
			}
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

            if (_dbApproveQueueSM != null)
            {
                _dbApproveQueueSM.Dispose();
            }

			base.Dispose( disposing );
		}
	}
}
