﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using NUnit.Framework;

namespace Matchnet.ApproveQueue.Test
{
    internal class DBApproveQueueSATest
    {
        [Test]
        public void SaveMemberTextAttributeApprovalTest()
        {
            var queueItemTexts = new List<QueueItemText>();
            DBApproveQueueSA.Instance.Enqueue(new QueueItemText(3, 103, 1003, 111644799, 2, TextType.Regular));
            queueItemTexts.Add(new QueueItemText(3, 103, 1003, 111644799, 2, TextType.Regular, 5504, "test1"));
            queueItemTexts.Add(new QueueItemText(3, 103, 1003, 111644799, 2, TextType.Regular, 5506, "test2"));

            DBApproveQueueSA.Instance.SaveMemberTextApprovalAttributes(queueItemTexts);
        }

        [Test]
        public void SaveQueueItemTextTest()
        {
            var queueItemText = new QueueItemText(3, 103, 1003, 111644154, 2, TextType.Regular);
            DBApproveQueueSA.Instance.Enqueue(queueItemText);
        }

        [Test]
        public void EnqueueDeQueueMemberFraudReviewTest()
        {
            var queueItemMember = new QueueItemMember(3, 103, 1003, 111644154, 111644154,DateTime.Now);
            
            DBApproveQueueSA.Instance.Enqueue(queueItemMember);

            var queueItems = DBApproveQueueSA.Instance.DequeueMemberBySite(103);
            Assert.IsNotNull(queueItems);
            Assert.IsTrue(queueItems.Where(itemMember => itemMember.MemberID == queueItemMember.MemberID).Count() == 1);
        }

        [Test]
        public void CompleteMemberFraudReviewTest()
        {
            var queueItemMember = new QueueItemMember(3, 103, 1003, 111644313, 111644154, DateTime.Now);
            DBApproveQueueSA.Instance.Enqueue(queueItemMember);

            var queueItems = DBApproveQueueSA.Instance.DequeueMemberBySite(103);
            Assert.IsNotNull(queueItems);
            Assert.IsTrue(queueItems.Where(itemMember => itemMember.MemberID == queueItemMember.MemberID).Count() == 1);

            var members = new List<QueueItemMember>();
            members.Add(queueItemMember);
            DBApproveQueueSA.Instance.CompleteMemberFraudReview(members);

            queueItems = DBApproveQueueSA.Instance.DequeueMemberBySite(103);
            Assert.IsNotNull(queueItems);
            Assert.IsTrue(queueItems.Where(itemMember => itemMember.MemberID == queueItemMember.MemberID).Count() == 0);
        }
    }
}
