﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.ApproveQueue.Test
{
    public class MockSettingsService : ISettingsSA
    {
        private Dictionary<int, Dictionary<string, string>> Settings = new Dictionary<int, Dictionary<string, string>>();
        #region ISettingsSA Members

        public void AddSetting(string key, string value)
        {
            if (!Settings.ContainsKey(0)) Settings.Add(0, new Dictionary<string, string>());
            Settings[0].Add(key, value);
        }

        public void RemoveSetting(string key)
        {
            if (SettingExistsFromSingleton(key, 0, 0))
            {
                Settings[0].Remove(key);
            }
        }

        public List<Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant)
        {
            try
            {
                return Settings[0][constant];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityID, int siteID)
        {
            throw new NotImplementedException();
        }

        public List<Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
