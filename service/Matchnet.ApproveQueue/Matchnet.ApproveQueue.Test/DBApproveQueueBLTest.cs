﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ApproveQueue.BusinessLogic;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using Matchnet.Data.Hydra;
using NUnit.Framework;

namespace Matchnet.ApproveQueue.Test
{
    public class DBApproveQueueBLTest
    {
        private MockSettingsService mockSettingsService = new MockSettingsService();
        private HydraWriter _hydraWriter; 

        [TestFixtureSetUp]
        public void StartUp()
        {
            mockSettingsService.AddSetting("CRX_POPULATE_MEMBER_INFO", "true");
            DBApproveQueueBL.Instance.SettingsService = mockSettingsService;
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
        }

        [SetUp]
        public void SetUp()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnMemberApproval", "mnMemberApprovalWrite", "mnAdmin" });
            _hydraWriter.Start();
        }

        [TearDown]
        public void Teardown()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }

        [Test]
        public void SaveTTAnswerTest()
        {
            TTAnswerMemberProfile prof = new TTAnswerMemberProfile();
            List < TTMemberAttributeText > texts = new List<TTMemberAttributeText>();
            
            prof.TTMemberProfileID = 62;
            prof.Suspend = false;
            prof.WarningContactInfo = true;
            prof.WarningInappropriateContent = true;
            prof.WarningGeneralViolation = true;

            TrainingToolBL.Instance.SaveTTAnswer(prof, texts);
            Assert.Pass();
        }

        [Test]
        public void InsertTTMemberProfilesTest()
        {
            var memberProfiles = new List<TTMemberProfile>();
            memberProfiles.Add(new TTMemberProfile() { BrandID = 1003, MemberID = 111645201 });
            memberProfiles[0].MemberAttributeDates = new List<TTMemberAttributeDate>();
            
            memberProfiles[0].MemberAttributeDates.Add(new TTMemberAttributeDate()
                {
                    FieldValue = DateTime.MinValue + (DateTime.Now.AddDays(10) - DateTime.Now),
                    IsFutureTimespan = false,
                    MemberAttributeName = "LastLogon1"
                });

            memberProfiles[0].MemberAttributeDates.Add(new TTMemberAttributeDate()
            {
                FieldValue = DateTime.MinValue + (DateTime.Now.AddDays(8) - DateTime.Now),
                IsFutureTimespan = false,
                MemberAttributeName = "LastLogon2"
            });
            memberProfiles[0].MemberAttributeDates.Add(new TTMemberAttributeDate()
            {
                FieldValue = DateTime.MinValue + (DateTime.Now.AddDays(4) - DateTime.Now),
                IsFutureTimespan = false,
                MemberAttributeName = "LastLogon3"
            });
            memberProfiles[0].MemberAttributeDates.Add(new TTMemberAttributeDate()
            {
                FieldValue = DateTime.MinValue + (DateTime.Now.AddDays(2) - DateTime.Now),
                IsFutureTimespan = false,
                MemberAttributeName = "LastLogon4"
            });
            memberProfiles[0].MemberAttributeDates.Add(new TTMemberAttributeDate()
            {
                FieldValue = DateTime.MinValue + (DateTime.Now.AddDays(1) - DateTime.Now),
                IsFutureTimespan = true,
                MemberAttributeName = "LastLogon5"
            });

            memberProfiles[0].MemberAtributeInts = new List<TTMemberAttributeInt>();
            memberProfiles[0].MemberAtributeInts.Add(new TTMemberAttributeInt()
                {
                    FieldValue = 0,
                    MemberAttributeName = "IsOnDNE"
                });
            memberProfiles[0].MemberAtributeInts.Add(new TTMemberAttributeInt()
            {
                FieldValue = 0,
                MemberAttributeName = "LastSuspendReasonID"
            });
            TrainingToolBL.Instance.InsertTTMemberProfiles(16, memberProfiles);
        }

        [Test]
        public void SaveMemberTextApprovalAttributeTest()
        {
            var queueItemTexts = new List<QueueItemText>();
            DBApproveQueueBL.Instance.Enqueue(new QueueItemText(3, 103, 1003, 111644154, 2, TextType.Regular));
            queueItemTexts.Add(new QueueItemText(3, 103, 1003, 111644154, 2, TextType.Regular, 5504, "test1"));
            queueItemTexts.Add(new QueueItemText(3, 103, 1003, 111644154, 2, TextType.Regular, 5506, "test2"));

            DBApproveQueueBL.Instance.SaveMemberTextApprovalAttributes(queueItemTexts);
        }

        [Test]
        public void SaveQueueItemTextTest()
        {
            var queueItemText = new QueueItemText(3, 103, 1003, 111644154, 2, TextType.Regular);
            DBApproveQueueBL.Instance.Enqueue(queueItemText);
        }

        [Test]
        public void EnqueueDeQueueMemberFraudReviewTest()
        {
            //var queueItemMember = new QueueItemMember(3, 103, 1003, 111644154, 111644154, DateTime.Now);
            var queueItemMember = new QueueItemMember(3, 105, 1003, 100170101, 111644154, DateTime.Now);
            DBApproveQueueBL.Instance.Enqueue(queueItemMember);

            var queueItems = DBApproveQueueBL.Instance.DequeueMemberBySite(103);
            Assert.IsNotNull(queueItems);
            Assert.IsTrue(queueItems.Where(itemMember => itemMember.MemberID == queueItemMember.MemberID).Count() == 1);
        }

        [Test]
        public void CompleteMemberFraudReviewTest()
        {
            var queueItemMember = new QueueItemMember(3, 103, 1003, 111644313, 111644154, DateTime.Now);
            DBApproveQueueBL.Instance.Enqueue(queueItemMember);

            var queueItems = DBApproveQueueBL.Instance.DequeueMemberBySite(103);
            Assert.IsNotNull(queueItems);
            Assert.IsTrue(queueItems.Where(itemMember => itemMember.MemberID == queueItemMember.MemberID).Count() == 1);

            var members = new List<QueueItemMember>();
            members.Add(queueItemMember);
            DBApproveQueueBL.Instance.CompleteMemberFraudReview(members);

            queueItems = DBApproveQueueBL.Instance.DequeueMemberBySite(103);
            Assert.IsNotNull(queueItems);
            Assert.IsTrue(queueItems.Where(itemMember => itemMember.MemberID == queueItemMember.MemberID).Count() == 0);
        }

        [Test]
        public void SaveQueueItemWithExpandedInfoTest()
        {
            var queueItemText = new QueueItemText(3, 103, 1003, 111644799, 2, TextType.Regular);
            queueItemText.PopulateMemberInfo = true;
            queueItemText.IPAddress = "127.0.0.1";
            queueItemText.ProfileRegionId = 3444721;
            queueItemText.GenderMask = 6;
            queueItemText.MaritalStatus = 2;
            queueItemText.FirstName = "G";
            queueItemText.LastName = "K";
            queueItemText.Email = "jdatetest10@gmail.com";
            queueItemText.OccupationDescription = "Architecture/Interior Design";
            queueItemText.EducationLevel = 128;
            queueItemText.Religion = Constants.NULL_INT;
            queueItemText.Ethnicity = Constants.NULL_INT;
            queueItemText.BillingPhoneNumber = "";
            queueItemText.SubscriptionStatus = "Never Subscribed";
            queueItemText.RegistrationDate = new DateTime(2012, 2, 22, 15, 40, 42);
            DBApproveQueueBL.Instance.Enqueue(queueItemText);
        }

        [Test]
        public void SaveAdminReviewSettingsTest()
        {
            int adminMemberId = 111644313;
            DBApproveQueueBL.Instance.SaveAdminReviewSettings(adminMemberId, null, null, false);
            DBApproveQueueBL.Instance.SaveAdminReviewSettings(adminMemberId, 10, null, true);
            DBApproveQueueBL.Instance.SaveAdminReviewSettings(adminMemberId, null, 20, true);
            DBApproveQueueBL.Instance.SaveAdminReviewSettings(adminMemberId, 10, 20, true);
        }

        [Test]
        public void GetTextToReviewTest()
        {
            var reviewText = DBApproveQueueBL.Instance.GetTextToReview(false,null,null);
            Assert.IsNotNull(reviewText);
        }

        [Test]
        public void GetReviewTextCountTest()
        {
            DBApproveQueueBL.Instance.GetTextApprovalReviewCounts(false);
        }

        [Test]
        public void CompleteAdminTextApprovalReviewTest()
        {
            var reviewText = new ReviewText
            {
                MemberTextApprovalId = 414,
                CommunityId = 3,
                SupervisorId = 111644154,
                SupervisorReviewStatusId = SupervisorReviewStatus.Approved,
                SupervisorNotes = "test"
            };
            DBApproveQueueBL.Instance.CompleteAdminTextApprovalReview(reviewText);
        }

        [Test]
        public void GetCRXSecondReviewTest()
        {
            var reviewText = DBApproveQueueBL.Instance.GetCRXTextForSecondReview();
            Assert.IsNotNull(reviewText);
        }

        [Test]
        public void PopulateOnDemandTextReviewTest()
        {
            DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime endDate = DateTime.Now;
            var settings = new List<AdminReviewSetting>();
            settings.Add(new AdminReviewSetting { AdminMemberId = 111644154 , OnDemandSampleRate = 2});
            settings.Add(new AdminReviewSetting { AdminMemberId = 111644313, OnDemandSampleRate = 100 });
            DBApproveQueueBL.Instance.PopulateOnDemandTextReview(startDate, endDate, null, 111644154, settings);
        }

        [Test]
        public void GetOnDemandReviewQueueCountsTest()
        {
            var reviewCounts = DBApproveQueueBL.Instance.GetOnDemandReviewQueueCounts();
            Assert.IsNotNull(reviewCounts);
        }

        [Test]
        public void GetApprovalHistoryCountsTest()
        {
            var approvalCounts = DBApproveQueueBL.Instance.GetApprovalHistoryCounts(
                DateTime.Now.AddMonths(-2), DateTime.Now, Constants.NULL_INT, 3, Constants.NULL_INT);
            Assert.IsNotNull(approvalCounts);
        }

        [Test]
        public void DequeueMemberProfileDuplicateTest()
        {
            var result = DBApproveQueueBL.Instance.DequeueMemberProfileDuplicateByLanguage(2);
            Assert.IsTrue(result == null || result.QueueItemMemberProfileDuplicateID > 0);
        }

        [Test]
        public void DequeueMemberProfileDuplicateReviewTest()
        {
            var result = DBApproveQueueBL.Instance.DequeueMemberProfileDuplicateReview(2);
            Assert.IsTrue(result == null || result.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryId > 0);
        }

        [Test]
        public void SaveMemberProfileDuplicateReviewTest()
        {
            var input = new MemberProfileDuplicateReview();
            input.MemberProfileDuplicateHistoryID = 100;
            input.AdminID = 100;
            input.SupervisorID = 100;
            input.LanguageID = 2;

            input.Entries = new List<MemberProfileDuplicateReviewEntry>();
            input.Entries.Add(new MemberProfileDuplicateReviewEntry
                                  {
                                      IsMatch = true,
                                      Ordinal = 1,
                                      Suspended = true
                                  });

            DBApproveQueueBL.Instance.SaveMemberProfileDuplicateReview(input);
            Assert.Pass();
        }

        [Test]
        public void SaveMemberProfileDuplicateHistoryTest()
        {
            var history = new MemberProfileDuplicateHistory();

            history.LanguageId = 2;
            history.QueueInsertDate = DateTime.Now;
            history.AdminId = 100066873;

            history.MemberProfileDuplicateHistoryEntries = new List<MemberProfileDuplicateHistoryEntry>();
            history.MemberProfileDuplicateHistoryEntries.Add(new MemberProfileDuplicateHistoryEntry()
                                                                 {
                                                                     MemberId = 100066873,
                                                                     CRXResponseId = 123,
                                                                     FieldType = FieldType.AboutMe,
                                                                     FieldValueOriginationDate = DateTime.Now,
                                                                     IsMatch = true,
                                                                     Suspended = true,
                                                                     AlreadySuspended = true
                                                                 });
            history.MemberProfileDuplicateHistoryEntries.Add(new MemberProfileDuplicateHistoryEntry()
                                                                 {
                                                                     MemberId = 100171517,
                                                                     CRXResponseId = 10083,
                                                                     FieldType = FieldType.AboutMe,
                                                                     FieldValueOriginationDate = DateTime.Now,
                                                                     IsMatch = true,
                                                                     Suspended = true,
                                                                     AlreadySuspended = true
                                                                 });

            DBApproveQueueBL.Instance.SaveMemberProfileDuplicateHistory(history, 100);
            Assert.Pass();
        }

        [Test]
        public void GetMemberProfileDuplicateReviewQueueItem()
        {
            var result = DBApproveQueueBL.Instance.GetMemberProfileDuplicateReviewQueueItem(6);
            Assert.IsTrue(result == null || result.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryId > 0);
        }

        [Test]
        public void GetMemberProfileDuplicateReviewQueueCountTest()
        {
            var result = DBApproveQueueBL.Instance.GetQueueCounts(QueueItemType.MemberProfileDuplicateReview);
            Assert.IsTrue(result != null);
        }

        [Test]
        public void GetLastCRXResponseResetTimeTest()
        {
            var result = DBApproveQueueBL.Instance.GetLastCRXResponseResetTime(10885);
            Assert.IsTrue(result != null);
        }

        [Test]
        public void SaveMemberProfileDuplicateTest()
        {
            var item = new QueueItemMemberProfileDuplicate();
            item.LanguageID = 2;
            item.MemberProfileDuplicates = new List<MemberProfileDuplicate>();

            item.MemberProfileDuplicates.Add(new MemberProfileDuplicate()
                                                 {
                                                     //CRXResponseId = 10885,
                                                     CRXResponseId = 123,
                                                     MemberId = 100066873,
                                                     FieldType = FieldType.AboutMe,
                                                     FieldValueOriginationDate = DateTime.Now,
                                                     FieldValue = "test mike",
                                                     OrdinalNumber = 1
                                                 });

            DBApproveQueueBL.Instance.SaveMemberProfileDuplicate(item);
            Assert.Pass();
        }
    }
}
