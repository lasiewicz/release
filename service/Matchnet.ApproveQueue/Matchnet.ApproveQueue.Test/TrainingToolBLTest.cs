﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.ApproveQueue.BusinessLogic;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using Matchnet.Data.Hydra;
using NUnit.Framework;

namespace Matchnet.ApproveQueue.Test
{
    public class TrainingToolBLTest
    {
        private MockSettingsService mockSettingsService = new MockSettingsService();
        private HydraWriter _hydraWriter;

        [TestFixtureSetUp]
        public void StartUp()
        {
            TrainingToolBL.Instance.SettingsService = mockSettingsService;
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
        }

        [SetUp]
        public void SetUp()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnAdmin" });
            _hydraWriter.Start();
        }

        [TearDown]
        public void Teardown()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }

        [Test]
        public void CreateNewGroupingTest()
        {
            TrainingToolBL.Instance.CreateNewGrouping("MikeTest1");
            Thread.Sleep(5000);
            Assert.Pass();
        }

        [Test]
        public void InsertMemberProfilesTest()
        {
            var data = new List<TTMemberProfile>();
            data.Add(new TTMemberProfile() { BrandID = 1003, MemberID = 100066873 });

            TrainingToolBL.Instance.InsertTTMemberProfiles("MikeTest1", data);
            Thread.Sleep(5000);
            Assert.Pass();
        }

        [Test]
        public void CreateNewTestInstanceTest()
        {
            TrainingToolBL.Instance.CreateNewTestInstance(1, 100066873);
            Thread.Sleep(5000);
            Assert.Pass();
        }

        [Test]
        public void GetNextTTMemberProfileQuestionTest()
        {
            var textAttributes = TrainingToolBL.Instance.GetNextTTMemberProfileQuestion(5, null);
            Assert.IsTrue(textAttributes.Count > 0);
        }

        [Test]
        public void GetTTMemberProfileCountTest()
        {
            var count = TrainingToolBL.Instance.GetTTMemberProfileCount(5, false);
            Assert.IsTrue(count > 0);
        }

        [Test]
        public void SaveAdminAnswerTest()
        {
            var answer = new TTAdminAnswer();
            answer.TTTestInstanceID = 5;
            answer.TTMemberProfileID = 13;
            answer.PossibleFraud = false;
            answer.Suspend = true;
            answer.SuspendReason = 1;
            answer.WContactInfo = true;
            answer.WGeneralViolation = true;
            answer.WInappropriateContent = true;
            answer.WNoWarning = false;

            answer.AdminAnswerEssays = new List<TTAdminAnswerEssay>();
            answer.AdminAnswerEssays.Add(new TTAdminAnswerEssay()
                {
                    TTMemberAttributeTextID = 111,
                    BadReasonMask = 0,
                    IsGood = true
                });

            TrainingToolBL.Instance.SaveAdminAnswer(answer, false);
            Thread.Sleep(5000);
            Assert.Pass();
        }
    }
}
