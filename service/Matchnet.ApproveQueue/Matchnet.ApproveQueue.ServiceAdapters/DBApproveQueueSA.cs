﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Messaging;
using System.Text;

using Matchnet;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.ApproveQueue.ValueObjects.ServiceDefinitions;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Member.ValueObjects.Photos;
using ServiceConstants = Matchnet.ApproveQueue.ValueObjects.ServiceConstants;

namespace Matchnet.ApproveQueue.ServiceAdapters
{
    public class DBApproveQueueSA : SABase
    {
        Cache _cache = null;
        
        public static readonly DBApproveQueueSA Instance = new DBApproveQueueSA();
        private DBApproveQueueSA() {
            _cache = Matchnet.Caching.Cache.Instance;
        }

        public void Enqueue(List<QueueItemBase> queueItems)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).Enqueue(queueItems);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot send queue items (uri: " + uri + ")", ex));
            }
        }

        public void Enqueue(QueueItemBase queueItem)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).Enqueue(queueItem);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot send queue items (uri: " + uri + ")", ex));
            }
        }

        public void EnqueueLight(QueueItemLightBase queueItem)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).EnqueueLight(queueItem);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot send a light queue item (uri: " + uri + ")", ex));
            }
        }

        public void DeletePhotoQueueItem(int memberPhotoID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).DeletePhotoQueueItem(memberPhotoID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot delete photo item (uri: " + uri + ")", ex));
            }
        }

        public List<IndividualTextApprovalStatus> GetIndividualTextApprovalStatuses()
        {
            string uri = "";
            List<IndividualTextApprovalStatus> statuses = null;

            try
            {

                if (_cache[CachedIndividualTextApprovalStatuses.GetCacheKey()] != null)
                {
                    statuses = ((CachedIndividualTextApprovalStatuses)_cache[CachedIndividualTextApprovalStatuses.GetCacheKey()]).Statuses;
                }
                else
                {
                    uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        statuses = getService(uri).GetIndividualTextApprovalStatuses();
                        _cache.Add(new CachedIndividualTextApprovalStatuses(statuses));
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return statuses;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Get Individual Text Approval Statuses (uri: " + uri + ")", ex));
            }
        }

        public void SaveIndividualTextAttributeApproval(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveIndividualTextAttributeApproval(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Save Individual Text Attribute Approval (uri: " + uri + ")", ex));
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Save Individual Text Attribute Approval (uri: " + uri + ")", ex));
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity, int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity, daysSinceFirstSubscription, billingPhoneNumber, subscriptionStatus,registrationDate);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Save Individual Text Attribute Approval (uri: " + uri + ")", ex));
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
         int statusMask, int adminMemberID, string textContent, bool isEdit,
         string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
         string occupationDescription, int educationLevel, int religion, int ethnicity,
         int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate,
         int? memberTextApprovalID, string adminTextContent)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity, daysSinceFirstSubscription, billingPhoneNumber, subscriptionStatus, registrationDate, memberTextApprovalID, adminTextContent);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Save Individual Text Attribute Approval (uri: " + uri + ")", ex));
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
         int statusMask, int adminMemberID, string textContent, bool isEdit,
         string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
         string occupationDescription, int educationLevel, int religion, int ethnicity,
         int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, 
         int? memberTextApprovalID, string adminTextContent, ApprovalStatus reviewStatus)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity, daysSinceFirstSubscription, billingPhoneNumber, subscriptionStatus, registrationDate, memberTextApprovalID, adminTextContent, reviewStatus);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Save Individual Text Attribute Approval (uri: " + uri + ")", ex));
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
                                                                int statusMask, int adminMemberID, string textContent,
                                                                bool isEdit,
                                                                string ip, int profileRegionId, int genderMask,
                                                                int maritalStatus, string firstName, string lastName,
                                                                string email,
                                                                string occupationDescription, int educationLevel,
                                                                int religion, int ethnicity,
                                                                int daysSinceFirstSubscription,
                                                                string billingPhoneNumber, string subscriptionStatus,
                                                                DateTime registrationDate,
                                                                int? memberTextApprovalID, string adminTextContent,
                                                                ApprovalStatus reviewStatus, bool deleteMovedRows)
        {
            string uri = "";

            try
            {
                uri =
                    getServiceManagerUri(
                        Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID,
                                                                                languageID, statusMask, adminMemberID,
                                                                                textContent, isEdit, ip, profileRegionId,
                                                                                genderMask, maritalStatus, firstName,
                                                                                lastName, email, occupationDescription,
                                                                                educationLevel, religion, ethnicity,
                                                                                daysSinceFirstSubscription,
                                                                                billingPhoneNumber, subscriptionStatus,
                                                                                registrationDate, memberTextApprovalID,
                                                                                adminTextContent, reviewStatus,
                                                                                deleteMovedRows);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot Save Individual Text Attribute Approval (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Use this method if you need to find the text that CRX saw. This method will automatically look at the queue tabe and then in the history table if not found
        /// in the queue table.
        /// </summary>
        /// <param name="crxResponseId">CRXResponseID</param>
        /// <returns></returns>
        public CRXText LocateCRXText(int crxResponseId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).LocateCRXText(crxResponseId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot locate the CRX text (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Takes an item off the MemberProfileDuplicate queue.
        /// </summary>
        /// <returns>NULL if queue has no item to process otherwise populated QueueItemMemberProfileDuplicate object.</returns>
        public QueueItemMemberProfileDuplicate DequeueMemberProfileDuplicateByLanguage(int languageId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueMemberProfileDuplicateByLanguage(languageId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue MemberProfileDuplicate (uri: " + uri + ")", ex));
            }
        }

        public QueueItemMemberProfileDuplicateReview DequeueMemberProfileDuplicateReview(int languageID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueMemberProfileDuplicateReview(languageID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue MemberProfileDuplicateReview (uri: " + uri + ")", ex));
            }
        }

        public QueueItemMemberProfileDuplicateReview GetMemberProfileDuplicateReviewQueueItem(int memberProfileDuplicateHistoryID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberProfileDuplicateReviewQueueItem(memberProfileDuplicateHistoryID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a MemberProfileDuplicateReview queue item (uri: " + uri + ")", ex));
            }
        }

        public QueueItemMemberProfileDuplicate GetMemberProfileDuplicateQueueItem(int duplicateQueueID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberProfileDuplicateQueueItem(duplicateQueueID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a MemberProfileDuplicateQueue item (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemBase> DequeuePhotosByCommunity(int maxCount, Int32 communityID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeuePhotosByCommunity(maxCount, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue photos (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemBase> DequeuePhotosBySite(int maxCount, Int32 siteID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeuePhotosBySite(maxCount, siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue photos (uri: " + uri + ")", ex));
            }
        }
        
        public List<QueueItemBase> DequeuePhotosByMember(int maxCount, Int32 memberID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeuePhotosByMember(maxCount, memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue photos (uri: " + uri + ")", ex));
            }
        }

        public void CompletePhotoApproval(int memberPhotoID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompletePhotoApproval(memberPhotoID, approvalStatus, adminMemberID, adminActionMask);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot complete photo approval (uri: " + uri + ")", ex));
            }
        }

        public void CompleteTextApproval(int memberID, int communityID, int languageID, TextType textType, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompleteTextApproval(memberID, communityID, languageID, textType, approvalStatus, adminMemberID, adminActionMask);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot complete text approval (uri: " + uri + ")", ex));
            }
        }

         public void CompleteTextApprovalMemberApproval(int memberID, int memberTextApprovalID,
                                                       ApprovalStatus approvalStatus, int adminMemberID,
                                                       int adminActionMask)
         {
             string uri = "";

             try
             {
                 uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                 base.Checkout(uri);
                 try
                 {
                     getService(uri).CompleteTextApprovalMemberApproval(memberID, memberTextApprovalID, approvalStatus,
                                                                        adminMemberID, adminActionMask);
                 }
                 finally
                 {
                     base.Checkin(uri);
                 }
             }
             catch (Exception ex)
             {
                 throw (new SAException("Cannot complete text approval for MemberApproval view (uri: " + uri + ")", ex));
             }
         }

        public QueueItemText DequeueTextByLanguage(Int32 languageID, TextType textType)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueTextByLanguage(languageID, textType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue text by language (uri: " + uri + ")", ex));
            }
        }

        public QueueItemText DequeueTextBySite(Int32 siteID, TextType textType)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueTextBySite(siteID, textType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue text by site (uri: " + uri + ")", ex));
            }
        }

        public QueueItemText DequeueTextByCommunity(Int32 communityID, TextType textType)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueTextByCommunity(communityID, textType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue text by community (uri: " + uri + ")", ex));
            }
        }

        public QueueItemText DequeueTextByMember(Int32 memberID, TextType textType)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueTextByMember(memberID, textType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue text by member (uri: " + uri + ")", ex));
            }
        }

        public List<ApprovalCountRecord> GetApprovalHistoryCounts(DateTime startDate, DateTime endDate, int adminMemberID, int communityID, int siteID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetApprovalHistoryCounts(startDate, endDate, adminMemberID, communityID, siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get history counts (uri: " + uri + ")", ex));
            }
        }

        public List<MemberProfileDuplicateHistoryCount> GetMemberProfileDuplicateHistoryCount(DateTime startDate, DateTime endDate, int adminMemberID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberProfileDuplicateHistoryCount(startDate, endDate, adminMemberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get memberprofileduplicate history counts (uri: " + uri + ")", ex));
            }
        }

        public List<QueueCountRecord> GetQueueCounts(QueueItemType itemType)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetQueueCounts(itemType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get queue counts (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemBase> GetApprovalPhotoQueueItems(int siteID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetApprovalPhotoQueueItems(siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get Approval photo queue items (uri: " + uri + ")", ex));
            }
        }

        public void SaveMemberTextApprovalAttributes(List<QueueItemText> queueItemTexts)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveMemberTextApprovalAttributes(queueItemTexts);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save member text approval attributes (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemMember> DequeueMemberBySite(Int32 siteId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueMemberBySite(siteId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue members from fraud review queue (uri: " + uri + ")", ex));
            }
        }

        public void CompleteMemberFraudReview(List<QueueItemMember> memberItems)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompleteMemberFraudReview(memberItems);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot complete members fraud review (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemText> GetTextAttributesForCRXApproval(int? communityId, int? languageId, int? textType, int? numberOfRows)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTextAttributesForCRXApproval(communityId, languageId, textType, numberOfRows);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get member text approval attributes (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemText> GetTextAttributesForMember(int memberID, int communityID, int languageID, TextType textType)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTextAttributesForMember(memberID, communityID, languageID, textType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get member text approval attributes for a member (uri: " + uri + ")", ex));
            }
        }

        public void CompleteCRXTextAttributeArroval(QueueItemText itemText)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompleteCRXTextAttributeArroval(itemText);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot complete CRX text attribute approval (uri: " + uri + ")", ex));
            }
        }

        public void SaveAdminReviewSettings(int adminMemberId, int? dailySampleRate, int? onDemandSampleRate, bool isEdit)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveAdminReviewSettings(adminMemberId, dailySampleRate, onDemandSampleRate, isEdit);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to Save Admin review settings. (uri: " + uri + ")", ex));
            }
        }

        public void SaveAdminReviewSettings(List<AdminReviewSetting> adminReviewSettings)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveAdminReviewSettings(adminReviewSettings);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to Save Admin review settings. (uri: " + uri + ")", ex));
            }
        }

        public TextApprovalReviewCount GetTextApprovalReviewCounts(bool isOnDemand)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTextApprovalReviewCounts(isOnDemand);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get FTA review counts. (uri: " + uri + ")", ex));
            }
        }

        public ReviewText GetTextToReview(bool isOnDemand, int? supervisorId, int? adminMemberId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTextToReview(isOnDemand,supervisorId,adminMemberId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get FTA review counts. (uri: " + uri + ")", ex));
            }
        }

        public void CompleteAdminTextApprovalReview(ReviewText reviewText)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompleteAdminTextApprovalReview(reviewText);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to complete FTA review. (uri: " + uri + ")", ex));
            }
        }

        public void AcknowledgeTrainingReport(int memberTextApprovalId, int adminMemberId, int supervisorId, bool isOnDemand)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).AcknowledgeTrainingReport(memberTextApprovalId, adminMemberId, supervisorId, isOnDemand);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to acknowledge admin FTA training report. (uri: " + uri + ")", ex));
            }
        }

        public List<TextApprovalReviewHistoryCount> GetTextApprovalReviewHistoryCounts(DateTime startDate, DateTime endDate, int? adminMemberId, int? communityId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTextApprovalReviewHistoryCounts(startDate, endDate, adminMemberId, communityId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get Text Approval Review HistoryCounts. (uri: " + uri + ")", ex));
            }
        }

        public List<AdminReviewSetting> GetAdminReviewSampleRates()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetAdminReviewSampleRates();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get Admin review sample rates. (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemText> GetApprovalTextAttributesCRXResponse(int memberTextApprovalId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetApprovalTextAttributesCRXResponse(memberTextApprovalId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get member text approval attributes and CRX Response. (uri: " + uri + ")", ex));
            }
        }

        public ReviewText GetCRXTextForSecondReview()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetCRXTextForSecondReview();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get FTA review counts. (uri: " + uri + ")", ex));
            }
        }

        public void CompleteCRXSecondReview(ReviewText reviewText)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompleteCRXSecondReview(reviewText);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to complete CRX FTA second review. (uri: " + uri + ")", ex));
            }
        }

        public TextApprovalReviewCount GetCRXSecondReviewCounts()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetCRXSecondReviewCounts();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get FTA CRX Second review counts. (uri: " + uri + ")", ex));
            }
        }

        public void PopulateOnDemandTextReview(DateTime startDate, DateTime endDate, int? communityId, int supervisorId, List<AdminReviewSetting> adminRates)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).PopulateOnDemandTextReview(startDate, endDate, communityId, supervisorId,adminRates);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to populate OnDemand Admin Text Apporval Review. (uri: " + uri + ")", ex));
            }
        }

        public QueueItemText GetCRXTextAttributeInfo(int CrxResponseId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetCRXTextAttributeInfo(CrxResponseId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get CRX text attribute info (uri: " + uri + ")", ex));
            }
        }

        public void RevertCRXResponse()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RevertCRXResponse();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot revert CRX Response (uri: " + uri + ")", ex));
            }
        }

        public DateTime GetLastCRXResponseResetTime(int CRXResponseID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetLastCRXResponseResetTime(CRXResponseID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get Last CRXResponseResetTime (uri: " + uri + ")", ex));
            }
        }

        public QueueItemText GetCRXResponse(int CrxResponseId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetCRXResponse(CrxResponseId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get CRX Response (uri: " + uri + ")", ex));
            }
        }

        public List<QueueItemText> VerifyCRXResponse(int? communityId, int? languageId, int? textType, int? numberOfRows)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).VerifyCRXResponse(communityId, languageId, textType, numberOfRows);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get member text approval attributes (uri: " + uri + ")", ex));
            }
        }

        public List<TextApprovalReviewCount> GetOnDemandReviewQueueCounts()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetOnDemandReviewQueueCounts();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to get admin OnDemand FTA review counts. (uri: " + uri + ")", ex));
            }
        }

        public void PurgeOnDemandReviewQueue(int adminMemberId, int supervisorId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).PurgeOnDemandReviewQueue(adminMemberId,supervisorId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to purge FTA on demand review queue. (uri: " + uri + ")", ex));
            }
        }

        public void SaveMemberProfileDuplicateHistory(MemberProfileDuplicateHistory history, int memberProfileDuplicateQueueId)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveMemberProfileDuplicateHistory(history, memberProfileDuplicateQueueId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to save MemberProfileDuplicateHistory. (uri: " + uri + ")", ex));
            }
        }

        /// When the queue item is deemed bad, call this method to remove it from the queue. Only use this method
        /// if the CRXResponseID is missing.
        public void DeleteMemberProfileDuplicateQueueItem(int MemberProfileDuplicateQueueID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).DeleteMemberProfileDuplicateQueueItem(MemberProfileDuplicateQueueID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to delete MemberProfileDuplicateQueueItem. (uri: " + uri + ")", ex));
            }
        }

        public void SaveMemberProfileDuplicate(QueueItemMemberProfileDuplicate queueItem)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveMemberProfileDuplicate(queueItem);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to save MemberProfileDuplicate. (uri: " + uri + ")", ex));
            }
        }

        public void SaveMemberProfileDuplicateReview(MemberProfileDuplicateReview duplicateReview)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveMemberProfileDuplicateReview(duplicateReview);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to save MemberProfileDuplicateReview. (uri: " + uri + ")", ex));
            }
        }

        private IDBApproveQueueService getService(string uri)
        {
            try
            {
                return (IDBApproveQueueService)Activator.GetObject(typeof(IDBApproveQueueService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("APPROVEQUEUESVC_SA_CONNECTION_LIMIT"));
        }

        private string getServiceManagerUri(string serviceManagerName)
        {
            return getServiceManagerUri(serviceManagerName, string.Empty);
        }

        private string getServiceManagerUri(string serviceManagerName, string hostName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName;

                if (hostName.Length == 0)
                {
                    overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("APPROVEQUEUESVC_SA_HOST_OVERRIDE");
                }
                else
                {
                    overrideHostName = hostName;
                }

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        public void SendToPhotoApprove(ApproveInfoCollection approveInfoCollection)
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("DBApproveQueueSA.SendToPhotoApprove() - Start");
                if (approveInfoCollection.Count.Equals(0))
                    return;

                MessageQueue queue = null;
                Message message = null;

                try
                {
                    string msmqPath = RuntimeSettings.GetSetting("PHOTO_APPROVE_MSMQ_PATH");
                    System.Diagnostics.Trace.WriteLine("DBApproveQueueSA.SendToPhotoApprove() - Sending to MSMQ at " + msmqPath);
                    queue =
                        new MessageQueue(msmqPath);
                    
                    message = new Message(approveInfoCollection, new BinaryMessageFormatter());

                    queue.Send(message);
                }
                finally
                {
                    if (queue != null)
                    {
                        queue.Dispose();
                    }
                    if (message != null)
                    {
                        message.Dispose();
                    }
                }

                System.Diagnostics.Trace.WriteLine("DBApproveQueueSA.SendToPhotoApprove() - Finished");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("DBApproveQueueSA.SendToPhotoApprove() - Error " + ex.Message);
                throw new Exception("Unable to send ApproveInfoCollection to backend msmq.", ex);
            }
        }

        public void DeleteFacebookLikeQueueItem(int memberID, int siteID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).DeleteFacebookLikeQueueItem(memberID, siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot delete facebook like from queue (uri: " + uri + ")", ex));
            }            
        }

        public List<QueueItemBase> DequeueFacebookLikesByCommunity(int maxCount, Int32 communityID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueFacebookLikesByCommunity(maxCount, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue facebook like from queue (uri: " + uri + ")", ex));
            }                        
        }

        public List<QueueItemBase> DequeueFacebookLikesBySite(int maxCount, Int32 siteID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueFacebookLikesBySite(maxCount, siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue facebook like from queue (uri: " + uri + ")", ex));
            }                                    
        }

        public List<QueueItemBase> DequeueFacebookLikesByMember(int maxCount, Int32 memberID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).DequeueFacebookLikesByCommunity(maxCount, memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue facebook like from queue (uri: " + uri + ")", ex));
            }                                    
        }

        public void CompleteFacebookLikesApproval(int memberId, int siteID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CompleteFacebookLikesApproval(memberId, siteID, approvalStatus, adminMemberID, adminActionMask);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot dequeue facebook like from queue (uri: " + uri + ")", ex));
            }
        }

        #region Training Tool

        public void CreateNewGrouping(string groupingName)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).CreateNewGrouping(groupingName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot create a new training tool grouping (uri: " + uri + ")", ex));
            }                                    
        }

        public List<TTGrouping> GetGroupingList()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetGroupingList();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a list of training tool groupings (uri: " + uri + ")", ex));
            }   
        }

        public TTGrouping GetTTGrouping(int ttGroupingID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTGrouping(ttGroupingID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the specified grouping (uri: " + uri + ")", ex));
            }
        }

        public List<TTGrouping> GetCompletedGroupingList()
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetCompletedGroupingList();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a list of completed training tool groupings (uri: " + uri + ")", ex));
            }   
        }

        /// <summary>
        /// Gets all the member attributes for a given TTMemberProfileID (which represents a MemberID + GroupingID combo).
        /// </summary>
        /// <param name="ttMemberProfileID"></param>
        /// <returns></returns>
        public TTMemberProfile GetTTMemberAttributes(int ttMemberProfileID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberAttributes(ttMemberProfileID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the training tool member attributes (uri: " + uri + ")", ex));
            }   
        }

        /// <summary>
        /// The method returns a single TTMessage specified by the TTMessageID.
        /// </summary>
        /// <param name="ttMessageID"></param>
        /// <returns></returns>
        public TTMessage GetTTMessage(int ttMessageID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMessage(ttMessageID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the training tool emails (uri: " + uri + ")", ex));
            }  
        }

        /// <summary>
        /// This method returns member emails that have been fronzen when the member was moved to a training tool bucket. It freezes the
        /// top 100 emails (both incoming and sent combined) sorted by InsertDate desc.  
        /// </summary>
        /// <param name="ttMemberProfileID"></param>
        /// <returns></returns>
        public List<TTMessage> GetTTMessages(int ttMemberProfileID, bool populateMessageBody)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMessages(ttMemberProfileID, populateMessageBody);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the training tool emails (uri: " + uri + ")", ex));
            }  
        }

        public TTAdminAnswerReport GetTTAdminAnswerReport(int ttTestInstanceID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTAdminAnswerReport(ttTestInstanceID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the training tool test results (uri: " + uri + ")", ex));
            }  
        }

        public List<TTMemberProfile> GetTTMemberProfiles(int groupingID, int pageNumber, int rowsPerPage, out int totalRows)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberProfiles(groupingID, pageNumber, rowsPerPage, out totalRows);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a list of member profiles for a given groupingID (uri: " + uri + ")", ex));
            }  
        }

        public List<TTMemberProfile> GetTTMemberProfiles(int memberID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberProfiles(memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a list of member profiles for a given memberID (uri: " + uri + ")", ex));
            } 
        }

        public TTMemberProfile GetTTMemberProfile(int memberID, int ttGroupingID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberProfile(memberID, ttGroupingID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the member profile for givent memberID+ttGroupingID (uri: " + uri + ")", ex));
            } 
        }

        /// <summary>
        /// If getAllVersions is set to TRUE, all versions of suspend reasons will be attached to the first element of TTAnswerMemberProfile.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupingID"></param>
        /// <param name="getAllVersions"></param>
        /// <param name="ttMemberProfileID"></param>
        /// <returns></returns>
        public List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTAnswerMemberProfiles(memberID, groupingID, getAllVersions, ttMemberProfileID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the specified TTAnswerMemberProfile (uri: " + uri + ")", ex));
            }  
        }

        /// <summary>
        /// TTAnswerMemberProfiles for the passed in TTTestInstanceID are returned with the suspend reasons attached.
        /// </summary>
        /// <param name="ttTestInstanceID"></param>
        /// <returns></returns>
        public List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int ttTestInstanceID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTAnswerMemberProfiles(ttTestInstanceID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve TTAnswerMemberProfiles for the given TTTestInstanceID (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// The answer key portion of TTMemberAttributeText is included in the returned list. This method does not return the actual field value
        /// or the IncludeInTest value.
        /// </summary>
        /// <param name="ttTestInstanceID"></param>
        /// <returns></returns>
        public List<TTMemberAttributeText> GetTTAnswerMemberAttributeTexts(int ttTestInstanceID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTAnswerMemberAttributeTexts(ttTestInstanceID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve TTAnswerMemberAttributeTexts for the given TTTestInstanceID (uri: " + uri + ")", ex));
            }
        }

        public List<TTMemberAttributeText> GetAllTTMemberTextAttributes(int memberID, int groupingID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetAllTTMemberTextAttributes(memberID, groupingID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a list of text attributes for a given member (uri: " + uri + ")", ex));
            }  
        }

        /// <summary>
        /// Retrieves the TTMemberAttributeText rows for a given memberID + groupingID combo.  If getAllVersions is set to true, all versions of
        /// answers tied to the text attributes are returned.  If the latest answer version is good enough, set getAllVersions to false.  If you are
        /// looking for a specific versions of the answers (for grading purpose for instance), set getAllVersions to true.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupingID"></param>
        /// <param name="getAllVersions"></param>
        /// <param name="ttMemberProfileID"></param>
        /// <returns></returns>
        public List<TTMemberAttributeText> GetAllTTMemberTextAttributesWithAnswer(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetAllTTMemberTextAttributesWithAnswer(memberID, groupingID, getAllVersions, ttMemberProfileID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve a list of text attributes with answer for a given member (uri: " + uri + ")", ex));
            }  
        }

        public void InsertTTMemberProfiles(string groupingName, List<TTMemberProfile> memberProfiles)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).InsertTTMemberProfiles(groupingName, memberProfiles);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot insert a member profile to a training tool grouping (uri: " + uri + ")", ex));
            }   
        }

        public void InsertTTMemberProfiles(int groupingID, List<TTMemberProfile> memberProfiles)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).InsertTTMemberProfiles(groupingID, memberProfiles);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot insert a member profile to a training tool grouping (uri: " + uri + ")", ex));
            }
        }

        public void SaveTTAnswer(TTAnswerMemberProfile profile, List<TTMemberAttributeText> texts)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveTTAnswer(profile, texts);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save Training Tool answer. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// This method will do all the bootstrapping tasks for a new Training Tool test. It will execute synchronously for this reason.
        /// </summary>
        /// <param name="groupingID"></param>
        /// <param name="adminID"></param>
        /// <returns>TTTestInstanceID of the test instance created</returns>
        public int CreateNewTestInstance(int groupingID, int adminID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).CreateNewTestInstance(groupingID, adminID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot create a new Training Tool test instance. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Writes the admin's answer to the DB. All the DB writes happen in asynch fashion, so partial writes can result if error occurs in the middle.
        /// </summary>
        /// <param name="adminAnswer"></param>
        public void SaveAdminAnswer(TTAdminAnswer adminAnswer, bool isSynchronousWrite)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveAdminAnswer(adminAnswer, isSynchronousWrite);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save an admin's response to a Training Tool question. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// This method should be used to end or pause a test.
        /// </summary>
        /// <param name="ttTestInstanceID"></param>
        /// <param name="testStatus"></param>
        public void UpdateTestStatus(int ttTestInstanceID, TTTestInstanceStatus testStatus)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).UpdateTestStatus(ttTestInstanceID, testStatus);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save test timespan info. (uri: " + uri + ")", ex));
            }
        }

        public void UpdateTTMemberProfileTTGroupingID(int ttMemberProfileID, int newGroupingID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).UpdateTTMemberProfileTTGroupingID(ttMemberProfileID, newGroupingID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save the TTMemberProfile's TTGroupingID. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// While the admins are using the Training Tool, continuous pulse should be sent using this method. Whenever a pulse isn't sent within 5 minutes of time,
        /// the backend assumes the browser has been closed.
        /// </summary>
        /// <param name="ttTestInstanceID"></param>
        public void TrainingToolKeepalivePulse(int ttTestInstanceID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).TrainingToolKeepalivePulse(ttTestInstanceID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot send a keepalive pulse. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Gets the next TTMemberProfile to use a test question. Use TTMemberProfileIDToExclude to exclude a value since using this method in combination with a asynch
        /// write could cause the client to pick up the same item it just processed.
        /// </summary>
        /// <param name="ttTestInstanceID">TTTestInstanceID of the test to search within</param>
        /// <param name="ttMemberProfileIDToExclude">TTMemberProfileIDToExclude to exclude from the search</param>
        /// <returns>null if none found or populated TTMemberAttributeText list object that contains which FTA fields to test on</returns>
        public List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                          int? ttMemberProfileIDToExclude)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetNextTTMemberProfileQuestion(ttTestInstanceID, ttMemberProfileIDToExclude);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the next Training Tool question. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Gets the next TTMemberProfile to use a test question. Use TTMemberProfileIDToExclude to exclude a value since using this method in combination with a asynch
        /// write could cause the client to pick up the same item it just processed. RemainingCount out parm includes the item that is being picked up since it's technically
        /// not processed yet.
        /// </summary>
        /// <param name="ttTestInstanceID">Test instance to pick the question from.</param>
        /// <param name="ttMemberProfileIDToExclude">If asynch writes are used, use this parameter to make sure the same item does not get picked up again.</param>
        /// <param name="remainingCount">How many items remain in this test instance. This count includes the item being picked up as well.</param>
        /// <param name="ttAnswerMemberProfileVersion">Answer version number from TTAnswerMemberProfile table.</param>
        /// <param name="ttAnswerSuspendReasonsVersion">Answer version from TTAnswerSuspendReasons table.</param>
        public List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                          int? ttMemberProfileIDToExclude,
                                                                          out int remainingCount,
                                                                          out int ttAnswerMemberProfileVersion,
                                                                          out int ttAnswerSuspendReasonsVersion)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri)
                        .GetNextTTMemberProfileQuestion(ttTestInstanceID, ttMemberProfileIDToExclude, out remainingCount,
                                                        out ttAnswerMemberProfileVersion,
                                                        out ttAnswerSuspendReasonsVersion);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the next Training Tool question. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Retrieves the total count of TTMemberProfiles for a given test instance. This is used to determine if the record the client is processing is the last one.
        /// When a report of the final grade needs to be calculated, this can be used to ensure everything was included.
        /// </summary>
        /// <param name="ttTestInstanceID">TTTestInstanceID of the test instance</param>
        /// <returns>0 if not found and the number of TTMemberProfiles if found</returns>
        public int GetTTMemberProfileCount(int ttTestInstanceID, bool answeredOnly)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberProfileCount(ttTestInstanceID, answeredOnly);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the count for the given TTTestInstanceID. (uri: " + uri + ")", ex));
            }
        }

        public List<TTTestInstance> GetTTTestInstances(int adminMemberID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTTestInstances(adminMemberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the list of test instances. (uri: " + uri + ")", ex));
            }
        }

        public TTTestInstance GetTTTestInstance(int ttTestInstanceID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTTestInstance(ttTestInstanceID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve the specified test instance. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Combines all the time segments to come up with the actual total time spent on the test in seconds.  testFinished
        /// out paramter will indicate if the test is done or not. If the test isn't finished, the unclosed timespan will not be
        /// included in the time calculation.
        /// </summary>
        /// <param name="ttTestInstanceID"></param>
        /// <returns></returns>
        public double GetTotalTimeSpentOnTest(int ttTestInstanceID, out bool testFinished)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTotalTimeSpentOnTest(ttTestInstanceID, out testFinished);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to total up the time spent on a test. (uri: " + uri + ")", ex));
            }
        }

        public List<TTMemberPhoto> GetTTMemberPhotos(int memberID, int groupingID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberPhotos(memberID, groupingID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to retrieve the TTMemberPhotos. (uri: " + uri + ")", ex));
            }
        }

        public List<TTMemberPhoto> GetTTMemberPhotos(int ttMemberProfileID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.DBQUEUE_SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetTTMemberPhotos(ttMemberProfileID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Unable to retrieve the TTMemberPhotos. (uri: " + uri + ")", ex));
            }
        }

        #endregion
    }
}
