using System;

namespace Matchnet.ApproveQueue.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	public class ServiceConstants
	{
		private ServiceConstants()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "APPROVEQUEUE_SVC";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.ApproveQueue.Service";
		/// <summary>
		/// 
		/// </summary>

        public const string DBQUEUE_SERVICE_MANAGER_NAME = "DBApproveQueueSM";
	}
}
