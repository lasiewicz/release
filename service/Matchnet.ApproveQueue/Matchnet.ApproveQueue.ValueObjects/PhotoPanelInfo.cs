using System;
using System.Drawing;

namespace Matchnet.ApproveQueue.ValueObjects
{
	/// <summary>
	/// </summary>
	[Serializable]
	public class PhotoPanelInfo : IValueObject
	{
		#region Private Variables
		private String _fileWebPath = String.Empty;
		private String _filePath = String.Empty;
		private int _memberPhotoID = Constants.NULL_INT;
		private int _memberID = Constants.NULL_INT;
		private int _communityID = Constants.NULL_INT;
		private int _fileID = Constants.NULL_INT;
		private int _thumbFileID = Constants.NULL_INT;
		private String _caption = String.Empty;
		private int _approvedFlag = Constants.NULL_INT;
		private int _listOrder = Constants.NULL_INT;
		private int _insertDate = Constants.NULL_INT;
		private int _albumID = Constants.NULL_INT;
		private int _privateFlag = Constants.NULL_INT;
		private int _genderMask = Constants.NULL_INT;
		private DateTime _birthDate = DateTime.MinValue;
		private String _emailAddress = String.Empty;
		private String _userName = String.Empty;
		private String _profileSite = String.Empty;
		private int _adminMemberID = Constants.NULL_INT;
		private Int16 _rotateAngle;
		private Int16 _thumbRectX;
		private Int16 _thumbRectY;
		private Int16 _thumbRectWidth;
		private Int16 _thumbRectHeight;
		private Int16 _cropRectX;
		private Int16 _cropRectY;
		private Int16 _cropRectWidth;
		private Int16 _cropRectHeight;
		private Rectangle _cropRect;
		private Rectangle _thumbRect;
		private int _emailBodyResourceID = Constants.NULL_INT;
		private String _photoRejectReason = String.Empty;
		private String _defaultHost;
		private int _captionApprovedFlag=Constants.NULL_INT;
		private int _siteID;
		#endregion

		#region Public Properties		
		public String FileWebPath 
		{
			get
			{
				return _fileWebPath;
			}
			set
			{
				_fileWebPath = value;
			}
		}
		
		public String FilePath 
		{
			get
			{
				return _filePath;
			}
			set
			{
				_filePath = value;
			}
		}
		
		public int MemberPhotoID 
		{
			get
			{
				return _memberPhotoID;
			}
			set
			{
				_memberPhotoID = value;
			}
		}
		
		public int MemberID 
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}
		
		public int CommunityID 
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

		public int FileID 
		{
			get
			{
				return _fileID;
			}
			set
			{
				_fileID = value;
			}
		}

		public int ThumbFileID 
		{
			get
			{
				return _thumbFileID;
			}
			set
			{
				_thumbFileID = value;
			}
		}

		public String Caption 
		{
			get
			{
				return _caption;
			}
			set
			{
				_caption = value;
			}
		}
		public int ApprovedFlag 
		{
			get
			{
				return _approvedFlag;
			}
			set
			{
				_approvedFlag = value;
			}
		}

		public int ListOrder 
		{
			get
			{
				return _listOrder;
			}
			set
			{
				_listOrder = value;
			}
		}
		
		public int InsertDate 
		{
			get
			{
				return _insertDate;
			}
			set
			{
				_insertDate = value;
			}
		}
		public int AlbumID 
		{
			get
			{
				return _albumID;
			}
			set
			{
				_albumID = value;
			}
		}
		public int PrivateFlag 
		{
			get
			{
				return _privateFlag;
			}
			set
			{
				_privateFlag = value;
			}
		}

		public int GenderMask 
		{
			get
			{
				return _genderMask;
			}
			set
			{
				_genderMask = value;
			}
		}

		public DateTime BirthDate 
		{
			get
			{
				return _birthDate;
			}
			set
			{
				_birthDate = value;
			}
		}

		public String EmailAddress 
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}

		public String UserName 
		{
			get
			{
				return _userName;
			}
			set
			{
				_userName = value;
			}
		}
		public String ProfileSite 
		{
			get
			{
				return _profileSite;
			}
			set
			{
				_profileSite = value;
			}
		}
		public int AdminMemberID 
		{
			get
			{
				return _adminMemberID;
			}
			set
			{
				_adminMemberID = value;
			}
		}
		
		public Int16 RotateAngle 
		{
			get
			{
				return _rotateAngle;
			}
			set
			{
				_rotateAngle = value;
			}
		}
		
		public Int16 ThumbRectX 
		{
			get
			{
				return _thumbRectX;
			}
			set
			{
				_thumbRectX = value;
			}
		}
		public Int16 ThumbRectY 
		{
			get
			{
				return _thumbRectY;
			}
			set
			{
				_thumbRectY = value;
			}
		}
		public Int16 ThumbRectWidth 
		{
			get
			{
				return _thumbRectWidth;
			}
			set
			{
				_thumbRectWidth = value;
			}
		}

		public Int16 ThumbRectHeight 
		{
			get
			{
				return _thumbRectHeight;
			}
			set
			{
				_thumbRectHeight = value;
			}
		}
		
		public Int16 CropRectX 
		{
			get
			{
				return _cropRectX;
			}
			set
			{
				_cropRectX = value;
			}
		}
		public Int16 CropRectY 
		{
			get
			{
				return _cropRectY;
			}
			set
			{
				_cropRectY = value;
			}
		}
		public Int16 CropRectWidth 
		{
			get
			{
				return _cropRectWidth;
			}
			set
			{
				_cropRectWidth = value;
			}
		}

		public Int16 CropRectHeight 
		{
			get
			{
				return _cropRectHeight;
			}
			set
			{
				_cropRectHeight = value;
			}
		}

		public Rectangle CropRect 
		{
			get
			{
				return _cropRect;
			}
			set
			{
				_cropRect = value;
			}
		}

		public Rectangle ThumbRect 
		{
			get
			{
				return _thumbRect;
			}
			set
			{
				_thumbRect = value;
			}
		}

		public int EmailBodyResourceID 
		{
			get
			{
				return _emailBodyResourceID;
			}
			set
			{
				_emailBodyResourceID = value;
			}
		}
		
		public String PhotoRejectReason 
		{
			get
			{
				return _photoRejectReason;
			}
			set
			{
				_photoRejectReason = value;
			}
		}

		public String DefaultHost 
		{
			get
			{
				return _defaultHost;
			}
			set
			{
				_defaultHost = value;
			}
		}
		public int CaptionApprovedFlag 
		{
			get
			{
				return _captionApprovedFlag;
			}
			set
			{
				_captionApprovedFlag = value;
			}
		}

		public int SiteID
		{
			get
			{
				return _siteID;
			}
			set
			{
				_siteID = value;
			}
		}
		#endregion

		#region Constructor
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="photo"></param>
		/// <param name="birthDate"></param>
		/// <param name="genderMask"></param>
		/// <param name="userName"></param>
		/// <param name="defaultHost">Usually retrieved from g.Brand.Site.DefaultHost.</param>
		public PhotoPanelInfo(Int32 memberID, 
			Int32 communityID, 
			Matchnet.Member.ValueObjects.Photos.Photo photo, 
			DateTime birthDate, 
			Int32 genderMask, 
			String userName, 
			String defaultHost)
		{
			this._communityID = communityID;
			this._memberID = memberID;
			this._fileID = photo.FileID;
			this._fileWebPath = photo.FileWebPath;
			this._memberPhotoID = photo.MemberPhotoID;
			this._thumbFileID = photo.ThumbFileID;
			this._listOrder = photo.ListOrder;
			this._privateFlag = Convert.ToInt32(photo.IsPrivate);
			this._birthDate = birthDate;
			this._genderMask = genderMask;
			this._approvedFlag = Convert.ToInt32(photo.IsApproved);
			this._userName = userName;
			this._defaultHost = defaultHost;
			this._caption=photo.Caption;
			this._captionApprovedFlag=Convert.ToInt32(photo.IsCaptionApproved);
		}
		#endregion
	}
}
