﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class MemberProfileDuplicateHistoryCount
    {
        public int AdminId { get; set; }
        public int ActionCount { get; set; }
        public int DuplicatesCount { get; set; }
        public int MatchesCount { get; set; }
        public int SuspendedCount { get; set; }
        public int AlreadySuspendedCount { get; set; }

    }
}
