﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class QueueItemPhoto: QueueItemBase, IValueObject
    {
        public int MemberPhotoID { get; set; }

        public QueueItemPhoto() { }
        public QueueItemPhoto(int communityID, int siteID, int brandID, int memberID, int memberPhotoID)
            : base(communityID, siteID, brandID, memberID)
        {
            MemberPhotoID = memberPhotoID;
        }
    }
}
