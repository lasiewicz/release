﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class ReviewText
    {
        public int MemberId { get; set; }
        public int AdminMemberId { get; set; }
        public int CommunityId { get; set; }
        public int LanguageId { get; set; }
        public int TextTypeId { get; set; }
        public int AdminActionMask { get; set; }
        public DateTime QueueInsertDate { get; set; }
        public DateTime StatusDate { get; set; }
        public int MemberTextApprovalId { get; set; }
        public int SupervisorId { get; set; }
        public SupervisorReviewStatus SupervisorReviewStatusId { get; set; }
        public string SupervisorNotes { get; set; }
        public bool IsOnDemand { get; set; }
        public int ReviewerId { get; set; }
        public string ReviewerAccount { get; set; }
        public int ReviewerActionMask { get; set; }
        public List<ReviewTextAttribute> TextAttributes { get; set; }

        public ReviewText()
        {
            TextAttributes = new List<ReviewTextAttribute>();
        }
    }

    [Serializable]
    public class ReviewTextAttribute
    {
        public int AttributeGroupId { get; set; }
        public string TextContent { get; set; }
        public string AdminTextContent { get; set; }
        public int AdminMemberId { get; set; }
        public int StatusMask { get; set; }
        public int SupervisorStatusMask { get; set; }
        public bool CRXApproval { get; set; }
        public string CRXNotes { get; set; }
        public int CRXResponseId { get; set; }
        public DateTime CRXReviewDate { get; set; }
        public int ReviewStatus { get; set; }
        public int ReviewerStatusMask { get; set; }
        public string ReviewerTextContent { get; set; }
        public bool? CRXTie { get; set; }
        public bool? ToolCorrectionRequested { get; set; }
    }

    public enum SupervisorReviewStatus
    {
        Approved = 1,
        Corrected = 2
    }
}
