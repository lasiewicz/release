﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class TextApprovalReviewHistoryCount
    {
        public int AdminMemberId { get; set; }
        public int TotalFTAProcessedCount { get; set; }
        public int TotalSampledCount { get; set; }
        public int TotalReviewedCount { get; set; }
        public int TotalApprovedCount { get; set; }
        public int TotalCorrectedCount { get; set; }
    }

    public enum HistoryCountType
    {
        Processed = 1,
        Sampled = 2,
        Reviewed = 3,
        Approved = 4,
        Corrected = 5
    }
}
