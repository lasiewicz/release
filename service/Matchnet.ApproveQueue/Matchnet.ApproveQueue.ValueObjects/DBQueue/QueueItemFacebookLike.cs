﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class QueueItemFacebookLike : QueueItemBase, IValueObject
    {
        public int AdminMemberId { get; set; }

        /// <summary>
        /// Date when item added to queue
        /// </summary>
        public DateTime InsertDate { get; set; }

        public QueueItemFacebookLike()
        {
        }

        public QueueItemFacebookLike(int communityId, int siteId, int brandId, int memberId, int adminMemberId, DateTime insertDate)
            : base(communityId, siteId, brandId, memberId)
        {
            AdminMemberId = adminMemberId;
            InsertDate = insertDate;
        }
    }
}
