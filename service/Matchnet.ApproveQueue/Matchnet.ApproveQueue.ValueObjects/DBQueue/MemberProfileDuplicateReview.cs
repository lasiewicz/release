﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class MemberProfileDuplicateReview : IValueObject
    {
        public int MemberProfileDuplicateHistoryID { get; set; }
        public int SupervisorID { get; set; }
        public int AdminID { get; set; }
        public int LanguageID { get; set; }
        public List<MemberProfileDuplicateReviewEntry> Entries { get; set; }
    }

    [Serializable]
    public class MemberProfileDuplicateReviewEntry : IValueObject, IComparable<MemberProfileDuplicateReviewEntry>
    {
        public int Ordinal { get; set; }
        public bool IsMatch { get; set; }
        public bool Suspended { get; set; }

        #region Implementation of IComparable<MemberProfileDuplicateReviewEntry>

        public int CompareTo(MemberProfileDuplicateReviewEntry other)
        {
            return Ordinal.CompareTo(other.Ordinal);
        }

        #endregion
    }
}
