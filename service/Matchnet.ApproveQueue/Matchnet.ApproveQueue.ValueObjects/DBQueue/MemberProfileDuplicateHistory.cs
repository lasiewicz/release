﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class MemberProfileDuplicateHistory : IValueObject
    {
        public int AdminId { get; set; }
        public int MemberProfileDuplicateHistoryId { get; set; }
        public int LanguageId { get; set; }
        public DateTime QueueInsertDate { get; set; }
        public List<MemberProfileDuplicateHistoryEntry> MemberProfileDuplicateHistoryEntries { get; set; }
    }

    [Serializable]
    public class MemberProfileDuplicateHistoryEntry : IValueObject, IComparable<MemberProfileDuplicateHistoryEntry>
    {
        public int MemberId { get; set; }
        public int CRXResponseId { get; set; }
        public FieldType FieldType { get; set; }
        public DateTime FieldValueOriginationDate { get; set; }
        public bool IsMatch { get; set; }
        public bool Suspended { get; set; }
        public bool AlreadySuspended { get; set; }
        public int OrdinalNumber { get; set; }
        public string TextContent { get; set; }

        #region Implementation of IComparable<MemberProfileDuplicateHistoryEntry>

        public int CompareTo(MemberProfileDuplicateHistoryEntry other)
        {
            return OrdinalNumber.CompareTo(other.OrdinalNumber);
        }

        #endregion
    }
}
