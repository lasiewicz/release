﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class AdminReviewSetting
    {
        public int AdminMemberId { get; set; }
        public int? DailySampleRate { get; set; }
        public int? OnDemandSampleRate { get; set; }
    }
}
