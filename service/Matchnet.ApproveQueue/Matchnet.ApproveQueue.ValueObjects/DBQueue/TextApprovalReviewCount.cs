﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class TextApprovalReviewCount : IValueObject
    {
        public int TotalItems { get; set; }
        public DateTime LatestRecordDate { get; set; }
        public DateTime OldRecordDate { get; set; }
        public int AdminMemberId { get; set; }
        public int SupervisorId { get; set; }
        public DateTime DateQueueAdded { get; set; }
    }
}
