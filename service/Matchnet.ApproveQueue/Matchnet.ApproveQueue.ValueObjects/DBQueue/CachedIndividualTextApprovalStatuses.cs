﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class CachedIndividualTextApprovalStatuses : ICacheable, IValueObject
    {

        List<IndividualTextApprovalStatus> _statuses = null;
        private int _CacheTTLSeconds;
        private CacheItemPriorityLevel _CachePriority;
        private static string _CacheKey = "^IndividualTextApprovalStatuses";
        
        public CachedIndividualTextApprovalStatuses(List<IndividualTextApprovalStatus> statuses)
        {
            _CachePriority = CacheItemPriorityLevel.Normal;
            _CacheTTLSeconds = 43200;
            _statuses = statuses;
        }

        public List<IndividualTextApprovalStatus> Statuses
        {
            get
            {
                return _statuses;
            }
        }

        #region ICacheable Members
        

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Absolute;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCacheKey()
        {
            return _CacheKey;
        }

        #endregion

        #region ICacheable Members

        CacheItemMode ICacheable.CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        CacheItemPriorityLevel ICacheable.CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        int ICacheable.CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        string ICacheable.GetCacheKey()
        {
            return _CacheKey;
        }

        #endregion
    }
}
