﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    /// <summary>
    /// Class used to retrieve the CRX TextContent by CRXResponseID. 
    /// </summary>
    [Serializable]
    public class CRXText
    {
        public int BrandId { get; set; }
        public int SiteId { get; set; }
        public int CommunityId { get; set; }
        public DateTime QueueInsertDate { get; set; }
        public string TextContent { get; set; }
    }
}
