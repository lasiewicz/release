﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    /// <summary>
    /// Class to store information about Member for possible fraud review
    /// </summary>
    [Serializable]
    public class QueueItemMember : QueueItemBase
    {
        /// <summary>
        /// Admin who marked the member as 'possible fraud'
        /// </summary>
        public int AdminMemberId { get; set; }
        
        /// <summary>
        /// Date when item added to queue
        /// </summary>
        public DateTime InsertDate { get; set; }

        /// <summary>
        /// Supervisor who reviewed the member fraud item
        /// </summary>
        public int ReviewerID { get; set; }

        /// <summary>
        /// Member is fraud or not
        /// </summary>
        public bool IsFraud { get; set; }

        public QueueItemMember(int communityId, int siteId, int brandId, int memberId, int adminMemberId)
            : base(communityId, siteId, brandId, memberId)
        {
            AdminMemberId = adminMemberId;
        }

        public QueueItemMember(int communityId, int siteId, int brandId, int memberId, int adminMemberId,int reviewerId, bool isFraud)
            : base(communityId, siteId, brandId, memberId)
        {
            AdminMemberId = adminMemberId;
            ReviewerID = reviewerId;
            IsFraud = isFraud;
        }

        public QueueItemMember(int communityId, int siteId, int brandId, int memberId, int adminMemberId, DateTime insertDate)
            : base(communityId, siteId, brandId, memberId)
        {
            AdminMemberId = adminMemberId;
            InsertDate = insertDate;
        }

    }
}
