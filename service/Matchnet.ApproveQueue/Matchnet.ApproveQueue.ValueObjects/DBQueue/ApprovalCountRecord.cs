﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    public enum CountType
    {
        Photo=0,
        RegularText=1,
        QAText=2,
        FacebookLike=3
    }
    
    [Serializable]
    public class ApprovalCountRecord
    {
        public int AdminMemberID { get; set; }
        public int PhotoApprovalCount { get; set; }
        public int TextApprovalCount { get; set; }
        public int QAApprovalCount { get; set; }
        public int FacebookLikeApprovalCount { get; set; }

        public ApprovalCountRecord() { }

        public ApprovalCountRecord(int adminMemberID, int photoApprovalCount, int textApprovalCount, int qaApprovalCount, int fbLikeApprovalCount)
        {
            AdminMemberID = adminMemberID;
            PhotoApprovalCount = photoApprovalCount;
            TextApprovalCount = textApprovalCount;
            QAApprovalCount = qaApprovalCount;
            FacebookLikeApprovalCount = fbLikeApprovalCount;
        }
    }
}
