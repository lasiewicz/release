﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class QueueItemText: QueueItemBase, IValueObject
    {
        public int MemberTextApprovalAttributeID { get; set; }
        public int LanguageID { get; set; }
        public TextType TextType { get; set; }
        public int MemberAttributeGroupID { get; set; }
        public string TextContent { get; set; }
        public string IPAddress { get; set; }
        public int ProfileRegionId { get; set; }
        public int GenderMask { get; set; }
        public int MaritalStatus { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OccupationDescription { get; set; }
        public int EducationLevel { get; set; }
        public int Religion { get; set; }
        public int Ethnicity { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string SubscriptionStatus { get; set; }
        public DateTime RegistrationDate { get; set; }
        public bool PopulateMemberInfo { get; set; }
        public int MemberTextApprovalID { get; set; }
        public DateTime InsertDate { get; set; }
        public int DaysSinceFirstSubscription { get; set; }
        public int CRXResponseID { get; set; }
        public bool CRXApproval { get; set; }
        public float CRXApprovalDisapprovalWeight { get; set; }
        public string CRXNotes { get; set; }
        public bool CRXAdminVerify { get; set; }
        public bool UpdateCRXOnly { get; set; }
        public bool IsExternal { get; set; }
        public string MingleTableColumnName { get; set; }
        public string OldTextContent { get; set; }
        public List<DateTime> CRXResetDates { get; set; }

        public QueueItemText() { CRXResetDates = new List<DateTime>();}
        public QueueItemText(int communityID, int siteID, int brandID, int memberID, int languageID, TextType textType)
            : base(communityID, siteID, brandID,  memberID)
        {
            LanguageID = languageID;
            TextType = textType;
        }

        public QueueItemText(int communityID, int siteID, int brandID, int memberID, int languageID, TextType textType, int memberAttributeGroupID, string textContent)
            : base(communityID, siteID, brandID, memberID)
        {
            LanguageID = languageID;
            TextType = textType;
            MemberAttributeGroupID = memberAttributeGroupID;
            TextContent = textContent;
        }
    }
}
