﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class QueueItemMemberProfileDuplicateReview : QueueItemLightBase
    {
        public MemberProfileDuplicateHistory MemberProfileDuplicateHistory { get; set; }
    }
}
