﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public enum ApprovalStatus
    {
        Inserted=1,
        Pending=2,
        Completed=3,
        CompletedPossibleFraud=4,
        CompletedButSuspended=5
    }

    [Serializable]
    public enum TextType
    {
        Regular = 1,
        QA = 2
    }

    [Serializable]
    public enum QueueItemType
    {
        Photo = 1,
        Text = 2,
        QuestionAnswer = 3,
        FacebookLike = 4,
        CrxSecondReview = 5,
        MemberProfileDuplicate = 6,
        MemberProfileDuplicateReview = 7
    }

    [Serializable]
    public class QueueItemBase : IValueObject
    {
        public int MemberID { get; set; }
        public int BrandID { get; set; }
        public int SiteID { get; set; }
        public int CommunityID { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime StatusDate { get; set; }

        public QueueItemBase() { }
        public QueueItemBase(int communityID, int siteID, int brandID, int memberID)
        {
            CommunityID = communityID;
            SiteID = siteID;
            BrandID = brandID;
            MemberID = memberID;
        }
    }
}
