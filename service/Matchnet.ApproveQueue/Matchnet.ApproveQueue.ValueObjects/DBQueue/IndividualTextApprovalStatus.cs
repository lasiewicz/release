﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    public enum IndividualTextApprovalStatusMask
    {
        Approve=0,
        Fraud=1
    }

    [Serializable]
    public class IndividualTextApprovalStatus: IValueObject
    {
        public int StatusMask { get; private set; }
        public string Description { get; private set; }
        public bool Display { get; private set; }

        public IndividualTextApprovalStatus(int statusMask, string description, bool display)
        {
            StatusMask = statusMask;
            Description = description;
            Display = display;
        }
    }
}
