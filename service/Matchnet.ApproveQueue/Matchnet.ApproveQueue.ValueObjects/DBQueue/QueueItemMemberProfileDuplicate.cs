﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    public enum FieldType
    {
        Unknown = 0,
        AboutMe = 1
    }

    public enum DuplicateStatus
    {
        Undetermined = 0,
        Valid = 1,
        Fraud = 2
    }

    [Serializable]
    public class QueueItemMemberProfileDuplicate : QueueItemLightBase, IValueObject
    {
        public int QueueItemMemberProfileDuplicateID { get; set; }
        public int LanguageID { get; set; }
        public List<MemberProfileDuplicate> MemberProfileDuplicates { get; set; }
    }

    [Serializable]
    public class MemberProfileDuplicate : IValueObject, IComparable<MemberProfileDuplicate>
    {
        public int MemberProfileDuplicateId { get; set; }
        public int MemberId { get; set; }
        public int CRXResponseId { get; set; }
        public FieldType FieldType { get; set; }
        public string FieldValue { get; set; }
        public DateTime FieldValueOriginationDate { get; set; }
        public DuplicateStatus DuplicateStatus { get; set; }
        public int OrdinalNumber { get; set; }

        #region Implementation of IComparable<MemberProfileDuplicate>

        public int CompareTo(MemberProfileDuplicate other)
        {
            return OrdinalNumber.CompareTo(other.OrdinalNumber);
        }

        #endregion
    }
}
