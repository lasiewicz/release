﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    public enum QueueCountGroupType
    {
        Community=0,
        Site=1,
        Language=2
    }
    
    [Serializable]
    public class QueueCountRecord
    {
        public int ItemCount { get; set; }
        public int ChildCount { get; set; }
        public int GroupID { get; set; }
        public int MaxHours { get; set; }
        public decimal AverageHours { get; set; }
        public QueueCountGroupType GroupType { get; set; }

        public QueueCountRecord() { }

        public QueueCountRecord(int itemCount, int groupID, int maxHours, int averageHours, QueueCountGroupType groupType)
        {
            ItemCount = itemCount;
            GroupID = groupID;
            MaxHours = maxHours;
            AverageHours = averageHours;
            GroupType = groupType;
        }
    }
}
