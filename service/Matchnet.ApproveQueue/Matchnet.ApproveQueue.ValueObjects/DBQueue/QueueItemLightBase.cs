﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.DBQueue
{
    [Serializable]
    public class QueueItemLightBase : IValueObject
    {
        public ApprovalStatus ApprovalStatus { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
