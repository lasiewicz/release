﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTAnswerMemberProfile
    {
        public int TTMemberProfileID { get; set; }
        public bool Suspend { get; set; }
        public bool WarningContactInfo { get; set; }
        public bool WarningInappropriateContent { get; set; }
        public bool WarningGeneralViolation { get; set; }
        public bool WarningNoWarning
        {
            get
            {
                return (!WarningContactInfo && !WarningInappropriateContent && !WarningGeneralViolation);
            }
        }
        public int Version { get; set; }
        public List<TTAnswerSuspendReason> SuspendReasons { get; set; } 
    }
}
