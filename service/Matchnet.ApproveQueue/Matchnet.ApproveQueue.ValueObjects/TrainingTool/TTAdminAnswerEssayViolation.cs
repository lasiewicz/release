﻿using System;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTAdminAnswerEssayViolation
    {
        public int BadReasonID { get; set; }
        public bool BadReasonIsCorrect { get; set; }
    }
}