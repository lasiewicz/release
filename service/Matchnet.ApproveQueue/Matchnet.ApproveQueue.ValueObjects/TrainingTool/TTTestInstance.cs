﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTTestInstance
    {
        public int TTTestInstanceID { get; set; }
           public int TTGroupingID { get; set; }
           public int AdminMemberID { get; set; }
           public TTTestInstanceStatus Status { get; set; }
           public DateTime StartDate { get; set; }
           public DateTime EndDate { get; set; }
           public DateTime KeepAliveContactDate { get; set; }
           public DateTime InsertDate { get; set; }
           public DateTime UpdateDate { get; set; }
    }
}
