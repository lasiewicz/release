﻿using System;
using System.Collections.Generic;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTAdminAnswer
    {
        public int TTTestInstanceID { get; set; }
        public int TTMemberProfileID { get; set; }
        public int SuspendReason { get; set; }
        public bool Suspend { get; set; }
        public bool WContactInfo { get; set; }
        public bool WInappropriateContent { get; set; }
        public bool WGeneralViolation { get; set; }
        public bool WNoWarning { get; set; }
        public bool PossibleFraud { get; set; }
        public List<TTAdminAnswerEssay> AdminAnswerEssays { get; set; }
        public int TTAnswerMemberProfileVersion { get; set; }
        public int TTAnswerSuspendReasonsVersion { get; set; }
    }
}