﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMemberAttributeDate : TTMemberAttributeBase
    {
        public DateTime FieldValue { get; set; }
        public bool IsFutureTimespan { get; set; }
    }
}
