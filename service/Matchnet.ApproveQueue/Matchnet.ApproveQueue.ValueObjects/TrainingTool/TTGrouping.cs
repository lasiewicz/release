﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTGrouping
    {
        public int TTGroupingID { get; set; }
        public string TTGroupingName { get; set; }
    }
}
