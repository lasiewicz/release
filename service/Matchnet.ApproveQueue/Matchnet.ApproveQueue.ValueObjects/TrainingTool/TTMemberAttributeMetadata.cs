﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMemberAttributeMetadata
    {
        public int TTAttributeID { get; set; }
        public string MemberAttributeName { get; set; }
        public TTAttributeDataType AttributeDataType { get; set; }
    }
}
