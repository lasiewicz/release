﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMessage
    {
        public int TTMessageID { get; set; }
        public int TTMemberProfileID { get; set; }
        public int MailID { get; set; }
        public int GroupID { get; set; }
        public int FromMemberID { get; set; }
        public int ToMemberID { get; set; }
        public DateTime InsertDate { get; set; }
        public int StatusMask { get; set; }
        public int MailTypeID { get; set; }
        public int MailOption { get; set; }
        public string MessageHeader { get; set; }
        public string MessageBody { get; set; }
    }
}
