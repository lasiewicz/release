﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTAdminAnswerReport
    {
        public List<TTAdminAnswer> TtAdminAnswers { get; set; }
        public List<TTAdminAnswerEssay> TtAdminAnswerEssays { get; set; }
        public List<TTAdminAnswerEssayViolation> TtAdminAnswerEssayViolations { get; set; } 
    }
}
