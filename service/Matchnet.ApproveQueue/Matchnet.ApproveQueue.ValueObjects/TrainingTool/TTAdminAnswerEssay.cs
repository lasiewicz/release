﻿using System;
using System.Collections.Generic;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTAdminAnswerEssay
    {
        public int TTMemberAttributeTextID { get; set; }
        public bool IsGood { get; set; }
        public int BadReasonMask { get; set; }
        public int TTAnswerMemberAttributeTextVersion { get; set; }
    }
}