﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMemberAttributeBase
    {
        public int TTMemberProfileID { get; set; }
        public int TTAttributeID { get; set; }
        public string MemberAttributeName { get; set; }
        public TTAttributeDataType TTAttributeDataType { get; set; }
        public bool DisplayOnly { get; set; }
    }
}
