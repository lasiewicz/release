﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMemberAttributeText : TTMemberAttributeBase
    {
        public int TTMemberAttributeTextID { get; set; }
        public string FieldValue { get; set; }
        public bool IncludeInTest { get; set; }
        public bool IsGood { get; set; }
        public int BadReasonMask { get; set; }
        public int AnswerVersion { get; set; }
    }
}
