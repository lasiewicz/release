﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTAnswerSuspendReason
    {
        public int TTMemberProfileID { get; set; }
        public int ReasonID { get; set; }
        public int Version { get; set; }
    }
}
