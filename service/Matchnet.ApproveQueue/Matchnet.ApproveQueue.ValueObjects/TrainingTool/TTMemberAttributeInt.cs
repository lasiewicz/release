﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMemberAttributeInt : TTMemberAttributeBase
    {
        public int FieldValue { get; set; }
    }
}
