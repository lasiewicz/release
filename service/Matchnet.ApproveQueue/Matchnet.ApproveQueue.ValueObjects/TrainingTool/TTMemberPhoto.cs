﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    [Serializable]
    public class TTMemberPhoto
    {
        public int Ordinal { get; set; }
        public string CloudPath { get; set; }
        public string ThumbnailCloudPath { get; set; }
    }
}
