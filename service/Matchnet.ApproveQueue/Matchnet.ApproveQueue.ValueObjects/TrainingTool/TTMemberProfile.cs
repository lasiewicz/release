﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ApproveQueue.ValueObjects.TrainingTool
{
    public enum TTAttributeDataType
    {
        Number = 1,
        Text = 2,
        Date = 3,
        Timespan = 4,
        Special = 5,
        SpecialNumber = 6,
        SpecialTimespan = 7
    }

    public enum TTTestInstanceStatus
    {
        Started = 0,
        InProgress = 1,
        Paused = 2,
        Ended = 3
    }

    [Serializable]
    public class TTMemberProfile
    {
        public int TTMemberProfileID { get; set; }
        public int MemberID { get; set; }
        public int BrandID { get; set; }
        public int TTGroupingID { get; set; }
        public string GroupingName { get; set; }
        public bool HasAnswer { get; set; }

        public List<TTMemberAttributeText> MemberAttributeTexts { get; set; }
        public List<TTMemberAttributeInt> MemberAtributeInts { get; set; }
        public List<TTMemberAttributeDate> MemberAttributeDates { get; set; }

        public string GetMemberAttributeText(string attributeName)
        {
            if (MemberAttributeTexts == null)
                return Constants.NULL_STRING;

            var query = (from a in MemberAttributeTexts
                         where a.MemberAttributeName.ToLower() == attributeName.ToLower()
                         select a).SingleOrDefault();

            return query == null ? Constants.NULL_STRING : query.FieldValue;
        }

        public int GetMemberAttributeInt(string attributeName)
        {
            if (MemberAtributeInts == null)
                return Constants.NULL_INT;

            var query = (from a in MemberAtributeInts
                         where a.MemberAttributeName.ToLower() == attributeName.ToLower()
                         select a).SingleOrDefault();

            return query == null ? Constants.NULL_INT : query.FieldValue;
        }
          
        public DateTime GetMemberAttributeDate(string attributeName)
        {
            if (MemberAttributeDates == null)
                return DateTime.MinValue;

            var query = (from a in MemberAttributeDates
                         where a.MemberAttributeName.ToLower() == attributeName.ToLower()
                         select a).SingleOrDefault();

            return query == null ? DateTime.MinValue : query.FieldValue;
        }

        public DateTime GetMemberAttributeTimespan(string attributeName, out bool isFutureTimespan)
        {
            if (MemberAttributeDates == null)
            {
                isFutureTimespan = false;
                return DateTime.MinValue;
            }

            isFutureTimespan = false;
            DateTime returnDate = DateTime.MinValue;

            var query = (from a in MemberAttributeDates
                         where a.MemberAttributeName.ToLower() == attributeName.ToLower()
                         select a).SingleOrDefault();

            if (query != null)
            {
                returnDate = query.FieldValue;
                isFutureTimespan = query.IsFutureTimespan;
            }
            
            return returnDate;
        }
    }
}
