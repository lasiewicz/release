﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using DBQ = Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace Matchnet.ApproveQueue.ValueObjects.ServiceDefinitions
{
    public interface IDBApproveQueueService
    {
        void EnqueueLight(QueueItemLightBase queueItem);
        void Enqueue(List<DBQ.QueueItemBase> queueItems);
        void Enqueue(DBQ.QueueItemBase queueItem);
        void CompletePhotoApproval(int memberPhotoID, DBQ.ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask);
        void CompleteTextApproval(int memberID, int communityID, int languageID, DBQ.TextType textType, DBQ.ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask);
        void SaveIndividualTextAttributeApproval(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit);
        void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity);
        void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity, int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate);

        void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
                                                         int statusMask, int adminMemberID, string textContent,
                                                         bool isEdit, string ip, int profileRegionId, int genderMask,
                                                         int maritalStatus, string firstName, string lastName,
                                                         string email, string occupationDescription, int educationLevel,
                                                         int religion, int ethnicity, int daysSinceFirstSubscription,
                                                         string billingPhoneNumber, string subscriptionStatus,
                                                         DateTime registrationDate, int? memberTextApprovalID,
                                                         string adminTextContent);
        void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity, int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, int? memberTextApprovalID, string adminTextContent, DBQ.ApprovalStatus reviewStatus);
        void DeletePhotoQueueItem(int memberPhotoID);
        CRXText LocateCRXText(int crxResponseId);
        QueueItemMemberProfileDuplicate DequeueMemberProfileDuplicateByLanguage(int languageId);
        QueueItemMemberProfileDuplicateReview DequeueMemberProfileDuplicateReview(int languageID);
        QueueItemMemberProfileDuplicateReview GetMemberProfileDuplicateReviewQueueItem(int memberProfileDuplicateHistoryID);
        QueueItemMemberProfileDuplicate GetMemberProfileDuplicateQueueItem(int duplicateQueueID);
        DBQ.QueueItemText DequeueTextByLanguage(Int32 languageID, DBQ.TextType textType);
        DBQ.QueueItemText DequeueTextByMember(Int32 memberID, DBQ.TextType textType);
        DBQ.QueueItemText DequeueTextBySite(Int32 siteID, DBQ.TextType textType);
        DBQ.QueueItemText DequeueTextByCommunity(Int32 communityID, DBQ.TextType textType);
        List<DBQ.ApprovalCountRecord> GetApprovalHistoryCounts(DateTime startDate, DateTime endDate, int adminMemberID, int communityID, int siteID);
        List<MemberProfileDuplicateHistoryCount> GetMemberProfileDuplicateHistoryCount(DateTime startDate,
                                                                                              DateTime endDate,
                                                                                              int adminMemberID);
        List<DBQ.QueueCountRecord> GetQueueCounts(DBQ.QueueItemType itemType);
        List<DBQ.QueueItemBase> DequeuePhotosByCommunity(int maxCount, int communityID);
        List<DBQ.QueueItemBase> DequeuePhotosBySite(int maxCount, int siteID);
        List<DBQ.QueueItemBase> DequeuePhotosByMember(int maxCount, int memberID);
        List<DBQ.IndividualTextApprovalStatus> GetIndividualTextApprovalStatuses();
        List<DBQ.QueueItemBase> GetApprovalPhotoQueueItems(int siteID);
        void SaveMemberTextApprovalAttributes(List<DBQ.QueueItemText> queueItemTexts);
        List<DBQ.QueueItemMember> DequeueMemberBySite(Int32 siteID);
        void CompleteMemberFraudReview(List<DBQ.QueueItemMember> memberItems);

        List<DBQ.QueueItemText> GetTextAttributesForCRXApproval(int? communityId, int? languageId, int? textType, int? numberOfRows);
        void CompleteCRXTextAttributeArroval(DBQ.QueueItemText itemText);

        void SaveAdminReviewSettings(int adminMemberId, int? dailySampleRate, int? onDemandSampleRate, bool isEdit);
        DBQ.TextApprovalReviewCount GetTextApprovalReviewCounts(bool isOnDemand);
        DBQ.ReviewText GetTextToReview(bool isOnDemand, int? supervisorId, int? adminMemberId);
        void CompleteAdminTextApprovalReview(DBQ.ReviewText reviewText);
        void AcknowledgeTrainingReport(int memberTextApprovalId, int adminMemberId, int supervisorId, bool isOnDemand);
        List<DBQ.TextApprovalReviewHistoryCount> GetTextApprovalReviewHistoryCounts(DateTime startDate, DateTime endDate,
                                                                                int? adminMemberId, int? communityId);

        DBQ.QueueItemText GetCRXTextAttributeInfo(int CRXResponseId);
        List<DBQ.AdminReviewSetting> GetAdminReviewSampleRates();
        void SaveAdminReviewSettings(List<DBQ.AdminReviewSetting> adminSettings);
        List<DBQ.QueueItemText> GetApprovalTextAttributesCRXResponse(int MemberTextApprovalID);
        DBQ.ReviewText GetCRXTextForSecondReview();
        void CompleteCRXSecondReview(DBQ.ReviewText reviewText);
        DBQ.TextApprovalReviewCount GetCRXSecondReviewCounts();
        void PopulateOnDemandTextReview(DateTime startDate, DateTime endDate, int? communityId, int supervisorId,
                                        List<DBQ.AdminReviewSetting> adminRates);
        List<DBQ.TextApprovalReviewCount> GetOnDemandReviewQueueCounts();
        void PurgeOnDemandReviewQueue(int adminMemberId, int supervisorId);
        void SaveMemberProfileDuplicate(QueueItemMemberProfileDuplicate queueItem);
        void SaveMemberProfileDuplicateHistory(MemberProfileDuplicateHistory history, int memberProfileDuplicateQueueId);
        void DeleteMemberProfileDuplicateQueueItem(int MemberProfileDuplicateQueueID);
        void SaveMemberProfileDuplicateReview(MemberProfileDuplicateReview duplicateReview);

        DateTime GetLastCRXResponseResetTime(int CRXResponseID);
        DBQ.QueueItemText GetCRXResponse(int CRXResponseId);
        void RevertCRXResponse();
        List<DBQ.QueueItemText> VerifyCRXResponse(int? communityId, int? languageId, int? textType, int? numberOfRows);
   
        void DeleteFacebookLikeQueueItem(int memberID, int siteID);
        List<DBQ.QueueItemBase> DequeueFacebookLikesByCommunity(int maxCount, Int32 communityID);
        List<DBQ.QueueItemBase> DequeueFacebookLikesBySite(int maxCount, Int32 siteID);
        List<DBQ.QueueItemBase> DequeueFacebookLikesByMember(int maxCount, Int32 memberID);
        void CompleteFacebookLikesApproval(int memberId, int siteID, DBQ.ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask);

        void CreateNewGrouping(string groupingName);
        List<TTGrouping> GetGroupingList();
        void InsertTTMemberProfiles(string groupingName, List<TTMemberProfile> memberProfiles);
        void InsertTTMemberProfiles(int groupingID, List<TTMemberProfile> memberProfiles);
        List<TTMemberProfile> GetTTMemberProfiles(int groupingID, int pageNumber, int rowsPerPage, out int totalRows);
        List<TTMemberProfile> GetTTMemberProfiles(int memberID);
        List<QueueItemText> GetTextAttributesForMember(int memberId, int communityId, int languageId, TextType textType);

        void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity, int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, int? memberTextApprovalID, string adminTextContent, ApprovalStatus reviewStatus, bool deleteMovedRows);

        void CompleteTextApprovalMemberApproval(int memberID, int memberTextApprovalID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask);

        List<TTMemberAttributeText> GetAllTTMemberTextAttributes(int memberID, int groupingID);

        List<TTMemberAttributeText> GetAllTTMemberTextAttributesWithAnswer(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileID);

        void SaveTTAnswer(TTAnswerMemberProfile profile, List<TTMemberAttributeText> texts);

        List<TTMemberAttributeText> GetTTAnswerMemberAttributeTexts(int ttTestInstanceID);
        List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int ttTestInstanceID);
        
        List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileI);
        int CreateNewTestInstance(int groupingId, int adminId);
        void SaveAdminAnswer(TTAdminAnswer adminAnswer, bool isSynchronousWrite);
        List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceId, int? ttMemberProfileIdToExclude);

        List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                   int? ttMemberProfileIDToExclude,
                                                                   out int remainingCount,
                                                                   out int ttAnswerMemberProfileVersion,
                                                                   out int ttAnswerSuspendReasonsVersion);
        
        int GetTTMemberProfileCount(int ttTestInstanceId, bool answeredOnly);
        TTMemberProfile GetTTMemberAttributes(int ttMemberProfileId);
        List<TTGrouping> GetCompletedGroupingList();
        TTAdminAnswerReport GetTTAdminAnswerReport(int ttTestInstanceId);
        void UpdateTestStatus(int ttTestInstanceId, TTTestInstanceStatus testStatus);
        void TrainingToolKeepalivePulse(int ttTestInstanceId);
        List<TTTestInstance> GetTTTestInstances(int adminMemberId);
        double GetTotalTimeSpentOnTest(int ttTestInstanceId, out bool testFinished);
        TTGrouping GetTTGrouping(int ttGroupingId);
        TTTestInstance GetTTTestInstance(int ttTestInstanceId);
        void UpdateTTMemberProfileTTGroupingID(int ttMemberProfileId, int newGroupingId);
        TTMemberProfile GetTTMemberProfile(int memberId, int ttGroupingId);
        List<TTMemberPhoto> GetTTMemberPhotos(int memberId, int groupingId);
        List<TTMemberPhoto> GetTTMemberPhotos(int ttMemberProfileID);
        List<TTMessage> GetTTMessages(int ttMemberProfileId, bool populateMessageBody);
        TTMessage GetTTMessage(int ttMessageId);
    }
}
