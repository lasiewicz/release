using System;

namespace Matchnet.ApproveQueue.ValueObjects
{
	/// <summary>
	/// </summary>
	[Serializable]
	public class PhotoPanelInfoCollection : System.Collections.CollectionBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public PhotoPanelInfoCollection()
		{

		}

		/// <summary>
		/// Integer indexer
		/// </summary>
		public PhotoPanelInfo this[int index]
		{
			get { return ((PhotoPanelInfo)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded Add method
		/// </summary>
		/// <param name="region"></param>
		/// <returns></returns>
		public int Add(PhotoPanelInfo photoPanelInfo)
		{
			return base.InnerList.Add(photoPanelInfo);
		}
	}
}
