﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.PhotoFile.ServiceAdapters;
using Spark.CloudStorage;
using Spark.Logging;
using Client = Matchnet.Data.Client;
using ApproveQueueVO = Matchnet.ApproveQueue.ValueObjects;

namespace Matchnet.ApproveQueue.BusinessLogic
{
    public class TrainingToolBL
    {
        public readonly static TrainingToolBL Instance = new TrainingToolBL();

        private ISettingsSA _settingsService;
        private List<TTMemberAttributeMetadata> _ttAttributeMetadatas = null;
        private const string CLASS_NAME = "TrainingToolBL";

        #region Properties
        public ISettingsSA SettingsService
        {
            get
            {
                if (_settingsService == null)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }
        public List<TTMemberAttributeMetadata> TTAttributeMetadatas
        {
            get
            {
                if(_ttAttributeMetadatas == null)
                {
                    _ttAttributeMetadatas = getTTAttributeMetadatas();
                }

                return _ttAttributeMetadatas;
            }
        }
        #endregion

        #region Public methods
        public TTMessage GetTTMessage(int ttMessageID)
        {
            SqlDataReader reader = null;

            try
            {
                var command = new Command("mnAdmin", "up_TTMessage_Get", 0);
                command.AddParameter("@TTMessageID", SqlDbType.Int, ParameterDirection.Input, ttMessageID);

                reader = Client.Instance.ExecuteReader(command);

                if (reader == null)
                    return null;

                TTMessage ttMessage = null;
                if (reader.Read())
                {
                    ttMessage = new TTMessage
                        {
                            TTMessageID = Convert.ToInt32(reader["TTMessageID"]),
                            TTMemberProfileID = Convert.ToInt32(reader["TTMemberProfileID"]),
                            MailID = Convert.ToInt32(reader["MailID"]),
                            GroupID = Convert.ToInt32(reader["GroupID"]),
                            FromMemberID = Convert.ToInt32(reader["FromMemberID"]),
                            ToMemberID = Convert.ToInt32(reader["ToMemberID"]),
                            InsertDate = Convert.ToDateTime(reader["InsertDate"]),
                            StatusMask = Convert.ToInt32(reader["StatusMask"]),
                            MailTypeID = reader["MailTypeID"] == null ? 0 : Convert.ToInt32(reader["MailTypeID"]),
                            MailOption = reader["MailOption"] == null ? 0 : Convert.ToInt32(reader["MailOption"]),
                            MessageHeader =
                                reader["MessageHeader"] == null ? string.Empty : reader["MessageHeader"].ToString(),
                            MessageBody = reader["MessageBody"] == null
                                              ? string.Empty
                                              : reader["MessageBody"].ToString()
                        };
                }

                return ttMessage;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get the single TTMessage.", ex);
            }
            finally
            {
                if(reader != null)
                    reader.Close();
            }
        }

        public List<TTMessage> GetTTMessages(int ttMemberProfileID, bool populateMessageBody)
        {
            try
            {
                var command = new Command("mnAdmin", "up_TTMessage_List", 0);
                command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);

                var dt = Client.Instance.ExecuteDataTable(command);
                if (dt == null)
                    return null;

                var ttMessages = new List<TTMessage>();
                foreach (DataRow row in dt.Rows)
                {
                    var newMessage = new TTMessage
                        {
                            TTMessageID = Convert.ToInt32(row["TTMessageID"]),
                            TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                            MailID = Convert.ToInt32(row["MailID"]),
                            GroupID = Convert.ToInt32(row["GroupID"]),
                            FromMemberID = Convert.ToInt32(row["FromMemberID"]),
                            ToMemberID = Convert.ToInt32(row["ToMemberID"]),
                            InsertDate = Convert.ToDateTime(row["InsertDate"]),
                            StatusMask = Convert.ToInt32(row["StatusMask"]),
                            MailTypeID = row["MailTypeID"] == null ? 0 : Convert.ToInt32(row["MailTypeID"]),
                            MailOption = row["MailOption"] == null ? 0 : Convert.ToInt32(row["MailOption"]),
                            MessageHeader =
                                row["MessageHeader"] == null ? string.Empty : row["MessageHeader"].ToString()
                        };

                    if (populateMessageBody)
                    {
                        newMessage.MessageBody = row["MessageBody"] == null
                                                     ? string.Empty
                                                     : row["MessageBody"].ToString();
                    }

                    ttMessages.Add(newMessage);
                }

                return ttMessages;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get TTMessages.", ex);
            }
        }
        
        public TTAdminAnswerReport GetTTAdminAnswerReport(int ttTestInstanceID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTAdminAnswer_GetAllAnswers", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);

                var ds = Client.Instance.ExecuteDataSet(command);
                if(ds == null || ds.Tables.Count != 2)
                {
                    return null;
                }

                var answerReport = new TTAdminAnswerReport();
                answerReport.TtAdminAnswers = new List<TTAdminAnswer>();
                answerReport.TtAdminAnswerEssays = new List<TTAdminAnswerEssay>();
                answerReport.TtAdminAnswerEssayViolations = new List<TTAdminAnswerEssayViolation>();

                var ansDt = ds.Tables[0];
                foreach (DataRow row in ansDt.Rows)
                {
                    answerReport.TtAdminAnswers.Add(new TTAdminAnswer()
                        {
                            TTTestInstanceID = Convert.ToInt32(row["TTTestInstanceID"]),
                            TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                            PossibleFraud = Convert.ToBoolean(row["PossibleFraud"]),
                            Suspend = Convert.ToBoolean(row["Suspend"]),
                            WContactInfo = Convert.ToBoolean(row["WContactInfo"]),
                            WInappropriateContent = Convert.ToBoolean(row["WInappropriateContent"]),
                            WGeneralViolation = Convert.ToBoolean(row["WGeneralViolation"]),
                            WNoWarning = Convert.ToBoolean(row["WNoWarning"]),
                            SuspendReason = Convert.ToInt32(row["SuspendReason"])
                        });
                }

                var essayDt = ds.Tables[1];
                foreach (DataRow row in essayDt.Rows)
                {
                    answerReport.TtAdminAnswerEssays.Add(new TTAdminAnswerEssay()
                        {
                            TTMemberAttributeTextID = Convert.ToInt32(row["TTMemberAttributeTextID"]),
                            IsGood = Convert.ToBoolean(row["IsGood"]),
                            BadReasonMask = Convert.ToInt32(row["BadReasonMask"])
                        });
                }

                return answerReport;

            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get TTAdminAnswerReport.", ex);
            }
        }

        /// <summary>
        /// Gets all the member attributes for a given TTMemberProfileID (which represents a MemberID + GroupingID combo).
        /// </summary>
        /// <param name="ttMemberProfileID"></param>
        /// <returns></returns>
        public TTMemberProfile GetTTMemberAttributes(int ttMemberProfileID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTMemberAttribute_List", 0);
                command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);

                var ds = Client.Instance.ExecuteDataSet(command);
                if (ds == null)
                {
                    return null;
                }

                var ttMemberProfile = new TTMemberProfile();
                ttMemberProfile.MemberAttributeTexts = new List<TTMemberAttributeText>();
                ttMemberProfile.MemberAtributeInts = new List<TTMemberAttributeInt>();
                ttMemberProfile.MemberAttributeDates = new List<TTMemberAttributeDate>();

                // main TTMemberProfile info
                var mainDt = ds.Tables[0];
                var onlyRow = mainDt.Rows[0];
                ttMemberProfile.TTMemberProfileID = ttMemberProfileID;
                ttMemberProfile.MemberID = Convert.ToInt32(onlyRow["MemberID"]);
                ttMemberProfile.BrandID = Convert.ToInt32(onlyRow["BrandID"]);
                ttMemberProfile.TTGroupingID = Convert.ToInt32(onlyRow["TTGroupingID"]);

                // Text attributes
                var textDt = ds.Tables[1];
                foreach (DataRow row in textDt.Rows)
                {
                    ttMemberProfile.MemberAttributeTexts.Add(new TTMemberAttributeText()
                        {
                            TTMemberAttributeTextID = Convert.ToInt32(row["TTMemberAttributeTextID"]),
                            TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                            FieldValue = row["Value"].ToString(),
                            MemberAttributeName = row["MemberAttributeName"].ToString(),
                            TTAttributeDataType = (TTAttributeDataType)Convert.ToInt32(row["DataTypeID"])
                        });
                }

                // Int attributes
                var intDt = ds.Tables[2];
                foreach (DataRow row in intDt.Rows)
                {
                    ttMemberProfile.MemberAtributeInts.Add(new TTMemberAttributeInt()
                        {
                            TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                            FieldValue = Convert.ToInt32(row["Value"]),
                            MemberAttributeName = row["MemberAttributeName"].ToString(),
                            TTAttributeDataType = (TTAttributeDataType)Convert.ToInt32(row["DataTypeID"])
                        });
                }

                // DateTime / Timespan attributes
                var dateDt = ds.Tables[3];
                foreach (DataRow row in dateDt.Rows)
                {
                    ttMemberProfile.MemberAttributeDates.Add(new TTMemberAttributeDate()
                        {
                            TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                            FieldValue = Convert.ToDateTime(row["Value"]),
                            IsFutureTimespan = row["IsFutureTimespan"] == DBNull.Value ? false : Convert.ToBoolean(row["IsFutureTimespan"]),
                            MemberAttributeName = row["MemberAttributeName"].ToString(),
                            TTAttributeDataType = (TTAttributeDataType)Convert.ToInt32(row["DataTypeID"])
                        });
                }

                return ttMemberProfile;

            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the training tool member attributes.", ex);
            }
        }

        public List<TTGrouping> GetCompletedGroupingList()
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTGrouping_ListOnlyCompleted", 0);

                var dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null)
                {
                    return null;
                }

                var groupingList = new List<TTGrouping>();
                foreach (DataRow row in dt.Rows)
                {
                    groupingList.Add(new TTGrouping()
                    {
                        TTGroupingID = Convert.ToInt32(row["TTGroupingID"]),
                        TTGroupingName = row["GroupingName"].ToString()
                    });
                }

                return groupingList;

            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the list of completed groupings.", ex);
            }
        }

        public List<TTGrouping> GetGroupingList()
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTGrouping_List", 0);

                var dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null)
                {
                    return null;
                }

                var groupingList = new List<TTGrouping>();
                foreach (DataRow row in dt.Rows)
                {
                    groupingList.Add(new TTGrouping()
                    {
                        TTGroupingID = Convert.ToInt32(row["TTGroupingID"]),
                        TTGroupingName = row["GroupingName"].ToString()
                    });
                }

                return groupingList;

            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the list of groupings.", ex);
            }
        }

        public TTGrouping GetTTGrouping(int ttGroupingID)
        {
            SqlDataReader reader = null;

            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTGrouping_List_ByTTGroupingID", 0);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, ttGroupingID);
                reader = Client.Instance.ExecuteReader(command);

                if (reader == null)
                    return null;

                TTGrouping retGrouping = null;

                if (reader.Read())
                {
                    retGrouping = new TTGrouping()
                        {
                            TTGroupingID = Convert.ToInt32(reader["TTGroupingID"]),
                            TTGroupingName = reader["GroupingName"].ToString()
                        };
                }

                return retGrouping;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the specified grouping.", ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public List<TTMemberProfile> GetTTMemberProfiles(int groupingID, int pageNumber, int rowsPerPage, out int totalRows)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTMemberProfile_List_ByTTGroupingID", 0);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);
                command.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
                command.AddParameter("@RowsPerPage", SqlDbType.Int, ParameterDirection.Input, rowsPerPage);

                var ds = Client.Instance.ExecuteDataSet(command);

                if (ds == null || ds.Tables.Count != 2)
                {
                    totalRows = 0;
                    return null;
                }

                var countDt = ds.Tables[0];
                totalRows = Convert.ToInt32(countDt.Rows[0]["TotalRows"]);

                return (from DataRow row in ds.Tables[1].Rows
                        select new TTMemberProfile
                            {
                                TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                                MemberID = Convert.ToInt32(row["MemberID"]),
                                BrandID = Convert.ToInt32(row["BrandID"]),
                                HasAnswer = row["IsSuspend"] != DBNull.Value
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the list of member profiles for a given groupingID.", ex);
            }
        }

        public List<TTMemberProfile> GetTTMemberProfiles(int memberID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTMemberProfile_List_ByMemberID", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

                var dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null)
                {
                    return null;
                }

                return (from DataRow row in dt.Rows
                        select new TTMemberProfile()
                            {
                                TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                                TTGroupingID = Convert.ToInt32(row["TTGroupingID"]),
                                GroupingName = row["GroupingName"].ToString(),
                                MemberID = Convert.ToInt32(row["MemberID"]),
                                BrandID = Convert.ToInt32(row["BrandID"])
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the list of member profiles for a given memberID.", ex);
            }
        }
        
        public TTMemberProfile GetTTMemberProfile(int memberID, int ttGroupingID)
        {
            SqlDataReader reader = null;

            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTMemberProfile_List_ByMemberIDTTGroupingID", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, ttGroupingID);

                reader = Client.Instance.ExecuteReader(command);
                if (reader == null)
                    return null;
                
                TTMemberProfile retObj = null;
                while (reader.Read())
                {
                    retObj = new TTMemberProfile()
                        {
                            TTMemberProfileID = Convert.ToInt32(reader["TTMemberProfileID"]),
                            TTGroupingID = Convert.ToInt32(reader["TTGroupingID"]),
                            MemberID = Convert.ToInt32(reader["MemberID"]),
                            BrandID = Convert.ToInt32(reader["BrandID"]),
                            GroupingName = reader["GroupingName"].ToString()
                        };
                }

                return retObj;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve a member profile with the memberID + ttGroupingID combo.");
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public List<TTMemberAttributeText> GetTTAnswerMemberAttributeTexts(int ttTestInstanceID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTAnswerMemberAttributeText_ListByTTTestInstanceID", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null || dt.Rows == null)
                    return null;

                return (from DataRow row in dt.Rows
                        select new TTMemberAttributeText
                            {
                                TTMemberAttributeTextID = Convert.ToInt32(row["TTMemberAttributeTextID"]),
                                IsGood = Convert.ToBoolean(row["IsGood"]),
                                BadReasonMask = Convert.ToInt32(row["BadReasonMask"]),
                                AnswerVersion = Convert.ToInt32(row["TTAnsMemAttrTextVer"])
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw new BLException(
                    "Unable to retrieve the TTAnswerMemberAttributeTexts for a given TTTestInstanceID.", ex);
            }
        }

        public List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int ttTestInstanceID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTAnswerMemberProfile_ListByTTTestInstanceID", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);

                var dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null || dt.Rows == null)
                    return null;

                var answerProfiles = new List<TTAnswerMemberProfile>();
                var lastMemProfID = Constants.NULL_INT;
                foreach (DataRow row in dt.Rows)
                {
                    if (lastMemProfID != Convert.ToInt32(row["TTMemberProfileID"]))
                    {
                        // Add the TTAnswerMemberProfile column values, intialize the TTAnswerSuspendReason list
                        // and then add the first suspend reason if that column is not null.
                        var profileToAdd = new TTAnswerMemberProfile
                            {
                                TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                                Suspend = Convert.ToBoolean(row["Suspend"]),
                                WarningContactInfo = Convert.ToBoolean(row["WarningContactInfo"]),
                                WarningGeneralViolation = Convert.ToBoolean(row["WarningGeneralViolation"]),
                                WarningInappropriateContent = Convert.ToBoolean(row["WarningInappropriateContent"]),
                                Version = Convert.ToInt32(row["TTAnsMemProfVer"])
                            };

                        profileToAdd.SuspendReasons = new List<TTAnswerSuspendReason>();

                        if (row["ReasonID"] != DBNull.Value)
                        {
                            // we want to ignore 0 because that's a placeholder to signal the transition from
                            // Suspend to Approve in suspend status
                            if (Convert.ToInt32(row["ReasonID"]) != 0)
                            {
                                profileToAdd.SuspendReasons.Add(new TTAnswerSuspendReason
                                    {
                                        ReasonID = Convert.ToInt32(row["ReasonID"]),
                                        Version = Convert.ToInt32(row["TTAnsSuspReasonVer"])
                                    });
                            }
                        }

                        answerProfiles.Add(profileToAdd);
                    }
                    else
                    {
                        // If TTMemberProfileID repeats, it means multiple suspend reasons were set as a part of the answer
                        // key, so just add the suspend reason to the last TTAnswerMemberProfile object.
                        var profileToMod = answerProfiles.Last();

                        if (row["ReasonID"] != DBNull.Value)
                        {
                            if (Convert.ToInt32(row["ReasonID"]) != 0)
                            {
                                profileToMod.SuspendReasons.Add(new TTAnswerSuspendReason
                                    {
                                        ReasonID = Convert.ToInt32(row["ReasonID"]),
                                        Version = Convert.ToInt32(row["TTAnsSuspReasonVer"])
                                    });
                            }
                        }
                    }

                    lastMemProfID = Convert.ToInt32(row["TTMemberProfileID"]);
                }

                return answerProfiles;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the TTAnswerMemberProfiles for a given TTTestInstanceID.", ex);
            }
        }

        public List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTAnswerMemberProfile_List", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);
                if (ttMemberProfileID.HasValue)
                {
                    command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                         ttMemberProfileID.Value);
                }
                command.AddParameter("@GetAllVersions", SqlDbType.Bit, ParameterDirection.Input, getAllVersions);
                
                DataSet ds = Client.Instance.ExecuteDataSet(command);
                if (ds == null || ds.Tables.Count == 0)
                    return null;
                
                DataTable mainDt = ds.Tables[0];

                if (mainDt.Rows.Count == 0)
                    return null;

                var answerProfiles = new List<TTAnswerMemberProfile>();
                foreach (DataRow row in mainDt.Rows)
                {
                    answerProfiles.Add(new TTAnswerMemberProfile
                    {
                        TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                        Suspend = Convert.ToBoolean(row["Suspend"]),
                        WarningContactInfo = Convert.ToBoolean(row["WarningContactInfo"]),
                        WarningGeneralViolation = Convert.ToBoolean(row["WarningGeneralViolation"]),
                        WarningInappropriateContent = Convert.ToBoolean(row["WarningInappropriateContent"]),
                        Version = Convert.ToInt32(row["TTAnsMemProfVer"])
                    });
                }

                // there is no real relationship between TTAnswerMemberProfile and TTAnswerSuspendReasons since they can change
                // independently, so add the suspend reasons to the first element of TTAnswerMemberProfile
                if (ds.Tables.Count == 2)
                {
                    var ansProfile = answerProfiles[0];
                    ansProfile.SuspendReasons = new List<TTAnswerSuspendReason>();
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ansProfile.SuspendReasons.Add(new TTAnswerSuspendReason
                            {
                                ReasonID = Convert.ToInt32(row["ReasonID"]),
                                Version = Convert.ToInt32(row["TTAnsSuspReasonVer"])
                            });
                    }
                }

                return answerProfiles;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the specified TTAnswerMemberProfile.", ex);
            }
        }

        /// <summary>
        /// For a given memberID + groupingID combo, all the text attributes are returned.  Answer key selections are not returned with this.
        /// Use GetAllMemberTextAttributesWithAnswer() instead if the answer key selections are needed.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupingID"></param>
        /// <returns></returns>
        public List<TTMemberAttributeText> GetAllTTMemberTextAttributes(int memberID, int groupingID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTMemberAttributeText_List", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);

                var dt = Client.Instance.ExecuteDataTable(command);
                if (dt == null)
                    return null;

                var textAttributes = new List<TTMemberAttributeText>();
                foreach (DataRow row in dt.Rows)
                {
                    textAttributes.Add(new TTMemberAttributeText()
                    {
                        TTMemberAttributeTextID = Convert.ToInt32(row["TTMemberAttributeTextID"]),
                        TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                        TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                        FieldValue = row["Value"].ToString(),
                        IncludeInTest = Convert.ToBoolean(row["IncludeInTest"]),
                        MemberAttributeName = row["MemberAttributeName"].ToString()
                    });
                }
                return textAttributes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve all the text attributes for a given member.", ex);
            }
        }

        /// <summary>
        /// For a given memberID + groupingID combo, all the text attributes with the answer key selections are returned.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupingID"></param>
        /// <param name="ttMemberProfileID"></param>
        /// <returns></returns>
        public List<TTMemberAttributeText> GetAllTTMemberTextAttributesWithAnswer(int memberID, int groupingID,
                                                                                  bool getAllVersions,
                                                                                  int? ttMemberProfileID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTMemberAttributeText_List_WithAnswer", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);
                if (ttMemberProfileID.HasValue)
                {
                    command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                         ttMemberProfileID.Value);
                }
                command.AddParameter("@GetAllVersions", SqlDbType.Bit, ParameterDirection.Input, getAllVersions);

                DataTable dt = Client.Instance.ExecuteDataTable(command);
                if (dt == null)
                    return null;

                var textAttributes = new List<TTMemberAttributeText>();
                foreach (DataRow row in dt.Rows)
                {
                    var attrText = new TTMemberAttributeText
                        {
                            TTMemberAttributeTextID = Convert.ToInt32(row["TTMemberAttributeTextID"]),
                            TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                            TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                            FieldValue = row["Value"].ToString(),
                            IncludeInTest = Convert.ToBoolean(row["IncludeInTest"]),
                            MemberAttributeName = row["MemberAttributeName"].ToString(),
                            DisplayOnly =
                                row["DisplayOnly"] == DBNull.Value ? false : Convert.ToBoolean(row["DisplayOnly"]),
                            IsGood =
                                row["IsGood"] == DBNull.Value
                                    ? false
                                    : Convert.ToBoolean(row["IsGood"]),
                            BadReasonMask = row["BadReasonMask"] == DBNull.Value
                                                ? 0
                                                : Convert.ToInt32(row["BadReasonMask"]),
                            AnswerVersion =
                                row["TTAnsMemAttrTextVer"] == DBNull.Value
                                    ? 0
                                    : Convert.ToInt32(row["TTAnsMemAttrTextVer"])
                        };

                    textAttributes.Add(attrText);
                }
                return textAttributes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve all the text attributes with answer for a given member.", ex);
            }
        }

        /// <summary>
        /// Gets the next TTMemberProfile to use a test question. Use TTMemberProfileIDToExclude to exclude a value since using this method in combination with a asynch
        /// write could cause the client to pick up the same item it just processed.
        /// </summary>
        /// <param name="ttTestInstanceID">TTTestInstanceID of the test to search within</param>
        /// <param name="ttMemberProfileIDToExclude">TTMemberProfileIDToExclude to exclude from the search</param>
        /// <returns>null if none found or populated TTMemberAttributeText list object that contains which FTA fields to test on</returns>
        public List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                          int? ttMemberProfileIDToExclude)
        {
            int remainingCount = 0;
            int ttAnswerMemberProfileVersion = 0;
            int ttAnswerSuspendReasonsVersion = 0;
            return GetNextTTMemberProfileQuestion(ttTestInstanceID, ttMemberProfileIDToExclude, out remainingCount, out ttAnswerMemberProfileVersion, out ttAnswerSuspendReasonsVersion);
        }

        /// <summary>
        /// Gets the next TTMemberProfile to use a test question. Use TTMemberProfileIDToExclude to exclude a value since using this method in combination with a asynch
        /// write could cause the client to pick up the same item it just processed. RemainingCount out parm includes the item that is being picked up since it's technically
        /// not processed yet.
        /// </summary>
        /// <param name="ttTestInstanceID"></param>
        /// <param name="ttMemberProfileIDToExclude"></param>
        /// <param name="remainingCount"></param>
        /// <returns></returns>
        public List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                          int? ttMemberProfileIDToExclude,
                                                                          out int remainingCount,
                                                                          out int ttAnswerMemberProfileVersion,
                                                                          out int ttAnswerSuspendReasonsVersion)
        {
            remainingCount = 0;
            ttAnswerMemberProfileVersion = 0;
            ttAnswerSuspendReasonsVersion = 0;

            try
            {
                int retValue = -1;

                var command = new Command("mnAdmin", "dbo.up_TTAdminAnswer_GetNext", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);
                if (ttMemberProfileIDToExclude.HasValue)
                {
                    command.AddParameter("@TTMemberProfileIDToExclude", SqlDbType.Int, ParameterDirection.Input,
                                         ttMemberProfileIDToExclude.Value);
                }

                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds == null || ds.Tables.Count != 4)
                {
                    return null;
                }

                DataTable dtText = ds.Tables[0];
                DataTable dtCount = ds.Tables[1];
                DataTable ansMemProfVer = ds.Tables[2];
                DataTable ansSuspReasonVer = ds.Tables[3];

                remainingCount = Convert.ToInt32(dtCount.Rows[0]["RemainingCount"]);
                ttAnswerMemberProfileVersion = ansMemProfVer.Rows[0]["TTAnsMemProfVer"] == DBNull.Value
                                                   ? 0
                                                   : Convert.ToInt32(ansMemProfVer.Rows[0]["TTAnsMemProfVer"]);
                ttAnswerSuspendReasonsVersion = ansSuspReasonVer.Rows[0]["TTAnsSuspReasonVer"] == DBNull.Value
                                                    ? 0
                                                    : Convert.ToInt32(ansSuspReasonVer.Rows[0]["TTAnsSuspReasonVer"]);

                return (from DataRow row in dtText.Rows
                        select new TTMemberAttributeText
                            {
                                TTMemberAttributeTextID = Convert.ToInt32(row["TTMemberAttributeTextID"]),
                                TTMemberProfileID = Convert.ToInt32(row["TTMemberProfileID"]),
                                TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                                FieldValue = row["Value"].ToString(),
                                IncludeInTest = Convert.ToBoolean(row["IncludeInTest"]),
                                MemberAttributeName = row["MemberAttributeName"].ToString(),
                                AnswerVersion = Convert.ToInt32(row["AnswerVersion"])
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the next TTMemberProfile question.", ex);
            }
        }

        /// <summary>
        /// Retrieves the total count of TTMemberProfiles for a given test instance. This is used to determine if the record the client is processing is the last one.
        /// When a report of the final grade needs to be calculated, this can be used to ensure everything was included.
        /// </summary>
        /// <param name="ttTestInstanceID">TTTestInstanceID of the test instance</param>
        /// <returns>0 if not found and the number of TTMemberProfiles if found</returns>
        public int GetTTMemberProfileCount(int ttTestInstanceID, bool answeredOnly)
        {
            SqlDataReader reader = null;

            try
            {
                var retValue = 0;

                var command = new Command("mnAdmin", "dbo.up_TTAdminAnswer_GetTotalCount", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);
                command.AddParameter("@AnsweredOnly", SqlDbType.Bit, ParameterDirection.Input, answeredOnly);

                reader = Client.Instance.ExecuteReader(command);

                if (reader == null)
                    return retValue;

                TTAnswerMemberProfile answerProfile = null;
                while (reader.Read())
                {
                    retValue = Convert.ToInt32(reader["TotalCount"]);
                }

                return retValue;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the total count of TTMemberProfiles in a given test instance.", ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public List<TTTestInstance> GetTTTestInstances(int adminMemberID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTTestInstance_List", 0);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null)
                    return null;

                return (from DataRow row in dt.Rows
                        select new TTTestInstance
                            {
                                TTTestInstanceID = Convert.ToInt32(row["TTTestInstanceID"]),
                                TTGroupingID = Convert.ToInt32(row["TTGroupingID"]),
                                AdminMemberID = Convert.ToInt32(row["AdminMemberID"]),
                                Status = (TTTestInstanceStatus) Convert.ToInt32(row["Status"]),
                                StartDate = Convert.ToDateTime(row["StartDate"]),
                                EndDate =
                                    row["EndDate"] == DBNull.Value
                                        ? DateTime.MinValue
                                        : Convert.ToDateTime(row["EndDate"]),
                                KeepAliveContactDate = Convert.ToDateTime(row["KeepAliveContactDate"]),
                                InsertDate = Convert.ToDateTime(row["InsertDate"]),
                                UpdateDate =
                                    row["UpdateDate"] == DBNull.Value
                                        ? DateTime.MinValue
                                        : Convert.ToDateTime(row["UpdateDate"])
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve a list of test instances.", ex);
            }
        }

        public TTTestInstance GetTTTestInstance(int ttTestInstanceID)
        {
            SqlDataReader reader = null;

            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTTestInstance_List_ByTTTestInstanceID", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);
                reader = Client.Instance.ExecuteReader(command);

                if (reader == null)
                    return null;

                TTTestInstance retTestInstance = null;
                if (reader.Read())
                {
                    retTestInstance = new TTTestInstance
                        {
                            TTTestInstanceID = Convert.ToInt32(reader["TTTestInstanceID"]),
                            TTGroupingID = Convert.ToInt32(reader["TTGroupingID"]),
                            AdminMemberID = Convert.ToInt32(reader["AdminMemberID"]),
                            Status = (TTTestInstanceStatus) Convert.ToInt32(reader["Status"]),
                            StartDate = Convert.ToDateTime(reader["StartDate"]),
                            EndDate =
                                reader["EndDate"] == DBNull.Value
                                    ? DateTime.MinValue
                                    : Convert.ToDateTime(reader["EndDate"]),
                            KeepAliveContactDate = Convert.ToDateTime(reader["KeepAliveContactDate"]),
                            InsertDate = Convert.ToDateTime(reader["InsertDate"]),
                            UpdateDate =
                                reader["UpdateDate"] == DBNull.Value
                                    ? DateTime.MinValue
                                    : Convert.ToDateTime(reader["UpdateDate"])
                        };
                }

                return retTestInstance;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the specified test instance.", ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        public double GetTotalTimeSpentOnTest(int ttTestInstanceID, out bool testFinished)
        {
            testFinished = true;

            try
            {
                var command = new Command("mnAdmin", "up_TTTestTimespan_List", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null)
                    return 0;

                double totalTimeSpent = 0;
                foreach (DataRow row in dt.Rows)
                {
                    if (row["EndDate"] == DBNull.Value)
                    {
                        // this means we have an open timespan, which means the test isn't done.
                        // we need to skip this entry for the sum calculation.
                        testFinished = false;
                    }
                    else
                    {
                        totalTimeSpent += (Convert.ToDateTime(row["EndDate"]) - Convert.ToDateTime(row["StartDate"])).TotalSeconds;
                    }
                }

                return totalTimeSpent;

                //return dt == null
                //           ? 0
                //           : (from DataRow row in dt.Rows
                //              select Convert.ToDateTime(row["EndDate"]) - Convert.ToDateTime(row["StartDate"])
                //              into difference select difference.TotalSeconds).Sum();
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to total up the time spent on a test.", ex);
            }
        }

        public List<TTMemberPhoto> GetTTMemberPhotos(int memberID, int groupingID)
        {
            var ttMemberProfile = GetTTMemberProfile(memberID, groupingID);
            if (ttMemberProfile == null)
                return null;

            return GetTTMemberPhotos(ttMemberProfile.TTMemberProfileID);
        }

        public List<TTMemberPhoto> GetTTMemberPhotos(int ttMemberProfileID)
        {
            SqlDataReader reader = null;

            try
            {
                var command = new Command("mnAdmin", "up_TTMemberPhoto_List", 0);
                command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                     ttMemberProfileID);

                reader = Client.Instance.ExecuteReader(command);
                if (reader == null)
                    return null;

                var ttMemberPhotos = new List<TTMemberPhoto>();
                while (reader.Read())
                {
                    ttMemberPhotos.Add(new TTMemberPhoto
                    {
                        Ordinal = Convert.ToInt32(reader["Ordinal"]),
                        CloudPath = reader["FileCloudPath"].ToString(),
                        ThumbnailCloudPath = reader["ThumbFileCloudPath"].ToString()
                    });
                }

                return ttMemberPhotos;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the TTMemberPhotos.", ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        #region Synch writes
        public void CreateNewGrouping(string groupingName)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTGrouping_Insert", 0);

                var groupingID = KeySA.Instance.GetKey("TTGroupingID");
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);
                command.AddParameter("@GroupingName", SqlDbType.VarChar, ParameterDirection.Input, groupingName);

                var sw = new SyncWriter();
                Exception swEx = null;
                sw.Execute(command, out swEx);
                if (swEx != null)
                {
                    throw swEx;
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to create a new Training Tool grouping.", ex);
            }
        }

        /// <summary>
        /// This method will do all the bootstrapping tasks for a new Training Tool test. It will execute synchronously for this reason.
        /// </summary>
        /// <param name="groupingID"></param>
        /// <param name="adminID"></param>
        /// <returns>TTTestInstanceID of the test instance created</returns>
        public int CreateNewTestInstance(int groupingID, int adminID)
        {
            try
            {
                var command = new Command("mnAdmin", "dbo.up_TTTestInstance_CreateNew", 0);

                var ttTestIstanceID = KeySA.Instance.GetKey("TTTestInstanceID");
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestIstanceID);
                command.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminID);
                command.AddParameter("@Status", SqlDbType.Int, ParameterDirection.Input, (int)TTTestInstanceStatus.Started);
                var startDate = DateTime.Now;
                command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                command.AddParameter("@KeepAliveContactDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);

                var sw = new SyncWriter();
                Exception swEx = null;
                sw.Execute(command, out swEx);
                if (swEx != null)
                {
                    throw swEx;
                }

                return ttTestIstanceID;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to create a new Training Tool Test Instance.", ex);
            }
        }

        private void saveAdminAnswerSynchronousely(TTAdminAnswer adminAnswer)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                var sw = new SyncWriter();
                Exception swEx = null;
                sw.Execute(getTTAdminAnswerSaveCommand(adminAnswer), out swEx);
                if (swEx != null)
                {
                    throw swEx;
                }

                foreach (TTAdminAnswerEssay adminAnswerEssay in adminAnswer.AdminAnswerEssays)
                {
                    var essayCommand = getTTAdminAnswerEssayInsertCommand(adminAnswerEssay, adminAnswer.TTTestInstanceID,
                                                                      adminAnswer.TTMemberProfileID);

                    swEx = null;
                    sw.Execute(essayCommand, out swEx);
                    if (swEx != null)
                    {
                        throw swEx;
                    }
                    
                }

                scope.Complete();
            }

        }

        /// <summary>
        /// Master record is written synchronously and the child records are written asynchronously.
        /// </summary>
        /// <param name="groupingID"></param>
        /// <param name="memberProfiles"></param>
        public void InsertTTMemberProfiles(int groupingID, List<TTMemberProfile> memberProfiles)
        {
            RollingFileLogger.Instance.EnterMethod(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "InsertTTMemberProfiles");

            try
            {
                foreach (TTMemberProfile mp in memberProfiles)
                {
                    // insert into TTMemberProfile (parent record)
                    int rowCount = 0;
                    var mpCommand = new Command("mnAdmin", "up_TTMemberProfile_Insert", 0);
                    var memberProfileID = KeySA.Instance.GetKey("TTMemberProfileID");
                    mpCommand.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, memberProfileID);
                    mpCommand.AddParameter("@TTGroupingID", SqlDbType.Int, ParameterDirection.Input, groupingID);
                    mpCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, mp.MemberID);
                    mpCommand.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, mp.BrandID);
                    mpCommand.AddParameter("@RowsAffected", SqlDbType.Int, ParameterDirection.Output, rowCount);

                    // if rowCount comes back 0 or exception occurs, just skip to the next profile to process
                    var sw = new SyncWriter();
                    Exception swEx = null;
                    sw.Execute(mpCommand, out swEx);
                    if (swEx != null)
                    {
                        rowCount = 0;
                    }
                    else
                    {
                        rowCount = Convert.ToInt32(mpCommand.Parameters["@RowsAffected"].ParameterValue);
                    }

                    if (rowCount == 0)
                        continue;

                    RollingFileLogger.Instance.LogInfoMessage(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                        string.Format("Synchro insert of TTMemberProfile complete. TTMemberProfileID: {0}. Asynchro inserts will begin.", memberProfileID), null);

                    // copy the member attributes here
                    var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(mp.BrandID);
                    Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(mp.MemberID, MemberLoadFlags.None);
                    foreach (TTMemberAttributeMetadata mtd in TTAttributeMetadatas)
                    {
                        switch (mtd.AttributeDataType)
                        {
                            case TTAttributeDataType.Number:
                            case TTAttributeDataType.SpecialNumber: // SpecialNumber type signals that the caller needs to send the value in because it's not an actual member attribute.
                                insertTTMemberAttributeInt(memberProfileID, mtd.TTAttributeID,
                                                           mtd.MemberAttributeName.ToLower() == "memberid"
                                                               ? member.MemberID
                                                               : getAttributeInt(member, brand, mtd.MemberAttributeName, mp));
                                break;
                            case TTAttributeDataType.Date:
                            case TTAttributeDataType.Timespan:  // Timespan usually means it's a date member attribute in the DB that we want to convert to a sliding date by storing the timespand instead.
                            case TTAttributeDataType.SpecialTimespan: // SpecialTimespan type signals that the caller needs to send the value in because it's not an actual member attribute.
                                bool isFutureTimespan = false;
                                var dateOrTimespan = getAttributeDate(member, brand, mtd, mp, out isFutureTimespan);
                                insertTTMemberAttributeDate(memberProfileID, mtd.TTAttributeID, dateOrTimespan, isFutureTimespan);
                                break;
                            case TTAttributeDataType.Text:
                                insertTTMemberAttributeText(memberProfileID, mtd.TTAttributeID,
                                                            getAttributeText(member, brand, mtd.MemberAttributeName),
                                                            false);
                                break;
                            //case TTAttributeDataType.SpecialNumber:
                            //    insertTTMemberAttributeInt(memberProfileID, mtd.TTAttributeID,
                            //        mp.GetMemberAttributeInt(mtd.MemberAttributeName));
                            //    break;

                        }
                    }

                    #region copy over the photos

                    RollingFileLogger.Instance.LogInfoMessage(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "Copying of member photos will begin.", null);

                    var photo = member.GetDefaultPhoto(brand.Site.Community.CommunityID, false);
                    if (photo != null)
                    {
                        // we are only storing 1 photo for now, so just set the ordinal to 1
                        int ordinal = 1;

                        var cloudClient = new Spark.CloudStorage.Client(brand.Site.Community.CommunityID,
                                                                        brand.Site.SiteID, RuntimeSettings.Instance);

                        var fullCloudUrl = cloudClient.GetFullCloudImagePath(photo.FileCloudPath, FileType.MemberPhoto,
                                                                             false);
                        var fullThumbCloudUrl = cloudClient.GetFullCloudImagePath(photo.ThumbFileCloudPath,
                                                                                  FileType.MemberPhoto, false);

                        RollingFileLogger.Instance.LogInfoMessage(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                                                  string.Format(
                                                                      "FullCloudURL: {0}, FullthumbCloudURL: {1}",
                                                                      fullCloudUrl, fullThumbCloudUrl), null);
                            
                        using (var client = new WebClient())
                        {
                            byte[] imageBytes = client.DownloadData(fullCloudUrl);
                            byte[] thumbBytes = client.DownloadData(fullThumbCloudUrl);

                            var fileTypeString = fullCloudUrl.Substring(fullCloudUrl.LastIndexOf(".") + 1,
                                                                        fullCloudUrl.Length -
                                                                        (fullCloudUrl.LastIndexOf(".") + 1));
                            var thumbFileTypeString = fullThumbCloudUrl.Substring(fullThumbCloudUrl.LastIndexOf(".") + 1,
                                                                        fullThumbCloudUrl.Length -
                                                                        (fullThumbCloudUrl.LastIndexOf(".") + 1));

                            var dateTimeNow = DateTime.Now;
                            var newCloudPath = string.Format("{0}{1}_{2}.{3}", dateTimeNow.ToString("/yyyy/MM/dd/HH/"), memberProfileID, ordinal, fileTypeString);
                            var newThumbCloudPath = string.Format("{0}{1}_{2}t.{3}", dateTimeNow.ToString("/yyyy/MM/dd/HH/"), memberProfileID, ordinal, thumbFileTypeString);

                            RollingFileLogger.Instance.LogInfoMessage(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                                                      string.Format(
                                                                          "NewCloudPath: {0}, NewThumbCloudPath: {1}",
                                                                          newCloudPath, newThumbCloudPath), null);

                            var saveResult = PhotoFileSA.Instance.SaveAdmintoolPhoto(imageBytes, newCloudPath);
                            var thumbSaveResult = PhotoFileSA.Instance.SaveAdmintoolPhoto(thumbBytes, newThumbCloudPath);

                            RollingFileLogger.Instance.LogInfoMessage(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                                                      string.Format(
                                                                          "SaveResult: {0}, ThumbSaveResult: {1}",
                                                                          saveResult, thumbSaveResult), null);

                            // even if we get a fail for one of them, we want to store whatever we can here so that the images don't get orphaned in the cloud all lonely and stuff.
                            if (saveResult.Success || thumbSaveResult.Success)
                            {
                                saveTTMemberPhoto(memberProfileID, ordinal,
                                    saveResult.Success ? newCloudPath : string.Empty,
                                    thumbSaveResult.Success ? newThumbCloudPath : string.Empty);
                            }
                        }
                    }
                    #endregion

                    #region copy over emails
                    RollingFileLogger.Instance.LogInfoMessage(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "Copying of member emails will begin.", null);
                    copyMemberEmails(memberProfileID, member.MemberID, brand.Site.Community.CommunityID);
                    #endregion
                }

            }
            catch (Exception ex)
            {
                throw new BLException("InsertMemberProfiles() failed - some of the member profiles could have made it.", ex);
            }

            RollingFileLogger.Instance.LeaveMethod(ApproveQueueVO.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "InsertTTMemberProfiles");
        }
        #endregion

        /// <summary>
        /// Locates the Grouping identified by the passed in groupingName and adds the member profiles to that Grouping. Member attributes are copied in the process.
        /// </summary>
        /// <param name="groupingName"></param>
        /// <param name="memberProfiles"></param>
        public void InsertTTMemberProfiles(string groupingName, List<TTMemberProfile> memberProfiles)
        {
            var groupingID = Constants.NULL_INT;

            try
            {
                // search for the GroupingID by GroupingName
                var command = new Command("mnAdmin", "up_TTGrouping_List_ByGroupingName", 0);
                command.AddParameter("@GroupingName", SqlDbType.VarChar, ParameterDirection.Input, groupingName);
                var dt = Client.Instance.ExecuteDataTable(command);

                if (dt == null)
                    throw new Exception("Invalid GroupingName");

                groupingID = Convert.ToInt32(dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                throw new BLException("Encountered an error while trying to determine the GroupingID from GroupingName.", ex);
            }

            InsertTTMemberProfiles(groupingID, memberProfiles);
        }

        public void SaveTTAnswer(TTAnswerMemberProfile profile, List<TTMemberAttributeText> texts)
        {
            try
            {
                var parentCommand = new Command("mnAdmin", "up_TTAnswerMemberProfile_Insert", 0);
                parentCommand.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                           profile.TTMemberProfileID);
                parentCommand.AddParameter("@Suspend", SqlDbType.Bit, ParameterDirection.Input, profile.Suspend);
                parentCommand.AddParameter("@WarningContactInfo", SqlDbType.Bit, ParameterDirection.Input,
                                           profile.WarningContactInfo);
                parentCommand.AddParameter("@WarningInappropriateContent", SqlDbType.Bit, ParameterDirection.Input,
                                           profile.WarningInappropriateContent);
                parentCommand.AddParameter("@WarningGeneralViolation", SqlDbType.Bit, ParameterDirection.Input,
                                           profile.WarningGeneralViolation);
                Client.Instance.ExecuteAsyncWrite(parentCommand);

                if (profile.Suspend && profile.SuspendReasons != null && profile.SuspendReasons.Count > 0)
                {
                    var reasonsCmd = new Command("mnAdmin", "up_TTAnswerSuspendReasons_Save", 0);
                    reasonsCmd.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                            profile.TTMemberProfileID);
                    reasonsCmd.AddParameter("@ReasonIDs", SqlDbType.VarChar, ParameterDirection.Input,
                        string.Join(",", profile.SuspendReasons.Select(x=>x.ReasonID.ToString()).ToArray()));
                    Client.Instance.ExecuteAsyncWrite(reasonsCmd);
                }
                else
                {
                    // If no suspend reason is provided, this means the answer key is saying this profile should be
                    // approved. In this case, we have to insert 0 to trigger a version update because if we don't do this, when the answer key
                    // changes from Suspend to Approve, the invalid suspend reasons get returned.
                    var reasonsCmd = new Command("mnAdmin", "up_TTAnswerSuspendReasons_Save", 0);
                    reasonsCmd.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                            profile.TTMemberProfileID);
                    reasonsCmd.AddParameter("@ReasonIDs", SqlDbType.VarChar, ParameterDirection.Input, "0");
                    Client.Instance.ExecuteAsyncWrite(reasonsCmd);
                }

                foreach (TTMemberAttributeText text in texts)
                {
                    var command = new Command("mnAdmin", "up_TTAnswerMemberAttributeText_Insert", 0);
                    command.AddParameter("@TTMemberAttributeTextID", SqlDbType.Int, ParameterDirection.Input,
                                         text.TTMemberAttributeTextID);
                    command.AddParameter("@Value", SqlDbType.NVarChar, ParameterDirection.Input, text.FieldValue);
                    command.AddParameter("@InclueInTest", SqlDbType.Bit, ParameterDirection.Input, text.IncludeInTest);
                    command.AddParameter("@IsGood", SqlDbType.Bit, ParameterDirection.Input, text.IsGood);
                    command.AddParameter("@BadReasonMask", SqlDbType.Int, ParameterDirection.Input, text.BadReasonMask);
                    Client.Instance.ExecuteAsyncWrite(command);
                }
            }
            catch (Exception ex)
            {
                throw new BLException(
                    "Error occurred during SaveTTAnswer: Async writes were used so partial writes are most likely", ex);
            }
        }

        /// <summary>
        /// Writes the admin's answer to the DB. All the DB writes happen in asynch fashion, so partial writes can result if error occurs in the middle.
        /// </summary>
        /// <param name="adminAnswer"></param>
        public void SaveAdminAnswer(TTAdminAnswer adminAnswer, bool isSynchronousWrite)
        {
            if (adminAnswer == null)
            {
                throw new BLException("Admin's answer object cannot be null.");
            }

            if (isSynchronousWrite)
            {
                try
                {
                    saveAdminAnswerSynchronousely(adminAnswer);
                }
                catch (Exception ex)
                {
                    throw new BLException(
                        string.Format(
                            "Unable to save an admin's answer. Transaction was rolled back TTTestInstanceID: {0} TTMemberProfileID: {1}",
                            adminAnswer.TTTestInstanceID, adminAnswer.TTMemberProfileID), ex);
                }
                
            }
            else
            {
                try
                {
                    saveAdminAnswer(adminAnswer);
                }
                catch (Exception ex)
                {
                    throw new BLException(
                        string.Format(
                            "Unable to save an admin's answer. Partial update could have completed TTTestInstanceID: {0} TTMemberProfileID: {1}",
                            adminAnswer.TTTestInstanceID, adminAnswer.TTMemberProfileID), ex);
                }
            }
        }

        public void UpdateTestStatus(int ttTestInstanceID, TTTestInstanceStatus testStatus)
        {
            try
            {
                var command = new Command("mnAdmin", "up_TTTestTimespan_UpdateStatus", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);
                command.AddParameter("@Status", SqlDbType.Int, ParameterDirection.Input, (int)testStatus);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error in updating the test timespan information.");
            }
        }

        public void KeepalivePulse(int ttTestInstanceID)
        {
            try
            {
                var command = new Command("mnAdmin", "up_TTTestTimespan_Keepalive", 0);
                command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception)
            {
                throw new BLException("Error in sending the keepalive pulse.");
            }
        }

        public void UpdateTTMemberProfileTTGroupingID(int ttMemberProfileID, int newGroupingID)
        {
            try
            {
                var command = new Command("mnAdmin", "up_TTMemberProfile_Update_TTGroupingID", 0);
                command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);
                command.AddParameter("@NewTTGroupingID", SqlDbType.Int, ParameterDirection.Input, newGroupingID);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error in updating the TTMemberProfile's TTGroupingID.");
            }
        }
       
        #endregion

        #region Private Methods
       
        private Command getTTAdminAnswerSaveCommand(TTAdminAnswer adminAnswer)
        {
            var command = new Command("mnAdmin", "up_TTAdminAnswer_Update", 0);
            command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input,
                                 adminAnswer.TTTestInstanceID);
            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input,
                                 adminAnswer.TTMemberProfileID);
            command.AddParameter("@PossibleFraud", SqlDbType.Bit, ParameterDirection.Input,
                                 adminAnswer.PossibleFraud);
            command.AddParameter("@Suspend", SqlDbType.Bit, ParameterDirection.Input,
                                 adminAnswer.Suspend);
            command.AddParameter("@WContactInfo", SqlDbType.Bit, ParameterDirection.Input,
                                 adminAnswer.WContactInfo);
            command.AddParameter("@WInappropriateContent", SqlDbType.Bit, ParameterDirection.Input,
                                 adminAnswer.WInappropriateContent);
            command.AddParameter("@WGeneralViolation", SqlDbType.Bit, ParameterDirection.Input,
                                 adminAnswer.WGeneralViolation);
            command.AddParameter("@WNoWarning", SqlDbType.Bit, ParameterDirection.Input,
                                 adminAnswer.WNoWarning);
            command.AddParameter("@SuspendReason", SqlDbType.Int, ParameterDirection.Input,
                                 adminAnswer.SuspendReason);
            command.AddParameter("@TTAnsMemProfVer", SqlDbType.Int, ParameterDirection.Input,
                                 adminAnswer.TTAnswerMemberProfileVersion);
            command.AddParameter("@TTAnsSuspReasonVer", SqlDbType.Int, ParameterDirection.Input,
                                 adminAnswer.TTAnswerSuspendReasonsVersion);


            return command;
        }

        private string getAttributeText(Member.ServiceAdapters.Member member, Brand brand, string attributeName)
        {
            var retValue = string.Empty;

            try
            {
                retValue = member.GetAttributeTextApproved(brand, attributeName, string.Empty);
            }
            catch (Exception ex)
            {
                if (!ex.Message.ToLower().Contains("attribute group not found"))
                {
                    throw ex;
                }
            }

            return retValue;
        }

        private int getAttributeInt(Member.ServiceAdapters.Member member, Brand brand, string attributeName, TTMemberProfile mp)
        {
            // Check to see if the value exists inside the passed in MemberProfile.  If it exists, it means it's not a member attribute
            // but a derived value, which is much easier to obtain from above ApproveQueue service.  If it exists, use that value instead
            // of trying to obtain it from the DB.
            int retValue = mp.GetMemberAttributeInt(attributeName);

            if (retValue != Constants.NULL_INT)
                return retValue;

            try
            {
                retValue = member.GetAttributeInt(brand, attributeName, Constants.NULL_INT);
            }
            catch (Exception ex)
            {
                if (!ex.Message.ToLower().Contains("attribute group not found"))
                {
                    throw ex;
                }
            }

            return retValue;
        }

        private DateTime getAttributeDate(Member.ServiceAdapters.Member member, Brand brand, TTMemberAttributeMetadata mtd, TTMemberProfile mp, out bool isFutureTimespan)
        {
            isFutureTimespan = false;
            DateTime retValue = DateTime.MinValue;

            if (mtd.AttributeDataType == TTAttributeDataType.Date)
            {
                // Check to see if the value exists inside the passed in MemberProfile.  If it exists, it means it's not a member attribute
                // but a derived value, which is much easier to obtain from above ApproveQueue service.  If it exists, use that value instead
                // of trying to obtain it from the DB.
                retValue = mp.GetMemberAttributeDate(mtd.MemberAttributeName);

                if (retValue == DateTime.MinValue)
                {
                    try
                    {
                        retValue = member.GetAttributeDate(brand, mtd.MemberAttributeName, DateTime.MinValue);
                    }
                    catch (Exception ex)
                    {
                        if (!ex.Message.ToLower().Contains("attribute group not found"))
                        {
                            throw ex;
                        }
                    }
                }
            }
            else if (mtd.AttributeDataType == TTAttributeDataType.Timespan || mtd.AttributeDataType == TTAttributeDataType.SpecialTimespan)
            {
                // If this value was passed in, the value should be converted to timespan already.  If we have to go to the DB to obtain the
                // member attribute, we have to calcuate the time difference between now and the date value in the DB.
                retValue = mp.GetMemberAttributeTimespan(mtd.MemberAttributeName, out isFutureTimespan);

                if (retValue == DateTime.MinValue)
                {
                    var dateFromDb = DateTime.MinValue;

                    try
                    {
                        dateFromDb = member.GetAttributeDate(brand, mtd.MemberAttributeName, DateTime.MinValue);
                    }
                    catch (Exception ex)
                    {
                        if (!ex.Message.ToLower().Contains("attribute group not found"))
                        {
                            throw ex;
                        }
                    }

                    // Calculate the time different and convert it to a DateTime after
                    var timeSpanDiff = DateTime.Now - dateFromDb;
                    isFutureTimespan = timeSpanDiff.CompareTo(TimeSpan.Zero) < 0;
                    retValue = DateTime.MinValue + timeSpanDiff.Duration();
                }
            }

            return retValue;
        }

        private void saveAdminAnswer(TTAdminAnswer adminAnswer)
        {
            Client.Instance.ExecuteAsyncWrite(getTTAdminAnswerSaveCommand(adminAnswer));

            foreach (TTAdminAnswerEssay adminAnswerEssay in adminAnswer.AdminAnswerEssays)
            {
                insertTTAdminAnswerEssay(adminAnswerEssay, adminAnswer.TTTestInstanceID, adminAnswer.TTMemberProfileID);
            }
        } 

        private void saveTTMemberPhoto(int ttMemberProfileId, int ordinal, string fileCloudPath, string thumbFileCloudPath)
        {
            var command = new Command("mnAdmin", "up_TTMemberPhoto_Save", 0);
            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileId);
            command.AddParameter("@Ordinal", SqlDbType.Int, ParameterDirection.Input, ordinal);
            command.AddParameter("@FileCloudPath", SqlDbType.VarChar, ParameterDirection.Input, fileCloudPath);
            command.AddParameter("@ThumbFileCloudPath", SqlDbType.VarChar, ParameterDirection.Input, thumbFileCloudPath);
            
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private void insertTTAdminAnswerEssay(TTAdminAnswerEssay ttAdminAnswerEssay, int ttTestInstanceID, int ttMemberProfileID)
        {
            Client.Instance.ExecuteAsyncWrite(getTTAdminAnswerEssayInsertCommand(ttAdminAnswerEssay, ttTestInstanceID, ttMemberProfileID));
        }

        private Command getTTAdminAnswerEssayInsertCommand(TTAdminAnswerEssay ttAdminAnswerEssay, int ttTestInstanceID,
                                                           int ttMemberProfileID)
        {
            var command = new Command("mnAdmin", "up_TTAdminAnswerEssay_Insert", 0);
            command.AddParameter("@TTTestInstanceID", SqlDbType.Int, ParameterDirection.Input, ttTestInstanceID);
            command.AddParameter("@TTMemberAttributeTextID", SqlDbType.Int, ParameterDirection.Input, ttAdminAnswerEssay.TTMemberAttributeTextID);
            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);
            command.AddParameter("@IsGood", SqlDbType.Bit, ParameterDirection.Input, ttAdminAnswerEssay.IsGood);
            command.AddParameter("@BadReasonMask", SqlDbType.Int, ParameterDirection.Input, ttAdminAnswerEssay.BadReasonMask);
            command.AddParameter("@TTAnsMemAttrTextVer", SqlDbType.Int, ParameterDirection.Input, ttAdminAnswerEssay.TTAnswerMemberAttributeTextVersion);

            return command;
        }

        private void insertTTMemberAttributeDate(int ttMemberProfileID, int ttAttributeID, DateTime value, bool isFutureTimeSpan)
        {
            var command = new Command("mnAdmin", "up_TTMemberAttributeDate_Insert", 0);
            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);
            command.AddParameter("@TTAttributeID", SqlDbType.Int, ParameterDirection.Input, ttAttributeID);
            command.AddParameter("@Value", SqlDbType.DateTime2, ParameterDirection.Input, value);
            command.AddParameter("@IsFutureTimeSpan", SqlDbType.Bit, ParameterDirection.Input, isFutureTimeSpan);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private void insertTTMemberAttributeInt(int ttMemberProfileID, int ttAttributeID, int value)
        {
            var command = new Command("mnAdmin", "up_TTMemberAttributeInt_Insert", 0);
            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);
            command.AddParameter("@TTAttributeID", SqlDbType.Int, ParameterDirection.Input, ttAttributeID);
            command.AddParameter("@Value", SqlDbType.Int, ParameterDirection.Input, value);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private void insertTTMemberAttributeText(int ttMemberProfileID, int ttAttributeID, string value, bool includeInTest)
        {
            var command = new Command("mnAdmin", "up_TTMemberAttributeText_Insert", 0);

            var memberAttributeTextID = KeySA.Instance.GetKey("TTMemberAttributeTextID");

            command.AddParameter("@TTMemberAttributeTextID", SqlDbType.Int, ParameterDirection.Input, memberAttributeTextID);
            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);
            command.AddParameter("@TTAttributeID", SqlDbType.Int, ParameterDirection.Input, ttAttributeID);
            command.AddParameter("@Value", SqlDbType.NVarChar, ParameterDirection.Input, value);
            command.AddParameter("@IncludeInTest", SqlDbType.Bit, ParameterDirection.Input, includeInTest);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void copyMemberEmails(int ttMemberProfileID, int memberID, int groupID)
        {
            MemberMailLog mailLog = EmailLogSA.Instance.RetrieveEmailLog(memberID, groupID);

            // we have no work to do here if this member has no emails
            if (mailLog == null || mailLog.Both == null)
                return;

            // Order by InsertDate desc and take the top 100 only
            // Calling Sort() on the internal collection will automatically sort in the manner we want
            mailLog.Both.Sort();
            for (int i = 0; i < mailLog.Both.Count && i < 100; i++)
            {
                copyMemberEmail(ttMemberProfileID, memberID, mailLog.Both[i]);
            }
        }

        private void copyMemberEmail(int ttMemberProfileID, int memberID, EmailLogEntry emailLogEntry)
        {
            var command = new Command("mnAdmin", "up_TTMessage_Insert", 0);

            Message message = EmailMessageSA.Instance.RetrieveMessageByMessageID(memberID, emailLogEntry.MailID);

            command.AddParameter("@TTMemberProfileID", SqlDbType.Int, ParameterDirection.Input, ttMemberProfileID);
            command.AddParameter("@MailID", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.MailID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.CommunityID);
            command.AddParameter("@FromMemberID", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.FromMemberID);
            command.AddParameter("@ToMemberID", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.ToMemberID);
            command.AddParameter("@InsertDate", SqlDbType.SmallDateTime, ParameterDirection.Input, emailLogEntry.InsertDate);
            command.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.StatusMask);
            command.AddParameter("@MailTypeID", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.MailType);
            command.AddParameter("@MailOption", SqlDbType.Int, ParameterDirection.Input, emailLogEntry.MailOption);
            command.AddParameter("@MessageHeader", SqlDbType.NVarChar, ParameterDirection.Input, string.IsNullOrEmpty(message.MessageHeader) ? string.Empty : message.MessageHeader);
            command.AddParameter("@MessageBody", SqlDbType.NText, ParameterDirection.Input, string.IsNullOrEmpty(message.MessageBody) ? null : message.MessageBody);
            
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private List<TTMemberAttributeMetadata> getTTAttributeMetadatas()
        {
          
            var command = new Command("mnAdmin", "up_TTMemberAttributeMetadata_List", 0);
            var dt = Client.Instance.ExecuteDataTable(command);

            if (dt == null)
                throw new Exception("Unable to retrieve TTMemberAttributeMetadata info from the database.");

            var metadatas = new List<TTMemberAttributeMetadata>();
            foreach (DataRow row in dt.Rows)
            {
                metadatas.Add(new TTMemberAttributeMetadata
                {
                    TTAttributeID = Convert.ToInt32(row["TTAttributeID"]),
                    MemberAttributeName = row["MemberAttributeName"].ToString(),
                    AttributeDataType =
                        (TTAttributeDataType)Convert.ToInt32(row["DataTypeID"])
                });
            }

            return metadatas;
        }
        #endregion
    }
}
