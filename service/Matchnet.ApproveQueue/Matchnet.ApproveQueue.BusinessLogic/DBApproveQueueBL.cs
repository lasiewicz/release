﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Messaging;
using System.Threading;
using System.Transactions;
using System.Web.Caching;
using System.Data;
using System.Linq;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.ApproveQueue.ValueObjects.ServiceDefinitions;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.ApproveQueue.BusinessLogic
{
    public class DBApproveQueueBL
    {
        public readonly static DBApproveQueueBL Instance = new DBApproveQueueBL();
        private const string CRXWaitTimeSetting = "CRX_RESPONSE_WAIT_TIME";
        private const string CRXPopulateMemberInfo = "CRX_POPULATE_MEMBER_INFO";
        private const string FTAExternalCommunities = "FTA_EXTERNAL_COMMUNITIES";

        private ISettingsSA _settingsService;
        public ISettingsSA SettingsService
        {
            get
            {
                if (_settingsService == null)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }
        
        public void Enqueue(List<QueueItemBase> queueItems)
        {
            foreach (QueueItemBase queueItem in queueItems)
            {
                Enqueue(queueItem);
            }
        }

        public void Enqueue(QueueItemBase queueItem)
        {
            try
            {
                switch (queueItem.GetType().Name)
                {
                    case "QueueItemPhoto":
                        SaveQueueItemPhoto((QueueItemPhoto) queueItem);
                        break;

                    case "QueueItemText":
                        SaveQueueItemText((QueueItemText) queueItem);
                        break;

                    case "QueueItemMember":
                        SaveQueueItemMember((QueueItemMember)queueItem);
                        break;

                    case "QueueItemFacebookLike":
                        SaveQueueItemFacebookLike((QueueItemFacebookLike)queueItem);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to enqueue (queueItem.MemberID: " + queueItem.MemberID.ToString() + ", " +
                    "queueItem.QueueDate: " + queueItem.InsertDate.ToString() + ", " +
                    "queueItem.CommunityID: " + queueItem.CommunityID.ToString() + ").", ex);
            }
        }

        public void EnqueueLight(QueueItemLightBase queueItem)
        {
            try
            {
                switch (queueItem.GetType().Name)
                {
                    // after the deployment, this case should go away since this is a sync write
                    case "QueueItemMemberProfileDuplicate":
                        SaveMemberProfileDuplicate((QueueItemMemberProfileDuplicate)queueItem);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to enqueue a light queue item.", ex);
            }
        }

        public List<QueueCountRecord> GetQueueCounts(QueueItemType itemType)
        {

            try
            {
                List<QueueCountRecord> records = null;

                Command selectCommand = null;

                switch (itemType)
                {
                    case QueueItemType.Photo:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetApprovalPhotoQueueCounts", 1);
                        break;
                    case QueueItemType.Text:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetApprovalTextQueueCounts", 1);
                        break;
                    case QueueItemType.QuestionAnswer:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetApprovalQAQueueCounts", 1);
                        break;
                    case QueueItemType.FacebookLike:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetApprovalFacebookLikeQueueCounts", 1);
                        break;
                    case QueueItemType.CrxSecondReview:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetCRXReviewQueueCounts", 1);
                        break;
                    case QueueItemType.MemberProfileDuplicate:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberProfileDuplicateQueueCount", 1);
                        break;
                    case QueueItemType.MemberProfileDuplicateReview:
                        selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberProfileDuplicateReviewQueueCount", 1);
                        break;
                }

                DataTable dt = Client.Instance.ExecuteDataTable(selectCommand);

                if (dt.Rows.Count > 0)
                {
                    records = new List<QueueCountRecord>();
                    foreach (DataRow row in dt.Rows)
                    {
                        records.Add(DataRowtoQueueCountRecord(row, itemType == QueueItemType.MemberProfileDuplicate));
                    }
                }

                return records;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to GetQueueCounts: itemType: " + itemType.ToString(), ex);
            }
        }

        public List<ApprovalCountRecord> GetApprovalHistoryCounts(DateTime startDate, DateTime endDate, int adminMemberID, int communityID, int siteID)
        {
            try
            {
                List<ApprovalCountRecord> counts = new List<ApprovalCountRecord>();

                Command selectCommand = new Command("mnMemberApproval", "dbo.up_GetApprovalHistoryCounts", adminMemberID);
                selectCommand.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                selectCommand.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
                if (adminMemberID != Constants.NULL_INT)
                {
                    selectCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                }
                if (communityID != Constants.NULL_INT)
                {
                    selectCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                }
                if (siteID != Constants.NULL_INT)
                {
                    selectCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                }

                DataTable dt = Client.Instance.ExecuteDataTable(selectCommand);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        int adminID = Convert.ToInt32(row["AdminMemberID"]);
                        int actionCount = Convert.ToInt32(row["ActionCount"]);
                        CountType countType = (CountType)Convert.ToInt32(row["CountType"]);

                        ApprovalCountRecord currentRecord = (from r in counts where r.AdminMemberID == adminID select r).FirstOrDefault();
                        if (currentRecord == null)
                        {
                            currentRecord = new ApprovalCountRecord();
                            currentRecord.AdminMemberID = adminID;
                            counts.Add(currentRecord);
                        }

                        switch (countType)
                        {
                            case CountType.Photo:
                                currentRecord.PhotoApprovalCount = actionCount;
                                break;
                            case CountType.QAText:
                                currentRecord.QAApprovalCount = actionCount;
                                break;
                            case CountType.RegularText:
                                currentRecord.TextApprovalCount = actionCount;
                                break;
                            case CountType.FacebookLike:
                                currentRecord.FacebookLikeApprovalCount = actionCount;
                                break;
                        }
                    }
                }

                return counts;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Get ApprovalHistoryCounts. StartDate: " + startDate.ToString()
                    + " EndDate: " + endDate.ToString() + " AdminMemberID: " + adminMemberID.ToString() + " CommunityID: " + communityID.ToString()
                    + " SiteID: " + siteID.ToString(), ex);
            }
        }

        public List<MemberProfileDuplicateHistoryCount> GetMemberProfileDuplicateHistoryCount(DateTime startDate, DateTime endDate, int adminMemberID)
        {
            try
            {
                List<MemberProfileDuplicateHistoryCount> counts = new List<MemberProfileDuplicateHistoryCount>();

                Command selectCommand = new Command("mnMemberApproval", "dbo.up_GetMemberProfileDuplicateHistoryCounts", adminMemberID);
                selectCommand.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                selectCommand.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
                if (adminMemberID != Constants.NULL_INT)
                {
                    selectCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                }

                DataTable dt = Client.Instance.ExecuteDataTable(selectCommand);

                if (dt == null || dt.Rows.Count == 0)
                    return null;

                foreach(DataRow row in dt.Rows)
                {
                    counts.Add(new MemberProfileDuplicateHistoryCount() {
                        AdminId = Convert.ToInt32(row["AdminID"]),
                        ActionCount = Convert.ToInt32(row["ActionCount"]),
                        DuplicatesCount = Convert.ToInt32(row["NumberOfDuplicates"]),
                        MatchesCount = Convert.ToInt32(row["Matches"]),
                        SuspendedCount = Convert.ToInt32(row["Suspended"]),
                        AlreadySuspendedCount = Convert.ToInt32(row["AlreadySuspended"])
                    });
                }

                return counts;
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("Unable to get MemberProfileDuplicateHistoryCount. StartDate: {0}, EndDate: {1}, AdminMemberID: {2}",
                    startDate.ToString(), endDate.ToString(), adminMemberID), ex);
            }
        }

        public void CompletePhotoApproval(int memberPhotoID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            try
            {
                Command updateCommand = new Command("mnMemberApproval", "dbo.up_CompletePhotoApproval", memberPhotoID);
                updateCommand.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
                updateCommand.AddParameter("@ApprovalStatus", SqlDbType.Int, ParameterDirection.Input, (int)approvalStatus);
                updateCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                updateCommand.AddParameter("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, adminActionMask);
                Client.Instance.ExecuteAsyncWrite(updateCommand);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete photoApproval for MemberPhotoID: " + memberPhotoID.ToString(), ex);
            }
        }

        public void DeletePhotoQueueItem(int memberPhotoID)
        {
            try
            {
                Command updateCommand = new Command("mnMemberApproval", "dbo.up_DeletePhotoQueueItem", memberPhotoID);
                updateCommand.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
                Client.Instance.ExecuteAsyncWrite(updateCommand);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to delete photo queue item for MemberPhotoID: " + memberPhotoID.ToString(), ex);
            }
        }

        public void CompleteFacebookLikesApproval(int memberId, int siteID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            try
            {
                Command updateCommand = new Command("mnMemberApproval", "dbo.up_CompleteFacebookLikesApproval", memberId);
                updateCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
                updateCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                updateCommand.AddParameter("@ApprovalStatus", SqlDbType.Int, ParameterDirection.Input, (int)approvalStatus);
                updateCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                updateCommand.AddParameter("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, adminActionMask);
                Client.Instance.ExecuteAsyncWrite(updateCommand);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete facebook likes approval for MemberID: " + memberId.ToString(), ex);
            }
        }

        public void DeleteFacebookLikeQueueItem(int memberID, int siteID)
        {
            try
            {
                Command updateCommand = new Command("mnMemberApproval", "dbo.up_DeleteFacebookLikeQueueItem", memberID);
                updateCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                updateCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                Client.Instance.ExecuteAsyncWrite(updateCommand);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to delete facebook like queue item for MemberID: " + memberID.ToString() + ", SiteID: " + siteID.ToString(), ex);
            }
        }

        public void CompleteTextApproval(int memberID, int communityID, int languageID, TextType textType, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            try
            {
                Command updateCommand = new Command("mnMemberApproval", "dbo.up_CompleteTextApproval", memberID);
                updateCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                updateCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                updateCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                updateCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)textType);
                updateCommand.AddParameter("@ApprovalStatusID", SqlDbType.Int, ParameterDirection.Input, (int)approvalStatus);
                updateCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                updateCommand.AddParameter("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, adminActionMask);
                Client.Instance.ExecuteAsyncWrite(updateCommand);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete textApproval for MemberPhotoID: " + memberID.ToString(), ex);
            }

        }

        public void CompleteTextApprovalMemberApproval(int memberID, int memberTextApprovalID,
                                                       ApprovalStatus approvalStatus, int adminMemberID,
                                                       int adminActionMask)
        {
            try
            {
                Command updateCommand = new Command("mnMemberApproval", "dbo.up_CompleteTextApprovalMemberApproval",
                                                    memberID);

                updateCommand.AddParameter("@ApprovalStatusID", SqlDbType.Int, ParameterDirection.Input,
                                           (int) approvalStatus);
                updateCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                updateCommand.AddParameter("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, adminActionMask);
                updateCommand.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input,
                                           memberTextApprovalID);
                Client.Instance.ExecuteAsyncWrite(updateCommand);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete textApproval for MemberApproval view MemberID: " + memberID.ToString(), ex);
            }
        }

        /// <summary>
        /// Use this method if you need to find the text that CRX saw. This method will automatically look at the queue tabe and then in the history table if not found
        /// in the queue table.
        /// </summary>
        /// <param name="crxResponseId">CRXResponseID</param>
        /// <returns></returns>
        public CRXText LocateCRXText(int crxResponseId)
        {
            SqlDataReader reader = null;
             
            try
            {
                var cmd = new Command("mnMemberApproval", "dbo.up_GetCRXTextByCRXResponseID", 0);
                cmd.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, crxResponseId);

                reader = Client.Instance.ExecuteReader(cmd);
                if (reader == null)
                    return null;

                CRXText crxText = null;
                while (reader.Read())
                {
                    crxText = new CRXText()
                                  {
                                      BrandId = Convert.ToInt32(reader["BrandID"]),
                                      SiteId = Convert.ToInt32(reader["SiteID"]),
                                      CommunityId = Convert.ToInt32(reader["CommunityID"]),
                                      QueueInsertDate = Convert.ToDateTime(reader["InsertDate"]),
                                      TextContent = reader["TextContent"].ToString()
                                  };

                }
                
                return crxText;
            }
            catch (Exception ex)
            {
                throw new BLException("Error locating the CRX text with CRXResponseId: " + crxResponseId.ToString(), ex);
            }
            finally
            {
                if(reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// Takes an item off the MemberProfileDuplicate queue.
        /// </summary>
        /// <returns>NULL if queue has no item to process otherwise populated QueueItemMemberProfileDuplicate object.</returns>
        public QueueItemMemberProfileDuplicate DequeueMemberProfileDuplicateByLanguage(int languageId)
        {
            try
            {
                var getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberProfileDuplicateQueueByLanguage", 0);
                getCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageId);
                var result = Client.Instance.ExecuteDataSet(getCommand);

                return ConvertToQueueItemMemberProfileDuplicate(result);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Dequeue a MemberProfileDuplicate queue item.", ex);
            }
        }

        public QueueItemMemberProfileDuplicate GetMemberProfileDuplicateQueueItem(int duplicateQueueID)
        {
            try
            {
                var getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberProfileDuplicateQueueByID", 0);
                getCommand.AddParameter("@MemberProfileDuplicateQueueID", SqlDbType.Int, ParameterDirection.Input, duplicateQueueID);
                var result = Client.Instance.ExecuteDataSet(getCommand);

                return ConvertToQueueItemMemberProfileDuplicate(result);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Dequeue a MemberProfileDuplicate queue item.", ex);
            }
        }

        public QueueItemMemberProfileDuplicateReview DequeueMemberProfileDuplicateReview(int languageID)
        {
            try
            {
                var getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberProfileDuplicateReviewQueue", 0);
                getCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                var result = Client.Instance.ExecuteDataSet(getCommand);

                return ConvertToQueueItemMemberProfileDuplicateReview(result);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Dequeue a MemberProfileDuplicateReview queue item.", ex);
            }
        }

        public QueueItemMemberProfileDuplicateReview GetMemberProfileDuplicateReviewQueueItem(int memberProfileDuplicateHistoryID)
        {
            try
            {
                var getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberProfileDuplicateReviewQueueByID", 0);
                getCommand.AddParameter("@MemberProfileDuplicateHistoryID", SqlDbType.Int, ParameterDirection.Input, memberProfileDuplicateHistoryID);
                var result = Client.Instance.ExecuteDataSet(getCommand);

                return ConvertToQueueItemMemberProfileDuplicateReview(result);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve a MemberProfileDuplicateReview queue item.", ex);
            }
        }

        public QueueItemText DequeueTextByLanguage(Int32 languageID, TextType textType)
        {
            try
            {
                QueueItemText queuedText = null;
                int CRXWaitTime = 0;
                Int32.TryParse(RuntimeSettings.GetSetting(CRXWaitTimeSetting), out CRXWaitTime);

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetTextToApproveByLanguage", languageID);
                getCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                getCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)textType);
                //Pass CRX Response Wait time only for Text and not QA
                if (textType == TextType.Regular)
                    getCommand.AddParameter("@CRXWaitTimeInMinutes", SqlDbType.Int, ParameterDirection.Input, CRXWaitTime);
                //Commenting the code until Provo is ready to use API to add mingle items to FTA queue
                //Get external communities from setting. Do not get queueitem from these communities, since those free texts are handled in a separate queue
                //string externalCommunities = SettingsService.GetSettingFromSingleton(FTAExternalCommunities);
                //if(!string.IsNullOrEmpty(externalCommunities))
                //    getCommand.AddParameter("@ExternalCommunityIds", SqlDbType.NVarChar, ParameterDirection.Input, externalCommunities);

                DataTable texts = Client.Instance.ExecuteDataTable(getCommand);
                if (texts != null && texts.Rows.Count > 0)
                {
                    queuedText = DataRowToItemText(texts.Rows[0]);
                }

                return queuedText;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Dequeue by language. LanguageID: " + languageID.ToString() + " TextType:" + textType.ToString(), ex);
            }
        }

        public QueueItemText DequeueTextBySite(Int32 siteID, TextType textType)
        {
            try
            {
                QueueItemText queuedText = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetTextToApproveBySite", siteID);
                getCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                getCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)textType);

                DataTable texts = Client.Instance.ExecuteDataTable(getCommand);
                if (texts != null && texts.Rows.Count > 0)
                {
                    queuedText = DataRowToItemText(texts.Rows[0]);
                }

                return queuedText;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Dequeue by site. SiteID: " + siteID.ToString() + " TextType:" + textType.ToString(), ex);
            }
        }

        public QueueItemText DequeueTextByCommunity(Int32 communityID, TextType textType)
        {

            try
            {
                QueueItemText queuedText = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetTextToApproveByCommunity", communityID);
                getCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                getCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)textType);

                DataTable texts = Client.Instance.ExecuteDataTable(getCommand);
                if (texts != null && texts.Rows.Count > 0)
                {
                    queuedText = DataRowToItemText(texts.Rows[0]);
                }

                return queuedText;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Dequeue by community. CommunityID: " + communityID.ToString() + " TextType:" + textType.ToString(), ex);
            }
        }

        public QueueItemText DequeueTextByMember(Int32 memberID, TextType textType)
        {
            try
            {
                QueueItemText queuedText = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.GetTextToApproveByMember", memberID);
                getCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                getCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)textType);

                DataTable texts = Client.Instance.ExecuteDataTable(getCommand);
                if (texts != null && texts.Rows.Count > 0)
                {
                    queuedText = DataRowToItemText(texts.Rows[0]);
                }

                return queuedText;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue text by member. MemberID: " + memberID.ToString() + " TextType:" + textType.ToString(), ex);
            }
        }
        
        public List<QueueItemBase> DequeuePhotosByCommunity(int maxCount, int communityID)
        {
            try
            {
                List<QueueItemBase> queuedPhotos = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetPhotosToApproveByCommunity", communityID);
                getCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                getCommand.AddParameter("@MaxCount", SqlDbType.Int, ParameterDirection.Input, maxCount);

                DataTable photos = Client.Instance.ExecuteDataTable(getCommand);
                if (photos != null && photos.Rows.Count > 0)
                {
                    queuedPhotos = new List<QueueItemBase>();
                    foreach (DataRow row in photos.Rows)
                    {
                        queuedPhotos.Add(DataRowToQueueItemPhoto(row));
                    }
                }

                return queuedPhotos;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue photos by community. CommunityID: " + communityID.ToString() + " MaxCount:" + maxCount.ToString(), ex);
            }
        }

        public List<QueueItemBase> DequeuePhotosBySite(int maxCount, int siteID)
        {
            try
            {
                List<QueueItemBase> queuedPhotos = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetPhotosToApproveBySite", siteID);
                getCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                getCommand.AddParameter("@MaxCount", SqlDbType.Int, ParameterDirection.Input, maxCount);

                DataTable photos = Client.Instance.ExecuteDataTable(getCommand);
                if (photos != null && photos.Rows.Count > 0)
                {
                    queuedPhotos = new List<QueueItemBase>();
                    foreach (DataRow row in photos.Rows)
                    {
                        queuedPhotos.Add(DataRowToQueueItemPhoto(row));
                    }
                }

                return queuedPhotos;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue photos by site. SiteID: " + siteID.ToString() + " MaxCount:" + maxCount.ToString(), ex);
            }
        }

        public List<QueueItemBase> DequeuePhotosByMember(int maxCount, int memberID)
        {
            try
            {
                List<QueueItemBase> queuedPhotos = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetPhotosToApproveByMember", memberID);
                getCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                getCommand.AddParameter("@MaxCount", SqlDbType.Int, ParameterDirection.Input, maxCount);

                DataTable photos = Client.Instance.ExecuteDataTable(getCommand);
                if (photos != null && photos.Rows.Count > 0)
                {
                    queuedPhotos = new List<QueueItemBase>();
                    foreach (DataRow row in photos.Rows)
                    {
                        queuedPhotos.Add(DataRowToQueueItemPhoto(row));
                    }
                }

                return queuedPhotos;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue photos by member. MemberID: " + memberID.ToString() + " MaxCount:" + maxCount.ToString(), ex);
            }
        }

        public List<QueueItemMember>  DequeueMemberBySite(int siteID)
        {
            try
            {
                var membersToReview = new List<QueueItemMember>();

                var command = new Command("mnMemberApproval", "dbo.up_GetMemberFraudReview", siteID);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

                DataTable members = Client.Instance.ExecuteDataTable(command);
                if(members != null && members.Rows.Count > 0)
                {
                    foreach (DataRow row in members.Rows)
                    {
                        membersToReview.Add(new QueueItemMember(Constants.NULL_INT, 
                            Convert.ToInt32(row["SiteID"]),Constants.NULL_INT,Convert.ToInt32(row["MemberID"]),Convert.ToInt32(row["AdminMemberID"]),
                            Convert.ToDateTime(row["InsertDate"])));
                    }
                }
                return membersToReview;
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("Unable to get members from fraud review queue. SiteID: {0}",siteID), ex);
            }
        }

        public List<QueueItemBase> DequeueFacebookLikesByCommunity(int maxCount, int communityID)
        {
            try
            {
                List<QueueItemBase> queuedFBLikes = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetFacebookLikesToApproveByCommunity", communityID);
                getCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                getCommand.AddParameter("@MaxCount", SqlDbType.Int, ParameterDirection.Input, maxCount);

                DataTable fbLikes = Client.Instance.ExecuteDataTable(getCommand);
                if (fbLikes != null && fbLikes.Rows.Count > 0)
                {
                    queuedFBLikes = new List<QueueItemBase>();
                    foreach (DataRow row in fbLikes.Rows)
                    {
                        queuedFBLikes.Add(DataRowToQueueItemFacebookLike(row));
                    }
                }

                return queuedFBLikes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue FacebookLikes by community. CommunityID: " + communityID.ToString() + " MaxCount:" + maxCount.ToString(), ex);
            }
        }

        public List<QueueItemBase> DequeueFacebookLikesBySite(int maxCount, int siteID)
        {
            try
            {
                List<QueueItemBase> queuedFBLikes = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetFacebookLikesToApproveBySite", siteID);
                getCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                getCommand.AddParameter("@MaxCount", SqlDbType.Int, ParameterDirection.Input, maxCount);

                DataTable fbLikes = Client.Instance.ExecuteDataTable(getCommand);
                if (fbLikes != null && fbLikes.Rows.Count > 0)
                {
                    queuedFBLikes = new List<QueueItemBase>();
                    foreach (DataRow row in fbLikes.Rows)
                    {
                        queuedFBLikes.Add(DataRowToQueueItemFacebookLike(row));
                    }
                }

                return queuedFBLikes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue FacebookLikes by site. SiteID: " + siteID.ToString() + " MaxCount:" + maxCount.ToString(), ex);
            }
        }

        public List<QueueItemBase> DequeueFacebookLikesByMember(int maxCount, int memberID)
        {
            try
            {
                List<QueueItemBase> queuedFBLikes = null;

                Command getCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetFacebookLikesToApproveByMember", memberID);
                getCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                getCommand.AddParameter("@MaxCount", SqlDbType.Int, ParameterDirection.Input, maxCount);

                DataTable fbLikes = Client.Instance.ExecuteDataTable(getCommand);
                if (fbLikes != null && fbLikes.Rows.Count > 0)
                {
                    queuedFBLikes = new List<QueueItemBase>();
                    foreach (DataRow row in fbLikes.Rows)
                    {
                        queuedFBLikes.Add(DataRowToQueueItemFacebookLike(row));
                    }
                }

                return queuedFBLikes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to dequeue FacebookLikes by member. MemberID: " + memberID.ToString() + " MaxCount:" + maxCount.ToString(), ex);
            }
        }

        public void CompleteMemberFraudReview(List<QueueItemMember> memberItems)
        {
            try
            {
                foreach (var queueItemMember in memberItems)
                {
                    CompleteFraudReview(queueItemMember);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete member fraud review", ex);
            }
        }

        public List<IndividualTextApprovalStatus> GetIndividualTextApprovalStatuses()
        {
            try
            {
                List<IndividualTextApprovalStatus> statuses = null;

                Command getCommand = new Command("mnMemberApproval", "dbo.up_IndividualTextApprovalStatus_List", 0);

                DataTable statusRecords = Client.Instance.ExecuteDataTable(getCommand);
                if (statusRecords != null && statusRecords.Rows.Count > 0)
                {
                    statuses = new List<IndividualTextApprovalStatus>();
                    foreach (DataRow row in statusRecords.Rows)
                    {
                        statuses.Add(DataRowToIndividualTextApprovalStatus(row));
                    }
                }

                return statuses;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to Get Individual Text Approval Statuses", ex);
            }
        }

        public void SaveIndividualTextAttributeApproval(int memberID, int memberAttributeGroupID, int languageID, 
            int statusMask, int adminMemberID, string textContent, bool isEdit)
        {
            int individualTextAttributeApprovalID = KeySA.Instance.GetKey("IndividualTextAttributeApprovalID");
            
            Command saveCommand = new Command("mnMemberApproval", "dbo.up_AddIndividualTextAttributeApproval", memberID);
            saveCommand.AddParameter("@IndividualTextAttributeApprovalID", SqlDbType.Int, ParameterDirection.Input, individualTextAttributeApprovalID);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            saveCommand.AddParameter("@MemberAttributeGroupID", SqlDbType.Int, ParameterDirection.Input, memberAttributeGroupID);
            saveCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
            saveCommand.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, statusMask);
            saveCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            saveCommand.AddParameter("@TextContent", SqlDbType.NVarChar, ParameterDirection.Input, textContent);
            saveCommand.AddParameter("@IsEdit", SqlDbType.Bit, ParameterDirection.Input, isEdit);
            Client.Instance.ExecuteAsyncWrite(saveCommand);
        }

        //to be deprecated
        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
            int statusMask, int adminMemberID, string textContent, bool isEdit,
            string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
            string occupationDescription, int educationLevel, int religion, int ethnicity)
        {
            SaveIndividualTextAttributeApprovalExpanded(memberAttributeGroupID, memberAttributeGroupID, languageID, statusMask,
                adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription,
                educationLevel, religion, ethnicity, -1, string.Empty, string.Empty, DateTime.MinValue);
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask,
           int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus,
           string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity,
           int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate
           )
        {
            SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID,
                                                        statusMask, adminMemberID, textContent, isEdit,
                                                        ip, profileRegionId, genderMask, maritalStatus, firstName,
                                                        lastName, email,
                                                        occupationDescription, educationLevel, religion, ethnicity,
                                                        daysSinceFirstSubscription, billingPhoneNumber,
                                                        subscriptionStatus, registrationDate, null, "", ApprovalStatus.Completed);
        }

         public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
            int statusMask, int adminMemberID, string textContent, bool isEdit, 
            string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
            string occupationDescription, int educationLevel, int religion, int ethnicity,
            int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, 
            int? memberTextApprovalID, string adminTextContent)
         {
             SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID,
                                                        statusMask, adminMemberID, textContent, isEdit,
                                                        ip, profileRegionId, genderMask, maritalStatus, firstName,
                                                        lastName, email,
                                                        occupationDescription, educationLevel, religion, ethnicity,
                                                        daysSinceFirstSubscription, billingPhoneNumber,
                                                        subscriptionStatus, registrationDate, memberTextApprovalID, adminTextContent, ApprovalStatus.Completed);
         }


        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
            int statusMask, int adminMemberID, string textContent, bool isEdit, 
            string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
            string occupationDescription, int educationLevel, int religion, int ethnicity,
            int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, 
            int? memberTextApprovalID, string adminTextContent, ApprovalStatus reviewStatus)
        {
            SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID,
                                                        statusMask, adminMemberID, textContent, isEdit,
                                                        ip, profileRegionId, genderMask, maritalStatus, firstName,
                                                        lastName, email,
                                                        occupationDescription, educationLevel, religion, ethnicity,
                                                        daysSinceFirstSubscription, billingPhoneNumber,
                                                        subscriptionStatus, registrationDate,
                                                        memberTextApprovalID, adminTextContent, reviewStatus, false);
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
            int statusMask, int adminMemberID, string textContent, bool isEdit, 
            string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
            string occupationDescription, int educationLevel, int religion, int ethnicity,
            int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, 
            int? memberTextApprovalID, string adminTextContent, ApprovalStatus reviewStatus, bool deleteMovedRows)
        {
            int individualTextAttributeApprovalID = KeySA.Instance.GetKey("IndividualTextAttributeApprovalID");

            Command saveCommand = new Command("mnMemberApproval", "dbo.up_AddIndividualTextAttributeApproval", memberID);
            saveCommand.AddParameter("@IndividualTextAttributeApprovalID", SqlDbType.Int, ParameterDirection.Input, individualTextAttributeApprovalID);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            saveCommand.AddParameter("@MemberAttributeGroupID", SqlDbType.Int, ParameterDirection.Input, memberAttributeGroupID);
            saveCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
            saveCommand.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, statusMask);
            saveCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            saveCommand.AddParameter("@TextContent", SqlDbType.NVarChar, ParameterDirection.Input, textContent);
            saveCommand.AddParameter("@IsEdit", SqlDbType.Bit, ParameterDirection.Input, isEdit);
            if (memberTextApprovalID.HasValue)
                saveCommand.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalID);
            if (!string.IsNullOrEmpty(adminTextContent))
                saveCommand.AddParameter("@AdminTextContent", SqlDbType.NVarChar, ParameterDirection.Input, adminTextContent);
            saveCommand.AddParameter("@ReviewStatus", SqlDbType.Int, ParameterDirection.Input, (int)reviewStatus);
            saveCommand.AddParameter("@DeleteMovedRows", SqlDbType.Bit, ParameterDirection.Input, deleteMovedRows);
            Client.Instance.ExecuteAsyncWrite(saveCommand);

            //This info should be available when an item enters the text approval queue when CRX_POPULATE_MEMBER_INFO is ON. Otherwise, this logic should occur in SaveQueueItemText method.
            bool populateCrxMemberAttr = false;
            bool.TryParse(SettingsService.GetSettingFromSingleton(CRXPopulateMemberInfo), out populateCrxMemberAttr);
            if (!string.IsNullOrEmpty(email) && !populateCrxMemberAttr)
            {
                Command saveCommand2 = new Command("mnMemberApproval", "dbo.up_AddIndividualTextAttributeApprovalExpanded", memberID);
                saveCommand2.AddParameter("@IndividualTextAttributeApprovalID", SqlDbType.Int, ParameterDirection.Input, individualTextAttributeApprovalID);
                saveCommand2.AddParameter("@IP", SqlDbType.NVarChar, ParameterDirection.Input, ip == string.Empty ? null : ip);
                if (profileRegionId != Constants.NULL_INT)
                    saveCommand2.AddParameter("@ProfileRegionID", SqlDbType.Int, ParameterDirection.Input, profileRegionId);
                if (genderMask != Constants.NULL_INT)
                    saveCommand2.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, genderMask);
                if (maritalStatus != Constants.NULL_INT)
                    saveCommand2.AddParameter("@MaritalStatus", SqlDbType.Int, ParameterDirection.Input, maritalStatus);
                saveCommand2.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, firstName == string.Empty ? null : firstName);
                saveCommand2.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, lastName == string.Empty ? null : lastName);
                saveCommand2.AddParameter("@Email", SqlDbType.NVarChar, ParameterDirection.Input, email == string.Empty ? null : email);
                saveCommand2.AddParameter("@OccupationDescription", SqlDbType.NVarChar, ParameterDirection.Input, occupationDescription == string.Empty ? null : occupationDescription);
                if (educationLevel != Constants.NULL_INT)
                    saveCommand2.AddParameter("@EducationLevel", SqlDbType.Int, ParameterDirection.Input, educationLevel);
                if (religion != Constants.NULL_INT)
                    saveCommand2.AddParameter("@Religion", SqlDbType.Int, ParameterDirection.Input, religion);
                if (ethnicity != Constants.NULL_INT)
                    saveCommand2.AddParameter("@Ethnicity", SqlDbType.Int, ParameterDirection.Input, ethnicity);
                if (billingPhoneNumber != string.Empty)
                    saveCommand2.AddParameter("@BillingPhoneNumber", SqlDbType.VarChar, ParameterDirection.Input, billingPhoneNumber);
                if (subscriptionStatus != string.Empty)
                    saveCommand2.AddParameter("@SubscriptionStatus", SqlDbType.VarChar, ParameterDirection.Input, subscriptionStatus);
                if (registrationDate != DateTime.MinValue)
                    saveCommand2.AddParameter("@RegistrationDate", SqlDbType.DateTime, ParameterDirection.Input, registrationDate);
                Client.Instance.ExecuteAsyncWrite(saveCommand2);
            }
        }

        public List<QueueItemBase> GetApprovalPhotoQueueItems(int siteID)
        {
            try
            {
                List<QueueItemBase> photoItems = null;

                var getCommand = new Command("mnMemberApproval", "up_GetApprovalPhotoQueueItems", 0);
                getCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

                DataTable queueItems = Client.Instance.ExecuteDataTable(getCommand);
                if (queueItems != null && queueItems.Rows.Count > 0)
                {
                    photoItems = (from DataRow row in queueItems.Rows select DataRowToQueueItemPhoto(row)).Cast<QueueItemBase>().ToList();
                }

                return photoItems;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get photo approval queue items", ex);
            }
        }

        public void SaveMemberTextApprovalAttributes(List<QueueItemText> queueItemTexts)
        {
            try
            {
                foreach (var queueItemText in queueItemTexts)
                {
                    SaveMemberTextApprovalAttribute(queueItemText);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to save member approval attributes", ex);
            }
        }

        /// <summary>
        /// This method only populates the ID fields. This method is used to determine which items were actually displayed on the UI so that
        /// we know which ones to delete.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="languageID"></param>
        /// <param name="textType"></param>
        /// <returns></returns>
        public List<QueueItemText> GetTextAttributesForMember(int memberID, int communityID, int languageID, TextType textType)
        {
            List<QueueItemText> textApprovalAttributes = null;
            try
            {
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetMemberTextApprovalAttributeByMember", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                command.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)textType);

                DataTable texts = Client.Instance.ExecuteDataTable(command);
                if ((texts != null) && (texts.Rows.Count > 0))
                {
                    textApprovalAttributes = DataTableToMemberTextApprovalAttributes(texts);
                }
                return textApprovalAttributes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get member text approval attributes for a member");
            }    
        }

        public List<QueueItemText> GetTextAttributesForCRXApproval(int? communityId, int? languageId, int? textType, int? numberOfRows)
        {
            var memberAttributes = new List<QueueItemText>();
            try
            {
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetTextAttributesForCRXApproval", 0);
                if (communityId.HasValue)
                {
                    command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId.Value);
                }
                if (languageId.HasValue)
                {
                    command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageId.Value);
                }
                if (textType.HasValue)
                {
                    command.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, textType.Value);
                }
                if (numberOfRows.HasValue)
                {
                    command.AddParameter("@Rows", SqlDbType.Int, ParameterDirection.Input, numberOfRows.Value);
                }
                DataTable texts = Client.Instance.ExecuteDataTable(command);
                if ((texts != null) && (texts.Rows.Count > 0))
                {
                    memberAttributes = DataRowToCRXTextItem(texts);
                }
                return memberAttributes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get member text approval attributes", ex);
            }
        }

        public void CompleteCRXTextAttributeArroval(QueueItemText itemText)
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_CompleteCRXTextAttributeApproval", 0);
                command.AddParameter("@UpdateCRXOnly", SqlDbType.Bit, ParameterDirection.Input, itemText.UpdateCRXOnly);
                command.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, itemText.CRXResponseID);
                command.AddParameter("@CRXApproval", SqlDbType.Bit, ParameterDirection.Input, itemText.CRXApproval);
                command.AddParameter("@CRXApprovalDisapprovalWeight", SqlDbType.Float, ParameterDirection.Input, itemText.CRXApprovalDisapprovalWeight);
                command.AddParameter("@CRXNotes", SqlDbType.NVarChar, ParameterDirection.Input, itemText.CRXNotes);
                command.AddParameter("@CRXAdminVerify", SqlDbType.Bit, ParameterDirection.Input, itemText.CRXAdminVerify);
                //Complete text attribute approval 
                if (!itemText.UpdateCRXOnly)
                {
                    command.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, itemText.MemberTextApprovalID);
                    int individualTextAttributeApprovalID = KeySA.Instance.GetKey("IndividualTextAttributeApprovalID");
                    command.AddParameter("@IndividualTextAttributeApprovalID", SqlDbType.Int, ParameterDirection.Input, individualTextAttributeApprovalID);
                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, itemText.MemberID);
                    command.AddParameter("@MemberAttributeGroupID", SqlDbType.Int, ParameterDirection.Input, itemText.MemberAttributeGroupID);
                    command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, itemText.LanguageID);
                    command.AddParameter("@TextContent", SqlDbType.NVarChar, ParameterDirection.Input, itemText.TextContent);
                }
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to save member text approval attributes", ex);
            }
        }

        public void SaveAdminReviewSettings(List<AdminReviewSetting> adminSettings)
        {
            foreach (var adminReviewSetting in adminSettings)
            {
                SaveAdminReviewSettings(adminReviewSetting.AdminMemberId, adminReviewSetting.DailySampleRate,
                                        adminReviewSetting.OnDemandSampleRate, true);
            }
        }

        public void SaveAdminReviewSettings(int adminMemberId, int? dailySampleRate, int? onDemandSampleRate, bool isEdit)
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_SaveAdminApprovalReviewSettings", adminMemberId);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
                if (dailySampleRate.HasValue)
                    command.AddParameter("@DailySampleRate", SqlDbType.Int, ParameterDirection.Input, dailySampleRate);
                if (onDemandSampleRate.HasValue)
                    command.AddParameter("@OnDemandSampleRate", SqlDbType.Int, ParameterDirection.Input, onDemandSampleRate);
                command.AddParameter("@IsEdit", SqlDbType.Bit, ParameterDirection.Input, isEdit);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("Unable to save admin review settings. Admin Member ID: {0}", adminMemberId.ToString()), ex);
            }
        }

        public TextApprovalReviewCount GetTextApprovalReviewCounts(bool isOnDemand)
        {
            try
            {
                var reviewCount = new TextApprovalReviewCount();
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetTextApprovalReviewCounts", 0);
                command.AddParameter("@IsOnDemand", SqlDbType.Bit, ParameterDirection.Input, isOnDemand);
                DataTable dt = Client.Instance.ExecuteDataTable(command);
                if (dt != null && dt.Rows.Count > 0)
                {
                    reviewCount.TotalItems = Convert.ToInt32(dt.Rows[0]["TotalItems"]);
                    reviewCount.LatestRecordDate = (dt.Rows[0]["Latest"] != DBNull.Value) ? Convert.ToDateTime(dt.Rows[0]["Latest"]) : DateTime.MinValue;
                    reviewCount.OldRecordDate = (dt.Rows[0]["Oldest"] != DBNull.Value) ? Convert.ToDateTime(dt.Rows[0]["Oldest"]) : DateTime.MinValue;
                }
                return reviewCount;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get admin FTA review counts.", ex);
            }
        }

        public ReviewText GetTextToReview(bool isOnDemand, int? supervisorId, int? adminMemberId)
        {
            try
            {
                var textReview = new ReviewText();
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetTextToReview", 0);
                command.AddParameter("@IsOnDemand", SqlDbType.Bit, ParameterDirection.Input, isOnDemand);
                if(supervisorId.HasValue)
                {
                    command.AddParameter("@SupervisorID", SqlDbType.Int, ParameterDirection.Input, supervisorId);
                }
                if (adminMemberId.HasValue)
                {
                    command.AddParameter("@AdminMemberId", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
                }
                DataSet ds = Client.Instance.ExecuteDataSet(command);
                if (ds != null && ds.Tables.Count > 0)
                {
                    textReview = ConvertToReviewText(ds);
                }
                return textReview;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get admin FTA for review.", ex);
            }
        }

        public void CompleteAdminTextApprovalReview(ReviewText reviewText)
        {
            try
            {
                int adminTextApprovalReviewHistoryId = KeySA.Instance.GetKey("AdminTextApprovalReviewHistoryID");
                var command = new Command("mnMemberApproval", "dbo.up_CompleteAdminTextApprovalReview", 0);
                command.AddParameter("@AdminTextApprovalReviewHistoryID", SqlDbType.Int, ParameterDirection.Input, adminTextApprovalReviewHistoryId);
                command.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, reviewText.MemberTextApprovalId);
                command.AddParameter("@SupervisorID", SqlDbType.Int, ParameterDirection.Input, reviewText.SupervisorId);
                command.AddParameter("@SupervisorReviewStatusID", SqlDbType.Int, ParameterDirection.Input, (int)reviewText.SupervisorReviewStatusId);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, reviewText.CommunityId);
                command.AddParameter("@Notes", SqlDbType.NVarChar, ParameterDirection.Input, reviewText.SupervisorNotes);
                Client.Instance.ExecuteAsyncWrite(command);

                if (reviewText.TextAttributes != null && reviewText.TextAttributes.Count > 0)
                {
                    foreach (ReviewTextAttribute textAttribute in reviewText.TextAttributes)
                    {
                        SaveSupervisorReview(reviewText.MemberTextApprovalId,
                                                       textAttribute.AttributeGroupId,
                                                       textAttribute.SupervisorStatusMask,
                                                       null,null,null, null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete admin FTA review.", ex);
            }
        }

        public void AcknowledgeTrainingReport(int memberTextApprovalId, int adminMemberId, int supervisorId, bool isOnDemand)
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_AcknowledgeTrainingReport", 0);
                command.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalId);
                command.AddParameter("@SupervisorID", SqlDbType.Int, ParameterDirection.Input, supervisorId);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
                command.AddParameter("@IsOnDemand", SqlDbType.Bit, ParameterDirection.Input, isOnDemand);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to acknowledge admin FTA training report.", ex);
            }
        }

        public List<TextApprovalReviewHistoryCount> GetTextApprovalReviewHistoryCounts(DateTime startDate, DateTime endDate, int? adminMemberId, int? communityId)
        {
            try
            {
                var historyCounts = new List<TextApprovalReviewHistoryCount>();

                var selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetTextApprovalReviewHistoryCounts", 0);
                selectCommand.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                selectCommand.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
                if (adminMemberId.HasValue)
                {
                    selectCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberId.Value);
                }
                if (communityId.HasValue)
                {
                    selectCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId.Value);
                }

                DataTable dt = Client.Instance.ExecuteDataTable(selectCommand);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        int adminId = Convert.ToInt32(row["AdminMemberID"]);
                        int actionCount = Convert.ToInt32(row["TotalCount"]);
                        var countType = (HistoryCountType)Convert.ToInt32(row["CountType"]);

                        TextApprovalReviewHistoryCount historyCount = (from r in historyCounts where r.AdminMemberId == adminId select r).FirstOrDefault();
                        if (historyCount == null)
                        {
                            historyCount = new TextApprovalReviewHistoryCount { AdminMemberId = adminId };
                            historyCounts.Add(historyCount);
                        }

                        switch (countType)
                        {
                            case HistoryCountType.Processed:
                                historyCount.TotalFTAProcessedCount = actionCount;
                                break;
                            case HistoryCountType.Sampled:
                                historyCount.TotalSampledCount = actionCount;
                                break;
                            case HistoryCountType.Reviewed:
                                historyCount.TotalReviewedCount = actionCount;
                                break;
                            case HistoryCountType.Approved:
                                historyCount.TotalApprovedCount = actionCount;
                                break;
                            case HistoryCountType.Corrected:
                                historyCount.TotalCorrectedCount = actionCount;
                                break;
                        }
                    }
                }

                return historyCounts;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get Text Approval Review HistoryCounts.", ex);
            }
        }

        public List<AdminReviewSetting> GetAdminReviewSampleRates()
        {
            try
            {
                var settings = new List<AdminReviewSetting>();
                var selectCommand = new Command("mnMemberApprovalWrite", "dbo.up_GetAdminApprovalReviewSettings", 0);
                DataTable dt = Client.Instance.ExecuteDataTable(selectCommand);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        settings.Add(new AdminReviewSetting
                        {
                            AdminMemberId = Convert.ToInt32(row["AdminMemberID"]),
                            DailySampleRate = Convert.ToInt32(row["DailySampleRate"])
                        });
                    }
                }
                return settings;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get Admin review sample rates.", ex);
            }
        }

        public ReviewText GetCRXTextForSecondReview()
        {
            try
            {
                var textReview = new ReviewText();
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetCRXReviewText", 0);
                DataSet ds = Client.Instance.ExecuteDataSet(command);
                if (ds != null && ds.Tables.Count > 0)
                {
                    textReview = ConvertToReviewText(ds);
                }
                return textReview;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get admin FTA for review.", ex);
            }
        }

        public void CompleteCRXSecondReview(ReviewText reviewText)
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_CompleteCRXSecondReview", 0);
                command.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, reviewText.MemberTextApprovalId);
                command.AddParameter("@ReviewerMemberID", SqlDbType.Int, ParameterDirection.Input, reviewText.ReviewerId);
                command.AddParameter("@ReviewerActionMask", SqlDbType.Int, ParameterDirection.Input, reviewText.ReviewerActionMask);
                command.AddParameter("@ReviewerAccount", SqlDbType.NVarChar, ParameterDirection.Input, reviewText.ReviewerAccount);
                Client.Instance.ExecuteAsyncWrite(command);

                if (reviewText.TextAttributes != null && reviewText.TextAttributes.Count > 0)
                {
                    foreach (ReviewTextAttribute textAttribute in reviewText.TextAttributes)
                    {
                        SaveSupervisorReview(reviewText.MemberTextApprovalId,
                                                       textAttribute.AttributeGroupId,
                                                       null,
                                                       textAttribute.ReviewerStatusMask,
                                                       textAttribute.ReviewerTextContent,
                                                       textAttribute.ReviewStatus,
                                                       textAttribute.CRXTie,
                                                       textAttribute.ToolCorrectionRequested);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete admin FTA review.", ex);
            }
        }

        public TextApprovalReviewCount GetCRXSecondReviewCounts()
        {
            try
            {
                var reviewCount = new TextApprovalReviewCount();
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetCRXSecondReviewCounts", 0);
                DataTable dt = Client.Instance.ExecuteDataTable(command);
                if (dt != null && dt.Rows.Count > 0)
                {
                    reviewCount.TotalItems = Convert.ToInt32(dt.Rows[0]["TotalItems"]);
                    reviewCount.LatestRecordDate = (dt.Rows[0]["Latest"] != DBNull.Value) ? Convert.ToDateTime(dt.Rows[0]["Latest"]) : DateTime.MinValue;
                    reviewCount.OldRecordDate = (dt.Rows[0]["Oldest"] != DBNull.Value) ? Convert.ToDateTime(dt.Rows[0]["Oldest"]) : DateTime.MinValue;
                }
                return reviewCount;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get FTA CRX second review counts.", ex);
            }
        }

        public void RevertCRXResponse()
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_RevertCRXResponse", 0);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to revert CRX Response", ex);
            }
        }

        public QueueItemText GetCRXTextAttributeInfo(int CRXResponseId)
        {
            var crxText = new QueueItemText();
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_GetCRXTextAttributeInfo", 0);
                command.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, CRXResponseId);
                DataTable response = Client.Instance.ExecuteDataTable(command);
                if ((response != null) && (response.Rows.Count > 0))
                {
                    crxText.MemberTextApprovalID = Convert.ToInt32(response.Rows[0]["MemberTextApprovalID"]);
                    crxText.MemberID = Convert.ToInt32(response.Rows[0]["MemberID"]);
                    crxText.CommunityID = Convert.ToInt32(response.Rows[0]["CommunityID"]);
                    crxText.LanguageID = Convert.ToInt32(response.Rows[0]["LanguageID"]);
                    crxText.MemberAttributeGroupID = Convert.ToInt32(response.Rows[0]["MemberAttributeGroupID"]);
                    crxText.TextContent = Convert.ToString(response.Rows[0]["TextContent"]);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get CRX Text attribute Info", ex);
            }
            return crxText;
        }

        public DateTime GetLastCRXResponseResetTime(int CRXResponseID)
        {
            DateTime ret = DateTime.MinValue;
            SqlDataReader dataReader = null;

            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_GetLastCRXResponseResetTime", 0);
                command.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, CRXResponseID);

                dataReader = Client.Instance.ExecuteReader(command);
                if(dataReader != null && dataReader.Read())
                {
                    ret = Convert.ToDateTime(dataReader[0]);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to retrieve the last CRXResponseResetTime for CRXResponseID: " + CRXResponseID.ToString());
            }
            finally
            {
                if(dataReader != null)
                    dataReader.Close();
            }

            return ret;
        }

        public QueueItemText GetCRXResponse(int CRXResponseId)
        {
            var crxResponse = new QueueItemText();
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_GetCRXResponse", 0);
                command.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, CRXResponseId);
                DataTable response = Client.Instance.ExecuteDataTable(command);
                if ((response != null) && (response.Rows.Count > 0))
                {
                    crxResponse.CRXAdminVerify = Convert.ToBoolean(response.Rows[0]["CRXAdminVerify"]);
                    crxResponse.CRXApproval = Convert.ToBoolean(response.Rows[0]["CRXApproval"]);
                    crxResponse.CRXApprovalDisapprovalWeight = (float)Convert.ToDouble(response.Rows[0]["CRXApprovalDisapprovalWeight"]);
                    crxResponse.CRXNotes = Convert.ToString(response.Rows[0]["CRXNotes"]);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get CRX Response", ex);
            }
            return crxResponse;
        }

        public List<QueueItemText> VerifyCRXResponse(int? communityId, int? languageId, int? textType, int? numberOfRows)
        {
            var memberAttributes = new List<QueueItemText>();
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_VerifyCRXResponse", 0);
                if (communityId.HasValue)
                {
                    command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId.Value);
                }
                if (languageId.HasValue)
                {
                    command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageId.Value);
                }
                if (textType.HasValue)
                {
                    command.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, textType.Value);
                }
                if (numberOfRows.HasValue)
                {
                    command.AddParameter("@Rows", SqlDbType.Int, ParameterDirection.Input, numberOfRows.Value);
                }
                DataTable texts = Client.Instance.ExecuteDataTable(command);
                if ((texts != null) && (texts.Rows.Count > 0))
                {
                    foreach (DataRow row in texts.Rows)
                    {
                        var queueItemText = new QueueItemText();
                        queueItemText.MemberID = Convert.ToInt32(row["MemberID"]);
                        queueItemText.MemberTextApprovalID = Convert.ToInt32(row["MemberTextApprovalID"]);
                        queueItemText.CommunityID = Convert.ToInt32(row["CommunityID"]);
                        queueItemText.LanguageID = Convert.ToInt32(row["LanguageID"]);
                        queueItemText.MemberAttributeGroupID = Convert.ToInt32(row["MemberAttributeGroupID"]);
                        queueItemText.CRXApproval = (row["CRXApproval"] != DBNull.Value) && Convert.ToBoolean(row["CRXApproval"]);
                        queueItemText.CRXApprovalDisapprovalWeight = (row["CRXApprovalDisapprovalWeight"] != DBNull.Value) ? (float)Convert.ToDouble(row["CRXApprovalDisapprovalWeight"]) : 0;
                        queueItemText.CRXNotes = (row["CRXNotes"] != DBNull.Value) ? Convert.ToString(row["CRXNotes"]) : "";
                        queueItemText.TextContent = (row["TextContent"] != DBNull.Value) ? Convert.ToString(row["TextContent"]) : "";
                        queueItemText.BrandID = Convert.ToInt32(row["BrandID"]);
                        queueItemText.SiteID = Convert.ToInt32(row["SiteID"]);
                        queueItemText.InsertDate = Convert.ToDateTime(row["InsertDate"]);
                        queueItemText.IPAddress = (row["IP"] != DBNull.Value) ? Convert.ToString(row["IP"]) : "";
                        queueItemText.ProfileRegionId = (row["ProfileRegionId"] != DBNull.Value) ? Convert.ToInt32(row["ProfileRegionId"]) : 0;
                        queueItemText.GenderMask = (row["GenderMask"] != DBNull.Value) ? Convert.ToInt32(row["GenderMask"]) : 0;
                        queueItemText.MaritalStatus = (row["MaritalStatus"] != DBNull.Value) ? Convert.ToInt32(row["MaritalStatus"]) : 0;
                        queueItemText.FirstName = (row["FirstName"] != DBNull.Value) ? Convert.ToString(row["FirstName"]) : "";
                        queueItemText.LastName = (row["LastName"] != DBNull.Value) ? Convert.ToString(row["LastName"]) : "";
                        queueItemText.Email = (row["Email"] != DBNull.Value) ? Convert.ToString(row["Email"]) : "";
                        queueItemText.OccupationDescription = (row["OccupationDescription"] != DBNull.Value) ? Convert.ToString(row["OccupationDescription"]) : "";
                        queueItemText.EducationLevel = (row["EducationLevel"] != DBNull.Value) ? Convert.ToInt32(row["EducationLevel"]) : 0;
                        queueItemText.Religion = (row["Religion"] != DBNull.Value) ? Convert.ToInt32(row["Religion"]) : 0;
                        queueItemText.Ethnicity = (row["Ethnicity"] != DBNull.Value) ? Convert.ToInt32(row["Ethnicity"]) : 0;
                        queueItemText.DaysSinceFirstSubscription = (row["DaysSinceFirstSubscription"] != DBNull.Value) ? Convert.ToInt32(row["DaysSinceFirstSubscription"]) : 0;
                        queueItemText.BillingPhoneNumber = (row["BillingPhoneNumber"] != DBNull.Value) ? Convert.ToString(row["BillingPhoneNumber"]) : "";
                        queueItemText.SubscriptionStatus = (row["SubscriptionStatus"] != DBNull.Value) ? Convert.ToString(row["SubscriptionStatus"]) : "";
                        queueItemText.RegistrationDate = (row["RegistrationDate"] != DBNull.Value) ? Convert.ToDateTime(row["RegistrationDate"]) : DateTime.MinValue;
                        queueItemText.CRXResponseID = (row["CRXResponseID"] != DBNull.Value) ? Convert.ToInt32(row["CRXResponseID"]) : 0;
                        memberAttributes.Add(queueItemText);
                    }
                }
                return memberAttributes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get member text approval attributes", ex);
            }
        }

        public List<QueueItemText> GetApprovalTextAttributesCRXResponse(int memberTextApprovalId)
        {
            var memberAttributes = new List<QueueItemText>();
            try
            {
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetApprovalTextAttributesCRXResponse", 0);
                command.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalId);
                DataTable texts = Client.Instance.ExecuteDataTable(command);
                if ((texts != null) && (texts.Rows.Count > 0))
                {
                    foreach (DataRow row in texts.Rows)
                    {
                        memberAttributes.Add(new QueueItemText { CRXResponseID = Convert.ToInt32(row["CRXResponseID"]),
                                                                 CRXApproval   = Convert.ToBoolean(row["CRXApproval"]),
                                                                 MemberAttributeGroupID = Convert.ToInt32(row["MemberAttributeGroupID"])
                        });
                    }
                }
                return memberAttributes;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get member text approval attributes and CRX Response", ex);
            }
        }

        public void PopulateOnDemandTextReview(DateTime startDate, DateTime endDate, int? communityId, int supervisorId, List<AdminReviewSetting> adminRates)
        {
            try
            {
                foreach (var adminRate in adminRates)
                {
                    PopulateOnDemandTextReviewByAdmin(startDate, endDate, communityId, supervisorId, adminRate);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to populate OnDemand Admin Text Apporval Review.", ex);
            } 
        }

        public List<TextApprovalReviewCount> GetOnDemandReviewQueueCounts()
        {
            try
            {
                var onDemandReviewCounts = new List<TextApprovalReviewCount>();
                var command = new Command("mnMemberApprovalWrite", "dbo.up_GetOnDemandReviewQueueCounts", 0);
                DataTable dt = Client.Instance.ExecuteDataTable(command);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                            onDemandReviewCounts.Add(new TextApprovalReviewCount { 
                            AdminMemberId = Convert.ToInt32(row["AdminMemberID"]),
                            SupervisorId = Convert.ToInt32(row["SupervisorID"]),
                            TotalItems = Convert.ToInt32(row["TotalItems"]),
                            DateQueueAdded = (row["DateQueueAdded"] != DBNull.Value) ? Convert.ToDateTime(row["DateQueueAdded"]) : DateTime.MinValue,
                            OldRecordDate = (row["OldestItem"] != DBNull.Value) ? Convert.ToDateTime(row["OldestItem"]) : DateTime.MinValue
                    });
                    }
                }
                return onDemandReviewCounts;
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to get admin OnDemand FTA review counts.", ex);
            }
        }

        public void PurgeOnDemandReviewQueue(int adminMemberId, int supervisorId)
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_PurgeOnDemandReviewQueue", 0);
                command.AddParameter("@SupervisorID", SqlDbType.Int, ParameterDirection.Input, supervisorId);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to purge FTA on demand review queue.", ex);
            }
        }

        public void SaveMemberProfileDuplicateHistory(MemberProfileDuplicateHistory history, int memberProfileDuplicateQueueId)
        {
            try
            {
                // write the history entry
                var historyKey = KeySA.Instance.GetKey("MemberProfileDuplicateHistoryID");
                var parentCmd = new Command("mnMemberApproval", "dbo.up_AddMemberProfileDuplicateHistory", 0);
                parentCmd.AddParameter("@MemberProfileDuplicateHistoryID", SqlDbType.Int, ParameterDirection.Input,
                                       historyKey);
                parentCmd.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, history.LanguageId);
                parentCmd.AddParameter("@NumberOfDuplicates", SqlDbType.Int, ParameterDirection.Input,
                                       history.MemberProfileDuplicateHistoryEntries.Count);
                parentCmd.AddParameter("@QueueInsertDate", SqlDbType.DateTime, ParameterDirection.Input,
                                       history.QueueInsertDate);
                parentCmd.AddParameter("@AdminID", SqlDbType.Int, ParameterDirection.Input, history.AdminId);
                Client.Instance.ExecuteAsyncWrite(parentCmd);

                var ordinal = 1;
                history.MemberProfileDuplicateHistoryEntries.Sort();
                foreach (MemberProfileDuplicateHistoryEntry entry in history.MemberProfileDuplicateHistoryEntries)
                {
                    var childCmd = new Command("mnMemberApproval", "dbo.up_AddMemberProfileDuplicateHistoryEntry", 0);
                    childCmd.AddParameter("@MemberProfileDuplicateHistoryID", SqlDbType.Int, ParameterDirection.Input,
                                          historyKey);
                    childCmd.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, entry.MemberId);
                    childCmd.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, entry.CRXResponseId);
                    childCmd.AddParameter("@FieldTypeID", SqlDbType.Int, ParameterDirection.Input, (int) entry.FieldType);
                    childCmd.AddParameter("@FieldValueOriginationDate", SqlDbType.DateTime, ParameterDirection.Input,
                                          entry.FieldValueOriginationDate);
                    childCmd.AddParameter("@IsMatch", SqlDbType.Bit, ParameterDirection.Input, entry.IsMatch);
                    childCmd.AddParameter("@Suspended", SqlDbType.Bit, ParameterDirection.Input, entry.Suspended);
                    childCmd.AddParameter("@AlreadySuspended", SqlDbType.Bit, ParameterDirection.Input,
                                          entry.AlreadySuspended);
                    childCmd.AddParameter("@Ordinal", SqlDbType.Int, ParameterDirection.Input, ordinal++);
                    childCmd.AddParameter("@FieldValue", SqlDbType.NVarChar, ParameterDirection.Input, entry.TextContent);
                    Client.Instance.ExecuteAsyncWrite(childCmd);
                }

                // delete the MemberProfileDuplicateQueue entry
                var deleteEntry = true;

#if DEBUG
                deleteEntry = false;
#endif

                if (deleteEntry)
                {
                    var delCmd = new Command("mnMemberApproval", "dbo.up_DeleteMemberProfileDuplicateQueueItem", 0);
                    delCmd.AddParameter("@MemberProfileDuplicateQueueID", SqlDbType.Int, ParameterDirection.Input,
                                        memberProfileDuplicateQueueId);
                    Client.Instance.ExecuteAsyncWrite(delCmd);
                }

            }
            catch (Exception ex)
            {
                throw new BLException("Unable to save MemberProfileDuplicateHistory", ex);
            }

        }

        /// <summary>
        /// When the queue item is deemed bad, call this method to remove it from the queue. Only use this method
        /// if the CRXResponseID is missing.
        /// </summary>
        /// <param name="MemberProfileDuplicateQueueID"></param>
        public void DeleteMemberProfileDuplicateQueueItem(int MemberProfileDuplicateQueueID)
        {
            try
            {
                DeleteMemberProfileDuplicateQueueItem(MemberProfileDuplicateQueueID, true);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to delete MemberProfileDuplicateQueueItem", ex);
            }
        }

        private void DeleteMemberProfileDuplicateQueueItem(int MemberProfileDuplicateQueueID,
                                                           bool IsMissingCRXResponseID)
        {
            var delCmd = new Command("mnMemberApproval", "dbo.up_DeleteMemberProfileDuplicateQueueItem", 0);
            delCmd.AddParameter("@MemberProfileDuplicateQueueID", SqlDbType.Int, ParameterDirection.Input,
                                MemberProfileDuplicateQueueID);
            delCmd.AddParameter("@IsMissingCRXResponseID", SqlDbType.Bit, ParameterDirection.Input,
                                IsMissingCRXResponseID);

            Client.Instance.ExecuteAsyncWrite(delCmd);
        }

        public void SaveMemberProfileDuplicateReview(MemberProfileDuplicateReview duplicateReview)
        {
            try
            {
                var parentCmd = new Command("mnMemberApproval", "dbo.up_AddMemberProfileDuplicateReview", 0);
                parentCmd.AddParameter("@MemberProfileDuplicateHistoryID", SqlDbType.Int, ParameterDirection.Input,
                                       duplicateReview.MemberProfileDuplicateHistoryID);
                parentCmd.AddParameter("@SupervisorID", SqlDbType.Int, ParameterDirection.Input,
                                       duplicateReview.SupervisorID);
                parentCmd.AddParameter("@AdminID", SqlDbType.Int, ParameterDirection.Input, duplicateReview.AdminID);
                parentCmd.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input,
                                       duplicateReview.LanguageID);
                Client.Instance.ExecuteAsyncWrite(parentCmd);

                var ordinal = 1;
                duplicateReview.Entries.Sort();
                foreach (MemberProfileDuplicateReviewEntry entry in duplicateReview.Entries)
                {
                    var childCmd = new Command("mnMemberApproval", "dbo.up_AddMemberProfileDuplicateReviewEntry", 0);
                    childCmd.AddParameter("@MemberProfileDuplicateHistoryID", SqlDbType.Int, ParameterDirection.Input,
                                          duplicateReview.MemberProfileDuplicateHistoryID);
                    childCmd.AddParameter("@Ordinal", SqlDbType.Int, ParameterDirection.Input, ordinal++);
                    childCmd.AddParameter("@IsMatch", SqlDbType.Bit, ParameterDirection.Input, entry.IsMatch);
                    childCmd.AddParameter("@Suspended", SqlDbType.Bit, ParameterDirection.Input, entry.Suspended);
                    Client.Instance.ExecuteAsyncWrite(childCmd);
                }

                // delete the MemberProfileDuplicateReviewQueue item
                var delCmd = new Command("mnMemberApproval", "dbo.up_DeleteMemberProfileDuplicateReviewQueueItem", 0);
                delCmd.AddParameter("@MemberProfileDuplicateHistoryID", SqlDbType.Int, ParameterDirection.Input,
                                    duplicateReview.MemberProfileDuplicateHistoryID);
                Client.Instance.ExecuteAsyncWrite(delCmd);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to save MemberProfileDuplicateReview", ex);
            }

        }

        private void PopulateOnDemandTextReviewByAdmin(DateTime startDate, DateTime endDate, int? communityId, int supervisorId, AdminReviewSetting adminRate )
        {
            var command = new Command("mnMemberApprovalWrite", "dbo.up_GetOnDemandAdminTextApprovalReview", 0);
            command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
            command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
            if (communityId.HasValue)
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId.Value);
            command.AddParameter("@AdminMemberId", SqlDbType.Int, ParameterDirection.Input, adminRate.AdminMemberId);
            command.AddParameter("@AdminRate", SqlDbType.Int, ParameterDirection.Input, adminRate.OnDemandSampleRate);
            DataTable response = Client.Instance.ExecuteDataTable(command);
            if ((response != null) && (response.Rows.Count > 0))
            {
                foreach (DataRow row in response.Rows)
                {
                    int adminMemberId = Convert.ToInt32(row["AdminMemberID"]);
                    int memberTextApprovalId = Convert.ToInt32(row["MemberTextApprovalID"]);
                    DateTime insertDate = Convert.ToDateTime(row["InsertDate"]);
                    DateTime updateDate = Convert.ToDateTime(row["UpdateDate"]);
                    int getcommuntiyId = Convert.ToInt32(row["CommunityID"]);

                    var saveCommand = new Command("mnMemberApproval", "dbo.up_PopulateOnDemandAdminTextApprovalReview", 0);
                    saveCommand.AddParameter("@AdminMemberId", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
                    saveCommand.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalId);
                    saveCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, getcommuntiyId);
                    saveCommand.AddParameter("@SupervisorId", SqlDbType.Int, ParameterDirection.Input, supervisorId);
                    saveCommand.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, insertDate);
                    saveCommand.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
                    Client.Instance.ExecuteAsyncWrite(saveCommand);
                }
            }
            
        }

        private void SaveMemberTextApprovalAttribute(QueueItemText queueItemText)
        {
            int CRXResponseID = KeySA.Instance.GetKey("CRXResponseID");
            int memberTextApprovalAttributeID = KeySA.Instance.GetKey("MemberTextApprovalAttributeID");
            var saveCommand = new Command("mnMemberApproval", "dbo.up_AddMemberTextApprovalAttribute", queueItemText.MemberID);
            saveCommand.AddParameter("@MemberTextApprovalAttributeID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalAttributeID);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItemText.MemberID);
            saveCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItemText.CommunityID);
            saveCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, queueItemText.LanguageID);
            saveCommand.AddParameter("@MemberAttributeGroupID", SqlDbType.Int, ParameterDirection.Input, queueItemText.MemberAttributeGroupID);
            saveCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)queueItemText.TextType);
            saveCommand.AddParameter("@TextContent", SqlDbType.NVarChar, ParameterDirection.Input, queueItemText.TextContent);
            saveCommand.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, CRXResponseID);
            saveCommand.AddParameter("@CRXResponseResetTime", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            //Commenting the code until Provo is ready to use API to add Mingle items to FTA queue
            //if (queueItemText.IsExternal) //For non-BH members
            //{
            //    saveCommand.AddParameter("@IsExternal", SqlDbType.Bit, ParameterDirection.Input,queueItemText.IsExternal);
            //    saveCommand.AddParameter("@MingleColumnName", SqlDbType.NVarChar, ParameterDirection.Input, queueItemText.MingleTableColumnName);
            //    saveCommand.AddParameter("@OldTextContent", SqlDbType.NVarChar, ParameterDirection.Input, queueItemText.OldTextContent);
            //}
            Client.Instance.ExecuteAsyncWrite(saveCommand);
        }

        private void SaveQueueItemPhoto(QueueItemPhoto queueItemPhoto)
        {
            Command saveCommand = new Command("mnMemberApproval", "dbo.up_AddPhotoApproval", queueItemPhoto.MemberID);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItemPhoto.MemberID);
            saveCommand.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, queueItemPhoto.BrandID);
            saveCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, queueItemPhoto.SiteID);
            saveCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItemPhoto.CommunityID);
            saveCommand.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, queueItemPhoto.MemberPhotoID);
            Client.Instance.ExecuteAsyncWrite(saveCommand);
        }

        private void SaveQueueItemText(QueueItemText queueItemText)
        {
            int memberTextApprovalID = KeySA.Instance.GetKey("MemberTextApprovalID");
            Command saveCommand = new Command("mnMemberApproval", "dbo.up_AddTextApproval", queueItemText.MemberID);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItemText.MemberID);
            saveCommand.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalID);
            saveCommand.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, queueItemText.BrandID);
            saveCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, queueItemText.SiteID);
            saveCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItemText.CommunityID);
            saveCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, queueItemText.LanguageID);
            saveCommand.AddParameter("@TextTypeID", SqlDbType.Int, ParameterDirection.Input, (int)queueItemText.TextType);

            //Populate Member Info based on CRX_POPULATE_MEMBER_INFO setting
            bool populateCrxMemberAttr = false;
            bool.TryParse(SettingsService.GetSettingFromSingleton(CRXPopulateMemberInfo), out populateCrxMemberAttr);
            if (populateCrxMemberAttr && queueItemText.PopulateMemberInfo)
            {
                int individualTextAttributeApprovalID = KeySA.Instance.GetKey("IndividualTextAttributeApprovalID");
                saveCommand.AddParameter("@IndividualTextAttributeApprovalID", SqlDbType.Int, ParameterDirection.Input,
                                          individualTextAttributeApprovalID);
                saveCommand.AddParameter("@IP", SqlDbType.NVarChar, ParameterDirection.Input,
                                          queueItemText.IPAddress == string.Empty ? null : queueItemText.IPAddress);
                if (queueItemText.ProfileRegionId != Constants.NULL_INT)
                    saveCommand.AddParameter("@ProfileRegionID", SqlDbType.Int, ParameterDirection.Input,
                                              queueItemText.ProfileRegionId);
                if (queueItemText.GenderMask != Constants.NULL_INT)
                    saveCommand.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, queueItemText.GenderMask);
                if (queueItemText.MaritalStatus != Constants.NULL_INT)
                    saveCommand.AddParameter("@MaritalStatus", SqlDbType.Int, ParameterDirection.Input, queueItemText.MaritalStatus);
                saveCommand.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input,
                                          queueItemText.FirstName == string.Empty ? null : queueItemText.FirstName);
                saveCommand.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input,
                                          queueItemText.LastName == string.Empty ? null : queueItemText.LastName);
                saveCommand.AddParameter("@Email", SqlDbType.NVarChar, ParameterDirection.Input,
                                          queueItemText.Email == string.Empty ? null : queueItemText.Email);
                saveCommand.AddParameter("@OccupationDescription", SqlDbType.NVarChar, ParameterDirection.Input,
                                          queueItemText.OccupationDescription == string.Empty ? null : queueItemText.OccupationDescription);
                if (queueItemText.EducationLevel != Constants.NULL_INT)
                    saveCommand.AddParameter("@EducationLevel", SqlDbType.Int, ParameterDirection.Input, queueItemText.EducationLevel);
                if (queueItemText.Religion != Constants.NULL_INT)
                    saveCommand.AddParameter("@Religion", SqlDbType.Int, ParameterDirection.Input, queueItemText.Religion);
                if (queueItemText.Ethnicity != Constants.NULL_INT)
                    saveCommand.AddParameter("@Ethnicity", SqlDbType.Int, ParameterDirection.Input, queueItemText.Ethnicity);
                if (queueItemText.BillingPhoneNumber != string.Empty)
                    saveCommand.AddParameter("@BillingPhoneNumber", SqlDbType.VarChar, ParameterDirection.Input,
                                              queueItemText.BillingPhoneNumber);
                if (queueItemText.SubscriptionStatus != string.Empty)
                    saveCommand.AddParameter("@SubscriptionStatus", SqlDbType.VarChar, ParameterDirection.Input,
                                              queueItemText.SubscriptionStatus);
                if (queueItemText.RegistrationDate != DateTime.MinValue)
                    saveCommand.AddParameter("@RegistrationDate", SqlDbType.DateTime, ParameterDirection.Input,
                                              queueItemText.RegistrationDate);
            }
            Client.Instance.ExecuteAsyncWrite(saveCommand);
        }

        private void SaveQueueItemMember(QueueItemMember queueItemMember)
        {
            int fraudReviewId = KeySA.Instance.GetKey("FraudReviewID");
            Command saveCommand = new Command("mnMemberApproval", "dbo.up_AddMemberFraudReview", queueItemMember.MemberID);
            saveCommand.AddParameter("@FraudReviewID", SqlDbType.Int, ParameterDirection.Input, fraudReviewId);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItemMember.MemberID);
            saveCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, queueItemMember.SiteID);
            saveCommand.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, queueItemMember.AdminMemberId);
            Client.Instance.ExecuteAsyncWrite(saveCommand);
        }

        private void SaveQueueItemFacebookLike(QueueItemFacebookLike queueItemFacebookLike)
        {
            Command saveCommand = new Command("mnMemberApproval", "dbo.up_AddFacebookLikeApproval", queueItemFacebookLike.MemberID);
            saveCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItemFacebookLike.MemberID);
            saveCommand.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, queueItemFacebookLike.BrandID);
            saveCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, queueItemFacebookLike.SiteID);
            saveCommand.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItemFacebookLike.CommunityID);
            Client.Instance.ExecuteAsyncWrite(saveCommand);
        }

        public void SaveMemberProfileDuplicate(QueueItemMemberProfileDuplicate queueItem)
        {
            if(queueItem == null || queueItem.MemberProfileDuplicates == null)
            {
                throw new BLException("Queue item is not valid");
            }

            using(TransactionScope scope = new TransactionScope())
            {
                var crxResponseID = (from MemberProfileDuplicate d in queueItem.MemberProfileDuplicates
                                     where d.OrdinalNumber == 1
                                     select d.CRXResponseId).DefaultIfEmpty(Constants.NULL_INT).First();

                var duplicateQueueId = KeySA.Instance.GetKey("MemberProfileDuplicateQueueID");
                var parentCommand = new Command("mnMemberApproval", "dbo.up_AddMemberProfileDuplicateQueue", 0);
                parentCommand.AddParameter("@MemberProfileDuplicateQueueID", SqlDbType.Int, ParameterDirection.Input, duplicateQueueId);
                parentCommand.AddParameter("@ApprovalStatusID", SqlDbType.Int, ParameterDirection.Input, (int)ApprovalStatus.Inserted);
                parentCommand.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, queueItem.LanguageID);
                parentCommand.AddParameter("@NumberOfDuplicates", SqlDbType.Int, ParameterDirection.Input, queueItem.MemberProfileDuplicates.Count);
                parentCommand.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, crxResponseID);

                var sw = new SyncWriter();
                Exception swEx = null;
                sw.Execute(parentCommand, out swEx);
                if (swEx != null)
                {
                    throw swEx;
                }

                var ordinal = 1;
                queueItem.MemberProfileDuplicates.Sort();
                foreach(MemberProfileDuplicate dup in queueItem.MemberProfileDuplicates)
                {
                    var duplicateId = KeySA.Instance.GetKey("MemberProfileDuplicateID");
                    var childCmd = new Command("mnMemberApproval", "dbo.up_AddMemberProfileDuplicate", 0);
                    childCmd.AddParameter("@MemberProfileDuplicateID", SqlDbType.Int, ParameterDirection.Input, duplicateId);
                    childCmd.AddParameter("@MemberProfileDuplicateQueueID", SqlDbType.Int, ParameterDirection.Input, duplicateQueueId);
                    childCmd.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, dup.MemberId);
                    childCmd.AddParameter("@CRXResponseID", SqlDbType.Int, ParameterDirection.Input, dup.CRXResponseId);
                    childCmd.AddParameter("@FieldTypeID", SqlDbType.Int, ParameterDirection.Input, (int)dup.FieldType);
                    childCmd.AddParameter("@FieldValueOriginationDate", SqlDbType.DateTime, ParameterDirection.Input, dup.FieldValueOriginationDate);
                    childCmd.AddParameter("@FieldValue", SqlDbType.NVarChar,ParameterDirection.Input, dup.FieldValue);
                    childCmd.AddParameter("@DuplicateStatusID", SqlDbType.Int, ParameterDirection.Input, (int)DuplicateStatus.Undetermined);
                    childCmd.AddParameter("@Ordinal", SqlDbType.Int, ParameterDirection.Input, ordinal++);

                    sw.Execute(childCmd, out swEx);
                    if (swEx != null)
                    {
                        throw swEx;
                    }
                }

                scope.Complete();
            }
        }

        private QueueCountRecord DataRowtoQueueCountRecord(DataRow dr, bool includeChildCount)
        {
            QueueCountRecord record = new QueueCountRecord();
            record.ItemCount = Convert.ToInt32(dr["ItemCount"]);
            record.GroupID = Convert.ToInt32(dr["GroupID"]);
            record.GroupType = (QueueCountGroupType)Convert.ToInt32(dr["GroupType"]);
            record.MaxHours = Convert.ToInt32(dr["MaxHours"]);
            record.AverageHours = Convert.ToInt32(dr["AverageHours"]);

            if(includeChildCount)
            {
                record.ChildCount = Convert.ToInt32(dr["ChildCount"]);
            }

            return record;
        }

        private void CompleteFraudReview(QueueItemMember queueItemMember)
        {
            var command = new Command("mnMemberApproval", "dbo.up_CompleteMemberFraudReview", queueItemMember.MemberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItemMember.MemberID);
            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, queueItemMember.SiteID);
            command.AddParameter("@ReviewerID", SqlDbType.Int, ParameterDirection.Input, queueItemMember.ReviewerID);
            command.AddParameter("@IsFraud", SqlDbType.Bit, ParameterDirection.Input, queueItemMember.IsFraud);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private IndividualTextApprovalStatus DataRowToIndividualTextApprovalStatus(DataRow dr)
        {
            IndividualTextApprovalStatus status = new IndividualTextApprovalStatus(
                    Convert.ToInt32(dr["StatusMask"]),
                    dr["Description"].ToString(),
                    Convert.ToBoolean(dr["Display"]));
            return status;
        }

        private QueueItemPhoto DataRowToQueueItemPhoto(DataRow dr)
        {
            QueueItemPhoto queueItemPhoto = new QueueItemPhoto();

            queueItemPhoto.MemberID = Convert.ToInt32(dr["MemberID"]);
            queueItemPhoto.BrandID = Convert.ToInt32(dr["BrandID"]);
            queueItemPhoto.SiteID = Convert.ToInt32(dr["SiteID"]);
            queueItemPhoto.CommunityID = Convert.ToInt32(dr["CommunityID"]);
            queueItemPhoto.ApprovalStatus = (ApprovalStatus)Convert.ToInt32(dr["ApprovalStatusID"]);
            queueItemPhoto.InsertDate = Convert.ToDateTime(dr["InsertDate"]);
            queueItemPhoto.StatusDate = Convert.ToDateTime(dr["StatusDate"]);
            queueItemPhoto.MemberPhotoID = Convert.ToInt32(dr["MemberPhotoID"]);
            return queueItemPhoto;
        }

        private List<QueueItemText> DataTableToMemberTextApprovalAttributes(DataTable dt)
        {
            var approvalAttributes = new List<QueueItemText>();
            foreach(DataRow row in dt.Rows)
            {
                approvalAttributes.Add(new QueueItemText()
                                        {
                                            MemberTextApprovalAttributeID =
                                                Convert.ToInt32(row["MemberTextApprovalAttributeID"]),
                                            MemberTextApprovalID = Convert.ToInt32(row["MemberTextApprovalID"]),
                                            MemberAttributeGroupID = Convert.ToInt32(row["MemberAttributeGroupID"])
                                        });
            }
            return approvalAttributes;
        }

        private QueueItemText DataRowToItemText(DataRow dr)
        {
            QueueItemText queueItemText = new QueueItemText();

            queueItemText.MemberID = Convert.ToInt32(dr["MemberID"]);
            queueItemText.BrandID = Convert.ToInt32(dr["BrandID"]);
            queueItemText.SiteID = Convert.ToInt32(dr["SiteID"]);
            queueItemText.CommunityID = Convert.ToInt32(dr["CommunityID"]);
            queueItemText.ApprovalStatus = (ApprovalStatus)Convert.ToInt32(dr["ApprovalStatusID"]);
            queueItemText.InsertDate = Convert.ToDateTime(dr["InsertDate"]);
            queueItemText.StatusDate = Convert.ToDateTime(dr["StatusDate"]);
            queueItemText.LanguageID = Convert.ToInt32(dr["LanguageID"]);
            queueItemText.TextType  = (TextType)Convert.ToInt32(dr["TextTypeID"]);
            queueItemText.MemberTextApprovalID = Convert.ToInt32(dr["MemberTextApprovalID"]);

            return queueItemText;
        }

        private List<QueueItemText> DataRowToCRXTextItem(DataTable dt)
        {
            var texts = new List<QueueItemText>();
            foreach (DataRow row in dt.Rows)
            {
                int crxResponseId = (row["CRXResponseID"] != DBNull.Value) ? Convert.ToInt32(row["CRXResponseID"]) : 0;
                QueueItemText queueItemText = texts.FirstOrDefault(text => text.CRXResponseID == crxResponseId);
                if (queueItemText != null)
                {
                    if ((row["CRXResponseResetTime"] != DBNull.Value))
                        queueItemText.CRXResetDates.Add(Convert.ToDateTime(row["CRXResponseResetTime"]));
                }
                else
                {
                    queueItemText = new QueueItemText();
                    queueItemText.MemberTextApprovalID = Convert.ToInt32(row["MemberTextApprovalID"]);
                    queueItemText.MemberAttributeGroupID = Convert.ToInt32(row["MemberAttributeGroupID"]);
                    queueItemText.MemberID = Convert.ToInt32(row["MemberID"]);
                    queueItemText.BrandID = Convert.ToInt32(row["BrandID"]);
                    queueItemText.SiteID = Convert.ToInt32(row["SiteID"]);
                    queueItemText.CommunityID = Convert.ToInt32(row["CommunityID"]);
                    queueItemText.LanguageID = Convert.ToInt32(row["LanguageID"]);
                    queueItemText.InsertDate = Convert.ToDateTime(row["InsertDate"]);
                    queueItemText.IPAddress = (row["IP"] != DBNull.Value) ? Convert.ToString(row["IP"]) : "";
                    queueItemText.ProfileRegionId = (row["ProfileRegionId"] != DBNull.Value)
                                                        ? Convert.ToInt32(row["ProfileRegionId"])
                                                        : 0;
                    queueItemText.GenderMask = (row["GenderMask"] != DBNull.Value) ? Convert.ToInt32(row["GenderMask"]) : 0;
                    queueItemText.MaritalStatus = (row["MaritalStatus"] != DBNull.Value)
                                                      ? Convert.ToInt32(row["MaritalStatus"])
                                                      : 0;
                    queueItemText.FirstName = (row["FirstName"] != DBNull.Value) ? Convert.ToString(row["FirstName"]) : "";
                    queueItemText.LastName = (row["LastName"] != DBNull.Value) ? Convert.ToString(row["LastName"]) : "";
                    queueItemText.Email = (row["Email"] != DBNull.Value) ? Convert.ToString(row["Email"]) : "";
                    queueItemText.OccupationDescription = (row["OccupationDescription"] != DBNull.Value)
                                                              ? Convert.ToString(row["OccupationDescription"])
                                                              : "";
                    queueItemText.EducationLevel = (row["EducationLevel"] != DBNull.Value)
                                                       ? Convert.ToInt32(row["EducationLevel"])
                                                       : 0;
                    queueItemText.Religion = (row["Religion"] != DBNull.Value) ? Convert.ToInt32(row["Religion"]) : 0;
                    queueItemText.Ethnicity = (row["Ethnicity"] != DBNull.Value) ? Convert.ToInt32(row["Ethnicity"]) : 0;
                    queueItemText.DaysSinceFirstSubscription = (row["DaysSinceFirstSubscription"] != DBNull.Value)
                                                                   ? Convert.ToInt32(row["DaysSinceFirstSubscription"])
                                                                   : 0;
                    queueItemText.BillingPhoneNumber = (row["BillingPhoneNumber"] != DBNull.Value)
                                                           ? Convert.ToString(row["BillingPhoneNumber"])
                                                           : "";
                    queueItemText.SubscriptionStatus = (row["SubscriptionStatus"] != DBNull.Value)
                                                           ? Convert.ToString(row["SubscriptionStatus"])
                                                           : "";
                    queueItemText.RegistrationDate = (row["RegistrationDate"] != DBNull.Value)
                                                         ? Convert.ToDateTime(row["RegistrationDate"])
                                                         : DateTime.MinValue;
                    queueItemText.TextContent = (row["TextContent"] != DBNull.Value)
                                                    ? Convert.ToString(row["TextContent"])
                                                    : "";
                    queueItemText.CRXResponseID = crxResponseId;
                    if ((row["CRXResponseResetTime"] != DBNull.Value))
                        queueItemText.CRXResetDates.Add(Convert.ToDateTime(row["CRXResponseResetTime"]));
                    texts.Add(queueItemText);
                }
            }
            return texts;
        }

        private void SaveSupervisorReview(int memberTextApprovalId, int attributeGroupId, int? supervisorReviewStatusMask, int? reviewerStatusMask, 
            string reviewerTextContent, int? reviewStatus, bool? crxTie, bool? toolCorrectionRequested)
        {
            try
            {
                var command = new Command("mnMemberApproval", "dbo.up_SaveSupervisorReview", 0);
                command.AddParameter("@MemberTextApprovalID", SqlDbType.Int, ParameterDirection.Input, memberTextApprovalId);
                command.AddParameter("@MemberAttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupId);
                if (supervisorReviewStatusMask.HasValue)
                    command.AddParameter("@SupervisorReviewStatusMask", SqlDbType.Int, ParameterDirection.Input, supervisorReviewStatusMask.Value);
                if (reviewerStatusMask.HasValue)
                    command.AddParameter("@ReviewerStatusMask", SqlDbType.Int, ParameterDirection.Input, reviewerStatusMask.Value);
                if (!string.IsNullOrEmpty(reviewerTextContent))
                    command.AddParameter("@ReviewerTextContent", SqlDbType.NVarChar, ParameterDirection.Input, reviewerTextContent);
                if (reviewStatus.HasValue)
                    command.AddParameter("@ReviewStatus", SqlDbType.Int, ParameterDirection.Input, reviewStatus.Value);
                if (crxTie.HasValue)
                    command.AddParameter("@CRXTie", SqlDbType.Int, ParameterDirection.Input, crxTie.Value);
                if (toolCorrectionRequested.HasValue)
                    command.AddParameter("@ToolCorrectionRequested", SqlDbType.Int, ParameterDirection.Input, toolCorrectionRequested.Value);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to complete admin FTA review.", ex);
            }
        }

        private ReviewText ConvertToReviewText(DataSet ds)
        {
            var reviewText = new ReviewText();
            var dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                reviewText.MemberTextApprovalId = Convert.ToInt32(dt.Rows[0]["MemberTextApprovalID"]);
                reviewText.MemberId = Convert.ToInt32(dt.Rows[0]["MemberID"]);
                reviewText.CommunityId = Convert.ToInt32(dt.Rows[0]["CommunityID"]);
                reviewText.LanguageId = Convert.ToInt32(dt.Rows[0]["LanguageID"]);
                reviewText.TextTypeId = Convert.ToInt32(dt.Rows[0]["TextTypeID"]);
                reviewText.AdminMemberId = Convert.ToInt32(dt.Rows[0]["AdminMemberID"]);
                reviewText.AdminActionMask = Convert.ToInt32(dt.Rows[0]["AdminActionMask"]);
                reviewText.QueueInsertDate = Convert.ToDateTime(dt.Rows[0]["InsertDate"]);
                reviewText.StatusDate = Convert.ToDateTime(dt.Rows[0]["StatusDate"]);
                if (dt.Columns.Contains("IsOnDemand"))
                    reviewText.IsOnDemand = Convert.ToBoolean(dt.Rows[0]["IsOnDemand"]);
                if (ds.Tables.Count > 1 && ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    dt = ds.Tables[1];
                    foreach (DataRow row in dt.Rows)
                    {
                        var reviewTextAttribute = new ReviewTextAttribute();
                        reviewTextAttribute.AdminMemberId = Convert.ToInt32(row["AdminMemberID"]);
                        reviewTextAttribute.AttributeGroupId = Convert.ToInt32(row["MemberAttributeGroupID"]);
                        reviewTextAttribute.StatusMask = Convert.ToInt32(row["StatusMask"]);
                        reviewTextAttribute.TextContent = row["TextContent"].ToString();
                        reviewTextAttribute.AdminTextContent = row["AdminTextContent"].ToString();
                        if (dt.Columns.Contains("CRXApproval") && row["CRXApproval"] != DBNull.Value)
                            reviewTextAttribute.CRXApproval =  Convert.ToBoolean(row["CRXApproval"]);
                        if (dt.Columns.Contains("CRXNotes") && row["CRXNotes"] != DBNull.Value)
                            reviewTextAttribute.CRXNotes = row["CRXNotes"].ToString();
                        if (dt.Columns.Contains("CRXResponseID") && row["CRXResponseID"] != DBNull.Value)
                            reviewTextAttribute.CRXResponseId = Convert.ToInt32(row["CRXResponseID"]);
                        if (dt.Columns.Contains("CRXReviewDate") && row["CRXReviewDate"] != DBNull.Value)
                            reviewTextAttribute.CRXReviewDate = Convert.ToDateTime(row["CRXReviewDate"]);
                        if (dt.Columns.Contains("ReviewStatus") && row["ReviewStatus"] != DBNull.Value)
                            reviewTextAttribute.ReviewStatus = Convert.ToInt32(row["ReviewStatus"]);

                        reviewText.TextAttributes.Add(reviewTextAttribute);
                    }
                }
            }
            return reviewText;
        }

        private QueueItemFacebookLike DataRowToQueueItemFacebookLike(DataRow dr)
        {
            QueueItemFacebookLike queueItem = new QueueItemFacebookLike();

            queueItem.MemberID = Convert.ToInt32(dr["MemberID"]);
            queueItem.BrandID = Convert.ToInt32(dr["BrandID"]);
            queueItem.SiteID = Convert.ToInt32(dr["SiteID"]);
            queueItem.CommunityID = Convert.ToInt32(dr["CommunityID"]);
            queueItem.ApprovalStatus = (ApprovalStatus)Convert.ToInt32(dr["ApprovalStatusID"]);
            queueItem.InsertDate = Convert.ToDateTime(dr["InsertDate"]);
            queueItem.StatusDate = Convert.ToDateTime(dr["StatusDate"]);
            return queueItem;
        }

        private QueueItemMemberProfileDuplicate ConvertToQueueItemMemberProfileDuplicate(DataSet ds)
        {
            QueueItemMemberProfileDuplicate queueMemberProfileDup = null;

            if (ds != null && ds.Tables != null && ds.Tables.Count == 2 && ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
            {
                queueMemberProfileDup = new QueueItemMemberProfileDuplicate();

                // table 1 contains the queue item info
                queueMemberProfileDup.QueueItemMemberProfileDuplicateID = Convert.ToInt32(ds.Tables[0].Rows[0]["MemberProfileDuplicateQueueID"]);
                queueMemberProfileDup.LanguageID = Convert.ToInt32(ds.Tables[0].Rows[0]["LanguageID"]);
                queueMemberProfileDup.InsertDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["InsertDate"]);

                queueMemberProfileDup.MemberProfileDuplicates = new List<MemberProfileDuplicate>();

                // table 2 contains the individual member profile values suspected of being duplicates
                foreach (DataRow row in ds.Tables[1].Rows)
                {
                    queueMemberProfileDup.MemberProfileDuplicates.Add(new MemberProfileDuplicate()
                    {
                        CRXResponseId = Convert.ToInt32(row["CRXResponseID"]),
                        DuplicateStatus = (DuplicateStatus)row["DuplicateStatusID"],
                        FieldType = (FieldType)row["FieldTypeID"],
                        FieldValue = row["FieldValue"].ToString(),
                        FieldValueOriginationDate = Convert.ToDateTime(row["FieldValueOriginationDate"]),
                        MemberId = Convert.ToInt32(row["MemberID"]),
                        OrdinalNumber = Convert.ToInt32(row["Ordinal"])
                    });
                }
            }

            return queueMemberProfileDup;
        }

        private QueueItemMemberProfileDuplicateReview ConvertToQueueItemMemberProfileDuplicateReview(DataSet ds)
        {
            QueueItemMemberProfileDuplicateReview queueItem = null;

            if (ds != null && ds.Tables != null && ds.Tables.Count == 2 && ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
            {
                queueItem = new QueueItemMemberProfileDuplicateReview();
                queueItem.MemberProfileDuplicateHistory = new MemberProfileDuplicateHistory();
                queueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryEntries =
                    new List<MemberProfileDuplicateHistoryEntry>();

                // table 1
                queueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryId =
                    Convert.ToInt32(ds.Tables[0].Rows[0]["MemberProfileDuplicateHistoryID"]);
                queueItem.MemberProfileDuplicateHistory.AdminId = Convert.ToInt32(ds.Tables[0].Rows[0]["AdminID"]);
                queueItem.MemberProfileDuplicateHistory.LanguageId = Convert.ToInt32(ds.Tables[0].Rows[0]["LanguageID"]);
                queueItem.MemberProfileDuplicateHistory.QueueInsertDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["QueueInsertDate"]);

                // table 2
                foreach (DataRow row in ds.Tables[1].Rows)
                {
                    var entry = new MemberProfileDuplicateHistoryEntry
                                     {
                                         MemberId = Convert.ToInt32(row["MemberID"]),
                                         CRXResponseId = Convert.ToInt32(row["CRXResponseID"]),
                                         FieldType = (FieldType) row["FieldTypeID"],
                                         FieldValueOriginationDate =
                                             Convert.ToDateTime(row["FieldValueOriginationDate"]),
                                         IsMatch = Convert.ToBoolean(row["IsMatch"]),
                                         Suspended = Convert.ToBoolean(row["Suspended"]),
                                         AlreadySuspended = Convert.ToBoolean(row["AlreadySuspended"]),
                                         OrdinalNumber = Convert.ToInt32(row["Ordinal"]),
                                         TextContent = row["FieldValue"] == DBNull.Value ? string.Empty : row["FieldValue"].ToString()
                                     };

                    queueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryEntries.Add(entry);
                }
            }

            return queueItem;
        }
    }
}
