﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceManagers;

namespace Matchnet.ApproveQueue.Harness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DBApproveQueueSA.Instance.GetApprovalPhotoQueueItems(103);
            //DBApproveQueueSM sm = new DBApproveQueueSM();
            //sm.GetApprovalPhotoQueueItems(103);
        }
    }

    public static class Program
    {

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new Form1());
        }

    }

    
    
}
