﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;

using Matchnet;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using Matchnet.Exceptions;
using Matchnet.ApproveQueue.BusinessLogic;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.ApproveQueue.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Data.Hydra;


namespace Matchnet.ApproveQueue.ServiceManagers
{
    public class DBApproveQueueSM : MarshalByRefObject, IDBApproveQueueService, IServiceManager, IDisposable
    {
        private HydraWriter _hydraWriter;

        public DBApproveQueueSM()
        {
            string machineName = System.Environment.MachineName;

            string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }

            #if DEBUG
                _hydraWriter = new HydraWriter(new string[] { "mnMemberApproval", "mnMemberApprovalWrite", "mnAdmin"});
            #else
			    _hydraWriter = new HydraWriter(new string[]{"mnMemberApproval", "mnMemberApprovalWrite", "mnAdmin"});
            #endif

            _hydraWriter.Start();
        }

        public void Enqueue(List<QueueItemBase> queueItems)
        {
            try
            {
                DBApproveQueueBL.Instance.Enqueue(queueItems);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to enqueue.", ex);
            }
        }

        public void Enqueue(QueueItemBase queueItem)
        {
            try
            {
                DBApproveQueueBL.Instance.Enqueue(queueItem);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to enqueue.", ex);
            }
        }

        public void EnqueueLight(QueueItemLightBase queueItem)
        {
            try
            {
                DBApproveQueueBL.Instance.EnqueueLight(queueItem);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to enqueue a light queue item.", ex);
            }
        }

        public void DeletePhotoQueueItem(int memberPhotoID)
        {
            try
            {
                DBApproveQueueBL.Instance.DeletePhotoQueueItem(memberPhotoID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to delete.", ex);
            }
        }

        public void DeleteFacebookLikeQueueItem(int memberID, int siteID)
        {
            try
            {
                DBApproveQueueBL.Instance.DeleteFacebookLikeQueueItem(memberID, siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to delete.", ex);
            }
        }


        public List<IndividualTextApprovalStatus> GetIndividualTextApprovalStatuses()
        {
            try
            {
                return DBApproveQueueBL.Instance.GetIndividualTextApprovalStatuses();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Ge tIndividual Text Approval aStatuses.", ex);
            }
        }

        public List<QueueItemBase> DequeuePhotosByCommunity(int maxCount, Int32 communityID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeuePhotosByCommunity(maxCount, communityID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Dequeue Photos By Community.", ex);
            }
        }

        public List<QueueItemBase> DequeuePhotosBySite(int maxCount, Int32 siteID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeuePhotosBySite(maxCount, siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Dequeue Photos By Site.", ex);
            }
        }
        
        public List<QueueItemBase> DequeuePhotosByMember(int maxCount, Int32 memberID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeuePhotosByMember(maxCount, memberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Dequeue Photos By Member.", ex);
            }
        }

        public void CompletePhotoApproval(int memberPhotoID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            try
            {
                DBApproveQueueBL.Instance.CompletePhotoApproval(memberPhotoID, approvalStatus, adminMemberID, adminActionMask);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete photo approval.", ex);
            }
        }

        public List<QueueItemBase> DequeueFacebookLikesByCommunity(int maxCount, Int32 communityID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueFacebookLikesByCommunity(maxCount, communityID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Dequeue Photos By Community.", ex);
            }
        }

        public List<QueueItemBase> DequeueFacebookLikesBySite(int maxCount, Int32 siteID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueFacebookLikesBySite(maxCount, siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Dequeue Photos By Site.", ex);
            }
        }

        public List<QueueItemBase> DequeueFacebookLikesByMember(int maxCount, Int32 memberID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueFacebookLikesByMember(maxCount, memberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Dequeue Photos By Member.", ex);
            }
        }

        public void CompleteFacebookLikesApproval(int memberId, int siteID, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteFacebookLikesApproval(memberId, siteID, approvalStatus, adminMemberID, adminActionMask);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete photo approval.", ex);
            }
        }

        public void CompleteTextApproval(int memberID, int communityID, int languageID, TextType textType, ApprovalStatus approvalStatus, int adminMemberID, int adminActionMask)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteTextApproval(memberID, communityID, languageID, textType, approvalStatus, adminMemberID, adminActionMask);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete text approval.", ex);
            }
        }

        public void CompleteTextApprovalMemberApproval(int memberID, int memberTextApprovalID,
                                                       ApprovalStatus approvalStatus, int adminMemberID,
                                                       int adminActionMask)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteTextApprovalMemberApproval(memberID, memberTextApprovalID,
                                                                             approvalStatus, adminMemberID,
                                                                             adminActionMask);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete text approval for MemberApproval view.", ex);
            }
        }

        public void SaveIndividualTextAttributeApproval(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveIndividualTextAttributeApproval(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Individual Text Attribute Approval.", ex);
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Individual Text Attribute Approval.", ex);
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID, int statusMask, int adminMemberID, string textContent, bool isEdit, string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity, int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity, daysSinceFirstSubscription, billingPhoneNumber, subscriptionStatus, registrationDate);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Individual Text Attribute Approval.", ex);
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
            int statusMask, int adminMemberID, string textContent, bool isEdit,
            string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
            string occupationDescription, int educationLevel, int religion, int ethnicity,
            int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, int? memberTextApprovalID, string adminTextContent)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity, daysSinceFirstSubscription, billingPhoneNumber, subscriptionStatus, registrationDate, memberTextApprovalID, adminTextContent);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Individual Text Attribute Approval.", ex);
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
            int statusMask, int adminMemberID, string textContent, bool isEdit,
            string ip, int profileRegionId, int genderMask, int maritalStatus, string firstName, string lastName, string email,
            string occupationDescription, int educationLevel, int religion, int ethnicity,
            int daysSinceFirstSubscription, string billingPhoneNumber, string subscriptionStatus, DateTime registrationDate, int? memberTextApprovalID, string adminTextContent, ApprovalStatus reviewStatus)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID, languageID, statusMask, adminMemberID, textContent, isEdit, ip, profileRegionId, genderMask, maritalStatus, firstName, lastName, email, occupationDescription, educationLevel, religion, ethnicity, daysSinceFirstSubscription, billingPhoneNumber, subscriptionStatus, registrationDate, memberTextApprovalID, adminTextContent, reviewStatus);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Individual Text Attribute Approval.", ex);
            }
        }

        public void SaveIndividualTextAttributeApprovalExpanded(int memberID, int memberAttributeGroupID, int languageID,
                                                                int statusMask, int adminMemberID, string textContent,
                                                                bool isEdit,
                                                                string ip, int profileRegionId, int genderMask,
                                                                int maritalStatus, string firstName, string lastName,
                                                                string email,
                                                                string occupationDescription, int educationLevel,
                                                                int religion, int ethnicity,
                                                                int daysSinceFirstSubscription,
                                                                string billingPhoneNumber, string subscriptionStatus,
                                                                DateTime registrationDate,
                                                                int? memberTextApprovalID, string adminTextContent,
                                                                ApprovalStatus reviewStatus, bool deleteMovedRows)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID, memberAttributeGroupID,
                                                                                      languageID, statusMask,
                                                                                      adminMemberID, textContent, isEdit,
                                                                                      ip, profileRegionId, genderMask,
                                                                                      maritalStatus, firstName, lastName,
                                                                                      email, occupationDescription,
                                                                                      educationLevel, religion,
                                                                                      ethnicity,
                                                                                      daysSinceFirstSubscription,
                                                                                      billingPhoneNumber,
                                                                                      subscriptionStatus,
                                                                                      registrationDate,
                                                                                      memberTextApprovalID,
                                                                                      adminTextContent, reviewStatus,
                                                                                      deleteMovedRows);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME,
                                                   "Unable to Save Individual Text Attribute Approval.", ex);
            }
        }

        public CRXText LocateCRXText(int crxResponseId)
        {
            try
            {
                return DBApproveQueueBL.Instance.LocateCRXText(crxResponseId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to locate the CRX text.", ex);
            }
        }

        public QueueItemMemberProfileDuplicate DequeueMemberProfileDuplicateByLanguage(int languageId)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueMemberProfileDuplicateByLanguage(languageId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue MemberProfileDuplicate.", ex);
            }
        }

        public QueueItemMemberProfileDuplicateReview DequeueMemberProfileDuplicateReview(int languageID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueMemberProfileDuplicateReview(languageID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue MemberProfileDuplicateReview.", ex);
            }
        }

        public QueueItemMemberProfileDuplicateReview GetMemberProfileDuplicateReviewQueueItem(int memberProfileDuplicateHistoryID)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetMemberProfileDuplicateReviewQueueItem(memberProfileDuplicateHistoryID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a MemberProfileDuplicateReview queue item.", ex);
            }
        }

        public QueueItemMemberProfileDuplicate GetMemberProfileDuplicateQueueItem(int duplicateQueueID)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetMemberProfileDuplicateQueueItem(duplicateQueueID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve MemberProfileDuplicate MemberProfileDuplicateQueueID: ." + duplicateQueueID.ToString(), ex);
            }
        }

        public QueueItemText DequeueTextByLanguage(Int32 languageID, TextType textType)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueTextByLanguage(languageID, textType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue text by language.", ex);
            }
        }

        public QueueItemText DequeueTextBySite(Int32 siteID, TextType textType)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueTextBySite(siteID, textType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue text by site.", ex);
            }
        }

        public QueueItemText DequeueTextByCommunity(Int32 communityID, TextType textType)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueTextByCommunity(communityID, textType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue text by community.", ex);
            }
        }

        public QueueItemText DequeueTextByMember(Int32 memberID, TextType textType)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueTextByMember(memberID, textType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue text by member.", ex);
            }
        }

        public List<ApprovalCountRecord> GetApprovalHistoryCounts(DateTime startDate, DateTime endDate, int adminMemberID, int communityID, int siteID)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetApprovalHistoryCounts(startDate, endDate, adminMemberID, communityID, siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get history counts.", ex);
            }
        }

        public List<MemberProfileDuplicateHistoryCount> GetMemberProfileDuplicateHistoryCount(DateTime startDate, DateTime endDate, int adminMemberID)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetMemberProfileDuplicateHistoryCount(startDate, endDate, adminMemberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get memberprofileduplicate history counts.", ex);
            }
        }

        public List<QueueCountRecord> GetQueueCounts(QueueItemType itemType)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetQueueCounts(itemType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get queue counts.", ex);
            }
        }

        public List<QueueItemBase> GetApprovalPhotoQueueItems(int siteID)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetApprovalPhotoQueueItems(siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get photos queue.", ex);
            }
        }

        public void SaveMemberTextApprovalAttributes(List<QueueItemText> queueItemTexts)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveMemberTextApprovalAttributes(queueItemTexts);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to save member approval attributes", ex);
            }
        }

        public List<QueueItemMember> DequeueMemberBySite(Int32 siteID)
        {
            try
            {
                return DBApproveQueueBL.Instance.DequeueMemberBySite(siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to dequeue member from Fraud Review queue.", ex);
            }
        }

        public void CompleteMemberFraudReview(List<QueueItemMember> memberItems)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteMemberFraudReview(memberItems);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete Member Fraud Review.", ex);
            }
        }

        public List<QueueItemText> GetTextAttributesForMember(int memberID, int communityID, int languageID, TextType textType)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetTextAttributesForMember(memberID, communityID, languageID, textType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get member text approval attributes for a member.", ex);
            }
        }
        
        public List<QueueItemText> GetTextAttributesForCRXApproval(int? communityId, int? languageId, int? textType, int? numberOfRows)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetTextAttributesForCRXApproval(communityId, languageId, textType, numberOfRows);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get member text approval attributes.", ex);
            }
        }

        public void CompleteCRXTextAttributeArroval(QueueItemText itemText)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteCRXTextAttributeArroval(itemText);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete CRX text attribute approval.", ex);
            }
        }

        public void SaveAdminReviewSettings(int adminMemberId, int? dailySampleRate, int? onDemandSampleRate, bool isEdit)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveAdminReviewSettings(adminMemberId, dailySampleRate, onDemandSampleRate, isEdit);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Admin review settings.", ex);
            }
        }

        public void SaveAdminReviewSettings(List<AdminReviewSetting> adminReviewSettings)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveAdminReviewSettings(adminReviewSettings);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to Save Admin review settings.", ex);
            }
        }

        public TextApprovalReviewCount GetTextApprovalReviewCounts(bool isOnDemand)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetTextApprovalReviewCounts(isOnDemand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get FTA review counts.", ex);
            }
        }


        public ReviewText GetTextToReview(bool isOnDemand, int? supervisorId, int? adminMemberId)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetTextToReview(isOnDemand,supervisorId,adminMemberId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get free text for review .", ex);
            }
        }

        public void CompleteAdminTextApprovalReview(ReviewText reviewText)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteAdminTextApprovalReview(reviewText);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete free text review .", ex);
            }
        }

        public void AcknowledgeTrainingReport(int memberTextApprovalId, int adminMemberId, int supervisorId, bool isOnDemand)
        {
            try
            {
                DBApproveQueueBL.Instance.AcknowledgeTrainingReport(memberTextApprovalId, adminMemberId, supervisorId, isOnDemand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to acknowledge admin FTA training report .", ex);
            }
        }

        public List<TextApprovalReviewHistoryCount> GetTextApprovalReviewHistoryCounts(DateTime startDate, DateTime endDate, int? adminMemberId, int? communityId)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetTextApprovalReviewHistoryCounts(startDate, endDate, adminMemberId, communityId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get Text Approval Review HistoryCounts.", ex);
            }
        }

        public List<AdminReviewSetting> GetAdminReviewSampleRates()
        {
            try
            {
                return DBApproveQueueBL.Instance.GetAdminReviewSampleRates();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get Admin review sample rates.", ex);
            }
        }

        public List<QueueItemText> GetApprovalTextAttributesCRXResponse(int memberTextApprovalId)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetApprovalTextAttributesCRXResponse(memberTextApprovalId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get member text approval attributes and CRX Response.", ex);
            }
        }

        public ReviewText GetCRXTextForSecondReview()
        {
            try
            {
                return DBApproveQueueBL.Instance.GetCRXTextForSecondReview();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get free text for second review .", ex);
            }
        }

        public void CompleteCRXSecondReview(ReviewText reviewText)
        {
            try
            {
                DBApproveQueueBL.Instance.CompleteCRXSecondReview(reviewText);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to complete CRX free text second review .", ex);
            }
        }

        public QueueItemText GetCRXTextAttributeInfo(int CrxResponseId)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetCRXTextAttributeInfo(CrxResponseId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get CRX text attribute info.", ex);
            }
        }

        public TextApprovalReviewCount GetCRXSecondReviewCounts()
        {
            try
            {
                return DBApproveQueueBL.Instance.GetCRXSecondReviewCounts();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get FTA CRX second review counts.", ex);
            }
        }

        public void PopulateOnDemandTextReview(DateTime startDate, DateTime endDate, int? communityId, int supervisorId, List<AdminReviewSetting> adminRates)
        {
            try
            {
                DBApproveQueueBL.Instance.PopulateOnDemandTextReview(startDate,endDate,communityId,supervisorId,adminRates);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to populate OnDemand Admin Text Apporval Review.", ex);
            }
        }

        public List<TextApprovalReviewCount> GetOnDemandReviewQueueCounts()
        {
            try
            {
                return DBApproveQueueBL.Instance.GetOnDemandReviewQueueCounts();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get OnDemand FTA review counts.", ex);
            }
        }

        public void PurgeOnDemandReviewQueue(int adminMemberId, int supervisorId)
        {
            try
            {
                DBApproveQueueBL.Instance.PurgeOnDemandReviewQueue(adminMemberId, supervisorId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to purge FTA on demand review queue.", ex);
            }
        }

        public void SaveMemberProfileDuplicate(QueueItemMemberProfileDuplicate queueItem)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveMemberProfileDuplicate(queueItem);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to save MemberProfileDuplicate.", ex);
            }
        }

        public void SaveMemberProfileDuplicateHistory(MemberProfileDuplicateHistory history, int memberProfileDuplicateQueueId)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveMemberProfileDuplicateHistory(history, memberProfileDuplicateQueueId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to save MemberProfileDuplicateHistory.", ex);
            }
        }

        public void DeleteMemberProfileDuplicateQueueItem(int MemberProfileDuplicateQueueID)
        {
            try
            {
                DBApproveQueueBL.Instance.DeleteMemberProfileDuplicateQueueItem(MemberProfileDuplicateQueueID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to delete MemberProfileDuplicateQueueItem.", ex);
            }
        }

        public void SaveMemberProfileDuplicateReview(MemberProfileDuplicateReview duplicateReview)
        {
            try
            {
                DBApproveQueueBL.Instance.SaveMemberProfileDuplicateReview(duplicateReview);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to save MemberProfileDuplicateReview.", ex);
            }
        }

        public void RevertCRXResponse()
        {
            try
            {
                DBApproveQueueBL.Instance.RevertCRXResponse();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to revert CRX Response.", ex);
            }
        }


        public DateTime GetLastCRXResponseResetTime(int CRXResponseID)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetLastCRXResponseResetTime(CRXResponseID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get Last CRXResponseResetTime.", ex);
            }
        }

        public QueueItemText GetCRXResponse(int CrxResponseId)
        {
            try
            {
                return DBApproveQueueBL.Instance.GetCRXResponse(CrxResponseId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get CRX response.", ex);
            }
        }

        public List<QueueItemText> VerifyCRXResponse(int? communityId, int? languageId, int? textType, int? numberOfRows)
        {
            try
            {
                return DBApproveQueueBL.Instance.VerifyCRXResponse(communityId, languageId, textType, numberOfRows);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to get member text approval attributes.", ex);
            }
        }

        #region TrainingToolBL
        public void CreateNewGrouping(string groupingName)
        {
            try
            {
                TrainingToolBL.Instance.CreateNewGrouping(groupingName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to create a new training tool grouping.", ex);
            }
        }

        public List<TTGrouping> GetGroupingList()
        {
            try
            {
                return TrainingToolBL.Instance.GetGroupingList();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a list of training tool groupings.", ex);
            }
        }

        public TTGrouping GetTTGrouping(int ttGroupingID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTGrouping(ttGroupingID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the specified grouping.", ex);
            }
        }

        public List<TTGrouping> GetCompletedGroupingList()
        {
            try
            {
                return TrainingToolBL.Instance.GetCompletedGroupingList();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a list of completed training tool groupings.", ex);
            }
        }

        public TTMemberProfile GetTTMemberAttributes(int ttMemberProfileID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberAttributes(ttMemberProfileID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the training tool member attributes.", ex);
            }
        }

        public TTMessage GetTTMessage(int ttMessageID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMessage(ttMessageID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the specified training tool email.", ex);
            }
        }

        public List<TTMessage> GetTTMessages(int ttMemberProfileID, bool populateMessageBody)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMessages(ttMemberProfileID, populateMessageBody);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the training tool emails.", ex);
            }
        }

        public TTAdminAnswerReport GetTTAdminAnswerReport(int ttTestInstanceID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTAdminAnswerReport(ttTestInstanceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the training tool test results.", ex);
            }
        }

        public List<TTMemberProfile> GetTTMemberProfiles(int groupingID, int pageNumber, int rowsPerPage, out int totalRows)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberProfiles(groupingID, pageNumber, rowsPerPage, out totalRows);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a list of member profiles for a given groupingID.", ex);
            }
        }

        public List<TTMemberProfile> GetTTMemberProfiles(int memberID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberProfiles(memberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a list of member profiles for a given memberID.", ex);
            }
        }

        public TTMemberProfile GetTTMemberProfile(int memberID, int ttGroupingID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberProfile(memberID, ttGroupingID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the member profile for given memberID+ttGroupingID.", ex);
            }
        }

        public List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int ttTestInstanceID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTAnswerMemberProfiles(ttTestInstanceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME,
                    "Unable to retrieve the TTAnswerMemberProfiles for given TTTestInstanceID.", ex);
            }
        }

        public List<TTAnswerMemberProfile> GetTTAnswerMemberProfiles(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTAnswerMemberProfiles(memberID, groupingID, getAllVersions, ttMemberProfileID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the specified TTAnswerMemberProfile.", ex);
            }
        }

        public List<TTMemberAttributeText> GetAllTTMemberTextAttributes(int memberID, int groupingID)
        {
            try
            {
                return TrainingToolBL.Instance.GetAllTTMemberTextAttributes(memberID, groupingID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a list of text attributes for a given member.", ex);
            }
        }

        public List<TTMemberAttributeText> GetAllTTMemberTextAttributesWithAnswer(int memberID, int groupingID, bool getAllVersions, int? ttMemberProfileID)
        {
            try
            {
                return TrainingToolBL.Instance.GetAllTTMemberTextAttributesWithAnswer(memberID, groupingID, getAllVersions, ttMemberProfileID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve a list of text attributes with answer for a given member.", ex);
            }
        }

        public void InsertTTMemberProfiles(string groupingName, List<TTMemberProfile> memberProfiles)
        {
            try
            {
                TrainingToolBL.Instance.InsertTTMemberProfiles(groupingName, memberProfiles);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to add a member profile to a training tool grouping.", ex);
            }
        }

        public void InsertTTMemberProfiles(int groupingID, List<TTMemberProfile> memberProfiles)
        {
            try
            {
                TrainingToolBL.Instance.InsertTTMemberProfiles(groupingID, memberProfiles);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to add a member profile to a training tool grouping.", ex);
            }
        }

        public void SaveTTAnswer(TTAnswerMemberProfile profile, List<TTMemberAttributeText> texts)
        {
            try
            {
                TrainingToolBL.Instance.SaveTTAnswer(profile, texts);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to add save the Training Tool answer.", ex);
            }
        }

        public List<TTMemberAttributeText> GetTTAnswerMemberAttributeTexts(int ttTestInstanceID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTAnswerMemberAttributeTexts(ttTestInstanceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve TTAnswerMemberAttributeTexts for a givent TTTestInstanceID.", ex);
            }
        }

        public int CreateNewTestInstance(int groupingID, int adminID)
        {
            try
            {
                return TrainingToolBL.Instance.CreateNewTestInstance(groupingID, adminID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to create a new Training Tool test instance.", ex);
            }
        }

        public void SaveAdminAnswer(TTAdminAnswer adminAnswer, bool isSynchronousWrite)
        {
            try
            {
                TrainingToolBL.Instance.SaveAdminAnswer(adminAnswer, isSynchronousWrite);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to save an admin's response to a Training Tool question.", ex);
            }
        }

        public void UpdateTestStatus(int ttTestInstanceID, TTTestInstanceStatus testStatus)
        {
            try
            {
                TrainingToolBL.Instance.UpdateTestStatus(ttTestInstanceID, testStatus);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to update the test timespan info.", ex);
            }
        }

        public void UpdateTTMemberProfileTTGroupingID(int ttMemberProfileID, int newGroupingID)
        {
            try
            {
                TrainingToolBL.Instance.UpdateTTMemberProfileTTGroupingID(ttMemberProfileID, newGroupingID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to update the TTMemberProfile's TTGroupingID.", ex);
            }
        }

        public void TrainingToolKeepalivePulse(int ttTestInstanceID)
        {
            try
            {
                TrainingToolBL.Instance.KeepalivePulse(ttTestInstanceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to send a keepalive pulse.", ex);
            }
        }

        public List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                          int? ttMemberProfileIDToExclude)
        {
            try
            {
                return TrainingToolBL.Instance.GetNextTTMemberProfileQuestion(ttTestInstanceID, ttMemberProfileIDToExclude);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the next Training Tool question.", ex);
            }
        }

        public List<TTMemberAttributeText> GetNextTTMemberProfileQuestion(int ttTestInstanceID,
                                                                          int? ttMemberProfileIDToExclude,
                                                                          out int remainingCount,
                                                                          out int ttAnswerMemberProfileVersion,
                                                                          out int ttAnswerSuspendReasonsVersion)
        {
            try
            {
                return TrainingToolBL.Instance.GetNextTTMemberProfileQuestion(ttTestInstanceID,
                                                                              ttMemberProfileIDToExclude,
                                                                              out remainingCount,
                                                                              out ttAnswerMemberProfileVersion,
                                                                              out ttAnswerSuspendReasonsVersion);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                                   "Unable to retrieve the next Training Tool question.", ex);
            }
        }

        public int GetTTMemberProfileCount(int ttTestInstanceID, bool answeredOnly)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberProfileCount(ttTestInstanceID, answeredOnly);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the count for the given ttTestInstanceID.", ex);
            }
        }

        public List<TTTestInstance> GetTTTestInstances(int adminMemberID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTTestInstances(adminMemberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the list of test instances.", ex);
            }
        }

        public TTTestInstance GetTTTestInstance(int ttTestInstanceID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTTestInstance(ttTestInstanceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the specified test instance.", ex);
            }
        }

        public double GetTotalTimeSpentOnTest(int ttTestInstanceID, out bool testFinished)
        {
            try
            {
                return TrainingToolBL.Instance.GetTotalTimeSpentOnTest(ttTestInstanceID, out testFinished);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to total up the time spent on a test.", ex);
            }
        }

        public List<TTMemberPhoto> GetTTMemberPhotos(int memberID, int groupingID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberPhotos(memberID, groupingID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the TTMemberPhotos.", ex);
            }
        }

        public List<TTMemberPhoto> GetTTMemberPhotos(int ttMemberProfileID)
        {
            try
            {
                return TrainingToolBL.Instance.GetTTMemberPhotos(ttMemberProfileID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.ApproveQueue.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to retrieve the TTMemberPhotos.", ex);
            }
        }


        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }


        public void PrePopulateCache()
        {
        }

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }
        #endregion
    }
}
