﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Matchnet.Configuration.ServiceAdapters;
using NUnit.Framework;

namespace Spark.FacebookLike.Tests
{
    public abstract class AbstractSparkUnitTest
    {
        protected bool ENABLE_PRINT = false;
        protected MockSettingsService _mockSettingsService = null;

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if(!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }


        protected void printMaskValues(List<int> values)
        {
            if (ENABLE_PRINT)
            {
                Console.WriteLine(string.Format("---------VALUES SIZE {0}---------", values.Count));
                foreach (int value in values)
                {
                    Console.WriteLine(string.Format("Value: {0}", value));
                }
            }
        }

        protected DataTable CreateKeywordDataTable(string tableName)
        {
            DataTable dataTable = new DataTable(tableName);
            //columns for search
            dataTable.Columns.Add("memberid", typeof(int));
            dataTable.Columns.Add("communityid", typeof(int));
            dataTable.Columns.Add("gendermask", typeof(int));
            dataTable.Columns.Add("lastactivedate", typeof(DateTime));
            dataTable.Columns.Add("updatedate", typeof(DateTime));
            dataTable.Columns.Add("depth1regionid", typeof(int));
            dataTable.Columns.Add("depth2regionid", typeof(int));
            dataTable.Columns.Add("depth3regionid", typeof(int));
            dataTable.Columns.Add("depth4regionid", typeof(int));
            dataTable.Columns.Add("longitude", typeof(double));
            dataTable.Columns.Add("latitude", typeof(double));
            dataTable.Columns.Add("areacode", typeof(string));
            //columns for keyword search
            dataTable.Columns.Add("aboutme", typeof(string));
            dataTable.Columns.Add("perfectmatchessay", typeof(string));
            dataTable.Columns.Add("perfectfirstdateessay", typeof(string));
            dataTable.Columns.Add("idealrelationshipessay", typeof(string));
            dataTable.Columns.Add("learnfromthepastessay", typeof(string));
            dataTable.Columns.Add("lifeandabmitionessay", typeof(string));
            dataTable.Columns.Add("historyofmylifeessay", typeof(string));
            dataTable.Columns.Add("onourfirstdateremindmetoessay", typeof(string));
            dataTable.Columns.Add("thingscantlivewithoutessay", typeof(string));
            dataTable.Columns.Add("favoritebooksmoviesetcessay", typeof(string));
            dataTable.Columns.Add("coolestplacesvisitedessay", typeof(string));
            dataTable.Columns.Add("forfuniliketoessay", typeof(string));
            dataTable.Columns.Add("onfrisatitypicallyessay", typeof(string));
            dataTable.Columns.Add("messagemeifyouessay", typeof(string));
            dataTable.Columns.Add("cuisine", typeof(string));
            dataTable.Columns.Add("entertainmentlocation", typeof(string));
            dataTable.Columns.Add("leisureactivity", typeof(string));
            dataTable.Columns.Add("music", typeof(string));
            dataTable.Columns.Add("personalitytrait", typeof(string));
            dataTable.Columns.Add("pets", typeof(string));
            dataTable.Columns.Add("physicalactivity", typeof(string));
            dataTable.Columns.Add("reading", typeof(string));
            dataTable.Columns.Add("essayfirstdate", typeof(string));
            dataTable.Columns.Add("essaygettoknowme", typeof(string));
            dataTable.Columns.Add("essaypastrelationship", typeof(string));
            dataTable.Columns.Add("essayimportantthings", typeof(string));
            dataTable.Columns.Add("essayperfectday", typeof(string));
            dataTable.Columns.Add("essaymorethingstoadd", typeof(string));
            dataTable.Columns.Add("favoritebands", typeof(string));
            dataTable.Columns.Add("favoritemovies", typeof(string));
            dataTable.Columns.Add("favoritetv", typeof(string));
            dataTable.Columns.Add("favoriterestaurant", typeof(string));
            dataTable.Columns.Add("schoolsattended", typeof(string));
            dataTable.Columns.Add("mygreattripidea", typeof(string));
            dataTable.Columns.Add("moviegenre", typeof(string));
            return dataTable;
        }

        protected DataTable CreateDataTable(string tableName)
        {
            DataTable dataTable = new DataTable(tableName);
            //columns for search
            dataTable.Columns.Add("memberid", typeof (int));
            dataTable.Columns.Add("communityid", typeof (int));
            dataTable.Columns.Add("gendermask", typeof (int));
            dataTable.Columns.Add("birthdate", typeof (DateTime));
            dataTable.Columns.Add("communityinsertdate", typeof (DateTime));
            dataTable.Columns.Add("lastactivedate", typeof(DateTime));
            dataTable.Columns.Add("hasphotoflag", typeof (int));
            dataTable.Columns.Add("educationlevel", typeof (int));
            dataTable.Columns.Add("religion", typeof (int));
            dataTable.Columns.Add("languagemask", typeof (int));
            dataTable.Columns.Add("ethnicity", typeof (int));
            dataTable.Columns.Add("smokinghabits", typeof (int));
            dataTable.Columns.Add("drinkinghabits", typeof (int));
            dataTable.Columns.Add("height", typeof (int));
            dataTable.Columns.Add("maritalstatus", typeof (int));
            dataTable.Columns.Add("jdatereligion", typeof (int));
            dataTable.Columns.Add("jdateethnicity", typeof (int));
            dataTable.Columns.Add("synagogueattendance", typeof (int));
            dataTable.Columns.Add("keepkosher", typeof (int));
            dataTable.Columns.Add("sexualidentitytype", typeof (int));
            dataTable.Columns.Add("relationshipmask", typeof (int));
            dataTable.Columns.Add("relationshipstatus", typeof (int));
            dataTable.Columns.Add("bodytype", typeof (int));
            dataTable.Columns.Add("zodiac", typeof (int));
            dataTable.Columns.Add("majortype", typeof (int));
            dataTable.Columns.Add("depth1regionid", typeof (int));
            dataTable.Columns.Add("depth2regionid", typeof (int));
            dataTable.Columns.Add("depth3regionid", typeof (int));
            dataTable.Columns.Add("depth4regionid", typeof (int));
            dataTable.Columns.Add("longitude", typeof (double));
            dataTable.Columns.Add("latitude", typeof (double));
            dataTable.Columns.Add("schoolid", typeof (int));
            dataTable.Columns.Add("areacode", typeof (string));
            dataTable.Columns.Add("emailcount", typeof (int));
            dataTable.Columns.Add("updatedate", typeof (DateTime));
            dataTable.Columns.Add("weight", typeof (int));
            dataTable.Columns.Add("relocateflag", typeof (int));
            dataTable.Columns.Add("childrencount", typeof (int));
            dataTable.Columns.Add("subscriptionexpirationdate", typeof (DateTime));
            dataTable.Columns.Add("activitylevel", typeof (int));
            dataTable.Columns.Add("custody", typeof (int));
            dataTable.Columns.Add("morechildrenflag", typeof (int));
            dataTable.Columns.Add("colorcode", typeof (int));

            //columns for reverse search
            dataTable.Columns.Add("preferredgendermask", typeof(int));
            dataTable.Columns.Add("preferredrelocation", typeof(int));
            dataTable.Columns.Add("preferredminheight", typeof (int));
            dataTable.Columns.Add("preferredmaxheight", typeof (int));
            dataTable.Columns.Add("preferredminage", typeof (int));
            dataTable.Columns.Add("preferredmaxage", typeof (int));
            dataTable.Columns.Add("preferredsynagogueattendance", typeof (int));
            dataTable.Columns.Add("preferredjdatereligion", typeof (int));
            dataTable.Columns.Add("preferredsmokinghabit", typeof (int));
            dataTable.Columns.Add("preferreddrinkinghabit", typeof (int));
            dataTable.Columns.Add("preferredjdateethnicity", typeof (int));
            dataTable.Columns.Add("preferredactivitylevel", typeof (int));
            dataTable.Columns.Add("preferredkosherstatus", typeof (int));
            dataTable.Columns.Add("preferredmaritalstatus", typeof (int));
            dataTable.Columns.Add("preferredlanguagemask", typeof (int));
            dataTable.Columns.Add("preferreddistance", typeof (int));
            dataTable.Columns.Add("preferredregionid", typeof (int));
            dataTable.Columns.Add("preferredareacodes", typeof (string));
            dataTable.Columns.Add("preferredhasphotoflag", typeof (int));
            dataTable.Columns.Add("preferrededucationlevel", typeof (int));
            dataTable.Columns.Add("preferredreligion", typeof (int));
            dataTable.Columns.Add("preferredethnicity", typeof (int));
            dataTable.Columns.Add("preferredsexualidentitytype", typeof (int));
            dataTable.Columns.Add("preferredrelationshipmask", typeof (int));
            dataTable.Columns.Add("preferredrelationshipstatus", typeof (int));
            dataTable.Columns.Add("preferredbodytype", typeof (int));
            dataTable.Columns.Add("preferredzodiac", typeof (int));
            dataTable.Columns.Add("preferredmajortype", typeof (int));
            dataTable.Columns.Add("preferredmorechildrenflag", typeof (int));
            dataTable.Columns.Add("preferredchildrencount", typeof (int));

            return dataTable;
        }

        protected DataRow CreateDataRow(DataTable dt, object[] itemArray)
        {
            DataRow dataRow = dt.NewRow();
            dataRow.ItemArray = itemArray;
            return dataRow;
        }

        protected void SetupMockSettingsService()
        {
            _mockSettingsService = new MockSettingsService();
        }
    }

    public class MockSettingsService : ISettingsSA
    {
        private Dictionary<int, Dictionary<string, string>> communitySettings = new Dictionary<int, Dictionary<string, string>>();
        #region ISettingsSA Members

        public void AddSetting(string key, string value)
        {
            AddSetting(key, value, 0);
        }

        public void AddSetting(string key, string value, int communityId)
        {
            if (!communitySettings.ContainsKey(communityId)) communitySettings.Add(communityId, new Dictionary<string, string>());
            communitySettings[communityId].Add(key, value);
        }

        public void RemoveSetting(string key)
        {
            RemoveSetting(key, 0);
        }

        public void RemoveSetting(string key, int communityId)
        {
            if (SettingExistsFromSingleton(key, communityId, 0))
            {
                communitySettings[communityId].Remove(key);
            }
            else if (SettingExistsFromSingleton(key, 0, 0))
            {
                communitySettings[0].Remove(key);
            }
        }

        public System.Collections.Generic.List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new System.NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant)
        {
            try
            {
                return communitySettings[0][constant];
            }
            catch (Exception e)
            {
                print(string.Format("Could not find setting key:{0}, community:{1}", constant, 0));
                throw e;
            }
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityId, int siteId)
        {
            bool settingExistsFromSingleton = communitySettings.ContainsKey(communityId) && communitySettings[communityId].ContainsKey(constant);
            //if (!settingExistsFromSingleton)
            //{
            //    print(string.Format("Could not find setting key:{0}, community:{1}", constant, communityId));
            //}
            return settingExistsFromSingleton;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if (!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        public List<Matchnet.Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
