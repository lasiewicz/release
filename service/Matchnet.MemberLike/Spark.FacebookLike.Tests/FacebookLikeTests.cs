﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.FacebookLike.BusinessLogic;
using Spark.FacebookLike.ServiceAdapters;
using Spark.FacebookLike.ValueObjects;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;


namespace Spark.FacebookLike.Tests
{
    [TestFixture]
    public class FacebookLikeTests : AbstractSparkUnitTest
    {
        [Test]
        public void TestGetFacebookLikeCategoryGroups()
        {
            FacebookLikeCategoryGroupList groupList = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance.GetFacebookLikeCategoryGroupList();
            foreach (FacebookLikeCategoryGroup categoryGroup in groupList.FacebookLikeCategoryGroups)
            {
                print(categoryGroup.ToString());
            }
        }

        [Test]
        public void TestInvalidFacebookParams()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(12345, 103, 10999903003, 41, string.Empty, "", "http://en.wikipedia.org/wiki/Modern_Family", "television");

            fbParams.Add(facebookLikeParams1);
            
            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.InvalidParameters, result.SaveStatus);
            Assert.True(result.InvalidParameters[facebookLikeParams1.FacebookId].Contains("FBName"));
            Assert.True(result.InvalidParameters[facebookLikeParams1.FacebookId].Contains("FbImageUrl"));

            facebookLikeParams1 = new FacebookLikeParams(-1, -1, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "television");
            fbParams.Clear();
            fbParams.Add(facebookLikeParams1);
            result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null); 
            Assert.AreEqual(FacebookLikeSaveStatusType.InvalidParameters, result.SaveStatus);
            Assert.True(result.InvalidParameters[facebookLikeParams1.FacebookId].Contains("MemberId"));
            Assert.True(result.InvalidParameters[facebookLikeParams1.FacebookId].Contains("SiteId"));

            facebookLikeParams1 = new FacebookLikeParams(12345, 103, -1, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", string.Empty);
            fbParams.Clear();
            fbParams.Add(facebookLikeParams1);
            result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.InvalidParameters, result.SaveStatus);
            Assert.True(result.InvalidParameters[facebookLikeParams1.FacebookId].Contains("FacebookId"));
            Assert.AreEqual(1,result.InvalidParameters[facebookLikeParams1.FacebookId].Count);
        }

        [Test]
        public void TestAddFacebookLike()
        {
            FacebookLikeParams facebookLikeParams = new FacebookLikeParams(4321, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "telvision");
            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.AddFacebookLike(facebookLikeParams, false, null, false);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams);
            AssertFacebookLike(facebookLikeParams,fbLikeListByMember);
            facebookLikeServiceBl.RemoveAllFacebookLikes(facebookLikeParams, false, null);
            System.Threading.Thread.Sleep(200);
        }

        [Test]
        public void TestAddSameFacebookLikeTwice()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(123467, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "telvision");
            fbParams.Add(facebookLikeParams1);

            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            AssertFacebookLike(facebookLikeParams1, fbLikeListByMember);

            result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);

            facebookLikeServiceBl.RemoveAllFacebookLikes(fbParams.First(), false, null);
            System.Threading.Thread.Sleep(200);
        }

        [Test]
        public void TestAddFacebookLikeWithNullFBType()
        {
            FacebookLikeParams facebookLikeParams = new FacebookLikeParams(43212, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", null);
            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.AddFacebookLike(facebookLikeParams, false, null, false);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams);
            AssertFacebookLike(facebookLikeParams, fbLikeListByMember);
            facebookLikeServiceBl.RemoveAllFacebookLikes(facebookLikeParams, false, null);
            System.Threading.Thread.Sleep(200);
        }

        [Test]
        public void TestProcessFacebookLikes()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(12346, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "telvision");
            FacebookLikeParams facebookLikeParams2 = new FacebookLikeParams(12346, 103, 10999903004, 41, "The Middle", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "telvision");
            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            AssertFacebookLike(facebookLikeParams1, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams2, fbLikeListByMember);
            fbParams.Remove(facebookLikeParams1);

            result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            AssertFacebookLike(facebookLikeParams2,fbLikeListByMember);
            facebookLikeServiceBl.RemoveAllFacebookLikes(fbParams.First(), false, null);
            System.Threading.Thread.Sleep(200);
        }

        [Test]
        public void TestAdminProcessFacebookLikes()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(1234567, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "telvision");
            FacebookLikeParams facebookLikeParams2 = new FacebookLikeParams(1234567, 103, 10999903004, 41, "The Middle", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "telvision");
            FacebookLikeParams facebookLikeParams3 = new FacebookLikeParams(1234567, 103, 10999903005, 76, "NBA", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/Nba", "sports");
            FacebookLikeParams facebookLikeParams4 = new FacebookLikeParams(1234567, 103, 10999903006, 76, "NFL", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nfl", "sports");
            FacebookLikeParams facebookLikeParams5 = new FacebookLikeParams(1234567, 103, 10999903007, 38, "Lost", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "television");
            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            fbParams.Add(facebookLikeParams3);
            fbParams.Add(facebookLikeParams4);
            fbParams.Add(facebookLikeParams5);

            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);

            facebookLikeParams1.AdminMemberId = 7654321;
            facebookLikeParams1.FbLikeStatus=FacebookLikeStatus.Approved;
            facebookLikeParams2.AdminMemberId = 7654321;
            facebookLikeParams2.FbLikeStatus = FacebookLikeStatus.Approved;
            facebookLikeParams3.AdminMemberId = 7654321;
            facebookLikeParams3.FbLikeStatus = FacebookLikeStatus.Rejected;
            
            fbParams.Clear();
            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            fbParams.Add(facebookLikeParams3);

            result = facebookLikeServiceBl.AdminProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            Assert.AreEqual(5, fbLikeListByMember.FacebookLikes.Count);
            AssertFacebookLike(facebookLikeParams1, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams2, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams3, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams4, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams5, fbLikeListByMember);
            
                        
            facebookLikeServiceBl.RemoveAllFacebookLikes(fbParams.First(), false, null);
            System.Threading.Thread.Sleep(200);
        }

        [Test]
        public void TestAdminProcessFacebookLikesByMember()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(12345678, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "telvision");
            FacebookLikeParams facebookLikeParams2 = new FacebookLikeParams(12345678, 103, 10999903004, 41, "The Middle", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "telvision");
            FacebookLikeParams facebookLikeParams3 = new FacebookLikeParams(12345678, 103, 10999903005, 76, "NBA", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/Nba", "sports");
            FacebookLikeParams facebookLikeParams4 = new FacebookLikeParams(12345678, 103, 10999903006, 76, "NFL", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nfl", "sports");
            FacebookLikeParams facebookLikeParams5 = new FacebookLikeParams(12345678, 103, 10999903007, 38, "Lost", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "television");
            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            fbParams.Add(facebookLikeParams3);
            fbParams.Add(facebookLikeParams4);
            fbParams.Add(facebookLikeParams5);

            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);

            facebookLikeParams1.AdminMemberId = 87654321;
            facebookLikeParams1.FbLikeStatus = FacebookLikeStatus.Approved;
            facebookLikeParams2.AdminMemberId = 87654321;
            facebookLikeParams2.FbLikeStatus = FacebookLikeStatus.Approved;
            facebookLikeParams3.AdminMemberId = 87654321;
            facebookLikeParams3.FbLikeStatus = FacebookLikeStatus.Rejected;

            fbParams.Clear();
            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            fbParams.Add(facebookLikeParams3);

            result = facebookLikeServiceBl.AdminProcessFacebookLikeByMember(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            Assert.AreEqual(3, fbLikeListByMember.FacebookLikes.Count);
            AssertFacebookLike(facebookLikeParams1, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams2, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams3, fbLikeListByMember);


            facebookLikeServiceBl.RemoveAllFacebookLikes(fbParams.First(), false, null);
            System.Threading.Thread.Sleep(200);
        }

        [Test]
        public void TestRemoveAllFacebookLikes()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(12347, 103, 10999903003, 41, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "television");
            FacebookLikeParams facebookLikeParams2 = new FacebookLikeParams(12347, 103, 10999903004, 41, "The Middle", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "television");
            FacebookLikeParams facebookLikeParams3 = new FacebookLikeParams(12347, 103, 10999903005, 61, "NBA", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/Nba", "sports");
            FacebookLikeParams facebookLikeParams4 = new FacebookLikeParams(12347, 103, 10999903006, 61, "NFL", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nfl", "sports");

            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            fbParams.Add(facebookLikeParams3);
            fbParams.Add(facebookLikeParams4);

            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            FacebookLikeList fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            AssertFacebookLike(facebookLikeParams1, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams2, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams3, fbLikeListByMember);
            AssertFacebookLike(facebookLikeParams4, fbLikeListByMember);

            result = facebookLikeServiceBl.RemoveAllFacebookLikes(fbParams.First(), false, null);
            System.Threading.Thread.Sleep(200);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            fbLikeListByMember = facebookLikeServiceBl.GetFBLikeListByMember(facebookLikeParams1);
            Assert.AreEqual(0,fbLikeListByMember.FacebookLikes.Count);
        }

        private static void AssertFacebookLike(FacebookLikeParams fbLikeParams, FacebookLikeList fbLikeListByMember)
        {
            ValueObjects.FacebookLike facebookLike = fbLikeListByMember.FacebookLikesDictionary[fbLikeParams.FacebookId];
            Assert.AreEqual(fbLikeParams.MemberId, facebookLike.MemberId);
            Assert.AreEqual(fbLikeParams.SiteId, facebookLike.SiteId);
            Assert.AreEqual(fbLikeParams.FacebookId, facebookLike.FacebookId);
            Assert.AreEqual(fbLikeParams.FbName, facebookLike.FacebookName);
            Assert.AreEqual(fbLikeParams.FbLinkHref, facebookLike.FacebookLinkHref);
            Assert.AreEqual(fbLikeParams.FbImageUrl, facebookLike.FacebookImageUrl);
            Assert.AreEqual(fbLikeParams.FbLikeCategoryId, facebookLike.FacebookLikeCategoryId);
            Assert.AreEqual(fbLikeParams.FbLikeStatus, facebookLike.FacebookLikeStatus);
            Assert.AreEqual(fbLikeParams.AdminMemberId, facebookLike.AdminMemberId);
        }

        [Test]
        public void TestGetRandomFacebookLikesForMember()
        {
            List<FacebookLikeParams> fbParams = new List<FacebookLikeParams>();
            FacebookLikeParams facebookLikeParams1 = new FacebookLikeParams(123478, 103, 10999903003, 38, "Modern Family", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "television");
            FacebookLikeParams facebookLikeParams2 = new FacebookLikeParams(123478, 103, 10999903004, 38, "The Middle", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "television");
            FacebookLikeParams facebookLikeParams3 = new FacebookLikeParams(123478, 103, 10999903005, 76, "NBA", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/Nba", "sports");
            FacebookLikeParams facebookLikeParams4 = new FacebookLikeParams(123478, 103, 10999903006, 76, "NFL", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nfl", "sports");            
            FacebookLikeParams facebookLikeParams5 = new FacebookLikeParams(123478, 103, 10999903007, 38, "Lost", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "television");
            FacebookLikeParams facebookLikeParams6 = new FacebookLikeParams(123478, 103, 10999903008, 38, "Alias", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "television");
            FacebookLikeParams facebookLikeParams7 = new FacebookLikeParams(123478, 103, 10999903009, 76, "MLB", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/mlb", "sports");
            FacebookLikeParams facebookLikeParams8 = new FacebookLikeParams(123478, 103, 10999903010, 76, "NHL", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nhl", "sports");
            FacebookLikeParams facebookLikeParams9 = new FacebookLikeParams(123478, 103, 10999903011, 11, "The Hunger Games", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "book");
            FacebookLikeParams facebookLikeParams10 = new FacebookLikeParams(123478, 103, 10999903012, 11, "Great Gatsby", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "book");
            FacebookLikeParams facebookLikeParams11 = new FacebookLikeParams(123478, 103, 10999903013, 21, "The Score", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/Nba", "album");
            FacebookLikeParams facebookLikeParams12 = new FacebookLikeParams(123478, 103, 10999903014, 21, "The White Album", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nfl", "album");
            FacebookLikeParams facebookLikeParams13 = new FacebookLikeParams(123478, 103, 10999903015, 33, "The Prestige", "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Modern_Family_Title.svg/250px-Modern_Family_Title.svg.png", "http://en.wikipedia.org/wiki/Modern_Family", "movie");
            FacebookLikeParams facebookLikeParams14 = new FacebookLikeParams(123478, 103, 10999903016, 34, "Martin Scorcese", "http://upload.wikimedia.org/wikipedia/en/thumb/2/28/Titlecard_for_The_Middle.png/250px-Titlecard_for_The_Middle.png", "http://en.wikipedia.org/wiki/The_Middle_(TV_series)", "Actor/Director");
            FacebookLikeParams facebookLikeParams15 = new FacebookLikeParams(123478, 103, 10999903017, 52, "Mario Batali", "http://upload.wikimedia.org/wikipedia/en/thumb/0/07/NBALogo.svg/100px-NBALogo.svg.png", "http://en.wikipedia.org/wiki/mlb", "chef");
            FacebookLikeParams facebookLikeParams16 = new FacebookLikeParams(123478, 103, 10999903018, 53, "Chris Rock", "http://upload.wikimedia.org/wikipedia/en/thumb/1/12/National_Football_League_2008.svg/150px-National_Football_League_2008.svg.png", "http://en.wikipedia.org/wiki/Nhl", "comedian");

            fbParams.Add(facebookLikeParams1);
            fbParams.Add(facebookLikeParams2);
            fbParams.Add(facebookLikeParams3);
            fbParams.Add(facebookLikeParams4);
            fbParams.Add(facebookLikeParams5);
            fbParams.Add(facebookLikeParams6);
            fbParams.Add(facebookLikeParams7);
            fbParams.Add(facebookLikeParams8);
            fbParams.Add(facebookLikeParams9);
            fbParams.Add(facebookLikeParams10);
            fbParams.Add(facebookLikeParams11);
            fbParams.Add(facebookLikeParams12);
            fbParams.Add(facebookLikeParams13);
            fbParams.Add(facebookLikeParams14);
            fbParams.Add(facebookLikeParams15);
            fbParams.Add(facebookLikeParams16);

            FacebookLikeServiceBL facebookLikeServiceBl = FacebookLike.BusinessLogic.FacebookLikeServiceBL.Instance;
            FacebookLikeSaveResult result = facebookLikeServiceBl.ProcessFacebookLikes(fbParams, false, null);
            Assert.AreEqual(FacebookLikeSaveStatusType.Success, result.SaveStatus);
            List<ValueObjects.FacebookLike> randomFacebookLikesListByMember = FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance.GetRandomFacebookLikesListByMember(facebookLikeParams1, 4, false);
            Assert.AreEqual(4, randomFacebookLikesListByMember.Count);

            randomFacebookLikesListByMember = FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance.GetRandomFacebookLikesListByMember(facebookLikeParams1, 0, false);
            Assert.Null(randomFacebookLikesListByMember);

            randomFacebookLikesListByMember = FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance.GetRandomFacebookLikesListByMember(facebookLikeParams1, 1, false);
            Assert.AreEqual(1, randomFacebookLikesListByMember.Count);

            randomFacebookLikesListByMember = FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance.GetRandomFacebookLikesListByMember(facebookLikeParams1, 8, false);
            Assert.AreEqual(8, randomFacebookLikesListByMember.Count);

            randomFacebookLikesListByMember = FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance.GetRandomFacebookLikesListByMember(facebookLikeParams1, 20, false);
            Assert.AreEqual(fbParams.Count, randomFacebookLikesListByMember.Count);

            facebookLikeServiceBl.RemoveAllFacebookLikes(fbParams.First(), false, null);
            System.Threading.Thread.Sleep(200);
        }

    }
}
