﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.RemotingServices;
using Matchnet.MemberLike.ServiceManagers;
using Spark.FacebookLike.ServiceManagers;

namespace Matchnet.MemberLike.Service
{
    public partial class MemberLikeService : RemotingServiceBase
    {
        private MemberLikeServiceSM m_memberLikeServiceSM;
        private FacebookLikeServiceSM m_facebookLikeServiceSM;

        public MemberLikeService()
        {
            InitializeComponent();
        }

        public static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new MemberLikeService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        protected override void RegisterServiceManagers()
        {
            System.Diagnostics.Trace.WriteLine("MemberLikeService, Start RegisterServiceManagers");

            m_memberLikeServiceSM = new MemberLikeServiceSM();
            base.RegisterServiceManager(m_memberLikeServiceSM);

            m_facebookLikeServiceSM = new FacebookLikeServiceSM();
            base.RegisterServiceManager(m_facebookLikeServiceSM);

            base.RegisterServiceManagers();
        }

        public static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string application_name = sender.ToString();
            Exception except = (Exception)e.ExceptionObject;
            string errormessage = "Application " + application_name + " [ Exception " + except.Message + " ]";
            System.Diagnostics.EventLog.WriteEntry(application_name, errormessage, EventLogEntryType.Error);
        }
    }
}
