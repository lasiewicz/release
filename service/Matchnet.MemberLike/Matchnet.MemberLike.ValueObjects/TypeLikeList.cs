﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberLike.ValueObjects
{
    /// <summary>
    /// Cacheable Container class of User Notification objects for a member
    /// </summary>
    [Serializable]
    public class TypeLikeList : IValueObject, ICacheable
    {
        List<MemberLikeObject> _TypeLikes = new List<MemberLikeObject>();

        int _LikeObjectId = Constants.NULL_INT;
        int _LikeTypeId = Constants.NULL_INT;
        int _SiteID = Constants.NULL_INT;

        #region Properties
        public List<MemberLikeObject> TypeLikes
        {
            get { return _TypeLikes; }
            set { _TypeLikes = value; }
        }

        public int LikeObjectId
        {
            get { return _LikeObjectId; }
            set { _LikeObjectId = value; }
        }

        public int LikeTypeId
        {
            get { return _LikeTypeId; }
            set { _LikeTypeId = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }
        #endregion


        #region ICacheable Members
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "TypeLikeList-{0}-{1}-{2}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_SiteID, _LikeObjectId, _LikeTypeId);
        }

        public static string GetCacheKeyString(int siteID, int likeObjectId, int likeTypeId)
        {
            return String.Format(CACHEKEYFORMAT, siteID, likeObjectId, likeTypeId);
        }

        #endregion

    }
}
