﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberLike.ValueObjects.ServiceDefinitions
{
    public interface IMemberLikeService
    {
        int AddMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId);
        int RemoveMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId);
        MemberLikeList GetLikeListByMember(MemberLikeParams memberLikeParams);
        MemberLikeList GetLikeListByMemberAndType(MemberLikeParams memberLikeParams, int likeObjectTypeId);
        TypeLikeList GetLikeListByObjectIdAndType(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId);
    }
}
