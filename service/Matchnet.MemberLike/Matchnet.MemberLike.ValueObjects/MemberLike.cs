﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberLike.ValueObjects
{
    [Serializable]
    public class MemberLikeParams
    {
        public int MemberId { get; set; }
        public int SiteId { get; set; }

        public static MemberLikeParams GetParamsObject(
            int memberId, 
            int siteId)
        {
            MemberLikeParams c = new MemberLikeParams();
            c.MemberId = memberId;
            c.SiteId = siteId;
            return c;
        }

        public override string ToString()
        {
            string format = "{{MemberId:{0}, SiteId:{1},}}";
            return string.Format(format, this.MemberId, this.SiteId);
        }
    }

    [Serializable]
    public class MemberLikeObject : IComparable, IEquatable<MemberLikeObject>, IReplicable, ICacheable
    {

        public MemberLikeObject() { }

        public MemberLikeObject(
            int memberId,
            int siteid,
            DateTime dateAdded,
            int likeObjectId,
            int likeObjectTypeId)
        {
            this.SiteId = siteid;
            this.MemberId = memberId;
            this.LikeObjectTypeId = likeObjectTypeId;
            this.LikeObjectId = likeObjectId;
            this.InsertDate = dateAdded;
        }

        public int MemberId {get; set;}
        public int SiteId { get; set; }
        public DateTime InsertDate {get; set;}
        public int LikeObjectId { get; set; }
        public int LikeObjectTypeId { get; set; }

        #region IComparable Members

        public Int32 CompareTo(object obj)
        {
            if (obj is MemberLikeObject)
            {
                MemberLikeObject ml = obj as MemberLikeObject;
                Int32 result = ml.InsertDate.CompareTo(this.InsertDate);
                if (result == 0)
                {
                    result = ml.MemberId.CompareTo(this.MemberId);
                    if (result == 0)
                    {
                        result = ml.LikeObjectTypeId.CompareTo(this.LikeObjectTypeId);
                        if (result == 0)
                        {
                            result = ml.LikeObjectId.CompareTo(this.LikeObjectId);
                        }
                    }
                }

                return result;
            }
            else
                throw new Exception("Compare object is not a MemberLike");
        }
        #endregion

        #region IEquatable<MemberLike> Members
        public bool Equals(MemberLikeObject other)
        {
            if (other is MemberLikeObject)
            {
                MemberLikeObject un = other as MemberLikeObject;
                string key1 = string.Format("{0}|{1}|{2}|{3}", un.MemberId, un.SiteId, un.LikeObjectId, un.LikeObjectTypeId);
                string key2 = string.Format("{0}|{1}|{2}|{3}", this.MemberId, this.SiteId, this.LikeObjectId, this.LikeObjectTypeId);
                return key2.Equals(key1);
            }
            return false;
        }
        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { throw new NotImplementedException(); }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string GetCacheKey()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ICacheable Members

        CacheItemMode ICacheable.CacheMode
        {
            get { throw new NotImplementedException(); }
        }

        CacheItemPriorityLevel ICacheable.CachePriority
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int ICacheable.CacheTTLSeconds
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string ICacheable.GetCacheKey()
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    [Serializable]
    public class LikeObjectType {
        public int LikeObjectTypeId { get; set; }
        public string TableName { get; set; }

        public LikeObjectType(int id, string tableName) {
            LikeObjectTypeId=id;
            TableName=tableName;
        }
    }
}
