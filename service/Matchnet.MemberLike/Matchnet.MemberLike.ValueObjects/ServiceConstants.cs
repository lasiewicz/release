﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberLike.ValueObjects
{
    public class ServiceConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_CONSTANT = "MEMBERLIKE_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.MemberLike.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "MemberLikeServiceSM";
        /// <summary>
        /// Performance counter name for total hits.
        /// </summary>
        public const string PERF_TOTALCOUNT_NAME = "Total hit count";

        public const string PREMIUMSTORE_LDB = "mnMemberLike";
    }
}
