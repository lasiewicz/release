﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MemberLike.ValueObjects.ReplicationAction
{
    [Serializable]
    public class MemberLikeReplicationAction : IReplicationAction
    {
        private MemberLikeObject _memberLikeObject;
        private MemberLikeActionType _actionType = MemberLikeActionType.Add;

        public MemberLikeReplicationAction() { }

        public MemberLikeObject MemberLike
        {
            get { return _memberLikeObject; }
            set { _memberLikeObject = value; }
        }

        public MemberLikeActionType ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }
    }

    [Serializable]
    public enum MemberLikeActionType
    {
        Add = 1,
        Remove = 2
    }

}
