﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Exceptions;
using Matchnet.Data;
using System.Data;
using System.Data.SqlClient;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.MemberLike.ValueObjects.ServiceDefinitions;
using Matchnet.MemberLike.ValueObjects.ReplicationAction;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects;

namespace Matchnet.MemberLike.BusinessLogic
{
    public class MemberLikeServiceBL : IMemberLikeService
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public readonly static MemberLikeServiceBL Instance = new MemberLikeServiceBL();
        private Matchnet.Caching.Cache _cache;
        private Dictionary<object,List<MemberLikeObject>> _likeObjectsByMember = new Dictionary<object,List<MemberLikeObject>>();
        private Dictionary<object, List<MemberLikeObject>> _likeObjectsByType = new Dictionary<object, List<MemberLikeObject>>();        
        const string MODULE_NAME = "MemberLikeServiceBL";

        private MemberLikeServiceBL() {
            System.Diagnostics.Trace.Write("MemberLikeServiceBL constructor.");
			_cache = Matchnet.Caching.Cache.Instance;
        }

        #region Replication
        /// <summary>
        /// This event handles the cache synchronization between service instances
        /// </summary>
        public delegate void ReplicationEventHandler(IReplicationAction replicationAction);
        public event ReplicationEventHandler ReplicationRequested;

        public void PlayReplicationAction(IReplicationAction replicationAction)
        {
            switch (replicationAction.GetType().Name)
            {
                case "MemberLikeReplicationAction":
                    MemberLikeReplicationAction memberLikeReplicationAction = replicationAction as MemberLikeReplicationAction;
                    PlayReplicatedAnswer(memberLikeReplicationAction);
                    break;
            }
        }

        private void ReplicationMemberLike(MemberLikeObject likeObject, MemberLikeActionType actionType)
        {
            MemberLikeReplicationAction replicationAction = new MemberLikeReplicationAction();
            replicationAction.ActionType = actionType;
            replicationAction.MemberLike = likeObject;
            ReplicationRequested(replicationAction);
        }

        private void PlayReplicatedAnswer(MemberLikeReplicationAction replicationAction)
        {
            if (replicationAction != null && replicationAction.MemberLike != null)
            {
                switch (replicationAction.ActionType)
                {
                    case MemberLikeActionType.Add:
                        AddMemberLikeToCache(replicationAction.MemberLike);
                        break;
                    case MemberLikeActionType.Remove:
                        RemoveMemberLikeFromCache(replicationAction.MemberLike);
                        break;
                }
            }
        }
        #endregion
        
        
        #region IIMemberLikeService Members
        public int AddMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            int count = 0;

            try
            {
                Command comm = new Command();
                comm.LogicalDatabaseName = Matchnet.MemberLike.ValueObjects.ServiceConstants.PREMIUMSTORE_LDB;
                comm.StoredProcedureName = "dbo.up_InsertMemberLikeObject";

                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.MemberId);
                comm.AddParameter("@LikeObjectID", SqlDbType.Int, ParameterDirection.Input, likeObjectId);
                comm.AddParameter("@LikeObjectTypeID", SqlDbType.Int, ParameterDirection.Input, likeObjectTypeId);
                comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.SiteId);

                Client.Instance.ExecuteAsyncWrite(comm);

                //Add to cache
                MemberLikeObject memberLike = new MemberLikeObject(memberLikeParams.MemberId,
                    memberLikeParams.SiteId,
                    DateTime.Now,
                    likeObjectId,
                    likeObjectTypeId);
                count = AddMemberLikeToCache(memberLike);
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "AddMemberLike failed for " + memberLikeParams.ToString() + "," + likeObjectId + "," + likeObjectTypeId, System.Diagnostics.EventLogEntryType.Error);
            }

            try
            {
                //activity recording
                if (likeObjectTypeId == Convert.ToInt32(LikeObjectTypes.Answer))
                {
                    ActivityRecordingSA.Instance.RecordActivity(
                        memberLikeParams.MemberId,
                        Constants.NULL_INT,
                        memberLikeParams.SiteId,
                        ActivityRecording.ValueObjects.Types.ActionType.LikeAnswer,
                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                        string.Format("<AnswerID>{0}</AnswerID>", likeObjectId));
                }
            }
            catch (Exception logEx)
            {
                string message = "ActivityRecording threw an exception: " + logEx.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, message, System.Diagnostics.EventLogEntryType.Error);
            }

            return count;
        }

        
        public int RemoveMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            int count = 0;
            try
            {
                Command comm = new Command();
                comm.LogicalDatabaseName = Matchnet.MemberLike.ValueObjects.ServiceConstants.PREMIUMSTORE_LDB;
                comm.StoredProcedureName = "dbo.up_RemoveMemberLikeObject";

                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.MemberId);
                comm.AddParameter("@LikeObjectID", SqlDbType.Int, ParameterDirection.Input, likeObjectId);
                comm.AddParameter("@LikeObjectTypeID", SqlDbType.Int, ParameterDirection.Input, likeObjectTypeId);
                comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.SiteId);

                Client.Instance.ExecuteAsyncWrite(comm);

                //Remove from cache
                MemberLikeObject memberLike = new MemberLikeObject(memberLikeParams.MemberId,
                    memberLikeParams.SiteId,
                    DateTime.Now,
                    likeObjectId,
                    likeObjectTypeId);
                count = RemoveMemberLikeFromCache(memberLike);
            }
            catch (Exception ex)
            {
                //log exception
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "RemoveMemberLike failed for " + memberLikeParams.ToString() + "," + likeObjectId + "," + likeObjectTypeId, System.Diagnostics.EventLogEntryType.Error);
            }

            try
            {
                //activity recording
                if (likeObjectTypeId == Convert.ToInt32(LikeObjectTypes.Answer))
                {
                    ActivityRecordingSA.Instance.RecordActivity(
                        memberLikeParams.MemberId,
                        Constants.NULL_INT,
                        memberLikeParams.SiteId,
                        ActivityRecording.ValueObjects.Types.ActionType.UnLikeAnswer,
                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                        string.Format("<AnswerID>{0}</AnswerID>", likeObjectId));
                }
            }
            catch (Exception logEx)
            {
                string message = "ActivityRecording threw an exception: " + logEx.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, message, System.Diagnostics.EventLogEntryType.Error);
            }

            return count;
        }
        
        public MemberLikeList GetLikeListByMember(MemberLikeParams memberLikeParams)
        {
            string key = MemberLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLikeParams.MemberId);
            MemberLikeList likeObjectsByMember = this._cache.Get(key) as MemberLikeList;            

            if (null == likeObjectsByMember)
            {
                likeObjectsByMember = new MemberLikeList();
                likeObjectsByMember.MemberID = memberLikeParams.MemberId;
                likeObjectsByMember.SiteID = memberLikeParams.SiteId;
                likeObjectsByMember.CacheTTLSeconds = GetCacheTTLSeconds();

                SqlDataReader dataReader = null;
                try
                {

                    Command comm = new Command();
                    comm.LogicalDatabaseName = Matchnet.MemberLike.ValueObjects.ServiceConstants.PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_GetMemberLikes_ByMemberID";

                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.MemberId);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.SiteId);
                    dataReader = Client.Instance.ExecuteReader(comm);

                    while (dataReader.Read())
                    {
                        MemberLikeObject likeObject= LoadMemberLikeObject(dataReader);
                        likeObjectsByMember.MemberLikes.Add(likeObject);
                    }

                    //update cache
                    if (likeObjectsByMember != null)
                        _cache.Add(likeObjectsByMember);

                }
                catch (Exception ex)
                {
                    //log exception
                    System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "GetListByMember failed for " + memberLikeParams.ToString(), System.Diagnostics.EventLogEntryType.Error);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Close();
                    }
                }
            }
            return likeObjectsByMember;
        }

        public MemberLikeList GetLikeListByMemberAndType(MemberLikeParams memberLikeParams, int likeObjectTypeId)
        {
            MemberLikeList likeObjectsByMember = GetLikeListByMember(memberLikeParams);

            List<MemberLikeObject> likeObjsByType = null;
            if (null != likeObjectsByMember)
            {
                likeObjsByType = likeObjectsByMember.MemberLikes.FindAll(delegate(MemberLikeObject likeObj)
                {
                    bool b = likeObj.LikeObjectTypeId == likeObjectTypeId;
                    return b;
                });
            }
            MemberLikeList mll= new MemberLikeList();
            mll.MemberLikes = likeObjsByType;
            return mll;
        }

        public TypeLikeList GetLikeListByObjectIdAndType(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            string key = TypeLikeList.GetCacheKeyString(memberLikeParams.SiteId, likeObjectId, likeObjectTypeId);
            TypeLikeList likeObjectsByType = this._cache.Get(key) as TypeLikeList;            

            if (null == likeObjectsByType)
            {
                likeObjectsByType = new TypeLikeList();
                likeObjectsByType.SiteID = memberLikeParams.SiteId;
                likeObjectsByType.LikeTypeId = likeObjectTypeId;
                likeObjectsByType.LikeObjectId = likeObjectId;
                likeObjectsByType.CacheTTLSeconds = GetCacheTTLSeconds();

                SqlDataReader dataReader = null;
                try
                {

                    Command comm = new Command();
                    comm.LogicalDatabaseName = Matchnet.MemberLike.ValueObjects.ServiceConstants.PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_GetMemberLikes_ByLikeObjectIDAndType";

                    comm.AddParameter("@LikeObjectID", SqlDbType.Int, ParameterDirection.Input, likeObjectId);
                    comm.AddParameter("@LikeObjectTypeID", SqlDbType.Int, ParameterDirection.Input, likeObjectTypeId);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberLikeParams.SiteId);
                    dataReader = Client.Instance.ExecuteReader(comm);

                    while (dataReader.Read())
                    {
                        MemberLikeObject likeObject = LoadMemberLikeObject(dataReader);
                        likeObjectsByType.TypeLikes.Add(likeObject);
                    }

                    //update cache
                    if (likeObjectsByType != null)
                        _cache.Add(likeObjectsByType);

                }
                catch (Exception ex)
                {
                    //log exception
                    System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "GetListForLikeObjectIDAndType failed for " + memberLikeParams.ToString() + "," + likeObjectId + "," + likeObjectTypeId, System.Diagnostics.EventLogEntryType.Error);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Close();
                    }
                }
            } 

            return likeObjectsByType;
        }

        #endregion

        #region private methods
        private int AddMemberLikeToCache(MemberLikeObject memberLike)
        {
            int count = 0;
            if (null != memberLike)
            {
                MemberLikeParams memberLikeParams = MemberLikeParams.GetParamsObject(memberLike.MemberId, memberLike.SiteId);

                MemberLikeList likeObjectsByMember = GetLikeListByMember(memberLikeParams);
                if (null != likeObjectsByMember)
                {
                    if (!likeObjectsByMember.MemberLikes.Contains(memberLike))
                    {
                        likeObjectsByMember.MemberLikes.Insert(0, memberLike);
                        this._cache.Remove(MemberLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLikeParams.MemberId));
                        this._cache.Add(likeObjectsByMember);
                    }
                }

                TypeLikeList likeObjectsByType = GetLikeListByObjectIdAndType(memberLikeParams, memberLike.LikeObjectId, memberLike.LikeObjectTypeId);
                if (null != likeObjectsByType)
                {
                    if (!likeObjectsByType.TypeLikes.Contains(memberLike))
                    {
                        likeObjectsByType.TypeLikes.Insert(0, memberLike);
                        this._cache.Remove(TypeLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLike.LikeObjectId, memberLike.LikeObjectTypeId));
                        this._cache.Add(likeObjectsByType);
                    }
                    count = likeObjectsByType.TypeLikes.Count;
                }
            }
            return count;
        }

        private int RemoveMemberLikeFromCache(MemberLikeObject memberLike)
        {
            int count = 0;
            if (null != memberLike)
            {
                MemberLikeParams memberLikeParams = MemberLikeParams.GetParamsObject(memberLike.MemberId, memberLike.SiteId);

                MemberLikeList likeObjectsByMember = GetLikeListByMember(memberLikeParams);
                if (null != likeObjectsByMember)
                {
                    if (likeObjectsByMember.MemberLikes.Contains(memberLike))
                    {
                        likeObjectsByMember.MemberLikes.Remove(memberLike);
                        this._cache.Remove(MemberLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLikeParams.MemberId));
                        this._cache.Add(likeObjectsByMember);
                    }
                }

                TypeLikeList likeObjectsByType = GetLikeListByObjectIdAndType(memberLikeParams, memberLike.LikeObjectId, memberLike.LikeObjectTypeId);
                if (null != likeObjectsByType)
                {
                    if (likeObjectsByType.TypeLikes.Contains(memberLike))
                    {
                        likeObjectsByType.TypeLikes.Remove(memberLike);
                        this._cache.Remove(TypeLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLike.LikeObjectId, memberLike.LikeObjectTypeId));
                        this._cache.Add(likeObjectsByType);
                    }
                    count = likeObjectsByType.TypeLikes.Count;
                }
            }
            return count;
        }

        private int GetCacheTTLSeconds()
        {
            int ttlSeconds = 300; //default to 5 minutes
            try
            {
                string str = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERLIKE_SVC_CACHE_TTL_SECONDS");
                if (!string.IsNullOrEmpty(str))
                {
                    ttlSeconds = Conversion.CInt(str);
                }
            }
            catch (Exception ex)
            {
                //return default seconds
            }
            return ttlSeconds;
        }

        private MemberLikeObject LoadMemberLikeObject(SqlDataReader dataReader)
        {
            MemberLikeObject memberLikeObject = new MemberLikeObject();
            memberLikeObject.MemberId = dataReader.IsDBNull(dataReader.GetOrdinal("MemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("MemberID"));
            memberLikeObject.SiteId = dataReader.IsDBNull(dataReader.GetOrdinal("SiteID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("SiteID"));
            memberLikeObject.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            memberLikeObject.LikeObjectId = dataReader.IsDBNull(dataReader.GetOrdinal("LikeObjectID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("LikeObjectID"));
            memberLikeObject.LikeObjectTypeId = dataReader.IsDBNull(dataReader.GetOrdinal("LikeObjectTypeID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("LikeObjectTypeID"));
            return memberLikeObject;
        }
        #endregion
    }

}
