﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Caching;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.MemberLike.ValueObjects.ServiceDefinitions;

namespace Matchnet.MemberLike.ServiceAdapters
{
    public class MemberLikeServiceSA : SABase
    {
        private Cache _cache;
        
        // ye ole' singleton instance
        public static readonly MemberLikeServiceSA Instance = new MemberLikeServiceSA();

        private MemberLikeServiceSA()
        {
            _cache = Cache.Instance;
        }

        private void WriteEventLog(string message)
        {
            EventLog.WriteEntry("WWW", message, EventLogEntryType.Error);
        }


        public int AddMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            string uri = string.Empty;
            int count = 0;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    count = getService(uri).AddMemberLike(memberLikeParams, likeObjectId,likeObjectTypeId);
                    _cache.Remove(MemberLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLikeParams.MemberId));
                    _cache.Remove(TypeLikeList.GetCacheKeyString(memberLikeParams.SiteId, likeObjectId, likeObjectTypeId));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("AddMemberLike() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, memberLikeParams.ToString())));
            }
            return count;
        }

        public int RemoveMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            string uri = string.Empty;
            int count = 0;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    count = getService(uri).RemoveMemberLike(memberLikeParams, likeObjectId, likeObjectTypeId);
                    _cache.Remove(MemberLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLikeParams.MemberId));
                    _cache.Remove(TypeLikeList.GetCacheKeyString(memberLikeParams.SiteId, likeObjectId, likeObjectTypeId));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RemoveMemberLike() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, memberLikeParams.ToString())));
            }
            return count;
        }

        public MemberLikeList GetLikeListByMember(MemberLikeParams memberLikeParams)
        {
            MemberLikeList memberLikes = null;
            string uri = string.Empty;
            try
            {
                //check cache
                memberLikes = _cache.Get(MemberLikeList.GetCacheKeyString(memberLikeParams.SiteId, memberLikeParams.MemberId)) as MemberLikeList;

                if (null == memberLikes)
                {
                    uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        memberLikes = getService(uri).GetLikeListByMember(memberLikeParams);
                        if (null != memberLikes)
                        {
                            _cache.Add(memberLikes);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetLikeListByMember() error (uri: {0}, exception:{1}, params:{2})", uri, ex, memberLikeParams.ToString())));
            }
            return memberLikes;
        }

        public MemberLikeList GetLikeListByMemberAndType(MemberLikeParams memberLikeParams, int likeObjectTypeId)
        {
            MemberLikeList memberLikes = null;
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    memberLikes = getService(uri).GetLikeListByMemberAndType(memberLikeParams, likeObjectTypeId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetLikeListByMemberAndType() error (uri: {0}, exception:{1}, params:{2})", uri, ex, memberLikeParams.ToString())));
            }
            return memberLikes;
        }

        public TypeLikeList GetLikeListByObjectIdAndType(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            TypeLikeList typeLikes = null;
            string uri = string.Empty;
            try
            {
                //check cache
                typeLikes = _cache.Get(TypeLikeList.GetCacheKeyString(memberLikeParams.SiteId, likeObjectId, likeObjectTypeId)) as TypeLikeList;

                //update cache if: notifications is null or if we need to set notifications as viewed
                if (null == typeLikes)
                {
                    uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        typeLikes = getService(uri).GetLikeListByObjectIdAndType(memberLikeParams, likeObjectId, likeObjectTypeId);
                        if (null != typeLikes)
                        {
                            _cache.Add(typeLikes);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetLikeListByObjectIdAndType() error (uri: {0}, exception:{1}, params:{2})", uri, ex, memberLikeParams.ToString())));
            }
            return typeLikes;
        }

        public int GetCountOfLikeListByMember(MemberLikeParams memberLikeParams)
        {
            int count = 0;
            List<MemberLikeObject> likes = GetLikeListByMember(memberLikeParams).MemberLikes;
            if (null != likes) count = likes.Count;
            return count;
        }

        public int GetCountOfLikeListByObjectIdAndType(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            int count = 0;
            List<MemberLikeObject> likes = GetLikeListByObjectIdAndType(memberLikeParams, likeObjectId, likeObjectTypeId).TypeLikes;
            if (null != likes) count = likes.Count;
            return count;            
        }


        private IMemberLikeService getService(string uri)
        {
            try
            {
                return (IMemberLikeService)Activator.GetObject(typeof(IMemberLikeService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        private string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERLIKE_SVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERLIKE_SVC_SA_CONNECTION_LIMIT"));
        }

    }
}
