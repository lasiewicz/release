﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data;
using Spark.FacebookLike.ValueObjects;
using Spark.FacebookLike.ValueObjects.ReplicationAction;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;


namespace Spark.FacebookLike.BusinessLogic
{
    public class FacebookLikeServiceBL
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public readonly static FacebookLikeServiceBL Instance = new FacebookLikeServiceBL();
        private Matchnet.Caching.Cache _cache;
        const string MODULE_NAME = "FacebookLikeServiceBL";
        /// <summary>
        /// This event handles the cache synchronization between service instances
        /// </summary>
        public delegate void ReplicationEventHandler(IReplicationAction replicationAction);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        public event SynchronizationEventHandler SynchronizationRequested;

        private ISettingsSA _settingsService = null;
        private ISettingsSA SettingsService
        {
            set { _settingsService = value; }
            get
            {
                if (null == _settingsService)
                {
                    return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.Instance;
                }
                return _settingsService;
            }
        }

        private FacebookLikeServiceBL()
        {
            System.Diagnostics.Trace.Write("FacebookLikeServiceBL constructor.");
			_cache = Matchnet.Caching.Cache.Instance;
        }

        #region Replication
        public void PlayReplicationAction(IReplicationAction replicationAction)
        {
            switch (replicationAction.GetType().Name)
            {
                case "FacebookLikeReplicationAction":
                    FacebookLikeReplicationAction facebookLikeReplicationAction = replicationAction as FacebookLikeReplicationAction;
                    PlayReplicatedFacebookLike(facebookLikeReplicationAction);
                    break;
            }
        }

        private void PlayReplicatedFacebookLike(FacebookLikeReplicationAction replicationAction)
        {
            if (replicationAction != null && replicationAction.FacebookLikeList != null)
            {
                if (!string.IsNullOrEmpty(replicationAction.ClientHostName))
                {
                    //need to perform sync to current clients of this service instance before we update cache from replication
                    FacebookLikeList existingFacebookListList = this._cache.Get(replicationAction.FacebookLikeList.GetCacheKey()) as FacebookLikeList;
                    if (existingFacebookListList != null)
                    {
                        SynchronizationRequested(existingFacebookListList.GetCacheKey(), existingFacebookListList.ReferenceTracker.Purge(replicationAction.ClientHostName));
                    }
                }

                switch (replicationAction.ActionType)
                {
                    case FacebookLikeActionType.Update:
                        UpdateFacebookLikeListInCache(replicationAction.FacebookLikeList);
                        break;
                    case FacebookLikeActionType.RemoveAll:
                        RemoveAllFacebookLikesFromCache(replicationAction.FacebookLikeList);
                        break;
                }

            }
        }

        private void ReplicationFacebookLikeList(FacebookLikeList fbLikeList, FacebookLikeActionType actionType, string clientHostName)
        {
            FacebookLikeReplicationAction replicationAction = new FacebookLikeReplicationAction();
            replicationAction.ActionType = actionType;
            replicationAction.FacebookLikeList = fbLikeList;
            replicationAction.ClientHostName = clientHostName;
            ReplicationRequested(replicationAction);
        }
        #endregion

        #region private cache methods
        private FacebookLikeList AddFacebookLikeToCache(FacebookLike.ValueObjects.FacebookLike facebookLike)
        {
            if (null != facebookLike)
            {
                FacebookLikeParams fbLikeParams = FacebookLikeParams.ParseFromFacebookLike(facebookLike);

                FacebookLikeList fbLikesByMember = GetFBLikeListByMember(fbLikeParams);
                if (null == fbLikesByMember)
                {
                    fbLikesByMember = new FacebookLikeList();
                    fbLikesByMember.MemberID = facebookLike.MemberId;
                    fbLikesByMember.SiteID = facebookLike.SiteId;
                }

                if (!fbLikesByMember.FacebookLikesDictionary.ContainsKey(facebookLike.FacebookId))
                {
                    fbLikesByMember.FacebookLikes.Insert(0, facebookLike);
                    fbLikesByMember.FacebookLikesDictionary.Add(facebookLike.FacebookId, facebookLike);
                    List<ValueObjects.FacebookLike> fbLikes = null;
                    if (fbLikesByMember.FacebookLikesGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryGroupId))
                    {
                        fbLikes = fbLikesByMember.FacebookLikesGroupDictionary[facebookLike.FacebookLikeCategoryGroupId];
                    }
                    else
                    {
                        fbLikes = new List<ValueObjects.FacebookLike>();
                        fbLikesByMember.FacebookLikesGroupDictionary.Add(facebookLike.FacebookLikeCategoryGroupId, fbLikes);
                    }
                    if(!fbLikes.Contains(facebookLike)) fbLikes.Add(facebookLike);

                    this._cache.Insert(fbLikesByMember);
                }
                return fbLikesByMember;
            }
            return null;
        }

        private FacebookLikeList UpdateFacebookLikeInCache(FacebookLikeParams fbLikeParams, ValueObjects.FacebookLike facebookLike)
        {
            FacebookLikeList facebookLikeList= null;
            if (null != facebookLike)
            {
                facebookLikeList = GetFBLikeListByMember(fbLikeParams);
                if (null != facebookLikeList)
                {
                    if (null != facebookLikeList.FacebookLikes)
                    {
                        if (facebookLikeList.FacebookLikes.Contains(facebookLike))
                        {
                            facebookLikeList.FacebookLikes.Remove(facebookLike);
                        }
                        facebookLikeList.FacebookLikes.Add(facebookLike);
                    }
                    if (null != facebookLikeList.FacebookLikesDictionary)
                    {
                        if (facebookLikeList.FacebookLikesDictionary.ContainsKey(facebookLike.FacebookId))
                        {
                            facebookLikeList.FacebookLikesDictionary.Remove(facebookLike.FacebookId);
                        }
                        facebookLikeList.FacebookLikesDictionary.Add(facebookLike.FacebookId, facebookLike);
                    }
                    if (null != facebookLikeList.FacebookLikesGroupDictionary)
                    {
                        if (facebookLikeList.FacebookLikesGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryGroupId))
                        {
                            List<ValueObjects.FacebookLike> fbLikes = facebookLikeList.FacebookLikesGroupDictionary[facebookLike.FacebookLikeCategoryGroupId];
                            if (null != fbLikes)
                            {
                                if (fbLikes.Contains(facebookLike)) fbLikes.Remove(facebookLike);
                                fbLikes.Add(facebookLike);
                            }
                        }
                    }
                }
            }
            return facebookLikeList;
        }


        private FacebookLikeList RemoveFacebookLikeFromCache(FacebookLike.ValueObjects.FacebookLike facebookLike)
        {
            if (null != facebookLike)
            {
                FacebookLikeParams fbLikeParams = FacebookLikeParams.ParseFromFacebookLike(facebookLike);

                FacebookLikeList fbLikesByMember = GetFBLikeListByMember(fbLikeParams);
                if (null != fbLikesByMember)
                {
                    if (fbLikesByMember.FacebookLikesDictionary.ContainsKey(facebookLike.FacebookId))
                    {
                        fbLikesByMember.FacebookLikes.Remove(facebookLike);
                        fbLikesByMember.FacebookLikesDictionary.Remove(facebookLike.FacebookId);
                        if (fbLikesByMember.FacebookLikesGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryGroupId))
                        {
                            List<ValueObjects.FacebookLike> fbLikes = fbLikesByMember.FacebookLikesGroupDictionary[facebookLike.FacebookLikeCategoryGroupId];
                            if (fbLikes.Contains(facebookLike)) fbLikes.Remove(facebookLike);
                        }
                        this._cache.Insert(fbLikesByMember);
                    }
                }
                return fbLikesByMember;
            }
            return null;
        }

        private void RemoveAllFacebookLikesFromCache(FacebookLikeList facebookLikeList)
        {
            if (null != facebookLikeList)
            {
                string cacheKeyString = facebookLikeList.GetCacheKey();
                if (null != this._cache.Get(cacheKeyString))
                {
                    this._cache.Remove(cacheKeyString);
                }
            }
        }

        private void UpdateFacebookLikeListInCache(FacebookLikeList facebookLikeList)
        {
            if (null != facebookLikeList)
            {                
                this._cache.Insert(facebookLikeList);
            }
        }

        private int GetCacheTTLSeconds(string settingKey)
        {
            int ttlSeconds = 300; //default to 5 minutes
            try
            {
                string str = SettingsService.GetSettingFromSingleton(settingKey);
                if (!string.IsNullOrEmpty(str))
                {
                    ttlSeconds = Conversion.CInt(str);
                }
            }
            catch (Exception ex)
            {
                //return default seconds
            }
            return ttlSeconds;
        }

        private ValueObjects.FacebookLike LoadFacebookLikeFromDB(SqlDataReader dataReader)
        {
            ValueObjects.FacebookLike facebookLike = new ValueObjects.FacebookLike();
            facebookLike.MemberId = dataReader.IsDBNull(dataReader.GetOrdinal("MemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("MemberID"));
            facebookLike.SiteId = dataReader.IsDBNull(dataReader.GetOrdinal("SiteID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("SiteID"));
            facebookLike.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            facebookLike.UpdateTime = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            facebookLike.FacebookId = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookID")) ? long.MinValue : dataReader.GetInt64(dataReader.GetOrdinal("FacebookID"));
            facebookLike.FacebookImageUrl = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookImageUrl")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("FacebookImageUrl"));
            facebookLike.FacebookLinkHref = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookLinkHref")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("FacebookLinkHref"));
            facebookLike.FacebookName = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookName")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("FacebookName"));
            facebookLike.FacebookType = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookType")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("FacebookType"));
            int fbLikeStatusID = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookLikeStatusID")) ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("FacebookLikeStatusID"));
            facebookLike.FacebookLikeStatus = (FacebookLikeStatus) Enum.Parse(typeof(FacebookLikeStatus), fbLikeStatusID.ToString());
            facebookLike.FacebookLikeCategoryId = dataReader.IsDBNull(dataReader.GetOrdinal("FacebookLikeCategoryId")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("FacebookLikeCategoryId"));
            facebookLike.FacebookLikeCategoryGroupId = dataReader.IsDBNull(dataReader.GetOrdinal("CategoryGroupID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CategoryGroupID"));
            if(!dataReader.IsDBNull(dataReader.GetOrdinal("AdminMemberID")))
            {
                facebookLike.AdminMemberId = dataReader.GetInt32(dataReader.GetOrdinal("AdminMemberID"));
            }
            return facebookLike;
        }

        private void LoadFacebookLikeCategoryGroupFromDB(SqlDataReader dataReader, FacebookLikeCategoryGroupList fbCategoryGroupList)
        {
            ValueObjects.FacebookLikeCategory fbLikeCategory = new FacebookLikeCategory();
            fbLikeCategory.FacebookLikeCategoryId = dataReader.IsDBNull(dataReader.GetOrdinal("CategoryID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CategoryID"));
            fbLikeCategory.FacebookLikeCategoryName = dataReader.IsDBNull(dataReader.GetOrdinal("CategoryName")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("CategoryName"));
            int fbLikeCategoryGroupID = dataReader.IsDBNull(dataReader.GetOrdinal("CategoryGroupID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("CategoryGroupID"));
            FacebookLikeCategoryGroup fbLikeCategoryGroup = null;
            foreach (FacebookLikeCategoryGroup group in fbCategoryGroupList.FacebookLikeCategoryGroups)
            {
                if (group.FacebookLikeCategoryGroupId == fbLikeCategoryGroupID)
                {
                    fbLikeCategoryGroup = group;
                    break;
                }
            }
            if (null == fbLikeCategoryGroup)
            {
                fbLikeCategoryGroup = new FacebookLikeCategoryGroup();
                fbLikeCategoryGroup.FacebookLikeCategoryGroupId = fbLikeCategoryGroupID;
                fbLikeCategoryGroup.FacebookCategoryGroupName = dataReader.IsDBNull(dataReader.GetOrdinal("CategoryGroupName")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("CategoryGroupName"));
                fbLikeCategoryGroup.FacebookCategoryGroupNameResourceKey = dataReader.IsDBNull(dataReader.GetOrdinal("CategoryResourceKey")) ? string.Empty : dataReader.GetString(dataReader.GetOrdinal("CategoryResourceKey"));
                fbCategoryGroupList.FacebookLikeCategoryGroups.Add(fbLikeCategoryGroup);
            }
            fbLikeCategoryGroup.AddFacebookLikeCategory(fbLikeCategory);
            //setup dictionary
            if (!fbCategoryGroupList.FacebookLikeCategoryGroupDictionary.ContainsKey(fbLikeCategory.FacebookLikeCategoryId))
            {
                fbCategoryGroupList.FacebookLikeCategoryGroupDictionary.Add(fbLikeCategory.FacebookLikeCategoryId, fbLikeCategoryGroup);
            }
        }        
        #endregion

        #region admin methods
        private void ApprovalEnqueue(FacebookLikeParams fbLikeParams)
        {

            try
            {
                Sites sites = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();

                Site site = (Site)(from Site s in sites where s.SiteID == fbLikeParams.SiteId select s).ToArray()[0];
                QueueItemFacebookLike queueItem = new QueueItemFacebookLike();
                queueItem.CommunityID = site.Community.CommunityID;
                queueItem.SiteID = fbLikeParams.SiteId;
                queueItem.BrandID = Constants.NULL_INT;
                queueItem.MemberID = fbLikeParams.MemberId;
                queueItem.InsertDate = DateTime.Now;
                queueItem.AdminMemberId = Constants.NULL_INT;

                DBApproveQueueSA.Instance.Enqueue(queueItem);
            }
            catch (Exception ex)
            {
                //log exception
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "ApprovalEnqueue() failed: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }
        #endregion

        public FacebookLikeCategoryGroupList GetFacebookLikeCategoryGroupList()
        {
            string key = FacebookLikeCategoryGroupList.GetCacheKeyString();
            FacebookLikeCategoryGroupList fbCategoryGroupList = this._cache.Get(key) as FacebookLikeCategoryGroupList;
            if (null == fbCategoryGroupList)
            {
                fbCategoryGroupList = new FacebookLikeCategoryGroupList();
                fbCategoryGroupList.CacheTTLSeconds = GetCacheTTLSeconds("FACEBOOKLIKE_CATEGORY_GROUPS_SVC_CACHE_TTL_SECONDS");

                SqlDataReader dataReader = null;
                try
                {
                    Command comm = new Command();
                    comm.LogicalDatabaseName = ServiceConstants.FACEBOOKLIKESTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_GetFacebookLikeCategoryGroups";
                    comm.Key = new Random().Next();
                    dataReader = Client.Instance.ExecuteReader(comm);

                    while (dataReader.Read())
                    {
                        LoadFacebookLikeCategoryGroupFromDB(dataReader, fbCategoryGroupList);
                    }

                    //update cache
                    if (fbCategoryGroupList != null) _cache.Insert(fbCategoryGroupList);

                }
                catch (Exception ex)
                {
                    //log exception
                    System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "GetFacebookLikeCategoryGroupList() failed: "+ ex.Message, System.Diagnostics.EventLogEntryType.Error);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Close();
                    }
                }
            }
            return fbCategoryGroupList;
        }

        public FacebookLikeList GetFBLikeListByMember(FacebookLikeParams fbLikeParams)
        {
            return GetFBLikeListByMember(fbLikeParams, null, null);
        }

        public FacebookLikeList GetFBLikeListByMember(FacebookLikeParams fbLikeParams, string clientHostName, CacheReference cacheReference)
        {
            FacebookLikeList fbLikesByMember=null;
            if (fbLikeParams.MemberId > 0 && fbLikeParams.SiteId > 0)
            {
                string key = FacebookLikeList.GetCacheKeyString(fbLikeParams.SiteId, fbLikeParams.MemberId);
                fbLikesByMember = this._cache.Get(key) as FacebookLikeList;

                if (null == fbLikesByMember)
                {
                    fbLikesByMember = new FacebookLikeList();
                    fbLikesByMember.MemberID = fbLikeParams.MemberId;
                    fbLikesByMember.SiteID = fbLikeParams.SiteId;
                    fbLikesByMember.CacheTTLSeconds = GetCacheTTLSeconds("FACEBOOKLIKES_SVC_CACHE_TTL_SECONDS");

                    SqlDataReader dataReader = null;
                    try
                    {

                        Command comm = new Command();
                        comm.LogicalDatabaseName = ServiceConstants.FACEBOOKLIKESTORE_LDB;
                        comm.StoredProcedureName = "dbo.up_GetFacebookLikes_ByMemberID";
                        comm.Key = fbLikeParams.MemberId;

                        comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.MemberId);
                        comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.SiteId);
                        dataReader = Client.Instance.ExecuteReader(comm);

                        while (dataReader.Read())
                        {
                            ValueObjects.FacebookLike facebookLike = LoadFacebookLikeFromDB(dataReader);
                            fbLikesByMember.FacebookLikes.Add(facebookLike);
                            fbLikesByMember.FacebookLikesDictionary.Add(facebookLike.FacebookId, facebookLike);
                            List<ValueObjects.FacebookLike> fbLikes = null;
                            if (fbLikesByMember.FacebookLikesGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryGroupId))
                            {
                                fbLikes = fbLikesByMember.FacebookLikesGroupDictionary[facebookLike.FacebookLikeCategoryGroupId];
                            }
                            else
                            {
                                fbLikes = new List<ValueObjects.FacebookLike>();
                                fbLikesByMember.FacebookLikesGroupDictionary.Add(facebookLike.FacebookLikeCategoryGroupId, fbLikes);
                            }
                            fbLikes.Add(facebookLike);
                        }

                        //update cache
                        if (fbLikesByMember != null)
                        {
                            _cache.Insert(fbLikesByMember);
                        }

                    }
                    catch (Exception ex)
                    {
                        //log exception
                        System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "GetFBLikeListByMember failed for " + fbLikeParams.ToString() + ": " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                    }
                    finally
                    {
                        if (dataReader != null)
                        {
                            dataReader.Close();
                        }
                    }
                }
            }

            if (null != fbLikesByMember && null != clientHostName && null != cacheReference)
            {
                fbLikesByMember.ReferenceTracker.Add(clientHostName, cacheReference);
            }

            return fbLikesByMember;
        }

        public FacebookLikeSaveResult AddFacebookLike(FacebookLikeParams fbLikeParams, bool replicate, string clientHostName, bool enqueue)
        {
            FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Success, fbLikeParams.MemberId);
            try
            {
                List<string> invalidParameters = Validate(fbLikeParams);
                if (null == invalidParameters)
                {
                    Command comm = new Command();
                    comm.LogicalDatabaseName = ServiceConstants.FACEBOOKLIKESTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_InsertFacebookLike";
                    comm.Key = fbLikeParams.MemberId;

                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.MemberId);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.SiteId);
                    comm.AddParameter("@FacebookID", SqlDbType.BigInt, ParameterDirection.Input, fbLikeParams.FacebookId);
                    comm.AddParameter("@FacebookLikeCategoryId", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.FbLikeCategoryId);
                    comm.AddParameter("@FacebookLikeImageUrl", SqlDbType.NVarChar, ParameterDirection.Input, fbLikeParams.FbImageUrl);
                    comm.AddParameter("@FacebookLikeLinkHref", SqlDbType.NVarChar, ParameterDirection.Input, fbLikeParams.FbLinkHref);
                    comm.AddParameter("@FacebookLikeType", SqlDbType.NVarChar, ParameterDirection.Input, fbLikeParams.FbType);
                    comm.AddParameter("@FacebookLikeName", SqlDbType.NVarChar, ParameterDirection.Input, fbLikeParams.FbName);
                    comm.AddParameter("@FacebookLikeStatusID", SqlDbType.Int, ParameterDirection.Input, ((Int32)fbLikeParams.FbLikeStatus));

                    Client.Instance.ExecuteAsyncWrite(comm);

                    //Add to cache
                    ValueObjects.FacebookLike facebookLike = ValueObjects.FacebookLike.ParseFromFacebookLikeParams(fbLikeParams);
                    FacebookLikeCategoryGroupList categoryGroupList = GetFacebookLikeCategoryGroupList();
                    if (categoryGroupList.FacebookLikeCategoryGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryId))
                    {
                        FacebookLikeCategoryGroup facebookLikeCategoryGroup = categoryGroupList.FacebookLikeCategoryGroupDictionary[facebookLike.FacebookLikeCategoryId];
                        facebookLike.FacebookLikeCategoryGroupId = facebookLikeCategoryGroup.FacebookLikeCategoryGroupId;
                    }
                    FacebookLikeList facebookLikeList = AddFacebookLikeToCache(facebookLike);
                    facebookLikeSaveResult.SavedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                    if (replicate)
                    {
                        facebookLikeSaveResult.FacebookLikeList = facebookLikeList;
                        //replicate to other service instances
                        ReplicationFacebookLikeList(facebookLikeList, FacebookLikeActionType.Update, clientHostName);
                    }

                    if (!string.IsNullOrEmpty(clientHostName) && null != facebookLikeList)
                    {
                        SynchronizationRequested(facebookLikeList.GetCacheKey(), facebookLikeList.ReferenceTracker.Purge(clientHostName));
                    }

                    if (enqueue)
                    {
                        ApprovalEnqueue(fbLikeParams);
                    }
                }
                else
                {
                    facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.InvalidParameters;
                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                    facebookLikeSaveResult.InvalidParameters.Add(fbLikeParams.FacebookId, invalidParameters);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "AddFacebookLike failed for " + fbLikeParams.ToString() + ": " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.Failed;
                facebookLikeSaveResult.FailedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                return facebookLikeSaveResult;
            }

//            try
//            {
//                //activity recording
//                if (likeObjectTypeId == Convert.ToInt32(LikeObjectTypes.Answer))
//                {
//                    ActivityRecordingSA.Instance.RecordActivity(
//                        fbLikeParams.MemberId,
//                        Constants.NULL_INT,
//                        fbLikeParams.SiteId,
//                        ActivityRecording.ValueObjects.Types.ActionType.LikeAnswer,
//                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
//                        string.Format("<AnswerID>{0}</AnswerID>", likeObjectId));
//                }
//            }
//            catch (Exception logEx)
//            {
//                string message = "ActivityRecording threw an exception: " + logEx.Message;
//                System.Diagnostics.EventLog.WriteEntry(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, message, System.Diagnostics.EventLogEntryType.Error);
//            }

            return facebookLikeSaveResult;
        }

        public FacebookLikeSaveResult RemoveFacebookLike(FacebookLikeParams fbLikeParams, bool replicate, string clientHostName)
        {
            FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Success, fbLikeParams.MemberId);
            try
            {
                List<string> invalidParameters = Validate(fbLikeParams);
                if (null == invalidParameters)
                {
                    Command comm = new Command();
                    comm.LogicalDatabaseName = ServiceConstants.FACEBOOKLIKESTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_RemoveFacebookLike";
                    comm.Key = fbLikeParams.MemberId;

                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.MemberId);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.SiteId);
                    comm.AddParameter("@FacebookID", SqlDbType.BigInt, ParameterDirection.Input, fbLikeParams.FacebookId);

                    Client.Instance.ExecuteAsyncWrite(comm);

                    //Remove from cache
                    ValueObjects.FacebookLike facebookLike = ValueObjects.FacebookLike.ParseFromFacebookLikeParams(fbLikeParams);
                    FacebookLikeCategoryGroupList categoryGroupList = GetFacebookLikeCategoryGroupList();
                    if (categoryGroupList.FacebookLikeCategoryGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryId))
                    {
                        FacebookLikeCategoryGroup facebookLikeCategoryGroup = categoryGroupList.FacebookLikeCategoryGroupDictionary[facebookLike.FacebookLikeCategoryId];
                        facebookLike.FacebookLikeCategoryGroupId = facebookLikeCategoryGroup.FacebookLikeCategoryGroupId;
                    }

                    FacebookLikeList facebookLikeList = RemoveFacebookLikeFromCache(facebookLike);
                    if (replicate)
                    {
                        facebookLikeSaveResult.FacebookLikeList = facebookLikeList;
                        //replicate to other service instances
                        ReplicationFacebookLikeList(facebookLikeList, FacebookLikeActionType.Update, clientHostName);
                    }

                    if (!string.IsNullOrEmpty(clientHostName) && null != facebookLikeList)
                    {
                        SynchronizationRequested(facebookLikeList.GetCacheKey(),
                                                 facebookLikeList.ReferenceTracker.Purge(clientHostName));
                    }
                }
                else
                {
                    facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.InvalidParameters;
                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                    facebookLikeSaveResult.InvalidParameters.Add(fbLikeParams.FacebookId,invalidParameters);
                }
            }
            catch (Exception ex)
            {
                //log exception
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "RemoveMemberLike failed for " + fbLikeParams.ToString() +": "+ex.Message, System.Diagnostics.EventLogEntryType.Error);
                facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.Failed;
                facebookLikeSaveResult.FailedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                return facebookLikeSaveResult;                
            }

//            try
//            {
//                //activity recording
//                if (likeObjectTypeId == Convert.ToInt32(LikeObjectTypes.Answer))
//                {
//                    ActivityRecordingSA.Instance.RecordActivity(
//                        fbLikeParams.MemberId,
//                        Constants.NULL_INT,
//                        fbLikeParams.SiteId,
//                        ActivityRecording.ValueObjects.Types.ActionType.UnLikeAnswer,
//                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
//                        string.Format("<AnswerID>{0}</AnswerID>", likeObjectId));
//                }
//            }
//            catch (Exception logEx)
//            {
//                string message = "ActivityRecording threw an exception: " + logEx.Message;
//                System.Diagnostics.EventLog.WriteEntry(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, message, System.Diagnostics.EventLogEntryType.Error);
//            }

            return facebookLikeSaveResult;
        }

        public FacebookLikeSaveResult UpdateFacebookLike(FacebookLikeParams fbLikeParams, bool replicate, string clientHostName)
        {
            FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Success, fbLikeParams.MemberId);
            try
            {
                List<string> invalidParameters = Validate(fbLikeParams);
                if (null == invalidParameters)
                {
                    Command comm = new Command();
                    comm.LogicalDatabaseName = ServiceConstants.FACEBOOKLIKESTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_UpdateFacebookLike";
                    comm.Key = fbLikeParams.MemberId;

                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.MemberId);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.SiteId);
                    comm.AddParameter("@FacebookID", SqlDbType.BigInt, ParameterDirection.Input, fbLikeParams.FacebookId);
                    comm.AddParameter("@FacebookLikeStatusID", SqlDbType.Int, ParameterDirection.Input, ((Int32)fbLikeParams.FbLikeStatus));
                    comm.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.AdminMemberId);

                    Client.Instance.ExecuteAsyncWrite(comm);

                    //update cache
                    ValueObjects.FacebookLike facebookLike = ValueObjects.FacebookLike.ParseFromFacebookLikeParams(fbLikeParams);
                    FacebookLikeCategoryGroupList categoryGroupList = GetFacebookLikeCategoryGroupList();
                    if (categoryGroupList.FacebookLikeCategoryGroupDictionary.ContainsKey(facebookLike.FacebookLikeCategoryId))
                    {
                        FacebookLikeCategoryGroup facebookLikeCategoryGroup = categoryGroupList.FacebookLikeCategoryGroupDictionary[facebookLike.FacebookLikeCategoryId];
                        facebookLike.FacebookLikeCategoryGroupId = facebookLikeCategoryGroup.FacebookLikeCategoryGroupId;
                    }
                    FacebookLikeList facebookLikeList = UpdateFacebookLikeInCache(fbLikeParams, facebookLike);

                    facebookLikeSaveResult.SavedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                    if (replicate)
                    {
                        facebookLikeSaveResult.FacebookLikeList = facebookLikeList;
                        //replicate to other service instances
                        ReplicationFacebookLikeList(facebookLikeList, FacebookLikeActionType.Update, clientHostName);
                    }

                    if (!string.IsNullOrEmpty(clientHostName) && null != facebookLikeList)
                    {
                        SynchronizationRequested(facebookLikeList.GetCacheKey(), facebookLikeList.ReferenceTracker.Purge(clientHostName));
                    }
                }
                else
                {
                    facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.InvalidParameters;
                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                    facebookLikeSaveResult.InvalidParameters.Add(fbLikeParams.FacebookId, invalidParameters);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "UpdateFacebookLike failed for " + fbLikeParams.ToString() + ": " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
                facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.Failed;
                facebookLikeSaveResult.FailedFacebookLikeIds.Add(fbLikeParams.FacebookId);
                return facebookLikeSaveResult;
            }

            //            try
            //            {
            //                //activity recording
            //                if (likeObjectTypeId == Convert.ToInt32(LikeObjectTypes.Answer))
            //                {
            //                    ActivityRecordingSA.Instance.RecordActivity(
            //                        fbLikeParams.MemberId,
            //                        Constants.NULL_INT,
            //                        fbLikeParams.SiteId,
            //                        ActivityRecording.ValueObjects.Types.ActionType.LikeAnswer,
            //                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
            //                        string.Format("<AnswerID>{0}</AnswerID>", likeObjectId));
            //                }
            //            }
            //            catch (Exception logEx)
            //            {
            //                string message = "ActivityRecording threw an exception: " + logEx.Message;
            //                System.Diagnostics.EventLog.WriteEntry(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, message, System.Diagnostics.EventLogEntryType.Error);
            //            }

            return facebookLikeSaveResult;
        }

        public FacebookLikeSaveResult RemoveAllFacebookLikes(FacebookLikeParams fbLikeParams, bool replicate, string clientHostName)
        {
            FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Success, fbLikeParams.MemberId);
            try
            {
                if (null!= fbLikeParams && fbLikeParams.MemberId > 0 && fbLikeParams.SiteId > 0)
                {
                    Command comm = new Command();
                    comm.LogicalDatabaseName = ServiceConstants.FACEBOOKLIKESTORE_LDB;
                    comm.StoredProcedureName = "up_RemoveAllFacebookLikesForMember";
                    comm.Key = fbLikeParams.MemberId;

                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.MemberId);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, fbLikeParams.SiteId);

                    Client.Instance.ExecuteAsyncWrite(comm);

                    //Remove from cache             
                    FacebookLikeList facebookLikeList = GetFBLikeListByMember(fbLikeParams);
                    RemoveAllFacebookLikesFromCache(facebookLikeList);

                    if (replicate)
                    {
                        //replicate to other service instances
                        ReplicationFacebookLikeList(facebookLikeList, FacebookLikeActionType.RemoveAll, clientHostName);
                    }

                    if (!string.IsNullOrEmpty(clientHostName) && null != facebookLikeList)
                    {
                        SynchronizationRequested(facebookLikeList.GetCacheKey(),
                                                 facebookLikeList.ReferenceTracker.Purge(clientHostName));
                    }
                }
                else
                {
                    facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.InvalidParameters;
                    facebookLikeSaveResult.InvalidParameters.Add(fbLikeParams.FacebookId, new List<string>(){"MemberId","SiteId"});
                }
            }
            catch (Exception ex)
            {
                //log exception
                System.Diagnostics.EventLog.WriteEntry(MODULE_NAME, "RemoveAllFacebookLikes failed for " + fbLikeParams.ToString()+": "+ex.Message, System.Diagnostics.EventLogEntryType.Error);
                facebookLikeSaveResult.SaveStatus = FacebookLikeSaveStatusType.Failed;
                return facebookLikeSaveResult;
            }

            //            try
            //            {
            //                //activity recording
            //                if (likeObjectTypeId == Convert.ToInt32(LikeObjectTypes.Answer))
            //                {
            //                    ActivityRecordingSA.Instance.RecordActivity(
            //                        fbLikeParams.MemberId,
            //                        Constants.NULL_INT,
            //                        fbLikeParams.SiteId,
            //                        ActivityRecording.ValueObjects.Types.ActionType.UnLikeAnswer,
            //                        ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
            //                        string.Format("<AnswerID>{0}</AnswerID>", likeObjectId));
            //                }
            //            }
            //            catch (Exception logEx)
            //            {
            //                string message = "ActivityRecording threw an exception: " + logEx.Message;
            //                System.Diagnostics.EventLog.WriteEntry(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, message, System.Diagnostics.EventLogEntryType.Error);
            //            }

            return facebookLikeSaveResult;
        }

        public FacebookLikeSaveResult ProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects, bool replicate, string clientHostName)
        {
            if (null != fbLikeParamsObjects && fbLikeParamsObjects.Count>0)
            {
                FacebookLikeSaveStatusType resultSuccess = FacebookLikeSaveStatusType.None;
                List<long> validFbLikes = new List<long>();
                FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(resultSuccess, -1); 
                FacebookLikeParams firstParams = null;
                List<string> invalidParams= null;
                foreach (FacebookLikeParams fblp in fbLikeParamsObjects)
                {                    
                    invalidParams = Validate(fblp);
                    if (null == invalidParams)
                    {
                        firstParams = fblp;
                        break;
                    }
                    else
                    {
                        resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                        facebookLikeSaveResult.InvalidParameters.Add(fblp.FacebookId, invalidParams);
                    }
                }

                if (null != firstParams)
                {

                    FacebookLikeList fbLikeListByMember = GetFBLikeListByMember(firstParams);
                    foreach (FacebookLikeParams facebookLikeParams in fbLikeParamsObjects)
                    {
                        if (null == fbLikeListByMember || !fbLikeListByMember.FacebookLikesDictionary.ContainsKey(facebookLikeParams.FacebookId))
                        {
                            FacebookLikeSaveResult result = AddFacebookLike(facebookLikeParams, false, null, false);
                            switch (result.SaveStatus)
                            {
                                case FacebookLikeSaveStatusType.Failed:
                                    if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                    }
                                    else
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.Failed;
                                    }
                                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                    break;
                                case FacebookLikeSaveStatusType.InvalidParameters:
                                    if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                    }
                                    else
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                                    }
                                    foreach (long key in result.InvalidParameters.Keys)
                                    {
                                        facebookLikeSaveResult.InvalidParameters.Add(key, result.InvalidParameters[key]);
                                    }
                                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                    break;
                                case FacebookLikeSaveStatusType.Success:
                                    if (resultSuccess == FacebookLikeSaveStatusType.Failed ||
                                        resultSuccess == FacebookLikeSaveStatusType.InvalidParameters)
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                    }
                                    else
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.Success;
                                    }
                                    facebookLikeSaveResult.SavedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                    break;
                                case FacebookLikeSaveStatusType.PartialSuccess:
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            //this will be called if we are re-adding existing likes
                            resultSuccess = FacebookLikeSaveStatusType.Success;
                        }
                        validFbLikes.Add(facebookLikeParams.FacebookId);
                    }

                    if (null != fbLikeListByMember)
                    {
                        List<ValueObjects.FacebookLike> deletes =
                            fbLikeListByMember.FacebookLikes.FindAll(delegate(ValueObjects.FacebookLike fbLike)
                                                                         {
                                                                             return !validFbLikes.Contains(fbLike.FacebookId);
                                                                         });
                        foreach (ValueObjects.FacebookLike facebookLike in deletes)
                        {
                            if (null != fbLikeListByMember &&
                                fbLikeListByMember.FacebookLikesDictionary.ContainsKey(facebookLike.FacebookId))
                            {
                                FacebookLikeParams facebookLikeParams = FacebookLikeParams.ParseFromFacebookLike(facebookLike);
                                FacebookLikeSaveResult result = RemoveFacebookLike(facebookLikeParams, false, null);
                                switch (result.SaveStatus)
                                {
                                    case FacebookLikeSaveStatusType.Failed:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.Failed;
                                        }
                                        facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        break;
                                    case FacebookLikeSaveStatusType.InvalidParameters:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                                        }
                                        foreach (long key in result.InvalidParameters.Keys)
                                        {
                                            facebookLikeSaveResult.InvalidParameters.Add(key,
                                                                                         result.InvalidParameters[key]);
                                        }
                                        facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        break;
                                    case FacebookLikeSaveStatusType.Success:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Failed ||
                                            resultSuccess == FacebookLikeSaveStatusType.InvalidParameters)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.Success;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    //Call replication, sync, and enqueue on this stuff
                    fbLikeListByMember = GetFBLikeListByMember(firstParams);

                    if (null != fbLikeListByMember)
                    {
                        facebookLikeSaveResult.FacebookLikeList = fbLikeListByMember;
                        if (replicate)
                        {
                            ReplicationFacebookLikeList(fbLikeListByMember, FacebookLikeActionType.Update, clientHostName);
                        }

                        if (!string.IsNullOrEmpty(clientHostName))
                        {
                            //call cache sync
                            SynchronizationRequested(fbLikeListByMember.GetCacheKey(), fbLikeListByMember.ReferenceTracker.Purge(clientHostName));
                        }

                        bool requiresApproval = false;
                        foreach (ValueObjects.FacebookLike fbl in fbLikeListByMember.FacebookLikes)
                        {
                            if (fbl.FacebookLikeStatus == FacebookLikeStatus.None || fbl.FacebookLikeStatus == FacebookLikeStatus.Pending)
                            {
                                requiresApproval = true;
                                break;
                            }
                        }

                        if (requiresApproval)
                        {
                            ApprovalEnqueue(firstParams);
                        }
                    }
                }
                facebookLikeSaveResult.SaveStatus = resultSuccess;
                               
                return facebookLikeSaveResult;
            }

            return new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Failed, Constants.NULL_INT);
        }

        /// <summary>
        /// This processes only a list of likes that were pending, and so represents a partial list of likes for a member being approved/rejected
        /// by the admin.  Approved likes will be updated and rejected likes will be removed.
        /// </summary>
        public FacebookLikeSaveResult AdminProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects, bool replicate, string clientHostName)
        {
            if (null != fbLikeParamsObjects && fbLikeParamsObjects.Count > 0)
            {
                FacebookLikeSaveStatusType resultSuccess = FacebookLikeSaveStatusType.None;
                FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(resultSuccess, -1);
                FacebookLikeParams firstParams = null;
                List<string> invalidParams = null;
                foreach (FacebookLikeParams fblp in fbLikeParamsObjects)
                {
                    invalidParams = Validate(fblp);
                    if (null == invalidParams)
                    {
                        firstParams = fblp;
                        break;
                    }
                    else
                    {
                        resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                        facebookLikeSaveResult.InvalidParameters.Add(fblp.FacebookId, invalidParams);
                    }
                }

                if (null != firstParams)
                {
                    FacebookLikeList fbLikeListByMember = GetFBLikeListByMember(firstParams);
                    if (fbLikeListByMember != null)
                    {
                        foreach (FacebookLikeParams facebookLikeParams in fbLikeParamsObjects)
                        {
                            if (fbLikeListByMember.FacebookLikesDictionary.ContainsKey(facebookLikeParams.FacebookId))
                            {
                                FacebookLikeSaveResult result = null;
                                if (facebookLikeParams.FbLikeStatus == FacebookLikeStatus.Approved)
                                {
                                    result = UpdateFacebookLike(facebookLikeParams, false, null);
                                }
                                else
                                {
                                    result = RemoveFacebookLike(facebookLikeParams, false, null);
                                }

                                switch (result.SaveStatus)
                                {
                                    case FacebookLikeSaveStatusType.Failed:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.Failed;
                                        }
                                        facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        break;
                                    case FacebookLikeSaveStatusType.InvalidParameters:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                                        }
                                        foreach (long key in result.InvalidParameters.Keys)
                                        {
                                            facebookLikeSaveResult.InvalidParameters.Add(key, result.InvalidParameters[key]);
                                        }
                                        facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        break;
                                    case FacebookLikeSaveStatusType.Success:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Failed ||
                                            resultSuccess == FacebookLikeSaveStatusType.InvalidParameters)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.Success;
                                        }
                                        if (facebookLikeParams.FbLikeStatus == FacebookLikeStatus.Approved)
                                        {
                                            facebookLikeSaveResult.SavedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        }
                                        break;
                                    case FacebookLikeSaveStatusType.PartialSuccess:
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    //Call replication, sync, and enqueue on this stuff
                    fbLikeListByMember = GetFBLikeListByMember(firstParams);

                    if (null != fbLikeListByMember)
                    {
                        facebookLikeSaveResult.FacebookLikeList = fbLikeListByMember;
                        if (replicate)
                        {
                            ReplicationFacebookLikeList(fbLikeListByMember, FacebookLikeActionType.Update, clientHostName);
                        }

                        if (!string.IsNullOrEmpty(clientHostName))
                        {
                            //call cache sync
                            SynchronizationRequested(fbLikeListByMember.GetCacheKey(), fbLikeListByMember.ReferenceTracker.Purge(clientHostName));
                        }
                    }
                }
                facebookLikeSaveResult.SaveStatus = resultSuccess;

                return facebookLikeSaveResult;
            }

            return new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Failed, Constants.NULL_INT);
        }

        /// <summary>
        /// This is similar to ProcessFacebookLikes, but by the admin and is passed the full list of likes for a member that are approved.
        /// Any likes not passed in are considered rejected and removed.
        /// </summary>
        public FacebookLikeSaveResult AdminProcessFacebookLikeByMember(List<FacebookLikeParams> fbLikeParamsObjects, bool replicate, string clientHostName)
        {
            if (null != fbLikeParamsObjects && fbLikeParamsObjects.Count > 0)
            {
                FacebookLikeSaveStatusType resultSuccess = FacebookLikeSaveStatusType.None;
                List<long> validFbLikes = new List<long>();
                FacebookLikeSaveResult facebookLikeSaveResult = new FacebookLikeSaveResult(resultSuccess, -1);
                FacebookLikeParams firstParams = null;
                List<string> invalidParams = null;
                foreach (FacebookLikeParams fblp in fbLikeParamsObjects)
                {
                    invalidParams = Validate(fblp);
                    if (null == invalidParams)
                    {
                        firstParams = fblp;
                        break;
                    }
                    else
                    {
                        resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                        facebookLikeSaveResult.InvalidParameters.Add(fblp.FacebookId, invalidParams);
                    }
                }

                if (null != firstParams)
                {

                    FacebookLikeList fbLikeListByMember = GetFBLikeListByMember(firstParams);
                    foreach (FacebookLikeParams facebookLikeParams in fbLikeParamsObjects)
                    {
                        if (null != fbLikeListByMember && fbLikeListByMember.FacebookLikesDictionary.ContainsKey(facebookLikeParams.FacebookId))
                        {
                            FacebookLikeSaveResult result = UpdateFacebookLike(facebookLikeParams, false, null);
                            switch (result.SaveStatus)
                            {
                                case FacebookLikeSaveStatusType.Failed:
                                    if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                    }
                                    else
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.Failed;
                                    }
                                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                    break;
                                case FacebookLikeSaveStatusType.InvalidParameters:
                                    if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                    }
                                    else
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                                    }
                                    foreach (long key in result.InvalidParameters.Keys)
                                    {
                                        facebookLikeSaveResult.InvalidParameters.Add(key, result.InvalidParameters[key]);
                                    }
                                    facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                    break;
                                case FacebookLikeSaveStatusType.Success:
                                    if (resultSuccess == FacebookLikeSaveStatusType.Failed ||
                                        resultSuccess == FacebookLikeSaveStatusType.InvalidParameters)
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                    }
                                    else
                                    {
                                        resultSuccess = FacebookLikeSaveStatusType.Success;
                                    }
                                    facebookLikeSaveResult.SavedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                    break;
                                case FacebookLikeSaveStatusType.PartialSuccess:
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            //this will be called if we are re-adding existing likes
                            resultSuccess = FacebookLikeSaveStatusType.Success;
                        }
                        validFbLikes.Add(facebookLikeParams.FacebookId);
                    }

                    if (null != fbLikeListByMember)
                    {
                        List<ValueObjects.FacebookLike> deletes = fbLikeListByMember.FacebookLikes.FindAll(delegate(ValueObjects.FacebookLike fbLike)
                            {
                                return !validFbLikes.Contains(fbLike.FacebookId);
                            });
                        foreach (ValueObjects.FacebookLike facebookLike in deletes)
                        {
                            if (null != fbLikeListByMember && fbLikeListByMember.FacebookLikesDictionary.ContainsKey(facebookLike.FacebookId))
                            {
                                FacebookLikeParams facebookLikeParams = FacebookLikeParams.ParseFromFacebookLike(facebookLike);
                                FacebookLikeSaveResult result = RemoveFacebookLike(facebookLikeParams, false, null);
                                switch (result.SaveStatus)
                                {
                                    case FacebookLikeSaveStatusType.Failed:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.Failed;
                                        }
                                        facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        break;
                                    case FacebookLikeSaveStatusType.InvalidParameters:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Success)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.InvalidParameters;
                                        }
                                        foreach (long key in result.InvalidParameters.Keys)
                                        {
                                            facebookLikeSaveResult.InvalidParameters.Add(key,
                                                                                         result.InvalidParameters[key]);
                                        }
                                        facebookLikeSaveResult.FailedFacebookLikeIds.Add(facebookLikeParams.FacebookId);
                                        break;
                                    case FacebookLikeSaveStatusType.Success:
                                        if (resultSuccess == FacebookLikeSaveStatusType.Failed ||
                                            resultSuccess == FacebookLikeSaveStatusType.InvalidParameters)
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.PartialSuccess;
                                        }
                                        else
                                        {
                                            resultSuccess = FacebookLikeSaveStatusType.Success;
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    //Call replication, sync, and enqueue on this stuff
                    fbLikeListByMember = GetFBLikeListByMember(firstParams);

                    if (null != fbLikeListByMember)
                    {
                        facebookLikeSaveResult.FacebookLikeList = fbLikeListByMember;
                        if (replicate)
                        {
                            ReplicationFacebookLikeList(fbLikeListByMember, FacebookLikeActionType.Update, clientHostName);
                        }

                        if (!string.IsNullOrEmpty(clientHostName))
                        {
                            //call cache sync
                            SynchronizationRequested(fbLikeListByMember.GetCacheKey(), fbLikeListByMember.ReferenceTracker.Purge(clientHostName));
                        }
                    }
                }
                facebookLikeSaveResult.SaveStatus = resultSuccess;

                return facebookLikeSaveResult;
            }

            return new FacebookLikeSaveResult(FacebookLikeSaveStatusType.Failed, Constants.NULL_INT);
        }
        
        public List<string> Validate(FacebookLikeParams fbLikeParams)
        {
            List<string> invalidParams = null;
            if (string.IsNullOrEmpty(fbLikeParams.FbImageUrl))
            {
                if (null == invalidParams) invalidParams = new List<string>();
                invalidParams.Add("FbImageUrl");
            }

            if (string.IsNullOrEmpty(fbLikeParams.FbLinkHref))
            {
                if (null == invalidParams) invalidParams = new List<string>();
                invalidParams.Add("FbLinkHref");
            }

            if (string.IsNullOrEmpty(fbLikeParams.FbName))
            {
                if (null == invalidParams) invalidParams = new List<string>();
                invalidParams.Add("FBName");
            }

            if (fbLikeParams.FacebookId <= 0)
            {
                if (null == invalidParams) invalidParams = new List<string>();
                invalidParams.Add("FacebookId");
            }

            if (fbLikeParams.MemberId <= 0)
            {
                if (null == invalidParams) invalidParams = new List<string>();
                invalidParams.Add("MemberId");
            }

            if (fbLikeParams.SiteId <= 0)
            {
                if (null == invalidParams) invalidParams = new List<string>();
                invalidParams.Add("SiteId");
            }

            return invalidParams;
        }
       
    }
}
