﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.FacebookLike.ValueObjects
{
    [Serializable]
    public class FacebookLikeCategoryGroup
    {
        private int _facebookLikeCategoryGroupId;
        private string _facebookCategoryGroupName;
        private string _facebookCategoryGroupNameResourceKey;
        private List<FacebookLikeCategory> _facebookLikeCategories;

        public int FacebookLikeCategoryGroupId
        {
            get { return _facebookLikeCategoryGroupId; }
            set { _facebookLikeCategoryGroupId = value; }
        }

        public string FacebookCategoryGroupName
        {
            get { return _facebookCategoryGroupName; }
            set { _facebookCategoryGroupName = value; }
        }

        public string FacebookCategoryGroupNameResourceKey
        {
            get { return _facebookCategoryGroupNameResourceKey; }
            set { _facebookCategoryGroupNameResourceKey = value; }
        }

        public List<FacebookLikeCategory> FacebookLikeCategories
        {
            get { return _facebookLikeCategories; }
            set { _facebookLikeCategories = value; }
        }

        public FacebookLikeCategoryGroup AddFacebookLikeCategory(FacebookLikeCategory fbLikeCategory)
        {
            if(null == FacebookLikeCategories) FacebookLikeCategories = new List<FacebookLikeCategory>();
            if (!FacebookLikeCategories.Contains(fbLikeCategory))
            {
                FacebookLikeCategories.Add(fbLikeCategory);
            }
            return this;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("FB Group - Id:{0}, Name:\"{1}\"\n", this.FacebookLikeCategoryGroupId,
                                    this.FacebookCategoryGroupName));
            foreach (FacebookLikeCategory likeCategory in FacebookLikeCategories)
            {
                sb.Append(string.Format("\tFB Category - Id:{0}, Name:\"{1}\"\n", likeCategory.FacebookLikeCategoryId,
                                        likeCategory.FacebookLikeCategoryName));
                
            }
            return sb.ToString();
        }
    }
}
