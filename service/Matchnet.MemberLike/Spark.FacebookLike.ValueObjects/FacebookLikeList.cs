﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.CacheSynchronization.Tracking;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;

namespace Spark.FacebookLike.ValueObjects
{
    /// <summary>
    /// Cacheable Container class of Facebook Like objects for a member
    /// </summary>
    [Serializable]
    public class FacebookLikeList : IValueObject, ICacheable, IReplicable
    {
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        List<FacebookLike> _facebookLikes = new List<FacebookLike>();
        Dictionary<long, FacebookLike> _facebookLikesDictionary = new Dictionary<long, FacebookLike>();
        Dictionary<int, List<FacebookLike>> _facebookLikesGroupDictionary = new Dictionary<int, List<FacebookLike>>();
        int _MemberID = Constants.NULL_INT;
        int _SiteID = Constants.NULL_INT;

        #region Properties

        public List<FacebookLike> FacebookLikes
        {
            get { return _facebookLikes; }
            set { _facebookLikes = value; }
        }

        public Dictionary<long, FacebookLike> FacebookLikesDictionary
        {
            get { return _facebookLikesDictionary; }
            set { _facebookLikesDictionary = value; }
        }

        public Dictionary<int, List<FacebookLike>> FacebookLikesGroupDictionary
        {
            get { return _facebookLikesGroupDictionary; }
            set { _facebookLikesGroupDictionary = value; }
        }

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }
        #endregion

        public List<FacebookLike> GetApprovedFacebookLists()
        {
            List<FacebookLike> fbLikes = new List<FacebookLike>();
            if (_facebookLikes != null && _facebookLikes.Count > 0)
            {
                foreach (FacebookLike fbLike in _facebookLikes)
                {
                    if (fbLike.FacebookLikeStatus == FacebookLikeStatus.Approved)
                    {
                        fbLikes.Add(fbLike);
                    }
                }
            }
            return fbLikes;
        }

        public List<FacebookLike> GetApprovedFacebookListsByGroupID(int categoryGroupID)
        {
            List<FacebookLike> fbLikes = new List<FacebookLike>();
            if (_facebookLikesGroupDictionary != null && _facebookLikesGroupDictionary.ContainsKey(categoryGroupID) && _facebookLikesGroupDictionary[categoryGroupID] != null)
            {
                foreach (FacebookLike fbLike in _facebookLikesGroupDictionary[categoryGroupID])
                {
                    if (fbLike.FacebookLikeStatus == FacebookLikeStatus.Approved)
                    {
                        fbLikes.Add(fbLike);
                    }
                }
            }
            return fbLikes;
        }


        #region ICacheable Members
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "FacebookLikeList-{0}-{1}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_SiteID, _MemberID);
        }

        public static string GetCacheKeyString(int siteID, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, siteID, memberid);
        }

        #endregion


        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }
        #endregion
    }
}
