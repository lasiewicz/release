﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.CacheSynchronization.ValueObjects;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;

namespace Spark.FacebookLike.ValueObjects
{
    [Serializable]
    public class FacebookLikeParams
    {
        private string _clientHostName;
        private CacheReference _cacheReference;
        private int _memberId;
        private int _siteId;
        private long _facebookId;
        private int _fbLikeCategoryId;
        private string _fbName;
        private string _fbImageUrl;
        private string _fbLinkHref;
        private string _fbType;

        private FacebookLikeStatus _fbLikeStatus = FacebookLikeStatus.None;
        private int _adminMemberId;

        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        public int SiteId
        {
            get { return _siteId; }
            set { _siteId = value; }
        }

        public long FacebookId
        {
            get { return _facebookId; }
            set { _facebookId = value; }
        }

        public int FbLikeCategoryId
        {
            get { return _fbLikeCategoryId; }
            set { _fbLikeCategoryId = value; }
        }

        public string FbName
        {
            get { return _fbName; }
            set { _fbName = value; }
        }

        public string FbImageUrl
        {
            get { return _fbImageUrl; }
            set { _fbImageUrl = value; }
        }

        public string FbLinkHref
        {
            get { return _fbLinkHref; }
            set { _fbLinkHref = value; }
        }

        public string FbType
        {
            get { return _fbType; }
            set { _fbType = value; }
        }

        public FacebookLikeStatus FbLikeStatus
        {
            get { return _fbLikeStatus; }
            set { _fbLikeStatus = value; }
        }

        public int AdminMemberId
        {
            get { return _adminMemberId; }
            set { _adminMemberId = value; }
        }

        public string ClientHostName
        {
            get { return _clientHostName; }
            set { _clientHostName = value; }
        }

        public CacheReference CacheReference
        {
            get { return _cacheReference; }
            set { _cacheReference = value; }
        }

        public FacebookLikeParams(){}

        public FacebookLikeParams(int memberId, int siteId, long facebookId, int fbLikeCategoryId, string fbName, string fbImageUrl, string fbLinkHref, string fbType) : this()
        {
            _memberId = memberId;
            _siteId = siteId;
            _facebookId = facebookId;
            _fbLikeCategoryId = fbLikeCategoryId;
            _fbName = fbName;
            _fbImageUrl = fbImageUrl;
            _fbLinkHref = fbLinkHref;
            _fbType = fbType;
        }

        public FacebookLikeParams(int memberId, int siteId, long facebookId, int fbLikeCategoryId, string fbName, string fbImageUrl, string fbLinkHref, string fbType, FacebookLikeStatus fbLikeStatus) : this(memberId,siteId, facebookId, fbLikeCategoryId, fbName, fbImageUrl, fbLinkHref, fbType)
        {
            _fbLikeStatus = fbLikeStatus;
        }

        public static FacebookLikeParams ParseFromFacebookLike(FacebookLike facebookLike)
        {
            FacebookLikeParams likeParams = new FacebookLikeParams(facebookLike.MemberId, facebookLike.SiteId, facebookLike.FacebookId, facebookLike.FacebookLikeCategoryId, facebookLike.FacebookName, facebookLike.FacebookImageUrl, facebookLike.FacebookLinkHref, facebookLike.FacebookType, facebookLike.FacebookLikeStatus);
            likeParams.AdminMemberId = facebookLike.AdminMemberId;
            return likeParams;
        }

        public override string ToString()
        {
            string format = "{{MemberId:{0}, SiteId:{1}, FacebookID:{2}, FacebookName:{3}, FacebookType:{4}, FacebookLikeCategoryID:{5}, FacebookLikeStatus:{6}, AdminMemberID:{7}}}";
            return string.Format(format, this.MemberId, this.SiteId, this.FacebookId, this.FbName, this.FbType, this.FbLikeCategoryId, this.FbLikeStatus, this.AdminMemberId);
        }

    }
}
