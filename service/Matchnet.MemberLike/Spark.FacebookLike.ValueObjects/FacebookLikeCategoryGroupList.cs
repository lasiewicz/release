﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.FacebookLike.ValueObjects
{
    /// <summary>
    /// Cacheable Container class of Facebook Like Category Groups for a member
    /// </summary>
    [Serializable]
    public class FacebookLikeCategoryGroupList : IValueObject, ICacheable
    {
        List<FacebookLikeCategoryGroup> _facebookLikeCategoryGroups = new List<FacebookLikeCategoryGroup>();
        Dictionary<int, FacebookLikeCategoryGroup> _facebookLikeCategoryGroupDictionary = new Dictionary<int, FacebookLikeCategoryGroup>();

        #region Properties
        public List<FacebookLikeCategoryGroup> FacebookLikeCategoryGroups
        {
            get { return _facebookLikeCategoryGroups; }
            set { _facebookLikeCategoryGroups = value; }
        }

        public Dictionary<int, FacebookLikeCategoryGroup> FacebookLikeCategoryGroupDictionary
        {
            get { return _facebookLikeCategoryGroupDictionary; }
            set { _facebookLikeCategoryGroupDictionary = value; }
        }
        #endregion

        #region ICacheable Members
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "FacebookLikeCategoryGroupList";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEYFORMAT;
        }

        #endregion

    }
}
