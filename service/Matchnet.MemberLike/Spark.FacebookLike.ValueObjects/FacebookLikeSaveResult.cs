﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.FacebookLike.ValueObjects
{
    /// <summary>
    /// Defines all possible outcomes of a Save attempt.
    /// </summary>
    [Serializable]
    public enum FacebookLikeSaveStatusType : int
    {
        /// <summary>An enum indicating a success in saving the FacebookLike.</summary>
        None=0,
        Success = 1,
        PartialSuccess = 2,
        Failed = 3,
        InvalidParameters=4
    }

    [Serializable]
    public class FacebookLikeSaveResult : IValueObject
    {
        private FacebookLikeSaveStatusType _saveStatus = FacebookLikeSaveStatusType.Success;
        private int _memberID = Constants.NULL_INT;
        private List<long> savedFacebookLikeIds = new List<long>();
        private List<long> failedFacebookLikeIds = new List<long>();
        private Dictionary<long,List<string>> invalidParameters = new Dictionary<long, List<string>>();
        private FacebookLikeList _facebookLikeList = null;

        public FacebookLikeSaveResult(FacebookLikeSaveStatusType status, int memberID)
        {
            SaveStatus = status;
            MemberId = memberID;
        }

        public FacebookLikeSaveStatusType SaveStatus
        {
            get { return _saveStatus; }
            set { _saveStatus = value; }
        }

        public int MemberId
        {
            get { return _memberID; }
            set { _memberID = value; }
        }

        public List<long> SavedFacebookLikeIds
        {
            get { return savedFacebookLikeIds; }
            set { savedFacebookLikeIds = value; }
        }

        public List<long> FailedFacebookLikeIds
        {
            get { return failedFacebookLikeIds; }
            set { failedFacebookLikeIds = value; }
        }

        public Dictionary<long, List<string>> InvalidParameters
        {
            get { return invalidParameters; }
            set { invalidParameters = value; }
        }

        public FacebookLikeList FacebookLikeList
        {
            get { return _facebookLikeList; }
            set { _facebookLikeList = value; }
        }
    }
}
