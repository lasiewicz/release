﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.FacebookLike.ValueObjects
{
    [Serializable]
    public class FacebookLikeCategory
    {
        private int _facebookLikeCategoryId;
        private string _facebookLikeCategoryName;

        public int FacebookLikeCategoryId
        {
            get { return _facebookLikeCategoryId; }
            set { _facebookLikeCategoryId = value; }
        }

        public string FacebookLikeCategoryName
        {
            get { return _facebookLikeCategoryName; }
            set { _facebookLikeCategoryName = value; }
        }
    }
}
