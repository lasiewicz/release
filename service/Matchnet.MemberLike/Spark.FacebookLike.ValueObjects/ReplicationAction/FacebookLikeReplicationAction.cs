﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.FacebookLike.ValueObjects.ReplicationAction
{
    [Serializable]
    public class FacebookLikeReplicationAction : IReplicationAction
    {
        private FacebookLikeList _facebookLikeList;
        private FacebookLikeActionType _actionType = FacebookLikeActionType.Update;
        private string _clientHostName;

        public FacebookLikeReplicationAction() { }

        public FacebookLikeList FacebookLikeList
        {
            get { return _facebookLikeList; }
            set { _facebookLikeList = value; }
        }

        public FacebookLikeActionType ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }

        public string ClientHostName
        {
            get { return _clientHostName; }
            set { _clientHostName = value; }
        }
    }

    [Serializable]
    public enum FacebookLikeActionType
    {
        Update = 1,
        RemoveAll = 2
    }

}
