﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.FacebookLike.ValueObjects.ServiceDefinitions
{
    public enum FacebookLikeStatus
    {
        None=0,
        Pending=1,
        Approved=2,
        Rejected=3
    }


    public class ServiceConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_CONSTANT = "MEMBERLIKE_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.MemberLike.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "FacebookLikeServiceSM";
        /// <summary>
        /// Performance counter name for total hits.
        /// </summary>
        public const string PERF_TOTALCOUNT_NAME = "Total hit count";

        public const string FACEBOOKLIKESTORE_LDB = "mnFacebookLike";

        private Dictionary<int, FacebookLikeCategoryGroup> _facebookLikeCategoryGroupDictionary = new Dictionary<int, FacebookLikeCategoryGroup>();

        public void AddFacebookLikeCategoryToGroup(FacebookLikeCategory fbLikeCategory, FacebookLikeCategoryGroup fbLikeCategoryGroup)
        {
            AddFacebookLikeCategoryToGroup(fbLikeCategory.FacebookLikeCategoryId, fbLikeCategoryGroup);
        }

        public void AddFacebookLikeCategoryToGroup(int facebookLikeCategoryId, FacebookLikeCategoryGroup fbLikeCategoryGroup)
        {
            _facebookLikeCategoryGroupDictionary.Add(facebookLikeCategoryId, fbLikeCategoryGroup);
        }

        public FacebookLikeCategoryGroup GetGroupForFacebookLikeCategory(FacebookLikeCategory fbLikeCategory)
        {
            FacebookLikeCategoryGroup fbLikeCategoryGroup = null;
            if (_facebookLikeCategoryGroupDictionary.ContainsKey(fbLikeCategory.FacebookLikeCategoryId))
            {
                fbLikeCategoryGroup = _facebookLikeCategoryGroupDictionary[fbLikeCategory.FacebookLikeCategoryId];
            }
            return fbLikeCategoryGroup;
        }
    }
}
