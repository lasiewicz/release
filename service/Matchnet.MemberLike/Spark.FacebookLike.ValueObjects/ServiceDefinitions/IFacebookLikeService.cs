﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.CacheSynchronization.ValueObjects;

namespace Spark.FacebookLike.ValueObjects.ServiceDefinitions
{
    public interface IFacebookLikeService
    {
        FacebookLikeSaveResult AddFacebookLike(FacebookLikeParams fbLikeParams, string clientHostName);
        FacebookLikeSaveResult RemoveFacebookLike(FacebookLikeParams fbLikeParams, string clientHostName);
        FacebookLikeSaveResult RemoveAllFacebookLikes(FacebookLikeParams fbLikeParams, string clientHostName);
        FacebookLikeSaveResult ProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects, string clientHostName);
        FacebookLikeSaveResult AdminProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects, string clientHostName);
        FacebookLikeSaveResult AdminProcessFacebookLikeByMember(List<FacebookLikeParams> fbLikeParamsObjects, string clientHostName);
        FacebookLikeList GetFacebookLikeListByMember(FacebookLikeParams fbLikeParams, string clientHostName, CacheReference cacheReference);
        FacebookLikeCategoryGroupList GetFacebookLikeCategoryGroupList();
    }
}