﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.FacebookLike.ValueObjects.ServiceDefinitions
{
    public interface IFacebookLikeSA
    {
        FacebookLikeSaveResult AddFacebookLike(FacebookLikeParams fbLikeParams);
        FacebookLikeSaveResult RemoveFacebookLike(FacebookLikeParams fbLikeParams);
        FacebookLikeSaveResult RemoveAllFacebookLikes(FacebookLikeParams fbLikeParams);
        FacebookLikeSaveResult ProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects);
        FacebookLikeList GetFacebookLikeListByMember(FacebookLikeParams fbLikeParams);
        FacebookLikeList GetFacebookLikeListByMember(FacebookLikeParams fbLikeParams, bool ignoreSACache);
        List<ValueObjects.FacebookLike> GetRandomFacebookLikesListByMember(FacebookLikeParams fbLikeParams, int count, bool approvedOnly);
        FacebookLikeCategoryGroupList GetFacebookLikeCategoryGroupList();
        FacebookLikeSaveResult AdminProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects);
        FacebookLikeSaveResult AdminProcessFacebookLikeByMember(List<FacebookLikeParams> fbLikeParamsObjects);
        void RemoveAllFacebookLikesFromCache(int siteID, int memberID);
    }
}
