﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;

namespace Spark.FacebookLike.ValueObjects
{
    [Serializable]
    public class FacebookLike : IEquatable<FacebookLike>
    {
        private int _memberId;
        private int _siteId;
        private int _facebookLikeCategoryId;
        private int _facebookLikeCategoryGroupId;
        private long _facebookId;
        private string _facebookName;
        private string _facebookImageUrl;
        private string _facebookLinkHref;
        private string _facebookType;
        private FacebookLikeStatus _facebookLikeStatus = FacebookLikeStatus.None;
        private DateTime _insertDate;
        private DateTime _updateTime;
        private int _adminMemberId;

        public DateTime UpdateTime
        {
            get { return _updateTime; }
            set { _updateTime = value; }
        }

        public DateTime InsertDate
        {
            get { return _insertDate; }
            set { _insertDate = value; }
        }

        public string FacebookType
        {
            get { return _facebookType; }
            set { _facebookType = value; }
        }

        public string FacebookLinkHref
        {
            get { return _facebookLinkHref; }
            set { _facebookLinkHref = value; }
        }

        public string FacebookImageUrl
        {
            get { return _facebookImageUrl; }
            set { _facebookImageUrl = value; }
        }

        public string FacebookName
        {
            get { return _facebookName; }
            set { _facebookName = value; }
        }

        public long FacebookId
        {
            get { return _facebookId; }
            set { _facebookId = value; }
        }

        public int FacebookLikeCategoryId
        {
            get { return _facebookLikeCategoryId; }
            set { _facebookLikeCategoryId = value; }
        }

        public int SiteId
        {
            get { return _siteId; }
            set { _siteId = value; }
        }

        public int MemberId
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        public int FacebookLikeCategoryGroupId
        {
            get { return _facebookLikeCategoryGroupId; }
            set { _facebookLikeCategoryGroupId = value; }
        }

        public FacebookLikeStatus FacebookLikeStatus
        {
            get { return _facebookLikeStatus; }
            set { _facebookLikeStatus = value; }
        }

        public int AdminMemberId
        {
            get { return _adminMemberId; }
            set { _adminMemberId = value; }
        }

        #region IEquatable<FacebookLike> Members

        public FacebookLike(){}

        public FacebookLike(int memberId, int siteId, int facebookLikeCategoryId, long facebookId, string facebookName, string facebookImageUrl, string facebookLinkHref, string facebookType, DateTime insertDate, DateTime updateTime, FacebookLikeStatus facebookLikeStatus)
        {
            _memberId = memberId;
            _siteId = siteId;
            _facebookLikeCategoryId = facebookLikeCategoryId;
            _facebookId = facebookId;
            _facebookName = facebookName;
            _facebookImageUrl = facebookImageUrl;
            _facebookLinkHref = facebookLinkHref;
            _facebookType = facebookType;
            _insertDate = insertDate;
            _updateTime = updateTime;
            _facebookLikeStatus = facebookLikeStatus;
        }

        public bool Equals(FacebookLike other)
        {
            bool a = other.FacebookId == this.FacebookId;
            bool b = other.MemberId == this.MemberId;
            bool c = other.SiteId == this.SiteId;
            return a && b && c;
        }

        #endregion

        public override string ToString()
        {
            string format = "{{MemberId:{0}, SiteId:{1}, FacebookID:{2}, FacebookName:{3}, FacebookType:{4}, FacebookLikeCategoryID:{5}, FacebookLikeCategoryGroupID:{6}, FacebookLikeStatus:{7}, AdminMemberID:{8} }}";
            return string.Format(format, this.MemberId, this.SiteId, this.FacebookId, this.FacebookName, this.FacebookType, this.FacebookLikeCategoryId, this.FacebookLikeCategoryGroupId, this.FacebookLikeStatus, this.AdminMemberId);
        }

        public static FacebookLike ParseFromFacebookLikeParams(FacebookLikeParams facebookLikeParams)
        {
            FacebookLike fbLike = new FacebookLike(facebookLikeParams.MemberId, facebookLikeParams.SiteId, facebookLikeParams.FbLikeCategoryId, facebookLikeParams.FacebookId, facebookLikeParams.FbName, facebookLikeParams.FbImageUrl, facebookLikeParams.FbLinkHref, facebookLikeParams.FbType, DateTime.Now, DateTime.Now, facebookLikeParams.FbLikeStatus);
            fbLike.AdminMemberId = facebookLikeParams.AdminMemberId;
            return fbLike;
        }
    }
}
