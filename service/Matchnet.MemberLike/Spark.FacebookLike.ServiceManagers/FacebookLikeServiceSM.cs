﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Exceptions;
using Spark.FacebookLike.ValueObjects;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;
using Spark.FacebookLike.BusinessLogic;
using Matchnet.Replication;
using Matchnet.Data.Hydra;

namespace Spark.FacebookLike.ServiceManagers
{
    public class FacebookLikeServiceSM : MarshalByRefObject, IServiceManager, IDisposable, IFacebookLikeService, IReplicationActionRecipient, IReplicationRecipient
    {
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;
        private PerformanceCounter m_perfAllHitCount;
        private Synchronizer _synchronizer = null;

        public FacebookLikeServiceSM()
        {
            try
            {
//                System.Threading.Thread.Sleep(10000);
                initPerfCounters();
                string machineName = System.Environment.MachineName;
              
                //start hydra writer
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { ServiceConstants.FACEBOOKLIKESTORE_LDB });
                _hydraWriter.Start();

                //replication
                FacebookLikeServiceBL.Instance.ReplicationRequested += new FacebookLikeServiceBL.ReplicationEventHandler(FacebookLikeServiceBL_ReplicationRequested);

                string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FACEBOOKLIKESVC_REPLICATION_OVERRIDE");
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, ServiceConstants.SERVICE_MANAGER_NAME, machineName);
                }

                if (!String.IsNullOrEmpty(replicationURI))
                {
                    _replicator = new Replicator("FACEBOOKLIKESVC");  //this uses specific name for FacebookLike
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                }

                //synchronization
                FacebookLikeServiceBL.Instance.SynchronizationRequested += new FacebookLikeServiceBL.SynchronizationEventHandler(FacebookLikeServiceBL_SynchronizationRequested);
                _synchronizer = new Synchronizer(ServiceConstants.SERVICE_NAME);
                _synchronizer.Start();

                //TODO: Warmup category group caches
                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM contstructor completed.");
            }
            catch (Exception e)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, e.ToString(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// keep the object alive indefinitely
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region FacebookLikeService Members

        public FacebookLikeSaveResult AddFacebookLike(FacebookLikeParams fbLikeParams, string clientHostName)
        {            
            try
            {
                return FacebookLikeServiceBL.Instance.AddFacebookLike(fbLikeParams, true, clientHostName, true);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AddFacebookLike() error. ", ex);
            }
        }

        public FacebookLikeSaveResult RemoveFacebookLike(FacebookLikeParams fbLikeParams, string clientHostName)
        {
            try
            {
                return FacebookLikeServiceBL.Instance.RemoveFacebookLike(fbLikeParams, true, clientHostName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemoveFacebookLike() error. ", ex);
            }
        }

        public FacebookLikeSaveResult RemoveAllFacebookLikes(FacebookLikeParams fbLikeParams, string clientHostName)
        {
            try
            {
                return FacebookLikeServiceBL.Instance.RemoveAllFacebookLikes(fbLikeParams, true, clientHostName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemoveAllFacebookLikes() error. ", ex);
            }
        }

        public FacebookLikeSaveResult ProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects, string clientHostName)
        {
            try
            {
                return FacebookLikeServiceBL.Instance.ProcessFacebookLikes(fbLikeParamsObjects, true, clientHostName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "ProcessFacebookLikes() error. ", ex);
            }
        }

        public FacebookLikeSaveResult AdminProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects, string clientHostName)
        {
            try
            {
                return FacebookLikeServiceBL.Instance.AdminProcessFacebookLikes(fbLikeParamsObjects, true, clientHostName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AdminProcessFacebookLikes() error. ", ex);
            }            
        }
        
        public FacebookLikeSaveResult AdminProcessFacebookLikeByMember(List<FacebookLikeParams> fbLikeParamsObjects, string clientHostName)
        {
            try
            {
                return FacebookLikeServiceBL.Instance.AdminProcessFacebookLikeByMember(fbLikeParamsObjects, true, clientHostName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AdminProcessFacebookLikeByMember() error. ", ex);
            }                        
        }

        public FacebookLikeList GetFacebookLikeListByMember(FacebookLikeParams fbLikeParams, string clientHostName, CacheReference cacheReference)
        {
            FacebookLikeList likes = null;
            try
            {
                likes = FacebookLikeServiceBL.Instance.GetFBLikeListByMember(fbLikeParams, clientHostName, cacheReference);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetFacebookLikeListByMember() error. ", ex);
            }
            return likes;
        }

        public FacebookLikeCategoryGroupList GetFacebookLikeCategoryGroupList()
        {
            FacebookLikeCategoryGroupList fbCategoryGroupList = null;
            try
            {
                fbCategoryGroupList = FacebookLikeServiceBL.Instance.GetFacebookLikeCategoryGroupList();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetFacebookLikeCategoryGroupList() error. ", ex);
            }
            return fbCategoryGroupList;
        }
        #endregion

        #region IServiceManager Members

        public void PrePopulateCache()
        {
            // no implementation
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

            if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }

        }

        #endregion

        #region Instrumentation
        private static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { new CounterCreationData(ServiceConstants.PERF_TOTALCOUNT_NAME, "Counter fo all incoming requests.", PerformanceCounterType.NumberOfItems64) };
        }

        public static void PerfCounterInstall()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM,Start PerfCounterInstall");

                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());

                PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME,
                    PerformanceCounterCategoryType.SingleInstance, ccdc);

                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM,Finish PerfCounterInstall");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM Exception in PerfCounterInstall.");
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "PerfCounterInstall() error. ", ex);
            }
        }

        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
        }

        private void initPerfCounters()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM, Start initPerfCounters");

                m_perfAllHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_TOTALCOUNT_NAME, false);
                resetPerfCounters();

                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM, Finish initPerfCounters");
            }
            catch
            {
                System.Diagnostics.Trace.WriteLine("FacebookLikeServiceSM exception in initPerfCounters()");
            }
        }

        private void resetPerfCounters()
        {
            m_perfAllHitCount.RawValue = 0;
            System.Diagnostics.Trace.WriteLine("Perf counters reseted.");
        }
        #endregion

        #region Replication Handler
        private void FacebookLikeServiceBL_ReplicationRequested(IReplicationAction replicationAction)
        {
            if (_replicator != null)
            {
                _replicator.Enqueue(replicationAction);
            }
        }
        #endregion

        #region IReplicationActionRecipient Members
        public void Receive(IReplicationAction replicationAction)
        {
            string functionName = "IReplicationActionRecipient.Receive";

            try
            {
                FacebookLikeServiceBL.Instance.PlayReplicationAction(replicationAction);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_MANAGER_NAME, functionName + ": " + ex.Message, EventLogEntryType.Error);
            }
        }
        #endregion

        #region IReplicationRecipient Members
        public void Receive(IReplicable replicableObject)
        {
            if (null != _replicator)
            {
                //_replicator.Enqueue(replicableObject);
            }
        }

        private void FacebookLikeServiceBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if (null != _synchronizer)
            {
                _synchronizer.Enqueue(key, cacheReferences);
            }
        }
        #endregion
    }
}
