﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.MemberLike.ValueObjects;
using Matchnet.MemberLike.ValueObjects.ServiceDefinitions;
using Matchnet.MemberLike.BusinessLogic;
using Matchnet.Replication;
using Matchnet.Data.Hydra;

namespace Matchnet.MemberLike.ServiceManagers
{
    public class MemberLikeServiceSM : MarshalByRefObject, IServiceManager, IDisposable, IMemberLikeService, IReplicationActionRecipient
    {
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;
        private PerformanceCounter m_perfAllHitCount;

        public MemberLikeServiceSM()
        {
            try
            {
                string machineName = "";
              
                //start hydra writer
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { ServiceConstants.PREMIUMSTORE_LDB });
                _hydraWriter.Start();

                //replication
                MemberLikeServiceBL.Instance.ReplicationRequested += new MemberLikeServiceBL.ReplicationEventHandler(MemberLikeServiceBL_ReplicationRequested);

                string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERLIKESVC_REPLICATION_OVERRIDE");
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.MemberLike.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME, machineName);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                {
                    _replicator = new Replicator("MEMBERLIKESVC");  //this uses specific name for MemberLike
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                }
                initPerfCounters();
                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM contstructor completed.");
            }
            catch (Exception e)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, e.ToString(), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// keep the object alive indefinitely
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region MemberLikeService Members

        public int AddMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            int count = 0;
            try
            {
                count=MemberLikeServiceBL.Instance.AddMemberLike(memberLikeParams,likeObjectId,likeObjectTypeId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AddMemberLike() error. ", ex);
            }
            return count;
        }

        public int RemoveMemberLike(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            int count = 0;
            try
            {
                count = MemberLikeServiceBL.Instance.RemoveMemberLike(memberLikeParams, likeObjectId, likeObjectTypeId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemoveMemberLike() error. ", ex);
            }
            return count;
        }

        public MemberLikeList GetLikeListByMember(MemberLikeParams memberLikeParams)
        {
            MemberLikeList likes = null;
            try
            {
                likes = MemberLikeServiceBL.Instance.GetLikeListByMember(memberLikeParams);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetLikeListByMember() error. ", ex);
            }
            return likes;
        }

        public MemberLikeList GetLikeListByMemberAndType(MemberLikeParams memberLikeParams, int likeObjectTypeId)
        {
            MemberLikeList likes = null;
            try
            {
                likes = MemberLikeServiceBL.Instance.GetLikeListByMemberAndType(memberLikeParams, likeObjectTypeId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetLikeListByMemberAndType() error. ", ex);
            }
            return likes;
        }

        public TypeLikeList GetLikeListByObjectIdAndType(MemberLikeParams memberLikeParams, int likeObjectId, int likeObjectTypeId)
        {
            TypeLikeList likes = null;
            try
            {
                likes = MemberLikeServiceBL.Instance.GetLikeListByObjectIdAndType(memberLikeParams, likeObjectId, likeObjectTypeId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetLikeListByMemberAndType() error. ", ex);
            }
            return likes;
        }

        #endregion

        #region IServiceManager Members

        public void PrePopulateCache()
        {
            // no implementation
        }

        #endregion

        #region Replication Handler
        private void MemberLikeServiceBL_ReplicationRequested(IReplicationAction replicationAction)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicationAction);
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

        }

        #endregion

        #region IReplicationActionRecipient Members
        public void Receive(IReplicationAction replicationAction)
        {
            string functionName = "IReplicationActionRecipient.Receive";

            try
            {
                MemberLikeServiceBL.Instance.PlayReplicationAction(replicationAction);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_MANAGER_NAME, functionName + ": " + ex.Message, EventLogEntryType.Error);
            }
        }
        #endregion

        #region Instrumentation
        private static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { new CounterCreationData(ServiceConstants.PERF_TOTALCOUNT_NAME, "Counter fo all incoming requests.", PerformanceCounterType.NumberOfItems64) };
        }

        public static void PerfCounterInstall()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM,Start PerfCounterInstall");

                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());

                PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME,
                    PerformanceCounterCategoryType.SingleInstance, ccdc);

                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM,Finish PerfCounterInstall");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM Exception in PerfCounterInstall.");
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "PerfCounterInstall() error. ", ex);
            }
        }

        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
        }

        private void initPerfCounters()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM, Start initPerfCounters");

                m_perfAllHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_TOTALCOUNT_NAME, false);
                resetPerfCounters();

                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM, Finish initPerfCounters");
            }
            catch
            {
                System.Diagnostics.Trace.WriteLine("MemberLikeServiceSM exception in initPerfCounters()");
            }
        }

        private void resetPerfCounters()
        {
            m_perfAllHitCount.RawValue = 0;
            System.Diagnostics.Trace.WriteLine("Perf counters reseted.");
        }

        #endregion
    }
}
