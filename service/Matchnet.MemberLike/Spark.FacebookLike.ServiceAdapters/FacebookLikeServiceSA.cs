﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.CacheSynchronization.Context;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Caching;
using Spark.FacebookLike.ValueObjects;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;

namespace Spark.FacebookLike.ServiceAdapters
{
    public class FacebookLikeServiceSA : SABase, IFacebookLikeSA
    {
        private Cache _cache;
        private ISettingsSA _settingsService = null;        

        // ye ole' singleton instance
        public static readonly FacebookLikeServiceSA Instance = new FacebookLikeServiceSA();

        private FacebookLikeServiceSA()
        {
            _cache = Cache.Instance;
        }

        private ISettingsSA SettingsService
        {
            set { _settingsService = value; }
            get
            {
                if (null == _settingsService)
                {
                    return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.Instance;
                }
                return _settingsService;
            }
        }

        private int GetMaxThreshold()
        {
            int maxThreshold = 500;
            try
            {
                string settingFromSingleton = SettingsService.GetSettingFromSingleton("MAX_LOOPS_FOR_RANDOM_FB_LIKES");
                maxThreshold = Convert.ToInt32(settingFromSingleton);
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("FacebookLikeServiceSA",e.Message,EventLogEntryType.Error);
                maxThreshold = 500;
            }
            return maxThreshold;
        }

        public FacebookLikeSaveResult AddFacebookLike(FacebookLikeParams fbLikeParams)
        {
            string uri = string.Empty;
            FacebookLikeSaveResult result = null;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).AddFacebookLike(fbLikeParams, System.Environment.MachineName);
                    if (result.SaveStatus == FacebookLikeSaveStatusType.Success || result.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                    {
                        _cache.Insert(result.FacebookLikeList);
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("AddFacebookLike() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, fbLikeParams.ToString())));
            }
            return result;
        }

        public FacebookLikeSaveResult RemoveFacebookLike(FacebookLikeParams fbLikeParams)
        {
            string uri = string.Empty;
            FacebookLikeSaveResult result = null;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).RemoveFacebookLike(fbLikeParams, System.Environment.MachineName);
                    if (result.SaveStatus == FacebookLikeSaveStatusType.Success || result.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                    {
                        _cache.Insert(result.FacebookLikeList);
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RemoveFacebookLike() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, fbLikeParams.ToString())));
            }
            return result;
        }

        public FacebookLikeSaveResult RemoveAllFacebookLikes(FacebookLikeParams fbLikeParams)
        {
            string uri = string.Empty;
            FacebookLikeSaveResult result = null;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).RemoveAllFacebookLikes(fbLikeParams, System.Environment.MachineName);
                    _cache.Remove(FacebookLikeList.GetCacheKeyString(fbLikeParams.SiteId, fbLikeParams.MemberId));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RemoveAllFacebookLikes() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, fbLikeParams.ToString())));
            }
            return result;
        }

        public FacebookLikeSaveResult ProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects)
        {
            string uri = string.Empty;
            FacebookLikeSaveResult result = null;
            FacebookLikeParams fbLikeParams = null;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).ProcessFacebookLikes(fbLikeParamsObjects, System.Environment.MachineName);
                    foreach (FacebookLikeParams fblp in fbLikeParamsObjects)
                    {
                        if (fblp.MemberId > 0 && fblp.SiteId > 0)
                        {
                            fbLikeParams = fblp;
                            break;
                        }
                    }                    
                    if (null != fbLikeParams)
                    {
                        if (result.SaveStatus == FacebookLikeSaveStatusType.Success || result.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                        {
                            _cache.Insert(result.FacebookLikeList);
                        }
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                string paramsString = (null != fbLikeParams) ? fbLikeParams.ToString() : string.Empty;
                throw (new SAException(string.Format("ProcessFacebookLikes() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, paramsString)));
            }
            return result;
        }

        public FacebookLikeSaveResult AdminProcessFacebookLikes(List<FacebookLikeParams> fbLikeParamsObjects)
        {
            string uri = string.Empty;
            FacebookLikeSaveResult result = null;
            FacebookLikeParams fbLikeParams = null;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).AdminProcessFacebookLikes(fbLikeParamsObjects, System.Environment.MachineName);
                    foreach (FacebookLikeParams fblp in fbLikeParamsObjects)
                    {
                        if (fblp.MemberId > 0 && fblp.SiteId > 0)
                        {
                            fbLikeParams = fblp;
                            break;
                        }
                    }
                    if (null != fbLikeParams)
                    {
                        if (result.SaveStatus == FacebookLikeSaveStatusType.Success || result.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                        {
                            _cache.Insert(result.FacebookLikeList);
                        }
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                string paramsString = (null != fbLikeParams) ? fbLikeParams.ToString() : string.Empty;
                throw (new SAException(string.Format("AdminProcessFacebookLikes() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, paramsString)));
            }
            return result;
        }

        public FacebookLikeSaveResult AdminProcessFacebookLikeByMember(List<FacebookLikeParams> fbLikeParamsObjects)
        {
            string uri = string.Empty;
            FacebookLikeSaveResult result = null;
            FacebookLikeParams fbLikeParams = null;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).AdminProcessFacebookLikeByMember(fbLikeParamsObjects, System.Environment.MachineName);
                    foreach (FacebookLikeParams fblp in fbLikeParamsObjects)
                    {
                        if (fblp.MemberId > 0 && fblp.SiteId > 0)
                        {
                            fbLikeParams = fblp;
                            break;
                        }
                    }
                    if (null != fbLikeParams)
                    {
                        if (result.SaveStatus == FacebookLikeSaveStatusType.Success || result.SaveStatus == FacebookLikeSaveStatusType.PartialSuccess)
                        {
                            _cache.Insert(result.FacebookLikeList);
                        }
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                string paramsString = (null != fbLikeParams) ? fbLikeParams.ToString() : string.Empty;
                throw (new SAException(string.Format("AdminProcessFacebookLikeByMember() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, paramsString)));
            }
            return result;
        }

        public FacebookLikeList GetFacebookLikeListByMember(FacebookLikeParams fbLikeParams)
        {
            return GetFacebookLikeListByMember(fbLikeParams, false);
        }

        public FacebookLikeList GetFacebookLikeListByMember(FacebookLikeParams fbLikeParams, bool ignoreSACache)
        {
            FacebookLikeList fbLikes = null;
            string uri = string.Empty;
            try
            {
                //check cache
                if (!ignoreSACache)
                {
                    fbLikes = _cache.Get(FacebookLikeList.GetCacheKeyString(fbLikeParams.SiteId, fbLikeParams.MemberId)) as FacebookLikeList;
                }

                if (null == fbLikes)
                {
                    uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        int cacheTTL=300;
                        try 
	                    {	        
		                    cacheTTL = Convert.ToInt32(SettingsService.GetSettingFromSingleton("FACEBOOKLIKES_SVC_CACHE_TTL_SECONDS"));
	                    }
	                    catch (Exception ignore)
	                    {
	                        cacheTTL = 300;
	                    }

                        fbLikes = getService(uri).GetFacebookLikeListByMember(fbLikeParams, System.Environment.MachineName, Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)));
                        if (null != fbLikes)
                        {
                            _cache.Insert(fbLikes);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetFacebookLikeListByMember() error (uri: {0}, exception:{1}, params:{2})", uri, ex, fbLikeParams.ToString())));
            }
            return fbLikes;
        }

        public List<ValueObjects.FacebookLike> GetRandomFacebookLikesListByMember(FacebookLikeParams fbLikeParams, int count, bool approvedOnly)
        {
            List<ValueObjects.FacebookLike> randomFacebookLikes = null;
            FacebookLikeList facebookLikeList = GetFacebookLikeListByMember(fbLikeParams);
            if (null != facebookLikeList && count > 0)
            {
                List<ValueObjects.FacebookLike> facebookLikes = null;
                if (approvedOnly)
                {
                    facebookLikes = facebookLikeList.GetApprovedFacebookLists();
                }
                else
                {
                    facebookLikes = facebookLikeList.FacebookLikes;
                }

                if (facebookLikes != null && facebookLikes.Count > 0)
                {
                    int maxThreshold = GetMaxThreshold();
                    int maxFBGroupCount = facebookLikeList.FacebookLikesGroupDictionary.Keys.Count;
                    if (count > facebookLikes.Count) count = facebookLikes.Count;
                    Random rng = new Random();
                    randomFacebookLikes = new List<ValueObjects.FacebookLike>();
                    int threshold = 0;

                    //otherwise pull fb groups randomly and then pull random likes from the groups
                    while (randomFacebookLikes.Count < count && threshold < maxThreshold)
                    {
                        int groupIdx = rng.Next(maxFBGroupCount);
                        int fbGroupID = facebookLikeList.FacebookLikesGroupDictionary.Keys.ElementAt(groupIdx);
                        List<ValueObjects.FacebookLike> fbLikeList = null;
                        if (approvedOnly)
                        {
                            fbLikeList = facebookLikeList.GetApprovedFacebookListsByGroupID(fbGroupID);
                        }
                        else
                        {
                            fbLikeList = facebookLikeList.FacebookLikesGroupDictionary[fbGroupID];
                        }

                        int maxFBLikesCount = (fbLikeList != null) ? fbLikeList.Count : 0;
                        int k = rng.Next(maxFBLikesCount);
                        try
                        {
                            if (maxFBLikesCount > 0)
                            {
                                ValueObjects.FacebookLike fbLike = fbLikeList[k];
                                if (!randomFacebookLikes.Contains(fbLike))
                                {
                                    randomFacebookLikes.Add(fbLike);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            EventLog.WriteEntry("GetRandomFacebookLikesListByMember()", string.Format("Error: {0}, GroupID: {1}, Idx: {2}, Count: {3}", e.Message, fbGroupID, k, maxFBLikesCount), EventLogEntryType.Error);
                        }
                        threshold++;
                    }
                }
            }
            return randomFacebookLikes;
        }

        public FacebookLikeCategoryGroupList GetFacebookLikeCategoryGroupList()
        {
            FacebookLikeCategoryGroupList fbCategoryGroupList = null;
            string uri = string.Empty;
            try
            {
                //check cache
                fbCategoryGroupList = _cache.Get(FacebookLikeCategoryGroupList.GetCacheKeyString()) as FacebookLikeCategoryGroupList;

                if (null == fbCategoryGroupList)
                {
                    uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        fbCategoryGroupList = getService(uri).GetFacebookLikeCategoryGroupList();
                        if (null != fbCategoryGroupList)
                        {
                            _cache.Insert(fbCategoryGroupList);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetFacebookLikeCategoryGroupList() error (uri: {0}, exception:{1})", uri, ex)));
            }
            return fbCategoryGroupList;
        }

        public void RemoveAllFacebookLikesFromCache(int siteID, int memberID)
        {
            if (siteID > 0 && memberID > 0)
            {
                string cacheKeyString = FacebookLikeList.GetCacheKeyString(siteID, memberID);
                this._cache.Remove(cacheKeyString);
            }
        }

        private IFacebookLikeService getService(string uri)
        {
            try
            {
                return (IFacebookLikeService)Activator.GetObject(typeof(IFacebookLikeService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        private string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName = SettingsService.GetSettingFromSingleton("MEMBERLIKE_SVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }
                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("MEMBERLIKE_SVC_SA_CONNECTION_LIMIT"));
        }
    }
}
