using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.VSSWrapper.ValueObjects
{
    /// <summary>
    /// Encapsulates a username password for VSS logon
    /// </summary>
    [Serializable]
    public class AuthInfo
    {
        private string _username;
        private string _password;

        public AuthInfo(string username, string password)
        {
            _username = username;
            _password = password;
        }

        public AuthInfo(string username)
        {
            _username = username;
            _password = string.Empty;
        }


        public string Username
        {
            get
            {
                return _username;
            }
        }


        public string Password
        {
            get
            {
                return _password;
            }
        }
    }
}
