using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.VSSWrapper.ValueObjects
{
    [Serializable]
    public class FileHistory
    {
        private Int32 _version;
        private DateTime _modifiedDate;
        private string _user;
        private string _comment;
        private bool _isLabel;

        public FileHistory(Int32 version, DateTime modifiedDate, string user, string comment, bool isLabel)
        {
            _version = version;
            _modifiedDate = modifiedDate;
            _user = user;
            _comment = comment;
            _isLabel = isLabel;
        }

        public Int32 Version
        {
            get
            {
                return _version;
            }
        }

        public DateTime ModifiedDate
        {
            get
            {
                return _modifiedDate;
            }
        }

        public string User
        {
            get
            {
                return _user;
            }
        }

        public string Comment
        {
            get
            {
                return _comment;
            }
        }

        public bool IsLabel
        {
            get
            {
                return _isLabel;
            }
        }

        public override string ToString()
        {
            return ModifiedDate.ToString("yyyy-MM-dd") + ", " + Version.ToString("000") + ", " + User + ", " + Comment;
        }
    }
}
