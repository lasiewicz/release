using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.VSSWrapper.ValueObjects
{
    public class VSSFileCollection : IEnumerable<string>, ICacheable
    {
        private List<string> _files = new List<string>();

        private CacheItemMode _cacheMode;
        private CacheItemPriorityLevel _cachePriority;
        private Int32 _cacheTTLSeconds = 300;
        private string _cacheKey;

        public VSSFileCollection(string cacheKey)
        {
            _cacheKey = cacheKey;
        }

        public void Add(string fileName)
        {
            _files.Add(fileName);
        }

        #region IEnumerable<string> Members

        public IEnumerator<string> GetEnumerator()
        {
            return _files.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _files.GetEnumerator();
        }

        #endregion

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
            set
            {
                _cacheMode = value;
            }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        public string GetCacheKey()
        {
            return _cacheKey;
        }

        #endregion
    }
}
