using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Matchnet.VSSWrapper.ValueObjects
{
    [Serializable]
    public class FileHistoryCollection : IEnumerable
    {
        private List<FileHistory> _fileHistorys;
        private Dictionary<Int32, FileHistory> _versions;
        

        public FileHistoryCollection()
        {
            _fileHistorys = new List<FileHistory>();
            _versions = new Dictionary<int,FileHistory>();
        }

        public void Add(FileHistory fileHistory)
        {
            if (!fileHistory.IsLabel)
            {
                if (!_versions.ContainsKey(fileHistory.Version))
                {
                    _versions.Add(fileHistory.Version, fileHistory);
                }
            }

            _fileHistorys.Add(fileHistory);
        }

        public bool Contains(FileHistory fileHistory)
        {
            return Contains(fileHistory.Version);
        }

        public bool Contains(int version)
        {
            return _versions.ContainsKey(version);
        }

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _fileHistorys.GetEnumerator();
        }

        #endregion
    }
}
