﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using Matchnet.P4Wrapper.ValueObjects;

namespace Matchnet.P4Wrapper
{
    public class P4Wrapper
    {
        public static string GetUser()
        {
            P4COM.p4 p4 = new P4COM.p4();

            return p4.User;
        }

        /// <summary>
        /// Manually run a command against p4.
        /// </summary>
        public static P4CommandResults RunCommand(string command)
        {
            P4CommandResults p4commandResults = new P4CommandResults();

            try
            {
                P4COM.p4 p4 = new P4COM.p4();

                p4.User = "sparkadmin";
                p4.Password = "1234";
                p4.Port = "laperforce01:1666";
                p4.Connect();

                System.Diagnostics.Trace.WriteLine("Matchnet.ResourcePush.Service - p4.Connect()");


                string[] results = (string[])p4.run(command);

                System.Diagnostics.Trace.WriteLine("Matchnet.ResourcePush.Service - p4.run(" + command + ")");

                List<string> resultsGeneric = new List<string>();
                for (int i = 0; i < results.Length; i++)
                {
                    System.Diagnostics.Trace.WriteLine("Matchnet.ResourcePush.Service - " + results[i]);
                    resultsGeneric.Add(results[i]);
                }

                string[] errors = (string[])p4.Errors;
                List<string> errorsGeneric = new List<string>();
                for (int i = 0; i < errors.Length; i++)
                {
                    System.Diagnostics.Trace.WriteLine("Matchnet.ResourcePush.Service - " + errors[i]);
                    resultsGeneric.Add(errors[i]);
                }

                p4commandResults.Results = resultsGeneric;
                p4commandResults.Errors = errorsGeneric;

                p4.Disconnect();

                System.Diagnostics.Trace.WriteLine("Matchnet.ResourcePush.Service - p4.Disconnect()");
            }
            catch (Exception ex)
            {
                // This means search returned no results. P4COM just blows up... 
                if (ex.ToString().ToLower().IndexOf("no such file") != -1)
                {
                    p4commandResults.Results = new List<string>();
                    p4commandResults.Errors = new List<string>();

                    return p4commandResults;
                }
                else
                {
                    throw new Exception("Error executing a command against P4. Command=" + command, ex);
                }
            }

            return p4commandResults;
        }


        /// <summary>
        /// Gets the latest from the speicifed path recursively. 
        /// Also expects a file filter for files such as *.resx or *.img.
        /// </summary>
        /// <param name="">Example: //depot/REL1.8/Web/bedrock.matchnet.com/.../*.resx This grabs all resource files under the web solution</param>
        /// <returns></returns>
        public static List<P4File> GetLatest(string path)
        {
            P4CommandResults p4CommandResults = P4Wrapper.RunCommand(@"sync -f " + path);

            List<P4File> p4files = new List<P4File>(p4CommandResults.Results.Count);

            for (int i = 0; i < p4CommandResults.Results.Count; i++)
            {
                string result = Convert.ToString(p4CommandResults.Results[i]);

                string depotPath = result.Substring(0, result.IndexOf(@"#"));
                string localPath = result.Substring(result.ToLower().IndexOf(@"c:\matchnet"), result.Length - result.ToLower().IndexOf(@"c:\matchnet"));

                P4File p4File = new P4File(depotPath, localPath);

                p4files.Add(p4File);
            }

            return p4files;
        }

        /// <summary>
        /// Populates revision history of a given file.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static List<P4FileRevision> GetFileRevisions(P4File p4file)
        {
            P4CommandResults p4CommandResults = P4Wrapper.RunCommand(@"filelog -t " + p4file.DepotPath);

            // Remove the first line in the results, it's just the depot path.
            List<P4FileRevision> p4FileRevisions = new List<P4FileRevision>(p4CommandResults.Results.Count - 1);

            for (int i = 1; i < p4CommandResults.Results.Count; i++)
            {
                string result = System.Convert.ToString(p4CommandResults.Results[i]);

                if (result.IndexOf("branch from") == -1)
                {
                    System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(result, @"^#[1-9]+\s");

                    string revision = match.Value.ToString();

                    P4FileRevision p4FileRevision = new P4FileRevision(revision, result);

                    p4FileRevisions.Add(p4FileRevision);
                }
            }

            return p4FileRevisions;
        }
    }
}
