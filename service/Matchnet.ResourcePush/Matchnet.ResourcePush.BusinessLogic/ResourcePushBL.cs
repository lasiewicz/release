using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.FileDeploy.ServiceAdapters;
using Matchnet.FileDeploy.ValueObjects;
using Matchnet.ResourcePush.ValueObjects;

using Matchnet.P4Wrapper;
using Matchnet.P4Wrapper.ValueObjects;

namespace Matchnet.ResourcePush.BusinessLogic
{
    public class ResourcePushBL
    {
        #region Constructor
        public static ResourcePushBL Instance = new ResourcePushBL();
        public const string RESX_PATH_REMOVE_PREFIX = @"\Web\bedrock.matchnet.com";

        private ResourcePushBL()
        {
        }

        #endregion

        #region Public Methods

        public List<P4File> ResourceList(string matching)
        {
            matching = matching.Replace(@"*", @"");
            return P4Wrapper.P4Wrapper.GetLatest(@" //depot/.../*" + matching + @"*");
        }

        public List<P4FileRevision> GetResourceHistory(P4File p4File)
        {
            return P4Wrapper.P4Wrapper.GetFileRevisions(p4File);
        }

        public FileDeploymentResultCollection DeployFiles(DeployFileCollection deployFileCollection, ServerType serverType, bool silent)
        {
            FileCollection fileCollection = new FileCollection();
            FileDeploymentResultCollection result = new FileDeploymentResultCollection();

            foreach (DeployFile deployFile in deployFileCollection)
            {
                string absolutePath;
                string relativePath;

                switch (deployFile.FileType)
                {
                    case FileType.Resource:
                        P4Wrapper.P4Wrapper.RunCommand(@"sync -f " + deployFile.FileName + deployFile.Version);
                        absolutePath = deployFile.FileName;
                        relativePath = absolutePath.ToLower().Replace(@"c:\matchnet\bedrock\web\bedrock.matchnet.com", "");

                        try
                        {
                            new System.Xml.XmlDocument().Load(absolutePath);
                        }
                        catch (Exception ex)
                        {
                            absolutePath = string.Empty;
                            result.Add(new FileDeploymentResult(deployFile.FileName));
                            result[deployFile.FileName].Add(new DeploymentResult("n/a", DeploymentResultStatus.Failure, "Error loading XML: " + ex.Message));
                        }

                        break;
                    case FileType.Image:
                        P4Wrapper.P4Wrapper.RunCommand(@"sync -f " + deployFile.FileName + deployFile.Version);
                        absolutePath = deployFile.FileName;
                        relativePath = absolutePath.ToLower().Replace(@"c:\matchnet\bedrock", "");
                        break;

                    default:
                        absolutePath = string.Empty;
                        relativePath = string.Empty;
                        break;
                }

                if (absolutePath != string.Empty)
                {
                    fileCollection.Add(new File(absolutePath, relativePath, deployFile.FileType));
                }
            }

            string[] hostNames = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("RESOURCEPUSHSVC_FILEDEPLOY_HOST_NAMES").Split(',');

            foreach (string hostName in hostNames)
            {
                result.Append(FileDeploySA.Instance.DeployFiles(fileCollection, serverType, hostName));
            }

            log(deployFileCollection, result);

            if (!silent && (serverType & ServerType.Production) == ServerType.Production)
            {
                sendEmailAlert(serverType, result);
            }
            return result;
        }

        #endregion

        #region Private Methods

        private bool matchSiteResx(string fileName, Site site)
        {
            //Look for "\Web..." + SiteID + ".xx-XX.resx"
            return Regex.IsMatch(fileName, @"\\Web.*" + site.SiteID + @"\.[a-z]{2}-[A-Z]{2}\.resx", RegexOptions.IgnoreCase);
        }

        private void log(DeployFileCollection deployFileCollection, FileDeploymentResultCollection fileDeploymentResultCollection)
        {
            System.IO.StreamWriter sw = null;

            try
            {
                sw = new System.IO.StreamWriter(LogFilePath, true);
                string deployTime = DateTime.Now.ToString();
                foreach (DeployFile deployFile in deployFileCollection)
                {
                    StringBuilder status = new StringBuilder();

                    if (deployFile.FileType == FileType.Resource)
                    {
                        foreach (DeploymentResult result in fileDeploymentResultCollection[deployFile.FileName.ToLower().Replace(@"c:\matchnet\bedrock\web\bedrock.matchnet.com", "")])
                        {
                            if (result.Status == DeploymentResultStatus.Failure)
                            {
                                status.Append(" " + result.ErrorText);
                            }
                        }
                    }
                    else
                    {
                        foreach (DeploymentResult result in fileDeploymentResultCollection[deployFile.FileName.ToLower().Replace(@"c:\matchnet\bedrock", "")])
                        {
                            if (result.Status == DeploymentResultStatus.Failure)
                            {
                                status.Append(" " + result.ErrorText);
                            }
                        }
                    }

                    if (status.Length == 0)
                    {
                        status.Append("Success");
                    }

                    sw.WriteLine(deployTime + "," + P4Wrapper.P4Wrapper.GetUser() + "," + deployFile.FileName + "," + deployFile.Version + "," + status.ToString());
                }
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        private void sendEmailAlert(ServerType serverType, FileDeploymentResultCollection fileDeploymentResultCollection)
        {
            try
            {
                string From = "Spark.Deploy.Client@spark.net";
                string To = "release@spark.net";
                string Subject = "Files deployed to " + serverType.ToString();

                StringBuilder bodyBuilder = new StringBuilder();
                bodyBuilder.Append(P4Wrapper.P4Wrapper.GetUser() + " on " + Environment.MachineName + " has deployed the following files to " + serverType.ToString() + ":" + Environment.NewLine);
                foreach (FileDeploymentResult fileDeploymentResult in fileDeploymentResultCollection)
                {
                    bool success = true;

                    foreach (DeploymentResult result in fileDeploymentResult)
                    {
                        if (result.Status != DeploymentResultStatus.Success)
                        {
                            success = false;
                        }
                    }

                    bodyBuilder.Append(fileDeploymentResult.FileName + ": " + (success ? "Success" : "Failure") + Environment.NewLine);
                }
                bodyBuilder.Append(Environment.NewLine + "More detailed results are available in the error log at " + LogFilePath + ".");
                string Body = bodyBuilder.ToString();

                MailMessage message = new MailMessage(From, To, Subject, Body);
                SmtpClient client = new SmtpClient();
                client.Host = "relay.matchnet.com";
                client.Send(message);
            }
            catch (Exception ex)
            {
                throw (new Exception("Failure sending alert email.", ex));
            }
        }            

        #endregion

        #region Configuration

        private string LogFilePath
        {
            get
            {
                //TODO: setting
                return @"\\devapp01\c$\matchnet\ResourceDeployLog.txt";
            }
        }

        public string ImageRoot
        {
            get
            {
                string imageRoot = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_ROOT");
                return imageRoot.Remove(imageRoot.Length - @"\img".Length);
            }
        }

        #endregion
    }
}
