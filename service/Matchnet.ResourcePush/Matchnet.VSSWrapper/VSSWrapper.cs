using System;
using System.Collections.Generic;
using System.Text;

using SourceSafeTypeLib;
using System.Collections;

using Matchnet.Caching;
using Matchnet.VSSWrapper.ValueObjects;

namespace Matchnet.VSSWrapper
{
    public class VSSWrapper
    {
        private static Cache _cache = Cache.Instance;
        private static string _vssPath = @"\\devvss01\VSS\srcsafe.ini";

        public static VSSFileCollection GetAllWebFiles(AuthInfo authInfo)
        {
            return GetAllFiles("$/Web", @"\Web", authInfo);
        }

        public static VSSFileCollection GetAllFiles(AuthInfo authInfo)
        {
            return GetAllFiles("$", string.Empty, authInfo);
        }

        public static VSSFileCollection GetAllFiles(string vssRoot, string fileRoot, AuthInfo authInfo)
        {
            VSSFileCollection result = _cache.Get(vssRoot) as VSSFileCollection;

            if (result == null)
            {
                result = new VSSFileCollection(vssRoot);
                
                VSSItem root = getVSSDatabase(authInfo).get_VSSItem(vssRoot, false);
                getAllFiles(root, fileRoot, result);

                _cache.Insert(result);
            }

            return result;
        }

        private static void getAllFiles(VSSItem root, string filePath, VSSFileCollection fileNames)
        {
            if (root.Type == (Int32) VSSItemType.VSSITEM_FILE)
            {
                fileNames.Add(filePath);
                return;
            }

            IVSSItems childItems = root.get_Items(false);

            foreach (VSSItem item in childItems)
            {
                getAllFiles(item, filePath + @"\" + item.Name, fileNames);
            }
        }

        public static FileHistoryCollection GetHistory(string fileName, AuthInfo authInfo)
        {
            FileHistoryCollection result = new FileHistoryCollection();
            IVSSDatabase database = getVSSDatabase(authInfo);
            VSSItem item = database.get_VSSItem(fileName, false);

            foreach (IVSSVersion version in item.get_Versions(0))
            {
                bool isLabel = version.Label != string.Empty;
                string comment = isLabel ? version.Label : version.Comment;

                result.Add(new FileHistory(version.VersionNumber, version.Date, version.Username, comment, isLabel));
            }

            return result;
        }

        public static string GetFile(string fileName, Int32 version, AuthInfo authInfo)
        {
            IVSSDatabase database = getVSSDatabase(authInfo);
            VSSItem item = database.get_VSSItem(fileName, false);
            VSSItem versionItem = item.get_Version(version);

            string local = null;
            versionItem.Get(ref local, 0);
            return local;
        }

        private static IVSSDatabase getVSSDatabase(AuthInfo authInfo)
        {
            IVSSDatabase database = new VSSDatabase();

            try
            {
                database.Open(_vssPath, authInfo.Username, authInfo.Password);
            }
            catch (Exception ex)
            {
                throw new Exception("Error opening database (vssPath: " + _vssPath + ", username: " + authInfo.Username + ").", ex);
            }

            return database;
        }
    }
}
