using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.ResourcePush.ValueObjects
{
    [Serializable]
    public class ResourceFile
    {
        private string _fileName;

        public ResourceFile(string fileName)
        {
            _fileName = fileName;
        }

        public string FileName
        {
            get
            {
                return _fileName;
            }
        }

        public override string ToString()
        {
            return _fileName;
        }
    }
}
