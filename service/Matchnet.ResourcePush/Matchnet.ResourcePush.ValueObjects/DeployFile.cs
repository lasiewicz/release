using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.FileDeploy.ValueObjects;

namespace Matchnet.ResourcePush.ValueObjects
{
    [Serializable]
    public class DeployFile
    {
        private string _fileName;
        private string _version;
        private FileType _fileType;

        public DeployFile(string fileName, string version, FileType fileType)
        {
            _fileName = fileName;
            _version = version;
            _fileType = fileType;
        }

        public string FileName
        {
            get
            {
                return _fileName;
            }
        }

        public string Version
        {
            get
            {
                return _version;
            }
        }

        public FileType FileType
        {
            get
            {
                return _fileType;
            }
        }

        public override string ToString()
        {
            return _fileName + "," + _version + "," + _fileType;
        }
    }
}
