using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.FileDeploy.ValueObjects;
using Matchnet.VSSWrapper.ValueObjects;

using Matchnet.P4Wrapper.ValueObjects;

namespace Matchnet.ResourcePush.ValueObjects
{
    public interface IResourcePushService
    {
        List<P4File> ResourceList(string matching);
        List<P4FileRevision> GetResourceHistory(P4File p4File);
        FileDeploymentResultCollection DeployFiles(DeployFileCollection deployFileCollection, ServerType serverType, bool silent);
    }
}
