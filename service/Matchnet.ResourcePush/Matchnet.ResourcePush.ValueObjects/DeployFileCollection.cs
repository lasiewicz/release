using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.ResourcePush.ValueObjects
{
    [Serializable]
    public class DeployFileCollection : IEnumerable
    {
        private ArrayList _deployFiles;

        public DeployFileCollection()
        {
            _deployFiles = new ArrayList();
        }

        public void Add(DeployFile deployFile)
        {
            _deployFiles.Add(deployFile);
        }

        public void Remove(DeployFile deployFile)
        {
            _deployFiles.Remove(deployFile);
        }

        public int Count
        {
            get { return _deployFiles.Count; }
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _deployFiles.GetEnumerator();
        }

        #endregion

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (DeployFile deployFile in _deployFiles)
            {
                sb.Append(deployFile.ToString() + Environment.NewLine);
            }

            return sb.ToString();
        }
    }
}
