using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Matchnet.ResourcePush.ValueObjects
{
    [Serializable]
    public class ResourceFileCollection : IEnumerable
    {
        private Hashtable _resourceFiles;

        public ResourceFileCollection()
        {
            _resourceFiles = new Hashtable();
        }

        public void Add(ResourceFile resourceFile)
        {
            _resourceFiles.Add(resourceFile.FileName, resourceFile);
        }
        
        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _resourceFiles.Values.GetEnumerator();
        }

        #endregion
    }
}
