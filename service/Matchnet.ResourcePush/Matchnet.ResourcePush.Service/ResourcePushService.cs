using System;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.ResourcePush.ServiceManagers;
using Matchnet.ResourcePush.ValueObjects;
using Matchnet.RemotingServices;

namespace Matchnet.ResourcePush.Service
{
    public class ResourcePushService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private System.ComponentModel.Container components = null;
        private ResourcePushSM _ResourcePushSM = null;

        public ResourcePushService()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }


        protected override void RegisterServiceManagers()
        {
            try
            {
                _ResourcePushSM = new ResourcePushSM();
                base.RegisterServiceManager(_ResourcePushSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service, see details:" + ex.Message);
            }
        }
    }
}
