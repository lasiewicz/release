using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Caching;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.FileDeploy.ValueObjects;
using Matchnet.RemotingClient;
using Matchnet.ResourcePush.ValueObjects;
using Matchnet.Configuration.ValueObjects.Servers;

using Matchnet.P4Wrapper.ValueObjects;

namespace Matchnet.ResourcePush.ServiceAdapters
{
    public class ResourcePushSA : SABase
    {
        public const string SERVICE_MANAGER_NAME = "ResourcePushSM";

        public static ResourcePushSA Instance = new ResourcePushSA();

        private Cache _cache = Cache.Instance;

        private ResourcePushSA()
        {
        }

        /// <summary>
        /// Retrieves P4 files based on the matching argument.
        /// </summary>
        /// <param name = param name="matching">Matching string. i.e. 103, .resx</param>
        /// <returns></returns>
        public List<P4File> ResourceList(string matching)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    return getService(uri).ResourceList(matching);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve list of resx files (uri: " + uri + ")", ex));
            }
        }

        public List<P4FileRevision> GetResourceHistory(P4File p4File)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetResourceHistory(p4File);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve resource history (uri: " + uri + ")", ex));
            }
        }

        public FileDeploymentResultCollection DeployFiles(DeployFileCollection deployFileCollection, ServerType serverType)
        {
            return DeployFiles(deployFileCollection, serverType, false);
        }

        public FileDeploymentResultCollection DeployFiles(DeployFileCollection deployFileCollection, ServerType serverType, bool silent)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    return getService(uri).DeployFiles(deployFileCollection, serverType, silent);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot deploy files (uri: " + uri + ")", ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("RESOURCEPUSHSVC_SA_CONNECTION_LIMIT"));
        }

        private string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.ResourcePush.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("RESOURCEPUSHSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private IResourcePushService getService(string uri)
        {
            try
            {
                return (IResourcePushService)Activator.GetObject(typeof(IResourcePushService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
