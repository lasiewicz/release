using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.Exceptions;
using Matchnet.ResourcePush.BusinessLogic;
using Matchnet.ResourcePush.ValueObjects;

using Matchnet.P4Wrapper.ValueObjects;

namespace Matchnet.ResourcePush.ServiceManagers
{
    public class ResourcePushSM : MarshalByRefObject, IServiceManager, IResourcePushService, IDisposable
    {
        #region Private Variables
        private const string SERVICE_MANAGER_NAME = "ResourcePushSM";
        #endregion

        #region Constructors
        public ResourcePushSM()
        {
        }

        #endregion

        #region IResourcePushService Implementation

        public List<P4File> ResourceList(string matching)
        {
            try
            {
                return ResourcePushBL.Instance.ResourceList(matching);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving list of resx files.", ex);
            }
        }

        public List<P4FileRevision> GetResourceHistory(P4File p4File)
        {
            try
            {
                return ResourcePushBL.Instance.GetResourceHistory(p4File);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving file history.", ex);
            }
        }

        public Matchnet.FileDeploy.ValueObjects.FileDeploymentResultCollection DeployFiles(DeployFileCollection deployFileCollection, ServerType serverType, bool silent)
        {
            try
            {
                return ResourcePushBL.Instance.DeployFiles(deployFileCollection, serverType, silent);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deploying files.", ex);
            }
        }

        #endregion


        public override object InitializeLifetimeService()
        {
            return null;
        }


        public void Dispose()
        {
        }


        public void PrePopulateCache()
        {
            // no implementation at this time
        }
    }
}
