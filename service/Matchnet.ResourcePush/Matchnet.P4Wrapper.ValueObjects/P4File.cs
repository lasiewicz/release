﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.P4Wrapper.ValueObjects
{
    [Serializable]
    public class P4File
    {
        private string depotPath;
        private string localPath;

        public string DepotPath
        {
            get
            {
                return this.depotPath;
            }
            set
            {
                this.depotPath = value;
            }
        }

        public string LocalPath
        {
            get
            {
                return this.localPath;
            }
            set
            {
                this.localPath = value;
            }
        }

        public P4File(string depotPath, string localPath)
        {
            this.depotPath = depotPath;
            this.localPath = localPath;
        }
    }
}
