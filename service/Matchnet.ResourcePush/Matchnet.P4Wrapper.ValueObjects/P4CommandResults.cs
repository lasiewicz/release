﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.P4Wrapper.ValueObjects
{
    /// <summary>
    /// Used for stoing results and errors from P4 commands.
    /// </summary>
    [Serializable]
    public class P4CommandResults
    {
        List<string> results;
        List<string> errors;

        public List<string> Results
        {
            get
            {
                return this.results;
            }
            set
            {
                this.results = value;
            }
        }

        public List<string> Errors
        {
            get
            {
                return this.errors;
            }
            set
            {
                this.errors = value;
            }
        }
    }
}
