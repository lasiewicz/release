﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.P4Wrapper.ValueObjects
{
    [Serializable]
    public class P4FileRevision
    {
        private string revision;
        private string description;

        public string Revision
        {
            set
            {
                this.revision = value;
            }
            get
            {
                return this.revision;
            }
        }

        public string Description
        {
            set
            {
                this.description = value;
            }
            get
            {
                return this.description;
            }
        }

        public P4FileRevision(string revision, string description)
        {
            this.revision = revision;
            this.description = description;
        }
    }
}
