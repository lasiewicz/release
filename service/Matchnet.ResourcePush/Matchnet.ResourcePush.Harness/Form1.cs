using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ResourcePush.ServiceAdapters;
using Matchnet.ResourcePush.ValueObjects;
using Matchnet.VSSWrapper.ValueObjects;
using Matchnet.FileDeploy.ValueObjects;

using Matchnet.P4Wrapper.ValueObjects;
using Matchnet.ResourcePush.BusinessLogic;

namespace Matchnet.ResourcePush.Harness
{
    public partial class Form1 : Form
    {
        private DeployFileCollection _deployFileCollection = new DeployFileCollection();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbServerTypes.Items.Add(ServerType.Staging);
            cbServerTypes.Items.Add(ServerType.Production);
            cbServerTypes.Items.Add(ServerType.ContentStaging);
            cbServerTypes.SelectedIndex = 0;

            listBox1.DisplayMember = "DepotPath";
            listBox1.Sorted = true;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Sorted = true;
            listBox2.DisplayMember = "Description";
            listBox2.Items.Clear();

            if (UseBLCheckBox.Checked)
            {
                foreach (P4FileRevision history in ResourcePushBL.Instance.GetResourceHistory(listBox1.SelectedItem as P4File))
                {
                    listBox2.Items.Add(history);
                }
            }
            else
            {
                foreach (P4FileRevision history in ResourcePushSA.Instance.GetResourceHistory(listBox1.SelectedItem as P4File))
                {
                    listBox2.Items.Add(history);
                }
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            P4File p4File = listBox1.SelectedItem as P4File;
            P4FileRevision p4FileRevision = listBox2.SelectedItem as P4FileRevision;

            if (p4File.DepotPath.ToLower().IndexOf(".resx") > -1)
            {
                _deployFileCollection.Add(new DeployFile(p4File.LocalPath, p4FileRevision.Revision, FileType.Resource));
            }
            else
            {
                _deployFileCollection.Add(new DeployFile(p4File.LocalPath, p4FileRevision.Revision, FileType.Image));
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            _deployFileCollection = new DeployFileCollection();
            tvResults.Nodes.Clear();
        }

        private void btnDeploy_Click(object sender, EventArgs e)
        {
            ServerType serverType = ServerType.Web | (ServerType)cbServerTypes.SelectedItem;
            FileDeploymentResultCollection results;

            if (UseBLCheckBox.Checked)
            {
                results = ResourcePushBL.Instance.DeployFiles(_deployFileCollection, serverType, cbSilent.Checked);
            }
            else
            {
                results = ResourcePushSA.Instance.DeployFiles(_deployFileCollection, serverType, cbSilent.Checked);
            }

            foreach (DeployFile deployFile in _deployFileCollection)
            {
                TreeNode fileNode = tvResults.Nodes.Add(deployFile.FileName);

                string fileRootPath = "";

                if (deployFile.FileType == FileType.Resource)
                {
                    fileRootPath = @"c:\matchnet\bedrock\web\bedrock.matchnet.com";
                }
                else
                {
                    fileRootPath = @"c:\matchnet\bedrock";
                }
                if (results[deployFile.FileName.ToLower().Replace(fileRootPath,"")] == null)
                {
                    fileNode.Nodes.Add("Error.  Did not receive FileDeploymentResult for this file");
                }
                else
                {
                    foreach (DeploymentResult result in results[deployFile.FileName.ToLower().Replace(fileRootPath, "")])
                    {
                        TreeNode resultNode = fileNode.Nodes.Add(result.ServerName + ": " + result.Status);
                        if (result.ErrorText != string.Empty)
                        {
                            resultNode.Nodes.Add(result.ErrorText);
                        }
                    }
                }
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            List<P4File> p4Files;

            if (UseBLCheckBox.Checked)
            {
                p4Files = ResourcePushBL.Instance.ResourceList(SearchTextBox.Text.Trim());
            }
            else
            {
                p4Files = ResourcePushSA.Instance.ResourceList(SearchTextBox.Text.Trim());
            }

            foreach (P4File p4File in p4Files)
            {
                listBox1.Items.Add(p4File);
            }
        }
    }
}