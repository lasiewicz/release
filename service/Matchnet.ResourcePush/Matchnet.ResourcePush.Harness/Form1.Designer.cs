namespace Matchnet.ResourcePush.Harness
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnDeploy = new System.Windows.Forms.Button();
            this.tvResults = new System.Windows.Forms.TreeView();
            this.cbServerTypes = new System.Windows.Forms.ComboBox();
            this.cbSilent = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.UseBLCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(16, 60);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(673, 238);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.HorizontalScrollbar = true;
            this.listBox2.Location = new System.Drawing.Point(16, 328);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(673, 147);
            this.listBox2.TabIndex = 1;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(324, 675);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(79, 38);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnDeploy
            // 
            this.btnDeploy.Location = new System.Drawing.Point(193, 674);
            this.btnDeploy.Name = "btnDeploy";
            this.btnDeploy.Size = new System.Drawing.Size(99, 39);
            this.btnDeploy.TabIndex = 3;
            this.btnDeploy.Text = "Deploy";
            this.btnDeploy.UseVisualStyleBackColor = true;
            this.btnDeploy.Click += new System.EventHandler(this.btnDeploy_Click);
            // 
            // tvResults
            // 
            this.tvResults.Location = new System.Drawing.Point(16, 510);
            this.tvResults.Name = "tvResults";
            this.tvResults.Size = new System.Drawing.Size(673, 143);
            this.tvResults.TabIndex = 4;
            // 
            // cbServerTypes
            // 
            this.cbServerTypes.FormattingEnabled = true;
            this.cbServerTypes.Location = new System.Drawing.Point(352, 10);
            this.cbServerTypes.Name = "cbServerTypes";
            this.cbServerTypes.Size = new System.Drawing.Size(114, 21);
            this.cbServerTypes.TabIndex = 6;
            // 
            // cbSilent
            // 
            this.cbSilent.AutoSize = true;
            this.cbSilent.Location = new System.Drawing.Point(496, 12);
            this.cbSilent.Name = "cbSilent";
            this.cbSilent.Size = new System.Drawing.Size(52, 17);
            this.cbSilent.TabIndex = 7;
            this.cbSilent.Text = "Silent";
            this.cbSilent.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Perforce Files";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 312);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Perforce File History";
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Location = new System.Drawing.Point(16, 10);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(237, 20);
            this.SearchTextBox.TabIndex = 11;
            this.SearchTextBox.Text = "103";
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(259, 10);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 12;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // UseBLCheckBox
            // 
            this.UseBLCheckBox.AutoSize = true;
            this.UseBLCheckBox.Checked = true;
            this.UseBLCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UseBLCheckBox.Location = new System.Drawing.Point(564, 12);
            this.UseBLCheckBox.Name = "UseBLCheckBox";
            this.UseBLCheckBox.Size = new System.Drawing.Size(61, 17);
            this.UseBLCheckBox.TabIndex = 13;
            this.UseBLCheckBox.Text = "Use BL";
            this.UseBLCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 725);
            this.Controls.Add(this.UseBLCheckBox);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.SearchTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbSilent);
            this.Controls.Add(this.cbServerTypes);
            this.Controls.Add(this.tvResults);
            this.Controls.Add(this.btnDeploy);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDeploy;
        private System.Windows.Forms.TreeView tvResults;
        private System.Windows.Forms.ComboBox cbServerTypes;
        private System.Windows.Forms.CheckBox cbSilent;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.CheckBox UseBLCheckBox;
    }
}

