using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Reflection;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Spark.Logging;
using DBQ = Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.SearchEngine.ServiceAdaptersNRT;


namespace Matchnet.Member.BusinessLogic
{
    public class MemberUpdateNotifier
    {
        public enum BulkMailType
        {
            MatchMail = 1,
            ViralMail = 2,
            NewMemberMail = 3
        }

        private ISettingsSA _settingsService;
        public ISettingsSA SettingsService
        {
            get
            {
                if (_settingsService == null)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }


        private const Int32 COMMUNITYID_FL = 9;

        private CachedMember _cachedMember;
        private MemberUpdate _memberUpdate;
        private ArrayList _communityIDList = null;
        private ArrayList _siteIDList = null;
        private Brands _brands = null;
        private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes _attributes = null;
        private Hashtable _searchPointers;
        private bool _searchAllCommunities = false;
        private ArrayList _textQueueItems;
        private bool _statusMaskUpdated;
        private ArrayList _matchCommunities;
        private ArrayList _viralCommunities;
        private ArrayList _newMemberCommunities;
        private Int32[] _unapprovedLanguages;
        private int _lastSetAttributeCommunityID = Constants.NULL_INT;
        private bool _memberSuspended = false;
        private bool _memberSuspendedOrSelfSuspended = false;
        private bool _memberUnSuspendedOrUnSelfSuspended = false;
        List<DBQ.QueueItemText> _dbqTextQueueItems;
        private Dictionary<int, Spark.SearchEngine.ValueObjects.SearchMemberUpdate> _searchMemberUpdateList = new Dictionary<int, Spark.SearchEngine.ValueObjects.SearchMemberUpdate>();
        private List<QAMemberInfoUpdate> _QuestionAnswerMemberInfoUpdateList = new List<QAMemberInfoUpdate>();

        public List<DBQ.QueueItemText> TextQueueItems { get { return _dbqTextQueueItems; } }

        public MemberUpdateNotifier(ref CachedMember cachedMember,
            MemberUpdate memberUpdate,
            Int32[] unapprovedLanguages)
        {
            _cachedMember = cachedMember;
            _memberUpdate = memberUpdate;
            _searchPointers = new Hashtable();
            _textQueueItems = new ArrayList();
            _matchCommunities = new ArrayList();
            _viralCommunities = new ArrayList();
            _newMemberCommunities = new ArrayList();
            _unapprovedLanguages = unapprovedLanguages;
            _dbqTextQueueItems = new List<DBQ.QueueItemText>();

            IDictionaryEnumerator deN = _memberUpdate.AttributesDate.GetEnumerator();

            while (deN.MoveNext())
            {
                SetAttributeDate(Attributes.GetAttributeGroup((Int32)deN.Key),
                    (DateTime)deN.Value);
            }

            deN = _memberUpdate.AttributesText.GetEnumerator();
            while (deN.MoveNext())
            {
                Int32 languageID = (Int32)deN.Key;
                Hashtable attributesText = deN.Value as Hashtable;
                IDictionaryEnumerator deA = attributesText.GetEnumerator();
                while (deA.MoveNext())
                {
                    SetAttributeText(Attributes.GetAttributeGroup((Int32)deA.Key),
                        ((TextValue)deA.Value),
                        languageID);
                }
            }

            deN = _memberUpdate.AttributesInt.GetEnumerator();
            while (deN.MoveNext())
            {
                SetAttributeInt(Attributes.GetAttributeGroup((Int32)deN.Key),
                    (Int32)deN.Value);
            }

        }


        public void SendNotifications()
        {
            //We'll use this setting to determine if we should write to the new BulkMail table or the old MatchMail table
            //this can be removed once we've completely migrated to the new tables. 
            bool UseBulkMailTables = Conversion.CBool(RuntimeSettings.GetSetting("BULKMAIL_USE_NEW_TABLES"));

            #region mnAdmin
            if (_memberUpdate.EmailAddress != Constants.NULL_STRING ||
                _memberUpdate.Username != Constants.NULL_STRING ||
                _statusMaskUpdated)
            {
                if (_cachedMember.EmailAddress == Constants.NULL_STRING)
                {
                    new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME,
                        "Unable to update mnAdmin (memberID: " + _cachedMember.MemberID.ToString() + ", emailAddress: " + _cachedMember.EmailAddress + ", username: " + _cachedMember.Username + ")");
                }
                else
                {
                    // PM-218
                    int groupID = Constants.NULL_INT;
                    int attributeGroupID = Constants.NULL_INT;
                    int languageID = Constants.NULL_INT;

                    // PM-218 Remove after deployed.
                    if (AttributeMetadataSA.Instance.GetAttributes().GetAttribute("Username").Scope
                        != ScopeType.Community)
                    {
                        groupID = CachedMember.GROUP_PERSONALS;
                    }
                    else
                    {
                        IDictionaryEnumerator languages = _memberUpdate.AttributesText.GetEnumerator();
                        while (languages.MoveNext())
                        {
                            languageID = (Int32)languages.Key;
                            Hashtable attributesText = languages.Value as Hashtable;
                            IDictionaryEnumerator attributeGroupIDs = attributesText.GetEnumerator();
                            while (attributeGroupIDs.MoveNext())
                            {
                                ArrayList usernameAttributeGroupIDs =
                                    Attributes.GetAttributeGroupIDs(CachedMember.ATTRIBUTEID_USERNAME);

                                if (usernameAttributeGroupIDs.Contains(attributeGroupIDs.Key))
                                {
                                    groupID = Attributes.GetAttributeGroup((int)attributeGroupIDs.Key).GroupID;
                                    attributeGroupID = (int)attributeGroupIDs.Key;
                                    break;
                                }
                            }
                        }
                    }

                    Command commandAdmin = new Command("mnAdmin", "dbo.up_Member_Save", 0);
                    commandAdmin.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                    commandAdmin.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, _cachedMember.EmailAddress);
                    commandAdmin.AddParameter("@GlobalStatusMask", SqlDbType.Int, ParameterDirection.Input, _cachedMember.GetAttributeInt(CachedMember.AG_GLOBALSTATUSMASK, 0));
                    commandAdmin.AddParameter("@AdminFlag", SqlDbType.Bit, ParameterDirection.Input, 0);
                    commandAdmin.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                    if (groupID != CachedMember.GROUP_PERSONALS)
                    {
                        TextStatusType textStatusType;
                        commandAdmin.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                        commandAdmin.AddParameter("@Username", SqlDbType.NVarChar, ParameterDirection.Input,
                            _cachedMember.GetAttributeText(attributeGroupID, languageID, _cachedMember.MemberID.ToString(), out textStatusType));
                    }
                    else
                    {
                        commandAdmin.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, CachedMember.GROUP_PERSONALS);
                        commandAdmin.AddParameter("@Username", SqlDbType.NVarChar, ParameterDirection.Input, _cachedMember.Username);
                    }
                    Client.Instance.ExecuteAsyncWrite(commandAdmin);
                }
            }
            #endregion

            #region mnAlert
            for (Int32 i = 0; i < _matchCommunities.Count; i++)
            {
                Int32 communityID = (Int32)_matchCommunities[i];
                Command commandMatch;
                if (communityID != COMMUNITYID_FL)
                {
                    AttributeGroup attributeGroup = Attributes.GetAttributeGroup(communityID, CachedMember.ATTRIBUTEID_MATCHNEWSLETTERPERIOD);
                    Int32 frequency = _cachedMember.GetAttributeInt(attributeGroup.ID, Convert.ToInt32(attributeGroup.DefaultValue));

                    if (UseBulkMailTables)
                    {
                        if (_cachedMember.IsEligibleForEmail(Attributes, communityID) && frequency > 0)
                        {
                            commandMatch = new Command("mnAlert", "dbo.up_BulkMailSchedule_Save", 0);
                            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                            commandMatch.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)BulkMailType.MatchMail);
                            commandMatch.AddParameter("@Frequency", SqlDbType.Int, ParameterDirection.Input, frequency);
                            commandMatch.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                            Client.Instance.ExecuteAsyncWrite(commandMatch);
                        }
                        else
                        {
                            commandMatch = new Command("mnAlert", "dbo.up_BulkMailSchedule_Delete", 0);
                            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                            commandMatch.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)BulkMailType.MatchMail);
                            Client.Instance.ExecuteAsyncWrite(commandMatch);
                        }
                    }
                    else
                    {
                        if (_cachedMember.IsEligibleForEmail(Attributes, communityID) && frequency > 0)
                        {
                            commandMatch = new Command("mnAlert", "dbo.up_MatchMailSchedule_Save", 0);
                            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                            commandMatch.AddParameter("@Frequency", SqlDbType.Int, ParameterDirection.Input, frequency);
                            commandMatch.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                            Client.Instance.ExecuteAsyncWrite(commandMatch);
                        }
                        else
                        {
                            commandMatch = new Command("mnAlert", "dbo.up_MatchMailSchedule_Delete", 0);
                            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                            Client.Instance.ExecuteAsyncWrite(commandMatch);
                        }
                    }
                }
            }

            //New Member Email can only use the bulk mail tables, so don't write out to them if they're not enabled yet
            if (UseBulkMailTables)
            {
                //mnAlert - new member email
                for (Int32 i = 0; i < _newMemberCommunities.Count; i++)
                {
                    Int32 communityID = (Int32)_newMemberCommunities[i];
                    if (communityID != COMMUNITYID_FL)
                    {
                        AttributeGroup attributeGroup = Attributes.GetAttributeGroup(communityID, CachedMember.ATTRIBUTEID_NEWMEMBEREMAILPERIOD);
                        Int32 frequency = _cachedMember.GetAttributeInt(attributeGroup.ID, Convert.ToInt32(attributeGroup.DefaultValue));

                        if (_cachedMember.IsEligibleForEmail(Attributes, communityID) && frequency > 0)
                        {
                            Command commandMatch = new Command("mnAlert", "dbo.up_BulkMailSchedule_Save", 0);
                            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                            commandMatch.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)BulkMailType.NewMemberMail);
                            commandMatch.AddParameter("@Frequency", SqlDbType.Int, ParameterDirection.Input, frequency);
                            commandMatch.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                            Client.Instance.ExecuteAsyncWrite(commandMatch);
                        }
                        else
                        {
                            Command commandMatch = new Command("mnAlert", "dbo.up_BulkMailSchedule_Delete", 0);
                            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                            commandMatch.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)BulkMailType.NewMemberMail);
                            Client.Instance.ExecuteAsyncWrite(commandMatch);
                        }
                    }
                }
            }

            //mnAlert - viral mail
            for (Int32 i = 0; i < _viralCommunities.Count; i++)
            {
                Int32 communityID = (Int32)_viralCommunities[i];
                if (communityID != COMMUNITYID_FL)
                {
                    AttributeGroup attributeGroup = Attributes.GetAttributeGroup(CachedMember.GROUP_PERSONALS, CachedMember.ATTRIBUTEID_GOTACLICKEMAILPERIOD);
                    Int32 frequency = _cachedMember.GetAttributeInt(attributeGroup.ID, Convert.ToInt32(attributeGroup.DefaultValue));

                    if (_cachedMember.IsEligibleForEmail(Attributes, communityID) && frequency > 0)
                    {
                        Command commandViral = new Command("mnAlert", "dbo.up_ViralMailSchedule_Save", 0);
                        commandViral.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                        commandViral.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                        commandViral.AddParameter("@Frequency", SqlDbType.Int, ParameterDirection.Input, frequency);
                        commandViral.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                        Client.Instance.ExecuteAsyncWrite(commandViral);
                    }
                    else
                    {
                        Command commandViral = new Command("mnAlert", "dbo.up_ViralMailSchedule_Delete", 0);
                        commandViral.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
                        commandViral.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                        Client.Instance.ExecuteAsyncWrite(commandViral);
                    }
                }
            }
            #endregion

            #region TextApproval
            if (_textQueueItems.Count > 0 || _dbqTextQueueItems.Count > 0)
            {
                List<DBQ.QueueItemBase> baseItems = new List<DBQ.QueueItemBase>();
                foreach (DBQ.QueueItemText queueItem in _dbqTextQueueItems)
                {
                    baseItems.Add((DBQ.QueueItemBase)queueItem);
                }

                DBApproveQueueSA.Instance.Enqueue(baseItems);

                //Insert Text Attributes. This is to store response from CRX.
                var textItems = _textQueueItems.Cast<DBQ.QueueItemText>().ToList();
                DBApproveQueueSA.Instance.SaveMemberTextApprovalAttributes(textItems);
            }
            #endregion

            #region e2 searcher
            if (_searchMemberUpdateList.Count > 0)
            {
                foreach (Spark.SearchEngine.ValueObjects.SearchMemberUpdate smu in _searchMemberUpdateList.Values)
                {
                    if (smu != null)
                    {
                        RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberUpdateNotifier.SendNotifications()", String.Format("Calling E2 NRT for memberID:{0}, communityID:{1}, update reason: {2}", smu.MemberID, smu.CommunityID, smu.UpdateReason), null);

                        try
                        {
                            if (smu.UpdateMode == Spark.SearchEngine.ValueObjects.UpdateModeEnum.remove)
                            {
                                if (smu.UpdateReason == Spark.SearchEngine.ValueObjects.UpdateReasonEnum.adminSuspend)
                                {
                                    if (CommunityIDList != null)
                                    {
                                        foreach (int communityID in CommunityIDList)
                                        {
                                            smu.CommunityID = communityID;
                                            SearcherNRTSA.Instance.NRTRemoveMember(smu);
                                        }
                                    }
                                }
                                else
                                {
                                    SearcherNRTSA.Instance.NRTRemoveMember(smu);
                                }
                            }
                            else
                            {
                                if (smu.UpdateReason == Spark.SearchEngine.ValueObjects.UpdateReasonEnum.adminUnsuspend)
                                {
                                    if (CommunityIDList != null)
                                    {
                                        foreach (int communityID in CommunityIDList)
                                        {
                                            smu.CommunityID = communityID;
                                            SearcherNRTSA.Instance.NRTUpdateMember(smu, getLastBrandID(smu.CommunityID), _cachedMember);
                                        }
                                    }
                                }
                                else
                                {
                                    SearcherNRTSA.Instance.NRTUpdateMember(smu, getLastBrandID(smu.CommunityID), _cachedMember);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberUpdateNotifier.SendNotifications()", String.Format("Error calling E2 NRT for memberID:{0}, communityID:{1}, update reason: {2}. Error: {3}", smu.MemberID, smu.CommunityID, smu.UpdateReason, ex.ToString()), null);
                            ServiceBoundaryException sbe = new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("MemberUpdateNotifier.SendNotifications() Error calling E2 NRT for memberID:{0}, communityID:{1}, update reason: {2}", smu.MemberID, smu.CommunityID, smu.UpdateReason), ex);
                        }
                    }
                }
            }
            #endregion

            #region Question and Answer
            if (_QuestionAnswerMemberInfoUpdateList.Count > 0)
            {
                foreach (QAMemberInfoUpdate memberInfoUpdate in _QuestionAnswerMemberInfoUpdateList)
                {
                    UpdateAnswerMemberInfo(memberInfoUpdate);
                }
            }
            #endregion
        }

        /// <summary>
        /// Remove all API access tokens of the member being admin suspended
        /// </summary>
        private void RemoveAppMembers()
        {
            try
            {
                Spark.API.ServiceAdapters.APISA.Instance.DeleteAppMembers(_memberUpdate.MemberID);
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT,
                                                          MethodBase.GetCurrentMethod().Name,
                                                          string.Format("Removed API app member records. memberId:{0}",
                                                                        _memberUpdate.MemberID), null);
            }
            catch (Exception exception)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT,
                                                        MethodBase.GetCurrentMethod().Name, exception,
                                                        _memberUpdate.MemberID);
            }
        }

        private void updatePhotoSearch(MemberUpdate update, CachedMember cachedMember, ArrayList communityids)
        {
            try
            {
                Matchnet.PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.UpdateMember(update, cachedMember, communityids);

            }
            catch (Exception ex)
            { System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.MemberBL", "Error doing PhotoUpdate:" + ex.Message, System.Diagnostics.EventLogEntryType.Warning); }

        }

        private void hidePhotos(CachedMember cachedMember, int communityId)
        {
            try
            {
                List<int> communityIDs = (communityId != Constants.NULL_INT
                                              ? new List<int>() {communityId}
                                              : cachedMember.Photos.CommunityIds.Cast<int>().ToList());


                foreach (int c in communityIDs)
                {
                    var photoCommunity = cachedMember.Photos.GetCommunity(c);
                    var siteID = Brands.GetDefaultSiteIDForCommunity(c);

                    if (photoCommunity != null && photoCommunity.Count > 0)
                    {
                        for (int i = 0; i < photoCommunity.Count; i++)
                        {
                            Matchnet.File.ServiceAdapters.FileSA.Instance.HideFile(
                                photoCommunity[(byte) i].FileWebPath, photoCommunity[(byte) i].FileCloudPath, c, siteID);
                            if (!string.IsNullOrEmpty(photoCommunity[(byte) i].ThumbFileWebPath))
                            {
                                Matchnet.File.ServiceAdapters.FileSA.Instance.HideFile(
                                    photoCommunity[(byte) i].ThumbFileWebPath,
                                    photoCommunity[(byte) i].ThumbFileCloudPath, c, siteID);
                            }
                            if (photoCommunity[(byte) i].Files == null) continue;
                            foreach (var photoFile in photoCommunity[(byte) i].Files)
                            {
                                if (photoFile.PhotoFileType != PhotoFileType.Original)
                                {
                                    Matchnet.File.ServiceAdapters.FileSA.Instance.HideFile(photoFile.FileWebPath,
                                                                                           photoFile.FileCloudPath, c,
                                                                                           siteID);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT,
                    "MemberUpdateNotifier.hidePhotos", ex, cachedMember.MemberID);
            }
        }

        private void unHidePhotos(CachedMember cachedMember, int communityId)
        {
            try
            {
                List<int> communityIDs = (communityId != Constants.NULL_INT
                                              ? new List<int>() {communityId}
                                              : cachedMember.Photos.CommunityIds.Cast<int>().ToList());


                foreach (int c in communityIDs)
                {
                    var photoCommunity = cachedMember.Photos.GetCommunity(c);
                    var siteID = Brands.GetDefaultSiteIDForCommunity(c);

                    if (photoCommunity != null && photoCommunity.Count > 0)
                    {
                        for (int i = 0; i < photoCommunity.Count; i++)
                        {
                            Matchnet.File.ServiceAdapters.FileSA.Instance.UnHideFile(
                                photoCommunity[(byte) i].FileWebPath, photoCommunity[(byte) i].FileCloudPath, c, siteID);

                            if (!string.IsNullOrEmpty(photoCommunity[(byte) i].ThumbFileWebPath))
                            {
                                Matchnet.File.ServiceAdapters.FileSA.Instance.UnHideFile(
                                    photoCommunity[(byte) i].ThumbFileWebPath,
                                    photoCommunity[(byte) i].ThumbFileCloudPath, c, siteID);
                            }
                            if (photoCommunity[(byte) i].Files == null) continue;
                            foreach (var photoFile in photoCommunity[(byte) i].Files)
                            {
                                if (photoFile.PhotoFileType != PhotoFileType.Original)
                                {
                                    Matchnet.File.ServiceAdapters.FileSA.Instance.UnHideFile(photoFile.FileWebPath,
                                                                                             photoFile.FileCloudPath, c,
                                                                                             siteID);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT,
                    "MemberUpdateNotifier.unhidePhotos", ex, cachedMember.MemberID);
            }
        }

        public void SetAttributeText(AttributeGroup attributeGroup,
            TextValue textValue,
            Int32 languageID)
        {
            Int32 communityID = Brands.GetCommunityID(attributeGroup.GroupID);

            setSearchCommunity(communityID,
                SearchUpdateType.Update);

            if (communityID == Constants.NULL_INT && _lastSetAttributeCommunityID != Constants.NULL_INT)
            {
                communityID = _lastSetAttributeCommunityID; // To enforce non community scope text attributes to make into approval queue (i.e. username)
            }

            if (communityID != Constants.NULL_INT)
            {
                if ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval &&
                    (textValue.TextStatus == TextStatusType.None || textValue.TextStatus == TextStatusType.Pending))
                {
                    queueDBTextApproval(communityID, languageID);
                    _textQueueItems.Add(new DBQ.QueueItemText(communityID, Constants.NULL_INT, Constants.NULL_INT,
                                                              _memberUpdate.MemberID, languageID, DBQ.TextType.Regular,
                                                              attributeGroup.ID, textValue.Text));
                }
            }
        }


        private void queueDBTextApproval(Int32 communityID, Int32 languageID)
        {
            DBQ.QueueItemText queueItemText = (from qit in _dbqTextQueueItems where qit.CommunityID == communityID && qit.LanguageID == languageID select qit).FirstOrDefault();
            if (queueItemText == null)
            {
                var newQueueItem = new DBQ.QueueItemText(communityID, Constants.NULL_INT, Constants.NULL_INT,
                                                         _memberUpdate.MemberID, languageID, DBQ.TextType.Regular);

                //Populate Member Attributes to be consumed by CRX
                bool populateCrxMemberAttr = false;
                bool.TryParse(SettingsService.GetSettingFromSingleton("CRX_POPULATE_MEMBER_INFO"), out populateCrxMemberAttr);
                if (populateCrxMemberAttr)
                {
                    newQueueItem.PopulateMemberInfo = true;

                    int brandId = Constants.NULL_INT;
                    _cachedMember.GetLastLogonDate(communityID, out brandId, Attributes, Brands);
                    var brand = Brands.GetBrand(brandId);

                    if (brand != null)
                    {
                        //IP
                        var attr = Attributes.GetAttribute("RegistrationIP");
                        int ipAddress =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);
                        if (ipAddress != Constants.NULL_INT)
                        {
                            var regIPAddress = new IPAddress(BitConverter.GetBytes(ipAddress));
                            newQueueItem.IPAddress = regIPAddress.ToString();
                        }

                        //ProfileRegionID
                        attr = Attributes.GetAttribute("RegionID");
                        newQueueItem.ProfileRegionId =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);

                        //GenderMask
                        attr = Attributes.GetAttribute("GenderMask");
                        newQueueItem.GenderMask =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);

                        //MaritalStatus
                        attr = Attributes.GetAttribute("MaritalStatus");
                        newQueueItem.MaritalStatus =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);

                        //FirstName
                        TextStatusType status;
                        attr = Attributes.GetAttribute("SiteFirstName");
                        newQueueItem.FirstName =
                            _cachedMember.GetAttributeText(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID,
                                languageID, "", out status);

                        //LastName
                        attr = Attributes.GetAttribute("SiteLastName");
                        newQueueItem.LastName =
                            _cachedMember.GetAttributeText(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID,
                                languageID, "", out status);

                        //Email
                        newQueueItem.Email = _cachedMember.EmailAddress;

                        //OccupationDescription 
                        attr = Attributes.GetAttribute("IndustryType");
                        newQueueItem.OccupationDescription =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT).ToString();

                        //EducationLevel
                        attr = Attributes.GetAttribute("EducationLevel");
                        newQueueItem.EducationLevel =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);

                        //Religion
                        attr = Attributes.GetAttribute("Religion");
                        newQueueItem.Religion =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);

                        //Ethnicity
                        attr = Attributes.GetAttribute("Ethnicity");
                        newQueueItem.Ethnicity =
                            _cachedMember.GetAttributeInt(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, Constants.NULL_INT);

                        //BillingPhoneNumber
                        var historyServiceAdapter = new OrderHistoryServiceAdapter();
                        OrderInfo[] arrOrderInfo =
                            historyServiceAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(
                                _cachedMember.MemberID, brand.Site.SiteID, 5000, 1);
                        var paymentProfileServiceAdapter =
                            new PaymentProfileServiceAdapter().GetProxyInstanceForBedrock();
                        PaymentProfileInfo paymentProfileResponse =
                            paymentProfileServiceAdapter.GetMemberDefaultPaymentProfile(_cachedMember.MemberID,
                                                                                        brand.Site.SiteID);
                        int orderIDForDefaultPaymentProfile = 0;

                        foreach (OrderInfo orderInfo in arrOrderInfo)
                        {
                            if (orderInfo.OrderDetail.Length > 0)
                            {
                                if (paymentProfileResponse != null &&
                                    orderInfo.UserPaymentGuid == paymentProfileResponse.PaymentProfileID)
                                {
                                    if (orderIDForDefaultPaymentProfile <= 0)
                                    {
                                        ObsfucatedPaymentProfileResponse obsfucatedpaymentProfileResponse =
                                            paymentProfileServiceAdapter.GetObsfucatedPaymentProfileByOrderID(
                                                orderInfo.OrderID, _cachedMember.MemberID, brand.Site.SiteID);
                                        if (obsfucatedpaymentProfileResponse != null &&
                                            obsfucatedpaymentProfileResponse.Code == "0"
                                            && obsfucatedpaymentProfileResponse.PaymentProfile != null &&
                                            obsfucatedpaymentProfileResponse.PaymentProfile is
                                            ObsfucatedCreditCardPaymentProfile)
                                        {
                                            newQueueItem.BillingPhoneNumber =
                                                ((ObsfucatedCreditCardPaymentProfile)
                                                 obsfucatedpaymentProfileResponse.PaymentProfile).PhoneNumber;
                                        }
                                    }
                                }
                            }
                        }

                        //SubscriptionStatus 
                        bool evaluateSubscriptionStatusAgain = false;
                        attr = Attributes.GetAttribute("SubscriptionStatus");
                        AttributeGroup attributeGroup = GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr);
                        int subStatus = _cachedMember.GetAttributeInt(attributeGroup.ID,
                                                                      Convert.ToInt16(attributeGroup.DefaultValue));
                        switch (subStatus)
                        {
                            case 2:
                                newQueueItem.SubscriptionStatus = "Ex Subscriber";
                                break;
                            case 1:
                                newQueueItem.SubscriptionStatus = "Never Subscribed";
                                break;
                            case 5:
                                newQueueItem.SubscriptionStatus = "Renew Subscriber";
                                break;
                            case 6:
                                newQueueItem.SubscriptionStatus = "Renew Winback";
                                break;
                            case 3:
                                newQueueItem.SubscriptionStatus = "First Time Subscriber";
                                break;
                            case 4:
                                newQueueItem.SubscriptionStatus = "Subscriber Winback";
                                break;
                            case 9:
                                newQueueItem.SubscriptionStatus = "Premium";
                                break;
                        }

                        //RegistrationDate
                        attr = Attributes.GetAttribute("BrandInsertDate");
                        newQueueItem.RegistrationDate =
                            _cachedMember.GetAttributeDate(
                                GetAttributeGroup(communityID, brand.Site.SiteID, brandId, attr).ID, DateTime.MinValue);
                    }
                }
                _dbqTextQueueItems.Add(newQueueItem);
            }

        }



        public void SetAttributeInt(AttributeGroup attributeGroup,
            Int32 attributeValue)
        {
            Int32 communityID = Brands.GetCommunityID(attributeGroup.GroupID);

            switch (attributeGroup.AttributeID)
            {
                case CachedMember.ATTRIBUTEID_SELFSUSPENDEDFLAG:
                    if (attributeValue == 1)
                    {
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.remove, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.selfSuspend);
                        _memberSuspended = true;
                        _memberSuspendedOrSelfSuspended = true;
                        hidePhotos(_cachedMember, communityID);
                    }
                    else
                    {
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.add, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.selfUnsuspend);
                        _memberUnSuspendedOrUnSelfSuspended = true;
                        unHidePhotos(_cachedMember, communityID);
                    }
                    setMatchCommunity(communityID);
                    setViralCommunity(communityID);
                    setQuestonAnswerMemberInfoUpdate(QAMemberInfoUpdateMode.SelfSuspendedFlag, communityID, attributeValue);
                    break;
                case CachedMember.ATTRIBUTEID_FTAAPPROVED:
                    if (attributeValue == 1)
                    {
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.add, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.unHide);
                    }
                    break;
                case CachedMember.ATTRIBUTEID_HIDEMASK:
                    if ((attributeValue & CachedMember.ATTRIBUTEOPTION_HIDE_SEARCH) == CachedMember.ATTRIBUTEOPTION_HIDE_SEARCH)
                    {
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.remove, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.hide);
                    }
                    else
                    {
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.add, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.unHide);
                    }
                    break;

                case CachedMember.ATTRIBUTEID_GLOBALSTATUSMASK:
                    _statusMaskUpdated = true;
                    if ((attributeValue & CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN) == CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN)
                    {
                        MemberBL.Instance.ToggleLogonDisabledFlag(_cachedMember.MemberID,  1);
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.remove, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.adminSuspend);
                        _memberSuspended = true;
                        _memberSuspendedOrSelfSuspended = true;
                        hidePhotos(_cachedMember, communityID);
                        RemoveAppMembers();
                    }
                    else
                    {
                        MemberBL.Instance.ToggleLogonDisabledFlag(_cachedMember.MemberID, 0);
                        setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.add, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.adminUnsuspend);
                        _memberUnSuspendedOrUnSelfSuspended = true;
                        unHidePhotos(_cachedMember, communityID);
                    }
                    setMatchCommunities();
                    setViralCommunities();
                    setQuestonAnswerMemberInfoUpdate(QAMemberInfoUpdateMode.GlobalStatusMask, communityID, attributeValue);
                    
                    break;

                case CachedMember.ATTRIBUTEID_COMMUNITYEMAILCOUNT:
                    Int32 oldCount = attributeValue - 1;

                    setSearchCommunity(communityID, SearchUpdateType.Update);
                    break;

                case CachedMember.ATTRIBUTEID_MATCHNEWSLETTERPERIOD:
                    setMatchCommunity(communityID);
                    break;

                case CachedMember.ATTRIBUTEID_GOTACLICKEMAILPERIOD:
                    setViralCommunities();
                    break;

                case CachedMember.ATTRIBUTEID_NEWMEMBEREMAILPERIOD:
                    setNewMemberCommunities();
                    break;

                //all this madness ensures usernames get queued for text approval upon reg
                case CachedMember.ATTRIBUTEID_BRANDLOGONCOUNT:
                    if (attributeValue == 1)
                    {
                        TextStatusType textStatus;

                        Int32 languageID = getLanguageID(getLastBrandID(communityID));

                        _cachedMember.GetAttributeText(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEGROUPID_USERNAME,
                            languageID,
                            "",
                            out textStatus);

                        if (textStatus == TextStatusType.None || textStatus == TextStatusType.Pending)
                        {
                            queueDBTextApproval(communityID, languageID);
                        }
                    }
                    break;
                case CachedMember.ATTRIBUTEID_HASPHOTOFLAG:
                    setQuestonAnswerMemberInfoUpdate(QAMemberInfoUpdateMode.HasPhotoFlag, communityID, attributeValue);
                    break;
                case CachedMember.ATTRIBUTEID_GENDERMASK:
                    setQuestonAnswerMemberInfoUpdate(QAMemberInfoUpdateMode.GenderMask, communityID, attributeValue);
                    break;
            }

        }


        public void SetAttributeDate(AttributeGroup attributeGroup,
            DateTime attributeValue)
        {
            Int32 communityID = Brands.GetCommunityID(attributeGroup.GroupID);
            _lastSetAttributeCommunityID = communityID;

            switch (attributeGroup.AttributeID)
            {
                case CachedMember.ATTRIBUTEID_LASTEMAILRECEIVEDATE:
                    break;

                case CachedMember.ATTRIBUTEID_BRANDINSERTDATE:
                    DateTime dtm = _cachedMember.GetAttributeDate(attributeGroup.ID, DateTime.Now);
                    if (dtm == attributeValue)
                    {
                        setMatchCommunities();
                    }
                    break;
                case CachedMember.ATTRIBUTEID_BRANDLASTLOGONDATE:
                    setMatchMailLastLogonDate(communityID, _cachedMember.GetAttributeDate(attributeGroup.ID, DateTime.Now));
                    setE2SearchUpdate(communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum.update, Spark.SearchEngine.ValueObjects.UpdateReasonEnum.logon);
                    //RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberUpdateNotifier.SetAttributeDate()", String.Format("Set E2 NRT for memberID:{0}, communityID:{1} update reason: {2}", _memberUpdate.MemberID, communityID, "logon"), null);
                    break;
            }

        }

        private void setMatchMailLastLogonDate(int communityID, DateTime lastLogonDate)
        {
            Command commandMatch = new Command("mnAlert", "dbo.up_BulkMailSchedule_Update_MM_LastLogonDate", 0);
            commandMatch.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, _cachedMember.MemberID);
            commandMatch.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
            commandMatch.AddParameter("@LastLogonDate", SqlDbType.DateTime, ParameterDirection.Input, lastLogonDate);
            Client.Instance.ExecuteAsyncWrite(commandMatch);
        }

        private void setAllSearchCommunities(SearchUpdateType updateType)
        {
            if (!_searchAllCommunities)
            {
                for (Int32 num = 0; num < CommunityIDList.Count; num++)
                {
                    setSearchCommunity((Int32)CommunityIDList[num],
                        updateType);
                }
            }

        }

        private void setSearchCommunity(Int32 communityID,
            SearchUpdateType updateType)
        {
            if (communityID == Constants.NULL_INT)
            {
                setAllSearchCommunities(updateType);
                return;
            }

            //check to see if it exists b/c we don't want to change an existing insert to a delete
            object o = _searchPointers[communityID];

            if (o == null)
            {
                if (updateType == SearchUpdateType.Remove || isSearchable(communityID))
                {
                    Int32 brandID = Constants.NULL_INT;
                    DateTime lastActiveDate = _cachedMember.GetLastLogonDate(communityID, out brandID, Attributes, Brands);

                    if (brandID == Constants.NULL_INT)
                    {
                        return;
                    }

                    Int32 attributeGroupIDLastActiveDate = Attributes.GetAttributeGroup(brandID, CachedMember.ATTRIBUTEID_BRANDLASTLOGONDATE).ID;
                    Int32 attributeGroupIDEmailCount = getAttributeGroupID(communityID, CachedMember.ATTRIBUTEID_COMMUNITYEMAILCOUNT);
                    Int32 attributeGroupIDBrandLogonCount = Attributes.GetAttributeGroup(brandID, CachedMember.ATTRIBUTEID_BRANDLOGONCOUNT).ID;

                    if (updateType == SearchUpdateType.Update)
                    {
                        Int32 brandLogonCount = 0;
                        if (_memberUpdate.AttributesInt.ContainsKey(attributeGroupIDBrandLogonCount))
                        {
                            brandLogonCount = (Int32)_memberUpdate.AttributesInt[attributeGroupIDBrandLogonCount];
                        }

                        //EmailCount attribute is always updated by itself
                        if (_memberUpdate.AttributesInt.ContainsKey(attributeGroupIDEmailCount))
                        {
                            updateType = SearchUpdateType.UpdateEmailCount;
                        }
                        //LastActiveDate attribute is only modified during logon and initial registration
                        //We can distinguish between reg. and logon by checking to see if BrandLogonCount is > 1
                        //BrandLogonCount will only be <= 1 if this is a reg, in which case we want to perform a full update, not just a LastActiveDate update.
                        else if (_memberUpdate.AttributesDate.ContainsKey(attributeGroupIDLastActiveDate) && brandLogonCount > 1)
                        {
                            updateType = SearchUpdateType.UpdateLastActiveDate;
                        }
                    }

                    _searchPointers.Add(communityID,
                        new SearchUpdate(_memberUpdate.MemberID,
                        communityID,
                        updateType,
                        _cachedMember.GetAttributeInt(attributeGroupIDEmailCount, Constants.NULL_INT),
                        lastActiveDate,
                        _cachedMember.HasApprovedPhoto(communityID)));
                }
            }
            else if (updateType == SearchUpdateType.Remove)
            {
                ((SearchUpdate)_searchPointers[communityID]).UpdateType = SearchUpdateType.Remove;
            }
        }

        private void setE2SearchUpdate(int communityID, Spark.SearchEngine.ValueObjects.UpdateModeEnum updateMode, Spark.SearchEngine.ValueObjects.UpdateReasonEnum updateReason)
        {
            if (_searchMemberUpdateList.ContainsKey(communityID))
            {
                if (updateMode == Spark.SearchEngine.ValueObjects.UpdateModeEnum.remove)
                {
                    _searchMemberUpdateList[communityID].UpdateMode = updateMode;
                    _searchMemberUpdateList[communityID].UpdateReason = updateReason;
                }
            }
            else
            {
                Spark.SearchEngine.ValueObjects.SearchMemberUpdate searchMemberUpdate = new Spark.SearchEngine.ValueObjects.SearchMemberUpdate();
                searchMemberUpdate.MemberID = _memberUpdate.MemberID;
                searchMemberUpdate.CommunityID = communityID;
                searchMemberUpdate.UpdateMode = updateMode;
                searchMemberUpdate.UpdateReason = updateReason;

                _searchMemberUpdateList.Add(communityID, searchMemberUpdate);
            }

        }

        private void setMatchCommunity(Int32 communityID)
        {
            if (!_matchCommunities.Contains(communityID))
            {
                _matchCommunities.Add(communityID);
            }
        }


        private void setMatchCommunities()
        {
            for (Int32 i = 0; i < CommunityIDList.Count; i++)
            {
                setMatchCommunity((Int32)CommunityIDList[i]);
            }
        }


        private void setViralCommunity(Int32 communityID)
        {
            if (!_viralCommunities.Contains(communityID))
            {
                _viralCommunities.Add(communityID);
            }
        }


        private void setViralCommunities()
        {
            for (Int32 i = 0; i < CommunityIDList.Count; i++)
            {
                setViralCommunity((Int32)CommunityIDList[i]);
            }
        }

        private void setNewMemberCommunities()
        {
            for (Int32 i = 0; i < CommunityIDList.Count; i++)
            {
                setNewMemberCommunity((Int32)CommunityIDList[i]);
            }
        }

        private void setNewMemberCommunity(Int32 communityID)
        {
            if (!_newMemberCommunities.Contains(communityID))
            {
                _newMemberCommunities.Add(communityID);
            }
        }

        private void setQuestonAnswerMemberInfoUpdate(QAMemberInfoUpdateMode memberInfoUpdateMode, int communityID, int attributeValueInt)
        {
            if (memberInfoUpdateMode == QAMemberInfoUpdateMode.GlobalStatusMask)
            {
                QAMemberInfoUpdate memberInfoUpdate = new QAMemberInfoUpdate();
                memberInfoUpdate.MemberID = _cachedMember.MemberID;
                memberInfoUpdate.UpdateMode = memberInfoUpdateMode;
                memberInfoUpdate.ValueInt = attributeValueInt;

                _QuestionAnswerMemberInfoUpdateList.Add(memberInfoUpdate);
            }
            else if (memberInfoUpdateMode == QAMemberInfoUpdateMode.GenderMask
                || memberInfoUpdateMode == QAMemberInfoUpdateMode.HasPhotoFlag
                || memberInfoUpdateMode == QAMemberInfoUpdateMode.SelfSuspendedFlag)
            {
                //these are community based attributes, we'll need to create updates for all sites in the community that member belongs to
                //since memberInfo for QA are flattened by site to match the questions and answers data
                foreach (int siteID in SiteIDList)
                {
                    Site site = Brands.GetSite(siteID);
                    if (site != null)
                    {
                        if (site.Community.CommunityID == communityID)
                        {
                            QAMemberInfoUpdate memberInfoUpdate = new QAMemberInfoUpdate();
                            memberInfoUpdate.MemberID = _cachedMember.MemberID;
                            memberInfoUpdate.UpdateMode = memberInfoUpdateMode;
                            memberInfoUpdate.ValueInt = attributeValueInt;
                            memberInfoUpdate.SiteID = site.SiteID;

                            _QuestionAnswerMemberInfoUpdateList.Add(memberInfoUpdate);
                        }
                    }
                }

            }
        }

        private bool isSearchable(Int32 communityID)
        {
            if (_cachedMember.GetAttributeInt(getAttributeGroupID(communityID, CachedMember.ATTRIBUTEID_SELFSUSPENDEDFLAG), Constants.NULL_INT) == 1)
            {
                return false;
            }

            if ((_cachedMember.GetAttributeInt(getAttributeGroupID(communityID, CachedMember.ATTRIBUTEID_HIDEMASK), 0) & CachedMember.ATTRIBUTEOPTION_HIDE_SEARCH) == CachedMember.ATTRIBUTEOPTION_HIDE_SEARCH)
            {
                return false;
            }

            if ((_cachedMember.GetAttributeInt(getAttributeGroupID(communityID, CachedMember.ATTRIBUTEID_GLOBALSTATUSMASK), 0) & CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN) == CachedMember.ATTRIBUTEOPTION_SUSPEND_ADMIN)
            {
                return false;
            }

            if (_cachedMember.GetAttributeInt(getAttributeGroupID(communityID, CachedMember.ATTRIBUTEID_FTAAPPROVED), 1) == 0)
            {
                return false;
            }

            return true;
        }


        private Int32 getAttributeGroupID(Int32 communityID,
            Int32 attributeID)
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = Attributes.GetAttribute(attributeID);
            Int32 groupID = 0;

            switch (attribute.Scope)
            {
                case ScopeType.Personals:
                    groupID = CachedMember.GROUP_PERSONALS;
                    break;

                case ScopeType.Community:
                    if (!(communityID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), communityID not set.");
                    }
                    groupID = communityID;
                    break;
            }

            if (groupID == 0)
            {
                throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ").");
            }

            return Attributes.GetAttributeGroup(groupID, attribute.ID).ID;
        }

        private AttributeGroup GetAttributeGroup(Int32 communityID,
           Int32 siteID,
           Int32 brandID,
           Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute)
        {
            Int32 groupID = 0;

            switch (attribute.Scope)
            {
                case ScopeType.Personals:
                    groupID = CachedMember.GROUP_PERSONALS;
                    break;

                case ScopeType.Community:
                    if (!(communityID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), communityID not set.");
                    }
                    groupID = communityID;
                    break;

                case ScopeType.Site:
                    if (!(siteID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), siteID not set.");
                    }
                    groupID = siteID;
                    break;

                case ScopeType.Brand:
                    if (!(brandID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), brandID not set.");
                    }
                    groupID = brandID;
                    break;
            }

            if (groupID == 0)
            {
                throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ").");
            }

            return Attributes.GetAttributeGroup(groupID, attribute.ID);
        }



        private Brands Brands
        {
            get
            {
                if (_brands == null)
                {
                    _brands = BrandConfigSA.Instance.GetBrands();
                }

                return _brands;
            }
        }


        private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    _attributes = AttributeMetadataSA.Instance.GetAttributes();
                }

                return _attributes;
            }
        }


        public ArrayList CommunityIDList
        {
            get
            {
                if (_communityIDList == null)
                {
                    _communityIDList = new ArrayList();
                    ArrayList attributeGroupIDs = Attributes.GetAttributeGroupIDs(Attributes.GetAttribute("BrandInsertDate").ID);

                    for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
                    {
                        Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                        if (_cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
                        {
                            Int32 communityID = Brands.GetBrand(Attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.Community.CommunityID;
                            if (!_communityIDList.Contains(communityID))
                            {
                                _communityIDList.Add(communityID);
                            }
                        }
                    }
                }

                return _communityIDList;
            }
        }

        public ArrayList SiteIDList
        {
            get
            {
                if (_siteIDList == null)
                {
                    _siteIDList = new ArrayList();
                    ArrayList attributeGroupIDs = Attributes.GetAttributeGroupIDs(Attributes.GetAttribute("BrandInsertDate").ID);

                    for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
                    {
                        Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                        if (_cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
                        {
                            Int32 siteID = Brands.GetBrand(Attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.SiteID;
                            if (!_siteIDList.Contains(siteID))
                            {
                                _siteIDList.Add(siteID);
                            }
                        }
                    }
                }

                return _siteIDList;
            }
        }


        private Int32 getLastBrandID(Int32 communityID)
        {
            ArrayList communities = new ArrayList();
            DateTime lastLogonDate = DateTime.MinValue;
            Int32 brandID = Constants.NULL_INT;

            Int32 attributeID = Attributes.GetAttribute("BrandLastLogonDate").ID;
            ArrayList attributeGroupIDs = Attributes.GetAttributeGroupIDs(attributeID);

            for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
            {
                Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                Brand brand = Brands.GetBrand(Attributes.GetAttributeGroup(attributeGroupID).GroupID);
                if (brand.Site.Community.CommunityID == communityID)
                {
                    DateTime currentDtm = _cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue);
                    if (currentDtm > lastLogonDate)
                    {
                        lastLogonDate = currentDtm;
                        brandID = brand.BrandID;
                    }
                }
            }

            return brandID;
        }

        private Int32 getLanguageID(Int32 brandID)
        {
            return Brands.GetBrand(brandID).Site.LanguageID;
        }


        private bool hasUnapprovedText(Int32 languageID)
        {
            for (Int32 i = 0; i < _unapprovedLanguages.Length; i++)
            {
                if (_unapprovedLanguages[i] == languageID)
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsAttributeSearchable(AttributeGroup attributegroup)
        {
            bool searchable = true;
            try
            {

                Matchnet.Content.ValueObjects.AttributeMetadata.SearchStoreAttributes searchableattributes = AttributeMetadataSA.Instance.GetSearchStoreAttributes();

                if (searchableattributes == null || searchableattributes.SearchAttributes.Count == 0)
                    return searchable;

                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attr = _attributes.GetAttribute(attributegroup.AttributeID);
                try
                {
                    String name = searchableattributes.SearchAttributes.Find(delegate(string attributename) { return attributename.ToLower().Trim() == attr.Name.ToLower().Trim(); });
                    if (String.IsNullOrEmpty(name))
                    {
                        searchable = false;

                    }
                }
                catch (Exception ex)
                {
                    searchable = false;
                }



                return searchable;
            }
            catch (Exception ex)
            {
                return searchable;
            }

        }

        private bool IsUpdateAnswerMemberInfoEnabled()
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QA_UPDATE_ANSWERMEMBERINFO");
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        private void UpdateAnswerMemberInfo(QAMemberInfoUpdate memberInfoUpdate)
        {
            try
            {
                if (IsUpdateAnswerMemberInfoEnabled() && memberInfoUpdate != null && memberInfoUpdate.UpdateMode != QAMemberInfoUpdateMode.None)
                {
                    Command comm = new Command();

                    comm.LogicalDatabaseName = "mnQuestionAnswer";
                    comm.StoredProcedureName = "dbo.up_UpdateAnswerMemberInfo";

                    if (memberInfoUpdate.UpdateMode == QAMemberInfoUpdateMode.GenderMask)
                    {
                        comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.MemberID);
                        comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.SiteID);
                        comm.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.ValueInt);
                    }
                    else if (memberInfoUpdate.UpdateMode == QAMemberInfoUpdateMode.GlobalStatusMask)
                    {
                        comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.MemberID);
                        comm.AddParameter("@GlobalStatusMask", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.ValueInt);
                    }
                    else if (memberInfoUpdate.UpdateMode == QAMemberInfoUpdateMode.SelfSuspendedFlag)
                    {
                        comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.MemberID);
                        comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.SiteID);
                        comm.AddParameter("@SelfSuspendedFlag", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.ValueInt);
                    }
                    else if (memberInfoUpdate.UpdateMode == QAMemberInfoUpdateMode.HasPhotoFlag)
                    {
                        comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.MemberID);
                        comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.SiteID);
                        comm.AddParameter("@HasPhotoFlag", SqlDbType.Int, ParameterDirection.Input, memberInfoUpdate.ValueInt);
                    }

                    Client.Instance.ExecuteAsyncWrite(comm);
                }
            }
            catch (Exception ex)
            {
                ServiceBoundaryException sbe = new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberUpdateNotifier.UpdateAnswerMemberInfo()", ex);
            }
        }
    }
}
