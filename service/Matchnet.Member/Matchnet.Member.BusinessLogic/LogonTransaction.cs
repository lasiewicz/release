using System;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Messaging;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Data.Configuration;
using Matchnet.Member.ValueObjects;
using Spark.Logging;


namespace Matchnet.Member.BusinessLogic
{
	[Transaction(TransactionOption.Required)]
	public class LogonTransaction : ServicedComponent
	{
        const string LOGICAL_DB = "mnLogon";

		/// <summary>
		/// 
		/// </summary>
		public LogonTransaction()
		{
		}

		/// <summary>
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="username"></param>
	    /// <param name="passwordHash"> </param>
	    /// <param name="groupID"> </param>
		/// <param name="exception"></param>
		/// <returns></returns>
		[AutoComplete]
        public RegisterStatusType RegisterWithSalt(int memberID, string emailAddress, string emailAddressHash, string username, string passwordHash, string passwordSalt, string cachedEmailAddressHash, int groupID, LogonActionType actionType, int disabledFlag, out Exception exception)
		{
			bool written = false, abortTransaction = false;
			Partition partition = ConnectionDispenser.Instance.GetLogicalDatabase(LOGICAL_DB).GetPartition(memberID);
			PhysicalDatabases physicalDatabases = partition.PhysicalDatabases;
			Int32 pdbCount = physicalDatabases.Count;
			SqlConnection[] connections = new SqlConnection[pdbCount];
			RegisterStatusType registerStatus = RegisterStatusType.Success;

			exception = null;

			try
			{
				for (Int32 pdbNum = 0; pdbNum < pdbCount; pdbNum++)
				{
					PhysicalDatabase pdb = physicalDatabases[pdbNum];

					var cmd = new SqlCommand();
					cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = GetStoredProcedureBasedOnActionType(actionType);
					cmd.Parameters.Add("@MemberID", SqlDbType.Int).Value = memberID;
                    if (groupID != Constants.NULL_INT && actionType != LogonActionType.DisabledFlagUpdate)
                    {
                        cmd.Parameters.Add("@GroupID", SqlDbType.VarChar).Value = groupID;
                    }

                    switch (actionType)
                    {
                        case LogonActionType.DisabledFlagUpdate:
                            if (disabledFlag==1)
                            {
                                cmd.Parameters.Add("@DisabledFlag", SqlDbType.Bit).Value = disabledFlag;
                            }
                            else
                            {
                                cmd.Parameters.Add("@DisabledFlag", SqlDbType.Bit).Value = DBNull.Value;
                            }
                            break;
                        
                        case LogonActionType.NewUser:
                            if (emailAddress != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar).Value = emailAddress;
                            }
                            if (username != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = username.Trim();
                            }
                            if (passwordHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@PasswordHash", SqlDbType.VarChar).Value = passwordHash;
                            }
                            if (passwordSalt != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@PasswordSalt", SqlDbType.VarChar).Value = passwordSalt;
                            }
                            if (emailAddressHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@EmailAddressHash", SqlDbType.VarChar).Value = emailAddressHash;
                            }
                            break;
                        case LogonActionType.EmailAddressUpdate:
                            if (emailAddress != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar).Value = emailAddress;

                                cmd.Parameters.Add("@ServiceUpdateDate", SqlDbType.DateTime).Value = DateTime.Now.ToString("G");
                            }
                            if (emailAddressHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@EmailAddressHash", SqlDbType.VarChar).Value = emailAddressHash;
                            }
                            if (cachedEmailAddressHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@CurrentEmailAddressHash", SqlDbType.VarChar).Value =
                                    cachedEmailAddressHash;
                            }
                            
                            break;
                        case LogonActionType.PasswordUpdate:
                            if (passwordHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@PasswordHash", SqlDbType.VarChar).Value = passwordHash;
                            }
                            if (passwordSalt != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@PasswordSalt", SqlDbType.VarChar).Value = passwordSalt;
                            }
                            if (cachedEmailAddressHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@CurrentEmailAddressHash", SqlDbType.VarChar).Value =
                                    cachedEmailAddressHash;
                            }
                            break;
                        case LogonActionType.UsernameUpdate:
                            if (username != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@UserName", SqlDbType.NVarChar).Value = username.Trim();
                            }
                            if (cachedEmailAddressHash != Constants.NULL_STRING)
                            {
                                cmd.Parameters.Add("@CurrentEmailAddressHash", SqlDbType.VarChar).Value =
                                    cachedEmailAddressHash;
                            }
                            break;
                    }


				    SqlParameter returnValueParam = cmd.Parameters.Add("ReturnValue", SqlDbType.Int);
					returnValueParam.Direction = ParameterDirection.ReturnValue;

                    SqlParameter rowCountParam = cmd.Parameters.Add("@RC_Out", SqlDbType.Int);
                    rowCountParam.Direction = ParameterDirection.Output;

					if (pdb.IsActive)
					{
						SqlConnection conn = new SqlConnection(pdb.ConnectionString);
						connections[pdbNum] = conn;

						cmd.Connection = conn;

						conn.Open();
						cmd.ExecuteNonQuery();
						registerStatus = (RegisterStatusType)Enum.Parse(typeof(RegisterStatusType), returnValueParam.Value.ToString());
					    int rowcount = (rowCountParam.Value != DBNull.Value) ? Convert.ToInt32(rowCountParam.Value) : 0;

                        if (registerStatus == RegisterStatusType.Success && rowcount != 9999) //Something wrong happened. It did not update\insert into all the required tables.
                        {
                            LogLogonTransactionWarning("Logon member save incomplete.", memberID, emailAddress, username,
                                                       groupID, pdb.ServerName, returnValueParam.Value.ToString(),
                                                       rowcount, cmd.CommandText);
                        }

						written = true;

					    LogLogonTransactionInfo("LogonMember Saved.", memberID, emailAddress, username, groupID, pdb.ServerName,
					                            returnValueParam.Value.ToString(),rowcount, cmd.CommandText);
                        
						if (registerStatus != RegisterStatusType.Success)
						{
						    abortTransaction = true;
							break;
						}
					}
					else
					{
                        LogLogonTransactionInfo("LogonMember Insert/update queued. DB is inactive.", memberID, emailAddress, username, groupID, pdb.ServerName,
                                                "",-1, cmd.CommandText);

                        //Command.Create is throwing null reference exception since output param value is null. Assigning value to output parameter.
                        rowCountParam.Value = 0;

						MessageQueue queue = HydraUtility.GetQueueRecovery(HydraUtility.GetKey(LOGICAL_DB, partition.Offset), pdb.ServerName);
                        if (queue != null)
                        {
                            queue.Formatter = new BinaryMessageFormatter();
                            queue.Send(Command.Create(LOGICAL_DB, memberID, pdb.ConnectionString, cmd),
                                       MessageQueueTransactionType.Single);
                            queue.Dispose();
                        }
					}
				}

				if (written == false)
				{
					throw new Exception("No active mnFile databases are active for offset " + partition.Offset.ToString());
				}

                //If the method exits from for loop with status type other than 'Success', abort the trasaction so that DBs are in sync.
                if(abortTransaction)
                    ContextUtil.SetAbort();
                else
				    ContextUtil.SetComplete();
			}
			catch (Exception ex)
			{
				exception = ex;
				ContextUtil.SetAbort();
			}
			finally
			{
				for (Int32 connectionNum = 0; connectionNum < connections.Length; connectionNum++)
				{
					SqlConnection conn = connections[connectionNum];
					if (conn != null)
					{
						if (conn.State == ConnectionState.Open)
						{
							conn.Close();
						}
					}
				}
			}

			return registerStatus;
		}

        private string GetStoredProcedureBasedOnActionType(LogonActionType actionType)
        {
            string spName = "up_LogonMemberCommunity_MemberValueCommunity_Insert";
            switch (actionType)
            {
                case LogonActionType.NewUser:
                    spName =  "up_LogonMemberCommunity_MemberValueCommunity_Insert";
                    break;
                case LogonActionType.EmailAddressUpdate:
                    spName = "up_LogonMemberCommunity_EmailAddress_Save";
                    break;
                case LogonActionType.PasswordUpdate:
                    spName = "up_LogonMemberCommunity_Password_Save";
                    break;
                case LogonActionType.UsernameUpdate:
                    spName = "up_LogonMemberCommunity_Username_Save";
                    break;
                case LogonActionType.DisabledFlagUpdate:
                    spName = "up_LogonMemberCommunity_DisabledFlag_Save";
                    break;
            }
            return spName;
        }

        private void LogLogonTransactionInfo(string message, int memberId, string email, string username, int groupId, string servername, string returnvalue, int outputParamVal, string spName)
        {
            RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL.RegisterWithSalt",
                        string.Format("{7} Params: MemberID: {0} Email: {1} UserName: {2} GroupId: {3} Server: {4} Sp return value: {5} Sp: {6} Output param(rowcount) : {8}", memberId,
                        email, username, groupId, servername, returnvalue, spName, message, outputParamVal), memberId);
        }

        private void LogLogonTransactionWarning(string message, int memberId, string email, string username, int groupId, string servername, string returnvalue, int outputParamVal, string spName)
        {
            RollingFileLogger.Instance.LogWarningMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL.RegisterWithSalt",
                            string.Format("{8} Params: MemberID: {0} Email: {1} UserName: {2} GroupId: {3} Server: {4} Sp return value: {5} Sp: {6} Output param(rowcount) : {7}", memberId,
                            email, username, groupId, servername, returnvalue, spName, outputParamVal, message), memberId);
        }
	}
}
