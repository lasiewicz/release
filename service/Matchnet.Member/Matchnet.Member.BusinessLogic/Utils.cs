﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ValueObjects.MemberDTO;

namespace Matchnet.Member.BusinessLogic.Utils
{
    public class Utils
    {
        public static readonly Utils Instance = new Utils();

        private Utils(){}

        public string HashString(string Value)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
            data = x.ComputeHash(data);
            string ret = "";
            for (int i = 0; i < data.Length; i++)
                ret += data[i].ToString("x2").ToLower();
            return ret;
        }
    }
}
