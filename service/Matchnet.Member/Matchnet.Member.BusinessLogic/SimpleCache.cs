using System;
using System.Collections;
using System.Threading;

using Matchnet;


namespace Matchnet.Member.BusinessLogic
{
	internal class SimpleCache
	{
		public static readonly SimpleCache Instance = new SimpleCache();

		private const Int32 ROOT_COUNT = 512;
		private CacheItem[] _roots;
		private static System.Timers.Timer _timer;


		private SimpleCache()
		{
			_roots = new CacheItem[ROOT_COUNT];

			for (Int32 i = 0; i < ROOT_COUNT; i++)
			{
				_roots[i] = new CacheItem(Constants.NULL_INT, 0, false, null);
			}

			_timer = new System.Timers.Timer(30000); //todo
			_timer.Enabled = false;
			_timer.AutoReset = true;
			_timer.Elapsed += new System.Timers.ElapsedEventHandler(_timer_Elapsed);
			_timer.Start();
		}


		public void Add(Int32 id,
			Int32 ttlSeconds,
			bool isSliding,
			object o)
		{
			CacheItem root = _roots[id % ROOT_COUNT];

			lock (this)
			{
				CacheItem cacheItem = getCacheItem(root, id);

				if (cacheItem != null)
				{
					cacheItem.Value = o;
				}
				else
				{
					cacheItem = new CacheItem(id,
						ttlSeconds,
						isSliding,
						o);

					cacheItem.NextCacheItem = root.NextCacheItem;
					root.NextCacheItem = cacheItem;
				}
			}
		}


		private CacheItem getCacheItem(CacheItem root, Int32 id)
		{
			CacheItem cacheItemCurrent = root.NextCacheItem;

			while (cacheItemCurrent != null)
			{
				if (cacheItemCurrent.ID == id)
				{
					return cacheItemCurrent;
				}

				cacheItemCurrent = cacheItemCurrent.NextCacheItem;
			}

			return null;
		}


		public object Get(Int32 id)
		{
			CacheItem cacheItem = getCacheItem(_roots[id % ROOT_COUNT], id);
			if (cacheItem != null)
			{
				cacheItem.UpdateExpireTime();
				return cacheItem.Value;
			}

			return null;
		}


		private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			DateTime beginTime = DateTime.Now;
			Int32 expireCount = 0;
			Int32 keepCount = 0;

			for (Int32 i = 0; i < ROOT_COUNT; i++)
			{
				CacheItem cacheItemRoot = _roots[i];
				CacheItem cacheItemLast = cacheItemRoot;
				CacheItem cacheItemCurrent = cacheItemRoot.NextCacheItem;

				while (cacheItemCurrent != null)
				{
					if (cacheItemCurrent.ExpireTime < DateTime.Now)
					{
						//MemberBL.Instance.ExpireMemberCallback(cacheItemCurrent.Value as Matchnet.Member.ValueObjects.CachedMemberBytes);
						cacheItemLast.NextCacheItem = cacheItemCurrent.NextCacheItem;
						expireCount++;
					}
					else
					{
						cacheItemLast = cacheItemCurrent;
						keepCount++;
					}

					cacheItemCurrent = cacheItemCurrent.NextCacheItem;
				}
			}

			System.Diagnostics.Trace.WriteLine("__expiration completed in " + DateTime.Now.Subtract(beginTime).TotalMilliseconds.ToString() + " milliseconds (" + expireCount.ToString() + " expired, " + keepCount.ToString() + " remaining)");
		}


		internal class CacheItem
		{
			private Int32 _id;
			private object _value;
			private Int32 _ttlSeconds;
			private bool _isSliding;
			private DateTime _expireTime;
			private CacheItem _cacheItemNext;

			public CacheItem(Int32 id,
				Int32 ttlSeconds,
				bool isSliding,
				object value)
			{
				_id = id;
				_value = value;
				_ttlSeconds = ttlSeconds;
				_isSliding = true;
				_expireTime = DateTime.Now.AddSeconds(_ttlSeconds);
			}

			public Int32 ID
			{
				get
				{
					return _id;
				}
			}

			public object Value
			{
				get
				{
					return _value;
				}
				set
				{
					_value = value;
				}
			}

			public void UpdateExpireTime()
			{
				if (_isSliding)
				{
					_expireTime = DateTime.Now.AddSeconds(_ttlSeconds);
				}
			}


			public DateTime ExpireTime
			{
				get
				{
					return _expireTime;
				}
				set
				{
					_expireTime = value;
				}
			}


			public CacheItem NextCacheItem
			{
				get
				{
					return _cacheItemNext;
				}
				set
				{
					_cacheItemNext = value;
				}
			}
		}
	}
}
