﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.Member.BusinessLogic
{
    public class MemberHelper
    {
        public static Int32 GetAttributeGroupID(Int32 communityID,
            Int32 attributeID,
            Attributes attributes)
        {
            if(attributes == null)
            {
                attributes = AttributeMetadataSA.Instance.GetAttributes();
            }

            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeID);

            int val = Constants.NULL_INT;
            AttributeGroup ag = GetAttributeGroup(communityID, Constants.NULL_INT, Constants.NULL_INT, attribute);
            if (ag != null)
            {
                val = ag.ID;
            }

            return val;
        }

        public static Int32 GetAttributeGroupID(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            Int32 attributeID,
            Attributes attributes)
        {
            if (attributes == null)
            {
                attributes = AttributeMetadataSA.Instance.GetAttributes();
            }

            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeID);

            int val = Constants.NULL_INT;
            AttributeGroup ag = GetAttributeGroup(communityID, siteID, brandID, attribute);
            if (ag != null)
            {
                val = ag.ID;
            }

            return val;
        }

        public static Int32 GetAttributeGroupID(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            string attributeName,
            Attributes attributes)
        {
            if (attributes == null)
            {
                attributes = AttributeMetadataSA.Instance.GetAttributes();
            }

            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeName);

            int val = Constants.NULL_INT;
            AttributeGroup ag = GetAttributeGroup(communityID, siteID, brandID, attribute);
            if (ag != null)
            {
                val = ag.ID;
            }

            return val;
        }

        public static AttributeGroup GetAttributeGroup(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute)
        {
            Int32 groupID = 0;

            switch (attribute.Scope)
            {
                case ScopeType.Personals:
                    groupID = CachedMember.GROUP_PERSONALS;
                    break;

                case ScopeType.Community:
                    if (!(communityID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), communityID not set.");
                    }
                    groupID = communityID;
                    break;

                case ScopeType.Site:
                    if (!(siteID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), siteID not set.");
                    }
                    groupID = siteID;
                    break;

                case ScopeType.Brand:
                    if (!(brandID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), brandID not set.");
                    }
                    groupID = brandID;
                    break;
            }

            if (groupID == 0)
            {
                throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ").");
            }

            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            return attributes.GetAttributeGroup(groupID, attribute.ID);
        }

        public static Int32 GetAttributeInt(CachedMember cachedMember,
            Int32 communityId,
            Int32 attributeId,
            Attributes attributes)
        {
            return GetAttributeInt(cachedMember, communityId, Constants.NULL_INT, Constants.NULL_INT, attributeId, attributes);
        }

        public static Int32 GetAttributeInt(CachedMember cachedMember,
            Int32 communityId,
            Int32 siteId,
            Int32 brandId,
            string attributeName)
        {
            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeName);
            if (attribute == null)
            {
                throw new Exception("MemberHelper: Unable to find attribute \"" + attribute.Name + "\"");
            }

            return GetAttributeInt(cachedMember, communityId, siteId, brandId, attribute.ID, attributes);
            
        }

        public static Int32 GetAttributeInt(CachedMember cachedMember,
            Int32 communityId,
            Int32 siteId,
            Int32 brandId,
            Int32 attributeId,
            Attributes attributes)
        {
            if (attributes == null)
            {
                attributes = AttributeMetadataSA.Instance.GetAttributes();
            }

            int attributeGroupId = GetAttributeGroupID(communityId, siteId, brandId, attributeId, attributes);
            int val = cachedMember.GetAttributeInt(attributeGroupId, Constants.NULL_INT);

            // if the attribute came back as nothing then set it to the default value specified in the DB
            if (val == Constants.NULL_INT)
            {
                int defaultValue;
                if (int.TryParse(attributes.GetAttributeGroup(attributeGroupId).DefaultValue, out defaultValue))
                {
                    val = defaultValue;
                }
            }

            return val;
        }
    }
}
