using System;
using System.Collections;

using Matchnet;


namespace Matchnet.Member.BusinessLogic
{
	public class BlockedDomainList : ArrayList, ICacheable
	{
		public const string CACHE_KEY = "BlockedDomainList";
		
		public BlockedDomainList()
		{
		}


		#region ICacheable Members
		public int CacheTTLSeconds
		{
			get
			{
				return 600;
			}
			set
			{
			}
		}


		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return CacheItemPriorityLevel.Normal;
			}
			set
			{
			}
		}


		public string GetCacheKey()
		{
			return CACHE_KEY;
		}
		#endregion
	}
}
