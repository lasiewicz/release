using System;
using System.Data;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Member.ValueObjects.Privilege;


namespace Matchnet.Member.BusinessLogic
{
	public class PrivilegeBL
	{
		public static readonly PrivilegeBL Instance = new PrivilegeBL();

		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;

		private Matchnet.Caching.Cache _cache;

		private PrivilegeBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID,
			string orderBy)
		{
			MemberPrivilegeCollection memberPrivilegeCollection = _cache.Get(MemberPrivilegeCollection.GetCacheKey(memberID)) as MemberPrivilegeCollection;

			if (memberPrivilegeCollection == null)
			{
				Command command = new Command("mnMember", "up_MemberPrivilege_List", memberID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, orderBy);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				memberPrivilegeCollection = new MemberPrivilegeCollection(memberID);

				foreach (DataRow row in dt.Rows)
				{
					Privilege privilege = new Privilege(Convert.ToInt32(row["PrivilegeID"]),
						row["Description"].ToString());
					MemberPrivilege memberPrivilege = new MemberPrivilege(privilege,
						(PrivilegeState)Convert.ToInt32(row["PrivilegeState"]));
				
					memberPrivilegeCollection.Add(memberPrivilege);
				}

				_cache.Add(memberPrivilegeCollection);
				ReplicationRequested(memberPrivilegeCollection);
			}

			return memberPrivilegeCollection;
		}


		public void SaveMemberPrivilege(Int32 memberID,
			Int32 privilegeID)
		{
			Privilege privilege = GetPrivileges().FindByID(privilegeID);

			if (privilege == null)
			{
				throw new Exception("Privilege " + privilegeID.ToString() + " not found.");
			}			
			
			MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID, "");

			Command command = new Command("mnMember", "up_MemberPrivilege_Save", memberID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
			Client.Instance.ExecuteAsyncWrite(command);

			memberPrivileges.Add(new MemberPrivilege(privilege, PrivilegeState.Individual));
			ReplicationRequested(memberPrivileges);		
		}


		public void DeleteMemberPrivilege(int memberID, int privilegeID)
		{
			Privilege privilege = GetPrivileges().FindByID(privilegeID);

			if (privilege == null)
			{
				throw new Exception("Privilege " + privilegeID.ToString() + " not found.");
			}			

			MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID, "");

			Command command = new Command("mnMember", "up_MemberPrivilege_Delete", memberID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
			Client.Instance.ExecuteAsyncWrite(command);

			memberPrivileges.Remove(privilegeID);
			ReplicationRequested(memberPrivileges);		
		}


		public GroupCollection GetGroups()
		{
			GroupCollection groupCollection = _cache.Get(GroupCollection.CACHE_KEY) as GroupCollection;

			if (groupCollection == null)
			{
				Command command = new Command("mnSystem", "up_Group_List", 0);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				groupCollection = new GroupCollection();

				foreach (DataRow row in dt.Rows)
				{
					Group group = new Group(Convert.ToInt32(row["GroupID"]),
						row["Description"].ToString());
					groupCollection.Add(group);
				}

				_cache.Insert(groupCollection);
			}

			return groupCollection;
		}


		public GroupMemberCollection GetGroupMembers(Int32 groupID)
		{
			GroupMemberCollection groupMemberCollection = _cache.Get(GroupMemberCollection.GetCacheKey(groupID)) as GroupMemberCollection;

			if (groupMemberCollection == null)
			{
				//todo - hit all member partitions ???
				Command command = new Command("mnSystem", "up_GroupMember_List", 0);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				groupMemberCollection = new GroupMemberCollection(groupID);

				foreach (DataRow row in dt.Rows)
				{
					groupMemberCollection.Add(Convert.ToInt32(row["MemberID"]));
				}

				_cache.Insert(groupMemberCollection);
			}

			return groupMemberCollection;
		}


		public void SaveGroupMember(Int32 memberID, Int32 groupID)
		{
			Command command = new Command("mnMember", "up_GroupMember_Save", memberID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
			Client.Instance.ExecuteAsyncWrite(command);

			//todo: cache & replicate
		}


		public void DeleteGroupMember(Int32 memberID, Int32 groupID)
		{
			Command command = new Command("mnMember", "up_GroupMember_Delete", memberID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
			Client.Instance.ExecuteAsyncWrite(command);

			//todo: cache & replicate
		}


		public PrivilegeCollection GetPrivileges()
		{
			PrivilegeCollection privilegeCollection = _cache.Get(PrivilegeCollection.CACHE_KEY) as PrivilegeCollection;

			if (privilegeCollection == null)
			{
				Command command = new Command("mnSystem", "up_Privilege_List", 0);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				privilegeCollection = new PrivilegeCollection();

				foreach (DataRow row in dt.Rows)
				{
					Privilege privilege = new Privilege(Convert.ToInt32(row["PrivilegeID"]),
						row["Description"].ToString());
					privilegeCollection.Add(privilege);
				}

				_cache.Insert(privilegeCollection);
			}

			return privilegeCollection;
		}


		public void SavePrivilege(Int32 privilegeID, string description)
		{
			Command command = new Command("mnSystem", "up_Privilege_Save", 0);
			command.AddParameter("PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
			command.AddParameter("Description", SqlDbType.VarChar, ParameterDirection.Input, description);
			Client.Instance.ExecuteAsyncWrite(command);
			
			//todo - cache and replicate
		}
		

		public void DeletePrivilege(int privilegeID)
		{
			Command command = new Command("mnSystem", "up_Privilege_Delete", 0);
			command.AddParameter("PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
			Client.Instance.ExecuteAsyncWrite(command);
		
			//todo - cache and replicate
		}
	}
}
