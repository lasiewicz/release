using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Caching;
using System.Collections.Generic;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Security;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ApproveQueue.ServiceAdapters;
using Spark.Common.Utilities;
using DBQueue = Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.Common.Adapter;
using Spark.Common.AccessService;
using Matchnet.Configuration.ValueObjects;
using Spark.Logging;
using ServiceConstants = Matchnet.Member.ValueObjects.ServiceConstants;


namespace Matchnet.Member.BusinessLogic
{
    public class MemberBL
    {
        private const Int32 GC_THRESHOLD = 10000;
        private const Int32 MAX_ATTRIBUTES_SAVE = 20;
        private const Int32 MAX_PHOTO_FILE_BYTES = 5120000;
        private const string DATE_FORMAT = "yyyy/MM/dd/HH/";
        private const Int32 DEFAULT_SAVE_DEPTH = 4;
        private const Int32 SUB_EXP_DATE_ATTRIBUTE_ID = 501;
        private const int MAX_LENGTH_EMAILADDRESS = 255;
        private const int CURRENT_MEMBERLOGON_VERSION = 1;

        public readonly static MemberBL Instance = new MemberBL();

        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        public event SynchronizationEventHandler SynchronizationRequested;

        //member events
        public delegate void MemberRequestEventHandler(bool cacheHit);
        public event MemberRequestEventHandler MemberRequested;

        public delegate void MemberAddEventHandler();
        public event MemberAddEventHandler MemberAdded;

        public delegate void MemberRemoveEventHandler();
        public event MemberRemoveEventHandler MemberRemoved;

        public delegate void MemberSaveEventHandler();
        public event MemberSaveEventHandler MemberSaved;

        //logon events
        public delegate void LogonRequestEventHandler(bool success);
        public event LogonRequestEventHandler LogonRequested;

        //registration events
        public delegate void RegistrationRequestEventHandler(bool success);
        public event RegistrationRequestEventHandler RegistrationRequested;

        public Matchnet.ICaching _cache;
        public Matchnet.ICaching _membaseCache;
        public Matchnet.ICaching _membaseDTOCache;
        private CacheItemRemovedCallback _expireMember;
        private CacheItemRemovedCallback _expireMemberAccess;
        private CacheItemRemovedCallback _expireMemberLogon;
        private string _expiredCountLock = "";
        private int _expiredCount = 0;
        private Int32 _ttl = 1200;

        public bool NUnit { get; set; }
        private MembaseConfig _membaseConfig;
        public MembaseConfig MembaseConfig { get { return _membaseConfig; } }
        private MembaseConfig _membaseDTOConfig;
        public MembaseConfig MembaseDTOConfig { get { return _membaseDTOConfig; } }
        private static ISettingsSA _settingsService = null;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public bool UseMembaseForBL
        {
            get
            {
                try
                {
                    string settingFromSingleton = SettingsService.GetSettingFromSingleton(ServiceConstants.MEMBASE_BL_SETTING_CONSTANT);
                    return Convert.ToBoolean(settingFromSingleton);
                }
                catch (Exception e)
                {
                    LogException(ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, e, Constants.NULL_INT);
                }
                return false;
            }
        }

        public bool EnableMemberDTO
        {
            get
            {
                try
                {
                    string settingFromSingleton = SettingsService.GetSettingFromSingleton(ServiceConstants.ENABLE_MEMBER_DTO_CONSTANT);
                    return Convert.ToBoolean(settingFromSingleton);
                }
                catch (Exception e)
                {
                    LogException(ServiceConstants.ENABLE_MEMBER_DTO_CONSTANT, e, Constants.NULL_INT);
                }
                return false;                
            }
        }

        public bool EnableMemberAttrSetCache
        {
            get
            {
                try
                {
                    string settingFromSingleton = SettingsService.GetSettingFromSingleton(ServiceConstants.ENABLE_MEMBER_ATTR_SET_CACHE_CONSTANT);
                    return Convert.ToBoolean(settingFromSingleton);
                }
                catch (Exception e)
                {
                    LogException(ServiceConstants.ENABLE_MEMBER_ATTR_SET_CACHE_CONSTANT, e, Constants.NULL_INT);
                }
                return false;
            }
        }

        public ICaching ICache
        {
            get
            {
                if (UseMembaseForBL)
                {
                    return _membaseCache;
                }
                return _cache;
            }
        }

        public ICaching MemberDTOCache
        {
            get
            {
                return _membaseDTOCache;
            }
        }

        private enum BroadcastMask : int
        {
            None = 0,
            Standard = 1,
            MOL = 2,
            Immediate = 4
        }

        public bool SaveLogonInfoFlag
        {
            get
            {
                bool b = false;
                try
                {
                    b = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("LOG_SAVE_LOGON_INFO"));
                }
                catch (Exception e)
                {
                    LogException("SaveLogonInfoFlag", e, Constants.NULL_INT);
                }
                return b;
            }
        }

        public List<int> BHCommunities
        {
            get
            {
                var communitiesList = new List<int>();
                try
                {
                    string communities = SettingsService.GetSettingFromSingleton(ServiceConstants.MNLOGON_COMMUNITY_LIST);
                    string[] communityArray = communities.Split(',');
                    communitiesList.AddRange(communityArray.Select(s => Convert.ToInt32(s)));
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberBL", e, null);
                }
                return communitiesList;
            }
        }

        private MemberBL()
        {
            NUnit = false;
            _membaseConfig = SettingsService.GetMembaseConfigByBucketFromSingleton(ServiceConstants.MEMBASE_WEB_BUCKET_NAME);
            _membaseCache = new Spark.Caching.MembaseCachingImpl(_membaseConfig);

            _membaseDTOConfig = SettingsService.GetMembaseConfigByBucketFromSingleton(ServiceConstants.MEMBASE_MEMBERDTO_BUCKET_NAME);
            _membaseDTOCache = new Spark.Caching.MembaseCachingImpl(_membaseDTOConfig);

                _cache = Matchnet.Caching.Cache.Instance;
            _expireMember = new CacheItemRemovedCallback(this.ExpireMemberCallback);
            _expireMemberAccess = new CacheItemRemovedCallback(this.ExpireMemberAccessCallback);
            _expireMemberLogon = new CacheItemRemovedCallback(this.ExpireMemberLogonCallback);
            _ttl = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SVC"));
            
        }

        private void IncrementExpirationCounter()
        {
            lock (_expiredCountLock)
            {
                _expiredCount++;

                if (_expiredCount > GC_THRESHOLD)
                {
                    _expiredCount = 0;
                    System.Diagnostics.Trace.WriteLine("___GC");
                    GC.Collect();
                }
            }
        }

        #region logon
        public Int32 Authenticate(string emailAddress, int communityId, string passwordHash, out int memberID)
        {
            try
            {

                var command = GetAuthCommand(emailAddress, communityId);

                SqlParameterCollection parameters = Client.Instance.ExecuteNonQuery(command);
                memberID = Convert.ToInt32(parameters["@MemberID"].Value);
                Int32 returnValue = Convert.ToInt32(parameters["ReturnValue"].Value);

                if (memberID > 0 && returnValue == 0)
                {
                    if (!passwordHash.Equals(parameters["@PasswordHash"].Value))
                    {
                        returnValue = -2;
                    }
                }

                LogonRequested(returnValue == 0);
                RollingFileLogger.Instance.LogInfoMessage(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL.Authenticate", string.Format("sp: {0}  CommunityId: {1} MemberId: {2}", command.StoredProcedureName, communityId, memberID), null);

                return returnValue;
            }
            catch (Exception ex)
            {
                throw (new BLException(String.Format("Authenticate() error (emailAddress:{0}, passwordHash:{1}", emailAddress, passwordHash)));
            }
        }

        private Command GetAuthCommand(string emailAddress, int communityId)
        {
            Command command = null;

            //BH members have same email and password for all communities.
            //Mingle members have different email and password for different communities.
            
            if(BHCommunities.Contains(communityId)) //BH Member. CommunityId is not required.
            {
                command = new Command("mnLogon", "up_Logon_Community_NoCase_NoCommunity", 0);
            }
            else //Non-BH member. CommunityId is required.
            {
                command = new Command("mnLogon", "up_Logon_Community_NoCase", 0);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);    
            }

            command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);
            command.AddParameter("@PasswordHash", SqlDbType.NVarChar, 100, ParameterDirection.Output);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Output);
            command.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
            return command;
        }

        public MemberRegisterResult Register(string emailAddress, string username, string passwordHash, Brand brand)
        {
            try
            {
                if(username == null) { username = string.Empty; }
                
                if (passwordHash.Length > MAX_LENGTH_EMAILADDRESS)
                {
                    RegistrationRequested(false);
                    return new MemberRegisterResult(RegisterStatusType.PasswordLengthExceeded, Constants.NULL_INT,
                                                    Constants.NULL_STRING);
                }

                if (!isValidEmailAddress(emailAddress))
                {
                    RegistrationRequested(false);
                    return new MemberRegisterResult(RegisterStatusType.EmailAddressNotAllowed, Constants.NULL_INT,
                                                    Constants.NULL_STRING);
                }

                Int32 memberID = KeySA.Instance.GetKey("MemberID");

                if (emailAddress.ToLower().IndexOf(username.ToLower()) > -1)
                {
                    username = memberID.ToString();
                }

                RegisterStatusType registerStatus = saveLogon(memberID, emailAddress, ref username, passwordHash,
                                                              brand.Site.Community.CommunityID, Constants.NULL_STRING, -1);

                if (registerStatus == RegisterStatusType.Success)
                {
                    Command command = new Command("mnMember", "dbo.up_Member_Save", memberID);
                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                    Client.Instance.ExecuteAsyncWrite(command);

                    SetMemberGroup(memberID, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

                    RegistrationRequested(registerStatus == RegisterStatusType.Success);
                    return new MemberRegisterResult(registerStatus, memberID, username);
                }
                else
                {
                    RegistrationRequested(registerStatus == RegisterStatusType.Success);
                    return new MemberRegisterResult(registerStatus, 0, Constants.NULL_STRING);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when registering the member.", ex));
            }
        }

        public CreateLogonCredentialsResult CreateLogonCredentials(string emailAddress, string username, string passwordHash, Brand brand)
        {
            try
            {
                var memberId = GetNewMemberID();

                var saveLogonResult = saveLogon(memberId, emailAddress, ref username, passwordHash,
                                      brand.Site.Community.CommunityID, Constants.NULL_STRING, -1);

                return new CreateLogonCredentialsResult
                {
                    Status = CreateLogonCredentialsResult.RegisterStatusToLogonCredentialsStatus(saveLogonResult),
                    UserName = username, 
                    MemberId = memberId
                };
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when creating client credenials.", ex));
            }
            

        }

        public void ToggleLogonDisabledFlag(int memberID, int disabledFlag)
        {
            string userName=string.Empty;

            // mark the disabled flag
            saveLogon(memberID, null, ref userName, null, 0, null, disabledFlag);

            try
            {
                // destroy the member's access token
                Spark.API.ServiceAdapters.APISA.Instance.DeleteAppMembers(memberID);
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT,
                                                          MethodBase.GetCurrentMethod().Name,
                                                          string.Format("Removed API app member records. memberId:{0}",
                                                                       memberID), null);
            }
            catch (Exception exception)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT,
                                                        MethodBase.GetCurrentMethod().Name, exception,
                                                        memberID);
            }
        }

        public RegisterStatusType saveLogon(Int32 memberID, string emailAddress, ref string username, string passwordHash, Int32 groupID, string currentEmailAddress, int disabledFlag)
        {
            //this does not run through the DAL b/c it is a sync write the depends on a good retval
            string usernameNew = username;
            string encryptedPasswordHash = Constants.NULL_STRING;
            string encryptedPasswordSalt = Constants.NULL_STRING;
            string encryptedEmailAddress = Constants.NULL_STRING;
            string encryptedCurrentEmailAddress = Constants.NULL_STRING;
            string key2 = SettingsService.GetSettingFromSingleton("KEY2");
           
            LogonActionType actionType = LogonActionType.NewUser;
            //Identify action type
            if (disabledFlag == 0 || disabledFlag == 1)
            {
                actionType = LogonActionType.DisabledFlagUpdate;
            }
            else if (!string.IsNullOrEmpty(currentEmailAddress))
            {
                if (!string.IsNullOrEmpty(username))
                {
                    actionType = LogonActionType.UsernameUpdate;
                }
                else if (!string.IsNullOrEmpty(emailAddress))
                {
                    actionType = LogonActionType.EmailAddressUpdate;
                }
                else if (!string.IsNullOrEmpty(passwordHash))
                {
                    actionType = LogonActionType.PasswordUpdate;
                }
            }

            if (!string.IsNullOrEmpty(passwordHash))
            {
                string[] encryptedTextAndSalt = Crypto.Instance.SplitEncryptedTextAndSalt(passwordHash);
                if (null != encryptedTextAndSalt)
                {
                    encryptedPasswordHash = encryptedTextAndSalt[0];
                    encryptedPasswordSalt = encryptedTextAndSalt[1];
                }
            }

            if (!string.IsNullOrEmpty(emailAddress))
            {
                try
                {
                    emailAddress = emailAddress.ToLower().Trim();
                    encryptedEmailAddress = Crypto.Instance.EncryptText(emailAddress, key2);
                }
                catch (Exception ignore)
                {
                }
            }

            if (!string.IsNullOrEmpty(currentEmailAddress))
            {
                try
                {
                    currentEmailAddress = currentEmailAddress.ToLower();
                    encryptedCurrentEmailAddress = Crypto.Instance.EncryptText(currentEmailAddress, key2);
                }
                catch (Exception ignore)
            {
                }
            }

            LogSaveLogonInfo("START", memberID, emailAddress, encryptedEmailAddress, encryptedCurrentEmailAddress, encryptedPasswordHash, encryptedPasswordSalt, usernameNew, groupID, false);
            
            PhysicalDatabases physicalDatabases = ConnectionDispenser.Instance.GetLogicalDatabase("mnLogon").GetPartition(0).PhysicalDatabases;
            RegisterStatusType registerStatus = RegisterStatusType.Success;
            for (Int32 attempts = 0; attempts < 5; attempts++)
            {
                LogonTransaction lt = new LogonTransaction();
                Exception ltEx;
                registerStatus = lt.RegisterWithSalt(memberID,
                                                        emailAddress,
                                                        encryptedEmailAddress,
                                                        usernameNew,
                                                        encryptedPasswordHash,
                                                        encryptedPasswordSalt,
                                                        encryptedCurrentEmailAddress,
                                                        groupID,
                                                        actionType,
                                                        disabledFlag,
                                                        out ltEx);
                lt.Dispose();

                if (ltEx != null)
                {
                    LogMessage("saveLogon", "Exception: " + ltEx.Message + ltEx.StackTrace, memberID);
                    throw ltEx;
                }

                if (registerStatus == RegisterStatusType.DuplicateUsername)
                {
                    usernameNew = username.Trim() + Guid.NewGuid().ToString().Substring(0, 4).ToUpper();
                }
                else
                {
                    break;
                }
            }

            username = usernameNew;
            
            LogSaveLogonInfo("END", memberID, emailAddress, encryptedEmailAddress, encryptedCurrentEmailAddress, encryptedPasswordHash, encryptedPasswordSalt, usernameNew, groupID, (registerStatus == RegisterStatusType.Success));

            return registerStatus;
        }

        private void LogSaveLogonInfo(string step, int memberID, string emailAddress, string encryptedEmailAddress, string encryptedCachedEmailAddress, string encryptedPasswordHash, string encryptedPasswordSalt, string usernameNew, int groupID, bool savedToDb)
        {
            if (SaveLogonInfoFlag)
            {
                string message =
                    string.Format(
                        "{0} saveLogon(): MemberId:{1}, Email: '{2}', EmailHash:'{3}', Username:'{4}', PasswordHash:'{5}', Salt:'{6}', CurrentEmailHash:'{7}', GroupId:{8}, Saved To DB: {9}",
                        step, memberID, emailAddress, encryptedEmailAddress, usernameNew, encryptedPasswordHash,
                        encryptedPasswordSalt, encryptedCachedEmailAddress, groupID, savedToDb);
                LogMessage("saveLogon",message,memberID);
                }
                }

        [Obsolete]
        public string GetPassword(Int32 memberID)
            {
            throw new NotSupportedException("Password retrieval is no longer supported.");
            }


        [Obsolete]
        public string GetPassword(string emailAddress)
        {
            throw new NotSupportedException("Password retrieval is no longer supported.");
        }

        public int GetNewMemberID()
        {
            return KeySA.Instance.GetKey("MemberID");
        }

        public int GetMemberID(string username)
        {
            return GetMemberID(username, Constants.NULL_INT);
        }

        public int GetMemberID(string username, int groupID)
        {
            try
            {
                // PM-218 Remove after deployed.
                if (AttributeMetadataSA.Instance.GetAttributes().GetAttribute("Username").Scope
                    != ScopeType.Community)
                {
                    groupID = CachedMember.GROUP_PERSONALS;
                }

                Command command = new Command("mnLogon", "dbo.up_LogonMemberCommunity_List", 0);

                command.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, username);

                if (groupID != Constants.NULL_INT)
                    command.AddParameter("@GroupID", SqlDbType.NVarChar, ParameterDirection.Input, groupID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count == 1)
                {
                    return Convert.ToInt32(dt.Rows[0]["MemberID"]);
                }
                else
                {
                    return Constants.NULL_INT;
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving memberID based off of username.", ex));
            }
        }

        public int GetMemberIDByEmail(string emailAddress)
        {
            return GetMemberIDByEmail(emailAddress, Constants.NULL_INT);
        }

        public int GetMemberIDByEmail(string emailAddress, int communityId)
        {
            try
            {
                Command command = new Command("mnLogon", "dbo.up_LogonMemberCommunity_MemberIDbyEmailAddress", 0);
                command.AddParameter("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, emailAddress);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Output);
                if(communityId != Constants.NULL_INT)
                {
                    command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input,communityId);
                }
                SqlParameterCollection results = Client.Instance.ExecuteNonQuery(command);

                if (results["@MemberID"].Value != DBNull.Value)
                {
                    return Convert.ToInt32(results["@MemberID"].Value);
                }
                else
                {
                    return Constants.NULL_INT;
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving memberID based off of e-mail address.", ex));
            }
        }

        /// <summary>
        /// Checks if an email address exists - used for registration. EmailAddress is shared across BH communities.
        /// For BH sites, checks if an email address exists in any one of the BH communities(shared logon communities).
        /// For Mingles sites, checks if an email exists in the given community.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public bool CheckEmailAddressExists(string emailAddress, int communityId)
        {
            try
            {
                var command = new Command("mnLogon", "dbo.up_LogonMemberCommunity_CheckEmailAddressExists", 0);
                command.AddParameter("@EmailAddress", SqlDbType.NVarChar, ParameterDirection.Input, emailAddress);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);
                command.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
                
                SqlParameterCollection results = Client.Instance.ExecuteNonQuery(command);

                bool emailExists = Convert.ToBoolean(results["ReturnValue"].Value);
                return emailExists;

            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when checking if an email address already exists", ex));
            }
        }

        //This should be deprecated after the next release (4/20/12)
        /// <summary>
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        public void SetMemberGroup(Int32 memberID,
            Int32 communityID,
            Int32 siteID,
            Int32 brandID)
        {
            Command command = new Command("mnMember", "dbo.up_MemberGroup_Save", memberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@GroupID1", SqlDbType.Int, ParameterDirection.Input, communityID);

            if (siteID != Constants.NULL_INT)
            {
                command.AddParameter("@GroupID2", SqlDbType.Int, ParameterDirection.Input, siteID);
            }
            if (brandID != Constants.NULL_INT)
            {
                command.AddParameter("@GroupID3", SqlDbType.Int, ParameterDirection.Input, brandID);
            }

            command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            Client.Instance.ExecuteAsyncWrite(command);
        }


        private bool isValidEmailAddress(string emailAddress)
        {
            int positionAtSign;
            string domainName;

            if (emailAddress.Length == 0)
            {
                return false;
            }

            positionAtSign = emailAddress.IndexOf("@");
            if (positionAtSign == -1)
            {
                return false;
            }

            if (emailAddress.IndexOf(".", positionAtSign) == -1)
            {
                return false;
            }

            if (emailAddress.ToLower().IndexOf("postmaster") == 0)
            {
                return false;
            }

            if (emailAddress.Length > positionAtSign)
            {
                string key = "BlockedDomainList";

                BlockedDomainList blockedDomainList = ICache.Get(BlockedDomainList.CACHE_KEY) as BlockedDomainList;

                if (blockedDomainList == null)
                {
                    blockedDomainList = new BlockedDomainList();

                    Command command = new Command("mnLogon", "dbo.up_BlockedDomain_List", 0);
                    DataTable dt = Client.Instance.ExecuteDataTable(command);

                    Int32 rowCount = dt.Rows.Count;
                    for (Int32 rowNum = 0; rowNum < rowCount; rowNum++)
                    {
                        DataRow row = dt.Rows[rowNum];
                        blockedDomainList.Add(row["DomainName"].ToString());
                    }

                    ICache.Add(blockedDomainList);
                }

                if (blockedDomainList.Contains(emailAddress.Substring(positionAtSign + 1)))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Once deployment is done, this method shouldn't used anymore.  Don't use this or you will get an serialization problem.
        /// </summary>
        /// <param name="clientHostname"></param>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="lastLogon"></param>
        public void SaveMemberLastLogon(string clientHostname, int memberID, int groupID, DateTime lastLogon)
        {

            SaveMemberLastLogon(clientHostname, memberID, groupID, lastLogon, Constants.NULL_INT);
        }

        public void SaveMemberLastLogon(string clientHostname, int memberID, int groupID, DateTime lastLogon, int adminMemberID)
        {

            // this method call will get us an instance of CachedMemberLogon no matter what even if it has to pull it from the DB.
            // if pulled from the DB, replication and web server synchro already took place
            CachedMemberLogon cachedMemberLogon = getCachedMemberLogon(clientHostname, null, memberID, groupID, false, CURRENT_MEMBERLOGON_VERSION);

            // save to db before doing anything
            saveMemberLastLogon(memberID, groupID, lastLogon, adminMemberID);

            #region Updating cache item
            int keepCount = 5;
            string strKeepCount = SettingsService.GetSettingFromSingleton("LAST_LOGON_KEEP_COUNT");
            if (strKeepCount != null && strKeepCount != string.Empty)
            {
                keepCount = Convert.ToInt32(strKeepCount);
            }

            // add the new last logon
            cachedMemberLogon.LastLogonsNew.Add(new LastLogon() {AdminMemberId = adminMemberID, LastLogonDate = lastLogon});

            if (cachedMemberLogon.LastLogonsNew.Count > keepCount)
            {
                // drop off the oldest date and add the new date
                cachedMemberLogon.LastLogonsNew.Sort((x, y) => y.LastLogonDate.CompareTo(x.LastLogonDate));
                cachedMemberLogon.LastLogonsNew.RemoveRange(keepCount, cachedMemberLogon.LastLogonsNew.Count - keepCount);
            }
            #endregion

            InsertCachedMemberLogon(cachedMemberLogon, !UseMembaseForBL);
            bool synchronize = null != clientHostname;
            if (synchronize)
            {
                SynchronizationRequested(cachedMemberLogon.GetCacheKey(), cachedMemberLogon.ReferenceTracker.Purge(clientHostname));
            }
        }

        private void saveMemberLastLogon(int memberID, int groupID, DateTime lastLogon, int adminMemberID)
        {
            Command command = new Command("mnLogon", "usp_Insert_MemberLastLogon", 0);
            command.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, groupID);
            command.AddParameter("@lastlogondate", SqlDbType.DateTime, ParameterDirection.Input, lastLogon);
            if(adminMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@adminmemberid", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            }
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public string GetMemberSalt(string encryptedEmail, int communityId)
        {
            string salt = Constants.NULL_STRING;
            Command command = null;
            //BH members have same email and password for all communities.
            //Mingle members have different email and password for different communities.

           command = new Command("mnLogon", "dbo.up_GetMemberValueCommunity", 0);
           command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);

            RollingFileLogger.Instance.LogInfoMessage(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT,
                                                      "MemberBL.GetMemberSalt",
                                                      string.Format("sp: {0} CommunityId: {1} Email: {2}",
                                                          command.StoredProcedureName, communityId, encryptedEmail), null);
            command.AddParameter("@Member", SqlDbType.NVarChar, ParameterDirection.Input, encryptedEmail);
            command.AddParameter("@Value", SqlDbType.NVarChar, 100, ParameterDirection.Output);
            SqlParameterCollection results = Client.Instance.ExecuteNonQuery(command);

            if (results["@Value"].Value != DBNull.Value)
            {
                salt = results["@Value"].Value.ToString();
            }
            return salt;
        }

        /// <summary>
        /// This method is going away. Don't use this or you will have problems with serialization.
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="cacheReference"></param>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="forceLoad"></param>
        /// <returns></returns>
        public CachedMemberLogon GetCachedMemberLogon(string clientHostName,
            CacheReference cacheReference, int memberID, int groupID, bool forceLoad)
        {
            return getCachedMemberLogon(clientHostName,
                cacheReference, memberID, groupID, forceLoad, 0);
        }

        public CachedMemberLogon GetCachedMemberLogon(string clientHostName,
            CacheReference cacheReference, int memberID, int groupID, bool forceLoad, int cachedMemberLogonVersion)
        {
            return getCachedMemberLogon(clientHostName,
                cacheReference, memberID, groupID, forceLoad, cachedMemberLogonVersion);
        }

        private CachedMemberLogon getCachedMemberLogon(string clientHostName,
            CacheReference cacheReference, int memberID, int groupID, bool forceLoad, int cachedMemberLogonVersion)
        {
            CachedMemberLogon cachedMemberLogon = null;

            try
            {
                cachedMemberLogon = ICache.Get(CachedMemberLogon.GetCacheKey(memberID, groupID)) as CachedMemberLogon;

                // if the versions do not match, dump it
                if (cachedMemberLogon != null && cachedMemberLogon.Version != cachedMemberLogonVersion)
                    cachedMemberLogon = null;

                if (cachedMemberLogon == null || forceLoad)
                {
                    cachedMemberLogon = new CachedMemberLogon(memberID, groupID);
                    cachedMemberLogon.Version = cachedMemberLogonVersion;
                    SqlDataReader dataReader = null;

                    try
                    {
                        try
                        {
                            Command command = new Command("mnLogon", "up_MemberLastLogon_List", 0);
                            command.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, memberID);
                            command.AddParameter("@groupid", SqlDbType.Int, ParameterDirection.Input, groupID);
                            dataReader = Client.Instance.ExecuteReader(command);
                        }
                        catch (Exception ex)
                        {
                            throw new BLException("BL error while retrieving MemberLastLogon entries from the database", ex);
                        }

                        while (dataReader.Read())
                        {
                            cachedMemberLogon.LastLogonsNew.Add(new LastLogon()
                                                                    {
                                                                        LastLogonDate =
                                                                            Convert.ToDateTime(
                                                                                dataReader["LastLogonDate"]),
                                                                        AdminMemberId =
                                                                            dataReader["AdminMemberID"] == DBNull.Value
                                                                                ? Constants.NULL_INT
                                                                                : Convert.ToInt32(
                                                                                    dataReader["AdminMemberID"])
                                                                    });
                        }
                    }
                    finally
                    {
                        if (dataReader != null)
                        {
                            dataReader.Close();
                        }
                    }

                    // since we got the data from the db, let's insert it into the cache and synchro all the web servers
                    InsertCachedMemberLogon(cachedMemberLogon, !UseMembaseForBL);
                    bool synchronize = null != clientHostName;
                    if (synchronize)
                    {
                        SynchronizationRequested(cachedMemberLogon.GetCacheKey(), cachedMemberLogon.ReferenceTracker.Purge(clientHostName));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("getCachedMemberLogon() error MemberID: {0}, GroupID: {1}", memberID, groupID), ex);
            }

            if (clientHostName != null && cacheReference != null)
            {
                cachedMemberLogon.ReferenceTracker.Add(clientHostName, cacheReference);
            }

            return cachedMemberLogon;
        }

        public void InsertCachedMemberLogon(CachedMemberLogon cachedMemberLogon, bool replicate)
        {
            cachedMemberLogon.CacheTTLSeconds = _ttl;
            cachedMemberLogon.CacheMode = CacheItemMode.Sliding;
            ICache.Insert(cachedMemberLogon);

            if (replicate)
            {
                ReplicationRequested(cachedMemberLogon);
            }
        }



        #region CachedMemberEmail
        /// <summary>
        /// Gets a passed in memberID's changed email address history
        /// The user story for this is FNOLA-369 & FNOLA-1384
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="cacheReference"></param>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="forceLoad"></param>
        /// <param name="cachedMemberLogonVersion"></param>
        /// <returns></returns>
        public CachedMemberEmail GetCachedMemberEmail(string   clientHostName,
            CacheReference cacheReference, int memberID, bool forceLoad, int cachedMemberEmailVersion)
        {
            return getCachedMemberEmail(clientHostName,
                cacheReference, memberID, forceLoad, cachedMemberEmailVersion);
        }

        private CachedMemberEmail getCachedMemberEmail(string clientHostName,
            CacheReference cacheReference, int memberID, bool forceLoad, int cachedMemberEmailVersion)
        {
            CachedMemberEmail cachedMemberEmail = null;

            try
            {
                cachedMemberEmail = ICache.Get(CachedMemberEmail.GetCacheKey(memberID)) as CachedMemberEmail;

                // if the versions do not match, dump it
                if (cachedMemberEmail != null && cachedMemberEmail.Version != cachedMemberEmailVersion)
                    cachedMemberEmail = null;

                if (cachedMemberEmail == null || forceLoad)
                {
                    cachedMemberEmail = new CachedMemberEmail(memberID);
                    cachedMemberEmail.Version = cachedMemberEmailVersion;
                    SqlDataReader dataReader = null;

                    try
                    {
                        try
                        {
                            Command command = new Command("mnLogon", "up_MemberLastEmail_List", 0);
                            command.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, memberID);
                            dataReader = Client.Instance.ExecuteReader(command);
                        }
                        catch (Exception ex)
                        {
                            throw new BLException("BL error while retrieving MemberLastEmail entries from the database", ex);
                        }

                        while (dataReader.Read())
                        {
                            cachedMemberEmail.LastEmails.Add(new LastEmail()
                            {
                                LastEmailAddress = dataReader["Email"].ToString(),
                                LastEmailDate = Convert.ToDateTime(dataReader["ServiceUpdateDate"]),
                                AdminMemberId = dataReader["AdminMemberID"] == DBNull.Value ? Constants.NULL_INT : Convert.ToInt32(dataReader["AdminMemberID"])
                            });
                        }
                    }
                    finally
                    {
                        if (dataReader != null)
                        {
                            dataReader.Close();
                        }
                    }

                    // We got the data from the db, let's insert it into the cache and synchro all the web servers
                    InsertCachedMemberEmail(cachedMemberEmail, !UseMembaseForBL);
                    bool synchronize = null != clientHostName;
                    if (synchronize)
                    {
                        SynchronizationRequested(cachedMemberEmail.GetCacheKey(), cachedMemberEmail.ReferenceTracker.Purge(clientHostName));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("getCachedMemberEmail() error MemberID: {0}", memberID), ex);
            }

            if (clientHostName != null && cacheReference != null)
            {
                cachedMemberEmail.ReferenceTracker.Add(clientHostName, cacheReference);
            }

            return cachedMemberEmail;
        }

        public void InsertCachedMemberEmail(CachedMemberEmail cachedMemberEmail, bool replicate)
        {
            ICache.Insert(cachedMemberEmail);

            if (replicate)
            {
                ReplicationRequested(cachedMemberEmail);
            }
        }

        #endregion



        /// <summary>
        /// Will create a new token for resetting the password.
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns>Guid token value generated and inserted into the database</returns>
        public string ResetPassword(int memberId)
        {
            var newToken = Guid.NewGuid();
            var tokenTimeout = Convert.ToInt32(RuntimeSettings.GetSetting("PASSWORD_RESET_TOKEN_TIMEOUT"));

            var cmd = new Command("mnLogon", "dbo.up_ResetPasswordToken_Save", 0);
            cmd.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
            cmd.AddParameter("@TokenGUID", SqlDbType.VarChar, ParameterDirection.Input, newToken.ToString());
            cmd.AddParameter("@ExpirationDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now.AddMinutes(tokenTimeout));

            var sw = new SyncWriter();
            Exception swEx = null;
            sw.Execute(cmd, out swEx);
            if(swEx != null)
            {
                throw swEx;
            }

            return newToken.ToString();
        }

        /// <summary>
        /// Validates the token to see if it hasn't expired yet. If it's still valid, memberID is returned.
        /// </summary>
        /// <param name="tokenGuidString"></param>
        /// <returns>MemberID if token is valid still, 0 if not.</returns>
        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            var cmd = new Command("mnLogon", "dbo.up_ResetPasswordToken_List", 0);
            cmd.AddParameter("@TokenGUID", SqlDbType.VarChar, ParameterDirection.Input, tokenGuidString);

            var dt = Client.Instance.ExecuteDataTable(cmd);
            if (dt.Rows.Count.Equals(0))
                return 0;

            var memberId = Convert.ToInt32(dt.Rows[0][0]);

            return memberId;
        }
        
        /// <summary>
        /// Generates a impersonate token admins can use to log in as a certain member.
        /// </summary>
        /// <param name="adminMemberId"></param>
        /// <param name="impersonateMemberId"></param>
        /// <param name="impersonateBrandId"></param>
        /// <returns></returns>
        public string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId)
        {
            var newToken = Guid.NewGuid();
            var tokenTimeout = Convert.ToInt32(RuntimeSettings.GetSetting("IMPERSONATE_TOKEN_TIMEOUT"));

            var cmd = new Command("mnLogon", "dbo.up_ImpersonateToken_Save", 0);
            cmd.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
            cmd.AddParameter("@ImpersonateMemberID", SqlDbType.Int, ParameterDirection.Input, impersonateMemberId);
            cmd.AddParameter("@ImpersonateBrandID", SqlDbType.Int, ParameterDirection.Input, impersonateBrandId);
            cmd.AddParameter("@TokenGUID", SqlDbType.VarChar, ParameterDirection.Input, newToken.ToString());
            cmd.AddParameter("@ExpirationDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now.AddMinutes(tokenTimeout));
            cmd.AddParameter("@IsValid", SqlDbType.Bit, ParameterDirection.Input, 1);

            var sw = new SyncWriter();
            Exception swEx = null;
            sw.Execute(cmd, out swEx);
            if (swEx != null)
            {
                throw swEx;
            }

            return newToken.ToString();
        }

        /// <summary>
        /// Validates the admin plus impersonate token combo and provides the details for the token if valid.
        /// </summary>
        /// <param name="adminMemberId"></param>
        /// <param name="impersonateTokenGUID"></param>
        /// <returns></returns>
        public ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            var cmd = new Command("mnLogon", "dbo.up_ImpersonateToken_List", 0);
            cmd.AddParameter("@TokenGUID", SqlDbType.VarChar, ParameterDirection.Input, impersonateTokenGUID);

            var dt = Client.Instance.ExecuteDataTable(cmd);
            if (dt.Rows.Count.Equals(0))
                return null;

            var impersonateMemberId = Convert.ToInt32(dt.Rows[0][0]);
            var impersonateBrandId = Convert.ToInt32(dt.Rows[0][1]);
            var adminMemberId = Convert.ToInt32(dt.Rows[0][2]);
            return new ImpersonateToken()
                       {
                           AdminMemberId = adminMemberId,
                           ImpersonateMemberId = impersonateMemberId,
                           ImperonateBrandId = impersonateBrandId,
                           TokenGUID = impersonateTokenGUID
                       };
        }

        /// <summary>
        /// Token should be only good for one use, so this method will expire the token belonging to an admin.
        /// </summary>
        /// <param name="adminMemberId"></param>
        public void ExpireImpersonationToken(int adminMemberId)
        {
            var cmd = new Command("mnLogon", "dbo.up_ImpersonateToken_UpdateIsValid", 0);
            cmd.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberId);
            cmd.AddParameter("@IsValid", SqlDbType.Bit, ParameterDirection.Input, 0);

            
            Client.Instance.ExecuteAsyncWrite(cmd);
        }
        #endregion

        #region member
        private void RemoveMemberDTOFromCache(int memberID)
        {
            if (EnableMemberDTO)
            {
                MemberDTOCache.RemoveWithoutGet(MemberDTO.GetCacheKey(memberID, MemberType.Search));
                MemberDTOCache.RemoveWithoutGet(MemberDTO.GetCacheKey(memberID, MemberType.MatchMail));
                MemberDTOCache.RemoveWithoutGet(MemberDTO.GetCacheKey(memberID, MemberType.Full));
            }
        }

        private void RemoveMemberAttributeSetsFromCache(int memberID)
        {
            if (EnableMemberAttrSetCache)
            {
                StringEnum attributeSetNames = new StringEnum(typeof(APIAttributeSets));
                Parallel.ForEach(attributeSetNames.GetStringValuesList(), attributeSetName =>
                {
                    _membaseCache.RemoveWithoutGet(attributeSetName + ServiceConstants.FIELD_SEPARATOR + memberID);
                });
            }
        }

        public void ExpireEntireMemberCache()
        {
            ICache.Clear();
            if (EnableMemberDTO)
            {
                MemberDTOCache.Clear();
            }
        }

        public void ExpireCachedMember(int memberID)
        {
            try
            {
                CachedMemberBytes cachedMemberBytes = ICache.Get(CachedMemberBytes.GetCacheKey(memberID)) as CachedMemberBytes;
                if (cachedMemberBytes != null)
                {
                    if (!UseMembaseForBL)
                    {
                        ReplicationRequested(cachedMemberBytes);
                    }
                    SynchronizationRequested(cachedMemberBytes.GetCacheKey(), cachedMemberBytes.ReferenceTracker.Purge(Constants.NULL_STRING));
                    
                }

                if (UseMembaseForBL)
                {
                    ICache.Remove(CachedMemberBytes.GetCacheKey(memberID));
                    RemoveMemberDTOFromCache(memberID);
                }

                //this will only excecute if flag is turned on
                RemoveMemberAttributeSetsFromCache(memberID);
            }
            catch (Exception ex)
            {
                throw new BLException("ExpireCachedMember() error (memberID: " + memberID.ToString() + ").", ex);
            }
        }

        public bool ExpireCachedMemberAccess(int memberID, bool replicate, bool synchronize, bool expireAccessSvc)
        {
            bool returnValue = true;
            AccessServiceAdapter adapter = new AccessServiceAdapter();
            try
            {
                if (UseMembaseForBL)
                {
                    ICache.Remove(CachedMemberAccess.GetCacheKey(memberID));
                    RemoveMemberDTOFromCache(memberID);
                }
                else
                {
                    //this will refresh, sync and replicate
                    getCachedMemberAccess("", null, memberID, true);
                }

                //this will only excecute if flag is turned on
                RemoveMemberAttributeSetsFromCache(memberID);

                if (expireAccessSvc)
                {
                    adapter.GetProxyInstanceForBedrock().ExpireCachedPrivilege(memberID);
                }

            }
            catch (Exception ex)
            {
                throw new BLException("ExpireCachedMemberAccess() error (memberID: " + memberID.ToString() + ").", ex);
            }
            finally
            {
                adapter.CloseProxyInstance();
            }

            return returnValue;
        }

        public CachedMember[] GetCachedMembers(string clientHostName,
            CacheReference cacheReference,
            Int32[] memberIDs,
            bool forceLoad)
        {
            CachedMember[] members = null;
            Dictionary<int, CachedMember> cachedMembersDictionary = GetCachedMembersDictionary(clientHostName, cacheReference, memberIDs, forceLoad);
            if (null != cachedMembersDictionary)
            {
                members = cachedMembersDictionary.Values.ToArray();
            }
            return members;
        }

        public CachedMemberAccess[] GetCachedMemberAccesses(string clientHostName,
            CacheReference cacheReference,
            Int32[] memberIDs,
            bool forceLoad)
        {
            CachedMemberAccess[] memberAccesses = null;
            Dictionary<int, CachedMemberAccess> cachedMemberAccessesDictionary = GetCachedMemberAccessesDictionary(clientHostName, cacheReference, memberIDs, forceLoad);
            if (null != cachedMemberAccessesDictionary)
            {
                memberAccesses = cachedMemberAccessesDictionary.Values.ToArray();
            }
            return memberAccesses;
        }

        public Dictionary<int, CachedMember> GetCachedMembersDictionary(string clientHostName,
                                                                        CacheReference cacheReference,
                                                                        Int32[] memberIDs,
                                                                        bool forceLoad) 
        {
            Dictionary<int, CachedMember> members = new Dictionary<int, CachedMember>();
            List<string> keys = (from int memberId in memberIDs select CachedMemberBytes.GetCacheKey(memberId)).ToList<string>();
            IDictionary<string, object> objects = ICache.Get(keys);
            if (objects != null)
            {
                for (int i = 0; i < memberIDs.Length; i++)
                {
                    int memberID = memberIDs[i];
                    string key = CachedMemberBytes.GetCacheKey(memberID);
                    CachedMember cachedMember = null;
                    if (objects.Keys.Contains(key))
                    {
                        cachedMember = objects[key] as CachedMember;
                    }

                    if (cachedMember != null)
                    {
                        members[memberID] = cachedMember;
                    }
                    else
                    {
                        members[memberID] = GetCachedMember(clientHostName, cacheReference, memberID, true);
                    }
                }
            }
            return members;
        }

        public Dictionary<int,CachedMemberAccess> GetCachedMemberAccessesDictionary(string clientHostName,
            CacheReference cacheReference,
            Int32[] memberIDs,
            bool forceLoad)
        {
            Dictionary<int, CachedMemberAccess> memberAccesses = new Dictionary<int, CachedMemberAccess>();
            List<string> keys = (from int memberId in memberIDs select CachedMemberAccess.GetCacheKey(memberId)).ToList<string>();
            IDictionary<string, object> objects = ICache.Get(keys);
            if (objects != null)
            {
                for (int i = 0; i < memberIDs.Length; i++)
                {
                    int memberID = memberIDs[i];
                    string key = CachedMemberAccess.GetCacheKey(memberID);
                    CachedMemberAccess memberAccess = null;
                    if (objects.Keys.Contains(key))
                    {
                        memberAccess = objects[key] as CachedMemberAccess;
                    }

                    if (memberAccess != null)
                    {
                        memberAccesses[memberID]= memberAccess;
                    }
                    else
                    {
                        memberAccesses[memberID]=GetCachedMemberAccess(clientHostName, cacheReference, memberID, true);
                    }
                }
            }
            return memberAccesses;
        }

        public CachedMember GetCachedMember(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad)
        {
            return GetCachedMember(clientHostName, cacheReference, memberID, forceLoad, 0);
        }

        [Obsolete]
        public byte[] GetCachedMemberBytes(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad)
        {
            CachedMemberBytes replaceMemberBytes = getCachedMemberBytes(clientHostName,
                cacheReference,
                memberID,
                forceLoad, CachedMemberBytes.CACHED_MEMBERBYTES_VERSION);
            CachedMember cachedMember = new CachedMember(replaceMemberBytes.MemberBytes, 0);
            return cachedMember.ToByteArray();
        }


        #region added versioning
        //versioning

        public CachedMember GetCachedMember(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad, int version)
        {
            CachedMember cm = new CachedMember(getCachedMemberBytes(clientHostName,
                cacheReference,
                memberID,
                forceLoad, version).MemberBytes, version);

            return cm;
        }


        public CachedMember[] GetCachedMembers(string clientHostName,
            CacheReference cacheReference,
            Int32[] memberIDs,
            bool forceLoad, int version)
        {
            CachedMember[] members = new CachedMember[memberIDs.Length];

            for (Int32 memberNum = 0; memberNum < memberIDs.Length; memberNum++)
            {
                members[memberNum] = GetCachedMember(clientHostName,
                    cacheReference,
                    memberIDs[memberNum],
                    forceLoad, version);
            }

            return members;
        }

        public byte[] GetCachedMemberBytes(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad, int version)
        {
            return getCachedMemberBytes(clientHostName,
                cacheReference,
                memberID,
                forceLoad, version).MemberBytes;
        }

        public CachedMemberAccess GetCachedMemberAccess(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad)
        {
            return getCachedMemberAccess(clientHostName, cacheReference, memberID, forceLoad);
        }

        private CachedMemberBytes getCachedMemberBytes(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad, int version)
        {
            

            CachedMemberBytes cachedMemberBytes = null;
            Attributes attributes;

            try
            {
                bool overrideClientVersionRequest = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST"));
                bool overrideForceLoad = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD"));

                bool ignoreCache = overrideForceLoad ? false : forceLoad;

                if (!ignoreCache)
                {
                    string cacheKey = CachedMemberBytes.GetCacheKey(memberID);
                    cachedMemberBytes = ICache.Get(cacheKey) as CachedMemberBytes;
                }

                if (cachedMemberBytes != null)
                {   //Always return version asked for. This used to always return the current version, but this was causing issues 
                    //with client deserialization. The 'MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST' setting will force a return to the
                    //original behavior.
                    if (cachedMemberBytes.Version == version || (overrideClientVersionRequest && cachedMemberBytes.Version == CachedMemberBytes.CACHED_MEMBERBYTES_VERSION))
                        MemberRequested(true);

                }

                //based on setting, look for either current version or client requested version
                bool reloadMemberBytes = overrideClientVersionRequest
                                                ? cachedMemberBytes == null || cachedMemberBytes.Version != CachedMemberBytes.CACHED_MEMBERBYTES_VERSION
                                                : cachedMemberBytes == null || cachedMemberBytes.Version != version;


                if (reloadMemberBytes)
                {
                    CachedMember cachedMember = new CachedMember(memberID, version);
                    SqlDataReader dataReader = null;

                    try
                    {
                        try
                        {
                            Command command = new Command("mnMember", "up_Member_List_Service2", memberID);
                            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                            dataReader = Client.Instance.ExecuteReader(command);
                        }
                        catch (Exception ex)
                        {
                            throw (new BLException("BL error occured when retrieving member profile.", ex));
                        }

                        attributes = AttributeMetadataSA.Instance.GetAttributes();

                        getTextAttributes(attributes, cachedMember, dataReader);

                        //int attributes
                        dataReader.NextResult();
                        getIntAttributes(cachedMember, dataReader);

                        //date attributes
                        dataReader.NextResult();
                        getDateAttributes(cachedMember, dataReader);

                        //photos
                        dataReader.NextResult();
                        getPhotos(cachedMember, dataReader);

                        //photoFiles
                        if(version >=5)
                        {
                            dataReader.NextResult();
                            getPhotoFiles(cachedMember, dataReader);
                        }
                    }
                    finally
                    {
                        if (dataReader != null)
                        {
                            dataReader.Close();
                        }
                    }

                    cachedMemberBytes = new CachedMemberBytes(memberID, cachedMember.ToByteArray(), version);

                    InsertCachedMemberBytes(cachedMemberBytes, !UseMembaseForBL);
                    bool synchronize = null != clientHostName;
                    if (synchronize)
                    {
                        string cacheKey = cachedMemberBytes.GetCacheKey();
                        SynchronizationRequested(cacheKey, cachedMemberBytes.ReferenceTracker.Purge(clientHostName));
                    }
                    MemberRequested(false);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("GetCachedMemberBytes() error (memberID: " + memberID.ToString() + ").", ex);
            }

            if (clientHostName != null && cacheReference != null)
            {
                cachedMemberBytes.ReferenceTracker.Add(clientHostName, cacheReference);
            }

            return cachedMemberBytes;
        }

        private CachedMemberAccess getCachedMemberAccess(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad)
        {
            AccessServiceAdapter adapter = new AccessServiceAdapter();
            CachedMemberAccess cachedMemberAccess = null;

            try
            {
                string cacheKey = CachedMemberAccess.GetCacheKey(memberID);
                cachedMemberAccess = ICache.Get(cacheKey) as CachedMemberAccess;

                #region Deprecated-we no longer need to perform this check
                //get list of sites that has migrated privileges completed
                //List<int> migratedPrivilegeSites = new List<int>();
                //List<int> sites = getSiteIDList(memberID);
                //foreach (int i in sites)
                //{
                //    if (HasPrivilegesBeenMigratedToUPSAccess(Constants.NULL_INT, i, Constants.NULL_INT))
                //    {
                //        migratedPrivilegeSites.Add(i);
                //    }
                //}

                ////Check for outdated CachedMemberAccess due to migration process
                ////This is to support migrating privilege process, so we don't have to restart web servers whenever
                ////we finish migrating privileges for a site.
                //if (cachedMemberAccess != null)
                //{
                //    foreach (int i in migratedPrivilegeSites)
                //    {
                //        if (!cachedMemberAccess.MigratedPrivilegeSites.Contains(i))
                //        {
                //            cachedMemberAccess = null;
                //            System.Diagnostics.EventLog.WriteEntry(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberBL.getCachedMemberAccess(): cached object does not contain migrated privilege for siteID: " + i.ToString() + ", memberID: " + memberID.ToString() + ". Will refresh from Access Svc."); 
                //            break;
                //        }
                //    }
                //}

                //if (migratedPrivilegeSites.Count <= 0)
                //{
                //    System.Diagnostics.EventLog.WriteEntry(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberBL.getCachedMemberAccess(): No sites has migrated privileges, will NOT call Access Svc for privileges for member: " + memberID.ToString());
                //}
                #endregion

                if (cachedMemberAccess == null || forceLoad)
                {

                    AccessPrivilege[] accessPrivilege;

                    if (forceLoad)
                    {
                        accessPrivilege = adapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAllWithIgnoreCache(memberID);
                    }
                    else
                    {
                        accessPrivilege = adapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAll(memberID);
                    }

                    if (cachedMemberAccess != null && null != clientHostName)
                    {
                        //System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "MemberBL.getCachedMemberAccess: Calling synchronization for cachekey: " + cachedMemberAccess.GetCacheKey());
                        SynchronizationRequested(cachedMemberAccess.GetCacheKey(), cachedMemberAccess.ReferenceTracker.Purge(clientHostName));
                    }

                    if (accessPrivilege != null)
                    {
                        cachedMemberAccess = new CachedMemberAccess(memberID, accessPrivilege);
                        cachedMemberAccess.MigratedPrivilegeSites = new List<int>();

                        if (clientHostName != null && cacheReference != null && cachedMemberAccess != null)
                        {
                            //System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "Adding to existing Cache referenceTracker clientHostName:" + clientHostName + ", memberID: " + memberID.ToString());
                            cachedMemberAccess.ReferenceTracker.Add(clientHostName, cacheReference);
                        }

                        //save into cache
                        InsertCachedMemberAccess(cachedMemberAccess, !UseMembaseForBL);
                    }
                    else
                    {
                        System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "MemberBL received null when calling AccessService.GetCustomerPrivilegeListAll(" + memberID.ToString() + ")", EventLogEntryType.Warning);
                        cachedMemberAccess = null;
                    }

                }
                else
                {
                    if (clientHostName != null && cacheReference != null && cachedMemberAccess != null)
                    {
                        //System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "Adding to existing Cache referenceTracker clientHostName:" + clientHostName + ", memberID: " + memberID.ToString());
                        cachedMemberAccess.ReferenceTracker.Add(clientHostName, cacheReference);
                    }
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberBL.GetCachedMemberAccess() error (memberID: " + memberID.ToString() + "): " + ex.Message);
            }
            finally
            {
                adapter.CloseProxyInstance();
            }

            return cachedMemberAccess;
        }

        public IMemberDTO GetMemberDTO(string clientHostName, CacheReference cacheReference, Int32 memberID, MemberType type, bool forceLoad)
        {
            IMemberDTO memberDto=null;
            if (EnableMemberDTO)
            {
                memberDto = MemberDTOCache.Get(MemberDTO.GetCacheKey(memberID, type)) as IMemberDTO;

                if (null == memberDto)
                {
                    CachedMember cachedMember = GetCachedMember(clientHostName, cacheReference, memberID, forceLoad);
                    CachedMemberAccess cachedMemberAccess = GetCachedMemberAccess(clientHostName, cacheReference,
                                                                                  memberID, forceLoad);
                    memberDto = MemberDTOFactory.Instance.GetIMemberDTO(cachedMember, cachedMemberAccess, type);
                    MemberDTOCache.Insert(memberDto);
                }
            }
            return memberDto;
        }

        public IMemberDTO[] GetMemberDTOs(string clientHostName, CacheReference cacheReference, Int32[] memberIDs, MemberType type, bool forceLoad)
        {
            IMemberDTO[] members = null;
            if (EnableMemberDTO)
            {
                List<string> keys =
                    (from int memberId in memberIDs select MemberDTO.GetCacheKey(memberId, type)).ToList<string>();
                IDictionary<string, object> memberDtoDictionary = MemberDTOCache.Get(keys);

                Dictionary<int, CachedMember> cachedMembersDictionary = null;
                Dictionary<int, CachedMemberAccess> cachedMemberAccessesDictionary = null;

                members = new IMemberDTO[memberIDs.Length];
                for (Int32 memberNum = 0; memberNum < memberIDs.Length; memberNum++)
                {
                    IMemberDTO memberDto = null;
                    int memberID = memberIDs[memberNum];
                    string cacheKey = MemberDTO.GetCacheKey(memberID, type);

                    //attempt to get dto from cache
                    if (memberDtoDictionary.ContainsKey(cacheKey))
                    {
                        memberDto = memberDtoDictionary[cacheKey] as IMemberDTO;
                    }

                    //if not in cache, create dto and insert into cache
                    if (null == memberDto)
                    {
                        if (null == cachedMembersDictionary)
                            cachedMembersDictionary = GetCachedMembersDictionary(clientHostName, cacheReference,
                                                                                 memberIDs, forceLoad);
                        CachedMember cachedMember = cachedMembersDictionary[memberID];
                        if (null == cachedMemberAccessesDictionary)
                            cachedMemberAccessesDictionary = GetCachedMemberAccessesDictionary(clientHostName,
                                                                                               cacheReference, memberIDs,
                                                                                               forceLoad);
                        CachedMemberAccess cachedMemberAccess = cachedMemberAccessesDictionary[memberID];
                        memberDto = MemberDTOFactory.Instance.GetIMemberDTO(cachedMember, cachedMemberAccess, type);
                        MemberDTOCache.Insert(memberDto);
                    }

                    //populate dto in array
                    members[memberNum] = memberDto;
                }
            }
            return members;
        }
        #endregion

        private void getTextAttributes(Attributes attributes,
            CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                bool colLookupDone = false;
                Int32 ordinalStatusMask = Constants.NULL_INT;
                Int32 ordinalValue = Constants.NULL_INT;
                Int32 ordinalLanguageID = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalStatusMask = dataReader.GetOrdinal("StatusMask");
                        ordinalValue = dataReader.GetOrdinal("Value");
                        ordinalLanguageID = dataReader.GetOrdinal("LanguageID");
                        colLookupDone = true;
                    }

                    TextStatusType textStatus = TextStatusType.None;
                    if (!dataReader.IsDBNull(ordinalStatusMask))
                    {
                        textStatus = (TextStatusType)Enum.Parse(typeof(TextStatusType), dataReader.GetByte(ordinalStatusMask).ToString());
                    }

                    Int32 attributeGroupID = dataReader.GetInt32(dataReader.GetOrdinal("AttributeGroupID"));
                    AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);

                    if (attributeGroup != null)
                    {
                        string currentValue = dataReader.GetString(ordinalValue);
                        switch (attributeGroup.AttributeID)
                        {
                            case Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_EMAILADDRESS:
                                cachedMember.EmailAddress = currentValue;
                                break;

                            // PM-218 Always get based on attributeGroupID from GetUserName() method. This should
                            // have been the case before this project, but there maybe some unknowns.
                            //							case Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME:
                            //								cachedMember.Username = currentValue;
                            //								break;
                        }

                        // PM-218 Else case would be members who signed up after this proj is launched.
                        if (attributeGroupID == Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEGROUPID_USERNAME)
                        {
                            cachedMember.Username = currentValue;
                        }

                        cachedMember.SetAttributeText(attributeGroupID,
                            dataReader.GetInt32(ordinalLanguageID),
                            currentValue,
                            textStatus);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured populating text member attributes.", ex));
            }
        }


        private void getIntAttributes(CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                bool colLookupDone = false;
                Int32 ordinalAttributeGroupID = Constants.NULL_INT;
                Int32 ordinalValue = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalAttributeGroupID = dataReader.GetOrdinal("AttributeGroupID");
                        ordinalValue = dataReader.GetOrdinal("Value");
                        colLookupDone = true;
                    }

                    cachedMember.SetAttributeInt(dataReader.GetInt32(ordinalAttributeGroupID),
                        dataReader.GetInt32(ordinalValue));
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured populating integer member attributes.", ex));
            }
        }


        private void getDateAttributes(CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                bool colLookupDone = false;
                Int32 ordinalAttributeGroupID = Constants.NULL_INT;
                Int32 ordinalValue = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalAttributeGroupID = dataReader.GetOrdinal("AttributeGroupID");
                        ordinalValue = dataReader.GetOrdinal("Value");
                        colLookupDone = true;
                    }

                    cachedMember.SetAttributeDate(dataReader.GetInt32(ordinalAttributeGroupID),
                        dataReader.GetDateTime(ordinalValue));
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured populating date member attributes.", ex));
            }
        }


        private void getPhotos(CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                PhotoCollection photoCollection = new PhotoCollection();
                cachedMember.Photos = photoCollection;

                bool colLookupDone = false;
                #region Ordinal holders
                Int32 ordinalMemberPhotoID = Constants.NULL_INT;
                Int32 ordinalGroupID = Constants.NULL_INT;
                Int32 ordinalAlbumID = Constants.NULL_INT;
                Int32 ordinalThumbFileID = Constants.NULL_INT;
                Int32 ordinalThumbFileWebPath = Constants.NULL_INT;
                Int32 ordinalFileID = Constants.NULL_INT;
                Int32 ordinalFileWebPath = Constants.NULL_INT;
                Int32 ordinalListOrder = Constants.NULL_INT;
                Int32 ordinalAdminMemberID = Constants.NULL_INT;
                Int32 ordinalApprovedFlag = Constants.NULL_INT;
                Int32 ordinalPrivateFlag = Constants.NULL_INT;
                Int32 ordinalCaption = Constants.NULL_INT;
                Int32 ordinalcaptionApproved = Constants.NULL_INT;
                Int32 ordinalFileCloudPath = Constants.NULL_INT;
                Int32 ordinalThumbFileCloudPath = Constants.NULL_INT;
                Int32 ordinalIsApprovedForMain = Constants.NULL_INT;
                Int32 ordinalIsMain = Constants.NULL_INT;
                Int32 ordinalFileHeight = Constants.NULL_INT;
                Int32 ordinalFileWidth = Constants.NULL_INT;
                Int32 ordinalFileSize = Constants.NULL_INT;
                Int32 ordinalThumbFileHeight = Constants.NULL_INT;
                Int32 ordinalThumbFileWidth = Constants.NULL_INT;
                Int32 ordinalThumbFileSize = Constants.NULL_INT;
                
                #endregion
                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalMemberPhotoID = dataReader.GetOrdinal("MemberPhotoID");
                        ordinalGroupID = dataReader.GetOrdinal("GroupID");
                        ordinalAlbumID = dataReader.GetOrdinal("AlbumID");
                        ordinalGroupID = dataReader.GetOrdinal("GroupID");
                        ordinalThumbFileID = dataReader.GetOrdinal("ThumbFileID");
                        ordinalThumbFileWebPath = dataReader.GetOrdinal("ThumbFileWebPath");
                        ordinalFileID = dataReader.GetOrdinal("FileID");
                        ordinalFileWebPath = dataReader.GetOrdinal("FileWebPath");
                        ordinalListOrder = dataReader.GetOrdinal("ListOrder");
                        ordinalAdminMemberID = dataReader.GetOrdinal("AdminMemberID");
                        ordinalApprovedFlag = dataReader.GetOrdinal("ApprovedFlag");
                        ordinalPrivateFlag = dataReader.GetOrdinal("PrivateFlag");
                        ordinalcaptionApproved = dataReader.GetOrdinal("CaptionApprovedFlag");
                        ordinalCaption = dataReader.GetOrdinal("Caption");
                        ordinalFileCloudPath = dataReader.GetOrdinal("FileCloudPath");
                        ordinalThumbFileCloudPath = dataReader.GetOrdinal("ThumbFileCloudPath");
                        ordinalIsApprovedForMain = dataReader.GetOrdinal("ApprovedForMain");
                        ordinalIsMain = dataReader.GetOrdinal("IsMain");

                        if (cachedMember.Version >= 4)
                        {
                            ordinalFileHeight = dataReader.GetOrdinal("FileHeight");
                            ordinalFileWidth = dataReader.GetOrdinal("FileWidth");
                            ordinalFileSize = dataReader.GetOrdinal("FileSize");
                            ordinalThumbFileHeight = dataReader.GetOrdinal("ThumbFileHeight");
                            ordinalThumbFileWidth = dataReader.GetOrdinal("ThumbFileWidth");
                            ordinalThumbFileSize = dataReader.GetOrdinal("ThumbFileSize");
                        }

                        colLookupDone = true;
                    }

                    //we are ignoring anything from an album right now, this was done in the proc (yuck)
                    if (dataReader.IsDBNull(ordinalAlbumID))
                    {
                        var memberPhotoId = dataReader.GetInt32(ordinalMemberPhotoID);
                        Int32 communityID = dataReader.GetInt32(ordinalGroupID);
                        Int32 thumbFileID = Constants.NULL_INT;
                        string webPath = Constants.NULL_STRING;
                        string webPathThumb = Constants.NULL_STRING;
                        Int32 albumID = Constants.NULL_INT;
                        Int32 adminMemberID = Constants.NULL_INT;
                        string caption = Constants.NULL_STRING;
                        bool isCaptionApproved = false;
                        string fileCloudPath = string.Empty;
                        string thumbFileCloudPath = string.Empty;
                        bool isApprovedForMain = false;
                        bool isMain = false;
                        int fileHeight = Constants.NULL_INT;
                        int fileWidth = Constants.NULL_INT;
                        int fileSize = Constants.NULL_INT;
                        int thumbFileHeight = Constants.NULL_INT;
                        int thumbFileWidth = Constants.NULL_INT;
                        int thumbFileSize = Constants.NULL_INT;

                        if (!dataReader.IsDBNull(ordinalThumbFileID))
                        {
                            thumbFileID = dataReader.GetInt32(ordinalThumbFileID);
                        }

                        if (!dataReader.IsDBNull(ordinalFileWebPath))
                        {
                            webPath = dataReader.GetString(ordinalFileWebPath);
                        }

                        if (!dataReader.IsDBNull(ordinalThumbFileWebPath))
                        {
                            webPathThumb = dataReader.GetString(ordinalThumbFileWebPath);
                        }

                        byte listOrder = dataReader.GetByte(ordinalListOrder);

                        if (!dataReader.IsDBNull(ordinalAlbumID))
                        {
                            albumID = dataReader.GetInt32(ordinalAlbumID);
                        }

                        if (!dataReader.IsDBNull(ordinalAdminMemberID))
                        {
                            adminMemberID = dataReader.GetInt32(ordinalAdminMemberID);
                        }

                        bool isPrivate = false;

                        if (!dataReader.IsDBNull(ordinalPrivateFlag))
                        {
                            isPrivate = dataReader.GetBoolean(ordinalPrivateFlag);
                        }

                        if (!dataReader.IsDBNull(ordinalcaptionApproved))
                        {
                            isCaptionApproved = dataReader.GetBoolean(ordinalcaptionApproved);
                        }

                        if (!dataReader.IsDBNull(ordinalCaption))
                        {
                            caption = dataReader.GetString(ordinalCaption);
                        }
                        else
                        {
                            caption = "";
                        }

                        if (!dataReader.IsDBNull(ordinalFileCloudPath))
                        {
                            fileCloudPath = dataReader.GetString(ordinalFileCloudPath).ToLower();
                        }
                        if (!dataReader.IsDBNull(ordinalThumbFileCloudPath))
                        {
                            thumbFileCloudPath = dataReader.GetString(ordinalThumbFileCloudPath).ToLower();
                        }

                        isApprovedForMain = dataReader.GetBoolean(ordinalIsApprovedForMain);
                        isMain = dataReader.GetBoolean(ordinalIsMain);

                        if (cachedMember.Version >= 4)
                        {
                            if (!dataReader.IsDBNull(ordinalFileHeight))
                            {
                                fileHeight = dataReader.GetInt32(ordinalFileHeight);
                            }
                            if (!dataReader.IsDBNull(ordinalFileWidth))
                            {
                                fileWidth = dataReader.GetInt32(ordinalFileWidth);
                            }
                            if (!dataReader.IsDBNull(ordinalFileSize))
                            {
                                fileSize = dataReader.GetInt32(ordinalFileSize);
                            }
                            if (!dataReader.IsDBNull(ordinalThumbFileHeight))
                            {
                                thumbFileHeight = dataReader.GetInt32(ordinalThumbFileHeight);
                            }
                            if (!dataReader.IsDBNull(ordinalThumbFileWidth))
                            {
                                thumbFileWidth = dataReader.GetInt32(ordinalThumbFileWidth);
                            }
                            if (!dataReader.IsDBNull(ordinalThumbFileSize))
                            {
                                thumbFileSize = dataReader.GetInt32(ordinalThumbFileSize);
                            }
                        }


                        Photo photo = null;
                        if (cachedMember.Version >= 4)
                        {
                            photo = new Photo(dataReader.GetInt32(ordinalMemberPhotoID),
                                dataReader.GetInt32(ordinalFileID),
                                webPath,
                                thumbFileID,
                                webPathThumb,
                                listOrder,
                                dataReader.GetBoolean(ordinalApprovedFlag),
                                isPrivate,
                                albumID,
                                adminMemberID, caption, isCaptionApproved,
                                fileCloudPath, thumbFileCloudPath,
                                isApprovedForMain, isMain,
                                fileHeight, fileWidth, fileSize,
                                thumbFileHeight, thumbFileWidth, thumbFileSize
                                );
                        }
                        else
                        {
                            photo = new Photo(dataReader.GetInt32(ordinalMemberPhotoID),
                                dataReader.GetInt32(ordinalFileID),
                                webPath,
                                thumbFileID,
                                webPathThumb,
                                listOrder,
                                dataReader.GetBoolean(ordinalApprovedFlag),
                                isPrivate,
                                albumID,
                                adminMemberID, caption, isCaptionApproved,
                                fileCloudPath, thumbFileCloudPath,
                                isApprovedForMain, isMain);
                        }
                        
                        photoCollection.GetCommunity(Convert.ToInt32(dataReader.GetInt32(ordinalGroupID)))[(byte)(listOrder - 1)] = photo;
                    }
                }

                cachedMember.Photos = photoCollection;
                
                if (UseMembaseForBL)
                {
                    ICache.Insert(cachedMember);
                    RemoveMemberDTOFromCache(cachedMember.MemberID);
                }

                //this will only excecute if flag is turned on
                RemoveMemberAttributeSetsFromCache(cachedMember.MemberID);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when populating the member's photos (MemberID:" + cachedMember.MemberID.ToString() + ").", ex));
            }
        }

        private void getPhotoFiles(CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                int ordinalMemberPhotoID = Constants.NULL_INT;
                int ordinalFileTypeID = Constants.NULL_INT;
                int ordinalFileHeight = Constants.NULL_INT;
                int ordinalFileWidth = Constants.NULL_INT;
                int ordinalFileSize = Constants.NULL_INT;
                int ordinalFileID = Constants.NULL_INT;
                int ordinalFileWebPath = Constants.NULL_INT;
                int ordinalFileCloudPath = Constants.NULL_INT;
                var colLookupDone = false;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalMemberPhotoID = dataReader.GetOrdinal("MemberPhotoID");
                        ordinalFileTypeID = dataReader.GetOrdinal("FileTypeID");
                        ordinalFileHeight = dataReader.GetOrdinal("FileHeight");
                        ordinalFileWidth = dataReader.GetOrdinal("FileWidth");
                        ordinalFileSize = dataReader.GetOrdinal("FileSize");
                        ordinalFileID = dataReader.GetOrdinal("FileID");
                        ordinalFileWebPath = dataReader.GetOrdinal("FileWebPath");
                        ordinalFileCloudPath = dataReader.GetOrdinal("FileCloudPath");
                        colLookupDone = true;
                    }
                    
                    var memberPhotoId = dataReader.GetInt32(ordinalMemberPhotoID);
                    var fileTypeID = (PhotoFileType)dataReader.GetInt32(ordinalFileTypeID);
                    var fileHeight = dataReader.GetInt32(ordinalFileHeight);
                    var fileWidth = dataReader.GetInt32(ordinalFileWidth);
                    var fileSize = dataReader.GetInt32(ordinalFileSize);
                    var fileID = Constants.NULL_INT;
                    var fileWebPath = string.Empty;
                    var fileCloudPath = string.Empty;

                    if(!dataReader.IsDBNull(ordinalFileID))
                    {
                        fileID = dataReader.GetInt32(ordinalFileID);
                    }
                    if (!dataReader.IsDBNull(ordinalFileWebPath))
                    {
                        fileWebPath = dataReader.GetString(ordinalFileWebPath);
                    }
                    if (!dataReader.IsDBNull(ordinalFileCloudPath))
                    {
                        fileCloudPath = dataReader.GetString(ordinalFileCloudPath);
                    }

                    var photoFile = new ValueObjects.Photos.PhotoFile(fileTypeID,
                                                                      fileHeight,
                                                                      fileWidth,
                                                                      fileSize,
                                                                      fileID,
                                                                      fileWebPath,
                                                                      fileCloudPath);

                    Photo matchingPhoto = null;

                    foreach(var communityID in cachedMember.Photos.CommunityIds)
                    {
                        var photoCommunity = cachedMember.Photos.GetCommunity((int)communityID);

                        foreach(Photo photo in photoCommunity)
                        {
                            if(photo.MemberPhotoID == memberPhotoId)
                            {
                                matchingPhoto = photo;
                                break;
                            }
                        }

                        if (matchingPhoto != null) break;
                    }

                    if(matchingPhoto != null)
                    {
                        matchingPhoto.Files.Add(photoFile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when populating the member's photos  files (MemberID:" + cachedMember.MemberID.ToString() +").", ex));
            }
        }

        private string stripHostFromPath(string path)
        {
            if (path.IndexOf("/") != 0)
            {
                ///return new Uri(path).AbsolutePath;
                return path;
            }

            return "http://www.collegeluv.com" + path;
        }

        public void InsertCachedMemberBytes(CachedMemberBytes cachedMemberBytes)
        {
            InsertCachedMemberBytes(cachedMemberBytes, !UseMembaseForBL);
        }


        public void InsertCachedMemberBytes(CachedMemberBytes cachedMemberBytes,
            bool replicate)
        {
            cachedMemberBytes.CacheTTLSeconds = _ttl;
            cachedMemberBytes.CacheMode = Matchnet.CacheItemMode.Sliding;
            ICache.Insert(cachedMemberBytes, _expireMember);
            RemoveMemberDTOFromCache(cachedMemberBytes.MemberID);

            //this will only excecute if flag is turned on
            RemoveMemberAttributeSetsFromCache(cachedMemberBytes.MemberID);

            if (replicate)
            {
                ReplicationRequested(cachedMemberBytes);
            }
            MemberAdded();
        }

        public void InsertCachedMemberAccess(CachedMemberAccess cachedMemberAccess,
            bool replicate)
        {
            if (cachedMemberAccess != null)
            {
                cachedMemberAccess.CacheTTLSeconds = _ttl;
                cachedMemberAccess.CacheMode = Matchnet.CacheItemMode.Sliding;
                ICache.Insert(cachedMemberAccess, _expireMemberAccess);
                RemoveMemberDTOFromCache(cachedMemberAccess.MemberID);
                //string privileges = (cachedMemberAccess.AccessPrivilegeList != null) ? cachedMemberAccess.AccessPrivilegeList.Count.ToString() : "null";
                //System.Diagnostics.EventLog.WriteEntry(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberBL.InsertCachedMemberAccess: member: " + cachedMemberAccess.MemberID.ToString() + ", privileges: " + privileges + ". Do Replicate: " + replicate.ToString());

                //this will only excecute if flag is turned on
                RemoveMemberAttributeSetsFromCache(cachedMemberAccess.MemberID);

                if (replicate)
                {
                    cachedMemberAccess.ReplicateForExpire = false;
                    ReplicationRequested(cachedMemberAccess);
                }
            }
        }

        public bool AdjustUnifiedAccessCountPrivilege(int memberID, int adminID, string adminDomainUserName, int[] privilegeTypeID, int brandID, int siteID, int communityID, int count)
        {
            bool response = true;

            AccessServiceAdapter adapter = new AccessServiceAdapter();

            try
            {
                //adjust access
                AccessResponse accessResponse = adapter.GetProxyInstanceForBedrock().AdjustCountPrivilege(memberID, privilegeTypeID, count, adminID, adminDomainUserName, Constants.NULL_INT, siteID, 1);
                if (accessResponse.InternalResponse.responsecode == "0")
                {
                    response = true;
                }
                else
                {
                    response = false;
                }

                //refresh cache (in case of a delay in callback from access)
                //getCachedMemberAccess("", null, memberID, true);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberBL.AdjustUnifiedAccessCountPrivilege() error (memberID: " + memberID.ToString() + "): " + ex.Message);

            }
            finally
            {
                adapter.CloseProxyInstance();
            }


            return response;

        }

        //This should be deprecated after the next release (4/20/12)
        public MemberSaveResult SaveNewMember(string clientHostName, MemberUpdate memberUpdate, int version, int groupID)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            if (memberUpdate.AttributesText.Count > 0)
            {
                IDictionaryEnumerator de = memberUpdate.AttributesText.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 languageID = (Int32)de.Key;
                    Hashtable attributesText = de.Value as Hashtable;
                    IDictionaryEnumerator deA = attributesText.GetEnumerator();
                    while (deA.MoveNext())
                    {
                        Int32 attributeGroupID = (Int32)deA.Key;
                        TextValue textValue = (TextValue)deA.Value;
                        string val = textValue.Text;

                        AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);

                        if (attributeGroup.EncryptFlag)
                        {
                            val = Crypto.Encrypt(SettingsService.GetSettingFromSingleton("KEY"), val);
                        }
                        Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                        string logLine = "Saving Attribute for MemberID {0}. AttributeGroupID: {1}, AttributeName: {2}, ValueText: {3}, LanguageID: {4}" + System.Environment.NewLine;
                        sb.Append(string.Format(logLine, memberUpdate.MemberID.ToString(), attribute.Name, attributeGroupID.ToString(), val, languageID.ToString()));
                    }
                }
            }

            Spark.Logging.RollingFileLogger.Instance.LogInfoMessage(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL", sb.ToString(), null);
            //this is for temporary use, to add logging code for saving members from registration. 
            return SaveMember(clientHostName, memberUpdate, version, groupID);
        }
        
        public MemberSaveResult SaveMember(string clientHostName,
                          MemberUpdate memberUpdate, int version, Brand brand, bool updateMemberGroup)
        {
            MemberSaveResult saveResult = SaveMember(clientHostName, memberUpdate, version,
                                                     brand.Site.Community.CommunityID);

            if(updateMemberGroup && saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                SetMemberGroup(memberUpdate.MemberID, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }

            return saveResult;
        }

        public bool SavePassword(int memberId, int communityId, string passwordHash)
        {
            LogMessage("SavePassword", "Entering SavePassword", memberId);
            
            try
            {
                var basicLogonInfo = GetMemberBasicLogonInfo(memberId, communityId);
                if (basicLogonInfo == null)
                {
                    LogMessage("SavePassword", "Invalid memberId", memberId);
                    return false;
                }

                string username = string.Empty;

                var registerStatus = saveLogon(memberId,
                                               string.Empty,
                                               ref username, passwordHash,
                                               communityId, basicLogonInfo.EmailAddress, -1);

                if (registerStatus != RegisterStatusType.Success)
                {
                    LogMessage("SavePassword", "Error saving password", memberId);
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogException("SavePassword", ex, memberId);
                throw ex;
            }

            LogMessage("SavePassword", "Password saved", memberId);
            return true;
        }

        public MemberSaveResult SaveMember(string clientHostName, MemberUpdate memberUpdate, int version, int groupID)
        {
            if (memberUpdate.MemberID < 1)
            {
                throw new ArgumentException("Must be greater than zero (" + memberUpdate.MemberID.ToString() + ").", "memberID");
            }

            LogMessage("SaveMember", string.Format("Entering SaveMember for Member {0} Group {1}", memberUpdate.MemberID, groupID), memberUpdate.MemberID);
            
            Int32 attributeNum = 1;
            Int32 attributeNumProc = 1;
            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            MemberSaveResult memberSaveResult = null;
            CachedMemberBytes cachedMemberBytes = getCachedMemberBytes(clientHostName,
                  null,
                  memberUpdate.MemberID,
                  false, version);
            CachedMember cachedMember = new CachedMember(cachedMemberBytes.MemberBytes, version);
            //first versioning has no change in member attributes so we do no distinction
            Int32[] unapprovedLanguages = cachedMember.GetUnapprovedAttributeLanguages(attributes);

            if ((memberUpdate.EmailAddress != Constants.NULL_STRING ||
                memberUpdate.Username != Constants.NULL_STRING ||
                memberUpdate.PasswordHash != Constants.NULL_STRING
                // PM-218 If groupID is valid then process as usual. No groupID means it's not updating username, password, or email address.
                ) && groupID != Constants.NULL_INT)
            {
                string username = memberUpdate.Username;

                var currentEmailAddress = GetCurrentEmailAddress(cachedMember.MemberID);
                if (username != Constants.NULL_STRING)
                {
                    if ((currentEmailAddress != null && currentEmailAddress.ToLower().IndexOf(username.ToLower()) > -1) ||
                        (memberUpdate.EmailAddress != Constants.NULL_STRING && memberUpdate.EmailAddress.ToLower().IndexOf(username.ToLower()) > -1))
                    {
                        username = memberUpdate.MemberID.ToString();
                    }

                    if (username.Length > 50)
                    {
                        throw new Exception("Username must not be longer than 50 characters.");
                    }
                }

                RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL.SaveMember()",
                   string.Format("UPDATE Email, Pass or username. Email: {0} Username: {1} passHash: {2} ", memberUpdate.EmailAddress, memberUpdate.Username, memberUpdate.PasswordHash)
                   , memberUpdate.MemberID);

                RegisterStatusType registerStatus;

                registerStatus = saveLogon(memberUpdate.MemberID,
                    memberUpdate.EmailAddress,
                    ref username, memberUpdate.PasswordHash,
                    groupID, currentEmailAddress, -1);

                LogMessage("SaveMember", string.Format("Logon saved RegisterStatus {0}", registerStatus), memberUpdate.MemberID);

                switch (registerStatus)
                {
                    case RegisterStatusType.AlreadyRegistered:
                        memberSaveResult = new MemberSaveResult(MemberSaveStatusType.EmailAddressExists, Constants.NULL_STRING);
                        break;

                    default:
                        memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, username);

                        foreach (Matchnet.Language language in Enum.GetValues(typeof(Matchnet.Language)))
                        {
                            Hashtable textHashtable = memberUpdate.AttributesText[(int)language] as Hashtable;
                            if (textHashtable != null)
                        {
                            int attributeGroupID = Constants.NULL_INT;

                                attributeGroupID = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(groupID,
                                    Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME).ID;

                                TextValue valUsername = textHashtable[attributeGroupID] as TextValue;

                            if (valUsername != null)
                            {
                                valUsername.Text = username;
                            }
                        }
                        }

                        break;
                }
            }
            else
            {
                memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, memberUpdate.Username);
            }


            Command commandMember = null;
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = null;

            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success &&
                (memberUpdate.AttributesText.Count > 0 || memberUpdate.AttributesInt.Count > 0 || memberUpdate.AttributesDate.Count > 0))
            {
                
                
                IDictionaryEnumerator de = memberUpdate.AttributesText.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 languageID = (Int32)de.Key;
                    Hashtable attributesText = de.Value as Hashtable;

                    LogMessage("SaveMember", string.Format("About to save {0} text attributes for language {1}", attributesText.Count, languageID), memberUpdate.MemberID);
                    IDictionaryEnumerator deA = attributesText.GetEnumerator();
                    while (deA.MoveNext())
                    {
                        Int32 attributeGroupID = (Int32)deA.Key;
                        TextValue textValue = (TextValue)deA.Value;
                        string val = textValue.Text;
                        attributeNumProc = getAttributeNumProc(attributeNum);
                        AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);
                        
                        createCommand(attributeNumProc, ref commandMember, memberUpdate.MemberID);

                        if (attributeGroup.EncryptFlag)
                        {
                            val = Crypto.Encrypt(SettingsService.GetSettingFromSingleton("KEY"), val);
                        }

                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "DataType", SqlDbType.VarChar, ParameterDirection.Input, "TEXT");
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                        if (val != attributeGroup.DefaultValue)
                        {
                            commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ValueText", SqlDbType.NVarChar, ParameterDirection.Input, val);
                        }
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "StatusMask", SqlDbType.TinyInt, ParameterDirection.Input, textValue.TextStatus);
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ImmutableFlag", SqlDbType.Bit, ParameterDirection.Input, ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) ? 1 : 0);

                        attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                        LogMessage("SaveMember", string.Format("Saved Text AttributeGroupID: {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, val), memberUpdate.MemberID);
                        
                        executeCommand(attributeNumProc, commandMember);
                        attributeNum++;
                    }
                }


                #region //PM-272 7+1(8 total) Attributes - Change scope from global to community. Need to write to both global and community
                // until the scope switch happens, then this can be safely removed.
                // 'Childrencount', 'DesiredMaritalStatus', 'HaveChildren', 'MaritalStatus','RelationshipStatus','Religion','ReligionActivityLevel','BodyType'
                // AttributeGroupIDs 500, 427, 423, 451, 514, 412, 495, 407 - No need to look up every friggin time plus this is temp. Same in dev, prod
                // This block has to be removed after the deployment. Keeping it separated in its own block. Easier to clean later.
                // If any one of the 8 attributes are included then include a write for community level as well.
                if (AttributeMetadataSA.Instance.GetAttributes().GetAttribute("Religion").Scope == ScopeType.Personals)
                {
                    try
                    {
                        List<Int32> globalAttributeGroupIDs = new List<Int32>(new Int32[] { 500, 427, 423, 451, 514, 412, 495, 407 });

                        int brandID = Constants.NULL_INT;
                        cachedMember.GetLastLogonDate(out brandID, AttributeMetadataSA.Instance.GetAttributes(), BrandConfigSA.Instance.GetBrands());

                        if (brandID != Constants.NULL_INT)
                        {
                            int communityID = BrandConfigSA.Instance.GetBrandByID(brandID).Site.Community.CommunityID;
                            Hashtable hashTableAttributeInt = (Hashtable)memberUpdate.AttributesInt.Clone();
                            IDictionaryEnumerator attributesIntEnumerator = hashTableAttributeInt.GetEnumerator();
                            while (attributesIntEnumerator.MoveNext())
                            {
                                Int32 attributeGroupID = (Int32)attributesIntEnumerator.Key;
                                Int32 val = (Int32)attributesIntEnumerator.Value;

                                // Write to both scopes if there's a write for a global value.
                                if (globalAttributeGroupIDs.Contains(attributeGroupID))
                                {
                                    // Get the Attribute
                                    int attributeID = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(attributeGroupID).AttributeID;

                                    // Get the AttributeGroupID for the commnity
                                    int communityAttributeGroupID = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(communityID, attributeID).ID;

                                    // Add it to memberUpdate.AttributesInt to add an extra record in community scope.
                                    memberUpdate.AttributesInt.Add(communityAttributeGroupID, val);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw (new BLException("BL error occured saving community level attributes for PM272.", ex));
                    }
                }
                #endregion


                LogMessage("SaveMember", string.Format("About to save {0} Int attributes", memberUpdate.AttributesInt.Count), memberUpdate.MemberID);
                de = memberUpdate.AttributesInt.GetEnumerator();

                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    Int32 val = (Int32)de.Value;
                    attributeNumProc = getAttributeNumProc(attributeNum);
                    AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);
                    attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                    createCommand(attributeNumProc, ref commandMember, memberUpdate.MemberID);
                    Int32 defaultValue = Constants.NULL_INT;

                    if (attributeGroup.DefaultValue != Constants.NULL_STRING)
                    {
                        defaultValue = Convert.ToInt32(attributeGroup.DefaultValue);
                    }

                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "DataType", SqlDbType.VarChar, ParameterDirection.Input, attribute.DataType.ToString());
                    if (val != defaultValue)
                    {
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ValueInt", SqlDbType.Int, ParameterDirection.Input, val);
                    }
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ImmutableFlag", SqlDbType.Bit, ParameterDirection.Input, ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) ? 1 : 0);

                    LogMessage("SaveMember", string.Format("Saved Int AttributeGroupID: {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, val), memberUpdate.MemberID);
                    
                    executeCommand(attributeNumProc, commandMember);
                    attributeNum++;
                }

                LogMessage("SaveMember", string.Format("About to save {0} Date attributes", memberUpdate.AttributesDate.Count), memberUpdate.MemberID);
                de = memberUpdate.AttributesDate.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    DateTime val = (DateTime)de.Value;
                    attributeNumProc = getAttributeNumProc(attributeNum);
                    AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);
                    attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                    createCommand(attributeNumProc, ref commandMember, memberUpdate.MemberID);
                    DateTime defaultValue = DateTime.MinValue;

                    if (attributeGroup.DefaultValue != Constants.NULL_STRING)
                    {
                        defaultValue = Convert.ToDateTime(attributeGroup.DefaultValue);
                    }

                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "DataType", SqlDbType.VarChar, ParameterDirection.Input, attribute.DataType.ToString());
                    if (val != defaultValue && val != DateTime.MinValue)
                    {
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ValueDate", SqlDbType.DateTime, ParameterDirection.Input, val);
                    }
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ImmutableFlag", SqlDbType.Bit, ParameterDirection.Input, ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) ? 1 : 0);

                    LogMessage("SaveMember", string.Format("Saving Date AttributeGroupID: {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, val), memberUpdate.MemberID);

                    executeCommand(attributeNumProc, commandMember);
                    attributeNum++;
                }

                if (attributeNumProc != MAX_ATTRIBUTES_SAVE)
                {
                    Client.Instance.ExecuteAsyncWrite(commandMember);
                }

                //update cachedMember
                de = memberUpdate.AttributesText.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 languageID = (Int32)de.Key;
                    Hashtable attributesText = de.Value as Hashtable;

                    IDictionaryEnumerator deA = attributesText.GetEnumerator();

                    LogMessage("SaveMember", string.Format("About to update cached member with {0} text attributes for language {1}", attributesText.Count, languageID), memberUpdate.MemberID);

                    while (deA.MoveNext())
                    {
                        TextStatusType textStatus;
                        Int32 attributeGroupID = (Int32)deA.Key;
                        AttributeGroup ag = attributes.GetAttributeGroup(attributeGroupID);
                        if ((ag.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) != Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable ||
                            cachedMember.GetAttributeText(attributeGroupID, languageID, null, out textStatus) == null)
                        {
                            TextValue textValue = (TextValue)deA.Value;
                            attribute = attributes.GetAttribute(ag.AttributeID);
                            LogMessage("SaveMember", string.Format("Updating cached member with Text attribute {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, textValue.Text), memberUpdate.MemberID);
                            
                            cachedMember.SetAttributeText(attributeGroupID, languageID, textValue.Text, textValue.TextStatus);

                            /*If a member has accounts on multiple sites within the same community, 
                                username change in one site\language should reflect on all sites\languages within the community.
                             Update cache on all sites within the community.
                            */
                            if (attribute.Name.ToLower() == "username" && textValue.TextStatus == TextStatusType.Human)
                            {
                                //get member's username on other sites/languages within the same community
                                ArrayList attributeGroupIDs =
                                    attributes.GetAttributeGroupIDs(attributes.GetAttribute("BrandInsertDate").ID);
                                Brands brands = BrandConfigSA.Instance.GetBrands();
                                var communityId = attributes.GetAttributeGroup(attributeGroupID).GroupID;

                                for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
                                {
                                    Int32 agID = (Int32) attributeGroupIDs[num];
                                    if (cachedMember.GetAttributeDate(agID, DateTime.MinValue) != DateTime.MinValue)
                                    {
                                        Site site = brands.GetBrand(attributes.GetAttributeGroup(agID).GroupID).Site;
                                        //Update username attribute for all sites in the community 
                                        if (site.Community.CommunityID == communityId && languageID != site.LanguageID)
                                        {
                                            cachedMember.SetAttributeText(attributeGroupID, site.LanguageID,
                                                                          textValue.Text, textValue.TextStatus);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                LogMessage("SaveMember", string.Format("About to update cached member with {0} Int attributes", memberUpdate.AttributesInt.Count), memberUpdate.MemberID);
                de = memberUpdate.AttributesInt.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    AttributeGroup ag = attributes.GetAttributeGroup(attributeGroupID);
                    if ((ag.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) != Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable ||
                        cachedMember.GetAttributeInt(attributeGroupID, Constants.NULL_INT) == Constants.NULL_INT)
                    {
                        attribute = attributes.GetAttribute(ag.AttributeID);
                        LogMessage("SaveMember", string.Format("Updating cached member with Int attribute {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, de.Value), memberUpdate.MemberID);
                        
                        cachedMember.SetAttributeInt(attributeGroupID, (Int32)de.Value);
                    }
                }

                bool isSubExpDateChanged = false;
                int subExpDateSiteId = Constants.NULL_INT;
                DateTime newSubExpDate = DateTime.MinValue;

                LogMessage("SaveMember", string.Format("About to update cached member with {0} Date attributes", memberUpdate.AttributesDate.Count), memberUpdate.MemberID);
                de = memberUpdate.AttributesDate.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    AttributeGroup ag = attributes.GetAttributeGroup(attributeGroupID);

                    // we need to know if the SubscriptionExpirationDate attribute has changed
                    if (!isSubExpDateChanged && ag.AttributeID == SUB_EXP_DATE_ATTRIBUTE_ID)
                    {
                        subExpDateSiteId = ag.GroupID;
                        newSubExpDate = (DateTime)de.Value;
                        isSubExpDateChanged = true;
                    }

                    if ((ag.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) != Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable ||
                        cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) == DateTime.MinValue)
                    {
                        attribute = attributes.GetAttribute(ag.AttributeID);
                        LogMessage("SaveMember", string.Format("Updating cached member with Date attribute {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, de.Value), memberUpdate.MemberID);
                        
                        cachedMember.SetAttributeDate((Int32)de.Key, (DateTime)de.Value);
                    }
                }

                if (memberUpdate.EmailAddress != Constants.NULL_STRING)
                {
                    cachedMember.EmailAddress = memberUpdate.EmailAddress;
                }

                if (memberUpdate.Username != Constants.NULL_STRING)
                {
                    cachedMember.Username = memberSaveResult.Username;
                }

                //update notifier
                MemberUpdateNotifier memberUpdateNotifier = new MemberUpdateNotifier(ref cachedMember, memberUpdate, unapprovedLanguages);
                memberUpdateNotifier.SendNotifications();
                LogMessage("SaveMember", "Member Update Notifier call done", memberUpdate.MemberID);

                cachedMemberBytes.MemberBytes = cachedMember.ToByteArray();

                //InsertCachedMember(cachedMember);
                ArrayList comm = memberUpdateNotifier.CommunityIDList;

                InsertCachedMemberBytes(cachedMemberBytes, !UseMembaseForBL);
                LogMessage("SaveMember", "Inserted member into cache", memberUpdate.MemberID);
                
                bool synchronize = null != clientHostName;
                if (synchronize)
                {
                    SynchronizationRequested(cachedMemberBytes.GetCacheKey(), cachedMemberBytes.ReferenceTracker.Purge(clientHostName));

                    LogMessage("SaveMember", "Synchronization Requested", memberUpdate.MemberID);
                }

                MemberSaved();
                updatePhotoSearch(memberUpdate, cachedMember, comm);

                // call messmo related function
                if (isSubExpDateChanged)
                {
                    SendMessmoUserTransactionStatus(cachedMember.MemberID, subExpDateSiteId, newSubExpDate);
                }

            }

            return memberSaveResult;
        }

        public string GetCurrentEmailAddress(int memberId)
        {
            string currentEmailAddress = Constants.NULL_STRING;

            Command command = new Command("mnLogon", "up_LogonMemberCommunity_GetCurrentEmail", 0);
            command.AddParameter("@MemberId", SqlDbType.Int, ParameterDirection.Input, memberId);
            command.AddParameter("@Value", SqlDbType.NVarChar, 255, ParameterDirection.Output);
            SqlParameterCollection results = Client.Instance.ExecuteNonQuery(command);

            if (results["@Value"].Value != DBNull.Value)
            {
                currentEmailAddress = results["@Value"].Value.ToString();
            }
            return currentEmailAddress;
        }

        //PM-272 Remove after deployed.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="attributeGroupID">This ID could be either global or community. This method will only act on global.</param>
        /// <returns>If no action is done NULL_INT is returned.</returns>
        private int GetAttributeGroupID(int attributeGroupID)
        {

            {
                return Constants.NULL_INT;
            }
        }


        private void updatePhotoSearch(MemberUpdate update, CachedMember cachedMember, ArrayList communityids)
        {
            try
            {
                ArrayList updatablecommunities = new ArrayList();

                for (int i = 0; i < communityids.Count; i++)
                {
                    int communityid = Conversion.CInt(communityids[i]);
                    bool updateStore = Conversion.CBool(SettingsService.GetSettingFromSingleton("ENABLE_PHOTOSTORE_UPDATE", communityid));
                    if (updateStore)
                    {
                        updatablecommunities.Add(communityid);
                    }
                }

                Matchnet.PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.UpdateMember(update, cachedMember, updatablecommunities);

            }
            catch (Exception ex)
            {
                var memberID = cachedMember == null ? 0 : cachedMember.MemberID;
                LogException("updatePhotoSearch", ex, memberID);
            }

        }

        public void UpdateSearch(int memberID, int communityID)
        {
            CachedMember cachedMember = GetCachedMember(null, null, memberID, false);
            Attributes attrs = AttributeMetadataSA.Instance.GetAttributes();

            if (MemberHelper.GetAttributeInt(cachedMember, communityID, CachedMember.ATTRIBUTEID_FTAAPPROVED, attrs) != 1)
                return;

        }

        private Int32 getAttributeNumProc(Int32 attributeNum)
        {
            Int32 attributeNumProc = (attributeNum % MAX_ATTRIBUTES_SAVE);
            if (attributeNumProc == 0)
            {
                attributeNumProc = MAX_ATTRIBUTES_SAVE;
            }

            return attributeNumProc;
        }


        private void createCommand(Int32 attributeNumProc,
            ref Command commandMember,
            Int32 memberID)
        {
            if (attributeNumProc == 1)
            {
                commandMember = new Command("mnMember", "dbo.up_MemberAttribute_Save_Multiple", memberID);
                commandMember.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                commandMember.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            }
        }


        private void executeCommand(Int32 attributeNumProc,
            Command commandMember)
        {
            if (attributeNumProc == MAX_ATTRIBUTES_SAVE)
            {
                Client.Instance.ExecuteAsyncWrite(commandMember);
            }
        }


        private void ExpireMemberCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            MemberRemoved();

            CachedMemberBytes cachedMemberBytes = value as CachedMemberBytes;
            SynchronizationRequested(cachedMemberBytes.GetCacheKey(),
                cachedMemberBytes.ReferenceTracker.Purge(Constants.NULL_STRING));
            IncrementExpirationCounter();
        }

        private void ExpireMemberAccessCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            CachedMemberAccess cachedMemberAccess = value as CachedMemberAccess;
            //System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "MemberBL.ExpireMemberAccessCallback: Calling synchronization for cachekey: " + cachedMemberAccess.GetCacheKey());

            SynchronizationRequested(cachedMemberAccess.GetCacheKey(),
                cachedMemberAccess.ReferenceTracker.Purge(Constants.NULL_STRING));

        }

        private void ExpireMemberLogonCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            CachedMemberLogon cachedMemberLogon = value as CachedMemberLogon;
            SynchronizationRequested(cachedMemberLogon.GetCacheKey(), cachedMemberLogon.ReferenceTracker.Purge(Constants.NULL_STRING));
        }
        #endregion

        #region Photo
        //versioning added
        public PhotoCollection SavePhotos(string clientHostName,
            Int32 communityID,
            Int32 memberID,
            PhotoUpdate[] photoUpdates, int version)
        {
            return SavePhotos(clientHostName, Constants.NULL_INT, Constants.NULL_INT, communityID, memberID, photoUpdates, version);
        }

        private void savePhotoFile(Int32 memberID,
            Int32 memberPhotoID,
            PhotoFileType fileTypeID,
		    Int32 fileHeight,
		    Int32 fileWidth,
		    Int32 fileSize,
            string fileCloudPath = null,
		    Int32 fileID = Constants.NULL_INT,
            string fileWebPath = null
            )
        {
            Command command = new Command("mnMember", "dbo.up_MemberPhoto_File_Save", memberID);
            command.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
            command.AddParameter("@FileTypeID", SqlDbType.Int, ParameterDirection.Input, (int)fileTypeID);
            command.AddParameter("@FileHeight", SqlDbType.Int, ParameterDirection.Input, fileHeight);
            command.AddParameter("@FileWidth", SqlDbType.Int, ParameterDirection.Input, fileWidth);
            command.AddParameter("@FileSize", SqlDbType.Int, ParameterDirection.Input, fileSize);

            if(fileID != Constants.NULL_INT)
            {
                command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
            }
            if (!string.IsNullOrEmpty(fileWebPath))
            {
                command.AddParameter("@FileWebPath", SqlDbType.VarChar, ParameterDirection.Input, fileWebPath);
            }
            if (!string.IsNullOrEmpty(fileCloudPath))
            {
                command.AddParameter("@FileCloudPath", SqlDbType.VarChar, ParameterDirection.Input, fileCloudPath);
            }
            
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public PhotoCollection SavePhotos(string clientHostName,
            Int32 brandID,
            Int32 siteID,
            Int32 communityID,
            Int32 memberID,
            PhotoUpdate[] photoUpdates, int version)
        {
            try
            {
                bool newPhoto = false;
                
                CachedMemberBytes cachedMemberBytes = getCachedMemberBytes(clientHostName,
                    null,
                    memberID,
                    false, version);
                CachedMember cachedMember = new CachedMember(cachedMemberBytes.MemberBytes, version);

                PhotoCommunity photoCommunity = cachedMember.Photos.GetCommunity(communityID);

                for (Int32 photoUpdateNum = 0; photoUpdateNum < photoUpdates.Length; photoUpdateNum++)
                {
                    PhotoUpdate photoUpdate = photoUpdates[photoUpdateNum];

                    if (photoUpdate.Delete)
                    {
                        Command command = new Command("mnMember", "dbo.up_MemberPhoto_Delete", memberID);
                        command.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, photoUpdate.MemberPhotoID);
                        Client.Instance.ExecuteAsyncWrite(command);

                        Photo photo = photoCommunity.Find(photoUpdate.MemberPhotoID);

                        cachedMember.Photos.GetCommunity(communityID).Remove(photoUpdate.MemberPhotoID);
                    }
                    else
                    {
                        Int32 memberPhotoID = photoUpdate.MemberPhotoID;

                        if (memberPhotoID == Constants.NULL_INT)
                        {
                            newPhoto = true;
                            memberPhotoID = KeySA.Instance.GetKey("MemberPhotoID");
                        }

                        Photo photo = new Photo(memberPhotoID,
                                              photoUpdate.FileID,
                                              photoUpdate.FileWebPath,
                                                //photoUpdate.FilePath,
                                              photoUpdate.ThumbFileID,
                                              photoUpdate.ThumbFileWebPath,
                                                //photoUpdate.ThumbFilePath,
                                              photoUpdate.ListOrder,
                                              photoUpdate.IsApproved,
                                              photoUpdate.IsPrivate,
                                              photoUpdate.AlbumID,
                                              photoUpdate.AdminMemberID, photoUpdate.Caption,
                                              photoUpdate.IsCaptionApproved,
                                              photoUpdate.FileCloudPath, photoUpdate.ThumbFileCloudPath,
                                              photoUpdate.IsApprovedForMain, photoUpdate.IsMain,
                                              photoUpdate.FileHeight, photoUpdate.FileWidth, photoUpdate.FileSize,
                                              photoUpdate.ThumbFileHeight, photoUpdate.ThumbFileWidth, photoUpdate.ThumbFileSize);

                        
                        // if this is an older PhotoUpdate object, we want the default values for IsMain to write, so pass in true if so.
                        savePhoto(memberID, communityID, photo,photoUpdate.IncludeIsMainValue, version);

                        if (newPhoto && !string.IsNullOrEmpty(photoUpdate.OriginalFileCloudPath))
                        {
                            //this is a new photo, so save the photofile record for the original image. 
                            savePhotoFile(memberID, 
                                memberPhotoID, 
                                PhotoFileType.Original, 
                                photoUpdate.FileHeight, 
                                photoUpdate.FileWidth, 
                                photoUpdate.FileSize, 
                                photoUpdate.OriginalFileCloudPath);
                        }

                        foreach(var photoFile in photoUpdate.Files)
                        {
                            savePhotoFile(memberID,
                                memberPhotoID,
                                photoFile.PhotoFileType,
                                photoFile.FileHeight,
                                photoFile.FileWidth,
                                photoFile.FileSize,
                                photoFile.FileCloudPath, 
                                photoFile.FileID,
                                photoFile.FileWebPath);

                            photo.Files.Add(new ValueObjects.Photos.PhotoFile(photoFile.PhotoFileType, photoFile.FileHeight, photoFile.FileWidth,
                                photoFile.FileSize, photoFile.FileID, photoFile.FileCloudPath, photoFile.FileWebPath));
                        }
                        
                        cachedMember.Photos.GetCommunity(communityID)[Convert.ToByte(photoUpdate.ListOrder - 1)] = photo;

                        bool approvalsEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.PHOTO_APPROVAL_ENABLED,
                                                                           communityID, siteID, brandID));

                        if (approvalsEnabled && (photoUpdate.IsNew || photoUpdate.IsNewCaption))
                        {
                            DBQueue.QueueItemPhoto queueItemPhoto = new DBQueue.QueueItemPhoto(communityID, siteID, brandID, memberID, memberPhotoID);
                            DBApproveQueueSA.Instance.Enqueue(queueItemPhoto);
                        }
                    }
                }

                // reorder the photos. usually deletes would create holes in the list; correct this.
                photoCommunity = reorderPhotos(cachedMember, communityID, version);

                cachedMemberBytes.MemberBytes = cachedMember.ToByteArray();
                InsertCachedMemberBytes(cachedMemberBytes, !UseMembaseForBL);
                bool synchronize = null != clientHostName;
                if (synchronize)
                {
                    SynchronizationRequested(cachedMemberBytes.GetCacheKey(), cachedMemberBytes.ReferenceTracker.Purge(clientHostName));
                }
                updatePhotoSearch(photoUpdates, cachedMember, memberID, communityID);
                return cachedMember.Photos;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured while saving photos.", ex));
            }
        }

        public void GetNewImageFile(out Int32 fileID,
            out string filePath,
            out string fileWebPath)
        {
            try
            {
                try
                {
                    fileID = KeySA.Instance.GetKey("FileID");
                }
                catch (Exception ex)
                {
                    throw (new BLException("Could not get new FileID when attempting to save photo.", ex));
                }

                filePath = assignFilePath(fileID, "jpg", out fileWebPath);
            }
            catch (Exception ex)
            {
                throw new BLException("Error getting new image file.", ex);
            }
        }


        private PhotoCommunity reorderPhotos(CachedMember cachedMember, int communityId, int version)
        {
            var photoCommunity = cachedMember.Photos.GetCommunity(communityId);
            var memberId = cachedMember.MemberID;
            var mainPhotoSet = false;
            byte isMainIndex = 0;

            #region Auto Main Photo setting logic
            // if member has a main eligible photo but it's not set to main, let's set it for them
            var firstPhoto = photoCommunity.Count > 0 ? photoCommunity[0] : null;
            if(version >= 3 && firstPhoto != null && !firstPhoto.IsMain && firstPhoto.IsApprovedForMain && firstPhoto.IsApproved)
            {
                firstPhoto.IsMain = true;
                savePhoto(cachedMember.MemberID, communityId, firstPhoto, true, version);
            }

            if (version >= 3 && firstPhoto != null)
            {
                mainPhotoSet = firstPhoto.IsMain;
            }
            
            // If mainPhotoSet is true at this point, either the first photo was main already or it was main eligible,
            // so we set it for the member. With this If block, we need to find a main photo outside slot 1 and set other
            // main photos to non-main if we happen to have multiple main photos by error.
            if (version >= 3 && !mainPhotoSet && photoCommunity.Count > 1)
            {
                var setToNonMain = false;
                // if the first photo was already main, we wouldn't be inside this if statement so skip the first item
                for (byte pn = 1; pn < photoCommunity.Count; pn++)
                {
                    if (photoCommunity[pn] != null && photoCommunity[pn].IsApprovedForMain && photoCommunity[pn].IsApproved)
                    {
                        if(isMainIndex == 0)
                        {
                            isMainIndex = pn;
                        }
                        else
                        {
                            // if we found the main we want already, make sure we don't have extra mains
                            setToNonMain = photoCommunity[pn].IsMain;
                        }
                    }

                    if(setToNonMain)
                    {
                        photoCommunity[pn].IsMain = false;
                        savePhoto(cachedMember.MemberID, communityId, photoCommunity[pn], true, version);
                    }

                    setToNonMain = false;
                }
            }

            // if main eligible photo was found outside slot 1, bubble it up to the top
            if(isMainIndex > 0)
            {
                for(byte pn = isMainIndex; pn > 0; pn--)
                {
                    var botPh = photoCommunity[pn];
                    var topPh = photoCommunity[Convert.ToByte(pn - 1)];

                    if(pn == isMainIndex)
                    {
                        botPh.IsMain = true;
                    }

                    photoCommunity[pn] = topPh;
                    photoCommunity[Convert.ToByte(pn - 1)] = botPh;
                }
            }
            #endregion

            // if Photo.ListOrder differs from the current index position, update the ListOrder in cache and the DB.
            var photoCommunityNew = new PhotoCommunity();
            byte index = 1;
            for (byte photoNum = 0; photoNum < photoCommunity.Count; photoNum++)
            {
                Photo photo = photoCommunity[photoNum];

                if (photo != null)
                {
                    if (photo.ListOrder == index)
                    {
                        photoCommunityNew[Convert.ToByte(index - 1)] = photo;
                    }
                    else
                    {
                        Photo photoNew = null;

                        #region build photoNew depending on version

                        if (version >= 5)
                        {
                            photoNew = new Photo(photo.MemberPhotoID,
                                                 photo.FileID,
                                                 photo.FileWebPath,
                                                 photo.ThumbFileID,
                                                 photo.ThumbFileWebPath,
                                                 index,
                                                 photo.IsApproved,
                                                 photo.IsPrivate,
                                                 photo.AlbumID,
                                                 photo.AdminMemberID, photo.Caption, photo.IsCaptionApproved,
                                                 photo.FileCloudPath, photo.ThumbFileCloudPath,
                                                 photo.IsApprovedForMain, photo.IsMain);

                            if(photo.Files != null && photo.Files.Count > 0)
                            {
                                foreach(var photoFile in photo.Files)
                                {
                                    photoNew.Files.Add(new ValueObjects.Photos.PhotoFile(photoFile.PhotoFileType,
                                                                                         photoFile.FileHeight,
                                                                                         photoFile.FileWidth,
                                                                                         photoFile.FileSize,
                                                                                         photoFile.FileID,
                                                                                         photoFile.FileWebPath,
                                                                                         photoFile.FileCloudPath));
                                }

                            }
                        }
                        else
                        {
                            photoNew = new Photo(photo.MemberPhotoID,
                                                 photo.FileID,
                                                 photo.FileWebPath,
                                                 photo.ThumbFileID,
                                                 photo.ThumbFileWebPath,
                                                 index,
                                                 photo.IsApproved,
                                                 photo.IsPrivate,
                                                 photo.AlbumID,
                                                 photo.AdminMemberID, photo.Caption, photo.IsCaptionApproved,
                                                 photo.FileCloudPath, photo.ThumbFileCloudPath);
                        }

                        #endregion
                        
                        // save the change to DB.
                        savePhoto(memberId, communityId, photoNew, true, version);

                        Byte byteIndex = Convert.ToByte(index - 1);
                        photoCommunityNew[byteIndex] = photoNew;

                        // Update cachedMember.
                        cachedMember.Photos.GetCommunity(communityId)[byteIndex] = photoNew;
                    }
                    index++;
                }
            }

            return photoCommunityNew;
        }

        private void savePhoto(Int32 memberID,
            Int32 communityID,
                               Photo photo,
                               bool includeIsMainValue,
                               int version)
        {
                savePhoto(memberID,
                    photo.MemberPhotoID,
                    communityID,
                    photo.FileID,
                    photo.FileWebPath,
                    photo.ThumbFileID,
                    photo.ThumbFileWebPath,
                    photo.AdminMemberID,
                    photo.AlbumID,
                    photo.ListOrder,
                    photo.IsApproved,
                    photo.IsPrivate,
                    photo.Caption,
                    photo.IsCaptionApproved,
                    photo.FileCloudPath,
                    photo.ThumbFileCloudPath,
                    photo.IsApprovedForMain,
                    photo.IsMain,
                    includeIsMainValue,
                    version >= 4, 
                    photo.FileHeight, 
                    photo.FileWidth, 
                    photo.FileSize, 
                    photo.ThumbFileHeight, 
                    photo.ThumbFileWidth, 
                    photo.ThumbFileSize); 
        }

        #region private savePhoto which doesn't called anymore (remove later)
        //private void savePhoto(Int32 memberID,
        //    Int32 memberPhotoID,
        //    Int32 communityID,
        //    Int32 fileID,
        //    string fileWebPath,
        //    Int32 thumbFileID,
        //    string thumbFileWebPath,
        //    Int32 adminMemberID,
        //    Int32 albumID,
        //    byte listOrder,
        //    bool isApproved,
        //    bool isPrivate,
        //    string caption,
        //    bool isCaptionApproved)
        //{
        //    savePhoto(memberID, memberPhotoID, communityID, fileID, fileWebPath, thumbFileID, thumbFileWebPath, adminMemberID, albumID, listOrder,
        //       isApproved, isPrivate, caption, isCaptionApproved, string.Empty, string.Empty, true, false);
        //}
        #endregion

        private void savePhoto(Int32 memberID,
            Int32 memberPhotoID,
            Int32 communityID,
            Int32 fileID,
            string fileWebPath,
            Int32 thumbFileID,
            string thumbFileWebPath,
            Int32 adminMemberID,
            Int32 albumID,
            byte listOrder,
            bool isApproved,
            bool isPrivate,
            string caption,
            bool isCaptionApproved,
            string fileCloudPath,
            string thumbFileCloudPath,
            bool isApprovedForMain,
            bool isMain,
            bool includeIsMainValue,
            bool saveExtendedData, 
            int fileHeight,
            int fileWidth, 
            int fileSize, 
            int thumbFileHeight, 
            int thumbFileWidth, 
            int thumbFileSize)
        {
            Command command = new Command("mnMember", "dbo.up_MemberPhoto_Save2", memberID);
            command.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);

            if (fileID != Constants.NULL_INT)
            {
                command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
            }

            if (fileWebPath != Constants.NULL_STRING)
            {
                command.AddParameter("@FileWebPath", SqlDbType.VarChar, ParameterDirection.Input, fileWebPath);
            }

            if (thumbFileID != Constants.NULL_INT)
            {
                command.AddParameter("@ThumbFileID", SqlDbType.Int, ParameterDirection.Input, thumbFileID);
            }

            if (thumbFileWebPath != Constants.NULL_STRING)
            {
                command.AddParameter("@ThumbFileWebPath", SqlDbType.VarChar, ParameterDirection.Input, thumbFileWebPath);
            }

            if (adminMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            }

            if (albumID != Constants.NULL_INT)
            {
                command.AddParameter("@AlbumID", SqlDbType.Int, ParameterDirection.Input, albumID);
            }

            if (listOrder > 0)
            {
                command.AddParameter("@ListOrder", SqlDbType.TinyInt, ParameterDirection.Input, listOrder);
            }

            command.AddParameter("@ApprovedFlag", SqlDbType.Int, ParameterDirection.Input, isApproved);
            command.AddParameter("@PrivateFlag", SqlDbType.Int, ParameterDirection.Input, isPrivate);
            command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            command.AddParameter("@Caption", SqlDbType.NVarChar, ParameterDirection.Input, caption);
            command.AddParameter("@CaptionApprovedFlag", SqlDbType.Int, ParameterDirection.Input, isCaptionApproved);

            if (fileCloudPath != string.Empty)
            {
                command.AddParameter("@FileCloudPath", SqlDbType.VarChar, ParameterDirection.Input, fileCloudPath.ToLower());
            }
            if (thumbFileCloudPath != string.Empty)
            {
                command.AddParameter("@ThumbFileCloudPath", SqlDbType.VarChar, ParameterDirection.Input, thumbFileCloudPath.ToLower());
            }
            
            command.AddParameter("@ApprovedForMain", SqlDbType.Int, ParameterDirection.Input, isApprovedForMain);
     
            if (includeIsMainValue)
            {
                command.AddParameter("@IsMain", SqlDbType.Int, ParameterDirection.Input, isMain);
            }

            if (saveExtendedData && fileHeight != Constants.NULL_INT)
            {
                command.AddParameter("@FileHeight", SqlDbType.Int, ParameterDirection.Input, fileHeight);
            }
            if (saveExtendedData && fileWidth != Constants.NULL_INT)
            {
                command.AddParameter("@FileWidth", SqlDbType.Int, ParameterDirection.Input, fileWidth);
            }
            if (saveExtendedData && fileSize != Constants.NULL_INT)
            {
                command.AddParameter("@FileSize", SqlDbType.Int, ParameterDirection.Input, fileSize);
            }
            if (saveExtendedData && thumbFileHeight != Constants.NULL_INT)
            {
                command.AddParameter("@ThumbFileHeight", SqlDbType.Int, ParameterDirection.Input, thumbFileHeight);
            }
            if (saveExtendedData && thumbFileWidth != Constants.NULL_INT)
            {
                command.AddParameter("@ThumbFileWidth", SqlDbType.Int, ParameterDirection.Input, thumbFileWidth);
            }
            if (saveExtendedData && thumbFileSize != Constants.NULL_INT)
            {
                command.AddParameter("@ThumbFileSize", SqlDbType.Int, ParameterDirection.Input, thumbFileSize);
            }

            Client.Instance.ExecuteAsyncWrite(command);
        }


        public void InsertFile(Int32 fileID,
            Int32 rootID,
            string fileExtension,
            DateTime insertDate)
        {
            Int32 fileRootID = KeySA.Instance.GetKey("FileRootID");

            Command command = new Command("mnFile", "dbo.up_File_Insert", fileID);
            command.AddParameter("@RootID", SqlDbType.Int, ParameterDirection.Input, rootID);
            command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
            command.AddParameter("@FileRootID", SqlDbType.Int, ParameterDirection.Input, fileRootID);
            command.AddParameter("@Extension", SqlDbType.VarChar, ParameterDirection.Input, fileExtension);
            command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, insertDate);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void InsertPhotoSearch(PhotoUpdate update, int memberid, int communityid)
        {
            try
            {
                Matchnet.PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.InsertPhoto(update, memberid, communityid);

            }
            catch (Exception ex)
            { System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.MemberBL", "Error inserting into PhotoStore:" + ex.Message, System.Diagnostics.EventLogEntryType.Warning); }
        }
        private string assignFilePath(Int32 fileID,
            string fileExtension,
            out string fileWebPath)
        {
            try
            {
                Root root = FileRootSA.Instance.GetRootGroups().GetWritableRoot();
                DateTime insertDate = Convert.ToDateTime(DateTime.Now.ToString("g")); // no seconds. sql server smalldatetime rounds seconds

                InsertFile(fileID,
                    root.ID,
                    fileExtension,
                    insertDate);

                string folder = insertDate.ToString(DATE_FORMAT);

                fileWebPath = root.WebPath + getFileRelativeWebPath(fileID, fileExtension, insertDate, DEFAULT_SAVE_DEPTH);
                return root.Path + getFileRelativePath(fileID, fileExtension, insertDate, DEFAULT_SAVE_DEPTH);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when inserting the member photo's file path.", ex));
            }
        }


        private string getFileRelativePath(Int32 fileID, string fileExtension, DateTime insertDate, Int32 saveDepth)
        {
            return getFileRelativeWebPath(fileID, fileExtension, insertDate, saveDepth).Replace("/", @"\");
        }


        private string getFileRelativeWebPath(Int32 fileID, string fileExtension, DateTime insertDate, Int32 saveDepth)
        {
            string dateFormat = DATE_FORMAT;

            if (saveDepth == 3)
            {
                dateFormat = "yyyy/MM/dd/";
            }

            return insertDate.ToString(dateFormat) + fileID.ToString() + "." + fileExtension;
        }
        #endregion

        #region admin member search
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByEmail(string machineName,
            CacheReference cacheReference,
            string email,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnLogon", "up_LogonMemberCommunity_List_ByEmail", 0);

                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, email);
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);
                command.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

                SqlParameterCollection totalRowsCount;
                DataTable dt = Client.Instance.ExecuteDataTable(command, out totalRowsCount);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false);

                    members.Add(cachedMember);
                }

                totalRows = Convert.ToInt32(totalRowsCount["@TotalRows"].Value); // members.Count; 
                if (totalRows > 100)
                    totalRows = 100;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by email.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByUserName(string machineName,
            CacheReference cacheReference,
            string userName,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnAdmin", "dbo.up_Member_List_ByUserName", 0);

                command.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, userName);
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);
                command.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

                SqlParameterCollection totalRowsCount;
                DataTable dt = Client.Instance.ExecuteDataTable(command, out totalRowsCount);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false);

                    members.Add(cachedMember);
                }

                totalRows = Convert.ToInt32(totalRowsCount["@TotalRows"].Value); // members.Count; 
                if (totalRows > 100)
                    totalRows = 100;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by username.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByPhoneNumber(string machineName,
            CacheReference cacheReference,
            string phoneNumber,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnAdmin", "dbo.up_AdminPaymentAttribute_List_ByPhone", 0);

                command.AddParameter("@PhoneNumber", SqlDbType.VarChar, ParameterDirection.Input, phoneNumber);
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false);

                    members.Add(cachedMember);
                }

                totalRows = members.Count;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by phone.", ex));
            }
        }

        private static string getHash(string routing, string checking)
        {
            return getHash(routing + checking);
        }

        private static string getHash(string creditCard)
        {
            return Crypto.Hash(creditCard.Trim() + "00025554F494");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByCreditCard(string machineName,
            CacheReference cacheReference,
            string creditCard,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            try
            {
                return getMembersByHash(machineName,
                    cacheReference,
                    getHash(creditCard),
                    startRow,
                    pageSize,
                    ref totalRows);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by credit card.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByChecking(string machineName,
            CacheReference cacheReference,
            string routing,
            string checking,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            try
            {
                return getMembersByHash(machineName,
                    cacheReference,
                    getHash(routing + checking),
                    startRow, pageSize,
                    ref totalRows);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by checking.", ex));
            }
        }

        private ArrayList getMembersByHash(string machineName,
            CacheReference cacheReference,
            string hash,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            ArrayList members = new ArrayList();

            Command command = new Command("mnLookup", "dbo.up_CSLookup_List", 0);

            command.AddParameter("@Hash", SqlDbType.VarChar, ParameterDirection.Input, hash);
            command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
            command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
            command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output);

            SqlParameterCollection totalRowsCount;
            DataTable dt = Client.Instance.ExecuteDataTable(command, out totalRowsCount);

            if (dt.Rows.Count.Equals(0))
                return members;

            for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
            {
                DataRow row = dt.Rows[rowNum];

                CachedMember cachedMember = GetCachedMember(machineName,
                    cacheReference,
                    Convert.ToInt32(row[0]),
                    false);

                members.Add(cachedMember);
            }

            totalRows = Convert.ToInt32(totalRowsCount["@TotalRows"].Value);

            return members;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByMemberID(string machineName,
            CacheReference cacheReference,
            int memberID,
            ref int totalRows)
        {
            return GetMembersByMemberID(machineName,
                cacheReference,
                memberID,
                0,
                0,
                ref totalRows);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByMemberID(string machineName,
            CacheReference cacheReference,
            int memberID,
            int startRow,
            int pageSize,
            ref int totalRows)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnAdmin", "dbo.up_Member_List_ByMemberID", 0);

                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false);

                    members.Add(cachedMember);
                }

                totalRows = members.Count;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by member ID.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public MemberPaymentCollection GetMemberPaymentInfo(int memberID)
        {
            try
            {
                MemberPaymentCollection memberPaymentCollection = new MemberPaymentCollection();

                Command command = new Command("mnAdmin", "dbo.up_AdminPaymentAttribute_List", 0);

                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count.Equals(0))
                    return memberPaymentCollection;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    int memberPaymentAttributeID = Convert.ToInt32(row["AdminPaymentAttributeID"]);
                    string phoneNumber = Convert.ToString(row["PhoneNumber"]);
                    //TOFIX - MemberPaymentAttribute table no longer has CreditCardNumber field, not sure if this is needed or not.
                    string creditCardNumber = string.Empty; //Convert.ToString(row["CreditCardNumber"]);
                    DateTime insertDate = Convert.ToDateTime(row["InsertDate"]);

                    MemberPayment payment =
                        new MemberPayment(memberPaymentAttributeID, phoneNumber, creditCardNumber, insertDate);

                    memberPaymentCollection.Add(payment);
                }

                return memberPaymentCollection;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member payment info by member ID.", ex));
            }
        }

        //versioning

        public int[] PerformAdminSearch(string email, string userName,string memberId, string phone, string firstName,
            string lastName, string zipcode, string city, string ipAddress, int communityId, bool exactUsername, int filterType)
        {
            try
            {
                Command command = new Command("mnMember_ProdFlat_CSSearch", "dbo.usp_CS_Search", 0);

                if (!string.IsNullOrEmpty(email))
                    command.AddParameter("@email", SqlDbType.VarChar, ParameterDirection.Input, email);

                if (!string.IsNullOrEmpty(userName))
                    command.AddParameter("@username", SqlDbType.VarChar, ParameterDirection.Input, userName);

                if (!string.IsNullOrEmpty(memberId))
                    command.AddParameter("@memberid", SqlDbType.VarChar, ParameterDirection.Input, memberId);

                if (!string.IsNullOrEmpty(phone))
                    command.AddParameter("@phone", SqlDbType.VarChar, ParameterDirection.Input, phone);

                if (!string.IsNullOrEmpty(firstName))
                    command.AddParameter("@firstname", SqlDbType.VarChar, ParameterDirection.Input, firstName);

                if (!string.IsNullOrEmpty(lastName))
                    command.AddParameter("@lastname", SqlDbType.VarChar, ParameterDirection.Input, lastName);

                if (!string.IsNullOrEmpty(zipcode))
                    command.AddParameter("@zipcode", SqlDbType.VarChar, ParameterDirection.Input, zipcode);

                if (!string.IsNullOrEmpty(city))
                    command.AddParameter("@city", SqlDbType.VarChar, ParameterDirection.Input, city);

                if (!string.IsNullOrEmpty(ipAddress))
                    command.AddParameter("@ipaddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);

                if (communityId != Constants.NULL_INT)
                    command.AddParameter("@communityid", SqlDbType.Int, ParameterDirection.Input, communityId);

                command.AddParameter("@exactmatch", SqlDbType.Bit, ParameterDirection.Input, exactUsername ? 1 : 0);

                 //None = 0,Active = 1,Subscriber = 2,NonSubscriber = 3,AdminSuspended = 4,SelfSuspended = 5
                switch(filterType)
                {
                    case 1:
                        command.AddParameter("@active", SqlDbType.Bit, ParameterDirection.Input, 1);
                        break;
                    case 4:
                        command.AddParameter("@AdminSuspended", SqlDbType.Bit, ParameterDirection.Input, 1);
                        break;
                    case 5:
                        command.AddParameter("@SelfSuspended", SqlDbType.Bit, ParameterDirection.Input, 1);
                        break;
                }

                var dt = Client.Instance.ExecuteDataTable(command);
                if (dt == null || dt.Rows.Count == 0)
                    return null;

                var memberIds = new int[dt.Rows.Count];
                for(int i=0; i<dt.Rows.Count; i++)
                {
                    memberIds[i] = Convert.ToInt32(dt.Rows[i]["memberid"]);
                }
                return memberIds;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when performing an admin search.", ex));
            }
        }

        public int[] PerformAdminSearchPagination(string email, string userName, string memberId, string phone, string firstName,
            string lastName, string zipcode, string city, string ipAddress, int communityId, bool exactUsername, int filterType,
            int pageNumber, int rowsPerPage)
        {
            try
            {
                Command command = new Command("mnMember_ProdFlat_CSSearch", "dbo.usp_CS_Search_Paging", 0);

                if (!string.IsNullOrEmpty(email))
                    command.AddParameter("@email", SqlDbType.VarChar, ParameterDirection.Input, email);

                if (!string.IsNullOrEmpty(userName))
                    command.AddParameter("@username", SqlDbType.VarChar, ParameterDirection.Input, userName);

                if (!string.IsNullOrEmpty(memberId))
                    command.AddParameter("@memberid", SqlDbType.VarChar, ParameterDirection.Input, memberId);

                if (!string.IsNullOrEmpty(phone))
                    command.AddParameter("@phone", SqlDbType.VarChar, ParameterDirection.Input, phone);

                if (!string.IsNullOrEmpty(firstName))
                    command.AddParameter("@firstname", SqlDbType.VarChar, ParameterDirection.Input, firstName);

                if (!string.IsNullOrEmpty(lastName))
                    command.AddParameter("@lastname", SqlDbType.VarChar, ParameterDirection.Input, lastName);

                if (!string.IsNullOrEmpty(zipcode))
                    command.AddParameter("@zipcode", SqlDbType.VarChar, ParameterDirection.Input, zipcode);

                if (!string.IsNullOrEmpty(city))
                    command.AddParameter("@city", SqlDbType.VarChar, ParameterDirection.Input, city);

                if (!string.IsNullOrEmpty(ipAddress))
                    command.AddParameter("@ipaddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);

                if (communityId != Constants.NULL_INT)
                    command.AddParameter("@communityid", SqlDbType.Int, ParameterDirection.Input, communityId);

                command.AddParameter("@exactmatch", SqlDbType.Bit, ParameterDirection.Input, exactUsername ? 1 : 0);

                //None = 0,Active = 1,Subscriber = 2,NonSubscriber = 3,AdminSuspended = 4,SelfSuspended = 5
                switch (filterType)
                {
                    case 1:
                        command.AddParameter("@active", SqlDbType.Bit, ParameterDirection.Input, 1);
                        break;
                    case 4:
                        command.AddParameter("@AdminSuspended", SqlDbType.Bit, ParameterDirection.Input, 1);
                        break;
                    case 5:
                        command.AddParameter("@SelfSuspended", SqlDbType.Bit, ParameterDirection.Input, 1);
                        break;
                }

                command.AddParameter("@rowsperpage", SqlDbType.Int, ParameterDirection.Input, rowsPerPage);
                command.AddParameter("@page", SqlDbType.Int, ParameterDirection.Input, pageNumber);

                var dt = Client.Instance.ExecuteDataTable(command);
                if (dt == null || dt.Rows.Count == 0)
                    return null;

                var memberIds = new int[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    memberIds[i] = Convert.ToInt32(dt.Rows[i]["memberid"]);
                }
                return memberIds;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when performing an admin search (Pagination version).", ex));
            }
        }

        public ArrayList GetMembersByEmail(string machineName,
            CacheReference cacheReference,
            string email,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            return getMembersByEmail(machineName, cacheReference, email, startRow, pageSize, ref totalRows, version, false);
        }

        public ArrayList GetMembersByEmail(string machineName,
            CacheReference cacheReference,
            string email,
            int startRow,
            int pageSize,
            ref int totalRows,
            int version,
            bool memberIdOrderByDesc)
        {
            return getMembersByEmail(machineName, cacheReference, email, startRow, pageSize, ref totalRows, version, memberIdOrderByDesc);
        }

        private ArrayList getMembersByEmail(string machineName,
            CacheReference cacheReference,
            string email,
            int startRow,
            int pageSize,
            ref int totalRows,
            int version,
            bool memberIdOrderByDesc)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnLogon", "up_LogonMemberCommunity_List_ByEmail", 0);

                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, email);
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@MemberIDOrderByDesc", SqlDbType.Bit, ParameterDirection.Input, memberIdOrderByDesc);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);
                command.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

                SqlParameterCollection totalRowsCount;
                DataTable dt = Client.Instance.ExecuteDataTable(command, out totalRowsCount);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false, version);

                    members.Add(cachedMember);
                }

                totalRows = Convert.ToInt32(totalRowsCount["@TotalRows"].Value); // members.Count; 
                if (totalRows > 100)
                    totalRows = 100;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by email.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByUserName(string machineName,
            CacheReference cacheReference,
            string userName,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            return getMembersByUserName(machineName, cacheReference, userName, startRow, pageSize, ref totalRows, version,
                false, false);
        }

        public ArrayList GetMembersByUserName(string machineName,
            CacheReference cacheReference,
            string userName,
            int startRow,
            int pageSize,
            ref int totalRows,
            int version,
            bool memberIdOrderByDesc,
            bool exactMatchOnly)
        {
            return getMembersByUserName(machineName, cacheReference, userName, startRow, pageSize, ref totalRows, version,
               memberIdOrderByDesc, exactMatchOnly);
        }

        private ArrayList getMembersByUserName(string machineName,
            CacheReference cacheReference,
            string userName,
            int startRow,
            int pageSize,
            ref int totalRows,
            int version,
            bool memberIdOrderByDesc,
            bool exactMatchOnly)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnAdmin", "dbo.up_Member_List_ByUserName", 0);

                command.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, userName);
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@MemberIDOrderByDesc", SqlDbType.Bit, ParameterDirection.Input, memberIdOrderByDesc);
                command.AddParameter("@ExactMatchOnly", SqlDbType.Bit, ParameterDirection.Input, exactMatchOnly);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);
                command.AddParameter("ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

                SqlParameterCollection totalRowsCount;
                DataTable dt = Client.Instance.ExecuteDataTable(command, out totalRowsCount);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false, version);

                    members.Add(cachedMember);
                }

                totalRows = Convert.ToInt32(totalRowsCount["@TotalRows"].Value); // members.Count; 
                if (totalRows > 100)
                    totalRows = 100;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by username.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByPhoneNumber(string machineName,
            CacheReference cacheReference,
            string phoneNumber,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnAdmin", "dbo.up_AdminPaymentAttribute_List_ByPhone", 0);

                command.AddParameter("@PhoneNumber", SqlDbType.VarChar, ParameterDirection.Input, phoneNumber);
                command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
                command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false, version);

                    members.Add(cachedMember);
                }

                totalRows = members.Count;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by phone.", ex));
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByCreditCard(string machineName,
            CacheReference cacheReference,
            string creditCard,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            try
            {
                return getMembersByHash(machineName,
                    cacheReference,
                    getHash(creditCard),
                    startRow,
                    pageSize,
                    ref totalRows, version);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by credit card.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByChecking(string machineName,
            CacheReference cacheReference,
            string routing,
            string checking,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            try
            {
                return getMembersByHash(machineName,
                    cacheReference,
                    getHash(routing + checking),
                    startRow, pageSize,
                    ref totalRows, version);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by checking.", ex));
            }
        }

        private ArrayList getMembersByHash(string machineName,
            CacheReference cacheReference,
            string hash,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            ArrayList members = new ArrayList();

            Command command = new Command("mnLookup", "dbo.up_CSLookup_List", 0);

            command.AddParameter("@Hash", SqlDbType.VarChar, ParameterDirection.Input, hash);
            command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
            command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
            command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output);

            SqlParameterCollection totalRowsCount;
            DataTable dt = Client.Instance.ExecuteDataTable(command, out totalRowsCount);

            if (dt.Rows.Count.Equals(0))
                return members;

            for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
            {
                DataRow row = dt.Rows[rowNum];

                CachedMember cachedMember = GetCachedMember(machineName,
                    cacheReference,
                    Convert.ToInt32(row[0]),
                    false, version);

                members.Add(cachedMember);
            }

            totalRows = Convert.ToInt32(totalRowsCount["@TotalRows"].Value);

            return members;
        }
        public ArrayList GetMembersByMemberID(string machineName,
            CacheReference cacheReference,
            int memberID,
            ref int totalRows, int version)
        {
            return GetMembersByMemberID(machineName,
                cacheReference,
                memberID,
                0,
                0,
                ref totalRows, version);
        }

        public MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId)
        {
            try
            {
                Command command = new Command("mnLogon", "dbo.up_LogonMemberCommunity_ListbyMemberID", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (!dt.Rows.Count.Equals(1))
                    return null;

                var emailaddress =dt.Rows[0]["EmailAddress"].ToString();
                var username = dt.Rows[0]["UserName"].ToString();

                return new MemberBasicLogonInfo {MemberId = memberId, EmailAddress = emailaddress, UserName = username};
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting basic member logon info by member ID.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByMemberID(string machineName,
            CacheReference cacheReference,
            int memberID,
            int startRow,
            int pageSize,
            ref int totalRows, int version)
        {
            try
            {
                ArrayList members = new ArrayList();

                Command command = new Command("mnAdmin", "dbo.up_Member_List_ByMemberID", 0);

                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count.Equals(0))
                    return members;

                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow row = dt.Rows[rowNum];

                    CachedMember cachedMember = GetCachedMember(machineName,
                        cacheReference,
                        Convert.ToInt32(row[0]),
                        false, version);

                    members.Add(cachedMember);
                }

                totalRows = members.Count;

                return members;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting member info by member ID.", ex));
            }
        }



        #endregion

        #region admin others
        public AdminMemberDomainMapperCollection GetAdminMemberDomainMappers()
        {
            AdminMemberDomainMapperCollection list = new AdminMemberDomainMapperCollection();
            SqlDataReader dataReader = null;
            try
            {

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnAdmin";
                comm.StoredProcedureName = "dbo.up_AdminMemberDomainMapper_List";
                dataReader = Client.Instance.ExecuteReader(comm);

                while (dataReader.Read())
                {
                    AdminMemberDomainMapper item = new AdminMemberDomainMapper();
                    item.MemberID = dataReader.IsDBNull(dataReader.GetOrdinal("MemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("MemberID"));
                    item.DomainAccount = dataReader.IsDBNull(dataReader.GetOrdinal("DomainAccount")) ? "" : dataReader.GetString(dataReader.GetOrdinal("DomainAccount"));
                    item.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
                    item.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
                    list.Add(item);
                }

            }
            catch (Exception ex)
            {
                throw (new BLException("BL.GetAdminMemberDomainMappers() error. " + ex.Message, ex));
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return list;
        }

        public void IncrementCommunicationCounter(int memberId, int siteId, int[] counterType)
        {
            if (counterType == null)
                return;

            foreach (int t in counterType)
            {
                incrementCommunicationCounter(memberId, siteId, t);
            }
        }

        private void incrementCommunicationCounter(int memberId, int siteId, int counterType)
        {
            try
            {
                var cmd = new Command("mnAdmin", "up_CommunicationWarning_Increment", 0);

                cmd.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
                cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteId);
                cmd.AddParameter("@WarningType", SqlDbType.Int, ParameterDirection.Input, counterType);

                Client.Instance.ExecuteAsyncWrite(cmd);
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("MemberBL.incrementCommunicationCounter() exception with MemberID:{0}, SiteID:{1}, CounterType:{2}",
                    memberId, siteId, counterType), ex);
            }
        }

        public Dictionary<int, int> GetCommunicationWarningCounts(int memberId, int siteId)
        {
            try
            {
                var cmd = new Command("mnAdmin", "up_CommunicationWarning_List", 0);

                cmd.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
                cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteId);

                var dt = Client.Instance.ExecuteDataTable(cmd);

                if(dt != null && dt.Rows.Count > 0)
                {
                    var dict = new Dictionary<int, int>();
                    foreach(DataRow row in dt.Rows)
                    {
                        dict.Add(Convert.ToInt32(row["WarningType"]), Convert.ToInt32(row["WarningCount"]));
                    }
                    return dict;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new BLException(string.Format("MemberBL.GetCommunicationWarningCounts() exception with MemberID:{0}, SiteID:{1}",
                    memberId, siteId), ex);
            }
        }
        #endregion

        #region Messmo Stuff
        public void SendMessmoUserTransactionStatus(int memberID, int siteID, DateTime newSubExpDate)
        {
            try
            {
                // check to see if the site has Messmo enabled
                Site aSite = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID);
                bool isSiteMessmoEnabled = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MESSMO_UI_ENABLED", aSite.Community.CommunityID, siteID));

                if (isSiteMessmoEnabled)
                {
                    CachedMemberBytes cachedMemberBytes = getCachedMemberBytes(string.Empty,
                                                                                null,
                                                                                memberID,
                                                                                false, CachedMemberBytes.CACHED_MEMBERBYTES_VERSION);

                    CachedMember cachedMember = new CachedMember(cachedMemberBytes.MemberBytes, CachedMemberBytes.CACHED_MEMBERBYTES_VERSION);

                    // check to see if the user is messmo capable
                    Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
                    AttributeGroup agMessmoCapable = attributes.GetAttributeGroup(aSite.Community.CommunityID, attributes.GetAttribute("MessmoCapable").ID);
                    int messmoCapable = cachedMember.GetAttributeInt(agMessmoCapable.ID, Constants.NULL_INT);

                    if (messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending ||
                        messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable)
                    {
                        // retrieve the user's messmo sub id
                        TextStatusType textStatus = TextStatusType.None;
                        AttributeGroup agMessmoSubID = attributes.GetAttributeGroup(aSite.Community.CommunityID, attributes.GetAttribute("MessmoSubscriberID").ID);
                        string messmoSubID = cachedMember.GetAttributeText(agMessmoSubID.ID, aSite.LanguageID, string.Empty, out textStatus);

                        // call externalSA to send the user's transaction status
                        ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoUserTransStatusRequestBySiteID(siteID,
                            DateTime.Now,
                            messmoSubID,
                            Matchnet.HTTPMessaging.Constants.MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL,
                            newSubExpDate);

                    }
                }
            }
            catch (Exception ex)
            {
                //lets just do logging no throwing exception
                BLException blex = new BLException(string.Format("SendMessmoUserTransactionStatus exception (just logging) --> MemberID: {0}, SiteID: {1}, SubExpDate: {2}", memberID, siteID, newSubExpDate), ex);
            }

        }

        /// <summary>
        /// Reverse lookup of our MemberID from Messmo's subscriber ID.  This is obviously not cached, so shouldn't be hit that often.
        /// Right now it's being used for the intitial sign-up of Messmo's service where Messmo passes back their subscriber ID to indicate
        /// to us that the member has agreed to use the Messmo service.
        /// </summary>
        /// <param name="messmoID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public int GetMemberIDByMessmoID(string messmoID, int communityID)
        {
            try
            {
                int memberID = Constants.NULL_INT;

                Command command = new Command("mnLogon", "dbo.up_LogonMessmo_ListByMessmoID", 0);
                command.AddParameter("@MessmoID", SqlDbType.VarChar, ParameterDirection.Input, messmoID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);

                SqlDataReader reader = Client.Instance.ExecuteReader(command);

                if (reader.Read())
                {
                    memberID = Conversion.CInt(reader[0]);
                }

                return memberID;
            }
            catch (Exception ex)
            {
                throw (new BLException("GetMemberIDByMessmodID() error (messmoID: " + messmoID + " communityID: " + communityID + ").", ex));
            }
        }

        /// <summary>
        /// During the intial sign-up of Messmo, we have both our memberID and the Messmo's subscriber ID; write these values
        /// to a table so that we can do a reverse lookup later when needed.
        /// </summary>
        /// <param name="messmoID"></param>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        public void SaveLogonMessmo(string messmoID, int memberID, int communityID)
        {
            try
            {
                Command command = new Command("mnLogon", "dbo.up_LogonMessmo_Save", 0);
                command.AddParameter("@MessmoID", SqlDbType.VarChar, ParameterDirection.Input, messmoID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw (new BLException(string.Format("SaveLogonMessmo() error (messmoID: {0}, memberID: {1}, communityID: {2}", messmoID, memberID, communityID), ex));
            }
        }

        /// <summary>
        /// Adds a row to mnAlert.ScheduledFeedSchedule table so that the ScheduledFeed service knows to process this member.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        public void SubscribeToMessmoDailyFeed(int memberID, int communityID)
        {
            try
            {
                Command command = new Command("mnAlert", "up_ScheduledFeedSchedule_Save", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@ScheduledFeedTypeID", SqlDbType.Int, ParameterDirection.Input, (int)Matchnet.Member.ValueObjects.Enumerations.ScheduledFeedType.Messmo);
                command.AddParameter("@Frequency", SqlDbType.Int, ParameterDirection.Input, 1); // 1 day (everyday)
                command.AddParameter("@NextAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw (new BLException(string.Format("SubscribeToMessmoDailyFeed() error (memberID: {0}, communityID: {1}", memberID, communityID), ex));
            }
        }

        /// <summary>
        /// Removes a row from mnAlert.ScheduledFeedSchedule table which then stops sending a Messmo daily feed to this member.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        public void UnsubscribeFromMessmoDailyFeed(int memberID, int communityID)
        {
            try
            {
                Command command = new Command("mnAlert", "up_ScheduledFeedSchedule_Delete", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@ScheduledFeedTypeID", SqlDbType.Int, ParameterDirection.Input, (int)Matchnet.Member.ValueObjects.Enumerations.ScheduledFeedType.Messmo);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw (new BLException(string.Format("UnsubscribeFromMessmoDailyFeed() error (memberID: {0}, communityID: {1}", memberID, communityID), ex));
            }
        }
        #endregion

        private void updatePhotoSearch(PhotoUpdate[] update, CachedMember cachedMember, int memberid, int communityid)
        {
            try
            {
                Matchnet.PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.SavePhotos(update, cachedMember, memberid, communityid);

            }
            catch (Exception ex)
            { System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.MemberBL", "Error doing PhotoUpdate:" + ex.Message, System.Diagnostics.EventLogEntryType.Warning); }

        }

        private List<int> getSiteIDList(int memberID)
        {
            List<int> sites = new List<int>();

            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributes.GetAttribute("BrandInsertDate").ID);
            Brands brands = BrandConfigSA.Instance.GetBrands();

            CachedMember cachedMember = GetCachedMember("", null, memberID, false);
            for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
            {
                Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                if (cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
                {
                    Int32 siteID = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.SiteID;
                    if (!sites.Contains(siteID))
                    {
                        sites.Add(siteID);
                    }
                }
            }

            return sites;
        }

        public static bool HasPrivilegesBeenMigratedToUPSAccess(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = SettingsService.GetSettingFromSingleton("UPS_HAS_BEDROCK_PRIVILEGES_BEEN_MIGRATED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsUPSAccessEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = SettingsService.GetSettingFromSingleton("IS_UPS_ACCESS_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        private void LogMessage(string method, string message, int memberID)
        {
            RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL." + method,
                message, memberID);
        }

        private void LogException(string method, Exception ex, int memberID)
        {
            RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL." + method,
                ex, memberID);
        }

        public MemberAppDeviceCollection GetMemberAppDevices(int memberID)
        {
            SqlDataReader dataReader = null;
            MemberAppDeviceCollection memberAppDeviceCollection;
            try
            {
                try
                {
                    Command command = new Command("mnMember", "up_MemberAppDevice_List", memberID);
                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    dataReader = Client.Instance.ExecuteReader(command);
                }
                catch (Exception ex)
                {
                    throw new BLException("BL error while retrieving MemberAppDevice entries from the database", ex);
                }

                memberAppDeviceCollection = new MemberAppDeviceCollection();

                while (dataReader.Read())
                {
                    MemberAppDevice memberAppDevice = new MemberAppDevice()
                    {
                        MemberID = memberID,
                        AppID = Convert.ToInt32(dataReader["AppID"]),
                        DeviceID = dataReader["DeviceID"].ToString(),
                        BrandID = Convert.ToInt32(dataReader["BrandID"]),
                        DeviceData = dataReader["DeviceData"] == DBNull.Value ? string.Empty : dataReader["DeviceData"].ToString(),
                        PushNotificationsFlags = Convert.ToInt64(dataReader["PushNotificationsFlags"])
                    };
                    memberAppDeviceCollection.AppDevices.Add(memberAppDevice);
                }

                memberAppDeviceCollection.MemberID = memberID;

                MemberDTOCache.Insert(memberAppDeviceCollection);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return memberAppDeviceCollection;
        }

        public void RemoveMemberAppDevice(int memberID, int appID, string deviceID)
        {
            Command command = new Command("mnMember", "up_MemberAppDevice_Delete", memberID);
            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DeviceID", SqlDbType.VarChar, ParameterDirection.Input, deviceID);
            
            Client.Instance.ExecuteAsyncWrite(command);

            RemoveMemberAppDevicesFromDTOCache(memberID);
        }

        public void UpdateMemberAppDeviceNotificationsFlags(int memberID, int appID, string deviceID, long pushNotificationsFlags)
        {
            RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "UpdateMemberAppDeviceNotificationsFlags",
                string.Format("Entered for MemberID: : {0} DeviceID: {1} NotificationFlagValue: {2}", memberID, deviceID, pushNotificationsFlags),
                null);
            Command command = new Command("mnMember", "up_MemberAppDevice_UpdateNotificationsFlags", memberID);
            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DeviceID", SqlDbType.VarChar, ParameterDirection.Input, deviceID);
            command.AddParameter("@PushNotificationsFlags", SqlDbType.BigInt, ParameterDirection.Input, pushNotificationsFlags);

            Client.Instance.ExecuteAsyncWrite(command);

            RemoveMemberAppDevicesFromDTOCache(memberID);
        }

        public void AddMemberAppDevice(int memberID, int appID, string deviceID, int brandID, string deviceData, long pushNotificationsFlags)
        {
            RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "AddMemberAppDevice",
                string.Format("Entered for MemberID: : {0} DeviceID: {1} BrandID: {2} NotificationFlagValue: {3}", memberID, deviceID, brandID, pushNotificationsFlags),
                null);
            Command command = new Command("mnMember", "up_MemberAppDevice_Add", memberID);
            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DeviceID", SqlDbType.NVarChar, ParameterDirection.Input, deviceID);
            command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, brandID);
            command.AddParameter("@PushNotificationsFlags", SqlDbType.BigInt, ParameterDirection.Input, pushNotificationsFlags);
            command.AddParameter("@DeviceData", SqlDbType.NVarChar, ParameterDirection.Input, deviceData);

            Client.Instance.ExecuteAsyncWrite(command);
            RemoveMemberAppDevicesFromDTOCache(memberID);
        }


        public void DeleteMemberAppDevice(int memberID, int appID, string deviceID)
        {
            RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "DeleteMemberAppDevice",
                string.Format("Entered for MemberID: : {0} DeviceID: {1}", memberID, deviceID),
                null);
            var command = new Command("mnMember", "up_MemberAppDevice_Delete", memberID);
            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DeviceID", SqlDbType.NVarChar, ParameterDirection.Input, deviceID);

            Client.Instance.ExecuteAsyncWrite(command);
            RemoveMemberAppDevicesFromDTOCache(memberID);
        }



        private void RemoveMemberAppDevicesFromDTOCache(int memberID)
        {
            MemberDTOCache.RemoveWithoutGet(MemberAppDeviceCollection.GetCacheKey(memberID));
        }
    }
}
