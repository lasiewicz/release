using System;
using System.Collections;
using System.Data;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Exceptions;
using Matchnet.Data.Hydra;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Privilege;


namespace Matchnet.Member.BusinessLogic
{
	public class MemberPrivilegeBL
	{
		public static readonly MemberPrivilegeBL Instance = new MemberPrivilegeBL();

		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;

		private Matchnet.Caching.Cache _cache;

		private MemberPrivilegeBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID)
		{
			try
			{
				MemberPrivilegeCollection memberPrivilegeCollection = _cache.Get(MemberPrivilegeCollection.GetCacheKey(memberID)) as MemberPrivilegeCollection;

				if (memberPrivilegeCollection == null)
				{
					Command command = new Command("mnMember", "dbo.up_MemberPrivilege_List", memberID);
					command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					DataTable dt = Client.Instance.ExecuteDataTable(command);

					memberPrivilegeCollection = new MemberPrivilegeCollection(memberID);

					PrivilegeCollection privileges = GetPrivileges();

					for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
					{
						DataRow row = dt.Rows[rowNum];

						Int32 privilegeID = Convert.ToInt32(row["PrivilegeID"]);
						Privilege privilege = privileges.FindByID(privilegeID);

						if (privilege == null)
						{
							throw new Exception("Invalid privilegeID (" + privilegeID.ToString() + ")");
						}

						MemberPrivilege memberPrivilege = new MemberPrivilege(privilege,
							(PrivilegeState)Convert.ToInt32(row["PrivilegeState"]));
					
						memberPrivilegeCollection.Add(memberPrivilege);
					}

					CacheMemberPrivileges(memberPrivilegeCollection, true);
				}

				return memberPrivilegeCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving member privileges.", ex));
			}
		}


		public void CacheMemberPrivileges(MemberPrivilegeCollection memberPrivilegeCollection,
			bool replicate)
		{
			_cache.Add(memberPrivilegeCollection);
			if (replicate)
			{
				ReplicationRequested(memberPrivilegeCollection);
			}
		}


		public void SaveMemberPrivilege(Int32 memberID,
			Int32 privilegeID)
		{
			try
			{
				Privilege privilege = GetPrivileges().FindByID(privilegeID);

				if (privilege == null)
				{
					throw new Exception("Privilege " + privilegeID.ToString() + " not found.");
				}			
				
				MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID);

				Command command = new Command("mnMember", "dbo.up_MemberPrivilege_Save", memberID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
				Client.Instance.ExecuteAsyncWrite(command);

				memberPrivileges.Add(new MemberPrivilege(privilege, PrivilegeState.Individual));
				ReplicationRequested(memberPrivileges);		
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving member privilege.", ex));
			}
		}


		public void DeleteMemberPrivilege(int memberID, int privilegeID)
		{
			try
			{
				Privilege privilege = GetPrivileges().FindByID(privilegeID);

				if (privilege == null)
				{
					throw new Exception("Privilege " + privilegeID.ToString() + " not found.");
				}			

				MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID);

				Command command = new Command("mnMember", "dbo.up_MemberPrivilege_Delete", memberID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
				Client.Instance.ExecuteAsyncWrite(command);

				memberPrivileges.Revoke(privilegeID);
				ReplicationRequested(memberPrivileges);		
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when deleteing member privilege.", ex));
			}
		}


		public SecurityGroupMemberCollection GetSecurityGroupMembers(Int32 securityGroupID)
		{
			try
			{
				// TT 16080:  Caching has been removed because it is causing problems with the Admin app, which requires immediates
				// changes to show.  Later, perhaps we can add another parameter to this method to determine whether or not to use
				// the cache or not (ie. bool useCache).
				//SecurityGroupMemberCollection securityGroupMemberCollection = _cache.Get(SecurityGroupMemberCollection.GetCacheKey(securityGroupID)) as SecurityGroupMemberCollection;
				SecurityGroupMemberCollection securityGroupMemberCollection;

				if (true)	// Once ICacheable is implemented, "true" can be replaced with "securityGroupMemberCollection == null".
				{
					securityGroupMemberCollection = new SecurityGroupMemberCollection(securityGroupID);

					LogicalDatabase memberDB = ConnectionDispenser.Instance.GetLogicalDatabase("mnMember");
					//Get list of SecurityGroup Members from all DB partitions
					for (short i = 0; i < memberDB.PartitionCount; i++)
					{
						Command command = new Command("mnMember", "dbo.up_SecurityGroupMember_List", i);
						command.AddParameter("@SecurityGroupID", SqlDbType.Int, ParameterDirection.Input, securityGroupID);
						DataTable dt = Client.Instance.ExecuteDataTable(command);

						for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
						{
							DataRow row = dt.Rows[rowNum];

							securityGroupMemberCollection.Add(Convert.ToInt32(row["MemberID"]));
						}
					}
					
					//todo: _cache.Insert(securityGroupMemberCollection);
				}

				return securityGroupMemberCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving security Group members.", ex));
			}
		}


		public void SaveSecurityGroupMember(Int32 memberID, Int32 securityGroupID)
		{
			try
			{
				Command command = new Command("mnMember", "dbo.up_SecurityGroupMember_Save", memberID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@SecurityGroupID", SqlDbType.Int, ParameterDirection.Input, securityGroupID);
				Client.Instance.ExecuteAsyncWrite(command);
				//todo: cache & replicate
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving security Group member.", ex));
			}
		}


		public void DeleteSecurityGroupMember(Int32 memberID, Int32 securityGroupID)
		{
			try
			{
				Command command = new Command("mnMember", "dbo.up_SecurityGroupMember_Delete", memberID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@SecurityGroupID", SqlDbType.Int, ParameterDirection.Input, securityGroupID);
				Client.Instance.ExecuteAsyncWrite(command);
				//todo: cache & replicate
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when deleting security Group member.", ex));
			}
		}


		public SecurityGroupCollection GetSecurityGroups()
		{
			try
			{
				SecurityGroupCollection securityGroupCollection = _cache.Get(SecurityGroupCollection.CACHE_KEY) as SecurityGroupCollection;

				if (securityGroupCollection == null)
				{
					Command command = new Command("mnSystem", "dbo.up_SecurityGroup_List", 0);
					DataTable dt = Client.Instance.ExecuteDataTable(command);

					securityGroupCollection = new SecurityGroupCollection();

					for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
					{
						DataRow row = dt.Rows[rowNum];

						SecurityGroup securityGroup = new SecurityGroup(Convert.ToInt32(row["SecurityGroupID"]),
							row["Description"].ToString());
						securityGroupCollection.Add(securityGroup);
					}

					_cache.Insert(securityGroupCollection);
				}

				return securityGroupCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving security Group.", ex));
			}
		}


		public PrivilegeCollection GetPrivileges()
		{
			try
			{
				PrivilegeCollection privilegeCollection = _cache.Get(PrivilegeCollection.CACHE_KEY) as PrivilegeCollection;

				if (privilegeCollection == null)
				{
					Command command = new Command("mnSystem", "dbo.up_Privilege_List", 0);
					DataTable dt = Client.Instance.ExecuteDataTable(command);

					privilegeCollection = new PrivilegeCollection();

					for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
					{
						DataRow row = dt.Rows[rowNum];
						Privilege privilege = new Privilege(Convert.ToInt32(row["PrivilegeID"]),
							row["Description"].ToString());
						privilegeCollection.Add(privilege);
					}

					_cache.Insert(privilegeCollection);
				}

				return privilegeCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving privileges.", ex));
			}
		}

		/// <summary>
		/// Calls up_MemberPrivilege_ListMember which selects all the memberIDs for this
		/// privilegeID.
		/// </summary>
		/// <param name="privilegeID">The PrivilegeID to select members by</param>
		/// <returns>An array list of all the memberIDs</returns>
		public ArrayList GetMembersByPrivilege(Int32 privilegeID)
		{
			try
			{
				ArrayList memberIDList = new ArrayList();//todo: create a MemberCollection class that implements ICacheable

				if (true)	// Once ICacheable is implemented, "true" can be replaced with "memberIDList == null".
				{
					LogicalDatabase memberDB = ConnectionDispenser.Instance.GetLogicalDatabase("mnMember");
					memberIDList = new ArrayList();
					
					//Get list of Members from all DB partitions
					for (short i = 0; i < memberDB.PartitionCount; i++)
					{
						Command command = new Command("mnMember", "dbo.up_MemberPrivilege_ListMember", i);
						command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
						DataTable dt = Client.Instance.ExecuteDataTable(command);

						for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
						{
							DataRow row = dt.Rows[rowNum];

							int memberID = Conversion.CInt(row["MemberID"]);
							memberIDList.Add(memberID);
						}
					}

					//todo: _cache.Insert(memberIDList);
				}

				return memberIDList;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving Members by Privilege.", ex));
			}
		}

		/// <summary>
		/// Returns all SecurityGroups that have a particular Privilege.
		/// </summary>
		/// <param name="privilegeID">The PrivilegeID of the Privilege to search for.</param>
		/// <returns>A SecurityGroupCollection that contains all the SecurityGroups with this Privilege.</returns>
		public SecurityGroupCollection GetGroupsByPrivilege(Int32 privilegeID)
		{
			try
			{
				SecurityGroupCollection securityGroupCollection = _cache.Get(SecurityGroupCollection.GetCacheKey(privilegeID)) as SecurityGroupCollection;

				if (securityGroupCollection == null)
				{
					Command command = new Command("mnSystem", "dbo.up_SecurityGroupPrivilege_ListGroup", 0);
					command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
					DataTable dt = Client.Instance.ExecuteDataTable(command);

					securityGroupCollection = new SecurityGroupCollection(privilegeID);

					for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
					{
						DataRow row = dt.Rows[rowNum];

						int securityGroupID = Conversion.CInt(row["SecurityGroupID"]);
						SecurityGroup group = GetSecurityGroups()[securityGroupID];
						securityGroupCollection.Add(group);
					}

					_cache.Insert(securityGroupCollection);
				}

				return securityGroupCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving Security Groups by Privilege.", ex));
			}
		}

		/// <summary>
		/// Adds the Privilege to the Privilege table by calling up_Privilege_Save
		/// </summary>
		/// <param name="description">The Description for this Privilege</param>
		/// <returns>The PrivilegeID of the newly created Privilege</returns>
		public Int32 AddPrivilege(string description)
		{
			try
			{
				Int32 privilegeID = KeySA.Instance.GetKey("PrivilegeID");
				SavePrivilege(privilegeID, description);
				return privilegeID;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when adding Privilege with description:"+description, ex));
			}
		}

		/// <summary>
		/// Saves the privileges for a specified privilegeID by calling up_Privilege_Save
		/// </summary>
		/// <param name="privilegeID">The privilegeId to save to</param>
		/// <param name="description">The new description</param>
		public void SavePrivilege(Int32 privilegeID, string description)
		{
			try
			{				
				Command command = new Command("mnSystem", "dbo.up_Privilege_Save", 0);
				command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
				command.AddParameter("@Description", SqlDbType.VarChar, ParameterDirection.Input, description);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving Privilege with description:"+description, ex));
			}
		}


		/// <summary>
		/// Adds a SecurityGroup
		/// </summary>
		/// <param name="description">The Description for this SecurityGroup</param>
		/// <returns>The SecurityGroupID of the newly created SecurityGroup</returns>
		public Int32 AddGroup(string description)
		{
			try
			{
				Int32 privilegeID = KeySA.Instance.GetKey("PrivilegeID");
				SavePrivilege(privilegeID, description);
				return privilegeID;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when adding SecurityGroup with description:"+description, ex));
			}
		}

		/// <summary>
		/// Saves a SecurityGroup.
		/// </summary>
		/// <param name="securityGroupID">The securityGroupID to save to</param>
		/// <param name="description">The new description</param>
		public void SaveGroup(Int32 securityGroupID, string description)
		{
			try
			{
				Command command = new Command("mnSystem", "dbo.up_SecurityGroup_Save", 0);
				command.AddParameter("@SecurityGroupID", SqlDbType.Int, ParameterDirection.Input, securityGroupID);
				command.AddParameter("@Description", SqlDbType.VarChar, ParameterDirection.Input, description);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving Privilege with description:"+description, ex));
			}
		}


		/// <summary>
		/// Grants a Privilege to a SecurityGroup by calling up_SecurityGroupPrivilege_Save.
		/// </summary>
		/// <param name="securityGroupID">The SecurityGroupID of the SecurityGroup to which to grant the Privilege.</param>
		/// <param name="privilegeID">The Privilege of the Privilege to be granted.</param>
		public void SaveGroupPrivilege(Int32 securityGroupID,Int32 privilegeID)
		{
			try
			{
				Command command = new Command("mnSystem", "dbo.up_SecurityGroupPrivilege_Save", 0);
				command.AddParameter("@SecurityGroupID", SqlDbType.Int, ParameterDirection.Input, securityGroupID);
				command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving the Group Privilege for SecurityGroupID:"+securityGroupID, ex));
			}
		}

		/// <summary>
		/// Deletes a Privilege for a group
		/// </summary>
		/// <param name="securityGroupID">The GroupID</param>
		/// <param name="privilegeID">The Privilege ID</param>
		public void DeleteGroupPrivilege(Int32 securityGroupID, Int32 privilegeID)
		{
			try
			{				
				Command command = new Command("mnSystem", "dbo.up_SecurityGroupPrivilege_Delete", 0);
				command.AddParameter("@SecurityGroupID", SqlDbType.Int, ParameterDirection.Input, securityGroupID);
				command.AddParameter("@PrivilegeID", SqlDbType.Int, ParameterDirection.Input, privilegeID);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				throw(new BLException(string.Format("BL error occured when saving the Group Privilege with SecurityGroupID {0} and PrivilegeID {1}:", securityGroupID, privilegeID), ex));
			}
		}
	}
}
