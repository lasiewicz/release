using System;
using System.Data;
using System.Diagnostics;
using System.Web.Caching;
using System.Threading;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.AttributeMetadata;


namespace Matchnet.Member.BusinessLogic
{
	public class AttributeMetadataBL
	{
		public readonly static AttributeMetadataBL Instance = new AttributeMetadataBL();

		private Matchnet.Caching.Cache _cache;

		private AttributeMetadataBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}

		public Attributes GetAttributes()
		{
			try
			{
				Attributes attributes = _cache.Get(Attributes.CACHE_KEY) as Attributes;

				if (attributes == null)
				{
					Command command = new Command("mnSystem", "up_AttributeMetadata_List", 0);
					DataSet ds = Client.Instance.ExecuteDataSet(command);
					DataTable dtGroup = ds.Tables[0];
					DataTable dtAttributeGroup = ds.Tables[1];
					DataTable dtAttribute = ds.Tables[2];

					attributes = new Attributes();

					foreach (DataRow row in dtGroup.Rows)
					{
						attributes.AddGroupScope(Convert.ToInt32(row["GroupID"]), 
							(ScopeType)Enum.Parse(typeof(ScopeType), row["ScopeID"].ToString()));
					}

					foreach (DataRow row in dtAttributeGroup.Rows)
					{
						Int16 length = 0;
						string defaultValue = Constants.NULL_STRING;

						if (row["Length"] != DBNull.Value)
						{
							length = Convert.ToInt16(row["Length"]);
						}

						if (row["DefaultValue"] != DBNull.Value)
						{
							defaultValue = row["DefaultValue"].ToString();
						}

						attributes.AddAttributeGroup(Convert.ToInt32(row["AttributeGroupID"]),
							Convert.ToInt32(row["GroupID"]),
							Convert.ToInt32(row["AttributeID"]),
							(StatusType)Enum.Parse(typeof(StatusType), row["AttributeStatusMask"].ToString()),
							length,
							Convert.ToByte(row["EncryptFlag"]) == 1 ? true : false,
							defaultValue);
					}

					foreach (DataRow row in dtAttribute.Rows)
					{
						attributes.AddAttribute(Convert.ToInt32(row["AttributeID"]),
							(ScopeType)Enum.Parse(typeof(ScopeType), row["ScopeID"].ToString()),
							row["AttributeName"].ToString(),
							(DataType)Enum.Parse(typeof(DataType), row["DataType"].ToString(), true));
					}

					_cache.Add(attributes);
				}

				return attributes;
			}
			catch(StoredProcedureException sqlEx)
			{
				throw(new BLException("SQL error occurred while retrieving attribute metadata", sqlEx));
			}
			catch(Exception ex)
			{
				throw(new BLException("Error occurred while retrieving attribute metadata.", ex));
			}
		}
	}
}
