using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.Mail;
using System.Security.Cryptography;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.Member.BusinessLogic
{

	/// <summary>
	/// Provides a means of managing member photos.
	/// </summary>
	public sealed class PhotoBL
	{
		public readonly static PhotoBL Instance = new PhotoBL();

		private const int MAX_PHOTO_FILE_BYTES = 5120000;
		private const int MAX_PHOTOS_PER_ALBUM = 100;

		private PhotoBL()
		{
		}
		
		
		/// <summary>
		/// Uploads a new member photo to the file system and stores the member photo meta data to the database.
		/// </summary>
		/// <param name="communityID">The identity of the community that this photo exists in.</param>
		/// <param name="memberID">The identity of the member that this photo belongs to.</param>
		/// <param name="listOrder">The list order of member photo on member photo upload page.</param>
		/// <param name="fileBytes">The uploaded file in byte array.</param>
		/// <param name="fileExtension">The file extension of the member photo that is being uploaded (such as "jpg")</param>
		/// <param name="memberPhotoID">Required when overwriting existing memberphoto</param>
		/// <returns>Member Photo object</returns>
		public Photo UploadMemberPhoto(Int32 communityID,
			Int32 memberID,
			byte listOrder,
			bool isPrivate,
			Byte[] fileBytes,
			string fileExtension,
			Int32 memberPhotoID)
		{
			string filePath = Constants.NULL_STRING;
			int fileID = Constants.NULL_INT;
			//Photo photo = new Photo();
			Photo photo = null;
			int fileLength = (int)fileBytes.Length;

			FileInfo info = null;

			// When file is empty, just update member photo metadata(list order, private photo)
			if (fileLength == 0)
			{
				/*
				photo.SetMemberPhotoID
					(saveMetaData(communityID, memberID, fileID, null, listOrder, isPrivate, memberPhotoID));
				photo.SetCommunityID(communityID);
				photo.SetFileID(fileID);
				photo.ListOrder = listOrder;
				photo.IsPrivate = isPrivate;
				*/
				return photo;
			}

			try
			{
				fileID = KeySA.Instance.GetKey("FileID");
			}
			catch(Exception ex)
			{
				throw(new BLException("Could not get new FileID when attempting to upload photo.", ex));
			}

			try
			{
				if (fileID > 0)
				{
					string webPath;

					// Assign a file path
					filePath = assignFilePath(fileID, ".jpg", out webPath);

					// Create directory (if it doesn't exist)
					info = new FileInfo(filePath);
					Directory.CreateDirectory(info.Directory.ToString());
					
					if (fileLength > MAX_PHOTO_FILE_BYTES)
					{
						throw(new BLException("Photo file exceeds maximum allowable size.", new Exception("Photo file exceeds maximum allowable size")));
					}
			
					// File format check
					System.Drawing.Image image = null;					
					try
					{
						MemoryStream stream = new MemoryStream(fileBytes);
						image = Image.FromStream(stream, true);
					}
					catch(Exception ex)
					{
						throw(new BLException("Invalid photo file type or corrupt photo file, could not convert from MemoryStream to System.Drawing.Image.", ex));
					}

					image.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg); 	
					image.Dispose();
				
					// Save file info to database
					/*
					photo.SetMemberPhotoID(saveMetaData(communityID, memberID, fileID, webPath, listOrder, isPrivate, memberPhotoID));
					photo.SetFileID(fileID);
					photo.IsPrivate = isPrivate;
					photo.ListOrder = listOrder;
					photo.SetFileWebPath(webPath);
					*/

				}
				return photo;
			}
			catch(SqlException sqlEx)
			{
				throw(new BLException("SQL error occured when uploading the member photo to database.", sqlEx));
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when uploading the member photo.", ex));
			}
		}

		/// <summary>
		/// Deletes the member photo.
		/// </summary>
		/// <param name="memberID">The identity of the member that this photo belongs to.</param>
		/// <param name="memberPhotoID">The identity of the member photo.</param>
		public void DeleteMemberPhoto(int memberID, int memberPhotoID)
		{
			try
			{
				Command command = new Command("mnMember", "up_MemberPhoto_Delete", memberID);
				command.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(StoredProcedureException sqlEx)
			{
				throw(new BLException("SQL error occured when deleting the member photo from the database.", sqlEx));
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when deleting the member photo.", ex));
			}
		}


		/// <summary>
		/// Creates the metadata that describes the photo.
		/// </summary>
		/// <param name="communityID">The identity of the community that the photo belongs in.</param>
		/// <param name="memberID">The identity of the member that the photo belongs to.</param>
		/// <param name="fileID">File ID of existing photo</param>
		/// <param name="listOrder">Order listed on photo upload page</param>
		/// <param name="isPrivate">Private status</param>
		/// <param name="memberPhotoID">ID of existing member photo</param>
		/// <returns>MemberPhotoID</returns>
		private int saveMetaData(Int32 communityID,
			Int32 memberID,
			Int32 fileID,
			string fileWebPath,
			byte listOrder,
			bool isPrivate,
			Int32 memberPhotoID)
		{
			try
			{
				if (memberPhotoID == Constants.NULL_INT)
				{
					memberPhotoID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("MemberPhotoID");
				}

				Command command = new Command("mnMember", "up_MemberPhoto_Save", memberID);
				command.AddParameter("@MemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				if (fileID != Constants.NULL_INT)
					command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
				command.AddParameter("@AlbumID", SqlDbType.Int, ParameterDirection.Input, null);
				command.AddParameter("@ListOrder", SqlDbType.Int, ParameterDirection.Input, listOrder);
				command.AddParameter("@MaximumPhotos", SqlDbType.Int, ParameterDirection.Input, MAX_PHOTOS_PER_ALBUM);
				if (memberPhotoID != Constants.NULL_INT)
				{
					command.AddParameter("@OriginalMemberPhotoID", SqlDbType.Int, ParameterDirection.Input, memberPhotoID);
				}
				command.AddParameter("@ThumbFileID", SqlDbType.Int, ParameterDirection.Input, System.DBNull.Value);
				if (fileWebPath != null)
				{
					command.AddParameter("@FileWebPath", SqlDbType.VarChar, ParameterDirection.Input, fileWebPath);
				}
				command.AddParameter("@PrivateFlag", SqlDbType.Bit, ParameterDirection.Input, isPrivate);
				
				Client.Instance.ExecuteAsyncWrite(command);

				return memberPhotoID;
			}
			catch(StoredProcedureException sqlEx)
			{
				throw(new BLException("SQL error occured when creating the member photo's metadata in the database.", sqlEx));
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when creating the member photo's metadata.", ex));
			}
		}



		/// <summary>
		/// Assigns a path to a given photo file.  This is facilitates the mapping of files in the file system to the queryable meta data.
		/// </summary>
		/// <param name="fileID">The identifier for the photo file.</param>
		/// <param name="fileExtension">The extension for the photo file.</param>
		/// <returns>The file system path where the photo should be stored.</returns>
		private string assignFilePath(int fileID,
			string fileExtension,
			out string webPath)
		{
			try
			{
				FileTransaction ft = new FileTransaction();
				ft.SaveFile(ConnectionDispenser.Instance.GetLogicalDatabase("mnFile").GetPartition(fileID).PhysicalDatabases,
					fileID,
					fileExtension);

				Command command = new Command("mnFile", "up_File_List", fileID);
				command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);				
				DataTable dt = Client.Instance.ExecuteDataTable(command);
				webPath = dt.Rows[0]["WebPath"].ToString();
				return dt.Rows[0]["Path"].ToString();
			}
			catch(StoredProcedureException sqlEx)
			{
				throw(new BLException("SQL error occured when inserting the member photo's file path in the database.", sqlEx));
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when inserting the member photo's file path.", ex));
			}
		}
	}
}
