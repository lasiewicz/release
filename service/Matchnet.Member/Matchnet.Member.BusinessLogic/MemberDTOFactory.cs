﻿using System;
using System.Collections;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Spark.Common.AccessService;
using Attribute = Matchnet.Content.ValueObjects.AttributeMetadata.Attribute;

namespace Matchnet.Member.BusinessLogic
{
    public class MemberDTOFactory
    {
        // INT attr names
        public const string ATTR_NAME_GLOBALSTATUSMASK = "GLOBALSTATUSMASK";
        public const string ATTR_NAME_SELFSUSPENDEDFLAG = "SELFSUSPENDEDFLAG";
        public const string ATTR_NAME_GENDERMASK = "GENDERMASK";
        public const string ATTR_NAME_HIGHLIGHTEDFLAG = "HIGHLIGHTEDFLAG";
        public const string ATTR_NAME_REGIONID = "REGIONID";
        public const string ATTR_NAME_HIDEMASK = "HIDEMASK";
        public const string ATTR_NAME_ENTERTAINMENTLOCATION = "ENTERTAINMENTLOCATION";
        public const string ATTR_NAME_LEISUREACTIVITY = "LEISUREACTIVITY";
        public const string ATTR_NAME_CUISINE = "CUISINE";
        public const string ATTR_NAME_MUSIC = "MUSIC";
        public const string ATTR_NAME_READING = "READING";
        public const string ATTR_NAME_PHYSICALACTIVITY = "PHYSICALACTIVITY";
        public const string ATTR_NAME_PETS = "PETS";
        public const string ATTR_NAME_MESSMOALERTMASK = "MESSMOALERTMASK";
        public const string ATTR_NAME_MESSMOCAPABLE = "MESSMOCAPABLE";
        public const string ATTR_NAME_RELATIONSHIPMASK = "RELATIONSHIPMASK";
        public const string ATTR_NAME_HEIGHT = "HEIGHT";
        public const string ATTR_NAME_RELATIONSHIPSTATUS = "RELATIONSHIPSTATUS";
        public const string ATTR_NAME_CHILDRENCOUNT = "CHILDRENCOUNT";
        public const string ATTR_NAME_SMOKINGHABITS = "SMOKINGHABITS";
        public const string ATTR_NAME_EDUCATIONLEVEL = "EDUCATIONLEVEL";
        public const string ATTR_NAME_ALERTEMAILMASK = "ALERTEMAILMASK";
        public const string ATTR_NAME_SMSALERTPREFERENCE = "SMSALERTPREFERENCE";
        public const string ATTR_NAME_SMSPHONEISVALIDATED = "SMSPHONEISVALIDATED";
        public const string ATTR_NAME_RELIGION = "RELIGION";
        public const string ATTR_NAME_JDATERELIGION = "JDATERELIGION";
        public const string ATTR_NAME_ETHNICITY = "ETHNICITY";
        public const string ATTR_NAME_JDATEETHNICITY = "JDATEETHNICITY";
        public const string ATTR_NAME_MARITALSTATUS = "MARITALSTATUS";
        public const string ATTR_NAME_CUSTODY = "CUSTODY";

        // TEXT attr names
        public const string ATTR_NAME_ABOUTME = "ABOUTME";
        public const string ATTR_NAME_USERNAME = "USERNAME";
        public const string ATTR_NAME_COLORCODEPRIMARYCOLOR = "COLORCODEPRIMARYCOLOR";
        public const string ATTR_NAME_COLORCODEHIDDEN = "COLORCODEHIDDEN";
        public const string ATTR_NAME_OCCUPATIONDESCRIPTION = "OCCUPATIONDESCRIPTION";
        public const string ATTR_NAME_MESSMOSUBSCRIBERID = "MESSMOSUBSCRIBERID";
        public const string ATTR_NAME_SITEFIRSTNAME = "SITEFIRSTNAME";
        public const string ATTR_NAME_SITELASTNAME = "SITELASTNAME";
        public const string ATTR_NAME_SMSPHONE = "SMSPHONE";

        // DATE attr names
        public const string ATTR_NAME_BIRTHDATE = "BIRTHDATE";
        public const string ATTR_NAME_BRANDINSERTDATE = "BRANDINSERTDATE";
        public const string ATTR_NAME_LASTUPDATED = "LASTUPDATED";
        public const string ATTR_NAME_PREMIUMAUTHENTICATEDEXPIRATIONDATE = "PREMIUMAUTHENTICATEDEXPIRATIONDATE";
        public const string ATTR_NAME_BRANDLASTLOGONDATE = "BRANDLASTLOGONDATE";
        public const string ATTR_NAME_MESSMOFULLPROFILESENDDATE = "MESSMOFULLPROFILESENDDATE";

        public static readonly MemberDTOFactory Instance = new MemberDTOFactory();
        private static ISettingsSA _settingsService = null;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private Attributes _attributes;

        private Attributes AttributeMetaData
        {
            get
            {
                if (_attributes == null)
                {
                    _attributes = AttributeMetadataSA.Instance.GetAttributes();
                }

                return _attributes;
            }
        }

        private ArrayList GetAttributeGroupIds(string attributeName)
        {
            ArrayList attributeGroupIDs = null;
            try
            {
                Attribute attribute = AttributeMetaData.GetAttribute(attributeName);
                attributeGroupIDs = AttributeMetaData.GetAttributeGroupIDs(attribute.ID);
                if (null == attributeGroupIDs || attributeGroupIDs.Count == 0)
                {
                    throw new Exception("No attribute group ids for attribute:"+attributeName);
                }
            }
            catch (Exception e)
            {
                Spark.Logging.RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT, "MemberDTOFactory", e);              
            }
            return attributeGroupIDs;
        }

        public ArrayList GetCommunityIDList(CachedMember cachedMember)
        {
            ArrayList communities = new ArrayList();

            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributes.GetAttribute(ATTR_NAME_BRANDINSERTDATE).ID);
            Brands brands = BrandConfigSA.Instance.GetBrands();

            for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
            {
                Int32 attributeGroupID = (Int32)attributeGroupIDs[num];

                if (cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
                {
                    Int32 communityID = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.Community.CommunityID;
                    if (!communities.Contains(communityID))
                    {
                        communities.Add(communityID);
                    }
                }
            }

            return communities;
        }

        private ArrayList GetSiteIDList(CachedMember cachedMember)
        {
            ArrayList sites = new ArrayList();

            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributes.GetAttribute(ATTR_NAME_BRANDINSERTDATE).ID);
            Brands brands = BrandConfigSA.Instance.GetBrands();

            for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
            {
                Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                if (cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
                {
                    Int32 siteID = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.SiteID;
                    if (!sites.Contains(siteID))
                    {
                        sites.Add(siteID);
                    }
                }
            }
            return sites;
        }


        private MemberDTOFactory(){}


        public IMemberDTO GetIMemberDTO(CachedMember cachedMember, CachedMemberAccess cachedMemberAccess, MemberType type)
        {
            IMemberDTO memberDTO = null;


            if (null == memberDTO)
            {
                MemberDTO memberDTOObj = new MemberDTO(type);
                memberDTO = memberDTOObj;
                memberDTOObj.MemberID = cachedMember.MemberID;
                memberDTOObj.EmailAddress = cachedMember.EmailAddress;
                memberDTOObj.AllBrands = BrandConfigSA.Instance.GetBrands();
                memberDTOObj.AttributeMetaData = AttributeMetaData;
                memberDTOObj.BrandLastLogonDateAttributeGroupIDs = GetAttributeGroupIds(ATTR_NAME_BRANDLASTLOGONDATE);
                memberDTOObj.SiteIDs = GetSiteIDList(cachedMember);
                memberDTOObj.CommunityIDs = GetCommunityIDList(cachedMember);
                int cacheTTLInSeconds = 3600;
                try
                {
                    string cacheTTLStr = SettingsService.GetSettingFromSingleton(string.Format("MEMBER_DTO_{0}_CACHE_TTL", type.ToString().ToUpper()));
                    cacheTTLInSeconds = Int32.Parse(cacheTTLStr);
                }
                catch (Exception e)
                {
                    Spark.Logging.RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT, "MemberDTOFactory", e);
                }
                memberDTOObj.CacheTTLSeconds = cacheTTLInSeconds;

                if (type == MemberType.Search)
                {
                    memberDTOObj.PhotoCollection = cachedMember.Photos;
                    if (null != cachedMemberAccess)
                    {
                        foreach (AccessPrivilege accessPrivilege in cachedMemberAccess.AccessPrivilegeList)
                        {
                            if (accessPrivilege.UnifiedPrivilegeType == PrivilegeType.HighlightedProfile
                                || accessPrivilege.UnifiedPrivilegeType == PrivilegeType.BasicSubscription)
                            {
                                memberDTOObj.AddUnifiedAccessPrivilege(accessPrivilege);
                            }
                        }
                    }

                    //set up text values
                    Hashtable attributesText = cachedMember.AttributesText;
                    foreach (int languageID in attributesText.Keys)
                    {
                        foreach (int attributeGroupID in ((Hashtable) attributesText[languageID]).Keys)
                        {
                            if (
                                GetAttributeGroupIds(ATTR_NAME_USERNAME).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_ABOUTME).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_COLORCODEPRIMARYCOLOR).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_COLORCODEHIDDEN).Contains(attributeGroupID)
                                )
                            {
                                TextValue tv = ((Hashtable) attributesText[languageID])[attributeGroupID] as TextValue;
                                memberDTOObj.SetAttributeText(attributeGroupID, languageID, tv.Text, tv.TextStatus);
                            }
                        }
                    }

                    //set up int values
                    Hashtable attributesInt = cachedMember.AttributesInt;
                    foreach (int attributeGroupID in attributesInt.Keys)
                    {
                        if (GetAttributeGroupIds(ATTR_NAME_GLOBALSTATUSMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_SELFSUSPENDEDFLAG).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_GENDERMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_HIGHLIGHTEDFLAG).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_REGIONID).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_HIDEMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_ENTERTAINMENTLOCATION).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_EDUCATIONLEVEL).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_LEISUREACTIVITY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_CUISINE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_MUSIC).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_READING).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_PHYSICALACTIVITY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_PETS).Contains(attributeGroupID))
                        {
                            memberDTOObj.SetAttributeInt(attributeGroupID, (int) attributesInt[attributeGroupID]);
                        }
                    }

                    //set up date values
                    Hashtable attributesDate = cachedMember.AttributesDate;
                    foreach (int attributeGroupID in attributesDate.Keys)
                    {
                        if (GetAttributeGroupIds(ATTR_NAME_BIRTHDATE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_BRANDINSERTDATE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_LASTUPDATED).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_PREMIUMAUTHENTICATEDEXPIRATIONDATE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_BRANDLASTLOGONDATE).Contains(attributeGroupID))
                        {
                            memberDTOObj.SetAttributeDate(attributeGroupID, (DateTime) attributesDate[attributeGroupID]);
                        }
                    }
                }
                else if (type == MemberType.MatchMail)
                {
                    memberDTOObj.PhotoCollection = cachedMember.Photos;
                    if (null != cachedMemberAccess)
                    {
                        foreach (AccessPrivilege accessPrivilege in cachedMemberAccess.AccessPrivilegeList)
                        {
                            if (accessPrivilege.UnifiedPrivilegeType == PrivilegeType.BasicSubscription)
                            {
                                memberDTOObj.AddUnifiedAccessPrivilege(accessPrivilege);
                            }
                        }
                    }

                    // add unsupported attributes
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_MESSMOSUBSCRIBERID);
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_SMSPHONE);
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_MESSMOALERTMASK);
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_MESSMOCAPABLE);
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_SMSALERTPREFERENCE);
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_SMSPHONEISVALIDATED);
                    memberDTOObj.AddUnsupportedAttribute(ATTR_NAME_MESSMOFULLPROFILESENDDATE);

                    //set up text values
                    Hashtable attributesText = cachedMember.AttributesText;
                    foreach (int languageID in attributesText.Keys)
                    {
                        foreach (int attributeGroupID in ((Hashtable)attributesText[languageID]).Keys)
                        {
                            if (
                                GetAttributeGroupIds(ATTR_NAME_USERNAME).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_ABOUTME).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_OCCUPATIONDESCRIPTION).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_SITEFIRSTNAME).Contains(attributeGroupID)
                                || GetAttributeGroupIds(ATTR_NAME_SITELASTNAME).Contains(attributeGroupID)
                                )
                            {
                                TextValue tv = ((Hashtable)attributesText[languageID])[attributeGroupID] as TextValue;
                                memberDTOObj.SetAttributeText(attributeGroupID, languageID, tv.Text, tv.TextStatus);
                            }
                        }
                    }

                    //set up int values
                    Hashtable attributesInt = cachedMember.AttributesInt;
                    foreach (int attributeGroupID in attributesInt.Keys)
                    {
                        if (GetAttributeGroupIds(ATTR_NAME_GLOBALSTATUSMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_SELFSUSPENDEDFLAG).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_GENDERMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_REGIONID).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_HIDEMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_ENTERTAINMENTLOCATION).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_LEISUREACTIVITY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_CUISINE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_MUSIC).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_READING).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_PHYSICALACTIVITY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_RELATIONSHIPMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_HEIGHT).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_RELATIONSHIPSTATUS).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_CHILDRENCOUNT).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_SMOKINGHABITS).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_EDUCATIONLEVEL).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_ALERTEMAILMASK).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_RELIGION).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_JDATERELIGION).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_ETHNICITY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_JDATEETHNICITY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_MARITALSTATUS).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_CUSTODY).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_PETS).Contains(attributeGroupID))
                        {
                            memberDTOObj.SetAttributeInt(attributeGroupID, (int)attributesInt[attributeGroupID]);
                        }
                    }

                    //set up date values
                    Hashtable attributesDate = cachedMember.AttributesDate;
                    foreach (int attributeGroupID in attributesDate.Keys)
                    {
                        if (GetAttributeGroupIds(ATTR_NAME_BIRTHDATE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_BRANDINSERTDATE).Contains(attributeGroupID)
                            || GetAttributeGroupIds(ATTR_NAME_BRANDLASTLOGONDATE).Contains(attributeGroupID))
                        {
                            memberDTOObj.SetAttributeDate(attributeGroupID, (DateTime)attributesDate[attributeGroupID]);
                        }
                    }                    
                }
            }
            return memberDTO;
        }

    }
}
