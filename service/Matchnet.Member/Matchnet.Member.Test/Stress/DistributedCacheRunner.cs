﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using NUnit.Framework;

namespace Matchnet.Member.Test.Stress
{
    class DistributedCacheRunner : IStressTestable
    {
        private const int MEMBER_DTO_LIST_SIZE = 1000;
        private ArrayList _memberIds = new ArrayList();
        private MemberType _memberType = MemberType.Full;
        private ReaderWriterLock _externalLock = null;

        private bool _runSucceeded = false;

        public bool RunSuceeded
        {
            get { return _runSucceeded; }
        }

        public DistributedCacheRunner(MemberType _type)
        {
            _externalLock = new ReaderWriterLock();
            _memberType = _type;
        }

        #region IStressTestable Members

        public void Prepare()
        {
            Random r = new Random();
            int length = TestUtils.ExistingDevMemberIds.Length;
            for (int i = 0; i < MEMBER_DTO_LIST_SIZE; i++)
            {
                int idx = r.Next(length - 1);
                _memberIds.Add(TestUtils.ExistingDevMemberIds[idx]);
            }
        }

        public void Run()
        {
            try
            {
                ArrayList memberDtOs = MemberSA.Instance.GetMemberDTOs(_memberIds, _memberType);
                foreach (IMemberDTO memberDto in memberDtOs)
                {
                    Assert.IsNotNull(memberDto.GetLastLogonDate(3));
                    if (memberDto is MemberDTO)
                    {
                        ((MemberDTO)memberDto).Dispose();
                    }
                }
                this._runSucceeded = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //System.Diagnostics.EventLog.WriteEntry("NUNIT", e.Message, System.Diagnostics.EventLogEntryType.Error);
                throw e;
            }
        }

        #endregion
    }
}
