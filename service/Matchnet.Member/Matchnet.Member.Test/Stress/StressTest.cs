﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.MemberDTO;
using NUnit.Framework;

namespace Matchnet.Member.Test.Stress
{
    [TestFixture]
    public class StressTest
    {

        [TestFixtureSetUp]
        public void StartUp()
        {
            MemberSA.Instance.NUnit = true;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            MemberSA.SettingsService = null;
            MemberSA.Instance.NUnit = false;
        }

        [Test]
        public void TestStressOnDistributedCache()
        {
            Random r = new Random();
            List<IStressTestable> runners = new List<IStressTestable>();
            int length = 200;
            for (int i = 0; i < length; i++)
            {
                MemberType _type = (i%3==0) ? MemberType.MatchMail : MemberType.Search;               
                runners.Add(new DistributedCacheRunner(_type));
            }

            using (StressTester tester = new StressTester(runners))
            {
                tester.RunConcurrently();
            }

            foreach (IStressTestable testable in runners)
            {                
                Assert.IsTrue(((DistributedCacheRunner)testable).RunSuceeded);
            }
        }
    }
}
