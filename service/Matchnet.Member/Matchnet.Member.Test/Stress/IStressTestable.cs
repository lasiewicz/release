﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Member.Test.Stress
{
    public interface IStressTestable  {
        //this method makes sure everything is ready to go 
        //before the stress testing starts
        void Prepare();
        //this method actually does the stress testing
        void Run();
    }
}
