﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Matchnet.Member.Test.Stress
{
    public class StressTester : IDisposable  {
        private readonly List<IStressTestable> _runners;
        private readonly List<Thread> _threads= new List<Thread>();

        public StressTester(List<IStressTestable> runners)  {
            _runners = runners;
            foreach (IStressTestable runner in _runners)
            {
                runner.Prepare();
            }
        }
        

        private static bool AnotherThreadAlive(List<Thread> threads)
        {            
            foreach (Thread t in threads)
            {
                if (t.IsAlive) return true;
            }
            return false;
        }

        public void RunConcurrently()  {
            foreach (IStressTestable runner in _runners)
            {
                
                Thread t1 = new Thread(new ThreadStart(delegate
                {
                    if (AnotherThreadAlive(_threads))
                    {
                        runner.Run();
                    }
                }));
                _threads.Add(t1);
                t1.Start();
                t1.Join();
            }            
        }

        public void Dispose() {
            foreach (Thread t in _threads)
            {
                t.Abort();
            }
        } 
    } 
}
