﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using NUnit.Framework;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Member.Test
{
    
    public class MemberSATest 
    {
        [Test]
        public void TestSuccessfulRegAndAttributes()
        {
            //this test actually creates a new member and then calls back through the BL to retrieve that member and verify all attributes were set correctly
            
            var regSessionID = Guid.NewGuid();
            var email = regSessionID.ToString() + "@spark.net";
            
            var attributes = new Dictionary<string, object>();
            attributes.Add("gendermask", 5);
            attributes.Add("SiteFirstName", "Matt");
            attributes.Add("ChildrenCount", 2);
            attributes.Add("JDateReligion", 2);
            attributes.Add("JDateEthnicity", 8);
            attributes.Add("PromotionID", 49098);
            attributes.Add("RegionId", 9805694);
            attributes.Add("RegistrationScenarioID", 1);
            attributes.Add("Relationshipmask", 1);
            attributes.Add("MaritalStatus", 2);
            attributes.Add("SmokingHabits", 4);
            attributes.Add("DrinkingHabits", 2);
            attributes.Add("MoreChildrenFlag", 2);
            attributes.Add("Height", 191);
            attributes.Add("IndustryType", 1073741824);
            attributes.Add("NewsletterMask", 4);
            attributes.Add("SynagogueAttendance", 16);
            
            attributes.Add("Birthdate", DateTime.Parse("05/09/1975"));

            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            var result = MemberSA.Instance.RegisterExtended(brand, email, regSessionID.ToString(), "1234", -838620992, regSessionID.ToString(),
                                               attributes);

            var member = MemberSA.Instance.GetMember(result.MemberID, MemberLoadFlags.IngoreSACache);

            Assert.IsTrue(result.RegisterStatus == RegisterStatusType.Success);
            Assert.IsTrue(member.EmailAddress == email);

            //supplied attributes
            Assert.IsTrue(member.GetAttributeInt(brand, "gendermask") == 5);
            Assert.IsTrue(member.GetAttributeInt(brand, "ChildrenCount") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "JDateReligion") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "JDateEthnicity") == 8);
            Assert.IsTrue(member.GetAttributeInt(brand, "PromotionID") == 49098);
            Assert.IsTrue(member.GetAttributeInt(brand, "RegionId") == 9805694);
            Assert.IsTrue(member.GetAttributeInt(brand, "RegistrationScenarioID") == 1);
            Assert.IsTrue(member.GetAttributeInt(brand, "MaritalStatus") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "SmokingHabits") == 4);
            Assert.IsTrue(member.GetAttributeInt(brand, "DrinkingHabits") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "MoreChildrenFlag") == 2);
            Assert.IsTrue(member.GetAttributeInt(brand, "Height") == 191);
            Assert.IsTrue(member.GetAttributeInt(brand, "IndustryType") == 1073741824);
            Assert.IsTrue(member.GetAttributeInt(brand, "NewsletterMask") == 4);
            Assert.IsTrue(member.GetAttributeInt(brand, "RegistrationIP") == -838620992);
            Assert.IsTrue(member.GetAttributeInt(brand, "SynagogueAttendance") == 16);
            Assert.IsTrue(member.GetAttributeDate(brand, "Birthdate") == DateTime.Parse("05/09/1975"));
            Assert.IsTrue(member.GetAttributeText(brand, "SiteFirstName") == "Matt");
            Assert.IsTrue(member.GetAttributeText(brand, "RegsistrationSessionID") == regSessionID.ToString());

            DateTime min = DateTime.Now.AddMinutes(-2);
            DateTime max = DateTime.Now;

            //generated attributes
            Assert.IsTrue(member.GetAttributeInt(brand, "BrandLogonCount") == 1);
            Assert.IsTrue(member.GetAttributeInt(brand, "FTAApproved") == 0);
            Assert.IsTrue(member.GetAttributeDate(brand, "BrandInsertDate") >= min && member.GetAttributeDate(brand, "BrandInsertDate") <= max);
            Assert.IsTrue(member.GetAttributeDate(brand, "BrandLastLogonDate") >= min && member.GetAttributeDate(brand, "BrandInsertDate") <= max);
            Assert.IsTrue(member.GetAttributeDate(brand, "LastUpdated") >= min && member.GetAttributeDate(brand, "BrandInsertDate") <= max);
        }


        [Test]
        public void TestDuplicateEmailCaught()
        {
            var regSessionID = Guid.NewGuid();
            var email = "mmishkoff@spark.net";
            var attributes = new Dictionary<string, object>();

            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            var result = MemberSA.Instance.RegisterExtended(brand, email, regSessionID.ToString(), "1234", -838620992, regSessionID.ToString(),attributes);

            Assert.IsTrue(result.RegisterStatus == RegisterStatusType.AlreadyRegistered);
        }

        [Test]
        public void TestLeadingTrailingSpaceEmailPassword()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            var email = "   dharris@spark.net   ";
            var password = "   1234   ";
            AuthenticationResult authenticationResult = MemberSA.Instance.Authenticate(brand, email, password);
            Assert.IsTrue(authenticationResult.Status == AuthenticationStatus.Authenticated);
        }

        [Test]
        public void TestMemberSADTODisposal()
        {
            MemberSA.SettingsService = null;
            ArrayList memberIDs=new ArrayList();
            int length = 1000;
            int[] existingDevMemberIds = TestUtils.ExistingDevMemberIds;
            foreach (int existingDevMemberId in existingDevMemberIds)
            {
                if (!memberIDs.Contains(existingDevMemberId)) memberIDs.Add(existingDevMemberId);
                if (memberIDs.Count == length) break;
            }
            ArrayList memberDtOs = null;
            for (int i = 0; i < 50; i++)
            {
                memberDtOs = MemberSA.Instance.GetMemberDTOs(memberIDs, MemberType.Search);
                Assert.AreEqual(length, memberDtOs.Count);
                foreach (IMemberDTO memberDto in memberDtOs)
                {
                    Assert.IsNotNull(memberDto.GetLastLogonDate(3));
                    if (memberDto is MemberDTO)
                    {
                        ((MemberDTO)memberDto).Dispose();
                    }
                }
            }
        }

    }
}
