﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Matchnet.Configuration.ServiceAdapters;
//using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using NUnit.Framework;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Member.Test
{
    public class MemberSAMembaseTest
    {
        private MembaseConfig membaseConfig;
        private MockSettingsService mockSettingsService = new MockSettingsService();

        [TestFixtureSetUp]
        public void StartUp()
        {
            MemberSA.Instance.NUnit = true;
            mockSettingsService.AddSetting("CRX_POPULATE_MEMBER_INFO", "true");
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");
            PopulateMockSettingFromDB("KEY");
            PopulateMockSettingFromDB("KEY2");
            PopulateMockSettingFromDB("MEMBERSVC_CACHE_TTL_MEMBER_SA");
            PopulateMockSettingFromDB("MEMBERSVC_SA_CONNECTION_LIMIT");
            PopulateMockSettingFromDB("PHOTO_REQUIRED_SITE");
            PopulateMockSettingFromDB("CRYPTO_WORK_FACTOR");
            PopulateMockSettingFromDB("MEMBERSVC_CACHE_TTL_MEMBER_SA");
            PopulateMockSettingFromDB("LAST_LOGON_KEEP_COUNT");
            PopulateMockSettingFromDB("MEMBERSVC_PHOTO_RELATIVEPATHS");
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
            mockSettingsService.AddSetting("ENABLE_MEMBER_DTO", "true");
            mockSettingsService.AddSetting("LOG_SAVE_LOGON_INFO", "false");
            MemberSA.SettingsService = mockSettingsService;
        }

        private void PopulateMockSettingFromDB(string constant)
        {
            mockSettingsService.AddSetting(constant, MemberSA.SettingsService.GetSettingFromSingleton(constant));
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
            MemberSA.Instance.NUnit = false;
            MemberSA.SettingsService = null;
        }

        [SetUp]
        public void SetUp()
        {
            MemberSA.Instance.NUnit = true;
           // membaseConfig = RuntimeSettings.GetMembaseConfigByBucket("webcache");
            //MemberSA.Instance._cache = Spark.Caching.MembaseCaching.GetInstance(membaseConfig);
        }

        [TearDown]
        public void Teardown()
        {
            MemberSA.Instance.NUnit = false;
//            MemberSA.Instance.UseMembase = false;
        }

        [Test]
        public void GetMemberMembaseTest()
        {
            int memberId = 111644154;

            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberId));
            MemberSA.Instance.ICache.Remove(CachedMemberAccess.GetCacheKey(memberId));
            MemberSA.Instance.ICache.Remove(CachedMemberBytes.GetCacheKey(memberId));

            Assert.Catch<SAException>(() => MemberSA.Instance.GetMember(0, MemberLoadFlags.None));

            //MemberLoadFlags.None
            ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            Assert.IsNotNull(member);
            var cachedMember = MemberSA.Instance.ICache.Get(CachedMember.GetCacheKey(memberId)) as CachedMember;
            Assert.IsNotNull(cachedMember);
            Assert.AreEqual(cachedMember.MemberID,memberId);

            //MemberLoadFlags.IngoreSACache
            member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            Assert.IsNotNull(member);
            cachedMember = MemberSA.Instance.ICache.Get(CachedMember.GetCacheKey(memberId)) as CachedMember;
            Assert.IsNotNull(cachedMember);
            Assert.AreEqual(cachedMember.MemberID, memberId);
            var cachedMemberAccess = MemberSA.Instance.ICache.Get(CachedMemberAccess.GetCacheKey(memberId)) as CachedMemberAccess;
            //When MembserLoadFlags = IgnoreSACache, CachedMemberAccess is removed from cache
            //Assert.IsNull(cachedMemberAccess);
        }

        [Test]
        public void GetMemberLocalCacheTest()
        {
            int memberId = 111644154;

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "false");
            MemberSA.SettingsService = mockSettingsService;

            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberId));
            MemberSA.Instance.ICache.Remove(CachedMemberAccess.GetCacheKey(memberId));
            MemberSA.Instance.ICache.Remove(CachedMemberBytes.GetCacheKey(memberId));

            //MemberLoadFlags.None
            ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            Assert.IsNotNull(member);
            var cachedMember = MemberSA.Instance.ICache.Get(CachedMember.GetCacheKey(memberId)) as CachedMember;
            Assert.IsNotNull(cachedMember);
            Assert.AreEqual(cachedMember.MemberID, memberId);
            //Check if Membase has the object
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberAccess.GetCacheKey(memberId)));
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberBytes.GetCacheKey(memberId)));
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMember.GetCacheKey(memberId)));
            
            //MemberLoadFlags.IngoreSACache
            member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            Assert.IsNotNull(member);
            Assert.IsNull(MemberSA.Instance.ICache.Get(CachedMemberAccess.GetCacheKey(memberId)));
            //Check if Membase has the object
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberAccess.GetCacheKey(memberId)));
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberBytes.GetCacheKey(memberId)));
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMember.GetCacheKey(memberId)));

            //reset cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
            MemberSA.SettingsService = mockSettingsService;
        }

        [Test]
        public void SaveMemberLastLogonMembaseTest()
        {
            int memberID = 111644154;
            int communityID = 3;
            int siteID = 103;
            int brandID = 1003;

            ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            MemberSA.Instance.ICache.Remove(CachedMemberLogon.GetCacheKey(memberID, brandID));
            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberID));

            DateTime lastlogonDate1 = member.GetLastLogonDate(3);

            MemberSA.Instance.SaveMemberLastLogon(memberID, brandID, Constants.NULL_INT);

            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberID));
            MemberSA.Instance.ICache.Remove(CachedMemberBytes.GetCacheKey(memberID));

            member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);

            DateTime lastlogonDate2 = member.GetAttributeDate(communityID, siteID, brandID, "BrandLastLogonDate", System.DateTime.MinValue);

            //Assert.AreEqual(1,lastlogonDate2.CompareTo(lastlogonDate1));

            //Check if object is available in cache
            //Assert.IsNull(MemberSA.Instance._cache.Get(CachedMemberLogon.GetCacheKey(memberID,brandID)));
        }

        [Test]
        public void SaveMemberLastLogonLocalCacheTest()
        {
            int memberID = 111644154;
            int communityID = 3;
            int siteID = 103;
            int brandID = 1003;

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "false");
            MemberSA.SettingsService = mockSettingsService;

            ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            MemberSA.Instance.ICache.Remove(CachedMemberLogon.GetCacheKey(memberID, brandID));
            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberID));

            DateTime lastlogonDate1 = member.GetLastLogonDate(3);

            MemberSA.Instance.SaveMemberLastLogon(memberID, brandID, Constants.NULL_INT);

            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberID));
            MemberSA.Instance.ICache.Remove(CachedMemberBytes.GetCacheKey(memberID));

            member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);

            DateTime lastlogonDate2 = member.GetAttributeDate(communityID, siteID, brandID, "BrandLastLogonDate", System.DateTime.MinValue);

            //Assert.IsNotNull(MemberSA.Instance._cache.Get(CachedMemberLogon.GetCacheKey(memberID, brandID)));

            //Assert.AreEqual(1,lastlogonDate2.CompareTo(lastlogonDate1));

            //Check if object is available in local and membase cache
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberLogon.GetCacheKey(memberID, brandID)));
            //Assert.IsNull(MemberSA.Instance._cache.Get(CachedMemberLogon.GetCacheKey(memberID, brandID)));

            //reset cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
            MemberSA.SettingsService = mockSettingsService;        
        }

        [Test]
        public void SaveMemberTest()
        {
            int memberID = 111644154;
            int communityID = 3;
            int siteID = 103;
            int brandID = 1003;

            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberID));
            ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            member.SetUsername("testAccount1",brand);
            MemberSA.Instance.SaveMember(member);
            //Check if object is available in membase cache
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberLogon.GetCacheKey(memberID, brandID)));

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "false");
            MemberSA.SettingsService = mockSettingsService;

            member.SetUsername("testAccount2", brand);
            MemberSA.Instance.SaveMember(member);
            Assert.IsNotNull(MemberSA.Instance.ICache.Get(CachedMember.GetCacheKey(memberID)));
            //Assert.IsNull(MemberSA.Instance.ICache.Get(CachedMemberLogon.GetCacheKey(memberID, brandID)));
            //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberLogon.GetCacheKey(memberID, brandID)));
            //reset cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
            MemberSA.SettingsService = mockSettingsService;
        }

       [Test]
        public void ExpireCachedMemberTest()
       {
           int memberID = 111644154;
           //ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
           MemberSA.Instance.ExpireCachedMember(memberID);
           Assert.IsNull(MemberSA.Instance.ICache.Get(CachedMemberBytes.GetCacheKey(memberID)));

           //local cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "false");
           MemberSA.SettingsService = mockSettingsService;

           //member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
           MemberSA.Instance.ExpireCachedMember(memberID);
           Assert.IsNull(MemberSA.Instance.ICache.Get(CachedMemberBytes.GetCacheKey(memberID))); 
           //Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberBytes.GetCacheKey(memberID)));
           //reset cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
           MemberSA.SettingsService = mockSettingsService;
       }

       [Test]
       public void ExpireCachedMemberAccessTest()
       {
           int memberID = 111644154;
           //ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
           MemberSA.Instance.ExpireCachedMemberAccess(memberID);
           Assert.IsNull(MemberSA.Instance.ICache.Get(CachedMemberAccess.GetCacheKey(memberID)));

           //local cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "false");
           MemberSA.SettingsService = mockSettingsService;

           //member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
           MemberSA.Instance.ExpireCachedMemberAccess(memberID);
           Assert.IsNull(MemberSA.Instance.ICache.Get(CachedMemberAccess.GetCacheKey(memberID)));
//           Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberAccess.GetCacheKey(memberID)));
           //reset cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
           MemberSA.SettingsService = mockSettingsService;
       }

        [Test]
        public void UpdateSearchTest()
        {
            int memberID = 111644154;
            int communityID = 3;
            MemberSA.Instance.UpdateSearch(memberID,communityID);
//            Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberBytes.GetCacheKey(memberID)));

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "false");
            MemberSA.SettingsService = mockSettingsService;

            MemberSA.Instance.UpdateSearch(memberID, communityID);
//            Assert.IsNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMemberBytes.GetCacheKey(memberID)));
            //Assert.IsNull(MemberSA.Instance._cache.Get(CachedMemberBytes.GetCacheKey(memberID)));
            //reset cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, "true");
            MemberSA.SettingsService = mockSettingsService;
        }

        [Test]
        public void SavePhotoTest()
        {
            int memberID = 111644154;
            int brandID = 1003;
            int communityID = 3;
            int siteID = 103;
            var updates = new PhotoUpdate[0];
            MemberSA.Instance.SavePhotos(brandID,siteID,communityID,memberID,updates);
//            Assert.IsNotNull(Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Get(CachedMember.GetCacheKey(memberID)));
        }

        [Test]
        public void GetMembersTest()
        {
            ArrayList memberIds = new ArrayList();
            memberIds.Add(116360126);
            memberIds.Add(116360318);
            memberIds.Add(127770427);
            //memberIds.Add(116360318);
            var members = MemberSA.Instance.GetMembers(memberIds, MemberLoadFlags.None);
        }

        [Test]
        public void GetMemberDTOMembaseTest()
        {
            int memberId = 111644154;

            MemberSA.Instance.ICache.Remove(CachedMember.GetCacheKey(memberId));
            MemberSA.Instance.ICache.Remove(CachedMemberAccess.GetCacheKey(memberId));
            MemberSA.Instance.ICache.Remove(CachedMemberBytes.GetCacheKey(memberId));
            MemberSA.Instance.MemberDTOCache.Remove(MemberDTO.GetCacheKey(memberId, MemberType.Search));
            MemberSA.Instance.MemberDTOCache.Remove(MemberDTO.GetCacheKey(memberId, MemberType.MatchMail));

            Assert.Catch<SAException>(() => MemberSA.Instance.GetMemberDTO(0, MemberType.Search));

            //membertype == search
            IMemberDTO memberDto = MemberSA.Instance.GetMemberDTO(memberId, MemberType.Search);
            Assert.IsNotNull(memberDto);
            IMemberDTO cachedMemberDTO = MemberSA.Instance.MemberDTOCache.Get(MemberDTO.GetCacheKey(memberId, MemberType.Search)) as IMemberDTO;
            Assert.IsNotNull(cachedMemberDTO);
            Assert.AreEqual(cachedMemberDTO.MemberID, memberId);
            Assert.AreEqual(cachedMemberDTO.MemberType, MemberType.Search);

            //membertype == matchmail
            cachedMemberDTO = MemberSA.Instance.MemberDTOCache.Get(MemberDTO.GetCacheKey(memberId, MemberType.MatchMail)) as IMemberDTO;
            Assert.IsNull(cachedMemberDTO);
            memberDto = MemberSA.Instance.GetMemberDTO(memberId, MemberType.MatchMail);
            Assert.IsNotNull(memberDto);
            cachedMemberDTO = MemberSA.Instance.MemberDTOCache.Get(MemberDTO.GetCacheKey(memberId, MemberType.MatchMail)) as IMemberDTO;
            Assert.IsNotNull(cachedMemberDTO);
            Assert.AreEqual(cachedMemberDTO.MemberID, memberId);
            Assert.AreNotEqual(cachedMemberDTO.MemberType, MemberType.Search);
            Assert.AreEqual(cachedMemberDTO.MemberType, MemberType.MatchMail);
        }

        [Test]
        public void GetManyMemberDTOsMembaseTest()
        {
//            MemberSA.Instance.ICache.Clear();
//            MemberSA.Instance.MemberDTOCache.Clear();

            //membertype == search
            IEnumerable<int> setOfMemberIds = TestUtils.ExistingDevMemberIds.Take(2000);
            ArrayList memberIds = new ArrayList(setOfMemberIds.ToArray());
            ArrayList memberDTOs = MemberSA.Instance.GetMemberDTOs(memberIds, MemberType.Search);
            Assert.IsNotNull(memberDTOs);

            List<string> keys = (from int memberId in setOfMemberIds select MemberDTO.GetCacheKey(memberId, MemberType.Search)).ToList<string>();
            IDictionary<string, object> cachedMemberDTODictionary = MemberSA.Instance.MemberDTOCache.Get(keys);
            Assert.IsNotNull(cachedMemberDTODictionary);
            StringBuilder missingMemberIds = new StringBuilder();
            foreach (int memberId in setOfMemberIds)
            {
                string cacheKey = MemberDTO.GetCacheKey(memberId, MemberType.Search);
                if (!cachedMemberDTODictionary.ContainsKey(cacheKey))
                {
                    missingMemberIds.Append(memberId+",");
                }
                else
                {
                    IMemberDTO cachedMemberDTO = cachedMemberDTODictionary[cacheKey] as IMemberDTO;
                    Assert.AreEqual(cachedMemberDTO.MemberID, memberId);
                    Assert.AreEqual(cachedMemberDTO.MemberType, MemberType.Search);
                }
            }
            Assert.AreEqual(0,missingMemberIds.Length, "These member ids are missing from the cache: "+missingMemberIds.ToString());
        }


    }
}
