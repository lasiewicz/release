﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using NUnit.Framework;

namespace Matchnet.Member.Test
{
    public class MemberSAMemberPhotoTest
    {
        [TestFixtureSetUp]
        public void StartUp()
        {
            
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
            
        }

        [SetUp]
        public void SetUp()
        {
            
        }

        [TearDown]
        public void Teardown()
        {
            
        }

        [Test]
        public void GetDefaultPhotoTest()
        {
            var cachedMember = new CachedMember(0, 3);
            cachedMember.Photos = new PhotoCollection();
            cachedMember.Photos.GetCommunity(3)[0] = new Photo(1, 1, string.Empty, 1, string.Empty, 1, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, true);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(2, 2, string.Empty, 2, string.Empty, 2, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, false);

            var testMember = new ServiceAdapters.Member(cachedMember);

            var defaultPhoto = testMember.GetDefaultPhoto(3, true);

            Assert.IsTrue(defaultPhoto != null && defaultPhoto.MemberPhotoID == 1);
        }

        [Test]
        public void GetRandomApprovedForMainPhotoTest()
        {
            var cachedMember = new CachedMember(0, 3);
            cachedMember.Photos = new PhotoCollection();
            cachedMember.Photos.GetCommunity(3)[0] = new Photo(1, 1, string.Empty, 1, string.Empty, 1, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, true);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(2, 2, string.Empty, 2, string.Empty, 2, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, false);

            var testMember = new ServiceAdapters.Member(cachedMember);

            var photo = testMember.GetRandomApprovedForMainPhoto(3, true);

            Assert.IsNotNull(photo);
        }

        [Test]
        public void GetApprovedForMainPhotosTest()
        {
            var cachedMember = new CachedMember(0, 3);
            cachedMember.Photos = new PhotoCollection();
            cachedMember.Photos.GetCommunity(3)[0] = new Photo(1, 1, string.Empty, 1, string.Empty, 1, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, true);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(2, 2, string.Empty, 2, string.Empty, 2, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, false);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(3, 3, string.Empty, 3, string.Empty, 3, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, false, false);

            var testMember = new ServiceAdapters.Member(cachedMember);

            var photos = testMember.GetApprovedForMainPhotos(3, true);

            Assert.IsTrue(photos != null && photos.Count == 2);
        }

        [Test]
        public void GetApprovedPhotosTest()
        {
            var cachedMember = new CachedMember(0, 3);
            cachedMember.Photos = new PhotoCollection();
            cachedMember.Photos.GetCommunity(3)[0] = new Photo(1, 1, string.Empty, 1, string.Empty, 1, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, true);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(2, 2, string.Empty, 2, string.Empty, 2, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, false);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(3, 3, string.Empty, 3, string.Empty, 3, false, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, false, false);

            var testMember = new ServiceAdapters.Member(cachedMember);

            var photos = testMember.GetApprovedPhotos(3, true);

            Assert.IsTrue(photos != null && photos.Count == 2);
        }

        [Test]
        public void GetApprovedPhotosExcludingMainTest()
        {
            var cachedMember = new CachedMember(0, 3);
            cachedMember.Photos = new PhotoCollection();
            cachedMember.Photos.GetCommunity(3)[0] = new Photo(1, 1, string.Empty, 1, string.Empty, 1, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, true);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(2, 2, string.Empty, 2, string.Empty, 2, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, true, false);
            cachedMember.Photos.GetCommunity(3)[1] = new Photo(3, 3, string.Empty, 3, string.Empty, 3, true, false, 1,
                0, string.Empty, false, string.Empty, string.Empty, false, false);

            var testMember = new ServiceAdapters.Member(cachedMember);

            var photos = testMember.GetApprovedPhotosExcludingMain(3, true);

            Assert.IsTrue(photos != null && photos.Count == 2);
        }
    }
}
