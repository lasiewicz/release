﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using NUnit.Framework;

namespace Matchnet.Member.Test
{
    public class MemberBLMembaseTest
    {
        private MembaseConfig membaseConfig;
        private MockSettingsService mockSettingsService = new MockSettingsService();

        private int[] existingDevMemberIds = TestUtils.ExistingDevMemberIds;

        [TestFixtureSetUp]
        public void StartUp()
        {
            MemberBL.Instance.NUnit = true;
            MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(MemberBL_MemberRequested);
            MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(MemberBL_MemberAdded);
            MemberBL.Instance.MemberRemoved += new Matchnet.Member.BusinessLogic.MemberBL.MemberRemoveEventHandler(MemberBL_MemberRemoved);
            MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(MemberBL_MemberSaved);

            MemberBL.Instance.LogonRequested += new Matchnet.Member.BusinessLogic.MemberBL.LogonRequestEventHandler(MemberBL_LogonRequested);

            MemberBL.Instance.RegistrationRequested += new Matchnet.Member.BusinessLogic.MemberBL.RegistrationRequestEventHandler(MemberBL_RegistrationRequested);
            MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(MemberBL_SynchronizationRequested);

            //replication
            MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(MemberBL_ReplicationRequested);
            mockSettingsService.AddSetting("CRX_POPULATE_MEMBER_INFO", "true");
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");
            mockSettingsService.AddSetting("KEY", MemberBL.SettingsService.GetSettingFromSingleton("KEY"));
            mockSettingsService.AddSetting("KEY2", MemberBL.SettingsService.GetSettingFromSingleton("KEY2"));
            mockSettingsService.AddSetting("LAST_LOGON_KEEP_COUNT","5");
            mockSettingsService.AddSetting("USE_MEMBASE_BL_CACHE", "true");
            mockSettingsService.AddSetting("ENABLE_MEMBER_DTO", "true");
            mockSettingsService.AddSetting("LOG_SAVE_LOGON_INFO", "false");
            MemberBL.SettingsService = mockSettingsService;
        }

        private void MemberBL_ReplicationRequested(IReplicable replicableobject)
        {
        }

        private void MemberBL_SynchronizationRequested(string key, Hashtable cachereferences)
        {
        }

        private void MemberBL_RegistrationRequested(bool success)
        {
        }

        private void MemberBL_LogonRequested(bool success)
        {
        }

        private void MemberBL_MemberSaved()
        {
        }

        private void MemberBL_MemberRemoved()
        {
        }

        private void MemberBL_MemberAdded()
        {
            
        }

        private void MemberBL_MemberRequested(bool cachehit)
        {
            
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
            MemberBL.Instance.NUnit = false;
        }

        [SetUp]
        public void SetUp()
        {
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "true");
            MemberBL.SettingsService = mockSettingsService;
        }

        [TearDown]
        public void Teardown()
        {
        }

        [Test]
        public void SaveMemberLastLogonTest()
        {
            int memberID = 116360126;
            int groupID = 3;
            DateTime lastLogon = DateTime.Now;
            MemberBL.Instance.SaveMemberLastLogon(null,memberID,groupID,lastLogon);
            var logon =MemberBL.Instance._membaseCache.Get(CachedMemberLogon.GetCacheKey(memberID, groupID)) as CachedMemberLogon;
            Assert.IsNotNull(logon);
            //Assert.Equals(lastLogon, logon.LastLogons[0]);

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "false");
            MemberBL.SettingsService = mockSettingsService;

            MemberBL.Instance.SaveMemberLastLogon(null, memberID, groupID, lastLogon);
            logon = MemberBL.Instance._cache.Get(CachedMemberLogon.GetCacheKey(memberID,groupID)) as CachedMemberLogon;
            Assert.IsNotNull(logon);
            //Assert.Equals(lastLogon, logon.LastLogons[0]);
        }

        [Test]
        public void GetMemberLastLogonTest()
        {
            int memberID = 116360126;
            int groupID = 3;
            DateTime lastLogon = DateTime.Now;
            MemberBL.Instance.GetCachedMemberLogon(null, null,memberID, groupID, false);
            var logon = MemberBL.Instance._membaseCache.Get(CachedMemberLogon.GetCacheKey(memberID, groupID)) as CachedMemberLogon;

            Assert.IsNotNull(logon);
            //Assert.Equals(lastLogon, logon.LastLogons[0]);

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "false");
            MemberBL.SettingsService = mockSettingsService;

            MemberBL.Instance.GetCachedMemberLogon(null, null, memberID, groupID, false);
            logon = MemberBL.Instance._cache.Get(CachedMemberLogon.GetCacheKey(memberID, groupID)) as CachedMemberLogon;
            Assert.IsNotNull(logon);
            //Assert.Equals(lastLogon, logon.LastLogons[0]);
        }

       [Test]
        public void getCachedMemberBytesTest()
        {
            int memberID = 111644154;
            var cachedMemberBytes = MemberBL.Instance.GetCachedMemberBytes(null, null, memberID, false);
            Assert.IsNotNull(cachedMemberBytes);
            Assert.IsNotNull(MemberBL.Instance._membaseCache.Get(CachedMemberBytes.GetCacheKey(memberID)));

            //local cache
            mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "false");
            MemberBL.SettingsService = mockSettingsService;
            cachedMemberBytes = MemberBL.Instance.GetCachedMemberBytes(null, null, memberID, false);
            Assert.IsNotNull(cachedMemberBytes);
            Assert.IsNotNull(MemberBL.Instance._cache.Get(CachedMemberBytes.GetCacheKey(memberID)));
        }

       //[Test]
       public void getCachedMemberAccessTest()
       {
           int memberID = 111644154;
           var cachedMemberAccess = MemberBL.Instance.GetCachedMemberAccess(null, null, memberID, false);
           Assert.IsNotNull(cachedMemberAccess);
           Assert.IsNotNull(MemberBL.Instance._membaseCache.Get(CachedMemberBytes.GetCacheKey(memberID)));

           //local cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "false");
           MemberBL.SettingsService = mockSettingsService;
           cachedMemberAccess = MemberBL.Instance.GetCachedMemberAccess(null, null, memberID, false);
           Assert.IsNotNull(cachedMemberAccess);
           Assert.IsNotNull(MemberBL.Instance._cache.Get(CachedMemberBytes.GetCacheKey(memberID)));
       }

       [Test]
       public void SaveMemberTest()
       {
           int memberID = 111644154;
           var mbr = new MemberUpdate(memberID);
           mbr.Username = "test123";
           var memberSaveResult = MemberBL.Instance.SaveMember(null, mbr, 1,3);
           Assert.IsNotNull(memberSaveResult);
           //Assert.Equals((int)memberSaveResult.SaveStatus, (int)MemberSaveStatusType.Success);
           var cachedMemberBytes =MemberBL.Instance._membaseCache.Get(CachedMemberBytes.GetCacheKey(memberID));
           Assert.IsNotNull(cachedMemberBytes);

           //local cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "false");
           MemberBL.SettingsService = mockSettingsService;

           memberSaveResult = MemberBL.Instance.SaveMember(null, mbr, 1, 3);
           Assert.IsNotNull(memberSaveResult);
           Assert.IsNotNull(MemberBL.Instance._cache.Get(CachedMemberBytes.GetCacheKey(memberID)));
           //Assert.Equals(memberSaveResult.SaveStatus, MemberSaveStatusType.Success); 
       }

        [Test]
       public void SavePhotosTest()
       {
           int memberID = 111644154;
            var photos = new PhotoUpdate[0];
            var photoCollection = MemberBL.Instance.SavePhotos(null, 3, memberID, photos,1);
           Assert.IsNotNull(photoCollection);
            var cachedMemberBytes =MemberBL.Instance._membaseCache.Get(CachedMember.GetCacheKey(memberID));
           Assert.IsNotNull(cachedMemberBytes);

           //local cache
           mockSettingsService.AddSetting(ValueObjects.ServiceConstants.MEMBASE_BL_SETTING_CONSTANT, "false");
           MemberBL.SettingsService = mockSettingsService;
           photoCollection = MemberBL.Instance.SavePhotos(null, 3, memberID, photos, 1);
           Assert.IsNotNull(MemberBL.Instance._cache.Get(CachedMember.GetCacheKey(memberID)));
       }

        [Test]
        public void GetManyCachedMemberBytesTest()
        {
            foreach (int existingDevMemberId in existingDevMemberIds)
            {
                var cachedMemberBytes = MemberBL.Instance.GetCachedMemberBytes(null, null, existingDevMemberId, false);
                Assert.IsNotNull(cachedMemberBytes);
            }

            List<string> keys = (from int memberId in existingDevMemberIds select CachedMemberBytes.GetCacheKey(memberId)).ToList<string>();
            IDictionary<string, object> objects = MemberBL.Instance._membaseCache.Get(keys);

            Assert.IsNotNull(objects);
            foreach (string key in keys)
            {                
                Assert.IsTrue(objects.ContainsKey(key));
                Assert.IsTrue(objects[key] is CachedMemberBytes);
                CachedMemberBytes cmb = objects[key] as CachedMemberBytes;
                Assert.IsTrue(existingDevMemberIds.Contains(cmb.MemberID));
            }

            foreach (string key in keys)
            {
                MemberBL.Instance._membaseCache.RemoveWithoutGet(key);
            }
        }

    }
}
