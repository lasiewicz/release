﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceAdapters.Mocks;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects.Privilege;
using NUnit.Framework;

namespace Matchnet.Member.Test
{
    class MemberBLTest
    {
        private MembaseConfig membaseConfig;
        private MockSettingsService mockSettingsService = new MockSettingsService();
        string serviceURI = "tcp://bhd-arodriguez:42000/MemberSM.rem";

        private void MemberBL_ReplicationRequested(IReplicable replicableobject)
        {
        }

        private void MemberBL_SynchronizationRequested(string key, Hashtable cachereferences)
        {
        }

        private void MemberBL_RegistrationRequested(bool success)
        {
        }

        private void MemberBL_LogonRequested(bool success)
        {
        }

        private void MemberBL_MemberSaved()
        {
        }

        private void MemberBL_MemberRemoved()
        {
        }

        private void MemberBL_MemberAdded()
        {

        }

        private void MemberBL_MemberRequested(bool cachehit)
        {

        }

        [TestFixtureSetUp]
        public void StartUp()
        {
            MemberBL.Instance.NUnit = true;
            MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(MemberBL_MemberRequested);
            MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(MemberBL_MemberAdded);
            MemberBL.Instance.MemberRemoved += new Matchnet.Member.BusinessLogic.MemberBL.MemberRemoveEventHandler(MemberBL_MemberRemoved);
            MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(MemberBL_MemberSaved);

            MemberBL.Instance.LogonRequested += new Matchnet.Member.BusinessLogic.MemberBL.LogonRequestEventHandler(MemberBL_LogonRequested);

            MemberBL.Instance.RegistrationRequested += new Matchnet.Member.BusinessLogic.MemberBL.RegistrationRequestEventHandler(MemberBL_RegistrationRequested);
            MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(MemberBL_SynchronizationRequested);

            //replication
            MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(MemberBL_ReplicationRequested);
            mockSettingsService.AddSetting("CRX_POPULATE_MEMBER_INFO", "true");
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");
            mockSettingsService.AddSetting("KEY", MemberBL.SettingsService.GetSettingFromSingleton("KEY"));
            mockSettingsService.AddSetting("KEY2", MemberBL.SettingsService.GetSettingFromSingleton("KEY2"));
            mockSettingsService.AddSetting("USE_MEMBASE_BL_CACHE","true");
            MemberBL.SettingsService = mockSettingsService;            
        }


        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {
            MemberBL.Instance.NUnit = false;
            MemberBL.SettingsService = null;
        }

        [SetUp]
        public void SetUp()
        {

        }

        [TearDown]
        public void Teardown()
        {
        }

        [Test]
        public void SaveMemberTest()
        {
            var attributes = new Hashtable();
            int memberID = 111644154;
            var mbr = new MemberUpdate(memberID);
            attributes.Add(5514, new TextValue("Friday Friday Friday Friday Friday Friday Friday Friday",
                                                 TextStatusType.Pending));
            mbr.AttributesText.Add(2,attributes);
            var memberSaveResult = MemberBL.Instance.SaveMember(null, mbr, 1, 3);
        }

        [Test]
        public void AddFreeTextApprovalQueueItemTest()
        {
            int memberId = 101801218;
            //int memberId = 111644154;
            //int memberId = 101801101;
            //int memberId = 100169378;

            var cachedMember = MemberBL.Instance.GetCachedMember(null, null, memberId, true);
            var memberUpdate = new MemberUpdate(memberId);
            var attributes = new Hashtable();
            attributes.Add(5514, new TextValue("Test Test Test", TextStatusType.Pending));
            memberUpdate.AttributesText.Add(2, attributes);
            var memberUpdateNotifier = new MemberUpdateNotifier(ref cachedMember, memberUpdate, null);
            memberUpdateNotifier.SettingsService = mockSettingsService;

            Assert.IsNotNull(memberUpdateNotifier.TextQueueItems);
            Assert.IsTrue(memberUpdateNotifier.TextQueueItems.Count == 1);
        }

        [Test]
        public void EncryptPasswordTest()
        {
           // Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            //string email = "jdatetest20@gmail.com";
            string pass = "«On se plaint, dit-il, qu'il n'y a plus de conversation de nos jours en France. J'en sais bien la raison. C'est que la patience d'écouter diminue chaque jour chez nos contemporains. L'on écoute mal ou plutôt l'on n'écoute plus du tout. J'ai fait cette remarque dans la meilleure compagnie que je fréquente.»";
            //string encryptPassword = MemberSA.Instance.EncryptPassword(pass);
            string salt = Matchnet.Security.Crypto.Instance.GenerateSalt(10);
            string encryptPassword = Matchnet.Security.Crypto.Instance.EncryptText(pass, salt);
            Console.Write(string.Format("Password/Encrypted Password/Salt = '{0}'/'{1}'/'{2}'", pass, encryptPassword,salt));
            Assert.IsTrue(Security.Crypto.Instance.VerifyEncryptedText(pass,salt,encryptPassword));
            Assert.AreEqual(29, salt.Length);
            Assert.AreEqual(31, encryptPassword.Length);
        }

        [Test]
        public void VerifyPasswordTest()
        {
//            string oldEncryptedPass = Security.Crypto.Encrypt(MemberBL.SettingsService.GetSettingFromSingleton("KEY"), "1111");
            string salt = "$2a$10$otjb/93uv/c8ZwnLZUi48."; // from Value column
            string newEncryptedPass = "Vj4eXDQeLx0joeNuDQQz0/lMv.IeAVy"; // from PasswordHash column
//            string decrypt = Security.Crypto.Decrypt(MemberBL.SettingsService.GetSettingFromSingleton("KEY"), oldEncryptedPass);
            string decrypt = "travel10";
            Console.WriteLine("Decrypted pass is "+decrypt);
            string email = "ccbrooks32@yahoo.com";
            string emailSalt = MemberBL.SettingsService.GetSettingFromSingleton("KEY2");
            Console.WriteLine(string.Format("Email:{0}, Encrypt:{1}", email, Security.Crypto.Instance.EncryptText(email, emailSalt)));
//            string email2 = "matthewschoenfeld123@yahoo.com";
//            Console.WriteLine(string.Format("Email:{0}, Encrypt:{1}", email2, Security.Crypto.Instance.EncryptText(email2, emailSalt)));

            Console.WriteLine(string.Format("Password/Encrypted Password/Salt = '{0}'/'{1}'/'{2}'", decrypt, newEncryptedPass, salt));
            Assert.IsTrue(Security.Crypto.Instance.VerifyEncryptedText(decrypt, salt, newEncryptedPass));
            Assert.AreEqual(29, salt.Length);
            Assert.AreEqual(31, newEncryptedPass.Length);
        }

        [Test]
        public void WriteEncryptedEmails()
        {
            string [] emails = new string[] {
                "melissa.towbin@gmail.com"};
            string emailSalt = MemberBL.SettingsService.GetSettingFromSingleton("KEY2");
            foreach (string email in emails)
            {
                Console.WriteLine(string.Format("Email:{0}, Encrypt:'{1}'", email,
                                                Security.Crypto.Instance.EncryptText(email, emailSalt)));
            }
        }

        [Test]
        public void AutheticateTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int memberId = Constants.NULL_INT;
            string email = Constants.NULL_STRING;
            string pass = Constants.NULL_STRING;
            string passwordHash = Constants.NULL_STRING;
            email = "NEWEMAILJDATETEST724117932@GMAIL.COM";
            pass = "test724117932";
            string encryptEmail = MemberSA.Instance.EncryptEmail(email);
            string memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail, 3);
            passwordHash = Security.Crypto.Instance.EncryptText(pass, memberSalt);
            int authenticate = MemberBL.Instance.Authenticate(email, 3, passwordHash, out memberId);
            Assert.AreEqual(0, authenticate);
        }

        [Test]
        public void RegisterTest()
        {
            int MemberID = 0;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string email = "jdatetest20@gmail.com";
            string username = "testtesttest123";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, 3);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }


            //MemberSA.Instance.Register(BrandConfigSA.Instance.GetBrandByID(1003), "jdatetest17@gmail.com",
            //                           "testtest1231234", "test1234", Constants.NULL_INT);
        }

        [Test]
        public void RegisterAndChangeEmailTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest"+i+"@gmail.com";
            string newemail = "NeWEmail"+email;
            string username = "testtesttest"+i;
            string pass = "test"+i;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, 3);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }

            Assert.AreEqual(email, member.EmailAddress);
            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            string oldSalt= MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);
            Assert.NotNull(oldSalt);
            member.EmailAddress = newemail;
            MemberSaveResult saveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.CommitChanges();
            }

            string newEncryptedEmail = MemberSA.Instance.EncryptEmail(newemail);
            string newSalt = MemberBL.Instance.GetMemberSalt(newEncryptedEmail, 3);
            Assert.NotNull(newSalt);
            Assert.AreEqual(oldSalt, newSalt, "Old salt is not equal to new salt!");
            Assert.AreNotEqual(email, member.EmailAddress);
            Assert.AreEqual(newemail, member.EmailAddress);

            Console.WriteLine(string.Format("Old Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
            Console.WriteLine(string.Format("New Email / EmailHash = {0} / {1}", newemail, newEncryptedEmail));

            int memberid = 0;
            string passwordHash = Security.Crypto.Instance.EncryptText(pass, newSalt);
            int authenticate = MemberBL.Instance.Authenticate(newemail, 3, passwordHash, out memberid);
            Assert.AreEqual(0, authenticate);
        }

        [Test]
        public void RegisterAndChangePasswordTest()
        {
            int MemberID = 0;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest" + i + "@gmail.com";
            string username = "testtesttest" + i;
            string pass = "test" + i;
            string newpass = "new" + pass;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3,
                                                                                        brand.Site.Community.CommunityID);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }

            string memberSalt = MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);
            string passwordHash = Security.Crypto.Instance.EncryptText(pass, memberSalt);
            int authenticate = MemberBL.Instance.Authenticate(member.EmailAddress, 3, passwordHash, out MemberID);
            Assert.AreEqual(0, authenticate);

            member.Password = newpass;
            member.MemberUpdate.Password = newpass;
            member.MemberUpdate.PasswordHash = MemberSA.Instance.EncryptPassword(newpass);

            MemberSaveResult saveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = saveResult.Username;
                member.CommitChanges();
            }

            int oldauthenticate = MemberBL.Instance.Authenticate(member.EmailAddress, 3, passwordHash, out MemberID);
            Assert.AreNotEqual(0, oldauthenticate);

            string newMemberSalt = MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);
            string newPasswordHash = Security.Crypto.Instance.EncryptText(newpass, newMemberSalt);

            int newauthenticate = MemberBL.Instance.Authenticate(member.EmailAddress, 3, newPasswordHash, out MemberID);
            Assert.AreEqual(0, newauthenticate);

            Console.WriteLine(string.Format("Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
        }

        [Test]
        public void RegisterAndChangeEmailTwiceTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest" + i + "@gmail.com";
            string newemail = "new" + email;
            string username = "testtesttest" + i;
            string pass = "test" + i;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3,
                                                                                        brand.Site.Community.CommunityID);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }

            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            string oldSalt = MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);
            Assert.AreEqual(email, member.EmailAddress);

            member.EmailAddress = newemail;
            MemberSaveResult saveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = saveResult.Username;
                member.CommitChanges();
            }

            string newEncryptedEmail = MemberSA.Instance.EncryptEmail(newemail);
            string newSalt = MemberBL.Instance.GetMemberSalt(newEncryptedEmail, 3);

            Assert.AreNotEqual(email, member.EmailAddress);
            Assert.AreEqual(newemail, member.EmailAddress);
            Assert.AreEqual(oldSalt, newSalt, "Old salt is not equal to new salt!");

            member.EmailAddress = email;
            MemberSaveResult saveResult2 = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult2.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = saveResult2.Username;
                member.CommitChanges();
            }

            string newSalt2 = MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);

            Assert.AreNotEqual(newemail, member.EmailAddress);
            Assert.AreEqual(email, member.EmailAddress);
            Assert.AreEqual(oldSalt, newSalt2, "Old salt is not equal to new salt!");

            Console.WriteLine(string.Format("Old Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
            Console.WriteLine(string.Format("New Email / EmailHash = {0} / {1}", newemail, newEncryptedEmail));
        }

        [Test]
        public void RegisterAndChangeEmailWithSameEmailTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest" + i + "@gmail.com";
            string newemail = "new" + email;
            string username = "testtesttest" + i;
            string pass = "test" + i;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3,
                                                                                        brand.Site.Community.CommunityID);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }

            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            string oldSalt = MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);
            Assert.AreEqual(email, member.EmailAddress);

            member.EmailAddress = email;
            MemberSaveResult saveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = saveResult.Username;
                member.CommitChanges();
            }

            string newEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            string newSalt = MemberBL.Instance.GetMemberSalt(newEncryptedEmail, 3);

            Assert.AreEqual(email, member.EmailAddress);
            Assert.AreEqual(oldSalt, newSalt, "Old salt is not equal to new salt!");

            Console.WriteLine(string.Format("Old Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
            //            Console.WriteLine(string.Format("New Email / EmailHash = {0} / {1}", newemail, newEncryptedEmail));
        }

        [Test]
        public void RegisterAndChangeEmailTwiceWithWrongCurrentEmailAddressTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest" + i + "@gmail.com";
            string newemail = "new" + email;
            string username = "testtesttest" + i;
            string pass = "test" + i;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3,
                                                                                        brand.Site.Community.CommunityID);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }

            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            string oldSalt = MemberBL.Instance.GetMemberSalt(oldEncryptedEmail, 3);
            Assert.AreEqual(email, member.EmailAddress);

            member.EmailAddress = newemail;
            MemberSaveResult saveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.CommitChanges();
            }

            string newEncryptedEmail = MemberSA.Instance.EncryptEmail(newemail);
            string newSalt = MemberBL.Instance.GetMemberSalt(newEncryptedEmail, 3);

            Assert.AreNotEqual(email, member.EmailAddress);
            Assert.AreEqual(newemail, member.EmailAddress);
            Assert.AreEqual(oldSalt, newSalt, "Old salt is not equal to new salt!");

            member.Use_Nunit = true;
            MemberBL.Instance.ExpireCachedMember(member.MemberID);
            member.MemberUpdate.EmailAddress = email;
            MemberSaveResult saveResult1 = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult1.SaveStatus == MemberSaveStatusType.Success)
            {
            member.CommitChanges();
            }
            member.MemberUpdate.EmailAddress = newemail;
            member.GetCachedMember().Version=3;
            CachedMemberBytes cachedMemberBytes = new CachedMemberBytes(member.MemberID, member.GetCachedMember().ToByteArray());
            cachedMemberBytes.Version = CachedMemberBytes.CACHED_MEMBERBYTES_VERSION;
            MemberBL.Instance._cache.Insert(CachedMemberBytes.GetCacheKey(member.MemberID), cachedMemberBytes);
            MemberSaveResult saveResult2 = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            if (saveResult2.SaveStatus == MemberSaveStatusType.Success)
            {
                member.CommitChanges();
            }
            
            string newSalt2 = MemberBL.Instance.GetMemberSalt(newEncryptedEmail, 3);

            Assert.AreNotEqual(email, member.EmailAddress);
            Assert.AreEqual(newemail, member.EmailAddress);
            Assert.AreEqual(oldSalt, newSalt2, "Old salt is not equal to new salt!");

            Console.WriteLine(string.Format("Old Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
            Console.WriteLine(string.Format("New Email / EmailHash = {0} / {1}", newemail, newEncryptedEmail));
        }

        [Test]
        public void SavePhotosNoMainTest()
        {
            int memberId = 100066873;

            var cachedMember = MemberBL.Instance.GetCachedMember(null, null, memberId, true, 3);
            if (!cachedMember.EmailAddress.ToLower().Contains("mcho"))
                return;

            var photoUpdates = new List<PhotoUpdate>();

            var photoCommunity = cachedMember.Photos.GetCommunity(3);
            // due to the nature of the test, we need at least 2 photos
            if (photoCommunity.Count < 2)
                return;

            for (int i = 0; i < photoCommunity.Count; i++)
            {
                var photo = photoCommunity[Convert.ToByte(i)];
                // hardcode photo approve status to true so we can control the test case
                photoUpdates.Add(new PhotoUpdate(null, false, photo.MemberPhotoID, photo.FileID, photo.FileWebPath,
                                                 photo.ThumbFileID, photo.ThumbFileWebPath,
                                                 photo.ListOrder, true, photo.IsPrivate, photo.AlbumID,
                                                 photo.AdminMemberID, photo.Caption, photo.IsCaptionApproved,
                                                 photo.FileCloudPath, photo.ThumbFileCloudPath, photo.IsApprovedForMain,
                                                 photo.IsMain, true));
            }

            // for whatever reason photos come back out of order although ListOrder is correct
            photoUpdates.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            // let's make sure we have no main photo and set the first photo as not main approved
            var howManyToNotMain = 1;
            var photoIdOfMain = 0;
            foreach (var phtUpdate in photoUpdates)
            {
                // mark every photo as not main but approved for main
                phtUpdate.IsMain = false;
                phtUpdate.IsApprovedForMain = true;

                // force first photo as not main approved
                if (howManyToNotMain > 0)
                {
                    phtUpdate.IsApprovedForMain = false;
                    howManyToNotMain--;
                }
                else if (howManyToNotMain == 0)
                {
                    // store the photo id of the photo that is to be the main photo
                    photoIdOfMain = phtUpdate.MemberPhotoID;
                    howManyToNotMain--;
                }
            }

            // Since we got no hydra thread running, it wont' write to the database.  it will just update the
            // item in cache which is good enough for our test.
            MemberBL.Instance.SavePhotos(null, 1003, 103, 3, memberId, photoUpdates.ToArray(), 3);

            cachedMember = MemberBL.Instance.GetCachedMember(null, null, memberId, false, 3);
            // check to see if we have at least 1 main photo
            photoCommunity = cachedMember.Photos.GetCommunity(3);
            var mainCount = 0;
            var photoIdOfMainAfter = -1;
            for (int i = 0; i < photoCommunity.Count; i++)
            {
                var photo = photoCommunity[Convert.ToByte(i)];
                if (photo.IsMain)
                {
                    mainCount++;
                    photoIdOfMainAfter = photo.MemberPhotoID;
                }
            }

            Assert.IsTrue(mainCount == 1 && photoIdOfMain == photoIdOfMainAfter);
    
        }

        [Test]
        public void SavePhotosMultipleMainTest()
        {
            int memberId = 100066873;

            var cachedMember = MemberBL.Instance.GetCachedMember(null, null, memberId, true, 3);
            if (!cachedMember.EmailAddress.ToLower().Contains("mcho"))
                return;

            var photoUpdates = new List<PhotoUpdate>();

            var photoCommunity = cachedMember.Photos.GetCommunity(3);
            // due to the nature of the test, we need at least 3 photos. slot one has specific logic so
            // even if you have multiple main photos, it will just select the first photo so we can't
            // test too much logic with that.
            if (photoCommunity.Count < 3)
                return;

            for (int i = 0; i < photoCommunity.Count; i++)
            {
                var photo = photoCommunity[Convert.ToByte(i)];
                // hardcode photo approve status to true so we can control the test case
                photoUpdates.Add(new PhotoUpdate(null, false, photo.MemberPhotoID, photo.FileID, photo.FileWebPath,
                                                 photo.ThumbFileID, photo.ThumbFileWebPath,
                                                 photo.ListOrder, true, photo.IsPrivate, photo.AlbumID,
                                                 photo.AdminMemberID, photo.Caption, photo.IsCaptionApproved,
                                                 photo.FileCloudPath, photo.ThumbFileCloudPath, photo.IsApprovedForMain,
                                                 photo.IsMain, true));
            }

            // for whatever reason photos come back out of order although ListOrder is correct
            photoUpdates.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            // first photo not main approved and not main, 2nd and 3rd main approved and both main
            var diffThanRest = 1;
            var photoIdOfMain = 0;
            foreach (var phtUpdate in photoUpdates)
            {
                phtUpdate.IsMain = true;
                phtUpdate.IsApprovedForMain = true;

                if (diffThanRest > 0)
                {
                    phtUpdate.IsApprovedForMain = false;
                    phtUpdate.IsMain = false;
                    diffThanRest--;
                }
                else if (diffThanRest == 0)
                {
                    // store the photo id of the photo that is to be the main photo
                    photoIdOfMain = phtUpdate.MemberPhotoID;
                    diffThanRest--;
                }
            }

            // Since we got no hydra thread running, it wont' write to the database.  it will just update the
            // item in cache which is good enough for our test.
            MemberBL.Instance.SavePhotos(null, 1003, 103, 3, memberId, photoUpdates.ToArray(), 3);

            cachedMember = MemberBL.Instance.GetCachedMember(null, null, memberId, false, 3);
            // check to see if we have only 1 main photo
            photoCommunity = cachedMember.Photos.GetCommunity(3);
            var mainCount = 0;
            var photoIdOfMainAfter = -1;
            for (int i = 0; i < photoCommunity.Count; i++)
            {
                var photo = photoCommunity[Convert.ToByte(i)];
                if (photo.IsMain)
                {
                    mainCount++;
                    photoIdOfMainAfter = photo.MemberPhotoID;
                }
            }

            Assert.IsTrue(mainCount == 1 && photoIdOfMain == photoIdOfMainAfter);

        }

        [Test]
        public void ChangeEmailBugTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest" + i + "@gmail.com";
            string newemail = "new" + email;
            string username = "testtesttest" + i;
            string pass = "test" + i;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3,
                                                                                        brand.Site.Community.CommunityID);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            username = null;
            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            RegisterStatusType registerStatusType = MemberBL.Instance.saveLogon(member.MemberID, newemail, ref username, string.Empty, 1003, email, -1);
            Assert.AreEqual(RegisterStatusType.Success, registerStatusType);
            Assert.AreEqual(MemberBL.Instance.GetCurrentEmailAddress(member.MemberID), newemail);
            RegisterStatusType registerStatusType2 = MemberBL.Instance.saveLogon(member.MemberID, email, ref username, string.Empty, 1003, email, -1);
            Assert.AreEqual(RegisterStatusType.InvalidEmail, registerStatusType2);
            Assert.AreEqual(MemberBL.Instance.GetCurrentEmailAddress(member.MemberID), newemail);

            Console.WriteLine(string.Format("Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
        }

        [Test]
        public void ChangePasswordBugTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            int i = new Random().Next();
            string email = "jdatetest" + i + "@gmail.com";
            string newemail = "new" + email;
            string username = "testtesttest" + i;
            string pass = "test" + i;
            string newpass = "newtest" + i;
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string newencryptedPass = MemberSA.Instance.EncryptPassword(newpass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveNewMember(null, member.MemberUpdate, 3,
                                                                                        brand.Site.Community.CommunityID);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            username = null;
            string oldEncryptedEmail = MemberSA.Instance.EncryptEmail(email);
            RegisterStatusType registerStatusType = MemberBL.Instance.saveLogon(member.MemberID, newemail, ref username, string.Empty, 1003, email, -1);
            Assert.AreEqual(RegisterStatusType.Success, registerStatusType);
            Assert.AreEqual(MemberBL.Instance.GetCurrentEmailAddress(member.MemberID), newemail);
            RegisterStatusType registerStatusType2 = MemberBL.Instance.saveLogon(member.MemberID, string.Empty, ref username, newencryptedPass, 1003, email, -1);
            Assert.AreEqual(RegisterStatusType.InvalidEmail, registerStatusType2);
            Assert.AreEqual(MemberBL.Instance.GetCurrentEmailAddress(member.MemberID), newemail);

            Console.WriteLine(string.Format("Email / EmailHash = {0} / {1}", email, oldEncryptedEmail));
        }

        [Test]
        public void SaveLogonRegisterTest()
        {
            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr  + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email,member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));
        }

        
        [Test]
        public void SaveLogonRegisterWithDuplicateEmailTest()
        {
            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            
            //Register with same email
            datestr = DateTime.Now.Ticks.ToString();
            string username2 = datestr + "test";
            memberRegisterResult = MemberBL.Instance.Register(email, username2, encryptedPass, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.AlreadyRegistered);
            Assert.AreEqual(memberRegisterResult.MemberID,0);

        }



        [Test]
        public void SaveLogonRegisterWithDuplicateUsernameTest()
        {
            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);

            //Register with same username. Generates a new username with random chars appended.
            string usernamenew = "";
            datestr = DateTime.Now.Ticks.ToString();
            string email2 = "jdateregtest" + datestr + "@gmail.com";
            string pass2 = "test1234";
            string encryptedPass2 = MemberSA.Instance.EncryptPassword(pass2);
            registrationSessionID = Guid.NewGuid().ToString();
            memberRegisterResult = MemberBL.Instance.Register(email2, username, encryptedPass2, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.Success);
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email2;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        usernamenew = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email2, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(usernamenew, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(usernamenew, member.GetUserName(brand));
            Assert.AreNotEqual(username, usernamenew);
        }

        [Test]
        public void SaveLogonRegisterReadFromNewTablesOnTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");

            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.Success);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Register with same email
            datestr = DateTime.Now.Ticks.ToString();
            string username2 = datestr + "test";
            memberRegisterResult = MemberBL.Instance.Register(email, username2, encryptedPass, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.AlreadyRegistered);
            Assert.AreEqual(memberRegisterResult.MemberID, 0);
        }

        [Test]
        public void SaveLogonRegisterWithDuplicateUsernameReadFromNewTablesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");
            
            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.Success);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);

            //Register with same username. Generates a new username with random chars appended.
            string usernamenew = "";
            datestr = DateTime.Now.Ticks.ToString();
            string email2 = "jdateregtest" + datestr + "@gmail.com";
            string pass2 = "test1234";
            string encryptedPass2 = MemberSA.Instance.EncryptPassword(pass2);
            memberRegisterResult = MemberBL.Instance.Register(email2, username, encryptedPass2, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.Success);

            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email2;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        usernamenew = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email2, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(usernamenew, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(usernamenew, member.GetUserName(brand));
            Assert.AreNotEqual(username, usernamenew);
        }


        [Test]
        public void SaveLogonUpdateEmailAddressTest()
        {
            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
           //update email
            string newEmail = "new" + email;
            member.EmailAddress = newEmail;
            MemberBL.Instance.SaveMember(null,member.MemberUpdate,3, brand.Site.Community.CommunityID);
            Assert.AreEqual(newEmail, MemberBL.Instance.GetCurrentEmailAddress(member.MemberID));
        }

        [Test]
        public void SaveLogonUpdateEmailAddressReadFromNewTablesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");
            
            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            Assert.AreEqual(memberRegisterResult.RegisterStatus, RegisterStatusType.Success);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //update email
            string newEmail = "new" + email;
            member.EmailAddress = newEmail;
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            Assert.AreEqual(newEmail, MemberBL.Instance.GetCurrentEmailAddress(member.MemberID));
        }

        [Test]
        public void SaveLogonUpdateWithDuplicateEmailAddressTest()
        {
            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email1 = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email1, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email1;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email1, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            string email2 = "jdateregtest2" + datestr + "@gmail.com";
            encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            registrationSessionID = Guid.NewGuid().ToString();
            memberRegisterResult = MemberBL.Instance.Register(email2, username, encryptedPass, brand);
            ServiceAdapters.Member member2 = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member2 = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member2.EmailAddress = email2;
                member2.SetUsername(memberRegisterResult.Username, brand);
                member2.SetAttributeInt(brand, "FTAApproved", 0);
                member2.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member2.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member2.MemberUpdate.Username = memberSaveResult.Username;
                        member2.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email2, member2.EmailAddress);
            //update with already existing email
            string newEmail = email1;
            member2.EmailAddress = newEmail;
            memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, brand.Site.Community.CommunityID);
            Assert.AreEqual(memberSaveResult.SaveStatus, MemberSaveStatusType.EmailAddressExists);
        }

        [Test]
        public void SaveLogonUpdateWithDuplicateEmailAddressReadFromNewTablesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");

            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email1 = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email1, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email1;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email1, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            string email2 = "jdateregtest2" + datestr + "@gmail.com";
            encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            registrationSessionID = Guid.NewGuid().ToString();
            memberRegisterResult = MemberBL.Instance.Register(email2, username, encryptedPass, brand);
            ServiceAdapters.Member member2 = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member2 = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member2.EmailAddress = email2;
                member2.SetUsername(memberRegisterResult.Username, brand);
                member2.SetAttributeInt(brand, "FTAApproved", 0);
                member2.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member2.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member2.MemberUpdate.Username = memberSaveResult.Username;
                        member2.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email2, member2.EmailAddress);
            //update with already existing email
            string newEmail = email1;
            member2.EmailAddress = newEmail;
            memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, brand.Site.Community.CommunityID);
            Assert.AreEqual(memberSaveResult.SaveStatus, MemberSaveStatusType.EmailAddressExists);
        }

        [Test]
        public void SaveLogonUpdateUsernameTest()
        {
            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));
            //update username
            string newusername = "new" + username;
            member.SetUsername(newusername,brand,TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username
            Assert.AreEqual(username, member.GetUserName(brand));
            //Approve username
            member.SetUsername(newusername, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(newusername, member.GetUserName(brand));

            //Save username from spark.com
            brand = BrandConfigSA.Instance.GetBrandByID(1001);
            member.SetUsername(newusername, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username or memberid
            Assert.AreEqual(member.GetUserName(brand), member.MemberID.ToString());
            //Approve username
            member.SetUsername(newusername, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(newusername, member.GetUserName(brand));

            //Update spark username
            member.SetUsername(username, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username or memberid
            //Assert.AreEqual(member.GetUserName(brand), member.MemberID.ToString());
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));
            string sparkusername = member.GetUserName(brand);
            brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string jdateusername = member.GetUserName(brand);
            Assert.AreNotEqual(sparkusername, jdateusername);
        }

        [Test]
        public void SaveLogonUpdateUsernameReadFromNewTablesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");

            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));
            //update username
            string newusername = "new" + username;
            member.SetUsername(newusername, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username
            Assert.AreEqual(username, member.GetUserName(brand));
            //Approve username
            member.SetUsername(newusername, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(newusername, member.GetUserName(brand));

            //Save username from spark.com
            brand = BrandConfigSA.Instance.GetBrandByID(1001);
            member.SetUsername(newusername, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username or memberid
            Assert.AreEqual(member.GetUserName(brand), member.MemberID.ToString());
            //Approve username
            member.SetUsername(newusername, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(newusername, member.GetUserName(brand));

            //Update spark username
            member.SetUsername(username, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username or memberid
            //Assert.AreEqual(member.GetUserName(brand), member.MemberID.ToString());
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));
            string sparkusername = member.GetUserName(brand);
            brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string jdateusername = member.GetUserName(brand);
            Assert.AreNotEqual(sparkusername, jdateusername);
        }

        [Test]
        public void SaveLogonUpdateWithDuplicateUsernameTest()
        {
            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email1 = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email1, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email1;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email1, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));

            //Member2
            string email2 = "jdateregtest2" + datestr + "@gmail.com";
            string username2 = "new" + username;
            encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            registrationSessionID = Guid.NewGuid().ToString();
            memberRegisterResult = MemberBL.Instance.Register(email2, username2, encryptedPass, brand);
            ServiceAdapters.Member member2 = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member2 = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member2.EmailAddress = email2;
                member2.SetUsername(memberRegisterResult.Username, brand);
                member2.SetAttributeInt(brand, "FTAApproved", 0);
                member2.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member2.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member2.MemberUpdate.Username = memberSaveResult.Username;
                        member2.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email2, member2.EmailAddress);
            //Approve username
            member2.SetUsername(username2, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member2.MemberUpdate.Username = memberSaveResult.Username;
                member2.CommitChanges();
            }
            Assert.AreEqual(username2, member2.GetUserName(brand));

            //Try to save member2 with already existing username, it will generate a username with random chars appended to the end.
            member2.SetUsername(username, brand, TextStatusType.Auto);
            var membersaveresult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Approve username
            member2.SetUsername(membersaveresult.Username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member2.MemberUpdate.Username = memberSaveResult.Username;
                member2.CommitChanges();
            }
            Assert.AreNotEqual(username, member2.GetUserName(brand));
        }

        [Test]
        public void SaveLogonUpdateWithDuplicateUsernameReadFromNewTablesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
            mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");

            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email1 = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email1, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email1;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email1, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username, member.GetUserName(brand));

            //Member2
            string email2 = "jdateregtest2" + datestr + "@gmail.com";
            string username2 = "new" + username;
            encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            registrationSessionID = Guid.NewGuid().ToString();
            memberRegisterResult = MemberBL.Instance.Register(email2, username2, encryptedPass, brand);
            ServiceAdapters.Member member2 = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member2 = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member2.EmailAddress = email2;
                member2.SetUsername(memberRegisterResult.Username, brand);
                member2.SetAttributeInt(brand, "FTAApproved", 0);
                member2.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member2.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member2.MemberUpdate.Username = memberSaveResult.Username;
                        member2.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email2, member2.EmailAddress);
            //Approve username
            member2.SetUsername(username2, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member2.MemberUpdate.Username = memberSaveResult.Username;
                member2.CommitChanges();
            }
            Assert.AreEqual(username2, member2.GetUserName(brand));

            //Try to save member2 with already existing username, it will generate a username with random chars appended to the end.
            member2.SetUsername(username, brand, TextStatusType.Auto);
            var membersaveresult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Approve username
            member2.SetUsername(membersaveresult.Username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member2.MemberUpdate.Username = memberSaveResult.Username;
                member2.CommitChanges();
            }
            Assert.AreNotEqual(username, member2.GetUserName(brand));
        }

        [Test]
        public void SaveLogonUpdateUsernameTestWithBlankSpaces()
        {
            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test   ";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(username, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(username.Trim(), member.GetUserName(brand));
            //update username
            string newusername = "new" + username;
            member.SetUsername(newusername, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username
            Assert.AreEqual(username.Trim(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(newusername, brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(newusername.Trim(), member.GetUserName(brand));
        }

        [Test]
        public void SaveLogonUpdateUsernameSubStringOfEmailTest()
        {
            MemberSaveResult memberSaveResult;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = "jdateregtest" + datestr;
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            string registrationSessionID = Guid.NewGuid().ToString();
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);
            //Username before admin approval should be equal to memberId
            Assert.AreEqual(memberRegisterResult.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(memberRegisterResult.MemberID.ToString(), brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(member.MemberID.ToString(), member.GetUserName(brand));
            //update username
            string newusername = username;
            member.SetUsername(newusername, brand, TextStatusType.Auto);
            MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            //Username before admin approval should be equal to last approved username
            Assert.AreEqual(member.MemberID.ToString(), member.GetUserName(brand));
            //Approve username
            member.SetUsername(member.MemberID.ToString(), brand, TextStatusType.Human);
            memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);
            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                member.MemberUpdate.Username = memberSaveResult.Username;
                member.CommitChanges();
            }
            Assert.AreEqual(member.MemberID.ToString(), member.GetUserName(brand));
        }

        [Test]
         public void SaveLogonUpdatePasswordTest()
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);

            //update password
            string newpass = "newtest1234";
            string newencryptedPass = MemberSA.Instance.EncryptPassword(newpass);
            int memberId;
            member.Password = newencryptedPass;
            MemberSaveResult memberResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
            string encryptEmail = MemberSA.Instance.EncryptEmail(email);
            string memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail, 3);
            string passwordHash = Security.Crypto.Instance.EncryptText(newpass, memberSalt);
            int authenticate = MemberBL.Instance.Authenticate(email, 3, passwordHash, out memberId);
            Assert.AreEqual(0, authenticate);

        }

        [Test]
            public void SaveLogonUpdatePasswordReadFromNewTablesTest()
            {
                mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
                mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST", "false");
                mockSettingsService.AddSetting("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD", "false");

                MemberBL.SettingsService = mockSettingsService;

                Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
                string datestr = DateTime.Now.Ticks.ToString();
                string email = "jdateregtest" + datestr + "@gmail.com";
                string username = datestr + "test";
                string pass = "test1234";
                string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

                MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

                ServiceAdapters.Member member = null;
                if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
                {
                    member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                    member.EmailAddress = email;
                    member.SetUsername(memberRegisterResult.Username, brand);
                    member.SetAttributeInt(brand, "FTAApproved", 0);
                    if (member.MemberUpdate.IsDirty)
                    {
                        MemberSaveResult memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                            Constants.NULL_INT);
                        if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                        {
                            member.MemberUpdate.Username = memberSaveResult.Username;
                            member.CommitChanges();
                        }
                    }
                }
                Assert.AreEqual(email, member.EmailAddress);

                //update password
                string newpass = "newtest1234";
                string newencryptedPass = MemberSA.Instance.EncryptPassword(newpass);
                int memberId;
                member.Password = newencryptedPass;
                MemberSaveResult memberResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);
                string encryptEmail = MemberSA.Instance.EncryptEmail(email);
                string memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail,3);
                string passwordHash = Security.Crypto.Instance.EncryptText(newpass, memberSalt);
                int authenticate = MemberBL.Instance.Authenticate(email,3, passwordHash, out memberId); 
                Assert.AreEqual(0, authenticate);

            }


        [Test]
        public void SaveUsernameOnMultipleSitesInSameCommunityTest()
        {
            var username = "bcool8";
            int memberId = 111646674;
            //Before admin approval
            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            member.SetUsername(username, brand);
            MemberSaveResult memberResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, brand.Site.Community.CommunityID);

            //After admin approval
            member.SetAttributeText(brand, "username", username, TextStatusType.Human);
            memberResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3, Constants.NULL_INT);

            var cachedMember = new CachedMember(MemberBL.Instance.GetCachedMemberBytes(null,null,memberId,false, 5), 5);
            //Check username on jdate.fr
            TextStatusType type;
            var username2 = cachedMember.GetAttributeText(2486, 8,"", out type);
            Assert.AreEqual(username,username2);

            var username3 = cachedMember.GetAttributeText(2486,2, "", out type);
            Assert.AreEqual(username3, username2);
        }

        [Test]
        public void mnLogonGettersTest()
        {
            MemberBL.SettingsService = null;
            
            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);

            //MemberId by username
            int memberIdRetrieved = MemberBL.Instance.GetMemberID(username, brand.Site.Community.CommunityID);
            int memberIdRetrievedWithoutCommunityId = MemberBL.Instance.GetMemberID(username);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrieved);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrievedWithoutCommunityId);

            //MemberId by email
            memberIdRetrieved = MemberBL.Instance.GetMemberIDByEmail(email);
            memberIdRetrievedWithoutCommunityId = MemberBL.Instance.GetMemberIDByEmail(email,3);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrieved);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrievedWithoutCommunityId);

            //Get Members list by email
            int totalRows = 0;
            var membersList = MemberBL.Instance.GetMembersByEmail(null, null, email, 1, 100, ref totalRows);
            Assert.AreEqual(1,totalRows);

        }

        [Test]
        public void mnLogonGettersReadFromNewTablesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            MemberBL.SettingsService = mockSettingsService;

            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);

            //MemberId by username
            int memberIdRetrieved = MemberBL.Instance.GetMemberID(username, brand.Site.Community.CommunityID);
            int memberIdRetrievedWithoutCommunityId = MemberBL.Instance.GetMemberID(username);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrieved);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrievedWithoutCommunityId);

            //MemberId by email
            memberIdRetrieved = MemberBL.Instance.GetMemberIDByEmail(email);
            memberIdRetrievedWithoutCommunityId = MemberBL.Instance.GetMemberIDByEmail(email, 3);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrieved);
            Assert.AreEqual(memberRegisterResult.MemberID, memberIdRetrievedWithoutCommunityId);

            //Get Members list by email
            int totalRows = 0;
            var membersList = MemberBL.Instance.GetMembersByEmail(null, null, email, 1, 100, ref totalRows);
            Assert.AreEqual(1, totalRows);

        }

        [Test]
        public void mnLogonLoginMultipleCommunitiesTest()
        {
            mockSettingsService.AddSetting("MNLOGON_READ_FROM_NEW_TABLES", "true");
            mockSettingsService.AddSetting("MNLOGON_BH_COMMUNITY_LIST", "1,3,10,23,24");
            MemberBL.SettingsService = mockSettingsService;
            int memberId = 0;

            //Register on jdate
            MemberSaveResult memberSaveResult;
            string registrationSessionID = Guid.NewGuid().ToString();
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            string datestr = DateTime.Now.Ticks.ToString();
            string email = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);
            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);
            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                memberId = memberRegisterResult.MemberID;
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
                if (member.MemberUpdate.IsDirty)
                {
                    memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            Assert.AreEqual(email, member.EmailAddress);

            //Authenticate on jdate
            string encryptEmail = MemberSA.Instance.EncryptEmail(email);
            string memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail, 3);
            string passwordHash = Security.Crypto.Instance.EncryptText(pass, memberSalt);
            int authenticate = MemberBL.Instance.Authenticate(email, 3, passwordHash, out memberId);
            Assert.AreEqual(0, authenticate);

            //Register on BBW
            brand = BrandConfigSA.Instance.GetBrandByID(90410);
            ServiceAdapters.Member member2 = null;
            member2 = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
            //member2.EmailAddress = email2;
            member2.SetUsername(memberRegisterResult.Username, brand);
            member2.SetAttributeInt(brand, "FTAApproved", 0);
            member2.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
            if (member2.MemberUpdate.IsDirty)
            {
                memberSaveResult = MemberBL.Instance.SaveMember(null, member2.MemberUpdate, 3, 23);
                if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                {
                    member2.MemberUpdate.Username = memberSaveResult.Username;
                    member2.CommitChanges();
                }
            }
            //Authenticate on BBW
            memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail, 23);
            passwordHash = Security.Crypto.Instance.EncryptText(pass, memberSalt);
            authenticate = MemberBL.Instance.Authenticate(email, 23, passwordHash, out memberId);
            Assert.AreEqual(0, authenticate);

            //Register on Spark
            brand = BrandConfigSA.Instance.GetBrandByID(1001);
            ServiceAdapters.Member member3 = null;
            member3 = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
            //member2.EmailAddress = email2;
            member3.SetUsername(memberRegisterResult.Username, brand);
            member3.SetAttributeInt(brand, "FTAApproved", 0);
            member3.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);
            if (member3.MemberUpdate.IsDirty)
            {
                memberSaveResult = MemberBL.Instance.SaveMember(null, member3.MemberUpdate, 3, 1);
                if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                {
                    member3.MemberUpdate.Username = memberSaveResult.Username;
                    member3.CommitChanges();
                }
            }
            //Authenticate on spark
            memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail, 1);
            passwordHash = Security.Crypto.Instance.EncryptText(pass, memberSalt);
            authenticate = MemberBL.Instance.Authenticate(email, 1, passwordHash, out memberId);
            Assert.AreEqual(0, authenticate);

            //Authenticate on non-BH community
            memberSalt = MemberBL.Instance.GetMemberSalt(encryptEmail, 100);
            Assert.IsNull(memberSalt);
            authenticate = MemberBL.Instance.Authenticate(email, 100, "", out memberId);
            Assert.AreEqual(-1, authenticate);
        }

        [Test]
        public void CheckEmailAddressExistsTest()
        {
            string datestr = DateTime.Now.Ticks.ToString();
            string emailAddress = "jdateregtest" + datestr + "@gmail.com";
            string username = datestr + "test";
            string pass = "test1234";
            int brandId = 1003;

            //Not a member of BH and Mingle site
            bool emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 3);//BH
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 52);//Believe
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 29);//Adventist
            Assert.AreEqual(false, emailExists);

            //Member of BH and not Mingle
            int memberId = Register(emailAddress, username, pass, brandId); //Register on jdate.
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 3);//BH
            Assert.AreEqual(true, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 1);//BH - Spark
            Assert.AreEqual(true, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 52);//Believe
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 29);//Adventist
            Assert.AreEqual(false, emailExists);

            //Member of both Mingle and BH sites
            memberId = Register(emailAddress, username, pass, 90110); //Register on Adventist
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 3);//BH
            Assert.AreEqual(true, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 1);//BH - Spark
            Assert.AreEqual(true, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 52);//Believe
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 29);//Adventist
            Assert.AreEqual(true, emailExists);

            //Member of Mingle and not BH sites
            emailAddress = "jdateregtest2" + datestr + "@gmail.com";
            username = datestr + "test";
            pass = "test1234";
            memberId = Register(emailAddress, username, pass, 90110); //Register on Adventist 
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 3);//BH
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 1);//BH - Spark
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 52);//Believe
            Assert.AreEqual(false, emailExists);
            emailExists = MemberBL.Instance.CheckEmailAddressExists(emailAddress, 29);//Adventist
            Assert.AreEqual(true, emailExists);

        }

        private int Register(string email, string username, string pass, int brandId)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            string encryptedPass = MemberSA.Instance.EncryptPassword(pass);

            MemberRegisterResult memberRegisterResult = MemberBL.Instance.Register(email, username, encryptedPass, brand);

            ServiceAdapters.Member member = null;
            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                member = MemberSA.Instance.GetMember(memberRegisterResult.MemberID, MemberLoadFlags.IngoreSACache);
                member.EmailAddress = email;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeInt(brand, "FTAApproved", 0);
                if (member.MemberUpdate.IsDirty)
                {
                    MemberSaveResult memberSaveResult = MemberBL.Instance.SaveMember(null, member.MemberUpdate, 3,
                                                                                        Constants.NULL_INT);
                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
            }
            
            return member.MemberID;
        }

        [Test]
        public void TestMemberBLDTODisposal()
        {
            MemberBL.SettingsService = null;
            int length = TestUtils.ExistingDevMemberIds.Length;
            IMemberDTO[] memberDtOs=null;
            for (int i = 0; i < 50; i++)
            {
                memberDtOs = MemberBL.Instance.GetMemberDTOs(null, null, TestUtils.ExistingDevMemberIds, MemberType.Search, false);
                Assert.AreEqual(length, memberDtOs.Length);
                foreach (IMemberDTO memberDto in memberDtOs)
                {
                    Assert.IsNotNull(memberDto.GetLastLogonDate(3));
                    if (memberDto is MemberDTO)
                    {
                        ((MemberDTO)memberDto).Dispose();
                    }
                }
            }
        }

		[Test]
        public void CompareMemberDTOSearchResultMemUsage()
		{
		    RunMemoryUsageForMemberType(MemberType.Search);
		}

        [Test]
        public void CompareMemberDTOMatchMailMemUsage()
        {
            RunMemoryUsageForMemberType(MemberType.MatchMail);
        }

        [Test]
        public void TestAddMemberNotifications()
        {
            int memberID = 1;
            int appID = 1077;
            string deviceID = "deviceID";
            string deviceName = "deviceName";
            string deviceData = "deviceData";
            long pushNotificationsMap = 6;
            //MemberBL.Instance.AddMemberNotifications(memberID, appID, deviceID, deviceName, deviceData, pushNotificationsMap);
        }

        private void RunMemoryUsageForMemberType(MemberType memberType)
        {
            MemberBL.SettingsService = null;
            int length = TestUtils.ExistingDevMemberIds.Length;
            Member.ServiceAdapters.Member[] members = new ServiceAdapters.Member[length];
            long oldSize = GC.GetTotalMemory(true);
            Console.WriteLine("Mem is currently " + (oldSize/1000) + " KB.");
            string clientHostName = "host";
            DateTime expirationDate = DateTime.Now.AddDays(3);
            CacheReference cacheReference = new CacheReference("blah", 8091, ProtocolType.Http, expirationDate);
            for (int i = 0; i < length; i++)
            {
                ServiceAdapters.Member member = MemberSA.Instance.GetMember(TestUtils.ExistingDevMemberIds[i], MemberLoadFlags.IngoreSACache);
                member.Use_Nunit = true;
                members[i] = member;
                member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.BasicSubscription, 1003, 103, 3);
            }
            long newSize = GC.GetTotalMemory(false);
            Console.WriteLine("Mem is currently " + (newSize/1000) + " KB.");
            long totalMemDeltaBytes = (newSize - oldSize);
            long totalMemDeltaKB = (totalMemDeltaBytes/1000);
            Console.WriteLine("Mem delta is " + totalMemDeltaKB + " KB.");
            double d = ((double) totalMemDeltaBytes/(double) length);
            Console.WriteLine("Each full member object is " + d + " bytes.");

            IMemberDTO[] mockMembers = new IMemberDTO[length];
            oldSize = GC.GetTotalMemory(true);
            Console.WriteLine("Mem is currently " + (oldSize/1000) + " KB.");
            for (int i = 0; i < length; i++)
            {
                mockMembers[i] = MemberDTOFactory.Instance.GetIMemberDTO(members[i].GetCachedMember(),
                                                                         members[i].GetCachedMemberAccess(), memberType);
            }
            newSize = GC.GetTotalMemory(false);
            Console.WriteLine("Mem is currently " + (newSize/1000) + " KB.");
            totalMemDeltaBytes = (newSize - oldSize);
            totalMemDeltaKB = (totalMemDeltaBytes/1000);
            Console.WriteLine("Mem delta is " + totalMemDeltaKB + " KB.");
            d = ((double) totalMemDeltaBytes/(double) mockMembers.Length);
            Console.WriteLine("Each "+memberType+" member object is " + d + " bytes.");
        }
    }
}
