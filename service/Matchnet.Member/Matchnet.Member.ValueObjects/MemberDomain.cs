using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Summary description for MemberDomain.
	/// </summary>
	[Serializable]
	public class MemberDomain
	{
		private int _memberDomainID;
		private int _privateLabelID;
		private DateTime _createDate;
		private DateTime _renewDate;
		private DateTime _lastActiveDate;
		private bool _selfSuspendedFlag;
		private int _memberID;
		private bool _adminSuspendedFlag;
		private string _emailAddress;
		private string _privateLabelDescription;
		private string _privateLabelURI;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberDomainID"></param>
		/// <param name="privateLabelID"></param>
		/// <param name="createDate"></param>
		/// <param name="renewDate"></param>
		/// <param name="lastActiveDate"></param>
		/// <param name="selfSuspendedFlag"></param>
		/// <param name="memberID"></param>
		/// <param name="adminSuspendedFlag"></param>
		/// <param name="emailAddress"></param>
		/// <param name="privateLabelDescription"></param>
		/// <param name="privateLabelURI"></param>
		public MemberDomain(int memberDomainID, int privateLabelID, DateTime createDate, DateTime renewDate, DateTime lastActiveDate,
							bool selfSuspendedFlag, int memberID, bool adminSuspendedFlag, string emailAddress, string privateLabelDescription, string privateLabelURI)
		{
			_memberDomainID = memberDomainID;
			_privateLabelID = privateLabelID;
			_createDate = createDate;
			_renewDate = renewDate;
			_lastActiveDate = lastActiveDate;
			_selfSuspendedFlag = selfSuspendedFlag;
			_memberID = memberID;
			_adminSuspendedFlag = adminSuspendedFlag;
			_emailAddress = emailAddress;
			_privateLabelDescription = privateLabelDescription;
			_privateLabelURI = privateLabelURI;
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberDomainID
		{
			get { return(_memberDomainID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PrivateLabelID
		{
			get { return(_privateLabelID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime CreateDate
		{
			get { return(_createDate); }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime RenewDate
		{
			get { return(_renewDate); }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime LastActiveDate
		{
			get { return(_lastActiveDate); }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool SelfSuspendedFlag
		{
			get { return(_selfSuspendedFlag); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get { return(_memberID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool AdminSuspendedFlag
		{
			get { return(_adminSuspendedFlag); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get { return(_emailAddress); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string PrivateLabelDescription
		{
			get { return(_privateLabelDescription); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string PrivateLabelURI
		{
			get { return(_privateLabelURI); }
		}
	}
}
