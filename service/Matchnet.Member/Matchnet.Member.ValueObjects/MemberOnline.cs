using System;

using Matchnet;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MemberOnline : IValueObject, ICacheable, IReplicable
	{
		private const string CACHE_KEY_PREFIX = "MemberOnline";

		private int _cacheTTLSeconds = 180;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private int _communityID;
		private int _memberID;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		public MemberOnline(int communityID, int memberID)
		{
			_communityID = communityID;
			_memberID = memberID;
		}


		#region public properties
		/// <summary>
		/// 
		/// </summary>
		public int CommunityID
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int communityID, int memberID)
		{
			return CACHE_KEY_PREFIX + communityID.ToString() + ":" + memberID.ToString();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return MemberOnline.GetCacheKey(_communityID, _memberID);
		}
		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
