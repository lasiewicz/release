﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Member.ValueObjects.Admin
{
    [Serializable]
    public class ImpersonateToken
    {
        public string TokenGUID { get; set; }
        public int AdminMemberId { get; set; }
        public int ImpersonateMemberId { get; set; }
        public int ImperonateBrandId { get; set; }        
    }
}
