using System;

namespace Matchnet.Member.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminMemberDomainMapperCollection.
	/// </summary>
	[Serializable]
	public class AdminMemberDomainMapperCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{
		public AdminMemberDomainMapperCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int Add(AdminMemberDomainMapper item)
		{
			return base.InnerList.Add(item);
		}

		public AdminMemberDomainMapper GetAdminMemberDomainMapper(int memberID)
		{
			AdminMemberDomainMapper item = null;
			foreach (AdminMemberDomainMapper i in base.InnerList)
			{
				if (i.MemberID == memberID)
				{
					item = i;
					break;
				}
			}
			return item;
		}

		public AdminMemberDomainMapper GetAdminMemberDomainMapper(string domainAccount)
		{
            AdminMemberDomainMapper item = null;
            
			if (domainAccount != null && domainAccount != "")
			{
				foreach (AdminMemberDomainMapper i in base.InnerList)
				{
                    
                    if (i.DomainAccount.ToLower().Trim() == domainAccount.ToLower().Trim())
                    {
                        item = i;
                        break;
                    }
            	}
			}
			return item;
		}

		#region ICacheable Members
		private int _cacheTTLSeconds = 3600; //1 hour
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
		const string CACHEKEYFORMAT = "AdminMemberDomainMapperCol";

		public CacheItemMode CacheMode
		{
			get { return _cacheItemMode; }
			set { _cacheItemMode = value; }
		}

		public CacheItemPriorityLevel CachePriority
		{
			get { return _cachePriority; }
			set { _cachePriority = value; }
		}

		public int CacheTTLSeconds
		{
			get { return _cacheTTLSeconds; }
			set { _cacheTTLSeconds = value; }
		}

		public string GetCacheKey()
		{
			return GetCacheKeyString();
		}

		public static string GetCacheKeyString()
		{
			return CACHEKEYFORMAT;
		}

		#endregion
	}
}
