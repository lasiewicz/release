using System;

namespace Matchnet.Member.ValueObjects.Admin
{
	/// <summary>
	/// Represents admin bedrock memberID and matchnet domain account mapping
	/// </summary>
	[Serializable]
	public class AdminMemberDomainMapper : IValueObject
	{
		private int _MemberID;
		private string _DomainAccount;
		private DateTime _InsertDate = DateTime.MinValue;
		private DateTime _UpdateDate = DateTime.MinValue;

		public AdminMemberDomainMapper()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public int MemberID
		{
			get { return _MemberID;}
			set { _MemberID = value;}
		}

		public string DomainAccount
		{
			get { return _DomainAccount;}
			set { _DomainAccount = value;}
		}

		public DateTime InsertDate
		{
			get { return _InsertDate;}
			set { _InsertDate = value;}
		}

		public DateTime UpdateDate
		{
			get { return _UpdateDate;}
			set { _UpdateDate = value;}
		}
	}
}
