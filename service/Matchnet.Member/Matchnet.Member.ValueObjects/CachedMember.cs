using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.CacheSynchronization.Tracking;


namespace Matchnet.Member.ValueObjects
{
	[Flags]
	public enum AttributeOptionMailboxPreference
	{
		IncludeOriginalMessagesInReplies = 1,
		SaveCopiesOfSentMessages = 2
	}
	
	/// <summary>
	/// Global status mask attribute
	/// </summary>
	[Flags]
	public enum GlobalStatusMask : int 
	{
		/// <summary>
		/// 
		/// </summary>
		Default = 0,
		/// <summary>
		/// Admin suspended (1 means suspended)
		/// </summary>
		AdminSuspended = 1,
		/// <summary>
		///  Email is bad (1 means it's bad)
		/// </summary>
		BouncedEmail = 2,
		/// <summary>
		/// Email verification (1 means verified, 0 means not verifies)
		/// </summary>
		VerifiedEmail = 4,
		/// <summary>
		/// Purged by Marti Masri (1 means it was purged)
		/// </summary>
		Archived = 8
	}

	[Flags]
	public enum PhotoStatusMask
	{
		
		/// <summary>
		/// This indicates member purchased subscription after AS was converted to spark.com
		/// in which case at least 1 approved photo is required.
		/// </summary>
		PhotoRequired = 1 
	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class CachedMember : Matchnet.IValueObject, ICacheable, IReplicable, ISerializable, IByteSerializable
	{
		private const string CACHE_KEY_PREFIX = "Member";

		/// <summary> </summary>
		public const Int32 GROUP_PERSONALS = 8383;
		/// <summary> </summary>
		public const Int32 AG_GLOBALSTATUSMASK = 445;

        #region AttributeID constants
	    public const Int32 ATTRIBUTEID_MAINPHOTOSETATTEMPTED = 745;
        public const Int32 ATTRIBUTEID_ABOUTME = 124;
		public const Int32 ATTRIBUTEID_ACTIVITYLEVEL = 149;
		public const Int32 ATTRIBUTEID_AREACODE1 = 458;
		public const Int32 ATTRIBUTEID_AREACODE2 = 460;
		public const Int32 ATTRIBUTEID_AREACODE3 = 462;
		public const Int32 ATTRIBUTEID_AREACODE4 = 464;
		public const Int32 ATTRIBUTEID_AREACODE5 = 466;
		public const Int32 ATTRIBUTEID_AREACODE6 = 468;
		public const Int32 ATTRIBUTEID_AWAYMESSAGE = 286;
		public const Int32 ATTRIBUTEID_BIRTHCOUNTRYREGIONID = 398;
		public const Int32 ATTRIBUTEID_BIRTHDATE = 70;
		public const Int32 ATTRIBUTEID_BODYTYPE = 87;
		public const Int32 ATTRIBUTEID_BRANDINSERTDATE = 497;
		public const Int32 ATTRIBUTEID_BRANDLASTLOGONDATE = 495;
		public const Int32 ATTRIBUTEID_BRANDLOGONCOUNT = 496;
		public const Int32 ATTRIBUTEID_CHATBANNED = 506;
		public const Int32 ATTRIBUTEID_CHILDRENCOUNT = 370;
		public const Int32 ATTRIBUTEID_COMMUNITYEMAILCOUNT = 499;
		public const Int32 ATTRIBUTEID_CONTACTUSLIST = 218;
		public const Int32 ATTRIBUTEID_CUISINE = 109;
		public const Int32 ATTRIBUTEID_CUSTODY = 107;
		public const Int32 ATTRIBUTEID_DESIREDDRINKINGHABITS = 388;
		public const Int32 ATTRIBUTEID_DESIREDEDUCATIONLEVEL = 386;
		public const Int32 ATTRIBUTEID_DESIREDJDATERELIGION = 382;
		public const Int32 ATTRIBUTEID_DESIREDMARITALSTATUS = 380;
		public const Int32 ATTRIBUTEID_DESIREDMAXAGE = 378;
		public const Int32 ATTRIBUTEID_DESIREDMINAGE = 376;
		public const Int32 ATTRIBUTEID_DESIREDSMOKINGHABITS = 390;
		public const Int32 ATTRIBUTEID_DISPLAYPHOTOSTOGUESTS = 76;
		public const Int32 ATTRIBUTEID_DISTANCE = 256;
		public const Int32 ATTRIBUTEID_DRINKINGHABITS = 93;
		public const Int32 ATTRIBUTEID_EDUCATIONLEVEL = 89;
		public const Int32 ATTRIBUTEID_EMAILADDRESS = 12;
		public const Int32 ATTRIBUTEID_ENTERTAINMENTLOCATION = 117;
		public const Int32 ATTRIBUTEID_ETHNICITY = 88;
		public const Int32 ATTRIBUTEID_EYECOLOR = 86;
        public const Int32 ATTRIBUTEID_FAVORITETIMEOFDAY = 100;
		public const Int32 ATTRIBUTEID_GENDERMASK = 69;
		public const Int32 ATTRIBUTEID_GLOBALSTATUSMASK = 268;
		public const Int32 ATTRIBUTEID_GOTACLICKEMAILPERIOD = 204;
		public const Int32 ATTRIBUTEID_GREWUPIN = 246;
		public const Int32 ATTRIBUTEID_HAIRCOLOR = 85;
		public const Int32 ATTRIBUTEID_HASNEWMAIL = 503;
		public const Int32 ATTRIBUTEID_HASNEWMUTUALYES = 236;
		public const Int32 ATTRIBUTEID_HASPHOTOFLAG = 180;
		public const Int32 ATTRIBUTEID_HEIGHT = 83;
		public const Int32 ATTRIBUTEID_HIDEMASK = 394;
		public const Int32 ATTRIBUTEID_IDEALRELATIONSHIPESSAY = 139;
		public const Int32 ATTRIBUTEID_INCOMELEVEL = 147;
		public const Int32 ATTRIBUTEID_INDUSTRYTYPE = 392;
		public const Int32 ATTRIBUTEID_ISRAELAREACODE = 174;
		public const Int32 ATTRIBUTEID_JDATEETHNICITY = 362;
		public const Int32 ATTRIBUTEID_JDATERELIGION = 364;
		public const Int32 ATTRIBUTEID_KEEPKOSHER = 368;
		public const Int32 ATTRIBUTEID_LANGUAGEMASK = 374;
		public const Int32 ATTRIBUTEID_LASTBRANDID = 452;
		public const Int32 ATTRIBUTEID_LASTEMAILRECEIVEDATE = 502;
		public const Int32 ATTRIBUTEID_LASTUPDATED = 120;
		public const Int32 ATTRIBUTEID_LEARNFROMTHEPASTESSAY = 141;
		public const Int32 ATTRIBUTEID_LEISUREACTIVITY = 115;
		public const Int32 ATTRIBUTEID_LUGGAGE = 272;
		public const Int32 ATTRIBUTEID_MAILBOXPREFERENCE = 354;
		public const Int32 ATTRIBUTEID_MAJORTYPE = 432;
		public const Int32 ATTRIBUTEID_MARITALSTATUS = 32;
		public const Int32 ATTRIBUTEID_MATCHNEWSLETTERPERIOD = 344;
		public const Int32 ATTRIBUTEID_MAXAGE = 484;
		public const Int32 ATTRIBUTEID_MAXHEIGHT = 474;
		public const Int32 ATTRIBUTEID_MEMBERID = 2;
		public const Int32 ATTRIBUTEID_MINAGE = 482;
		public const Int32 ATTRIBUTEID_MINHEIGHT = 472;
		public const Int32 ATTRIBUTEID_MORECHILDRENFLAG = 250;
		public const Int32 ATTRIBUTEID_MOVIEGENREMASK = 418;
		public const Int32 ATTRIBUTEID_MUSIC = 78;
		public const Int32 ATTRIBUTEID_NEWSLETTERMASK = 346;
		public const Int32 ATTRIBUTEID_NEWMEMBEREMAILPERIOD = 562;
		public const Int32 ATTRIBUTEID_NEXTREGISTRATIONACTIONPAGEID = 126;
		public const Int32 ATTRIBUTEID_OCCUPATIONDESCRIPTION = 145;
		public const Int32 ATTRIBUTEID_PACKAGETYPE = 300;
		public const Int32 ATTRIBUTEID_PERFECTFIRSTDATEESSAY = 137;
		public const Int32 ATTRIBUTEID_PERFECTMATCHESSAY = 125;
		public const Int32 ATTRIBUTEID_PERSONALITYTRAIT = 97;
		public const Int32 ATTRIBUTEID_PETS = 240;
		public const Int32 ATTRIBUTEID_PHOTOREJECTREASON = 127;
		public const Int32 ATTRIBUTEID_PHYSICALACTIVITY = 116;
		public const Int32 ATTRIBUTEID_PLANETS = 206;
		public const Int32 ATTRIBUTEID_POLITICALORIENTATION = 102;
		public const Int32 ATTRIBUTEID_PROMOTIONID = 79;
		public const Int32 ATTRIBUTEID_READING = 188;
		public const Int32 ATTRIBUTEID_REGIONID = 122;
		public const Int32 ATTRIBUTEID_REGISTRATIONREFERRERPAGE = 504;
		public const Int32 ATTRIBUTEID_REGISTRATIONTARGETPAGE = 505;
		public const Int32 ATTRIBUTEID_RELATIONSHIPMASK = 262;
		public const Int32 ATTRIBUTEID_RELATIONSHIPSTATUS = 414;
		public const Int32 ATTRIBUTEID_RELIGION = 95;
		public const Int32 ATTRIBUTEID_RELOCATEFLAG = 244;
		public const Int32 ATTRIBUTEID_REPORTABUSECOUNT = 507;
		public const Int32 ATTRIBUTEID_REPORTABUSEREASON = 508;
		public const Int32 ATTRIBUTEID_SCHOOLID = 436;
		public const Int32 ATTRIBUTEID_SEARCHORDERBY = 476;
		public const Int32 ATTRIBUTEID_SEARCHTYPEID = 480;
		public const Int32 ATTRIBUTEID_SELFSUSPENDEDFLAG = 270;
		public const Int32 ATTRIBUTEID_SELFSUSPENDEDREASONTYPE = 266;
		public const Int32 ATTRIBUTEID_SEXUALIDENTITYTYPE = 328;
		public const Int32 ATTRIBUTEID_SMOKINGHABITS = 92;
		public const Int32 ATTRIBUTEID_SMSBALANCE = 200;
		public const Int32 ATTRIBUTEID_SMSCAPABLE = 176;
		public const Int32 ATTRIBUTEID_STUDIESEMPHASIS = 252;
		public const Int32 ATTRIBUTEID_SUBSCRIPTIONEXPIRATIONDATE = 501;
		public const Int32 ATTRIBUTEID_SUSPECTWORDS = 310;
		public const Int32 ATTRIBUTEID_SYNAGOGUEATTENDANCE = 366;
		public const Int32 ATTRIBUTEID_TEASE = 334;
		public const Int32 ATTRIBUTEID_USERNAME = 302;
		public const Int32 ATTRIBUTEID_VACATIONSTARTDATE = 430;
		public const Int32 ATTRIBUTEID_WEIGHT = 84;
		public const Int32 ATTRIBUTEID_WHENIHAVETIME = 186;
		public const Int32 ATTRIBUTEID_ZODIAC = 94;
		public const Int32 ATTRIBUTEID_REGISTRATIONIP = 509;
	    public const Int32 ATTRIBUTEID_FTAAPPROVED = 733;
        #endregion

        public const Int32 ATTRIBUTEOPTION_HIDE_SEARCH = 1;
		public const Int32 ATTRIBUTEOPTION_SUSPEND_ADMIN = 1;
		public const Int32 ATTRIBUTEOPTION_BADEMAIL = 2;

		private const byte VERSION_000 = 0;
		private const byte MARKER_END = 0;
		private const byte MARKER_ATTRIBUTESTEXT = 1;
		private const byte MARKER_ATTRIBUTESDATE = 2;
		private const byte MARKER_ATTRIBUTESINT = 3;
		private const byte MARKER_PHOTOS = 4;
        private const byte MARKER_PHOTO_FILES = 5;

	    public const int CACHED_MEMBER_VERSION=1;

		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;
		
		private int _memberID;
		private string _username = Constants.NULL_STRING;
		private string _emailAddress = Constants.NULL_STRING;
		private string _password = Constants.NULL_STRING;

		private Hashtable _attributesText;
		private Hashtable _attributesDate;
		private Hashtable _attributesInt;

		private PhotoCollection _photos = new PhotoCollection();

		[NonSerialized]
		private ReferenceTracker _referenceTracker;

		private int _version=0;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		public CachedMember(Int32 memberID)
		{
			this.MemberID = memberID;
			_attributesText = new Hashtable();
			_attributesDate = new Hashtable();
			_attributesInt = new Hashtable();
		}

		public CachedMember(Int32 memberID, int version)
		{
			this.MemberID = memberID;
			_version=version;
			_attributesText = new Hashtable();
			_attributesDate = new Hashtable();
			_attributesInt = new Hashtable();
		}

		public CachedMember(byte[] bytes)
		{
			FromByteArray(bytes);
		}

		public CachedMember(byte[] bytes, int version)
		{
			_version=version;
			FromByteArray(bytes);
		}

		protected CachedMember(SerializationInfo info, StreamingContext context)
		{
			try
			{
				_version=(int)info.GetValue("version",typeof(int));
			}
			catch(Exception ex){}

			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			info.AddValue("bytearray",ToByteArray());
			info.AddValue("version", _version);
		}

        #region Properties

        /// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Username
		{
			get
			{
				return _username;
			}
			set
			{
				_username = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}
		

		/// <summary>
		/// 
		/// </summary>
		public string Password
		{
			set
			{
				_password = value;
			}
		}
		

		/// <summary>
		/// 
		/// </summary>
		public PhotoCollection Photos
		{
			get
			{
				return _photos;
			}
			set
			{
				_photos = value;
			}
		}

		public int Version
		{
			get
		 {
			 return _version;
		 }
			set
			{
				_version = value;
			}

		}
        #endregion

        /// <summary>
		/// 
		/// </summary>
		/// <param name="attributeGroupID"></param>
		/// <param name="languageID"></param>
		/// <param name="text"></param>
		/// <param name="textStatus"></param>
		public void SetAttributeText(Int32 attributeGroupID,
			Int32 languageID,
			string text,
			TextStatusType textStatus)
		{
			Hashtable attributes = _attributesText[languageID] as Hashtable;
			TextValue textValue = null;

			if (attributes == null)
			{
				attributes = new Hashtable();
				_attributesText.Add(languageID, attributes);
			}

			textValue = attributes[attributeGroupID] as TextValue;
			
			if (textValue != null)
			{
				textValue.Text = text;
				textValue.TextStatus = textStatus;
			}
			else
			{
				attributes.Add(attributeGroupID, new TextValue(text, textStatus));
			}
		}
		
			
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeGroupID"></param>
		/// <param name="languageID"></param>
		/// <param name="defaultValue"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 attributeGroupID,
			Int32 languageID,
			string defaultValue,
			out TextStatusType textStatus)
		{
			textStatus = TextStatusType.Auto;
			TextValue textValue =  this.GetAttributeText(attributeGroupID, languageID);
			
			if( (textValue == null && attributeGroupID == ServiceConstants.ATTRIBUTEGROUPID_EMAILADDRESS) )
			{
				TextValue tempTextValue = this.GetAttributeText(attributeGroupID, Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH);

				if(tempTextValue != null)
					textValue = tempTextValue;
			}			

			if (textValue != null)
			{
				textStatus = textValue.TextStatus;
				return textValue.Text;
			}
			else
			{
				return defaultValue;
			}			
		}
		
		private TextValue GetAttributeText(Int32 attributeGroupID, Int32 languageID)
		{	
			TextValue retValue = null;
			Hashtable attributes = _attributesText[languageID] as Hashtable;
			
			if(attributes != null)
			{
				retValue = attributes[attributeGroupID] as TextValue;
			}		

			return retValue;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeGroupID"></param>
		/// <param name="date"></param>
		public void SetAttributeDate(Int32 attributeGroupID,
			DateTime date)
		{
			if (_attributesDate.ContainsKey(attributeGroupID))
			{
				if (date != DateTime.MinValue)
				{
					_attributesDate[attributeGroupID] = date;
				}
				else
				{
					_attributesDate.Remove(attributeGroupID);
				}
			}
			else if (date != DateTime.MinValue)
			{
				_attributesDate.Add(attributeGroupID, date);
			}
		}
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeGroupID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Int32 attributeGroupID,
			DateTime defaultValue)
		{
			object o = _attributesDate[attributeGroupID];

			if (o != null)
			{
				return (DateTime)o;
			}

			return defaultValue;
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeGroupID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Int32 attributeGroupID,
			Int32 defaultValue)
		{
			object o = _attributesInt[attributeGroupID];

			if (o != null)
			{
				return (Int32)o;
			}

			return defaultValue;
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeGroupID"></param>
		/// <param name="integer"></param>
		public void SetAttributeInt(Int32 attributeGroupID,
			Int32 integer)
		{
			if (_attributesInt.ContainsKey(attributeGroupID))
			{
				_attributesInt[attributeGroupID] = integer;
			}
			else
			{
				_attributesInt.Add(attributeGroupID, integer);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributes"></param>
		/// <param name="brands"></param>
		/// <returns></returns>
		public ArrayList GetCommunityIDList(Attributes attributes,
			Brands brands)
		{
			ArrayList communities = new ArrayList();

			ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributes.GetAttribute("BrandInsertDate").ID);

			for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
			{
				Int32 attributeGroupID = (Int32)attributeGroupIDs[num];

				if (GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
				{
					Int32 communityID = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.Community.CommunityID;
					if (!communities.Contains(communityID))
					{
						communities.Add(communityID);
					}
				}
			}

			return communities;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributes"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public bool IsEligibleForEmail(Attributes attributes,
			Int32 communityID)
		{
			Int32 globalStatusMask = GetAttributeInt(AG_GLOBALSTATUSMASK, 0);
			if ((globalStatusMask & ATTRIBUTEOPTION_SUSPEND_ADMIN) != ATTRIBUTEOPTION_SUSPEND_ADMIN &&
				(globalStatusMask & ATTRIBUTEOPTION_BADEMAIL) != ATTRIBUTEOPTION_BADEMAIL &&
				GetAttributeInt(attributes.GetAttributeGroup(communityID, ATTRIBUTEID_SELFSUSPENDEDFLAG).ID, 0) == 0)
			{
				return true;
			}

			return false;
		}		
		

		public Int32[] GetUnapprovedAttributeLanguages(Attributes attributes)
		{
			ArrayList list = new ArrayList();

			IDictionaryEnumerator deLanguage = _attributesText.GetEnumerator();
			while (deLanguage.MoveNext())
			{
				Int32 languageID = (Int32)deLanguage.Key;

				IDictionaryEnumerator deAttributeGroupID = (deLanguage.Value as Hashtable).GetEnumerator();
				while (deAttributeGroupID.MoveNext())
				{
					TextValue textValue = deAttributeGroupID.Value as TextValue;
					if (textValue.TextStatus != TextStatusType.Auto
						&& textValue.TextStatus != TextStatusType.Human
						&& (attributes.GetAttributeGroup((Int32)deAttributeGroupID.Key).Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval)
					{
						list.Add(languageID);
						break;
					}
				}
			}

			return (Int32[])list.ToArray(typeof(Int32));
		}


		public bool HasApprovedPhoto(Int32 communityID)
		{
			PhotoCommunity photos = Photos.GetCommunity(communityID);

			for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
			{
                if (photos[photoNum].IsApproved && photos[photoNum].IsApprovedForMain)
				{
					return true;
				}
			}

			return false;
		}

        public bool HasAnyApprovedPhoto(int communityId)
        {
            PhotoCommunity photos = Photos.GetCommunity(communityId);

            for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
            {
                if (photos[photoNum].IsApproved)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Altered the original method to ignore communityID as a parameter for use in PM272 inside MemberBL.SaveMember()
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="attributes"></param>
        /// <param name="brands"></param>
        /// <returns></returns>
        public DateTime GetLastLogonDate(
            out Int32 brandID,
            Attributes attributes,
            Brands brands)
        {
            ArrayList communities = new ArrayList();
            DateTime lastLogonDate = DateTime.MinValue;
            brandID = Constants.NULL_INT;

            Int32 attributeID = attributes.GetAttribute("BrandLastLogonDate").ID;
            ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributeID);

            for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
            {
                Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                Brand brand = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID);

                DateTime currentDtm = GetAttributeDate(attributeGroupID, DateTime.MinValue);
                if (currentDtm > lastLogonDate)
                {
                    lastLogonDate = currentDtm;
                    brandID = brand.BrandID;
                }
            }

            return lastLogonDate;
        }

		public DateTime GetLastLogonDate(Int32 communityID,
			out Int32 brandID,
			Attributes attributes,
			Brands brands)
		{
			ArrayList communities = new ArrayList();
			DateTime lastLogonDate = DateTime.MinValue;
			brandID = Constants.NULL_INT;

			Int32 attributeID = attributes.GetAttribute("BrandLastLogonDate").ID;
			ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributeID);

			for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
			{
				Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
				Brand brand = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID);
				if (brand.Site.Community.CommunityID == communityID)
				{
					DateTime currentDtm = GetAttributeDate(attributeGroupID, DateTime.MinValue);
					if (currentDtm > lastLogonDate)
					{
						lastLogonDate = currentDtm;
						brandID = brand.BrandID;
					}
				}
			}

			return lastLogonDate;
		}


		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}
		

		#region ICacheable Members

		/// <summary>
		/// 
		
		public static string GetCacheKey(int memberID)
		{
			return CACHE_KEY_PREFIX +  memberID.ToString();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{	 string key="";
			
				key= CachedMember.GetCacheKey(_memberID);
			

			return key;
		}


		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion

		#region IByteSerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="bytes"></param>
		public void FromByteArray(byte[] bytes)
		{
			byte marker;
			Int32 languageID;
			Int32 count;
			Int32 communityID;

			_attributesText = new Hashtable();
			_attributesDate = new Hashtable();
			_attributesInt = new Hashtable();

			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			_cacheTTLSeconds = br.ReadInt32();
			_cachePriority = (Matchnet.CacheItemPriorityLevel)Enum.Parse(typeof(Matchnet.CacheItemPriorityLevel), br.ReadInt32().ToString());
			_memberID = br.ReadInt32();
			_username = br.ReadString();
			
			if (_username.Length == 0)
			{
				_username = null;
			}
			_password = br.ReadString();
			if (_password.Length == 0)
			{
				_password = null;
			}
			_emailAddress = br.ReadString();
			if (_emailAddress.Length == 0)
			{
				_emailAddress = null;
			}

			marker = br.ReadByte();
			while (marker != MARKER_END)
			{
				switch (marker)
				{
					case MARKER_ATTRIBUTESTEXT:
						languageID = br.ReadInt32();
						count = br.ReadInt32();

						for (Int32 attributeNum = 0; attributeNum < count; attributeNum++)
						{
							this.SetAttributeText(br.ReadInt32(),
								languageID,
								br.ReadString(),
								(TextStatusType)br.ReadInt32());
						}
						break;

					case MARKER_ATTRIBUTESDATE:
						count = br.ReadInt32();

						for (Int32 attributeNum = 0; attributeNum < count; attributeNum++)
						{
							this.SetAttributeDate(br.ReadInt32(),
								DateTime.FromFileTime(br.ReadInt64()));
						}
						break;

					case MARKER_ATTRIBUTESINT:
						count = br.ReadInt32();

						for (Int32 attributeNum = 0; attributeNum < count; attributeNum++)
						{
							this.SetAttributeInt(br.ReadInt32(),
								br.ReadInt32());
						}
						break;

					case MARKER_PHOTOS:
						communityID = br.ReadInt32();
						count = br.ReadInt32();

						PhotoCommunity photoCommunity = this.Photos.GetCommunity(communityID);

						for (Int32 photoNum = 0; photoNum < count; photoNum++)
						{
							Int32 memberPhotoID = br.ReadInt32();
							Int32 fileID = br.ReadInt32();
							string fileWebPath = br.ReadString();
							if (fileWebPath.Length == 0)
							{
								fileWebPath = null;
							}
							string filePath = br.ReadString();
							if (filePath.Length == 0)
							{
								filePath = null;
							}
							Int32 thumbFileID = br.ReadInt32();
							string thumbFileWebPath = br.ReadString();
							if (thumbFileWebPath.Length == 0)
							{
								thumbFileWebPath = null;
							}
							string thumbFilePath = br.ReadString();
							if (thumbFilePath.Length == 0)
							{
								thumbFilePath = null;
							}
							byte listOrder = br.ReadByte();
							bool isApproved = br.ReadBoolean();
							bool isPrivate = br.ReadBoolean();
							Int32 albumID = br.ReadInt32();
							Int32 adminMemberID = br.ReadInt32();
                            string caption=br.ReadString();
							bool isCaptionApproved=br.ReadBoolean();

						    // version 2 attributes
                            string fileCloudPath = br.ReadString();
							string thumbFileCloudPath = br.ReadString();

						    bool isApprovedforMain = br.ReadBoolean();
						    bool isMain = br.ReadBoolean();

                            int fileHeight = Constants.NULL_INT;
                            int fileWidth = Constants.NULL_INT;
                            int fileSize = Constants.NULL_INT;
                            int thumbFileHeight = Constants.NULL_INT;
                            int thumbFileWidth = Constants.NULL_INT;
                            int thumbFileSize = Constants.NULL_INT;

                            if (_version >= 4)
                            {
                                fileHeight = br.ReadInt32();
                                fileWidth = br.ReadInt32();
                                fileSize = br.ReadInt32();
                                thumbFileHeight = br.ReadInt32();
                                thumbFileWidth = br.ReadInt32();
                                thumbFileSize = br.ReadInt32();
                            }

						    photoCommunity[(byte) photoNum] = new Photo(memberPhotoID,
						                                                fileID,
						                                                fileWebPath,
						                                                //filePath,
						                                                thumbFileID,
						                                                thumbFileWebPath,
						                                                //thumbFilePath,
						                                                listOrder,
						                                                isApproved,
						                                                isPrivate,
						                                                albumID,
						                                                adminMemberID,
						                                                caption,
						                                                isCaptionApproved,
						                                                fileCloudPath,
						                                                thumbFileCloudPath,
						                                                isApprovedforMain,
						                                                isMain,
						                                                fileHeight,
						                                                fileWidth,
                                                                        fileSize,
                                                                        thumbFileHeight,
                                                                        thumbFileWidth,
                                                                        thumbFileSize);

                            if (_version >= 5)
                            {
                                int photoFileCount = br.ReadInt32();
                                for(var i=1;i<=photoFileCount;i++)
                                {
                                    var photoFileType = (PhotoFileType)br.ReadInt32();
                                    var photoFileFileID = br.ReadInt32();
                                    var photoFileFileWebPath = br.ReadString();
                                    var photoFileFileCloudPath = br.ReadString();
                                    var photoFileFileWidth = br.ReadInt32();
                                    var photoFileFileHeight = br.ReadInt32();
                                    var photoFileFileFileSize = br.ReadInt32();

                                    var photoFile = new PhotoFile(photoFileType, 
                                        photoFileFileHeight, 
                                        photoFileFileWidth, 
                                        photoFileFileFileSize,
                                        photoFileFileID > 0 ? photoFileFileID : Constants.NULL_INT,
                                        photoFileFileWebPath,
                                        photoFileFileCloudPath);

                                    photoCommunity[(byte) photoNum].Files.Add(photoFile);
                                }
                            }

						}
						break;

					default:
						throw new Exception("invalid marker (" + marker.ToString() + ")");
				}

				marker = br.ReadByte();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(8192);
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_cacheTTLSeconds);
			bw.Write((Int32)_cachePriority);
			bw.Write(_memberID);
			bw.Write(_username != null ? _username : "");
			bw.Write(_password != null ? _password : "");
			bw.Write(_emailAddress != null ? _emailAddress : "");

			//text attributes
			IDictionaryEnumerator deTextLanguage = _attributesText.GetEnumerator();
			while (deTextLanguage.MoveNext())
			{
				Int32 languageID = (Int32)deTextLanguage.Key;
				Hashtable text = deTextLanguage.Value as Hashtable;
				if (text.Count > 0)
				{
					bw.Write(MARKER_ATTRIBUTESTEXT);
					bw.Write(languageID);
					bw.Write(text.Count);

					IDictionaryEnumerator deText = text.GetEnumerator();
					while (deText.MoveNext())
					{
						bw.Write((Int32)deText.Key);	//attributeGroupID
						TextValue textValue = deText.Value as TextValue;
						bw.Write(textValue.Text != null ? textValue.Text : "");
						bw.Write((Int32)textValue.TextStatus);
					}
				}
			}

			//date attributes
			if (_attributesDate.Count > 0)
			{
				bw.Write(MARKER_ATTRIBUTESDATE);
				bw.Write(_attributesDate.Count);
				IDictionaryEnumerator deDate = _attributesDate.GetEnumerator();
				while (deDate.MoveNext())
				{
					bw.Write((Int32)deDate.Key);	//attributeGroupID
					bw.Write(((DateTime)deDate.Value).ToFileTime());
				}
			}

			//int attributes
			if (_attributesInt.Count > 0)
			{
				bw.Write(MARKER_ATTRIBUTESINT);
				bw.Write(_attributesInt.Count);
				IDictionaryEnumerator deInt = _attributesInt.GetEnumerator();
				while (deInt.MoveNext())
				{
					bw.Write((Int32)deInt.Key);	//attributeGroupID
					bw.Write((Int32)deInt.Value);
				}
			}

			//photos
			IDictionaryEnumerator dePhotoCommunities = _photos.Communities.GetEnumerator();
			while (dePhotoCommunities.MoveNext())
			{
				Int32 communityID = (Int32)dePhotoCommunities.Key;
				PhotoCommunity photoCommunity = dePhotoCommunities.Value as PhotoCommunity;
				if (photoCommunity.Count > 0)
				{
					bw.Write(MARKER_PHOTOS);
                    bw.Write(communityID);
					bw.Write(photoCommunity.Count);
					IEnumerator ePhotos = photoCommunity.GetEnumerator();
					while (ePhotos.MoveNext())
					{
						Photo photo = ePhotos.Current as Photo;
						bw.Write(photo.MemberPhotoID);
						bw.Write(photo.FileID);
						bw.Write(photo.FileWebPath != null ? photo.FileWebPath : "");
						bw.Write(photo.FilePath != null ? photo.FilePath : "");
						bw.Write(photo.ThumbFileID);
						bw.Write(photo.ThumbFileWebPath != null ? photo.ThumbFileWebPath : "");
						bw.Write(photo.ThumbFilePath != null ? photo.ThumbFilePath : "");
						bw.Write(photo.ListOrder);
						bw.Write(photo.IsApproved);
						bw.Write(photo.IsPrivate);
						bw.Write(photo.AlbumID);
						bw.Write(photo.AdminMemberID);
                        bw.Write(photo.Caption != null ? photo.Caption : "");
                        bw.Write(photo.IsCaptionApproved);
                        bw.Write(photo.FileCloudPath);
                        bw.Write(photo.ThumbFileCloudPath);
                        bw.Write(photo.IsApprovedForMain);
                        bw.Write(photo.IsMain);

                        if (_version >= 4)
                        {
                            bw.Write(photo.FileHeight);
                            bw.Write(photo.FileWidth);
                            bw.Write(photo.FileSize);
                            bw.Write(photo.ThumbFileHeight);
                            bw.Write(photo.ThumbFileWidth);
                            bw.Write(photo.ThumbFileSize);
                        }
                        
                        if (_version >= 5)
                        {
                            bw.Write(photo.Files.Count);
                            foreach (var photoFile in photo.Files)
                            {
                                bw.Write((int)photoFile.PhotoFileType);
                                bw.Write(photoFile.FileID);
                                bw.Write(photoFile.FileWebPath);
                                bw.Write(photoFile.FileCloudPath);
                                bw.Write(photoFile.FileWidth);
                                bw.Write(photoFile.FileHeight);
                                bw.Write(photoFile.FileSize);
                            }
                        }
					}
				}
			}

			bw.Write(MARKER_END);

			return ms.ToArray();
		}
		#endregion

	    public Hashtable AttributesText
	    {
	        get { return _attributesText.Clone() as Hashtable; }
	    }

	    public Hashtable AttributesDate
	    {
            get { return _attributesDate.Clone() as Hashtable; }
	    }

	    public Hashtable AttributesInt
	    {
            get { return _attributesInt.Clone() as Hashtable; }
	    }
	}
}
