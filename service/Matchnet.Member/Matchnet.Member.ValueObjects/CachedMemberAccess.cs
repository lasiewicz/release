﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.AccessService;
using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Member.ValueObjects
{
    /// <summary>
    /// Encapsulates member access privileges from Unified Access Service
    /// </summary>
    [Serializable]
    public class CachedMemberAccess : IValueObject, ICacheable, IReplicable
    {
        private const string CACHE_KEY_PREFIX = "CachedMemberAccess";

        private int _memberID;
        private int _cacheTTLSeconds = 60 * 60;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;

        private List<AccessPrivilege> _AccessPrivilegeList = new List<AccessPrivilege>();
        private List<int> _MigratedPrivilegeSites = new List<int>();

        private bool _replicateForExpire = false;

        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public CachedMemberAccess(int memberID, AccessPrivilege[] privilegeList)
        {
            _memberID = memberID;
            if (privilegeList != null)
            {
                foreach (AccessPrivilege ap in privilegeList)
                {
                    _AccessPrivilegeList.Add(ap);
                }
            }
        }

        #region Properties
        public bool ReplicateForExpire
        {
            get { return _replicateForExpire; }
            set { _replicateForExpire = value; }
        }

        public List<AccessPrivilege> AccessPrivilegeList
        {
            get { return _AccessPrivilegeList; }
        }

        public List<int> MigratedPrivilegeSites
        {
            get { return _MigratedPrivilegeSites; }
            set { _MigratedPrivilegeSites = value; }
        }

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
            set
            {
                _referenceTracker = value;
            }
        }

        public int MemberID
        {
            get { return _memberID; }
        }
        #endregion

        #region Methods
        public AccessPrivilege GetAccessPrivilege(PrivilegeType privilegeType, int siteID)
        {
            AccessPrivilege accessPrivilege = null;
            if (_AccessPrivilegeList != null)
            {
                foreach (AccessPrivilege ap in _AccessPrivilegeList)
                {
                    if (ap.CallingSystemID == siteID && ap.UnifiedPrivilegeType == privilegeType)
                    {
                        accessPrivilege = ap;
                        break;
                    }
                }
            }

            return accessPrivilege;
        }

        public void AdjustAccessCountPrivilege(int[] privilegeTypeID, int brandID, int siteID, int communityID, int count)
        {
            //Get Privilege
            foreach (int i in privilegeTypeID)
            {
                AccessPrivilege accessPrivilege = GetAccessPrivilege((PrivilegeType)i, siteID);
                if (accessPrivilege != null)
                {
                    accessPrivilege.RemainingCount += count;
                    if (accessPrivilege.RemainingCount < 0)
                    {
                        //remaining count can't be less than 0
                        accessPrivilege.RemainingCount = 0;
                    }
                }
            }

        }
        #endregion


        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public static string GetCacheKey(int memberID)
        {
            return CACHE_KEY_PREFIX + memberID.ToString();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            string key = "";

            key = CachedMemberAccess.GetCacheKey(_memberID);

            return key;
        }


        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
            set
            {
                _cacheMode = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }


        #endregion

        #region IReplicable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion

    }
}
