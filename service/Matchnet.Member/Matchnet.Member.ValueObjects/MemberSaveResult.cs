using System;

using Matchnet;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Defines all possible outcomes of a Save attempt.
	/// </summary>
	[Serializable]
	public enum MemberSaveStatusType : int
	{
		/// <summary>An enum indicating a success in saving the Member.</summary>
		Success = 0,
		
		/// <summary>An enum indicating an email address that is in the banned list.  The Member cannot be saved.</summary>
		BannedEmailAddress = 519238,
		
		/// <summary>An enum indicating an email address that already exists in the system.  The Member cannot be saved. </summary>
		EmailAddressExists = 400000,
		
		/// <summary>An enum indicating an email address that is invalid due to length or invalid characters.  The Member cannot be saved.</summary>
		InvalidEmail = 519385
	}

	/// <summary>
	/// Represents the result of a member Save action.
	/// </summary>
	[Serializable]
	public sealed class MemberSaveResult : IValueObject
	{
		private MemberSaveStatusType _saveStatus = MemberSaveStatusType.Success;
		private string _userName = Constants.NULL_STRING;

		private MemberSaveResult()
		{
		}


		/// <summary>
		/// Initializes the MemberSaveResult object.
		/// </summary>
		/// <param name="status">The status returned from the save action.</param>
		/// <param name="userName">The Member's userName.  userName is used here because userName is the only unique identifer that exists for new Members prior to save.</param>
		public MemberSaveResult(MemberSaveStatusType status, string userName)
		{
			_saveStatus = status;
			_userName = userName;
		}


		/// <summary>Getter method for the SaveStatus</summary>
		public MemberSaveStatusType SaveStatus
		{
			get
			{
				return _saveStatus;
			}
		}


		/// <summary>Getter method for the Username</summary>
		public string Username
		{
			get
			{
				return _userName;
			}
		}
	}
}
