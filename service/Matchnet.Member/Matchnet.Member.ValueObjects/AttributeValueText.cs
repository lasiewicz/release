using System;

using Matchnet;


namespace Matchnet.Member.ValueObjects
{
	/// <summary>Defines all possible outcomes of an approval attempt. </summary>
	public enum ApprovalStatusType : int
	{
		/// <summary>DOCUMENTATION NEEDED:  What is the difference between the enums 'None' and 'Pending'.</summary>
		None = 0,
		/// <summary>DOCUMENTATION NEEDED:  What is the difference between the enums 'None' and 'Pending'.</summary>
		Pending = 1,
		/// <summary>Indicates a free text field approved by an automated method, be it a Bayesian application or another method.</summary>
		Auto = 2,
		/// <summary>Indicates a free text field approved by a member of Spark Networks' Customer Service staff.</summary>
		Human = 4
	}


	/// <summary>AttributeValueText holds the value of a single text Attribute for a given Member.  Since Free Text Attributes must be approved prior to posting to the site for other Members to view, this object also includes an ApprovalStatusType to specify whether or not the Attribute should be displayed to the Member site or listed in the free text approval queue.</summary>
	[Serializable]
	public class AttributeValueText : AttributeValue, IValueObject
	{
		private ApprovalStatusType _approvalStatus = ApprovalStatusType.None;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="val">The value for the attribute.  This is the text entered by a Member in a given free text field</param>
		/// <param name="approvalStatus">Whether the text field is approved for display.</param>
		public AttributeValueText(string val, ApprovalStatusType approvalStatus)
		{
			base.Val = val;
			_approvalStatus = approvalStatus;
		}


		#region public properties
		/// <summary>Getter and setter methods for the ApprovalStatusType</summary>
		public ApprovalStatusType ApprovalStatus
		{
			get
			{
				return _approvalStatus;
			}
			set
			{
				_approvalStatus = value;
			}
		}
		#endregion
	}
}
