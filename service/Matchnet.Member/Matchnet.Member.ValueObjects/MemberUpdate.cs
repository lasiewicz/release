using System;
using System.Collections;

using Matchnet.Member.ValueObjects.Photos;


namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MemberUpdate : IValueObject
	{
		private int _memberID;
		private string _username = Constants.NULL_STRING;
		private string _password = Constants.NULL_STRING;
        private string _passwordHash = Constants.NULL_STRING;
		private string _emailAddress = Constants.NULL_STRING;
		private bool _isAdminUpdated = false;
		private Hashtable _attributesText;
		private Hashtable _attributesInt;
		private Hashtable _attributesDate;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// 
		public MemberUpdate(){}
		public MemberUpdate(int memberID)
		{
			_memberID = memberID;
			_attributesText = new Hashtable();
			_attributesInt = new Hashtable();
			_attributesDate = new Hashtable();
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsDirty
		{
			get
			{
				if (
					_username != Constants.NULL_STRING ||
					_password != Constants.NULL_STRING ||
                    _passwordHash != Constants.NULL_STRING ||
					_emailAddress != Constants.NULL_STRING ||
					_attributesText.Count > 0 ||
					_attributesInt.Count > 0 ||
					_attributesDate.Count > 0
					)
				{
					return true;
				}

				return false;
			}
		}

		/// <summary>
		/// Indiate whether this update is done by an admin.
		/// True - a new approval queue can be added for this memberupdate.
		/// </summary>
		public bool IsAdminUpdated
		{
			get
			{
				return _isAdminUpdated;
			}
			set
			{
				_isAdminUpdated = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Username
		{
			get
			{
				return _username;
			}
			set
			{
				/*
				if (value == Constants.NULL_STRING)
				{
					throw new Exception("Username cannot be set to null.");
				}
				*/
				_username = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Password
		{
			get
			{
				return _password;
			}
			set
			{
				_password = value;
			}
		}

	    public string PasswordHash
	    {
	        get { return _passwordHash; }
	        set { _passwordHash = value; }
	    }

		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
			set
			{
				_emailAddress = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Hashtable AttributesText
		{
			get
			{
				return _attributesText;
			}
		}
			

		/// <summary>
		/// 
		/// </summary>
		public Hashtable AttributesInt
		{
			get
			{
				return _attributesInt;
			}
		}
			

		/// <summary>
		/// 
		/// </summary>
		public Hashtable AttributesDate
		{
			get
			{
				return _attributesDate;
			}
		}
	}
}
