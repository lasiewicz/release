﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Member.ValueObjects.MemberAppDevices
{
    [Serializable]
    public class MemberAppDevice
    {
        public int MemberID { get; set; }
        public int AppID { get; set; }
        public string DeviceID { get; set; }
        public int BrandID { get; set; }
        public string DeviceData { get; set; }
        public long PushNotificationsFlags { get; set; }
    }
}
