﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Matchnet.Member.ValueObjects.MemberAppDevices
{
    public static class MemberAppDeviceHelper
    {
        public static string SerializeDeviceData(string deviceType = "", string appVersion = "", string OSVersion = "")
        {
            var xmlFormatString = "<DeviceData><DeviceType>{0}</DeviceType><AppVersion>{1}</AppVersion><OSVersion>{2}</OSVersion></DeviceData>";
            return string.Format(xmlFormatString, deviceType, appVersion, OSVersion);
        }

        public static string GenerateMemberIdHash(int memberId = 0)
        {
            string hash = string.Empty;

            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(memberId.ToString()));
                var sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                hash = sBuilder.ToString();
            }

            return hash;
        }
    }
}
