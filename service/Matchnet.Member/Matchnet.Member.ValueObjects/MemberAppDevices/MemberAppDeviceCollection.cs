﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Member.ValueObjects.MemberAppDevices
{
    [Serializable]
    public class MemberAppDeviceCollection : IDisposable, ICacheable
    {

        private const string CACHE_KEY_BASE = "MemberAppDevice-";
        private int _cacheTTLSeconds = 60 * 60;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private CacheItemMode _cacheMode = CacheItemMode.Absolute;
        private List<MemberAppDevice> _AppDevices;

        public int MemberID { get; set; }

        public List<MemberAppDevice> AppDevices
        {
            get
            {
                if (_AppDevices == null)
                {
                    _AppDevices = new List<MemberAppDevice>();
                }
                return _AppDevices;
            }

            set
            {
                _AppDevices = value;
            }
        }

        public bool ContainsDevice(string deviceId, int appId)
        {
            return (from ad in AppDevices where ad.DeviceID == deviceId && ad.AppID == appId select ad).Count() > 0;
        }

        public MemberAppDevice GetDevice(string deviceId, int appId)
        {
            return (from ad in AppDevices where ad.DeviceID == deviceId && ad.AppID == appId select ad).FirstOrDefault();
        }

        #region ICacheable Members
        public CacheItemMode CacheMode
        {
            get { return _cacheMode; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKey(MemberID);
        }

        public static string GetCacheKey(int memberId)
        {
            return CACHE_KEY_BASE + memberId;
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion

    }
}
