using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Summary description for MemberPaymentCollection.
	/// </summary>
	[Serializable]
	public class MemberPaymentCollection : System.Collections.CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		public MemberPaymentCollection()
		{

		}

		/// <summary>
		/// Indexer overload
		/// </summary>
		public MemberPayment this[int index]
		{
			get { return ((MemberPayment)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="memberPayment">banner to add to collection</param>
		/// <returns></returns>
		public int Add(MemberPayment memberPayment)
		{
			return base.InnerList.Add(memberPayment);
		}
	}
}
