using System;

using Matchnet;


namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// AttributeValue holds the value of a single numeric attribute for a given Member.
	/// </summary>
	[Serializable]
	public class AttributeValue : IValueObject
	{
		private string _val;

		/// <summary>
		/// 
		/// </summary>
		public AttributeValue()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="val">the value for the attribute.  Typically this value will be a mask rather than the actual numeric data input by the Member.</param>
		public AttributeValue(string val)
		{
			_val = val;
		}


		#region public properties
		/// <summary>
		/// 
		/// </summary>
		public string Val
		{
			get
			{
				return _val;
			}
			set
			{
				_val = value;
			}
		}
		#endregion
	}
}
