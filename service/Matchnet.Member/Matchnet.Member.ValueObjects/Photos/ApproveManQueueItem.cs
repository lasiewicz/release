using System;
using System.Data;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// Summary description for ApproveManQueueItem.
	/// </summary>
	public class ApproveManQueueItem
	{
		DataSet _QueueDataSet;

		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		public ApproveManQueueItem()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public DataSet QueueDataSet
		{
			get
			{
				return _QueueDataSet;
			}
			set
			{
				_QueueDataSet = value;
			}
		}
		#endregion

		#region Public Methods
	
		/// <summary>
		/// Process each dataset.
		/// </summary>
		public void Execute()
		{
			//ApproveManQueues.Process(this.QueueDataSet);
		}

		#endregion
	}
}
