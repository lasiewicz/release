using System;
using System.Collections;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ApproveInfoCollection : CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		public ApproveInfoCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public ApproveInfo this[int index]
		{
			get { return ((ApproveInfo)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="approveInfo"></param>
		/// <returns></returns>
		public int Add(ApproveInfo approveInfo)
		{
			return base.InnerList.Add(approveInfo);
		}
	}
}
