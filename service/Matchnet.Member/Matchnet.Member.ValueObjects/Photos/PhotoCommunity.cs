using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PhotoCommunity
	{
		private ArrayList _list;

		/// <summary>
		/// 
		/// </summary>
		public PhotoCommunity()
		{
			_list = new ArrayList();
		}

		public void StripHostFromPaths()
		{
			for (Int32 i = 0; i < _list.Count; i++)
			{
				((_list[i]) as Photo).StripHostFromPaths();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Photo this[byte index]
		{
			get
			{
				return _list[index] as Photo;
			}
			set
			{
				if (_list.Count > index && ((Photo)_list[index]).MemberPhotoID == value.MemberPhotoID)
				{
					_list[index] = value;
				}
				else
				{
					Remove(value.MemberPhotoID);

					if (index >= _list.Count)
					{
						_list.Add(value);
					}
					else
					{
						_list.Insert(index, value);
					}
				}
			}	
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		public void Remove(Int32 memberPhotoID)
		{
			Int32 index = 0;
			bool found = false;

			for (Int32 num = 0; num < _list.Count; num++)
			{
				Photo photo = _list[num] as Photo;

				if (photo.MemberPhotoID == memberPhotoID)
				{
					found = true;
					break;
				}

				index++;
			}

			if (found)
			{
				_list.RemoveAt(index);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <returns></returns>
		public Photo Find(Int32 memberPhotoID)
		{
			for (Int32 num = 0; num < _list.Count; num++)
			{
				Photo photo = _list[num] as Photo;

				if (photo.MemberPhotoID == memberPhotoID)
				{
					return photo;
				}
			}

			return null;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 Count
		{
			get
			{
				return _list.Count;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _list.GetEnumerator();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
	    public List<Photo> GetList()
        {
            return _list.Cast<Photo>().ToList();
        }
	}
}
