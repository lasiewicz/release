using System;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// Summary description for ApproveManQueue.
	/// </summary>
	public class ApproveManQueue : ApproveQueue
	{
		private int _EmailBodyResourceID = Matchnet.Constants.NULL_INT;

		/// <summary>
		/// 
		/// </summary>
		public int EmailBodyResourceID
		{
			get
			{
				return _EmailBodyResourceID;
			}
			set
			{
				_EmailBodyResourceID= value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ApproveManQueue()
		{
		}

		
	}
}
