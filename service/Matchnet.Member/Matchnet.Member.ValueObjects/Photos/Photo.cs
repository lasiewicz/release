using System;
using System.Collections.Generic;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Photo : IValueObject
    {
        #region Data Members
        /// <summary> </summary>
		protected Int32 _memberPhotoID;
		/// <summary> </summary>
		protected Int32 _fileID;
		/// <summary> </summary>
		protected string _fileWebPath;
		/// <summary> </summary>
		protected string _filePath = Constants.NULL_STRING;
		/// <summary> </summary>
		protected Int32 _thumbFileID = Constants.NULL_INT;
		/// <summary> </summary>
		protected string _thumbFileWebPath = Constants.NULL_STRING;
		/// <summary> </summary>
		protected string _thumbFilePath = Constants.NULL_STRING;
		/// <summary> </summary>
		protected byte _listOrder;
		/// <summary> </summary>
		protected bool _isApproved = false;
		/// <summary> </summary>
		protected bool _isPrivate = false;
		/// <summary> </summary>
		protected Int32 _albumID;
		/// <summary> </summary>
		protected Int32 _adminMemberID;
		/// <summary> </summary>
		protected bool _isDirty = false;

		protected string _caption="";

	    protected string _fileCloudPath;

	    protected string _thumbFileCloudPath;

		protected bool _isCaptionApproved = false;

	    protected bool _isMain = false;

	    protected bool _isApprovedForMain = false;

	    protected List<PhotoFile> _files;

        protected int _fileHeight = Constants.NULL_INT;
        protected int _fileWidth = Constants.NULL_INT;
        protected int _fileSize = Constants.NULL_INT;
        protected int _thumbFileHeight = Constants.NULL_INT;
        protected int _thumbFileWidth = Constants.NULL_INT;
        protected int _thumbFileSize = Constants.NULL_INT;

        #endregion

        #region Internal ctors
        internal Photo()
		{
		    _fileCloudPath = string.Empty;
		    _thumbFileCloudPath = string.Empty;
            _files = new List<PhotoFile>();
		}

		internal Photo(Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			string filePath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			string thumbFilePath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID): this()
		{
			_memberPhotoID = memberPhotoID;
			_fileID = fileID;
			_fileWebPath = fileWebPath;
			_filePath = filePath;
			_thumbFileID = thumbFileID;
			_thumbFileWebPath = thumbFileWebPath;
			_thumbFilePath = thumbFilePath;
			_listOrder = listOrder;
			_isApproved = isApproved;
			_isPrivate = isPrivate;
			_albumID = albumID;
			_adminMemberID = adminMemberID;
		}

		internal Photo(Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			string filePath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			string thumbFilePath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID,
			string caption,
            bool isCaptionApproved)
            : this(memberPhotoID, fileID, fileWebPath, filePath, thumbFileID, thumbFileWebPath, thumbFilePath,
                listOrder, isApproved, isPrivate, albumID, adminMemberID)
		{
			_caption=caption;
			_isCaptionApproved=isCaptionApproved;
		}

	    internal Photo(Int32 memberPhotoID,
	                   Int32 fileID,
	                   string fileWebPath,
	                   string filePath,
	                   Int32 thumbFileID,
	                   string thumbFileWebPath,
	                   string thumbFilePath,
	                   byte listOrder,
	                   bool isApproved,
	                   bool isPrivate,
	                   Int32 albumID,
	                   Int32 adminMemberID,
	                   string caption,
	                   bool isCaptionApproved,
	                   string fileCloudPath,
	                   string thumbFileCloudPath)
	        : this(memberPhotoID, fileID, fileWebPath, filePath, thumbFileID, thumbFileWebPath, thumbFilePath,
	               listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved)
	    {
	        _fileCloudPath = fileCloudPath;
	        _thumbFileCloudPath = thumbFileCloudPath;
	    }

	    internal Photo(Int32 memberPhotoID,
	                   Int32 fileID,
	                   string fileWebPath,
	                   string filePath,
	                   Int32 thumbFileID,
	                   string thumbFileWebPath,
	                   string thumbFilePath,
	                   byte listOrder,
	                   bool isApproved,
	                   bool isPrivate,
	                   Int32 albumID,
	                   Int32 adminMemberID,
	                   string caption,
	                   bool isCaptionApproved,
	                   string fileCloudPath,
	                   string thumbFileCloudPath,
	                   bool isApprovedForMain,
	                   bool isMain)
	        : this(memberPhotoID, fileID, fileWebPath, filePath, thumbFileID, thumbFileWebPath, thumbFilePath,
	               listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved, fileCloudPath,
	               thumbFileCloudPath)
	    {
	        _isApprovedForMain = isApprovedForMain;
	        _isMain = isMain;
	    }

        


        #endregion

        internal void StripHostFromPaths()
		{
			_fileWebPath = stripHost(_fileWebPath);
			_thumbFileWebPath = stripHost(_thumbFileWebPath);
		}

		private string stripHost(string path)
		{
			if (path != null)
			{
				if (path.IndexOf("/") != 0)
				{
					return new Uri(path).AbsolutePath;
				}
			}

			return path;
		}

        #region Public ctors
        /// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <param name="fileID"></param>
		/// <param name="fileWebPath"></param>
		/// <param name="listOrder"></param>
		public Photo(Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			byte listOrder): this()
		{
			_memberPhotoID = memberPhotoID;
			_fileID = fileID;
			_fileWebPath = fileWebPath;
			_listOrder = listOrder;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <param name="fileID"></param>
		/// <param name="fileWebPath"></param>
		/// <param name="thumbFileID"></param>
		/// <param name="thumbFileWebPath"></param>
		/// <param name="listOrder"></param>
		/// <param name="isApproved"></param>
		/// <param name="isPrivate"></param>
		/// <param name="albumID"></param>
		/// <param name="adminMemberID"></param>
		public Photo(Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID) : this(memberPhotoID, fileID, fileWebPath, listOrder)
		{
			_thumbFileID = thumbFileID;
			_thumbFileWebPath = thumbFileWebPath;
			_isApproved = isApproved;
			_isPrivate = isPrivate;
			_albumID = albumID;
			_adminMemberID = adminMemberID;
		}

		public Photo(Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID,
			string caption,
			bool isCaptionApproved) : this(memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath, listOrder, isApproved, isPrivate, albumID, adminMemberID )
		{
			_caption=caption;
			_isCaptionApproved=isCaptionApproved;
		}

        public Photo(Int32 memberPhotoID,
            Int32 fileID,
            string fileWebPath,
            Int32 thumbFileID,
            string thumbFileWebPath,
            byte listOrder,
            bool isApproved,
            bool isPrivate,
            Int32 albumID,
            Int32 adminMemberID,
            string caption,
            bool isCaptionApproved, 
            string fileCloudPath,
            string thumbFileCloudPath): this(memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath, listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved)
        {
            _fileCloudPath = fileCloudPath;
            _thumbFileCloudPath = thumbFileCloudPath;
        }
        /// <summary>
        /// CachedMember version 3 ctor. Added the ability to specify "main" photo.
        /// </summary>
        /// <param name="memberPhotoID"></param>
        /// <param name="fileID"></param>
        /// <param name="fileWebPath"></param>
        /// <param name="thumbFileID"></param>
        /// <param name="thumbFileWebPath"></param>
        /// <param name="listOrder"></param>
        /// <param name="isApproved"></param>
        /// <param name="isPrivate"></param>
        /// <param name="albumID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="caption"></param>
        /// <param name="isCaptionApproved"></param>
        /// <param name="fileCloudPath"></param>
        /// <param name="thumbFileCloudPath"></param>
        /// <param name="isApprovedForMain">This photo can be selected as the main photo for a member</param>
        /// <param name="isMain">Indicates if this is the main photo of member's photo collection</param>
        public Photo(Int32 memberPhotoID,
            Int32 fileID,
            string fileWebPath,
            Int32 thumbFileID,
            string thumbFileWebPath,
            byte listOrder,
            bool isApproved,
            bool isPrivate,
            Int32 albumID,
            Int32 adminMemberID,
            string caption,
            bool isCaptionApproved,
            string fileCloudPath,
            string thumbFileCloudPath,
            bool isApprovedForMain,
            bool isMain)
            : this(memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath, listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved, fileCloudPath, thumbFileCloudPath)
        {
            _isApprovedForMain = isApprovedForMain;
            _isMain = isMain;
        }

        public Photo(Int32 memberPhotoID,
                       Int32 fileID,
                       string fileWebPath,
                       Int32 thumbFileID,
                       string thumbFileWebPath,
                       byte listOrder,
                       bool isApproved,
                       bool isPrivate,
                       Int32 albumID,
                       Int32 adminMemberID,
                       string caption,
                       bool isCaptionApproved,
                       string fileCloudPath,
                       string thumbFileCloudPath,
                       bool isApprovedForMain,
                       bool isMain,
                       int fileHeight,
                       int fileWidth,
                       int fileSize,
                       int thumbFileHeight,
                       int thumbFileWidth,
                       int thumbFileSize)
            : this(memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath,
                   listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved, fileCloudPath,
                   thumbFileCloudPath, isApprovedForMain, isMain)
        {
            _fileHeight = fileHeight;
            _fileWidth = fileWidth;
            _fileSize = fileSize;
            _thumbFileHeight = thumbFileHeight;
            _thumbFileWidth = thumbFileWidth;
            _thumbFileSize = thumbFileSize;
        }
        #endregion

        #region Properties
        /// <summary>
		/// 
		/// </summary>
		public Int32 MemberPhotoID
		{
			get
			{
				return _memberPhotoID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 FileID
		{
			get
			{
				return _fileID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string FileWebPath
		{
			get
			{
				return _fileWebPath;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 ThumbFileID
		{
			get
			{
				return _thumbFileID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string ThumbFileWebPath
		{
			get
			{
				return _thumbFileWebPath;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public byte ListOrder
		{
			get
			{
				return _listOrder;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsApproved
		{
			get
			{
				return _isApproved;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsPrivate
		{
			get
			{
				return _isPrivate;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 AlbumID
		{
			get
			{
				return _albumID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 AdminMemberID
		{
			get
			{
				return _adminMemberID;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string FilePath 
		{
			get
			{
				return _filePath;
			}
			set
			{
				_filePath = value;
			}
		}

        public string FileCloudPath
        {
            get
            {
                return _fileCloudPath.ToLower();
            }
            set
            {
                _fileCloudPath = value.ToLower();
            }
        }

		/// <summary>
		/// 
		/// </summary>
        public string ThumbFilePath 
		{
			get
			{
				return _thumbFilePath;
			}
			set
			{
				_thumbFilePath = value;
			}
		}

        public string ThumbFileCloudPath
        {
            get
            {
                return _thumbFileCloudPath.ToLower();
            }
            set
            {
                _thumbFileCloudPath = value.ToLower();
            }
        }

		/// <summary>
		/// 
		/// </summary>
		public bool IsDirty
		{
			get
			{
				return _isDirty;
			}
			set
			{
				_isDirty = value;
			}
		}

		public string Caption 
		{
			get
			{
				return _caption;
			}
			set
			{
				_caption = value;
			}
		}

		public bool IsCaptionApproved
		{
			get
			{
				return _isCaptionApproved;
			}
			set
			{
				_isCaptionApproved = value;
			}
        }
        /// <summary>
        /// Indicates if this photo is the main photo or not. We used photo ordering to determine the main photo before.
        /// With this property, we are decoupling the idea of "main" photo from the photo ordering.  We are going to still
        /// move the main photo to the photo slot 1 since there are things that depend on that ordering, but slowly we will
        /// decouple the photo ordering and the main photo concept.
        /// </summary>
	    public bool IsMain
	    {
	        get { return _isMain; }
            set { _isMain = value; }
	    }
        /// <summary>
        /// Is this photo allowed to be the main photo?
        /// </summary>
	    public bool IsApprovedForMain
	    {
            get { return _isApprovedForMain; }
            set { _isApprovedForMain = value; }
	    }

        public int FileHeight
        {
            get { return _fileHeight; }
            set { _fileHeight = value; }
        }

        
        public int FileWidth
        {
            get { return _fileWidth; }
            set { _fileWidth = value; }
        }

        public int FileSize
        {
            get { return _fileSize; }
            set { _fileSize = value; }
        }

        public int ThumbFileHeight
        {
            get { return _thumbFileHeight; }
            set { _thumbFileHeight = value; }
        }

        public int ThumbFileWidth
        {
            get { return _thumbFileWidth; }
            set { _thumbFileWidth = value; }
        }

        public int ThumbFileSize
        {
            get { return _thumbFileSize; }
            set { _thumbFileSize = value; }
        }

        public List<PhotoFile> Files
	    {
            get { return _files; }
            set { _files = value; }
	    }
        #endregion

    }
}
