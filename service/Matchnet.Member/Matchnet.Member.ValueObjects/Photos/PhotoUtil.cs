using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// Summary description for PhotoUtil.
	/// </summary>
	public class PhotoUtil
	{
		// Photo Approval Subject
		/// <summary>
		/// 
		/// </summary>
		public const int MEMBER_PHOTO = 521414;
		/// <summary>
		/// 
		/// </summary>
		public const int PLEASE_LOGIN_AND_RESUBMIT_YOUR_PHOTOS = 519384;

		// Photo Approval Email Body
		/// <summary>
		/// 
		/// </summary>
		public const int PHOTO_APPROVED_RESOURCEID = 518162;
		/// <summary>
		/// 
		/// </summary>
		public const int BAD_FILE_FORMAT_RESOURCEID = 518582;
		/// <summary>
		/// 
		/// </summary>
		public const int COPYRIGHT_REJECT_LETTER_RESOURCEID = 518000;
		/// <summary>
		/// 
		/// </summary>
		public const int BLURY_REJECT_LETTER_RESOURCEID = 518001;
		/// <summary>
		/// 
		/// </summary>
		public const int FILE_SIZE_REJECT_LETTER_RESOURCEID = 518002;
		/// <summary>
		/// 
		/// </summary>
		public const int GENERAL_REJECT_LETTER_RESOURCEID = 518004;
		/// <summary>
		/// 
		/// </summary>
		public const int UNKNOWN_MEMBER_RESOURCEID = 518005;
		/// <summary>
		/// 
		/// </summary>
		public const int SUGGESTIVE_PHOTO_RESOURCEID = 518006;

		private static int _MaxPhotoWidth = Matchnet.Constants.NULL_INT;
		private static int _MaxPhotoHeight = Matchnet.Constants.NULL_INT;
		private static int _YNM4MaxPhotoWidth = Matchnet.Constants.NULL_INT;
		private static int _YNM4MaxPhotoHeight = Matchnet.Constants.NULL_INT;
		private static int _MaxPhotoSizeBytes = Matchnet.Constants.NULL_INT;
		private static int _ThumbnailWidth = Matchnet.Constants.NULL_INT;
		private static int _ThumbnailHeight = Matchnet.Constants.NULL_INT;
		private static int _DefaultPhotoSizeBytes = Matchnet.Constants.NULL_INT;
		private static string _MSMQPath = Matchnet.Constants.NULL_STRING;

		// Adminaction mask
		/// <summary>
		/// 
		/// </summary>
		public const int ADMIN_ACTION_REJECT_PHOTO = 16;
		/// <summary>
		/// 
		/// </summary>
		public const int ADMIN_ACTION_APPROVE_PHOTO = 8;


	}
}
