/*
using System;
using System.Collections;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Represents a collection of member profile photos.  (Photo objects).  A PhotoCollection is used to group all photos belonging to a particular user for a given community.  A PhotoCollection also manages the photo display order (listOrder). 
	/// </summary>
	[Serializable]
	public class PhotoCollection_ : CollectionBase, IValueObject, ICloneable
	{
		/// <summary>
		/// Initializes a new Member.ValueObjects.PhotoCollection object.
		/// </summary>
		public PhotoCollection_()
		{

		}

		/// <summary>
		/// Indexer for accessing photo in this collection for the specified index.
		/// </summary>
		public Photo this[int index]
		{
			get
			{
				return (Photo)base.InnerList[index];
			}
		}

		/// <summary>
		/// Adds a photo to this collection.
		/// </summary>
		/// <param name="photo">The Photo that is to be added.</param>
		/// <returns>The index of the newly added photo.</returns>
		public int Add(Photo photo)
		{
			// PRE-CONDITION: A member profile photo shall not not be null.
			if(photo == null)
			{
				throw(new ArgumentNullException("photo", "Cannot add null Photo object to the PhotoCollection."));
			}

			// PRE-CONDITION: A member profile photo shall not be added twice to the same collection.
			if(Exists(photo.MemberPhotoID))
			{
				throw(new Exception("Cannot add the Photo because it already exists in the PhotoCollection (MemberPhotoID: " + photo.MemberPhotoID + " )."));
			}

			// Return the index of the newly added photo.
			return base.InnerList.Add(photo);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="photo"></param>
		public void Update(Photo photo)
		{
			// PRE-CONDITION: A member profile photo shall not not be null.
			if(photo == null)
			{
				throw(new ArgumentNullException("photo", "Cannot update null Photo object to the PhotoCollection."));
			}

			// Replace the photo.
			for(int index = 0; index < this.Count; index++)
			{
				if(this[index].MemberPhotoID == photo.MemberPhotoID)
				{
					// Replace/update the existing photo and bail out
					base.InnerList[index] = photo;
					this.MarkAsClean();
					return;
				}
			}

			// Add the photo if it cannot be found in the collection.
			this.Add(photo);
		}

		/// <summary>
		/// Returns Boolean indicating whether or not the specified photo exists in this collection.
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <returns></returns>
		public bool Exists(int memberPhotoID)
		{
			Photo photo = null;
			return TryFind(memberPhotoID, out photo);
		}


		/// <summary>
		/// Returns the photo given the specified photoID (or throws an exception if not found).
		/// </summary>
		/// <param name="memberPhotoID">The unique identifier for the photo.</param>
		/// <returns>The photo object.</returns>
		public Photo Find(int memberPhotoID)
		{
			Photo photo = null;
			if(TryFind(memberPhotoID, out photo))
			{
				// Return the photo
				return photo;
			}
			else
			{
				// Throw an exception if the photo is not found.
				throw(new Exception("Could not find the specified Photo in the PhotoCollection (MemberPhotoID=" + memberPhotoID.ToString() + ")"));
			}
		}

		/// <summary>
		/// Returns a Boolean indicating whether or not the photo could be found (and may return the Photo as an output parameter).
		/// </summary>
		/// <param name="memberPhotoID">The unique identifier for the photo.</param>
		/// <param name="photo">The photo if found, otherwise null.</param>
		/// <returns>Boolean indicating whether or not the photo was found.</returns>
		public bool TryFind(int memberPhotoID, out Photo photo)
		{
			// Loop through and try to find the photo
			for(int index = 0; index < this.Count; index++)
			{
				if(this[index].MemberPhotoID == memberPhotoID)
				{
					// Found the photo
					photo = this[index];
					return true;
				}
			}

			// Could not find the photo in this collecton
			photo = null;
			return false;
		}

		/// <summary>
		/// Marks all the photo object in this collection as "clean" indicating that they have been saved.
		/// </summary>
		public void MarkAsClean()
		{
			for(int index = 0; index < this.Count; index++)
			{
				this[index].MarkAsClean();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsDirty
		{
			get
			{
				for(int index = 0; index < this.Count; index++)
				{
					if(this[index].IsDirty)
					{
						return true;
					}
				}
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public object Clone()
		{
			return (PhotoCollection)this.MemberwiseClone();		
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public PhotoCollection GetPhotosByCommunity(int communityID)
		{
			PhotoCollection communityPhotos = new PhotoCollection();
			for(int index = 0; index < this.Count; index++)
			{
				if(this[index].CommunityID == communityID)
				{
					communityPhotos.Add(this[index]);
				}
			}

			return communityPhotos;
		}
	}
}
*/