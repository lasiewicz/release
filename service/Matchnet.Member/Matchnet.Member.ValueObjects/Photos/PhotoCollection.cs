using System;
using System.Collections;


namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PhotoCollection
	{
		private Hashtable _communities;

		/// <summary>
		/// 
		/// </summary>
		public PhotoCollection()
		{
			_communities = new Hashtable();
		}

        /// <summary>
        /// Returns community Id's that are currently contained in the Communities hashtable.
        /// </summary>
	    public ICollection CommunityIds
	    {
	        get { return _communities.Keys; }
	    }

		internal Hashtable Communities
		{
			get
			{
				return _communities;
			}
		}


		public void StripHostFromPaths(Int32 communityID)
		{
			PhotoCommunity photoCommunity = _communities[communityID] as PhotoCommunity;
			if (photoCommunity != null)
			{
				photoCommunity.StripHostFromPaths();
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public PhotoCommunity GetCommunity(Int32 communityID)
		{
            PhotoCommunity photoCommunity = null;

            if (_communities.ContainsKey(communityID))
            {
                photoCommunity = _communities[communityID] as PhotoCommunity;
            }
            else
            {
                lock (_communities.SyncRoot)
                {
                    if (_communities.ContainsKey(communityID))
                    {
                        photoCommunity = _communities[communityID] as PhotoCommunity;
                    }
                    else
                    {
                        photoCommunity = new PhotoCommunity();
                        _communities.Add(communityID,
                            photoCommunity);
                    }
                }
            }

            return photoCommunity;
		}
	}
}
