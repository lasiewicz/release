using System;

namespace Matchnet.Member.ValueObjects.Photos
{
	using System;
	//using System.Drawing;

	//using Matchnet.Lib;

	/// <summary>
	/// Summary description for MemberPhoto.
	/// </summary>
	public class MemberPhoto : BasePhoto
	{
		#region Private Members

		private ThumbNail _ThumbNail = new ThumbNail();
		private int _MemberPhotoID = Matchnet.Constants.NULL_INT;
		private int _MemberID = Matchnet.Constants.NULL_INT;
		private int _DomainID = Matchnet.Constants.NULL_INT;
		private string _EmailAddress = Matchnet.Constants.NULL_STRING;
		private int _PrivateFlag = 0; // 0 - Public 1 - Private
		private int _ApprovedFlag = 0;
		private int _PrivateLabelID = Matchnet.Constants.NULL_INT;
		private int _AdminMemberID = Matchnet.Constants.NULL_INT;
		private int _ListOrder = Matchnet.Constants.NULL_INT;

		#endregion

		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		public MemberPhoto()
		{
			
		}

		#endregion


		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public int AdminMemberID
		{
			get
			{
				return _AdminMemberID;
			}
			set
			{
				_AdminMemberID = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _EmailAddress;
			}
			set
			{
				_EmailAddress = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public int PrivateLabelID
		{
			get
			{
				return _PrivateLabelID;
			}
			set
			{
				_PrivateLabelID = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public int PrivateFlag
		{
			get
			{
				return _PrivateFlag;
			}
			set
			{
				_PrivateFlag = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public int MemberPhotoID
		{
			get
			{
				return _MemberPhotoID;
			}
			set
			{
				_MemberPhotoID = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public int DomainID
		{
			get
			{
				return _DomainID;
			}
			set
			{
				_DomainID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ApprovedFlag
		{
			get
			{
				return _ApprovedFlag;
			}
			set
			{
				_ApprovedFlag = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ThumbNail ThumbNail
		{
			get
			{
				return _ThumbNail;
			}
			set
			{
				_ThumbNail = value;
			}
		}
	
		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrder
		{
			get
			{
				return _ListOrder;
			}
			set
			{
				_ListOrder = value;
			}
		}

		#endregion
	}

	/// <summary>
	/// 
	/// </summary>
	public class ThumbNail : BasePhoto
	{
	}
}
