using System;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class FileInstance : IComparable
	{
		private bool _isPrimary;
		string _path;
		string _webPath;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="isPrimary"></param>
		/// <param name="path"></param>
		/// <param name="webPath"></param>
		public FileInstance(bool isPrimary,
			string path,
			string webPath)
		{
			_isPrimary = isPrimary;
			_path = path;
			_webPath = webPath;
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsPrimary
		{
			get
			{
				return _isPrimary;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Path
		{
			get
			{
				return _path;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string WebPath
		{
			get
			{
				return _webPath;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			if (obj.GetType().Name != "FileInstance")
			{
				return 0;
			}

			FileInstance fileInstance = obj as FileInstance;

			if (fileInstance.IsPrimary && !this.IsPrimary)
			{
				return 1;
			}
			else if (!fileInstance.IsPrimary && this.IsPrimary)
			{
				return -1;
			}

			return 0;
		}
	}
}
