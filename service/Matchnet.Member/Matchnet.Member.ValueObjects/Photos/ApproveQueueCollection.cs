using System;
using System.Collections;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// Summary description for ApproveQueueCollection.
	/// </summary>
	public class ApproveQueueCollection : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public ApproveQueueCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="approveQueue"></param>
		public void Add(ApproveQueue approveQueue)
		{
			base.InnerList.Add(approveQueue);
		}

		/// <summary>
		/// 
		/// </summary>
		public ApproveQueue this[int index]
		{
			get
			{
				return (ApproveQueue) base.InnerList[index];
			}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add ApproveQueueCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add ApproveQueueCollection.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add ApproveQueueCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add ApproveQueueCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add ApproveQueueCollection.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add ApproveQueueCollection.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
