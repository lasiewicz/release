using System;
using System.Collections.Generic;

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public enum ApproveStatusType
	{
		/// <summary>
		/// 
		/// </summary>
		Approved = 0,
		/// <summary>
		/// 
		/// </summary>
		Ignore = 1,
		/// <summary>
		/// 
		/// </summary>
		Rejected_Blurry = 2,
		/// <summary>
		/// 
		/// </summary>
		Rejected_Copyright = 3,
		/// <summary>
		/// 
		/// </summary>
		Rejected_FileSize = 4,
		/// <summary>
		/// 
		/// </summary>
		Rejected_General = 5,
		/// <summary>
		/// 
		/// </summary>
		Rejected_Suggestive = 6,
		/// <summary>
		/// 
		/// </summary>
		Rejected_UnknownMember = 7,
		/// <summary>
		/// 
		/// </summary>
		Rejected_BadFileFormat = 8,
        /// <summary>
        /// 
        /// </summary>
        Approved_PossibleFraud = 9,
        /// <summary>
        /// 
        /// </summary>
        Approved_ButSuspend = 10
	
	};

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ApproveInfo
    {
        #region Data Members
        private Int32 _memberPhotoID;
		private ApproveStatusType _approveStatus;
		private Int32 _fileID;
		private Int32 _fileIDThumb;
		private Int16 _rotateAngle;
		private Int16 _cropX;
		private Int16 _cropY;
		private Int16 _cropWidth;
		private Int16 _cropHeight;
		private Int16 _rotateAngleThumb;
		private Int16 _cropXThumb;
		private Int16 _cropYThumb;
		private Int16 _cropWidthThumb;
		private Int16 _cropHeightThumb;
		private Int32 _adminMemberID;
		private String _filePath;
		private String _fileWebPath;
        private String _fileCloudPath;
        private String _thumbFilePath;
		private String _thumbWebPath;
        private String _thumbFileCloudPath;
		private Int32 _memberID;
		private Int32 _communityID;
	    private Int32 _siteID;
		private bool _captionDeleteFlag=false;
		private bool _captionOnlyApproval=false;
	    private bool _isApprovedForMain = false;
        private int _fileHeight = Constants.NULL_INT;
        private int _fileWidth = Constants.NULL_INT;
        private int _fileSize = Constants.NULL_INT;
        private int _thumbFileHeight = Constants.NULL_INT;
        private int _thumbFileWidth = Constants.NULL_INT;
        private int _thumbFileSize = Constants.NULL_INT;
	    private const int _version = 1;
	    private List<PhotoFile> _files;
        #endregion

        #region Ctors
        /// <summary>
		/// 
		/// </summary>
		public ApproveInfo()
		{
            _files = new List<PhotoFile>();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <param name="approveStatus"></param>
		/// <param name="fileID"></param>
		/// <param name="fileIDThumb"></param>
		/// <param name="rotateAngle"></param>
		/// <param name="cropX"></param>
		/// <param name="cropY"></param>
		/// <param name="cropWidth"></param>
		/// <param name="cropHeight"></param>
		/// <param name="rotateAngleThumb"></param>
		/// <param name="cropXThumb"></param>
		/// <param name="cropYThumb"></param>
		/// <param name="cropWidthThumb"></param>
		/// <param name="cropHeightThumb"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		public ApproveInfo(Int32 memberPhotoID,
			ApproveStatusType approveStatus,
			Int32 fileID,
			Int32 fileIDThumb,
			Int16 rotateAngle,
			Int16 cropX,
			Int16 cropY,
			Int16 cropWidth,
			Int16 cropHeight,
			Int16 rotateAngleThumb,
			Int16 cropXThumb,
			Int16 cropYThumb,
			Int16 cropWidthThumb,
			Int16 cropHeightThumb,
			Int32 adminMemberID,
			Int32 communityID,
			Int32 memberID)
		{
			_memberPhotoID = memberPhotoID;
			_approveStatus = approveStatus;
			_fileID = fileID;
			_fileIDThumb = fileIDThumb;
			_rotateAngle = rotateAngle;
			_cropX = cropX;
			_cropY = cropY;
			_cropWidth = cropWidth;
			_cropHeight = cropHeight;
			_rotateAngleThumb = rotateAngleThumb;
			_cropXThumb = cropXThumb;
			_cropYThumb = cropYThumb;
			_cropWidthThumb = cropWidthThumb;
			_cropHeightThumb = cropHeightThumb;
			_adminMemberID = adminMemberID;
			_communityID = communityID;
			_memberID = memberID;
            _files = new List<PhotoFile>();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <param name="approveStatus"></param>
		/// <param name="fileID"></param>
		/// <param name="fileIDThumb"></param>
		/// <param name="rotateAngle"></param>
		/// <param name="cropX"></param>
		/// <param name="cropY"></param>
		/// <param name="cropWidth"></param>
		/// <param name="cropHeight"></param>
		/// <param name="rotateAngleThumb"></param>
		/// <param name="cropXThumb"></param>
		/// <param name="cropYThumb"></param>
		/// <param name="cropWidthThumb"></param>
		/// <param name="cropHeightThumb"></param>
		/// <param name="adminMemberID"></param>
		public ApproveInfo(Int32 memberPhotoID,
			ApproveStatusType approveStatus,
			Int32 fileID,
			Int32 fileIDThumb,
			Int16 rotateAngle,
			Int16 cropX,
			Int16 cropY,
			Int16 cropWidth,
			Int16 cropHeight,
			Int16 rotateAngleThumb,
			Int16 cropXThumb,
			Int16 cropYThumb,
			Int16 cropWidthThumb,
			Int16 cropHeightThumb,
			Int32 adminMemberID)
		{
			_memberPhotoID = memberPhotoID;
			_approveStatus = approveStatus;
			_fileID = fileID;
			_fileIDThumb = fileIDThumb;
			_rotateAngle = rotateAngle;
			_cropX = cropX;
			_cropY = cropY;
			_cropWidth = cropWidth;
			_cropHeight = cropHeight;
			_rotateAngleThumb = rotateAngleThumb;
			_cropXThumb = cropXThumb;
			_cropYThumb = cropYThumb;
			_cropWidthThumb = cropWidthThumb;
			_cropHeightThumb = cropHeightThumb;
			_adminMemberID = adminMemberID;
            _files = new List<PhotoFile>();
		}



		//adding caption delete flag
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <param name="approveStatus"></param>
		/// <param name="fileID"></param>
		/// <param name="fileIDThumb"></param>
		/// <param name="rotateAngle"></param>
		/// <param name="cropX"></param>
		/// <param name="cropY"></param>
		/// <param name="cropWidth"></param>
		/// <param name="cropHeight"></param>
		/// <param name="rotateAngleThumb"></param>
		/// <param name="cropXThumb"></param>
		/// <param name="cropYThumb"></param>
		/// <param name="cropWidthThumb"></param>
		/// <param name="cropHeightThumb"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		public ApproveInfo(Int32 memberPhotoID,
			ApproveStatusType approveStatus,
			Int32 fileID,
			Int32 fileIDThumb,
			Int16 rotateAngle,
			Int16 cropX,
			Int16 cropY,
			Int16 cropWidth,
			Int16 cropHeight,
			Int16 rotateAngleThumb,
			Int16 cropXThumb,
			Int16 cropYThumb,
			Int16 cropWidthThumb,
			Int16 cropHeightThumb,
			Int32 adminMemberID,
			Int32 communityID,
			Int32 memberID,
			bool captionDelete,
			bool captionOnly, 
            Int32 siteID
			)
		{
			_memberPhotoID = memberPhotoID;
			_approveStatus = approveStatus;
			_fileID = fileID;
			_fileIDThumb = fileIDThumb;
			_rotateAngle = rotateAngle;
			_cropX = cropX;
			_cropY = cropY;
			_cropWidth = cropWidth;
			_cropHeight = cropHeight;
			_rotateAngleThumb = rotateAngleThumb;
			_cropXThumb = cropXThumb;
			_cropYThumb = cropYThumb;
			_cropWidthThumb = cropWidthThumb;
			_cropHeightThumb = cropHeightThumb;
			_adminMemberID = adminMemberID;
			_communityID = communityID;
			_memberID = memberID;
			_captionDeleteFlag=captionDelete;
			_captionOnlyApproval=captionOnly;
		    _siteID = siteID;
            _files = new List<PhotoFile>();
		}

	    /// <summary>
		/// 
		/// </summary>
		/// <param name="memberPhotoID"></param>
		/// <param name="approveStatus"></param>
		/// <param name="fileID"></param>
		/// <param name="fileIDThumb"></param>
		/// <param name="rotateAngle"></param>
		/// <param name="cropX"></param>
		/// <param name="cropY"></param>
		/// <param name="cropWidth"></param>
		/// <param name="cropHeight"></param>
		/// <param name="rotateAngleThumb"></param>
		/// <param name="cropXThumb"></param>
		/// <param name="cropYThumb"></param>
		/// <param name="cropWidthThumb"></param>
		/// <param name="cropHeightThumb"></param>
		/// <param name="adminMemberID"></param>
		public ApproveInfo(Int32 memberPhotoID,
			ApproveStatusType approveStatus,
			Int32 fileID,
			Int32 fileIDThumb,
			Int16 rotateAngle,
			Int16 cropX,
			Int16 cropY,
			Int16 cropWidth,
			Int16 cropHeight,
			Int16 rotateAngleThumb,
			Int16 cropXThumb,
			Int16 cropYThumb,
			Int16 cropWidthThumb,
			Int16 cropHeightThumb,
			Int32 adminMemberID,
			bool captionDelete,
            bool captionOnly)
            : this(memberPhotoID,approveStatus,fileID,fileIDThumb,rotateAngle,cropX,cropY,cropWidth,cropHeight,rotateAngleThumb,cropXThumb,
			    cropYThumb,cropWidthThumb,cropHeightThumb,adminMemberID,captionDelete,captionOnly, Constants.NULL_INT)
            {

		    }

        public ApproveInfo(Int32 memberPhotoID,
            ApproveStatusType approveStatus,
            Int32 fileID,
            Int32 fileIDThumb,
            Int16 rotateAngle,
            Int16 cropX,
            Int16 cropY,
            Int16 cropWidth,
            Int16 cropHeight,
            Int16 rotateAngleThumb,
            Int16 cropXThumb,
            Int16 cropYThumb,
            Int16 cropWidthThumb,
            Int16 cropHeightThumb,
            Int32 adminMemberID,
            bool captionDelete,
            bool captionOnly,
            Int32 siteID)
        {
            _memberPhotoID = memberPhotoID;
            _approveStatus = approveStatus;
            _fileID = fileID;
            _fileIDThumb = fileIDThumb;
            _rotateAngle = rotateAngle;
            _cropX = cropX;
            _cropY = cropY;
            _cropWidth = cropWidth;
            _cropHeight = cropHeight;
            _rotateAngleThumb = rotateAngleThumb;
            _cropXThumb = cropXThumb;
            _cropYThumb = cropYThumb;
            _cropWidthThumb = cropWidthThumb;
            _cropHeightThumb = cropHeightThumb;
            _adminMemberID = adminMemberID;
            _captionDeleteFlag = captionDelete;
            _captionOnlyApproval = captionOnly;
            _siteID = siteID;
            _files = new List<PhotoFile>();
        }
        #endregion

        #region Properties
        /// <summary>
		/// 
		/// </summary>
		public Int32 MemberPhotoID
		{
			get
			{
				return _memberPhotoID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public ApproveStatusType ApproveStatus
		{
			get
			{
				return _approveStatus;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 FileID
		{
			get
			{
				return _fileID;
			}
			set
			{
				_fileID = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 FileIDThumb
		{
			get
			{
				return _fileIDThumb;
			}
			set
			{
				_fileIDThumb = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 RotateAngle
		{
			get
			{
				return _rotateAngle;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropX
		{
			get
			{
				return _cropX;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropY
		{
			get
			{
				return _cropY;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropWidth
		{
			get
			{
				return _cropWidth;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropHeight
		{
			get
			{
				return _cropHeight;
			}
		}		

		/// <summary>
		/// 
		/// </summary>
		public Int16 RotateAngleThumb
		{
			get
			{
				return _rotateAngleThumb;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropXThumb
		{
			get
			{
				return _cropXThumb;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropYThumb
		{
			get
			{
				return _cropYThumb;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropWidthThumb
		{
			get
			{
				return _cropWidthThumb;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CropHeightThumb
		{
			get
			{
				return _cropHeightThumb;
			}
		}		


		/// <summary>
		/// 
		/// </summary>
		public Int32 AdminMemberID
		{
			get
			{
				return _adminMemberID;
			}
		}	
	
		/// <summary>
		/// 
		/// </summary>
		public String FilePath 
		{
			get
			{
				return _filePath;
			}
			set
			{
				_filePath = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public String FileWebPath 
		{
			get
			{
				return _fileWebPath;
			}
			set
			{
				_fileWebPath = value;
			}
		}

        public String FileCloudPath 
		{
			get
			{
				return _fileCloudPath;
			}
			set
			{
                _fileCloudPath = value;
			}
		}

        public String ThumbFileCloudPath 
		{
			get
			{
                return _thumbFileCloudPath;
			}
			set
			{
                _thumbFileCloudPath = value;
			}
		}
        

		/// <summary>
		/// 
		/// </summary>
		public String ThumbFilePath 
		{
			get
			{
				return _thumbFilePath;
			}
			set
			{
				_thumbFilePath = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public String ThumbWebPath 
		{
			get
			{
				return _thumbWebPath;
			}
			set
			{
				_thumbWebPath = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID 
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 CommunityID 
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

	    public Int32 SiteID
	    {
	        get { return _siteID; }
            set { _siteID = value; }
	    }

		/// <summary>
		/// 
		/// </summary>
		public bool CaptionDeleteFlag
		{
			get
			{
				return _captionDeleteFlag;
			}
			set
			{
				_captionDeleteFlag = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool CaptionOnlyApproval
		{
			get
			{
				return _captionOnlyApproval;
			}
			set
			{
				_captionOnlyApproval = value;
			}
        }

        /// <summary>
        /// This value indicates if the member can use this photo as a main photo.
        /// </summary>
	    public bool IsApprovedForMain
	    {
	        get { return _isApprovedForMain; }
            set { _isApprovedForMain = value; }
	    }


        public int FileHeight
        {
            get { return _fileHeight; }
            set { _fileHeight = value; }
        }


        public int FileWidth
        {
            get { return _fileWidth; }
            set { _fileWidth = value; }
        }

        public int FileSize
        {
            get { return _fileSize; }
            set { _fileSize = value; }
        }

        public int ThumbFileHeight
        {
            get { return _thumbFileHeight; }
            set { _thumbFileHeight = value; }
        }

        public int ThumbFileWidth
        {
            get { return _thumbFileWidth; }
            set { _thumbFileWidth = value; }
        }

        public int ThumbFileSize
        {
            get { return _thumbFileSize; }
            set { _thumbFileSize = value; }
        }

        public List<PhotoFile> Files
        {
            get { return _files; }
            set { _files = value; }
        }
        /// <summary>
        /// Since this object is used by 3 different components (admintool, approvequeue, photoapprove), having this version
        /// can offer flexibility for deployments.  Added on 4/2/2012.
        /// </summary>
	    public int Version
	    {
            get { return _version;  }
	    }
        #endregion

    }
}
