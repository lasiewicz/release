#region

using System;

#endregion

namespace Matchnet.Member.ValueObjects.Photos
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PhotoUpdate : Photo
	{
		private bool _delete;
		private byte[] _imageData;
		private bool _isNew = false;
		private bool _isNewCaption = false;
        private bool _includeIsMainValue = true;
	    private string _originalFileCloudPath = string.Empty;
        
		/// <summary>
		/// 
		/// </summary>
		/// <param name="imageData"></param>
		/// <param name="delete"></param>
		/// <param name="memberPhotoID"></param>
		/// <param name="fileID"></param>
		/// <param name="fileWebPath"></param>
		/// <param name="thumbFileID"></param>
		/// <param name="thumbFileWebPath"></param>
		/// <param name="listOrder"></param>
		/// <param name="isApproved"></param>
		/// <param name="isPrivate"></param>
		/// <param name="albumID"></param>
		/// <param name="adminMemberID"></param>
		/// 
		public PhotoUpdate(){}

        public PhotoUpdate(byte[] imageData,
			bool delete,
			Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID)
		{
			_imageData = imageData;
			_delete = delete;
			base._memberPhotoID = memberPhotoID;
			base._fileID = fileID;
			base._fileWebPath = fileWebPath;
			base._thumbFileID = thumbFileID;
			base._thumbFileWebPath = thumbFileWebPath;
			base._listOrder = listOrder;
			base._isApproved = isApproved;
			base._isPrivate = isPrivate;
			base._albumID = albumID;
			base._adminMemberID = adminMemberID;
            

			if (fileID == Constants.NULL_INT)
			{
				_isNew = true;
			}
		}

		public PhotoUpdate(byte[] imageData,
			bool delete,
			Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID,
			string caption,
			bool isCaptionApproved)
		{
			_imageData = imageData;
			_delete = delete;
			base._memberPhotoID = memberPhotoID;
			base._fileID = fileID;
			base._fileWebPath = fileWebPath;
			base._thumbFileID = thumbFileID;
			base._thumbFileWebPath = thumbFileWebPath;
			base._listOrder = listOrder;
			base._isApproved = isApproved;
			base._isPrivate = isPrivate;
			base._albumID = albumID;
			base._adminMemberID = adminMemberID;
			
			base._isCaptionApproved=isCaptionApproved;
			
			if (fileID == Constants.NULL_INT  )
			{
				_isNew = true;
			}

			base.Caption=caption;
			if(caption != null && caption.Trim() != "")
			{
				_isNewCaption=!isCaptionApproved;
				
			}

		}

        public PhotoUpdate(byte[] imageData,
			bool delete,
			Int32 memberPhotoID,
			Int32 fileID,
			string fileWebPath,
			Int32 thumbFileID,
			string thumbFileWebPath,
			byte listOrder,
			bool isApproved,
			bool isPrivate,
			Int32 albumID,
			Int32 adminMemberID,
			string caption,
			bool isCaptionApproved, 
            string fileCloudPath,
            string thumbFileCloudPath) : this(imageData, delete, memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath,
            listOrder, isApproved, isPrivate, albumID, adminMemberID, caption,  isCaptionApproved)
        {
            if (!string.IsNullOrEmpty(fileCloudPath))
            {
                this.FileCloudPath = fileCloudPath;
            }
            if (!string.IsNullOrEmpty(thumbFileCloudPath))
            {
                this.ThumbFileCloudPath = thumbFileCloudPath;
            }
        }

        /// <summary>
        /// This ctor was introduced with the concept of main photo in mind.  Although not fully supported from the start, any photo
        /// in the member's photo colletion can be th emain photo.
        /// </summary>
        /// <param name="imageData"></param>
        /// <param name="delete"></param>
        /// <param name="memberPhotoID"></param>
        /// <param name="fileID"></param>
        /// <param name="fileWebPath"></param>
        /// <param name="thumbFileID"></param>
        /// <param name="thumbFileWebPath"></param>
        /// <param name="listOrder"></param>
        /// <param name="isApproved"></param>
        /// <param name="isPrivate"></param>
        /// <param name="albumID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="caption"></param>
        /// <param name="isCaptionApproved"></param>
        /// <param name="fileCloudPath"></param>
        /// <param name="thumbFileCloudPath"></param>
        /// <param name="isApprovedForMain"></param>
        /// <param name="isMain"></param>
        public PhotoUpdate(byte[] imageData,
                           bool delete,
                           Int32 memberPhotoID,
                           Int32 fileID,
                           string fileWebPath,
                           Int32 thumbFileID,
                           string thumbFileWebPath,
                           byte listOrder,
                           bool isApproved,
                           bool isPrivate,
                           Int32 albumID,
                           Int32 adminMemberID,
                           string caption,
                           bool isCaptionApproved,
                           string fileCloudPath,
                           string thumbFileCloudPath,
                           bool isApprovedForMain,
                           bool isMain,
                           bool includeIsMainValue)
            : this(imageData, delete, memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath,
                   listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved, fileCloudPath,
                   thumbFileCloudPath)
        {
            IsApprovedForMain = isApprovedForMain;
            IsMain = isMain;
            _includeIsMainValue = includeIsMainValue;
        }

        /// <summary>
        /// This ctor was introduced with the concept of main photo in mind.  Although not fully supported from the start, any photo
        /// in the member's photo colletion can be th emain photo.
        /// </summary>
        /// <param name="imageData"></param>
        /// <param name="delete"></param>
        /// <param name="memberPhotoID"></param>
        /// <param name="fileID"></param>
        /// <param name="fileWebPath"></param>
        /// <param name="thumbFileID"></param>
        /// <param name="thumbFileWebPath"></param>
        /// <param name="listOrder"></param>
        /// <param name="isApproved"></param>
        /// <param name="isPrivate"></param>
        /// <param name="albumID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="caption"></param>
        /// <param name="isCaptionApproved"></param>
        /// <param name="fileCloudPath"></param>
        /// <param name="thumbFileCloudPath"></param>
        /// <param name="isApprovedForMain"></param>
        /// <param name="isMain"></param>
        public PhotoUpdate(byte[] imageData,
                           bool delete,
                           Int32 memberPhotoID,
                           Int32 fileID,
                           string fileWebPath,
                           Int32 thumbFileID,
                           string thumbFileWebPath,
                           byte listOrder,
                           bool isApproved,
                           bool isPrivate,
                           Int32 albumID,
                           Int32 adminMemberID,
                           string caption,
                           bool isCaptionApproved,
                           string fileCloudPath,
                           string thumbFileCloudPath,
                           bool isApprovedForMain,
                           bool isMain,
                           bool includeIsMainValue, 
                           int fileHeight,
                           int fileWidth, 
                           int fileSize, 
                           int  thumbFileHeight, 
                           int thumbFileWidth, 
                           int  thumbFileSize)
            : this(imageData, delete, memberPhotoID, fileID, fileWebPath, thumbFileID, thumbFileWebPath,
                   listOrder, isApproved, isPrivate, albumID, adminMemberID, caption, isCaptionApproved, fileCloudPath,
                   thumbFileCloudPath, isApprovedForMain, isMain, includeIsMainValue)
        {
            _fileHeight = fileHeight;
            _fileWidth = fileWidth;
            _fileSize = fileSize;
            _thumbFileHeight = thumbFileHeight;
            _thumbFileWidth = thumbFileWidth;
            _thumbFileSize = thumbFileSize;
        }


        #region Properties
        /// <summary>
		/// 
		/// </summary>
		public byte[] ImageData
		{
			get
			{
				return _imageData;
			}
			set
			{
				_imageData = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 FileID
		{
			get
			{
				return base._fileID;
			}
			set
			{
				base._fileID = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string FileWebPath
		{
			get
			{
				return base._fileWebPath;
			}
			set
			{
				base._fileWebPath = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool Delete
		{
			get
			{
				return _delete;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsNew
		{
			get
			{
				return _isNew;
			}
		}

		public bool IsNewCaption
		{

			get
			{
				return _isNewCaption;
			}
        }

	    public bool IncludeIsMainValue
	    {
            get { return _includeIsMainValue; }
	    }

	    public string OriginalFileCloudPath
	    {
            get { return _originalFileCloudPath; }
            set { _originalFileCloudPath = value; }
	    }

        #endregion
    }
}
