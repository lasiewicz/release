﻿#region

using System;

#endregion

namespace Matchnet.Member.ValueObjects.Photos
{
    public enum PhotoFileType
    {
        RegularFWS = 1,
        Thumbnail = 2,
        Original = 3,
        iPhone4S = 4,
        MinglePreview = 5
    }

    [Serializable]
    public class PhotoFile
    {
        public PhotoFile()
        {
        }

        public PhotoFile(PhotoFileType photoFileType, int fileHeight, int fileWidth, int fileSize, int fileID,
            string fileWebPath, string fileCloudPath)
        {
            PhotoFileType = photoFileType;
            FileHeight = fileHeight;
            FileWidth = fileWidth;
            FileSize = fileSize;
            FileID = fileID;
            FileWebPath = fileWebPath;
            FileCloudPath = fileCloudPath;
        }

        public PhotoFileType PhotoFileType { get; set; }
        public int FileHeight { get; set; }
        public int FileWidth { get; set; }
        public int FileSize { get; set; }
        public int FileID { get; set; }
        public string FileWebPath { get; set; }
        public string FileCloudPath { get; set; }
    }
}