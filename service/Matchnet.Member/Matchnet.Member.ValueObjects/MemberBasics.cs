using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// PRELIMINARY COMMENTS -- NEED TECHNICAL REVIEW: MemberBasics holds the information on a Member that is retrieved or updated upon a Member's login.     
	/// MemberBasics contains the information needed to determine whether the Member is active or an administrator, so that the Presentation Layer will know what UI content to provide.
	/// MemberBasics does not hold Attribute information on the Member.  
	/// Similarly, Photos and Photo Collections/Albums are handled by these Photo Value Objects.
	/// This object contains no setter methods.  
	/// </summary>
	[Serializable]
	public class MemberBasics
	{
		private string _emailAddress;
		private string _userName;
		private int _globalStatusMask;
		private bool _adminFlag;
		private DateTime _updateDate;
		private int _adminNoteCount;

		/// <summary></summary>
		/// <param name="emailAddress">The Member's email address.  Email addresses can be up to 255 characters and include any characters compatable with a valid email address.</param>
		/// <param name="userName">The Member's username.  usernames must be between 2 and 25 characters, and can include spaces, upper case, lower case, and special characters such as *.</param>
		/// <param name="globalStatusMask">globalStatusMask</param>
		/// <param name="adminFlag">Indicates whether or not the Member is an admin.  1=yes, 0=no.</param>
		/// <param name="updateDate">PRELIMINARY COMMENTS -- NEED TECHNICAL REVIEW.  The last datetime the Member logged in.  If the Member has just logged in this will reflect the current login datetime.</param>
		/// <param name="adminNoteCount">DOCUMENTATION NEEDED.  WHY IS THE NUMBER OF ADMIN NOTES RELEVANT?</param>
		public MemberBasics(string emailAddress, string userName, int globalStatusMask, bool adminFlag, DateTime updateDate, int adminNoteCount)
		{
			_emailAddress = emailAddress;
			_userName = userName;
			_globalStatusMask = globalStatusMask;
			_adminFlag = adminFlag;
			_updateDate = updateDate;
			_adminNoteCount = adminNoteCount;
		}

		/// <summary>Getter method for the Member's email address.</summary>
		public string EmailAddress
		{
			get { return(_emailAddress); }
		}

		/// <summary>Getter method for the Member's UserName</summary>
		public string UserName
		{
			get { return(_userName); }
		}

		/// <summary>Getter method for the Member's GlobalStatusMask.</summary>
		public int GlobalStatusMask
		{
			get { return(_globalStatusMask); }
		}

		/// <summary>Getter method for the Member's AdminFlag.</summary>
		public bool AdminFlag
		{
			get { return(_adminFlag); }
		}

		/// <summary>Getter method for the Member's UpdateDate</summary>
		public DateTime UpdateDate
		{
			get { return(_updateDate); }
		}

		/// <summary>Getter method for the Member's AdminNoteCount</summary>
		public int AdminNoteCount
		{
			get { return(_adminNoteCount); }
		}
	}
}
