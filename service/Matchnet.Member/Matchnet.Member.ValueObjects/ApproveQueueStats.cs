using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Used to represent approval queue (photo, free text) statistics for a single language/domain on a single member database.
	/// </summary>
	public class ApproveQueueStats
	{
		/// <summary>
		/// 
		/// </summary>
		public ApproveQueueStats()
		{
		}

		private int _count;
		private int _languageID;
		private string _language;
		private int _domainID;
		private string _domain;
		private string _database;
		private System.DateTime _oldestInsertDate;
		private System.DateTime _averageInsertDate;

		/// <summary>
		/// The number of items pending approval.
		/// </summary>
		public int Count
		{
			get
			{
				return _count;
			}
			set
			{
				_count = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int LanguageID
		{
			get
			{
				return _languageID;
			}
			set
			{
				_languageID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Language
		{
			get
			{
				return _language;
			}
			set
			{
				_language = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int DomainID
		{
			get
			{
				return _domainID;
			}
			set
			{
				_domainID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Domain
		{
			get
			{
				return _domain;
			}
			set
			{
				_domain = value;
			}
		}

		/// <summary>
		/// The name of the member database.
		/// </summary>
		public string Database
		{
			get
			{
				return _database;
			}
			set
			{
				_database = value;
			}
		}

		/// <summary>
		/// The date/time at which the oldest item was added to the queue.
		/// </summary>
		public DateTime OldestInsertDate
		{
			get
			{
				return _oldestInsertDate;
			}
			set
			{
				_oldestInsertDate = value;
			}
		}

		/// <summary>
		/// The average insert date/time for all items in the queue.
		/// </summary>
		public DateTime AverageInsertDate
		{
			get
			{
				return _averageInsertDate;
			}
			set
			{
				_averageInsertDate = value;
			}
		}
	}
}
