﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Member.ValueObjects.Enumerations
{
    [Serializable]
    public enum DeviceOS
    {
        Unknown = -1, //can't determine what it is
        None = 0, //no reg device data (e.g. members who registered before we started collecting)
        IPhone = 1,
        IPad = 2,
        Android_Phone = 3,
        Android_Tablet = 4,
        Blackberry = 5,
        Blackberry_Tablet = 6,
        Linux_Phone = 7,
        Palm_Phone = 8,
        Palm_Tablet = 9,
        Symbian_Phone = 10,
        Windows_Phone = 11,
        Windows_Tablet = 12,
        Apple_Desktop = 13,
        Windows_Desktop = 14,
        Linux_Desktop = 15
    }
}
