using System;
using System.Collections;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Summary description for MemberPrivilegeCollection.
	/// </summary>
	public class MemberPrivilegeCollection : CollectionBase	, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public MemberPrivilegeCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPrivilege"></param>
		public void Add(MemberPrivilege memberPrivilege)
		{
			base.InnerList.Add(memberPrivilege);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add MemberPrivilegeCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add MemberPrivilegeCollection.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add MemberPrivilegeCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add MemberPrivilegeCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add MemberPrivilegeCollection.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add MemberPrivilegeCollection.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
