using System;

namespace Matchnet.Member.ValueObjects
{
	//Note: The enumeration below depends on the implementation of up_MemberPrivilege_List as of 12/16/04
	//If a user has a privilege individually as well as through a group, it will have a PrivilegeState of -1 (Group).
	/// <summary>
	/// 
	/// </summary>
	public enum PrivilegeState : int
	{
		/// <summary>
		/// 
		/// </summary>
		Denied = 0,		//Member does not have this privilege
		/// <summary>
		/// 
		/// </summary>
		Group = -1,		//Member has this privilege through their membership in a group
		/// <summary>
		/// 
		/// </summary>
		Individual = 1	//Member has this privilege assigned to them individually
	}

	/// <summary>
	/// Summary description for Privilege.
	/// </summary>
	public class MemberPrivilege : IValueObject, ICacheable
	{
		#region Private Members

		private int _privilegeID;
		private string _description;
		private PrivilegeState _privilegeState;

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilegeID"></param>
		/// <param name="description"></param>
		/// <param name="privilegeState"></param>
		public MemberPrivilege(int privilegeID, string description, PrivilegeState privilegeState)
		{
			_privilegeID = privilegeID;
			_description = description;
			_privilegeState = privilegeState;
		}

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public int PrivilegeID
		{
			get
			{
				return _privilegeID;
			}
			set
			{
				_privilegeID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public PrivilegeState PrivilegeState
		{
			get
			{
				return _privilegeState;
			}
			set
			{
				_privilegeState = value;
			}
		}


		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add Privilege.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add Privilege.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add Privilege.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add Privilege.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add Privilege.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add Privilege.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
