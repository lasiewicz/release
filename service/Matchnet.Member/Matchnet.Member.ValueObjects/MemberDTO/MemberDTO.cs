﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Common.AccessService;

namespace Matchnet.Member.ValueObjects.MemberDTO
{
    [Serializable]
    public enum MemberType
    {
        [EnumMember]
        Full = 0,
        [EnumMember]
        Search = 1,
        [EnumMember]
        MatchMail = 2
    }

    [Serializable]
    public class MemberDTO : IMemberDTO, IDisposable
    {
        private const int GROUP_PERSONALS = 8383;
        private const string LAST_UPDATED_ATTR = "LastUpdated";
        private const string BRAND_INSERT_DATE_ATTR = "BrandInsertDate";
        private const int NEW_DAY_LIMIT = 14;
        private const string KEY_SEPARATOR = "-";
        private const string MEMBER_DTO_BASE = "MemberDTO";

        private PhotoCollection _photoCollection;
        private List<AccessPrivilege> _AccessPrivilegeList;
        private Dictionary<int, Dictionary<int, TextValue>> _attributesText;
        private Dictionary<int, DateTime> _attributesDate;
        private Dictionary<int, int> _attributesInt;
        [NonSerialized]
        private Attributes _attributeMetaData;
        [NonSerialized]
        private Brands _allBrands;
        [NonSerialized]
        private ISettingsSA _settingsService = null;

        private ArrayList _siteIds;
        private ArrayList _communityIds;
        private ArrayList _brandLastLogonDate_Attribute_Group_IDs;
        private readonly MemberType _memberType;
        private int _memberId;
        private string _emailAddress;
        private List<string> _unsupportedAttributes = null;
        private int _cacheTTLSeconds = 60 * 60;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private CacheItemMode _cacheMode = CacheItemMode.Absolute;

        public PhotoCollection PhotoCollection
        {
            get { return _photoCollection; }
            set { _photoCollection = value; }
        }

        public List<AccessPrivilege> AccessPrivilegeList
        {
            get { return _AccessPrivilegeList; }
            set { _AccessPrivilegeList = value; }
        }

        public int MemberID
        {
            get { return _memberId; }
            set { _memberId = value; }
        }

        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }

        public MemberType MemberType
        {
            get { return _memberType; }
        }

        public ArrayList SiteIDs
        {
            get { return _siteIds; }
            set { _siteIds = value; }
        }

        public ArrayList CommunityIDs
        {
            get { return _communityIds; }
            set { _communityIds = value; }
        }

        public Attributes AttributeMetaData
        {
            get
            {
                if (_attributeMetaData == null)
                {
                    _attributeMetaData = AttributeMetadataSA.Instance.GetAttributes();
                }
                return _attributeMetaData;
            }
            set { _attributeMetaData = value; }
        }

        public ArrayList BrandLastLogonDateAttributeGroupIDs
        {
            get { return _brandLastLogonDate_Attribute_Group_IDs; }
            set { _brandLastLogonDate_Attribute_Group_IDs = value; }
        }

        public Brands AllBrands
        {
            get
            {
                if (null == _allBrands) _allBrands = BrandConfigSA.Instance.GetBrands();
                return _allBrands;
            }
            set { _allBrands = value; }
        }

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }


        public MemberDTO(MemberType memberType)
        {
            _memberType = memberType;
        }

        public void AddUnsupportedAttribute(string attributeName)
        {
            if(null == _unsupportedAttributes) _unsupportedAttributes = new List<string>();
            if(!_unsupportedAttributes.Contains(attributeName)) _unsupportedAttributes.Add(attributeName.ToLower());
        }

        public void AddUnifiedAccessPrivilege(AccessPrivilege privilege)
        {
            if (null == _AccessPrivilegeList) _AccessPrivilegeList = new List<AccessPrivilege>();
            if (!_AccessPrivilegeList.Contains(privilege))
            {
                _AccessPrivilegeList.Add(privilege);
            }
        }

        public AccessPrivilege GetUnifiedAccessPrivilege(PrivilegeType privilegeType, int brandID, int siteID, int communityID)
        {
            AccessPrivilege accessPrivilege = null;
            if (_AccessPrivilegeList != null)
            {
                foreach (AccessPrivilege ap in _AccessPrivilegeList)
                {
                    if (ap.CallingSystemID == siteID && ap.UnifiedPrivilegeType == privilegeType)
                    {
                        accessPrivilege = ap;
                        break;
                    }
                }
            }
            return accessPrivilege;
        }

        /// <summary>
        /// Selects a "default" photo object to use as a thumbnail (or whatever). Looks for a photo marked with IsMain first, ApprovedForMain and not private 2nd,
        /// ApprovedForMain and private 3rd.
        /// </summary>
        /// <returns>The member's Photo or null if there is no photo.</returns>
        public Photo GetDefaultPhoto(Int32 communityID, bool publicOnly)
        {
            Photo retVal = null;

            var photos = GetPhotos(communityID);
            if (photos == null || photos.Count == 0)
                return null;

            //keep track of main photo with lowest list order
            byte curMainListOrder = byte.MaxValue;
            byte curMainIndex = byte.MaxValue;

            //keep track of non-main private photo with lowest list order
            byte curPrivateListOrder = byte.MaxValue;
            byte curPrivateIndex = byte.MaxValue;

            //keep track of non-main public photo with lowest list order
            byte curPublicListOrder = byte.MaxValue;
            byte curPublicIndex = byte.MaxValue;

            for (byte i = 0; i < photos.Count; i++)
            {
                if (photos[i].IsApproved && photos[i].IsApprovedForMain)
                {
                    if (!publicOnly || (publicOnly && !photos[i].IsPrivate))
                    {
                        if (photos[i].IsMain)
                        {
                            //main photo
                            if (photos[i].ListOrder <= 1)
                            {
                                //main photo with list order 1, we're done
                                return photos[i];
                            }

                            if (photos[i].ListOrder < curMainListOrder)
                            {
                                curMainListOrder = photos[i].ListOrder;
                                curMainIndex = i;
                            }
                        }
                        else
                        {
                            if (!photos[i].IsPrivate)
                            {
                                //public photo
                                if (photos[i].ListOrder < curPublicListOrder)
                                {
                                    curPublicListOrder = photos[i].ListOrder;
                                    curPublicIndex = i;
                                }
                            }
                            else
                            {
                                //private photo (note: we won't even get here if user passes in public only)
                                if (photos[i].ListOrder < curPrivateListOrder)
                                {
                                    curPrivateListOrder = photos[i].ListOrder;
                                    curPrivateIndex = i;
                                }
                            }
                        }
                    }
                }
            }

            //main photo
            if (curMainIndex < byte.MaxValue && curMainIndex < photos.Count)
            {
                return photos[curMainIndex];
            }

            //public photo
            if (curPublicIndex < byte.MaxValue && curPublicIndex < photos.Count)
            {
                return photos[curPublicIndex];
            }

            //private photo
            if (curPrivateIndex < byte.MaxValue && curPrivateIndex < photos.Count)
            {
                return photos[curPrivateIndex];
            }

            return retVal;
        }

        /// <summary>
        /// Retrieves one random photo out of all main-eligible photos.
        /// </summary>
        /// <returns>Photo object or null if none found.</returns>
        public Photo GetRandomApprovedForMainPhoto(int communityId, bool publicOnly)
        {
            var photos = GetApprovedForMainPhotos(communityId, publicOnly);
            if (photos == null || photos.Count == 0)
                return null;

            var random = new Random();
            return photos[random.Next(0, photos.Count - 1)];
        }

        /// <summary>
        /// Gets all photos that approved for main.
        /// </summary>
        /// <returns>List of Photo or null if none found.</Photo></returns>
        public List<Photo> GetApprovedForMainPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        /// <summary>
        /// Gets any approved photos; including approved photos not suitable for main.
        /// </summary>
        /// <returns>List of Photo or null if none found.</returns>
        public List<Photo> GetApprovedPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        /// <summary>
        /// Gets any approved photos exluding the main photo.
        /// </summary>
        /// <returns>List of Photo or null if none found.</returns>
        public List<Photo> GetApprovedPhotosExcludingMain(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (!photos[i].IsApproved || photos[i].IsMain) continue;

                if (publicOnly)
                {
                    if (!photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    approvedPhotos.Add(photos[i]);
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public PhotoCommunity GetPhotos(Int32 communityID)
        {
            return PhotoCollection.GetCommunity(communityID);
        }

        public void SetAttributeInt(Int32 attributeGroupID, Int32 integer)
        {
            if (null == _attributesInt) _attributesInt = new Dictionary<int, int>();
            if (_attributesInt.ContainsKey(attributeGroupID))
            {
                _attributesInt[attributeGroupID] = integer;
            }
            else
            {
                _attributesInt.Add(attributeGroupID, integer);
            }
        }

        public void SetAttributeDate(Int32 attributeGroupID, DateTime date)
        {
            if (null == _attributesDate) _attributesDate = new Dictionary<int, DateTime>();
            if (_attributesDate.ContainsKey(attributeGroupID))
            {
                if (date != DateTime.MinValue)
                {
                    _attributesDate[attributeGroupID] = date;
                }
                else
                {
                    _attributesDate.Remove(attributeGroupID);
                }
            }
            else if (date != DateTime.MinValue)
            {
                _attributesDate.Add(attributeGroupID, date);
            }
        }

        public void SetAttributeText(Int32 attributeGroupID, Int32 languageID, string text, TextStatusType textStatus)
        {
            if (null == _attributesText) _attributesText = new Dictionary<int, Dictionary<int, TextValue>>();
            Dictionary<int, TextValue> attributes = null;
            if (_attributesText.ContainsKey(languageID)) attributes = _attributesText[languageID];

            if (attributes == null)
            {
                attributes = new Dictionary<int, TextValue>();
                _attributesText.Add(languageID, attributes);
            }

            TextValue textValue = null;
            if (attributes.ContainsKey(attributeGroupID)) textValue = attributes[attributeGroupID];

            if (textValue != null)
            {
                textValue.Text = text;
                textValue.TextStatus = textStatus;
            }
            else
            {
                attributes.Add(attributeGroupID, new TextValue(text, textStatus));
            }
        }

        public AttributeGroup getAttributeGroup(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute)
        {
            Int32 groupID = 0;

            switch (attribute.Scope)
            {
                case ScopeType.Personals:
                    groupID = GROUP_PERSONALS;
                    break;

                case ScopeType.Community:
                    if (!(communityID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), communityID not set.");
                    }
                    groupID = communityID;
                    break;

                case ScopeType.Site:
                    if (!(siteID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), siteID not set.");
                    }
                    groupID = siteID;
                    break;

                case ScopeType.Brand:
                    if (!(brandID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), brandID not set.");
                    }
                    groupID = brandID;
                    break;
            }

            if (groupID == 0)
            {
                throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ").");
            }

            return AttributeMetaData.GetAttributeGroup(groupID, attribute.ID);
        }

        public string GetAttributeText(Int32 attributeGroupID, Int32 languageID, string defaultValue, out TextStatusType textStatus)
        {
            textStatus = TextStatusType.Auto;
            TextValue textValue = GetAttributeText(attributeGroupID, languageID);

            if ((textValue == null && attributeGroupID == ServiceConstants.ATTRIBUTEGROUPID_EMAILADDRESS))
            {
                TextValue tempTextValue = GetAttributeText(attributeGroupID, Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH);

                if (tempTextValue != null) textValue = tempTextValue;
            }

            if (textValue != null)
            {
                textStatus = textValue.TextStatus;
                return textValue.Text;
            }
            else
            {
                return defaultValue;
            }
        }

        private TextValue GetAttributeText(Int32 attributeGroupID, Int32 languageID)
        {
            TextValue retValue = null;
            Dictionary<int, TextValue> attributes = (null != _attributesText && _attributesText.ContainsKey(languageID)) ? _attributesText[languageID] : null;

            if (attributes != null)
            {
                retValue = (attributes.ContainsKey(attributeGroupID)) ? attributes[attributeGroupID] : null;
            }

            return retValue;
        }

        public Int32 GetAttributeInt(Int32 attributeGroupID, Int32 defaultValue)
        {
            int i = (null != _attributesInt && _attributesInt.ContainsKey(attributeGroupID)) ? _attributesInt[attributeGroupID] : defaultValue;
            return i;
        }

        public DateTime GetAttributeDate(Int32 attributeGroupID, DateTime defaultValue)
        {
            DateTime dt = (null != _attributesDate && _attributesDate.ContainsKey(attributeGroupID)) ? _attributesDate[attributeGroupID] : defaultValue;
            return dt;
        }

        private Int32 getAttributeInt(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
            Int32 defaultValue,
            bool ignoreUnifiedAccessPrivilege)
        {
            if (null != _unsupportedAttributes && _unsupportedAttributes.Contains(attribute.Name.ToLower()))
            {
                throw new Exception("Attribute: " + attribute.Name + " is no longer supported!");
            }

            AttributeGroup attributeGroup = getAttributeGroup(communityID,
                siteID,
                brandID,
                attribute);

            if (attributeGroup == null)
            {
                throw new Exception("AttributeGroup for Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") does not exist. Community:" + communityID.ToString() + ", Site:" + siteID.ToString() + ", Brand:" + brandID.ToString());
            }

            if (attribute.DataType != DataType.Bit && attribute.DataType != DataType.Mask && attribute.DataType != DataType.Number)
            {
                throw new Exception("Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") is not an integer attribute.");
            }

            if (attributeGroup.DefaultValue != Constants.NULL_STRING)
            {
                defaultValue = Convert.ToInt32(attributeGroup.DefaultValue);
            }

            int returnValue = defaultValue;

            //Determine if we should use Unified Access
            if (!ignoreUnifiedAccessPrivilege && (attribute.Name.ToLower() == "coloranalysis"))
            {
                AccessPrivilege accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.ColorAnalysis, brandID, siteID, communityID);

                if (accessPrivilege != null)
                {
                    DateTime date = accessPrivilege.EndDatePST;
                    if (date == DateTime.MinValue || date > DateTime.Now)
                        returnValue = 1;
                    else
                        returnValue = 0;
                }
            }
            else
            {
                returnValue = GetAttributeInt(attributeGroup.ID, defaultValue);
            }

            return returnValue;
        }


        private DateTime getAttributeDate(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
            DateTime defaultValue, bool ignoreUnifiedAccessPrivilege)
        {
            if (null != _unsupportedAttributes && _unsupportedAttributes.Contains(attribute.Name.ToLower()))
            {
                throw new Exception("Attribute: " + attribute.Name + " is no longer supported!");
            }

            AttributeGroup attributeGroup = getAttributeGroup(communityID,
                siteID,
                brandID,
                attribute);

            if (attributeGroup == null)
            {
                throw new Exception("AttributeGroup for Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") does not exist. Community:" + communityID.ToString() + ", Site:" + siteID.ToString() + ", Brand:" + brandID.ToString());
            }

            if (attribute.DataType != DataType.Date)
            {
                throw new Exception("Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") is not a date attribute.");
            }

            if (attributeGroup.DefaultValue != Constants.NULL_STRING)
            {
                defaultValue = Convert.ToDateTime(attributeGroup.DefaultValue);
            }
            else
            {
                defaultValue = DateTime.MinValue;
            }


            DateTime date = defaultValue;

            //Determine if we should use Unified Access
            if (!ignoreUnifiedAccessPrivilege && (attribute.Name.ToLower() == "subscriptionexpirationdate"
                    || attribute.Name.ToLower() == "highlightedexpirationdate"
                    || attribute.Name.ToLower() == "spotlightexpirationdate"
                    || attribute.Name.ToLower() == "jmeterexpirationdate"))
            {
                AccessPrivilege accessPrivilege = null;
                switch (attribute.Name.ToLower())
                {
                    case "subscriptionexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, brandID, siteID, communityID);
                        break;
                    case "highlightedexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, brandID, siteID, communityID);
                        break;
                    case "spotlightexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, brandID, siteID, communityID);
                        break;
                    case "jmeterexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.JMeter, brandID, siteID, communityID);
                        break;
                }

                if (accessPrivilege != null)
                    date = accessPrivilege.EndDatePST;
            }
            else
            {
                date = GetAttributeDate(attributeGroup.ID, defaultValue);
            }

            return date;
        }

        private string getAttributeText(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            Int32 languageID,
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
            string defaultValue,
            out TextStatusType textStatus)
        {
            if (null != _unsupportedAttributes && _unsupportedAttributes.Contains(attribute.Name.ToLower()))
            {
                throw new Exception("Attribute: " + attribute.Name + " is no longer supported!");
            }

            AttributeGroup attributeGroup = getAttributeGroup(communityID, siteID, brandID, attribute);

            if (attribute.DataType != DataType.Text)
            {
                throw new Exception("Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") is not a text attribute.");
            }

            if (attributeGroup.DefaultValue != Constants.NULL_STRING)
            {
                defaultValue = attributeGroup.DefaultValue;
            }

            return GetAttributeText(attributeGroup.ID, languageID, defaultValue, out textStatus);
        }


        public DateTime GetAttributeDate(Content.ValueObjects.BrandConfig.Brand brand, string attributeName)
        {
            return getAttributeDate(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                AttributeMetaData.GetAttribute(attributeName),
                DateTime.MinValue, false);
        }

        public DateTime GetAttributeDate(Content.ValueObjects.BrandConfig.Brand brand, string attributeName, DateTime defaultValue)
        {
            return getAttributeDate(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue, false);
        }

        public DateTime GetAttributeDate(Int32 communityID, Int32 siteID, Int32 brandID, string attributeName, DateTime defaultValue)
        {
            return getAttributeDate(communityID,
                siteID,
                brandID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue, false);
        }

        public int GetAttributeInt(Content.ValueObjects.BrandConfig.Brand brand, string attributeName)
        {
            return getAttributeInt(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                AttributeMetaData.GetAttribute(attributeName),
                Constants.NULL_INT, false);
        }

        public int GetAttributeInt(Content.ValueObjects.BrandConfig.Brand brand, string attributeName, int defaultValue)
        {
            return getAttributeInt(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue, false);
        }

        public Int32 GetAttributeInt(Int32 communityID, Int32 siteID, Int32 brandID, string attributeName, Int32 defaultValue)
        {
            return getAttributeInt(communityID,
                siteID,
                brandID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue, false);
        }

        public bool GetAttributeBool(Content.ValueObjects.BrandConfig.Brand brand, string attributeName)
        {
            return getAttributeInt(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                AttributeMetaData.GetAttribute(attributeName),
                Constants.NULL_INT, false) == 1;
        }

        public bool GetAttributeBool(Int32 communityID, Int32 siteID, Int32 brandID, string attributeName)
        {
            return getAttributeInt(communityID,
                siteID,
                brandID,
                AttributeMetaData.GetAttribute(attributeName),
                Constants.NULL_INT, false) == 1;
        }

        public string GetAttributeText(Brand brand, string attributeName, string defaultValue)
        {
            TextStatusType textStatus;

            return getAttributeText(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                brand.Site.LanguageID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue,
                out textStatus);
        }

        public string GetAttributeText(Brand brand, string attributeName, out TextStatusType textStatus)
        {
            return getAttributeText(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                brand.Site.LanguageID,
                AttributeMetaData.GetAttribute(attributeName),
                "",
                out textStatus);
        }

        public string GetAttributeText(Content.ValueObjects.BrandConfig.Brand brand, string attributeName)
        {
            TextStatusType textStatus;

            return getAttributeText(brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                brand.BrandID,
                brand.Site.LanguageID,
                AttributeMetaData.GetAttribute(attributeName),
                "",
                out textStatus);
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName)
        {
            TextStatusType textStatus;

            return getAttributeText(communityID,
                siteID,
                brandID,
                languageID,
                AttributeMetaData.GetAttribute(attributeName),
                Constants.NULL_STRING,
                out textStatus);
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, int attributeID, string defaultValue)
        {
            TextStatusType textStatus;

            return getAttributeText(communityID,
                siteID,
                brandID,
                languageID,
                AttributeMetaData.GetAttribute(attributeID),
                defaultValue,
                out textStatus);
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName, string defaultValue)
        {
            TextStatusType textStatus;

            return getAttributeText(communityID,
                siteID,
                brandID,
                languageID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue,
                out textStatus);
        }

        public string GetAttributeText(Int32 communityID, Int32 siteID, Int32 brandID, Int32 languageID, string attributeName, string defaultValue, out TextStatusType textStatus)
        {
            return getAttributeText(communityID,
                siteID,
                brandID,
                languageID,
                AttributeMetaData.GetAttribute(attributeName),
                defaultValue,
                out textStatus);
        }

        public string GetAttributeTextApproved(Content.ValueObjects.BrandConfig.Brand brand, string attributeName, string unapprovedResource)
        {
            TextStatusType textStatus;
            string val = GetAttributeText(brand, attributeName, out textStatus);

            if (textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
            {
                return unapprovedResource;
            }
            else
            {
                return val;
            }
        }

        public string GetAttributeTextApproved(Content.ValueObjects.BrandConfig.Brand brand, string attributeName, int viewerMemberID, string unapprovedResourceValue)
        {
            return GetAttributeTextApproved(brand,
                        new string[] { attributeName },
                        viewerMemberID,
                        unapprovedResourceValue);
        }

        public string GetAttributeTextApproved(Content.ValueObjects.BrandConfig.Brand brand, int languageID, string attributeName, string defaultValue, string unapprovedResource)
        {
            TextStatusType textStatus;

            string val = GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, languageID, attributeName, defaultValue, out textStatus);

            if (textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
            {
                return unapprovedResource;
            }
            else
            {
                return val;
            }
        }

        public string GetAttributeTextApproved(Brand brand, string[] attributeNames, Int32 viewerMemberID, string unapprovedResourceValue)
        {
            bool foundUnapproved = false;
            string val = Constants.NULL_STRING;

            for (Int32 num = 0; num < attributeNames.Length; num++)
            {
                string attributeName = attributeNames[num];
                TextStatusType textStatus;
                val = GetAttributeText(brand, attributeName, out textStatus);
                if (viewerMemberID != MemberID && textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
                {
                    foundUnapproved = true;
                    val = Constants.NULL_STRING;
                }
                else
                {
                    break;
                }
            }

            if (val == Constants.NULL_STRING && foundUnapproved)
            {
                val = unapprovedResourceValue;
            }

            return val;
        }

        public string GetUserName(Content.ValueObjects.BrandConfig.Brand brand)
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetaData.GetAttribute(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME);
            string username = "";

            if (SettingsService.GetSettingFromSingleton("USERNAME_APPROVAL_FLAG", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID).ToLower() == "true")
            {
                // To do - This should be te proper call, but it's causing issues like telling the username is not available after updating.
                username = this.GetAttributeTextApproved(brand, brand.Site.LanguageID, "UserName", username, this.MemberID.ToString());
            }
            else
            {
                username = this.GetAttributeText(brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    brand.BrandID,
                    brand.Site.LanguageID,
                    "UserName");
            }
            if (username == "")
            {
                if (SettingsService.GetSettingFromSingleton("USERNAME_APPROVAL_FLAG", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID).ToLower() == "true")
                {
                    // To do - This should be te proper call, but it's causing issues like telling the username is not available after updating.
                    username = this.GetAttributeTextApproved(brand, Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH, "UserName", username, this.MemberID.ToString());
                }
                else
                {
                    username = this.GetAttributeText(brand.Site.Community.CommunityID,
                        brand.Site.SiteID,
                        brand.BrandID,
                        Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH,
                        "UserName");
                }
            }

            if (username == "")
            {
                //if username is STILL blank, it's most likely because the username is saved under a different language
                //than the language of the brand being passed in (ie, French username from a member that turned up in a
                //search of paris from Jdate.com instead of Jdate.fr). In this case return the memberid. 
                username = this.MemberID.ToString();
            }

            return username;
        }

        public bool IsPayingMember(int siteID)
        {
            Site[] sites = AllBrands.GetSites();
            Int32 communityID = AllBrands.GetSite(siteID).Community.CommunityID;

            for (Int32 siteNum = 0; siteNum < sites.Length; siteNum++)
            {
                Site site = sites[siteNum];
                if (site.Community.CommunityID == communityID)
                {
                    //we give a 15min grace period to allow for renewals to be processed
                    AccessPrivilege ap = GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, Constants.NULL_INT, site.SiteID, Constants.NULL_INT);
                    if (ap != null)
                    {
                        if (ap.EndDatePST.AddMinutes(15) > DateTime.Now)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool HasApprovedPhoto(int communityID)
        {
            PhotoCommunity photos = PhotoCollection.GetCommunity(communityID);

            for (byte photoNum = 0; null != photos && photoNum < photos.Count; photoNum++)
            {
                if (photos[photoNum].IsApproved && photos[photoNum].IsApprovedForMain)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsUpdatedMember(int communityID)
        {
            DateTime updateDate = this.GetAttributeDate(communityID, Constants.NULL_INT, Constants.NULL_INT, LAST_UPDATED_ATTR, DateTime.MaxValue);

            if (updateDate != DateTime.MaxValue)
            {
                if ((DateTime.Now.Subtract(updateDate).TotalDays <= NEW_DAY_LIMIT))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Although brand is passed in, the community of the brand is used in this function.  Looks
        /// through the BrandInsertDate of all the brands that the member belongs to within a community,
        /// then determines if this user is new to the community.
        /// </summary>
        /// <param name="communityID">Brands under this CommunityID are only considered.</param>
        /// <returns></returns>
        public bool IsNewMember(Int32 communityID)
        {
            return IsNewMember(communityID, NEW_DAY_LIMIT);
        }

        /// <summary>
        /// Although brand is passed in, the community of the brand is used in this function.  Looks
        /// through the BrandInsertDate of all the brands that the member belongs to within a community,
        /// then determines if this user is registered with the community within given days.
        /// </summary>
        /// <param name="communityID">Brands under this CommunityID are only considered.</param>
        /// <param name="days">Number of days pas registration</param>
        /// <returns></returns>
        public bool IsNewMember(Int32 communityID, int days)
        {
            DateTime insertDate = DateTime.MinValue;
            DateTime tempInsertDate = DateTime.MinValue;

            // Cycle through all the aBrands that the member is a part of, then
            // pick off the lowest value of BrandInsertDate.  This will be the community insert date.
            foreach (Brand aBrand in AllBrands)
            {
                // We only care about brands that are in the community of the brand that was passed in
                if (aBrand.Site.Community.CommunityID == communityID)
                {
                    // only care about the sites that the member is a part of
                    if (null != SiteIDs && SiteIDs.Contains(aBrand.Site.SiteID))
                    {
                        tempInsertDate = GetAttributeDate(aBrand, BRAND_INSERT_DATE_ATTR, DateTime.MinValue);

                        // only care if a meaningful date came back
                        if (tempInsertDate != DateTime.MinValue)
                        {
                            // updateDate hasn't been set yet? then just set it without the less than compare
                            if (insertDate == DateTime.MinValue)
                            {
                                insertDate = tempInsertDate;
                            }
                            else
                            {
                                if (tempInsertDate < insertDate)
                                    insertDate = tempInsertDate;
                            }
                        }
                    }
                }
            }

            // Was community insert date ever found?
            if (insertDate != DateTime.MinValue)
            {
                if (DateTime.Now.Subtract(insertDate).TotalDays <= days)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public DateTime GetLastLogonDate(Int32 communityID)
        {
            Int32 brandID;
            return GetLastLogonDate(communityID, out brandID);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        public DateTime GetLastLogonDate(Int32 communityID, out Int32 brandID)
        {
            DateTime lastLogonDate = DateTime.MinValue;
            try
            {
                brandID = Constants.NULL_INT;

                for (Int32 num = 0; null != BrandLastLogonDateAttributeGroupIDs && num < BrandLastLogonDateAttributeGroupIDs.Count; num++)
                {
                    Int32 attributeGroupID = (Int32)BrandLastLogonDateAttributeGroupIDs[num];
                    Brand brand = (null != AllBrands && null != AttributeMetaData) ? AllBrands.GetBrand(AttributeMetaData.GetAttributeGroup(attributeGroupID).GroupID) : null;
                    if (null != brand && brand.Site.Community.CommunityID == communityID)
                    {
                        DateTime currentDtm = (null != _attributesDate && _attributesDate.ContainsKey(attributeGroupID)) ? _attributesDate[attributeGroupID] : DateTime.MinValue;
                        if (currentDtm > lastLogonDate)
                        {
                            lastLogonDate = currentDtm;
                            brandID = brand.BrandID;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Last logon date cannot be found for member:{0} and community:{1}",MemberID,communityID));
            }
            return lastLogonDate;
        }

        public bool IsSiteMember(int siteID)
        {
            return (null != SiteIDs && SiteIDs.Contains(siteID));
        }

        public int[] GetSiteIDList()
        {
            int[] siteIdList = null;
            if (null != SiteIDs)
            {
                siteIdList = (int[]) SiteIDs.ToArray(typeof (Int32));
            }
            return siteIdList;
        }

        public bool IsCommunityMember(int communityID)
        {
            return (null != CommunityIDs && CommunityIDs.Contains(communityID));
        }

        public int[] GetCommunityIDList()
        {
            int[] communityIdList = null;
            if (null != CommunityIDs)
            {
                communityIdList = (int[]) CommunityIDs.ToArray(typeof (Int32));
            }
            return communityIdList;
        }


        #region ICacheable Members
        public CacheItemMode CacheMode
        {
            get { return _cacheMode; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKey(MemberID, MemberType);
        }

        public static string GetCacheKey(int memberId, MemberType memberType)
        {
            return MEMBER_DTO_BASE + memberId + KEY_SEPARATOR + memberType.ToString();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (null != this._AccessPrivilegeList)
            {
                this._AccessPrivilegeList.Clear();
                this._AccessPrivilegeList = null;
            }

            if (null != this._attributesDate)
            {
                this._attributesDate.Clear();
                this._attributesDate = null;

            }

            if (null != this._attributesInt)
            {
                this._attributesInt.Clear();
                this._attributesInt = null;
            }

            if (null != this._attributesText)
            {
                this._attributesText.Clear();
                this._attributesText = null;
            }

            if (null != this._brandLastLogonDate_Attribute_Group_IDs)
            {
                this._brandLastLogonDate_Attribute_Group_IDs.Clear();
                this._brandLastLogonDate_Attribute_Group_IDs = null;
            }

            if (null != this._communityIds)
            {
                this._communityIds.Clear();
                this._communityIds = null;
            }

            if (null != this._siteIds)
            {
                this._siteIds.Clear();
                this._siteIds = null;
            }

            if (null != this._unsupportedAttributes)
            {
                this._unsupportedAttributes.Clear();
                this._unsupportedAttributes = null;
            }

            if (null != this._photoCollection && null != this._photoCollection.Communities)
            {
                this._photoCollection.Communities.Clear();
            }

            this._photoCollection = null;
            this._allBrands = null;
            this._attributeMetaData = null;
            this._settingsService = null;

            // no need to finalize later
            System.GC.SuppressFinalize(this); 
        }
        #endregion
    }
}
