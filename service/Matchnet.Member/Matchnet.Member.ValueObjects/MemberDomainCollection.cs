using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Summary description for MemberDomainCollection.
	/// </summary>
	[Serializable]
	public class MemberDomainCollection : System.Collections.CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		public MemberDomainCollection()
		{
			
		}

		/// <summary>
		/// Indexer overload
		/// </summary>
		public MemberDomain this[int index]
		{
			get { return ((MemberDomain)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="memberDomain">banner to add to collection</param>
		/// <returns></returns>
		public int Add(MemberDomain memberDomain)
		{
			return base.InnerList.Add(memberDomain);
		}
	}
}
