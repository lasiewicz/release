using System;

using Matchnet;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Defines all possible outcomes of a Register attempt.
	/// </summary>
	[Serializable]
	public enum RegisterStatusType : int
	{
		/// <summary>
		/// Indicates that the user was successfully registered.
		/// </summary>
		Success = 0,
        InvalidAttributes =1,
        FailedFraud=2,
		
		/// <summary>
		/// Indicates that the user's password exceeded the maximum length.
		/// </summary>
		PasswordLengthExceeded = 401005,
		
		/// <summary>
		/// Indicates that the user's e-mail address is not allowed because the domain has been blocked.
		/// </summary>
		EmailAddressNotAllowed = 519238,
		
		/// <summary>
		/// Indicates that the user is already registered in the system.
		/// </summary>
		AlreadyRegistered = 401003,
		
		/// <summary>
		/// Indicates that the username is already in use.
		/// </summary>
		DuplicateUsername = 401002,
		
		/// <summary>
		/// Indicates that the user's e-mail address has been blocked.
		/// </summary>
		EmailAddressBlocked = 519239,
		
		/// <summary>
		/// Indicates that the user's e-mail address is invalid.
		/// </summary>
		InvalidEmail = 519385,

        /// <summary>
        /// Indicates that the user's IP address is blocked
        /// </summary>
        IPBlocked = 519390 
	}

    /// <summary>
    /// Defines type of action performed on LogonMember
    /// </summary>
    [Serializable]
    public enum LogonActionType
    {
        NewUser = 1,
        EmailAddressUpdate = 2,
        UsernameUpdate = 3,
        PasswordUpdate = 4, 
        DisabledFlagUpdate=5
    }


	/// <summary>
	/// Represents the result of a member Register action.
	/// </summary>
	[Serializable]
	public sealed class MemberRegisterResult : IValueObject
	{
		private RegisterStatusType _RegisterStatus = RegisterStatusType.Success;
		private int _memberID = Constants.NULL_INT;
		private string _userName = Constants.NULL_STRING;

        public string ErrorMessage { get; set; }


		private MemberRegisterResult()
		{
			// Hide the default constructor.
		}


		/// <summary>
		/// Initializes the Matchnet.Member.ValueObjects.RegisterResult object.
		/// </summary>
		/// <param name="status">The status returned from the Register action.</param>
		/// <param name="memberID">The member id.</param>
		/// <param name="userName">The user name.</param>
		public MemberRegisterResult(RegisterStatusType status, int memberID, string userName)
		{
			_RegisterStatus = status;
			_memberID = memberID;
			_userName = userName;
		}

		/// <summary>
		/// Gets the status of the member Register action.
		/// </summary>
		public RegisterStatusType RegisterStatus
		{
			get{return _RegisterStatus;}
            set { _RegisterStatus = value; }
		}


		/// <summary>
		/// Gets the MemberID produced through the Register action.
		/// </summary>
		public int MemberID
		{
			get{return _memberID;}
		}

		/// <summary>
		/// Gets the user name used in the Register action.
		/// </summary>
		public string Username
		{
			get{return _userName;}
		}
	}
}
