﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Matchnet.CacheSynchronization.Tracking;


namespace Matchnet.Member.ValueObjects
{
    [Serializable]
    public class CachedMemberEmail : IValueObject, ICacheable, IReplicable, ISerializable
    {

        private const string CACHE_KEY_PREFIX = "CachedMemberEmail";
        private int _version = 0;
        private int _memberID;
        private int _groupID;
        private int _cacheTTLSeconds = 60 * 60;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;

        private List<LastEmail> _lastEmails = new List<LastEmail>();

        private bool _replicateForExpire = false;

        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        #region Properties
        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public bool ReplicateForExpire
        {
            get { return _replicateForExpire; }
            set { _replicateForExpire = value; }
        }


        public List<LastEmail> LastEmails
        {
            get { return _lastEmails; }
        }


        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
            set
            {
                _referenceTracker = value;
            }
        }

        public int MemberID
        {
            get { return _memberID; }
        }
        #endregion

        #region Contructors
        public CachedMemberEmail(int memberID)
        {
            _memberID = memberID;
        }

        /// <summary>
        /// Deserialization c'tor
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected CachedMemberEmail(SerializationInfo info, StreamingContext context)
        {
            _version = info.GetInt32("version");
            _memberID = info.GetInt32("memberid");
            _groupID = info.GetInt32("groupid");
            _cacheTTLSeconds = info.GetInt32("cachettlseconds");
            _cachePriority = (CacheItemPriorityLevel)info.GetValue("cachepriority", _cachePriority.GetType());
            _cacheMode = (CacheItemMode)info.GetValue("cachemode", _cacheMode.GetType());
            _lastEmails = (List<LastEmail>)info.GetValue("lastEmails", _lastEmails.GetType());
            _replicateForExpire = info.GetBoolean("replicationforexpire");

        }
        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
            set
            {
                _cacheMode = value;
            }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        public string GetCacheKey()
        {
            string key = string.Empty;

            key = CachedMemberEmail.GetCacheKey(_memberID);

            return key;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public static string GetCacheKey(int memberID)
        {
            return CACHE_KEY_PREFIX + memberID.ToString();
        }
        #endregion

        #region Implementation of ISerializable
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("version", _version, _version.GetType());
            info.AddValue("memberid", _memberID, _memberID.GetType());
            info.AddValue("groupid", _groupID, _groupID.GetType());
            info.AddValue("cachettlseconds", _cacheTTLSeconds, _cacheTTLSeconds.GetType());
            info.AddValue("cachepriority", _cachePriority, _cachePriority.GetType());
            info.AddValue("cachemode", _cacheMode, _cacheMode.GetType());
            info.AddValue("lastEmails", _lastEmails, _lastEmails.GetType());
            info.AddValue("replicationforexpire", _replicateForExpire, _replicateForExpire.GetType());
        }
        #endregion

    }

    [Serializable]
    public class LastEmail
    {
        public string LastEmailAddress { get; set; }
        public int AdminMemberId { get; set; }
        public DateTime LastEmailDate { get; set; }
    }
}
