using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable, Flags]
	public enum TextStatusType : int
	{
		/// <summary>DOCUMENTATION NEEDED:  What is the difference between the enums 'None' and 'Pending'.</summary>
		None = 0,
		/// <summary>DOCUMENTATION NEEDED:  What is the difference between the enums 'None' and 'Pending'.</summary>
		Pending = 1,
		/// <summary>Indicates a free text field approved by an automated method, be it a Bayesian application or another method.</summary>
		Auto = 2,
		/// <summary>Indicates a free text field approved by a member of Spark Networks' Customer Service staff.</summary>
		Human = 4
	};

	
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class TextValue
	{
		private string _text;
		private TextStatusType _textStatus;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="textStatus"></param>
		public TextValue(string text, TextStatusType textStatus)
		{
			_text = text;
			_textStatus = textStatus;
		}


		/// <summary>
		/// 
		/// </summary>
		public string Text
		{
			get
			{
				return _text;
			}
			set
			{
				_text = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public TextStatusType TextStatus
		{
			get
			{
				return _textStatus;
			}
			set
			{
				_textStatus = value;
			}
		}
	}
}
