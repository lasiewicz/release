using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;


namespace Matchnet.Member.ValueObjects
{
    /// <summary>
    /// Indicates the type of update being sent to e1.  Only SearchUpdateType.Update neccessitates a call back to the member service.
    /// </summary>
    [Serializable]
	public enum SearchUpdateType : byte
	{
        /// <summary>General update, e1 should retrieve all member data from the member service and reload the member.</summary>
		Update = 1,
        /// <summary>Update of EmailCount attribute only.  Occurs when a member receives an e-mail.  The new value is sent over in the SearchUpdate object.</summary>
		UpdateEmailCount = 2,
        /// <summary>Update of LastActiveDate attribute only.  Occurs when a member logs in.</summary>
		UpdateLastActiveDate = 3,
        /// <summary>Update the member's MOL status.  This update is sent from the MOL service.</summary>
		UpdateIsOnline = 4,
        /// <summary>Update whether the member has a photo.</summary>
        UpdateHasPhoto = 5,
        /// <summary>Remove the member from search.  Occurs when member is self- or admin-suspended.</summary>
		Remove = 99
	};


	[Serializable]
	public class SearchUpdate : IValueObject, ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;

		private Int32 _memberID;
		private Int32 _communityID;
		private SearchUpdateType _updateType;
		private Int32 _emailCount;
		private DateTime _lastActiveDate;
		private bool _hasPhoto;
		private bool _isOnline;


		public SearchUpdate(Int32 memberID,
			Int32 communityID,
			SearchUpdateType updateType,
			Int32 emailCount,
			DateTime lastActiveDate,
			bool hasPhoto)
		{
			_memberID = memberID;
			_communityID = communityID;
			_updateType = updateType;
			_emailCount = emailCount;
			_lastActiveDate = lastActiveDate;
			_hasPhoto = hasPhoto;
		}

		public SearchUpdate(Int32 memberID,
			Int32 communityID,
			bool isOnline)
		{
			_memberID = memberID;
			_communityID = communityID;
			_isOnline = isOnline;
			_updateType = SearchUpdateType.UpdateIsOnline;
		}


		protected SearchUpdate(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray",ToByteArray());
		}


		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_memberID = br.ReadInt32();
					_communityID = br.ReadInt32();
					_updateType = (SearchUpdateType)br.ReadByte();
					_emailCount = br.ReadInt32();
					_lastActiveDate = new DateTime(br.ReadInt64());
					_hasPhoto = br.ReadBoolean();
					_isOnline = br.ReadBoolean();
					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}


		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(24);	//24 = # of bytes in all private members + 1 byte version #
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(_memberID);
			bw.Write(_communityID);
			bw.Write((byte)_updateType);
			bw.Write(_emailCount);
			bw.Write(_lastActiveDate.Ticks);
			bw.Write(_hasPhoto);
			bw.Write(_isOnline);

			return ms.ToArray();
		}


		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public SearchUpdateType UpdateType
		{
			get
			{
				return _updateType;
			}
			set
			{
				_updateType = value;
			}
		}

		public Int32 EmailCount
		{
			get
			{
				return _emailCount;
			}
		}

		public DateTime LastActiveDate
		{
			get
			{
				return _lastActiveDate;
			}
		}
		
		public bool HasPhoto
		{
			get
			{
				return _hasPhoto;
			}
		}

		public bool IsOnline
		{
			get
			{
				return _isOnline;
			}
		}
	}
}
