﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.MemberAppDevices;

namespace Matchnet.Member.ServiceDefinitions
{
    public interface IMemberAppDeviceService
    {
        //Push notifications
        MemberAppDeviceCollection GetMemberAppDevices(Int32 memberID);
        void UpdateMemberAppDeviceNotificationsFlags(Int32 memberID, Int32 appID, string deviceID, Int64 pushNotificationsFlags);
        void DeleteMemberAppDevice(Int32 memberID, Int32 appID, string deviceID);
        void AddMemberAppDevice(Int32 memberID, Int32 appID, string deviceID, int brandID, string deviceData, Int64 pushNotificationsFlags);
    }
}
