using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.CacheSynchronization.ValueObjects;


namespace Matchnet.Member.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IMemberService
	{
	    void ExpireImpersonationToken(int adminMemberId);
	    string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId);

	    ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID);

	    int ValidateResetPasswordToken(string tokenGuidString);

	    string ResetPassword(int memberId);

        void SendMessmoUserTransactionStatus(int memberID, int siteID, DateTime newSubExpDate);

		void SubscribeToMessmoDailyFeed(int memberID, int communityID);

		void UnsubscribeFromMessmoDailyFeed(int memberID, int communityID);
		
		int GetMemberIDByMessmoID(string messmoID, int communityID);

        void ToggleLogonDisabledFlag(int memberID, int activeFlag);

		void SaveLogonMessmo(string messmoID, int memberID, int communityID);

		void ExpireCachedMember(int memberID);
        void ExpireCachedMemberAccess(int memberID);
        bool ExpireCachedMemberAccess(int memberID, bool replicate, bool synchronize, bool expireAccessSvc);
        void ExpireEntireMemberCache();

		Int32 GetMemberIDByEmail(string emailAddress);
        Int32 GetMemberIDByEmail(string emailAddress, int communityId);

		CachedMember GetCachedMember(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad);


		byte[] GetCachedMemberBytes(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad);


		CachedMember[] GetCachedMembers(string clientHostName,
			CacheReference cacheReference,
			Int32[] memberIDs,
			bool forceLoad);
		
		//versioning
		CachedMember GetCachedMember(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad, int version);


		byte[] GetCachedMemberBytes(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad, int version);


		CachedMember[] GetCachedMembers(string clientHostName,
			CacheReference cacheReference,
			Int32[] memberIDs,
			bool forceLoad, int version);

        IMemberDTO GetMemberDTO(string clientHostName, CacheReference cacheReference, Int32 memberID, MemberType type, bool forceLoad);
        IMemberDTO[] GetMemberDTOs(string clientHostName, CacheReference cacheReference, Int32[] memberIDs, MemberType type, bool forceLoad);

        CachedMemberAccess GetCachedMemberAccess(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad);

        bool AdjustUnifiedAccessCountPrivilege(int memberID, int adminID, string adminDomainUserName, int[] privilegeTypeID, int brandID, int siteID, int communityID, int count);

        CachedMemberLogon GetCachedMemberLogon(string clientHostName,
           CacheReference cacheReference, int memberID, int groupID, bool forceLoad);

	    CachedMemberLogon GetCachedMemberLogon(string clientHostName,
	                                           CacheReference cacheReference, int memberID, int groupID, bool forceLoad,
	                                           int cachedMemberLogonVersion);

        CachedMemberEmail GetCachedMemberEmail(string clientHostName, CacheReference cacheReference, int memberID, bool forceLoad,int cachedMemberEmailVersion);

        void SaveMemberLastLogon(string clientHostname, int memberID, int groupID, DateTime lastLogon);

        void SaveMemberLastLogon(string clientHostname, int memberID, int groupID, DateTime lastLogon, int adminMemberID);

        MemberSaveResult SaveNewMember(string clientHostName,
            MemberUpdate memberUpdate, int version, int groupID);

        CreateLogonCredentialsResult CreateLogonCredentials(string emailAddress, string username, string passwordHash, Brand brand);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="memberUpdate"></param>
		/// <returns></returns>
		MemberSaveResult SaveMember(string clientHostName,
			MemberUpdate memberUpdate);

		/// PM-218
		/// <param name="clientHostName"></param>
		/// <param name="memberUpdate"></param>
		/// <returns></returns>
		MemberSaveResult SaveMember(string clientHostName,
			MemberUpdate memberUpdate, int version, int communityID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="memberUpdate"></param>
		/// <param name="version"></param>
		/// <returns></returns>
		MemberSaveResult SaveMember(string clientHostName,
			MemberUpdate memberUpdate, int version);

	    MemberSaveResult SaveMember(string clientHostName,
	                                       MemberUpdate memberUpdate, int version, Brand brand, bool updateMemberGroup);


		/// <summary>
		/// PM-218 Remove after it's rolled out.
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
        [Obsolete]
		MemberRegisterResult Register(string emailAddress, string username, string password);

		/// <summary>
        /// PM-218 Remove after it's rolled out.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <returns></returns>
        [Obsolete]
        MemberRegisterResult Register(string emailAddress, string username, string password, string passwordHash);

        /// <summary>
		/// 
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="communityID">PM-218</param>
		/// <returns></returns>
        [Obsolete]
		MemberRegisterResult Register(string emailAddress, string username, string password, int communityID);


		/// <summary>
		/// 
		/// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <param name="communityID">PM-218</param>
        /// <returns></returns>
        [Obsolete]
        MemberRegisterResult Register(string emailAddress, string username, string password, string passwordHash, int communityID);

		/// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <param name="brand"></param>
        /// <returns></returns>
        [Obsolete]
        MemberRegisterResult Register(string emailAddress, string username, string password, string passwordHash, Brand brand);

        /// <summary>
        /// New method to register that takes hashed passwords       
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        MemberRegisterResult Register(Brand brand, string emailAddress, string username, string passwordHash);
        
        /// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		string GetPassword(Int32 memberID);
			
		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <returns></returns>
		string GetPassword(string emailAddress);

		/// <summary>
		/// PM-218 Remove this once rolled out.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		int GetMemberID(string username);
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="username"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		int GetMemberID(string username, int communityID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailAddress"></param>
		/// <param name="password"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
        [Obsolete]
		int Auth(string emailAddress, string password, out Int32 memberID);

		/// <summary>
		/// 
		/// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <param name="memberID"></param>
        /// <returns></returns>
        [Obsolete]
        int Auth(string emailAddress, string password, string passwordHash, out Int32 memberID);

        /// <summary>
        /// New method to authenticate that takes hashed passwords       
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="emailAddress"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        int Auth(out Int32 memberID, string emailAddress, string passwordHash);

        /// <summary>
        /// New method to authenticate for all bh, mingle, and non-dating site users
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityId"></param>
        /// <param name="emailAddress"></param>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
	    int Auth(out Int32 memberID, int communityId, string emailAddress, string passwordHash);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		void SetMemberGroup(Int32 memberID,
			Int32 communityID,
			Int32 siteID,
			Int32 brandID);
			
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="photoUpdates"></param>
		/// <returns></returns>
		PhotoCollection SavePhotos(string clientHostName,
			Int32 communityID,
			Int32 memberID,
			PhotoUpdate[] photoUpdates);
		/// <param name="clientHostName"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="photoUpdates"></param>
		/// <returns></returns>
		PhotoCollection SavePhotos(string clientHostName,
			Int32 communityID,
			Int32 memberID,
			PhotoUpdate[] photoUpdates, int version);
        /// <param name="clientHostName"></param>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberID"></param>
        /// <param name="photoUpdates"></param>
        /// <returns></returns>
        PhotoCollection SavePhotos(string clientHostName,
            Int32 brandID,
            Int32 siteID,
            Int32 communityID,
            Int32 memberID,
            PhotoUpdate[] photoUpdates, int version);

		/// <param name="update"></param>
		/// <param name="memberid"></param>
		/// <param name="communityid"></param>
		/// <returns></returns>
		 void InsertPhotoSearch(PhotoUpdate update, int memberid,int communityid);

         void UpdateSearch(int memberID, int communityID);


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privilegeID"></param>
		void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privilegeID"></param>
		void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		SecurityGroupMemberCollection GetSecurityGroupMembers(Int32 groupID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		void SaveSecurityGroupMember(Int32 memberID, Int32 groupID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		void DeleteSecurityGroupMember(Int32 memberID, Int32 groupID);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		SecurityGroupCollection GetSecurityGroups();
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		PrivilegeCollection GetPrivileges();

		/// <summary>
		/// Gets all the members who have this particular privilege
		/// </summary>
		/// <param name="privilegeID">The privilege required</param>
		/// <returns>An array list of members</returns>
		ArrayList GetMembersByPrivilege(Int32 privilegeID);

		/// <summary>
		/// This gets all the groups that have this privilege
		/// </summary>
		/// <param name="privilegeID">The Id for this privilege</param>
		/// <returns>A SecurityGroupCollection that have these privileges</returns>
		SecurityGroupCollection GetGroupsByPrivilege(Int32 privilegeID);
	
		/// <summary>
		/// Add a new privilege
		/// </summary>
		/// <param name="description">A description of the privilege</param>
		/// <returns>Number of rows affected</returns>
		Int32 AddPrivilege(String description);

		/// <summary>
		/// Updates the Description of the Privilege with the PrivilegeID specified
		/// </summary>
		/// <param name="privilegeID">PrivilegeID to update</param>
		/// <param name="description">New description</param>
		void SavePrivilege(Int32 privilegeID, String description);

		/// <summary>
		/// Creates a new SecurityGroup.
		/// </summary>
		/// <param name="description"></param>
		/// <returns></returns>
		Int32 AddGroup(String description);

		/// <summary>
		/// Changes the description of an existing SecurityGroup.
		/// </summary>
		/// <param name="securityGroupID"></param>
		/// <param name="description"></param>
		void SaveGroup(Int32 securityGroupID, String description);
		
		/// <summary>
		/// Saves the Group Privileges for the specified Group
		/// </summary>
		/// <param name="securityGroupID">The Group to save the privilege</param>
		/// <param name="privilegeID">The privilege to save</param>
		void SaveGroupPrivilege(Int32 securityGroupID, Int32 privilegeID);

		/// <summary>
		/// Deletes the privilege for the specified groupID
		/// </summary>
		/// <param name="securityGroupID">The GroupID</param>
		/// <param name="privilegeID">The PrivilegeID</param>
		void DeleteGroupPrivilege(Int32 securityGroupID, Int32 privilegeID);

		#region admin member search crap -> move this to admin later

	    int[] PerformAdminSearch(string email, string userName, string memberId, string phone, string firstName,
                                 string lastName, string zipcode, string city, string ipAddress, int communityId, bool exactUsername, int filterType);

	    int[] PerformAdminSearchPagination(string email, string userName, string memberId, string phone,
	                                              string firstName,string lastName, string zipcode, string city, string ipAddress,
	                                              int communityId, bool exactUsername, int filterType, int pageNumber, int rowsPerPage);

		//adding more crap
		/// <summary>
		/// 
		/// </summary>
		/// <param name="email"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByEmail(string email, int startRow, int pageSize, ref int totalRows);
		ArrayList GetMembersByEmail(string machineName, CacheReference cacheReference, string email, int startRow, int pageSize, ref int totalRows);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByUserName(string userName, int startRow, int pageSize, ref int totalRows);
		ArrayList GetMembersByUserName(string machineName, CacheReference cacheReference, string userName, int startRow, int pageSize, ref int totalRows);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByPhoneNumber(string phoneNumber, int startRow, int pageSize, ref int totalRows);
		ArrayList GetMembersByPhoneNumber(string machineName, CacheReference cacheReference, string phoneNumber, int startRow, int pageSize, ref int totalRows);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="creditCard"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByCreditCard(string creditCard, int startRow, int pageSize, ref int totalRows);
		ArrayList GetMembersByCreditCard(string machineName, CacheReference cacheReference, string creditCard, int startRow, int pageSize, ref int totalRows);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="routing"></param>
		/// <param name="checking"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByChecking(string routing, string checking, int startRow, int pageSize, ref int totalRows);
		ArrayList GetMembersByChecking(string machineName, CacheReference cacheReference, string routing, string checking, int startRow, int pageSize, ref int totalRows);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByMemberID(int memberID, ref int totalRows);
		ArrayList GetMembersByMemberID(string machineName, CacheReference cacheReference, int memberID, ref int totalRows);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		MemberPaymentCollection GetMemberPaymentInfo(int memberID);

		//more crap for versioning
		/// <summary>
		/// 
		/// </summary>
		/// <param name="email"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByEmail(string email, int startRow, int pageSize, ref int totalRows, int version);
		ArrayList GetMembersByEmail(string machineName, CacheReference cacheReference, string email, int startRow, int pageSize, ref int totalRows, int version);
        ArrayList GetMembersByEmail(string machineName, CacheReference cacheReference, string email, int startRow, int pageSize, ref int totalRows, int version, bool memberIdOrderByDesc);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByUserName(string userName, int startRow, int pageSize, ref int totalRows, int version);
		ArrayList GetMembersByUserName(string machineName, CacheReference cacheReference, string userName, int startRow, int pageSize, ref int totalRows, int version);
        ArrayList GetMembersByUserName(string machineName, CacheReference cacheReference, string userName, int startRow, int pageSize, ref int totalRows, int version, bool memberIdOrderByDesc, bool exactMatchOnly);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByPhoneNumber(string phoneNumber, int startRow, int pageSize, ref int totalRows, int version);
		ArrayList GetMembersByPhoneNumber(string machineName, CacheReference cacheReference, string phoneNumber, int startRow, int pageSize, ref int totalRows, int version);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="creditCard"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByCreditCard(string creditCard, int startRow, int pageSize, ref int totalRows, int version);
		ArrayList GetMembersByCreditCard(string machineName, CacheReference cacheReference, string creditCard, int startRow, int pageSize, ref int totalRows, int version);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="routing"></param>
		/// <param name="checking"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByChecking(string routing, string checking, int startRow, int pageSize, ref int totalRows, int version);
		ArrayList GetMembersByChecking(string machineName, CacheReference cacheReference, string routing, string checking, int startRow, int pageSize, ref int totalRows, int version);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		ArrayList GetMembersByMemberID(int memberID, ref int totalRows, int version);
		ArrayList GetMembersByMemberID(string machineName, CacheReference cacheReference, int memberID, ref int totalRows, int version);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		
		#endregion

        #region admin others
		AdminMemberDomainMapperCollection GetAdminMemberDomainMappers();

	    void IncrementCommunicationCounter(int memberId, int siteId, int[] counterType);

	    Dictionary<int, int> GetCommunicationWarningCounts(int memberId, int siteId);
        #endregion

        string GetMemberSalt(string encryptedEmail);

        string GetMemberSalt(string encryptedEmail, int communityId);

	    int GetNewMemberID();

	    MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId);
	    bool SavePassword(int memberId, int communityId, string passwordHash);
	    bool CheckEmailAddressExists(string emailAddress, int communityId);
	}
}
