using System;

using Matchnet.Member.ValueObjects.AttributeMetadata;


namespace Matchnet.Member.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAttributeMetadataService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		Attributes GetAttributes();
	}
}
