﻿using System;
using System.Collections.Generic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Common.AccessService;

namespace Matchnet.Member.ValueObjects.Interfaces
{
    public interface IMemberDTO : ICacheable
    {
        Photo GetDefaultPhoto(Int32 communityID, bool publicOnly);
        Photo GetRandomApprovedForMainPhoto(int communityId, bool publicOnly);
        List<Photo> GetApprovedForMainPhotos(int communityId, bool publicOnly);
        List<Photo> GetApprovedPhotos(int communityId, bool publicOnly);
        List<Photo> GetApprovedPhotosExcludingMain(int communityId, bool publicOnly);
        DateTime GetAttributeDate(Brand brand, string attributeName);
        DateTime GetAttributeDate(Brand brand, string attributeName,DateTime defaultValue);
        DateTime GetAttributeDate(Int32 communityID, Int32 siteID, Int32 brandID, string attributeName, DateTime defaultValue);
        Int32 GetAttributeInt(Brand brand, string attributeName);
        Int32 GetAttributeInt(Brand brand, string attributeName, Int32 defaultValue);
        Int32 GetAttributeInt(Int32 communityID, Int32 siteID, Int32 brandID, string attributeName, Int32 defaultValue);
        bool GetAttributeBool(Brand brand, string attributeName);
        bool GetAttributeBool(Int32 communityID, Int32 siteID, Int32 brandID, string attributeName);
        string GetAttributeText(Brand brand, string attributeName);
        string GetAttributeText(Brand brand, string attributeName, string defaultValue);
        string GetAttributeText(Brand brand, string attributeName, out TextStatusType textStatus);
        string GetAttributeText(Int32 communityID, Int32 siteID, Int32 brandID, Int32 languageID, string attributeName);
        string GetAttributeText(Int32 communityID, Int32 siteID, Int32 brandID, Int32 languageID, Int32 attributeID, string defaultValue);
        string GetAttributeText(Int32 communityID, Int32 siteID, Int32 brandID, Int32 languageID, string attributeName, string defaultValue);
        string GetAttributeText(Int32 communityID, Int32 siteID, Int32 brandID, Int32 languageID, string attributeName, string defaultValue, out TextStatusType textStatus);
        string GetAttributeTextApproved(Brand brand, string attributeName, string unapprovedResource);
        string GetAttributeTextApproved(Brand brand, string attributeName, int viewerMemberID, string unapprovedResourceValue);
        string GetAttributeTextApproved(Brand brand, string[] attributeNames, Int32 viewerMemberID, string unapprovedResourceValue);
        string GetAttributeTextApproved(Brand brand, int languageID, string attributeName, string defaultValue, string unapprovedResource);
        PhotoCommunity GetPhotos(Int32 communityID);
        int MemberID { get; }
        MemberType MemberType { get; }
        int[] GetSiteIDList();
        int[] GetCommunityIDList();
        string GetUserName(Brand brand);
        bool IsPayingMember(Int32 siteID);
        bool IsSiteMember(int siteID);
        bool IsCommunityMember(int communityID);
        AccessPrivilege GetUnifiedAccessPrivilege(PrivilegeType privilegeType, int brandID, int siteID, int communityID);
        bool HasApprovedPhoto(Int32 communityID);
        bool IsUpdatedMember(Int32 communityID);
        bool IsNewMember(Int32 communityID);
        bool IsNewMember(Int32 communityID, int days);
        DateTime GetLastLogonDate(Int32 communityID);
        DateTime GetLastLogonDate(Int32 communityID, out Int32 brandID);
//        MemberUpdate MemberUpdate { get; }
        string EmailAddress { get; }
    }
}
