using System;
using System.Collections;


namespace Matchnet.Member.ValueObjects.Privilege
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PrivilegeCollection : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "^PC~";

		private Int32 _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;
		private Hashtable _index;

		/// <summary>
		/// 
		/// </summary>
		public PrivilegeCollection()
		{
			_index = new Hashtable();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilege"></param>
		public void Add(Privilege privilege)
		{
			if (!_index.ContainsKey(privilege.PrivilegeID))
			{
				_index.Add(privilege.PrivilegeID, privilege);
			}

			base.InnerList.Add(privilege);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilegeID"></param>
		/// <returns></returns>
		public Privilege FindByID(Int32 privilegeID)
		{
			return _index[privilegeID] as Privilege;
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}
		#endregion
	}
}
