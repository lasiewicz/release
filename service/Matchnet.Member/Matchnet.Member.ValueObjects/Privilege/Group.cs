using System;

namespace Matchnet.Member.ValueObjects.Privilege
{
	/// <summary>
	/// Summary description for SecurityGroup.
	/// </summary>
	[Serializable]
	public class SecurityGroup : IValueObject//, ICacheable
	{
		private int _securityGroupID;
		private string _description;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="securityGroupID"></param>
		/// <param name="description"></param>
		public SecurityGroup(int securityGroupID, string description)
		{
			_securityGroupID = securityGroupID;
			_description = description;
		}

		/// <summary>
		/// 
		/// </summary>
		public int SecurityGroupID
		{
			get
			{
				return _securityGroupID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
		}

/*		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add SecurityGroup.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add SecurityGroup.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add SecurityGroup.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add SecurityGroup.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add SecurityGroup.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add SecurityGroup.GetCacheKey implementation
			return null;
		}

		#endregion*/
	}
}
