using System;
using System.Collections;

namespace Matchnet.Member.ValueObjects.Privilege
{
	/// <summary>
	/// Summary description for MemberPrivilegeCollection.
	/// </summary>
	[Serializable]
	public class MemberPrivilegeCollection : CollectionBase	, IValueObject, ICacheable, IReplicable
	{
		private const string CACHE_KEY_PREFIX = "^MPC~";

		private Int32 _memberID;
		private Hashtable _index;

		private Int32 _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;

		/// <summary>
		/// 
		/// </summary>
		public MemberPrivilegeCollection(Int32 memberID)
		{
			_memberID = memberID;
			_index = new Hashtable();
		}

		public MemberPrivilege this[Int32 index]
		{
			get
			{
				return base.InnerList[index] as MemberPrivilege;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPrivilege"></param>
		public void Add(MemberPrivilege memberPrivilege)
		{
			MemberPrivilege mpOld = _index[memberPrivilege.Privilege.PrivilegeID] as MemberPrivilege;

			if (mpOld == null)
			{
				_index.Add(memberPrivilege.Privilege.PrivilegeID, memberPrivilege);
				base.InnerList.Add(memberPrivilege);
			}
			else
			{
				base.InnerList.Remove(mpOld);
				base.InnerList.Add(memberPrivilege);
				_index[memberPrivilege.Privilege.PrivilegeID] = memberPrivilege;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilegeID"></param>
		public void Revoke(Int32 privilegeID)
		{
			MemberPrivilege mp = _index[privilegeID] as MemberPrivilege;

			if (mp != null && mp.PrivilegeState == PrivilegeState.Individual)  //don't allow revoking group-inherited privileges
			{
				mp.PrivilegeState = PrivilegeState.Denied;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilegeID"></param>
		public void Remove(Int32 privilegeID)
		{
			MemberPrivilege mp = _index[privilegeID] as MemberPrivilege;

			if (mp != null)
			{
				_index.Remove(privilegeID);
				base.InnerList.Remove(mp);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilegeID"></param>
		/// <returns></returns>
		public bool HasPrivilege(Int32 privilegeID)
		{
			MemberPrivilege mp = _index[privilegeID] as MemberPrivilege;

			if (mp != null && mp.PrivilegeState != PrivilegeState.Denied)
			{
				return true;
			}

			return false;
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return MemberPrivilegeCollection.GetCacheKey(_memberID);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 memberID)
		{
			return CACHE_KEY_PREFIX + memberID.ToString();
		}
		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(this.GetCacheKey());
		}

		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		Matchnet.CacheItemMode Matchnet.ICacheable.CacheMode
		{
			get
			{
				// TODO:  Add MemberPrivilegeCollection.Matchnet.ICacheable.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		#endregion
	}
}
