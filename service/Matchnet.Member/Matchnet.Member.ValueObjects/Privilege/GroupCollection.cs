using System;
using System.Collections;

namespace Matchnet.Member.ValueObjects.Privilege
{
	/// <summary>
	/// Summary description for SecurityGroupCollection.
	/// </summary>
	[Serializable]
	public class SecurityGroupCollection : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "^GC~";

		private Int32 _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;

		private Int32 _privilegeID = Constants.NULL_INT; //for cache key

		private Hashtable securityGroups;

		/// <summary>
		/// 
		/// </summary>
		public SecurityGroupCollection()
		{
			securityGroups = new Hashtable();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="privilegeID"></param>
		public SecurityGroupCollection(Int32 privilegeID)
		{
			_privilegeID = privilegeID;
			securityGroups = new Hashtable();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="securityGroup"></param>
		public void Add(SecurityGroup securityGroup)
		{
			base.InnerList.Add(securityGroup);
			securityGroups.Add(securityGroup.SecurityGroupID, securityGroup);
		}

		/// <summary>
		/// 
		/// </summary>
		public SecurityGroup this[int securityGroupID]
		{
			get
			{
				return securityGroups[securityGroupID] as SecurityGroup;
			}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return GetCacheKey(_privilegeID);
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="PrivilegeID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 PrivilegeID)
		{
			return CACHE_KEY + PrivilegeID.ToString();
		}
	}
}
