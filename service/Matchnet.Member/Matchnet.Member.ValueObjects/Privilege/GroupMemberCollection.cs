using System;

namespace Matchnet.Member.ValueObjects.Privilege
{
	/// <summary>
	/// Summary description for SecurityGroupMemberCollection.
	/// </summary>
	[Serializable]
	public class SecurityGroupMemberCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{
		private const string CACHE_KEY_PREFIX = "^GMC~";

		private Int32 _securityGroupID;

		private Int32 _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;

		/// <summary>
		/// 
		/// </summary>
		public SecurityGroupMemberCollection(Int32 securityGroupID)
		{
			_securityGroupID = securityGroupID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		public void Add(Int32 memberID)
		{
			base.InnerList.Add(memberID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		public void Remove(Int32 memberID)
		{
			base.InnerList.Remove(memberID);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return GetCacheKey(_securityGroupID);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="securityGroupID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 securityGroupID)
		{
			return CACHE_KEY_PREFIX + securityGroupID.ToString();
		}
		#endregion
	}
}
