﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Member.ValueObjects
{
    [Serializable]   
    public enum LogonCredentialsStatus
    {
        /// <summary>
        /// Indicates that the user was successfully registered.
        /// </summary>
        Success = 0,

        /// <summary>
        /// If an unknown registerresult comes back from the savelogon method, this is what we'll get
        /// </summary>
        Unknown=1,

        /// <summary>
        /// Indicates that the user's password exceeded the maximum length.
        /// </summary>
        PasswordLengthExceeded = 401005,

        /// <summary>
        /// Indicates that the user's e-mail address is not allowed because the domain has been blocked.
        /// </summary>
        EmailAddressNotAllowed = 519238,

        /// <summary>
        /// Indicates that the user is already registered in the system.
        /// </summary>
        AlreadyRegistered = 401003,

        /// <summary>
        /// Indicates that the user's e-mail address has been blocked.
        /// </summary>
        EmailAddressBlocked = 519239,

        /// <summary>
        /// Indicates that the user's e-mail address is invalid.
        /// </summary>
        InvalidEmail = 519385
    }

    [Serializable]   
    public class CreateLogonCredentialsResult
    {
        public LogonCredentialsStatus Status { get; set; }

        public string UserName { get; set; }

        public int MemberId { get; set; }

        public static LogonCredentialsStatus RegisterStatusToLogonCredentialsStatus(RegisterStatusType registerStatus)
        {
            switch(registerStatus)
            {
                case RegisterStatusType.PasswordLengthExceeded:
                    return LogonCredentialsStatus.PasswordLengthExceeded;
                case RegisterStatusType.EmailAddressNotAllowed:
                    return LogonCredentialsStatus.EmailAddressNotAllowed;
                case RegisterStatusType.Success:
                    return LogonCredentialsStatus.Success;
                case RegisterStatusType.InvalidEmail:
                    return LogonCredentialsStatus.InvalidEmail;
                case RegisterStatusType.AlreadyRegistered:
                    return LogonCredentialsStatus.AlreadyRegistered;
                default:
                    return LogonCredentialsStatus.Unknown;
            }
        }
    }

    
}
