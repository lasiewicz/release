﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Member.ValueObjects
{
    [Serializable]
    public class MemberBasicLogonInfo
    {
        public int MemberId { get; set; }
        public string EmailAddress { get; set; }
        public string UserName { get; set; }
    }
}
