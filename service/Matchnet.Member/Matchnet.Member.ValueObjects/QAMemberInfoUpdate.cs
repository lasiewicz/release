﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Member.ValueObjects
{
    [Serializable]
    public class QAMemberInfoUpdate : IValueObject
    {
        public QAMemberInfoUpdateMode UpdateMode { get; set; }
        public int SiteID { get; set; }
        public int MemberID { get; set; }
        public int ValueInt { get; set; }

        public QAMemberInfoUpdate()
        {
            UpdateMode = QAMemberInfoUpdateMode.None;
            SiteID = Constants.NULL_INT;
            MemberID = Constants.NULL_INT;
            ValueInt = Constants.NULL_INT;
        }
    }

    public enum QAMemberInfoUpdateMode : int
    {
        None = 0,
        GenderMask = 1,
        SelfSuspendedFlag = 2,
        GlobalStatusMask = 3,
        HasPhotoFlag = 4
    }
}
