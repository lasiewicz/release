using System;

using Matchnet;

namespace Matchnet.Member.ValueObjects
{


	/// <summary>
	/// Defines all possible outcomes of an authentication attempt.
	/// </summary>
	[Serializable]
	public enum AuthenticationStatus : int
	{
		/// <summary>
		/// Indicates the the user has been authenticated.
		/// </summary>
		Authenticated = 0,
		
		/// <summary>
		/// Indicates that the user provided an invalid e-mail address.
		/// </summary>
		InvalidEmailAddress = -1,
		
		/// <summary>
		/// Indicates that the user provide an invalid password.
		/// </summary>
		InvalidPassword = -2,

		/// <summary>
		/// Indicates that the member has been admin suspended.
		/// </summary>
		AdminSuspended = -3
	}

	/// <summary>
	/// Represents the result of an authentication attempt.
	/// </summary>
	[Serializable]
	public sealed class AuthenticationResult : Matchnet.IValueObject
	{
		private string _userName = Constants.NULL_STRING;
		private int _memberID = Constants.NULL_INT;
		private AuthenticationStatus _status;


		private AuthenticationResult()
		{
			// Hide the default constructor.
		}


		/// <summary>
		/// Initializes a new Matchnet.Member.AuthenticationResult object.
		/// </summary>
		/// <param name="status">The status returned from the authentication action.</param>
		/// <param name="memberID">The member id.</param>
		/// <param name="userName">The user name</param>
		public AuthenticationResult(AuthenticationStatus status, int memberID, string userName)
		{
			_status = status;
			_memberID = memberID;
			_userName = userName;
		}

		/// <summary>
		/// Gets the status of the authentication action.
		/// </summary>
		public AuthenticationStatus Status
		{
			get{return _status;}
		}

		/// <summary>
		/// Gets the unique identifier for the member.
		/// </summary>
		public int MemberID
		{
			get{return _memberID;}
		}

		/// <summary>
		/// Gets the user name of the member.
		/// </summary>
		public string UserName
		{
			get{return _userName;}
		}

	}
}
