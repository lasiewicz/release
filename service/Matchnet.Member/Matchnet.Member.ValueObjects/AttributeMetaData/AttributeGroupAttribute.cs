using System;

namespace Matchnet.Member.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public sealed class AttributeGroupAttribute
	{
		private int _ID;
		private bool _isOptional;

		private AttributeGroupAttribute()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="isOptional"></param>
		public AttributeGroupAttribute(int ID, bool isOptional)
		{
			_ID = ID;
			_isOptional = isOptional;
		}


		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			get
			{
				return _ID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsOptional
		{
			get
			{
				return  _isOptional;
			}
		}

	}
}
