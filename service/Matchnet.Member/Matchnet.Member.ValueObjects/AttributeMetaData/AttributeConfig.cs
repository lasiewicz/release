using System;

using Matchnet;


namespace Matchnet.Member.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// Attributes are pieces of information related to a Member, such as Birth Date, Political Orientation, and Last Updated Date.  
	/// Attributes are typically free text, single-select from options, or multi-select from checkboxes.  (A limited number of Attributes 
	/// may also be of datatype 'bit'.  
	/// </summary>
	/// <seealso cref="AttributeDataType">See the enum AttributeDataType.</seealso>
	[Serializable]
	public enum AttributeType : byte
	{
		/// <summary>
		/// An Attribute whereby the Member enters text by typing into a box.  Examples include "My idea of a perfect first date" and "Headline".
		/// </summary>
		FreeText = 1,
		/// <summary>
		/// An Attribute whereby the Member selects a single value from a drop-down option list.  Examples include "Body Style" and "Height".
		/// </summary>
		SingleSelect = 2,
		/// <summary>
		/// An Attribute whereby the Member selects multiple values from checkboxes.  Examples include "My favorite activities" and "Languages I speak".
		/// </summary>
		MultiSelect = 3
	}


	/// <summary>
	/// Attributes are pieces of information related to a Member, such as Birth Date, Political Orientation, and Last Updated Date.
	/// AttributeStatusType is not related to whether or not the Attribute has been approved for display in the UI.  Instead, AttributeStatusType indicates
	/// whether the Attribute has any usage restrictions.   
	/// </summary>
	[Serializable]
	[Flags]
	public enum AttributeStatusType : byte
	{
		/// <summary>
		/// Indicates that there are no restrictions on use of this attribute.
		/// </summary>
		None = 0,
		/// <summary>
		/// (DEVTODO: THIS IS NOT HOW TO SPELL 'IMMUTABLE'.)  Indicates that the Attribute cannot or will not be changed by the Member.  
		/// </summary>
		Immuatable = 1,
		/// <summary>
		/// DEVTODO: The term 'Domain' has been replaced with either 'Community' or 'Site'.  Indicates that the Attribute is applicable to only one or more Sites,
		/// but not all.  
		/// For example, the Attribute "Outness" is only applicable to Glimpse.com, and the Attribute 'Synagogue' is only applicable to Jewish themed Sites.
		/// </summary>
		DomainSpecific = 2
	}


	/// <summary>
	/// Attributes are pieces of information related to a Member, such as Birth Date, Political Orientation, and Last Updated Date.  
	/// Attributes may be free text, single-select from options, or multi-select from checkboxes, and are managed within the system as 
	/// one of the 5 AttributeDataTypes.
	/// </summary>
	[Serializable]
	public enum AttributeDataType
	{
		/// <summary>
		/// PRELIMINARY: Used to manage flag Attributes, such as the 'isAdmin' Attribute.
		/// </summary>
		Bit,
		/// <summary>
		/// A date value.  Attriubtes of Date type include LastUpdatedDate.
		/// </summary>
		Date,
		/// <summary>
		/// A mask describing a multi-select Attribute value, such as the types of food the Member likes.
		/// </summary>
		Mask,
		/// <summary>
		/// This number could be a number entered by the Member, or the internal number associated with a single value within a single-select 
		/// Attribute, such as the Member's Education Emphasis selection.
		/// </summary>
		Number,
		/// <summary>
		/// Text typed in by the Member, such as the My Ideal Relationship field.
		/// </summary>
		Text
	}


	/// <summary>
	/// Sealed AttributeConfig.
	/// </summary>
	[Serializable]
	public sealed class AttributeConfig : IValueObject, ICacheable
	{
		private int _cacheTTLSeconds = 60 * 60;  // Default to 1-hour
		private int _ID;
		private string _name;
		private AttributeType _type;
		private AttributeStatusType	_status;
		private AttributeDataType _dataType;
		private int _textLength;
		private bool _isEncrypted = false;
		private string _defaultValue;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private bool _useStringKey = false;

		private AttributeConfig()
		{
			// Hide default constructor
		}


		/// <summary>
		/// Constructor for AttributeConfig.  AttributeConfig contains all of the basic information on a single attribute, but does not include any option values.  AttributeConfig
		/// provides the Attribute name, type, data type, and default or maximum values.
		/// </summary>
		/// <param name="ID">The ID of the Attribute.</param>
		/// <param name="name">The name of the Attribute.  For example, "Ethnicity".</param>
		/// <param name="type">The AttributeType.</param>
		/// <param name="status">The AttributeStatusType.</param>
		/// <param name="dataType">The AttributeDataType.</param>
		/// <param name="textLength">The maximum allowed length, if the Attribute is of AttributeType=1(Free Text).</param>
		/// <param name="isEncrypted">PRELIMINARY: Why have this flag?  Passwords are the only things encrypted, and they're not Attributes.</param>
		/// <param name="defaultValue">The default value for the Attribute, if any.</param>
		public AttributeConfig(int ID,
			string name,
			AttributeType type,
			AttributeStatusType	status,
			AttributeDataType dataType,
			int textLength,
			bool isEncrypted,
			string defaultValue)
		{
			_ID = ID;
			_name = name;
			_type = type;
			_status = status;
			_dataType = dataType;
			_textLength = textLength;
			_isEncrypted = isEncrypted;
			_defaultValue = defaultValue;
		}


		/// <summary>
		/// GetAttributeKey is used to find an Attribute on a given Community and Language.  
		/// </summary>
		/// <param name="communityID">The primary key of a Community this Attribute is associated with.</param>
		/// <param name="languageID">The primary key of a Language this attribute is associated with.</param>
		/// <returns>PRELIMINARY: Returns the AttributeID (as a string key value) for any attribute that exists on the given Community and Language.  If no Attribute exists
		/// for the given Community and Language an empty string will be returned.</returns>
		public string GetAttributeKey(int communityID, int languageID)
		{
			string key = _ID.ToString();

			key = key + ":";
			if ((this.Status & AttributeStatusType.DomainSpecific) == AttributeStatusType.DomainSpecific)
			{
				key = key + communityID;
			}

			key = key + ":";
			if (languageID != Constants.NULL_INT && this.DataType == AttributeDataType.Text)
			{
				key = key + languageID;
			}

			return key;
		}


		/// <summary>
		/// Getter method for CacheKey.
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			if (!_useStringKey)
			{
				return AttributeConfig.GetCacheKey(_ID);
			}
			else
			{
				return AttributeConfig.GetCacheKey(_name);
			}
		}


		/// <summary>
		/// Getter Method for CacheKey, given the AttributeID.
		/// </summary>
		/// <param name="ID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int ID)
		{
			return "~ATTRIBUTE^" + ID.ToString();
		}


		/// <summary>
		/// Getter Method for CacheKey, given the Attribute name.
		/// </summary>
		/// <param name="attributeName">The name of the Attribute.  e.g. "Ethnicity".</param>
		/// <returns></returns>
		public static string GetCacheKey(string attributeName)
		{
			return "~ATTRIBUTE^" + attributeName.ToLower();
		}

		
		/// <summary>
		/// Getter method for AttributeID.
		/// </summary>
		public int ID
		{
			get
			{
				return _ID;
			}
		}


		/// <summary>
		/// Getter method for Attribute name.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}


		/// <summary>
		/// Getter method for AttributeType.
		/// </summary>
		public AttributeType Type
		{
			get
			{
				return _type;
			}
		}


		/// <summary>
		/// Getter method for AttributeStatusType.
		/// </summary>
		public AttributeStatusType Status
		{
			get
			{
				return _status;
			}
		}


		/// <summary>
		/// Getter method for AttributeDataType.
		/// </summary>
		public AttributeDataType DataType
		{
			get
			{
				return _dataType;
			}
		}


		/// <summary>
		/// Getter method for textLength.
		/// </summary>
		public int TextLength
		{
			get
			{
				return _textLength;
			}
		}


		/// <summary>
		/// Getter method for isEncrypted.
		/// </summary>
		public bool IsEncrypted
		{
			get
			{
				return _isEncrypted;
			}
		}


		/// <summary>
		/// Getter method defaultValue.
		/// </summary>
		public string DefaultValue
		{
			get
			{
				return _defaultValue;
			}
		}


		/// <summary>
		/// Getter method for useStringKey.  
		/// </summary>
		public bool UseStringKey
		{
			get
			{
				return _useStringKey;
			}
			set
			{
				_useStringKey = value;
			}
		}


		#region ICacheable Members

		/// <summary>
		/// Getter method for cacheTTLSeconds.
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// Getter method for the CachItemMode.
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}


		/// <summary>
		/// Getter method for cachePriority.
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		#endregion
	}
}
