using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MemberSite : IValueObject
	{
		private Int16 _communityID;
		private DateTime _insertDate;
		private DateTime _lastActiveDate;
		private bool _isPayingMember = false;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="insertDate"></param>
		/// <param name="lastActiveDate"></param>
		/// <param name="isPayingMember"></param>
		public MemberSite(Int16 communityID, DateTime insertDate, DateTime lastActiveDate, bool isPayingMember)
		{
			_communityID = communityID;
			_insertDate = insertDate;
			_lastActiveDate = lastActiveDate;
			_isPayingMember = isPayingMember;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 CommunityID
		{
			get			
			{
				return _communityID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get
			{
				return _insertDate;
			}
			set
			{
				_insertDate = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime LastActiveDate
		{
			get
			{
				return _lastActiveDate;
			}
			set
			{
				_lastActiveDate = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsPayingMember
		{
			get
			{
				return _isPayingMember;
			}
			set
			{
				_isPayingMember = value;
			}
		}
	}
}
