using System;
using Spark.Common.Utilities;

namespace Matchnet.Member.ValueObjects
{
    /// <summary>
    /// This is enum is used to create a cache key for
    /// the attribute set returned from an attribute set request
    /// </summary>
    public enum APIAttributeSets
    {
        /// <summary>
        /// The mini profile
        /// </summary>
        [StringValue("miniprofile")]
        MiniProfile = 1,
        /// <summary>
        /// The full profile
        /// </summary>
        [StringValue("fullprofile")]
        FullProfile = 2,
        /// <summary>
        /// The visitor profile
        /// </summary>
        [StringValue("visitorprofile")]
        VisitorProfile = 3,
        /// <summary>
        /// The result set profile
        /// </summary>
        [StringValue("resultsetprofile")]
        ResultSetProfile = 4,
        /// <summary>
        /// The secret admirer profile
        /// </summary>
        [StringValue("secretadmirerprofile")]
        SecretAdmirerProfile = 5,
        /// <summary>
        /// The targeting profile_us
        /// </summary>
        [StringValue("targetingprofile_us")]
        TargetingProfile_us = 6,
        /// <summary>
        /// The targeting profile_il
        /// </summary>
        [StringValue("targetingprofile_il")]
        TargetingProfile_il = 7,
        /// <summary>
        /// The mini profile v2
        /// </summary>
        [StringValue("miniprofilev2")]
        MiniProfileV2 = 8,
        /// <summary>
        /// The full profile v2
        /// </summary>
        [StringValue("fullprofilev2")]
        FullProfileV2 = 9,
        /// <summary>
        /// The visitor profile v2
        /// </summary>
        [StringValue("visitorprofilev2")]
        VisitorProfileV2 = 10,
        /// <summary>
        /// The result set profile v2
        /// </summary>
        [StringValue("resultsetprofilev2")]
        ResultSetProfileV2 = 11,
        /// <summary>
        /// The secret admirer profile v2
        /// </summary>
        [StringValue("secretadmirerprofilev2")]
        SecretAdmirerProfileV2 = 12,
        /// <summary>
        /// The Admin Attributes includes fraud response
        /// </summary>
        [StringValue("adminattributes")]
        AdminAttributes = 13,
        /// <summary>
        /// The result set profile big v2, same as result set profile but includes big data like About Me essay
        /// </summary>
        [StringValue("resultsetprofilev2")]
        ResultSetProfileBigV2 = 14,
        /// <summary>
        /// Tealium targeting info
        /// </summary>
        [StringValue("tealium")]
        Tealium = 15,
        /// <summary>
        /// Tealium + gam targeting info combined us
        /// </summary>
        [StringValue("tealiumtargetingprofile_us")]
        TealiumTargetingProfile_US = 16,
        /// <summary>
        /// Tealium + gam targeting info combined il
        /// </summary>
        [StringValue("tealiumtargetingprofile_il")]
        TealiumTargetingProfile_IL = 17
    }

	/// <summary>
	/// 
	/// </summary>
	public class ServiceConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "MEMBER_SVC";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.Member.Service";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_MANAGER_NAME = "MemberSM";

		/// <summary>
		/// 
		/// </summary>
		public const int ATTRIBUTEID_EMAILADDRESS = 12;
		/// <summary>
		/// 
		/// </summary>
		public const int ATTRIBUTEID_USERNAME = 302;
		/// <summary>
		/// 
		/// </summary>
		public const int LANGUAGE_ENGLISH = 2;
        /// <summary>
        /// 
        /// </summary>
        public const int LANGUAGE_HEBREW = 262144;
         /// <summary>
        /// 
        /// </summary>
        public const int LANGUAGE_FRENCH = 8;
		/// <summary>
		/// 
		/// </summary>
		public const Int32 ATTRIBUTEGROUPID_USERNAME = 438;
		/// <summary>
		/// 
		/// </summary>
		public const Int32 ATTRIBUTEGROUPID_USERNAME_COMMUNITY = 2486;
		/// <summary>
		/// 
		/// </summary>
		public const Int32 ATTRIBUTEGROUPID_EMAILADDRESS = 530;

	    public const Int32 EMAIL_ADDRESS_MAX_LENGTH = 255;

	    public const Int32 USERNAME_MAX_LENGTH = 50;
        
		private ServiceConstants()
		{
		}

	    public const string MEMBASE_BL_SETTING_CONSTANT = "USE_MEMBASE_BL_CACHE";
        public const string MEMBASE_SA_SETTING_CONSTANT = "USE_MEMBASE_SA_CACHE";
        public const string MNLOGON_READ_SETTING_CONSTANT = "MNLOGON_READ_FROM_NEW_TABLES";
	    public const string ENABLE_MEMBER_DTO_CONSTANT = "ENABLE_MEMBER_DTO";
	    public const string MEMBASE_WEB_BUCKET_NAME = "webcache";
	    public const string MEMBASE_MEMBERDTO_BUCKET_NAME = "memberdtocache";
	    public const string MNLOGON_COMMUNITY_LIST = "MNLOGON_BH_COMMUNITY_LIST";
	    public const string MNLOGON_STOP_WRITING_TO_OLD_TABLES = "MNLOGON_STOP_WRITING_TO_OLD_TABLES";
        public const string USE_AUTHENTICATE_SAMETHOD_WITHOUT_BRAND = "USE_AUTHENTICATE_SAMETHOD_WITHOUT_BRAND";
        public const string ENABLE_MEMBER_ATTR_SET_CACHE_CONSTANT = "ENABLE_MEMBER_ATTR_SET_CACHE";
	    public const string FIELD_SEPARATOR = "-";
	}
}
