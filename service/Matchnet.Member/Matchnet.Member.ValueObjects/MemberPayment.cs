using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Summary description for MemberPayment.
	/// </summary>
	[Serializable]
	public class MemberPayment
	{
		private int _memberPaymentAttributeID;
		private string _phoneNumber;
		private string _creditCardNumber;
		private DateTime _insertDate;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberPaymentAttributeID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="creditCardNumber"></param>
		/// <param name="insertDate"></param>
		public MemberPayment(int memberPaymentAttributeID, string phoneNumber, string creditCardNumber, DateTime insertDate)
		{
			_memberPaymentAttributeID = memberPaymentAttributeID;
			_phoneNumber = phoneNumber;
			_creditCardNumber = creditCardNumber;
			_insertDate = insertDate;
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberPaymentAttributeID
		{
			get { return(_memberPaymentAttributeID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string PhoneNumber
		{
			get { return(_phoneNumber); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string CreditCardNumber
		{
			get { return(_creditCardNumber); }
			//	Hate to have mutable value object fields, but often we need to
			//	modify the credit card field for display purposes and this
			//	is the easiest way...
			set { _creditCardNumber = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return(_insertDate); }
		}
	}
}
