using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.CacheSynchronization.Tracking;


namespace Matchnet.Member.ValueObjects
{
	[Serializable]
	public class CachedMemberBytes : IValueObject, ICacheable, IReplicable
	{
		private const string CACHE_KEY_PREFIX = "Member";

		private Int32 _memberID;
		private byte[] _memberBytes;
		private Int32 _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheMode = Matchnet.CacheItemMode.Absolute;
		public const int CACHED_MEMBERBYTES_VERSION=3;
	
		private int _version=0;
		[NonSerialized]
		private ReferenceTracker _referenceTracker;

		public CachedMemberBytes(Int32 memberID,
			byte[] memberBytes)
		{
			_memberID = memberID;
			_memberBytes = memberBytes;
		}

		public CachedMemberBytes(Int32 memberID,
			byte[] memberBytes, int version)
		{
			_memberID = memberID;
			_memberBytes = memberBytes;
			_version=version;
		}
		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}
		

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public byte[] MemberBytes
		{
			get
			{
				return _memberBytes;
			}
			set
			{
				_memberBytes = value;
			}
		}

		public Int32 Version
		{
			get
			{
				return _version;
			}
			set
			{
				_version=value;
			}

		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int memberID)
		{
			return CACHE_KEY_PREFIX +  memberID.ToString();
		}
		


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			string key="";
			
				key= CachedMember.GetCacheKey(_memberID);
			
			return key;
		}


		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
