using System;

namespace Matchnet.Member.ValueObjects
{
	/// <summary>
	/// Summary description for MemberBasicsCollection.
	/// </summary>
	[Serializable]
	public class MemberBasicsCollection : System.Collections.CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		public MemberBasicsCollection()
		{

		}

		/// <summary>
		/// Indexer overload
		/// </summary>
		public MemberBasics this[int index]
		{
			get { return ((MemberBasics)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="memberBasics">banner to add to collection</param>
		/// <returns></returns>
		public int Add(MemberBasics memberBasics)
		{
			return base.InnerList.Add(memberBasics);
		}
	}
}
