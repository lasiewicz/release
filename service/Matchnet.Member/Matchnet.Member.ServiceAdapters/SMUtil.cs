using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceDefinitions;


namespace Matchnet.Member.ServiceAdapters
{
	internal class SMUtil
	{
		private SMUtil()
		{
		}

        internal static IMemberAppDeviceService getAppDeviceService(string uri)
        {
            try
            {
                return (IMemberAppDeviceService)Activator.GetObject(typeof(IMemberAppDeviceService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

		internal static IMemberService getService(string uri)
		{
			try
			{
				return (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		internal static IMemberService getService(string uri, int memberID)
		{
			try
			{
				return (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		internal static string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		
		internal static string getServiceManagerUri(int memberID)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

	}
}
