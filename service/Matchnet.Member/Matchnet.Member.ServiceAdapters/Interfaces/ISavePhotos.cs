﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ValueObjects.Photos;

namespace Matchnet.Member.ServiceAdapters.Interfaces
{
    public interface ISavePhotos
    {
        bool SavePhotos(Int32 brandID, Int32 siteID, Int32 communityID, Int32 memberID, PhotoUpdate[] photoUpdates);
    }
}
