﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Common.AccessService;

namespace Matchnet.Member.ServiceAdapters.Interfaces
{
    public interface IMember
    {
        Photo GetDefaultPhoto(Int32 communityID, bool publicOnly);
        Photo GetRandomApprovedForMainPhoto(int communityId, bool publicOnly);
        List<Photo> GetApprovedForMainPhotos(int communityId, bool publicOnly);
        List<Photo> GetApprovedPhotos(int communityId, bool publicOnly);
        List<Photo> GetApprovedPhotosExcludingMain(int communityId, bool publicOnly);
        DateTime GetAttributeDate(Brand brand, string attributeName);
        DateTime GetAttributeDate(Brand brand, string attributeName,DateTime defaultValue);
        Int32 GetAttributeInt(Brand brand, string attributeName);
        Int32 GetAttributeInt(Brand brand, string attributeName, Int32 defaultValue);
        bool GetAttributeBool(Brand brand, string attributeName);
        string GetAttributeText(Brand brand, string attributeName);
        void SetAttributeDate(Brand brand, string attributeName, DateTime value);
        void SetAttributeInt(Brand brand, string attributeName, Int32 value);
        void SetAttributeText(Brand brand, string attributeName, string value, TextStatusType textStatus);
        AccessPrivilege GetUnifiedAccessPrivilege(PrivilegeType privilegeType, int brandID, int siteID, int communityID);
        PhotoCommunity GetPhotos(Int32 communityID);
        int MemberID { get; }
        MemberUpdate MemberUpdate { get; }
        void CommitChanges();
        string GetUserName(Brand brand);
        bool IsPayingMember(Int32 siteID);
        IMemberDTO GetIMemberDTO();
    }
}
