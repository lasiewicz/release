﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ValueObjects.Admin;
using System.Collections;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;

namespace Matchnet.Member.ServiceAdapters.Interfaces
{
    public interface IGetMember
    {
        int GetMemberIDByEmail(string emailAddress);
        int ValidateResetPasswordToken(string tokenGuidString);
        Member GetMember(int memberID, MemberLoadFlags memberLoadFlags);
        IMember GetIMember(int memberID, MemberLoadFlags memberLoadFlags);
        ArrayList GetMembers(ArrayList memberIDs, MemberLoadFlags memberLoadFlags);
        ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID);
        IMemberDTO GetMemberDTO(int memberID, MemberType type);
        ArrayList GetMemberDTOs(ArrayList memberIDs, MemberType type);
    }
}
