﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Member.ServiceAdapters.Interfaces
{
    public interface ISaveMember
    {
        MemberSaveResult SaveMember(IMember member);
        MemberSaveResult SaveMember(IMember member, int communityID);
        string ResetPassword(int memberId);
        string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId);
        void ExpireImpersonationToken(int adminMemberId);
    }
}
