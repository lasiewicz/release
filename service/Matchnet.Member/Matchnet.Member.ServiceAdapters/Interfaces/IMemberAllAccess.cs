using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.Member.ServiceAdapters
{
    /// <summary>
    ///     Adapter for All Access and Get/Set Member
    /// </summary>
    public interface IMemberAllAccess : IGetMember, ISaveMember
    {
        ///
        bool AdjustUnifiedAccessCountPrivilege(int memberID, int adminID, string adminDomainUserName,
            int[] privilegeTypeID, int brandID, int siteID, int communityID, int count);
    }
}