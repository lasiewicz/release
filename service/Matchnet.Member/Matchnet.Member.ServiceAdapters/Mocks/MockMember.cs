﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Spark.Common.AccessService;

namespace Matchnet.Member.ServiceAdapters.Mocks
{
    public class MockMember : IMember
    {
        private Dictionary<string, DateTime> _dateDictionary;
        private Dictionary<string, Int32> _intDictionary;
        private Dictionary<string, bool> _boolDictionary;
        private Dictionary<string, string> _stringDictionary;
        private PhotoCommunity _photoCommunity;
        private string _userName = string.Empty;
        private bool _isPayingMember = false;

        private PhotoCollection _photoCollection;
        private List<AccessPrivilege> _AccessPrivilegeList;
        private Dictionary<int, Dictionary<int, TextValue>> _attributesText;
        private Dictionary<int, DateTime> _attributesDate;
        private Dictionary<int, int> _attributesInt;

        public void SetIsPayingMember(bool paying)
        {
            _isPayingMember = paying;
        }

        public MockMember()
        {
            _dateDictionary = new Dictionary<string, DateTime>();
            _intDictionary = new Dictionary<string, int>();
            _boolDictionary = new Dictionary<string, bool>();
            _stringDictionary = new Dictionary<string, string>();
            _photoCommunity = new PhotoCommunity();
        }

        public bool IsPayingMember(Int32 siteID)
        {
            return _isPayingMember;
        }

        public int MemberID { get; set; }
        public MemberUpdate MemberUpdate { get; private set; }
        public void CommitChanges()
        {

        }

        public string GetUserName(Brand brand)
        {
            return _userName;
        }

        public void SetUserName(string userName)
        {
            _userName = userName;
        }

        public void SetAttributeInt(string attributeName, Int32 value)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                _intDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _intDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeInt(Brand brand, string attributeName, Int32 value)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                _intDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _intDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeDate(string attributeName, DateTime value)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                _dateDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _dateDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeDate(Brand brand, string attributeName, DateTime value)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                _dateDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _dateDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeBool(string attributeName, bool value)
        {
            if (_boolDictionary.ContainsKey(attributeName.ToLower()))
            {
                _boolDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _boolDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeText(string attributeName, string value)
        {

            if (_stringDictionary.ContainsKey(attributeName.ToLower()))
            {
                _stringDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _stringDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void SetAttributeText(Brand brand, string attributeName, string value, TextStatusType textStatusType)
        {

            if (_stringDictionary.ContainsKey(attributeName.ToLower()))
            {
                _stringDictionary[attributeName.ToLower()] = value;
            }
            else
            {
                _stringDictionary.Add(attributeName.ToLower(), value);
            }
        }

        public void AddPhoto(Photo photo)
        {
            _photoCommunity[(byte)_photoCommunity.Count] = photo;
        }

        public DateTime GetAttributeDate(Brand brand, string attributeName)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _dateDictionary[attributeName.ToLower()];
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        public DateTime GetAttributeDate(Brand brand, string attributeName, DateTime defaultValue)
        {
            if (_dateDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _dateDictionary[attributeName.ToLower()];
            }
            else
            {
                return defaultValue;
            }
        }

        public Int32 GetAttributeInt(Brand brand, string attributeName)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _intDictionary[attributeName.ToLower()];
            }
            else
            {
                return Matchnet.Constants.NULL_INT;
            }
        }

        public Int32 GetAttributeInt(Brand brand, string attributeName, Int32 defaultValue)
        {
            if (_intDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _intDictionary[attributeName.ToLower()];
            }
            else
            {
                return defaultValue;
            }
        }

        public bool GetAttributeBool(Brand brand, string attributeName)
        {
            if (_boolDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _boolDictionary[attributeName.ToLower()];
            }
            else
            {
                return false;
            }
        }

        public string GetAttributeText(Brand brand, string attributeName)
        {
            if (_stringDictionary.ContainsKey(attributeName.ToLower()))
            {
                return _stringDictionary[attributeName.ToLower()];
            }
            else
            {
                return string.Empty;
            }
        }

        public PhotoCommunity GetPhotos(Int32 communityID)
        {
            return _photoCommunity;
        }

        public List<Photo> GetApprovedForMainPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        public List<Photo> GetApprovedPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        public List<Photo> GetApprovedPhotosExcludingMain(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (!photos[i].IsApproved || photos[i].IsMain) continue;

                if (publicOnly)
                {
                    if (!photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    approvedPhotos.Add(photos[i]);
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        public Photo GetDefaultPhoto(int communityID, bool publicOnly)
        {
            Photo retVal = null;

            var photos = GetApprovedForMainPhotos(communityID, publicOnly);
            if (photos == null)
                return null;

            // photo priority in this order; photo marked as IsMain, 1st photo approved for main and public, 1st photo approved for main but private
            var query = from p in photos
                        where p.IsMain
                        orderby p.ListOrder
                        select p;

            retVal = query.FirstOrDefault();
            if (retVal != null)
                return retVal;

            // no photo was marked as main explicitly, look for another candidate
            foreach (Photo photo in photos)
            {
                if (photo.IsPrivate)
                {
                    retVal = photo;
                }
                else
                {
                    retVal = photo;
                    break;
                }
            }

            return retVal;
        }

        public Photo GetRandomApprovedForMainPhoto(int communityId, bool publicOnly)
        {
            var photos = GetApprovedForMainPhotos(communityId, publicOnly);
            if (photos == null || photos.Count == 0)
                return null;

            var random = new Random();
            return photos[random.Next(0, photos.Count - 1)];
        }


        public string EmailAddress { get; set; }
        public PhotoCollection PhotoCollection
        {
            get { return _photoCollection; }
            set { _photoCollection = value; }
        }

        public void SetAttributeInt(Int32 attributeGroupID, Int32 integer)
        {
            if (null == _attributesInt) _attributesInt = new Dictionary<int, int>();
            if (_attributesInt.ContainsKey(attributeGroupID))
            {
                _attributesInt[attributeGroupID] = integer;
            }
            else
            {
                _attributesInt.Add(attributeGroupID, integer);
            }
        }

        public void SetAttributeDate(Int32 attributeGroupID, DateTime date)
        {
            if (null == _attributesDate) _attributesDate = new Dictionary<int, DateTime>();
            if (_attributesDate.ContainsKey(attributeGroupID))
            {
                if (date != DateTime.MinValue)
                {
                    _attributesDate[attributeGroupID] = date;
                }
                else
                {
                    _attributesDate.Remove(attributeGroupID);
                }
            }
            else if (date != DateTime.MinValue)
            {
                _attributesDate.Add(attributeGroupID, date);
            }
        }

        public void SetAttributeText(Int32 attributeGroupID, Int32 languageID, string text, TextStatusType textStatus)
        {
            if (null == _attributesText) _attributesText = new Dictionary<int, Dictionary<int, TextValue>>();
            Dictionary<int, TextValue> attributes=null;
            if (_attributesText.ContainsKey(languageID)) attributes = _attributesText[languageID];

            if (attributes == null)
            {
                attributes = new Dictionary<int, TextValue>();
                _attributesText.Add(languageID, attributes);
            }

            TextValue textValue = null;
            if(attributes.ContainsKey(attributeGroupID)) textValue = attributes[attributeGroupID];

            if (textValue != null)
            {
                textValue.Text = text;
                textValue.TextStatus = textStatus;
            }
            else
            {
                attributes.Add(attributeGroupID, new TextValue(text, textStatus));
            }
        }

        public void AddUnifiedAccessPrivilege(AccessPrivilege privilege)
        {
            if(null == _AccessPrivilegeList) _AccessPrivilegeList = new List<AccessPrivilege>();
            if (!_AccessPrivilegeList.Contains(privilege))
            {
                _AccessPrivilegeList.Add(privilege);
            }
        }

        public AccessPrivilege GetUnifiedAccessPrivilege(PrivilegeType privilegeType, int brandID, int siteID, int communityID)
        {
            AccessPrivilege accessPrivilege = null;
            if (_AccessPrivilegeList != null)
            {
                foreach (AccessPrivilege ap in _AccessPrivilegeList)
                {
                    if (ap.CallingSystemID == siteID && ap.UnifiedPrivilegeType == privilegeType)
                    {
                        accessPrivilege = ap;
                        break;
                    }
                }
            }
            return accessPrivilege;
        }

//        //TEXT ATTR GROUP IDS
//        private static int[] ATTR_GRP_GLOBALSTATUSMASK = new int[] { 445 };
//        private static int[] ATTR_GRP_ABOUTME = new int[] {44,92,140,188,284,332,380,1467,1913,2032,3043,4047,5343,1727};
//        private static int[] ATTR_GRP_USERNAME = new int[] {2485,2513,2486,2514,2487,2515,2516,2517,2518,2519,3081,4087,5374,438};
//        private static int[] ATTR_GRP_COLORCODEPRIMARYCOLOR = new int[] {1823,1824,1880,1889,1926,2045,3066,4072,5359};
//        private static int[] ATTR_GRP_COLORCODEHIDDEN = new int[] {1837,1838,1920,2039,3060,4066,5353 };
//
//        //INT ATTR GROUP IDS
//        private static int[] ATTR_GRP_GENDERMASK = new int[] { 46, 94, 142, 190, 286, 334, 382, 1469, 1940, 2059, 3080, 4086, 5373, 1707 };
//        private static int[] ATTR_GRP_HIGHLIGHTEDFLAG = new int[] { 1287, 1288, 1292, 1593, 1289, 1268, 1290, 1291, 3365, 1566, 3115, 4134, 2448, 2100, 1981, 1512, 5449 };
//        private static int[] ATTR_GRP_REGIONID = new int[] { 481 };
//        private static int[] ATTR_GRP_HIDEMASK = new int[] {10,58,106,154,250,298,346,1433,1904,2023,3020,4022,2701,5332,1691};
//        private static int[] ATTR_GRP_ENTERTAINMENTLOCATION = new int[] { 487 };
//        private static int[] ATTR_GRP_LEISUREACTIVITY = new int[] { 485 };
//        private static int[] ATTR_GRP_CUISINE = new int[] { 490 };
//        private static int[] ATTR_GRP_MUSIC = new int[] { 507 };
//        private static int[] ATTR_GRP_READING = new int[] { 471 };
//        private static int[] ATTR_GRP_PHYSICALACTIVITY = new int[] { 486 };
//        private static int[] ATTR_GRP_PETS = new int[] { 446 };
//
//        //DATE ATTR GROUP IDS
//        private static int[] ATTR_GRP_BIRTHDATE = new int[] { 510 };
//        private static int[] ATTR_GRP_BRANDINSERTDATE = new int[] {674,675,676,677,679,3398,683,684,728,1604,1146,1202,685,851,689,690,691,692,693,695,696,697,698,699,700,701,702,703,704,705,706,707,3145,4155,2463,2115,1996,1518,5451};
//        private static int[] ATTR_GRP_LASTUPDATED = new int[] {841,842,843,844,846,847,848,1477,1914,2033,3038,4042,5344,1744};
//        private static int[] ATTR_GRP_PREMIUMAUTHENTICATEDEXPIRATIONDATE = new int[] {1270,1271,1276,1590,1272,1273,1274,1275,3391,3138,4148,2459,2111,1992,1503,5422};
//        private static int[] ATTR_GRP_BRANDLASTLOGONDATE = new int[] {602,603,604,605,607,3400,611,612,726,1603,1144,1200,613,849,617,618,619,620,621,623,624,625,626,627,628,629,630,631,632,633,634,635,3147,4157,2465,2117,1998,1516,5453 };
//
//        public static MockMember Parse(CachedMember cachedMember, CachedMemberAccess cachedMemberAccess, MemberType type)
//        {
//            MockMember mockMember = new MockMember(cachedMember.MemberID, MemberType.Search);
//            mockMember.PhotoCollection = cachedMember.Photos;
//            
//            if (type == MemberType.Search)
//            {
//                mockMember.EmailAddress = cachedMember.EmailAddress;
//                if (null != cachedMemberAccess)
//                {
//                    foreach (AccessPrivilege accessPrivilege in cachedMemberAccess.AccessPrivilegeList)
//                    {
//                        if (accessPrivilege.UnifiedPrivilegeType == PrivilegeType.HighlightedProfile
//                            || accessPrivilege.UnifiedPrivilegeType == PrivilegeType.BasicSubscription)
//                        {
//                            mockMember.AddUnifiedAccessPrivilege(accessPrivilege);
//                        }
//                    }
//                }
//
//                //set up text values
//                foreach (int languageID in cachedMember.AttributesText.Keys)
//                {
////                    if (2 == languageID)
////                    {
//                        foreach (int attributeGroupID in ((Hashtable) cachedMember.AttributesText[languageID]).Keys)
//                        {
//                            if (
//                                ATTR_GRP_USERNAME.Contains(attributeGroupID)
//                                                            || ATTR_GRP_ABOUTME.Contains(attributeGroupID)
//                                                             || ATTR_GRP_COLORCODEPRIMARYCOLOR.Contains(attributeGroupID)
//                                                            || ATTR_GRP_COLORCODEHIDDEN.Contains(attributeGroupID)
//                                )
//                            {
//                                TextValue tv =
//                                    ((Hashtable) cachedMember.AttributesText[languageID])[attributeGroupID] as TextValue;
//                                mockMember.SetAttributeText(attributeGroupID, languageID, tv.Text, tv.TextStatus);
////                                Console.WriteLine("Value = "+tv.Text+", MemberId = "+ cachedMember.MemberID);
//                            }
//                        }
////                    }
//                }
//
//                //set up int values
//                foreach (int attributeGroupID in cachedMember.AttributesInt.Keys)
//                {
//                    if (ATTR_GRP_GLOBALSTATUSMASK.Contains(attributeGroupID)
//                        || ATTR_GRP_GENDERMASK.Contains(attributeGroupID)
//                        || ATTR_GRP_HIGHLIGHTEDFLAG.Contains(attributeGroupID)
//                        || ATTR_GRP_REGIONID.Contains(attributeGroupID)
//                        || ATTR_GRP_HIDEMASK.Contains(attributeGroupID)
//                        || ATTR_GRP_ENTERTAINMENTLOCATION.Contains(attributeGroupID)
//                        || ATTR_GRP_LEISUREACTIVITY.Contains(attributeGroupID)
//                        || ATTR_GRP_CUISINE.Contains(attributeGroupID)
//                        || ATTR_GRP_MUSIC.Contains(attributeGroupID)
//                        || ATTR_GRP_READING.Contains(attributeGroupID)
//                        || ATTR_GRP_PHYSICALACTIVITY.Contains(attributeGroupID)
//                        || ATTR_GRP_PETS.Contains(attributeGroupID))
//                    {
//                        mockMember.SetAttributeInt(attributeGroupID, (int)cachedMember.AttributesInt[attributeGroupID]);
//                    }                    
//                }
//
//                //set up date values
//                foreach (int attributeGroupID in cachedMember.AttributesDate.Keys)
//                {
//                    if (ATTR_GRP_BIRTHDATE.Contains(attributeGroupID)
//                        || ATTR_GRP_BRANDINSERTDATE.Contains(attributeGroupID)
//                        || ATTR_GRP_LASTUPDATED.Contains(attributeGroupID)
//                        || ATTR_GRP_PREMIUMAUTHENTICATEDEXPIRATIONDATE.Contains(attributeGroupID)
//                        || ATTR_GRP_BRANDLASTLOGONDATE.Contains(attributeGroupID))
//                    {
//                        mockMember.SetAttributeDate(attributeGroupID, (DateTime)cachedMember.AttributesDate[attributeGroupID]);
//                    }
//                }
//            }
//            return mockMember;
//        }


        public bool HasApprovedPhoto(int communityID)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, int attributeID, string defaultValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName, string defaultValue)
        {
            throw new NotImplementedException();
        }

        public bool IsUpdatedMember(int communityID)
        {
            throw new NotImplementedException();
        }

        public bool IsNewMember(int communityID)
        {
            throw new NotImplementedException();
        }

        public bool IsNewMember(int communityID, int days)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogonDate(int communityID)
        {
            throw new NotImplementedException();
        }

        public DateTime GetLastLogonDate(int communityID, out int brandID)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, string attributeName, string unapprovedResource)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, string attributeName, int viewerMemberID, string unapprovedResourceValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, string[] attributeNames, int viewerMemberID, string unapprovedResourceValue)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeTextApproved(Brand brand, int languageID, string attributeName, string defaultValue, string unapprovedResource)
        {
            throw new NotImplementedException();
        }

        public string GetAttributeText(int communityID, int siteID, int brandID, int languageID, string attributeName, string defaultValue, out TextStatusType textStatus)
        {
            throw new NotImplementedException();
        }

        public DateTime GetAttributeDate(int communityID, int siteID, int brandID, string attributeName, DateTime defaultValue)
        {
            throw new NotImplementedException();
        }


        #region IMember Members


        public ValueObjects.Interfaces.IMemberDTO GetIMemberDTO()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
