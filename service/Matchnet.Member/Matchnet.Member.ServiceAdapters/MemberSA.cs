using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.CacheSynchronization.Context;
using Matchnet.File.ServiceAdapters;
using System.Collections.Generic;
using Spark.Common.Adapter;
using Matchnet.File.ValueObjects;
using Spark.Common.Fraud;
using Spark.Logging;
using Matchnet.Member.ValueObjects.Enumerations;
using log4net;

namespace Matchnet.Member.ServiceAdapters
{
    /// <summary>
    /// 
    /// </summary>
    [Flags]
    public enum MemberLoadFlags
    {
        /// <summary>
        /// 
        /// </summary>
        None,

        /// <summary>
        /// 
        /// </summary>
        IngoreSACache
    }

    /// <summary>
    /// The ServiceAdapter implementation that provides access to
    /// the persisted Member.
    /// </summary>
    public class MemberSA : SABase, ISavePhotos, IMemberAppDeviceService, IMemberAllAccess
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly MemberSA Instance = new MemberSA();

        public ICaching _cache;
        public ICaching _membaseCache;
        public ICaching _membaseDTOCache;
        public const string ATTRIBUTE_NAME_PROMOTIONID = "PromotionID";
        public const string ATTRIBUTE_NAME_LANDINGPAGEID = "LandingPageID";
        public const string ATTRIBUTE_NAME_BANNERID = "BannerID";
        public const int CACHED_MEMBER_VERSION = 5;
        public const int CACHED_MEMBER_LOGON_VERSION = 1;
        public const int CACHED_MEMBER_EMAIL_VERSION = 1;
        private const int JDATE_COMMUNITY_ID = 3;

        private bool _nunit = false;

        public bool NUnit
        {
            get { return _nunit; }
            set { _nunit = value; }
        }

        private MembaseConfig _membaseConfig;
        private MembaseConfig _membaseDTOConfig;
        private static ISettingsSA _settingsService = null;
        private static readonly ILog Log = LogManager.GetLogger(typeof (MemberSA));
        private const int PROFILE_VIEWED_EMAIL_OPT_OUT = 16;


        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public bool UseMembaseForSA
        {
            get
            {
                try
                {
                    string settingFromSingleton =
                        SettingsService.GetSettingFromSingleton(
                            ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT);
                    return Convert.ToBoolean(settingFromSingleton);
                }
                catch (Exception e)
                {
                    LogException(ValueObjects.ServiceConstants.MEMBASE_SA_SETTING_CONSTANT, e, Constants.NULL_INT);
                }
                return false;
            }
        }

        public bool EnableMemberDTO
        {
            get
            {
                try
                {
                    string settingFromSingleton =
                        SettingsService.GetSettingFromSingleton(ValueObjects.ServiceConstants.ENABLE_MEMBER_DTO_CONSTANT);
                    return Convert.ToBoolean(settingFromSingleton);
                }
                catch (Exception e)
                {
                    LogException(ValueObjects.ServiceConstants.ENABLE_MEMBER_DTO_CONSTANT, e, Constants.NULL_INT);
                }
                return false;
            }
        }

        public bool UseAuthenticateMethodWithoutBrand
        {
            get
            {
                try
                {
                    string settingFromSingleton =
                        SettingsService.GetSettingFromSingleton(
                            ValueObjects.ServiceConstants.USE_AUTHENTICATE_SAMETHOD_WITHOUT_BRAND);
                    return Convert.ToBoolean(settingFromSingleton);
                }
                catch (Exception e)
                {
                    LogException(ValueObjects.ServiceConstants.USE_AUTHENTICATE_SAMETHOD_WITHOUT_BRAND, e,
                        Constants.NULL_INT);
                }
                return false;
            }
        }


        public ICaching ICache
        {
            get
            {
                if (UseMembaseForSA)
                {
                    return _membaseCache;
                }
                return _cache;
            }
        }

        public ICaching MemberDTOCache
        {
            get { return _membaseDTOCache; }
        }

        private MemberSA()
        {
            _membaseConfig =
                SettingsService.GetMembaseConfigByBucketFromSingleton(
                    ValueObjects.ServiceConstants.MEMBASE_WEB_BUCKET_NAME);
            _membaseCache = Spark.Caching.MembaseCaching.GetInstance(_membaseConfig);

            _membaseDTOConfig =
                SettingsService.GetMembaseConfigByBucketFromSingleton(
                    ValueObjects.ServiceConstants.MEMBASE_MEMBERDTO_BUCKET_NAME);
            _membaseDTOCache = Spark.Caching.MembaseCaching.GetInstance(_membaseDTOConfig);

            _cache = Matchnet.Caching.Cache.Instance;
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections =
                Convert.ToByte(SettingsService.GetSettingFromSingleton("MEMBERSVC_SA_CONNECTION_LIMIT"));
        }

        #region Messmo

        public void SendMessmoUserTransactionStatus(int memberID, int siteID, DateTime newSubExpDate)
        {
            string uri = string.Empty;

            try
            {
                int ret = Constants.NULL_INT;
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    SMUtil.getService(uri).SendMessmoUserTransactionStatus(memberID, siteID, newSubExpDate);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred in SendMessmoUserTransactionStatus. (uri: " + uri + ")", ex);
            }
        }

        public int GetMemberIDByMessmoID(string messmoID, int communityID)
        {
            string uri = string.Empty;

            try
            {
                int ret = Constants.NULL_INT;
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    ret = SMUtil.getService(uri).GetMemberIDByMessmoID(messmoID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return ret;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred in GetMemberIDByMessmodID. (uri: " + uri + ")", ex);
            }
        }

        public void ToggleLogonDisabledFlag(int memberID, int activeFlag)
        {
            string uri = string.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    SMUtil.getService(uri).ToggleLogonDisabledFlag(memberID, activeFlag);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error occured while toggling logon disabled flag. (uri: " + uri + ")", ex);
            }
        }

        public void SaveLogonMessmo(string messmoID, int memberID, int communityID)
        {
            string uri = string.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    SMUtil.getService(uri).SaveLogonMessmo(messmoID, memberID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred in SaveLogonMessmo. (uri: " + uri + ")", ex);
            }
        }

        public void SubscribeToMessmoDailyFeed(int memberID, int communityID)
        {
            string uri = string.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    SMUtil.getService(uri).SubscribeToMessmoDailyFeed(memberID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred in SubscribeToMessmoDailyFeed. (uri: " + uri + ")", ex);
            }
        }

        public void UnsubscribeFromMessmoDailyFeed(int memberID, int communityID)
        {
            string uri = string.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    SMUtil.getService(uri).UnsubscribeFromMessmoDailyFeed(memberID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred in UnsubscribeFromMessmoDailyFeed. (uri: " + uri + ")", ex);
            }
        }

        #endregion

        #region logon

        /// <summary>
        /// DEPRECATED:  Kept for backwards compatibility.
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <param name="promotionID"></param>
        /// <returns></returns>
        public AuthenticationResult Authenticate(Brand brand, string emailAddress, string password, Int32 promotionID)
        {
            Hashtable trackingVals = new Hashtable(1);

            trackingVals.Add(ATTRIBUTE_NAME_PROMOTIONID, promotionID);

            return Authenticate(brand, emailAddress, password, Constants.NULL_STRING, trackingVals);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <param name="trackingVals">A hashtable containing values such as PromotionID, LandingPageID, and BannerID.</param>
        /// <param name="logLastLogon"></param>
        /// <returns></returns>
        public AuthenticationResult Authenticate(Brand brand, string emailAddress, string password, string passwordHash,
            Hashtable trackingVals, bool logLastLogon = true)
        {
            const int ADMIN_SUSPENDED = 1;
            int returnValue = Constants.NULL_INT;
            int memberID = Constants.NULL_INT;
            string userName = Constants.NULL_STRING;
            string uri = "";


            Stopwatch stopwatch = null;
            long lastElapsedMilliseconds = 0;
            long currentElapsedMilliseconds = 0;

            try
            {
                var logTimingData =
                    Conversion.CBool(RuntimeSettings.GetSetting(SettingConstants.LOG_LOGIN_TIMING_DATA,
                        brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));

                if (logTimingData)
                {
                    stopwatch = new Stopwatch();
                    stopwatch.Start();
                }

                try
                {
                    int communityId = brand.Site.Community.CommunityID;
                    uri = SMUtil.getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        passwordHash = GetPasswordForAuth(emailAddress, communityId, password, passwordHash,
                            out returnValue, uri);

                        if (logTimingData)
                        {
                            lastElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                            Log.DebugFormat(
                                "Member.Authenticate Timing EmailAddress: {0} BrandId: {1} Event: GotPasswordHash ElapsedMilliseconds: {2} ",
                                emailAddress, brand.BrandID, lastElapsedMilliseconds);
                        }

                        if (returnValue == Constants.NULL_INT)
                        {
                            returnValue = SMUtil.getService(uri)
                                .Auth(out memberID, communityId, emailAddress, passwordHash);

                            if (logTimingData)
                            {
                                currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                                Log.DebugFormat(
                                    "Member.Authenticate Timing EmailAddress: {0} BrandId: {1} Event: FinishedAuth TotalElapsedMilliseconds: {2} LastOpElapsedMilliseconds: {3}",
                                    emailAddress, brand.BrandID, currentElapsedMilliseconds,
                                    (currentElapsedMilliseconds - lastElapsedMilliseconds));
                                lastElapsedMilliseconds = currentElapsedMilliseconds;
                            }
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
                catch (Exception ex)
                {
                    throw (new SAException(
                        "Error occurred while attempting to authenticate member. (uri: " + uri + ")", ex));
                }

                AuthenticationResult authenticationResult =
                    new AuthenticationResult((AuthenticationStatus) (int) returnValue, memberID, userName);

                uri = "";

                if (authenticationResult.Status == AuthenticationStatus.Authenticated)
                {
                    Member member = this.GetMember(memberID, MemberLoadFlags.None);

                    try
                    {
                        logon(brand, member, trackingVals, Constants.NULL_INT, logLastLogon);
                        if (logTimingData)
                        {
                            currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                            Log.DebugFormat(
                                "Member.Authenticate Timing EmailAddress: {0} BrandId: {1} Event: FinishedLogon TotalElapsedMilliseconds: {2} LastOpElapsedMilliseconds: {3}",
                                emailAddress, brand.BrandID, currentElapsedMilliseconds,
                                (currentElapsedMilliseconds - lastElapsedMilliseconds));
                            lastElapsedMilliseconds = currentElapsedMilliseconds;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw (new SAException("Error occurred while attempting to log on member. (uri: " + uri + ")",
                            ex));
                    }

                    try
                    {
                        if ((member.GetAttributeInt(brand, "GlobalStatusMask") & ADMIN_SUSPENDED) == ADMIN_SUSPENDED)
                        {
                            authenticationResult = new AuthenticationResult(AuthenticationStatus.AdminSuspended,
                                authenticationResult.MemberID, authenticationResult.UserName);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw (new SAException(
                            "Error occurred while attempting to check member adminsuspend status. (uri: " + uri + ")",
                            ex));
                    }
                }

                return authenticationResult;
            }
            finally
            {
                if (stopwatch != null) stopwatch.Stop();
            }
        }

        private string GetPasswordForAuth(string emailAddress, int communityId, string password, string passwordHash,
            out int returnValue, string uri)
        {
            emailAddress = emailAddress.Trim();
            returnValue = Constants.NULL_INT;
            var encryptedEmail = string.Empty;

            Stopwatch stopwatch = null;
            long lastElapsedMilliseconds = 0;
            long currentElapsedMilliseconds = 0;

            try
            {
                var logTimingData = Conversion.CBool(RuntimeSettings.GetSetting(SettingConstants.LOG_LOGIN_TIMING_DATA));

                if (logTimingData)
                {
                    stopwatch = new Stopwatch();
                    stopwatch.Start();
                }

                if (string.IsNullOrEmpty(passwordHash))
                {
                    //throw exception if email address is null
                    if (string.IsNullOrEmpty(emailAddress))
                    {
                        returnValue = (int) AuthenticationStatus.InvalidEmailAddress;
                        throw new Exception("Email address is null!");
                    }
                    encryptedEmail = EncryptEmail(emailAddress);

                    if (logTimingData)
                    {
                        lastElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                        Log.DebugFormat(
                            "Member.GetPasswordForAuth Timing EmailAddress: {0} Event: EncryptedEmail ElapsedMilliseconds: {1} ",
                            emailAddress, lastElapsedMilliseconds);
                    }

                    string salt = SMUtil.getService(uri).GetMemberSalt(encryptedEmail, communityId);

                    if (logTimingData)
                    {
                        currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                        Log.DebugFormat(
                            "Member.GetPasswordForAuth Timing EmailAddress: {0} Event: GotSalt TotalElapsedMilliseconds: {1} LastOpElapsedMilliseconds: {2}",
                            emailAddress, currentElapsedMilliseconds,
                            (currentElapsedMilliseconds - lastElapsedMilliseconds));
                        lastElapsedMilliseconds = currentElapsedMilliseconds;
                    }


                    if (string.IsNullOrEmpty(salt))
                    {
                        returnValue = (int) AuthenticationStatus.InvalidEmailAddress;
                    }
                    else if (string.IsNullOrEmpty(password))
                    {
                        returnValue = (int) AuthenticationStatus.InvalidPassword;
                    }
                    else
                    {
                        passwordHash = Security.Crypto.Instance.EncryptText(password, salt);

                        if (logTimingData)
                        {
                            currentElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
                            Log.DebugFormat(
                                "Member.GetPasswordForAuth Timing EmailAddress: {0} Event: EncryptedPasswordHash TotalElapsedMilliseconds: {1} LastOpElapsedMilliseconds: {2}",
                                emailAddress, currentElapsedMilliseconds,
                                (currentElapsedMilliseconds - lastElapsedMilliseconds));
                            lastElapsedMilliseconds = currentElapsedMilliseconds;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string clientIp = string.Empty;
                if (null != HttpContext.Current.Request)
                {
                    clientIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_CONSTANT,
                    "MemberSA.GetPasswordForAuth()",
                    string.Format(
                        "GetPasswordForAuth(), Email:{0}, EmailHash:{1}, PassHash:{2}, ClientIp:{3}, CommunityId:{4}",
                        emailAddress, encryptedEmail, passwordHash, clientIp, communityId), ex, null);
            }
            finally
            {
                if (stopwatch != null) stopwatch.Stop();
            }
            return passwordHash;
        }

        public string GetPasswordForAuth(string emailAddress, int communityId, string password)
        {
            string passwordHash = null;
            string uri = "";
            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    int returnValue = 0;
                    passwordHash = GetPasswordForAuth(emailAddress, communityId, password, string.Empty, out returnValue,
                        uri);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + uri + ")", ex));
            }
            return passwordHash;
        }

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public AuthenticationResult Authenticate(Brand brand, string emailAddress, string password)
        {
            emailAddress = emailAddress.Trim();
            password = password.Trim();
            return Authenticate(brand, emailAddress, password, Constants.NULL_INT);
        }

        /// <summary>
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <returns></returns>        
        public AuthenticationResult Authenticate(Brand brand, string emailAddress, string password, string passwordHash)
        {
            return Authenticate(brand, emailAddress, password, passwordHash, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"></param>
        /// <param name="logLastLogon"></param>
        /// <returns></returns>
        public AuthenticationResult Authenticate(Brand brand, string emailAddress, string password, string passwordHash,
            bool logLastLogon)
        {
            return Authenticate(brand, emailAddress, password, passwordHash, null, logLastLogon);
        }

        public bool VerifyPassword(string email, int communityId, string password)
        {
            int returnValue = Constants.NULL_INT;
            int memberID = Constants.NULL_INT;
            string uri = "";
            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    string passwordHash = GetPasswordForAuth(email, communityId, password);
                    if (returnValue == Constants.NULL_INT)
                    {
                        returnValue = SMUtil.getService(uri).Auth(out memberID, communityId, email, passwordHash);
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + uri + ")", ex));
            }
            return (returnValue == 0);
        }

        //This method does an auth and resturns an auth result but doesn't do any of the post-auth stuff like updating
        //brandlastlogon, logoncount, etc. 
        public AuthenticationResult BasicAuthenticate(string email, string password, int communityId)
        {
            int returnValue = Constants.NULL_INT;
            int memberID = Constants.NULL_INT;
            string uri = "";
            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    string passwordHash = GetPasswordForAuth(email, communityId, password);
                    if (returnValue == Constants.NULL_INT)
                    {
                        returnValue = SMUtil.getService(uri).Auth(out memberID, communityId, email, passwordHash);
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to do a basic authentication. (uri: " + uri + ")", ex));
            }

            return new AuthenticationResult((AuthenticationStatus) (int) returnValue, memberID, null);
        }


        public MemberRegisterResult Register(Brand brand,
            string emailAddress,
            string username,
            string password,
            Int32 ipAddress)
        {
            return Register(brand, emailAddress, username, password, ipAddress, 0);
        }

        public MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetMemberBasicLogonInfo(memberId, communityId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to get member member basic logon info (uri: " + uri + ").", ex));
            }
        }

        public bool ChangePasswordToRandomString(int memberId, int communityId)
        {
            var randomString = Path.GetRandomFileName().Replace(".", "");

            return SavePassword(memberId, communityId, randomString);
        }

        public bool SavePassword(int memberId, int communityId, string password)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    var passwordHash = EncryptPassword(password);
                    return SMUtil.getService(uri).SavePassword(memberId, communityId, passwordHash);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to save member's password (uri: " + uri + ").",
                    ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public MemberRegisterResult Register(Brand brand,
            string emailAddress,
            string username,
            string password,
            Int32 ipAddress,
            Int32 siteID)
        {
            string uri = "";
            MemberRegisterResult memberRegisterResult;

            try
            {
                string ipAddressString = new System.Net.IPAddress(BitConverter.GetBytes(ipAddress)).ToString();
                bool allow = IPBlockerServiceAdapter.Instance.CheckIPAccess(ipAddressString, siteID,
                    Spark.Common.IPBlockerAccessType.Registration);

                if (!allow)
                {
                    memberRegisterResult = new MemberRegisterResult(RegisterStatusType.IPBlocked, 0, username);
                    return memberRegisterResult;
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to connect to IPBlocker. (uri: " + uri + ")", ex));
            }

            try
            {

                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    string encryptPassword = EncryptPassword(password);
                    memberRegisterResult = SMUtil.getService(uri)
                        .Register(emailAddress.ToLower(), username, password, encryptPassword, brand);
                }
                finally
                {
                    base.Checkin(uri);
                }

                if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
                {
                    Member member = GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);
                    member.EmailAddress = emailAddress;
                    member.SetUsername(memberRegisterResult.Username, brand);
                    //member.Username = memberRegisterResult.Username;

                    //moved most of logon method into here to ensure important member attrs are saved on registration success - 7/16/2012 arod
                    if (ipAddress != Constants.NULL_INT)
                    {
                        member.SetAttributeInt(brand, "RegistrationIP", ipAddress);
                    }

                    DateTime now = DateTime.Now;
                    member.SetAttributeDate(brand, "BrandInsertDate", now);
                    member.SetAttributeDate(brand, "BrandLastLogonDateOldValue",
                        member.GetAttributeDate(brand, "BrandLastLogonDate"));
                    member.SetAttributeDate(brand, "BrandLastLogonDate", now);
                    member.SetAttributeInt(brand, "LastBrandID", brand.BrandID);
                    Int32 brandLogonCount = member.GetAttributeInt(brand, "BrandLogonCount", 0);
                    member.SetAttributeInt(brand, "BrandLogonCount", brandLogonCount + 1);

                    // photo requirement related logging
                    bool photoRequiredSite =
                        Convert.ToBoolean(SettingsService.GetSettingFromSingleton("PHOTO_REQUIRED_SITE",
                            brand.Site.Community.CommunityID,
                            brand.Site.SiteID));
                    if (photoRequiredSite)
                    {
                        DateTime rebrandLogonDate = member.GetAttributeDate(brand, "RebrandFirstLogonDate",
                            DateTime.MinValue);
                        if (rebrandLogonDate == DateTime.MinValue)
                        {
                            member.SetAttributeDate(brand, "RebrandFirstLogonDate", now);
                            // this attribute is immutable so it's safe to set it multiple times
                        }
                    }

                    // Let's flip the FTAApproved status to false here so that they have to go through the FTA approval check
                    member.SetAttributeInt(brand, "FTAApproved", 0);
                    logon(brand, member, ipAddress);
                }

                return memberRegisterResult;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to register new member. (uri: " + uri + ")", ex));
            }
        }

        public string EncryptPassword(string password)
        {
            string passwordHash = Constants.NULL_STRING;
            if (!string.IsNullOrEmpty(password))
            {
                int workFactor = 10;
                try
                {
                    string workFactorStr = SettingsService.GetSettingFromSingleton("CRYPTO_WORK_FACTOR");
                    workFactor = Convert.ToInt32(workFactorStr);
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberBL", e, null);
                    workFactor = 10;
                }
                passwordHash = Security.Crypto.Instance.EncryptTextPlusSalt(password, workFactor);
            }
            return passwordHash;
        }

        public string EncryptEmail(string emailAddress)
        {
            string encryptedEmail = Constants.NULL_STRING;
            if (!string.IsNullOrEmpty(emailAddress))
            {
                string key2 = SettingsService.GetSettingFromSingleton("KEY2");
                encryptedEmail = Security.Crypto.Instance.EncryptText(emailAddress.ToLower(), key2);
            }
            return encryptedEmail;
        }


        /// <summary>
        /// Public wrapper method for logon(). Used by the API.
        /// </summary>
        public void PostLogonUpdate(Brand brand, Member member, bool logLastLogon)
        {
            logon(brand, member, null, Constants.NULL_INT, logLastLogon);
        }

        /// <summary>
        /// Used by registration, authenticate and API authenticate
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="member"></param>
        /// <param name="trackingVals"></param>
        /// <param name="ipAddress"></param>
        /// <param name="logLastLogon"></param>
        private void logon(Brand brand,
            Member member,
            Hashtable trackingVals,
            Int32 ipAddress, bool logLastLogon = true)
        {

            if (ipAddress != Constants.NULL_INT)
            {
                member.SetAttributeInt(brand, "RegistrationIP", ipAddress);
            }

            Int32 brandLogonCount = member.GetAttributeInt(brand, "BrandLogonCount", 0);
            if (brandLogonCount < 1)
            {
                // This statement gets executed when this is a logon after a registration.
                // Let's flip the FTAApproved status to false here so that they have to go through the FTA approval check
                member.SetAttributeInt(brand, "FTAApproved", 0);
            }

            DateTime now = DateTime.Now;
            DateTime brandInsertDate = member.GetAttributeDate(brand, "BrandInsertDate", DateTime.MinValue);
            if (brandInsertDate == DateTime.MinValue) // set to know only when the date is null
            {
                member.SetAttributeDate(brand, "BrandInsertDate", now);
                    //this attribute is immutable so it is safe to attempt updates all day
            }
            member.SetAttributeDate(brand, "BrandLastLogonDateOldValue",
                member.GetAttributeDate(brand, "BrandLastLogonDate"));
            member.SetAttributeDate(brand, "BrandLastLogonDate", now);
            member.SetAttributeInt(brand, "LastBrandID", brand.BrandID);
            member.SetAttributeInt(brand, "BrandLogonCount", brandLogonCount + 1);

            // photo requirement related logging
            bool photoRequiredSite =
                Convert.ToBoolean(SettingsService.GetSettingFromSingleton("PHOTO_REQUIRED_SITE",
                    brand.Site.Community.CommunityID, brand.Site.SiteID));
            if (photoRequiredSite)
            {
                DateTime rebrandLogonDate = member.GetAttributeDate(brand, "RebrandFirstLogonDate", DateTime.MinValue);
                if (rebrandLogonDate == DateTime.MinValue)
                {
                    member.SetAttributeDate(brand, "RebrandFirstLogonDate", now);
                        // this attribute is immutable so it's safe to set it multiple times
                }
            }

            if (trackingVals != null)
            {
                Int32 trackingVal = Constants.NULL_INT;
                if (trackingVals.ContainsKey(ATTRIBUTE_NAME_PROMOTIONID))
                {
                    trackingVal = Convert.ToInt32(trackingVals[ATTRIBUTE_NAME_PROMOTIONID]);
                    if (trackingVal != Constants.NULL_INT)
                    {
                        member.SetAttributeInt(brand, ATTRIBUTE_NAME_PROMOTIONID, trackingVal);
                            // This attribute is immutable so we don't need to worry about overwritting it.
                    }
                }

                if (trackingVals.ContainsKey(ATTRIBUTE_NAME_LANDINGPAGEID))
                {
                    trackingVal = Convert.ToInt32(trackingVals[ATTRIBUTE_NAME_LANDINGPAGEID]);
                    if (trackingVal != Constants.NULL_INT)
                    {
                        member.SetAttributeInt(brand, ATTRIBUTE_NAME_LANDINGPAGEID, trackingVal);
                            // This attribute is immutable so we don't need to worry about overwritting it.
                    }
                }

                if (trackingVals.ContainsKey(ATTRIBUTE_NAME_BANNERID))
                {
                    trackingVal = Convert.ToInt32(trackingVals[ATTRIBUTE_NAME_BANNERID]);
                    if (trackingVal != Constants.NULL_INT)
                    {
                        member.SetAttributeInt(brand, ATTRIBUTE_NAME_BANNERID, trackingVal);
                            // This attribute is immutable so we don't need to worry about overwritting it.
                    }
                }
            }

            SaveMember(member, brand, true);
            if (logLastLogon)
                SaveMemberLastLogon(member.MemberID, brand.BrandID, Constants.NULL_INT);
        }


        /// <summary>
        /// Used by registration
        /// </summary>
        /// <param name="brand"></param>
        /// <param name="member"></param>
        /// <param name="ipAddress"></param>
        private void logon(Brand brand,
            Member member,
            Int32 ipAddress)
        {
            logon(brand, member, null, ipAddress);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public string GetPassword(int memberID)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetPassword(memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to get password for member (uri: " + uri + ", MemberID: " +
                    memberID + ").", ex));
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public string GetPassword(string emailAddress)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetPassword(emailAddress);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to get password for member (uri: " + uri + ", Email Address: " +
                    emailAddress + ").", ex));
            }
        }

        public int GetMemberID(string username)
        {
            return GetMemberID(username, Constants.NULL_INT);
        }


        public int GetNewMemberID()
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetNewMemberID();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to get new member id (uri: " + uri + ").", ex));
            }
        }

        /// <summary>
        /// Returns a MemberID provided a member's username.
        /// </summary>
        /// <param name="username">The member's username.</param>
        /// <returns>An integer representing the member's id.</returns>
        public int GetMemberID(string username, int communityID)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetMemberID(username, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to get member id (uri: " + uri + ", Username: " + username + ").",
                    ex));
            }
        }

        /// <summary>
        /// Returns the MemberID of the Member whose e-mail address exactly matches the parameter passed.
        /// If no match is found, returns Constants.NULL_INT.
        /// To search for all members whose e-mail address starts with a particular string, use GetMembersByEmail().
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public int GetMemberIDByEmail(string emailAddress)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetMemberIDByEmail(emailAddress);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to get member id (uri: " + uri + ", Email: " + emailAddress + ").",
                    ex));
            }
        }

        /// <summary>
        /// Returns the MemberID of the Member whose e-mail address exactly matches the parameter passed.
        /// If no match is found, returns Constants.NULL_INT.
        /// To search for all members whose e-mail address starts with a particular string, use GetMembersByEmail().
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public int GetMemberIDByEmail(string emailAddress, int communityId)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetMemberIDByEmail(emailAddress, communityId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to get member id (uri: " + uri + ", Email: " + emailAddress + ").",
                    ex));
            }
        }

        /// <summary>
        /// This will generate a reset password token that's good for the configured timeout value. This does not change the password though.
        /// </summary>
        /// <param name="memberId">MemberId of the member to reset the password for</param>
        /// <returns>Token value generated</returns>
        public string ResetPassword(int memberId)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).ResetPassword(memberId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to reset password for member (uri: " + uri + ", MemberId: " +
                    memberId + ").", ex));
            }
        }

        /// <summary>
        /// Validates the reset password token to see if it's still active.  If so, memberID is returned or 0 otherwise.
        /// </summary>
        /// <param name="tokenGuidString"></param>
        /// <returns>MemberID if token is valid or 0 if it isnt</returns>
        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).ValidateResetPasswordToken(tokenGuidString);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to validate reset password token (uri: " + uri +
                    ", TokenGUIDString: " + tokenGuidString + ").", ex));
            }
        }

        public ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).ValidateImpersonateToken(impersonateTokenGUID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    string.Format(
                        "Error occurred while attempting to validate an impersonation token (uri:{0}, TokenGUIDString:{1}",
                        uri, impersonateTokenGUID), ex));

            }

        }

        public string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri)
                        .GenerateImpersonateToken(adminMemberId, impersonateMemberId, impersonateBrandId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    string.Format(
                        "Error occurred while attempting to generate an impersonation token (uri:{0}, AdminMemberID:{1}, ImperonateMemberID:{2}, ImperonateBrandID:{3}",
                        uri, adminMemberId, impersonateMemberId, impersonateBrandId), ex));

            }

        }

        public void ExpireImpersonationToken(int adminMemberId)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    SMUtil.getService(uri).ExpireImpersonationToken(adminMemberId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    string.Format(
                        "Error occurred while attempting to expire an impersonation token (uri:{0}, AdminMemberID:{1}",
                        uri, adminMemberId), ex));

            }
        }

        #endregion

        #region member

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberIDs"></param>
        /// <param name="memberLoadFlags"></param>
        /// <returns></returns>
        public ArrayList GetMembers(ArrayList memberIDs, MemberLoadFlags memberLoadFlags)
        {
            var members = new ArrayList();

            var cachedMembers = getCachedMembers(memberIDs, memberLoadFlags);

            foreach (CachedMember cachedMember in cachedMembers)
            {
                members.Add(new Member(cachedMember));
            }

            return members;
        }

        /// <summary>
        /// Almost 100% of the time we call GetMember with MemberLoadFlags.None
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public Member GetMember(int memberId)
        {
            return GetMember(memberId, MemberLoadFlags.None);
        }

        public IMember GetIMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            return GetMember(memberID, memberLoadFlags);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="memberLoadFlags"></param>
        /// <returns></returns>
        public Member GetMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            try
            {
                Member member = null;
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                CachedMember cachedMember = getCachedMember(memberID, memberLoadFlags);

                if (cachedMember != null)
                {
                    member = new Member(cachedMember);
                }

                if (memberLoadFlags == MemberLoadFlags.IngoreSACache)
                {
                    ICache.Remove(CachedMemberAccess.GetCacheKey(memberID));
                }

                #region Commented Out - We will only get Access on deman within Member.cs

                /*
                if (member != null && !Member.IsUPSAccessMasterReadKillSwitchEnabled())
                {
                    //CachedMemberAccess cachedMemberAccess = getCachedMemberAccess(memberID, member.GetSiteIDList(), memberLoadFlags, false);
                    CachedMemberAccess cachedMemberAccess = getCachedMemberAccess(memberID, memberLoadFlags, false);

                    if (cachedMemberAccess != null)
                    {
                        member.CachedMemberAccess = cachedMemberAccess;
                    }
                }
                 * */

                #endregion

                return member;
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Error occurred while attempting to retrieve member from cache or middle-tier service.", ex);
            }
        }

        public Member PopulateMemberObj(CachedMember cachedMember)
        {
            try
            {

                if (cachedMember != null)
                {
                    return new Member(cachedMember);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to populate member from cachedMember.", ex);
            }
        }

        public IMemberDTO GetMemberDTO(int memberID, MemberType type)
        {
            string uri = "";
            IMemberDTO memberDTO = null;

            try
            {
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                    if (type == MemberType.Full)
                    {
                        memberDTO = GetMember(memberID, MemberLoadFlags.None);
                    }

                if (EnableMemberDTO)
                {

                    if (null == memberDTO)
                    {
                        memberDTO = MemberDTOCache.Get(MemberDTO.GetCacheKey(memberID, type)) as IMemberDTO;
                    }

                    if (null == memberDTO)
                    {

                        Int32 cacheTTL =
                            Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                        uri = SMUtil.getServiceManagerUri(memberID);
                        DateTime begin = DateTime.Now;
                        base.Checkout(uri);
                        try
                        {
                            memberDTO = SMUtil.getService(uri).GetMemberDTO(Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                memberID,
                                type,
                                false);
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }

                        Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                        if (totalSeconds > 1)
                        {
                            Trace.WriteLine("__GetMemberDTO(" + memberID.ToString() + "," + type + ") " +
                                            totalSeconds.ToString());
                        }
                    }
                }
                return memberDTO;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to get memberdto. (uri: " + uri + ")", ex);
            }
        }

        public ArrayList GetMemberDTOs(ArrayList memberIDs, MemberType type)
        {
            string uri = "";
            ArrayList memberDTOs = null;

            try
            {
                if (type == MemberType.Full)
                {
                    memberDTOs = GetMembers(memberIDs, MemberLoadFlags.None);
                }

                if (EnableMemberDTO)
                {
                    DateTime begin = DateTime.Now;
                    List<string> keys =
                        (from int memberId in memberIDs select MemberDTO.GetCacheKey(memberId, type)).ToList<string>();
                    IDictionary<string, object> memberDtoDictionary = MemberDTOCache.Get(keys);

                    if (null == memberDtoDictionary) memberDtoDictionary = new Dictionary<string, object>();

                    if (memberDtoDictionary.Count != memberIDs.Count)
                    {
                        List<int> missingMemberIds = new List<int>();

                        for (int i = 0; i < memberIDs.Count; i++)
                        {
                            int memberID = (int) memberIDs[i];
                            if (!memberDtoDictionary.ContainsKey(MemberDTO.GetCacheKey(memberID, type)))
                            {
                                missingMemberIds.Add(memberID);
                            }
                        }

                        Int32 cacheTTL =
                            Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));
                        uri = SMUtil.getServiceManagerUri();
                        base.Checkout(uri);
                        try
                        {
                            IMemberDTO[] dtos = SMUtil.getService(uri).GetMemberDTOs(Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                missingMemberIds.ToArray(),
                                type,
                                false);
                            //put the missing memberdtos into the dictionary
                            if (null != dtos)
                            {
                                foreach (IMemberDTO memberDto in dtos)
                                {
                                    memberDtoDictionary.Add(MemberDTO.GetCacheKey(memberDto.MemberID, type), memberDto);
                                }
                            }
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }

                    }

                    memberDTOs = ArrayList.Adapter(memberDtoDictionary.Values.ToList());
                    // member dtos need to be sorted when they come from cache
                    if (null != memberDTOs)
                    {
                        memberDTOs.Sort(new MemberDTOComparer(memberIDs.OfType<int>().ToArray(), false));
                    }

                    Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                    if (totalSeconds > 1)
                    {
                        Trace.WriteLine("__GetMemberDTOs(memberIDs," + type + ") " + totalSeconds.ToString());
                    }
                }
                return memberDTOs;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to get memberdto. (uri: " + uri + ")", ex);
            }
        }

        public void SaveMemberLastLogon(int memberID, int groupID, int adminMemberID)
        {
            string uri = "";

            try
            {
                // MembaseConfig membaseConfig =
                //           RuntimeSettings.GetMembaseConfigByBucket(MEMBASE_WEB_BUCKET_NAME);
                DateTime lastLogon = DateTime.Now;
                uri = SMUtil.getServiceManagerUri(memberID);
                base.Checkout(uri);
                try
                {
                    SMUtil.getService(uri)
                        .SaveMemberLastLogon(UseMembaseForSA ? null : System.Environment.MachineName, memberID, groupID,
                            lastLogon, adminMemberID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                // update the local SA cache item
                CachedMemberLogon cachedMemberLogon =
                    _cache.Get(CachedMemberLogon.GetCacheKey(memberID, groupID)) as CachedMemberLogon;
                if (cachedMemberLogon != null)
                {
                    int keepCount = 5;
                    string strKeepCount = SettingsService.GetSettingFromSingleton("LAST_LOGON_KEEP_COUNT");
                    if (strKeepCount != null && strKeepCount != string.Empty)
                    {
                        keepCount = Convert.ToInt32(strKeepCount);
                    }

                    // add the new last logon
                    cachedMemberLogon.LastLogonsNew.Add(new LastLogon()
                    {
                        AdminMemberId = adminMemberID,
                        LastLogonDate = lastLogon
                    });

                    if (cachedMemberLogon.LastLogonsNew.Count > keepCount)
                    {
                        // drop off the oldest date and add the new date
                        cachedMemberLogon.LastLogonsNew.Sort((x, y) => y.LastLogonDate.CompareTo(x.LastLogonDate));
                        cachedMemberLogon.LastLogonsNew.RemoveRange(keepCount,
                            cachedMemberLogon.LastLogonsNew.Count - keepCount);
                    }
                }
                else
                {
                    //Invalidate Cachedlogon object in membase.
                    //if(!useMembase)
                    //    Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Remove(CachedMemberLogon.GetCacheKey(memberID,groupID));
                }

            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to save member logon. (uri: " + uri + ")", ex);
            }
        }

        /*public MemberSaveResult SaveMemberAndMemberGroup(Member member, Brand brand)
        {
            ///this method will both save the member and set the membergroup
            MemberSaveResult saveResult = SaveMember(member, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                setMemberGroup(member, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            return saveResult;
        }

        public MemberSaveResult SaveMemberAndMemberGroup(Member member, int communityID)
        {
            ///this method will both save the member and set the membergroup
            MemberSaveResult saveResult = SaveMember(member, communityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                setMemberGroup(member, communityID, Constants.NULL_INT, Constants.NULL_INT);
            }
            return saveResult;
        }

        public MemberSaveResult SaveNewMemberAndMemberGroup(Member member, Brand brand)
        {
            ///this method will both save the member and set the membergroup
            MemberSaveResult saveResult = SaveNewMember(member, brand.Site.Community.CommunityID);
            if (saveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                setMemberGroup(member, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            return saveResult;
        }*/


        /// <summary>
        /// Primary method for saving member data. 
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public MemberSaveResult SaveMember(IMember member)
        {
            return SaveMember(member, Constants.NULL_INT);
        }

        /// <summary>
        /// PM-218 Username Scope Change (From Global to Community). This method should be used only when changing a username, a password or an email address.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public MemberSaveResult SaveMember(IMember member, int communityID)
        {
            MemberSaveResult memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, Constants.NULL_STRING);
            string uri = "";

            try
            {
                if (member.MemberUpdate.IsDirty)
                {
                    uri = SMUtil.getServiceManagerUri(member.MemberID);
                    base.Checkout(uri);
                    try
                    {
                        if (!string.IsNullOrEmpty(member.MemberUpdate.PasswordHash))
                        {
                            member.MemberUpdate.PasswordHash = EncryptPassword(member.MemberUpdate.PasswordHash);
                        }
                        //If the server uses membase, no need for synchronization callback.
                        memberSaveResult = SMUtil.getService(uri)
                            .SaveMember(System.Environment.MachineName, member.MemberUpdate, CACHED_MEMBER_VERSION,
                                communityID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
                return memberSaveResult;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to save member. (uri: " + uri + ")", ex);
            }
        }

        public MemberSaveResult SaveMember(IMember member, Brand brand, bool updateMemberGroup)
        {
            MemberSaveResult memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, Constants.NULL_STRING);
            string uri = "";

            try
            {
                if (member.MemberUpdate.IsDirty)
                {
                    uri = SMUtil.getServiceManagerUri(member.MemberID);
                    base.Checkout(uri);
                    try
                    {
                        if (!string.IsNullOrEmpty(member.MemberUpdate.PasswordHash))
                        {
                            member.MemberUpdate.PasswordHash = EncryptPassword(member.MemberUpdate.PasswordHash);
                        }
                        //If the server uses membase, no need for synchronization callback.
                        memberSaveResult =
                            SMUtil.getService(uri).SaveMember(UseMembaseForSA ? null : System.Environment.MachineName,
                                member.MemberUpdate, CACHED_MEMBER_VERSION, brand, updateMemberGroup);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        //						MemberUpdate update=member.MemberUpdate;
                        //						int[] commIds=member.GetCommunityIDList();
                        member.CommitChanges();

                        //MembaseConfig membaseConfig =
                        //    RuntimeSettings.GetMembaseConfigByBucket(MEMBASE_WEB_BUCKET_NAME);
                        ////Invalidate CachedMemberBytes object from membase cache. For servers using local cache, invalidation is done by Synchronizer.
                        //Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Remove(
                        //    CachedMemberBytes.GetCacheKey(member.MemberID));
                    }
                }
                return memberSaveResult;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to save member. (uri: " + uri + ")", ex);
            }
        }

        /*public MemberSaveResult SaveNewMember(Member member, int communityID)
        {
            MemberSaveResult memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, Constants.NULL_STRING);
            string uri = "";

            try
            {
                if (member.MemberUpdate.IsDirty)
                {
                    bool useMembase = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(MEMBASE_SETTING_CONSTANT));
                    uri = SMUtil.getServiceManagerUri(member.MemberID);
                    base.Checkout(uri);
                    try
                    {
                        //If the server uses membase, no need for synchronization callback.
                        memberSaveResult = SMUtil.getService(uri).SaveNewMember(useMembase ? null : System.Environment.MachineName, 
                                                                                member.MemberUpdate, CACHED_MEMBER_VERSION, communityID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        //						MemberUpdate update=member.MemberUpdate;
                        //						int[] commIds=member.GetCommunityIDList();
                        member.CommitChanges();

                        //MembaseConfig membaseConfig =
                        //    RuntimeSettings.GetMembaseConfigByBucket(MEMBASE_WEB_BUCKET_NAME);
                        ////Invalidate CachedMemberBytes object from membase cache. For servers using local cache, invalidation is done by Synchronizer.
                        //Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Remove(CachedMemberBytes.GetCacheKey(member.MemberID));
                    }
                }


                return memberSaveResult;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to save member. (uri: " + uri + ")", ex);
            }
        }*/

        public ArrayList GetCachedMembers(ArrayList memberIDs, MemberLoadFlags memberLoadFlags)
        {
            ArrayList cachedMembers = new ArrayList();

            for (Int32 num = 0; num < memberIDs.Count; num++)
            {
                cachedMembers.Add(GetCachedMember((Int32) memberIDs[num], memberLoadFlags));
            }

            return cachedMembers;
        }

        public CachedMember GetCachedMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            try
            {
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                return getCachedMember(memberID, memberLoadFlags);
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Error occurred while attempting to retrieve CachedMember from cache or middle-tier service.", ex);
            }
        }

        public void ExpireEntireMemberCache()
        {
            string uri = String.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    SMUtil.getService(uri).ExpireEntireMemberCache();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to expire member cache. (uri: " + uri + ").", ex));
            }
        }

        public void ExpireCachedMember(int memberID)
        {
            string uri = String.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri(memberID);
                base.Checkout(uri);
                try
                {
                    SMUtil.getService(uri).ExpireCachedMember(memberID);
                    ICache.Remove(CachedMember.GetCacheKey(memberID));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while attempting to expire member from cache. (uri: " + uri + ", MemberID: " +
                    memberID + ").", ex));
            }
        }

        public void ExpireCachedMemberAccess(int memberID)
        {
            ExpireCachedMemberAccess(memberID, false);
        }

        public bool ExpireCachedMemberAccess(int memberID, bool refreshCache)
        {
            string uri = String.Empty;
            bool returnValue = true;
            try
            {
                ICache.Remove(CachedMemberAccess.GetCacheKey(memberID));

                uri = SMUtil.getServiceManagerUri(memberID);
                base.Checkout(uri);
                try
                {
                    returnValue = SMUtil.getService(uri).ExpireCachedMemberAccess(memberID, true, true, true);
                }
                finally
                {
                    base.Checkin(uri);
                }

                //refresh cache, ignoring all downstream cache
                if (refreshCache)
                {
                    getCachedMemberAccess(memberID, MemberLoadFlags.IngoreSACache, true);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    String.Format(
                        "Error occurred while attempting to expire cached member access from cache. (uri: " + uri +
                        ", MemberID: {0}).", memberID), ex));
            }

            return returnValue;
        }

        /// <summary>
        /// Checks if an email address exists - used for registration. To get memberId by emailaddress, use GetMemberIDByEmail
        /// EmailAddress is shared across BH communities.
        /// For BH sites, checks if an email address exists in any one of the BH communities(shared logon communities).
        /// For Mingles sites, checks if an email exists in the given community.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public bool CheckEmailAddressExists(string emailAddress, int communityId)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).CheckEmailAddressExists(emailAddress, communityId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while checking if an email address already exists (uri: " + uri + ", Email: " +
                    emailAddress + ", CommunityId: " + communityId + ").", ex));
            }
        }

        internal CachedMember getCachedMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            string uri = "";

            try
            {
                CachedMember cachedMember = null;
                bool ignoreSACache = (memberLoadFlags & MemberLoadFlags.IngoreSACache) == MemberLoadFlags.IngoreSACache;

                if (!ignoreSACache)
                {
                    cachedMember = ICache.Get(CachedMember.GetCacheKey(memberID)) as CachedMember;
                }
                else
                {
                    // floods eventlog, commented out
                    //System.Diagnostics.EventLog.WriteEntry("WWW", "geCachedMember: IgnoreSACache " + memberID.ToString());
                }

                if (cachedMember == null || cachedMember.Version != CACHED_MEMBER_VERSION)
                {
                    TraceVersion(cachedMember);
                    Int32 cacheTTL =
                        Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = SMUtil.getServiceManagerUri(memberID);
                    DateTime begin = DateTime.Now;
                    base.Checkout(uri);
                    try
                    {
                        cachedMember =
                            new CachedMember(
                                SMUtil.getService(uri)
                                    .GetCachedMemberBytes(UseMembaseForSA ? null : System.Environment.MachineName,
                                        Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                        memberID,
                                        false, CACHED_MEMBER_VERSION), CACHED_MEMBER_VERSION);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    ArrayList communityIDList =
                        cachedMember.GetCommunityIDList(AttributeMetadataSA.Instance.GetAttributes(),
                            BrandConfigSA.Instance.GetBrands());

                    for (Int32 i = 0; i < communityIDList.Count; i++)
                    {
                        Int32 communityID = (Int32) communityIDList[i];
                        if (
                            Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MEMBERSVC_PHOTO_RELATIVEPATHS",
                                communityID)))
                        {
                            cachedMember.Photos.GetCommunity(communityID).StripHostFromPaths();
                        }
                    }

                    Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                    if (totalSeconds > 1)
                    {
                        System.Diagnostics.Trace.WriteLine("__GetCachedMember(" + memberID.ToString() + ") " +
                                                           totalSeconds.ToString());
                    }

                    cachedMember.CacheTTLSeconds = cacheTTL;
                    ICache.Insert(cachedMember);
                }

                return cachedMember;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to get cached member. (uri: " + uri + ")", ex);
            }
        }

        internal ArrayList getCachedMembers(ArrayList memberIDs, MemberLoadFlags memberLoadFlags)
        {
            var cachedMemberBytesArray = new ArrayList();
            string uri = "";
            try
            {
                return GetCachedMembers(memberIDs, memberLoadFlags);
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to get cached member. (uri: " + uri + ")", ex);
            }
        }

        internal CachedMemberAccess getCachedMemberAccess(int memberID, MemberLoadFlags memberLoadFlags, bool forceLoad)
        {
            string uri = "";

            try
            {

                CachedMemberAccess cachedMemberAccess = null;

                //get from cache
                bool ignoreSACache = (memberLoadFlags & MemberLoadFlags.IngoreSACache) == MemberLoadFlags.IngoreSACache;

                if (!ignoreSACache && !forceLoad)
                {
                    cachedMemberAccess = ICache.Get(CachedMemberAccess.GetCacheKey(memberID)) as CachedMemberAccess;
                    //if (cachedMemberAccess != null)
                    //{ System.Diagnostics.EventLog.WriteEntry("WWW", "getCachedMemberAccess: Retrieved from SA Cache, memberID: " + memberID.ToString() + ", accessPrivileges: " + cachedMemberAccess.AccessPrivilegeList.Count.ToString()); }
                }
                else
                {
                    // floods eventlog, commented out
                    //System.Diagnostics.EventLog.WriteEntry("WWW",
                    //    "getCachedMemberAccess: IgnoreSACache " + memberID.ToString());
                }

                #region Deprecated - We no longer need to perform this check

                //List<int> migratedPrivilegeSites = new List<int>();
                //if (cachedMemberAccess != null)
                //{
                //    //System.Diagnostics.EventLog.WriteEntry("WWW", "Success: MemberSA.getCachedMemberAccess() retrieved CachedMemberAccess from cache for memberID: " + memberID.ToString()); 

                //    //Check for outdated CachedMemberAccess due to migration process
                //    //This is to support migrating privilege process, so we don't have to restart web servers whenever
                //    //we finish migrating privileges for a site.
                //    if (memberSiteIDs != null)
                //    {
                //        foreach (int i in memberSiteIDs)
                //        {
                //            bool siteHasAlreadyMigratedPrivileges = Member.HasPrivilegesBeenMigratedToUPSAccess(Constants.NULL_INT, i, Constants.NULL_INT);
                //            if (siteHasAlreadyMigratedPrivileges)
                //            {
                //                migratedPrivilegeSites.Add(i);
                //                if (!cachedMemberAccess.MigratedPrivilegeSites.Contains(i))
                //                {
                //                    cachedMemberAccess = null;
                //                    System.Diagnostics.EventLog.WriteEntry("WWW", "MemberSA.getCachedMemberAccess(): cached object does not contain migrated privilege for siteID: " + i.ToString() + ", memberID: " + memberID.ToString() + ". Will refresh from MT.");
                //                    break;
                //                }
                //            }
                //        }
                //    }
                //}

                //System.Diagnostics.EventLog.WriteEntry("WWW", "MemberSA.getCachedMemberAccess(): TEST HasPrivilegesBeenMigratedToUPS 101: " + Member.HasPrivilegesBeenMigratedToUPSAccess(Constants.NULL_INT, 101, Constants.NULL_INT).ToString());
                //System.Diagnostics.EventLog.WriteEntry("WWW", "MemberSA.getCachedMemberAccess(): TEST HasPrivilegesBeenMigratedToUPS 103: " + Member.HasPrivilegesBeenMigratedToUPSAccess(Constants.NULL_INT, 103, Constants.NULL_INT).ToString());

                #endregion

                if (cachedMemberAccess == null)
                {
                    Int32 cacheTTL =
                        Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = SMUtil.getServiceManagerUri(memberID);
                    DateTime begin = DateTime.Now;
                    base.Checkout(uri);
                    try
                    {
                        cachedMemberAccess =
                            SMUtil.getService(uri)
                                .GetCachedMemberAccess(UseMembaseForSA ? null : System.Environment.MachineName,
                                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                    memberID,
                                    forceLoad);

                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                    if (totalSeconds > 1)
                    {
                        System.Diagnostics.Trace.WriteLine("__GetCachedMemberAccess(" + memberID.ToString() + ") " +
                                                           totalSeconds.ToString());
                    }

                    if (cachedMemberAccess != null)
                    {
                        cachedMemberAccess.CacheTTLSeconds = cacheTTL;
                        ICache.Insert(cachedMemberAccess);
                    }
                }

                return cachedMemberAccess;
            }
            catch (Exception ex)
            {
                throw new SAException(
                    String.Format(
                        "Error occurred while attempting to get cached member access. (uri: " + uri + ", memberID:{0})",
                        memberID), ex);
            }

        }

        internal CachedMemberLogon getCachedMemberLogon(int memberID, int groupID, MemberLoadFlags memberLoadFlags)
        {
            string uri = string.Empty;
            try
            {
                CachedMemberLogon cachedMemberLogon = null;
                bool ignoreSACache = (memberLoadFlags & MemberLoadFlags.IngoreSACache) == MemberLoadFlags.IngoreSACache;

                if (!ignoreSACache)
                {
                    cachedMemberLogon =
                        ICache.Get(CachedMemberLogon.GetCacheKey(memberID, groupID)) as CachedMemberLogon;
                }

                // cache hit?  if not call the service to get it
                if (cachedMemberLogon == null)
                {
                    Int32 cacheTTL =
                        Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = SMUtil.getServiceManagerUri(memberID);
                    base.Checkout(uri);
                    try
                    {
                        cachedMemberLogon =
                            SMUtil.getService(uri)
                                .GetCachedMemberLogon(UseMembaseForSA ? null : System.Environment.MachineName,
                                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                    memberID,
                                    groupID,
                                    false,
                                    CACHED_MEMBER_LOGON_VERSION);

                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (cachedMemberLogon != null)
                    {
                        cachedMemberLogon.CacheTTLSeconds = cacheTTL;
                        ICache.Insert(cachedMemberLogon);
                    }
                }

                return cachedMemberLogon;
            }
            catch (Exception ex)
            {
                throw new SAException(
                    String.Format(
                        "Error occurred while attempting to get cached member logon. (uri: " + uri +
                        ", memberID:{0}, groupid:{1})", memberID, groupID), ex);
            }
        }


        /// <summary>
        /// The user stories for this are FNOLA-369 & FNOLA-1384
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="memberLoadFlags"></param>
        /// <returns></returns>
        internal CachedMemberEmail getCachedMemberEmail(int memberID, MemberLoadFlags memberLoadFlags)
        {
            string uri = string.Empty;
            try
            {
                CachedMemberEmail cachedMemberEmail = null;
                bool ignoreSACache = (memberLoadFlags & MemberLoadFlags.IngoreSACache) == MemberLoadFlags.IngoreSACache;

                if (!ignoreSACache)
                {
                    cachedMemberEmail =
                        ICache.Get(CachedMemberEmail.GetCacheKey(memberID)) as CachedMemberEmail;
                }

                // cache hit?  if not call the service to get it
                if (cachedMemberEmail == null)
                {
                    Int32 cacheTTL =
                        Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = SMUtil.getServiceManagerUri(memberID);
                    base.Checkout(uri);
                    try  
                    {
                        cachedMemberEmail =
                            SMUtil.getService(uri)
                                .GetCachedMemberEmail(UseMembaseForSA ? null : System.Environment.MachineName,
                                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                    memberID,
                                    false,
                                    CACHED_MEMBER_EMAIL_VERSION);

                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (cachedMemberEmail != null)
                    {
                        cachedMemberEmail.CacheTTLSeconds = cacheTTL;
                        ICache.Insert(cachedMemberEmail);
                    }
                }

                return cachedMemberEmail;
            }
            catch (Exception ex)
            {
                throw new SAException(
                    String.Format(
                        "Error occurred while attempting to get cached member Email. (uri: " + uri +
                        ", memberID:{0})", memberID), ex);
            }
        }



        /// <summary>
        /// This updates the "count" for count-based privileges as Members use their privileges, allows for 
        /// immediate update of local cache instead of waiting for synchronization from Access Service (which will still happen).
        /// As of now, only count-based privileges need this, the rest of the privilege modes are only updated
        /// from admins or purchases, so those should call Access Service directly.
        /// </summary>
        /// <returns></returns>
        public bool AdjustUnifiedAccessCountPrivilege(int memberID, int adminID, string adminDomainUserName,
            int[] privilegeTypeID, int brandID, int siteID, int communityID, int count)
        {
            string uri = "";
            try
            {
                bool response = true;
                //Update local cached privilege
                CachedMemberAccess cachedMemberAccess =
                    ICache.Get(CachedMemberAccess.GetCacheKey(memberID)) as CachedMemberAccess;
                if (cachedMemberAccess != null)
                {
                    cachedMemberAccess.AdjustAccessCountPrivilege(privilegeTypeID, brandID, siteID, communityID, count);
                    ICache.Insert(cachedMemberAccess);
                }

                //Update MT
                uri = SMUtil.getServiceManagerUri(memberID);
                DateTime begin = DateTime.Now;
                base.Checkout(uri);
                try
                {
                    response = SMUtil.getService(uri)
                        .AdjustUnifiedAccessCountPrivilege(memberID, adminID, adminDomainUserName, privilegeTypeID,
                            brandID, siteID, communityID, count);

                }
                finally
                {
                    base.Checkin(uri);
                }

                return response;
            }
            catch (Exception ex)
            {
                throw new SAException(
                    String.Format(
                        "Error in MemberSA.AdjustUnifiedAccessCountPrivilege (uri: " + uri + ", memberID:{0})", memberID),
                    ex);
            }

        }


        public void UpdateSearch(int memberID, int communityID)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri(memberID);
                base.Checkout(uri);
                try
                {
                    SMUtil.getService(uri).UpdateSearch(memberID, communityID);

                    ICache.Remove(CachedMemberBytes.GetCacheKey(memberID));
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to update search. (uri: " + uri + ")", ex);
            }
        }

        #endregion

        #region photo

        //temp
        /*public void GetFilePath(Int32 fileID,
			out string filePath,
			out string fileWebPath)
		{
			FileSA.Instance.GetFilePath(fileID,
				out filePath,
				out fileWebPath);
		}*/


        public bool SavePhotos(Int32 brandID, Int32 siteID, Int32 communityID, Int32 memberID,
            PhotoUpdate[] photoUpdates)
        {
            string uri = "";
            bool success = true;

            try
            {
                uri = SMUtil.getServiceManagerUri(memberID);
                CachedMember cachedMember = getCachedMember(memberID, MemberLoadFlags.None);
                IMemberService svc = SMUtil.getService(uri);

                for (Int32 num = 0; num < photoUpdates.Length; num++)
                {
                    PhotoUpdate photoUpdate = photoUpdates[num];
                    string enteringPhotoUpdateLog =
                        string.Format(
                            "MemberPhotoID: {0} MemberID: {1} BrandID: {2} : ListOrder: {3} FileID: {4} ThumbFileID: {5} WebPath: {6} ThumbPath: {7}",
                            photoUpdate.MemberPhotoID.ToString(), memberID.ToString(), brandID.ToString(),
                            photoUpdate.ListOrder.ToString(),
                            photoUpdate.FileID.ToString(), photoUpdate.ThumbFileID.ToString(), photoUpdate.FileWebPath,
                            photoUpdate.ThumbFileWebPath);
                    RollingFileLogger.Instance.LogInfoMessage("WEB", "MemberSA",
                        "Processing PhotoUpdate: " + enteringPhotoUpdateLog, null);

                    if (photoUpdate.FileID == Constants.NULL_INT && photoUpdate.ImageData != null)
                    {
                        System.Drawing.Image image = null;
                        MemoryStream streamIn;
                        MemoryStream streamOut;
                        try
                        {
                            streamIn = new MemoryStream(photoUpdate.ImageData);
                            image = System.Drawing.Image.FromStream(streamIn, true);
                            streamOut = new MemoryStream();
                            image.Save(streamOut, System.Drawing.Imaging.ImageFormat.Jpeg);
                            photoUpdate.FileHeight = image.Height;
                            photoUpdate.FileWidth = image.Width;
                        }
                        catch (Exception ex)
                        {
                            throw (new BLException(
                                "Invalid photo file type or corrupt photo file, could not convert from MemoryStream to System.Drawing.Image.",
                                ex));
                        }
                        base.Checkout(uri);
                        CreateNewFileResult newFileResult = null;
                        try
                        {
                            newFileResult = FileSA.Instance.SaveNewFile(streamOut.ToArray(),
                                "jpg",
                                communityID, 
                                siteID, memberID, true);

                            success = newFileResult.Success;
						    photoUpdate.FileSize = newFileResult.OriginalFileSize;

                            string resultString =
                                string.Format(
                                    "MemberPhotoID: {0} RootGroupID: {1} WebPath: {2} FileID: {3} Success {4}",
                                    photoUpdate.MemberPhotoID.ToString(), newFileResult.FileRootID.ToString(),
                                    newFileResult.WebPath, newFileResult.FileID.ToString(), success.ToString());
                            RollingFileLogger.Instance.LogInfoMessage("WEB", "MemberSA", "Full Result: " + resultString,
                                null);
                        }
                        catch (Exception ex)
                        {
                            RollingFileLogger.Instance.LogException("WEB", "MemberSA Exception", ex);
                            success = false;
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }

                        if (!success)
                        {
                            //if one upload fails, don't continue and attempt to save the photos.
                            break;
                        }

                        if (newFileResult != null)
                        {
                            photoUpdate.FileID = newFileResult.FileID;
                            photoUpdate.FileWebPath = newFileResult.WebPath;
                            photoUpdate.FileCloudPath = newFileResult.CloudPath;
                            photoUpdate.ImageData = null;
                            photoUpdate.OriginalFileCloudPath = newFileResult.OriginalFileCloudPath;
                        }
                    }
                    else if (photoUpdate.Delete)
                    {
                        PhotoCommunity photoCommunity = cachedMember.Photos.GetCommunity(communityID);

                        for (byte photoNum = 0; photoNum < photoCommunity.Count; photoNum++)
                        {
                            Photo photo = photoCommunity[photoNum];
                            if (photo.MemberPhotoID == photoUpdate.MemberPhotoID)
                            {
                                FileSA.Instance.DeleteFile(photo.FileID, communityID, siteID);
                                if (photo.ThumbFileID != Constants.NULL_INT)
                                {
                                    FileSA.Instance.DeleteFile(photo.ThumbFileID, communityID, siteID);
                                }
                                //delete all photo types for this photos (iphone optimized, etc)
                                if (null != photo.Files)
                                {
                                    foreach (PhotoFile file in photo.Files)
                                    {
                                        FileSA.Instance.DeleteFile(file.FileID, communityID, siteID);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }

                if (success)
                {
                    //only update the member record if the file upload happened successfully. This is a change from before when the 
                    //member update always happened. Reason for the change is the upload to S3 - that could fail and we should
                    //reacy accordingly.
                    base.Checkout(uri);
                    try
                    {
                        if (brandID == Constants.NULL_INT)
                        {
                            //this is the old API, not using the DB based queue
                            cachedMember.Photos = svc.SavePhotos(
                                UseMembaseForSA ? null : System.Environment.MachineName,
                                communityID,
                                memberID,
                                photoUpdates, CACHED_MEMBER_VERSION);
                        }
                        else
                        {
                            cachedMember.Photos = svc.SavePhotos(
                                UseMembaseForSA ? null : System.Environment.MachineName,
                                brandID,
                                siteID,
                                communityID,
                                memberID,
                                photoUpdates, CACHED_MEMBER_VERSION);
                        }

                        //MembaseConfig membaseConfig = RuntimeSettings.GetMembaseConfigByBucket(MEMBASE_WEB_BUCKET_NAME);
                        ////Remove cachedMember object from membase cache. For servers using local cache, invalidation is done by Synchronizer.
                        //Spark.Caching.MembaseCaching.GetInstance(membaseConfig).Remove(CachedMember.GetCacheKey(memberID));

                        //update HasPhotoFlag attribute
                        Member member = GetMember(memberID, MemberLoadFlags.None);
                        if (member != null)
                        {
                            int hasPhotoFlag = member.GetAttributeInt(communityID, siteID, brandID, "HasPhotoFlag", 0);
                            if (cachedMember.HasApprovedPhoto(communityID) && hasPhotoFlag != 1)
                            {
                                member.SetAttributeInt(communityID, siteID, brandID, "HasPhotoFlag", 1);
                                SaveMember(member);
                            }
                            else if (!cachedMember.HasApprovedPhoto(communityID) && hasPhotoFlag != 0)
                            {
                                member.SetAttributeInt(communityID, siteID, brandID, "HasPhotoFlag", 0);
                                SaveMember(member);
                            }
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return success;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting save photos (uri: " + uri + ").", ex));
            }
        }



        #endregion

//		private void updatePhotoSearch(MemberUpdate update, int[] communityids)
//		{
//			try
//			{
//				Matchnet.PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.UpdateMember(update, communityids);
//				
//			}
//			catch(Exception ex)
//			{System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.MemberSA","Error updating PhotoStore:" + ex.Message,System.Diagnostics.EventLogEntryType.Warning);}
//
//		}

        #region admin member search

        /// <summary>
        /// This method is replacing all Admin Tool member search methods.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userName"></param>
        /// <param name="memberId"></param>
        /// <param name="phone"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="zipcode"></param>
        /// <param name="city"></param>
        /// <param name="ipAddress"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public int[] PerformAdminSearch(string email, string userName, string memberId, string phone, string firstName,
            string lastName, string zipcode, string city, string ipAddress, int communityId, bool exactUsername,
            int filterType)
        {
            string uri = string.Empty;
            int[] memberIds = null;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    memberIds = SMUtil.getService(uri).PerformAdminSearch(email, userName, memberId, phone, firstName,
                        lastName, zipcode, city, ipAddress,
                        communityId, exactUsername, filterType);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return memberIds;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while performing an Admin Search. (uri: " + uri + ")", ex));
            }
        }

        public int[] PerformAdminSearchPagination(string email, string userName, string memberId, string phone,
            string firstName, string lastName, string zipcode, string city, string ipAddress,
            int communityId, bool exactUsername, int filterType,
            int pageNumber, int rowsPerPage)
        {
            string uri = string.Empty;
            int[] memberIds = null;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);

                try
                {
                    memberIds = SMUtil.getService(uri).PerformAdminSearchPagination(email, userName, memberId, phone,
                        firstName, lastName, zipcode, city, ipAddress,
                        communityId, exactUsername, filterType,
                        pageNumber, rowsPerPage);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return memberIds;
            }
            catch (Exception ex)
            {
                throw (new SAException(
                    "Error occurred while performing an Admin Search (Pagination version). (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByEmail(string email, int startRow, int pageSize, ref int totalRows)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByEmail(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                email,
                                startRow,
                                pageSize,
                                ref totalRows, CACHED_MEMBER_VERSION);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by email. (uri: " + uri + ")", ex));
            }
        }

        public ArrayList GetMembersByEmail(string email, int startRow, int pageSize, ref int totalRows,
            bool memberIdOrderByDesc)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByEmail(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                email,
                                startRow,
                                pageSize,
                                ref totalRows,
                                CACHED_MEMBER_VERSION,
                                memberIdOrderByDesc);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by email. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByUserName(string userName, int startRow, int pageSize, ref int totalRows)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByUserName(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                userName,
                                startRow,
                                pageSize,
                                ref totalRows, CACHED_MEMBER_VERSION);
                }
                finally
                {
                    base.Checkin(uri);
                }

                // if  th euser was not found in mnAdmin/membercache, then pull it out by username
                if (members.Count < 1)
                {
                    int memberID = GetMemberID(userName);

                    if (memberID > 0)
                    {
                        CachedMember member = getCachedMember(memberID, MemberLoadFlags.None);
                        members.Add(member);
                    }
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by user name. (uri: " + uri + ")", ex));
            }
        }

        public ArrayList GetMembersByUserName(string userName, int startRow, int pageSize, ref int totalRows,
            bool memberIdOrderByDesc, bool exactMatchOnly)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByUserName(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                userName,
                                startRow,
                                pageSize,
                                ref totalRows,
                                CACHED_MEMBER_VERSION,
                                memberIdOrderByDesc,
                                exactMatchOnly);
                }
                finally
                {
                    base.Checkin(uri);
                }

                // if  th euser was not found in mnAdmin/membercache, then pull it out by username
                if (members.Count < 1)
                {
                    int memberID = GetMemberID(userName);

                    if (memberID > 0)
                    {
                        CachedMember member = getCachedMember(memberID, MemberLoadFlags.None);
                        members.Add(member);
                    }
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by user name. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByPhoneNumber(string phoneNumber, int startRow, int pageSize, ref int totalRows)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByPhoneNumber(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                phoneNumber,
                                startRow,
                                pageSize,
                                ref totalRows, CACHED_MEMBER_VERSION);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by phone number. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByCreditCard(string creditCard, int startRow, int pageSize, ref int totalRows)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByCreditCard(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                creditCard,
                                startRow,
                                pageSize,
                                ref totalRows, CACHED_MEMBER_VERSION);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by credit card. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routing"></param>
        /// <param name="checking"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByChecking(string routing, string checking, int startRow, int pageSize,
            ref int totalRows)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByChecking(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                routing,
                                checking,
                                startRow,
                                pageSize,
                                ref totalRows, CACHED_MEMBER_VERSION);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by checking. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Even with PerformAdminSearch(), this method will be kept for exact MemberID search.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public ArrayList GetMembersByMemberID(int memberID, ref int totalRows)
        {
            ArrayList members = new ArrayList();
            string uri = "";

            try
            {
                Int32 cacheTTL =
                    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    members =
                        SMUtil.getService(uri)
                            .GetMembersByMemberID(UseMembaseForSA ? null : System.Environment.MachineName,
                                Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                                memberID,
                                ref totalRows, CACHED_MEMBER_VERSION);
                }
                finally
                {
                    base.Checkin(uri);
                }

                if (members.Count < 1)
                {
                    CachedMember member = getCachedMember(memberID, MemberLoadFlags.None);
                    if (member != null)
                    {
                        members.Add(member);
                        totalRows = 1;
                    }
                }

                return members;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member by credit card. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <returns></returns>
        public MemberPaymentCollection GetMemberPaymentInfo(int memberID)
        {
            MemberPaymentCollection memberPaymentCollection = new MemberPaymentCollection();
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    memberPaymentCollection = SMUtil.getService(uri).GetMemberPaymentInfo(memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return memberPaymentCollection;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while getting member payment info. (uri: " + uri + ")", ex));
            }
        }

        #endregion

        #region admin others

        public AdminMemberDomainMapperCollection GetAdminMemberDomainMappers()
        {
            AdminMemberDomainMapperCollection list = new AdminMemberDomainMapperCollection();
            string uri = "";
            try
            {
                list =
                    ICache.Get(AdminMemberDomainMapperCollection.GetCacheKeyString()) as
                        AdminMemberDomainMapperCollection;
                if (list == null)
                {
                    uri = SMUtil.getServiceManagerUri();

                    base.Checkout(uri);
                    try
                    {
                        list = SMUtil.getService(uri).GetAdminMemberDomainMappers();
                        if (list != null && list.Count > 0)
                        {
                            ICache.Insert(list);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                throw (new SAException("MemberSA.GetAdminMemberDomainMappers() error", ex));
            }
        }

        public void IncrementCommunicationCounter(int memberId, int siteId, int[] counterType)
        {
            string uri = string.Empty;
            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    SMUtil.getService(uri).IncrementCommunicationCounter(memberId, siteId, counterType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("MemberSA.IncrementCommunicationCounter() error", ex));
            }
        }

        public Dictionary<int, int> GetCommunicationWarningCounts(int memberId, int siteId)
        {
            string uri = string.Empty;
            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return SMUtil.getService(uri).GetCommunicationWarningCounts(memberId, siteId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("MemberSA.GetCommunicationWarningCounts() error", ex));
            }
        }

        #endregion

        //trace method for troubleshooting cachedmember version
        private void TraceVersion(CachedMember cachedmember)
        {
            try
            {
                string format = "MemberSA has unexpected CachedMember version:{0}";

                if (cachedmember != null)
                {
                    if (cachedmember.Version != CACHED_MEMBER_VERSION)
                        System.Diagnostics.EventLog.WriteEntry("WWW", String.Format(format, cachedmember.Version));
                }

            }
            catch (Exception ex)
            {
                ex = null;
            }

        }

        private ValidateAttributeResult ValidateAttributeValues(Dictionary<string, object> attributeValues)
        {
            foreach (var attribute in attributeValues)
            {
                var mdAttribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attribute.Key);
                if (mdAttribute == null)
                {
                    //attibute not found
                    return new ValidateAttributeResult
                    {
                        Validated = false,
                        ErrorMessage = string.Format("Attribute {0} not found", attribute.Key)
                    };
                }

                switch (mdAttribute.DataType.ToString().ToUpper())
                {
                    case "MASK":
                    case "NUMBER":
                        if (!(attribute.Value is Int64) && !(attribute.Value is Int32))
                        {
                            return new ValidateAttributeResult
                            {
                                Validated = false,
                                ErrorMessage = string.Format("Attribute {0} was not valid number", attribute.Key)
                            };
                        }
                        break;
                    case "TEXT":
                        if (!(attribute.Value is string))
                        {
                            return new ValidateAttributeResult
                            {
                                Validated = false,
                                ErrorMessage = string.Format("Attribute {0} was not valid string", attribute.Key)
                            };
                        }
                        break;
                    case "BIT":
                        if (!(attribute.Value is Boolean))
                        {
                            return new ValidateAttributeResult
                            {
                                Validated = false,
                                ErrorMessage = string.Format("Attribute {0} was not valid boolean", attribute.Key)
                            };
                        }
                        break;
                    case "DATE":
                        DateTime dateTimeAttribute;
                        if (!(DateTime.TryParse(Convert.ToString(attribute.Value), out dateTimeAttribute)))
                        {
                            return new ValidateAttributeResult
                            {
                                Validated = false,
                                ErrorMessage = string.Format("Attribute {0} was not valid date", attribute.Key)
                            };
                        }
                        break;
                    default:
                        return new ValidateAttributeResult
                        {
                            Validated = false,
                            ErrorMessage =
                                string.Format("Attribute {0} had invalid data type {1}", attribute.Key,
                                    mdAttribute.DataType.ToString().ToUpper())
                        };

                }
            }

            return new ValidateAttributeResult {Validated = true, ErrorMessage = string.Empty};
        }

        public CreateLogonCredentialsResult CreateLogonCredentials(string emailAddress, string username, string password,
            Brand brand)
        {
            var uri = string.Empty;

            try
            {
                uri = SMUtil.getServiceManagerUri();
                Checkout(uri);
                try
                {
                    var encryptedPassword = EncryptPassword(password);
                    return SMUtil.getService(uri)
                        .CreateLogonCredentials(emailAddress, username, encryptedPassword, brand);
                }
                finally
                {
                    Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogError("MEMBER_SVC", "MemberServiceSA", ex,
                    "Error occurred while creating client credentials.");
                throw (new SAException("Error occurred whilecreating client credentials. (uri: " + uri + ")", ex));
            }
        }

        //JS-1573 Taking out the  //string iovationBlackBox,
        public MemberRegisterResult RegisterExtended(Brand brand,
            string emailAddress,
            string username,
            string password,
            Int32 ipAddress,
            string registrationSessionID,
            Dictionary<string, object> attributeValues
            )
        {
            string uri = "";
            MemberRegisterResult memberRegisterResult;

            try
            {
                RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA",
                    "Entering registration" + registrationSessionID, registrationSessionID);

                var validateAttributeResponse = ValidateAttributeValues(attributeValues);
                if (!validateAttributeResponse.Validated)
                {
                    RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA",
                        "Registration attributes not valid: " + validateAttributeResponse.ErrorMessage,
                        registrationSessionID);
                    memberRegisterResult = new MemberRegisterResult(RegisterStatusType.InvalidAttributes, 0, username);
                    memberRegisterResult.ErrorMessage = validateAttributeResponse.ErrorMessage;
                    return memberRegisterResult;
                }

                string ipAddressString = new System.Net.IPAddress(BitConverter.GetBytes(ipAddress)).ToString();
                bool allow = IPBlockerServiceAdapter.Instance.CheckIPAccess(ipAddressString, brand.Site.SiteID,
                    Spark.Common.IPBlockerAccessType.Registration);

                if (!allow)
                {
                    RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "IPBlocked!",
                        registrationSessionID);
                    memberRegisterResult = new MemberRegisterResult(RegisterStatusType.IPBlocked, 0, username);
                    return memberRegisterResult;
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberServiceSA", ex,
                    "Error occurred while attempting to connect to IPBlocker. Session: " + registrationSessionID);
                throw (new SAException("Error occurred while attempting to connect to IPBlocker. (uri: " + uri + ")", ex));
            }

            try
            {
                uri = SMUtil.getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    string encryptPassword = EncryptPassword(password);
                    memberRegisterResult = SMUtil.getService(uri)
                        .Register(emailAddress, username, password, encryptPassword, brand);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogError("MEMBER_SVC", "MemberServiceSA", ex,
                    "Error occurred while registering. Session: " + registrationSessionID);
                throw (new SAException("Error occurred while attempting to register new member. (uri: " + uri + ")", ex));
            }

            RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA",
                "MemberRegisterResult: " + memberRegisterResult.RegisterStatus.ToString(), registrationSessionID);

            if (memberRegisterResult.RegisterStatus == RegisterStatusType.Success)
            {
                Member member = GetMember(memberRegisterResult.MemberID, MemberLoadFlags.None);

                member.EmailAddress = emailAddress;
                member.SetUsername(memberRegisterResult.Username, brand);
                member.SetAttributeText(brand, "RegsistrationSessionID", registrationSessionID, TextStatusType.Auto);

                if (ipAddress != Constants.NULL_INT)
                {
                    member.SetAttributeInt(brand, "RegistrationIP", ipAddress);
                }

                try
                {
                    var enforceCanadianAutoOptOut =
                        Convert.ToBoolean(
                            RuntimeSettings.GetSetting(SettingConstants.AUTO_OPT_OUT_OF_ALL_EMAIL_FOR_CANADIAN_MEMBERS,
                                brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
                    if (enforceCanadianAutoOptOut)
                    {
                        AdjustMailAttributesForCanadianMembers(member, brand, attributeValues);
                    }

                    var requiredAttributeCollector = new Dictionary<string, object>();
                    SetRequiredMemberAttributes(member, brand, requiredAttributeCollector);
                    SetSuppliedMemberAttributes(member, brand, attributeValues);
                    LogRegisterAttributes(attributeValues, requiredAttributeCollector, registrationSessionID);
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogError("MEMBER_SVC", "MemberServiceSA", ex,
                        "Error occurred while setting attributes. Session: " + registrationSessionID);
                    //hit an error, go ahead and save whatever attributes have been set
                    SaveMember(member);
                    throw (new SAException(
                        "Error occurred while attempting to set new member attributes. (uri: " + uri + ")", ex));
                }

                var passedFraud = PassedFraud(member, brand, emailAddress, attributeValues, ipAddress);
                RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "PassedFraud: " + passedFraud.ToString(), registrationSessionID);

                #region KOUNT FRAUD CHECK

                //Do the Kount fraud check if the setting is enabled for that site 
                if ((RuntimeSettings.GetSetting("ENABLE_REG_KOUNT_FRAUD_CHECK", Constants.NULL_INT, brand.Site.SiteID).ToLower() == "true"))
                {
                    RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "Begin Kount fraud check ", null);
                    var passedKountFraudCheck = DoKountFraudCheck(member, brand, emailAddress, attributeValues, username, ipAddress, registrationSessionID);
                    //Log the result
                    RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "passedKountFraudCheck: " + passedKountFraudCheck, registrationSessionID);

                    ////Consider the Kount fraud check response if the setting is enabled for that site and update the flag
                    if (RuntimeSettings.GetSetting("ENABLE_REG_KOUNT_FRAUD_RESPONSE_CHECK", Constants.NULL_INT, brand.Site.SiteID).ToLower() == "true")
                    {
                        RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "Consider Kount Fraud Check Response ", null);
                        passedFraud = passedKountFraudCheck;
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "Ignore Kount Fraud Check Response ", null);
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "Kount Fraud check is not turned on for Site ID:", brand.Site.SiteID);
                }
                
                #endregion KOUNT FRAUD CHECK


                if (!passedFraud)
                {
                    //suspend user
                    var globalStatusMask = (GlobalStatusMask) member.GetAttributeInt(brand, "GlobalStatusMask");
                    globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                    member.SetAttributeInt(brand, "GlobalStatusMask", (Int32) globalStatusMask);
                    member.SetAttributeInt(brand, "LastSuspendedAdminReasonID", (Int32) AdminActionReasonID.ROLF);
                    memberRegisterResult.RegisterStatus = RegisterStatusType.FailedFraud;
                }

                SaveMember(member);

                //8-30-12 I'm commenting this out for now because the only client for this, the new reg site, does a call to Authenticate via the
                //via the GetOrCreateAccessTokenUsingPassword method in the API, which was leading to two logins being recorded for every reg. 
                //SaveMemberLastLogon(member.MemberID, brand.BrandID, Constants.NULL_INT);

                RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA",
                    "Member saved, register complete", registrationSessionID);

                if (!passedFraud)
                {
                    ExpireCachedMember(member.MemberID);
                }
            }

            return memberRegisterResult;
        }

        private void LogRegisterAttributes(Dictionary<string, object> attributeValues,
            Dictionary<string, object> requiredAttributeCollector, string registrationSessionID)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Member Attribute Dump");
            foreach (var attribute in attributeValues)
            {
                var value = attribute.Key.ToLower() == "password" ? string.Empty : attribute.Value.ToString();
                sb.AppendLine(attribute.Key + ": " + value);
            }
            foreach (var attribute in requiredAttributeCollector)
            {
                var value = attribute.Key.ToLower() == "password" ? string.Empty : attribute.Value.ToString();
                sb.AppendLine(attribute.Key + ": " + value);
            }

            RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", sb.ToString(),
                registrationSessionID);
        }

        private void AdjustMailAttributesForCanadianMembers(Member member, Brand brand,
            Dictionary<string, object> attributeValues)
        {
            if (attributeValues.ContainsKey("RegionID"))
            {
                var regionID = Convert.ToInt32(attributeValues["RegionID"].ToString());
                var isCanadian = RegionSA.Instance.IsRegionInCountry(regionID, brand.Site.LanguageID,
                    CountryConstants.CANADA);
                if (isCanadian)
                {
                    attributeValues.Add("AlertEmailMask", PROFILE_VIEWED_EMAIL_OPT_OUT);
                    attributeValues.Add("MatchNewsletterPeriod", 0);
                    attributeValues.Add("GotAClickEmailPeriod", 0);
                    if (attributeValues.ContainsKey("NewsletterMask"))
                    {
                        attributeValues["NewsletterMask"] = 0;
                    }
                    else
                    {
                        attributeValues.Add("NewsletterMask", 0);
                    }
                }
            }
        }

        private void SetRequiredMemberAttributes(Member member, Brand brand, Dictionary<string, object> attributeValues)
        {
            DateTime now = DateTime.Now;
            member.SetAttributeDate(brand, "BrandInsertDate", now);
            attributeValues.Add("BrandInsertDate", now);

            member.SetAttributeDate(brand, "LastUpdated", now);
            attributeValues.Add("LastUpdated", now);

            member.SetAttributeDate(brand, "BrandLastLogonDateOldValue",
                member.GetAttributeDate(brand, "BrandLastLogonDate"));
            attributeValues.Add("BrandLastLogonDateOldValue", member.GetAttributeDate(brand, "BrandLastLogonDate"));

            member.SetAttributeDate(brand, "BrandLastLogonDate", now);
            attributeValues.Add("BrandLastLogonDate", now);

            member.SetAttributeInt(brand, "LastBrandID", brand.BrandID);
            attributeValues.Add("LastBrandID", brand.BrandID);

            Int32 brandLogonCount = member.GetAttributeInt(brand, "BrandLogonCount", 0);
            member.SetAttributeInt(brand, "BrandLogonCount", brandLogonCount + 1);
            attributeValues.Add("BrandLogonCount", brandLogonCount + 1);

            // photo requirement related logging
            bool photoRequiredSite =
                Convert.ToBoolean(SettingsService.GetSettingFromSingleton("PHOTO_REQUIRED_SITE",
                    brand.Site.Community.CommunityID,
                    brand.Site.SiteID));
            if (photoRequiredSite)
            {
                DateTime rebrandLogonDate = member.GetAttributeDate(brand, "RebrandFirstLogonDate", DateTime.MinValue);
                if (rebrandLogonDate == DateTime.MinValue)
                {
                    member.SetAttributeDate(brand, "RebrandFirstLogonDate", now);
                    attributeValues.Add("RebrandFirstLogonDate", now);
                    // this attribute is immutable so it's safe to set it multiple times
                }
            }

            // Let's flip the FTAApproved status to false here so that they have to go through the FTA approval check
            member.SetAttributeInt(brand, "FTAApproved", 0);
            attributeValues.Add("FTAApproved", 0);
        }

        private void SetSuppliedMemberAttributes(Member member, Brand brand, Dictionary<string, object> attributeValues)
        {
            foreach (var attribute in attributeValues)
            {
                var mdAttribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attribute.Key);

                switch (mdAttribute.DataType.ToString().ToUpper())
                {
                    case "MASK":
                    case "NUMBER":
                        var intVal = Constants.NULL_INT;
                        if (attribute.Value is Int64)
                        {
                            intVal = (int) (long) (attribute.Value);
                                // direct cast to int would fail, need to cast to long first
                        }
                        else if (attribute.Value is Int32)
                        {
                            intVal = (int) (attribute.Value);
                        }

                        member.SetAttributeInt(brand, attribute.Key, intVal);
                        break;
                    case "TEXT":
                        var mdAttributeGroup =
                            AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(mdAttribute, brand);
                        var textValue = attribute.Value as string;
                        var requiresApproval = (((int) mdAttributeGroup.Status) &
                                                ((int)
                                                    Content.ValueObjects.AttributeMetadata.StatusType
                                                        .RequiresTextApproval)) ==
                                               ((int)
                                                   Content.ValueObjects.AttributeMetadata.StatusType
                                                       .RequiresTextApproval);
                        var textStatusType = requiresApproval ? TextStatusType.None : TextStatusType.Auto;

                        member.SetAttributeText(brand, attribute.Key, textValue, textStatusType);

                        break;
                    case "BIT":
                        var boolVal = ((bool) attribute.Value) ? 1 : 0;
                        member.SetAttributeInt(brand, attribute.Key, boolVal);
                        break;
                    case "DATE":
                        DateTime dateTimeAttribute;
                        DateTime.TryParse(Convert.ToString(attribute.Value), out dateTimeAttribute);
                        member.SetAttributeDate(brand, attribute.Key, dateTimeAttribute);
                        break;
                }
            }
        }

        private bool PassedFraud(Member member, Brand brand, string emailAddress,
            Dictionary<string, object> attributeValues,  int ipAddress)
        {
            ROLFResponse response = new ROLFResponse();

            try
            {
                //first, read the values into a case insensitive dictionary so that we don't miss values because of casing issues
                var caseNeutralAttributeValues =
                    new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                foreach (var attribute in attributeValues)
                {
                    caseNeutralAttributeValues.Add(attribute.Key, attribute.Value);
                }

                //now read out the necessary values
                var genderMask = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("GenderMask") &&
                    caseNeutralAttributeValues["GenderMask"] is int)
                {
                    genderMask = Conversion.CInt(caseNeutralAttributeValues["gendermask"]);
                }

                var maritalStatusInt = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("MaritalStatus") &&
                    caseNeutralAttributeValues["MaritalStatus"] is int)
                {
                    maritalStatusInt = Conversion.CInt(caseNeutralAttributeValues["MaritalStatus"]);
                }

                var regionID = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("RegionID") && caseNeutralAttributeValues["RegionID"] is int)
                {
                    regionID = Conversion.CInt(caseNeutralAttributeValues["RegionID"]);
                }

                var religionInt = Constants.NULL_INT;
                if (brand.Site.Community.CommunityID == JDATE_COMMUNITY_ID &&
                    caseNeutralAttributeValues.ContainsKey("JDateReligion") &&
                    caseNeutralAttributeValues["JDateReligion"] is int)
                {
                    religionInt = Conversion.CInt(caseNeutralAttributeValues["JDateReligion"]);
                }
                else if (brand.Site.Community.CommunityID != JDATE_COMMUNITY_ID &&
                         caseNeutralAttributeValues.ContainsKey("Religion") &&
                         caseNeutralAttributeValues["Religion"] is int)
                {
                    religionInt = Conversion.CInt(caseNeutralAttributeValues["Religion"]);
                }

                var ethnicityInt = Constants.NULL_INT;
                if (brand.Site.Community.CommunityID == JDATE_COMMUNITY_ID &&
                    caseNeutralAttributeValues.ContainsKey("JDateEthnicity") &&
                    caseNeutralAttributeValues["JDateEthnicity"] is int)
                {
                    ethnicityInt = Conversion.CInt(caseNeutralAttributeValues["JDateEthnicity"]);
                }
                else if (brand.Site.Community.CommunityID != JDATE_COMMUNITY_ID &&
                         caseNeutralAttributeValues.ContainsKey("Ethnicity") &&
                         caseNeutralAttributeValues["Ethnicity"] is int)
                {
                    ethnicityInt = Conversion.CInt(caseNeutralAttributeValues["Ethnicity"]);
                }

                var prmInt = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("PromotionID") &&
                    caseNeutralAttributeValues["PromotionID"] is int)
                {
                    prmInt = Conversion.CInt(caseNeutralAttributeValues["PromotionID"]);
                }

                var firstName = caseNeutralAttributeValues.ContainsKey("SiteFirstName")
                    ? caseNeutralAttributeValues["SiteFirstName"]
                    : string.Empty;

                var lastName = caseNeutralAttributeValues.ContainsKey("SiteLastName")
                    ? caseNeutralAttributeValues["SiteLastName"]
                    : string.Empty;

                var colorCodeAnswers = caseNeutralAttributeValues.ContainsKey("ColorCodeQuizAnswers")
                    ? caseNeutralAttributeValues["ColorCodeQuizAnswers"].ToString()
                    : string.Empty;

                var luggage = caseNeutralAttributeValues.ContainsKey("Luggage")
                    ? caseNeutralAttributeValues["Luggage"]
                    : string.Empty;


                var rolfClient = new MingleROLFAdapter(member.MemberID, brand.BrandID, brand.Site.SiteID,
                    brand.Site.Community.CommunityID, emailAddress)
                {
                    RegIP = ipAddress,
                    GenderMask = genderMask,
                    FirstName = firstName.ToString(),
                    LastName = lastName.ToString(),
                    MaritalStatus = GetMaritalStatusDescription(maritalStatusInt),
                    Ethnicity = GetDescriptions("JDateEthnicity", ethnicityInt, brand),
                    Religion = GetDescription("JDateReligion", religionInt, brand)
                };

                if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_COLORCODE", brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID)))
                {
                    rolfClient.ColorCode = colorCodeAnswers == "0" ? string.Empty : colorCodeAnswers;
                }

                var region = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);
                var postalRegion = RegionSA.Instance.RetrieveRegionByID(region.PostalCodeRegionID, brand.Site.LanguageID);
                var countryRegion = RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, brand.Site.LanguageID);
                rolfClient.PostalCode = postalRegion.Description;
                rolfClient.Country = countryRegion.Description;

                rolfClient.LGID = luggage.ToString();
                rolfClient.PRM = (prmInt == Constants.NULL_INT) ? "" : Convert.ToString(prmInt);

                response = rolfClient.GetROLFResponse();

                member.SetAttributeInt(brand, "ROLFScore", response.Code);
                member.SetAttributeText(brand, "ROLFScoreDescription", response.Description, TextStatusType.Auto);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogError("MEMBER_SVC", "MemberServiceSA", ex, "Error in ROLF call");
            }

            return response.Passed;
        }


        private bool DoKountFraudCheck(Member member, Brand brand, string emailAddress,
           Dictionary<string, object> attributeValues, string userName, int ipAddress, string sessionID)
        {
            var registrationRiskData = new RegistrationRiskInquiryData();
            // Make sure Kount should not break any registrations
            bool _passedKountFraudCheck = true;
            try
            {
                //first, read the values into a case insensitive dictionary so that we don't miss values because of casing issues
                var caseNeutralAttributeValues = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                foreach (var attribute in attributeValues)
                {
                    caseNeutralAttributeValues.Add(attribute.Key, attribute.Value);
                }

                #region UDFS
                //now read out the necessary values
                //1.       USERNAME
                registrationRiskData.UserName = string.IsNullOrEmpty(userName)? string.Empty: userName;
                //2.       AGE
                registrationRiskData.DateOfBirth = Constants.NULL_STRING; 
                if (caseNeutralAttributeValues.ContainsKey("Birthdate") && caseNeutralAttributeValues["Birthdate"] is string)
                {
                    registrationRiskData.DateOfBirth = (caseNeutralAttributeValues["Birthdate"]).ToString();
                    //TODO: check on age
                    registrationRiskData.Age = CalculateAge(registrationRiskData.DateOfBirth);
                }

                //3.       MARITALSTATUS
                if (caseNeutralAttributeValues.ContainsKey("MaritalStatus") && caseNeutralAttributeValues["MaritalStatus"] is int)
                {
                    registrationRiskData.MaritalStatus = GetDescription("MaritalStatus", Conversion.CInt(caseNeutralAttributeValues["MaritalStatus"]), brand);
                }
                //4.       OCCUPATION
                if (caseNeutralAttributeValues.ContainsKey("OccupationDescription") && caseNeutralAttributeValues["OccupationDescription"] is string)
                {
                    registrationRiskData.Occupation = caseNeutralAttributeValues["OccupationDescription"].ToString();
                }
              

                //5.       EDUCATION
                if (caseNeutralAttributeValues.ContainsKey("EducationLevel") && caseNeutralAttributeValues["EducationLevel"] is int)
                {
                    registrationRiskData.Education = GetDescription("EducationLevel", Conversion.CInt(caseNeutralAttributeValues["EducationLevel"]), brand);
                }

                //6.       RELIGION
                var religionInt = Constants.NULL_INT;
                if (brand.Site.Community.CommunityID == JDATE_COMMUNITY_ID && caseNeutralAttributeValues.ContainsKey("JDateReligion") && caseNeutralAttributeValues["JDateReligion"] is int)
                {
                    religionInt = Conversion.CInt(caseNeutralAttributeValues["JDateReligion"]);
                    registrationRiskData.Religion = GetDescription("JDateReligion", religionInt, brand);
                }
                else if (brand.Site.Community.CommunityID != JDATE_COMMUNITY_ID && caseNeutralAttributeValues.ContainsKey("Religion") && caseNeutralAttributeValues["Religion"] is int)
                {
                    religionInt = Conversion.CInt(caseNeutralAttributeValues["Religion"]);
                    registrationRiskData.Religion = GetDescription("Religion", religionInt, brand);
                }

                //7.	ETHNICITY
                if (brand.Site.Community.CommunityID == JDATE_COMMUNITY_ID && caseNeutralAttributeValues.ContainsKey("JDateEthnicity") && caseNeutralAttributeValues["JDateEthnicity"] is int)
                {
                    registrationRiskData.Ethnicity = GetDescription("JDateEthnicity", Conversion.CInt(caseNeutralAttributeValues["JDateEthnicity"]), brand);
                }
                else if (brand.Site.Community.CommunityID != JDATE_COMMUNITY_ID && caseNeutralAttributeValues.ContainsKey("Ethnicity") && caseNeutralAttributeValues["Ethnicity"] is int)
                {
                    registrationRiskData.Ethnicity = GetDescription("Ethnicity", Conversion.CInt(caseNeutralAttributeValues["Ethnicity"]), brand);
                }
                //8.	ABOUTME
                if (caseNeutralAttributeValues.ContainsKey("AboutMe") && caseNeutralAttributeValues["AboutMe"] is string)
                {
                    // Allows only upto 255 Chars Max
                    var aboutMe = caseNeutralAttributeValues["AboutMe"].ToString();
                    registrationRiskData.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);
                }
                
                //9.	CAMPAIGNSRC : Depricated

                //10.	CAMPAIGNCAT : Renamed to PromotionID
                if (caseNeutralAttributeValues.ContainsKey("PromotionID") && caseNeutralAttributeValues["PromotionID"] is int)
                {
                    registrationRiskData.PromotionID = (Conversion.CInt(caseNeutralAttributeValues["PromotionID"])).ToString();
                }

                var firstName = caseNeutralAttributeValues.ContainsKey("SiteFirstName")? caseNeutralAttributeValues["SiteFirstName"]: string.Empty;
                var lastName = caseNeutralAttributeValues.ContainsKey("SiteLastName")? caseNeutralAttributeValues["SiteLastName"]: string.Empty;
                //TODO: check right pattern
                registrationRiskData.Name = firstName + " " + lastName;

                //11.   EYES
                if (caseNeutralAttributeValues.ContainsKey("EyeColor") && caseNeutralAttributeValues["EyeColor"] is int)
                {
                    registrationRiskData.Eyes = GetDescription("EyeColor", Conversion.CInt(caseNeutralAttributeValues["EyeColor"]), brand);
                }
                
                //12.   HAIR
                if (caseNeutralAttributeValues.ContainsKey("HairColor") && caseNeutralAttributeValues["HairColor"] is int)
                {
                    registrationRiskData.Hair = GetDescription("EyeColor", Conversion.CInt(caseNeutralAttributeValues["HairColor"]), brand);
                }
                 
                //13.   HEIGHT
                if (caseNeutralAttributeValues.ContainsKey("Height") && caseNeutralAttributeValues["Height"] is int)
                {
                    registrationRiskData.Height = (Conversion.CInt(caseNeutralAttributeValues["Height"])).ToString();
                }
                else if (caseNeutralAttributeValues.ContainsKey("Heightcm") && caseNeutralAttributeValues["Heightcm"] is int)
                {
                    registrationRiskData.Height = (Conversion.CInt(caseNeutralAttributeValues["Heightcm"])).ToString();
                }

                #endregion UDF

                //Compile other KOUNT fields
                registrationRiskData.Email = emailAddress;
                registrationRiskData.CustomerID = member.MemberID.ToString();
                registrationRiskData.SessionID = sessionID.Replace("-", "");
				registrationRiskData.DateOfRegistration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                var genderMask = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("GenderMask") && caseNeutralAttributeValues["GenderMask"] is int)
                {
                    genderMask = Conversion.CInt(caseNeutralAttributeValues["gendermask"]);
                    registrationRiskData.Gender = (genderMask == Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";
                }

                var regionID = Constants.NULL_INT;
                if (caseNeutralAttributeValues.ContainsKey("RegionID") && caseNeutralAttributeValues["RegionID"] is int)
                {
                    regionID = Conversion.CInt(caseNeutralAttributeValues["RegionID"]);
                }
                // Set the Address
                var region = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);
                var postalRegion = RegionSA.Instance.RetrieveRegionByID(region.PostalCodeRegionID, brand.Site.LanguageID);
                var countryRegion = RegionSA.Instance.RetrieveRegionByID(region.CountryRegionID, brand.Site.LanguageID);
                registrationRiskData.AddressPostalCode = postalRegion.Description;
                registrationRiskData.AddressCountry = countryRegion.Abbreviation.Trim();
                registrationRiskData.AddressState = (RegionSA.Instance.RetrieveRegionByID(region.StateRegionID, brand.Site.LanguageID)).Description;
                registrationRiskData.AddressCity = (RegionSA.Instance.RetrieveRegionByID(region.CityRegionID, brand.Site.LanguageID)).Description;

                registrationRiskData.SiteID = brand.Site.SiteID.ToString();
                registrationRiskData.CallingSystemTypeID = brand.Site.SiteID.ToString();

                RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "IPAddress:", ipAddress);
                registrationRiskData.IPAddress = new IPAddress(BitConverter.GetBytes(ipAddress)).ToString();
              

                var objJSONRiskInquiryRequestData = new JSONRegistrationRiskInquiryRequestData();
                objJSONRiskInquiryRequestData.registrationRiskInquiryRequestData = registrationRiskData.SerializeRiskInquiryData;
                
                //logging risk data
                RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "registrationRiskInquiryRequestData", objJSONRiskInquiryRequestData.registrationRiskInquiryRequestData);
                RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "Invoke fraud DoRegistrationRiskInquiry", null);

               // var fraudServiceAdapter = new FraudServiceWebAdapter();
                var riskInquiryResponse = FraudServiceWebAdapter.GetProxyInstance().DoRegistrationRiskInquiry(objJSONRiskInquiryRequestData.registrationRiskInquiryRequestData);

                if (riskInquiryResponse != null)
                {
                    if (riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.A
                        || riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.R)
                    {
                        //Acceptable RISK
                        member.SetAttributeInt(brand, "KOUNTScore", Convert.ToInt32(riskInquiryResponse.RiskInquiryScore));
                        member.SetAttributeInt(brand, "KOUNTAutomatedRiskInquiryStatusID", Convert.ToInt32(riskInquiryResponse.AutomateRiskInquiryStatus));
                        _passedKountFraudCheck = true;
                    }
                    else if (riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.D)
                    {
                        // Declined
                        member.SetAttributeInt(brand, "KOUNTScore", Convert.ToInt32(riskInquiryResponse.RiskInquiryScore));
                        member.SetAttributeInt(brand, "KOUNTAutomatedRiskInquiryStatusID", Convert.ToInt32(riskInquiryResponse.AutomateRiskInquiryStatus));
                        _passedKountFraudCheck = false;

                    }
                    else if (riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.E
                             || riskInquiryResponse.AutomateRiskInquiryStatus == Spark.Common.FraudService.AutomatedRiskInquiryStatus.None)
                    {
                        RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "The fraud response has Error code. Allow the registration process to continue.", null);
                        // In this case still allow the Registration to process 
                        _passedKountFraudCheck = true;
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage("MEMBER_SVC", "MemberServiceSA", "The fraud response was NULL. Allow the registration process to continue.",null);
                }

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogError("MEMBER_SVC", "MemberServiceSA", ex, "Error in KOUNT call");
            }

            return _passedKountFraudCheck;
        }

        private string CalculateAge(string birthDate)
        {
            var dtBirthDate = Convert.ToDateTime(birthDate);
            DateTime dtToday = DateTime.Now;

            // get the difference in year
            int years = dtToday.Year - dtBirthDate.Year;
            // subtract another year if we're before the
            // birth day in the current year
            if (dtToday.Month < dtBirthDate.Month || (dtToday.Month == dtBirthDate.Month && dtToday.Day < dtBirthDate.Day))
                years = years - 1;

            return years.ToString();
        }
        
        
        private string GetMaritalStatusDescription(int optionID)
        {
            if (optionID == Constants.NULL_INT) return string.Empty;

            var options = AttributeOptionSA.Instance.GetAttributeOptionCollection(
                "MaritalStatus", 8383);

            if ((from AttributeOption o in options where o.AttributeOptionID == optionID select o).ToList().Count == 0)
                return string.Empty;

            var description =
                (from AttributeOption o in options where o.AttributeOptionID == optionID select o).First().Description;

            return description;
        }

        private List<string> GetDescriptions(string attributeName, int attributeMaskValue, Brand brand)
        {
            var descriptions = new List<string>();

            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return descriptions;

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(attributeMaskValue))
                {
                    descriptions.Add(attributeOption.Description);
                }
            }

            return descriptions;
        }

        public string GetDescription(string attributeName, int attributeValue, Brand brand)
        {
            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            var attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption != null)
                return attributeOption.Description;

            return string.Empty;
        }

        private class ValidateAttributeResult
        {
            public bool Validated { get; set; }
            public string ErrorMessage { get; set; }
        }

        /// <summary>
        /// Gets the member's device that was used to register
        /// </summary>
        /// <param name="member"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public DeviceOS GetRegisteredDevice(Matchnet.Member.ServiceAdapters.Interfaces.IMember member,
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            DeviceOS result = DeviceOS.None;

            try
            {
                //first check - previous determination
                string trackingRegDevice = member.GetAttributeText(brand, "TrackingRegDevice");
                if (!string.IsNullOrEmpty(trackingRegDevice))
                {
                    result = (DeviceOS) Enum.Parse(typeof (DeviceOS), trackingRegDevice);
                }

                if (result == DeviceOS.None || result == DeviceOS.Unknown)
                {
                    //second check - registration info
                    string trackingRegFormFactor = member.GetAttributeText(brand, "TrackingRegFormFactor");
                    string trackingRegOS = member.GetAttributeText(brand, "TrackingRegOS");
                    if (!string.IsNullOrEmpty(trackingRegFormFactor) && !string.IsNullOrEmpty(trackingRegOS))
                    {
                        bool isMobile = false;
                        bool isTablet = false;

                        StringReader sr = new StringReader(trackingRegFormFactor);
                        System.Xml.XPath.XPathDocument xmlDoc = new System.Xml.XPath.XPathDocument(sr);
                        System.Xml.XPath.XPathNavigator xmlNav = xmlDoc.CreateNavigator();
                        System.Xml.XPath.XPathNavigator deviceNode = xmlNav.SelectSingleNode("DeviceProperties");
                        if (deviceNode != null)
                        {
                            System.Xml.XPath.XPathNavigator isMobileDeviceNode =
                                deviceNode.SelectSingleNode("IsMobileDevice");
                            if (isMobileDeviceNode != null && !string.IsNullOrEmpty(isMobileDeviceNode.Value) &&
                                isMobileDeviceNode.Value.ToLower() == "true")
                            {
                                isMobile = true;
                            }

                            System.Xml.XPath.XPathNavigator isTabletNode = deviceNode.SelectSingleNode("IsTablet");
                            if (isTabletNode != null && !string.IsNullOrEmpty(isTabletNode.Value) &&
                                isTabletNode.Value.ToLower() == "true")
                            {
                                isTablet = true;
                            }
                        }

                        trackingRegOS = trackingRegOS.ToLower();
                        if (isTablet || isMobile)
                        {
                            //mobile
                            if (trackingRegOS.Contains("iphone") || trackingRegOS.Contains("apple"))
                            {
                                if (isTablet)
                                    result = DeviceOS.IPad;
                                else if (isMobile)
                                    result = DeviceOS.IPhone;
                            }
                            else if (trackingRegOS.Contains("android"))
                            {
                                if (isTablet)
                                    result = DeviceOS.Android_Tablet;
                                else if (isMobile)
                                    result = DeviceOS.Android_Phone;
                            }
                            else if (trackingRegOS.Contains("rim"))
                            {
                                if (isTablet)
                                    result = DeviceOS.Blackberry_Tablet;
                                else if (isMobile)
                                    result = DeviceOS.Blackberry;
                            }
                            else if (trackingRegOS.Contains("linux"))
                            {
                                if (isMobile)
                                    result = DeviceOS.Linux_Phone;
                            }
                            else if (trackingRegOS.Contains("webos"))
                            {
                                if (isTablet)
                                    result = DeviceOS.Palm_Tablet;
                                else if (isMobile)
                                    result = DeviceOS.Palm_Phone;
                            }
                            else if (trackingRegOS.Contains("symbian"))
                            {
                                if (isMobile)
                                    result = DeviceOS.Symbian_Phone;
                            }
                            else if (trackingRegOS.Contains("window") || trackingRegOS.Contains("winnt") ||
                                     trackingRegOS.Contains("winxp") || trackingRegOS.StartsWith("win"))
                            {
                                if (isTablet)
                                    result = DeviceOS.Windows_Tablet;
                                else if (isMobile)
                                    result = DeviceOS.Windows_Phone;
                            }
                            else
                            {
                                result = DeviceOS.Unknown;
                            }
                        }
                        else
                        {
                            //desktop
                            if (trackingRegOS.Contains("apple") || trackingRegOS.Contains("mac"))
                            {
                                result = DeviceOS.Apple_Desktop;
                            }
                            else if (trackingRegOS.Contains("linux"))
                            {
                                result = DeviceOS.Linux_Desktop;
                            }
                            else if (trackingRegOS.Contains("window") || trackingRegOS.Contains("winnt") ||
                                     trackingRegOS.Contains("winxp") || trackingRegOS.StartsWith("win"))
                            {
                                result = DeviceOS.Windows_Desktop;
                            }
                            else
                            {
                                result = DeviceOS.Unknown;
                            }
                        }

                        //save device so we won't have to re-determine each time
                        member.SetAttributeText(brand, "TrackingRegDevice", result.ToString(), TextStatusType.Auto);
                        SaveMember(member);
                    }
                    else
                    {
                        //no registered tracking data
                        result = DeviceOS.None;
                    }
                }
            }
            catch (Exception ex)
            {
                //likely error would be invalid xml structure (if it ever changes)
                RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberServiceSA.GetRegisteredDevice()",
                    ex, null);
            }

            return result;
        }

        private void LogException(string method, Exception ex, int memberID)
        {
            RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberSA." + method,
                ex, memberID);
        }

        /// <summary>
        /// Gets the member's push notifications preferences
        /// </summary>
        /// <param name="memberID"></param>
        public MemberAppDeviceCollection GetMemberAppDevices(int memberID)
        {
            string uri = "";
            MemberAppDeviceCollection memberAppDeviceCollection = null;

            try
            {
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                memberAppDeviceCollection =
                    MemberDTOCache.Get(MemberAppDeviceCollection.GetCacheKey(memberID)) as MemberAppDeviceCollection;

                if (memberAppDeviceCollection == null)
                {

                    //Int32 cacheTTL =
                    //    Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = SMUtil.getServiceManagerUri(memberID);
                    DateTime begin = DateTime.Now;
                    base.Checkout(uri);
                    try
                    {
                        memberAppDeviceCollection = SMUtil.getAppDeviceService(uri).GetMemberAppDevices(memberID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }



                    Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                    if (totalSeconds > 1)
                    {
                        Trace.WriteLine("__GetMemberNotifications(" + memberID.ToString() + ") " +
                                        totalSeconds.ToString());
                    }
                }

                return memberAppDeviceCollection;
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Error occurred while attempting to get Member App Devices. (uri: " + uri + ")", ex);
            }
        }

        public void DeleteMemberAppDevice(int memberID, int appID, string deviceID)
        {
            string uri = "";

            try
            {
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                if (appID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "appID");
                }

                if (string.IsNullOrEmpty(deviceID))
                {
                    throw new ArgumentException("value must not be an empty string", "deviceID");
                }


                uri = SMUtil.getServiceManagerUri(memberID);
                DateTime begin = DateTime.Now;
                base.Checkout(uri);
                try
                {
                    SMUtil.getAppDeviceService(uri).DeleteMemberAppDevice(memberID, appID, deviceID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                if (totalSeconds > 1)
                {
                    Trace.WriteLine("__RemoveMemberAppDevice(" + memberID.ToString() + ") " +
                                    totalSeconds.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Error occurred while attempting to remove MemberAppDevice. (uri: " + uri + ")", ex);
            }
        }

        public void AddMemberAppDevice(Int32 memberID, Int32 appID, string deviceID, int brandID, string deviceData, Int64 pushNotificationsFlags)
        {
            string uri = "";

            try
            {
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                if (appID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "appID");
                }

                if (string.IsNullOrEmpty(deviceID))
                {
                    throw new ArgumentException("value must not be an empty string", "deviceID");
                }


                uri = SMUtil.getServiceManagerUri(memberID);
                DateTime begin = DateTime.Now;
                base.Checkout(uri);
                try
                {
                    SMUtil.getAppDeviceService(uri).AddMemberAppDevice
                        (memberID, appID, deviceID, brandID, deviceData, pushNotificationsFlags);
                }
                finally
                {
                    base.Checkin(uri);
                }

                Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                if (totalSeconds > 1)
                {
                    Trace.WriteLine("__AddMemberAppDevice(" + memberID.ToString() + ") " +
                                    totalSeconds.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Error occurred while attempting to add MemberAppDevice. (uri: " + uri + ")", ex);
            }
        }

        public void UpdateMemberAppDeviceNotificationsFlags(Int32 memberID, Int32 appID, string deviceID, Int64 pushNotificationsFlags)
        {
            string uri = "";

            try
            {
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                if (appID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "appID");
                }

                if (string.IsNullOrEmpty(deviceID))
                {
                    throw new ArgumentException("value must not be an empty string", "deviceID");
                }


                uri = SMUtil.getServiceManagerUri(memberID);
                DateTime begin = DateTime.Now;
                base.Checkout(uri);
                try
                {
                    SMUtil.getAppDeviceService(uri)
                        .UpdateMemberAppDeviceNotificationsFlags(memberID, appID, deviceID, pushNotificationsFlags);
                }
                finally
                {
                    base.Checkin(uri);
                }

                Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                if (totalSeconds > 1)
                {
                    Trace.WriteLine("__UpdateMemberAppDevice(" + memberID.ToString() + ") " +
                                    totalSeconds.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Error occurred while attempting to update MemberAppDevice. (uri: " + uri + ")", ex);
            }
        }

        public class MemberDTOComparer : IComparer
        {
            private int[] _ids;
            private bool _reverse = false;


            public MemberDTOComparer(int[] ids, bool reverse)
            {
                _ids = ids;
                _reverse = reverse;
            }

            #region IComparer Members

            public int Compare(object x, object y)
            {
                int idx1 = GetIndex((MemberDTO) x);
                int idx2 = GetIndex((MemberDTO) y);
                int result = (idx1 - idx2);
                return (_reverse) ? -1*result : result;
            }

            #endregion

            private int GetIndex(MemberDTO memberDTO)
            {
                for (int i = 0; i < _ids.Length; i++)
                {
                    if (_ids[i] == memberDTO.MemberID)
                    {
                        return i;
                    }
                }
                return -1;
            }

        }

    }
}
