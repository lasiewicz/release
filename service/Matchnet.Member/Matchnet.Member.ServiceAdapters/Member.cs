using System;
using System.Collections;
using System.Linq;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.Photos;
using Spark.Common.AccessService;
using System.Collections.Generic;


namespace Matchnet.Member.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
    public class Member : IMember, IMemberDTO
	{

		private CachedMember _cachedMember;
		private MemberUpdate _memberUpdate = null;
		private Attributes _attributes;

        private CachedMemberLogon _cachedMemberLogon;
        private CachedMemberEmail _cachedMemberEmail;

        //Unified Access privileges
        private CachedMemberAccess _cachedMemberAccess;

		private const int GROUP_PERSONALS			= 8383;
		private const int NEW_DAY_LIMIT				= 14;
		private const int UPDATED_DAY_LIMIT			= 14;
		private const string LAST_UPDATED_ATTR		= "LastUpdated";
		private const string BRAND_INSERT_DATE_ATTR	= "BrandInsertDate";

		public Member(CachedMember cachedMember)
		{
			_cachedMember = cachedMember;
		}

#if DEBUG		  
	    public bool Use_Nunit { get; set; }
	    public CachedMember GetCachedMember()
	    {
	        CachedMember cm = null;
            if (Use_Nunit)
            {
                cm = this._cachedMember;
            }
	        return cm;
	    }
        public CachedMemberAccess GetCachedMemberAccess()
        {
            CachedMemberAccess cma = null;
            if (Use_Nunit)
            {
                cma = _cachedMemberAccess;
            }
            return cma;
        }
#endif

		public MemberUpdate MemberUpdate
		{
			get
			{
				if (_memberUpdate == null)
				{
					_memberUpdate = new MemberUpdate(this.MemberID);
				}
				return _memberUpdate;
			}
		}


		public bool IsDirty
		{
			get
			{
				if (_memberUpdate == null)
				{
					return false;
				}

				return _memberUpdate.IsDirty;
			}
		}


		internal CachedMember CachedMember
		{
			set
			{
				_cachedMember = value;
			}
		}

        /// <summary>
        /// Returns back the CachedMemberAccess, which contains AccessPrivileges from Unified Access
        /// </summary>
        internal CachedMemberAccess CachedMemberAccess
        {
            get { return _cachedMemberAccess; }
            set { _cachedMemberAccess = value; }
        }


		private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attributes
		{
			get
			{
				if (_attributes == null)
				{
					_attributes = AttributeMetadataSA.Instance.GetAttributes();
				}

				return _attributes;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get
			{
				return _cachedMember.MemberID;
			}
		}


        ///// <summary>
        ///// 
        ///// </summary>
        //public string Username
        //{
        //    get
        //    {
        //        return _cachedMember.Username;
				
        //    }
        //}


		/// <summary>
		/// PM-218 Grab a username given the community scope. It no longers retrieves from logonmember table.
		/// </summary>
		/// <param name="brand"></param>
		/// <returns></returns>
		public string GetUserName(Brand brand)
		{
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME);
            string username = "";
			
			if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNAME_APPROVAL_FLAG",brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID).ToLower() == "true")
			{
				// To do - This should be te proper call, but it's causing issues like telling the username is not available after updating.
				username =  this.GetAttributeTextApproved(brand, brand.Site.LanguageID, "UserName", username, this.MemberID.ToString());
			}
			else
			{
				username = this.GetAttributeText(brand.Site.Community.CommunityID,
					brand.Site.SiteID, 
					brand.BrandID,
					brand.Site.LanguageID,
					"UserName");
			}
            if (username == "")
            {
                if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNAME_APPROVAL_FLAG", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID).ToLower() == "true")
                {
                    // To do - This should be te proper call, but it's causing issues like telling the username is not available after updating.
                    username = this.GetAttributeTextApproved(brand, Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH, "UserName", username, this.MemberID.ToString());
                }
                else
                {
                    username = this.GetAttributeText(brand.Site.Community.CommunityID,
                        brand.Site.SiteID,
                        brand.BrandID,
                        Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH,
                        "UserName");
                }
            }

            if (username == "")
            {
                //if username is STILL blank, it's most likely because the username is saved under a different language
                //than the language of the brand being passed in (ie, French username from a member that turned up in a
                //search of paris from Jdate.com instead of Jdate.fr). In this case return the memberid. 
                username = this.MemberID.ToString();
            }

            return username;
			
		}
		
		/// <summary>
		/// Changed again back to using language default of english. Also made changes to free text approval
		/// so it pulls UserName attribute with English languageID. 2/25/09
		/// Old Note: 1.9 Release TT#19065 - Username should not have a default languageID value of English which the currenty property Username is doing.
		/// </summary>
		/// <param name="userName"></param>
		/// <param name="brand"></param>
		public void SetUsername(string userName, Brand brand)
		{
			SetUsername(userName, brand, TextStatusType.None);
		}

		public void SetUsername(string userName, Brand brand, TextStatusType textStatType)
		{
			if (userName == this.MemberID.ToString() || userName != this.GetUserName(brand))
			{
				this.MemberUpdate.Username = userName;

				if (AttributeMetadataSA.Instance.GetAttributes().GetAttribute("Username").Scope != ScopeType.Community)
				{
					// Write Global
					this.setAttributeText(brand.Site.Community.CommunityID,
						Constants.NULL_INT,
						Constants.NULL_INT,
						brand.Site.LanguageID,
						attributes.GetAttribute(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME),
						userName.Trim(),
						textStatType);
					// Write Community
					this.setAttributeText(brand.Site.Community.CommunityID,
						Constants.NULL_INT,
						Constants.NULL_INT,
                        brand.Site.LanguageID,
						attributes.GetAttribute(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME),
                        userName.Trim(),
						textStatType,
						true);
				}
				else
				{
					this.setAttributeText(brand.Site.Community.CommunityID,
						Constants.NULL_INT,
						Constants.NULL_INT,
                        brand.Site.LanguageID,
						attributes.GetAttribute(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME),
                        userName.Trim(),
						textStatType);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _cachedMember.EmailAddress;
			}
			set
			{
				if (value != _cachedMember.EmailAddress && !string.IsNullOrEmpty(value.Trim()))
				{
					this.MemberUpdate.EmailAddress = value;
					this.setAttributeText(Constants.NULL_INT,
						Constants.NULL_INT,
						Constants.NULL_INT,
						Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH,
						attributes.GetAttribute(Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_EMAILADDRESS),
						value,
						TextStatusType.Auto);
				}
			}
		}
		

		internal void setLogon(string emailAddress, string username)
		{
			_cachedMember.EmailAddress = emailAddress;
			_cachedMember.Username = username;
		}


		/// <summary>
		/// 
		/// </summary>
		public string Password
		{
			set
			{
				this.MemberUpdate.Password = value;
                this.MemberUpdate.PasswordHash = value;
			}
		}

		
		/// <summary>
		/// Although brand is passed in, the community of the brand is used in this function.  Looks
		/// through the BrandInsertDate of all the brands that the member belongs to within a community,
		/// then determines if this user is new to the community.
		/// </summary>
		/// <param name="communityID">Brands under this CommunityID are only considered.</param>
		/// <returns></returns>
		public bool IsNewMember(Int32 communityID)
		{
			DateTime insertDate = DateTime.MinValue;
			DateTime tempInsertDate = DateTime.MinValue;

			ArrayList siteIDs = getSiteIDList();
            Brands aBrands = BrandConfigSA.Instance.GetBrands();
			
			// Cycle through all the aBrands that the member is a part of, then
			// pick off the lowest value of BrandInsertDate.  This will be the community insert date.
            foreach(Brand aBrand in aBrands)
			{
				// We only care about brands that are in the community of the brand that was passed in
				if(aBrand.Site.Community.CommunityID == communityID)
				{
					// only care about the sites that the member is a part of
					if(siteIDs.Contains(aBrand.Site.SiteID)) 
					{
						tempInsertDate = GetAttributeDate(aBrand, BRAND_INSERT_DATE_ATTR, DateTime.MinValue);
						
						// only care if a meaningful date came back
						if(tempInsertDate != DateTime.MinValue) 
						{
							// updateDate hasn't been set yet? then just set it without the less than compare
							if(insertDate == DateTime.MinValue)
							{
								insertDate = tempInsertDate;
							}
							else
							{
								if(tempInsertDate < insertDate)
									insertDate = tempInsertDate;
							}
						}
					}
				}
			}

			// Was community insert date ever found?
			if(insertDate != DateTime.MinValue)
			{
				if (DateTime.Now.Subtract(insertDate).TotalDays <= NEW_DAY_LIMIT)
				{
					return true;
				}
			}

			return false;
		}

        /// <summary>
        /// Although brand is passed in, the community of the brand is used in this function.  Looks
        /// through the BrandInsertDate of all the brands that the member belongs to within a community,
        /// then determines if this user is registered with the community within given days.
        /// </summary>
        /// <param name="communityID">Brands under this CommunityID are only considered.</param>
        /// <param name="days">Number of days pas registration</param>
        /// <returns></returns>
        public bool IsNewMember(Int32 communityID, int days)
        {
            DateTime insertDate = DateTime.MinValue;
            DateTime tempInsertDate = DateTime.MinValue;

            ArrayList siteIDs = getSiteIDList();
            Brands aBrands = BrandConfigSA.Instance.GetBrands();

            // Cycle through all the aBrands that the member is a part of, then
            // pick off the lowest value of BrandInsertDate.  This will be the community insert date.
            foreach (Brand aBrand in aBrands)
            {
                // We only care about brands that are in the community of the brand that was passed in
                if (aBrand.Site.Community.CommunityID == communityID)
                {
                    // only care about the sites that the member is a part of
                    if (siteIDs.Contains(aBrand.Site.SiteID))
                    {
                        tempInsertDate = GetAttributeDate(aBrand, BRAND_INSERT_DATE_ATTR, DateTime.MinValue);

                        // only care if a meaningful date came back
                        if (tempInsertDate != DateTime.MinValue)
                        {
                            // updateDate hasn't been set yet? then just set it without the less than compare
                            if (insertDate == DateTime.MinValue)
                            {
                                insertDate = tempInsertDate;
                            }
                            else
                            {
                                if (tempInsertDate < insertDate)
                                    insertDate = tempInsertDate;
                            }
                        }
                    }
                }
            }

            // Was community insert date ever found?
            if (insertDate != DateTime.MinValue)
            {
                if (DateTime.Now.Subtract(insertDate).TotalDays <= days)
                {
                    return true;
                }
            }

            return false;
        }


		/// <summary>
		/// Within the community, it checks to see if the member has made updates recently.
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public bool IsUpdatedMember(Int32 communityID)
		{
			DateTime updateDate = this.GetAttributeDate(communityID,
				Constants.NULL_INT,
				Constants.NULL_INT,
				LAST_UPDATED_ATTR,
				DateTime.MinValue);
			
			if(updateDate != DateTime.MaxValue)
			{
				if ( (DateTime.Now.Subtract(updateDate).TotalDays <= NEW_DAY_LIMIT) )
					return true;
			}
			
			return false;			
		}
        

		private ArrayList getCommunityIDList()
		{
			Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
			Brands brands = BrandConfigSA.Instance.GetBrands();
			return _cachedMember.GetCommunityIDList(attributes, brands);
		}


		private ArrayList getSiteIDList()
		{
			ArrayList sites = new ArrayList();

			Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
			ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributes.GetAttribute("BrandInsertDate").ID);
			Brands brands = BrandConfigSA.Instance.GetBrands();

			for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
			{
				Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
				if (_cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) != DateTime.MinValue)
				{
					Int32 siteID = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID).Site.SiteID;
					if (!sites.Contains(siteID))
					{
						sites.Add(siteID);
					}
				}
			}

			return sites;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int[] GetCommunityIDList()
		{
			return (int[])getCommunityIDList().ToArray(typeof(Int32));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int[] GetSiteIDList()
		{
			return (int[])getSiteIDList().ToArray(typeof(Int32));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public bool IsCommunityMember(int communityID)
		{
			return getCommunityIDList().Contains(communityID);
		}

		public bool IsSiteMember(int siteID)
		{
			return getSiteIDList().Contains(siteID);
		}

        #region Photos

        /// <summary>
        /// Selects a "default" photo object to use as a thumbnail (or whatever). Looks for a photo marked with IsMain first, ApprovedForMain and not private 2nd,
        /// ApprovedForMain and private 3rd.
        /// </summary>
        /// <param name="communityID">The community identifier.</param>
        /// <param name="publicOnly">if set to <c>true</c> [public only].</param>
        /// <returns>
        /// The member's Photo or null if there is no photo.
        /// </returns>
        public Photo GetDefaultPhoto(Int32 communityID, bool publicOnly)
        {
            var photos = GetPhotos(communityID);
            if (photos == null || photos.Count == 0)
                return null;

            //keep track of main photo with lowest list order
            byte curMainListOrder = byte.MaxValue;
            byte curMainIndex = byte.MaxValue;

            //keep track of non-main private photo with lowest list order
            byte curPrivateListOrder = byte.MaxValue;
            byte curPrivateIndex = byte.MaxValue;

            //keep track of non-main public photo with lowest list order
            byte curPublicListOrder = byte.MaxValue;
            byte curPublicIndex = byte.MaxValue;

            for (byte i = 0; i < photos.Count; i++)
            {
                if (!photos[i].IsApproved || !photos[i].IsApprovedForMain) continue;
                if (publicOnly && (photos[i].IsPrivate)) continue;
                if (photos[i].IsMain)
                {
                    //main photo
                    if (photos[i].ListOrder <= 1)
                    {
                        //main photo with list order 1, we're done
                        return photos[i];
                    }

                    if (photos[i].ListOrder >= curMainListOrder) continue;
                    curMainListOrder = photos[i].ListOrder;
                    curMainIndex = i;
                }
                else
                {
                    if (!photos[i].IsPrivate)
                    {
                        //public photo
                        if (photos[i].ListOrder >= curPublicListOrder) continue;
                        curPublicListOrder = photos[i].ListOrder;
                        curPublicIndex = i;
                    }
                    else
                    {
                        //private photo (note: we won't even get here if user passes in public only)
                        if (photos[i].ListOrder >= curPrivateListOrder) continue;
                        curPrivateListOrder = photos[i].ListOrder;
                        curPrivateIndex = i;
                    }
                }
            }

            //main photo
            if (curMainIndex < byte.MaxValue && curMainIndex < photos.Count)
            {
                return photos[curMainIndex];
            }

            //public photo
            if (curPublicIndex < byte.MaxValue && curPublicIndex < photos.Count)
            {
                return photos[curPublicIndex];
            }

            //private photo
            if (curPrivateIndex < byte.MaxValue && curPrivateIndex < photos.Count)
            {
                return photos[curPrivateIndex];
            }

            return null;
        }

        /// <summary>
        /// Retrieves one random photo out of all main-eligible photos.
        /// </summary>
        /// <returns>Photo object or null if none found.</returns>
        public Photo GetRandomApprovedForMainPhoto(int communityId, bool publicOnly)
        {
            var photos = GetApprovedForMainPhotos(communityId, publicOnly);
            if (photos == null || photos.Count == 0)
                return null;

            var random = new Random();
            return photos[random.Next(0, photos.Count - 1)];
        }

        /// <summary>
        /// Gets all photos that approved for main.
        /// </summary>
        /// <returns>List of Photo or null if none found.</Photo></returns>
        public List<Photo> GetApprovedForMainPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if(publicOnly)
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved && photos[i].IsApprovedForMain)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        /// <summary>
        /// Gets any approved photos; including approved photos not suitable for main.
        /// </summary>
        /// <returns>List of Photo or null if none found.</returns>
        public List<Photo> GetApprovedPhotos(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (publicOnly)
                {
                    if (photos[i].IsApproved && !photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    if (photos[i].IsApproved)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        /// <summary>
        /// Gets any approved photos exluding the main photo.
        /// </summary>
        /// <returns>List of Photo or null if none found.</returns>
        public List<Photo> GetApprovedPhotosExcludingMain(int communityId, bool publicOnly)
        {
            var photos = GetPhotos(communityId);
            if (photos == null || photos.Count == 0)
                return null;

            var approvedPhotos = new List<Photo>();
            for (byte i = 0; i < photos.Count; i++)
            {
                if (!photos[i].IsApproved || photos[i].IsMain) continue;

                if (publicOnly)
                {
                    if (!photos[i].IsPrivate)
                    {
                        approvedPhotos.Add(photos[i]);
                    }
                }
                else
                {
                    approvedPhotos.Add(photos[i]);
                }
            }

            approvedPhotos.Sort((p1, p2) => p1.ListOrder.CompareTo(p2.ListOrder));

            return approvedPhotos;
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public PhotoCommunity GetPhotos(Int32 communityID)
		{
			return _cachedMember.Photos.GetCommunity(communityID);
		}


		/// <summary>
		/// Determines whether a person has a main photo that's approved. Renaming this method would be more clear, except it would require deploying of numerous services at once.
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns>True = member has an approved main photo. False = no approved main photo.</returns>
		public bool HasApprovedPhoto(Int32 communityID)
		{
		    return _cachedMember.HasApprovedPhoto(communityID);
		}

        /// <summary>
        /// Determines if the member has any approved photos. This method behaves how HasApprovedPhoto used to. It doesn't not take into account IsApprovedForMain attribute.
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns>True if member has any approved photos, and False if he doesn't</returns>
        public bool HasAnyApprovedPhoto(Int32 communityID)
        {
            return _cachedMember.HasAnyApprovedPhoto(communityID);
        }

        #endregion

        /// <summary>
		/// 
		/// </summary>
		public bool IsAdminUpdated
		{
			get
			{
				return this.MemberUpdate.IsAdminUpdated;
			}
			set
			{
				this.MemberUpdate.IsAdminUpdated = value;
			}
		}

		public void CommitChanges()
		{
			if (this.MemberUpdate.Username != Constants.NULL_STRING)
			{
				_cachedMember.Username = this.MemberUpdate.Username;
			}

			if (this.MemberUpdate.Password != Constants.NULL_STRING)
			{
				_cachedMember.Password = this.MemberUpdate.Password;
			}

			if (this.MemberUpdate.EmailAddress != Constants.NULL_STRING)
			{
				_cachedMember.EmailAddress = this.MemberUpdate.EmailAddress;
			}

			IDictionaryEnumerator de = this.MemberUpdate.AttributesText.GetEnumerator();
			while (de.MoveNext())
			{
				Int32 languageID = (Int32)de.Key;
				Hashtable attributes = de.Value as Hashtable;

				IDictionaryEnumerator deA = attributes.GetEnumerator();
				while (deA.MoveNext())
				{
				    var attributeGroupID = (Int32) deA.Key;
					TextValue textValue = deA.Value as TextValue;

                    /*If a member has accounts on multiple sites within the same community, 
                        username change in one site\language should reflect on all sites\languages within the community.
                    */
                    Attributes attributescoll = AttributeMetadataSA.Instance.GetAttributes();
                    AttributeGroup ag = attributescoll.GetAttributeGroup(attributeGroupID);
                    var attribute = attributescoll.GetAttribute(ag.AttributeID);
                    if (attribute.Name.ToLower() == "username" && textValue.TextStatus == TextStatusType.Human)
                    {
                        //get member's username on other sites/languages within the same community
                        ArrayList attributeGroupIDs = attributescoll.GetAttributeGroupIDs(attributescoll.GetAttribute("BrandInsertDate").ID);
                        Brands brands = BrandConfigSA.Instance.GetBrands();
                        var communityId = attributescoll.GetAttributeGroup(attributeGroupID).GroupID;

                        for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
                        {
                            Int32 agID = (Int32)attributeGroupIDs[num];
                            if (_cachedMember.GetAttributeDate(agID, DateTime.MinValue) != DateTime.MinValue)
                            {
                                Site site = brands.GetBrand(attributescoll.GetAttributeGroup(agID).GroupID).Site;
                                //Update username attribute for all sites in the community 
                                if (site.Community.CommunityID == communityId && languageID != site.LanguageID)
                                {
                                    _cachedMember.SetAttributeText(attributeGroupID, site.LanguageID, textValue.Text, textValue.TextStatus);
                                }
                            }
                        }
                    } 
					_cachedMember.SetAttributeText((Int32)deA.Key,
						languageID,
						textValue.Text,
						textValue.TextStatus);
				}

			}

			de = this.MemberUpdate.AttributesInt.GetEnumerator();
			while (de.MoveNext())
			{
				_cachedMember.SetAttributeInt((Int32)de.Key,
					(Int32)de.Value);
			}

			de = this.MemberUpdate.AttributesDate.GetEnumerator();
			while (de.MoveNext())
			{
				_cachedMember.SetAttributeDate((Int32)de.Key,
					(DateTime)de.Value);
			}

			_memberUpdate = null;
		}


		public AttributeGroup getAttributeGroup(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute)
		{
			Int32 groupID = 0;

			switch (attribute.Scope)
			{
				case ScopeType.Personals:
					groupID = GROUP_PERSONALS;
					break;

				case ScopeType.Community:
					if (!(communityID > 0))
					{
						throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), communityID not set.");
					}
					groupID = communityID;
					break;

				case ScopeType.Site:
					if (!(siteID > 0))
					{
						throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), siteID not set.");
					}
					groupID = siteID;
					break;

				case ScopeType.Brand:
					if (!(brandID > 0))
					{
						throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), brandID not set.");
					}
					groupID = brandID;
					break;
			}

			if (groupID == 0)
			{
				throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ").");
			}

			return attributes.GetAttributeGroup(groupID, attribute.ID);
		}

		
		#region text attributes
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="viewerMemberID"></param>
		/// <param name="unapprovedResourceValue"></param>
		/// <returns></returns>
		public string GetAttributeTextApproved(Brand brand,
			string attributeName,
			int viewerMemberID,
			string unapprovedResourceValue)
		{
			return GetAttributeTextApproved(brand,
				new string[] {attributeName},
				viewerMemberID,
				unapprovedResourceValue);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeNames"></param>
		/// <param name="viewerMemberID"></param>
		/// <param name="unapprovedResourceValue"></param>
		/// <returns></returns>
		public string GetAttributeTextApproved(Brand brand,
			string[] attributeNames,
			Int32 viewerMemberID,
			string unapprovedResourceValue)
		{
			bool foundUnapproved = false;
			string val = Constants.NULL_STRING;

			for (Int32 num = 0; num < attributeNames.Length; num++)
			{
				string attributeName = attributeNames[num];
				TextStatusType textStatus;
				val = GetAttributeText(brand, attributeName, out textStatus);
				if (viewerMemberID != _cachedMember.MemberID && textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
				{
					foundUnapproved = true;
					val = Constants.NULL_STRING;
				}
				else
				{
					break;
				}
			}

			if (val == Constants.NULL_STRING && foundUnapproved)
			{
				val = unapprovedResourceValue;
			}

			return val;
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="unapprovedResource"></param>
		/// <returns></returns>
		public string GetAttributeTextApproved(Brand brand,
			string attributeName,
			string unapprovedResource)
		{
			TextStatusType textStatus;
			string val = GetAttributeText(brand, attributeName, out textStatus);

			if (textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
			{
				return unapprovedResource;
			}
			else
			{
				return val;
			}
		}

        public string GetAttributeTextApproved(Brand brand,
            int languageID,
            string attributeName,
            string defaultValue,
            string unapprovedResource)
        {
            TextStatusType textStatus;
            
            string val = GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, 
                languageID, attributeName, defaultValue, out textStatus);

            if (textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
            {
                return unapprovedResource;
            }
            else
            {
                return val;
            }
        }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			string attributeName)
		{
			TextStatusType textStatus;

			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeName),
				"",
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			Int32 attributeID)
		{
			TextStatusType textStatus;

			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeID),
				"",
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			string attributeName,
			string defaultValue)
		{
			TextStatusType textStatus;

			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeName),
				defaultValue,
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			Int32 attributeID,
			string defaultValue)
		{
			TextStatusType textStatus;

			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeID),
				defaultValue,
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			string attributeName,
			out TextStatusType textStatus)
		{
			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeName),
				"",
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			Int32 attributeID,
			out TextStatusType textStatus)
		{
			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeID),
				"",
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			string attributeName,
			string defaultValue,
			out TextStatusType textStatus)
		{
			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeName),
				defaultValue,
				out textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Brand brand,
			Int32 attributeID,
			string defaultValue,
			out TextStatusType textStatus)
		{
			return getAttributeText(brand.Site.Community.CommunityID, 
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeID),
				defaultValue,
				out textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeName"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			string attributeName)
		{
			TextStatusType textStatus;

			return getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeName),
				Constants.NULL_STRING,
				out textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Int32 attributeID)
		{
			TextStatusType textStatus;

			return getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeID),
				Constants.NULL_STRING,
				out textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			string attributeName,
			string defaultValue)
		{
			TextStatusType textStatus;

			return getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeName),
				defaultValue,
				out textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Int32 attributeID,
			string defaultValue)
		{
			TextStatusType textStatus;

			return getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeID),
				defaultValue,
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			string attributeName,
			string defaultValue,
			out TextStatusType textStatus)
		{
			return getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeName),
				defaultValue,
				out textStatus);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <param name="textStatus"></param>
		/// <returns></returns>
		public string GetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Int32 attributeID,
			string defaultValue,
			out TextStatusType textStatus)
		{
			return getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeID),
				defaultValue,
				out textStatus);
		}

		
		private string getAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			string defaultValue,
			out TextStatusType textStatus)
		{
			AttributeGroup attributeGroup = getAttributeGroup(communityID, 
				siteID,
				brandID,
				attribute);

			if (attribute.DataType != DataType.Text)
			{
				throw new Exception("Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") is not a text attribute.");
			}

			if (attributeGroup.DefaultValue != Constants.NULL_STRING)
			{
				defaultValue = attributeGroup.DefaultValue;
			}

			return _cachedMember.GetAttributeText(attributeGroup.ID,
				languageID,
				defaultValue,
				out textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="textVal"></param>
		/// <param name="textStatus"></param>
		public void SetAttributeText(Brand brand,
			string attributeName,
			string textVal,
			TextStatusType textStatus)
		{
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeName);

			if (isFakeAttribute(attribute.ID, textVal, textStatus, brand.Site.Community.CommunityID))
			{
				return;
			}

			setAttributeText(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attribute,
				textVal,
				textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="textVal"></param>
		/// <param name="textStatus"></param>
		public void SetAttributeText(Brand brand,
			Int32 attributeID,
			string textVal,
			TextStatusType textStatus)
		{
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeID);

			if (isFakeAttribute(attribute.ID, textVal, textStatus, brand.Site.Community.CommunityID))
			{
				return;
			}
			
			setAttributeText(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				brand.Site.LanguageID,
				attributes.GetAttribute(attributeID),
				textVal,
				textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeName"></param>
		/// <param name="textVal"></param>
		/// <param name="textStatus"></param>
		public void SetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			string attributeName,
			string textVal,
			TextStatusType textStatus)
		{
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeName);

			if (isFakeAttribute(attribute.ID, textVal, textStatus, communityID))
			{
				return;
			}

			setAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attributes.GetAttribute(attributeName),
				textVal,
				textStatus);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="languageID"></param>
		/// <param name="attributeID"></param>
		/// <param name="textVal"></param>
		/// <param name="textStatus"></param>
		public void SetAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Int32 attributeID,
			string textVal,
			TextStatusType textStatus)
		{
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeID);

			if (isFakeAttribute(attribute.ID, textVal, textStatus, communityID))
			{
				return;
			}

			setAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attribute,
				textVal,
				textStatus);
		}


		private void setAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			string textVal,
			TextStatusType textStatus)
		{
			this.setAttributeText(communityID, siteID, brandID, languageID, attribute, textVal, textStatus, false);
		}


		private void setAttributeText(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			string textVal,
			TextStatusType textStatus,
			// PM-218 Remove after
			bool writeCommunityUserName)
		{
			if (attribute.DataType != DataType.Text)
			{
				throw new Exception("SetAttributeText() was passed non-text attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ")");
			}

            if(attribute.ID == Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_EMAILADDRESS && string.IsNullOrEmpty(textVal.Trim()))
            {
                //this an attempt to save a blank email address which should not be allowed, 
                //so we'll ignore the change. 
                return;
            }


			AttributeGroup attributeGroup;

			// PM-218 Remove after
			if (writeCommunityUserName)
			{
				attributeGroup = attributes.GetAttributeGroup(communityID, attribute.ID);
			}
			else
			{
				attributeGroup = getAttributeGroup(communityID,
					siteID,
					brandID,
					attribute);
			}

			if (textVal != null && textVal.Length > attributeGroup.Length)
			{
				throw new Exception("Attribute value exceeds maximum length (attributeGroupID: " + attributeGroup.ID.ToString() + ", maxlength: " + attributeGroup.Length.ToString() + ", value: " + textVal + ")");
			}

			TextStatusType currentStatus;
			string currentVal = getAttributeText(communityID,
				siteID,
				brandID,
				languageID,
				attribute,
				Constants.NULL_STRING,
				out currentStatus);

			// no need to update if the value in the db and the passed in value are the same
			// if status is "Human Approved", we need to update the status of the text attribute regardless
			// of the text value
			if ((currentVal != textVal && textVal != String.Empty) || textStatus == TextStatusType.Human)
			{
				Hashtable attributes = MemberUpdate.AttributesText[languageID] as Hashtable;

				if (attributes == null)
				{
					attributes = new Hashtable();
					MemberUpdate.AttributesText.Add(languageID, attributes);
				}
			
				TextValue textValue = attributes[attributeGroup.ID] as TextValue;

				if (textValue != null)
				{
					textValue.Text = textVal;
					textValue.TextStatus = textStatus;
				}
				else
				{
					attributes.Add(attributeGroup.ID, new TextValue(textVal, textStatus));
				}
				
				// if this text is being approved, go delete the "Old value container" if it exists
				if (textStatus == TextStatusType.Human)
				{
					if (attribute.OldValueContainerID != Constants.NULL_INT && 
						oldApprovedValueExists(communityID, siteID, brandID, languageID, attribute))
					{
						// Delete the old value container value.  we don't need this anymore since
						// admin just approved this text
						AttributeGroup attributeGroupToRemove = getAttributeGroup(communityID, siteID, brandID,
								this.attributes.GetAttribute(attribute.OldValueContainerID));

                        if (attributes[attributeGroupToRemove.ID] != null)
                        {
                            attributes[attributeGroupToRemove.ID] = new TextValue(null, TextStatusType.Human);
                        }
                        else
                        {
						attributes.Add(attributeGroupToRemove.ID, new TextValue(null, TextStatusType.Human));
					}
				}
				}
				else if (textStatus == TextStatusType.None)
				{
					// If textStatus is "None", this means the user just updated this text, so we need to copy this to
					// another attribute if specified in the attribute metadata.  If there is an approved text in the
					// old value container already, this means we don't make copies because this would mean that the user
					// made multiple updates before the admin could get to the approval screen to approve it.  We also
					// have to make sure the current status of the text is approved, otherwise do nothing.
					// TODO
					if((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval &&
						attribute.OldValueContainerID != Constants.NULL_INT)
					{
						if (!oldApprovedValueExists(communityID, siteID, brandID, languageID, attribute) &&
							(currentStatus == TextStatusType.Human || currentStatus == TextStatusType.Auto))
						{
							// Copy the attribute to the old value container
							AttributeGroup attributeGroupToCopyTo = getAttributeGroup(communityID, siteID, brandID,
								this.attributes.GetAttribute(attribute.OldValueContainerID));

							attributes.Add(attributeGroupToCopyTo.ID, new TextValue(currentVal, currentStatus));
						}
					}
				}
			}
		}

		private bool oldApprovedValueExists(Int32 communityID,
			Int32 siteID,
			Int32 brandID,
			Int32 languageID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute existingAttribute)
		{
			TextStatusType oldValContainerStatus;
			string oldVal = GetAttributeText(communityID, siteID, brandID, languageID, existingAttribute.OldValueContainerID, 
					Constants.NULL_STRING, out oldValContainerStatus);

			if(oldVal != Constants.NULL_STRING && oldVal != string.Empty && oldValContainerStatus == TextStatusType.Human)
				return true;
						
			return false;
		}

		private bool isFakeAttribute(Int32 attributeID, string textVal, TextStatusType textStatus, int communityID)
		{
			switch (attributeID)
			{
				case (ServiceConstants.ATTRIBUTEID_USERNAME):
					this.MemberUpdate.Username = textVal;
					return false;

				case (ServiceConstants.ATTRIBUTEID_EMAILADDRESS):
					this.EmailAddress = textVal;
					return true;
			}

			return false;
		}
		#endregion

		#region integer attributes
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <returns></returns>
		public bool GetAttributeBool(Brand brand,
			string attributeName)
		{
			return getAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeName),
				Constants.NULL_INT, false) == 1;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public bool GetAttributeBool(Brand brand,
			Int32 attributeID)
		{
			return getAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeID),
				Constants.NULL_INT, false) == 1;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeName"></param>
		/// <returns></returns>
		public bool GetAttributeBool(Int32 communityID,
			Int32 siteID,
			Int32 brandID,
			string attributeName)
		{
			return getAttributeInt(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeName),
				Constants.NULL_INT, false) == 1;
		}

		
		public bool GetAttributeBool(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 attributeID)
		{
			return getAttributeInt(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeID),
				Constants.NULL_INT, false) == 1;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Brand brand,
			string attributeName)
		{
			return getAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeName),
				Constants.NULL_INT, false);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Brand brand,
			Int32 attributeID)
		{
			return getAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeID),
				Constants.NULL_INT, false);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Brand brand,
			string attributeName,
			Int32 defaultValue)
		{
			return getAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeName),
				defaultValue, false);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Brand brand,
			Int32 attributeID,
			Int32 defaultValue)
		{
			return getAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeID),
				defaultValue, false);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			string attributeName,
			Int32 defaultValue)
		{
			return getAttributeInt(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeName),
				defaultValue, false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public Int32 GetAttributeInt(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 attributeID,
			Int32 defaultValue)
		{
			return getAttributeInt(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeID),
				defaultValue, false);
		}

        public Int32 GetAttributeInt(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            string attributeName,
            Int32 defaultValue,
            bool ignoreUnifiedAccessPrivilege)
        {
            return getAttributeInt(communityID,
                siteID,
                brandID,
                attributes.GetAttribute(attributeName),
                defaultValue, ignoreUnifiedAccessPrivilege);
        }
		
		private Int32 getAttributeInt(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			Int32 defaultValue, 
            bool ignoreUnifiedAccessPrivilege)
		{
			AttributeGroup attributeGroup = getAttributeGroup(communityID, 
				siteID,
				brandID,
				attribute);

            if (attributeGroup == null)
            {
                throw new Exception("AttributeGroup for Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") does not exist. Community:" + communityID.ToString() + ", Site:" + siteID.ToString() + ", Brand:" + brandID.ToString());
            }

			if (attribute.DataType != DataType.Bit && attribute.DataType != DataType.Mask && attribute.DataType != DataType.Number)
			{
				throw new Exception("Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") is not an integer attribute.");
			}

            if (attributeGroup.DefaultValue != Constants.NULL_STRING)
            {
                defaultValue = Convert.ToInt32(attributeGroup.DefaultValue);
            }

            int returnValue = defaultValue;

            //Determine if we should use Unified Access
            if (!ignoreUnifiedAccessPrivilege && (attribute.Name.ToLower() == "coloranalysis"))
            {
                AccessPrivilege accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.ColorAnalysis, brandID, siteID, communityID);

                if (accessPrivilege != null)
                {
                    DateTime date = accessPrivilege.EndDatePST;
                    if (date == DateTime.MinValue || date > DateTime.Now)
                        returnValue = 1;
                    else
                        returnValue = 0;
                }
            }
            else
            {
                returnValue = _cachedMember.GetAttributeInt(attributeGroup.ID,
				defaultValue);
            }

            return returnValue;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="intVal"></param>
		public void SetAttributeInt(Brand brand,
			string attributeName,
			Int32 intVal)
		{
			setAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeName),
				intVal);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="intVal"></param>
		public void SetAttributeInt(Brand brand,
			Int32 attributeID,
			Int32 intVal)
		{
			setAttributeInt(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeID),
				intVal);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeName"></param>
		/// <param name="intVal"></param>
		public void SetAttributeInt(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			string attributeName,
			Int32 intVal)
		{
			setAttributeInt(communityID,
				siteID,
				brandID,
				attributes.GetAttribute(attributeName),
				intVal);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeID"></param>
		/// <param name="intVal"></param>
		public void SetAttributeInt(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 attributeID,
			Int32 intVal)
		{
			setAttributeInt(communityID,
				siteID,
				brandID,
				attributes.GetAttribute(attributeID),
				intVal);
		}


		private void setAttributeInt(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			Int32 intVal)
		{
			if (attribute.DataType != DataType.Bit && attribute.DataType != DataType.Mask && attribute.DataType != DataType.Number)
			{
				throw new Exception("SetAttributeInt() was passed non-integer attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ")");
			}

			AttributeGroup attributeGroup = getAttributeGroup(communityID,
				siteID,
				brandID,
				attribute);

			if (getAttributeInt(communityID,
				siteID,
				brandID,
				attribute,
				Constants.NULL_INT, false) != intVal)
			{
				object o = MemberUpdate.AttributesInt[attributeGroup.ID];

				if (o != null)
				{
					MemberUpdate.AttributesInt[attributeGroup.ID] = intVal;
				}
				else
				{
					MemberUpdate.AttributesInt.Add(attributeGroup.ID, intVal);
				}
			}
		}
		#endregion

		#region date attributes
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Brand brand,
			string attributeName)
		{
			return getAttributeDate(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeName),
				DateTime.MinValue, false);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Brand brand,
			Int32 attributeID)
		{
			return getAttributeDate(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeID),
				DateTime.MinValue, false);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Brand brand,
			string attributeName,
			DateTime defaultValue)
		{
			return getAttributeDate(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeName),
				defaultValue, false);
		}
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Brand brand,
			Int32 attributeID,
			DateTime defaultValue)
		{
			return getAttributeDate(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributes.GetAttribute(attributeID),
				defaultValue, false);
		}
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			string attributeName,
			DateTime defaultValue)
		{
			return getAttributeDate(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeName),
				defaultValue, false);
		}

        public DateTime GetAttributeDate(Int32 communityID,
            Int32 siteID,
            Int32 brandID,
            string attributeName,
            DateTime defaultValue, bool ignoreUnifiedAccessPrivilege)
        {
            return getAttributeDate(communityID,
                siteID,
                brandID,
                attributes.GetAttribute(attributeName),
                defaultValue, ignoreUnifiedAccessPrivilege);
        }
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeID"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public DateTime GetAttributeDate(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 attributeID,
			DateTime defaultValue)
		{
			return getAttributeDate(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeID),
				defaultValue, false);
		}

        
		private DateTime getAttributeDate(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			DateTime defaultValue, bool ignoreUnifiedAccessPrivilege)
		{
			AttributeGroup attributeGroup = getAttributeGroup(communityID, 
				siteID,
				brandID,
				attribute);

            if (attributeGroup == null)
            {
                throw new Exception("AttributeGroup for Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") does not exist. Community:" + communityID.ToString() + ", Site:" + siteID.ToString() + ", Brand:" + brandID.ToString());
            }

            if (attribute.DataType != DataType.Date)
			{
				throw new Exception("Attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ") is not a date attribute.");
			}

            if (attributeGroup.DefaultValue != Constants.NULL_STRING)
            {
                defaultValue = Convert.ToDateTime(attributeGroup.DefaultValue);
            }
            else
            {
                defaultValue = DateTime.MinValue;
            }


            DateTime date = defaultValue;

            //Determine if we should use Unified Access
            if (!ignoreUnifiedAccessPrivilege && (attribute.Name.ToLower() == "subscriptionexpirationdate"
                    || attribute.Name.ToLower() == "highlightedexpirationdate"
                    || attribute.Name.ToLower() == "spotlightexpirationdate"
                    || attribute.Name.ToLower() == "jmeterexpirationdate"))
            {
                AccessPrivilege accessPrivilege = null;
                switch (attribute.Name.ToLower())
                {
                    case "subscriptionexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, brandID, siteID, communityID);
                        break;
                    case "highlightedexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.HighlightedProfile, brandID, siteID, communityID);
                        break;
                    case "spotlightexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.SpotlightMember, brandID, siteID, communityID);
                        break;
                    case "jmeterexpirationdate":
                        accessPrivilege = GetUnifiedAccessPrivilege(PrivilegeType.JMeter, brandID, siteID, communityID);
                        break;
                }

                if (accessPrivilege != null)
                    date = accessPrivilege.EndDatePST;
            }
            else if (_cachedMember != null)
            {
                date = _cachedMember.GetAttributeDate(attributeGroup.ID,
                    defaultValue);
            }

            return date;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeName"></param>
		/// <param name="dateVal"></param>
		public void SetAttributeDate(Brand brand,
			string attributeName,
			DateTime dateVal)
		{
			SetAttributeDate(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributeName,
				dateVal);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="attributeID"></param>
		/// <param name="dateVal"></param>
		public void SetAttributeDate(Brand brand,
			Int32 attributeID,
			DateTime dateVal)
		{
			SetAttributeDate(brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID,
				attributeID,
				dateVal);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeName"></param>
		/// <param name="dateVal"></param>
		public void SetAttributeDate(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			string attributeName,
			DateTime dateVal)
		{
			setAttributeDate(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeName),
				dateVal);
		}
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="attributeID"></param>
		/// <param name="dateVal"></param>
		public void SetAttributeDate(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Int32 attributeID,
			DateTime dateVal)
		{
			setAttributeDate(communityID, 
				siteID,
				brandID,
				attributes.GetAttribute(attributeID),
				dateVal);
		}
		

		private void setAttributeDate(Int32 communityID, 
			Int32 siteID,
			Int32 brandID,
			Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute,
			DateTime dateVal)
		{
			if (attribute.DataType != DataType.Date)
			{
				throw new Exception("SetAttributeDate() was passed non-date attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ")");
			}

			AttributeGroup attributeGroup = getAttributeGroup(communityID,
				siteID,
				brandID,
				attribute);

			if (getAttributeDate(communityID,
				siteID,
				brandID,
				attribute,
				DateTime.MinValue, true) != dateVal)
			{
				object o = MemberUpdate.AttributesDate[attributeGroup.ID];

				if (o != null)
				{
					MemberUpdate.AttributesDate[attributeGroup.ID] = dateVal;
				}
				else
				{
					MemberUpdate.AttributesDate.Add(attributeGroup.ID, dateVal);
				}
			}
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public DateTime GetLastLogonDate(Int32 communityID)
		{
			Int32 brandID;
			return GetLastLogonDate(communityID,
				out brandID);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public DateTime GetLastLogonDate(Int32 communityID, out Int32 brandID)
		{
			Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
			Brands brands = BrandConfigSA.Instance.GetBrands();

			return _cachedMember.GetLastLogonDate(communityID, out brandID, attributes, brands);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public DateTime GetInsertDate(Int32 communityID)
		{
			ArrayList communities = new ArrayList();
			DateTime firstInsertDate = DateTime.MaxValue;

			Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
			Int32 attributeID = attributes.GetAttribute("BrandInsertDate").ID;
			ArrayList attributeGroupIDs = attributes.GetAttributeGroupIDs(attributeID);
			Brands brands = BrandConfigSA.Instance.GetBrands();

			for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
			{
				Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
				Brand brand = brands.GetBrand(attributes.GetAttributeGroup(attributeGroupID).GroupID);
				if (brand.Site.Community.CommunityID == communityID)
				{
					DateTime currentDtm = GetAttributeDate(brand, attributeID);
					if (currentDtm < firstInsertDate && currentDtm != DateTime.MinValue)
					{
						firstInsertDate = currentDtm;
					}
				}
			}

			if (firstInsertDate == DateTime.MaxValue)
			{
				return DateTime.MinValue;
			}

			return firstInsertDate;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public bool IsPayingMember(Int32 siteID)
		{
			//getSiteIDList
			Brands brands = BrandConfigSA.Instance.GetBrands();
			Site[] sites = brands.GetSites();
			Int32 communityID = brands.GetSite(siteID).Community.CommunityID;

			for (Int32 siteNum = 0; siteNum < sites.Length; siteNum++)
			{
				Site site = sites[siteNum];
				if (site.Community.CommunityID == communityID)
				{
					//we give a 15min grace period to allow for renewals to be processed
                    AccessPrivilege ap = GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, Constants.NULL_INT, site.SiteID, Constants.NULL_INT);
                    if (ap != null)
                    {
                        if (ap.EndDatePST.AddMinutes(15) > DateTime.Now)
                        {
                            return true;
                        }
                    }
				}
			}

			return false;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public bool IsEligibleForEmail(Int32 communityID)
		{
			return _cachedMember.IsEligibleForEmail(AttributeMetadataSA.Instance.GetAttributes(), communityID);
		}

        /// <summary>
        /// This determines whether Unified Access is enabled for the site, used to determine whether we 
        /// read privileges from attributes or from Access svc.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsUPSAccessEnabled(int brandID, int siteID, int communityID)
        {
            return true;

            /*
             * Note: We're always using Access now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
            **/

        }

        /// <summary>
        /// This is a kill switch so Bedrock systems will stop reading Access data
        /// </summary>
        /// <returns></returns>
        public static bool IsUPSAccessMasterReadKillSwitchEnabled()
        {
            return false;

            /*
             * Note: We're always using Access now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_MASTER_READ_KILLSWITCH_ENABLED");
            }
            catch (Exception ex)
            {

                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
             * */

        }

        /// <summary>
        /// This setting determines whether Bedrock systems will update Access Service with changes to privileges (e.g. Admin Adjust)
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsUPSAccessUpdateEnabled(int brandID, int siteID, int communityID)
        {
            return true;

            /*
             * Note: We're always using Access now.
             * 
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_UPDATE_PRIVILEGE_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {

                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
             * */

        }

	    /// <summary>
	    ///     Goes to the overloaded method below
	    /// </summary>
	    /// <param name="privilegeType"></param>
	    /// <param name="brandID"></param>
	    /// <param name="siteID"></param>
	    /// <param name="communityID"></param>
	    /// <returns></returns>
	    public AccessPrivilege GetUnifiedAccessPrivilege(PrivilegeType privilegeType, int brandID, int siteID,
	        int communityID)
	    {
	        return GetUnifiedAccessPrivilege(privilegeType, brandID, siteID, communityID, MemberLoadFlags.None);
	    }

        /// <summary>
        /// Gets a specific Privilege from Unified Access service, which is cached as CachedMemberAccess
        /// 
        /// Note: Existing privileges (subscriptionExpirationDate, Highlight..,Spotlight..JMeter..ColorCode) can still call
        /// GetAttributeDate() or GetAttributeInt() which has been updated to automatically get the values from either member attributes
        /// or Unified Access (via CachedMemberAccess) depending on whether Access is enabled for the site.
        /// </summary>
        /// <param name="privilegeType"></param>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public AccessPrivilege GetUnifiedAccessPrivilege(PrivilegeType privilegeType, int brandID, int siteID, int communityID, MemberLoadFlags memberLoadFlags = MemberLoadFlags.None )
        {
            AccessPrivilege accessPrivilege = null;

            if (_cachedMemberAccess == null)
            {
                _cachedMemberAccess = MemberSA.Instance.getCachedMemberAccess(MemberID, memberLoadFlags, false);
            }

            //Get Privilege
            if (_cachedMemberAccess != null)
            {
                accessPrivilege = _cachedMemberAccess.GetAccessPrivilege(privilegeType, siteID);
                //System.Diagnostics.EventLog.WriteEntry("WWW", String.Format("Member.GetUnifiedAccessPrivilege({0}, {1}, {2}, {3}). EndDatePST: {4}", privilegeType, brandID, siteID, communityID, (accessPrivilege == null) ? "null" : accessPrivilege.EndDatePST.ToShortDateString()));
            }
            else
            {
                System.Diagnostics.EventLog.WriteEntry("WWW", String.Format("Member.GetUnifiedAccessPrivilege({0}, {1}, {2}, {3}). _cacheMemberAccess is null for memberID: {4}", privilegeType, brandID, siteID, communityID, MemberID)); 
            }

            return accessPrivilege;
        }

        /// <summary>
        /// Given a communityID, it retrieves the latest SubscriptionLastInitialPurchaseDate member attribute.
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public DateTime GetSubscriptionLastInitialPurchaseDate(int communityID)
        {
            var siteIds = getSiteIDList();
            DateTime lastSubInitialPurchaseDate = DateTime.MinValue;
            DateTime tempDate = DateTime.MinValue;

            foreach (var siteId in siteIds)
            {
                var brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsBySite((int)siteId);
                if (brands.GetSite((int)siteId).Community.CommunityID == communityID)
                {
                    tempDate = GetAttributeDate(communityID, (int)siteId, Constants.NULL_INT,
                                     "SubscriptionLastInitialPurchaseDate", DateTime.MinValue);

                    lastSubInitialPurchaseDate = tempDate > lastSubInitialPurchaseDate
                                                     ? tempDate
                                                     : lastSubInitialPurchaseDate;
                }
            }

            return lastSubInitialPurchaseDate;
        }

        public List<LastLogon> GetMemberLastLogons(int groupID, MemberLoadFlags memberLoadFlags)
        {
            if (_cachedMemberLogon == null)
            {
                _cachedMemberLogon = MemberSA.Instance.getCachedMemberLogon(this.MemberID, groupID, memberLoadFlags);
            }

            return _cachedMemberLogon.LastLogonsNew;
        }

       
        /// <summary>
        /// The user stories for this are FNOLA-369 & FNOLA-1384
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="memberLoadFlags"></param>
        /// <returns></returns>
        public List<LastEmail> GetMemberLastEmail(int groupID, MemberLoadFlags memberLoadFlags)
        {
            if (_cachedMemberEmail == null)
            {
                _cachedMemberEmail = MemberSA.Instance.getCachedMemberEmail(this.MemberID, memberLoadFlags);
            }

            return _cachedMemberEmail.LastEmails;
        }

        public IMemberDTO GetIMemberDTO()
        {
            return this;
        }

        public MemberType MemberType
        {
            get { return MemberType.Full; }
        }

        #region ICacheable Members
        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return CacheItemPriorityLevel.Normal; }
            set { }
        }

        public int CacheTTLSeconds
        {
            get { return 60*60; }
            set { }
        }

        public string GetCacheKey()
        {
            return MemberID + "-" + MemberType.Full.ToString();
        }
        #endregion

    }
}
