using System;
using System.Collections;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Privilege;


namespace Matchnet.Member.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
	public class MemberPrivilegeSA : SABase
	{
		/// <summary>
		/// 
		/// </summary>
		public static readonly MemberPrivilegeSA Instance = new MemberPrivilegeSA();

		private Matchnet.Caching.Cache _cache;

		private MemberPrivilegeSA()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public bool IsAdmin(Int32 memberID)
		{
			return IsAdmin(memberID, System.Web.HttpContext.Current);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="currentHttpContext"></param>
		/// <returns></returns>
		public bool IsAdmin(Int32 memberID, System.Web.HttpContext currentHttpContext)
		{
			bool allowAdmin = false;
			bool adminHost = false;

			if (currentHttpContext != null)
			{
				adminHost = currentHttpContext.Request.Url.Host.ToLower().StartsWith("admin.");
			}

			if( !allowAdmin && Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_WEB_ADMIN")) 
				&& (adminHost || Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"))))
			{
				allowAdmin = true;
			}

			if (!allowAdmin)
			{
				return false;
			}

			MemberPrivilegeCollection privs = GetMemberPrivileges(memberID);
			for (Int32 num = 0; num < privs.Count; num++)
			{
				MemberPrivilege memberPrivilege = privs[num];
				if (memberPrivilege.PrivilegeState != PrivilegeState.Denied)
				{
					return true;
				}
			}

			return false;
		}
		
		/// <summary>
		/// IsAdmin check method that doesn't care about the host or any runtime settings.
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public bool IsAdminByPrivilegesOnly(Int32 memberID)
		{
			MemberPrivilegeCollection privs = GetMemberPrivileges(memberID);
			for (Int32 num = 0; num < privs.Count; num++)
			{
				MemberPrivilege memberPrivilege = privs[num];
				if (memberPrivilege.PrivilegeState != PrivilegeState.Denied)
				{
					return true;
				}
			}

			return false;
		}
		
		/// <summary>
		/// Returns true if the IP provided is Userplane or Web19 and the user is an admin.
		/// </summary>
		/// <param name="memberID">The memberid to determine admin status</param>
		/// <param name="userIP">The IP address of the incoming request</param>
		/// <returns>True if: The user is an admin and the ip address is from Userplane. False otherwise</returns>
		public bool IsUserplaneRequestAdmin(Int32 memberID, string userIP)
		{			
			MemberPrivilegeCollection privs = GetMemberPrivileges(memberID);
			for (Int32 num = 0; num < privs.Count; num++)
			{
				MemberPrivilege memberPrivilege = privs[num];
				if (memberPrivilege.PrivilegeState != PrivilegeState.Denied)
				{
					return true;
				}
			}

			return false;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privilegeID"></param>
		/// <returns></returns>
		public bool HasPrivilege(Int32 memberID, Int32 privilegeID)
		{
			return HasPrivilege(memberID, privilegeID, System.Web.HttpContext.Current);
		}

			
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privilegeID"></param>
		/// <param name="currentHttpContext"></param>
		/// <returns></returns>
		public bool HasPrivilege(Int32 memberID, Int32 privilegeID, System.Web.HttpContext currentHttpContext)
		{
			if (IsAdmin(memberID, currentHttpContext))
			{
				return GetMemberPrivileges(memberID).HasPrivilege(privilegeID);
			}
			return false;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID)
		{
			string uri = "";

			try
			{
				MemberPrivilegeCollection memberPrivileges = _cache.Get(MemberPrivilegeCollection.GetCacheKey(memberID)) as MemberPrivilegeCollection;

				if (memberPrivileges == null)
				{
					uri = SMUtil.getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						memberPrivileges = SMUtil.getService(uri).GetMemberPrivileges(memberID);
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Insert(memberPrivileges);
				}

				return memberPrivileges;
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve member privileges (uri: " + uri + ", MemberID: " + memberID + ").", ex));
			} 
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privilegeID"></param>
		public void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID)
		{
			string uri = "";

			try
			{
				MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID);
				uri = SMUtil.getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					SMUtil.getService(uri).SaveMemberPrivilege(memberID, privilegeID);
				}
				finally
				{
					base.Checkin(uri);
				}
				memberPrivileges.Add(new MemberPrivilege(GetPrivileges().FindByID(privilegeID), PrivilegeState.Individual));
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to save member privilege (uri: " + uri + ", MemberID: " + memberID + ", privilegeID: " + privilegeID.ToString() + ").", ex));
			} 
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="privilegeID"></param>
		public void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID)
		{
			string uri = "";

			try
			{
				MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID);
				uri = SMUtil.getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					SMUtil.getService(uri).DeleteMemberPrivilege(memberID, privilegeID);
				}
				finally
				{
					base.Checkin(uri);
				}
				memberPrivileges.Revoke(privilegeID);
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to delete member privilege (uri: " + uri + ", MemberID: " + memberID + ", privilegeID: " + privilegeID.ToString() + ").", ex));
			} 
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="securityGroupID"></param>
		/// <returns></returns>
		public SecurityGroupMemberCollection GetSecurityGroupMembers(Int32 securityGroupID)
		{
			string uri = "";

			try
			{
				SecurityGroupMemberCollection securityGroupMembers = _cache.Get(SecurityGroupMemberCollection.GetCacheKey(securityGroupID)) as SecurityGroupMemberCollection;

				if (securityGroupMembers == null)
				{
					uri = SMUtil.getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						securityGroupMembers = SMUtil.getService(uri).GetSecurityGroupMembers(securityGroupID);
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Insert(securityGroupMembers);
				}

				return securityGroupMembers;
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve securityGroup members (uri: " + uri + ", securityGroupID: " + securityGroupID + ").", ex));
			} 
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="securityGroupID"></param>
		public void SaveSecurityGroupMember(Int32 memberID, Int32 securityGroupID)
		{
			string uri = "";

			try
			{
				SecurityGroupMemberCollection securityGroupMembers = GetSecurityGroupMembers(securityGroupID);
				uri = SMUtil.getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					SMUtil.getService(uri).SaveSecurityGroupMember(memberID, securityGroupID);
				}
				finally
				{
					base.Checkin(uri);
				}
				securityGroupMembers.Add(memberID);

				//Remove the cached copy of the member's privileges to force it to update and reflect the group membership:
				_cache.Remove(MemberPrivilegeCollection.GetCacheKey(memberID));
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to save securityGroup member (uri: " + uri + ", memberID: " + memberID.ToString() + ", securityGroupID: " + securityGroupID + ").", ex));
			} 
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="securityGroupID"></param>
		public void DeleteSecurityGroupMember(Int32 memberID, Int32 securityGroupID)
		{
			string uri = "";

			try
			{
				SecurityGroupMemberCollection securityGroupMembers = GetSecurityGroupMembers(securityGroupID);
				uri = SMUtil.getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					SMUtil.getService(uri).DeleteSecurityGroupMember(memberID, securityGroupID);
				}
				finally
				{
					base.Checkin(uri);
				}
				securityGroupMembers.Remove(memberID);

				//Remove the cached copy of the member's privileges to force it to update and reflect the group membership:
				_cache.Remove(MemberPrivilegeCollection.GetCacheKey(memberID));
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to delete securityGroup member (uri: " + uri + ", memberID: " + memberID.ToString() + ", securityGroupID: " + securityGroupID + ").", ex));
			} 
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public SecurityGroupCollection GetSecurityGroups()
		{
			string uri = "";

			try
			{
				SecurityGroupCollection securityGroupCollection = _cache.Get(SecurityGroupCollection.CACHE_KEY) as SecurityGroupCollection;

				if (securityGroupCollection == null)
				{
					uri = SMUtil.getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						securityGroupCollection = SMUtil.getService(uri).GetSecurityGroups();
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return securityGroupCollection;
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while retrieving security securityGroups (uri: " + uri + ").", ex);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PrivilegeCollection GetPrivileges()
		{
			string uri = "";

			try
			{
				PrivilegeCollection privilegeCollection = _cache.Get(PrivilegeCollection.CACHE_KEY) as PrivilegeCollection;

				if (privilegeCollection == null)
				{
					uri = SMUtil.getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						privilegeCollection = SMUtil.getService(uri).GetPrivileges();
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return privilegeCollection;
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while retrieving security privileges (uri: " + uri + ").", ex);
			}
		}

		// Kept for backwards combatibility.  Do we really need to return an array of Member objects
		// instead of an array of CachedMember objects?  If not, we can delete this.
		public ArrayList GetMembersByPrivilege(Int32 privilegeID)
		{
			return GetMembersByPrivilege(privilegeID, false);
		}

		/// <summary>
		/// Get all the members who have this particular privilege
		/// </summary>
		/// <param name="privilegeID">The privilege required</param>
		/// <returns>An array list of members</returns>
		public ArrayList GetMembersByPrivilege(Int32 privilegeID, Boolean getCachedMembersFlag)
		{
			string uri = "";

			try
			{
				uri = SMUtil.getServiceManagerUri();
				ArrayList retVal;
				ServiceDefinitions.IMemberService service = SMUtil.getService(uri);
				base.Checkout(uri);
				try
				{
					retVal = service.GetMembersByPrivilege(privilegeID);
				}
				finally
				{
					base.Checkin(uri);
				}

				if (!getCachedMembersFlag)
					return MemberSA.Instance.GetMembers(retVal, MemberLoadFlags.None);
				else
				{
					return MemberSA.Instance.GetCachedMembers(retVal, MemberLoadFlags.None);
				}
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while retrieving Security privileges for PrivilegeId:"+privilegeID+" (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// Get all the groups that have this privilege
		/// </summary>
		/// <param name="privilegeID">The Id for this privilege</param>
		/// <returns>A SecurityGroupCollection containing all SecurityGroups that have this privilege</returns>
		public SecurityGroupCollection GetGroupsByPrivilege(Int32 privilegeID)
		{
			string uri = "";

			try
			{			
				uri = SMUtil.getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return SMUtil.getService(uri).GetGroupsByPrivilege(privilegeID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while retrieving Groups for PrivilegeId:"+privilegeID+" (uri: " + uri + ").", ex);
			}
		}
	

		/* These functions are unused for now.  To perform these actions, you must manually modify brSystem.
		/// <summary>
		/// Add a new privilege
		/// </summary>
		/// <param name="description">A description of the privilege</param>
		/// <returns>The PrivilegeID of the new Privilege.</returns>
		public int AddPrivilege(String description)
		{
			string uri = "";

			try
			{
				PrivilegeCollection privilegeCollection = GetPrivileges();

				uri = SMUtil.getServiceManagerUri();
				Int32 privilegeID = SMUtil.getService(uri).AddPrivilege(description);

				privilegeCollection.Add(new Privilege(privilegeID, description));

				return privilegeID;
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while adding a privilege with description:"+description+" (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// Updates the Description of the Privilege with the PrivilegeID specified
		/// </summary>
		/// <param name="privilegeID">PrivilegeID to update</param>
		/// <param name="description">New description</param>
		public void SavePrivilege(Int32 privilegeID, String description)
		{
			string uri = "";

			try
			{			
				uri = SMUtil.getServiceManagerUri();
				SMUtil.getService(uri).SavePrivilege(privilegeID,description);
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while saving a privilege with description:"+description+" (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="description"></param>
		/// <returns></returns>
		public int AddGroup(String description)
		{
			string uri = "";

			try
			{			
				uri = SMUtil.getServiceManagerUri();
				return SMUtil.getService(uri).AddGroup(description);
			}
			catch(Exception ex)
			{
				throw new SAException("Error creating new SecurityGroup '" + description + "' (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="securityGroupID"></param>
		/// <param name="description"></param>
		public void SaveGroup(Int32 securityGroupID, String description)
		{
			string uri = "";

			try
			{			
				uri = SMUtil.getServiceManagerUri();
				SMUtil.getService(uri).SaveGroup(securityGroupID, description);
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while saving SecurityGroup with SecurityGroupID=:" + securityGroupID + " (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// Saves the Group Privileges for the specified Group
		/// </summary>
		/// <param name="securityGroupID">The Group to save the privilege</param>
		/// <param name="privilegeID">The privilege to save</param>
		public void SaveGroupPrivilege(Int32 securityGroupID, Int32 privilegeID)
		{
			string uri = "";

			try
			{	
				SecurityGroupMemberCollection securityGroupMembers = GetSecurityGroupMembers(securityGroupID);
				uri = SMUtil.getServiceManagerUri();
				SMUtil.getService(uri).SaveGroupPrivilege(securityGroupID, privilegeID);

				foreach (Int32 memberID in securityGroupMembers)
				{
					MemberPrivilegeCollection memberPrivileges = GetMemberPrivileges(memberID);
					Privilege privilege = GetPrivileges().FindByID(privilegeID);
					memberPrivileges.Add(new MemberPrivilege(privilege, PrivilegeState.SecurityGroup));
				}
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while saving a group privilege with SecurityGroupID:"+securityGroupID+" (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// Deletes the privilege for the specified groupID
		/// </summary>
		/// <param name="securityGroupID">The GroupID</param>
		/// <param name="privilegeID">The PrivilegeID</param>
		public void DeleteGroupPrivilege(Int32 securityGroupID, Int32 privilegeID)
		{
			string uri = "";

			try
			{
				uri = SMUtil.getServiceManagerUri();
				SMUtil.getService(uri).DeleteGroupPrivilege(securityGroupID,privilegeID);
			}
			catch(Exception ex)
			{
				throw new SAException("Error occured while deleting a group privilege with GroupID:"+securityGroupID+" (uri: " + uri + ").", ex);
			}
		}*/
	}
}
