using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceManagers;

namespace Matchnet.Member
{
	public class MemberService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private MemberSM _memberSM;

		public MemberService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new MemberService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void RegisterServiceManagers()
		{
			try
			{
				_memberSM = new MemberSM();
				base.RegisterServiceManager(_memberSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
			}
		}

	
		protected override void Dispose( bool disposing )
		{
			if(disposing)
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}
	}
}
