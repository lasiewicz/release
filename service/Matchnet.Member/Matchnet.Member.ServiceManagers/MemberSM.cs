using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections;

using System.IO;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using ServiceConstants = Matchnet.Member.ValueObjects.ServiceConstants;


namespace Matchnet.Member.ServiceManagers
{
    public class MemberSM : MarshalByRefObject, IMemberService, IServiceManager, IReplicationRecipient, IDisposable, IMemberAppDeviceService
	{
		//member perf
		private PerformanceCounter _perfCountMember;
		private PerformanceCounter _perfRequestsSecMember;
		private PerformanceCounter _perfHitRateMember;
		private PerformanceCounter _perfHitRateMember_Base;
		private PerformanceCounter _perfSavesSecMember;

		//logon perf
		private PerformanceCounter _perfRequestsSecLogon;
		private PerformanceCounter _perfSuccessRateLogon;
		private PerformanceCounter _perfSuccessRateLogon_Base;

		//reg perf
		private PerformanceCounter _perfRequestsSecRegister;
		private PerformanceCounter _perfSuccessRateRegister;
		private PerformanceCounter _perfSuccessRateRegister_Base;

		private HydraWriter _hydraWriter;
		private Replicator _replicator = null;
		private Synchronizer _synchronizer = null;

		public MemberSM()
		{
			//perf
			initPerfCounters();
			MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(MemberBL_MemberRequested);
			MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(MemberBL_MemberAdded);
			MemberBL.Instance.MemberRemoved += new Matchnet.Member.BusinessLogic.MemberBL.MemberRemoveEventHandler(MemberBL_MemberRemoved);
			MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(MemberBL_MemberSaved);

			MemberBL.Instance.LogonRequested += new Matchnet.Member.BusinessLogic.MemberBL.LogonRequestEventHandler(MemberBL_LogonRequested);

			MemberBL.Instance.RegistrationRequested += new Matchnet.Member.BusinessLogic.MemberBL.RegistrationRequestEventHandler(MemberBL_RegistrationRequested);
			MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(MemberBL_SynchronizationRequested);

			//replication
			MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(MemberBL_ReplicationRequested);
			MemberPrivilegeBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberPrivilegeBL.ReplicationEventHandler(MemberBL_ReplicationRequested);
			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

#if DEBUG
			_hydraWriter = new HydraWriter(new string[]{"mnMember", "mnLogon", "mnAdmin", "mnFile", "mnAlert", "mnSearchStore", "mnQuestionAnswer"});
#else
			_hydraWriter = new HydraWriter(new string[]{"mnMember", "mnLogon", "mnAdmin", "mnFile", "mnAlert", "mnQuestionAnswer"});
#endif
            _hydraWriter.Start();

			string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME, machineName);
			}
			if(replicationURI!= null && replicationURI.Length != 0)
			{
                System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "Replication URI: " + replicationURI);								  
				_replicator = new Replicator(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME);
				_replicator.SetDestinationUri(replicationURI);
				_replicator.Start();
			}

			_synchronizer = new Synchronizer(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME);
			_synchronizer.Start();
		}	


		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		public void PrePopulateCache()
		{
		}


		private void MemberBL_ReplicationRequested(IReplicable replicableObject)
		{
			if(_replicator != null)
			{
					bool replicate=Conversion.CBool(RuntimeSettings.GetSetting("MEMBER_SVC_REPLICATION_FLAG"));
				if(replicate)
					_replicator.Enqueue(replicableObject);

			}
		}


		private void MemberBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
		{
			_synchronizer.Enqueue(key, cacheReferences);
		}


		#region logon
        [Obsolete]
        public int Auth(string emailAddress, string password, out int memberID)
        {
            throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "This method is no longer supported", new NotSupportedException());
        }

        [Obsolete]
        public int Auth(string emailAddress, string password, string passwordHash, out int memberID)
		{
            Spark.Logging.RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_MANAGER_NAME,"Obsolete Auth() used.", null);
		    return Auth(out memberID, emailAddress, passwordHash);
		}

        public int Auth(out Int32 memberID, string emailAddress, string passwordHash)
        {
            try
            {
                return Auth(out memberID, 0, emailAddress, passwordHash);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing Authenticate.", ex);
            }
        }

        public int Auth(out Int32 memberID, int communityId, string emailAddress, string passwordHash)
        {
            try
            {
                return MemberBL.Instance.Authenticate(emailAddress, communityId, passwordHash, out memberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing Authenticate.", ex);
            }
        }

        /// <summary>
        /// PM-218 Remove after it's rolled out.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [Obsolete]
        public MemberRegisterResult Register(string emailAddress, string username, string password)
        {
            throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "This method is no longer supported", new NotSupportedException());
        }

        /// <summary>
        /// PM-218 Remove after it's rolled out.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="passwordHash"> </param>
        /// <returns></returns>
        [Obsolete]
        public MemberRegisterResult Register(string emailAddress, string username, string password, string passwordHash)
        {
            throw new NotSupportedException();
        }

        [Obsolete]
        public MemberRegisterResult Register(string emailAddress, string username, string password, int groupID)
        {
            throw new NotSupportedException();
        }

        [Obsolete]
        public MemberRegisterResult Register(string emailAddress, string username, string password, string passwordHash, int groupID)
        {
            throw new NotSupportedException();
        }


        [Obsolete]
        public MemberRegisterResult Register(string emailAddress, string username, string password, string passwordHash, Brand brand)
		{
            Spark.Logging.RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_MANAGER_NAME, "Obsolete Register() used.", null);
            return Register(brand, emailAddress, username, passwordHash);
		}

        public CreateLogonCredentialsResult CreateLogonCredentials(string emailAddress, string username, string passwordHash, Brand brand)
        {
            try
            {
                return MemberBL.Instance.CreateLogonCredentials(emailAddress, username, passwordHash, brand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure creating logon credentials.", ex);
            }
        }


        public MemberRegisterResult Register(Brand brand, string emailAddress, string username, string passwordHash)
        {
            try
            {
                return MemberBL.Instance.Register(emailAddress, username, passwordHash, brand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing Register.", ex);
            }
        }

		public string GetPassword(int memberID)
		{
			try 
			{
				return MemberBL.Instance.GetPassword(memberID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting password.", ex);
			}
		}


		public string GetPassword(string emailAddress)
		{
			try 
			{
				return MemberBL.Instance.GetPassword(emailAddress);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting password.", ex);
			}
		}


        public int GetNewMemberID()
        {
            try
            {
                return MemberBL.Instance.GetNewMemberID();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting memberID.", ex);
            }
        }

		public int GetMemberID(string username)
		{
			return GetMemberID(username, Constants.NULL_INT);
		}

		public int GetMemberID(string username, int groupID)
		{
            try
            {
                return MemberBL.Instance.GetMemberID(username, groupID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting memberID.", ex);
            }
		}


		public void SetMemberGroup(Int32 memberID,
			Int32 communityID,
			Int32 siteID,
			Int32 brandID)
		{
			try 
			{
				MemberBL.Instance.SetMemberGroup(memberID,
					communityID,
					siteID,
					brandID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure setting memberGroup.", ex);
			}
		}

		public int GetMemberIDByEmail(string emailAddress)
		{
			try 
			{
				return MemberBL.Instance.GetMemberIDByEmail(emailAddress);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting memberID.", ex);
			}
		}

        public int GetMemberIDByEmail(string emailAddress, int communityId)
        {
            try
            {
                return MemberBL.Instance.GetMemberIDByEmail(emailAddress, communityId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting memberID.", ex);
            }
        }

        public bool CheckEmailAddressExists(string emailAddress, int communityId)
        {
            try
            {
                return MemberBL.Instance.CheckEmailAddressExists(emailAddress, communityId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure checking if an email address already exists.", ex);
            }
        }

        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            try
            {
                return MemberBL.Instance.ValidateResetPasswordToken(tokenGuidString);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure validating reset password token.", ex);
            }
        }

	    public string ResetPassword(int memberId)
	    {
	        try
	        {
	            return MemberBL.Instance.ResetPassword(memberId);
	        }
	        catch (Exception ex)
	        {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure generating a password reset token.", ex);
	        }
	    }

        public void ExpireImpersonationToken(int adminMemberId)
        {
            try
            {
                MemberBL.Instance.ExpireImpersonationToken(adminMemberId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure expiring an impersonation token.", ex);
            }
        }

        public string GenerateImpersonateToken(int adminMemberId, int impersonateMemberId, int impersonateBrandId)
        {
            try
            {
                return MemberBL.Instance.GenerateImpersonateToken(adminMemberId, impersonateMemberId, impersonateBrandId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure generating an impersonation token.", ex);
            }
        }

        public ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            try
            {
                return MemberBL.Instance.ValidateImpersonateToken(impersonateTokenGUID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure validating an impersonation token.", ex);
            }
        }
	    #endregion

		#region member

        public void ExpireEntireMemberCache()
        {
            try
            {
                MemberBL.Instance.ExpireEntireMemberCache();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure expiring member cache.", ex);
            }
        }


		public void ExpireCachedMember(int memberID)
		{
			try 
			{
				MemberBL.Instance.ExpireCachedMember(memberID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure expiring member from cache.", ex);
			}
		}

        public void ExpireCachedMemberAccess(int memberID)
        {
            try
            {
                MemberBL.Instance.ExpireCachedMemberAccess(memberID, true, true, false);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure expiring CachedMemberAccess from cache. MemberID: {0}", memberID), ex);
            }
        }

        public bool ExpireCachedMemberAccess(int memberID, bool replicate, bool synchronize, bool expireAccessSvc)
        {
            bool returnValue = true;
            try
            {
                returnValue = MemberBL.Instance.ExpireCachedMemberAccess(memberID, replicate, synchronize, expireAccessSvc);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure expiring CachedMemberAccess from cache. MemberID: {0}", memberID), ex);
            }

            return returnValue;
        }
		
		public CachedMember GetCachedMember(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad)
		{
			try 
			{
				return MemberBL.Instance.GetCachedMember(clientHostName,
					cacheReference,
					memberID,
					forceLoad);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Member.", ex);
			}
		}

        public MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId)
        {
            try
            {
                return MemberBL.Instance.GetMemberBasicLogonInfo(memberId, communityId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting member basic logon info.", ex);
            }
        }

        public IMemberDTO GetMemberDTO(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            MemberType type,
            bool forceLoad)
        {
            try
            {
                return MemberBL.Instance.GetMemberDTO(clientHostName,
                    cacheReference,
                    memberID,
                    type,
                    forceLoad);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting MemberDTO.", ex);
            }
        }

        public IMemberDTO[] GetMemberDTOs(string clientHostName, CacheReference cacheReference, Int32[] memberIDs, MemberType type, bool forceLoad)
        {
            try
            {
                return MemberBL.Instance.GetMemberDTOs(clientHostName,
                    cacheReference,
                    memberIDs,
                    type,
                    forceLoad);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting MemberDTOs.", ex);
            }            
        }

		public byte[] GetCachedMemberBytes(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad)
		{
			try 
			{
				return MemberBL.Instance.GetCachedMemberBytes(clientHostName,
					cacheReference,
					memberID,
					forceLoad);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Member.", ex);
			}
		}

		public CachedMember[] GetCachedMembers(string clientHostName,
			CacheReference cacheReference,
			Int32[] memberIDs,
			bool forceLoad)
		{
			try 
			{
				return MemberBL.Instance.GetCachedMembers(clientHostName,
					cacheReference,
					memberIDs,
					forceLoad);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Members.", ex);
			}
		}



		//versioning

		public CachedMember GetCachedMember(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad, int version)
		{
			try 
			{
				return MemberBL.Instance.GetCachedMember(clientHostName,
					cacheReference,
					memberID,
					forceLoad,version);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Member.", ex);
			}
		}


		public byte[] GetCachedMemberBytes(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			bool forceLoad,int version)
		{
			try 
			{
				return MemberBL.Instance.GetCachedMemberBytes(clientHostName,
					cacheReference,
					memberID,
					forceLoad,version);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Member.", ex);
			}
		}

		public CachedMember[] GetCachedMembers(string clientHostName,
			CacheReference cacheReference,
			Int32[] memberIDs,
			bool forceLoad,int version)
		{
			try 
			{
				return MemberBL.Instance.GetCachedMembers(clientHostName,
					cacheReference,
					memberIDs,
					forceLoad,version);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Members.", ex);
			}
		}

        public CachedMemberLogon GetCachedMemberLogon(string clientHostName,
           CacheReference cacheReference, int memberID, int groupID, bool forceLoad)
        {
            try
            {
                return MemberBL.Instance.GetCachedMemberLogon(clientHostName, cacheReference, memberID, groupID, forceLoad);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure in MemberSM.GetCachedMemberLogon(). MemberID: {0}, GroupID: {1}", memberID, groupID), ex);
            }
        }

        public CachedMemberLogon GetCachedMemberLogon(string clientHostName,
         CacheReference cacheReference, int memberID, int groupID, bool forceLoad, int cachedMemberLogonVersion)
        {
            try
            {
                return MemberBL.Instance.GetCachedMemberLogon(clientHostName, cacheReference, memberID, groupID, forceLoad, cachedMemberLogonVersion);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure in MemberSM.GetCachedMemberLogon(). MemberID: {0}, GroupID: {1}", memberID, groupID), ex);
            }
        }

        /// <summary>
        /// The user stories for this are FNOLA-369 & FNOLA-1384
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="cacheReference"></param>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="forceLoad"></param>
        /// <param name="cachedMemberEmailVersion"></param>
        /// <returns></returns>
        public CachedMemberEmail GetCachedMemberEmail(string clientHostName,
         CacheReference cacheReference, int memberID, bool forceLoad, int cachedMemberEmailVersion)
        {
            try
            {
                return MemberBL.Instance.GetCachedMemberEmail(clientHostName, cacheReference, memberID, forceLoad, cachedMemberEmailVersion);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure in MemberSM.GetCachedMemberEmail(). MemberID: {0}", memberID), ex);
            }
        }


        public CachedMemberAccess GetCachedMemberAccess(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad)
        {
            try
            {
                return MemberBL.Instance.GetCachedMemberAccess(clientHostName, cacheReference, memberID, forceLoad);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure in MemberSM.GetCachedMemberAccess(). MemberID: {0}", memberID), ex);
            }
        }

        public bool AdjustUnifiedAccessCountPrivilege(int memberID, int adminID, string adminDomainUserName, int[] privilegeTypeID, int brandID, int siteID, int communityID, int count)
        {
            try
            {
                return MemberBL.Instance.AdjustUnifiedAccessCountPrivilege(memberID, adminID, adminDomainUserName, privilegeTypeID, brandID, siteID, communityID, count);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format("Failure in MemberSM.AdjustUnifiedAccessCountPrivilege(). MemberID: {0}", memberID), ex);
            }

        }

        public void SaveMemberLastLogon(string clientHostname, int memberID, int groupID, DateTime lastLogon)
        {
            try
            {
                MemberBL.Instance.SaveMemberLastLogon(clientHostname, memberID,  groupID, lastLogon);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, string.Format("Failure saving MemberLastLogon. MemberID: {0}, GroupID: {1}", memberID, groupID), ex);
            }
        }

        public void SaveMemberLastLogon(string clientHostname, int memberID, int groupID, DateTime lastLogon, int adminMemberID)
        {
            try
            {
                MemberBL.Instance.SaveMemberLastLogon(clientHostname, memberID, groupID, lastLogon, adminMemberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, string.Format("Failure saving MemberLastLogon. MemberID: {0}, GroupID: {1}", memberID, groupID), ex);
            }
        }

        public MemberSaveResult SaveNewMember(string clientHostName,
        MemberUpdate memberUpdate, int version, int groupID)
        {
            try
            {
                return MemberBL.Instance.SaveNewMember(clientHostName,
                    memberUpdate, version, groupID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving New Member.", ex);
            }	
        }


		public MemberSaveResult SaveMember(string clientHostName,
			MemberUpdate memberUpdate)
		{
			try 
			{
				return MemberBL.Instance.SaveMember(clientHostName,
					memberUpdate,0, Constants.NULL_INT);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving Member.", ex);
			}	
		}

		public MemberSaveResult SaveMember(string clientHostName,
			MemberUpdate memberUpdate, int version)
		{
			try 
			{
				return MemberBL.Instance.SaveMember(clientHostName,
					memberUpdate,version, Constants.NULL_INT);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving Member.", ex);
			}	
		}

		
		public MemberSaveResult SaveMember(string clientHostName,
			MemberUpdate memberUpdate, int version, int groupID)
		{
			try 
			{
				return MemberBL.Instance.SaveMember(clientHostName,
					memberUpdate,  version, groupID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving Member.", ex);
			}
		}

        public MemberSaveResult SaveMember(string clientHostName,
                                           MemberUpdate memberUpdate, int version, Brand brand, bool updateMemberGroup)
        {
            try
            {
                return MemberBL.Instance.SaveMember(clientHostName,
                    memberUpdate, version, brand, updateMemberGroup);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving Member.", ex);
            }
        }

        public void UpdateSearch(int memberID, int communityID)
        {
            try
            {
                //MemberBL.Instance.UpdateSearch(memberID, communityID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure updating search.", ex);
            }
        }

        public string GetMemberSalt(string encryptedEmail)
        {
            return GetMemberSalt(encryptedEmail,0);
        }

	    public string GetMemberSalt(string encryptedEmail, int communityId)
        {
            try
            {
                return MemberBL.Instance.GetMemberSalt(encryptedEmail, communityId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting salt for member", ex);
            }
        }

	    #endregion

		#region memberprivilege
		public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID)
		{
			try
			{
				return MemberPrivilegeBL.Instance.GetMemberPrivileges(memberID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured when retrieving member privileges", ex);
			}
		}


		public void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID)
		{
			try
			{
				MemberPrivilegeBL.Instance.SaveMemberPrivilege(memberID, privilegeID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured when saving member privilege.", ex);
			}
		}


		public void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID)
		{
			try
			{
				MemberPrivilegeBL.Instance.DeleteMemberPrivilege(memberID, privilegeID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured when deleteing member privilege.", ex);
			}
		}


		public SecurityGroupMemberCollection GetSecurityGroupMembers(Int32 securitySecurityGroupID)
		{
			try
			{
				return MemberPrivilegeBL.Instance.GetSecurityGroupMembers(securitySecurityGroupID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured when retrieving security securitySecurityGroup members.", ex);
			}
		}


		public void SaveSecurityGroupMember(Int32 memberID, Int32 securitySecurityGroupID)
		{
			try
			{
				MemberPrivilegeBL.Instance.SaveSecurityGroupMember(memberID, securitySecurityGroupID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured when saving security securitySecurityGroup member.", ex);
			}
		}


		public void DeleteSecurityGroupMember(Int32 memberID, Int32 securitySecurityGroupID)
		{
			try
			{
				MemberPrivilegeBL.Instance.DeleteSecurityGroupMember(memberID, securitySecurityGroupID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured when deleting security securitySecurityGroup member.", ex);
			}
		}


		public SecurityGroupCollection GetSecurityGroups()
		{
			try
			{
				return MemberPrivilegeBL.Instance.GetSecurityGroups();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while retrieving security securitySecurityGroups.", ex);
			}
		}


		public PrivilegeCollection GetPrivileges()
		{
			try
			{
				return MemberPrivilegeBL.Instance.GetPrivileges();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while retrieving security privileges.", ex);
			}
		}

		public void DeleteGroupPrivilege(Int32 securityGroupID, Int32 privilegeID)
		{
			try
			{
				MemberPrivilegeBL.Instance.DeleteGroupPrivilege(securityGroupID,privilegeID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while deleting group privileges.", ex);
			}
		}

		public void SaveGroupPrivilege(Int32 securityGroupID,Int32 privilegeID)
		{
			try
			{
				MemberPrivilegeBL.Instance.SaveGroupPrivilege(securityGroupID, privilegeID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving group privileges.", ex);
			}
		}

		public void SavePrivilege(Int32 privilegeID, string description)
		{
			try
			{
				MemberPrivilegeBL.Instance.SavePrivilege(privilegeID,description);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving a privilege.", ex);
			}
		}

		public int AddPrivilege(string description)
		{
			try
			{
				return MemberPrivilegeBL.Instance.AddPrivilege(description);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while adding a privilege.", ex);
			}
		}

		public int AddGroup(string description)
		{
			try
			{
				return MemberPrivilegeBL.Instance.AddGroup(description);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while adding a SecurityGroup.", ex);
			}
		}

		public void SaveGroup(int securityGroupID, string description)
		{
			try
			{
				MemberPrivilegeBL.Instance.SaveGroup(securityGroupID, description);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving a SecurityGroup.", ex);
			}
		}

		public SecurityGroupCollection GetGroupsByPrivilege(Int32 privilegeID)
		{
			try
			{
				return MemberPrivilegeBL.Instance.GetGroupsByPrivilege(privilegeID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while retrieving privilages by a group.", ex);
			}
		}

		public ArrayList GetMembersByPrivilege(Int32 privilegeID)
		{
			try
			{
				return MemberPrivilegeBL.Instance.GetMembersByPrivilege(privilegeID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while retrieving members by a privilegeID.", ex);
			}
		}
		#endregion

		#region photo
		public void GetNewImageFile(out Int32 fileID,
			out string filePath,
			out string fileWebPath)
		{
			try
			{
				MemberBL.Instance.GetNewImageFile(out fileID,
					out filePath,
					out fileWebPath);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error getting new image file.", ex));
			}
		}

		
		public PhotoCollection SavePhotos(string clientHostName,
			Int32 communityID,
			Int32 memberID,
			PhotoUpdate[] photoUpdates)
		{
			try
			{
				return MemberBL.Instance.SavePhotos(clientHostName,
					communityID,
					memberID,
					photoUpdates, 0);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving photos.", ex));
			}
		}

		public PhotoCollection SavePhotos(string clientHostName,
			Int32 communityID,
			Int32 memberID,
			PhotoUpdate[] photoUpdates, int version)
		{
			try
			{
				return MemberBL.Instance.SavePhotos(clientHostName,
					communityID,
					memberID,
					photoUpdates, version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving photos.", ex));
			}
		}

        public PhotoCollection SavePhotos(string clientHostName,
            Int32 brandID,
            Int32 siteID,
            Int32 communityID,
            Int32 memberID,
            PhotoUpdate[] photoUpdates, int version)
        {
            try
            {
                return MemberBL.Instance.SavePhotos(clientHostName,
                    brandID,
                    siteID,
                    communityID,
                    memberID,
                    photoUpdates, version);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving photos.", ex));
            }
        }

		public void InsertPhotoSearch(PhotoUpdate update, int memberid,int communityid) 
		{
			try
			{
				MemberBL.Instance.InsertPhotoSearch(update,memberid, communityid);
						
			}
			catch(Exception ex)
			{System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.MemberSM","Error inserting into PhotoStore:" + ex.Message,System.Diagnostics.EventLogEntryType.Warning);}
		}

		/*
		public void GetFilePath(Int32 fileID,
			out string filePath,
			out string fileWebPath)
		{
			try
			{
				MemberBL.Instance.GetFilePath(fileID,
					out filePath,
					out fileWebPath);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting file path (fileID: " + fileID.ToString() + ").", ex));
			}
		}


		public void UpdateFile(Int32 fileID, long fileSize)
		{
			try
			{
				MemberBL.Instance.UpdateFile(fileID, fileSize);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error updating file (fileID: " + fileID.ToString() + ", fileSize: " + fileSize.ToString() + ").", ex);
			}
		}


		public void InsertFile(Int32 fileID,
			Int32 rootID,
			string fileExtension,
			DateTime insertDate)
		{
			try
			{
				MemberBL.Instance.InsertFile(fileID,
					rootID,
					fileExtension,
					insertDate);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error inserting file (fileID: " + fileID.ToString() + ").", ex);
			}
		}

			
		public void DeleteFile(Int32 fileID)
		{
			try
			{
				MemberBL.Instance.DeleteFile(fileID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error deleting file (fileID: " + fileID.ToString() + ").", ex);
			}
		}
		*/
		#endregion
		
		#region Messmo Stuff
		public void SubscribeToMessmoDailyFeed(int memberID, int communityID)
		{
			try
			{
				MemberBL.Instance.SubscribeToMessmoDailyFeed(memberID, communityID);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while trying to subscribe member to Messmo's daily feed.", ex));
			}
		}

		public void UnsubscribeFromMessmoDailyFeed(int memberID, int communityID)
		{
			try
			{
				MemberBL.Instance.UnsubscribeFromMessmoDailyFeed(memberID, communityID);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while trying to unsubscribe member from Messmo's daily feed.", ex));
			}
		}

        public void ToggleLogonDisabledFlag(int memberID, int activeFlag)
        {
            try
            {
                MemberBL.Instance.ToggleLogonDisabledFlag(memberID, activeFlag);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while toggling logon disabled flag.", ex));
            }
        }

		public void SaveLogonMessmo(string messmoID, int memberID, int communityID)
		{
			try
			{
				MemberBL.Instance.SaveLogonMessmo(messmoID,
					memberID,
					communityID);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while saving LogonMessmo.", ex));
			}
		}

        public void SendMessmoUserTransactionStatus(int memberID, int siteID, DateTime newSubExpDate)
        {
            try
            {
                MemberBL.Instance.SendMessmoUserTransactionStatus(memberID, siteID, newSubExpDate);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured in SendMessmoUserTransactionStatus().", ex));
            }
        }

		public int GetMemberIDByMessmoID(string messmoID, int communityID)
		{
			try
			{
				return MemberBL.Instance.GetMemberIDByMessmoID(messmoID, communityID);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while retrieving LogonMessmo.", ex));
			}
		}

        public bool SavePassword(int memberId, int communityId, string passwordHash)
        {
            try
            {
                return MemberBL.Instance.SavePassword(memberId, communityId, passwordHash);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while saving password.", ex));
            }
        }
		#endregion

        #region IMemberAppDeviceService implementation

        public MemberAppDeviceCollection GetMemberAppDevices(Int32 memberID)
        {
            try
            {
                return MemberBL.Instance.GetMemberAppDevices(memberID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting Member App Devices.", ex);
            }
        }

        public void RemoveMemberAppDevice(int memberID, int AppID, string DeviceID)
        {
            try
            {
                MemberBL.Instance.RemoveMemberAppDevice(memberID, AppID, DeviceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure removing Member App Device.", ex);
            }
        }

        public void UpdateMemberAppDeviceNotificationsFlags(Int32 memberID, Int32 AppID, string DeviceID, Int64 PushNotificationsMap)
        {
            try
            {
                MemberBL.Instance.UpdateMemberAppDeviceNotificationsFlags(memberID, AppID, DeviceID, PushNotificationsMap);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure updating Member App Device.", ex);
            }
        }

        public void AddMemberAppDevice(Int32 memberID, Int32 appID, string deviceID, int brandID, string deviceData, Int64 pushNotificationsFlags)
        {
            try
            {
                MemberBL.Instance.AddMemberAppDevice(memberID, appID, deviceID, brandID, deviceData, pushNotificationsFlags);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure adding Member App Device.", ex);
            }
        }


        public void DeleteMemberAppDevice(Int32 memberID, Int32 appID, string deviceID)
        {
            try
            {
                MemberBL.Instance.DeleteMemberAppDevice(memberID, appID, deviceID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure found when deleting Member App Device.", ex);
            }
        }

        #endregion  

        void IReplicationRecipient.Receive(IReplicable replicableObject)
		{
			try
			{
				bool replicate=Conversion.CBool(RuntimeSettings.GetSetting("MEMBER_SVC_REPLICATION_FLAG"));
				if(!replicate)
					return;
				if(replicableObject == null)
					return;
				switch (replicableObject.GetType().Name)
				{
					case "CachedMemberBytes": 
					    CachedMemberBytes cachedBytes=(CachedMemberBytes)replicableObject;
                        //System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "Received cachedMemberBytes for replication.");

                        //OLD - if(cachedBytes != null & cachedBytes.Version == CachedMemberBytes.CACHED_MEMBERBYTES_VERSION)
                        //Removing version check and inserting member into cache irrespective of version number
						if(cachedBytes != null)
						{	
                            MemberBL.Instance.InsertCachedMemberBytes((CachedMemberBytes)replicableObject,false);
                        }
						//else
						//{
						//	System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service","Received old replication object version:" + cachedBytes.Version.ToString() + ", MemberID=" + cachedBytes.MemberID.ToString());
						//}
						break;
					
                    case "CachedMemberAccess":
                        CachedMemberAccess cachedMemberAccess = (CachedMemberAccess)replicableObject;
                        //System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "Received CachedMemberAccess for replication. Expire: " + cachedMemberAccess.ReplicateForExpire.ToString() + ", MemberID:" + cachedMemberAccess.MemberID, EventLogEntryType.Warning);
                        if (cachedMemberAccess != null)
                        {
                            if (cachedMemberAccess.ReplicateForExpire)
                            {
                                MemberBL.Instance.ExpireCachedMemberAccess(cachedMemberAccess.MemberID, false, true, false);
                            }
                            else
                            {
                                MemberBL.Instance.InsertCachedMemberAccess(cachedMemberAccess, false);
                            }
                        }
                        break;
                    case "CachedMemberLogon":
                        CachedMemberLogon cachedMemberLogon = (CachedMemberLogon)replicableObject;
                        if (cachedMemberLogon != null)
                        {
                            MemberBL.Instance.InsertCachedMemberLogon(cachedMemberLogon, false);
                        }
                        else
                        {
                            System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "CachedMemberLogon is NULL. Cannot replicate this.");
                        }
                        break;
                    case "CachedMemberEmail":
                        CachedMemberEmail cachedMemberEmail = (CachedMemberEmail)replicableObject;
                        if (cachedMemberEmail != null)
                        {
                            MemberBL.Instance.InsertCachedMemberEmail(cachedMemberEmail, false);
                        }
                        else
                        {
                            System.Diagnostics.EventLog.WriteEntry("Matchnet.Member.Service", "CachedMemberEmail is NULL. Cannot replicate this.");
                        }
                        break;
					case "MemberPrivilegeCollection":
						MemberPrivilegeBL.Instance.CacheMemberPrivileges((MemberPrivilegeCollection)replicableObject,
							false);
						break;

					default:
						throw new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
				}
			}
			catch(Exception ex)
			{new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Exception in replication recipient: " + ex.ToString());}
		}

        #region admin member search crap
        public int[] PerformAdminSearch(string email, string userName, string memberId, string phone, string firstName,
                                string lastName, string zipcode, string city, string ipAddress, int communityId, bool exactUsername, int filterType)
        {
            try
            {
                return MemberBL.Instance.PerformAdminSearch(email, userName, memberId, phone, firstName, lastName, zipcode,
                                                            city, ipAddress, communityId, exactUsername, filterType);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while performing an Admin Search.", ex));
            }
        }

        public int[] PerformAdminSearchPagination(string email, string userName, string memberId, string phone, string firstName,
                             string lastName, string zipcode, string city, string ipAddress, int communityId, bool exactUsername, int filterType,
                                int pageNumber, int rowsPerPage)
        {
            try
            {
                return MemberBL.Instance.PerformAdminSearchPagination(email, userName, memberId, phone, firstName, lastName, zipcode,
                                                            city, ipAddress, communityId, exactUsername, filterType, pageNumber, rowsPerPage);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while performing an Admin Search (Pagination version).", ex));
            }
        }

	    //added the word crap
		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByEmail(string email,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			return GetMembersByEmail(null,
				null,
				email,
				startRow,
				pageSize,
				ref totalRows);
		}


		public ArrayList GetMembersByEmail(string machineName,
			CacheReference cacheReference,
			string email,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			try
			{
				return MemberBL.Instance.GetMembersByEmail(machineName,
					cacheReference,
					email,
					startRow,
					pageSize,
					ref totalRows);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by email.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByUserName(string userName,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			return GetMembersByUserName(null,
				null,
				userName,
				startRow,
				pageSize,
				ref totalRows);
		}
			
			
		public ArrayList GetMembersByUserName(string machineName,
			CacheReference cacheReference,
			string userName,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			try
			{
				return MemberBL.Instance.GetMembersByUserName(machineName,
					cacheReference,
					userName,
					startRow,
					pageSize,
					ref totalRows);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by user name.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByPhoneNumber(string phoneNumber,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			return GetMembersByPhoneNumber(null,
				null,
				phoneNumber,
				startRow,
				pageSize,
				ref totalRows);
		}


		public ArrayList GetMembersByPhoneNumber(string machineName,
			CacheReference cacheReference,
			string phoneNumber,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			try
			{
				return MemberBL.Instance.GetMembersByPhoneNumber(machineName,
					cacheReference,
					phoneNumber,
					startRow,
					pageSize,
					ref totalRows);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by phone number.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByCreditCard(string creditCard,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			return GetMembersByCreditCard(null,
				null,
				creditCard,
				startRow,
				pageSize,
				ref totalRows);
		}


		public ArrayList GetMembersByCreditCard(string machineName,
			CacheReference cacheReference,
			string creditCard,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			try
			{
				return MemberBL.Instance.GetMembersByCreditCard(machineName,
					cacheReference,
					creditCard,
					startRow,
					pageSize,
					ref totalRows);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by credit card.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByChecking(string routing,
			string checking,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			return GetMembersByChecking(null,
				null,
				routing,
				checking,
				startRow,
				pageSize,
				ref totalRows);
		}

		
		public ArrayList GetMembersByChecking(string machineName,
			CacheReference cacheReference,
			string routing,
			string checking,
			int startRow,
			int pageSize,
			ref int totalRows)
		{
			try
			{
				return MemberBL.Instance.GetMembersByChecking(machineName,
					cacheReference,
					routing,
					checking,
					startRow,
					pageSize,
					ref totalRows);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by checking.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByMemberID(int memberID,
			ref int totalRows)
		{
			return GetMembersByMemberID(null,
				null,
				memberID,
				ref totalRows);
		}

		
		public ArrayList GetMembersByMemberID(string machineName,
			CacheReference cacheReference,
			int memberID,
			ref int totalRows)
		{
			try
			{
				return MemberBL.Instance.GetMembersByMemberID(machineName,
					cacheReference,
					memberID,
					ref totalRows);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by memberID.", ex));
			}
		}


		public MemberPaymentCollection GetMemberPaymentInfo(int memberID)
		{
			try
			{
				return MemberBL.Instance.GetMemberPaymentInfo(memberID);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member payment info.", ex));
			}
		}

		//versioning added
		public ArrayList GetMembersByEmail(string email,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			return GetMembersByEmail(null,
				null,
				email,
				startRow,
				pageSize,
				ref totalRows,version);
		}


		public ArrayList GetMembersByEmail(string machineName,
			CacheReference cacheReference,
			string email,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			try
			{
				return MemberBL.Instance.GetMembersByEmail(machineName,
					cacheReference,
					email,
					startRow,
					pageSize,
					ref totalRows,version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by email.", ex));
			}
		}

        public ArrayList GetMembersByEmail(string machineName,
            CacheReference cacheReference,
            string email,
            int startRow,
            int pageSize,
            ref int totalRows,
            int version,
            bool memberIdOrderByDesc)
        {
            try
            {
                return MemberBL.Instance.GetMembersByEmail(machineName,
                    cacheReference,
                    email,
                    startRow,
                    pageSize,
                    ref totalRows,
                    version,
                    memberIdOrderByDesc);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by email.", ex));
            }
        }


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByUserName(string userName,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			return GetMembersByUserName(null,
				null,
				userName,
				startRow,
				pageSize,
				ref totalRows,version);
		}
			
			
		public ArrayList GetMembersByUserName(string machineName,
			CacheReference cacheReference,
			string userName,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			try
			{
				return MemberBL.Instance.GetMembersByUserName(machineName,
					cacheReference,
					userName,
					startRow,
					pageSize,
					ref totalRows,version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by user name.", ex));
			}
		}

        public ArrayList GetMembersByUserName(string machineName,
            CacheReference cacheReference,
            string userName,
            int startRow,
            int pageSize,
            ref int totalRows,
            int version,
            bool memberIdOrderByDesc,
            bool exactMatchOnly)
        {
            try
            {
                return MemberBL.Instance.GetMembersByUserName(machineName,
                    cacheReference,
                    userName,
                    startRow,
                    pageSize,
                    ref totalRows,
                    version,
                    memberIdOrderByDesc,
                    exactMatchOnly);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by user name.", ex));
            }
        }


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByPhoneNumber(string phoneNumber,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			return GetMembersByPhoneNumber(null,
				null,
				phoneNumber,
				startRow,
				pageSize,
				ref totalRows,version);
		}


		public ArrayList GetMembersByPhoneNumber(string machineName,
			CacheReference cacheReference,
			string phoneNumber,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			try
			{
				return MemberBL.Instance.GetMembersByPhoneNumber(machineName,
					cacheReference,
					phoneNumber,
					startRow,
					pageSize,
					ref totalRows,version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by phone number.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByCreditCard(string creditCard,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			return GetMembersByCreditCard(null,
				null,
				creditCard,
				startRow,
				pageSize,
				ref totalRows,version);
		}


		public ArrayList GetMembersByCreditCard(string machineName,
			CacheReference cacheReference,
			string creditCard,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			try
			{
				return MemberBL.Instance.GetMembersByCreditCard(machineName,
					cacheReference,
					creditCard,
					startRow,
					pageSize,
					ref totalRows,version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by credit card.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByChecking(string routing,
			string checking,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			return GetMembersByChecking(null,
				null,
				routing,
				checking,
				startRow,
				pageSize,
				ref totalRows,version);
		}

		
		public ArrayList GetMembersByChecking(string machineName,
			CacheReference cacheReference,
			string routing,
			string checking,
			int startRow,
			int pageSize,
			ref int totalRows, int version)
		{
			try
			{
				return MemberBL.Instance.GetMembersByChecking(machineName,
					cacheReference,
					routing,
					checking,
					startRow,
					pageSize,
					ref totalRows,version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by checking.", ex));
			}
		}


		//deprecated, remove after next web tier deployment
		public ArrayList GetMembersByMemberID(int memberID,
			ref int totalRows, int version)
		{
			return GetMembersByMemberID(null,
				null,
				memberID,
				ref totalRows,version);
		}

		
		public ArrayList GetMembersByMemberID(string machineName,
			CacheReference cacheReference,
			int memberID,
			ref int totalRows, int version)
		{
			try
			{
				return MemberBL.Instance.GetMembersByMemberID(machineName,
					cacheReference,
					memberID,
					ref totalRows,version);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured while getting member by memberID.", ex));
			}
		}

      
	
		#endregion

		#region admin others
		public AdminMemberDomainMapperCollection GetAdminMemberDomainMappers()
		{
			try
			{
				return MemberBL.Instance.GetAdminMemberDomainMappers();
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured in MemberSm.GetAdminMemberDomainMappers()", ex));
			}
		}

        public void IncrementCommunicationCounter(int memberId, int siteId, int[] counterType)
        {
            try
            {
                MemberBL.Instance.IncrementCommunicationCounter(memberId, siteId, counterType);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured in MemberSM.IncrementCommunicationCounter()", ex));
            }
        }

        public Dictionary<int, int> GetCommunicationWarningCounts(int memberId, int siteId)
        {

            try
            {
                return MemberBL.Instance.GetCommunicationWarningCounts(memberId, siteId);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Error occured in MemberSM.GetCommunicationWarningCounts()", ex));
            }
        }

	    #endregion

		#region instrumentation
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("Member Items", "Member Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("Member Requests/second", "Member Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Member Hit Rate", "Member Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Member Hit Rate_Base","Member Hit Rate_Base", PerformanceCounterType.RawBase),
														 new CounterCreationData("Member Saves/second", "Member Saves/second", PerformanceCounterType.RateOfCountsPerSecond32),

														 new CounterCreationData("Logon Requests/second", "Logon Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Logon Success Rate", "Logon Success Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Logon Success Rate_Base","Logon Success Rate", PerformanceCounterType.RawBase),

														 new CounterCreationData("Registration Requests/second", "Registration Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Registration Success Rate", "Registration Success Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Registration Success Rate_Base","Registration Success Rate", PerformanceCounterType.RawBase)
													 });
			PerformanceCounterCategory.Create(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME);
		}


		private void initPerfCounters()
		{
			_perfCountMember = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Member Items", false);
			_perfRequestsSecMember = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Member Requests/second", false);
			_perfHitRateMember = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Member Hit Rate", false);
			_perfHitRateMember_Base = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Member Hit Rate_Base", false);
			_perfSavesSecMember = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Member Saves/second", false);

			_perfRequestsSecLogon = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Logon Requests/second", false);
			_perfSuccessRateLogon = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Logon Success Rate", false);
			_perfSuccessRateLogon_Base = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Logon Success Rate_Base", false);

			_perfRequestsSecRegister = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Registration Requests/second", false);
			_perfSuccessRateRegister = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Registration Success Rate", false);
			_perfSuccessRateRegister_Base = new PerformanceCounter(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME, "Registration Success Rate_Base", false);

			resetPerfCounters();
		}


		private void resetPerfCounters()
		{
			_perfCountMember.RawValue = 0;
			_perfRequestsSecMember.RawValue = 0;
			_perfHitRateMember.RawValue = 0;
			_perfHitRateMember_Base.RawValue = 0;
			_perfSavesSecMember.RawValue = 0;

			_perfRequestsSecLogon.RawValue = 0;
			_perfSuccessRateLogon.RawValue = 0;
			_perfSuccessRateLogon_Base.RawValue = 0;

			_perfRequestsSecRegister.RawValue = 0;
			_perfSuccessRateRegister.RawValue = 0;
			_perfSuccessRateRegister_Base.RawValue = 0;
		}


		private void MemberBL_MemberRequested(bool cacheHit)
		{
			_perfRequestsSecMember.Increment();
			_perfHitRateMember_Base.Increment();

			if (cacheHit)
			{
				_perfHitRateMember.Increment();
			}
		}


		private void MemberBL_MemberAdded()
		{
			_perfCountMember.Increment();
		}


		private void MemberBL_MemberRemoved()
		{
			if (_perfCountMember.RawValue > 0)
			{
				_perfCountMember.Decrement();
			}
		}


		private void MemberBL_MemberSaved()
		{
			_perfSavesSecMember.Increment();
		}


		private void MemberBL_LogonRequested(bool success)
		{
			_perfRequestsSecLogon.Increment();
			_perfSuccessRateLogon_Base.Increment();

			if (success)
			{
				_perfSuccessRateLogon.Increment();
			}
		}


		private void MemberBL_RegistrationRequested(bool success)
		{
			_perfRequestsSecRegister.Increment();
			_perfSuccessRateRegister_Base.Increment();

			if (success)
			{
				_perfSuccessRateRegister.Increment();
			}
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			Matchnet.Caching.Cache.Instance.Clear();

			if (_hydraWriter != null)
			{
				_hydraWriter.Stop();
			}

			if (_replicator != null)
			{
				_replicator.Stop();
			}

			if (_synchronizer != null)
			{
				_synchronizer.Stop();
			}

			resetPerfCounters();
		}
		#endregion





       
    }
}
