using System;
using System.Diagnostics;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.AttributeMetadata;


namespace Matchnet.Member.ServiceManagers
{
	public class AttributeMetadataSM : MarshalByRefObject, IServiceManager, IDisposable, IAttributeMetadataService
	{
		public AttributeMetadataSM()
		{
		}

		
		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		public void PrePopulateCache()
		{
		}
		
		
		public Attributes GetAttributes()
		{
			try
			{
				return AttributeMetadataBL.Instance.GetAttributes();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred while retrieving attribute metadata.", ex);
			}
		}


		#region IDisposable Members
		public void Dispose()
		{
		}
		#endregion
	}
}
