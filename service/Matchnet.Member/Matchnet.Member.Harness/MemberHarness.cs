using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;


namespace Matchnet.Member.Harness
{
	/// <summary>
	/// Summary description for MemberHarness.
	/// </summary>
	public class MemberHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtMemberId;
		private System.Windows.Forms.Label lblMsgCount;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox txtCommunityId;
		private System.Windows.Forms.TextBox txtFromfolder;
		private System.Windows.Forms.TextBox txtToFolder;
		private System.Windows.Forms.Button button3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MemberHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.txtMemberId = new System.Windows.Forms.TextBox();
			this.lblMsgCount = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.txtCommunityId = new System.Windows.Forms.TextBox();
			this.txtFromfolder = new System.Windows.Forms.TextBox();
			this.txtToFolder = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(24, 16);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtMemberId
			// 
			this.txtMemberId.Location = new System.Drawing.Point(24, 48);
			this.txtMemberId.Name = "txtMemberId";
			this.txtMemberId.Size = new System.Drawing.Size(288, 20);
			this.txtMemberId.TabIndex = 1;
			this.txtMemberId.Text = "";
			// 
			// lblMsgCount
			// 
			this.lblMsgCount.Location = new System.Drawing.Point(24, 144);
			this.lblMsgCount.Name = "lblMsgCount";
			this.lblMsgCount.Size = new System.Drawing.Size(160, 23);
			this.lblMsgCount.TabIndex = 2;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(352, 48);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(112, 23);
			this.button2.TabIndex = 3;
			this.button2.Text = "Move Msgs";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// txtCommunityId
			// 
			this.txtCommunityId.Location = new System.Drawing.Point(24, 88);
			this.txtCommunityId.Name = "txtCommunityId";
			this.txtCommunityId.Size = new System.Drawing.Size(104, 20);
			this.txtCommunityId.TabIndex = 4;
			this.txtCommunityId.Text = "";
			// 
			// txtFromfolder
			// 
			this.txtFromfolder.Location = new System.Drawing.Point(24, 120);
			this.txtFromfolder.Name = "txtFromfolder";
			this.txtFromfolder.Size = new System.Drawing.Size(80, 20);
			this.txtFromfolder.TabIndex = 5;
			this.txtFromfolder.Text = "";
			// 
			// txtToFolder
			// 
			this.txtToFolder.Location = new System.Drawing.Point(200, 120);
			this.txtToFolder.Name = "txtToFolder";
			this.txtToFolder.Size = new System.Drawing.Size(80, 20);
			this.txtToFolder.TabIndex = 6;
			this.txtToFolder.Text = "";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(352, 88);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(112, 23);
			this.button3.TabIndex = 7;
			this.button3.Text = "Delete Msgs";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// MemberHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(480, 273);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.txtToFolder);
			this.Controls.Add(this.txtFromfolder);
			this.Controls.Add(this.txtCommunityId);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.lblMsgCount);
			this.Controls.Add(this.txtMemberId);
			this.Controls.Add(this.button1);
			this.Name = "MemberHarness";
			this.Text = "MemberHarness";
			this.Load += new System.EventHandler(this.MemberHarness_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{

			int memberId;
			int communityId;
			int folderId;
			ICollection foundMessages = null;
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(100000000, MemberLoadFlags.None);
			//Matchnet.Email.ValueObjects.EmailFolder inbox=Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.RetrieveFolder(member.MemberID, communityId, Matchnet.Email.ValueObjects.SystemFolders.Inbox,0);
//			Matchnet.Email.ServiceAdapters.EmailMessageSA emailSA=Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance;
//			
//			foundMessages= emailSA.RetrieveMessages(member.MemberID, communityId,Matchnet.Email.ValueObjects.SystemFolders.Inbox);
//			foundMessages.Count;
			Console.WriteLine(member.Username);

			
			//Uri uri = new Uri("http://photo2.matchnet.com/beta/2006/03/31/00/125042174.jpg?siteID=103");
			//Console.WriteLine(uri.Host);


			//Console.WriteLine(Matchnet.Member.BusinessLogic.MemberBL.translatePhotoPath("http://photo2.matchnet.com/beta/2006/03/31/00/125042174.jpg"));

		}

		private void MemberHarness_Load(object sender, System.EventArgs e)
		{
		
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			
//			int memberId=Int32.Parse( txtMemberId.Text);
//			int communityId=Int32.Parse( txtCommunityId.Text);
//		
//			ArrayList msgList=new ArrayList();
//		
//			
//			//Matchnet.Email.ValueObjects.EmailFolder inbox=Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.RetrieveFolder(member.MemberID, communityId, Matchnet.Email.ValueObjects.SystemFolders.Inbox,0);
//						
//			EmailMessageCollection coll= Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessages(memberId, communityId,1);
//			
//			foreach(EmailMessage m in coll)
//			{
//				msgList.Add(m.MemberMailID);
//			}
//			
//			
//			
//			EmailMessageSA.Instance.MoveToFolder(memberId,communityId, msgList,4);
			
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
//			int memberId=Int32.Parse( txtMemberId.Text);
//			int communityId=Int32.Parse( txtCommunityId.Text);
//		
//			ArrayList msgList=new ArrayList();
//		
//			
//			//Matchnet.Email.ValueObjects.EmailFolder inbox=Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.RetrieveFolder(member.MemberID, communityId, Matchnet.Email.ValueObjects.SystemFolders.Inbox,0);
//						
//			EmailMessageCollection coll= Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessages(memberId, communityId,1);
//			
//			foreach(EmailMessage m in coll)
//			{
//				msgList.Add(m.MemberMailID);
//			}
//			
//			
//			
//			EmailMessageSA.Instance.DeleteMessage(memberId,communityId, msgList);
			
		}
	}
}
