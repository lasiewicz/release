using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ServiceManagers;

namespace Matchnet.Member.Harness
{
	/// <summary>
	/// Summary description for Privileges.
	/// </summary>
	public class Privileges : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnGetPrivileges;
		private System.Windows.Forms.TextBox tbOutput;
		private System.Windows.Forms.Button btnGetGroups;
		private System.Windows.Forms.Button btnGetGroupMembers;
		private System.Windows.Forms.Label lblGroupID;
		private System.Windows.Forms.TextBox tbGroupID;
		private System.Windows.Forms.Button GetMemberPrivilegesButton;
		private System.Windows.Forms.TextBox MemberIDTextBox;
		private System.Windows.Forms.TextBox PrivilegeIDTextBox;
		private System.Windows.Forms.Label MemberIDLabel;
		private System.Windows.Forms.Label PrivilegeIDLabel;
		private System.Windows.Forms.Button SaveMemberPrivilegeButton;
		private System.Windows.Forms.Button DeleteMemberPrivilegeButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		[STAThread]
		static void Main() 
		{
			Application.Run(new Privileges());
		}

		public Privileges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnGetPrivileges = new System.Windows.Forms.Button();
			this.tbOutput = new System.Windows.Forms.TextBox();
			this.btnGetGroups = new System.Windows.Forms.Button();
			this.btnGetGroupMembers = new System.Windows.Forms.Button();
			this.lblGroupID = new System.Windows.Forms.Label();
			this.tbGroupID = new System.Windows.Forms.TextBox();
			this.GetMemberPrivilegesButton = new System.Windows.Forms.Button();
			this.MemberIDTextBox = new System.Windows.Forms.TextBox();
			this.SaveMemberPrivilegeButton = new System.Windows.Forms.Button();
			this.DeleteMemberPrivilegeButton = new System.Windows.Forms.Button();
			this.PrivilegeIDTextBox = new System.Windows.Forms.TextBox();
			this.MemberIDLabel = new System.Windows.Forms.Label();
			this.PrivilegeIDLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnGetPrivileges
			// 
			this.btnGetPrivileges.Location = new System.Drawing.Point(8, 120);
			this.btnGetPrivileges.Name = "btnGetPrivileges";
			this.btnGetPrivileges.Size = new System.Drawing.Size(88, 23);
			this.btnGetPrivileges.TabIndex = 0;
			this.btnGetPrivileges.Text = "Get Privileges";
			this.btnGetPrivileges.Click += new System.EventHandler(this.btnGetPrivileges_Click);
			// 
			// tbOutput
			// 
			this.tbOutput.Location = new System.Drawing.Point(8, 8);
			this.tbOutput.Multiline = true;
			this.tbOutput.Name = "tbOutput";
			this.tbOutput.ReadOnly = true;
			this.tbOutput.Size = new System.Drawing.Size(376, 104);
			this.tbOutput.TabIndex = 1;
			this.tbOutput.Text = "";
			// 
			// btnGetGroups
			// 
			this.btnGetGroups.Location = new System.Drawing.Point(8, 152);
			this.btnGetGroups.Name = "btnGetGroups";
			this.btnGetGroups.Size = new System.Drawing.Size(88, 23);
			this.btnGetGroups.TabIndex = 2;
			this.btnGetGroups.Text = "Get Groups";
			this.btnGetGroups.Click += new System.EventHandler(this.btnGetGroups_Click);
			// 
			// btnGetGroupMembers
			// 
			this.btnGetGroupMembers.Location = new System.Drawing.Point(8, 184);
			this.btnGetGroupMembers.Name = "btnGetGroupMembers";
			this.btnGetGroupMembers.Size = new System.Drawing.Size(120, 23);
			this.btnGetGroupMembers.TabIndex = 3;
			this.btnGetGroupMembers.Text = "Get Group Members";
			this.btnGetGroupMembers.Click += new System.EventHandler(this.btnGetGroupMembers_Click);
			// 
			// lblGroupID
			// 
			this.lblGroupID.Location = new System.Drawing.Point(144, 184);
			this.lblGroupID.Name = "lblGroupID";
			this.lblGroupID.Size = new System.Drawing.Size(56, 16);
			this.lblGroupID.TabIndex = 4;
			this.lblGroupID.Text = "Group ID:";
			// 
			// tbGroupID
			// 
			this.tbGroupID.Location = new System.Drawing.Point(216, 184);
			this.tbGroupID.Name = "tbGroupID";
			this.tbGroupID.TabIndex = 5;
			this.tbGroupID.Text = "";
			// 
			// GetMemberPrivilegesButton
			// 
			this.GetMemberPrivilegesButton.Location = new System.Drawing.Point(8, 240);
			this.GetMemberPrivilegesButton.Name = "GetMemberPrivilegesButton";
			this.GetMemberPrivilegesButton.Size = new System.Drawing.Size(232, 23);
			this.GetMemberPrivilegesButton.TabIndex = 6;
			this.GetMemberPrivilegesButton.Text = "Get MemberPrivileges";
			this.GetMemberPrivilegesButton.Click += new System.EventHandler(this.GetMemberPrivilegesButton_Click);
			// 
			// MemberIDTextBox
			// 
			this.MemberIDTextBox.Location = new System.Drawing.Point(248, 256);
			this.MemberIDTextBox.Name = "MemberIDTextBox";
			this.MemberIDTextBox.TabIndex = 7;
			this.MemberIDTextBox.Text = "";
			// 
			// SaveMemberPrivilegeButton
			// 
			this.SaveMemberPrivilegeButton.Location = new System.Drawing.Point(8, 272);
			this.SaveMemberPrivilegeButton.Name = "SaveMemberPrivilegeButton";
			this.SaveMemberPrivilegeButton.Size = new System.Drawing.Size(232, 23);
			this.SaveMemberPrivilegeButton.TabIndex = 8;
			this.SaveMemberPrivilegeButton.Text = "Save MemberPrivileges";
			this.SaveMemberPrivilegeButton.Click += new System.EventHandler(this.SaveMemberPrivilegeButton_Click);
			// 
			// DeleteMemberPrivilegeButton
			// 
			this.DeleteMemberPrivilegeButton.Location = new System.Drawing.Point(8, 304);
			this.DeleteMemberPrivilegeButton.Name = "DeleteMemberPrivilegeButton";
			this.DeleteMemberPrivilegeButton.Size = new System.Drawing.Size(232, 23);
			this.DeleteMemberPrivilegeButton.TabIndex = 9;
			this.DeleteMemberPrivilegeButton.Text = "Delete MemberPrivileges";
			this.DeleteMemberPrivilegeButton.Click += new System.EventHandler(this.DeleteMemberPrivilegeButton_Click);
			// 
			// PrivilegeIDTextBox
			// 
			this.PrivilegeIDTextBox.Location = new System.Drawing.Point(248, 304);
			this.PrivilegeIDTextBox.Name = "PrivilegeIDTextBox";
			this.PrivilegeIDTextBox.TabIndex = 10;
			this.PrivilegeIDTextBox.Text = "";
			// 
			// MemberIDLabel
			// 
			this.MemberIDLabel.Location = new System.Drawing.Point(248, 240);
			this.MemberIDLabel.Name = "MemberIDLabel";
			this.MemberIDLabel.Size = new System.Drawing.Size(88, 16);
			this.MemberIDLabel.TabIndex = 11;
			this.MemberIDLabel.Text = "MemberID";
			// 
			// PrivilegeIDLabel
			// 
			this.PrivilegeIDLabel.Location = new System.Drawing.Point(248, 288);
			this.PrivilegeIDLabel.Name = "PrivilegeIDLabel";
			this.PrivilegeIDLabel.Size = new System.Drawing.Size(88, 16);
			this.PrivilegeIDLabel.TabIndex = 12;
			this.PrivilegeIDLabel.Text = "PrivilegeID";
			// 
			// Privileges
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(392, 341);
			this.Controls.Add(this.PrivilegeIDLabel);
			this.Controls.Add(this.MemberIDLabel);
			this.Controls.Add(this.PrivilegeIDTextBox);
			this.Controls.Add(this.DeleteMemberPrivilegeButton);
			this.Controls.Add(this.SaveMemberPrivilegeButton);
			this.Controls.Add(this.MemberIDTextBox);
			this.Controls.Add(this.GetMemberPrivilegesButton);
			this.Controls.Add(this.tbGroupID);
			this.Controls.Add(this.tbOutput);
			this.Controls.Add(this.lblGroupID);
			this.Controls.Add(this.btnGetGroupMembers);
			this.Controls.Add(this.btnGetGroups);
			this.Controls.Add(this.btnGetPrivileges);
			this.Name = "Privileges";
			this.Text = "Privileges";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGetPrivileges_Click(object sender, System.EventArgs e)
		{
			/*
			PrivilegeCollection pc = PrivilegeSA.Instance.GetPrivileges();
			this.tbOutput.Text = "PrivilegeID: Description" + System.Environment.NewLine;

			foreach (Privilege p in pc)
			{
				this.tbOutput.Text += String.Format("{0}: {1}", p.PrivilegeID.ToString(), p.Description) + System.Environment.NewLine;
			}
			*/

			//MemberPrivilegeSA.Instance.GetMembersByPrivilege(2);
			MemberPrivilegeBL.Instance.GetMembersByPrivilege(2);
		}

		private void btnGetGroups_Click(object sender, System.EventArgs e)
		{
			/*
			GroupCollection gc = PrivilegeSA.Instance.GetGroups();
			this.tbOutput.Text = "GroupID: Description" + System.Environment.NewLine;

			foreach (Group g in gc)
			{
				this.tbOutput.Text += String.Format("{0}: {1}", g.GroupID.ToString(), g.Description) + System.Environment.NewLine;
			}	
			*/		
		}

		private void btnGetGroupMembers_Click(object sender, System.EventArgs e)
		{
			/*
			int groupID = Convert.ToInt32(this.tbGroupID.Text);
			GroupMemberCollection gmc = PrivilegeSA.Instance.GetGroupMembers(groupID);
			this.tbOutput.Text = "Group Members:" + System.Environment.NewLine;

			foreach (int memberID in gmc)
			{
				this.tbOutput.Text += memberID.ToString() + System.Environment.NewLine;
			}
			*/
		}

		private void GetMemberPrivilegesButton_Click(object sender, System.EventArgs e)
		{
			tbOutput.Text = String.Empty;

			MemberSM memberSM = new MemberSM();
			MemberPrivilegeCollection memberPrivilegeCollection = memberSM.GetMemberPrivileges(Convert.ToInt32(MemberIDTextBox.Text));

			foreach (MemberPrivilege memberPrivilege in memberPrivilegeCollection)
			{
				if (memberPrivilege.PrivilegeState == PrivilegeState.Individual || memberPrivilege.PrivilegeState == PrivilegeState.SecurityGroup)
				{
					tbOutput.Text += memberPrivilege.Privilege.PrivilegeID.ToString() + System.Environment.NewLine;
				}
			}
		}

		private void SaveMemberPrivilegeButton_Click(object sender, System.EventArgs e)
		{
			// MemberPrivilegeBL.Instance.SaveMemberPrivilege(Convert.ToInt32(MemberIDTextBox.Text), Convert.ToInt32(PrivilegeIDTextBox.Text));
			MemberPrivilegeSA.Instance.SaveMemberPrivilege(Convert.ToInt32(MemberIDTextBox.Text), Convert.ToInt32(PrivilegeIDTextBox.Text));
		}

		private void DeleteMemberPrivilegeButton_Click(object sender, System.EventArgs e)
		{
			// MemberPrivilegeBL.Instance.DeleteMemberPrivilege(Convert.ToInt32(MemberIDTextBox.Text), Convert.ToInt32(PrivilegeIDTextBox.Text));
			MemberPrivilegeSA.Instance.DeleteMemberPrivilege(Convert.ToInt32(MemberIDTextBox.Text), Convert.ToInt32(PrivilegeIDTextBox.Text));
		}
	}
}
