using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;

using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.Member.Harness
{
	/// <summary>
	/// Summary description for Photo.
	/// </summary>
	public class Photo : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button SavePhoto;
		private System.Windows.Forms.TextBox MemberID;
		private System.Windows.Forms.TextBox CommunityID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button SaveAttribute;
		private System.Windows.Forms.TextBox AttributeName;
		private System.Windows.Forms.TextBox AttributeValue;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Photo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SavePhoto = new System.Windows.Forms.Button();
			this.MemberID = new System.Windows.Forms.TextBox();
			this.CommunityID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SaveAttribute = new System.Windows.Forms.Button();
			this.AttributeName = new System.Windows.Forms.TextBox();
			this.AttributeValue = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// SavePhoto
			// 
			this.SavePhoto.Location = new System.Drawing.Point(120, 88);
			this.SavePhoto.Name = "SavePhoto";
			this.SavePhoto.TabIndex = 0;
			this.SavePhoto.Text = "Save Photo";
			this.SavePhoto.Click += new System.EventHandler(this.SavePhoto_Click);
			// 
			// MemberID
			// 
			this.MemberID.Location = new System.Drawing.Point(120, 16);
			this.MemberID.Name = "MemberID";
			this.MemberID.TabIndex = 1;
			this.MemberID.Text = "16000206";
			// 
			// CommunityID
			// 
			this.CommunityID.Location = new System.Drawing.Point(120, 48);
			this.CommunityID.Name = "CommunityID";
			this.CommunityID.Size = new System.Drawing.Size(56, 20);
			this.CommunityID.TabIndex = 2;
			this.CommunityID.Text = "3";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 3;
			this.label1.Text = "Member ID:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 48);
			this.label2.Name = "label2";
			this.label2.TabIndex = 4;
			this.label2.Text = "Community ID:";
			// 
			// SaveAttribute
			// 
			this.SaveAttribute.Location = new System.Drawing.Point(304, 296);
			this.SaveAttribute.Name = "SaveAttribute";
			this.SaveAttribute.Size = new System.Drawing.Size(128, 23);
			this.SaveAttribute.TabIndex = 5;
			this.SaveAttribute.Text = "Save Attribute";
			this.SaveAttribute.Click += new System.EventHandler(this.SaveAttribute_Click);
			// 
			// AttributeName
			// 
			this.AttributeName.Location = new System.Drawing.Point(320, 216);
			this.AttributeName.Name = "AttributeName";
			this.AttributeName.TabIndex = 6;
			this.AttributeName.Text = "SelfSuspendedFlag";
			// 
			// AttributeValue
			// 
			this.AttributeValue.Location = new System.Drawing.Point(320, 256);
			this.AttributeValue.Name = "AttributeValue";
			this.AttributeValue.TabIndex = 7;
			this.AttributeValue.Text = "1";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(200, 216);
			this.label3.Name = "label3";
			this.label3.TabIndex = 8;
			this.label3.Text = "AttributeName";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(200, 256);
			this.label4.Name = "label4";
			this.label4.TabIndex = 9;
			this.label4.Text = "AttributeValue";
			// 
			// Photo
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(464, 347);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.AttributeValue);
			this.Controls.Add(this.AttributeName);
			this.Controls.Add(this.SaveAttribute);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.CommunityID);
			this.Controls.Add(this.MemberID);
			this.Controls.Add(this.SavePhoto);
			this.Name = "Photo";
			this.Text = "Photo";
			this.ResumeLayout(false);

		}
		#endregion

		private byte[] ConvertImageToByteArray(System.Drawing.Image image) 
		{ 
			byte[] ret; 
  
			MemoryStream stream = new MemoryStream();   
			image.Save(stream, ImageFormat.Jpeg); 
			ret = stream.ToArray(); 
			stream.Close(); 

			return ret; 
		} 

		private void SavePhoto_Click(object sender, System.EventArgs e)
		{
			// Get photo file
			string filePath = String.Empty;
			OpenFileDialog openFileDialog = new OpenFileDialog();

			openFileDialog.InitialDirectory = "c:\\Documents and Settings" ;
			openFileDialog.Filter = "photo files (*.jpg)|*.jpg|All files (*.*)|*.*" ;
			openFileDialog.FilterIndex = 2 ;

			if(openFileDialog.ShowDialog() != DialogResult.OK)
				return;

			// Get the photo file path
			filePath = openFileDialog.FileName;
	
			// Get the photo file 
			Image image = Image.FromFile(filePath);
			
			// Populate photo update object
			Matchnet.Member.ValueObjects.Photos.PhotoUpdate[] photoUpdates;
			photoUpdates = new Matchnet.Member.ValueObjects.Photos.PhotoUpdate[1];

			Matchnet.Member.ValueObjects.Photos.PhotoUpdate photoUpdate = new 
				Matchnet.Member.ValueObjects.Photos.PhotoUpdate(ConvertImageToByteArray(image),
				false,
				Constants.NULL_INT,
				Constants.NULL_INT,
				Constants.NULL_STRING,
				Constants.NULL_INT,
				Constants.NULL_STRING,
				1,
				false,
				false,
				Constants.NULL_INT,
				16000206);

			photoUpdates[0] = photoUpdate;

			MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(Instance_MemberAdded);
			MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(Instance_MemberRequested);
			MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(Instance_MemberSaved);
			MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(Instance_SynchronizationRequested);
			MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(Instance_ReplicationRequested);
			
			// Save photo as new photo
			MemberBL.Instance.SavePhotos("DEVAPP01",
				Convert.ToInt32(CommunityID.Text),
				Convert.ToInt32(MemberID.Text),
				photoUpdates);			
		}

		private void Instance_MemberAdded()
		{

		}

		private void Instance_MemberRequested(bool cacheHit)
		{

		}

		private void Instance_MemberSaved()
		{

		}

		private void Instance_SynchronizationRequested(string key, Hashtable cacheReferences)
		{

		}

		private void Instance_ReplicationRequested(IReplicable replicableObject)
		{

		}

		private void SaveAttribute_Click(object sender, System.EventArgs e)
		{
			MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(Instance_MemberAdded);
			MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(Instance_MemberRequested);
			MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(Instance_MemberSaved);
			MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(Instance_SynchronizationRequested);
			MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(Instance_ReplicationRequested);
			
			Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(
				47481631, Matchnet.Member.ServiceAdapters.MemberLoadFlags.IngoreSACache);
			MemberUpdate memberUpdate = new MemberUpdate(47481631);
			memberUpdate.AttributesInt.Add(445, 5);
			MemberBL.Instance.SaveMember("svcmember02", memberUpdate);
		}
	}
}
