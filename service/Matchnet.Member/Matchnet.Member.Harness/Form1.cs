using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;

using System.Data;
using System.Data.SqlClient;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ServiceManagers;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Security;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;


namespace Matchnet.Member.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.Button btnLogon;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label lblAuthenticationStatus;
		private System.ComponentModel.Container components = null;

		private CachedMember[] _mbrs;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.btnLogon = new System.Windows.Forms.Button();
			this.lblAuthenticationStatus = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(8, 40);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(296, 224);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblAuthenticationStatus);
			this.groupBox1.Controls.Add(this.btnLogon);
			this.groupBox1.Controls.Add(this.txtPassword);
			this.groupBox1.Controls.Add(this.lblPassword);
			this.groupBox1.Controls.Add(this.txtUsername);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(312, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(376, 256);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Authentication / Login";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Username:";
			// 
			// txtUsername
			// 
			this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtUsername.Location = new System.Drawing.Point(96, 24);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(264, 20);
			this.txtUsername.TabIndex = 1;
			this.txtUsername.Text = "";
			// 
			// lblPassword
			// 
			this.lblPassword.Location = new System.Drawing.Point(16, 56);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(72, 23);
			this.lblPassword.TabIndex = 2;
			this.lblPassword.Text = "Password:";
			// 
			// txtPassword
			// 
			this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtPassword.Location = new System.Drawing.Point(96, 56);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(144, 20);
			this.txtPassword.TabIndex = 3;
			this.txtPassword.Text = "";
			// 
			// btnLogon
			// 
			this.btnLogon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLogon.Location = new System.Drawing.Point(280, 56);
			this.btnLogon.Name = "btnLogon";
			this.btnLogon.TabIndex = 4;
			this.btnLogon.Text = "Logon";
			this.btnLogon.Click += new System.EventHandler(this.btnLogon_Click);
			// 
			// lblAuthenticationStatus
			// 
			this.lblAuthenticationStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblAuthenticationStatus.Location = new System.Drawing.Point(16, 88);
			this.lblAuthenticationStatus.Name = "lblAuthenticationStatus";
			this.lblAuthenticationStatus.Size = new System.Drawing.Size(344, 23);
			this.lblAuthenticationStatus.TabIndex = 5;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(696, 273);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new MemberHarness());

			Application.Run(new Form1());

			// Saves photo file, does not save to db yet
			Application.Run(new Photo()); 

			Application.Run(new AdminMemberSearch());

			Application.Run(new Privileges());
		}

		string[] mbrs;

		private void button1_Click(object sender, System.EventArgs e)
		{
			
			/*


			member.Username = "54833702";
			member.SetAttributeDate(brand, "BrandInsertDate", DateTime.Now);	//this attribute is immutable so it is safe to attempt updates all day
			member.SetAttributeDate(brand, "BrandLastLogonDate", DateTime.Now);
			member.SetAttributeInt(brand, "LastBrandID", brand.BrandID);
			member.SetAttributeInt(brand, "BrandLogonCount", 1);

			MemberSA.Instance.SaveMember(member);
*/

			/*
			Console.WriteLine(MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);
			Console.WriteLine(MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).GetAttributeInt(3,103,1003,"SuspendMask", Constants.NULL_INT));
			Console.WriteLine(MemberSA.Instance.GetPassword(memberID));

			MemberSA.Instance.Authenticate(BrandConfigSA.Instance.GetBrands().GetBrand(1003),
				MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress,
				MemberSA.Instance.GetPassword(memberID));


			StreamReader sr = new StreamReader(@"c:\members.txt");
			mbrs = sr.ReadToEnd().Replace("\r","").Split('\n');

			for (Int32 i = 0; i < 6; i++)
			{
				Thread t = new Thread(new ThreadStart(stressCycle));
				t.Start();
			}
			*/

			/*
			foreach (string mbr in mbrs)
			{
				if (mbr.Trim().Length > 0)
				{
					Int32 memberID = Convert.ToInt32(mbr.Trim());
					//Console.WriteLine(memberID);

					Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

					//Console.WriteLine(member.Username);
					//Console.WriteLine(member.EmailAddress);
					//Console.WriteLine(MemberSA.Instance.GetPassword(memberID));

					SqlConnection conn = new SqlConnection("SERVER=CLSQL00;DATABASE=mnLogon;UID=webuser;PWD=dbaccess");

					try
					{
						conn.Open();

						SqlCommand cmd = new SqlCommand();
						cmd.Connection = conn;
						cmd.CommandText = "dbo.up_LogonMember_Save";
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.Parameters.Add("@MemberID", memberID);
						cmd.Parameters.Add("@EmailAddress", member.EmailAddress);
						cmd.Parameters.Add("@UserName", member.Username);
						cmd.Parameters.Add("@Password", MemberSA.Instance.GetPassword(memberID));

						cmd.ExecuteNonQuery();
						conn.Close();
					}
					catch (Exception ex)
					{
						Console.WriteLine(memberID);
						Console.WriteLine(ex.ToString());
					}
					finally
					{
						conn.Close();
					}
				}
			}
			*/

			/*

			Command command = new Command("mnLogonOG", "dbo.up_LogonMember_Save", 0);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);
			command.AddParameter("@UserName", SqlDbType.NVarChar, ParameterDirection.Input, username);
			command.AddParameter("@Password", SqlDbType.NVarChar, ParameterDirection.Input, passwordPlain);
			Client.Instance.ExecuteAsyncWrite(command);
			*/


			/*
			textBox1.Text = "";

			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(8578217 , MemberLoadFlags.None);
			member.SetAttributeDate(2, 102, Constants.NULL_INT, "SubscriptionExpirationDate", DateTime.Now.AddDays(-2));
			MemberSA.Instance.SaveMember(member);
			*/

			/*
			StreamReader sr = new StreamReader(@"c:\renew.csv");
			string[] recs = sr.ReadToEnd().Replace("\r","").Split('\n');

			foreach (string rec in recs)
			{
				string[] recParts = rec.Split(',');
				Int32 memberID = Convert.ToInt32(recParts[0]);
				Int32 siteID = Convert.ToInt32(recParts[1]);

				Console.WriteLine(memberID.ToString() + ", " + siteID.ToString());

				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				member.SetAttributeDate(Constants.NULL_INT,
					siteID,
					Constants.NULL_INT,
					"SubscriptionExpirationDate",
					DateTime.Now.AddDays(1));

				MemberSA.Instance.SaveMember(member);
			}
			*/

			


			/*
			IMemberService memberSvc = (IMemberService)Activator.GetObject(typeof(IMemberService), "tcp://localhost:42000/MemberSM.rem");
			CachedMember mbr = memberSvc.GetCachedMember(null, null, 23248462, false);

			BinaryFormatter formatter = new BinaryFormatter();
			FileStream fs = new FileStream(@"c:\temp\CachedMember.bin", FileMode.Create);
			formatter.Serialize(fs, mbr);
			fs.Close();

			Console.WriteLine(mbr.Photos.GetCommunity(1)[0].FileWebPath);

			FileStream fs2 = new FileStream(@"c:\temp\CachedMember.bin", FileMode.Open);
			mbr = formatter.Deserialize(fs2) as CachedMember;
			fs2.Close();

			Console.WriteLine(mbr.MemberID);
			*/

			/*
			ICacheManagerService cacheSM;
			cacheSM = (ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), "tcp://172.16.1.101:47000/CacheManagerSM.rem");
			cacheSM.Remove("Brands");
			cacheSM = (ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService),  "tcp://172.16.1.102:47000/CacheManagerSM.rem");
			cacheSM.Remove("Brands");

			cacheSM = (ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), "tcp://172.16.1.11:42000/CacheManagerSM.rem");
			cacheSM.Remove("Brands");
			cacheSM = (ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), "tcp://172.16.1.12:42000/CacheManagerSM.rem");
			cacheSM.Remove("Brands");

			Console.WriteLine("done");
			*/

			/*
			IMemberService memberSvc = (IMemberService)Activator.GetObject(typeof(IMemberService), "tcp://172.16.1.1:42000/MemberSM.rem");
			CachedMember mbr = memberSvc.GetCachedMember(null, null, 100023386 , false);

			TextStatusType textStatus;
			Console.WriteLine(mbr.GetAttributeText(92, 2, "", out textStatus));
			Console.WriteLine(textStatus.ToString());
			*/

			//Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(51583908, MemberLoadFlags.None);
			/*


			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(23248462 , MemberLoadFlags.None);

			Console.WriteLine(member.GetAttributeInt(Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, "RegionID", Constants.NULL_INT));
			string val = Guid.NewGuid().ToString();
			member.SetAttributeText(12, Constants.NULL_INT, Constants.NULL_INT, 2, "AboutMe", val, TextStatusType.None);
			MemberSA.Instance.SaveMember(member);
			Console.WriteLine("done - " + val);
			*/

			//textBox1.Text = textBox1.Text = member.IsCommunityMember(17).ToString();

			//textBox1.Text = textBox1.Text = member.GetLastLogonDate(17).ToString();



			/*
			textBox1.Text = textBox1.Text + "GenderMask:" + member.GetAttributeInt(1,0,0,"GenderMask", 0).ToString() + "\r\n";
			textBox1.Text = textBox1.Text + "Headline:" + member.GetAttributeText(1,0,0,2,"Headline") + "\r\n";
			textBox1.Text = textBox1.Text + "BirthDate:" + member.GetAttributeDate(1,0,0,"BirthDate", DateTime.Now).ToString() + "\r\n";

			member.SetAttributeInt(1,0,0,"GenderMask", 6);
			member.SetAttributeText(1,0,0,2,"Headline",Guid.NewGuid().ToString(), TextStatusType.None);
			member.SetAttributeDate(1,0,0,"BirthDate", new DateTime(1976, 12, 26));
			MemberSA.Instance.SaveMember(member);
			
			textBox1.Text = textBox1.Text + "GenderMask:" + member.GetAttributeInt(1,0,0,"GenderMask", 0).ToString() + "\r\n";
			textBox1.Text = textBox1.Text + "Headline:" + member.GetAttributeText(1,0,0,2,"Headline") + "\r\n";
			textBox1.Text = textBox1.Text + "BirthDate:" + member.GetAttributeDate(1,0,0,"BirthDate", DateTime.Now).ToString() + "\r\n";
			*/

			//textBox1.Text = member.GetLastLogonDate(1).ToString();

			/*
			textBox1.Text = textBox1.Text + member.IsCommunityMember(1).ToString() + "\r\n";
			textBox1.Text = textBox1.Text + member.IsCommunityMember(12).ToString() + "\r\n";
			*/

			/*
			Int32 memberID;
			Brand brand = BrandConfigSA.Instance.GetBrandByID(1017);
			AuthenticationResult ar = MemberSA.Instance.Authenticate(brand, "gpeterson@matchnet.com", "password");
			textBox1.Text = ar.Status.ToString();
			*/

			//textBox1.Text = Crypto.Encrypt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("KEY"), "password");


			/*
			try
			{
				MemberSM sm = new MemberSM();
				sm.Dispose();
				MemberRegisterResult result = sm.Register("e@sdfgasdfsdf.com",
					"gmoney",
					"password");

				textBox1.Text = textBox1.Text + result.RegisterStatus.ToString() + "\r\n";
				textBox1.Text = textBox1.Text + result.MemberID.ToString() + "\r\n";
				if (result.Username != Constants.NULL_STRING)
				{
					textBox1.Text = textBox1.Text + result.Username.ToString() + "\r\n";
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			*/
			
			/* updating text attribute text via BL - won
			MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(Instance_MemberAdded);
			MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(Instance_MemberRequested);
			MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(Instance_MemberSaved);
			MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(Instance_SynchronizationRequested);
			MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(Instance_ReplicationRequested);
			
			// Save photo as new photo
			MemberUpdate memberUpdate = new MemberUpdate(16000206);
			Matchnet.Member.ValueObjects.
			TextValue textValue = new TextValue("sucka1234", TextStatusType.None);
			memberUpdate.AttributesText.Add(438, textValue);
			memberUpdate.Username = "wontonnewusername";
			MemberBL.Instance.SaveMember(System.Environment.MachineName, memberUpdate);
			*/

			Console.WriteLine("done");
		}
		private void Instance_MemberAdded()
		{

		}

		private void Instance_MemberRequested(bool cacheHit)
		{

		}

		private void Instance_MemberSaved()
		{

		}

		private void Instance_SynchronizationRequested(string key, Hashtable cacheReferences)
		{

		}

		private void Instance_ReplicationRequested(IReplicable replicableObject)
		{

		}
		private Int32 getMemberID()
		{
			string s = mbrs[new System.Random().Next(0, mbrs.Length - 2)];
			return Convert.ToInt32(s);
		}

		private void stressCycle()
		{
			bool b = false;

			while (true)
			{
				IMemberService svc;

				if (b)
				{
					svc = (IMemberService)Activator.GetObject(typeof(IMemberService), "tcp://172.16.1.131:42000/MemberSM.rem");
				}
				else
				{
					svc = (IMemberService)Activator.GetObject(typeof(IMemberService), "tcp://172.16.1.132:42000/MemberSM.rem");
				}

				svc.GetCachedMemberBytes(null, null, getMemberID(), false);

				if (b == true)
				{
					b = false;
				}
				else
				{
					b = true;
				}

				Thread.Sleep(5);
			}
		}

		private void btnLogon_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnLogon.Enabled = false;
				Brand brand = BrandConfigSA.Instance.GetBrandByID(1017);
				Int32 memberID = new Int32();
				
				AuthenticationResult ar = MemberSA.Instance.Authenticate(brand, txtUsername.Text, txtPassword.Text);
			
				lblAuthenticationStatus.Text = ar.Status.ToString();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{
				btnLogon.Enabled = true;
			}
		}

	}
}
