using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Data;

using Matchnet.Member.BusinessLogic;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Member.Harness
{
	/// <summary>
	/// Summary description for AdminMemberSearch.
	/// </summary>
	public class AdminMemberSearch : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox Email;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox MemberID;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox UserName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox PhoneNumber;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox CreditCard;
		private System.Windows.Forms.Button Search;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ListView MemberList;
		private System.Windows.Forms.ListView SiteList;
		private System.Windows.Forms.ColumnHeader MemberList_MemberID;
		private System.Windows.Forms.ColumnHeader SiteList_SiteID;
		private System.Windows.Forms.ColumnHeader SiteList_MemberID;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.Label TotalFound;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		public AdminMemberSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Email = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.MemberID = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.UserName = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.PhoneNumber = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.CreditCard = new System.Windows.Forms.TextBox();
			this.Search = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.MemberList = new System.Windows.Forms.ListView();
			this.SiteList = new System.Windows.Forms.ListView();
			this.MemberList_MemberID = new System.Windows.Forms.ColumnHeader();
			this.SiteList_SiteID = new System.Windows.Forms.ColumnHeader();
			this.SiteList_MemberID = new System.Windows.Forms.ColumnHeader();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.TotalFound = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Email
			// 
			this.Email.Location = new System.Drawing.Point(120, 8);
			this.Email.Name = "Email";
			this.Email.TabIndex = 0;
			this.Email.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "Email";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			this.label2.Text = "Member ID";
			// 
			// MemberID
			// 
			this.MemberID.Location = new System.Drawing.Point(120, 40);
			this.MemberID.Name = "MemberID";
			this.MemberID.TabIndex = 2;
			this.MemberID.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 72);
			this.label3.Name = "label3";
			this.label3.TabIndex = 5;
			this.label3.Text = "User Name";
			// 
			// UserName
			// 
			this.UserName.Location = new System.Drawing.Point(120, 72);
			this.UserName.Name = "UserName";
			this.UserName.TabIndex = 4;
			this.UserName.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 104);
			this.label4.Name = "label4";
			this.label4.TabIndex = 7;
			this.label4.Text = "Phone Number";
			// 
			// PhoneNumber
			// 
			this.PhoneNumber.Location = new System.Drawing.Point(120, 104);
			this.PhoneNumber.Name = "PhoneNumber";
			this.PhoneNumber.TabIndex = 6;
			this.PhoneNumber.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 136);
			this.label5.Name = "label5";
			this.label5.TabIndex = 9;
			this.label5.Text = "Credit Card #";
			// 
			// CreditCard
			// 
			this.CreditCard.Location = new System.Drawing.Point(120, 136);
			this.CreditCard.Name = "CreditCard";
			this.CreditCard.TabIndex = 8;
			this.CreditCard.Text = "";
			// 
			// Search
			// 
			this.Search.Location = new System.Drawing.Point(40, 240);
			this.Search.Name = "Search";
			this.Search.Size = new System.Drawing.Size(160, 48);
			this.Search.TabIndex = 11;
			this.Search.Text = "Search";
			this.Search.Click += new System.EventHandler(this.Search_Click);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(272, 8);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(192, 23);
			this.label6.TabIndex = 12;
			this.label6.Text = "Found Members(Select to get sites):";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(272, 200);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(128, 23);
			this.label7.TabIndex = 14;
			this.label7.Text = "Sites(Private Labels):";
			// 
			// MemberList
			// 
			this.MemberList.BackColor = System.Drawing.Color.WhiteSmoke;
			this.MemberList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.MemberList_MemberID,
																						 this.columnHeader1,
																						 this.columnHeader2});
			this.MemberList.FullRowSelect = true;
			this.MemberList.GridLines = true;
			this.MemberList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.MemberList.Location = new System.Drawing.Point(272, 32);
			this.MemberList.MultiSelect = false;
			this.MemberList.Name = "MemberList";
			this.MemberList.Size = new System.Drawing.Size(592, 160);
			this.MemberList.TabIndex = 15;
			this.MemberList.View = System.Windows.Forms.View.Details;
			this.MemberList.SelectedIndexChanged += new System.EventHandler(this.MemberList_SelectedIndexChanged);
			// 
			// SiteList
			// 
			this.SiteList.BackColor = System.Drawing.Color.WhiteSmoke;
			this.SiteList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					   this.SiteList_SiteID,
																					   this.SiteList_MemberID,
																					   this.columnHeader3});
			this.SiteList.FullRowSelect = true;
			this.SiteList.GridLines = true;
			this.SiteList.Location = new System.Drawing.Point(272, 224);
			this.SiteList.Name = "SiteList";
			this.SiteList.Size = new System.Drawing.Size(592, 160);
			this.SiteList.TabIndex = 16;
			this.SiteList.View = System.Windows.Forms.View.Details;
			this.SiteList.SelectedIndexChanged += new System.EventHandler(this.SiteList_SelectedIndexChanged);
			// 
			// MemberList_MemberID
			// 
			this.MemberList_MemberID.Text = "MemberID";
			this.MemberList_MemberID.Width = 103;
			// 
			// SiteList_SiteID
			// 
			this.SiteList_SiteID.Text = "SiteID";
			this.SiteList_SiteID.Width = 113;
			// 
			// SiteList_MemberID
			// 
			this.SiteList_MemberID.Text = "MemberID";
			this.SiteList_MemberID.Width = 113;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Email Address";
			this.columnHeader1.Width = 150;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "User Name";
			this.columnHeader2.Width = 109;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "URI";
			this.columnHeader3.Width = 186;
			// 
			// TotalFound
			// 
			this.TotalFound.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(192)));
			this.TotalFound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.TotalFound.Location = new System.Drawing.Point(80, 328);
			this.TotalFound.Name = "TotalFound";
			this.TotalFound.TabIndex = 17;
			// 
			// AdminMemberSearch
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(888, 406);
			this.Controls.Add(this.TotalFound);
			this.Controls.Add(this.SiteList);
			this.Controls.Add(this.MemberList);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.Search);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.CreditCard);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.PhoneNumber);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.UserName);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.MemberID);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Email);
			this.Name = "AdminMemberSearch";
			this.ResumeLayout(false);

		}
		#endregion

		private void Instance_MemberAdded()
		{

		}

		private void Instance_MemberRequested(bool cacheHit)
		{

		}

		private void Instance_MemberSaved()
		{

		}

		private void Instance_SynchronizationRequested(string key, Hashtable cacheReferences)
		{

		}

		private void Instance_ReplicationRequested(IReplicable replicableObject)
		{

		}

		private void Search_Click(object sender, System.EventArgs e)
		{
			try
			{	
				Search.Text = "Hold on mang";
				Search.Enabled = false;

				MemberBL.Instance.MemberAdded += new Matchnet.Member.BusinessLogic.MemberBL.MemberAddEventHandler(Instance_MemberAdded);
				MemberBL.Instance.MemberRequested += new Matchnet.Member.BusinessLogic.MemberBL.MemberRequestEventHandler(Instance_MemberRequested);
				MemberBL.Instance.MemberSaved += new Matchnet.Member.BusinessLogic.MemberBL.MemberSaveEventHandler(Instance_MemberSaved);
				MemberBL.Instance.SynchronizationRequested += new Matchnet.Member.BusinessLogic.MemberBL.SynchronizationEventHandler(Instance_SynchronizationRequested);
				MemberBL.Instance.ReplicationRequested += new Matchnet.Member.BusinessLogic.MemberBL.ReplicationEventHandler(Instance_ReplicationRequested);

				string textVal;
				string method = whichSearch(out textVal);
				int totalRows = 0;

				object[] args =  {textVal, 0, 10, totalRows};

				ArrayList result = execeuteDynamicMethod(TierType.BusinessLogic, method, args) as ArrayList;
			
				for (Int32 num = 0; num < result.Count; num++)
				{
					CachedMember member = result[num] as CachedMember;
					ListViewItem item = new ListViewItem();
					item.Text = member.MemberID.ToString();
					item.SubItems.Add(member.EmailAddress);
					item.SubItems.Add(member.Username);
					MemberList.Items.Add(item);
				}

				TotalFound.Text = "Total Found: " + totalRows;
				MemberList.Refresh();
				clearTexts();
			}
			catch(Exception ex)
			{
				MessageBox.Show("ERROR:" + ex.ToString());
			}
			finally
			{
				Search.Text = "Search";
				Search.Enabled = true;
			}
		}

		private void clearTexts()
		{
			Email.Text = String.Empty;
			UserName.Text = String.Empty;
			MemberID.Text = String.Empty;
			PhoneNumber.Text = String.Empty;
			CreditCard.Text = String.Empty;
		}

		private object execeuteDynamicMethod(string tier, string method, object[] args)
		{
			Type tierType = getTierType(tier);

			FieldInfo fieldInfo = tierType.GetField("Instance");

			object target = fieldInfo.GetValue(this);


			object result = tierType.InvokeMember(method, 
				BindingFlags.InvokeMethod, 
				null,
				target, 
				args);


			return result;
		}

		private Type getTierType(string tierClass)
		{
			Type tierType = null;
			string tierAssembly = tierClass.Substring(0, tierClass.LastIndexOf("."));
			
			Assembly assembly = 
				Assembly.Load(Assembly.CreateQualifiedName(tierAssembly, tierAssembly));

			Type[] types = assembly.GetTypes();
			for (Int32 num = 0; num < types.Length; num++)
			{
				Type type = types[num];

				if (type.ToString().Equals(tierClass))
				{
					tierType = type;			
				}
			}

			return tierType;
		}

		private string whichSearch(out string textVal)
		{
			if (!Email.Text.Equals(String.Empty))
			{
				textVal = Email.Text;
				return SearchType.Email;
			}
			if (!MemberID.Text.Equals(String.Empty))
			{
				textVal = MemberID.Text;
				return SearchType.MemberID;
			}
			if (!UserName.Text.Equals(String.Empty))
			{
				textVal = UserName.Text;
				return SearchType.UserName;
			}
			if (!PhoneNumber.Text.Equals(String.Empty))
			{
				textVal = PhoneNumber.Text;
				return SearchType.PhoneNumber;
			}
			if (!CreditCard.Text.Equals(String.Empty))
			{
				textVal = CreditCard.Text;
				return (SearchType.CreditCard);
			}

			throw new Exception("No search type");
		}
		
		private void MemberList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				ListViewItem item = MemberList.SelectedItems[0];
			
				Int32 memberID = Conversion.CInt(item.Text);
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                
				SiteList.Items.Clear();

				Int32[] sites = member.GetSiteIDList();
				for (Int32 num = 0; num < sites.Length; num++)
				{
					Int32 siteID = sites[num];
					ListViewItem addItem = new ListViewItem();
					addItem.Text = siteID.ToString();
					addItem.SubItems.Add(memberID.ToString());
					addItem.SubItems.Add(siteID.ToString());
					SiteList.Items.Add(addItem);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error while populating sites for member. Inner Exception: " + ex.ToString());
			}
		}

		private void SiteList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
		}

	}

	public class SearchType
	{
		public const string Email = "GetMembersByEmail";
		public const string MemberID = "GetMembersByMemberID";
		public const string UserName = "GetMembersByUserName";
		public const string PhoneNumber = "GetMembersByPhoneNumber";
		public const string CreditCard = "GetMembersByCreditCard";
	}

	public class TierType
	{
		public const string BusinessLogic = "Matchnet.Member.BusinessLogic.MemberBL";
		public const string ServiceAdapter = "MemberSA";
	}
}
