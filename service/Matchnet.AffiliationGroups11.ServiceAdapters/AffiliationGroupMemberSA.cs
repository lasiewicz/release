using System;
using System.Text;
using System.Data;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects;

namespace Matchnet.AffiliationGroups11.ServiceAdapters
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
    public class AffiliationGroupMemberSA:SABase
	{
		public static readonly AffiliationGroupMemberSA Instance = new AffiliationGroupMemberSA();
		private string testURI="";
		private const string TRACE_FORMAT="BrandID={0}\r\nMemberID={1}\r\nExpDate={2}\r\nEnableFlag={3}\r\nURI={4}";
		private AffiliationGroupMemberSA()
		{
			
		}
		
		public  void SaveAffiliationMember(int brandid, int memberid)
		{
			try
			{
				string uri = getServiceManagerUri();
				IAffiliationGroupMember m1=null;
				base.Checkout(uri);
				try
				{
						
					m1= getService(uri);
					m1.SaveAffiliationMember(brandid,memberid);

				}
				finally
				{
					base.Checkin(uri);
					m1=null;
				}


			}
			catch(Exception ex)
			{throw new SAException(ex);}
		}
	
		#region SABase implementation
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_SA_CONNECTION_LIMIT"));
		}
		#endregion SABase implementation


		#region service proxy

		private IAffiliationGroupMember getService(string uri)
		{
         
			try
			{
				return (IAffiliationGroupMember)Activator.GetObject(typeof(IAffiliationGroupMember), uri);
			}
			catch (Exception ex)
			{
				throw (new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri()
		{
			string uri = "";
			try
			{
				if (testURI != null && testURI != "")
					uri = testURI;
				else
				{
					uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_METADATA_MANAGER_NAME);
				}
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
			}
		}


		public String TestURI
		{
			get { return testURI; }
			set { testURI = value; }
		}
		#endregion
		
	}
}
