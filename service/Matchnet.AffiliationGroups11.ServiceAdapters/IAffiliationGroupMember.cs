using System;

namespace Matchnet.AffiliationGroups11.ServiceAdapters
{
	/// <summary>
	/// Summary description for IAffiliationGroupMember.
	/// </summary>
	public interface IAffiliationGroupMember
	{
		void SaveAffiliationMember(int brandid, int memberid);
	}
}
