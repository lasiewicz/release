﻿#region

using System;
using System.Collections;
using System.Threading;
using Matchnet.Configuration.ValueObjects;
using NUnit.Framework;
using Spark.Caching;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

#endregion

namespace Matchnet.Search.Tests
{
    [Serializable]
    public class CacheableArrayList : ArrayList, ICacheable
    {
        public CacheableArrayList()
        {
            CacheTTLSeconds = 60;
            CacheMode = CacheItemMode.Absolute;
            CachePriority = CacheItemPriorityLevel.Default;
        }

        public string GetCacheKey()
        {
            return "DailyMatches-3-27029711";
        }

        public int CacheTTLSeconds { get; set; }
        public CacheItemMode CacheMode { get; private set; }
        public CacheItemPriorityLevel CachePriority { get; set; }
    }

    /// <summary>
    ///     Spark.Caching has its own unit tests but due to many library references, adding another test to ensure it works
    ///     within Matchnet.Search
    /// </summary>
    [TestFixture]
    public class SparkCachingTest
    {
        private ICaching _cacheInstance;
        private CacheableArrayList _cacheableObj;

        [TestFixtureSetUp]
        public void Initialize()
        {
            var config = new MembaseConfig("searchresults", "", int.MinValue, int.MinValue, int.MinValue, int.MinValue,
                int.MinValue, int.MinValue, int.MinValue, int.MinValue, 3600);
            // production - lacache08("172.16.101.158")
            // stage- lastgcache01,02("172.16.210.136", "172.16.210.137")
            config.Servers.Add(new MembaseConfigServer("172.16.210.136", 8091));
            config.Servers.Add(new MembaseConfigServer("172.16.210.137", 8091));
            _cacheInstance = MembaseCaching.GetSingleton(config);

            _cacheableObj = new CacheableArrayList { 100, 200, 300, 400, 500 };
        }

        [Test]
        public void Remove()
        {
            _cacheInstance.Remove(_cacheableObj.GetCacheKey());
            _cacheInstance.Remove("DailyMatchesScrubList-3-27029711");
        }

        [Test]
        public void PutAndGetTest()
        {
            _cacheInstance.Remove(_cacheableObj.GetCacheKey());
            _cacheInstance.Insert(_cacheableObj);
            var response = _cacheInstance.Get(_cacheableObj.GetCacheKey());
            Assert.IsNotNull(response, "get failed");
            var actualval = response as CacheableArrayList;
            Assert.IsNotNull(actualval, "cast failed");
            Assert.IsTrue(actualval.Contains(100));
            Assert.IsTrue(actualval.Contains(200));
            Assert.IsTrue(actualval.Contains(300));
            Assert.IsTrue(actualval.Contains(400));
            Assert.IsTrue(actualval.Contains(500));
        }

        [Test]
        public void PutAndGetAfterExpiration()
        {
            _cacheableObj.CacheTTLSeconds = 30;
            _cacheInstance.Insert(_cacheableObj);
            var response1 = _cacheInstance.Get(_cacheableObj.GetCacheKey());
            Assert.IsNotNull(response1, "get failed. should not be null.");
            Thread.Sleep(new TimeSpan(0, 0, 60));
            var response2 = _cacheInstance.Get(_cacheableObj.GetCacheKey());
            Assert.IsNull(response2, "get failed. should be null.");
        }
    }
}
