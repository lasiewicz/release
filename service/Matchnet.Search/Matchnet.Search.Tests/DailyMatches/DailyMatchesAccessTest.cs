﻿#region

using System;
using System.Collections.Generic;
using System.Threading;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters.DailyMatches;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.DailyMatches;
using NUnit.Framework;
using Spark.Caching;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

#endregion

namespace Matchnet.Search.Tests.DailyMatches
{
    [TestFixture]
    public class DailyMatchesAccessTest
    {
        private IDailyMatchesAccess _access;
        private int _communityId;
        private int _memberId;
        private int _cacheTtlSeconds;
        private MatchnetQueryResults _queryResultDailyMatches;
        private DailyMatchesScrubList _scrubList;

        [TestFixtureSetUp]
        public void Initialize()
        {
            _access =
                new DailyMatchesAccess(
                    MembaseCaching.GetSingleton(RuntimeSettings.GetMembaseConfigByBucket("searchresults")));

            _memberId = 27029711; // good ol' wlee@spark.net
            _communityId = 3;
            _cacheTtlSeconds = 120;

            _queryResultDailyMatches = new MatchnetQueryResults(5);
            _queryResultDailyMatches.AddResult(10);
            _queryResultDailyMatches.AddResult(11);
            _queryResultDailyMatches.AddResult(12);
            _queryResultDailyMatches.AddResult(13);
            _queryResultDailyMatches.AddResult(113697481); // member I said No to on Stage

            _scrubList = new DailyMatchesScrubList(_communityId, _memberId, _cacheTtlSeconds, new HashSet<int>
            {
                10,
                20,
                21
            }, DateTime.Now);
        }

        [Test]
        public void AppendToScrubListTest()
        {
            // Setup
            var appendItems = new List<IMatchnetResultItem>
            {
                new MatchnetResultItem(10, 82),
                new MatchnetResultItem(55, 84),
                new MatchnetResultItem(56, 75)
            };

            // Test
            var result = _access.AppendToScrubList(_scrubList, appendItems);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.MemberIds.Count == 5);
        }

        [Test]
        public void CreateDailyMatchesTest()
        {
            // Setup

            // Test
            var result = _access.CreateDailyMatches(_communityId, _memberId, _queryResultDailyMatches);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.CommunityId == _communityId);
            Assert.IsTrue(result.MemberId == _memberId);
        }

        [Test]
        public void CreateAndSaveAndGetDailyMatchesTest()
        {
            var dailyMatchesResults = _access.CreateDailyMatches(_communityId, _memberId, _queryResultDailyMatches);

            Assert.IsNotNull(dailyMatchesResults);

            _access.SaveDailyMatches(dailyMatchesResults);

            var retrievedDailyMatchesResults = _access.GetDailyMatches(_communityId, _memberId);

            Assert.IsNotNull(retrievedDailyMatchesResults);
            Assert.IsTrue(retrievedDailyMatchesResults.MemberId == _memberId, "cached MemberId does not match");
            Assert.IsTrue(retrievedDailyMatchesResults.CommunityId == _communityId, "cached communityId does not match");
        }


        [Test]
        public void CreateAndSaveAndGetScrubListTest()
        {
            // Setup

            // Test
            var result = _access.CreateScrubList(_communityId, _memberId);
            result.MemberIds.Add(100);
            result.MemberIds.Add(200);
            _access.SaveScrubList(result, false);
            var scrubList = _access.GetScrubList(_communityId, _memberId);

            // Assert
            Assert.IsNotNull(result, "create failed");
            Assert.IsTrue(result.CommunityId == _communityId);
            Assert.IsTrue(result.MemberId == _memberId);
            Assert.IsNotNull(scrubList, "scrubList not found in cache");
            Assert.IsTrue(scrubList.MemberId == _memberId, "cached MemberId does not match");
            Assert.IsTrue(scrubList.CommunityId == _communityId, "cached communityId does not match");
            Assert.IsTrue(scrubList.GetCacheKey() == result.GetCacheKey(),
                "CacheKey does not match. " + scrubList.GetCacheKey() + " " + result.GetCacheKey());
        }

        [Test]
        public void ExcludeScrubMembersWithNoScrubList()
        {
            // Setup

            // Test
            var daily = _access.CreateDailyMatches(_communityId, _memberId, _queryResultDailyMatches);
            var result = _access.ExcludeScrubMembers(daily, null);

            // Assert
            Assert.IsTrue(daily.Items.Count == 5, "need to have 5 items in the daily");
            Assert.IsNotNull(result, "response from the method cannot be null");
            Assert.IsTrue(result.Count == 5, "after excluding nothing the total should be the same");
        }

        [Test]
        public void ExcludeScrubMembersWithScrubList()
        {
            // Setup

            // Test
            var daily = _access.CreateDailyMatches(_communityId, _memberId, _queryResultDailyMatches);
            var result = _access.ExcludeScrubMembers(daily, _scrubList);

            // Assert
            Assert.IsTrue(daily.Items.Count == 5, "need to have 5 items in the daily");
            Assert.IsNotNull(result, "response from the method cannot be null");
            Assert.IsTrue(result.Count == 4, "after excluding the total should be 4");
        }


        [Test]
        public void SaveAndExpireScrubList()
        {
            var scrubList = _access.CreateScrubList(_communityId, _memberId);
            scrubList.CacheTTLSeconds = 30;
            _access.SaveScrubList(scrubList, false);
            Thread.Sleep(new TimeSpan(0, 0, 60));
            var result = _access.GetScrubList(_communityId, _memberId);
            Assert.IsNull(result);

        }
    }
}