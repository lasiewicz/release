﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Search.ServiceAdapters.DailyMatches;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.DailyMatches;
using NUnit.Framework;
using Spark.Caching;

#endregion

namespace Matchnet.Search.Tests.DailyMatches
{
    [TestFixture]
    public class DailyMatchesCacheRepositoryTest
    {
        private int _cacheTtlSeconds;
        private int _communityId;
        private DailyMatchesResults _dailyMatchesResults;
        private DailyMatchesScrubList _dailyMatchesScrubList;
        private MatchnetResultItems _dailyResultItems;
        private int _memberId;
        private MatchnetQueryResults _queryResultDailyMatches;
        private ICacheRepository _repository;

        [TestFixtureSetUp]
        public void Initialize()
        {
            var config = new MembaseConfig("searchresults", "", int.MinValue, int.MinValue, int.MinValue, int.MinValue,
                int.MinValue, int.MinValue, int.MinValue, int.MinValue, 3600);
            // production - lacache08("172.16.101.158")
            // stage- lastgcache01,02("172.16.210.136", "172.16.210.137")
            config.Servers.Add(new MembaseConfigServer("172.16.210.136", 8091));
            config.Servers.Add(new MembaseConfigServer("172.16.210.137", 8091));
            var cacheInstance = MembaseCaching.GetSingleton(config);

            _repository = new SparkCachingRepository(cacheInstance);

            _memberId = 27029711; // good ol' wlee@spark.net
            _communityId = 3;
            _cacheTtlSeconds = 120;

            _queryResultDailyMatches = new MatchnetQueryResults(2);
            _queryResultDailyMatches.AddResult(10);
            _queryResultDailyMatches.AddResult(11);

            _dailyResultItems = new MatchnetResultItems(2);
            _dailyResultItems.AddResult(new MatchnetResultItem(10, 88));
            _dailyResultItems.AddResult(new MatchnetResultItem(20, 88));

            _dailyMatchesResults = new DailyMatchesResults(_communityId, _memberId, _cacheTtlSeconds, _dailyResultItems);

            _dailyMatchesScrubList = new DailyMatchesScrubList(_communityId, _memberId, _cacheTtlSeconds,
                new HashSet<int> {20, 30, 40}, DateTime.Now);
        }

        /// <summary>
        ///     Functional Test
        /// </summary>
        [Test]
        public void PutAndGetDailyMatches()
        {
            // Setup

            // Test
            _repository.Insert(_dailyMatchesResults);
            var result = _repository.Get<DailyMatchesResults>(_dailyMatchesResults.GetCacheKey());

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Items.Count == 2);
        }

        /// <summary>
        ///     Functional Test
        /// </summary>
        [Test]
        public void PutAndGetScrubList()
        {
            // Setup

            // Test
            _repository.Insert(_dailyMatchesScrubList);
            var result = _repository.Get<DailyMatchesScrubList>(_dailyMatchesScrubList.GetCacheKey());

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.MemberIds.Count == _dailyMatchesScrubList.MemberIds.Count);
        }

        [Test]
        public void PutAndRemoveDailyMatches()
        {
            // Setup

            // Test
            _repository.Insert(_dailyMatchesResults);
            _repository.Delete(_dailyMatchesResults.GetCacheKey());
            var result = _repository.Get<ArrayList>(_dailyMatchesResults.GetCacheKey());

            // Assert
            Assert.IsNull(result);
        }

        [Test]
        public void PutAndRemoveScrubList()
        {
            // Setup

            // Test
            _repository.Insert(_dailyMatchesScrubList);
            _repository.Delete(_dailyMatchesScrubList.GetCacheKey());
            var result = _repository.Get<DailyMatchesScrubList>(_dailyMatchesScrubList.GetCacheKey());

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(_dailyMatchesScrubList.GetCacheKey()));
            Assert.IsNull(result);
        }

        [Test]
        public void PutAndExpireScrubList()
        {
            // Setup

            // Test
            _dailyMatchesScrubList.CacheTTLSeconds = 1200;
            _repository.Insert(_dailyMatchesScrubList);

            Thread.Sleep(new TimeSpan(0, 0, 1250));

            var result = _repository.Get<DailyMatchesScrubList>(_dailyMatchesScrubList.GetCacheKey());

            // Assert
            Assert.IsNull(result);
        }
    }
}