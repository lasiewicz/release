﻿#region

using Matchnet.Content.ServiceAdapters;
using Matchnet.Search.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Spark.SAL;

#endregion

namespace Matchnet.Search.Tests.DailyMatches
{
    // !!! to test, uncomment //using Spark.SAL and //public void SearchDailyMatchesNewTest()
    // make sure to comment back in after testing.
    // they're commented out due to a circular dependency in the build script which builds Matchnet.Search 1st then Spark.SAL 
    [TestClass]
    public class DailyMatchesMemberSaTest
    {
        [TestInitialize]
        public void Initialize()
        {
        }



        //[TestMethod]
        //public void SearchDailyMatchesNewTest()
        //{
        //    var brand = BrandConfigSA.Instance.GetBrandByID(1003);
        //    // um.. this .Load method requests for the users's session from Session Serivce
        //    // which requires http context be present.. we should get rid of that dependency
        //    // otherwise makes unit testing not ideal (even if with fake httpcontext)
        //    var search = MemberSearchCollection.Load(27029711, brand).PrimarySearch;
        //    var prefs = MemberSearch.getSearchPreferenceCollection(search);
        //    var results = MemberSearchSA.Instance.Search(prefs, 3, 103, 0, 1);

        //    Assert.IsNotNull(results, "results cannot be null");
        //    Assert.IsNotNull(results.Items, "results does not contain any items");
        //    Assert.IsTrue(results.MatchesFound > 0, "matches found must be larger than 0");
        //}
    }
}