﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Search.ServiceAdapters.Utils;
using NUnit.Framework;

namespace Matchnet.Search.Tests
{
    [TestFixture]
    public class MemberSearchSATests
    {
//        int pMemberId = 100004579;
        int pMemberId = 100046782;
        [Test]
        public void TestBlockedMemberIds()
        {
            
            StringBuilder parallelBlockedMemberIds = ListFilterAccess.GetBlockedMemberIDsForMember(pMemberId, 3, 103, true, null);
            StringBuilder blockedMemberIds = ListFilterAccess.GetBlockedMemberIDsForMember(pMemberId, 3, 103, false, null);

            Assert.AreEqual(parallelBlockedMemberIds.Length, blockedMemberIds.Length);
        }

    }
}
