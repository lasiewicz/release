using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.DistributedCaching.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.RemotingClient;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Spark.Logger;
using System.Threading.Tasks;
using Matchnet.Search.ServiceAdapters.DailyMatches;
using Matchnet.Search.ServiceAdapters.Utils;
using Matchnet.Search.ValueObjects.DailyMatches;
using Spark.Caching;

namespace Matchnet.Search.ServiceAdapters
{
    /// <summary>
    /// Service Adapter for MemberSearch service.
    /// </summary>
    public class MemberSearchSA : SABase, IMemberSearchSA
    {
        #region Constants

        private const string ServiceManagerName = "MemberSearchSM";
        private const string ServiceConstant = "SEARCH_SVC";
        private const string SearchResultsNamedCache = "searchresults";
        /// <summary>
        /// 
        /// </summary>
        public const string CommaSeparator = ",";

        #endregion

        #region Class members
        private readonly Caching.Cache _cache;
        private const ResultsKey.ResultsCacheVersion CacheMemberResultsVersion = ResultsKey.ResultsCacheVersion.Version2; //version to control changes to member specific search results cache so it doesn't break with prod deploy as we convert

        #endregion

        #region Singleton implementation
        /// <summary>
        /// The singleton instance of the MemberSearch service adapter.
        /// </summary>
        public static readonly MemberSearchSA Instance = new MemberSearchSA();

        private MemberSearchSA()
        {	// Singleton
            _cache = Matchnet.Caching.Cache.Instance;
        }
        #endregion


        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_SA_CONNECTION_LIMIT"));
        }

        
        #region Service adapter public interface

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, Matchnet.Constants.NULL_INT, pSearchEngineType, pSearchType, false);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID			
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , bool ignoreCache) 
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID, pSearchEngineType, pSearchType, ignoreCache, false);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly
            , bool ignoreAllCache)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize,
                pMemberID, pSearchEngineType, pSearchType, SearchEntryPoint.None,
                ignoreSACache, ignoreSAFilteredCacheOnly, ignoreAllCache);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID,
            int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , SearchEntryPoint pSearchEntryPoint
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly
            , bool ignoreAllCache)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize,
                pMemberID, pSearchEngineType, pSearchType, pSearchEntryPoint,
                ignoreSACache, ignoreSAFilteredCacheOnly, ignoreAllCache, null);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, SearchEngineType.FAST, SearchType.WebSearch);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID, SearchEngineType.FAST, SearchType.WebSearch, false);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, bool ignoreSAFilteredCacheOnly, SearchType pSearchType)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID, SearchEngineType.FAST, pSearchType, false, ignoreSAFilteredCacheOnly, false);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, bool ignoreSACache, bool ignoreSAFilteredCacheOnly, bool ignoreAllCache, SearchType pSearchType)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID, SearchEngineType.FAST, pSearchType, ignoreSACache, ignoreSAFilteredCacheOnly, ignoreAllCache);
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly)
        {
            return Search(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID, pSearchEngineType, pSearchType, ignoreSACache, ignoreSAFilteredCacheOnly, false);
        }

        /// <summary>
        /// Executes a member search in accordance with the supplied parameters
        /// </summary>
        /// <param name="pPreferences"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pSiteID"></param>
        /// <param name="pStartRow"></param>
        /// <param name="pPageSize"></param>
        /// <param name="pMemberID">If we need to filter the search results with the member's exclusion list</param>
        /// <param name="pSearchEngineType">Relational or FAST search.</param>
        /// <param name="pSearchType">Search Type (WebSearch, MatchMail, etc.)</param>
        /// <param name="ignoreSACache">Ignores all SA Cached search results</param>
        /// <param name="ignoreSAFilteredCacheOnly">Ignores any filtered SA search results</param>
        /// <param name="ignoreAllCache">Ignores all cached search results</param>
        /// <returns>A MemberSearchResults value object representing the identified members corresponding to the search criteria.</returns>
        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , SearchEntryPoint pSearchEntryPoint
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly
            , bool ignoreAllCache,
            ISparkLogger logger)
        {
            MatchnetQueryResults queryResults = null;

            #region Search Settings
            // If this is a Profile Blocking-enabled community
            var isProfileBlockingEnabled = (pMemberID != Constants.NULL_INT && Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", pCommunityID)));
            var isE2ProfileBlockingEnabled = (pMemberID != Constants.NULL_INT && Convert.ToBoolean(RuntimeSettings.GetSetting("E2_PROFILE_BLOCK_ENABLED", pCommunityID)));
            var isE2YnmNoFilterEnabled = (pMemberID != Constants.NULL_INT && Convert.ToBoolean(RuntimeSettings.GetSetting("E2_PROFILE_YNM_NO_FILTER_ENABLED", pCommunityID)));

            // find out if we need to do YNM NO filtering after results are returned
            var isYnmNoFilterEnabled = GetIsYnmNoFilterEnabled(pPreferences, pSearchType, isE2YnmNoFilterEnabled);

            var isMatchRatingsCalculationEnabled = pMemberID > 0 && MatchRatingUtil.IsMatchRatingCalculationEnabled(pCommunityID, pSearchType);
            #endregion
            
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                string methodName = "Search()";

                if (pStartRow < 0)
                    pStartRow = 0;
                if (pPageSize < 0)
                    pPageSize = 1;

                // DomainID needs to be added as a search preference.
                pPreferences["DomainID"] = pCommunityID.ToString();

                // SearchType gets passed to the QueryBuilder as a preference.
                pPreferences["SearchType"] = ((int)pSearchType).ToString();

                // pass memberid into search prefs
                pPreferences["MemberId"] = pMemberID.ToString();

                if (null != logger)
                {
                    logger.LogInfoMessage(string.Format("START: Method:{0}, Section:pPreferences.Validate()", methodName), null);
                    stopwatch.Restart();                    
                }
                // Ensure that the minimum amount of criteria has been supplied.
                ValidationResult result = pPreferences.Validate();

                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis1 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(string.Format("END: Method:{0}, Section:pPreferences.Validate(), ElapsedTime:{1}ms", methodName, ellapsedTimeInMillis1), null);
                }

                // Do not attempt a search unless we have a valid set of search preferences.
                if (PreferencesValidationStatus.Failed.Equals(result.Status))
                {
                    throw new Exception(string.Format("Invalid search preferences: {0}, memberid: {1}, preferences \r\n{2}", result.ValidationError, pMemberID, pPreferences.ToString()));
                }

                if (null != logger)
                {
                    logger.LogInfoMessage(string.Format("START: Method:{0}, Section:pPreferences.TransformSearchPreferences(prefs, communityId:{1})", methodName, pCommunityID), null);
                    stopwatch.Restart();
                }

                // Before looking up cached results, we need to transform / augment some of the 
                // preferences.
                FASTQueryTransformer.TransformSearchPreferences(pPreferences, pCommunityID);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis2 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(string.Format("END: Method:{0}, Section:pPreferences.TransformSearchPreferences(prefs, communityId:{1}), ElapsedTime:{2}ms", methodName, pCommunityID, ellapsedTimeInMillis2), null);
                }

                // add YNM NO members to search preferences to filter by E2
                // exclude applying adding of NO members to search preferences for the following cases
                if (pSearchType == SearchType.YourMatchesWebSearch || pSearchType == SearchType.MatchMail ||
                    pPreferences["excludeynmnoflag"] == "1")
                {
                    AddExcludeYnmMemberIdsSearchPreference(pPreferences, pCommunityID, pSiteID, pMemberID, logger,
                        isE2YnmNoFilterEnabled, methodName, stopwatch);

                    // add blocked members to filter
                    AddBlockedMemberIdsSearchPreference(pPreferences, pCommunityID, pSiteID, pMemberID, logger,
                        isE2ProfileBlockingEnabled, methodName, stopwatch);
                }

                // add match rating
                pPreferences = AddMatchingRating(pPreferences, pCommunityID, pSiteID, pMemberID, logger, isMatchRatingsCalculationEnabled, methodName, stopwatch);

                //members online searches, don't use cache for page 1 so it's always updated
                if (pSearchType == SearchType.MembersOnline && pStartRow <= 1)
                {
                    ignoreAllCache = true;
                }

                // ensure mol search always contains the online only preference
                if (pSearchType == SearchType.MembersOnline && string.IsNullOrEmpty(pPreferences["online"]))
                {
                    pPreferences["online"] = "1";
                }

                // check to see if we have a cached response for DailyMatches
                IDailyMatchesAccess dailyMatchesAccess = null;
                if (pSearchType == SearchType.DailyMatches)
                {
                    // only create an instance for DailyMatches
                    dailyMatchesAccess =
                        new DailyMatchesAccess(MembaseCaching.GetSingleton(RuntimeSettings.GetMembaseConfigByBucket(SearchResultsNamedCache)));
                    var dailyMatchesResult = dailyMatchesAccess.GetDailyMatches(pCommunityID, pMemberID);
                    if (dailyMatchesResult != null)
                    {
                        queryResults = dailyMatchesAccess.GetDailyMatchnetQueryResults(dailyMatchesResult);

                        LogSearchPageViewedActivity(pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID,
                            pSearchType, pSearchEntryPoint, logger, methodName, stopwatch, queryResults);

                        // finish the call immediately and return
                        return queryResults;
                    }
                }
                else
                {
                    // this applies for all other search types
                    queryResults = GetMemCachedMembers(pPreferences, pCommunityID, pSiteID, pStartRow, pPageSize,
                        pMemberID, ignoreSACache, ignoreSAFilteredCacheOnly, ignoreAllCache, logger,
                        isProfileBlockingEnabled, isE2ProfileBlockingEnabled, isMatchRatingsCalculationEnabled,
                        isE2YnmNoFilterEnabled, methodName, stopwatch, isYnmNoFilterEnabled, queryResults);
                }

                //if necessary, check common cache (unfiltered) or call E2 to get new results
                if (queryResults == null)
                {
                    queryResults = GetLocalCacheMembers(pPreferences, pCommunityID, pSiteID, ignoreSACache, ignoreAllCache, logger, methodName, stopwatch, queryResults);

                    //call E2 to get new results
                    if (queryResults == null)
                    {
                        //System.Diagnostics.Trace.WriteLine("In search adapter Null results in cache:" +  ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID));
                        // Execute search against the middle tier and cache the results.
                        if (null != logger)
                        {
                            logger.LogInfoMessage(string.Format("START: Method:{0}, Section:ExecuteSearch(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}, pSearchEngineType:{4}, ignoreAllCache:{5})", methodName, pPreferences.Count, pCommunityID, pSiteID, pSearchEngineType, ignoreAllCache), null);
                            stopwatch.Restart();
                        }
                        queryResults = ExecuteSearch(pPreferences, pCommunityID, pSiteID, pSearchEngineType, ignoreAllCache);
                        if (null != logger)
                        {
                            stopwatch.Stop();
                            long ellapsedTimeInMillis10 = stopwatch.ElapsedMilliseconds;
                            logger.LogInfoMessage(string.Format("END: Method:{0}, Section:ExecuteSearch(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}, pSearchEngineType:{4}, ignoreAllCache:{5}), ElapsedTime:{6}ms", methodName, pPreferences.Count, pCommunityID, pSiteID, pSearchEngineType, ignoreAllCache, ellapsedTimeInMillis10), null);
                        }
                        if (null != logger)
                        {
                            logger.LogInfoMessage(string.Format("START: Method:{0}, Section:CacheResults(queryResults:{1})", methodName, queryResults.Items.Count), null);
                            stopwatch.Restart();
                        }
                        CacheResults(queryResults);
                        if (null != logger)
                        {
                            stopwatch.Stop();
                            long ellapsedTimeInMillis11 = stopwatch.ElapsedMilliseconds;
                            logger.LogInfoMessage(string.Format("END: Method:{0}, Section:CacheResults(queryResults:{1}), ElapsedTime:{2}ms", methodName, queryResults.Items.Count, ellapsedTimeInMillis11), null);
                        }
                        //System.Diagnostics.Trace.WriteLine("In search adapter Insert in cache:" +  ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID));
                    }

                    // if this is member specific filters or results, let's use distributed caching for member specific results
                    if (isE2ProfileBlockingEnabled || isMatchRatingsCalculationEnabled || isE2YnmNoFilterEnabled)
                    {
                        var cacheKey = GetMemberSpecificCacheKey(pPreferences, pCommunityID, pSiteID, pMemberID, logger, methodName, stopwatch);

                        PutQueryResultsInMemcache(logger, methodName, cacheKey, stopwatch, queryResults);

                        if (isYnmNoFilterEnabled)
                        {
                            var resultsMembers = ListFilterAccess.RemoveYnmNoMembers(pCommunityID, pMemberID, queryResults, methodName, stopwatch, logger);

                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3})", methodName, resultsMembers.Count, pStartRow, pPageSize), null);
                                stopwatch.Restart();
                            }
                            queryResults = PagingUtil.PageQueryResults(resultsMembers, pStartRow, pPageSize);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis15 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3}), ElapsedTime:{4}ms", methodName, resultsMembers.Count, pStartRow, pPageSize, ellapsedTimeInMillis15), null);
                            }
                        }
                        else
                        {
                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3})", methodName, queryResults.Items.Count, pStartRow, pPageSize), null);
                                stopwatch.Restart();
                            }
                            queryResults = PagingUtil.PageQueryResults(queryResults, pStartRow, pPageSize);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis16 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3}), ElapsedTime:{4}ms", methodName, queryResults.Items.Count, pStartRow, pPageSize, ellapsedTimeInMillis16), null);
                            }
                        }
                        
                    }
                    else if (isProfileBlockingEnabled)
                    {
                        //member specific cache
                        ArrayList filteredResults = null;
                        if (isYnmNoFilterEnabled)
                        {
                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:RemoveHotlistedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}, [HotListCategory.ExcludeListInternal, HotListCategory.MembersClickedNo])", methodName, queryResults.Items.Count, pCommunityID, pMemberID), null);
                                stopwatch.Restart();
                            }
                            List<HotListCategory> hotlistCategories = new List<HotListCategory>();
                            hotlistCategories.Add(HotListCategory.ExcludeListInternal);
                            hotlistCategories.Add(HotListCategory.MembersClickedNo);
                            filteredResults = ListFilterAccess.RemoveHotlistedMembers(queryResults, pCommunityID, pMemberID, hotlistCategories);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis17 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:RemoveHotlistedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}, [HotListCategory.ExcludeListInternal, HotListCategory.MembersClickedNo]), ElapsedTime:{4}ms", methodName, queryResults.Items.Count, pCommunityID, pMemberID, ellapsedTimeInMillis17), null);
                            }
                        }
                        else
                        {
                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:RemoveExludedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3})", methodName, queryResults.Items.Count, pCommunityID, pMemberID), null);
                                stopwatch.Restart();
                            }
                            filteredResults = ListFilterAccess.RemoveExludedMembers(queryResults, pCommunityID, pMemberID);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis18 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:RemoveExludedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}), ElapsedTime:{4}ms", methodName, queryResults.Items.Count, pCommunityID, pMemberID, ellapsedTimeInMillis18), null);
                            }
                        }

                        if (null != logger)
                        {
                            logger.LogInfoMessage(string.Format("START: Method:{0}, Section:GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}), pMemberID:{4})", methodName, pPreferences.Count, pCommunityID, pSiteID, pMemberID), null);
                            stopwatch.Restart();
                        }
                        string cacheKey = GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID), pMemberID);
                        if (null != logger)
                        {
                            stopwatch.Stop();
                            long ellapsedTimeInMillis19 = stopwatch.ElapsedMilliseconds;
                            logger.LogInfoMessage(string.Format("END: Method:{0}, Section:GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}), pMemberID:{4}), ElapsedTime:{5}ms", methodName, pPreferences.Count, pCommunityID, pSiteID, pMemberID, ellapsedTimeInMillis19), null);
                        }

                        if (null != logger)
                        {
                            logger.LogInfoMessage(string.Format("START: Method:{0}, Section:DistributedCachingSA.Instance.Put(cacheKey:{1}, filteredResults:{2}, SEARCH_RESULTS_NAMED_CACHE)", methodName, cacheKey, filteredResults.Count), null);
                            stopwatch.Restart();
                        }
                        DistributedCachingSA.Instance.Put(cacheKey, filteredResults, SearchResultsNamedCache);
                        if (null != logger)
                        {
                            stopwatch.Stop();
                            long ellapsedTimeInMillis20 = stopwatch.ElapsedMilliseconds;
                            logger.LogInfoMessage(string.Format("END: Method:{0}, Section:DistributedCachingSA.Instance.Put(cacheKey:{1}, filteredResults:{2}, SEARCH_RESULTS_NAMED_CACHE), ElapsedTime:{3}ms", methodName, cacheKey, filteredResults.Count, ellapsedTimeInMillis20), null);
                        }

                        if (null != logger)
                        {
                            logger.LogInfoMessage(string.Format("START: Method:{0}, Section:PageQueryResults(filteredResults:{1}, pStartRow:{2}, pPageSize:{3})", methodName, filteredResults.Count, pStartRow, pPageSize), null);
                            stopwatch.Restart();
                        }
                        queryResults = PagingUtil.PageQueryResults(filteredResults, pStartRow, pPageSize);
                        if (null != logger)
                        {
                            stopwatch.Stop();
                            long ellapsedTimeInMillis21 = stopwatch.ElapsedMilliseconds;
                            logger.LogInfoMessage(string.Format("END: Method:{0}, Section:PageQueryResults(filteredResults:{1}, pStartRow:{2}, pPageSize:{3}), ElapsedTime:{4}ms", methodName, filteredResults.Count, pStartRow, pPageSize, ellapsedTimeInMillis21), null);
                        }
                    }
                    else
                    {
                        // return the desired subset / page of results.
                        if (isYnmNoFilterEnabled)
                        {
                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:RemoveHotlistedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}, HotListCategory.MembersClickedNo)", methodName, queryResults.Items.Count, pCommunityID, pMemberID), null);
                                stopwatch.Restart();
                            }
                            ArrayList resultsMembers = ListFilterAccess.RemoveHotlistedMembers(queryResults, pCommunityID, pMemberID, HotListCategory.MembersClickedNo);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis22 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:RemoveHotlistedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}, HotListCategory.MembersClickedNo), ElapsedTime:{4}ms", methodName, queryResults.Items.Count, pCommunityID, pMemberID, ellapsedTimeInMillis22), null); 
                            }

                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3})", methodName, resultsMembers.Count, pStartRow, pPageSize), null);
                                stopwatch.Restart();
                            }
                            queryResults = PagingUtil.PageQueryResults(resultsMembers, pStartRow, pPageSize);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis23 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3}), ElapsedTime:{4}ms", methodName, resultsMembers.Count, pStartRow, pPageSize, ellapsedTimeInMillis23), null);
                            }
                        }
                        else
                        {
                            if (null != logger)
                            {
                                logger.LogInfoMessage(string.Format("START: Method:{0}, Section:PageQueryResults(queryResults:{1}, pStartRow:{2}, pPageSize:{3})", methodName, queryResults.Items.Count, pStartRow, pPageSize), null);
                                stopwatch.Restart();
                            }
                            queryResults = PagingUtil.PageQueryResults(queryResults, pStartRow, pPageSize);
                            if (null != logger)
                            {
                                stopwatch.Stop();
                                long ellapsedTimeInMillis24 = stopwatch.ElapsedMilliseconds;
                                logger.LogInfoMessage(string.Format("END: Method:{0}, Section:PageQueryResults(queryResults:{1}, pStartRow:{2}, pPageSize:{3}), ElapsedTime:{4}ms", methodName, queryResults.Items.Count, pStartRow, pPageSize, ellapsedTimeInMillis24), null);
                            }
                        }
                    }
                }

                // store new results in DailyMatches and update ScrubList
                // this block will only get executed when there was no cached response earlier
                if (pSearchType == SearchType.DailyMatches)
                {
                    dailyMatchesAccess.SaveDailyMatches(
                        dailyMatchesAccess.CreateDailyMatches(pCommunityID, pMemberID,
                            queryResults));

                    var dailyMatchesResult = dailyMatchesAccess.GetDailyMatches(pCommunityID, pMemberID);
                    if (dailyMatchesResult != null)
                    {
                        // this will apply other business rules such as filtering
                        queryResults =
                            dailyMatchesAccess.GetDailyMatchnetQueryResults(dailyMatchesResult);
                    }
                }

                LogSearchPageViewedActivity(pCommunityID, pSiteID, pStartRow, pPageSize, pMemberID, pSearchType, pSearchEntryPoint, logger, methodName, stopwatch, queryResults);

                return queryResults;

            }
            catch (ExceptionBase ex)
            {
                string preferences = "";
                if (pPreferences != null)
                    preferences = pPreferences.ToString();
                throw (new SAException("Error occurred while attempting to execute member search. Preferences \r\n" + preferences, ex));
            }
            catch (Exception ex)
            {
                string preferences = "";
                if (pPreferences != null)
                    preferences = pPreferences.ToString();
                throw (new SAException("Error occurred while attempting to execute member search.Preferences \r\n" + preferences, ex));
            }
        }

        private static void PutQueryResultsInMemcache(ISparkLogger logger, string methodName, string cacheKey,
            Stopwatch stopwatch, MatchnetQueryResults queryResults)
        {
            if (null != logger)
            {
                logger.LogInfoMessage(
                    string.Format(
                        "START: Method:{0}, Section:DistributedCachingSA.Instance.Put(cacheKey:{1}, queryResults.ToArrayListOfMemberIDMatchscore(), SEARCH_RESULTS_NAMED_CACHE)",
                        methodName, cacheKey), null);
                stopwatch.Restart();
            }
            DistributedCachingSA.Instance.Put(cacheKey, queryResults.ToArrayListOfMemberIDMatchscore(), SearchResultsNamedCache);
            if (null != logger)
            {
                stopwatch.Stop();
                long ellapsedTimeInMillis13 = stopwatch.ElapsedMilliseconds;
                logger.LogInfoMessage(
                    string.Format(
                        "END: Method:{0}, Section:DistributedCachingSA.Instance.Put(cacheKey:{1}, queryResults.ToArrayListOfMemberIDMatchscore(), SEARCH_RESULTS_NAMED_CACHE), ElapsedTime:{2}ms",
                        methodName, cacheKey, ellapsedTimeInMillis13), null);
            }
        }

        private string GetMemberSpecificCacheKey(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID,
            int pMemberID, ISparkLogger logger, string methodName, Stopwatch stopwatch)
        {
//member specific cache
            if (null != logger)
            {
                logger.LogInfoMessage(
                    string.Format(
                        "START: Method:{0}, Section:GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}), pMemberID:{4})",
                        methodName, pPreferences.Count, pCommunityID, pSiteID, pMemberID), null);
                stopwatch.Restart();
            }
            string cacheKey = GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID), pMemberID);
            if (null != logger)
            {
                stopwatch.Stop();
                long ellapsedTimeInMillis12 = stopwatch.ElapsedMilliseconds;
                logger.LogInfoMessage(
                    string.Format(
                        "END: Method:{0}, Section:GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}), pMemberID:{4}), ElapsedTime:{5}ms",
                        methodName, pPreferences.Count, pCommunityID, pSiteID, pMemberID, ellapsedTimeInMillis12), null);
            }
            return cacheKey;
        }

        private MatchnetQueryResults GetLocalCacheMembers(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID,
            bool ignoreSACache, bool ignoreAllCache, ISparkLogger logger, string methodName, Stopwatch stopwatch,
            MatchnetQueryResults queryResults)
        {
// Look for cached search results matching these preferences.
            if (!ignoreSACache && !ignoreAllCache)
            {
                if (null != logger)
                {
                    logger.LogInfoMessage(
                        string.Format(
                            "START: Method:{0}, Section:_cache.Get(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}))",
                            methodName, pPreferences.Count, pCommunityID, pSiteID), null);
                    stopwatch.Restart();
                }
                queryResults = _cache.Get(ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID)) as MatchnetQueryResults;
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis9 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(
                        string.Format(
                            "END: Method:{0}, Section:_cache.Get(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3})), ElapsedTime:{4}ms",
                            methodName, pPreferences.Count, pCommunityID, pSiteID, ellapsedTimeInMillis9), null);
                }
            }
            return queryResults;
        }

        private MatchnetQueryResults GetMemCachedMembers(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID,
            int pStartRow, int pPageSize, int pMemberID, bool ignoreSACache, bool ignoreSAFilteredCacheOnly, bool ignoreAllCache,
            ISparkLogger logger, bool isProfileBlockingEnabled, bool isE2ProfileBlockingEnabled,
            bool isMatchRatingsCalculationEnabled, bool isE2YnmNoFilterEnabled, string methodName, Stopwatch stopwatch,
            bool isYnmNoFilterEnabled, MatchnetQueryResults queryResults)
        {
// let's check the distributed cache for member specific cached results (filtered)
            if ((isProfileBlockingEnabled || isE2ProfileBlockingEnabled || isMatchRatingsCalculationEnabled ||
                 isE2YnmNoFilterEnabled) && !ignoreSACache && !ignoreSAFilteredCacheOnly && !ignoreAllCache)
            {
                if (null != logger)
                {
                    logger.LogInfoMessage(
                        string.Format(
                            "START: Method:{0}, Section:GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}), pMemberID:{4})",
                            methodName, pPreferences.Count, pCommunityID, pSiteID, pMemberID), null);
                    stopwatch.Restart();
                }
                string cacheKey = GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID),
                    pMemberID);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis5 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(
                        string.Format(
                            "END: Method:{0}, Section:GetMemberSpecificCacheKey(ResultsKey.GetCacheKey(pPreferences:{1}, pCommunityID:{2}, pSiteID:{3}), pMemberID:{4}), ElapsedTime:{5}ms",
                            methodName, pPreferences.Count, pCommunityID, pSiteID, pMemberID, ellapsedTimeInMillis5), null);
                }

                if (null != logger)
                {
                    logger.LogInfoMessage(
                        string.Format(
                            "START: Method:{0}, Section:DistributedCachingSA.Instance.GetArrayList(cacheKey:{1}, SEARCH_RESULTS_NAMED_CACHE)",
                            methodName, cacheKey), null);
                    stopwatch.Restart();
                }

                ArrayList resultsMembers = DistributedCachingSA.Instance.GetArrayList(cacheKey, SearchResultsNamedCache);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis6 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(
                        string.Format(
                            "END: Method:{0}, Section:DistributedCachingSA.Instance.GetArrayList(cacheKey:'{1}', SEARCH_RESULTS_NAMED_CACHE), ElapsedTime:{2}ms",
                            methodName, cacheKey, ellapsedTimeInMillis6), null);
                }

                if (resultsMembers != null)
                {
                    if (isYnmNoFilterEnabled)
                    {
                        if (null != logger)
                        {
                            logger.LogInfoMessage(
                                string.Format(
                                    "START: Method:{0}, Section:RemoveHotlistedMembers(resultsMembers:{1}, pCommunityID:{2}, pMemberID:{3}, HotListCategory.MembersClickedNo)",
                                    methodName, resultsMembers.Count, pCommunityID, pMemberID), null);
                            stopwatch.Restart();
                        }
                        resultsMembers = ListFilterAccess.RemoveHotlistedMembers(resultsMembers, pCommunityID, pMemberID,
                            HotListCategory.MembersClickedNo);
                        if (null != logger)
                        {
                            stopwatch.Stop();
                            long ellapsedTimeInMillis7 = stopwatch.ElapsedMilliseconds;
                            logger.LogInfoMessage(
                                string.Format(
                                    "END: Method:{0}, Section:RemoveHotlistedMembers(resultsMembers:{1}, pCommunityID:{2}, pMemberID:{3}, HotListCategory.MembersClickedNo), ElapsedTime:{4}ms",
                                    methodName, resultsMembers.Count, pCommunityID, pMemberID, ellapsedTimeInMillis7), null);
                        }
                    }
                    if (null != logger)
                    {
                        logger.LogInfoMessage(
                            string.Format(
                                "START: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3})",
                                methodName, resultsMembers.Count, pStartRow, pPageSize), null);
                        stopwatch.Restart();
                    }
                    queryResults = PagingUtil.PageQueryResults(resultsMembers, pStartRow, pPageSize);
                    if (null != logger)
                    {
                        stopwatch.Stop();
                        long ellapsedTimeInMillis8 = stopwatch.ElapsedMilliseconds;
                        logger.LogInfoMessage(
                            string.Format(
                                "END: Method:{0}, Section:PageQueryResults(resultsMembers:{1}, pStartRow:{2}, pPageSize:{3}), ElapsedTime:{4}ms",
                                methodName, resultsMembers.Count, pStartRow, pPageSize, ellapsedTimeInMillis8), null);
                    }
                }
            }
            return queryResults;
        }

        private static SearchPreferenceCollection AddMatchingRating(SearchPreferenceCollection pPreferences, int pCommunityID,
            int pSiteID, int pMemberID, ISparkLogger logger, bool isMatchRatingsCalculationEnabled, string methodName,
            Stopwatch stopwatch)
        {
//add member attribute data for mutual match ratings calculation support
            if (isMatchRatingsCalculationEnabled)
            {
                if (null != logger)
                {
                    logger.LogInfoMessage(
                        string.Format(
                            "START: Method:{0}, Section:MatchRatingUtil.AddMatchRatingHasData(pMemberID:{1}, pPreferences:{2}, pCommunityID:{3}, pSiteID:{4})",
                            methodName, pMemberID, pPreferences.Count, pCommunityID, pSiteID), null);
                    stopwatch.Restart();
                }
                pPreferences = MatchRatingUtil.AddMatchRatingHasData(pMemberID, pPreferences, pCommunityID, pSiteID);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis4 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(
                        string.Format(
                            "END: Method:{0}, Section:MatchRatingUtil.AddMatchRatingHasData(pMemberID:{1}, pPreferences:{2}, pCommunityID:{3}, pSiteID:{4}), ElapsedTime:{5}ms",
                            methodName, pMemberID, pPreferences.Count, pCommunityID, pSiteID, ellapsedTimeInMillis4), null);
                }
            }
            return pPreferences;
        }

        private void AddBlockedMemberIdsSearchPreference(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID,
            int pMemberID, ISparkLogger logger, bool isE2ProfileBlockingEnabled, string methodName, Stopwatch stopwatch)
        {
//if e2 profile blocking enabled, add blocked ids to prefs here
            if (isE2ProfileBlockingEnabled)
            {
                if (null != logger)
                {
                    logger.LogInfoMessage(
                        string.Format(
                            "START: Method:{0}, Section:GetBlockedMemberIDsForMember(pMemberID:{1}, pCommunityID:{2}, pSiteID:{3})",
                            methodName, pMemberID, pCommunityID, pSiteID), null);
                    stopwatch.Restart();
                }
                StringBuilder allBlockedMemberIdString = ListFilterAccess.GetBlockedMemberIDsForMember(pMemberID, pCommunityID, pSiteID, true,
                    logger);

                if (allBlockedMemberIdString.Length > 0)
                {
                    pPreferences.Add("blockedmemberids",
                        allBlockedMemberIdString.ToString(0, allBlockedMemberIdString.Length - 1));
                }
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis3 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(
                        string.Format(
                            "END: Method:{0}, Section:GetBlockedMemberIDsForMember(pMemberID:{1}, pCommunityID:{2}, pSiteID:{3}), ElapsedTime:{4}ms",
                            methodName, pMemberID, pCommunityID, pSiteID, ellapsedTimeInMillis3), null);
                }
            }
        }

        /// <summary>
        ///     Determines if YNM NO filtering is required after results are back
        ///     This is different than what E2 does with data from AddExcludeYnmMemberIdsSearchPreference
        /// </summary>
        /// <param name="pPreferences"></param>
        /// <param name="pSearchType"></param>
        /// <param name="isE2YnmNoFilterEnabled"></param>
        /// <returns></returns>
        private static bool GetIsYnmNoFilterEnabled(SearchPreferenceCollection pPreferences,
            SearchType pSearchType, bool isE2YnmNoFilterEnabled)
        {
            var isYnmNoFilterEnabled = false;

            //Set Filtering YNM No's for match searches
            //E2 ynm filter
            if (pSearchType == SearchType.YourMatchesWebSearch || pSearchType == SearchType.MatchMail ||
                pPreferences["excludeynmnoflag"] == "1")
            {
                //non-e2 ynm filter, this is not needed if E2 ynm filter is enabled
                if (!isE2YnmNoFilterEnabled && pPreferences["excludeynmnoflag"] == "1")
                {
                    isYnmNoFilterEnabled = true;
                }
            }
            return isYnmNoFilterEnabled;
        }

        /// <summary>
        ///     If E2 YNM No filtering is enabled, get a list of NO member Ids and pass that in
        ///     for excludeynmmemberids search preference
        /// </summary>
        /// <param name="pPreferences"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pSiteID"></param>
        /// <param name="pMemberID"></param>
        /// <param name="logger"></param>
        /// <param name="isE2YNMNoFilterEnabled"></param>
        /// <param name="methodName"></param>
        /// <param name="stopwatch"></param>
        private void AddExcludeYnmMemberIdsSearchPreference(SearchPreferenceCollection pPreferences,
            int pCommunityID, int pSiteID, int pMemberID, ISparkLogger logger, bool isE2YNMNoFilterEnabled, string methodName,
            Stopwatch stopwatch)
        {
            if (!isE2YNMNoFilterEnabled) return;
            if (null != logger)
            {
                logger.LogInfoMessage(
                    string.Format(
                        "START: Method:{0}, Section:GetYNMNoMemberIDsForMember(pMemberID:{1}, pCommunityID:{2}, pSiteID:{3})",
                        methodName, pMemberID, pCommunityID, pSiteID), null);
                stopwatch.Restart();
            }
            StringBuilder sbYNMNoMemberIDs = ListFilterAccess.GetHotlistedMemberIDs(pCommunityID, pSiteID, pMemberID,
                HotListCategory.MembersClickedNo);

            if (sbYNMNoMemberIDs.Length > 0)
            {
                pPreferences.Add("excludeynmmemberids", sbYNMNoMemberIDs.ToString(0, sbYNMNoMemberIDs.Length - 1));
            }
            if (null != logger)
            {
                stopwatch.Stop();
                long ellapsedTimeInMillis3 = stopwatch.ElapsedMilliseconds;
                logger.LogInfoMessage(
                    string.Format(
                        "END: Method:{0}, Section:GetYNMNoMemberIDsForMember(pMemberID:{1}, pCommunityID:{2}, pSiteID:{3}), ElapsedTime:{4}ms",
                        methodName, pMemberID, pCommunityID, pSiteID, ellapsedTimeInMillis3), null);
            }
        }

        private void LogSearchPageViewedActivity(int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID,
            SearchType pSearchType, SearchEntryPoint pSearchEntryPoint, ISparkLogger logger, string methodName,
            Stopwatch stopwatch, MatchnetQueryResults queryResults)
        {
            //activity log for search page viewed
            if (pSearchType != SearchType.None && pSearchEntryPoint != SearchEntryPoint.None
                && IsActivityLogViewProfileEnabled(pCommunityID, pSiteID))
            {
                var callingSystem = CallingSystem.Web;

                //get page number
                var pageNum = (pStartRow/pPageSize) + 1;

                if (null != logger)
                {
                    logger.LogInfoMessage(string.Format("START: Method:{0}, Section:ActivityRecording", methodName), null);
                    stopwatch.Restart();
                }
                //get xml
                var xmlForLog = GetSearchPageViewedActivityLogXML(pSiteID, pCommunityID, pSearchEntryPoint,
                    queryResults, pSearchType, pageNum, pPageSize, pMemberID);

                //save to activity log                    
                MiscUtils.FireAndForget(
                    o =>
                        ActivityRecordingSA.Instance.RecordActivity(pMemberID, int.MinValue, pSiteID,
                            ActionType.SearchPageViewed, callingSystem, xmlForLog.ToString()),
                    "MemberSearchSA", "Error writing search to ActivityRecording");
                if (null != logger)
                {
                    stopwatch.Stop();
                    var ellapsedTimeInMillis25 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(
                        string.Format("END: Method:{0}, Section:ActivityRecording, ElapsedTime:{1}ms", methodName,
                            ellapsedTimeInMillis25), null);
                }
            }
        }

        public ArrayList SearchDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            if (pPreferences.ContainsKey("MemberId"))
            {
                int pMemberId = Convert.ToInt32(pPreferences["MemberId"]);
                if ((pMemberId != Constants.NULL_INT &&
                     Convert.ToBoolean(RuntimeSettings.GetSetting("E2_PROFILE_BLOCK_ENABLED", pCommunityID))))
                {
                    StringBuilder allBlockedMemberIdString = ListFilterAccess.GetBlockedMemberIDsForMember(pMemberId, pCommunityID, pSiteID, true, null);

                    if (allBlockedMemberIdString.Length > 0)
                    {
                        pPreferences.Add("blockedmemberids",
                                         allBlockedMemberIdString.ToString(0, allBlockedMemberIdString.Length - 1));
                    }

                }
            }

            string uri = getServiceManagerUri();
            return getService(uri).SearchDetailed(pPreferences, pCommunityID, pSiteID);
        }

        public ArrayList SearchExplainDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            string uri = getServiceManagerUri();
            return getService(uri).SearchExplainDetailed(pPreferences, pCommunityID, pSiteID);
        }

        public MatchnetQueryResults SearchForPushFlirtMatches(ISearchPreferences pPreferences, int memberID, int communityID, int siteID, int brandID, int numMatchesToReturn)
        {
            string uri = getServiceManagerUri();
            return (MatchnetQueryResults) getService(uri).SearchForPushFlirtMatches(pPreferences, memberID, communityID, siteID, brandID, numMatchesToReturn);
        }

        #endregion

        #region Public methods
        public StringBuilder GetSearchPageViewedActivityLogXML(int siteID, int communityID, SearchEntryPoint searchEntryPoint, MatchnetQueryResults results, SearchType searchType, int pageNumber, int resultsPerPage, int memberID)
        {
            bool isMatchRatingsCalculationEnabled = memberID > 0 && MatchRatingUtil.IsMatchRatingCalculationEnabled(communityID, searchType);
            StringBuilder xmlForLog = new StringBuilder("<SearchPageViewed>");
            xmlForLog.Append(string.Format("<SearchType>{0}</SearchType>", ((int)searchType).ToString()));
            xmlForLog.Append(string.Format("<SearchEntryPoint>{0}</SearchEntryPoint>", ((int)searchEntryPoint)).ToString());
            xmlForLog.Append(string.Format("<PageNumber>{0}</PageNumber>", pageNumber.ToString()));
            xmlForLog.Append(string.Format("<ResultsPerPage>{0}</ResultsPerPage>", resultsPerPage.ToString()));
            if (results != null && results.Items != null)
            {
                xmlForLog.Append(string.Format("<MatchesFound>{0}</MatchesFound>", results.Items.List.Count));
            }
            else
            {
                xmlForLog.Append("<MatchesFound>0</MatchesFound>");
            }

            if (results != null && results.Items != null)
            {
                xmlForLog.Append("<MemberResultSet>");
                foreach (IMatchnetResultItem result in results.Items.List)
                {
                    xmlForLog.Append("<Member>");
                    xmlForLog.Append(string.Format("<Id>{0}</Id>", result.MemberID));
                    if (isMatchRatingsCalculationEnabled)
                    {
                        xmlForLog.Append(string.Format("<Score>{0}</Score>", result.MatchScore));
                    }
                    xmlForLog.Append("</Member>");
                }
                xmlForLog.Append("</MemberResultSet>");
            }
            xmlForLog.Append(string.Format("<TimeStamp>{0}</TimeStamp>", DateTime.Now));
            xmlForLog.Append("</SearchPageViewed>");
            return xmlForLog;
        }

        
        #endregion

        #region Private methods

        /// <summary>
        /// Executes a search using a remoting connection to the SearchService.
        /// </summary>
        /// <param name="pPreferences"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pSiteID"></param>
        /// <param name="pSearchEngineType">Relational or FAST search.</param>
        /// <param name="ignoreMTCache"></param>
        /// <returns></returns>
        private MatchnetQueryResults ExecuteSearch(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, SearchEngineType pSearchEngineType, bool ignoreMTCache)
        {
            string uri = null;
            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    if (ignoreMTCache)
                    {
                        return (MatchnetQueryResults)getService(uri).Search(pPreferences, pCommunityID, pSiteID, pSearchEngineType, ignoreMTCache);
                    }
                    else
                    {
                        return (MatchnetQueryResults)getService(uri).Search(pPreferences, pCommunityID, pSiteID, pSearchEngineType);
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch(ExceptionBase ex)
            {
                throw(new SAException("Error occurred while attempting to execute member search. (uri: " + uri + ")", ex));
            } 
            catch(Exception ex)
            {
                throw(new SAException("Error occurred while attempting to execute member search. (uri: " + uri + ")", ex));
            } 
        }


        private void CacheResults(MatchnetQueryResults pQueryResults)
        {
            pQueryResults.CachePriority = Matchnet.CacheItemPriorityLevel.Normal;
            pQueryResults.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_CACHE_TTL_SA"));
            _cache.Insert(pQueryResults);
        }

        private IMemberSearchService getService(string uri)
        {
            try
            {
                return (IMemberSearchService)Activator.GetObject(typeof(IMemberSearchService), uri);
            }
            catch(Exception ex)
            {
                throw(new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstant, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceManagerName);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch(Exception ex)
            {
                throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
            }
        }

        private string GetMemberSpecificCacheKey(string pCacheKey, int pMemberID)
        {
            return pMemberID.ToString() + "_cacheVer" + ((int)CacheMemberResultsVersion).ToString() + "_" + pCacheKey;
        }

        private bool IsActivityLogViewProfileEnabled(int communityID, int siteID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCHPAGEVIEWED_ACTIVITYLOG", communityID, siteID, Constants.NULL_INT);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        #endregion
    }
}
