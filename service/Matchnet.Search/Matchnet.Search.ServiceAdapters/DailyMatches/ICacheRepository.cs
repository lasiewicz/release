﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Search.ValueObjects.DailyMatches;

#endregion

namespace Matchnet.Search.ServiceAdapters.DailyMatches
{
    /// <summary>
    ///     Cache access class for Daily Matches
    ///     Allows switching to a different caching provider like the client for Couchbase
    /// </summary>
    public interface ICacheRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        T Get<T>(string cacheKey);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheableObject"></param>
        void Insert(ICacheable cacheableObject);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        void Delete(string cacheKey);
    }
}