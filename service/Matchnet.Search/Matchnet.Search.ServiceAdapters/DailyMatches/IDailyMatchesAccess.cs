#region

using System.Collections;
using System.Collections.Generic;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.DailyMatches;

#endregion

namespace Matchnet.Search.ServiceAdapters.DailyMatches
{
    /// <summary>
    ///     Data access class for Daily Matches
    /// </summary>
    public interface IDailyMatchesAccess
    {
        /// <summary>
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="queryResults"></param>
        /// <returns></returns>
        DailyMatchesResults CreateDailyMatches(int communityId, int memberId, MatchnetQueryResults queryResults);

        /// <summary>
        ///     Returns profiles used for the day
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        DailyMatchesResults GetDailyMatches(int communityId, int memberId);

        /// <summary>
        /// </summary>
        /// <param name="dailyMatchesResults></param>
        /// <returns></returns>
        MatchnetQueryResults GetDailyMatchnetQueryResults(DailyMatchesResults dailyMatchesResults);

        /// <summary>
        ///     Saves profiles used for the day
        /// </summary>
        /// <param name="results>
        ///     returns the saved object that's been filtered. this is to save an extra trip to call GET right
        ///     after saving
        /// </param>
        void SaveDailyMatches(DailyMatchesResults results);

        /// <summary>
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        void RemoveDailyMatches(int communityId, int memberId);

        /// <summary>
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="queryResults"></param>
        /// <returns></returns>
        DailyMatchesScrubList CreateScrubList(int communityId, int memberId);

        /// <summary>
        ///     Returns the member Ids in the scrub list
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        DailyMatchesScrubList GetScrubList(int communityId, int memberId);

        /// <summary>
        /// </summary>
        /// <param name="result"></param>
        /// <param name="isAppend"></param>
        void SaveScrubList(DailyMatchesScrubList result, bool isAppend);

        /// <summary>
        ///     Saves the scrub list with new Ids appended
        /// </summary>
        /// <param name="list"></param>
        /// <param name="appendDailyMatchesResult"></param>
        DailyMatchesScrubList AppendToScrubList(DailyMatchesScrubList list,
            System.Collections.Generic.List<IMatchnetResultItem> appendDailyMatchesResult);

        /// <summary>
        ///     Purges the scrub list
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        void RemoveScrubList(int communityId, int memberId);

        /// <summary>
        ///     Filter the list using these business rules
        ///     1. Use the DB setting to select top
        ///     2. Exclude ones in the scrub list
        /// </summary>
        /// <param name="results></param>
        /// <param name="scrubList"></param>
        List<MatchnetResultItem> ExcludeScrubMembers(DailyMatchesResults results, DailyMatchesScrubList scrubList);

        /// <summary>
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="result"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        MatchnetQueryResults ExcludeYnmNoMembers(int communityId, int memberId, MatchnetQueryResults result);
    }
}