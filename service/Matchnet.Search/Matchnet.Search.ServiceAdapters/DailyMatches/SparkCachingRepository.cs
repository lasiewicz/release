﻿#region

#endregion

#region



#endregion

namespace Matchnet.Search.ServiceAdapters.DailyMatches
{
    /// <summary>
    ///     Spark Caching implementation of ICacheRepository
    ///     Communiates with Memcached directly
    /// </summary>
    public class SparkCachingRepository : ICacheRepository
    {
        /// <summary>
        /// </summary>
        public const string SearchResultsBucketName = "searchresults";

        private readonly ICaching _instance;

        /// <summary>
        ///     For DI. Using JSON serialization due to seeing only ArrayList support in Distributed Caching.
        ///     was having weird enyim error(magic number?) in spark.caching so switched back to Distributed Caching
        /// </summary>
        /// <param name="instance"></param>
        public SparkCachingRepository(ICaching instance)
        {
            _instance = instance;
        }

        /// <summary>
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Get<T>(string cacheKey)
        {
            var data = _instance.Get(cacheKey);
            return data is T ? (T) data : default(T);
        }

        public void Insert(ICacheable cacheableObject)
        {
            _instance.Insert(cacheableObject);
        }

        public void Delete(string cacheKey)
        {
            _instance.Remove(cacheKey);
        }
    }
}