﻿#region

#endregion

#region

using System;
using Matchnet.DistributedCaching.ServiceAdapters;
using Newtonsoft.Json;

#endregion

namespace Matchnet.Search.ServiceAdapters.DailyMatches
{
    /// <summary>
    ///     Spark Caching implementation of IDailyMatchesCacheRepository
    ///     Communiates with Memcached directly
    /// </summary>
    public class DailyMatchesCacheRepository : IDailyMatchesCacheRepository
    {
        /// <summary>
        /// </summary>
        public const string SearchResultsBucketName = "searchresults";

        private readonly DistributedCachingSA _instance;

        /// <summary>
        ///     For DI. Using JSON serialization due to seeing only ArrayList support in Distributed Caching.
        ///     was having weird enyim error(magic number?) in spark.caching so switched back to Distributed Caching
        /// </summary>
        /// <param name="instance"></param>
        public DailyMatchesCacheRepository(DistributedCachingSA instance)
        {
            _instance = instance;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Get<T>(string cacheKey)
        {
            var data = _instance.Get(cacheKey, SearchResultsBucketName) as string;
            var deserialized = default(T);
            if (!string.IsNullOrEmpty(data))
                deserialized = JsonConvert.DeserializeObject<T>(data);
            return deserialized;
        }

        public void Put<T>(T data, string cacheKey, int cacheTtlSeconds)
        {
            // need to override the cache key, we do not want to use ICacheable's cache key for Daily Matches
            var serialized = JsonConvert.SerializeObject(data, Formatting.None);
            _instance.Put(cacheKey, serialized, SearchResultsBucketName, cacheTtlSeconds);
        }

        public void Delete(string cacheKey)
        {
            _instance.Remove(cacheKey, SearchResultsBucketName);
        }
    }
}