﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters.Utils;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.DailyMatches;

#endregion

namespace Matchnet.Search.ServiceAdapters.DailyMatches
{
    /// <summary>
    ///     Data access class to faciliate Daily Matches
    ///     Refer to CONS-3641 and its own documentation on Confluence
    ///     https://confluence.matchnet.com/display/SOFTENG/LA+API+-+Search+-+Daily+Matches
    ///     todo - introduce an Interface for DI and have another helper for calling DistributedCaching
    /// </summary>
    public class DailyMatchesAccess : IDailyMatchesAccess
    {
        private readonly ICacheRepository _cacheRepository;

        /// <summary>
        /// </summary>
        public DailyMatchesAccess(ICaching cache)
        {
            _cacheRepository = new SparkCachingRepository(cache);
        }

        /// <summary>
        ///     Creates a new DailyMatchesResults object with custom TTL minutes
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="queryResults"></param>
        /// <returns></returns>
        public DailyMatchesResults CreateDailyMatches(int communityId, int memberId, MatchnetQueryResults queryResults)
        {
            // calculate how much is left in the day, this is to enforce the midnight custom rule
            // todo: change all seconds variables to use double instead of int? will need to change all the way through into DistributedCaching
            var secondsLeftToday = (int) (DateTime.Now.AddDays(1).Date - DateTime.Now).TotalSeconds;

            // retrieve the database setting for how many minutes to cache 
            var dbSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SEARCH_DAILY_MATCHES_MINUTES", communityId))*60;

            // if the db setting is higher than what's left of today, take the shorter time frame
            // to ensure mid night expiration
            var cacheTtlSeconds = (dbSeconds > secondsLeftToday) ? secondsLeftToday : dbSeconds;

            var dailyMatchesResult = new DailyMatchesResults(communityId, memberId, cacheTtlSeconds, queryResults.Items);

            return dailyMatchesResult;
        }

        /// <summary>
        ///     Returns profiles used for the day. Only returns what's in cache.
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public DailyMatchesResults GetDailyMatches(int communityId, int memberId)
        {
            // anything in cache?
            var cachedResults =
                _cacheRepository.Get<DailyMatchesResults>(DailyMatchesResults.GetCacheKey(communityId,
                    memberId));

            // as long as it's returned, it has not expired
            if (cachedResults == null) return null;

            //Should avoid ArrayList: Though its bad to convert List<int> to ArrayList, PageQueryResults accepts ArrayList
            var newList = new List<int[]>(cachedResults.Items.Count);
            for (var i = 0; i < cachedResults.Items.Count; i++)
            {
                newList.Add(new[] {cachedResults.Items[i].MemberID, cachedResults.Items[i].MatchScore});
            }
            var arrayList = new ArrayList(newList);
            var queryResults = PagingUtil.PageQueryResults(arrayList, 0, arrayList.Count);

            // remove any YNM No members
            var noFilteredResult = ExcludeYnmNoMembers(communityId, memberId, queryResults);

            var dailyMatchesResults = CreateDailyMatches(communityId, memberId, noFilteredResult);

            return dailyMatchesResults;
        }

        /// <summary>
        /// </summary>
        /// <param name="dailyMatchesResults>
        /// </param>
        /// <returns></returns>
        public MatchnetQueryResults GetDailyMatchnetQueryResults(DailyMatchesResults dailyMatchesResults)
        {
            return PagingUtil.PageQueryResults(dailyMatchesResults, 0, dailyMatchesResults.Items.Count);
        }

        /// <summary>
        ///     Saves profiles used for the day with custom filtering
        ///     1. Removes matching scrubbed members
        ///     2. Update the scrub list with the current results
        /// </summary>
        /// <param name="results">
        ///     returns the saved object that's been filtered.
        ///     this is to save an extra trip to call GET right after saving
        /// </param>
        public void SaveDailyMatches(DailyMatchesResults results)
        {
            var isScrubListAppend = true;

            var scrubList = GetScrubList(results.CommunityId, results.MemberId);

            if (scrubList == null)
            {
                scrubList = CreateScrubList(results.CommunityId, results.MemberId);
                isScrubListAppend = false;
            }

            // filter out any matching scrub member in the current results
            var scrubbedResult = ExcludeScrubMembers(results, scrubList);

            // update the scrub list so the current results can be also scrubbed next time 
            var appendedScrubList = AppendToScrubList(scrubList,
                scrubbedResult.Cast<IMatchnetResultItem>().ToList());

            SaveScrubList(appendedScrubList, isScrubListAppend);

            var resultItems = new MatchnetResultItems(scrubbedResult.Count);
            foreach (var item in scrubbedResult)
            {
                resultItems.AddResult(new MatchnetResultItem(item.MemberID, item.MatchScore));
            }
            results.Items = resultItems;

            _cacheRepository.Insert(results);
        }

        /// <summary>
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        public void RemoveDailyMatches(int communityId, int memberId)
        {
            _cacheRepository.Delete(DailyMatchesResults.GetCacheKey(communityId,
                memberId));
        }

        /// <summary>
        ///     Create an empty scrub list
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public DailyMatchesScrubList CreateScrubList(int communityId, int memberId)
        {
            var cacheTtlSeconds =
                Convert.ToInt32(RuntimeSettings.GetSetting("SEARCH_DAILY_MATCHES_SCRUB_THRESHOLD_MINUTES", communityId))*
                60;

            return new DailyMatchesScrubList(communityId, memberId, cacheTtlSeconds, new HashSet<int>(), DateTime.Now);
        }

        /// <summary>
        ///     Returns the scrub list in cache
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        public DailyMatchesScrubList GetScrubList(int communityId, int memberId)
        {
            if (communityId <= 0) throw new ArgumentException("communityId");
            if (memberId <= 0) throw new ArgumentException("memberId");

            return
                _cacheRepository.Get<DailyMatchesScrubList>(DailyMatchesScrubList.GetCacheKey(communityId,
                    memberId));
        }

        public void SaveScrubList(DailyMatchesScrubList result, bool isAppend)
        {
            if (result.MemberId <= 0) throw new ArgumentException("communityId");
            if (result.CommunityId <= 0) throw new ArgumentException("memberId");

            // two types of saves here, new or updating an existing scrub list
            // 8:30 expire in 20 minutes, 8:50(Insert + TTL)
            // 8:40 expire in 10 minutes, 8:50(Insert + TTL)
            // update the TTL with 10 minutes
            if (isAppend)
            {
                // figure out how much time is left from the original TTL
                result.CacheTTLSeconds = (result.InsertDateTime.AddSeconds(result.CacheTTLSeconds) -  DateTime.Now).Seconds;

                // could we run into this condition? 
                // todo: does Couchbase immediately expire with a <= 0 TTL?
                if (result.CacheTTLSeconds <= 0)
                {
                    result.MemberIds = new HashSet<int>();
                }
            }

            _cacheRepository.Insert(result);
        }

        /// <summary>
        ///     Removes the scrub list in cache
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        public void RemoveScrubList(int communityId, int memberId)
        {
            _cacheRepository.Delete(DailyMatchesScrubList.GetCacheKey(communityId, memberId));
        }

        /// <summary>
        ///     Filter the list using these business rules
        ///     1. Exclude ones in the scrub list
        ///     2. Use the DB setting to select top
        /// </summary>
        /// <param name="results"></param>
        /// <param name="scrubList"></param>
        public List<MatchnetResultItem> ExcludeScrubMembers(DailyMatchesResults results, DailyMatchesScrubList scrubList)
        {
            if (results == null) throw new Exception("DailyMatchesResults cannot be null");

            if (scrubList == null)
                scrubList = CreateScrubList(results.CommunityId, results.MemberId);

            var dailyMatchesCount =
                Convert.ToInt32(RuntimeSettings.GetSetting("SEARCH_DAILY_MATCHES_DAILY_PROFILES_COUNT",
                    results.CommunityId));

            var filteredList = ListFilterAccess.RemoveScrubbedMembersInQueryResults(results, scrubList);

            var finalList = filteredList.Take(dailyMatchesCount).ToList();

            return finalList;
        }

        /// <summary>
        ///     Apart from E2 filtering internally and the SA filtering internally
        ///     We need to be able to filter out No members in real time
        ///     Reusing the logic already used by the SA
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="queryResults"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public MatchnetQueryResults ExcludeYnmNoMembers(int communityId, int memberId, MatchnetQueryResults queryResults)
        {
            var filteredList = ListFilterAccess.RemoveYnmNoMembers(communityId, memberId, queryResults);

            var pagedList = PagingUtil.PageQueryResults(filteredList, 0, filteredList.Count);

            return pagedList;
        }

        /// <summary>
        ///     Updates the scrub list with new Ids appended
        /// </summary>
        /// <param name="scrubList">member's scrub list</param>
        /// <param name="appendDailyMatchesResult">new daily matches to append to the scrub list</param>
        public DailyMatchesScrubList AppendToScrubList(DailyMatchesScrubList scrubList,
            List<IMatchnetResultItem> appendDailyMatchesResult)
        {
            // append a member Id if the scrub list doesn't already have it
            foreach (var matchnetResultItem in appendDailyMatchesResult)
            {
                // this hash set will only add unique Ids
                scrubList.MemberIds.Add(matchnetResultItem.MemberID);
            }

            return scrubList;
        }
    }
}