using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet;
using Matchnet.Caching;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Search.ServiceAdapters
{
	/// <summary>
	/// Summary description for FASTQueryTransformer.
	/// 
	/// This utility class assists the MemberSearch service adapter in 
	/// 
	/// </summary>
	public class FASTQueryTransformer
	{

		private FASTQueryTransformer()
		{
		}

        public static bool GetSetting(string name, bool defaultvalue)
        {
            try
            {
                return Conversion.CBool(RuntimeSettings.GetSetting(name), defaultvalue);

            }
            catch (Exception ex)
            {
                return defaultvalue;
            }
        }

        public static bool GetSetting(string name, int communityid, bool defaultvalue)
        {
            try
            {
                return Conversion.CBool(RuntimeSettings.GetSetting(name, communityid), defaultvalue);

            }
            catch (Exception ex)
            {
                return defaultvalue;
            }
        }

        public static int GetSetting(string name, int communityid, int defaultvalue)
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.GetSetting(name, communityid), defaultvalue);

            }
            catch (Exception ex)
            {
                return defaultvalue;
            }
        }

		/// <summary>
		/// Transforms a collection of search preferences into the correct set of elements
		/// based on the SearchTypeID (i.e. PostalCode, AreaCode, etc.) in the search 
		/// preferences collection.  Un-necessary preferences will be removed from the
		/// collection.
		/// </summary>
        /// <param name="pSearchPreferences"></param>
        /// <param name="pCommunityId"></param>
        public static void TransformSearchPreferences(SearchPreferenceCollection pSearchPreferences, int pCommunityId)
		{
			SearchTypeID searchTypeID = (SearchTypeID)Convert.ToInt32(pSearchPreferences["SearchTypeID"]);

			switch (searchTypeID)
			{
				case SearchTypeID.AreaCode:
					transformAreaCodeSearch(pSearchPreferences);
					break;
				case SearchTypeID.College:
					transformCollegeSearch(pSearchPreferences);
					break;
				case SearchTypeID.PostalCode:
					transformPostalCodeSearch(pSearchPreferences);
					break;
				case SearchTypeID.Region:
					transformRegionSearch(pSearchPreferences);
					break;
				default:
					throw new Exception("Invalid SearchTypeID supplied for Member Search.");
			}
			
			// Transform Distance into GeoDistance values.            
            if (!string.IsNullOrEmpty(pSearchPreferences.Value("Distance")))
			{
                int distance = Convert.ToInt32(pSearchPreferences.Value("Distance"));
                string distanceAttribute = (GetSetting("USE_E2_DISTANCE", pCommunityId, false)) ? "E2Distance" : "Distance";
                if (GetSetting("USE_MINGLE_DISTANCE", pCommunityId, false))
                {
                    distanceAttribute = "MingleDistance";
                }

                //grab distance options from db (try cache first)
                CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(distanceAttribute, Constants.NULL_INT)) as CacheAttributeOptionCollection;
                if (null == cachedOptionCollection)
                {
                    AttributeOptionCollection options = AttributeOptionSA.Instance.GetAttributeOptionCollection(distanceAttribute, Constants.NULL_INT);
                    cachedOptionCollection = new CacheAttributeOptionCollection(options, distanceAttribute, Constants.NULL_INT);
                    Cache.Instance.Add(cachedOptionCollection);
                }
                
                int defaultListOrder = GetSetting("DEFAULT_DISTANCE_LIST_ORDER", pCommunityId, 3);
                bool foundValue = false;
                foreach (AttributeOption option in cachedOptionCollection.OptionCollection)
                {
                    if (option.Value == distance)
                    {
                        pSearchPreferences["GeoDistance"] = option.ListOrder.ToString();
                        foundValue = true;
                        break;
                    }
                }
                //if value not found, use default
                if (!foundValue)
                {
                    pSearchPreferences["GeoDistance"] = defaultListOrder.ToString();
                }
			}
		}

		private static void transformAreaCodeSearch(SearchPreferenceCollection pSearchPreferences)
		{
			// If the CountryRegionID is Isreal we have to do something special
			int countryRegionID = Conversion.CInt(pSearchPreferences["CountryRegionID"]);

			if (countryRegionID == 105)
			{
				transformIsraelAreaCodeSearch(pSearchPreferences);
			}
			else
			{
				// Collaps the six individual area code fields into a single comma-delimeted string.
				StringBuilder sb = new StringBuilder();
				string areaCode;
				for (int i = 1; i < 7; i++)
				{
					areaCode = pSearchPreferences["AreaCode" + i];
					if (areaCode != null)
					{
						if (areaCode.Trim() != string.Empty)
						{
							sb.Append(areaCode.Trim() + ",");
						}
					}
				}
				resetSearchPreferences(pSearchPreferences);
				pSearchPreferences["AreaCodes"] = sb.ToString();

			}
		}

		private static void transformIsraelAreaCodeSearch(SearchPreferenceCollection pSearchPreferences)
		{
			// pull back the area code options for Israel
			AttributeOptionCollection israelAreaCodes =				
				AttributeOptionSA.Instance.GetAttributeOptionCollection("IsraelAreaCode", 10, 15, 1015);			

			StringBuilder sb = new StringBuilder();
			string areaCode = string.Empty;

			for (int i = 1; i < 7; i++)
			{
				// for each area code, concat the string of Areacode OptionIDs
				areaCode = pSearchPreferences["AreaCode" + i];
				if ((areaCode != null) && (areaCode.Trim() != string.Empty))
				{
					areaCode = getAttributeOptionID(israelAreaCodes, areaCode);
					if (areaCode != null)
					{
						sb.Append(areaCode + ",");
					}
				}
			}
			resetSearchPreferences(pSearchPreferences);
			pSearchPreferences["AreaCodes"] = sb.ToString();
		}

		private static string getAttributeOptionID(AttributeOptionCollection pAttributes, string pAttributeValue)
		{
			AttributeOption option = null;
			IEnumerator attributeEnumerator = pAttributes.GetEnumerator();
			while (attributeEnumerator.MoveNext())
			{
				option = (AttributeOption)attributeEnumerator.Current;
				int iOptionDescription=Conversion.CInt(option.Description,Constants.NULL_INT);
				int iAttributeValue=Conversion.CInt(pAttributeValue,Constants.NULL_INT);
				if(iAttributeValue!=Constants.NULL_INT && iOptionDescription!=Constants.NULL_INT)
				{
					 if ( iOptionDescription == iAttributeValue)
					 {
						 if (option.Value < 6)
						 {	// Only valid IsralAreaCode AttributeOptionID's are 1-5.
							 return option.Value.ToString();
						 }
						 return null;	// invalid option.Value
					 }
				}
			}
			return null;
		}

		private static void transformCollegeSearch(SearchPreferenceCollection pSearchPreferences)
		{
			string schoolID = pSearchPreferences.Value("SchoolID");
			resetSearchPreferences(pSearchPreferences);
			pSearchPreferences["SchoolID"] = schoolID;
		}

		private static void transformPostalCodeSearch(SearchPreferenceCollection pSearchPreferences)
		{
			// currently just a pass-through, should eventually move the resolution of RegionID
			// based on postal code value into the SA layer (done in the web-tier currently).
			transformRegionSearch(pSearchPreferences);
		}

		private static void transformRegionSearch(SearchPreferenceCollection pSearchPreferences)
		{
            int regionID = (pSearchPreferences.ContainsKey("preferredregionid")) ? Conversion.CInt(pSearchPreferences["preferredregionid"]) : Conversion.CInt(pSearchPreferences["RegionID"]);
			resetSearchPreferences(pSearchPreferences);

			RegionLanguage regionLanguage = null;

			if (regionID != Constants.NULL_INT)
			{
				regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, 2);
			}

			if (regionLanguage != null)
			{
				pSearchPreferences["CountryRegionID"] = regionLanguage.CountryRegionID.ToString();
				pSearchPreferences["RegionIDCity"] = regionLanguage.CityRegionID.ToString();
				pSearchPreferences["Latitude"] = regionLanguage.Latitude.ToString();
				pSearchPreferences["Longitude"] = regionLanguage.Longitude.ToString();
			}
			else
			{
				throw new Exception("Unable to determine: CountryRegionID, CityRegionID, Latitude and Longitude from RegionID: " + regionID);
			}
		}

		/// <summary>
		/// Use this method to reset all critical search preferences before populating those that
		/// are appropriate for the selected SearchTypeID.
		/// </summary>
		/// <param name="pSearchPreferences"></param>
		private static void resetSearchPreferences(SearchPreferenceCollection pSearchPreferences)
		{
			//Updating these search preferences to null values are causing problems
			//with previous prefs being lost.
			// remove area codes
			pSearchPreferences["AreaCodes"] = null;

			// remove long, lat preferences.
			pSearchPreferences["RegionIDCity"] = null;
			pSearchPreferences["Longitude"] = null;
			pSearchPreferences["Latitude"] = null;

			// remove SchoolID
			pSearchPreferences["SchoolID"] = null;
		}
	}

    public class CacheAttributeOptionCollection : ICacheable
    {
        private AttributeOptionCollection _optionCollection = null;
        private int _communityId;
        private string _attributeName;
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
        const string CACHEKEYFORMAT = "AttributeOption-{0}-{1}";

        public AttributeOptionCollection OptionCollection
        {
            get { return _optionCollection; }
            set { _optionCollection = value; }
        }

        public int CommunityId
        {
            get { return _communityId; }
            set { _communityId = value; }
        }

        public string AttributeName
        {
            get { return _attributeName; }
            set { _attributeName = value; }
        }

        public CacheAttributeOptionCollection(AttributeOptionCollection collection, string attributeName, int commmunityId)
        {
            this._communityId = commmunityId;
            this._attributeName = attributeName;
            this._optionCollection = collection;
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_attributeName, _communityId);
        }

        public static string GetCacheKeyString(string attributeName, int communityId)
        {
            return String.Format(CACHEKEYFORMAT, attributeName, communityId);
        }


        #endregion
    }
}
