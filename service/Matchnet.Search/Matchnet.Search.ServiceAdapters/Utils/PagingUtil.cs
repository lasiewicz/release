﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;

#endregion

namespace Matchnet.Search.ServiceAdapters.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public class PagingUtil
    {
        /// <summary>
        /// This method will return a subset (page) of query results based on the starting row/index and desired page size.
        /// </summary>
        /// <param name="pQueryResults"></param>
        /// <param name="pStartRow"></param>
        /// <param name="pPageSize"></param>
        /// <returns></returns>
        public static MatchnetQueryResults PageQueryResults(IMatchnetQueryResults pQueryResults, int pStartRow, int pPageSize)
        {
            MatchnetQueryResults pagedResults;

            if (pStartRow > pQueryResults.Items.Count)
            {
                pStartRow = pQueryResults.Items.Count - 1;
                pPageSize = 0;
            }

            if (pStartRow < 0)
                pStartRow = 0;
            if (pPageSize < 0)
                pPageSize = 1;

            // Determine the exact number of results for this page.
            if ((pStartRow + pPageSize) <= pQueryResults.MatchesReturned)
            {
                pagedResults = new MatchnetQueryResults(pPageSize);
            }
            else
            {	// Requested page size is too large for the number of results remaining in the master listing.
                pagedResults = new MatchnetQueryResults(pQueryResults.MatchesReturned - pStartRow);
            }

            int nextPage = pStartRow + pagedResults.Items.Count;
    
            while (pStartRow < nextPage)
            {
                pagedResults.AddResult(pQueryResults.Items[pStartRow]);
                pStartRow++;
            }
            pagedResults.MatchesFound = pQueryResults.MatchesFound;	// Used for setting up the chapter/paging views.
            pagedResults.MoreResultsAvailable = (nextPage < pQueryResults.MatchesReturned);
            return pagedResults;

        }

        /// <summary>
        /// This method will return a subset (page) of query results based on the starting row/index and desired page size.
        /// </summary>
        /// <param name="pQueryResults"></param>
        /// <param name="pStartRow"></param>
        /// <param name="pPageSize"></param>
        /// <returns></returns>
        public static MatchnetQueryResults PageQueryResults(ArrayList pQueryResults, int pStartRow, int pPageSize)
        {
            MatchnetQueryResults pagedResults;

            if (pStartRow > pQueryResults.Count)
            {
                pStartRow = pQueryResults.Count - 1;
                pPageSize = 0;
            }

            if (pStartRow < 0)
                pStartRow = 0;
            if (pPageSize < 0)
                pPageSize = 1;

            // Determine the exact number of results for this page.
            if ((pStartRow + pPageSize) <= pQueryResults.Count)
            {
                pagedResults = new MatchnetQueryResults(pPageSize);
            }
            else
            {	// Requested page size is too large for the number of results remaining in the master listing.
                pagedResults = new MatchnetQueryResults(pQueryResults.Count - pStartRow);
            }
            int nextPage = pStartRow + pagedResults.Items.Count;

            while (pStartRow < nextPage)
            {
                //arraylist of array[memberID, matchscore]
                int[] arrayItem = pQueryResults[pStartRow] as int[];
                if (arrayItem != null) pagedResults.AddResult(arrayItem[0], arrayItem[1]);

                pStartRow++;
            }
            pagedResults.MatchesFound = pQueryResults.Count;	// Used for setting up the chapter/paging views.
            pagedResults.MoreResultsAvailable = (nextPage < pQueryResults.Count);
            return pagedResults;
        }
    }
}
