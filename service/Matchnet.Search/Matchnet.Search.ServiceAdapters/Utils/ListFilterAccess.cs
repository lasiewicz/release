﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.DailyMatches;
using Spark.Logger;

namespace Matchnet.Search.ServiceAdapters.Utils
{
    /// <summary>
    ///     todo:Refactor futher to use DI
    /// </summary>
    public class ListFilterAccess
    {
        private ListSA _listAccess;

        /// <summary>
        /// </summary>
        /// <param name="listAccess"></param>
        public ListFilterAccess(ListSA listAccess)
        {
            _listAccess = listAccess;
        }

        /// <summary>
        /// </summary>
        /// <param name="pQueryResults"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pMemberID"></param>
        /// <param name="hotlistCategory"></param>
        /// <returns></returns>
        public static ArrayList RemoveHotlistedMembers(IMatchnetQueryResults pQueryResults, int pCommunityID,
            int pMemberID, HotListCategory hotlistCategory)
        {
            var filteredList = new ArrayList();
            var list = ListSA.Instance.GetList(pMemberID);

            if (list == null || pQueryResults == null) return filteredList;

            for (var i = 0; i < pQueryResults.Items.Count; i++)
            {
                if (!list.IsHotListed(hotlistCategory, pCommunityID, pQueryResults.Items[i].MemberID))
                {
                    filteredList.Add(new[] {pQueryResults.Items[i].MemberID, pQueryResults.Items[i].MatchScore});
                }
            }

            return filteredList;
        }

        /// <summary>
        ///     Removes matching members in queryResults and returns
        /// </summary>
        /// <param name="queryResults"></param>
        /// <param name="scrubList"></param>
        /// <returns></returns>
        public static List<MatchnetResultItem> RemoveScrubbedMembersInQueryResults(DailyMatchesResults queryResults, DailyMatchesScrubList scrubList)
        {
            if (queryResults == null) throw new ArgumentNullException("queryResults");
            if (scrubList == null) throw new ArgumentNullException("scrubList");

            var filteredResults = new List<MatchnetResultItem>();
            for (var i = 0; i < queryResults.Items.Count; i++)
            {
                // this member is not in the scrub list, add to the response
                if (!scrubList.MemberIds.Contains(queryResults.Items[i].MemberID))
                {
                    filteredResults.Add(new MatchnetResultItem(queryResults.Items[i].MemberID,
                        queryResults.Items[i].MatchScore));
                }
            }

            return filteredResults;
        }

        public static ArrayList RemoveExludedMembers(IMatchnetQueryResults pQueryResults, int pCommunityID, int pMemberID)
        {
            return (ListFilterAccess.RemoveHotlistedMembers(pQueryResults, pCommunityID, pMemberID, HotListCategory.ExcludeListInternal));
        }

        public static ArrayList RemoveHotlistedMembers(IMatchnetQueryResults pQueryResults, int pCommunityID, int pMemberID, List<HotListCategory> hotlistCategoryList)
        {
            ArrayList filteredList = new ArrayList();
            List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);

            if (list != null && pQueryResults != null)
            {
                for (int i = 0; i < pQueryResults.Items.Count; i++)
                {
                    bool hotlisted = false;
                    foreach (HotListCategory hotlistCategory in hotlistCategoryList)
                    {
                        if (list.IsHotListed(hotlistCategory, pCommunityID, pQueryResults.Items[i].MemberID))
                        {
                            hotlisted = true;
                            break;
                        }
                    }

                    if (!hotlisted)
                    {
                        filteredList.Add(new int[] { pQueryResults.Items[i].MemberID, pQueryResults.Items[i].MatchScore });
                    }
                }
            }

            return filteredList;
        }

        public static ArrayList RemoveHotlistedMembers(ArrayList pQueryResults, int pCommunityID, int pMemberID, HotListCategory hotlistCategory)
        {
            ArrayList filteredList = new ArrayList();
            List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);

            if (list != null && pQueryResults != null)
            {
                for (int i = 0; i < pQueryResults.Count; i++)
                {
                    //arraylist of array[memberID, matchscore]
                    int[] arrayItem = pQueryResults[i] as int[];
                    if (arrayItem != null)
                    {
                        if (!list.IsHotListed(hotlistCategory, pCommunityID, arrayItem[0]))
                        {
                            filteredList.Add(arrayItem);
                        }
                    }
                }
            }

            return filteredList;
        }

        public static ArrayList RemoveHotlistedMembers(ArrayList pQueryResults, int pCommunityID, int pMemberID, List<HotListCategory> hotlistCategoryList)
        {
            ArrayList filteredList = new ArrayList();
            List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);

            if (list != null && pQueryResults != null)
            {
                for (int i = 0; i < pQueryResults.Count; i++)
                {
                    //arraylist of array[memberID, matchscore]
                    int[] arrayItem = pQueryResults[i] as int[];
                    if (arrayItem != null)
                    {
                        bool hotlisted = false;
                        foreach (HotListCategory hotlistCategory in hotlistCategoryList)
                        {
                            if (list.IsHotListed(hotlistCategory, pCommunityID, arrayItem[0]))
                            {
                                hotlisted = true;
                                break;
                            }
                        }

                        if (!hotlisted)
                        {
                            filteredList.Add(arrayItem);
                        }
                    }
                }
            }

            return filteredList;
        }

        public static StringBuilder GetHotlistedMemberIDs(int pCommunityID, int pSiteID, int pMemberID, HotListCategory hotlistCategory)
        {
            StringBuilder sbMemberIDs = new StringBuilder();
            if (pMemberID > 0)
            {
                List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);

                if (list != null)
                {
                    int totalrows = 0;
                    ArrayList listMemberIDs = list.GetListMembers(hotlistCategory, pCommunityID, pSiteID, 1, 1000, out totalrows);
                    for (Int32 num = 0; num < listMemberIDs.Count; num++)
                    {
                        sbMemberIDs.Append(listMemberIDs[num].ToString() + MemberSearchSA.CommaSeparator);
                    }
                    
                }
            }

            return sbMemberIDs;
        }

        public static List<int> convertToGeneric(ArrayList list)
        {
            List<int> converted = new List<int>();
            if (null != list && list.Count > 0)
            {
                foreach (int i in list)
                {
                    converted.Add(i);
                }
            }
            return converted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pMemberID"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pSiteID"></param>
        /// <returns> comma seperated string of ids. empty string is return if no blocked memebers ids are found. </returns>
        public static StringBuilder GetBlockedMemberIDsForMember(int pMemberID, int pCommunityID, int pSiteID)
        {
            return GetBlockedMemberIDsForMember(pMemberID, pCommunityID, pSiteID, true, null);
        }

        public static StringBuilder GetBlockedMemberIDsForMember(int pMemberID, int pCommunityID, int pSiteID, bool useParallel,
            ISparkLogger logger)
        {
            Stopwatch stopwatch = new Stopwatch();
            string methodName = "GetBlockedMemberIDsForMember";
            var allBlockedMemberIdString = new StringBuilder();
            if (pMemberID > 0)
            {
                if (null != logger)
                {
                    logger.LogInfoMessage(String.Format("START: Method:{0}, Section:ListSA.Instance.GetList(pMemberID:{1})", methodName,pMemberID), null);
                    stopwatch.Restart();
                }
                List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis1 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(String.Format("END: Method:{0}, Section:ListSA.Instance.GetList(pMemberID:{1}), ElapsedTime:{2}ms", methodName, pMemberID, ellapsedTimeInMillis1), null);
                }

                if (null != logger)
                {
                    logger.LogInfoMessage(String.Format("START: Method:{0}, Section:QuotaDefinitionSA.Instance.GetQuotaDefinitions().GetQuotaDefinition(QuotaType.ExcludeFromList, pCommunityID:{1}).MaxAllowed", methodName, pCommunityID), null);
                    stopwatch.Restart();
                }
                int maxAllowedFromList = QuotaDefinitionSA.Instance.GetQuotaDefinitions().GetQuotaDefinition(QuotaType.ExcludeFromList, pCommunityID).MaxAllowed;
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis2 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(String.Format("END: Method:{0}, Section:QuotaDefinitionSA.Instance.GetQuotaDefinitions().GetQuotaDefinition(QuotaType.ExcludeFromList, pCommunityID:{1}).MaxAllowed, ElapsedTime:{2}ms", methodName, pCommunityID, ellapsedTimeInMillis2), null);
                }

                int rowCount;

                if (null != logger)
                {
                    logger.LogInfoMessage(String.Format("START: Method:{0}, Section:list.GetListMembers(HotListCategory.ExcludeListInternal, pCommunityID:{1}, pSiteID:{2}, 1, maxAllowedFromList:{3}, out rowCount)", methodName, pCommunityID, pSiteID, maxAllowedFromList), null);
                    stopwatch.Restart();
                }
                ArrayList excludeListInternal = list.GetListMembers(HotListCategory.ExcludeListInternal, pCommunityID, pSiteID, 1, maxAllowedFromList, out rowCount);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis3 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(String.Format("END: Method:{0}, Section:list.GetListMembers(HotListCategory.ExcludeListInternal, pCommunityID:{1}, pSiteID:{2}, 1, maxAllowedFromList:{3}, out rowCount), ElapsedTime:{4}ms", methodName, pCommunityID, pSiteID, maxAllowedFromList, ellapsedTimeInMillis3), null);
                }

                if (null != logger)
                {
                    logger.LogInfoMessage(String.Format("START: Method:{0}, Section:list.GetListMembers(HotListCategory.ExcludeFromList, pCommunityID:{1}, pSiteID:{2}, 1, maxAllowedFromList:{3}, out rowCount)", methodName, pCommunityID, pSiteID, maxAllowedFromList), null);
                    stopwatch.Restart();
                }
                ArrayList excludeFromList = list.GetListMembers(HotListCategory.ExcludeFromList, pCommunityID, pSiteID, 1, maxAllowedFromList, out rowCount);
                if (null != logger)
                {
                    stopwatch.Stop();
                    long ellapsedTimeInMillis4 = stopwatch.ElapsedMilliseconds;
                    logger.LogInfoMessage(String.Format("END: Method:{0}, Section:list.GetListMembers(HotListCategory.ExcludeFromList, pCommunityID:{1}, pSiteID:{2}, 1, maxAllowedFromList:{3}, out rowCount), ElapsedTime:{4}ms", methodName, pCommunityID, pSiteID, maxAllowedFromList, ellapsedTimeInMillis4), null);
                }

                if (null != logger)
                {
                    logger.LogInfoMessage(String.Format("START: Method:{0}, Section:Parallel.For(HotListCategory.ExcludeListInternal)", methodName), null);
                    stopwatch.Restart();
                }

                if (useParallel)
                {
                    if (null != excludeListInternal && excludeListInternal.Count > 0)
                    {
                        var excludeCount = excludeListInternal.Count;
                        var blockedIds = new int[excludeCount];
                        Parallel.For(0, excludeCount, i =>
                        {
                            var blockedMemberId = (int) excludeListInternal[i];
                            blockedIds[i] = blockedMemberId;
                        });
                        allBlockedMemberIdString.Append(String.Join(MemberSearchSA.CommaSeparator,blockedIds) + MemberSearchSA.CommaSeparator);
                    }
                    if (null != logger)
                    {
                        stopwatch.Stop();
                        long ellapsedTimeInMillis5 = stopwatch.ElapsedMilliseconds;
                        logger.LogInfoMessage(String.Format("END: Method:{0}, Section:Parallel.For(HotListCategory.ExcludeListInternal), ElapsedTime:{1}ms", methodName, ellapsedTimeInMillis5), null);
                    }

                    if (null != logger)
                    {
                        logger.LogInfoMessage(String.Format("START: Method:{0}, Section:Parallel.For(HotListCategory.ExcludeFromList)", methodName), null);
                        stopwatch.Restart();
                    }
                    if (null != excludeFromList && excludeFromList.Count > 0)
                    {
                        string tempBlockedIds = allBlockedMemberIdString.ToString();
                        var excludeListCount = excludeFromList.Count;
                        var blockedIds = new int[excludeListCount];
                        Parallel.For(0, excludeListCount, i =>
                        {
                            var blockedMemberId = (int)excludeFromList[i];
                            if (tempBlockedIds.IndexOf(blockedMemberId.ToString()) == -1)
                            {
                                blockedIds[i] = blockedMemberId;
                            }
                        });
                        //have to iterate to skip empty ids
                        foreach (int id in blockedIds)
                        {
                            if (id > 0)
                            {
                                allBlockedMemberIdString.Append(id + MemberSearchSA.CommaSeparator);
                            }
                        }	                
                    }
                    if (null != logger)
                    {
                        stopwatch.Stop();
                        long ellapsedTimeInMillis5 = stopwatch.ElapsedMilliseconds;
                        logger.LogInfoMessage(String.Format("END: Method:{0}, Section:Parallel.For(HotListCategory.ExcludeFromList), ElapsedTime:{1}ms, BlockedIds:'{2}'", methodName, ellapsedTimeInMillis5, allBlockedMemberIdString), null);
                    }
                }
                else
                {
                    List<int> blockedMemberIds = ListFilterAccess.convertToGeneric(excludeListInternal);
                    List<int> blockedMemberIds2 = ListFilterAccess.convertToGeneric(excludeFromList);
                    List<int> allBlockedMemberIds = blockedMemberIds.Union(blockedMemberIds2).ToList();

                    if (allBlockedMemberIds.Count > 0)
                    {
                        foreach (Int32 id in allBlockedMemberIds)
                        {
                            allBlockedMemberIdString.Append(id + MemberSearchSA.CommaSeparator);
                        }
                    }
                }
            }
            return allBlockedMemberIdString;
        }

        public static ArrayList RemoveYnmNoMembers(int pCommunityID, int pMemberID, MatchnetQueryResults queryResults, string methodName = null, Stopwatch stopwatch = null, ISparkLogger logger = null)
        {
            if (null != logger)
            {
                logger.LogInfoMessage(
                    String.Format(
                        "START: Method:{0}, Section:RemoveHotlistedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}, HotListCategory.MembersClickedNo)",
                        methodName, queryResults.Items.Count, pCommunityID, pMemberID), null);
                stopwatch.Restart();
            }
            ArrayList resultsMembers = RemoveHotlistedMembers(queryResults, pCommunityID, pMemberID,
                HotListCategory.MembersClickedNo);
            if (null != logger)
            {
                stopwatch.Stop();
                long ellapsedTimeInMillis14 = stopwatch.ElapsedMilliseconds;
                logger.LogInfoMessage(
                    String.Format(
                        "END: Method:{0}, Section:RemoveHotlistedMembers(queryResults:{1}, pCommunityID:{2}, pMemberID:{3}, HotListCategory.MembersClickedNo), ElapsedTime:{4}ms",
                        methodName, queryResults.Items.Count, pCommunityID, pMemberID, ellapsedTimeInMillis14), null);
            }
            return resultsMembers;
        }
    }
}