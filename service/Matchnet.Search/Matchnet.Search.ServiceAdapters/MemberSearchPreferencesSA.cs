﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Spark.SearchPreferences;

namespace Matchnet.Search.ServiceAdapters
{
    public class MemberSearchPreferencesSA: SABase
    {
        #region Constants
        private const string SERVICE_MANAGER_NAME = "MemberSearchPreferencesSM";
        private const string SERVICE_CONSTANT = "SEARCH_SVC";
        #endregion

        #region Class members
        private Cache _cache;
        #endregion

        protected override void GetConnectionLimit()
        {
            MaxConnections = Convert.ToByte(RuntimeSettings.GetSetting("SEARCHSVC_SA_CONNECTION_LIMIT"));
        }

        #region Private methods

        		#region Singleton implementation
		/// <summary>
		/// Singleton instance of the SearchPreferences service adapter.
		/// </summary>
        public static readonly MemberSearchPreferencesSA Instance = new MemberSearchPreferencesSA();

        private MemberSearchPreferencesSA()
		{
			_cache = Cache.Instance;
		}
		#endregion

        private void CachePreferences(MemberSearchCollection preferences, int memberId, int communityId)
        {
            preferences.SetCacheKey = MemberSearchCollectionKey.GetCacheKey(memberId, communityId);
            preferences.CachePriority = CacheItemPriorityLevel.Normal;
            preferences.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SEARCHSVC_CACHE_TTL_SA"));
            _cache.Insert(preferences);
        }

        public void SaveMemberSearchCollection(MemberSearchCollection memberSearchCollection, int memberId, int communityId)
        {
            string uri = null;
            try
            {
                string cacheKey = MemberSearchCollectionKey.GetCacheKey(memberId, communityId);

                // Update Cached Preferences.
                var cachedPreferences = _cache.Get(cacheKey) as MemberSearchCollection;
                if (cachedPreferences != null)
                {
                    _cache.Remove(cacheKey);
                }
                
                uri = getServiceManagerUri();
                Checkout(uri);
                
                try
                {
                    getService(uri).SaveMemberSearchCollection(memberSearchCollection, memberId, communityId);
                    CachePreferences(memberSearchCollection, memberId, communityId);
                }
                finally
                {
                    Checkin(uri);
                }
                
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while attempting to save member search preferences. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to save member search preferences. (uri: " + uri + ")", ex));
            } 
        }

        public MemberSearchCollection GetMemberSearchCollection(int memberId, int communityId, bool ignoreSACache)
        {
            string uri = null;
            try
            {
                MemberSearchCollection memberSearchCollection = null;

                if (!ignoreSACache)
                {
                    var cacheKey = MemberSearchCollectionKey.GetCacheKey(memberId, communityId);
                    memberSearchCollection = _cache.Get(cacheKey) as MemberSearchCollection;
                }

                if (memberSearchCollection == null)
                {
                    uri = getServiceManagerUri();
                    Checkout(uri);

                    try
                    {
                        memberSearchCollection = getService(uri).GetMemberSearchCollection(memberId, communityId);
                        if (memberSearchCollection != null)
                        {
                            CachePreferences(memberSearchCollection, memberId, communityId);
                        }
                    }
                    finally
                    {
                        Checkin(uri);
                    }
                }

                return memberSearchCollection;
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
            }
        }

        private IMemberSearchPreferencesService getService(string uri)
        {
            try
            {
                return (IMemberSearchPreferencesService)Activator.GetObject(typeof(IMemberSearchPreferencesService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri(Int32 memberID)
        {
            try
            {
                string uri = AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
                string overrideHostName = RuntimeSettings.GetSetting("SEARCHSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private string getServiceManagerUri()
        {
            try
            {
                string uri = AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
                string overrideHostName = RuntimeSettings.GetSetting("SEARCHSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        #endregion
    }
}
