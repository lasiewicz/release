﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Search.ServiceAdapters
{
    public class MatchRatingUtil
    {
        private static Dictionary<int, int> childrenCountTable = new Dictionary<int, int>();
        private static Dictionary<int, int> _moreChildrenFlagTableReverse = new Dictionary<int, int>();
        private static List<MatchRatingAttribute> _matchAttributes = new List<MatchRatingAttribute>();
        private static List<MatchRatingAttribute> _matchAttributesE2 = new List<MatchRatingAttribute>();
        private static object _lockObjectMatchAttributes = new object();
        private static object _lockObjectMatchAttributesE2 = new object();
        private static ISettingsSA _settingsService = null;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) return RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public enum MatchRatingCalculationVersion
        {
            None = 0,
            Version2 = 2, //this is a formula that reduces non-matches by a percentage of overall total
            Version3 = 3 //this is a formula like mingle that reduces non-matches by a percentage of running total
        }

        /// <summary>
        /// Adds member's profile data as part of search preferences to be used for match rating calculation
        /// This is their "has" data
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="preferences"></param>
        /// <returns></returns>
        public static SearchPreferenceCollection AddMatchRatingHasData(int memberID, SearchPreferenceCollection preferences, int communityID, int siteID)
        {
            if (memberID > 0)
            {
                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
                                                                                            MemberLoadFlags.None);
                if (member != null)
                {
                    preferences.Add(QuerySearchParam.HasMemberID.ToString().ToLower(), member.MemberID.ToString());
                    preferences.Add(QuerySearchParam.HasActivityLevel.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "activitylevel",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasBirthDate.ToString().ToLower(),
                                     member.GetAttributeDate(communityID, siteID, Constants.NULL_INT, "birthdate",
                                                             DateTime.MinValue).Ticks.ToString());
                    preferences.Add(QuerySearchParam.HasBodyType.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "bodytype",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasChildrenCount.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "childrencount",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasCustody.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "custody",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasDrinkingHabits.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "drinkinghabits",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasEducationLevel.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "educationlevel",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasHeight.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "height",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasLanguageMask.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "languagemask",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasMaritalStatus.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "maritalstatus",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasMoreChildrenFlag.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                            "morechildrenflag", Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasSynagogueAttendance.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                            "synagogueattendance", Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasRelocateFlag.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "relocateflag",
                                                            Constants.NULL_INT).ToString());
                    preferences.Add(QuerySearchParam.HasSmokingHabits.ToString().ToLower(),
                                     member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "smokinghabits",
                                                            Constants.NULL_INT).ToString());

                    if (communityID == 3)
                    {
                        preferences.Add(QuerySearchParam.HasJDateEthnicity.ToString().ToLower(),
                                         member.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                                "jdateethnicity", Constants.NULL_INT).ToString());
                        preferences.Add(QuerySearchParam.HasJDateReligion.ToString().ToLower(),
                                         member.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                                "jdatereligion", Constants.NULL_INT).ToString());
                        preferences.Add(QuerySearchParam.HasKeepKosher.ToString().ToLower(),
                                         member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "keepkosher",
                                                                Constants.NULL_INT).ToString());
                    }
                    else
                    {
                        preferences.Add(QuerySearchParam.HasEthnicity.ToString().ToLower(),
                                         member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "ethnicity",
                                                                Constants.NULL_INT).ToString());
                        preferences.Add(QuerySearchParam.HasReligion.ToString().ToLower(),
                                         member.GetAttributeInt(communityID, siteID, Constants.NULL_INT, "religion",
                                                                Constants.NULL_INT).ToString());
                    }
                }
            }

            return preferences;
        }

        public static bool IsMatchRatingCalculationEnabled(int communityID, SearchType searchType)
        {
            bool isEnabled = false;

            if (Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_MATCH_RATING_CALCULATION", communityID)))
            {
                //restrict by search type to limit scope while we're releasing and testing this feature
                if (searchType == SearchType.YourMatchesWebSearch || searchType == SearchType.APISearch || searchType == SearchType.DailyMatches)
                {
                    isEnabled = true;
                }
            }

            return isEnabled;
        }

        public static bool IsMatchRatingCaptureMismatchDataOnProfileEnabled(int communityID)
        {
            return
                (Convert.ToBoolean(
                    SettingsService.GetSettingFromSingleton("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA_PROFILE",
                                                            communityID)));
        }

        public static int GetMatchRatingDefaultImportanceLevel(int communityID)
        {
            int defaultValue = Convert.ToInt32((SettingsService.GetSettingFromSingleton("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", communityID)));
            if (defaultValue > 5 || defaultValue < 1)
                defaultValue = 3;

            return defaultValue;
        }

        public static int GetMatchRatingSearchMemberPercentage(int communityID)
        {
            int defaultValue = Convert.ToInt32((SettingsService.GetSettingFromSingleton("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", communityID)));
            if (defaultValue > 100 || defaultValue < 0)
                defaultValue = 50;

            return defaultValue;
        }

        public static MatchRatingCalculationVersion GetMatchRatingFormulaVersion(int communityID)
        {
            int defaultValue = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MATCH_RATING_FORMULA_VERSION", communityID));
            MatchRatingCalculationVersion version = MatchRatingCalculationVersion.Version2;
            switch (defaultValue)
            {
                case 3:
                    version = MatchRatingCalculationVersion.Version3;
                    break;
            }

            return version;
        }

        public static int GetMatchRatingMinimumAttributesRequired(int communityID)
        {
            return Convert.ToInt32((SettingsService.GetSettingFromSingleton("MATCH_RATING_MINIMUM_ATTRIBUTES_REQUIRED", communityID)));
        }

        /// <summary>
        /// Gets a list of attributes used for match rating calculation
        /// The names of attributes here for a member's "has" and "wants" are based on Member and SearchPreferenceCollection data
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static List<MatchRatingAttribute> GetMatchRatingAttributeList(int communityID)
        {
            if (_matchAttributes.Count == 0)
            {
                lock (_lockObjectMatchAttributes)
                {
                    if (_matchAttributes.Count == 0)
                    {
                        _matchAttributes.Add(new MatchRatingAttribute("activitylevel"));
                        _matchAttributes.Add(new MatchRatingAttribute("birthdate",
                            "minage",
                            "maxage"));
                        _matchAttributes.Add(new MatchRatingAttribute("height",
                                "minheight",
                                "maxheight"));
                        _matchAttributes.Add(new MatchRatingAttribute("bodytype"));
                        _matchAttributes.Add(new MatchRatingAttribute("childrencount"));
                        _matchAttributes.Add(new MatchRatingAttribute("custody"));
                        _matchAttributes.Add(new MatchRatingAttribute("drinkinghabits"));
                        _matchAttributes.Add(new MatchRatingAttribute("educationlevel"));
                        _matchAttributes.Add(new MatchRatingAttribute("languagemask"));
                        _matchAttributes.Add(new MatchRatingAttribute("maritalstatus"));
                        _matchAttributes.Add(new MatchRatingAttribute("morechildrenflag"));
                        _matchAttributes.Add(new MatchRatingAttribute("relocateflag"));
                        _matchAttributes.Add(new MatchRatingAttribute("smokinghabits"));

                        if (communityID == 3)
                        {
                            _matchAttributes.Add(new MatchRatingAttribute("jdateethnicity"));
                            _matchAttributes.Add(new MatchRatingAttribute("jdatereligion"));
                            _matchAttributes.Add(new MatchRatingAttribute("keepkosher"));
                            _matchAttributes.Add(new MatchRatingAttribute("synagogueattendance"));
                        }
                        else
                        {
                            _matchAttributes.Add(new MatchRatingAttribute("ethnicity"));
                            _matchAttributes.Add(new MatchRatingAttribute("religion"));
                        }
                    }
                }
            }

            return _matchAttributes;
        }

        /// <summary>
        /// Gets a list of attributes used for match rating calculation
        /// The names of attributes here for a member's "has" and "wants" are based on Lucene indexed data
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static List<MatchRatingAttribute> GetMatchRatingAttributeListE2(int communityID)
        {
            if (_matchAttributesE2.Count == 0)
            {
                lock (_lockObjectMatchAttributesE2)
                {
                    if (_matchAttributesE2.Count == 0)
                    {
                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasActivityLevel.ToString().ToLower(),
                            QuerySearchParam.ActivityLevel.ToString().ToLower(),
                            QuerySearchParam.ActivityLevel.ToString().ToLower(),
                            QuerySearchParam.PreferredActivityLevel.ToString().ToLower(),
                            QuerySearchParam.PreferredActivityLevel.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasBirthDate.ToString().ToLower(),
                            "birthdate",
                            QuerySearchParam.AgeMin.ToString().ToLower(),
                            QuerySearchParam.AgeMax.ToString().ToLower(),
                            "preferredminage",
                            "preferredmaxage"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasHeight.ToString().ToLower(),
                                "height",
                                QuerySearchParam.HeightMin.ToString().ToLower(),
                                QuerySearchParam.HeightMax.ToString().ToLower(),
                                "preferredminheight",
                                "preferredmaxheight"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasBodyType.ToString().ToLower(),
                            QuerySearchParam.BodyType.ToString().ToLower(),
                            QuerySearchParam.BodyType.ToString().ToLower(),
                            QuerySearchParam.PreferredBodyType.ToString().ToLower(),
                            QuerySearchParam.PreferredBodyType.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasChildrenCount.ToString().ToLower(),
                                QuerySearchParam.ChildrenCount.ToString().ToLower(),
                                QuerySearchParam.ChildrenCount.ToString().ToLower(),
                                QuerySearchParam.PreferredChildrenCount.ToString().ToLower(),
                                QuerySearchParam.PreferredChildrenCount.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasCustody.ToString().ToLower(),
                                 QuerySearchParam.Custody.ToString().ToLower(),
                                 QuerySearchParam.Custody.ToString().ToLower(),
                                 QuerySearchParam.PreferredCustody.ToString().ToLower(),
                                 QuerySearchParam.PreferredCustody.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasDrinkingHabits.ToString().ToLower(),
                                 QuerySearchParam.DrinkingHabits.ToString().ToLower(),
                                 QuerySearchParam.DrinkingHabits.ToString().ToLower(),
                                 QuerySearchParam.PreferredDrinkingHabit.ToString().ToLower(),
                                 QuerySearchParam.PreferredDrinkingHabit.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasEducationLevel.ToString().ToLower(),
                                 QuerySearchParam.EducationLevel.ToString().ToLower(),
                                 QuerySearchParam.EducationLevel.ToString().ToLower(),
                                 QuerySearchParam.PreferredEducationLevel.ToString().ToLower(),
                                 QuerySearchParam.PreferredEducationLevel.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasLanguageMask.ToString().ToLower(),
                                 QuerySearchParam.LanguageMask.ToString().ToLower(),
                                 QuerySearchParam.LanguageMask.ToString().ToLower(),
                                 QuerySearchParam.LanguageMask.ToString().ToLower() + "mask",
                                 QuerySearchParam.PreferredLanguageMask.ToString().ToLower(),
                                 QuerySearchParam.PreferredLanguageMask.ToString().ToLower() + "mask",
                                 "", "", "", ""));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasMaritalStatus.ToString().ToLower(),
                                 QuerySearchParam.MaritalStatus.ToString().ToLower(),
                                 QuerySearchParam.MaritalStatus.ToString().ToLower(),
                                 QuerySearchParam.PreferredMaritalStatus.ToString().ToLower(),
                                 QuerySearchParam.PreferredMaritalStatus.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasMoreChildrenFlag.ToString().ToLower(),
                                 QuerySearchParam.MoreChildrenFlag.ToString().ToLower(),
                                 QuerySearchParam.MoreChildrenFlag.ToString().ToLower(),
                                 QuerySearchParam.PreferredMoreChildrenFlag.ToString().ToLower(),
                                 QuerySearchParam.PreferredMoreChildrenFlag.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasRelocateFlag.ToString().ToLower(),
                                 QuerySearchParam.RelocateFlag.ToString().ToLower(),
                                 QuerySearchParam.RelocateFlag.ToString().ToLower(),
                                 QuerySearchParam.PreferredRelocation.ToString().ToLower(),
                                 QuerySearchParam.PreferredRelocation.ToString().ToLower() + "mask"));

                        _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasSmokingHabits.ToString().ToLower(),
                                 QuerySearchParam.SmokingHabits.ToString().ToLower(),
                                 QuerySearchParam.SmokingHabits.ToString().ToLower(),
                                 QuerySearchParam.PreferredSmokingHabit.ToString().ToLower(),
                                 QuerySearchParam.PreferredSmokingHabit.ToString().ToLower() + "mask"));

                        if (communityID == 3)
                        {
                            _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasJDateEthnicity.ToString().ToLower(),
                                  QuerySearchParam.JDateEthnicity.ToString().ToLower(),
                                  QuerySearchParam.JDateEthnicity.ToString().ToLower(),
                                  QuerySearchParam.PreferredJdateEthnicity.ToString().ToLower(),
                                  QuerySearchParam.PreferredJdateEthnicity.ToString().ToLower() + "mask"));

                            _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasJDateReligion.ToString().ToLower(),
                                     QuerySearchParam.JDateReligion.ToString().ToLower(),
                                     QuerySearchParam.JDateReligion.ToString().ToLower(),
                                     QuerySearchParam.PreferredJdateReligion.ToString().ToLower(),
                                     QuerySearchParam.PreferredJdateReligion.ToString().ToLower() + "mask"));

                            _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasKeepKosher.ToString().ToLower(),
                                     QuerySearchParam.KeepKosher.ToString().ToLower(),
                                     QuerySearchParam.KeepKosher.ToString().ToLower(),
                                     QuerySearchParam.PreferredKosherStatus.ToString().ToLower(),
                                     QuerySearchParam.PreferredKosherStatus.ToString().ToLower() + "mask"));

                            _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasSynagogueAttendance.ToString().ToLower(),
                                  QuerySearchParam.SynagogueAttendance.ToString().ToLower(),
                                  QuerySearchParam.SynagogueAttendance.ToString().ToLower(),
                                  QuerySearchParam.PreferredSynagogueAttendance.ToString().ToLower(),
                                  QuerySearchParam.PreferredSynagogueAttendance.ToString().ToLower() + "mask"));
                        }
                        else
                        {
                            _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasEthnicity.ToString().ToLower(),
                                  QuerySearchParam.Ethnicity.ToString().ToLower(),
                                  QuerySearchParam.Ethnicity.ToString().ToLower(),
                                  QuerySearchParam.PreferredEthnicity.ToString().ToLower(),
                                  QuerySearchParam.PreferredEthnicity.ToString().ToLower() + "mask"));

                            _matchAttributesE2.Add(new MatchRatingAttribute(QuerySearchParam.HasReligion.ToString().ToLower(),
                                  QuerySearchParam.Religion.ToString().ToLower(),
                                  QuerySearchParam.Religion.ToString().ToLower(),
                                  QuerySearchParam.PreferredReligion.ToString().ToLower(),
                                  QuerySearchParam.PreferredReligion.ToString().ToLower() + "mask"));
                        }
                    }
                }
            }

            return _matchAttributesE2;
        }

        /// <summary>
        /// Calculates a match rating between two members
        /// </summary>
        /// <param name="memberID1"></param>
        /// <param name="memberID2"></param>
        /// <param name="communityID"></param>
        /// <param name="siteID"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        public static MatchRatingResult CalculateMatchPercentage(int memberID1, int memberID2, int communityID, int siteID, SearchType searchType)
        {
            MatchRatingResult matchRatingResult = new MatchRatingResult();
            matchRatingResult.Member1ID = memberID1;
            matchRatingResult.Member2ID = memberID2;

            int matchPercent = 0;
            string attrName = "";

            if (memberID1 > 0 && memberID2 > 0)
            {
                try
                {
                    //get member profile and match search prefs data
                    Matchnet.Member.ServiceAdapters.Member member1 = MemberSA.Instance.GetMember(memberID1,
                                                                                            MemberLoadFlags.None);
                    SearchPreferenceCollection searchPreferences1 =
                        SearchPreferencesSA.Instance.GetSearchPreferences(memberID1, communityID);

                    Matchnet.Member.ServiceAdapters.Member member2 = MemberSA.Instance.GetMember(memberID2,
                                                                                            MemberLoadFlags.None);
                    SearchPreferenceCollection searchPreferences2 =
                        SearchPreferencesSA.Instance.GetSearchPreferences(memberID2, communityID);

                    if (member1 != null && member2 != null && searchPreferences1 != null && searchPreferences2 != null)
                    {
                        List<MatchRatingAttribute> matchRatingAttributeList = GetMatchRatingAttributeList(communityID);

                        //get match rating settings
                        MatchRatingCalculationVersion matchVersion = GetMatchRatingFormulaVersion(communityID);
                        double defaultImportance = GetMatchRatingDefaultImportanceLevel(communityID);
                        string mismatchFormat = "Wants {0}, Target Has {1}";
                        string missingFormat = "Missing {0}, Value {1}";

                        //minimum attribute required for calculation related
                        int minimumProfileAttributeRequired = GetMatchRatingMinimumAttributesRequired(communityID);
                        int maxMissingAttributeForNoRating = matchRatingAttributeList.Count - minimumProfileAttributeRequired;

                        int member1MissingAttributeCount = 0;
                        int member2MissingAttributeCount = 0;

                        //calculate
                        double member1Subtotal = 1;
                        double member2Subtotal = 1;
                        
                        foreach (MatchRatingAttribute matchAttribute in matchRatingAttributeList)
                        {
                            attrName = matchAttribute.hasAttribute;

                            if (minimumProfileAttributeRequired > 0 && (member1MissingAttributeCount > maxMissingAttributeForNoRating || member2MissingAttributeCount > maxMissingAttributeForNoRating))
                            {
                                //member 1 or 2 missing more profile attributes than allowed for calculation
                                //return 0 to indicate no match rating
                                member1Subtotal = 0;
                                member2Subtotal = 0;
                                break;
                            }

                            if (attrName == "birthdate")
                            {
                                //member1 wants
                                int member1WantsMin = (searchPreferences1.ContainsKey(matchAttribute.wantattributeMin))
                                        ? int.Parse(searchPreferences1[matchAttribute.wantattributeMin])
                                        : 18;
                                int member1WantsMax = (searchPreferences1.ContainsKey(matchAttribute.wantattributeMax))
                                        ? int.Parse(searchPreferences1[matchAttribute.wantattributeMax])
                                        : 99;

                                DateTime member2HasBirthdate = member2.GetAttributeDate(communityID, siteID,
                                                                                       Constants.NULL_INT, matchAttribute.hasAttribute,
                                                                                       DateTime.MinValue);
                                int member2Age = 18;
                                if (member2HasBirthdate > DateTime.MinValue)
                                {
                                    member2Age = (int)((double)DateTime.Now.Subtract(member2HasBirthdate).Days / 365.25);
                                }
                                else
                                {
                                    member2MissingAttributeCount++;
                                    matchRatingResult.MissingAttributeMember2[attrName] = string.Format(missingFormat, attrName, member2HasBirthdate.ToString());
                                }

                                if ((member2Age < member1WantsMin) || (member2Age > member1WantsMax))
                                {
                                    if (matchVersion == MatchRatingCalculationVersion.Version2)
                                    {
                                        member1Subtotal -= ((defaultImportance * defaultImportance) / 100);
                                    }
                                    else if (matchVersion == MatchRatingCalculationVersion.Version3)
                                    {
                                        member1Subtotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                    }

                                    matchRatingResult.MismatchesMember1[attrName] = string.Format(mismatchFormat,
                                                                                                  member1WantsMin
                                                                                                      .ToString() + "-" +
                                                                                                  member1WantsMax
                                                                                                      .ToString(),
                                                                                                  member2Age.ToString());
                                }

                                //member2 wants
                                int member2WantsMin = (searchPreferences2.ContainsKey(matchAttribute.wantattributeMin))
                                        ? int.Parse(searchPreferences2[matchAttribute.wantattributeMin])
                                        : 18;
                                int member2WantsMax = (searchPreferences2.ContainsKey(matchAttribute.wantattributeMax))
                                        ? int.Parse(searchPreferences2[matchAttribute.wantattributeMax])
                                        : 99;

                                DateTime member1HasBirthdate = member1.GetAttributeDate(communityID, siteID,
                                                                                       Constants.NULL_INT, matchAttribute.hasAttribute,
                                                                                       DateTime.MinValue);
                                int member1Age = 18;
                                if (member1HasBirthdate > DateTime.MinValue)
                                {
                                    member1Age = (int) ((double) DateTime.Now.Subtract(member1HasBirthdate).Days/365.25);
                                }
                                else
                                {
                                    member1MissingAttributeCount++;
                                    matchRatingResult.MissingAttributeMember1[attrName] = string.Format(missingFormat, attrName, member1HasBirthdate.ToString());
                                }

                                if ((member1Age < member2WantsMin) ||
                                    (member1Age > member2WantsMax))
                                {
                                    if (matchVersion == MatchRatingCalculationVersion.Version2)
                                    {
                                        member2Subtotal -= ((defaultImportance*defaultImportance)/100);
                                    }
                                    else if (matchVersion == MatchRatingCalculationVersion.Version3)
                                    {
                                        member2Subtotal *= 1 - ((defaultImportance*defaultImportance)/100);
                                    }

                                    matchRatingResult.MismatchesMember2[attrName] = string.Format(mismatchFormat,
                                                                                                  member2WantsMin
                                                                                                      .ToString() + "-" +
                                                                                                  member2WantsMax
                                                                                                      .ToString(),
                                                                                                  member1Age.ToString());
                                }
                            }
                            else if (attrName == "height")
                            {
                                //member1 wants
                                int member1WantsMin = (searchPreferences1.ContainsKey(matchAttribute.wantattributeMin))
                                        ? int.Parse(searchPreferences1[matchAttribute.wantattributeMin])
                                        : int.MinValue;
                                int member1WantsMax = (searchPreferences1.ContainsKey(matchAttribute.wantattributeMax))
                                        ? int.Parse(searchPreferences1[matchAttribute.wantattributeMax])
                                        : int.MinValue;

                                int member2Has = member2.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                                             matchAttribute.hasAttribute,
                                                                             Constants.NULL_INT);

                                if (member2Has == Constants.NULL_INT)
                                {
                                    member2MissingAttributeCount++;
                                    matchRatingResult.MissingAttributeMember2[attrName] = string.Format(missingFormat, attrName, member2Has.ToString());
                                }

                                if ((member1WantsMin > 0 && member2Has < member1WantsMin)
                                    || (member1WantsMax > 0 && member2Has > member1WantsMax))
                                {
                                    if (matchVersion == MatchRatingCalculationVersion.Version2)
                                    {
                                        member1Subtotal -= ((defaultImportance * defaultImportance) / 100);
                                    }
                                    else if (matchVersion == MatchRatingCalculationVersion.Version3)
                                    {
                                        member1Subtotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                    }

                                    matchRatingResult.MismatchesMember1[attrName] = string.Format(mismatchFormat,
                                                                                                  member1WantsMin
                                                                                                      .ToString() + "-" +
                                                                                                  member1WantsMax
                                                                                                      .ToString(),
                                                                                                  member2Has.ToString());
                                }

                                //member2 wants
                                int member2WantsMin = (searchPreferences2.ContainsKey(matchAttribute.wantattributeMin))
                                        ? int.Parse(searchPreferences2[matchAttribute.wantattributeMin])
                                        : int.MinValue;
                                int member2WantsMax = (searchPreferences2.ContainsKey(matchAttribute.wantattributeMax))
                                        ? int.Parse(searchPreferences2[matchAttribute.wantattributeMax])
                                        : int.MinValue;

                                int member1Has = member1.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                                             matchAttribute.hasAttribute,
                                                                             Constants.NULL_INT);

                                if (member1Has == Constants.NULL_INT)
                                {
                                    member1MissingAttributeCount++;
                                    matchRatingResult.MissingAttributeMember1[attrName] = string.Format(missingFormat, attrName, member1Has.ToString());
                                }

                                if ((member2WantsMin > 0 && member1Has < member2WantsMin)
                                    || (member2WantsMax > 0 && member1Has > member2WantsMax))
                                {
                                    if (matchVersion == MatchRatingCalculationVersion.Version2)
                                    {
                                        member2Subtotal -= ((defaultImportance*defaultImportance)/100);
                                    }
                                    else if (matchVersion == MatchRatingCalculationVersion.Version3)
                                    {
                                        member2Subtotal *= 1 - ((defaultImportance*defaultImportance)/100);
                                    }

                                    matchRatingResult.MismatchesMember2[attrName] = string.Format(mismatchFormat,
                                                                                                  member2WantsMin
                                                                                                      .ToString() + "-" +
                                                                                                  member2WantsMax
                                                                                                      .ToString(),
                                                                                                  member1Has.ToString());
                                }
                            }
                            else
                            {
                                //member1 wants
                                int member1Wants = (searchPreferences1.ContainsKey(matchAttribute.wantAttribute))
                                        ? int.Parse(searchPreferences1[matchAttribute.wantAttribute])
                                        : Constants.NULL_INT;

                                int member2Has = member2.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                                             matchAttribute.hasAttribute,
                                                                             Constants.NULL_INT);

                                if (member2Has == Constants.NULL_INT)
                                {
                                    member2MissingAttributeCount++;
                                    matchRatingResult.MissingAttributeMember2[attrName] = string.Format(missingFormat, attrName, member2Has.ToString());
                                }

                                if (member1Wants > 0)
                                {
                                    bool member1WantsMatch = false;
                                    
                                    if (member2Has >= 0)
                                    {
                                        if (attrName == "languagemask")
                                        {
                                            //mask vs mask comparison
                                            if (member2Has > 0)
                                            {
                                                int higherMask = member2Has > member1Wants
                                                                        ? member2Has
                                                                        : member1Wants;
                                                for (long i = 1; (long) Math.Pow(2, i) <= higherMask; i++)
                                                {
                                                    long step = (long) Math.Pow(2, i);
                                                    if (((step & member2Has) == step) &&
                                                        ((step & member1Wants) == step))
                                                    {
                                                        member1WantsMatch = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //single value comparison (profile data is single value, member prefs is mask value)
                                            if (attrName == "morechildrenflag" && GetMoreChildrenFlagConversionTableReverse().ContainsKey(member2Has))
                                            {
                                                member2Has = GetMoreChildrenFlagConversionTableReverse()[member2Has];
                                            }
                                            else if (attrName == "childrencount" && GetChildrenCountConversionTable().ContainsKey(member2Has))
                                            {
                                                member2Has = GetChildrenCountConversionTable()[member2Has];
                                            }

                                            if (member2Has > 0)
                                            {
                                                if ((member2Has & member1Wants) == member2Has)
                                                {
                                                    member1WantsMatch = true;
                                                }
                                            }
                                        }                                        
                                    }

                                    if (!member1WantsMatch)
                                    {
                                        if (matchVersion == MatchRatingCalculationVersion.Version2)
                                        {
                                            member1Subtotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingCalculationVersion.Version3)
                                        {
                                            member1Subtotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }

                                        matchRatingResult.MismatchesMember1[attrName] = string.Format(mismatchFormat,
                                                                                                      member1Wants,
                                                                                                      member2Has);
                                    }
                                }

                                //member2 wants
                                int member2Wants = (searchPreferences2.ContainsKey(matchAttribute.wantAttribute))
                                        ? int.Parse(searchPreferences2[matchAttribute.wantAttribute])
                                        : Constants.NULL_INT;

                                int member1Has = member1.GetAttributeInt(communityID, siteID, Constants.NULL_INT,
                                                                             matchAttribute.hasAttribute,
                                                                             Constants.NULL_INT);

                                if (member1Has == Constants.NULL_INT)
                                {
                                    member1MissingAttributeCount++;
                                    matchRatingResult.MissingAttributeMember1[attrName] = string.Format(missingFormat, attrName, member1Has.ToString());
                                }

                                if (member2Wants > 0)
                                {
                                    bool member2WantsMatch = false;

                                    if (member1Has >= 0)
                                    {
                                        if (attrName == "languagemask")
                                        {
                                            //mask vs mask comparison
                                            if (member1Has > 0)
                                            {
                                                int higherMask = member1Has > member2Wants
                                                                     ? member1Has
                                                                     : member2Wants;
                                                for (long i = 1; (long) Math.Pow(2, i) <= higherMask; i++)
                                                {
                                                    long step = (long) Math.Pow(2, i);
                                                    if (((step & member1Has) == step) &&
                                                        ((step & member2Wants) == step))
                                                    {
                                                        member2WantsMatch = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //single value comparison (profile data is single value, member prefs is mask value)
                                            if (attrName == "morechildrenflag" && GetMoreChildrenFlagConversionTableReverse().ContainsKey(member1Has))
                                            {
                                                member1Has = GetMoreChildrenFlagConversionTableReverse()[member1Has];
                                            }
                                            else if (attrName == "childrencount" && GetChildrenCountConversionTable().ContainsKey(member1Has))
                                            {
                                                member1Has = GetChildrenCountConversionTable()[member1Has];
                                            }

                                            if (member1Has > 0)
                                            {
                                                if ((member1Has & member2Wants) == member1Has)
                                                {
                                                    member2WantsMatch = true;
                                                }
                                            }
                                        }                                        
                                    }

                                    if (!member2WantsMatch)
                                    {
                                        if (matchVersion == MatchRatingCalculationVersion.Version2)
                                        {
                                            member2Subtotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingCalculationVersion.Version3)
                                        {
                                            member2Subtotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }

                                        matchRatingResult.MismatchesMember2[attrName] = string.Format(mismatchFormat,
                                                                                                      member2Wants,
                                                                                                      member1Has);
                                    }
                                }
                            }
                        }

                        if (member1Subtotal < 0)
                            member1Subtotal = 0;

                        if (member2Subtotal < 0)
                            member2Subtotal = 0;

                        //determine total match percentage
                        int member1MatchPercentage = GetMatchRatingSearchMemberPercentage(communityID);
                        matchRatingResult.MatchScoreMember1 = member1MatchPercentage*member1Subtotal;

                        int member2MatchPercentage = 100 - member1MatchPercentage;
                        matchRatingResult.MatchScoreMember2 = member2MatchPercentage*member2Subtotal;

                        matchPercent =
                            (int)
                            Math.Floor((member1MatchPercentage * member1Subtotal) +
                                       (member2MatchPercentage * member2Subtotal));
                    }
                }
                catch (Exception ex)
                {

                }
            }

            matchRatingResult.MatchScore = matchPercent;
            return matchRatingResult;
        }

        /// <summary>
        /// Provides conversion of member's attribute data to preferences data which supports mask
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, int> GetChildrenCountConversionTable()
        {
            if (childrenCountTable.Count == 0)
            {
                //NOTE: DB is used by profile info, Web is used by Preferences
                // 0 children, db = 0, web = 1
                childrenCountTable[0] = 1;
                // 1 child, db = 1, web = 2
                childrenCountTable[1] = 2;
                // 2 children, db = 2, web = 4			
                childrenCountTable[2] = 4;
                // 3 or more children, db = 3, web = 8			
                childrenCountTable[3] = 8;
            }
            return childrenCountTable;
        }

        /// <summary>
        /// Provides conversion of member's attribute data to preferences data which supports mask
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, int> GetMoreChildrenFlagConversionTableReverse()
        {
            if (_moreChildrenFlagTableReverse.Count == 0)
            {
                //NOTE: DB is used by preferences, Web is used by profile
                // No more children, db = 1, web = 0
                _moreChildrenFlagTableReverse[0] = 1;
                // Yes more children, db = 2, web = 1
                _moreChildrenFlagTableReverse[1] = 2;
                // Not sure, db = 4, web = 2
                _moreChildrenFlagTableReverse[2] = 4;
            }
            return _moreChildrenFlagTableReverse;
        }
    }
}
