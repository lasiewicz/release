using System;

using Matchnet.Caching;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.ReplicationActions;
using Matchnet.Search.ValueObjects.ServiceDefinitions;

namespace Matchnet.Search.ServiceAdapters
{
	/// <summary>
	/// Summary description for SearchPreferencesSA.
	/// </summary>
    public class SearchPreferencesSA : SABase, ISearchPreferencesSA
	{
		#region Constants
		private const string SERVICE_MANAGER_NAME = "SearchPreferencesSM";
		private const string SERVICE_CONSTANT = "SEARCH_SVC";
		#endregion

		#region Class members
		private Cache _cache;
		#endregion

		#region Singleton implementation
		/// <summary>
		/// Singleton instance of the SearchPreferences service adapter.
		/// </summary>
		public static readonly SearchPreferencesSA Instance = new SearchPreferencesSA();
		
		private SearchPreferencesSA()
		{
			_cache = Cache.Instance;
		}
		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_SA_CONNECTION_LIMIT"));
		}

		#region Search Preferences Adapter public interface

        public SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID)
        {
            return GetSearchPreferences(pMemberID, pCommunityID, false);
        }

		/// <summary>
		/// Retrieves a collection of search preferences based on the supplied parameters.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
        /// <param name="ignoreSACache"></param>
		/// <returns></returns>
		public SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID, bool ignoreSACache)
		{
			SearchPreferenceCollection preferences = null;

			try
			{
                if (!ignoreSACache)
                {
                    // Attempt to locate this member's preferences from the local cache.
                    preferences = _cache.Get(SearchPreferencesKey.GetCacheKey(pMemberID, pCommunityID)) as SearchPreferenceCollection;
                }

				if (preferences == null)
				{
					// Retrieve the preferences from the middle-tier and cache them locally.
					preferences = getPreferences(pMemberID, pCommunityID);
					CachePreferences(preferences, pMemberID, pCommunityID);
				}
				return preferences;
			}
			catch (ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to get search preferences", ex));
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred while attempting to get search preferences", ex));
			}
		}

		/// <summary>
		/// Save the search preferences for session and member (as applicable).
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSearchPreferences"></param>
		public void Save(int pMemberID, int pCommunityID, ISearchPreferences pSearchPreferences)
		{
			string uri = null;
			try
			{
				// Do not attempt to save preferences unless they are deemed valid.
				ValidationResult result = ((SearchPreferenceCollection)pSearchPreferences).Validate();
				if (PreferencesValidationStatus.Failed.Equals(result.Status))
				{
					throw new Exception("Invalid search prefernces: " + result.ValidationError);
				}

				string cacheKey = SearchPreferencesKey.GetCacheKey(pMemberID, pCommunityID);
				
				// Update Cached Preferences.
				ISearchPreferences cachedPreferences = _cache.Get(cacheKey) as ISearchPreferences;
				if (cachedPreferences != null)
				{	
					_cache.Remove(cacheKey);
				}

			    ((SearchPreferenceCollection) pSearchPreferences).IsDefaultPreferences = false;
				CachePreferences((SearchPreferenceCollection)pSearchPreferences, pMemberID, pCommunityID);
				uri = getServiceManagerUri(pMemberID);
				base.Checkout(uri);
				try
				{
					getService(uri).Save(pMemberID, pCommunityID, (ISearchPreferences)pSearchPreferences);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to save search preferences. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to save search preferences. (uri: " + uri + ")", ex));
			} 
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PreferenceCollection GetPreferences()
		{
			string uri = null;
			try
			{
				PreferenceCollection preferences = _cache.Get(PreferenceCollection.CACHE_KEY) as PreferenceCollection;

				if (preferences == null)
				{
					uri = getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						preferences = getService(uri).GetPreferences();

						_cache.Insert(preferences);
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return preferences;
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
		}

		public MemberSearchCollection GetMemberSearches(Int32 memberID, Int32 groupID)
		{
			string uri = null;
			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).GetMemberSearches(memberID, groupID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
		}

		public void SaveMemberSearches(MemberSearchCollection searches)
		{
			string uri = null;
			try
			{
				if (searches.IsDirty)
				{
					//Build a collection of all changes
					ReplicationActionCollection actions = new ReplicationActionCollection();

					foreach (MemberSearch search in searches)
					{
						if (search.IsDirty)
						{
							actions.Add(new ChangeSearchAction(search.MemberSearchID, search.MemberID, search.GroupID, search.SearchName, search.IsPrimary));
						
							foreach (MemberSearchPreference preference in search)
							{
								if (preference.IsDirty)
								{
									if (preference.IsNull)
									{
										actions.Add(new DeletePreferenceAction(search.MemberID, search.MemberSearchID, preference.PreferenceID));
									}
									else
									{
										actions.Add(new ChangePreferenceAction(search.MemberID, search.MemberSearchID, preference.PreferenceID,
											preference.Weight, preference.Value, preference.MinValue, preference.MaxValue));
									}
								}
							}
						}
					}
                    
					uri = getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						getService(uri).ProcessAction(actions);
					}
					finally
					{
						base.Checkin(uri);
					}
				}
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
		}

		public void DeleteMemberSearch(Int32 memberID, Int32 memberSearchID)
		{
			string uri = null;
			try
			{
				DeleteSearchAction action = new DeleteSearchAction(memberID, memberSearchID);

				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					getService(uri).ProcessAction(action);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve preferences. (uri: " + uri + ")", ex));
			} 
		}
		#endregion

		#region Private methods

		private void CachePreferences(SearchPreferenceCollection pPreferences, int pMemberID, int pCommunityID)
		{
			pPreferences.SetCacheKey = SearchPreferencesKey.GetCacheKey(pMemberID, pCommunityID);
			pPreferences.CachePriority = CacheItemPriorityLevel.Normal;
			pPreferences.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SEARCHSVC_CACHE_TTL_SA"));
			_cache.Insert(pPreferences);
		}

		/// <summary>
		/// Retrieves the search preferences for this member/session from the middle-tier.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <returns></returns>
		private SearchPreferenceCollection getPreferences(int pMemberID, int pCommunityID)
		{
			string uri = null;
			try
			{
				uri = getServiceManagerUri(pMemberID);
				base.Checkout(uri);
				try
				{
					return getService(uri).GetSearchPreferences(pMemberID, pCommunityID) as SearchPreferenceCollection;
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to get search preferences. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to get search preferences. (uri: " + uri + ")", ex));
			} 
		}

		private Matchnet.Search.ValueObjects.ServiceDefinitions.ISearchPreferencesService getService(string uri)
		{
			try
			{
				return (Matchnet.Search.ValueObjects.ServiceDefinitions.ISearchPreferencesService)Activator.GetObject(typeof(Matchnet.Search.ValueObjects.ServiceDefinitions.ISearchPreferencesService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		private string getServiceManagerUri(Int32 memberID)
		{
			try
			{
				string uri = AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = RuntimeSettings.GetSetting("SEARCHSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Trim().Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		private string getServiceManagerUri()
		{
			try
			{
				string uri = AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = RuntimeSettings.GetSetting("SEARCHSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Trim().Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		#endregion

	}
}
