﻿#region

using System;
using System.Runtime.Serialization;
using Matchnet.Search.Interfaces;

#endregion

namespace Matchnet.Search.ValueObjects.DailyMatches
{
    /// <summary>
    ///     MatchnetQueryResults cannot be used as it does not cache based on Member Id
    /// </summary>
    [Serializable]
    public class DailyMatchesResults : IMatchnetQueryResults, ICacheable
    {
        private const string CacheKeyPrefix = "DailyMatches";

        /// <summary>
        /// </summary>
        /// <param name="resultItems"></param>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="cacheTtlSeconds"></param>
        public DailyMatchesResults(int communityId, int memberId, int cacheTtlSeconds,
            IMatchnetResultItems resultItems)
        {
            MemberId = memberId;
            CommunityId = communityId;
            CacheTTLSeconds = cacheTtlSeconds;
            Items = resultItems;
            MatchesFound = resultItems.Count;
            MatchesReturned = resultItems.Count;

            CacheMode = CacheItemMode.Absolute;
            CachePriority = CacheItemPriorityLevel.Default;
        }

        /// <summary>
        /// </summary>
        public int MemberId { get; set; }

        /// <summary>
        /// </summary>
        public int CommunityId { get; set; }

        public string GetCacheKey()
        {
            return GetCacheKey(CommunityId, MemberId);
        }

        public int CacheTTLSeconds { get; set; }
        public CacheItemMode CacheMode { get; private set; }
        public CacheItemPriorityLevel CachePriority { get; set; }
        public IMatchnetResultItems Items { get; set; }
        public int MatchesFound { get; set; }
        public int MatchesReturned { get; private set; }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static string GetCacheKey(int communityId, int memberId)
        {
            return string.Format("{0}-{1}-{2}", CacheKeyPrefix, communityId, memberId);
        }
    }
}