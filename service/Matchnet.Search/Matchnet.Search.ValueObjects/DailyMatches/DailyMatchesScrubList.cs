﻿#region

#endregion

#region

using System;
using System.Collections.Generic;

#endregion

namespace Matchnet.Search.ValueObjects.DailyMatches
{
    /// <summary>
    ///     Using a hash set to contain only unique member Ids
    /// </summary>
    [Serializable]
    public class DailyMatchesScrubList : ICacheable
    {
        private const string CacheKeyPrefix = "DailyMatchesScrubList";

        /// <summary>
        ///     For serialization
        /// </summary>
        public DailyMatchesScrubList()
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="cacheTtlSeconds"></param>
        /// <param name="memberIds"></param>
        /// <param name="insertDateTime"></param>
        public DailyMatchesScrubList(int communityId, int memberId, int cacheTtlSeconds, HashSet<int> memberIds,
            DateTime insertDateTime)
        {
            MemberId = memberId;
            CommunityId = communityId;
            CacheTTLSeconds = cacheTtlSeconds;
            MemberIds = memberIds;
            InsertDateTime = insertDateTime;
        }

        /// <summary>
        ///     Scrub list of unique member Ids
        /// </summary>
        public HashSet<int> MemberIds { get; set; }

        /// <summary>
        ///     In order to support appending to the list, need to update Cache TTL with
        ///     (InsertDateTime + TTL) - DateTime.Now = time left in TTL
        /// </summary>
        public DateTime InsertDateTime { get; set; }

        /// <summary>
        /// </summary>
        public int MemberId { get; set; }

        /// <summary>
        /// </summary>
        public int CommunityId { get; set; }

        public string GetCacheKey()
        {
            return GetCacheKey(CommunityId, MemberId);
        }

        public int CacheTTLSeconds { get; set; }
        public CacheItemMode CacheMode { get; private set; }
        public CacheItemPriorityLevel CachePriority { get; set; }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static string GetCacheKey(int communityId, int memberId)
        {
            return string.Format("{0}-{1}-{2}", CacheKeyPrefix, communityId, memberId);
        }
    }
}