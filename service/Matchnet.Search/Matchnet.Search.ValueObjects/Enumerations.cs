using System;

namespace Matchnet.Search.ValueObjects 
{

	/// <summary>
	/// Query Search Parameters. These are parameters used to indicate filtering - what I want to narrow by.
	/// </summary>
	public enum QuerySearchParam: int 
	{
		AgeMax = 1,
		AgeMin = 2,
		AreaCode = 3,
		BodyType = 4,
		DomainID = 5,
		DrinkingHabits = 6,
		EducationLevel = 7,
		Ethnicity = 8,
		GenderMask = 9,
		HasPhotoFlag = 10,
		HeightMax = 11,
		HeightMin = 12,
		JDateEthnicity = 13,
		JDateReligion = 14,
		KeepKosher = 15,
		LanguageMask = 16,
		MajorType = 17,
		MaritalStatus = 18,
		RegionID = 19,
		RegionIDCountry = 20,
		RelationshipMask = 21,
		RelationshipStatus = 22,
		Religion = 23,
		SchoolID = 24,
		SexualIdentityType = 25,
		SmokingHabits = 26,
		SynagogueAttendance = 27,
		Zodiac = 28,
		EssayWords = 29,
		AllTextFields = 30,
		Username = 31

	}

	/// <summary>
	/// Query Sort Parameters. These are parameters used to indicate the ordering of the resultset.
	/// </summary>
	public enum QuerySortParam: int 
	{
		JoinDate = 1,
		LastLogonDate = 2,
		Proximity = 3,
		Popularity  = 4
	}

	/// <summary>
	/// Query Sort Direction Parameters. The sorting direction to apply to sorting parameters.
	/// </summary>
	public enum QuerySortDirectionParam: int 
	{
		Ascending = 1,
		Descending = 2
	}

}
