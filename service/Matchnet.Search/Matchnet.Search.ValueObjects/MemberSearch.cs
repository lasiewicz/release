using System;
using System.Collections;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for MemberSearchPreference.
	/// </summary>
	[Serializable]
	public class MemberSearch : IEnumerable
	{
		private Int32 _memberSearchID;
		private Int32 _memberID;
		private Int32 _groupID;
		private string _searchName;
		private bool _isPrimary;

		private bool _isDirty = false;
		private MemberSearchCollection _parent;

		private Hashtable _preferences = new Hashtable();

		public MemberSearch(Int32 memberSearchID, Int32 memberID, Int32 groupID, string searchName, bool isPrimary)
		{
            _memberSearchID = memberSearchID;
			_memberID = memberID;
			_groupID = groupID;
			_searchName = searchName;
			_isPrimary = isPrimary;
		}
		
		public void AddPreference(MemberSearchPreference preference)
		{
			preference.Parent = this;
			_preferences.Add(preference.PreferenceID, preference);
			OnDirty();
		}

		internal void OnDirty()
		{
			_isDirty = true;
			if (Parent != null)
			{
				Parent.OnDirty();
			}
		}

		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		public string SearchName
		{
			get
			{
				return _searchName;
			}
			set
			{
				_searchName = value;
				OnDirty();
			}
		}

		public bool IsPrimary
		{
			get
			{
				return _isPrimary;
			}
			set
			{
				_isPrimary = value;
				OnDirty();
			}
		}

		public bool IsDirty
		{
			get
			{
				return _isDirty;
			}
		}

		public MemberSearchPreference this[Int32 preferenceID]
		{
			get
			{
				return _preferences[preferenceID] as MemberSearchPreference;
			}
		}

		internal MemberSearchCollection Parent
		{
			get
			{
				return _parent;
			}
			set
			{
				_parent = value;
			}
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _preferences.Values.GetEnumerator();
		}

		#endregion
	}
}
