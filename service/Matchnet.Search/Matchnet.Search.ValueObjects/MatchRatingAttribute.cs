﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Search.ValueObjects
{
    /// <summary>
    /// Class used to store attribute has/wants names used to retrieve the right attribute data during match rating calculation
    /// </summary>
    [Serializable()]
    public class MatchRatingAttribute
    {
        //outside of search, both member data uses same attribute names, more intuitive when looking at the formula
        public string hasAttribute;
        public string wantAttribute;
        public string wantattributeMin;
        public string wantattributeMax;

        //inside search where names can be different
        //searcher member attribute names
        public string searcherMemberHasAttribute;
        public string searcherMemberWantAttribute;
        public string searcherMemberWantAttributeMin;
        public string searcherMemberWantAttributeMax;

        //resulting member attribute names
        public string resultingMemberHasAttribute;
        public string resultingMemberHasAttributeMask;
        public string resultingMemberWantAttribute;
        public string resultingMemberWantAttributeMask;
        public string resultingMemberWantAttributeMin;
        public string resultingMemberWantAttributeMax;


        public MatchRatingAttribute()
        {
        }

        public MatchRatingAttribute(string pAttribute)
        {
            hasAttribute = pAttribute;
            wantAttribute = pAttribute;

            searcherMemberHasAttribute = pAttribute;
            searcherMemberWantAttribute = pAttribute;

            resultingMemberHasAttribute = pAttribute;
            resultingMemberWantAttribute = pAttribute;
        }

        public MatchRatingAttribute(string pHasAttribute,
                                    string pWantAttribute)
        {
            hasAttribute = pHasAttribute;
            wantAttribute = pWantAttribute;

            searcherMemberHasAttribute = pHasAttribute;
            searcherMemberWantAttribute = pWantAttribute;

            resultingMemberHasAttribute = pHasAttribute;
            resultingMemberWantAttribute = pWantAttribute;
        }

        public MatchRatingAttribute(string pHasAttribute,
                                    string pWantAttributeMin,
                                    string pWantAttributeMax)
        {
            hasAttribute = pHasAttribute;
            wantattributeMin = pWantAttributeMin;
            wantattributeMax = pWantAttributeMax;

            searcherMemberHasAttribute = pHasAttribute;
            searcherMemberWantAttributeMax = pWantAttributeMax;
            searcherMemberWantAttributeMin = pWantAttributeMin;

            resultingMemberHasAttribute = pHasAttribute;
            resultingMemberWantAttributeMax = pWantAttributeMax;
            resultingMemberWantAttributeMin = pWantAttributeMin;
        }

        public MatchRatingAttribute(string pSearcherMemberHasAttribute,
                                    string pSearcherMemberWantAttribute,
                                    string pResultingMemberHasAttribute,
                                    string pResultingMemberWantAttribute,
                                    string pResultingMemberWantAttributeMask)
        {
            searcherMemberHasAttribute = pSearcherMemberHasAttribute;
            searcherMemberWantAttribute = pSearcherMemberWantAttribute;

            resultingMemberHasAttribute = pResultingMemberHasAttribute;
            resultingMemberWantAttribute = pResultingMemberWantAttribute;
            resultingMemberWantAttributeMask = pResultingMemberWantAttributeMask;
        }

        public MatchRatingAttribute(string pSearcherMemberHasAttribute,
                                    string pResultingMemberHasAttribute,
                                    string pSearcherMemberWantAttributeMin,
                                    string pSearcherMemberWantAttributeMax,
                                    string pResultingMemberWantAttributeMin,
                                    string pResultingMemberWantAttributeMax)
        {
            searcherMemberHasAttribute = pSearcherMemberHasAttribute;
            searcherMemberWantAttributeMax = pSearcherMemberWantAttributeMax;
            searcherMemberWantAttributeMin = pSearcherMemberWantAttributeMin;

            resultingMemberHasAttribute = pResultingMemberHasAttribute;
            resultingMemberWantAttributeMax = pResultingMemberWantAttributeMax;
            resultingMemberWantAttributeMin = pResultingMemberWantAttributeMin;
        }

        public MatchRatingAttribute(string pSearcherMemberHasAttribute,
                                    string pSearcherMemberWantAttribute,
                                    string pResultingMemberHasAttribute,
                                    string pResultingMemberHasAttributeMask,
                                    string pResultingMemberWantAttribute,
                                    string pResultingMemberWantAttributeMask,
                                    string pSearcherMemberWantAttributeMin,
                                    string pSearcherMemberWantAttributeMax,
                                    string pResultingMemberWantAttributeMin,
                                    string pResultingMemberWantAttributeMax)
        {
            searcherMemberHasAttribute = pSearcherMemberHasAttribute;
            searcherMemberWantAttribute = pSearcherMemberWantAttribute;
            searcherMemberWantAttributeMax = pSearcherMemberWantAttributeMax;
            searcherMemberWantAttributeMin = pSearcherMemberWantAttributeMin;

            resultingMemberHasAttribute = pResultingMemberHasAttribute;
            resultingMemberHasAttributeMask = pResultingMemberHasAttributeMask;
            resultingMemberWantAttribute = pResultingMemberWantAttribute;
            resultingMemberWantAttributeMask = pResultingMemberWantAttributeMask;
            resultingMemberWantAttributeMax = pResultingMemberWantAttributeMax;
            resultingMemberWantAttributeMin = pResultingMemberWantAttributeMin;
        }

    }
}
