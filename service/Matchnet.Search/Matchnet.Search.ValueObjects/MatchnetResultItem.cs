using System;
using System.Collections.Specialized;
using Matchnet;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects {
	/// <summary>
	/// Summary description for MatchnetQueryResultItem.
	/// </summary>
	/// 
	[Serializable()]
	public class MatchnetResultItem : IMatchnetResultItem {
		private int _MemberID;
		private string [] _Attributes;
        private int _matchScore;

		private const int ITEM_ATTRIBUTE_COUNT = 7;

		/// <summary>
		/// Constructor - initializes the MemberID property with the supplied parameter.
		/// </summary>
		/// <param name="pMemberID"></param>
		public MatchnetResultItem(int pMemberID) {
			_MemberID = pMemberID;
		}

        public MatchnetResultItem(int pMemberID, int matchScore)
        {
            _MemberID = pMemberID;
            _matchScore = matchScore;
        }

		#region IMatchnetResultItem Implementation

		/// <summary>
		/// MemberID represented by this result item.
		/// </summary>
		public int MemberID {
			get {
				return _MemberID;
			}
		}

        public int MatchScore
        {
            get
            {
                return _matchScore;
            }
            set
            {
                _matchScore = value;
            }
        }

		/// <summary>
		/// Gets or sets extra member attribute values.
		/// </summary>
		/// <remarks>Strin.Empty will be returned if either the attribute is empty or not supported.</remarks>
		public string this[string AttributeName]{
			get {
				if (_Attributes == null){
					return String.Empty;
				}
				else {
					int index = GetAttributeIndex(ref AttributeName);
					if ( index >= 0){
						return _Attributes[index];
					}
					else{
						return String.Empty;
					}
				}
			}
			set {
				int index = GetAttributeIndex(ref AttributeName);
				if ( index >= 0){
					if (_Attributes == null){
						_Attributes = new string [ITEM_ATTRIBUTE_COUNT];
					}
					_Attributes[index] = value;
				}
			}
		}

		private int GetAttributeIndex(ref string AttributeName){
			int Index = -1;
			// Map name to an index value between 0 and ITEM_ATTRIBUTE_COUNT.
			
			switch (AttributeName.ToLower()){
				/* Mapping for mini profile attributes */
				case "username":	Index = 0; break;
				case "birthdate":	Index = 1; break;
				case "regionid":	Index = 2; break;
				case "gendermask":	Index = 3; break;
				case "aboutme":		Index = 4; break;
				case "headline":	Index = 5; break;
				case "thumbpath":	Index = 6; break;
				/* end Mapping for mini profile attributes */
			}
			return Index;
		}
		#endregion
	}
}
