using System;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for Preference.
	/// </summary>
	[Serializable]
	public class Preference : IValueObject
	{
		private Int32 _preferenceID;
		private Int32 _attributeID;
		private Int32 _preferenceTypeID;
		private bool _allowNull;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="preferenceID"></param>
		/// <param name="attributeID"></param>
		/// <param name="preferenceTypeID"></param>
		public Preference(Int32 preferenceID, Int32 attributeID, Int32 preferenceTypeID, bool allowNull)
		{
			_preferenceID = preferenceID;
			_attributeID = attributeID;
			_preferenceTypeID = preferenceTypeID;
			_allowNull = allowNull;
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 PreferenceID
		{
			get
			{
				return _preferenceID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 AttributeID
		{
			get
			{
				return _attributeID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 PreferenceTypeID
		{
			get
			{
				return _preferenceTypeID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool AllowNull
		{
			get
			{
				return _allowNull;
			}
		}
		}
}
