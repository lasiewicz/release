﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Search.ValueObjects
{
    [Serializable()]
    public class MatchRatingResult : IValueObject
    {
        public int MatchScore { get; set; }
        public double MatchScoreMember1 { get; set; }
        public double MatchScoreMember2 { get; set; }
        public int Member1ID { get; set; }
        public int Member2ID { get; set; }
        public Dictionary<string, string> MismatchesMember1 { get; set; }
        public Dictionary<string, string> MismatchesMember2 { get; set; }
        public Dictionary<string, string> MissingAttributeMember1 { get; set; }
        public Dictionary<string, string> MissingAttributeMember2 { get; set; }

        public MatchRatingResult()
        {
            MatchScore = 0;
            MatchScoreMember1 = 0;
            MatchScoreMember2 = 0;
            MismatchesMember1 = new Dictionary<string, string>();
            MismatchesMember2 = new Dictionary<string, string>();
            MissingAttributeMember1 = new Dictionary<string, string>();
            MissingAttributeMember2 = new Dictionary<string, string>();
        }
    }
}
