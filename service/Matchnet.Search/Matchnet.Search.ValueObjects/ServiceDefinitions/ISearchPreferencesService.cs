using System;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Search.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for ISearchPreferencesService.
	/// </summary>
	public interface ISearchPreferencesService
	{
		/// <summary>
		/// Retrieves a collection of search preferences based on the supplied parameters.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <returns></returns>
		ISearchPreferences GetSearchPreferences(int pMemberID, int pCommunityID);

		/// <summary>
		/// Saves a collection of search preferences for the associated session, member and community objects.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSearchPreferences"></param>
		void Save(
			int pMemberID
			, int pCommunityID
			, ISearchPreferences pSearchPreferences);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		PreferenceCollection GetPreferences();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		MemberSearchCollection GetMemberSearches(Int32 memberID, Int32 groupID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="searches"></param>
		void ProcessAction(IReplicationAction action);
	}
}
