﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Search.ValueObjects.ServiceDefinitions
{
    public interface IMemberSearchPreferencesService
    {
        void SaveMemberSearchCollection(Spark.SearchPreferences.MemberSearchCollection memberSearchCollection, int memberID, int communityID);
        Spark.SearchPreferences.MemberSearchCollection GetMemberSearchCollection(int memberID, int communityID);
    }
}
