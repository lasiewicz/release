﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects.ServiceDefinitions
{
    public interface ISearchPreferencesSA
    {
        SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID);
        SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID, bool ignoreSACache);
        void Save(int pMemberID, int pCommunityID, ISearchPreferences pSearchPreferences);
    }
}
