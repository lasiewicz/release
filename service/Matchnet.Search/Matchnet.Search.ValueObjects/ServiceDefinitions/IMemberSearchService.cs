using System;
using Matchnet;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Search.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IMemberSearchService interface definition.
	/// </summary>
	public interface IMemberSearchService
	{
		/// <summary>
		/// Executes a member search in accordance with the supplied parameters
		/// </summary>
		/// <param name="pPreferences">A search preference collection.</param>
		/// <param name="pCommunityID">The community or domain id for the search.</param>
		/// <param name="pSiteID">The site or private label id for the search.</param>
		/// <param name="pStartRow">A starting row or position</param>
		/// <param name="pPageSize">The page size or number of results to return.</param>
		/// <returns>A MemberSearchResults value object representing the identified members corresponding to the search criteria.</returns>
		IMatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID);
	}
}
