﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects.ServiceDefinitions
{
    public interface IMemberSearchSA
    {
        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType);

        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , bool ignoreSACache);

        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly);

        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly
            , bool ignoreAllCache);

        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize
            , int pMemberID
            , SearchEngineType pSearchEngineType
            , SearchType pSearchType
            , SearchEntryPoint pSearchEntryPoint
            , bool ignoreSACache
            , bool ignoreSAFilteredCacheOnly
            , bool ignoreAllCache);

        MatchnetQueryResults SearchForPushFlirtMatches(ISearchPreferences pPreferences, int memberID,
                                                               int communityID, int siteID, int brandID,
                                                               int numMatchesToReturn);


        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize);

        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID);

        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, bool ignoreSAFilteredCacheOnly, SearchType pSearchType);
        MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, bool ignoreSACache, bool ignoreSAFilteredCacheOnly, bool ignoreAllCache, SearchType pSearchType);

        ArrayList SearchDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID);

        ArrayList SearchExplainDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID);
    }
}
