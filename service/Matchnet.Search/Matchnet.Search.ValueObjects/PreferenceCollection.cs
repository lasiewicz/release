using System;
using System.Collections;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for PreferenceCollection.
	/// </summary>
	[Serializable]
	public class PreferenceCollection : IEnumerable, IValueObject, ICacheable
	{
		public const string CACHE_KEY = "PreferenceCollection";
		private Hashtable _preferences = new Hashtable();

		public PreferenceCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="preference"></param>
		public void Add(Preference preference)
		{
			_preferences.Add(preference.PreferenceID, preference);
		}

		/// <summary>
		/// 
		/// </summary>
		public Preference this[Int32 preferenceID]
		{
			get
			{
				return _preferences[preferenceID] as Preference;
			}
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _preferences.Values.GetEnumerator();
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return 3600;
			}
			set
			{
				throw new NotImplementedException("This method has not been implemented.");
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return CacheItemPriorityLevel.Normal;
			}
			set
			{
				throw new NotImplementedException("This method has not been implemented.");
			}
		}

		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
