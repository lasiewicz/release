using System;

namespace Matchnet.Search.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for DeleteSearchAction.
	/// </summary>
	[Serializable]
	public class DeletePreferenceAction : IReplicationAction
	{
		private Int32 _memberID;
		private Int32 _memberSearchID;
		private Int32 _preferenceID;

		public DeletePreferenceAction(Int32 memberID, Int32 memberSearchID, Int32 preferenceID)
		{
			_memberID = memberID;
			_memberSearchID = memberSearchID;
			_preferenceID = preferenceID;
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}

		public Int32 PreferenceID
		{
			get
			{
				return _preferenceID;
			}
		}
	}
}
