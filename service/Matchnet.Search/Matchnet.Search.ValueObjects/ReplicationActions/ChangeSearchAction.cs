using System;

namespace Matchnet.Search.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for DeleteSearchAction.
	/// </summary>
	[Serializable]
	public class ChangeSearchAction : IReplicationAction
	{
		private Int32 _memberSearchID;
		private Int32 _memberID;
		private Int32 _groupID;
		private string _searchName;
		private bool _isPrimary;

		public ChangeSearchAction(Int32 memberSearchID, Int32 memberID, Int32 groupID, string searchName, bool isPrimary)
		{
			_memberID = memberID;
			_memberSearchID = memberSearchID;
			_searchName = searchName;
			_isPrimary = isPrimary;
		}


		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}
		
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		public string SearchName
		{
			get
			{
				return _searchName;
			}
		}

		public bool IsPrimary
		{
			get
			{
				return _isPrimary;
			}
		}
	}
}
