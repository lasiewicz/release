using System;

namespace Matchnet.Search.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for DeleteSearchAction.
	/// </summary>
	[Serializable]
	public class DeleteSearchAction : IReplicationAction
	{
		private Int32 _memberID;
		private Int32 _memberSearchID;

		public DeleteSearchAction(Int32 memberID, Int32 memberSearchID)
		{
			_memberID = memberID;
			_memberSearchID = memberSearchID;
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}
	}
}
