using System;

namespace Matchnet.Search.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for DeleteSearchAction.
	/// </summary>
	[Serializable]
	public class ChangePreferenceAction : IReplicationAction
	{
		private Int32 _memberID;
		private Int32 _memberSearchID;
		private Int32 _preferenceID;
		private Int32 _weight;
		private string _value;
		private Int32 _minValue;
		private Int32 _maxValue;

		public ChangePreferenceAction(Int32 memberID, Int32 memberSearchID, Int32 preferenceID, Int32 weight, string val, Int32 minValue, Int32 maxValue)
		{
			_memberID = memberID;
			_memberSearchID = memberSearchID;
			_preferenceID = preferenceID;
			_weight = weight;
			_value = val;
			_minValue = minValue;
			_maxValue = maxValue;
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}

		public Int32 PreferenceID
		{
			get
			{
				return _preferenceID;
			}
		}

		public Int32 Weight
		{
			get
			{
				return _weight;
			}
		}

		public string Value
		{
			get
			{
				return _value;
			}
		}

		public Int32 MinValue
		{
			get
			{
				return _minValue;
			}
		}

		public Int32 MaxValue
		{
			get
			{
				return _maxValue;
			}
		}

	}
}
