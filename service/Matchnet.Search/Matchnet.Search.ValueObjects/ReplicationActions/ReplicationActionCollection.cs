using System;
using System.Collections;

namespace Matchnet.Search.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for ReplicationActionCollection.
	/// </summary>
	public class ReplicationActionCollection : IReplicationAction, IEnumerable
	{
		private ArrayList _actions = new ArrayList();

		public void Add(IReplicationAction action)
		{
			_actions.Add(action);
		}
		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _actions.GetEnumerator();
		}

		#endregion
	}
}
