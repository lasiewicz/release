using System;
using System.Collections;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Represents a full result set, must include result list , but may have other stuff such as drill down, 
	/// expansion, categories, timing etc.
	/// </summary>
	public interface IMatchnetQueryResults : ICacheable
	{
		/// <summary>
		/// Gets a list of items which represent query result items
		/// </summary>
		IMatchnetResultItems Items
		{
			get;
		}

	}

	/// <summary>
	/// Represents a list of items returned from a query
	/// </summary>
	public interface IMatchnetResultItems{
		/// <summary>
		/// Returns the whole collection of items
		/// </summary>
		ICollection List
		{
			get;
		}

		/// <summary>
		/// Returns the IMatchnetResultItem in [index] position (zero based).
		/// </summary>
		IMatchnetResultItem this[int index] 
		{
			get;
		}

		/// <summary>
		/// Number of items in this list
		/// </summary>
		int Count
		{
			get;
		}
	}

	/// <summary>
	/// Represents a member or profile
	/// </summary>
	public interface IMatchnetResultItem
	{
		/// <summary>
		/// Represents the member identifier associated with this result item.
		/// </summary>
		int MemberID
		{
			get;
		}
	}

}
