using System;
using System.Collections;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for DetailedQueryResult.
	/// </summary>
	[Serializable]
    public class DetailedQueryResult : IMatchnetResultItem
	{
		private Int32 _memberID;
		private Int32 _brandID;
		private Int32 _communityID;
		private Int32 _genderMask;
		private DateTime _activeDate;
		private DateTime _registerDate;
		private bool _isOnline;
		private bool _hasPhoto;
		private Int32 _score;
		private Int32 _totalScore;
		private Int32 _biasScore;
		private Int32 _matchScore;
		private Int32 _ageScore;
		private Int32 _ldgScore;
		private Int32 _csczScore;
		private Int32 _activityScore;
		private Int32 _popularityScore;
		private Int32 _heightScore;
		private Int32 _newScore;
		private string _thumbPath;
		private string _username;
        private int _colorcode = 0;
        private double _distance;

		private Hashtable _tags = new Hashtable();

		public DetailedQueryResult()
		{
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public Int32 BrandID
		{
			get
			{
				return _brandID;
			}
			set
			{
				_brandID = value;
			}
		}

		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

		public Int32 GenderMask
		{
			get
			{
				return _genderMask;
			}
			set
			{
				_genderMask = value;
			}
		}

		public DateTime ActiveDate
		{
			get
			{
				return _activeDate;
			}
			set
			{
				_activeDate = value;
			}
		}

		public DateTime RegisterDate
		{
			get
			{
				return _registerDate;
			}
			set
			{
				_registerDate = value;
			}
		}

		public bool IsOnline
		{
			get
			{
				return _isOnline;
			}
			set
			{
				_isOnline = value;
			}
		}

		public bool HasPhoto
		{
			get
			{
				return _hasPhoto;
			}
			set
			{
				_hasPhoto = value;
			}
		}

		public Int32 Score
		{
			get
			{
				return _score;
			}
			set
			{
				_score = value;
			}
		}

		public Int32 TotalScore
		{
			get
			{
				return _totalScore;
			}
			set
			{
				_totalScore = value;
			}
		}

		public Int32 BiasScore
		{
			get
			{
				return _biasScore;
			}
			set
			{
				_biasScore = value;
			}
		}

		public Int32 MatchScore
		{
			get
			{
				return _matchScore;
			}
			set
			{
				_matchScore = value;
			}
		}

		public Int32 AgeScore
		{
			get
			{
				return _ageScore;
			}
			set
			{
				_ageScore = value;
			}
		}

		public Int32 LdgScore
		{
			get
			{
				return _ldgScore;
			}
			set
			{
				_ldgScore = value;
			}
		}

		public Int32 CsczScore
		{
			get
			{
				return _csczScore;
			}
			set
			{
				_csczScore = value;
			}
		}

		public Int32 ActivityScore
		{
			get
			{
				return _activityScore;
			}
			set
			{
				_activityScore = value;
			}
		}

		public Int32 PopularityScore
		{
			get
			{
				return _popularityScore;
			}
			set
			{
				_popularityScore = value;
			}
		}

		public Int32 HeightScore
		{
			get
			{
				return _heightScore;
			}
			set
			{
				_heightScore = value;
			}
		}

		public Int32 NewScore
		{
			get
			{
				return _newScore;
			}
			set
			{
				_newScore = value;
			}
		}

		public string ThumbPath
		{
			get
			{
				return _thumbPath;
			}
			set
			{
				_thumbPath = value;
			}
		}

		public string UserName
		{
			get
			{
				return _username;
			}
			set
			{
				_username = value;
			}
		}

		public Hashtable Tags
		{
			get
			{
				return _tags;
			}
		}
		public Int32 ColorCode
		{
			get
			{
				return _colorcode;
			}
			set
			{
				_colorcode = value;
			}
		}

        public double Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }

        #region IMatchnetResultItem Members
        
        string IMatchnetResultItem.this[string AttributeName]
        {
            get
            {
                return string.Empty;
            }
            set
            {
            }
        }

        #endregion
    }
}
