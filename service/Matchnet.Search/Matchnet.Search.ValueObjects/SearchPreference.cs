using System;
using Matchnet;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// An eumeration for the SearchTypeID search preference that indicates the
	/// type of Member Search being performed (i.e. City, Postal code, Area Code, College, etc.)
	/// </summary>
	public enum SearchTypeID : int
	{
		/// <summary>
		/// Postal Code search type id (1)
		/// </summary>
		PostalCode = 1,
		/// <summary>
		/// Region search type id (4)
		/// </summary>
		Region = 4,
		/// <summary>
		/// AreaCode search type id (2)
		/// </summary>
		AreaCode = 2,
		/// <summary>
		/// College search type id (8)
		/// </summary>
		College = 8
	}



	/// <summary>
	/// SearchPreference is a name/value pair that represents a string/int of a search preference.
	/// </summary>
	[Serializable]
	public class SearchPreference : ISearchPreference, IValueObject, ICacheable
	{
		private string _name;
		private string _value;
		private bool _excludeFromSession;

		/// <summary>
		/// Constructor for a SearchPreference name/value pair
		/// </summary>
		/// <param name="thisName">The string name of the object</param>
		/// <param name="thisValue">The integer value of the object</param>
		public SearchPreference(string thisName, string thisValue)
		{
			Name = thisName;
			Value = thisValue;

			_excludeFromSession = false;
		}

		/// <summary>
		/// The Name of the value being represented
		/// </summary>
		public string Name
		{
			get {return _name;}
			set {_name = value;}
		}

		/// <summary>
		/// The value of the search preference.
		/// </summary>
		public string Value
		{
			get {return _value;}
			set {_value = value;}
		}

		/// <summary>
		/// This value is used to determine if the pair is to be stored in the session
		/// </summary>
		public bool ExcludeFromSession
		{
			get {return _excludeFromSession;}
			set {_excludeFromSession = value;}
		}		

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add SearchPreference.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add SearchPreference.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add SearchPreference.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add SearchPreference.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add SearchPreference.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add SearchPreference.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
