using System;
using System.Collections;

using Matchnet;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects 
{
	/// <summary>
	/// A query object containing Matchnet native valued parameters as query specification.
	/// </summary>
	[Serializable()]
	public class MatchnetQuery : IMatchnetQuery 
	{
		
		private ArrayList _Parameters;
		private SearchType _SearchType;

		/// <summary>
		/// Constructor
		/// </summary>
		public MatchnetQuery()
		{
			_Parameters  = new ArrayList();
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="paramcount">number of parameters expected</param>
		public MatchnetQuery(int paramcount)
		{
			_Parameters  = new ArrayList(paramcount);
		}

		#region IMatchnetQuery Members

		/// <summary>
		/// Adds a parameter to this query
		/// </summary>
		/// <param name="Parameter">The parameter to add</param>
		/// <returns>Ordinal location in the list where this object was insertd</returns>
		public int AddParameter(IMatchnetQueryParameter Parameter) {
			return _Parameters.Add(Parameter);
		}

        public int RemoveParameter(IMatchnetQueryParameter Parameter)
        {
            int idx = -1;
            if (_Parameters.Contains(Parameter))
            {
                idx = _Parameters.IndexOf(Parameter);
                _Parameters.Remove(Parameter);
            }
            return idx;
        }


		/// <summary>
		/// 
		/// </summary>
		public IMatchnetQueryParameter this[int index] 
		{
			get 
			{		
				return _Parameters[index] as IMatchnetQueryParameter;
			}
		}

		/// <summary>
		/// Number of parameters this object contains or can contain (if synthetically)
		/// </summary>
		public int Count 
		{
			get 
			{
				return _Parameters.Count;
			}
		}

		/// <summary>
		/// The type of query being performed.
		/// </summary>
		public SearchType SearchType
		{
			get
			{
				return _SearchType;
			}
			set
			{
				_SearchType = value;
			}
		}

		public override string ToString()
		{
			try
			{
				System.Text.StringBuilder bldr=new System.Text.StringBuilder();
				bldr.Append("SearchType: " + _SearchType.ToString());
				for(int i=0; i< _Parameters.Count;i++)
				{
					IMatchnetQueryParameter param=(IMatchnetQueryParameter)_Parameters[i];
					bldr.Append("\r\nParam: " +  param.Parameter.ToString() + ", Val:" + param.Value);
				}
				return bldr.ToString();
			}
			catch(Exception ex)
			{return "";}
		}

        public void Sort()
        {
            this._Parameters.Sort();
        }
		#endregion
    }

}
