using System;
using System.Collections;
using Matchnet;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// A collection that represents any number of SearchPreference objects.
	/// </summary>
	[Serializable]
	public class SearchPreferenceCollection : DictionaryBase, ISearchPreferences, IValueObject, ICacheable, IReplicable
	{
        public bool IsDefaultPreferences { get; set; }

		/// <summary>
		/// Constructor for SearchPreferenceCollection.
		/// </summary>
		public SearchPreferenceCollection()
		{
		    IsDefaultPreferences = false;
		}
		
		#region Public Methods

		/// <summary>
		/// Used to Add a SearchPreference to the current collection.
		/// </summary>
		/// <param name="searchPref">The SearchPreference object to be added at the end of the collection.</param>
		/// <returns>The System.Collections.ArrayList index at which the value has been added.</returns>
		public int Add(ISearchPreference searchPref)
		{
			if (searchPref == null)
			{
				throw(new ArgumentNullException("searchPref", "Cannot add null SearchPreference object to the SearchPreferenceCollection."));
			}

			if (this.InnerHashtable.ContainsKey(searchPref.Name.ToLower()))
			{
				this.InnerHashtable[searchPref.Name.ToLower()] = searchPref.Value;
			}
			else
			{
				this.InnerHashtable.Add(searchPref.Name.ToLower(), searchPref.Value);
			}
			return Constants.NULL_INT;
		}


		/// <summary>
		/// Overloaded Add will convert name value pair to a Search Preference object for addition to the collection.
		/// </summary>
		/// <param name="pName"></param>
		/// <param name="pValue"></param>
		/// <returns>The System.Collections.ArrayList index at which the value has been added.</returns>
		public int Add(string pName, string pValue)
		{
			return Add(new SearchPreference(pName, pValue));
		}

		/// <summary>
		/// The collection indexer for setting or returning values at a given index.
		/// </summary>
		public string this[string name]
		{
			get
			{
				string pref = null;
				if (this.InnerHashtable.ContainsKey(name.ToLower()))
				{
					pref = (string)this.InnerHashtable[name.ToLower()];
				}

				if (pref != null)
				{
					return pref;
				}
				else
				{
					return string.Empty;
				}
			}

			set
			{
				if (this.InnerHashtable.ContainsKey(name.ToLower()))
				{
					this.InnerHashtable[name.ToLower()] = value;
				}
				else
				{
					this.InnerHashtable.Add(name.ToLower(), value);
				}				
			}
		}

		/// <summary>
		/// Determines if the provided key exists in the current SearchPreferenceCollection.
		/// </summary>
		/// <param name="key">The name of the SearchPreference object to search for.</param>
		/// <returns>True if the key is not a null integer.</returns>
		public bool ContainsKey(string key)
		{
			return (this.InnerHashtable.ContainsKey(key.ToLower()));
		}

		/// <summary>
		/// Returns a collection of search preference keys for this instance.
		/// </summary>
		/// <returns></returns>
		public ICollection Keys()
		{
			return this.InnerHashtable.Keys;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		/// <summary>
		/// Validates the search preferences based on the existence of the SearchTypeID and SearchOrderBy preferences.
		/// </summary>
		/// <returns></returns>
		public ValidationResult Validate()
		{
			int iSearchTypeID = Conversion.CInt(this["SearchTypeID"]);
			int iSearchOrderBy = Conversion.CInt(this["SearchOrderBy"]);
			int iCountryRegionID = Conversion.CInt(this["CountryRegionID"]);
            int iRegionID = (this.ContainsKey("preferredregionid")) ? Conversion.CInt(this["preferredregionid"]) : Conversion.CInt(this["RegionID"]);
            int iDistance = (this.ContainsKey("preferreddistance")) ? Conversion.CInt(this["preferreddistance"]) : Conversion.CInt(this["Distance"]);
            int iRadius = (this.ContainsKey("preferredradius")) ? Conversion.CInt(this["preferredradius"]) : Conversion.CInt(this["Radius"]);

			if (iSearchTypeID == Constants.NULL_INT)
			{
				return new ValidationResult(PreferencesValidationError.SearchTypeIDRequired);
			}

			// SearchOrderBy  is always required.
			if (iSearchOrderBy == Constants.NULL_INT)
			{
				return new ValidationResult(PreferencesValidationError.SearchOrderByRequired);
			}

			// Validate College type
			if (iSearchTypeID == (int)SearchTypeID.College)
			{
				int iSchoolID = Conversion.CInt(this["SchoolID"]);
				if (iSchoolID == Constants.NULL_INT)
				{
					return new ValidationResult(PreferencesValidationError.SchoolIDRequired);
				}
				return ValidationResult.Success();
			}

			// Validate AreaCode
			if (iSearchTypeID == (int)SearchTypeID.AreaCode)
			{
				/*if (iCountryRegionID == Constants.NULL_INT)
				{
					return new ValidationResult(PreferencesValidationError.AreaCodeRequiresCountryRegionID);
				}*/

                if ((this.ContainsKey("preferredareacodes") && this["preferredareacodes"] == string.Empty) 
                    || (this.ContainsKey("AreaCode1") && this["AreaCode1"] == string.Empty))
				{
					return new ValidationResult(PreferencesValidationError.AreaCodeRequired);
				}
				return ValidationResult.Success();
			}

			// City / Region Search
			// RegionID is always required (need to compute Longitude and Latitude values).
			if (iRegionID <= 0) 
			{
				return new ValidationResult(PreferencesValidationError.RegionIDRequired);
			}

			// SearchOrderBy was tested for null above.
			if (iSearchOrderBy != (int)QuerySorting.Proximity)
			{
				if (iDistance == Constants.NULL_INT && iRadius == Constants.NULL_INT)
				{
					//either distance or radius can be used, but at least one must be present
                    return new ValidationResult(PreferencesValidationError.DistanceRequired);
				}
			}
			return ValidationResult.Success();
		}

		/// <summary>
		/// Access the value of a search preference by key.
		/// </summary>
		/// <param name="name"></param>
		/// <returns>Value corresponding to the search preference key.</returns>
		public string Value(string name) 
		{
			if (ContainsKey(name.ToLower()))
			{
				if (this.InnerHashtable[name.ToLower()] != null)
				{
					return this.InnerHashtable[name.ToLower()].ToString();
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion

		public  override string ToString()
		{
			try
			{
				System.Text.StringBuilder bldr=new System.Text.StringBuilder();
				
				foreach(string key in this.Keys())
				{
					bldr.Append("\r\nPref:" + key);
					bldr.Append("\r\nValue:" + this.InnerHashtable[key]);
				}
				return bldr.ToString();
			}
			catch(Exception ex)
			{return "error in ToString()";}  
		}

	}
}