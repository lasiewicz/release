using System;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for MemberSearchPreference.
	/// </summary>
	[Serializable]
	public class MemberSearchPreference
	{
		private Int32 _memberSearchID;
		private Int32 _preferenceID;
		private Int32 _weight = Constants.NULL_INT;
		private string _value = Constants.NULL_STRING;
		private Int32 _minValue = Constants.NULL_INT;
		private Int32 _maxValue = Constants.NULL_INT;

		private bool _isDirty = false;
		private MemberSearch _parent;

		public MemberSearchPreference(Int32 memberSearchID, Int32 preferenceID)
		{
			_memberSearchID = memberSearchID;
			_preferenceID = preferenceID;
		}

		internal void OnDirty()
		{
			_isDirty = true;
			if (Parent != null)
			{
				Parent.OnDirty();
			}
		}

		public Int32 MemberSearchID
		{
			get
			{
				return _memberSearchID;
			}
		}

		public Int32 PreferenceID
		{
			get
			{
				return _preferenceID;
			}
		}

		public Int32 Weight
		{
			get
			{
				return _weight;
			}
			set
			{
				_weight = value;
				_isDirty = true;
			}
		}

		public string Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
				_isDirty = true;
			}
		}

		public Int32 MinValue
		{
			get
			{
				return _minValue;
			}
			set
			{
				_minValue = value;
			}
		}

		public Int32 MaxValue
		{
			get
			{
				return _maxValue;
			}
			set
			{
				_maxValue = value;
			}
		}

		public bool IsNull
		{
			get
			{
				return (Weight == Constants.NULL_INT && Value == Constants.NULL_STRING && MinValue == Constants.NULL_INT && MaxValue == Constants.NULL_INT);
			}
		}

		internal MemberSearch Parent
		{
			get
			{
				return _parent;
			}
			set
			{
				_parent = value;
			}
		}

		public bool IsDirty
		{
			get
			{
				return _isDirty;
			}
		}
	}
}
