using System;
using System.Collections;

using Matchnet;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects 
{

	/// <summary>
	/// Represents query execution results
	/// </summary>
	[Serializable()]
	public class MatchnetQueryResults : IMatchnetQueryResults, IValueObject, ICacheable, IReplicable
	{
		private const string CACHE_KEY_PREFIX = "SearchResults";

		private MatchnetResultItems		_ResultItems;
		private bool					_MoreResultsAvailable;
		
		private string					_CacheKey;
		private int						_CacheTTLSeconds;
		private CacheItemPriorityLevel	_CacheItemPriorityLevel;	
		private int						_MatchesFound;

		/// <summary>
		/// Constructor. Creates an instance that can hold specified number of results.
		/// </summary>
		/// <param name="pResultCount">Number of results this object can contain</param>
		public MatchnetQueryResults(int pResultCount) 
		{
			_ResultItems = new MatchnetResultItems(pResultCount);
		}

		#region IQueryResults Members

		/// <summary>
		/// The list of result items
		/// </summary>
		public IMatchnetResultItems Items 
		{
			get 
			{	
				return _ResultItems;
			}
		}

		/// <summary>
		/// Represents the total number of items identified by the search engine.
		/// </summary>
		public int MatchesFound
		{
			get
			{
				return _MatchesFound;
			}
			set
			{
				_MatchesFound = value;
			}
		}

		/// <summary>
		/// Represents the number of items in this instance of query results.
		/// </summary>
		public int MatchesReturned
		{
			get
			{
				return _ResultItems.Count;
			}
		}
		#endregion

		/// <summary>
		/// Appends an item to the result list
		/// </summary>
		/// <param name="MemberID"></param>
		/// <returns></returns>
		public int AddResult(string MemberID)
		{
			return _ResultItems.AddResult(new MatchnetResultItem(Int32.Parse(MemberID)));
		}

        public int AddResult(int MemberID)
        {
            return _ResultItems.AddResult(new MatchnetResultItem(MemberID));
        }

        public int AddResult(int MemberID, int matchScore)
        {
            return _ResultItems.AddResult(new MatchnetResultItem(MemberID, matchScore));
        }

		/// <summary>
		/// Appends an item to the result list
		/// </summary>
		/// <param name="item">A result item to be appended</param>
		/// <returns></returns>
		public int AddResult(IMatchnetResultItem item){
			return _ResultItems.AddResult(item);
		}

		/// <summary>
		/// The unique key for this ICacheable value object.
		/// </summary>
		public string CacheKey
		{
			get { return _CacheKey; }
			set { _CacheKey = value; }
		}

		/// <summary>
		/// Indicates if more query results are available.
		/// </summary>
		public bool MoreResultsAvailable
		{
			get
			{
				return _MoreResultsAvailable;
			}
			set
			{
				_MoreResultsAvailable = value;
			}
		}

		/// <summary>
		/// Returns an ArrayList of the memberIDs that correspond to the MatchnetResultItems in 
		/// this Items collection.  The size of this array will comply with the Items.Count property.
		/// </summary>
		/// <returns>System.Collections.ArrayList containing the identified memberIds.</returns>
		public ArrayList ToArrayList()
		{
			ArrayList arrayList = new ArrayList(this.Items.Count);

			foreach (MatchnetResultItem item in this.Items.List)
			{
				arrayList.Add(item.MemberID);                   
			}

			return arrayList;
		}

        /// <summary>
        /// Returns an ArrayList of memberID and matchScore
        /// </summary>
        /// <returns></returns>
        public ArrayList ToArrayListOfMemberIDMatchscore()
        {
            ArrayList arrayList = new ArrayList(this.Items.Count);

            foreach (MatchnetResultItem item in this.Items.List)
            {
                arrayList.Add(new int[]{item.MemberID, item.MatchScore});
            }

            return arrayList;
        }

		#region ICacheable Members

		/// <summary>
		/// Represents the number of seconds this object should reside in the cache before expiration.
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{ 
				return _CacheTTLSeconds; 
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// CacheMode (Absolute)
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// Represents the Cache Priority for this object. 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CacheItemPriorityLevel;
			}
			set
			{
				_CacheItemPriorityLevel = value;
			}
		}

		/// <summary>
		/// Represents the cache key for this object (baed on Search Preferences, Community and Site IDs)
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		#endregion

		#region IReplicable Members

		/// <summary>
		/// Returns a replication placeholder initialized with the CacheKey of this item.
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
