using System;
using System.Collections;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for MemberSearchPreference.
	/// </summary>
	[Serializable]
	public class MemberSearchCollection : IValueObject, ICacheable, IReplicable, IEnumerable
	{
		private Int32 _memberID;
		private Int32 _groupID;

		private bool _isDirty = false;

		private Hashtable _searches = new Hashtable();

		public MemberSearchCollection(Int32 memberID, Int32 groupID)
		{
			_memberID = memberID;
			_groupID = groupID;
		}

		public void Add(MemberSearch memberSearch)
		{
			memberSearch.Parent = this;
			_searches.Add(memberSearch.MemberSearchID, memberSearch);
			OnDirty();
		}

		internal void OnDirty()
		{
			_isDirty = true;
		}

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		public bool IsDirty
		{
			get
			{
				return _isDirty;
			}
		}

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add MemberSearchCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add MemberSearchCollection.CacheTTLSeconds setter implementation
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add MemberSearchCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add MemberSearchCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add MemberSearchCollection.CachePriority setter implementation
			}
		}

		public string GetCacheKey()
		{
			// TODO:  Add MemberSearchCollection.GetCacheKey implementation
			return null;
		}

		#endregion

		#region IReplicable Members

		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			// TODO:  Add MemberSearchCollection.GetReplicationPlaceholder implementation
			return null;
		}

		#endregion

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _searches.Values.GetEnumerator();
		}

		#endregion
	}
}
