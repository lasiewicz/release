using System;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// A Matchnet Query Parameter
	/// </summary>
	[Serializable()]
	public class MatchnetQueryParameter : IMatchnetQueryParameter, IComparable
	{

		private QuerySearchParam _Parameter;
		private object _Value;
		private int _Weight;
		private QuerySortDirectionParam _Direction;

		/// <summary>
		/// 
		/// </summary>
		public MatchnetQueryParameter()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parameter"></param>
		/// <param name="Value"></param>
		public MatchnetQueryParameter(QuerySearchParam parameter, object Value)
		{
			_Parameter = parameter;
			_Value = Value;
		}

		#region IMatchnetQueryParameter Members

		/// <summary>
		/// 
		/// </summary>
		public QuerySearchParam Parameter 
		{
			get 
			{
				return _Parameter;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public QuerySortDirectionParam Direction 
		{
			get 
			{
				return _Direction;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Weight 
		{
			get 
			{
				return _Weight;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public object Value 
		{
			get 
			{
				return _Value;
			}
		}


		#endregion

        #region IComparable Members

        public int CompareTo(object obj)
        {
            MatchnetQueryParameter mqp = (MatchnetQueryParameter)obj;
            return this._Parameter.ToString().CompareTo(mqp._Parameter.ToString());
        }

        #endregion
    }
}
