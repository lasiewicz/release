using System;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Summary description for SearchPreferencesKey.
	/// </summary>
	public class SearchPreferencesKey
	{
		private const string CACHE_KEY_MEMBER	= "~PREFERENCES^{0}^{1}";
		private const string CACHE_KEY_SESSION	= "~PREFERENCES^{0}";
		private SearchPreferencesKey()
		{}

		/// <summary>
		/// Generates a unique key for caching an instance of SearchPreferenceCollection for a member / community.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int pMemberID, int pCommunityID)
		{
			return CACHE_KEY_MEMBER + "^" + pMemberID +  "^" + pCommunityID;
			//return string.Format(CACHE_KEY_MEMBER, pMemberID, pCommunityID);
		}

		/// <summary>
		/// Returns a cache key for preferences related to a user session.
		/// </summary>
		/// <param name="pSessionKey"></param>
		/// <returns></returns>
		public static string GetCacheKey(string pSessionKey)
		{
			return CACHE_KEY_SESSION + "^" + pSessionKey;
			//return String.Format(CACHE_KEY_SESSION, pSessionKey);
		}
	}
}
