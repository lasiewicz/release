using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// Represents a list of result items
	/// </summary>
	[Serializable()]
	public class MatchnetResultItems: IMatchnetResultItems, IValueObject, IEnumerable
	{
		private IMatchnetResultItem [] _ResultItems;
		private int NextItem = 0;

		#region IResultItems Members

		/// <summary>
		/// Constructor. Creates a result items list of the given size.
		/// </summary>
		/// <param name="ResultCount">The number of items this list will contain</param>
		public MatchnetResultItems(int ResultCount)
		{
			_ResultItems = new IMatchnetResultItem[ResultCount];
		}

		/// <summary>
		/// The list of items this object contains
		/// </summary>
		public ICollection List 
		{
			get 
			{
				return _ResultItems;
			}
		}

		/// <summary>
		/// Indexer
		/// </summary>
		public IMatchnetResultItem this[int index] 
		{
			get 
			{
				return _ResultItems[index];
			}
			set 
			{
				_ResultItems[index] = value;
			}
			
		}

		/// <summary>
		/// The number of items in the list
		/// </summary>
		public int Count 
		{
			get 
			{
				return _ResultItems.Length;
			}
		}

		#endregion

		#region IEnumerable implementation
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _ResultItems.GetEnumerator();
		}

		#endregion

		/// <summary>
		/// Adds a result to the list. 
		/// </summary>
		/// <param name="item">The MemberID to add to the listing of results.</param>
		/// <returns>ordinal number in the underlying list</returns>
		public int AddResult(IMatchnetResultItem item)
		{
			_ResultItems[NextItem] = item;
			return NextItem++;
		}

		/// <summary>
		/// Returns an ArrayList of MemberIDs representing this instance of result items.
		/// </summary>
		/// <returns></returns>
		public ArrayList ToArrayList()
		{
			ArrayList items = new ArrayList(_ResultItems.Length);
			IEnumerator members = List.GetEnumerator();
			while (members.MoveNext())
			{
				items.Add(((MatchnetResultItem)members.Current).MemberID);
			}
			return items;

		}

/*****************************************
 * Better serialization code. Awaiting opportune time to deploy and test
 * 
 * 
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(NextItem);
			if (NextItem > 0)
			{
				bool hasMiniProfiles = (_ResultItems[0]["username"] != String.Empty &&  _ResultItems[0]["username"] != null);
				bw.Write(hasMiniProfiles);

				if (hasMiniProfiles)
				{
					for (int i = 0; i < _ResultItems.Length; i++)
					{
						bw.Write(_ResultItems[i].MemberID);
						bw.Write(_ResultItems[i]["username"]);
						bw.Write(_ResultItems[i]["birthdate"]);
						bw.Write(_ResultItems[i]["regionid"]);
						bw.Write(_ResultItems[i]["gendermask"]);
						bw.Write(_ResultItems[i]["aboutme"]);
						bw.Write(_ResultItems[i]["headline"]);
						bw.Write(_ResultItems[i]["thumbpath"]);
					}
				}
				else 
				{
					for (int i = 0; i < _ResultItems.Length; i++)
					{
						bw.Write(_ResultItems[i].MemberID);
					}
				}
			} 

			info.AddValue("byteArray",ms.GetBuffer());
		}


		protected MatchnetResultItems(SerializationInfo info, StreamingContext context)
		{

			
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new System.IO.BinaryReader(ms);

			NextItem = br.ReadInt32();
			if (NextItem == 0) return;

			_ResultItems = new IMatchnetResultItem [NextItem];
			
			bool hasMiniProfile = br.ReadBoolean();

			if (hasMiniProfile) 
			{
				for ( int i = 0; i < _ResultItems.Length; i++)
				{
					MatchnetResultItem ri = new MatchnetResultItem(br.ReadInt32());
					ri["username"] = br.ReadString();
					ri["birthdate"] = br.ReadString();
					ri["regionid"] = br.ReadString();
					ri["gendermask"] = br.ReadString();
					ri["aboutme"] = br.ReadString();
					ri["headline"] = br.ReadString();
					ri["thumbpath"] = br.ReadString();

					_ResultItems[i] = ri;					
				}	
			}
			else 
			{
				for ( int i = 0; i < _ResultItems.Length; i++)
				{
					MatchnetResultItem ri = new MatchnetResultItem(br.ReadInt32());
					_ResultItems[i] = ri;
				}	
			}
		}
***********************************************/

	}



}


