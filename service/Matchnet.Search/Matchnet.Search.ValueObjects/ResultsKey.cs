using System;
using System.Collections;
using System.Text;
using Matchnet;

namespace Matchnet.Search.ValueObjects
{
	/// <summary>
	/// A helper class for generating a cache key for search results based on search preferences, community and site information.
	/// </summary>
	[Serializable()]
	public class ResultsKey
	{
        public enum ResultsCacheVersion
        {
            None = 0,
            Version1 = 1,
            Version2 = 2
        }

		private const string CACHE_KEY_PREFIX	= "~MEMBERQUERYRESULTS";
		private const string SEPARATOR			= "^";

		private const string AGEMAX				= "maxage";
		private const string AGEMIN				= "minage";
		private const string BODYTYPE			= "bodytype";
		private const string DRINKINGHABITS		= "drinkinghabits";
		private const string EDUCATIONLEVEL		= "educationlevel";
		private const string ETHNICITY			= "ethnicity";
		private const string GAYSCENEMASK		= "gayscenemask";
		private const string GENDERMASK			= "gendermask";
		private const string GEODISTANCE		= "geodistance";
		private const string HASPHOTOFLAG		= "hasphotoflag";
		private const string HEIGHTMAX			= "maxheight";
		private const string HEIGHTMIN			= "minheight";
		private const string JDATEETHNICITY		= "jdateethnicity";
		private const string JDATERELIGION		= "jdatereligion";
		private const string KEEPKOSHER			= "keepkosher";
		private const string LANGUAGEMASK		= "languagemask";
		private const string MAJORTYPE			= "majortype";
		private const string MARITALSTATUS		= "maritalstatus";
		private const string RELATIONSHIPMASK	= "relationshipmask";
		private const string RELATIONSHIPSTATUS = "relationshipstatus";
		private const string RELIGION			= "religion";
		private const string SEARCHTYPE			= "searchtype";
		private const string SEXUALIDENTITYTYPE = "sexualidentitytype";
		private const string SMOKINGHABITS		= "smokinghabits";
		private const string SYNAGOGUEATTENDANCE= "synagogueattendance";
		private const string ZODIAC				= "zodiac";
		private const string WEIGHTMIN			= "weightmin";
		private const string WEIGHTMAX			= "weightmax";
		private const string CHILDRENCOUNT		= "childrencount";
		private const string RELOCATEFLAG		= "relocateflag";
		private const string ACTIVITYLEVEL		= "activitylevel";
        private const string CUSTODY			= "custody";
		private const string MORECHILDRENFLAG	= "morechildrenflag";
		private const string COLORCODE	= "colorcode";
        private const string SEARCHREDESIGN30 = "searchredesign30";
        private const string RAMAHALUM = "ramahalum";
        private const string ONLINE = "online";
        public const string PREFERREDAGE = "preferredage";
        public const string PREFERREDDISTANCE = "preferreddistance";
        public const string PREFERREDGENDERMASK = "preferredgendermask";
        public const string PREFERREDRELOCATION = "preferredrelocation";
        public const string PREFERREDHEIGHT = "preferredheight";
        public const string PREFERREDSYNAGOGUEATTENDANCE = "preferredsynagogueattendance";
        public const string PREFERREDJDATERELIGION = "preferredjdatereligion";
        public const string PREFERREDSMOKINGHABIT = "preferredsmokinghabit";
        public const string PREFERREDDRINKINGHABIT = "preferreddrinkinghabit";
        public const string PREFERREDJDATEETHNICITY = "preferredjdateethnicity";
        public const string PREFERREDACTIVITYLEVEL = "preferredactivitylevel";
        public const string PREFERREDKOSHERSTATUS = "preferredkosherstatus";
        public const string PREFERREDLANGUAGEMASK = "preferredlanguagemask";
        public const string PREFERREDREGIONID = "preferredregionid";
        public const string PREFERREDHASPHOTOFLAG = "preferredhasphotoflag";
        public const string PREFERREDEDUCATIONLEVEL = "preferrededucationlevel";
        public const string PREFERREDMORECHILDRENFLAG = "preferredmorechildrenflag";
        public const string PREFERREDCHILDRENCOUNT = "preferredchildrencount";
        public const string KEYWORDSEARCH = "keywordsearch";
        public const string BLOCKEDMEMBERIDS = "blockedmemberids";
        public const string EXCLUDEYNMNOFLAG = "excludeynmnoflag";
	    public const string EXCLUDEYNMMEMBERIDS = "excludeynmmemberids";
        public const string HASMEMBERID = "hasMemberID";
        public const string PREF_VALUE_PLACEHOLDER = "^_";


	    private ResultsKey()
		{}

		/// <summary>
		/// Returns a cache key based on the values of SearchPreferences, CommunityID and SiteID.
		/// </summary>
		/// <param name="pSearchPreferences"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSiteID"></param>
		/// <returns></returns>
		public static string GetCacheKey(SearchPreferenceCollection pSearchPreferences
			, int pCommunityID
			, int pSiteID)
		{
			SearchTypeID searchTypeID = (SearchTypeID)Convert.ToInt32(pSearchPreferences["SearchTypeID"]);
			
			string cacheKey = CACHE_KEY_PREFIX + SEPARATOR + searchTypeID
				+ SEPARATOR + pSearchPreferences[SEARCHTYPE]
				+ SEPARATOR + pSearchPreferences["SearchOrderBy"]
				+ SEPARATOR	+ pCommunityID
				+ SEPARATOR + pSiteID;

			switch (searchTypeID)
			{
				case SearchTypeID.AreaCode:
					cacheKey += SEPARATOR + pSearchPreferences["CountryRegionID"];
					cacheKey += SEPARATOR + pSearchPreferences["AreaCodes"];
					break;
				
				case SearchTypeID.College:
					cacheKey += SEPARATOR + pSearchPreferences["CountryRegionID"];
					cacheKey += SEPARATOR + pSearchPreferences["SchoolID"];
					break;

				default:
					cacheKey += SEPARATOR + pSearchPreferences["CountryRegionID"];
					cacheKey += SEPARATOR + pSearchPreferences["RegionIDCity"];
					cacheKey += SEPARATOR + pSearchPreferences["RegionID"];
					break;
			}

			// Now process the remaining search preferences that can influence the search results.
			cacheKey += appendPreferenceValue(pSearchPreferences, AGEMIN);
			cacheKey += appendPreferenceValue(pSearchPreferences, AGEMAX);
			cacheKey += appendPreferenceValue(pSearchPreferences, GENDERMASK);
			cacheKey += appendPreferenceValue(pSearchPreferences, BODYTYPE);
			cacheKey += appendPreferenceValue(pSearchPreferences, DRINKINGHABITS);
			cacheKey += appendPreferenceValue(pSearchPreferences, EDUCATIONLEVEL);
			cacheKey += appendPreferenceValue(pSearchPreferences, ETHNICITY);
			cacheKey += appendPreferenceValue(pSearchPreferences, GEODISTANCE);
			cacheKey += appendPreferenceValue(pSearchPreferences, HASPHOTOFLAG);
			cacheKey += appendPreferenceValue(pSearchPreferences, HEIGHTMAX);
			cacheKey += appendPreferenceValue(pSearchPreferences, HEIGHTMIN);
			cacheKey += appendPreferenceValue(pSearchPreferences, JDATEETHNICITY);
			cacheKey += appendPreferenceValue(pSearchPreferences, JDATERELIGION);
			cacheKey += appendPreferenceValue(pSearchPreferences, KEEPKOSHER);
			cacheKey += appendPreferenceValue(pSearchPreferences, LANGUAGEMASK);
			cacheKey += appendPreferenceValue(pSearchPreferences, MAJORTYPE);
			cacheKey += appendPreferenceValue(pSearchPreferences, MARITALSTATUS);
			cacheKey += appendPreferenceValue(pSearchPreferences, RELATIONSHIPMASK);
			cacheKey += appendPreferenceValue(pSearchPreferences, RELATIONSHIPSTATUS);
			cacheKey += appendPreferenceValue(pSearchPreferences, RELIGION);
			cacheKey += appendPreferenceValue(pSearchPreferences, SEXUALIDENTITYTYPE);
			cacheKey += appendPreferenceValue(pSearchPreferences, SMOKINGHABITS);
			cacheKey += appendPreferenceValue(pSearchPreferences, SYNAGOGUEATTENDANCE);
			cacheKey += appendPreferenceValue(pSearchPreferences, ZODIAC);
			cacheKey += appendPreferenceValue(pSearchPreferences, WEIGHTMIN);
			cacheKey += appendPreferenceValue(pSearchPreferences, WEIGHTMAX);
			cacheKey += appendPreferenceValue(pSearchPreferences, CHILDRENCOUNT);
			cacheKey += appendPreferenceValue(pSearchPreferences, RELOCATEFLAG);
			cacheKey += appendPreferenceValue(pSearchPreferences, ACTIVITYLEVEL);
			cacheKey += appendPreferenceValue(pSearchPreferences, CUSTODY);
            cacheKey += appendPreferenceValue(pSearchPreferences, MORECHILDRENFLAG);
            cacheKey += appendPreferenceValue(pSearchPreferences, RAMAHALUM);
            cacheKey += appendPreferenceValue(pSearchPreferences, ONLINE);

			if(Conversion.CInt(pSearchPreferences[COLORCODE])  > 0)
			{
				cacheKey += appendPreferenceValue(pSearchPreferences, COLORCODE);
			}

            if (Conversion.CInt(pSearchPreferences[SEARCHREDESIGN30]) > 0)
            {
                cacheKey += appendPreferenceValue(pSearchPreferences, SEARCHREDESIGN30);
            }

            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDAGE);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDDISTANCE);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDGENDERMASK);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDRELOCATION);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDHEIGHT);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDSYNAGOGUEATTENDANCE);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDJDATERELIGION);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDSMOKINGHABIT);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDDRINKINGHABIT);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDJDATEETHNICITY);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDACTIVITYLEVEL);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDKOSHERSTATUS);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDLANGUAGEMASK);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDREGIONID);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDHASPHOTOFLAG);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDEDUCATIONLEVEL);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDMORECHILDRENFLAG);
            cacheKey += appendPreferenceValue(pSearchPreferences, PREFERREDCHILDRENCOUNT);

            //keyword changes should run a new search
            if (pSearchPreferences.ContainsKey(KEYWORDSEARCH) && !string.IsNullOrEmpty(pSearchPreferences[KEYWORDSEARCH]))
            {
                cacheKey += HashString(pSearchPreferences[KEYWORDSEARCH]);
            }
            else
            {
                cacheKey += PREF_VALUE_PLACEHOLDER;
            }

            //blocked member id changes should run a new search
            if (pSearchPreferences.ContainsKey(BLOCKEDMEMBERIDS) && !string.IsNullOrEmpty(pSearchPreferences[BLOCKEDMEMBERIDS]))
            {
                cacheKey += HashString(pSearchPreferences[BLOCKEDMEMBERIDS]);
            }
            else
            {
                cacheKey += PREF_VALUE_PLACEHOLDER;
            }

            //match rating calculation should run a new search (member specific cache)
            if (pSearchPreferences.ContainsKey(HASMEMBERID) && !string.IsNullOrEmpty(pSearchPreferences[HASMEMBERID]))
            {
                cacheKey += appendPreferenceValue(pSearchPreferences, HASMEMBERID);
            }

            //ynm NO filter is member specific, should run a new search 
            if (pSearchPreferences.ContainsKey(EXCLUDEYNMMEMBERIDS) && !string.IsNullOrEmpty(pSearchPreferences[EXCLUDEYNMMEMBERIDS]))
            {
                cacheKey += HashString(pSearchPreferences[EXCLUDEYNMMEMBERIDS]);
            }

            return cacheKey;
		}

		private static string appendPreferenceValue(SearchPreferenceCollection pSearchPreferences, string pKey)
		{
			int prefValue = Conversion.CInt(pSearchPreferences[pKey]);

			if (prefValue != Constants.NULL_INT)
			{
				return SEPARATOR + prefValue;
			}
			else
			{
				return PREF_VALUE_PLACEHOLDER;
			}
		}

        private static string HashString(string Value)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
            data = x.ComputeHash(data);
            string ret = "";
            for (int i = 0; i < data.Length; i++)
                ret += data[i].ToString("x2").ToLower();
            return ret;
        }

	}
}
