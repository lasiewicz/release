﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Search.ValueObjects.Exceptions
{
    public class InvalidSearchPreferencesException:ApplicationException
    {
        
        public InvalidSearchPreferencesException(ValidationResult validationResult): base("SearchPreference collection missing required fields." + validationResult.ValidationError.ToString())
        {
            
        }

        public InvalidSearchPreferencesException(string message, ValidationResult validationResult): base(message + validationResult.ValidationError.ToString())
        {

        }

        public InvalidSearchPreferencesException(string message, Exception innerException, ValidationResult validationResult)
            : base(message + validationResult.ValidationError.ToString(), innerException)
        {
             
        }
    }
}
