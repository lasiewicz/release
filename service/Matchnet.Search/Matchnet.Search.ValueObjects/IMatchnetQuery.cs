using System;
using System.Collections;

namespace Matchnet.Search.ValueObjects 
{
	/// <summary>
	/// Summary description for IMatchnetQuery.
	/// </summary>
	public interface IMatchnetQuery 
	{
		/// <summary>
		/// Adds a query paraemter. This parameter may be used for filtering or for ranking of results.
		/// </summary>
		/// <param name="Parameter">The target parameter</param>
		/// <returns>The ordinal ID of where the parameter was inserted</returns>
		int AddParameter(IMatchnetQueryParameter Parameter);


		/// <summary>
		/// Indexer
		/// </summary>
		IMatchnetQueryParameter this[int index] 
		{
			get;
		}

		/// <summary>
		/// Represents the number of parameters contained within this instance.
		/// </summary>
		int Count
		{
			get;
		}

	}

	/// <summary>
	/// 
	/// </summary>
	public interface IMatchnetQueryParameter
	{
		/// <summary>
		/// The Matchnet.Search.ValueObjects.Enumerations.QuerySearchParam value for this parameter.
		/// </summary>
		QuerySearchParam Parameter 
		{ 
			get; 
		}

		/// <summary>
		/// The Matchnet.Search.ValueObjects.Enumerations.QuerySortDirection value for this parameter.
		/// </summary>
		QuerySortDirectionParam Direction 
		{ 
			get;
		}

		/// <summary>
		/// The weighted value of this parameter.
		/// </summary>
		int Weight
		{ 
			get; 
		}

		/// <summary>
		/// The value of this query parameter.
		/// </summary>
		object Value 
		{ 
			get;
		}
	}

	
}