using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using System.IO;
namespace TestClient
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox txtBrandID;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.TextBox txtExpDate;
		private System.Windows.Forms.CheckBox chkEnabled;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.CheckBox chkAsync;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox txtFile;
		private System.Windows.Forms.TextBox txtThreadNum;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnLoadFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox txtURI;
		private System.Windows.Forms.Label URI;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.CheckBox chkUseURI;

		ArrayList _members=new ArrayList();
		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			txtURI.Text="tcp://bhd-skener:49500/SpotlightMemberSM.rem";

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtBrandID = new System.Windows.Forms.TextBox();
			this.txtMemberID = new System.Windows.Forms.TextBox();
			this.txtExpDate = new System.Windows.Forms.TextBox();
			this.chkEnabled = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.chkAsync = new System.Windows.Forms.CheckBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.txtFile = new System.Windows.Forms.TextBox();
			this.txtThreadNum = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnLoadFile = new System.Windows.Forms.Button();
			this.txtURI = new System.Windows.Forms.TextBox();
			this.URI = new System.Windows.Forms.Label();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.chkUseURI = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// txtBrandID
			// 
			this.txtBrandID.Location = new System.Drawing.Point(88, 32);
			this.txtBrandID.Name = "txtBrandID";
			this.txtBrandID.TabIndex = 0;
			this.txtBrandID.Text = "";
			// 
			// txtMemberID
			// 
			this.txtMemberID.Location = new System.Drawing.Point(88, 72);
			this.txtMemberID.Name = "txtMemberID";
			this.txtMemberID.TabIndex = 1;
			this.txtMemberID.Text = "";
			// 
			// txtExpDate
			// 
			this.txtExpDate.Location = new System.Drawing.Point(88, 112);
			this.txtExpDate.Name = "txtExpDate";
			this.txtExpDate.TabIndex = 2;
			this.txtExpDate.Text = "";
			// 
			// chkEnabled
			// 
			this.chkEnabled.Location = new System.Drawing.Point(88, 144);
			this.chkEnabled.Name = "chkEnabled";
			this.chkEnabled.TabIndex = 3;
			this.chkEnabled.Text = "Enabled";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(40, 280);
			this.button1.Name = "button1";
			this.button1.TabIndex = 4;
			this.button1.Text = "Save";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// chkAsync
			// 
			this.chkAsync.Location = new System.Drawing.Point(224, 144);
			this.chkAsync.Name = "chkAsync";
			this.chkAsync.TabIndex = 6;
			this.chkAsync.Text = "Async";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(136, 280);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(128, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "Threaded test";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(440, 216);
			this.button3.Name = "button3";
			this.button3.TabIndex = 9;
			this.button3.Text = "Browse";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// txtFile
			// 
			this.txtFile.Location = new System.Drawing.Point(80, 216);
			this.txtFile.Name = "txtFile";
			this.txtFile.Size = new System.Drawing.Size(336, 20);
			this.txtFile.TabIndex = 8;
			this.txtFile.Text = "";
			// 
			// txtThreadNum
			// 
			this.txtThreadNum.Location = new System.Drawing.Point(80, 240);
			this.txtThreadNum.Name = "txtThreadNum";
			this.txtThreadNum.Size = new System.Drawing.Size(120, 20);
			this.txtThreadNum.TabIndex = 10;
			this.txtThreadNum.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 240);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 24);
			this.label1.TabIndex = 11;
			this.label1.Text = "Threads#";
			// 
			// btnLoadFile
			// 
			this.btnLoadFile.Location = new System.Drawing.Point(528, 216);
			this.btnLoadFile.Name = "btnLoadFile";
			this.btnLoadFile.Size = new System.Drawing.Size(64, 24);
			this.btnLoadFile.TabIndex = 12;
			this.btnLoadFile.Text = "Load file";
			this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
			// 
			// txtURI
			// 
			this.txtURI.Location = new System.Drawing.Point(80, 176);
			this.txtURI.Name = "txtURI";
			this.txtURI.Size = new System.Drawing.Size(336, 20);
			this.txtURI.TabIndex = 13;
			this.txtURI.Text = "";
			// 
			// URI
			// 
			this.URI.Location = new System.Drawing.Point(16, 184);
			this.URI.Name = "URI";
			this.URI.Size = new System.Drawing.Size(56, 16);
			this.URI.TabIndex = 14;
			this.URI.Text = "URI";
			// 
			// chkUseURI
			// 
			this.chkUseURI.Location = new System.Drawing.Point(448, 176);
			this.chkUseURI.Name = "chkUseURI";
			this.chkUseURI.TabIndex = 15;
			this.chkUseURI.Text = "Use URI";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(600, 381);
			this.Controls.Add(this.chkUseURI);
			this.Controls.Add(this.URI);
			this.Controls.Add(this.txtURI);
			this.Controls.Add(this.btnLoadFile);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtThreadNum);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.txtFile);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.chkAsync);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.chkEnabled);
			this.Controls.Add(this.txtExpDate);
			this.Controls.Add(this.txtMemberID);
			this.Controls.Add(this.txtBrandID);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				int brandid = Int32.Parse(txtBrandID.Text);
				int memberid = Int32.Parse(txtMemberID.Text);
				DateTime expDate = DateTime.Parse(txtExpDate.Text);
				bool enable = chkEnabled.Checked;
				
				if(chkUseURI.Checked)
				{
					SpotlightMemberSA.Instance.TestURI=txtURI.Text;
				}
				
				SpotlightMemberSA.Instance.SaveMember(brandid, memberid, expDate, enable, chkAsync.Checked);
				

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			try
			{
				int threadcount=Int32.Parse(txtThreadNum.Text);
				for(int i=0;i < threadcount;i++)
				{
					System.Threading.ThreadStart threadDelegate = new System.Threading.ThreadStart(runThread);
					System.Threading.Thread t=new System.Threading.Thread(threadDelegate);
					t.Start();
				}

			}
			catch(Exception ex)
			{ MessageBox.Show(ex.ToString());}
		}

		private void btnLoadFile_Click(object sender, System.EventArgs e)
		{
			TextReader reader=null;

			reader=new StreamReader(txtFile.Text);
			string memberid=reader.ReadLine();
			while(memberid != null)
			{
				
				_members.Add(memberid);
				memberid=reader.ReadLine();
			}
			reader.Close();
		}



		private void runThread()
		{
			string uri=txtURI.Text;
			int brandid=  Int32.Parse(txtBrandID.Text);
			DateTime expDate=DateTime.Parse(txtExpDate.Text);
			for(int i=0;i < _members.Count; i++)
			{
				int mid=Int32.Parse( _members[i].ToString());
				ISpotlightMember11 serviceSpotlight=(ISpotlightMember11)Activator.GetObject(typeof(ISpotlightMember11), uri);
				serviceSpotlight.SaveMember11(brandid,mid,expDate,chkEnabled.Checked,chkAsync.Checked);
				}

		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			openFileDialog1 = new OpenFileDialog();
			openFileDialog1.ShowDialog();
			txtFile.Text = openFileDialog1.FileName;
		}
		
	}
}
