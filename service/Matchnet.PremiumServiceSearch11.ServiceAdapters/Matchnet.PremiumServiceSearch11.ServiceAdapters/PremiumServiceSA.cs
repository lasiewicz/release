using System;
using System.Collections;

using Matchnet.RemotingClient;
using Matchnet.Member.ServiceAdapters;
using System.Collections.Generic;

namespace Matchnet.PremiumServiceSearch11.ServiceAdapters
{
	/// <summary>
	/// Summary description for PremiumServiceSA.
	/// </summary>
	public class PremiumServiceSA : SABase
	{
		public static readonly PremiumServiceSA Instance = new PremiumServiceSA();

		private PremiumServiceSA()
		{

		}

		/// <summary>
		/// Used only to check whether the member has a valid highlighted profile premium
		/// service.  If so, then the member will be allowed to enable or disable highlighted 
		/// profile in member services.  
		/// </summary>
		/// <param name="memberID">Member ID being checked</param>
		/// <param name="brand">Current brand</param>
		/// <returns>True if the member currently has the highlighted profile and is not expired or false if the member does not have the highlighted profile. </returns>
		public bool MemberHasHighlightProfilePremiumService(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			try
			{
				Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteHighlightProfileExpirationDate = privilege.EndDatePST;//objMember.GetAttributeDate(brand, "HighlightedExpirationDate", DateTime.MinValue);

                if (dteHighlightProfileExpirationDate == DateTime.MinValue)
                {
                    // Member does not have the highlighted profile
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteHighlightProfileExpirationDate)
                    {
                        // Highlighted profile purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // Highlighted profile purchase has expired  
                        return false;
                    }
                }

			}
			catch (Exception ex)
			{
				// Member may not exists or attribute may not exists for this member
				return false;  
			}
		}

		/// <summary>
		/// Used only to check whether the member has a valid JMeter premium
		/// service.  If so, then the member will be allowed to enable or disable JMeter 
		/// in member services.  
		/// </summary>
		/// <param name="memberID">Member ID being checked</param>
		/// <param name="brand">Current brand</param>
		/// <returns>True if the member currently has JMeter and is not expired or false if the member does not have JMeter. </returns>
		public bool MemberHasJMeterPremiumService(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			try
			{
				Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.JMeter, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteJMeterExpirationDate = privilege.EndDatePST;//objMember.GetAttributeDate(brand, "JMeterExpirationDate", DateTime.MinValue);

                if (dteJMeterExpirationDate == DateTime.MinValue)
                {
                    // Member does not have JMeter
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteJMeterExpirationDate)
                    {
                        // JMeter purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // JMeter purchase has expired  
                        return false;
                    }
                }

			}
			catch (Exception ex)
			{
				// Member may not exists or attribute may not exists for this member
				return false;  
			}
		}

		/// <summary>
		/// Used only to check whether the member has a valid spotlight member premium
		/// service.  If so, then the member will be allowed to enable or disable spotlight 
		/// member in member services.  
		/// </summary>
		/// <param name="memberID">Member ID being checked</param>
		/// <param name="brand">Current brand</param>
		/// <returns>True if the member currently has spotlight member and is not expired or false if the member does not have spotlight member. </returns>
		public bool MemberHasSpotlightMemberPremiumService(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			try
			{
				Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                Spark.Common.AccessService.AccessPrivilege privilege = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteSpotlightMemberExpirationDate = privilege.EndDatePST;//objMember.GetAttributeDate(brand, "SpotlightExpirationDate", DateTime.MinValue);

                if (dteSpotlightMemberExpirationDate == DateTime.MinValue)
                {
                    // Member does not have spotlight member
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteSpotlightMemberExpirationDate)
                    {
                        // Spotlight member purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // Spotlight member purchase has expired  
                        return false;
                    }
                }
			}
			catch (Exception ex)
			{
				// Member may not exists or attribute may not exists for this member
				return false;  
			}
		}

        /// <summary>
        /// Used only to check whether the member has a valid all access premium
        /// service.  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the member currently has the all access premium service and is not expired or false if the member does not have spotlight member. </returns>
        public bool MemberHasAllAccessPremiumService(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteAllAccessExpirationDate = privilege.EndDatePST;

                if (dteAllAccessExpirationDate == DateTime.MinValue)
                {
                    // Member does not have all access premium service 
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteAllAccessExpirationDate)
                    {
                        // All access premium service purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // All access premium service purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        /// <summary>
        /// Used only to check whether the member has all access emails premium
        /// service.  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the member currently has the all access emails premium service and is not expired, else false </returns>
        public bool MemberHasAllAccessEmailsPremiumService(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.AllAccessEmails, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                if (privilege.RemainingCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        /// <summary>
        /// Used only to check whether the member has a valid read receipts
        /// service.  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the member currently has the read receipts that hasn't expired, else false </returns>
        public bool MemberHasReadReceiptsPremiumService(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Matchnet.Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = objMember.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.ReadReceipt, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteExpirationDate = privilege.EndDatePST;

                if (dteExpirationDate == DateTime.MinValue)
                {
                    // Member does not have premium service 
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteExpirationDate)
                    {
                        // Read Receipts premium service purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // Read Receipts premium service purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

		/// <summary>
		/// Returns a boolean value of whether this member currently has a valid 
		/// premium plan subscription  
		/// </summary>
		/// <param name="memberID">Member ID being checked</param>
		/// <param name="brand">Current brand</param>
		/// <returns>True if the member currently has a valid premium plan subscription or false if the member has not subscribed to the premium plan or has expired</returns>
		public ArrayList GetMemberActivePremiumServices(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			try
			{
				ArrayList arrActivePremiumServices = new ArrayList();

				if (this.MemberHasHighlightProfilePremiumService(memberID, brand))
				{
					arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.HighlightedProfile.ToString());
				}

				if (this.MemberHasSpotlightMemberPremiumService(memberID, brand))
				{
					arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.SpotlightMember.ToString());
				}

				if(this.MemberHasJMeterPremiumService(memberID, brand))
				{
					arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.JMeter.ToString());
				}

                if (this.MemberHasAllAccessPremiumService(memberID, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.AllAccess.ToString());
                }

                if (this.MemberHasAllAccessEmailsPremiumService(memberID, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.AllAccessEmails.ToString());
                }

                if (this.MemberHasReadReceiptsPremiumService(memberID, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.ReadReceipt.ToString());
                }

				return arrActivePremiumServices;	
			}
			catch (Exception ex)
			{
				// Member may not exists or attribute may not exists for this member
				return null;
			}
		}

        #region Method overloads accepting Member as input instead of MemberId. This is to avoid calls to MemberSA and to Membase

        public ArrayList GetMemberActivePremiumServices(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                ArrayList arrActivePremiumServices = new ArrayList();

                if (this.MemberHasHighlightProfilePremiumService(member, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.HighlightedProfile.ToString());
                }

                if (this.MemberHasSpotlightMemberPremiumService(member, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.SpotlightMember.ToString());
                }

                if (this.MemberHasJMeterPremiumService(member, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.JMeter.ToString());
                }

                if (this.MemberHasAllAccessPremiumService(member, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.AllAccess.ToString());
                }

                if (this.MemberHasAllAccessEmailsPremiumService(member, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.AllAccessEmails.ToString());
                }

                if (this.MemberHasReadReceiptsPremiumService(member, brand))
                {
                    arrActivePremiumServices.Add(Spark.Common.AccessService.PrivilegeType.ReadReceipt.ToString());
                }

                return arrActivePremiumServices;
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return null;
            }
        }

        public bool MemberHasHighlightProfilePremiumService(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteHighlightProfileExpirationDate = privilege.EndDatePST;//objMember.GetAttributeDate(brand, "HighlightedExpirationDate", DateTime.MinValue);

                if (dteHighlightProfileExpirationDate == DateTime.MinValue)
                {
                    // Member does not have the highlighted profile
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteHighlightProfileExpirationDate)
                    {
                        // Highlighted profile purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // Highlighted profile purchase has expired  
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        public bool MemberHasSpotlightMemberPremiumService(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteSpotlightMemberExpirationDate = privilege.EndDatePST;//objMember.GetAttributeDate(brand, "SpotlightExpirationDate", DateTime.MinValue);

                if (dteSpotlightMemberExpirationDate == DateTime.MinValue)
                {
                    // Member does not have spotlight member
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteSpotlightMemberExpirationDate)
                    {
                        // Spotlight member purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // Spotlight member purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        public bool MemberHasJMeterPremiumService(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.JMeter, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteJMeterExpirationDate = privilege.EndDatePST;//objMember.GetAttributeDate(brand, "JMeterExpirationDate", DateTime.MinValue);

                if (dteJMeterExpirationDate == DateTime.MinValue)
                {
                    // Member does not have JMeter
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteJMeterExpirationDate)
                    {
                        // JMeter purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // JMeter purchase has expired  
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        public bool MemberHasAllAccessPremiumService(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteAllAccessExpirationDate = privilege.EndDatePST;

                if (dteAllAccessExpirationDate == DateTime.MinValue)
                {
                    // Member does not have all access premium service 
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteAllAccessExpirationDate)
                    {
                        // All access premium service purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // All access premium service purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        public bool MemberHasAllAccessEmailsPremiumService(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.AllAccessEmails, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                if (privilege.RemainingCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        /// <summary>
        /// Used only to check whether the member has a valid read receipts
        /// service.  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns>True if the member currently has the read receipts that hasn't expired, else false </returns>
        public bool MemberHasReadReceiptsPremiumService(Member.ServiceAdapters.Member member, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.ReadReceipt, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                if (privilege == null)
                {
                    return false;
                }

                DateTime dteExpirationDate = privilege.EndDatePST;

                if (dteExpirationDate == DateTime.MinValue)
                {
                    // Member does not have premium service 
                    return false;
                }
                else
                {
                    if (DateTime.Now < dteExpirationDate)
                    {
                        // Read Receipts premium service purchase has not yet expired
                        return true;
                    }
                    else
                    {
                        // Read Receipts premium service purchase has expired  
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        #endregion

        #region SABase Members

        protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESVC_SA_CONNECTION_LIMIT"));
		}

		#endregion

	}
}
