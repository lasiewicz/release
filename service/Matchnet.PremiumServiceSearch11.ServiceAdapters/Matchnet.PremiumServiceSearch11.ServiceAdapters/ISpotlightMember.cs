using System;

namespace Matchnet.PremiumServiceSearch11.ServiceAdapters
{
	/// <summary>
	/// Summary description for ISpotlightMember.
	/// </summary>

		public interface ISpotlightMember11
		{
	
			void SaveMember11(int brandid, int memberid, DateTime expirationDate, bool enableFlag, bool async);
			
		}
	}


