using System;
using System.Text;
using System.Data;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects;
using Matchnet.Member.ServiceAdapters;
namespace Matchnet.PremiumServiceSearch11.ServiceAdapters
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class SpotlightMemberSA:SABase
	{
		public static readonly SpotlightMemberSA Instance = new SpotlightMemberSA();
		private string testUri="";
		private const string TRACE_FORMAT="BrandID={0}\r\nMemberID={1}\r\nExpDate={2}\r\nEnableFlag={3}\r\nURI={4}";
		private SpotlightMemberSA()
		{
			
		}


		public void SaveMember(int  brandid, int memberid,DateTime expirationdate, bool enableflag, bool async)
		{	ISpotlightMember11 m1=null;
			string functionName="SaveMember";
				string trace="";
			try
			{
				string uri = getServiceManagerUri();
			
				base.Checkout(uri);
				try
				{trace=getTrace(brandid,memberid,expirationdate,enableflag,uri);
					m1= getService(uri);
					
					m1.SaveMember11(brandid,memberid,expirationdate,enableflag, async);

					Matchnet.Member.ServiceAdapters.Member m = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid,MemberLoadFlags.None);
					
					Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
					m.SetAttributeDate(brand,"SpotlightExpirationDate",expirationdate);
					Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(m);
				}
				finally
				{
					base.Checkin(uri);
					m1=null;
				}

				

			}
			catch(Exception ex)
			{
				LogException(functionName,"Cannot save member",trace,ex);
				throw(new SAException("Cannot SaveMember ", ex,null));
			}
		}
		#region SABase implementation

		public string TestURI
		{
			get{return testUri;}
			set{testUri=value;}
		}
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESVC_SA_CONNECTION_LIMIT"));
		}
		#endregion SABase implementation

		#region service proxy

		private ISpotlightMember11 getService(string uri)
		{
			try
			{
				
				return (ISpotlightMember11)Activator.GetObject(typeof(ISpotlightMember11), uri);
			}
			catch(Exception ex)
			{

				throw(new Exception("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition("PREMIUMSERVICESEARCH_SVC", Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri("SpotlightMemberSM");

				if(testUri != null && testUri != "")
					uri=testUri;

				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESEARCHSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new Exception("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		#endregion
	
		private void LogException(string source, string message, string tracedata, Exception ex)
		{
			try
			{
					string msg=source + "\r\n" + message + "\r\n"  + tracedata + "\r\n" + ex.ToString();
					System.Diagnostics.EventLog.WriteEntry("Matchnet.PremiumServiceSearch11.SpotlightMemberSA", msg,System.Diagnostics.EventLogEntryType.Error);
			}
			catch(Exception e)
			{e=null;}

		}

		private string getTrace(int  brandid, int memberid,DateTime expirationdate, bool enableflag, string uri)
		{
			string trace="";
			try
			{
				trace=String.Format(TRACE_FORMAT,brandid,memberid,expirationdate,enableflag,uri);
				return trace;
			}
			catch(Exception ex)
			{return trace;}

		}
	}
}
