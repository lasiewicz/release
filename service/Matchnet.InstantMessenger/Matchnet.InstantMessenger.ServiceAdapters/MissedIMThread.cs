#region

using System;
using System.Threading;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;

#endregion

namespace Matchnet.InstantMessenger.ServiceAdapters
{
    /// <summary>
    ///   Used for sending an on-site Email and ExternalMail notification for a missed IM message.
    ///   The sending is performed in a new thread.
    /// </summary>
    public class MissedIMThread
    {
        private readonly MessageSave _atQuotaErrorMessage;
        private readonly Brand _brand;
        private readonly MessageSave _messageSave;
        private readonly bool _saveInSent;

        /// <summary>
        /// </summary>
        /// <param name = "messageSave"></param>
        /// <param name = "saveInSent"></param>
        /// <param name = "brand"></param>
        /// <param name = "atQuotaErrorMessage"></param>
        public MissedIMThread(MessageSave messageSave, bool saveInSent, Brand brand, MessageSave atQuotaErrorMessage)
        {
            _messageSave = messageSave;
            _saveInSent = saveInSent;
            _brand = brand;
            _atQuotaErrorMessage = atQuotaErrorMessage;
        }

        ///<summary>
        ///</summary>
        public void Start()
        {
#if DEBUG
            DoSend();
#else
            new Thread(DoSend).Start();
#endif
        }

        private void DoSend()
        {
            try
            {
                var messageToSend = _messageSave;

                if (QuotaSA.Instance.IsAtQuota(_messageSave.GroupID, _messageSave.FromMemberID, QuotaType.MissedIM))
                {
                    messageToSend = _atQuotaErrorMessage;
                }
                else
                {
                    QuotaSA.Instance.AddQuotaItem(_messageSave.GroupID, _messageSave.FromMemberID, QuotaType.MissedIM);
                }

                var messageSendResult = EmailMessageSA.Instance.SendMessage(messageToSend, _saveInSent,
                                                                            Constants.NULL_INT, false);
                if (messageSendResult.Status == MessageSendStatus.Success)
                {
                    ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage, _brand.BrandID,
                                                                         messageSendResult.RecipientUnreadCount);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error sending missed IM", ex);
            }
        }
    }
}