using System;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.InstantMessenger.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;


namespace Matchnet.InstantMessenger.ServiceAdapters
{
	/// <summary>
	/// Summary description for InstantMessengerSettingsSA.
	/// </summary>
	public class InstantMessengerSettingsSA : SABase
	{
		#region Singleton implementation
		public static readonly InstantMessengerSettingsSA Instance = new InstantMessengerSettingsSA();

		private InstantMessengerSettingsSA()
		{}

		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// Retrieves away settings from the database by MemberID and CommunityID.
		/// </summary>
		/// <param name="memberID">The MemberID for which to retrieve the IMSettings.</param>
		/// <param name="communityID">The CommunityID for which to retrieve the IMSettings.</param>
		/// <returns>An IMSettings object representing the member's away settings.</returns>
		public IMSettings GetSettings(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			try
			{	// We rely on the caching of these settings my the Member SA and middle-tier.
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				IMSettings settings = new IMSettings();
				settings.AwayMessage = member.GetAttributeText(brand, "AwayMessage");
				settings.CommunityID = brand.Site.Community.CommunityID;
				settings.MemberID = memberID;
				return settings;
			}
			catch (ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to get IMSettings", ex));
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred while attempting to get IMSettings", ex));
			}
		}

		/// <summary>
		/// Saves / updates a Members IM settings for the associated brand.
		/// </summary>
		/// <param name="iMSettings"></param>
		/// <param name="brand"></param>
		public void SaveSettings(IMSettings iMSettings, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			try
			{
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(iMSettings.MemberID, MemberLoadFlags.None);
				member.SetAttributeText(brand, "AwayMessage", iMSettings.AwayMessage, TextStatusType.Auto);
				MemberSA.Instance.SaveMember(member);
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to save IMSettings.", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to save IMSettings.", ex));
			} 
		}

	}
}
