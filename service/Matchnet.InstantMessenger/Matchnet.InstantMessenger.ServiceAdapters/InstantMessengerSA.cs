using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.InstantMessenger.ServiceDefinitions;
using Spark.Caching;
using Spark.Common.Localization;
using log4net;
using Message = Matchnet.InstantMessenger.ValueObjects.Message;
using Spark.Common.AccessService;
using System.Collections.Generic;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;


namespace Matchnet.InstantMessenger.ServiceAdapters
{
	/// <summary>
	/// Service Adapter for Instant Messenger Service
	/// </summary>
	public class InstantMessengerSA : SABase
	{
		#region Private Members
		private const string SERVICE_MANAGER_NAME	= "InstantMessengerSM";
		private const string SERVICE_CONSTANT		= "INSTANTMESSENGER_SVC";
		private const string PERF_COUNTER_CATEGORY	= "Matchnet.InstantMessenger.Service";
	    private const string ConversationTokensCouchbase = "ConversationTokens";
        private const string VipEmailEnabled = "VIP_EMAIL_ENABLED";
        private static readonly ILog Log = LogManager.GetLogger(typeof(InstantMessengerSA));
		private Matchnet.Caching.Cache				_Cache;
		private Participant						_SystemParticipant;

		// Performance counters and instrumentation members
		private CacheItemRemovedCallback			_expireConversation;
		//private PerformanceCounter					_perfIMConversations;
		//private PerformanceCounter					_perfMessagesPerSecond;

		#endregion
		
		#region Constructors
		public static readonly InstantMessengerSA Instance = new InstantMessengerSA();

		private InstantMessengerSA()
		{
			_Cache = Matchnet.Caching.Cache.Instance;

			// Initialize a System participant used to communicate decline responses
			// on behalf of the declining participant.
			_SystemParticipant = new Participant();
			_SystemParticipant.MemberID = 0;
			_SystemParticipant.Username = "SYSTEM";

			_expireConversation = new CacheItemRemovedCallback(this.expireIMConversationCallback);
			initializePerformanceCounters();
		}
		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_SA_CONNECTION_LIMIT"));
		}
		
		#region Public Methods

		/// <summary>
		/// Allows you to save an existing conversation invitation for the purposes of marking it as Read for example.
		/// </summary>
		public void Save(ConversationInvite conversationInvite) 
		{
			string uri = null;
			try
			{
				uri = getServiceManagerUri(conversationInvite.RecipientID);

				base.Checkout(uri);
				try
				{
					getService(uri).Save(conversationInvite);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to save an IM Invitation. (uri: " + uri + ")", ex));
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred while attempting to save an IM Invitation. (uri: " + uri + ")", ex));
			}
		}

		public void DeclineInvitation(ConversationInvite conversationInvite)
		{
			try
			{	// Decline is effectively the same implementation as joining, we simply need to have the
				// invitation removed from the m/t invitation cache, invoking acceptiInvitation(...) accomplishes this.
				acceptInvitation(conversationInvite);

				// Now the last piece is to inform the originating member that their invitation was declined
				// by the recipient.
				//notifyParticipantOfDeclinedInvitation(conversationInvite.ConversationKey);
			}
			catch (Exception ex)
			{
				throw new SAException("Unable to decline the invitation to an IM Conversation.", ex);
			}
		}

		/// <summary>
		/// Invoke this method accept and invitation to an IM Conversation.  The participant member associated
		/// with the invitation will be added to the conversation.
		/// 
		/// This implementation has been modified in support of the Userplane IM implementation, all we need to 
		/// do in the UP environment is accept the invite so the member is not continuously alerted by the DHTML notify.
		/// </summary>
		/// <param name="conversationInvite"></param>
		public void JoinConversation(ConversationInvite conversationInvite)
		{
			try
			{
				acceptInvitation(conversationInvite);	// inform the middle-tier that the recipient has joined the conversation.
			}
			catch (Exception ex)
			{
				throw new SAException("Unable to join the IM Conversation.", ex);
			}
		}

		public void JoinConversation(int recipientMemberID, int initiatingMemberID, int communityID)
		{
			// get invites for this recipient
			ConversationInvites invites = GetInvites(recipientMemberID, communityID);

			if (invites == null)
			{
				return;
			}

			lock (invites.SyncRoot)
			{
				foreach (ConversationInvite invite in invites)
				{
					// look for a match using InitiatingMember/CommunityID
					if (invite.Message.Sender.MemberID.Equals(initiatingMemberID))
					{
						// use the invite VO to JoinConversation
						JoinConversation(invite);
						break;
					}
				}
			}
		}

		public ConversationKey InitiateConversation(int initiatingMemberID
			, int recipientMemberID
			, int communityID)
		{
			return InitiateConversation(initiatingMemberID, recipientMemberID, communityID, Constants.NULL_STRING);
		}

		/// <summary>
		/// Called when User A clicks "Send" on their Instant Messenger client. Adds an invitation to a particular 
		/// user's invitation collection
		/// </summary>
		/// <param name="initiatingMemberID">The member ID of user who is trying to send an Instant Message.</param>
		/// <param name="recipientMemberID">The member ID of user that Initiating Member is trying to send a message to.</param>
		/// <param name="communityID">Community that both members belong to.</param>
		/// <param name="externalTransactionID">Optional unique transaction ID supplied by Userplane in order to help handle duplicate invitations.</param>
		/// <returns></returns>
		public ConversationKey InitiateConversation(int initiatingMemberID
			, int recipientMemberID
			, int communityID
			, string externalTransactionID)
		{
			ConversationKey key = new ConversationKey();
			ConversationInvite invite = new ConversationInvite();
			invite.CommunityID = communityID;
			invite.ConversationKey = key;
			invite.InviteDate = DateTime.Now;
			invite.Message = new Message(string.Empty, Participant.New(initiatingMemberID, communityID));
			invite.RecipientID = recipientMemberID;
			//associate this invite with externally supplied transaction ID if supplied
			if (externalTransactionID != Constants.NULL_STRING) 
			{
				invite.ExternalTransactionID = new ExternalTransactionID(externalTransactionID, UserplaneTransactionType.StartConversation, communityID);
			}
			sendInvitation(invite);
			return key;

		}

		/// <summary>
		/// Adds a ConversationInvite object to BL cache.  This overloaded version is written for Provo to call.
		/// </summary>
		/// <param name="initiatingMemberID"></param>
		/// <param name="recipientMemberID"></param>
		/// <param name="communityID"></param>
		/// <param name="imID"></param>
		/// <returns></returns>
		public ConversationKey InitiateConversation(int initiatingMemberID
			, int recipientMemberID
			, int communityID
			, int imID)
		{
			ConversationKey key = new ConversationKey();
			ConversationInvite invite = new ConversationInvite();
			invite.Version = 2;
			invite.CommunityID = communityID;
			invite.ConversationKey = key;
			invite.InviteDate = DateTime.Now;
			invite.Message = new Message(string.Empty, Participant.New(initiatingMemberID, communityID));
			invite.RecipientID = recipientMemberID;
			invite.IMID = imID;

			sendInvitation(invite);
			return key;
		}

		/// <summary>
		/// Use this method to retrieve any pending IM Conversations or Invites for the recipient
		/// member and community identified in the parameter list.  Any IM conversation that is
		/// pending this recipient's response (either decline or accept) will be returned in the collection.
		/// </summary>
		/// <param name="recipientMemberID">The member id of the recipient member.</param>
		/// <param name="communityID">The community id for this application context.</param>
		/// <returns>Either a collection of pending invites or null if none exist.</returns>
		public ConversationInvites GetInvites(int recipientMemberID, int communityID)
		{
			string uri = null;
			try
			{
				uri = getServiceManagerUri(recipientMemberID);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetInvites(recipientMemberID, communityID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to get conversation invites. (uri: " + uri + ")", ex));
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred while attempting to get conversation invites. (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Temporary overload for backwards compatibility
		/// Can be removed after 1.3 has been deployed to production.
		/// </summary>
		/// <param name="messageSave"></param>
		/// <param name="saveInSent"></param>
		/// <param name="brand"></param>
		/// <param name="transactionID"></param>
		public void SendMissedIM(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID, string atQuotaErrorMessage)
		{
			Matchnet.Email.ValueObjects.MessageSave errorMessage = new Matchnet.Email.ValueObjects.MessageSave(messageSave.FromMemberID, messageSave.FromMemberID, messageSave.GroupID, Matchnet.Email.ValueObjects.MailType.MissedIM, messageSave.MessageHeader, atQuotaErrorMessage);
			SendMissedIM(messageSave, saveInSent, brand, transactionID, errorMessage);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageSave"></param>
		/// <param name="saveInSent"></param>
		/// <param name="brand"></param>
		/// <param name="transactionID"></param>
		/// <param name="atQuotaErrorMessage">Error message to display in notification if user has reached missed IM quota.</param>
		public void SendMissedIM(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID, Matchnet.Email.ValueObjects.MessageSave atQuotaErrorMessage)
		{
			string uri = null;
			try
			{
				ExternalTransactionID externalTransactionID = null;

				if (transactionID != Constants.NULL_STRING) 
				{
					externalTransactionID = new ExternalTransactionID(transactionID, UserplaneTransactionType.NotifyConnectionClosed, brand.Site.Community.CommunityID);
				}

				uri = getServiceManagerUri();

				base.Checkout(uri);
				try
				{
					if (getService(uri).CacheExternalTransactionID(externalTransactionID))  //If this ExternalTransactionID is already cached, don't send the missed IM -- we already sent it
					{
						return;
					}
				}
				finally
				{
					base.Checkin(uri);
				}

				new MissedIMThread(messageSave, saveInSent, brand, atQuotaErrorMessage).Start();
			}
			catch (ExceptionBase ex)
			{
				throw(new SAException("Error occurred while sending missed IM notification. (uri: " + uri + ")", ex));
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred while sending missed IM notification. (uri: " + uri + ")", ex));
			}
		}

	    /// <summary>
	    /// 12/12. Deprecate the other sendmissedim methods once this method is deployed to prod. 
	    /// used once in bedrock in instantmessenger\xmlresponses\notifyconnectionclosed. to be also used by the API.
	    /// </summary>
	    /// <param name="senderMemberId"></param>
	    /// <param name="targetMemberId">member receiving the missed IM message.</param>
	    /// <param name="messagesXml"></param>
	    /// <param name="senderBrand"></param>
	    public void SendMissedIM(int senderMemberId, int targetMemberId, string messagesXml, Brand senderBrand)
	    {
            var uri = getServiceManagerUri();

	        try
	        {
	            Checkout(uri);
	            try
	            {
	                getService(uri).SendMissedIM(senderMemberId, targetMemberId, messagesXml, senderBrand);
	            }
	            finally
	            {
	                Checkin(uri);
	            }
	        }
	        catch (Exception ex)
	        {
	            throw (new SAException("Error occurred while sending missed IM notification. (uri: " + uri + ")", ex));
	        }
	    }

        /// <summary>
        /// Processes Missed IM messages which may include custom markup to support photos, emoticons, etc
        /// </summary>
        /// <param name="senderMemberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="messages"></param>
        /// <param name="senderBrand"></param>
        public void SendMissedIM(int senderMemberId, int targetMemberId, List<string> messages, Brand senderBrand)
        {
            var uri = getServiceManagerUri();

            try
            {
                Checkout(uri);
                try
                {
                    getService(uri).SendMissedIM(senderMemberId, targetMemberId, messages, senderBrand);
                }
                finally
                {
                    Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while sending missed IM notification. (uri: " + uri + ")", ex));
            }
        }
		
        #endregion

	    #region Couchbase to CRUD conversation tokens for ejabberd IM

	    /// <summary>
	    /// </summary>
	    /// <param name="senderMemberId"></param>
	    /// <param name="recipientMemberId"></param>
	    /// <param name="communityId"></param>
	    public ConversationToken CreateConverationToken(int senderMemberId, int recipientMemberId, int communityId)
	    {
            // Clear whatever's cached for these params
	        DeleteConversationToken(senderMemberId, recipientMemberId, communityId);

	        var conversationTokenId = Guid.NewGuid().GetHashCode().ToString(CultureInfo.InvariantCulture).Replace("-", "0");
	        if (conversationTokenId[0] != '0')
	            conversationTokenId = conversationTokenId.Insert(0, "1");

	        var conversationToken = new ConversationToken
	            {
	                ConversationTokenId = conversationTokenId,
	                SenderMemberId = senderMemberId,
	                RecipientMemberId = recipientMemberId,
	                CommunityId = communityId
	            };

	        var membaseId = ConversationToken.GenerateMembaseId(senderMemberId, recipientMemberId, communityId);
	        GetCacheInstance(ConversationTokensCouchbase).Insert(membaseId, conversationToken);
	        Log.InfoFormat("Inserted new item into {0} Id:{1}", ConversationTokensCouchbase, membaseId);
	        
            return conversationToken;
	    }

	    /// <summary>
	    /// 
	    /// </summary>
	    /// <param name="senderMemberId"></param>
	    /// <param name="recipientMemberId"></param>
	    /// <param name="communityId"></param>
	    /// <returns></returns>
	    public ConversationToken ReadConversationToken(int senderMemberId, int recipientMemberId, int communityId)
	    {
	        var membaseId = ConversationToken.GenerateMembaseId(senderMemberId, recipientMemberId, communityId);
	        var conversationToken =
                GetCacheInstance(ConversationTokensCouchbase).Get(membaseId) as ConversationToken;
            Log.InfoFormat("Retrieved item from {0} Id:{1}", ConversationTokensCouchbase, membaseId);
	        return conversationToken;
	    }

	    /// <summary>
	    /// </summary>
	    /// <param name="senderMemberId"></param>
	    /// <param name="recipientMemberId"></param>
	    /// <param name="communityId"></param>
	    public void DeleteConversationToken(int senderMemberId, int recipientMemberId, int communityId)
	    {
	        var membaseId = ConversationToken.GenerateMembaseId(senderMemberId, recipientMemberId, communityId);
	        GetCacheInstance(ConversationTokensCouchbase).Remove(membaseId);
	        Log.InfoFormat("Removed item from {0} Id:{1}", ConversationTokensCouchbase, membaseId);
	    }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bucketName"></param>
        /// <returns></returns>
	    private static ICaching GetCacheInstance(string bucketName)
	    {
	        return MembaseCaching.GetSingleton(RuntimeSettings.GetMembaseConfigByBucket(bucketName));
	    }

	    #endregion

        #region Private Methods

        private void acceptInvitation(ConversationInvite conversationInvite)
		{
			string uri = null;
			try
			{
				uri = getServiceManagerUri(conversationInvite.RecipientID);

				base.Checkout(uri);
				try
				{
					getService(uri).AcceptInvitation(conversationInvite);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to accept an IM Invitation. (uri: " + uri + ")", ex));
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred while attempting to accept an IM Invitation. (uri: " + uri + ")", ex));
			}
		}

		/*private Conversation getConversation(ConversationKey conversationKey)
		{
			return _Cache[conversationKey.GUID] as Conversation;
		}

		private void loadConversation(Conversation conversation)
		{
			// Conversations exist in the web-tier / service adapter so the item will be cached locally, no communication with
			// the middle-tier exists for the conversation.
			conversation.CacheTTLSeconds = 
				Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_CONVERSATION_CACHE_TTL"));
			
			// Add the Conversation to the local cache and increment the performance counter accordingly.
			_Cache.Add(conversation, null, Matchnet.CacheItemPriorityLevel.Normal, _expireConversation);
			
			//TODO: Re-enable perf counter was a solution exists for installing them dynamically at the web tier.
			//_perfIMConversations.Increment();
		}

		private void notifyParticipantOfDeclinedInvitation(ConversationKey conversationKey)
		{
			Conversation conversation = getConversation(conversationKey);
			Message declineNotificationMessage = new Message(
				Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_CONVERSATION_DECLINED_MSG") 
				, _SystemParticipant);
			declineNotificationMessage.ConversationKey = conversationKey;
			declineNotificationMessage.Type = MessageType.Admin;
			conversation.DeliverMessage(declineNotificationMessage);
		}*/

		private void sendInvitation(ConversationInvite invite)
		{
			string uri = null;
			try
			{
				uri = getServiceManagerUri(invite.RecipientID);

				base.Checkout(uri);
				try
				{
					getService(uri).SendInvitation(invite);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(ExceptionBase ex)
			{
				throw(new SAException("Error occurred while attempting to send invitation for IM Conversation. (uri: " + uri + ")", ex));
			} 
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to send invitation for IM Conversation. (uri: " + uri + ")", ex));
			} 
		}

		private void validateMessage(Message message)
		{
			// Message cannot be null
			if (message == null)
			{
				throw(new Exception("Message object was passed as null."));
			}

			// ConversationKey cannot be null
			if (message.ConversationKey == null)
			{
				throw(new Exception("Message object had an invalid ConversationKey (null)."));
			}

			// Message must have a valid sender (Participant)
			if (message.Sender == null)
			{
				throw(new Exception("Message object had an invalid Sender (null)."));
			}
		}

		private void validateReceiveCriteria(ConversationKey conversationKey, Participant recipient)
		{
			if (conversationKey == null)
			{
				throw(new Exception("The ConversationKey object supplied as a parameter was null."));
			}

			if (recipient == null)
			{
				throw(new Exception("The Recipient (Participant) supplied as a parameter was null."));		
			}
		}

        /// <summary>
        /// Determine if the recipient is allowed to reply back to the sender
        /// Note: For full conditions with reasons, use CanMemberIM()
        /// </summary>
        /// <param name="senderMemberId"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public bool CanMemberReply(int senderMemberId, int recipientMemberId, Brand brand)
        {
            var recipient = MemberSA.Instance.GetMember(recipientMemberId, MemberLoadFlags.None);
            if (recipient.IsPayingMember(brand.Site.SiteID))
            {
                Log.InfoFormat("true. recipient is a paying member. memberId:{0}", recipientMemberId);
                return true;
            }

            if (Convert.ToBoolean(RuntimeSettings.GetSetting(VipEmailEnabled, brand.Site.Community.CommunityID,
                                                             brand.Site.SiteID, brand.BrandID)))
            {
                var sender = MemberSA.Instance.GetMember(senderMemberId, MemberLoadFlags.None);
                int senderBrandId;
                sender.GetLastLogonDate(brand.Site.Community.CommunityID, out senderBrandId);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(senderBrandId);
                var privdate = sender.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, senderBrand.BrandID,
                                                                senderBrand.Site.SiteID,
                                                                senderBrand.Site.Community.CommunityID);
                if (privdate != null)
                {
                    Log.Debug(string.Format("SenderId:{0} SenderBrandId:{1} AAExpireDate:{2}", sender.MemberID,
                                            senderBrand.BrandID,
                                            privdate.EndDatePST));
                    var allAccessExpireDate = privdate.EndDatePST;

                    if (allAccessExpireDate > DateTime.Now)
                    {
                        Log.InfoFormat("true. sender has AA. memberId:{0}", senderMemberId);
                        return true;
                    }
                }
            }

            Log.InfoFormat("false. recipient cannot reply. senderId:{0} recipientId:{1}", senderMemberId,
                recipientMemberId);

            return false;
        }

        /// <summary>
        /// Determine if IM can occur between sender and target member
        /// </summary>
        /// <param name="senderMemberId"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public CanMemberIMResponse CanMemberIM(int senderMemberId, int targetMemberId, Brand brand, bool CanInviteCheckOnly)
        {
            CanMemberIMResponse response = new CanMemberIMResponse();
            response.CanIM = false;
            response.Reason = CanIMReasons.None;

            //very sender prevent statuses
            var senderMember = MemberSA.Instance.GetMember(senderMemberId, MemberLoadFlags.None);
            if (senderMember == null || string.IsNullOrEmpty(senderMember.EmailAddress))
            {
                response.Reason = CanIMReasons.SenderInvalid;
                return response;
            }
            //self suspended
            else if (senderMember.GetAttributeInt(brand, "SelfSuspendedFlag", 0) == 1)
            {
                response.Reason = CanIMReasons.SenderSelfSuspended;
                return response;
            }
            //admin suspend
            else if ((senderMember.GetAttributeInt(brand, "GlobalStatusMask", 0) & 1) == 1)
            {
                response.Reason = CanIMReasons.SenderSuspended;
                return response;
            }
            //blocked
            else if (ListSA.Instance.GetList(senderMemberId, ListLoadFlags.None).IsHotListed(HotListCategory.IgnoreList, brand.Site.Community.CommunityID, targetMemberId))
            {
                response.Reason = CanIMReasons.SenderBlockedTarget;
                return response;
            }

            //very target prevent statuses
            var targetMember = MemberSA.Instance.GetMember(targetMemberId, MemberLoadFlags.None);
            if (targetMember == null || string.IsNullOrEmpty(targetMember.EmailAddress))
            {
                response.Reason = CanIMReasons.TargetInvalid;
                return response;
            }
            //self suspended
            else if (targetMember.GetAttributeInt(brand, "SelfSuspendedFlag", 0) == 1)
            {
                response.Reason = CanIMReasons.TargetSelfSuspended;
                return response;
            }

            //admin suspend
            else if ((targetMember.GetAttributeInt(brand, "GlobalStatusMask", 0) & 1) == 1)
            {
                response.Reason = CanIMReasons.TargetSuspended;
                return response;
            }
            //blocked
            else if (ListSA.Instance.GetList(targetMemberId, ListLoadFlags.None).IsHotListed(HotListCategory.IgnoreList, brand.Site.Community.CommunityID, senderMemberId))
            {
                response.Reason = CanIMReasons.TargetBlockedSender;
                return response;
            }

            //verify subscriptions
            if (senderMember.IsPayingMember(brand.Site.SiteID))
            {
                //both are subscribers 
                if (targetMember.IsPayingMember(brand.Site.SiteID))
                {
                    response.CanIM = true;
                    response.Reason = CanIMReasons.BothAreSubscribers;
                    return response;
                }
                //check if sender is all access
                else
                {
                    if (Convert.ToBoolean(RuntimeSettings.GetSetting(VipEmailEnabled, brand.Site.Community.CommunityID,
                                                             brand.Site.SiteID, brand.BrandID)))
                    {
                        int senderBrandId;
                        senderMember.GetLastLogonDate(brand.Site.Community.CommunityID, out senderBrandId);
                        var senderBrand = BrandConfigSA.Instance.GetBrandByID(senderBrandId);
                        var privdate = senderMember.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, senderBrand.BrandID,
                                                                        senderBrand.Site.SiteID,
                                                                        senderBrand.Site.Community.CommunityID);
                        if (privdate != null)
                        {
                            Log.Debug(string.Format("SenderId:{0} SenderBrandId:{1} AAExpireDate:{2}", senderMember.MemberID,
                                                    senderBrand.BrandID,
                                                    privdate.EndDatePST));
                            var allAccessExpireDate = privdate.EndDatePST;

                            if (allAccessExpireDate > DateTime.Now)
                            {
                                response.CanIM = true;
                                response.Reason = CanIMReasons.SenderIsAllAccess;
                                return response;
                            }
                        }
                    }

                    if (CanInviteCheckOnly)
                    {
                        //we will allow sender to still send an IM, so the recipient can get an invite
                        response.CanIM = true;
                        response.Reason = CanIMReasons.SenderIsSubscriberCanInvite;
                        return response;
                    }
                    else
                    {
                        response.Reason = CanIMReasons.TargetIsNonSubscriber;
                        return response;
                    }
                }

            }
            else
            {
                response.Reason = CanIMReasons.SenderIsNonSubscriber;
                return response;
            }

        }
		#endregion

		#region Performance Counters and Instrumentation

		private void initializePerformanceCounters()
		{
			/*
			 * disabled until a solution for installing these in the web context can be identified.
			 * 
			// Check for existence of the performance counter category for IM, install them if they don't exist.
			if (!PerformanceCounterCategory.Exists(PERF_COUNTER_CATEGORY))
			{
				CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
				ccdc.AddRange(new CounterCreationData [] {	
															 new CounterCreationData("IM Conversations", "IM Conversations", PerformanceCounterType.NumberOfItems64),
															 new CounterCreationData("IM Messages/second", "IM Messages/second", PerformanceCounterType.RateOfCountsPerSecond32),
				});	
				PerformanceCounterCategory.Create(PERF_COUNTER_CATEGORY, PERF_COUNTER_CATEGORY, ccdc);
			}

			_perfIMConversations = new PerformanceCounter(PERF_COUNTER_CATEGORY, "IM Conversations", false);
			_perfMessagesPerSecond = new PerformanceCounter(PERF_COUNTER_CATEGORY, "IM Messages/second", false);
			resetPerformanceCounters();
			*/
		}

		private void resetPerformanceCounters()
		{
			//_perfIMConversations.RawValue = 0;
			//_perfMessagesPerSecond.RawValue = 0;
		}


		// Event Handler for dealing with IMConversations when they are removed from the cache.
		private void expireIMConversationCallback(string key, object value, CacheItemRemovedReason callbackreason)
		{
			//TODO: Re-enable this perf counter when a solution for their installation is identified (1/20 k.landrus)
			//_perfIMConversations.Decrement();
		}

		#endregion

		#region Remoting implementation

		private IInstantMessengerService getService(string uri)
		{
			try
			{
				return (IInstantMessengerService)Activator.GetObject(typeof(IInstantMessengerService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Trim().Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

        private string getServiceManagerUri(int recipientMemberId)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, recipientMemberId).ToUri(SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

		#endregion
	}
}
