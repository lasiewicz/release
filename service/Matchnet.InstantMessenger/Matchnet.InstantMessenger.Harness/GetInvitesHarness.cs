using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.InstantMessenger.ServiceDefinitions;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for GetInvites.
	/// </summary>
	public class GetInvitesHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cmbCommunityID;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button btnGetInvites;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ListView lvInvitations;
		private System.Windows.Forms.ColumnHeader chConversationGUID;
		private System.Windows.Forms.ColumnHeader chSender;
		private System.Windows.Forms.ComboBox cmbRecipientMemberID;
		private System.Windows.Forms.ContextMenu cmInvitations;
		private System.Windows.Forms.MenuItem miJoinConversation;
		private System.Windows.Forms.MenuItem miDeclineInvite;
		private System.Windows.Forms.ColumnHeader chServer;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public GetInvitesHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cmbCommunityID = new System.Windows.Forms.ComboBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cmbRecipientMemberID = new System.Windows.Forms.ComboBox();
			this.btnGetInvites = new System.Windows.Forms.Button();
			this.lvInvitations = new System.Windows.Forms.ListView();
			this.chConversationGUID = new System.Windows.Forms.ColumnHeader();
			this.chSender = new System.Windows.Forms.ColumnHeader();
			this.cmInvitations = new System.Windows.Forms.ContextMenu();
			this.miJoinConversation = new System.Windows.Forms.MenuItem();
			this.miDeclineInvite = new System.Windows.Forms.MenuItem();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.chServer = new System.Windows.Forms.ColumnHeader();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Recipient MemberID:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(272, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 23);
			this.label2.TabIndex = 2;
			this.label2.Text = "CommunityID:";
			// 
			// cmbCommunityID
			// 
			this.cmbCommunityID.Items.AddRange(new object[] {
																"1",
																"2",
																"3",
																"10",
																"12"});
			this.cmbCommunityID.Location = new System.Drawing.Point(368, 24);
			this.cmbCommunityID.Name = "cmbCommunityID";
			this.cmbCommunityID.Size = new System.Drawing.Size(121, 21);
			this.cmbCommunityID.TabIndex = 3;
			this.cmbCommunityID.Text = "1";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cmbRecipientMemberID);
			this.groupBox1.Controls.Add(this.btnGetInvites);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.cmbCommunityID);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(0, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(504, 88);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Check for Conversation Invitations";
			// 
			// cmbRecipientMemberID
			// 
			this.cmbRecipientMemberID.Items.AddRange(new object[] {
																	  "999111111",
																	  "999222222",
																	  "999333333",
																	  "999444444",
																	  "999555555",
																	  "999666666",
																	  "999777777",
																	  "999888888",
																	  "999999999"});
			this.cmbRecipientMemberID.Location = new System.Drawing.Point(136, 24);
			this.cmbRecipientMemberID.Name = "cmbRecipientMemberID";
			this.cmbRecipientMemberID.Size = new System.Drawing.Size(121, 21);
			this.cmbRecipientMemberID.TabIndex = 5;
			this.cmbRecipientMemberID.Text = "999654321";
			// 
			// btnGetInvites
			// 
			this.btnGetInvites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnGetInvites.Location = new System.Drawing.Point(376, 56);
			this.btnGetInvites.Name = "btnGetInvites";
			this.btnGetInvites.Size = new System.Drawing.Size(112, 23);
			this.btnGetInvites.TabIndex = 4;
			this.btnGetInvites.Text = "Get Invites";
			this.btnGetInvites.Click += new System.EventHandler(this.btnGetInvites_Click);
			// 
			// lvInvitations
			// 
			this.lvInvitations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lvInvitations.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																							this.chConversationGUID,
																							this.chSender,
																							this.chServer});
			this.lvInvitations.ContextMenu = this.cmInvitations;
			this.lvInvitations.GridLines = true;
			this.lvInvitations.Location = new System.Drawing.Point(8, 24);
			this.lvInvitations.Name = "lvInvitations";
			this.lvInvitations.Size = new System.Drawing.Size(480, 160);
			this.lvInvitations.TabIndex = 5;
			this.lvInvitations.View = System.Windows.Forms.View.Details;
			// 
			// chConversationGUID
			// 
			this.chConversationGUID.Text = "Conversation Key (GUID)";
			this.chConversationGUID.Width = 250;
			// 
			// chSender
			// 
			this.chSender.Text = "Sender ID";
			this.chSender.Width = 120;
			// 
			// cmInvitations
			// 
			this.cmInvitations.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.miJoinConversation,
																						  this.miDeclineInvite});
			// 
			// miJoinConversation
			// 
			this.miJoinConversation.Index = 0;
			this.miJoinConversation.Text = "Join Conversation";
			this.miJoinConversation.Click += new System.EventHandler(this.miJoinConversation_Click);
			// 
			// miDeclineInvite
			// 
			this.miDeclineInvite.Index = 1;
			this.miDeclineInvite.Text = "Decline Invite";
			this.miDeclineInvite.Click += new System.EventHandler(this.miDeclineInvite_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.lvInvitations);
			this.groupBox2.Location = new System.Drawing.Point(0, 112);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(504, 192);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Current Invitations:";
			// 
			// chServer
			// 
			this.chServer.Text = "Server";
			this.chServer.Width = 100;
			// 
			// GetInvitesHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(528, 325);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "GetInvitesHarness";
			this.Text = "GetInvitesHarness (GetInvites)";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGetInvites_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnGetInvites.Enabled = false;

				IInstantMessengerService imService = null;
				ConversationInvites invites = null;
				
				lvInvitations.Items.Clear();


				// DEVAPP01 Instance
				/*imService = getService("tcp://devapp01:49000/InstantMessengerSM.rem");
			
				invites = imService.GetInvites(Convert.ToInt32(cmbRecipientMemberID.SelectedItem)
					, Convert.ToInt32(cmbCommunityID.SelectedItem));*/
				invites = InstantMessengerSA.Instance.GetInvites(Convert.ToInt32(cmbRecipientMemberID.Text)
					, Convert.ToInt32(cmbCommunityID.SelectedItem));

				if (invites != null)
				{
					ListViewItem listItem;
					lock (invites.SyncRoot)
					{
						foreach (ConversationInvite invite in invites)
						{
							listItem = new ListViewItem(invite.ConversationKey.GUID.ToString());	// Column 1
							listItem.SubItems.Add(invite.Message.Sender.MemberID.ToString());	// Column 2
							listItem.SubItems.Add("DEVAPP01");	// Column 3

							listItem.Tag = invite;
							lvInvitations.Items.Add(listItem);						
						}
					}
				}

				// DEVAPP02 Instance
				imService = getService("tcp://devapp02:49000/InstantMessengerSM.rem");
			
				invites = imService.GetInvites(Convert.ToInt32(cmbRecipientMemberID.SelectedItem)
					, Convert.ToInt32(cmbCommunityID.SelectedItem));

				if (invites != null)
				{
					ListViewItem listItem;
					lock (invites.SyncRoot)
					{
						foreach (ConversationInvite invite in invites)
						{
							listItem = new ListViewItem(invite.ConversationKey.GUID.ToString());	// Column 1
							listItem.SubItems.Add(invite.Message.Sender.MemberID.ToString());	// Column 2
							listItem.SubItems.Add("DEVAPP02");	// Column 3

							listItem.Tag = invite;
							lvInvitations.Items.Add(listItem);						
						}
					}
				}

			}
			finally
			{
				btnGetInvites.Enabled = true;
			}
		}

		private void miJoinConversation_Click(object sender, System.EventArgs e)
		{
			if (lvInvitations.SelectedItems.Count > 0)
			{
				ListViewItem listItem = lvInvitations.SelectedItems[0];
				ConversationInvite invite = (ConversationInvite)listItem.Tag;

				if (invite != null)
				{
					// join the convo.
					InstantMessengerSA.Instance.JoinConversation(invite);

					// spawn conversation window...
					IMConversationHarness frmIMConversation = new IMConversationHarness(invite.ConversationKey
						, Participant.New(invite.RecipientID, invite.CommunityID));
					frmIMConversation.MdiParent = this.MdiParent;	// same parent window
					frmIMConversation.Show();

					// remove the invite from the listing
					lvInvitations.Items.Remove(listItem);
				}
			}
		}

		private void miDeclineInvite_Click(object sender, System.EventArgs e)
		{
			if (lvInvitations.SelectedItems.Count > 0)
			{
				ListViewItem listItem = lvInvitations.SelectedItems[0];
				ConversationInvite invite = (ConversationInvite)listItem.Tag;

				if (invite != null)
				{
					InstantMessengerSA.Instance.DeclineInvitation(invite);
					lvInvitations.Items.Remove(listItem);
				}
			}
		}

		// Used to explicity get an instance of one of the devapp01/02 instances of the
		// InstantMessengerSM object.
		private IInstantMessengerService getService(string uri)
		{
			try
			{
				return (IInstantMessengerService)Activator.GetObject(typeof(IInstantMessengerService), uri);
			}
			catch(Exception ex)
			{
				throw(new Exception("Cannot activate remote service manager at " + uri, ex));
			}
		}

	}
}
