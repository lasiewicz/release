using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for InviteHarness.
	/// </summary>
	public class InviteHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblSender;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label lblRecipient;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblGUID;
		private System.Windows.Forms.Label lblIMServer;
		private System.Windows.Forms.Button btnInitiateConversation;
		private System.Windows.Forms.ComboBox cbCommunityID;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox txtNumberOfThreads;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtInvitesPerThread;
		private System.Windows.Forms.Button btnStartThreads;
		private System.Windows.Forms.Label lblAutoPilotStatus;
		private System.Windows.Forms.ComboBox cmbSendingMemberID;
		private System.Windows.Forms.ComboBox cmbRecipientMemberID;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InviteHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblSender = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.btnInitiateConversation = new System.Windows.Forms.Button();
			this.cbCommunityID = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.lblRecipient = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.lblIMServer = new System.Windows.Forms.Label();
			this.lblGUID = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.lblAutoPilotStatus = new System.Windows.Forms.Label();
			this.btnStartThreads = new System.Windows.Forms.Button();
			this.txtInvitesPerThread = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.txtNumberOfThreads = new System.Windows.Forms.TextBox();
			this.cmbSendingMemberID = new System.Windows.Forms.ComboBox();
			this.cmbRecipientMemberID = new System.Windows.Forms.ComboBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblSender
			// 
			this.lblSender.Location = new System.Drawing.Point(16, 24);
			this.lblSender.Name = "lblSender";
			this.lblSender.Size = new System.Drawing.Size(136, 23);
			this.lblSender.TabIndex = 0;
			this.lblSender.Text = "Sending Member ID:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.cmbRecipientMemberID);
			this.groupBox1.Controls.Add(this.cmbSendingMemberID);
			this.groupBox1.Controls.Add(this.txtUsername);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.btnInitiateConversation);
			this.groupBox1.Controls.Add(this.cbCommunityID);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.txtMessage);
			this.groupBox1.Controls.Add(this.lblMessage);
			this.groupBox1.Controls.Add(this.lblRecipient);
			this.groupBox1.Controls.Add(this.lblSender);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(496, 184);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Send IM Invitation";
			// 
			// txtUsername
			// 
			this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtUsername.Location = new System.Drawing.Point(384, 56);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(96, 20);
			this.txtUsername.TabIndex = 10;
			this.txtUsername.Text = "user";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(296, 56);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 23);
			this.label4.TabIndex = 9;
			this.label4.Text = "Username:";
			// 
			// btnInitiateConversation
			// 
			this.btnInitiateConversation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnInitiateConversation.Location = new System.Drawing.Point(344, 144);
			this.btnInitiateConversation.Name = "btnInitiateConversation";
			this.btnInitiateConversation.Size = new System.Drawing.Size(136, 23);
			this.btnInitiateConversation.TabIndex = 8;
			this.btnInitiateConversation.Text = "Initiate Conversation";
			this.btnInitiateConversation.Click += new System.EventHandler(this.btnInitiateConversation_Click);
			// 
			// cbCommunityID
			// 
			this.cbCommunityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbCommunityID.Items.AddRange(new object[] {
															   "1",
															   "2",
															   "3",
															   "10",
															   "12"});
			this.cbCommunityID.Location = new System.Drawing.Point(384, 24);
			this.cbCommunityID.Name = "cbCommunityID";
			this.cbCommunityID.Size = new System.Drawing.Size(96, 21);
			this.cbCommunityID.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(296, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 23);
			this.label1.TabIndex = 6;
			this.label1.Text = "CommunityID:";
			// 
			// txtMessage
			// 
			this.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtMessage.Location = new System.Drawing.Point(160, 88);
			this.txtMessage.Multiline = true;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(320, 48);
			this.txtMessage.TabIndex = 5;
			this.txtMessage.Text = "Your invite message goes here.";
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(16, 88);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.TabIndex = 4;
			this.lblMessage.Text = "Message:";
			// 
			// lblRecipient
			// 
			this.lblRecipient.Location = new System.Drawing.Point(16, 56);
			this.lblRecipient.Name = "lblRecipient";
			this.lblRecipient.Size = new System.Drawing.Size(136, 23);
			this.lblRecipient.TabIndex = 1;
			this.lblRecipient.Text = "Recipient Member ID:";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.lblIMServer);
			this.groupBox2.Controls.Add(this.lblGUID);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(8, 208);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(496, 100);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Invite Response (ConversationKey)";
			// 
			// lblIMServer
			// 
			this.lblIMServer.Location = new System.Drawing.Point(128, 56);
			this.lblIMServer.Name = "lblIMServer";
			this.lblIMServer.Size = new System.Drawing.Size(352, 23);
			this.lblIMServer.TabIndex = 3;
			// 
			// lblGUID
			// 
			this.lblGUID.Location = new System.Drawing.Point(128, 24);
			this.lblGUID.Name = "lblGUID";
			this.lblGUID.Size = new System.Drawing.Size(352, 23);
			this.lblGUID.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 56);
			this.label3.Name = "label3";
			this.label3.TabIndex = 1;
			this.label3.Text = "IM Server:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 24);
			this.label2.Name = "label2";
			this.label2.TabIndex = 0;
			this.label2.Text = "GUID:";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.lblAutoPilotStatus);
			this.groupBox3.Controls.Add(this.btnStartThreads);
			this.groupBox3.Controls.Add(this.txtInvitesPerThread);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.txtNumberOfThreads);
			this.groupBox3.Location = new System.Drawing.Point(8, 320);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(488, 80);
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Auto Pilot Test";
			// 
			// lblAutoPilotStatus
			// 
			this.lblAutoPilotStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblAutoPilotStatus.Location = new System.Drawing.Point(8, 48);
			this.lblAutoPilotStatus.Name = "lblAutoPilotStatus";
			this.lblAutoPilotStatus.Size = new System.Drawing.Size(472, 23);
			this.lblAutoPilotStatus.TabIndex = 5;
			// 
			// btnStartThreads
			// 
			this.btnStartThreads.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnStartThreads.Location = new System.Drawing.Point(344, 16);
			this.btnStartThreads.Name = "btnStartThreads";
			this.btnStartThreads.Size = new System.Drawing.Size(128, 23);
			this.btnStartThreads.TabIndex = 4;
			this.btnStartThreads.Text = "Start Threads";
			this.btnStartThreads.Click += new System.EventHandler(this.btnStartThreads_Click);
			// 
			// txtInvitesPerThread
			// 
			this.txtInvitesPerThread.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtInvitesPerThread.Location = new System.Drawing.Point(280, 16);
			this.txtInvitesPerThread.Name = "txtInvitesPerThread";
			this.txtInvitesPerThread.Size = new System.Drawing.Size(48, 20);
			this.txtInvitesPerThread.TabIndex = 3;
			this.txtInvitesPerThread.Text = "128";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(176, 16);
			this.label6.Name = "label6";
			this.label6.TabIndex = 2;
			this.label6.Text = "Invites per Thread:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(112, 23);
			this.label5.TabIndex = 1;
			this.label5.Text = "Number of Threads:";
			// 
			// txtNumberOfThreads
			// 
			this.txtNumberOfThreads.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtNumberOfThreads.Location = new System.Drawing.Point(120, 16);
			this.txtNumberOfThreads.Name = "txtNumberOfThreads";
			this.txtNumberOfThreads.Size = new System.Drawing.Size(32, 20);
			this.txtNumberOfThreads.TabIndex = 0;
			this.txtNumberOfThreads.Text = "16";
			// 
			// cmbSendingMemberID
			// 
			this.cmbSendingMemberID.Items.AddRange(new object[] {
																	"999111111",
																	"999222222",
																	"999333333",
																	"999444444",
																	"999555555",
																	"999666666",
																	"999777777",
																	"999888888",
																	"999999999"});
			this.cmbSendingMemberID.Location = new System.Drawing.Point(160, 24);
			this.cmbSendingMemberID.Name = "cmbSendingMemberID";
			this.cmbSendingMemberID.Size = new System.Drawing.Size(121, 21);
			this.cmbSendingMemberID.TabIndex = 11;
			this.cmbSendingMemberID.Text = "999123456";
			// 
			// cmbRecipientMemberID
			// 
			this.cmbRecipientMemberID.Items.AddRange(new object[] {
																	  "999111111",
																	  "999222222",
																	  "999333333",
																	  "999444444",
																	  "999555555",
																	  "999666666",
																	  "999777777",
																	  "999888888",
																	  "999999999"});
			this.cmbRecipientMemberID.Location = new System.Drawing.Point(160, 56);
			this.cmbRecipientMemberID.Name = "cmbRecipientMemberID";
			this.cmbRecipientMemberID.Size = new System.Drawing.Size(121, 21);
			this.cmbRecipientMemberID.TabIndex = 12;
			this.cmbRecipientMemberID.Text = "999654321";
			// 
			// InviteHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 413);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "InviteHarness";
			this.Text = "InviteHarness (InitiateConversation)";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnInitiateConversation_Click(object sender, System.EventArgs e)
		{
			Matchnet.InstantMessenger.ValueObjects.Message message = new Matchnet.InstantMessenger.ValueObjects.Message();
			message.Body = txtMessage.Text;
			message.Type = MessageType.Normal;
			
			Participant sendingMember = new Participant();
			sendingMember.CommunityID = Convert.ToInt32(cbCommunityID.SelectedItem);
			sendingMember.MemberID = getSenderID();
			sendingMember.Username = txtUsername.Text;
			message.Sender = sendingMember;
			
			// generate the brand 
			Matchnet.Content.ValueObjects.BrandConfig.Brands brands =
				BrandConfigSA.Instance.GetBrandsByCommunity(Convert.ToInt32(cbCommunityID.SelectedItem));

			Brand brand = null;
			IEnumerator brandEnumerator = brands.GetEnumerator();
			while (brandEnumerator.MoveNext())
			{	// take the first brand.
				brand = (Brand)brandEnumerator.Current;
				break;
			}

			ConversationKey key = InstantMessengerSA.Instance.InitiateConversation(
				sendingMember.MemberID
				, getRecipientID()
				, sendingMember.CommunityID
				);

			if (key != null)
			{
				lblGUID.Text = key.GUID.ToString();
				lblIMServer.Text = key.IMServerUrl;

				// spawn conversation window...
				IMConversationHarness frmIMConversation = new IMConversationHarness(key, sendingMember);
				frmIMConversation.MdiParent = this.MdiParent;	// same parent window
				frmIMConversation.Show();
			}
		}

		private void btnStartThreads_Click(object sender, System.EventArgs e)
		{
			btnStartThreads.Enabled = false;
			int numberOfThreads = Convert.ToInt32(txtNumberOfThreads.Text);
			int invitesPerThread = Convert.ToInt32(txtInvitesPerThread.Text);

			ArrayList threads = new ArrayList(numberOfThreads);
			InvitationThread inviteThread;

			// Setup the invitation threads.
			for (int i = 0; i < numberOfThreads; i++)
			{
				inviteThread = new InvitationThread(invitesPerThread);
				Thread thread = new Thread(new ThreadStart(inviteThread.ThreadStart));
				threads.Add(thread);
			}

			// Kickoff the invitation threads
			foreach (Thread thread in threads)
			{
				thread.Start();
			}

			// Join each of the threads.
			foreach (Thread thread in threads)
			{
				thread.Join();
			}

			lblAutoPilotStatus.Text = "Auto pilot invitations have been generated.";
			btnStartThreads.Enabled = true;
		}

		private int getSenderID()
		{
			if (cmbSendingMemberID.SelectedItem != null)
			{
				return Convert.ToInt32(cmbSendingMemberID.SelectedItem);
			}
			else
			{
				return Convert.ToInt32(cmbSendingMemberID.Text);
			}
		}

		private int getRecipientID()
		{
			if (cmbRecipientMemberID.SelectedItem != null)
			{
				return Convert.ToInt32(cmbRecipientMemberID.SelectedItem);
			}
			else
			{
				return Convert.ToInt32(cmbRecipientMemberID.Text);
			}
		}

	}
}
