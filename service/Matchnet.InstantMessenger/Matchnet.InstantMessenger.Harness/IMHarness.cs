using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet;
using Matchnet.InstantMessenger.BusinessLogic;
using Matchnet.InstantMessenger.ServiceManagers;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for IMHarness.
	/// </summary>
	public class IMHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ToolBar toolBar1;
		private System.Windows.Forms.Button btnIMSettings;
		private System.Windows.Forms.Button btnInitiateConversation;
		private System.Windows.Forms.Button btnGetInvites;
		private System.Windows.Forms.Panel panel1;
		//private Matchnet.InstantMessenger.Harness.ucPerformanceCounters ucPerformanceCounters1;
		private System.Windows.Forms.Button btnTestReplication;
		private System.Windows.Forms.Button btnTestSerialization;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public IMHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new IMHarness());
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.toolBar1 = new System.Windows.Forms.ToolBar();
			this.btnIMSettings = new System.Windows.Forms.Button();
			this.btnInitiateConversation = new System.Windows.Forms.Button();
			this.btnGetInvites = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnTestReplication = new System.Windows.Forms.Button();
			this.btnTestSerialization = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// toolBar1
			// 
			this.toolBar1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.toolBar1.ButtonSize = new System.Drawing.Size(39, 28);
			this.toolBar1.DropDownArrows = true;
			this.toolBar1.Location = new System.Drawing.Point(0, 0);
			this.toolBar1.Name = "toolBar1";
			this.toolBar1.ShowToolTips = true;
			this.toolBar1.Size = new System.Drawing.Size(1016, 35);
			this.toolBar1.TabIndex = 0;
			// 
			// btnIMSettings
			// 
			this.btnIMSettings.Location = new System.Drawing.Point(8, 8);
			this.btnIMSettings.Name = "btnIMSettings";
			this.btnIMSettings.Size = new System.Drawing.Size(136, 23);
			this.btnIMSettings.TabIndex = 2;
			this.btnIMSettings.Text = "Test IM Settings";
			this.btnIMSettings.Click += new System.EventHandler(this.btnIMSettings_Click);
			// 
			// btnInitiateConversation
			// 
			this.btnInitiateConversation.Location = new System.Drawing.Point(150, 8);
			this.btnInitiateConversation.Name = "btnInitiateConversation";
			this.btnInitiateConversation.Size = new System.Drawing.Size(136, 23);
			this.btnInitiateConversation.TabIndex = 3;
			this.btnInitiateConversation.Text = "Send Invites";
			this.btnInitiateConversation.Click += new System.EventHandler(this.btnInitiateConversation_Click);
			// 
			// btnGetInvites
			// 
			this.btnGetInvites.Location = new System.Drawing.Point(288, 8);
			this.btnGetInvites.Name = "btnGetInvites";
			this.btnGetInvites.Size = new System.Drawing.Size(136, 23);
			this.btnGetInvites.TabIndex = 4;
			this.btnGetInvites.Text = "Get Invites";
			this.btnGetInvites.Click += new System.EventHandler(this.btnGetInvites_Click);
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel1.Location = new System.Drawing.Point(0, 35);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(250, 538);
			this.panel1.TabIndex = 6;
			// 
			// btnTestReplication
			// 
			this.btnTestReplication.Location = new System.Drawing.Point(432, 8);
			this.btnTestReplication.Name = "btnTestReplication";
			this.btnTestReplication.Size = new System.Drawing.Size(112, 23);
			this.btnTestReplication.TabIndex = 0;
			this.btnTestReplication.Text = "Test Replication";
			// 
			// btnTestSerialization
			// 
			this.btnTestSerialization.Location = new System.Drawing.Point(552, 8);
			this.btnTestSerialization.Name = "btnTestSerialization";
			this.btnTestSerialization.Size = new System.Drawing.Size(120, 24);
			this.btnTestSerialization.TabIndex = 8;
			this.btnTestSerialization.Text = "Test Serialization";
			this.btnTestSerialization.Click += new System.EventHandler(this.btnTestSerialization_Click);
			// 
			// IMHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1016, 573);
			this.Controls.Add(this.btnTestSerialization);
			this.Controls.Add(this.btnTestReplication);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.btnIMSettings);
			this.Controls.Add(this.btnInitiateConversation);
			this.Controls.Add(this.btnGetInvites);
			this.Controls.Add(this.toolBar1);
			this.IsMdiContainer = true;
			this.Name = "IMHarness";
			this.Text = "Instant Messenger Test Harness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnIMSettings_Click(object sender, System.EventArgs e)
		{
			IMSettingsHarness frmIMSettings = new IMSettingsHarness();
			frmIMSettings.MdiParent = this;
			frmIMSettings.Show();
		}

		private void btnInitiateConversation_Click(object sender, System.EventArgs e)
		{
			InviteHarness frmInviteHarness = new InviteHarness();
			frmInviteHarness.MdiParent = this;
			frmInviteHarness.Show();

		}

		private void btnGetInvites_Click(object sender, System.EventArgs e)
		{
			GetInvitesHarness frmGetInvites = new GetInvitesHarness();
			frmGetInvites.MdiParent = this;
			frmGetInvites.Show();
		}

		private void btnTestSerialization_Click(object sender, System.EventArgs e)
		{
			ConversationKey.TestSerialization();
			IMSettings.TestSerialization();
			Participant.TestSerialization();
			Matchnet.InstantMessenger.ValueObjects.Message.TestSerialization();
			ExternalTransactionID.TestSerialization();
			ConversationInvite.TestSerialization();
			ConversationInvites.TestSerialization();
			MessageBox.Show("Serialization test succeeded.");
		}

	}
}
