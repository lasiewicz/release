using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.InstantMessenger.BusinessLogic;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for IMSettingsHarness.
	/// </summary>
	public class IMSettingsHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox cmbCommunityID;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btnGetSettings;
		private System.Windows.Forms.Button btnSaveSettings;
		private System.Windows.Forms.CheckBox chkIsAway;
		private System.Windows.Forms.TextBox txtAwayMessage;
		private System.Windows.Forms.ComboBox cmbMemberID;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public IMSettingsHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.cmbCommunityID = new System.Windows.Forms.ComboBox();
			this.chkIsAway = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtAwayMessage = new System.Windows.Forms.TextBox();
			this.btnGetSettings = new System.Windows.Forms.Button();
			this.btnSaveSettings = new System.Windows.Forms.Button();
			this.cmbMemberID = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Community ID:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 48);
			this.label2.Name = "label2";
			this.label2.TabIndex = 1;
			this.label2.Text = "Member ID:";
			// 
			// cmbCommunityID
			// 
			this.cmbCommunityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbCommunityID.Items.AddRange(new object[] {
																"1",
																"2",
																"3",
																"10",
																"12"});
			this.cmbCommunityID.Location = new System.Drawing.Point(144, 16);
			this.cmbCommunityID.Name = "cmbCommunityID";
			this.cmbCommunityID.Size = new System.Drawing.Size(120, 21);
			this.cmbCommunityID.TabIndex = 3;
			// 
			// chkIsAway
			// 
			this.chkIsAway.Location = new System.Drawing.Point(8, 88);
			this.chkIsAway.Name = "chkIsAway";
			this.chkIsAway.Size = new System.Drawing.Size(120, 24);
			this.chkIsAway.TabIndex = 5;
			this.chkIsAway.Text = "Is Away";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 120);
			this.label3.Name = "label3";
			this.label3.TabIndex = 6;
			this.label3.Text = "Away Message";
			// 
			// txtAwayMessage
			// 
			this.txtAwayMessage.Location = new System.Drawing.Point(120, 120);
			this.txtAwayMessage.MaxLength = 4000;
			this.txtAwayMessage.Multiline = true;
			this.txtAwayMessage.Name = "txtAwayMessage";
			this.txtAwayMessage.Size = new System.Drawing.Size(376, 96);
			this.txtAwayMessage.TabIndex = 7;
			this.txtAwayMessage.Text = "";
			// 
			// btnGetSettings
			// 
			this.btnGetSettings.Location = new System.Drawing.Point(392, 48);
			this.btnGetSettings.Name = "btnGetSettings";
			this.btnGetSettings.Size = new System.Drawing.Size(104, 23);
			this.btnGetSettings.TabIndex = 8;
			this.btnGetSettings.Text = "Get Settings";
			this.btnGetSettings.Click += new System.EventHandler(this.btnGetSettings_Click);
			// 
			// btnSaveSettings
			// 
			this.btnSaveSettings.Enabled = false;
			this.btnSaveSettings.Location = new System.Drawing.Point(384, 224);
			this.btnSaveSettings.Name = "btnSaveSettings";
			this.btnSaveSettings.Size = new System.Drawing.Size(112, 23);
			this.btnSaveSettings.TabIndex = 9;
			this.btnSaveSettings.Text = "Save Settings";
			this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
			// 
			// cmbMemberID
			// 
			this.cmbMemberID.Items.AddRange(new object[] {
															 "988272252",
															 "988272336"});
			this.cmbMemberID.Location = new System.Drawing.Point(144, 48);
			this.cmbMemberID.Name = "cmbMemberID";
			this.cmbMemberID.Size = new System.Drawing.Size(121, 21);
			this.cmbMemberID.TabIndex = 10;
			// 
			// IMSettingsHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(504, 253);
			this.Controls.Add(this.cmbMemberID);
			this.Controls.Add(this.btnSaveSettings);
			this.Controls.Add(this.btnGetSettings);
			this.Controls.Add(this.txtAwayMessage);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.chkIsAway);
			this.Controls.Add(this.cmbCommunityID);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "IMSettingsHarness";
			this.Text = "IMSettingsHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGetSettings_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnGetSettings.Enabled = false;

				// generate the brand 
				Matchnet.Content.ValueObjects.BrandConfig.Brands brands =
					BrandConfigSA.Instance.GetBrandsByCommunity(Convert.ToInt32(cmbCommunityID.SelectedItem));

				Brand brand = null;
				IEnumerator brandEnumerator = brands.GetEnumerator();
				while (brandEnumerator.MoveNext())
				{	// take the first brand.
					brand = (Brand)brandEnumerator.Current;
					break;
				}

				// This block of code uses the Service Adapter
				IMSettings settings = InstantMessengerSettingsSA.Instance.GetSettings(
					Convert.ToInt32(cmbMemberID.SelectedItem)
					, brand);

				if (settings != null)
				{
					chkIsAway.Checked = settings.IsAway;
					txtAwayMessage.Text = settings.AwayMessage;
					btnSaveSettings.Enabled = true;
				}
				else
				{	// Member doesn't exist, disable save.
					btnSaveSettings.Enabled = false;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				btnGetSettings.Enabled = true;
			}
		}

		private void btnSaveSettings_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnSaveSettings.Enabled = false;
				IMSettings settings = new IMSettings(Convert.ToInt32(cmbMemberID.SelectedItem)
					, Convert.ToInt32(cmbCommunityID.SelectedItem));
				settings.AwayMessage = txtAwayMessage.Text;

				// generate the brand 
				Matchnet.Content.ValueObjects.BrandConfig.Brands brands =
					BrandConfigSA.Instance.GetBrandsByCommunity(Convert.ToInt32(cmbCommunityID.SelectedItem));

				Brand brand = null;
				IEnumerator brandEnumerator = brands.GetEnumerator();
				while (brandEnumerator.MoveNext())
				{	// take the first brand.
					brand = (Brand)brandEnumerator.Current;
					break;
				}

				InstantMessengerSettingsSA.Instance.SaveSettings(settings, brand);

			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				btnSaveSettings.Enabled = true;
			}
		}
	}
}
