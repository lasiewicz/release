using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Timers;
using System.Windows.Forms;

using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for IMConversationHarness.
	/// </summary>
	public class IMConversationHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtMessageBody;
		private System.Windows.Forms.Button btnSend;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ConversationKey _ConversationKey;
		private System.Windows.Forms.TextBox txtMessageHistory;
		private System.Windows.Forms.Label lblKey;
		private Participant _Participant;

		private System.Timers.Timer _ReceiveMessagesTimer;

		public IMConversationHarness(ConversationKey conversationKey, Participant participant)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			_ConversationKey = conversationKey;
			_Participant = participant;

			this.Text = participant.Username + " (" + participant.MemberID + ")";
			this.lblKey.Text += _ConversationKey.GUID.ToString();

			// Setup the polling timer to receive messages for this participant.
			_ReceiveMessagesTimer = new System.Timers.Timer(1000);
			_ReceiveMessagesTimer.AutoReset = true;
			_ReceiveMessagesTimer.Enabled = true;
			_ReceiveMessagesTimer.Elapsed += new System.Timers.ElapsedEventHandler(_ReceiveMessagesTimer_Elapsed);
			_ReceiveMessagesTimer.Start();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtMessageHistory = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnSend = new System.Windows.Forms.Button();
			this.txtMessageBody = new System.Windows.Forms.TextBox();
			this.lblKey = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtMessageHistory
			// 
			this.txtMessageHistory.BackColor = System.Drawing.Color.White;
			this.txtMessageHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtMessageHistory.Location = new System.Drawing.Point(8, 24);
			this.txtMessageHistory.Multiline = true;
			this.txtMessageHistory.Name = "txtMessageHistory";
			this.txtMessageHistory.ReadOnly = true;
			this.txtMessageHistory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtMessageHistory.Size = new System.Drawing.Size(400, 160);
			this.txtMessageHistory.TabIndex = 0;
			this.txtMessageHistory.Text = "";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnSend);
			this.groupBox1.Controls.Add(this.txtMessageBody);
			this.groupBox1.Location = new System.Drawing.Point(8, 192);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(400, 88);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "To:";
			// 
			// btnSend
			// 
			this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSend.Location = new System.Drawing.Point(336, 16);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(56, 23);
			this.btnSend.TabIndex = 1;
			this.btnSend.Text = "Send";
			this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// txtMessageBody
			// 
			this.txtMessageBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtMessageBody.Location = new System.Drawing.Point(8, 16);
			this.txtMessageBody.Multiline = true;
			this.txtMessageBody.Name = "txtMessageBody";
			this.txtMessageBody.Size = new System.Drawing.Size(320, 64);
			this.txtMessageBody.TabIndex = 0;
			this.txtMessageBody.Text = "";
			// 
			// lblKey
			// 
			this.lblKey.Location = new System.Drawing.Point(8, 0);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(376, 23);
			this.lblKey.TabIndex = 2;
			this.lblKey.Text = "Key:";
			// 
			// IMConversationHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(416, 285);
			this.Controls.Add(this.lblKey);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.txtMessageHistory);
			this.Name = "IMConversationHarness";
			this.Text = "IMConversationHarness";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnSend_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnSend.Enabled = false;
				Matchnet.InstantMessenger.ValueObjects.Message message = new Matchnet.InstantMessenger.ValueObjects.Message(txtMessageBody.Text, _Participant);
				message.ConversationKey = _ConversationKey;
				//InstantMessengerSA.Instance.SendMessage(message);
			}
			catch (Exception ex)
			{
				MessageBox.Show(this.MdiParent, "Exception caught: " + ex.Message);
			}
			finally
			{
				txtMessageBody.Text = string.Empty;
				btnSend.Enabled = true;
			}

		}

		public void _ReceiveMessagesTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs args)
		{
//			Messages newMessages = InstantMessengerSA.Instance.ReceiveMessages(_ConversationKey, _Participant);
//			if (newMessages != null)
//			{
//				foreach (Message message in newMessages.List)
//				{
//					txtMessageHistory.Text += System.Environment.NewLine
//						+ message.Sender.Username + ": "
//						+ message.Body;
//				}
//			}

		}

	}
}
