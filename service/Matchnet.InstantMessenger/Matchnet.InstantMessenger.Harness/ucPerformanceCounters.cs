using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.InstantMessenger.BusinessLogic;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for ucPerformanceCounters.
	/// </summary>
	public class ucPerformanceCounters : System.Windows.Forms.UserControl
	{
		private DataTable _dtPerformanceCounters;
		private PerformanceCounterCategory _performanceCounterCategory;
		private System.Windows.Forms.DataGrid dgPerformanceCounters;

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Panel panel1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ucPerformanceCounters()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			// Setup the table for displaying performance counter data
			_dtPerformanceCounters = new DataTable("PerformanceCounters");
			_dtPerformanceCounters.Columns.Add("Counter");
			_dtPerformanceCounters.Columns.Add("Value");

			// setup the performance counter category for IM
			_performanceCounterCategory = new PerformanceCounterCategory(ServiceConstants.SERVICE_NAME);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgPerformanceCounters = new System.Windows.Forms.DataGrid();
			this.label1 = new System.Windows.Forms.Label();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.dgPerformanceCounters)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgPerformanceCounters
			// 
			this.dgPerformanceCounters.AllowNavigation = false;
			this.dgPerformanceCounters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.dgPerformanceCounters.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgPerformanceCounters.CaptionVisible = false;
			this.dgPerformanceCounters.DataMember = "";
			this.dgPerformanceCounters.FlatMode = true;
			this.dgPerformanceCounters.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgPerformanceCounters.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.dgPerformanceCounters.Location = new System.Drawing.Point(0, 40);
			this.dgPerformanceCounters.Name = "dgPerformanceCounters";
			this.dgPerformanceCounters.ParentRowsVisible = false;
			this.dgPerformanceCounters.PreferredColumnWidth = 100;
			this.dgPerformanceCounters.ReadOnly = true;
			this.dgPerformanceCounters.RowHeadersVisible = false;
			this.dgPerformanceCounters.Size = new System.Drawing.Size(250, 376);
			this.dgPerformanceCounters.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "Performance Counters:";
			// 
			// btnRefresh
			// 
			this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRefresh.Location = new System.Drawing.Point(152, 8);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.TabIndex = 2;
			this.btnRefresh.Text = "Refresh";
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.btnRefresh);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(232, 40);
			this.panel1.TabIndex = 3;
			// 
			// ucPerformanceCounters
			// 
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.dgPerformanceCounters);
			this.Name = "ucPerformanceCounters";
			this.Size = new System.Drawing.Size(232, 376);
			((System.ComponentModel.ISupportInitialize)(this.dgPerformanceCounters)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnRefresh_Click(object sender, System.EventArgs e)
		{
			_dtPerformanceCounters.Rows.Clear();
			
			foreach (PerformanceCounter counter in _performanceCounterCategory.GetCounters())
			{
				_dtPerformanceCounters.Rows.Add(new string[] {counter.CounterName, counter.RawValue.ToString()});
			}
			dgPerformanceCounters.DataSource = _dtPerformanceCounters;
		}
	}
}
