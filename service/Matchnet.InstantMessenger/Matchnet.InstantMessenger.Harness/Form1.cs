using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.Lib.Util;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblSenderMemberID;
		private System.Windows.Forms.Label lblRecipientMemberID;
		private System.Windows.Forms.TextBox tbSenderMemberID;
		private System.Windows.Forms.TextBox tbRecipientMemberID;
		private System.Windows.Forms.Button btnSend;
		private System.Windows.Forms.TextBox tbMessageBody;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.TextBox tbDomainID;
		private System.Windows.Forms.Label lblDomainID;
		private System.Windows.Forms.Label lblMessageType;
		private System.Windows.Forms.ComboBox cbMessageType;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox tbMessages;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblSenderMemberID = new System.Windows.Forms.Label();
			this.lblRecipientMemberID = new System.Windows.Forms.Label();
			this.tbSenderMemberID = new System.Windows.Forms.TextBox();
			this.tbRecipientMemberID = new System.Windows.Forms.TextBox();
			this.btnSend = new System.Windows.Forms.Button();
			this.tbMessageBody = new System.Windows.Forms.TextBox();
			this.lblMessage = new System.Windows.Forms.Label();
			this.tbDomainID = new System.Windows.Forms.TextBox();
			this.lblDomainID = new System.Windows.Forms.Label();
			this.lblMessageType = new System.Windows.Forms.Label();
			this.cbMessageType = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.tbMessages = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblSenderMemberID
			// 
			this.lblSenderMemberID.Location = new System.Drawing.Point(16, 136);
			this.lblSenderMemberID.Name = "lblSenderMemberID";
			this.lblSenderMemberID.Size = new System.Drawing.Size(96, 16);
			this.lblSenderMemberID.TabIndex = 0;
			this.lblSenderMemberID.Text = "SenderMemberID";
			// 
			// lblRecipientMemberID
			// 
			this.lblRecipientMemberID.Location = new System.Drawing.Point(16, 168);
			this.lblRecipientMemberID.Name = "lblRecipientMemberID";
			this.lblRecipientMemberID.Size = new System.Drawing.Size(104, 16);
			this.lblRecipientMemberID.TabIndex = 1;
			this.lblRecipientMemberID.Text = "RecipientMemberID";
			// 
			// tbSenderMemberID
			// 
			this.tbSenderMemberID.Location = new System.Drawing.Point(128, 136);
			this.tbSenderMemberID.Name = "tbSenderMemberID";
			this.tbSenderMemberID.Size = new System.Drawing.Size(64, 20);
			this.tbSenderMemberID.TabIndex = 2;
			this.tbSenderMemberID.Text = "";
			// 
			// tbRecipientMemberID
			// 
			this.tbRecipientMemberID.Location = new System.Drawing.Point(128, 168);
			this.tbRecipientMemberID.Name = "tbRecipientMemberID";
			this.tbRecipientMemberID.Size = new System.Drawing.Size(64, 20);
			this.tbRecipientMemberID.TabIndex = 3;
			this.tbRecipientMemberID.Text = "";
			// 
			// btnSend
			// 
			this.btnSend.Location = new System.Drawing.Point(232, 136);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(56, 24);
			this.btnSend.TabIndex = 4;
			this.btnSend.Text = "Send";
			this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// tbMessageBody
			// 
			this.tbMessageBody.Location = new System.Drawing.Point(128, 208);
			this.tbMessageBody.Multiline = true;
			this.tbMessageBody.Name = "tbMessageBody";
			this.tbMessageBody.Size = new System.Drawing.Size(184, 72);
			this.tbMessageBody.TabIndex = 5;
			this.tbMessageBody.Text = "Test message.";
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(16, 208);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(104, 16);
			this.lblMessage.TabIndex = 6;
			this.lblMessage.Text = "Message";
			// 
			// tbDomainID
			// 
			this.tbDomainID.Location = new System.Drawing.Point(128, 104);
			this.tbDomainID.Name = "tbDomainID";
			this.tbDomainID.Size = new System.Drawing.Size(64, 20);
			this.tbDomainID.TabIndex = 9;
			this.tbDomainID.Text = "";
			// 
			// lblDomainID
			// 
			this.lblDomainID.Location = new System.Drawing.Point(16, 104);
			this.lblDomainID.Name = "lblDomainID";
			this.lblDomainID.Size = new System.Drawing.Size(96, 16);
			this.lblDomainID.TabIndex = 8;
			this.lblDomainID.Text = "DomainID";
			// 
			// lblMessageType
			// 
			this.lblMessageType.Location = new System.Drawing.Point(16, 288);
			this.lblMessageType.Name = "lblMessageType";
			this.lblMessageType.Size = new System.Drawing.Size(104, 16);
			this.lblMessageType.TabIndex = 10;
			this.lblMessageType.Text = "Message Type";
			// 
			// cbMessageType
			// 
			this.cbMessageType.Location = new System.Drawing.Point(128, 288);
			this.cbMessageType.Name = "cbMessageType";
			this.cbMessageType.Size = new System.Drawing.Size(200, 21);
			this.cbMessageType.TabIndex = 11;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(232, 168);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 24);
			this.button1.TabIndex = 12;
			this.button1.Text = "Receive";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// tbMessages
			// 
			this.tbMessages.Location = new System.Drawing.Point(16, 8);
			this.tbMessages.Multiline = true;
			this.tbMessages.Name = "tbMessages";
			this.tbMessages.ReadOnly = true;
			this.tbMessages.Size = new System.Drawing.Size(368, 80);
			this.tbMessages.TabIndex = 13;
			this.tbMessages.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(408, 341);
			this.Controls.Add(this.tbMessages);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.cbMessageType);
			this.Controls.Add(this.lblMessageType);
			this.Controls.Add(this.tbDomainID);
			this.Controls.Add(this.lblDomainID);
			this.Controls.Add(this.lblMessage);
			this.Controls.Add(this.tbMessageBody);
			this.Controls.Add(this.tbRecipientMemberID);
			this.Controls.Add(this.tbSenderMemberID);
			this.Controls.Add(this.btnSend);
			this.Controls.Add(this.lblRecipientMemberID);
			this.Controls.Add(this.lblSenderMemberID);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnSend_Click(object sender, System.EventArgs e)
		{
			Matchnet.InstantMessenger.ValueObjects.Message message = new Matchnet.InstantMessenger.ValueObjects.Message();

			int domainID = Util.CInt(this.tbDomainID.Text);
			int senderMemberID = Util.CInt( this.tbSenderMemberID.Text);
			int recipientMemberID = Util.CInt(this.tbRecipientMemberID.Text);
			message.Body = this.tbMessageBody.Text;

			InstantMessengerSA.Instance.SendIM(senderMemberID, recipientMemberID, domainID, message); 
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			DataRow row;
			DataTable dt = new DataTable();
			dt.Columns.Add("Value");
			dt.Columns.Add("Display");

			row = dt.NewRow();
			row["Value"] = MessageType.Admin;
			row["Display"] = "Admin";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["Value"] = MessageType.Normal;
			row["Display"] = "Normal";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["Value"] = MessageType.Close;
			row["Display"] = "Close";
			dt.Rows.Add(row);

			cbMessageType.DataSource = dt;
			cbMessageType.ValueMember = "Value";
			cbMessageType.DisplayMember = "Display";
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			int domainID = Util.CInt(this.tbDomainID.Text, Constants.NULL_INT);
			int senderMemberID = Util.CInt( this.tbSenderMemberID.Text, Constants.NULL_INT);
			int recipientMemberID = Util.CInt(this.tbRecipientMemberID.Text, Constants.NULL_INT);
			int pollingInterval = 0;

			if (domainID == Constants.NULL_INT || recipientMemberID == Constants.NULL_INT)
			{
				return;
			}

			MessageCollection messageCollection;

			if (senderMemberID != Constants.NULL_INT)
			{
				messageCollection = InstantMessengerSA.Instance.IMMessagesRecv(recipientMemberID, senderMemberID, domainID, out pollingInterval);
				MessageBox.Show("Polling Interval: " + pollingInterval.ToString());
			}
			else
			{
				messageCollection = InstantMessengerSA.Instance.IMConversationsPending(recipientMemberID, domainID);
			}

			foreach (Matchnet.InstantMessenger.ValueObjects.Message message in messageCollection)
			{
				this.tbMessages.Text += message.SenderMemberID + ": " + message.Body + System.Environment.NewLine;
			}
		}
	}
}
