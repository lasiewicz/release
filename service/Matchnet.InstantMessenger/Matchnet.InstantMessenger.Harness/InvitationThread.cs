using System;
using System.Collections;
using System.Threading;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.InstantMessenger.ServiceAdapters;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.Harness
{
	/// <summary>
	/// Summary description for InvitationThread.
	/// </summary>
	public class InvitationThread
	{
		private int _NumberOfInvitations;

		public InvitationThread(int numberOfInvitations)
		{
			_NumberOfInvitations = numberOfInvitations;
		}

		public void ThreadStart()
		{
			Matchnet.InstantMessenger.ValueObjects.Message _message;
			Participant _sendingMember;
			System.Random _random = new Random();

			try
			{
				for (int i = 0; i < _NumberOfInvitations; i++)
				{
					// Create a new participant each time.
					_sendingMember = new Participant();
					_sendingMember.CommunityID = _random.Next(12);
					_sendingMember.MemberID = _random.Next(999999999);
					_sendingMember.Username = _sendingMember.MemberID.ToString();
	                
					// create a new message each time.
					_message = new Message("Message body, this represents the initial message sent as the invitation"
						, _sendingMember);

//					ConversationKey key = InstantMessengerSA.Instance.InitiateConversation(_message
//						, _random.Next(999999999), getBrand(), "server url");

					Thread.Sleep(100);	
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error occurred in InvitationThread", ex);
			}
		}

		// returns a random brand object.
		private Matchnet.Content.ValueObjects.BrandConfig.Brand getBrand()
		{
			// generate the brand 
			Matchnet.Content.ValueObjects.BrandConfig.Brands brands =
				BrandConfigSA.Instance.GetBrandsByCommunity(1);

			Brand brand = null;
			IEnumerator brandEnumerator = brands.GetEnumerator();
			while (brandEnumerator.MoveNext())
			{	// take the first brand.
				brand = (Brand)brandEnumerator.Current;
				break;
			}
			return brand;
		}

	}
}
