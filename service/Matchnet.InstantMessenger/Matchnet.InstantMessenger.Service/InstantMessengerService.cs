using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using log4net;
using Matchnet.Exceptions;
using Matchnet.InstantMessenger.BusinessLogic;
using Matchnet.InstantMessenger.ServiceManagers;
using Matchnet.RemotingServices;

namespace Matchnet.InstantMessenger.Service
{
	public class InstantMessengerService : Matchnet.RemotingServices.RemotingServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private static readonly ILog Log = LogManager.GetLogger(typeof(InstantMessengerService));
		private InstantMessengerSM _instantMessengerSM = null;
        
		public InstantMessengerService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new InstantMessengerService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Register the service managers for this service.
		/// - InstantMessenger
		/// </summary>
		protected override void RegisterServiceManagers()
		{
			try
			{
                Log.Info("Registering the service manager.");

				// Initialize service managers.
				_instantMessengerSM = new InstantMessengerSM();

				// Register them.
				base.RegisterServiceManager(_instantMessengerSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service, see details: " + ex.Message);
			}
		}

		protected override void OnStop()
		{
			base.OnStop ();
		}

	}
}
