using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Represents an individual Member's Instant Messenger settings
	/// </summary>
	[Serializable()]
	public class IMSettings : IValueObject, ICacheable, ISerializable, IByteSerializable, ICustomSerializable
	{
		#region Private Members

		private int _communityID;
		private int _memberID;
		private string _awayMessage;

		private const string CACHE_KEY = "~IMSETTINGS^{0}^{1}";
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
		private int _cacheTTLSeconds = 0;

		private const byte VERSION_001 = 1;

		#endregion

		#region Constructors

		public IMSettings()
		{
		}

		public IMSettings(int memberID, int communityID)
		{
			MemberID = memberID;
			CommunityID = communityID;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Represents an individual Member's Instant Messenger away message.
		/// </summary>
		/// <preliminary>
		/// <param name="AwayMessage">The text message, if any, to accompany a Member's away status of 1 (yes).  This value is set by the Member for display when other Members try to initiate an IM session to the Member.</param>
		/// <returns>The text message, regardless of whether the request was a getter or setter request.  </returns>
		/// <exception cref="">NA</exception>
		/// <seealso cref="IsAway">A Member's AwayMessage will only be displayed to other Members when IsAway is set to 1.</seealso>
		/// </preliminary>
		public string AwayMessage
		{
			get 
			{
				return _awayMessage;
			}
			set 
			{
				_awayMessage = value;
			}
		}

		/// <summary>
		/// Represents the community ID for which these IM settings are applicable.
		/// </summary>
		public int CommunityID
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

		/// <summary>
		/// Represents an individual Member's Instant Messenger away status.
		/// </summary>
		/// <preliminary>
		/// <param name="IsAway">The value set by the Member to show his/her status as Away when other Members try to initiate an IM session to the Member.  0=not away (default), 1=away.</param>
		/// <returns>The Member's away status (0 or 1), regardless of whether the request was a getter or setter request.</returns>
		/// <exception cref="">NA</exception>
		/// <seealso cref="AwayMessage">When an Away Status is set to 1 the Member MAY also have an AwayMessage set.</seealso>
		/// </preliminary>
		public bool IsAway
		{
			get 
			{	// If they have an away message defined, they are away.
				return (_awayMessage != string.Empty);
			}
		}

		/// <summary>
		/// Represents the member ID for which these IM settings are applicable.
		/// </summary>
		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		public string GetCacheKey()
		{
			return IMSettings.GetCacheKey(MemberID, CommunityID);
		}
		
		public static string GetCacheKey(int memberID, int communityID)
		{
			return string.Format(CACHE_KEY, memberID, communityID);
		}

		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			IMSettings i = obj as IMSettings;

			if (i == null)
			{
				return false;
			}
			
			return (i.MemberID == MemberID && i.CommunityID == CommunityID && i.AwayMessage == AwayMessage);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode ();
		}
		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray",ToByteArray());
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region Serialization helper methods
		internal IMSettings(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected IMSettings(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(VERSION_001);
			serializer.Write(_communityID);
			serializer.Write(_memberID);
			serializer.Write(_awayMessage);
			serializer.Write((Int32) _cachePriority);
			serializer.Write(_cacheTTLSeconds);
		}

		public void FromBytes(Deserializer deserializer)
		{
			byte version = deserializer.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_communityID = deserializer.ReadInt32();
					_memberID = deserializer.ReadInt32();
					_awayMessage = deserializer.ReadString();
					_cachePriority = (CacheItemPriorityLevel) deserializer.ReadInt32();
					_cacheTTLSeconds = deserializer.ReadInt32();
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 12; //version, _memberID, _communityID
			if (_awayMessage != null)
			{
				bytes += _awayMessage.Length * 2;
			}
			return bytes;
		}

		public static void TestSerialization()
		{
			IMSettings settings1 = new IMSettings(123, 456);
			settings1.AwayMessage = "test";
			
			SerializationInfo info = new SerializationInfo(typeof(ConversationKey), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			settings1.GetObjectData(info, context);

			//Deserialize
			IMSettings settings2 = new IMSettings(info, context);

			System.Diagnostics.Debug.Assert(settings1.MemberID == settings2.MemberID && settings1.CommunityID == settings2.CommunityID
				&& settings1.AwayMessage == settings2.AwayMessage,
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion
	}
}
