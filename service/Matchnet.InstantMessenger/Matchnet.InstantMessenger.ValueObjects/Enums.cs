﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.InstantMessenger.ValueObjects
{
    public enum CanIMReasons
    {
        None,
        SenderInvalid,
        TargetInvalid,
        SenderIsAllAccess,
        BothAreSubscribers,
        SenderBlockedTarget,
        TargetBlockedSender,
        SenderIsNonSubscriber,
        TargetIsNonSubscriber,
        SenderSuspended,
        TargetSuspended,
        SenderSelfSuspended,
        TargetSelfSuspended,
        TargetFraudCheckPending,
        SenderFraudCheckPending,
        SenderHidden,
        TargetHidden,
        SenderIsSubscriberCanInvite

    }
}
