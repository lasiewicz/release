using System;
using System.Collections;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for IConversationInvites.
	/// </summary>
	public interface IConversationInvites : ICacheable, IReplicable, IEnumerable
	{
		IConversationInvite this[IConversationKey conversationKey]
		{
			get; set;
		}

		int Count
		{
			get;
		}

		//TODO: This is needed for compatibility with v1.1 of the web code.
		//It should be removed after the 1.2 release.
		ICollection List
		{
			get;
		}

		void Add(IConversationInvite conversationInvite);

		void Remove(IConversationInvite conversationInvite);

		object SyncRoot {get;}
	}

	public interface IConversationInvite 
	{

		int CommunityID
		{
			get;
			set;
		}

		DateTime InviteDate
		{
			get;
			set;
		}

		IMessage Message
		{
			get;
			set;
		}

		int RecipientID
		{
			get;
			set;
		}

		IConversationKey ConversationKey
		{
			get;
			set;
		}

		ExternalTransactionID ExternalTransactionID
		{
			get;
			set;
		}
		bool IsRead {get;set;}
	}

	public interface IConversationKey
	{
		string GUID
		{
			get;
		}

		string IMServerUrl
		{
			get;
			set;
		}
	}
}
