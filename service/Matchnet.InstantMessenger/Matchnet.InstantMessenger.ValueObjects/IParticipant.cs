using System;
using System.Collections;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// A collection of participants in a conversation.
	/// </summary>
	public interface IParticipants
	{
		// Need some List/Collection property that is dictionary based but still enumerable.

		void Add(IParticipant Participant);

		/// <summary>
		/// Returns the IParticipant in [index] position (zero based).
		/// </summary>
		IParticipant this[string participantKey] 
		{
			get;
		}

		/// <summary>
		/// Number of participants in this list
		/// </summary>
		int Count
		{
			get;
		}

		ICollection List
		{
			get;
		}

	}

	/// <summary>
	/// Summary description for IParticipant.
	/// </summary>
	public interface IParticipant
	{
		int CommunityID
		{
			get;
			set;
		}

		int MemberID
		{
			get;
			set;
		}

		string Username
		{
			get;
			set;
		}

		IMSettings Settings
		{
			get;
			set;
		}

	}
}
