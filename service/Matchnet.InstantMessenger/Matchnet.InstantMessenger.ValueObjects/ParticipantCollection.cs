using System;
using System.Collections;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// A collection (Dictionary) of Participants in the context of the Instant Messenger service.
	/// The dictionary is keyed by the Participant.MemberID attribute.
	/// </summary>
	[Serializable()]
	public class ParticipantCollection : CollectionBase, IValueObject
	{
		
		public ParticipantCollection()
		{
		}

		public Participant this[string memberID]
		{
			get
			{
				lock (this.InnerList.SyncRoot)
				{
					foreach (Participant participant in this.InnerList)
					{
						if (participant.MemberID.Equals(memberID))
						{
							return participant;
						}
					}
				}
				return null;
			}
		}
		
		public void Add(Participant participant)
		{
			Participant existingParticipant = this[participant.MemberID.ToString()] as Participant;
			if (existingParticipant != null)
			{
				this.InnerList.Remove(existingParticipant);
			}
			this.InnerList.Add(participant);
		}

		public void Remove(Participant participant)
		{
			if (participant != null)
			{
				this.InnerList.Remove(participant);
			}
		}

		public new ICollection List
		{
			get
			{
				return this.InnerList;
			}
		}
	}
}
