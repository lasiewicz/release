using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for ConversationKey.
	/// </summary>
	[Serializable()]
	public class ConversationKey : IValueObject, ISerializable, IByteSerializable, ICustomSerializable, IConversationKey
	{
		private string _GUID;
		private string _IMServerUrl;

		private const byte VERSION_001 = 1;

		public ConversationKey()
		{
			_GUID = System.Guid.NewGuid().ToString();
		}

		private ConversationKey(string GUIDOverride)
		{
			_GUID = GUIDOverride;
		}

		#region Properties

		public string GUID
		{
			get
			{
				return _GUID;
			}
		}

		public string IMServerUrl
		{
			get
			{
				return _IMServerUrl;
			}
			set
			{
				_IMServerUrl = value;
			}
		}

		#endregion

		public static ConversationKey New(string conversationKeyGUID)
		{
			return new ConversationKey(conversationKeyGUID);
		}

		#region Overrides
		public override bool Equals(object obj)
		{
			ConversationKey keyToCompare = obj as ConversationKey;
			return _GUID.Equals(keyToCompare.GUID);
		}
        
		public override int GetHashCode()
		{
			return _GUID.GetHashCode();
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region ICustomSerializable Members

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(VERSION_001);
			serializer.Write(_GUID);
			serializer.Write(_IMServerUrl);
		}

		public void FromBytes(Deserializer deserializer)
		{
			byte version = deserializer.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_GUID = deserializer.ReadString();
					_IMServerUrl = deserializer.ReadString();
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		#endregion

		#region Serialization helper methods
		internal ConversationKey(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected ConversationKey(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 4; //version
			if (_GUID != null)
			{
				bytes += _GUID.Length * 2;
			}
			if (_IMServerUrl != null)
			{
				bytes += _IMServerUrl.Length * 2;
			}
			return bytes;
		}

		public static void TestSerialization()
		{
			//Create a ConversationKey object
			ConversationKey conversationKey1 = new ConversationKey();
			
			SerializationInfo info = new SerializationInfo(typeof(ConversationKey), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			conversationKey1.GetObjectData(info, context);

			//Deserialize
			ConversationKey conversationKey2 = new ConversationKey(info, context);

			//Compare
			System.Diagnostics.Debug.Assert(conversationKey1.Equals(conversationKey2),
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion
	}
}
