using System;
using System.Collections;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// A collection of messages.
	/// </summary>
	public interface IMessages
	{
		ICollection List
		{
			get;
		}

		/// <summary>
		/// Number of messages in this list.
		/// </summary>
		int Count
		{
			get;
		}

		void Add(IMessage message);

	}

	/// <summary>
	/// Summary description for IMessage.
	/// </summary>
	public interface IMessage
	{
		IParticipant Sender
		{
			get;
			set;
		}

		string Body
		{
			get;
			set;
		}

		MessageType Type
		{
			get;
			set;
		}

		IConversationKey ConversationKey
		{
			get;
			set;
		}
	}
}
