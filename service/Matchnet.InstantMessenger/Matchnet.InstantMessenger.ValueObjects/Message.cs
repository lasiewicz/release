using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Represents a single message transmitted through the IM client
	/// </summary>
	[Serializable()]
	public class Message : IValueObject, ISerializable, IByteSerializable, ICustomSerializable, IMessage
	{
		#region Private Members
		
		private string _body;
		private MessageType _type = MessageType.Normal;
		private ConversationKey _conversationKey;

		// this becomes Participant Sender
		private int _senderMemberID;
		private Participant _sender;

		private const byte VERSION_001 = 1;
		
		#endregion


		#region Constructors

		public Message()
		{
		}

		public Message(string body)
		{
			_body = body;
		}

		public Message(string body, Participant sender)
		{
			_sender = sender;
			_body = body;
		}

		#endregion


		#region Attributes

		public string Body
		{
			get {return _body;}
			set {_body = value;}
		}

		public MessageType Type
		{
			get {return _type;}
			set {_type = value;}
		}

		public int SenderMemberID
		{
			get {return _senderMemberID;}
			set {_senderMemberID = value;}
		}

		public Participant Sender
		{
			get
			{
				return _sender;
			}
			set
			{
				_sender = value;
			}
		}

		public ConversationKey ConversationKey
		{
			get
			{
				return _conversationKey;
			}
			set
			{
				_conversationKey = value;
			}
		}

		#endregion


		#region Overrides
		public override bool Equals(object obj)
		{
			Message message = obj as Message;
			return (message.ConversationKey == ConversationKey && message.Body == Body && message.SenderMemberID == SenderMemberID && message.Type == Type);
		}
        
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region ICustomSerializable Members

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(VERSION_001);
			serializer.Write(_body);
			serializer.Write(_conversationKey);
			serializer.Write(_sender);
			serializer.Write(_senderMemberID);
			serializer.Write((Int32) _type);
		}

		public void FromBytes(Deserializer deserializer)
		{
			byte version = deserializer.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_body = deserializer.ReadString();
					_conversationKey = (ConversationKey) deserializer.ReadObject(new ConversationKey());
					_sender = (Participant) deserializer.ReadObject(new Participant());
					_senderMemberID = deserializer.ReadInt32();
					_type = (MessageType) deserializer.ReadInt32();
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		#endregion

		#region Serialization helper methods
		internal Message(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected Message(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 12; //version, _senderMemberID, _type;
			bytes += Serializer.GetByteCount(_body);
			bytes += Serializer.GetByteCount(_conversationKey);
			bytes += Serializer.GetByteCount(_sender);
			
			return bytes;
		}

		public static void TestSerialization()
		{
			//Create a Message object
			Message message1 = new Message();
			message1.Body = "body";
			message1.Sender = new Participant();
			message1.Sender.MemberID = 12345;
			message1.Type = MessageType.Close;
			message1.SenderMemberID = 56789;
			
			SerializationInfo info = new SerializationInfo(typeof(Message), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			message1.GetObjectData(info, context);

			//Deserialize
			Message message2 = new Message(info, context);

			//Compare
			System.Diagnostics.Debug.Assert(message1.Equals(message2),
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion

		#region IMessage Members

		IParticipant Matchnet.InstantMessenger.ValueObjects.IMessage.Sender
		{
			get
			{
				return _sender as IParticipant;
			}
			set
			{
				_sender = value as Participant;
			}
		}

		IConversationKey Matchnet.InstantMessenger.ValueObjects.IMessage.ConversationKey
		{
			get
			{
				return _conversationKey;
			}
			set
			{
				_conversationKey = value as ConversationKey;
			}
		}

		#endregion
	}

	public enum MessageType : int
	{
		Normal = 0,
		Close = -1,
		Admin = 1
	}
}
