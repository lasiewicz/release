using System;
using System.Collections;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for IConversation.
	/// </summary>
	public interface IConversation : ICacheable
	{

		/// <summary>
		/// Returns the total participant count since the inception of this conversation.
		/// </summary>
		int ParticipantCount
		{
			get;
		}

		/// <summary>
		/// Returns the total number of messages sent during this conversation.
		/// </summary>
		int MessageCount
		{
			get;
		}

		/// <summary>
		/// Adds a new participant to the conversation based on the supplied parameter.
		/// </summary>
		/// <param name="participant"></param>
		void AddParticipant(IParticipant participant);

		/// <summary>
		/// Delivers a message to all participants involved in this conversation.  NOTE: The sender is excluded from message delivery.
		/// </summary>
		/// <param name="message"></param>
		void DeliverMessage(IMessage message);

		/// <summary>
		/// Returns an IMessages object representing any new messages not previously delivered to the participant making this request.
		/// </summary>
		/// <param name="participant"></param>
		/// <returns></returns>
		IMessages ReceiveMessages(IParticipant participant);

		/// <summary>
		/// Permanently removes the participant corresponding to the supplied memberID from this conversation instance. 
		/// </summary>
		/// <param name="memberID"></param>
		void RemoveParticipant(int memberID);
	}



}
