using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.Caching;


namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// ExternalTransactionID represents a unique transaction ID passed from Userplane
	/// to detect when duplicate requests occur.
	/// </summary>
	[Serializable]
	public class ExternalTransactionID : ICacheable, IReplicable, ISerializable, IByteSerializable, ICustomSerializable
	{
		private const string CACHE_KEY_PREFIX = "ExternalTransactionID";
		private const byte VERSION_001 = 1;

		string _transactionID;
		UserplaneTransactionType _userplaneTransactionType;
		Int32 _communityID;
		private Int32 _cacheTTLSeconds = 300;
		private CacheItemMode _cacheMode = CacheItemMode.Absolute;
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;

		#region Constructors

		internal ExternalTransactionID()
		{
		}

		public ExternalTransactionID(string transactionID, UserplaneTransactionType userplaneTransactionType, Int32 communityID)
		{
			_transactionID = transactionID;
			_userplaneTransactionType = userplaneTransactionType;
			_communityID = communityID;
		}
		#endregion

		#region Properties
		public string TransactionID
		{
			get
			{
				return _transactionID;
			}
		}

		public UserplaneTransactionType UserplaneTransactionType
		{
			get
			{
				return _userplaneTransactionType;
			}
		}

		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}
		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		public string GetCacheKey()
		{
			return GetCacheKey(_transactionID, _userplaneTransactionType, _communityID);
		}

		public static string GetCacheKey(string transactionID, UserplaneTransactionType userplaneTransactionType, Int32 communityID)
		{
			return CACHE_KEY_PREFIX + "^" + transactionID + "^" + userplaneTransactionType.ToString() + "^" + communityID.ToString();
		}

		#endregion

		#region IReplicable Members

		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
		
		#region Overrides
		public override bool Equals(object obj)
		{
			ExternalTransactionID externalTransactionID = obj as ExternalTransactionID;
			return (externalTransactionID.CommunityID == CommunityID
				&& externalTransactionID.TransactionID == TransactionID
				&& externalTransactionID.UserplaneTransactionType == UserplaneTransactionType);
		}
        
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region ICustomSerializable Members

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(VERSION_001);
			serializer.Write(_communityID);
			serializer.Write(_transactionID);
			serializer.Write((Int32) _userplaneTransactionType);
		}

		public void FromBytes(Deserializer deserializer)
		{
			byte version = deserializer.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_communityID = deserializer.ReadInt32();
					_transactionID = deserializer.ReadString();
					_userplaneTransactionType = (UserplaneTransactionType) deserializer.ReadInt32();
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		#endregion

		#region Serialization helper methods
		internal ExternalTransactionID(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected ExternalTransactionID(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 4; //version
			/*if (_GUID != null)
			{
				bytes += _GUID.Length * 2;
			}
			if (_IMServerUrl != null)
			{
				bytes += _IMServerUrl.Length * 2;
			}*/
			return bytes;
		}

		public static void TestSerialization()
		{
			//Create a ExternalTransactionID object
			ExternalTransactionID externalTransactionID1 = new ExternalTransactionID("test", UserplaneTransactionType.AddFriend, 69);
			
			SerializationInfo info = new SerializationInfo(typeof(ExternalTransactionID), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			externalTransactionID1.GetObjectData(info, context);

			//Deserialize
			ExternalTransactionID externalTransactionID2 = new ExternalTransactionID(info, context);

			//Compare
			System.Diagnostics.Debug.Assert(externalTransactionID1.Equals(externalTransactionID2),
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion
	}

	/// <summary>
	/// 
	/// </summary>
	public enum UserplaneTransactionType
	{
		/// <summary></summary>
		AddFriend = 1,
		/// <summary></summary>
		GetBlockedStatus,
		/// <summary></summary>
		GetDomainPreferences,
		/// <summary></summary>
		GetMemberID,
		/// <summary></summary>
		NotifyConnectionClosed,
		/// <summary></summary>
		SetBlockedStatus,
		/// <summary></summary>
		StartConversation,
		/// <summary></summary>
		StartIC
	}
}
