﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.InstantMessenger.ValueObjects
{
    public class CanMemberIMResponse
    {
        public bool CanIM { get; set; }
        public CanIMReasons Reason { get; set; }
    }
}
