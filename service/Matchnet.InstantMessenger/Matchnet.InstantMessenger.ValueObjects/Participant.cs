using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for Participant.
	/// </summary>
	[Serializable()]
	public class Participant : IValueObject, ISerializable, IByteSerializable, ICustomSerializable, IParticipant
	{
		#region Private Members
		private int _communityID;
		private int _memberID;
		private string _username;
		private IMSettings _settings;

		private const byte VERSION_001 = 1;
		#endregion

		#region Constructors
		public Participant()
		{

		}
		#endregion

		#region Properties
		public int CommunityID
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}

		public string Username
		{
			get
			{
				return _username;
			}
			set
			{
				_username = value;
			}
		}

		public IMSettings Settings
		{
			get
			{
				return _settings;
			}
			set
			{
				_settings = value;
			}
		}
		#endregion

		#region Static methods
		public static Participant New(int memberID, int communityID)
		{
			Participant participant = new Participant();
			participant.MemberID = memberID;
			participant.CommunityID = communityID;
			return participant;
		}
		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			Participant p = obj as Participant;

			if (p == null)
			{
				return false;
			}

			return ((p.MemberID == MemberID && p.CommunityID == CommunityID && p.Username == Username) &&
				(p.Settings == null && Settings == null) || p.Settings.Equals(Settings));
		}

		public override int GetHashCode()
		{
			return base.GetHashCode ();
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region ICustomSerializable Members

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(VERSION_001);
			serializer.Write(_communityID);
			serializer.Write(_memberID);
			serializer.Write(_username);
			serializer.Write(_settings);
		}

		public void FromBytes(Deserializer deserializer)
		{
			byte version = deserializer.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_communityID = deserializer.ReadInt32();
					_memberID = deserializer.ReadInt32();
					_username = deserializer.ReadString();
					_settings = (IMSettings) deserializer.ReadObject(new IMSettings());
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		#endregion

		#region Serialization helper methods
		internal Participant(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected Participant(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 12; //version, _memberID, _communityID
			if (_username != null)
			{
				bytes += _username.Length * 2;
			}
			if (_settings != null)
			{
				bytes += _settings.GetByteCount();
			}
			return bytes;
		}

		public static void TestSerialization()
		{
			//Create a Participant object
			Participant participant1 = new Participant();
			participant1.MemberID = 123;
			participant1.CommunityID = 456;
			participant1.Username = "test";
			participant1.Settings = new IMSettings(666, 999);
			
			SerializationInfo info = new SerializationInfo(typeof(ConversationKey), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			participant1.GetObjectData(info, context);

			//Deserialize
			Participant participant2 = new Participant(info, context);

			//Compare
			System.Diagnostics.Debug.Assert(participant1.Equals(participant2),
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion
	}
}
