using System;
using System.Collections;


namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for IMConversation.
	/// </summary>
	[Serializable()]
	public class IMConversation : IValueObject
	{
		private ConversationKey					_conversationKey;
		private ParticipantCollection			_participants;
		private int								_messageCount;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private int								_cacheTTLSeconds;
		private Hashtable						_pendingMessages;

		public IMConversation(ConversationKey conversationKey, Participant originatingMember)
		{
			// Initialize the pending messages collection
			_pendingMessages = new Hashtable(2);		// currently defaults to 2 participants.

			// create & initialize the participants collection
			_participants = new ParticipantCollection();

			this.AddParticipant(originatingMember);

			_conversationKey = conversationKey;	// initialize the conversation key.
		}

		#region Conversation implementation

		/// <summary>
		/// Returns the total participant count since the inception of this conversation.
		/// </summary>
		public int ParticipantCount
		{
			get
			{
				return _participants.Count;
			}
		}

		/// <summary>
		/// Returns the total number of messages sent during this conversation.
		/// </summary>
		public int MessageCount
		{
			get
			{
				return _messageCount;
			}
		}

		/// <summary>
		/// Adds a new participant to the conversation based on the supplied parameter.
		/// </summary>
		/// <param name="participant"></param>
		public void AddParticipant(Participant participant)
		{
			_participants.Add(participant);

			// Initialize mesage collection for this participant.
			_pendingMessages.Add(participant.MemberID, new MessageCollection());
		}

		/// <summary>
		/// Delivers a message to all participants involved in this conversation.  NOTE: The sender is excluded from message delivery.
		/// </summary>
		/// <param name="message"></param>
		public void DeliverMessage(Message message)
		{
			_messageCount++;
			lock (_participants.List.SyncRoot)
			{
				foreach (Participant participant in _participants)
				{
					lock (_pendingMessages.SyncRoot)
					{
						if (message.Sender.MemberID != participant.MemberID)	//Don't send the message to the sender
						{
							MessageCollection messages = _pendingMessages[participant.MemberID] as MessageCollection;
							messages.Add(message);
						}
					}
				}
			}
		}

		/// <summary>
		/// Returns a MessageCollection object representing any new messages not previously delivered to the participant making this request.
		/// </summary>
		/// <param name="participant"></param>
		/// <returns></returns>
		public MessageCollection ReceiveMessages(Participant participant)
		{
			MessageCollection pendingMessagesForThisParticipant = null;
			lock (_pendingMessages.SyncRoot)
			{
				if (_pendingMessages.ContainsKey(participant.MemberID))
				{
					pendingMessagesForThisParticipant = _pendingMessages[participant.MemberID] as MessageCollection;
					// remove the message collection
					_pendingMessages.Remove(pendingMessagesForThisParticipant);
					
					// re-initialize a new collection for future messages.
					_pendingMessages[participant.MemberID] = new MessageCollection();
				}
			}
			return pendingMessagesForThisParticipant;
		}

		public void RemoveParticipant(int participantID)
		{
			// remove the associated participant entry from the participants collection
			lock (_participants.List.SyncRoot)
			{
				foreach (Participant participant in _participants.List)
				{
					if (participant.MemberID.Equals(participantID))
					{
						_participants.Remove(participant);
						break;
					}
				}
			}

			// remove the messages collection for this participant
			MessageCollection messages = _pendingMessages[participantID] as MessageCollection;
			if (messages != null)
			{
				_pendingMessages.Remove(messages);
			}
		}

		#endregion

		#region ICacheable implementation

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		public string GetCacheKey()
		{
			return _conversationKey.GUID;
		}

		#endregion
	}
}
