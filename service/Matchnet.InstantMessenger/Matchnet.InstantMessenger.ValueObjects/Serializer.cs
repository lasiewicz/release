using System;
using System.IO;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for Serializer.
	/// </summary>
	public class Serializer
	{
		private MemoryStream _ms;
		private BinaryWriter _bw;

		public Serializer(Int32 bytes)
		{
			_ms = new MemoryStream(bytes);
			_bw = new BinaryWriter(_ms);
		}

		public void Write(ICustomSerializable obj)
		{
			_bw.Write(obj == null);

			if (obj != null)
			{
				obj.ToBytes(this);
			}			
		}

		public void Write(Byte val)
		{
			_bw.Write(val);
		}

		public void Write(Int32 val)
		{
			_bw.Write(val);
		}

		public void Write(string val)
		{
			_bw.Write(val == null);

			if (val != null)
			{
				_bw.Write(val);
			}
		}

		public void Write(DateTime val)
		{
			_bw.Write(val.Ticks);
		}

		public void Write(bool val)
		{
			_bw.Write(val);
		}
        
		public byte[] ToByteArray()
		{
			return _ms.ToArray();
		}

		//returns number of bytes required to serialize this object with nulls
		public static Int32 GetByteCount(string s)
		{
			return s == null ? 1 : 1 + s.Length * 2;
		}

		//returns number of bytes required to serialize this object with nulls
		public static Int32 GetByteCount(ICustomSerializable obj)
		{
			return obj == null ? 1 : 1 + obj.GetByteCount();
		}
	}

	public class Deserializer
	{
		private MemoryStream _ms;
		private BinaryReader _br;

		public Deserializer(byte[] bytes)
		{
            _ms = new MemoryStream(bytes);
			_br = new BinaryReader(_ms);
		}

		public Int32 ReadInt32()
		{
			return _br.ReadInt32();
		}

		public byte ReadByte()
		{
			return _br.ReadByte();
		}

		public string ReadString()
		{
			bool isNull = _br.ReadBoolean();
			return isNull ? null : _br.ReadString();
		}

		public DateTime ReadDateTime()
		{
			return new DateTime(_br.ReadInt64());
		}

		public Boolean ReadBoolean()
		{
			return _br.ReadBoolean();
		}

		public ICustomSerializable ReadObject(ICustomSerializable obj)
		{
			bool isNull = _br.ReadBoolean();

			if (isNull)
			{
				return null;
			}
			else
			{
				obj.FromBytes(this);
				return obj;
			}			
		}
	}

	public interface ICustomSerializable
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="bw"></param>
		void ToBytes(Serializer serializer);
		void FromBytes(Deserializer deserializer);
		Int32 GetByteCount();
	}
}
