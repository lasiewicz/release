using System;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Summary description for IMSettingsKey.
	/// </summary>
	public class IMSettingsKey
	{
		private const string CACHE_KEY = "~IMSETTINGS^{0}^{1}";

		private IMSettingsKey()
		{
		}

		public static string GetCacheKey(int memberID, int communityID)
		{
			return string.Format(CACHE_KEY, memberID, communityID);
		}
	}
}
