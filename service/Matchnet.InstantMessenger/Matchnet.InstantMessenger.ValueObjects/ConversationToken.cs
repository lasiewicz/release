﻿#region

using System;

#endregion

namespace Matchnet.InstantMessenger.ValueObjects
{
    /// <summary>
    ///     Stored in Couchbase for supporting AA
    ///     Please refer to "All Access support for ejabberd IM" on Confluence
    /// </summary>
    [Serializable]
    public class ConversationToken
    {
        /// <summary>
        ///     Unique identifier. Guid hash code
        /// </summary>
        public string ConversationTokenId { get; set; }

        /// <summary>
        ///     Member who initiated the conversation
        /// </summary>
        public int SenderMemberId { get; set; }

        /// <summary>
        ///     Member who received an invitation from the sender member
        /// </summary>
        public int RecipientMemberId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CommunityId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GenerateMembaseId(int senderMemberId, int recipientMemberId, int communityId)
        {
            return senderMemberId + "_" + recipientMemberId + "_" + communityId;
        }
    }
}