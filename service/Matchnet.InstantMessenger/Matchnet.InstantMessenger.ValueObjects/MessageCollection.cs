using System;
using System.Collections;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// Represents a collection of messages transmitted through the IM client
	/// </summary>
	[Serializable()]
	public class MessageCollection : CollectionBase, IValueObject
	{
		#region Constructors

		public MessageCollection()
		{
		}

		#endregion

		#region Messages Members

		public void Add(Message message)
		{
			lock(this.InnerList.SyncRoot)
			{
				this.InnerList.Add(message);
			}
		}

		public new ICollection List
		{
			get
			{
				return this.InnerList;
			}
		}

		public new int Count
		{
			get
			{
				return this.InnerList.Count;
			}
		}

		#endregion
	}
}
