using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// The ConversationInvites object represents a collection of conversation invites, typically
	/// </summary>
	[Serializable()]
	public class ConversationInvites : IValueObject, IEnumerable, ICacheable, IReplicable, IConversationInvites
	{
		public const string INVITE_CACHE_PREFIX = "~IMINVITE^{0}^{1}";

		private Hashtable _invites;
		private string _cacheKey;
		private int _cacheTTLSeconds;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

		private const byte VERSION_001 = 1;

		#region Constructors

		public ConversationInvites(ConversationInvite conversationInvite, int cacheTTLSeconds)
		{
			_invites = new Hashtable(1);
			Add(conversationInvite);
			_cacheKey = ConversationInvites.GetCacheKey(conversationInvite.RecipientID, conversationInvite.CommunityID);
			_cacheTTLSeconds = cacheTTLSeconds;
		}

		#endregion 

		#region ConversationInvites implementation

		public ConversationInvite this[ConversationKey conversationKey]
		{
			get
			{
				lock (_invites.SyncRoot)
				{
					return _invites[conversationKey] as ConversationInvite;
				}
			}
			set
			{
				lock (_invites.SyncRoot)
				{
					_invites[conversationKey] = value;
				}
			}
		}

		/// <summary>
		/// The ConversationInvite will be added to this invites collection as long
		/// as the Message's Sender does NOT already have an invite in this collection.
		/// </summary>
		/// <param name="conversationInvite"></param>
		public void Add(ConversationInvite conversationInvite)
		{
			// on the add, we are going to impose a unique constraint on 1 invite per member/recipient combo
			bool inviteAlreadyExists = false;
			lock (_invites.SyncRoot)
			{
				foreach (ConversationInvite invite in _invites.Values)
				{
					if (invite.Message.Sender.MemberID.Equals(
						conversationInvite.Message.Sender.MemberID))
					{
						inviteAlreadyExists = true;
						break;
					}
				}
			}

			if (! inviteAlreadyExists)
			{
				lock (_invites.SyncRoot)
				{
					this._invites.Add(conversationInvite.ConversationKey, conversationInvite);
				}
			}
		}

		public void Remove(ConversationInvite conversationInvite)
		{
			lock (_invites.SyncRoot)
			{
				_invites.Remove(conversationInvite.ConversationKey);
			}
		}

		public int Count
		{
			get
			{
				lock (_invites.SyncRoot)
				{
					return _invites.Count;
				}
			}
		}

		//TODO: This is needed for compatibility with v1.1 of the web code.
		//It should be removed after the 1.2 release.
		public ICollection List
		{
			get
			{
				return _invites.Values;
			}
		}

		#endregion

		#region ICacheable implementation

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		public string GetCacheKey()
		{
			return _cacheKey;
		}

		#endregion

		#region Static Methods
		public static string GetCacheKey(int recipientID, int communityID)
		{
			return String.Format(INVITE_CACHE_PREFIX, recipientID, communityID);
		}
		
		public static string GetCacheKey(ConversationInvite conversationInvite)
		{
			return ConversationInvites.GetCacheKey(conversationInvite.RecipientID, conversationInvite.CommunityID);
		}
		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(this.GetCacheKey());
		}

		#endregion

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _invites.Values.GetEnumerator();
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get
			{
				return _invites.Values.SyncRoot;
			}
		}


		#region Overrides
		public override bool Equals(object obj)
		{
			ConversationInvites invites = obj as ConversationInvites;
			bool equal = true;
			
			foreach (ConversationInvite invite in invites)
			{
				if (this[invite.ConversationKey] == null)
				{
					equal = false;
				}
			}

			return equal;
		}
        
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region ICustomSerializable Members

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(VERSION_001);
			serializer.Write(_cacheKey);
			serializer.Write(_cacheTTLSeconds);
			serializer.Write((Int32) _cachePriority);

			serializer.Write(_invites.Count);
			foreach (ConversationInvite invite in this)
			{
				serializer.Write(invite);
			}
		}

		public void FromBytes(Deserializer deserializer)
		{
			byte version = deserializer.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_cacheKey = deserializer.ReadString();
					_cacheTTLSeconds = deserializer.ReadInt32();
					_cachePriority = (CacheItemPriorityLevel) deserializer.ReadInt32();

					Int32 count = deserializer.ReadInt32();
					_invites = new Hashtable(count);
					for (Int32 i = 0; i < count; i++)
					{
						Add((ConversationInvite) deserializer.ReadObject(new ConversationInvite()));
					}
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 13; //version, _cacheTTLSeconds, _cachePriority, _invites.Count
			bytes += Serializer.GetByteCount(_cacheKey);
			foreach (ConversationInvite invite in this)
			{
				bytes += Serializer.GetByteCount(invite);
			}
			return bytes;
		}

		#endregion

		#region Serialization helper methods
		internal ConversationInvites(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected ConversationInvites(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public static void TestSerialization()
		{
			//Create a ConversationInvites object
			ConversationInvite conversationInvite = new ConversationInvite();
			conversationInvite.ConversationKey = new ConversationKey();
			ConversationInvites conversationInvites1 = new ConversationInvites(conversationInvite, 123);
			
			SerializationInfo info = new SerializationInfo(typeof(ConversationInvites), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			conversationInvites1.GetObjectData(info, context);

			//Deserialize
			ConversationInvites conversationInvites2 = new ConversationInvites(info, context);

			//Compare
			System.Diagnostics.Debug.Assert(conversationInvites1.Equals(conversationInvites2),
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion

		#region IConversationInvites Members

		IConversationInvite Matchnet.InstantMessenger.ValueObjects.IConversationInvites.this[IConversationKey conversationKey]
		{
			get
			{
				return _invites[conversationKey as ConversationKey] as IConversationInvite;
			}
			set
			{
				_invites[conversationKey as ConversationKey] = value as ConversationInvite;
			}
		}

		void Matchnet.InstantMessenger.ValueObjects.IConversationInvites.Add(IConversationInvite conversationInvite)
		{
			Add(conversationInvite as ConversationInvite);
		}

		void Matchnet.InstantMessenger.ValueObjects.IConversationInvites.Remove(IConversationInvite conversationInvite)
		{
			Remove(conversationInvite as ConversationInvite);
		}

		#endregion
	}
}
