using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.InstantMessenger.ValueObjects
{
	/// <summary>
	/// A ConversationInvite represents the initial IM message from one member to another.
	/// The invite contains the message along with the necessary information for the 
	/// recipient to join the conversation on the appropriate IM server.
	/// </summary>
	[Serializable()]
	public class ConversationInvite : IValueObject, ISerializable, IByteSerializable, ICustomSerializable, IConversationInvite
	{
		private int _communityID;
		private DateTime _inviteDate;
		private Message _message;
		private int _recipientID;
		private ConversationKey _conversationKey;
		private ExternalTransactionID _externalTransactionID;
		private bool _isRead = false;
		private int _imID;

		private const byte VERSION_001 = 1;
		private const byte VERSION_002 = 2;
		private byte _version = VERSION_001;

		#region Constructors
		public ConversationInvite()
		{
		}
		#endregion

		#region ConversationInvite implementation

		public int CommunityID
		{
			get
			{
				return _communityID;
			}
			set
			{
				_communityID = value;
			}
		}

		public DateTime InviteDate
		{
			get
			{
				return _inviteDate;
			}
			set
			{
				_inviteDate = value;
			}
		}

		public Message Message
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}

		public int RecipientID
		{
			get
			{
				return _recipientID;
			}
			set
			{
				_recipientID = value;
			}
		}

		public ConversationKey ConversationKey
		{
			get
			{
				return _conversationKey;
			}
			set
			{
				_conversationKey = value;
			}
		}

		public ExternalTransactionID ExternalTransactionID
		{
			get
			{
				return _externalTransactionID;
			}
			set
			{
				_externalTransactionID = value;
			}
		}

		/// <summary>
		/// Whether or not this invitation was rendered to the user (used for purposes of playing alert sound once only, for example)
		/// </summary>
		public bool IsRead
		{
			get
			{
				return _isRead;
			}
			set
			{
				_isRead = value;
			}

		}

		/// <summary>
		/// IM id that Provo uses
		/// </summary>
		public int IMID
		{
			get
			{
				return _imID;
			}
			set
			{
				_imID = value;
			}

		}

		public byte Version
		{
			get
			{
				return _version;
			}
			set
			{
				_version = value;
			}

		}
		#endregion

		#region Overrides
		public override bool Equals(object obj)
		{
			ConversationInvite invite = obj as ConversationInvite;
			return (invite.ConversationKey == null && ConversationKey == null) || ConversationKey.Equals(invite.ConversationKey);
		}
        
		public override int GetHashCode()
		{
			return ConversationKey.GetHashCode();
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			FromBytes(new Deserializer(bytes));
		}

		public byte[] ToByteArray()
		{
			Serializer serializer = new Serializer(GetByteCount());

			ToBytes(serializer);

			return serializer.ToByteArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region ICustomSerializable Members

		public void ToBytes(Serializer serializer)
		{
			serializer.Write(_version);
			serializer.Write(_communityID);
			serializer.Write(_inviteDate);
			serializer.Write(_message);
			serializer.Write(_recipientID);
			serializer.Write(_conversationKey);
			serializer.Write(_externalTransactionID);
			serializer.Write(_isRead);

            if(_version == VERSION_002)
				serializer.Write(_imID);
		}

		public void FromBytes(Deserializer deserializer)
		{
			_version = deserializer.ReadByte();

			switch (_version)
			{
				case VERSION_001:
					_communityID = deserializer.ReadInt32();
					_inviteDate = deserializer.ReadDateTime();
					_message = (Message) deserializer.ReadObject(new Message());
					_recipientID = deserializer.ReadInt32();
					_conversationKey = (ConversationKey) deserializer.ReadObject(new ConversationKey());
					_externalTransactionID = (ExternalTransactionID) deserializer.ReadObject(new ExternalTransactionID());
					_isRead = deserializer.ReadBoolean();
					break;
				case VERSION_002:	// _imID added
					_communityID = deserializer.ReadInt32();
					_inviteDate = deserializer.ReadDateTime();
					_message = (Message) deserializer.ReadObject(new Message());
					_recipientID = deserializer.ReadInt32();
					_conversationKey = (ConversationKey) deserializer.ReadObject(new ConversationKey());
					_externalTransactionID = (ExternalTransactionID) deserializer.ReadObject(new ExternalTransactionID());
					_isRead = deserializer.ReadBoolean();
					_imID = deserializer.ReadInt32();
					break;
				default:
					throw new Exception("Unsupported serialization version (" + _version.ToString() + ").");
			}
		}

		public Int32 GetByteCount()
		{
			Int32 bytes = 21; //version, _communityID, _inviteDate, _recepientID, _isRead
			bytes += Serializer.GetByteCount(_message) + Serializer.GetByteCount(_conversationKey) + Serializer.GetByteCount(_externalTransactionID);
			
			if(_version == VERSION_002)
				bytes += 4;
			
			return bytes;
		}

		#endregion

		#region Serialization helper methods
		internal ConversationInvite(Deserializer deserializer)
		{
			FromBytes(deserializer);
		}

		protected ConversationInvite(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public static void TestSerialization()
		{
			//Create a ConversationInvite object
			ConversationInvite conversationInvite1 = new ConversationInvite();
			conversationInvite1.Version = 2;
			conversationInvite1.IMID = 123456;
			
			SerializationInfo info = new SerializationInfo(typeof(ConversationInvite), new System.Runtime.Serialization.FormatterConverter());
			StreamingContext context = new StreamingContext(StreamingContextStates.All);

			//Serialize
			conversationInvite1.GetObjectData(info, context);

			//Deserialize
			ConversationInvite conversationInvite2 = new ConversationInvite(info, context);

			//Compare
			System.Diagnostics.Debug.Assert(conversationInvite1.Equals(conversationInvite2),
				"Serialization test failed.", "Deserialized object did not match original object.");
		}
		#endregion

		#region IConversationInvite Members

		IMessage Matchnet.InstantMessenger.ValueObjects.IConversationInvite.Message
		{
			get
			{
				return _message as IMessage;
			}
			set
			{
				_message = value as Message;
			}
		}

		IConversationKey Matchnet.InstantMessenger.ValueObjects.IConversationInvite.ConversationKey
		{
			get
			{
				return _conversationKey as IConversationKey;
			}
			set
			{
				_conversationKey = value as ConversationKey;
			}
		}

		#endregion
	}
}
