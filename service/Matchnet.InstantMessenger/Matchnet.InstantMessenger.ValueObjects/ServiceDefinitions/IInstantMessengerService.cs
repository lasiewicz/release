using System;
using System.Collections.Generic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IInstantMessengerService.
	/// </summary>
	public interface IInstantMessengerService
	{
		/// <summary>
		/// Used to inform the InstantMessenger service that a recipient has accepted a conversation to 
		/// join an IM Conversation.  The service will subsequently remove this invite from the cache.
		/// </summary>
		/// <param name="ConversationInvite"></param>
		void AcceptInvitation(ConversationInvite ConversationInvite);

		/// <summary>
		/// Used to send the initial message from one member to another to begin an
		/// IM Conversation.  The invite will reside in the middle-tier conversations
		/// directory until it is accepted.
		/// 
		/// If an invitation is not joined after a certain amount of time, it will self-expire.
		/// </summary>
		/// <param name="ConversationInvite"></param>
		void SendInvitation(ConversationInvite ConversationInvite);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		ConversationInvites GetInvites(int recipientMemberID, int communityID);
									// replaces the IMConvesationsPending call (polling for messages)
									// consumed by the DHTML notify script to launch IM window for recipient.

		/// <summary>
		/// 
		/// </summary>
		/// <param name="externalTransactionID"></param>
		/// <param name="replicate"></param>
		/// <returns></returns>
		bool CacheExternalTransactionID(ExternalTransactionID externalTransactionID);

		/// <summary>
		/// Allows you to save an existing conversation invitation for the purposes of marking it as Read for example.
		/// </summary>
		void Save(ConversationInvite conversationInvite);

		/// <summary>
		/// Deprecate after the other method is in production.
		/// </summary>
		/// <param name="messageSave"></param>
		/// <param name="saveInSent"></param>
		/// <param name="brand"></param>
		/// <param name="transactionID"></param>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void SendMissedIM(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID);

	    /// <summary>
        /// Old MissedIM with XML used for userplane process
	    /// </summary>
	    /// <param name="senderMemberId"></param>
	    /// <param name="targetMemberId"></param>
	    /// <param name="messagesXml"></param>
	    /// <param name="senderBrand"></param>
        [System.Runtime.Remoting.Messaging.OneWay()]
	    void SendMissedIM(int senderMemberId, int targetMemberId, string messagesXml, Brand senderBrand);

        /// <summary>
        /// Processes Missed IM messages which may include custom markup to support photos, emoticons, etc
        /// </summary>
        /// <param name="senderMemberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="messages"></param>
        /// <param name="senderBrand"></param>
        [System.Runtime.Remoting.Messaging.OneWay()]
	    void SendMissedIM(int senderMemberId, int targetMemberId, List<string> messages, Brand senderBrand);
	}
}
