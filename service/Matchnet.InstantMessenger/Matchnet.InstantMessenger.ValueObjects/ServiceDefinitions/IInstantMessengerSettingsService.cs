using System;

using Matchnet.InstantMessenger.ValueObjects;

namespace Matchnet.InstantMessenger.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IInstantMessengerSettingsService.
	/// </summary>
	public interface IInstantMessengerSettingsService
	{
		/// <summary>
		/// Retrieve a member's IMSettings.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		IMSettings GetSettings(int memberID, int communityID);

		/// <summary>
		/// Save a member's IMSettings.
		/// </summary>
		/// <param name="awaySettings"></param>
		void SaveSettings(IMSettings settings);

	}
}
