﻿using Matchnet.Content.ServiceAdapters;
using Matchnet.InstantMessenger.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.InstantMessenger.Test
{
    
    
    /// <summary>
    ///This is a test class for InstantMessengerBLTest and is intended
    ///to contain all InstantMessengerBLTest Unit Tests
    ///</summary>
    [TestClass()]
    public class InstantMessengerBLTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SendMissedIM
        ///</summary>
        [TestMethod()]
        public void SendMissedIMTest()
        {
            const int senderMemberId = 101802106; //wlee2@spark.net
            const int targetMemberId = 16000206; //wlee@spark.net
            const string messagesXml =
                "<?xml version='1.0' encoding='iso-8859-1'?><undeliveredMessages><message>This was the first typed message.</message><message>This was the second typed message.</message></undeliveredMessages>";
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(1003);
            InstantMessengerBL.Instance.SendMissedIM(senderMemberId,
                                                     targetMemberId,
                                                     messagesXml,
                                                     senderBrand);
            Assert.Inconclusive("Check the target member's inbox for the missed IM message to verify.");
        }
    }
}
