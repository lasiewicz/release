#region

using System;
using System.Threading;
using log4net;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.List.ServiceAdapters;

#endregion

namespace Matchnet.InstantMessenger.BusinessLogic
{
    /// <summary>
    ///   Used for sending an on-site Email and ExternalMail notification for a missed IM message.
    ///   The sending is performed in a new thread.
    /// </summary>
    public class MissedIMThread
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (InstantMessengerBL));
        private readonly MessageSave _atQuotaErrorMessage;
        private readonly Brand _brand;
        private readonly MessageSave _messageSave;
        private readonly bool _saveInSent;

        /// <summary>
        /// </summary>
        /// <param name = "messageSave"></param>
        /// <param name = "saveInSent"></param>
        /// <param name = "brand"></param>
        /// <param name = "atQuotaErrorMessage"></param>
        public MissedIMThread(MessageSave messageSave, bool saveInSent, Brand brand, MessageSave atQuotaErrorMessage)
        {
            _messageSave = messageSave;
            _saveInSent = saveInSent;
            _brand = brand;
            _atQuotaErrorMessage = atQuotaErrorMessage;
        }

        ///<summary>
        ///</summary>
        public void Start()
        {
//#if DEBUG
            DoSend();
//#else
//            new Thread(DoSend).Start();
//#endif
        }

        private void DoSend()
        {
            try
            {
                var messageToSend = _messageSave;

                if (QuotaSA.Instance.IsAtQuota(_messageSave.GroupID, _messageSave.FromMemberID, QuotaType.MissedIM))
                {
                    Log.DebugFormat("MissedIMThread.DoSend() - Quota Reached. ToMemberID:{0} FromMemberID:{1}",
                                    _messageSave.ToMemberID,
                                    _messageSave.FromMemberID);
                    messageToSend = _atQuotaErrorMessage;
                }
                else
                {
                    QuotaSA.Instance.AddQuotaItem(_messageSave.GroupID, _messageSave.FromMemberID, QuotaType.MissedIM);
                }

                var messageSendResult = EmailMessageSA.Instance.SendMessage(messageToSend, _saveInSent,
                                                                            Constants.NULL_INT, false);
                if (messageSendResult.Status == MessageSendStatus.Success)
                {
                    Log.DebugFormat("MissedIMThread.DoSend() - Sent missed IM internal mail. ToMemberID:{0} FromMemberID:{1}",
                                    messageSendResult.EmailMessage.ToMemberID,
                                    messageSendResult.EmailMessage.FromMemberID);

                    if (ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage,
                                                                             _brand.BrandID,
                                                                             messageSendResult.RecipientUnreadCount))
                        Log.DebugFormat("MissedIMThread.DoSend() - Sent missed IM external mail. ToMemberID:{0} FromMemberID:{1}",
                                        messageSendResult.EmailMessage.ToMemberID,
                                        messageSendResult.EmailMessage.FromMemberID);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Error sending missed IM", ex);
            }
        }
    }
}