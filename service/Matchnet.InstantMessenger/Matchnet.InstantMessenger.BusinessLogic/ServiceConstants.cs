using System;

namespace Matchnet.InstantMessenger.BusinessLogic
{
	/// <summary>
	/// Summary description for ServiceConstants.
	/// </summary>
	public class ServiceConstants
	{
		private ServiceConstants()
		{
		}

		/// <summary>
		/// Service name (Matchnet.InstantMessenger.Service)
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.InstantMessenger.Service";

		/// <summary>
		/// Service Constant (INSTANTMESSENGER_SVC)
		/// </summary>
		public const string SERVICE_CONSTANT = "INSTANTMESSENGER_SVC";
	}
}
