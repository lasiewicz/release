using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Xml;
using log4net;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Spark.Common.Localization;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;

namespace Matchnet.InstantMessenger.BusinessLogic
{
	/// <summary>
	/// Summary description for InstantMessengerBL.
	/// </summary>
	public class InstantMessengerBL
	{
        private static readonly ILog Log = LogManager.GetLogger(typeof(InstantMessengerBL));
		public const int GC_THRESHOLD = 1000;

		#region Class members

		private Matchnet.Caching.Cache _Cache;
		private CacheItemRemovedCallback _expireInvitation;
		private string _expiredCountLock = "";
		private int _expiredCount = 0;

		#endregion

		#region Singleton implementation
		public static readonly InstantMessengerBL Instance = new InstantMessengerBL();

		private InstantMessengerBL()
		{
            _Cache = Matchnet.Caching.Cache.Instance;
			
			// Initialize call back reference for handling events related to items being removed from the cache.
			_expireInvitation = new CacheItemRemovedCallback(this.expireInvitationCallback);
		}
		#endregion

		#region Delegates and Event definitions

		/// <summary>
		/// Delegate signaute for handling ReplicationRequested events.
		/// </summary>
		public delegate void ReplicationEventHandler(IReplicable replicableObject);

		/// <summary>
		/// Event raised when this service encouners/alters a VO that requires replication.
		/// </summary>
		public event ReplicationEventHandler ReplicationRequested;

		/// <summary>
		/// Delegate signature for handling a InvitationAdded event.
		/// </summary>
		public delegate void InvitationAddEventHandler();

		/// <summary>
		/// Event raised when invitations are added to the invitation queue.
		/// </summary>
		public event InvitationAddEventHandler InvitationAdded;

		/// <summary>
		/// Delegate signature for handling a InvitationRemoved event.
		/// </summary>
		public delegate void InvitationRemoveEventHandler();
		/// <summary>
		/// Event raised when an invitation is removed from the invitation queue.
		/// </summary>
		public event InvitationRemoveEventHandler InvitationRemoved;

		#endregion

		public void AcceptInvitation(ConversationInvite conversationInvite)
		{
			ConversationInvites invites = _Cache[ConversationInvites.GetCacheKey(conversationInvite)] as ConversationInvites;
			if (invites != null)
			{
				// remove this invite, then replicate the invites container
				invites.Remove(conversationInvite);
				ReplicationRequested(invites);
			}
		}

		/// <summary>
		/// This method will return a collection of conversation invites (i.e. Pending IM Conversations)
		/// for the recipient member and community supplied in the parameter list.
		/// 
		/// Returns null if none exist.
		/// </summary>
		/// <param name="recipientMemberID"></param>
		/// <param name="communityID"></param>
		/// <returns>An ConversationInvites collection, returns null if none exist for the supplied parameters.</returns>
		public ConversationInvites GetInvites(int recipientMemberID, int communityID)
		{
			return _Cache[ConversationInvites.GetCacheKey(recipientMemberID, communityID)] as ConversationInvites;
		}

		public void SendInvitation(ConversationInvite conversationInvite)
		{
			addInvitation(conversationInvite);		// Add invitation to recipient invite queue.
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageSave"></param>
		/// <param name="saveInSent"></param>
		/// <param name="brand"></param>
		/// <param name="transactionID"></param>
		public void SendMissedIM(MessageSave messageSave, bool saveInSent, Brand brand, string transactionID)
		{
			ExternalTransactionID externalTransactionID = null;

			if (transactionID != Constants.NULL_STRING) 
			{
				externalTransactionID = new ExternalTransactionID(transactionID, UserplaneTransactionType.NotifyConnectionClosed, brand.Site.Community.CommunityID);
			}

			if (CacheExternalTransactionID(externalTransactionID))  //If this ExternalTransactionID is already cached, don't send the missed IM -- we already sent it
			{
				return;
			}

			MessageSendResult messageSendResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.SendMessage(messageSave, saveInSent, Constants.NULL_INT, false);
			if (messageSendResult.Status == MessageSendStatus.Success)
			{
				Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendInternalMailNotification(messageSendResult.EmailMessage, brand.BrandID, messageSendResult.RecipientUnreadCount);
			}
		}

        /// <summary>
        /// Old MissedIM with XML used for userplane process
        /// </summary>
        /// <param name="senderMemberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="messagesXml"></param>
        /// <param name="senderBrand"></param>
        public void SendMissedIM(int senderMemberId, int targetMemberId, string messagesXml, Brand senderBrand)
        {
            try
            {
                Log.DebugFormat("InstantMessengerBL.SendMissedIM() - SenderMemberID:{0} TargetMemberID:{1} MessagesXML:{2}", senderMemberId, targetMemberId, messagesXml);

                List.ServiceAdapters.List list = ListSA.Instance.GetList(targetMemberId);
                if (list.IsHotListed(HotListCategory.IgnoreList, senderBrand.Site.Community.CommunityID, senderMemberId))
                {
                    //sender is blocked, do not send missed IM mail
                    //note: for now we will also not send an ignore email
                    Log.DebugFormat("InstantMessengerBL.SendMissedIM() - Sender is BLOCKED, no missed IM email will be sent.  SenderMemberID:{0} TargetMemberID:{1} BrandID:{2}", senderMemberId, targetMemberId, senderBrand.BrandID);
                }
                else
                {
                    int brandId;
                    var targetMember = MemberSA.Instance.GetMember(targetMemberId, MemberLoadFlags.None);
                    targetMember.GetLastLogonDate(senderBrand.Site.Community.CommunityID, out brandId);
                    var targetBrand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    var senderMember = MemberSA.Instance.GetMember(senderMemberId, MemberLoadFlags.None);

                    var messageSave = new MessageSave(senderMemberId,
                                                      targetMemberId,
                                                      senderBrand.Site.Community.CommunityID,
                                                      senderBrand.Site.SiteID,
                                                      MailType.MissedIM,
                                                      ResourceProvider.Instance.GetResourceValue(
                                                          targetBrand.Site.SiteID, targetBrand.Site.CultureInfo,
                                                          ResourceGroupEnum.
                                                              InstantMessenger,
                                                          "IM_MESSAGES_MISSED"),
                                                      TransformUndeliveredMessages(messagesXml));

                    var saveCopy = false;
                    var mailboxMask =
                        (AttributeOptionMailboxPreference)
                        senderMember.GetAttributeInt(senderBrand, "MailboxPreference");

                    if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) ==
                        AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
                    {
                        saveCopy = true;
                    }

                    var quotaMessage = GetQuotaErrorMessage(targetMember, targetBrand, senderBrand, messageSave);

                    new MissedIMThread(messageSave, saveCopy, senderBrand, quotaMessage).Start();
                }
            }
            catch (Exception exception)
            {
                throw new BLException(
                    String.Format("Error in SendMissedIM. SenderMemberID:{0} TargetMemberID:{1} MessagesXML:{2}",
                                  senderMemberId, targetMemberId, messagesXml), exception);
            }
        }

        /// <summary>
        /// Processes Missed IM messages which may include custom markup to support photos, emoticons, etc
        /// </summary>
        /// <param name="senderMemberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="messages"></param>
        /// <param name="senderBrand"></param>
        public void SendMissedIM(int senderMemberId, int targetMemberId, List<string> messages, Brand senderBrand)
        {
            if (messages != null && messages.Count > 0)
            {
                try
                {
                    Log.DebugFormat(
                        "InstantMessengerBL.SendMissedIM() - SenderMemberID:{0} TargetMemberID:{1} Messages:{2}",
                        senderMemberId, targetMemberId, string.Join(", ", messages.ToArray()));

                    List.ServiceAdapters.List list = ListSA.Instance.GetList(targetMemberId);
                    if (list.IsHotListed(HotListCategory.IgnoreList, senderBrand.Site.Community.CommunityID,
                                         senderMemberId))
                    {
                        //sender is blocked, do not send missed IM mail
                        //note: for now we will also not send an ignore email
                        Log.DebugFormat(
                            "InstantMessengerBL.SendMissedIM() - Sender is BLOCKED, no missed IM email will be sent.  SenderMemberID:{0} TargetMemberID:{1} BrandID:{2}",
                            senderMemberId, targetMemberId, senderBrand.BrandID);
                    }
                    else
                    {
                        int brandId;
                        var targetMember = MemberSA.Instance.GetMember(targetMemberId, MemberLoadFlags.None);
                        targetMember.GetLastLogonDate(senderBrand.Site.Community.CommunityID, out brandId);
                        var targetBrand = BrandConfigSA.Instance.GetBrandByID(brandId);
                        var senderMember = MemberSA.Instance.GetMember(senderMemberId, MemberLoadFlags.None);

                        var messageSave = new MessageSave(senderMemberId,
                                                          targetMemberId,
                                                          senderBrand.Site.Community.CommunityID,
                                                          senderBrand.Site.SiteID,
                                                          MailType.MissedIM,
                                                          ResourceProvider.Instance.GetResourceValue(
                                                              targetBrand.Site.SiteID, targetBrand.Site.CultureInfo,
                                                              ResourceGroupEnum.
                                                                  InstantMessenger,
                                                              "IM_MESSAGES_MISSED"),
                                                          TransformMissedIMMessages(messages));

                        var saveCopy = false;
                        var mailboxMask =
                            (AttributeOptionMailboxPreference)
                            senderMember.GetAttributeInt(senderBrand, "MailboxPreference");

                        if ((mailboxMask & AttributeOptionMailboxPreference.SaveCopiesOfSentMessages) ==
                            AttributeOptionMailboxPreference.SaveCopiesOfSentMessages)
                        {
                            saveCopy = true;
                        }

                        var quotaMessage = GetQuotaErrorMessage(targetMember, targetBrand, senderBrand, messageSave);

                        new MissedIMThread(messageSave, saveCopy, senderBrand, quotaMessage).Start();
                    }
                }
                catch (Exception exception)
                {
                    throw new BLException(
                        String.Format("Error in SendMissedIM. SenderMemberID:{0} TargetMemberID:{1} Messages:{2}",
                                      senderMemberId, targetMemberId, string.Join(", ", messages.ToArray())), exception);
                }
            }
        }

		public int InvitationCount
		{
			get
			{
				return _Cache.Count;
			}
		}


		/// <summary>
		/// Allows you to save an existing conversation invitation for the purposes of marking it as Read for example.
		/// </summary>
		public void Save(ConversationInvite conversationInvite) 
		{
			save(conversationInvite,true);
		}



		#region Private methods

		/// <summary>
		/// Allows you to save an existing conversation invitation for the purposes of marking it as Read for example.
		/// </summary>
		private void save(ConversationInvite conversationInvite, bool replicate) 
		{
			ConversationInvites invites = _Cache[ConversationInvites.GetCacheKey(conversationInvite)] as ConversationInvites;
			if (invites != null)
			{
				if (invites[conversationInvite.ConversationKey] != null) 
				{
					//only saves the invitation if it already exists.
					invites[conversationInvite.ConversationKey] = conversationInvite;
				}
			}
			else
			{	
				//If invites collection does not already exist, we decided not to create one, although 
				//it may be necessary to change that logic if we need to :)
			
			}
			if (replicate)
			{
				ReplicationRequested(invites);
			}
		}
		
		public bool CacheExternalTransactionID(ExternalTransactionID externalTransactionID)
		{
			return CacheExternalTransactionID(externalTransactionID, true);
		}

		/// <summary>
		/// Caches an ExternalTransactionID.
		/// </summary>
		/// <param name="externalTransactionID">The ExternalTransactionID to cache.</param>
		/// <returns>True if the ExternalTransactionID is already present in the cache.</returns>
		public bool CacheExternalTransactionID(ExternalTransactionID externalTransactionID, bool replicate)
		{
			if (externalTransactionID != null)
			{
				if (_Cache[externalTransactionID.GetCacheKey()] != null)
				{
					return true;
				}

				_Cache.Add(externalTransactionID);

				if (replicate)
				{
					ReplicationRequested(externalTransactionID);
				}
			}

			return false;
		}

		private void addInvitation(ConversationInvite conversationInvite)
		{
			addInvitation(conversationInvite, true);
		}

		// All conversation invitations are keyed by recipient MemberID
		private void addInvitation(ConversationInvite conversationInvite, bool replicate)
		{
			//check if this transacation was not already processed (Userplane sends duplicate transactions
			// if there is timeout). We keep transaction IDs supplied by Userplance cached for a short time.
			if (CacheExternalTransactionID(conversationInvite.ExternalTransactionID, replicate))
			{
				return;
			}

			ConversationInvites invites = _Cache[ConversationInvites.GetCacheKey(conversationInvite)] as ConversationInvites;
			if (invites != null)
			{
				invites.Add(conversationInvite);
			}
			else
			{	// Construct new conversation invites collection and insert in the cache.
				invites = new ConversationInvites(conversationInvite
					, Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_INVITES_CACHE_TTL")));

				_Cache.Add(invites, _expireInvitation);
				InvitationAdded();	// Performance counter event raised when we add an Invites collection to the cache.
			}

			if (replicate)
			{
				ReplicationRequested(invites);
			}
		}

		private void expireInvitationCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			InvitationRemoved();
			incrementExpirationCounter();
		}

		private void incrementExpirationCounter()
		{
			lock (_expiredCountLock)
			{
				_expiredCount++;

				if (_expiredCount > GC_THRESHOLD)
				{
					_expiredCount = 0;
					GC.Collect();
				}
			}
		}

	    // Ported from the web solution. todo: change to send and receive in json format.
	    /* XML format:
         * 
         * 		<?xml version='1.0' encoding='iso-8859-1'?>
         *		<undeliveredMessages>
         *			<message>This was the first typed message.</message>
         *			<message>This was the second typed message.</message>
         *		</undeliveredMessages>
         *
         */
	    private static string TransformUndeliveredMessages(string pXmlData)
	    {
	        //Refactored this method: removed unnecessary HtmlDecode (16119) and 
	        //improved handling of empty strings, close StringReader and stopped using catch block
	        //as part of handling logic! (no longer count on ReadElementString to fail!)
	        var sr = new StringReader(pXmlData);
	        var reader = new XmlTextReader(sr)
	                         {
	                             WhitespaceHandling = WhitespaceHandling.None
	                         };
	        var builder = new StringBuilder();
	        try
	        {
	            while (reader.Read())
	            {
	                //read until we hit the message element
	                if (reader.MoveToContent() == XmlNodeType.Element && reader.Name == "message")
	                {
	                    builder.Append(reader.ReadString() + " ");
	                }
	            }
	        }
	        catch (Exception exXml)
	        {
	            //catch badly formed XML errors, such as <> tags inside 
	            throw new ApplicationException("Error parsing userplane xml: " + pXmlData, exXml);
	            //write output so error will show up in UP app inspector as invalid XML response.
	            //DO NOT write out error! Or else Userplane will keep trying ad infinitum... 17191
	            //Response.Write("<![CDATA[Error parsing XML: " + ex.ToString() + "]]>");
	        }
	        finally
	        {
	            reader.Close();
	            sr.Close();
	        }

	        //mike roberts 7/12/05: get rid of &apos; is a hack to see if we can eliminate the &apos; that keep getting through. See bug 16686 and its predecessor 14273�
	        //apparently, &apos; is a XHTML specification not supported by HTML 4.0. Google on &apos; for more info.
	        var originalMessages = builder.ToString().Trim().Replace("&apos;", "'");

	        //16119 remove all html tags that might be coming from Userplane 
	        //(they probably come HtmlEncoded already, or XML would be invalid)
	        //First decode back into HTML (e.g., convert &lt; into <) - 
	        var cleanedMessages = HttpUtility.HtmlDecode(originalMessages);
	        //see Compose.ascx.cs - uses same pattern - HACK alert - ideally this would be common method
	        cleanedMessages = Regex.Replace(cleanedMessages, "<[^>]*>", "");
	        //HtmlEncode it back, just in case
	        cleanedMessages = HttpUtility.HtmlEncode(cleanedMessages);
	        //check if result is the same or if HTML tags were found:
	        if (Regex.IsMatch(HttpUtility.HtmlDecode(originalMessages), "<[^>]*>"))
	        {
	            //throw an exception so that we can know if Userplane is sending us HTML when it shouldn't
	            throw new ApplicationException("Found HTML tags in IM messages from Userplane NotifyConnectionClosed: " +
	                                           originalMessages);
	            //Don't output any unexpeced XML to Userplane! see 16961 and 17191
	            //Response.Write("<![CDATA[HTML tags found in message: " + originalMessages + "]]>");
	        }

	        //return missed messages, cleaned of HTML
	        return cleanedMessages;
	    }

        private static string TransformMissedIMMessages(List<string> messages)
        {
            StringBuilder messageSB = new StringBuilder();

            foreach (string m in messages)
            {
                //verify messages are encoded
                if (m.IndexOf("<") > 0)
                {
                    messageSB.Append(HttpUtility.HtmlEncode(m));
                }
                else
                {
                    messageSB.Append(m);
                }
                messageSB.Append(" ");
            }

            return messageSB.ToString();
        }

	    /// <summary>
	    ///   Returns the a MessageSave object that tells the sender s/he has gone over the quota of sending missed IMs in the correct language!
	    /// </summary>
	    /// <param name = "targetMember"></param>
	    /// <param name = "targetBrand"></param>
	    /// <param name = "senderBrand"></param>
	    /// <param name = "originalMessageSave"></param>
	    /// <returns></returns>
	    private static MessageSave GetQuotaErrorMessage(IMember targetMember, Brand targetBrand, Brand senderBrand,
	                                                    MessageSave originalMessageSave)
	    {
	        var expansionTokens = new StringDictionary
	                                  {
	                                      {"USERNAME", targetMember.GetUserName(targetBrand)},
	                                      {"ORIGINALMESSAGE", originalMessageSave.MessageBody}
	                                  };

	        return new MessageSave(originalMessageSave.FromMemberID,
	                               originalMessageSave.FromMemberID,
	                               originalMessageSave.GroupID,
	                               senderBrand.Site.SiteID,
	                               originalMessageSave.MailType,
                                   ResourceProvider.Instance.GetResourceValue(targetBrand.Site.SiteID, targetBrand.Site.CultureInfo,
	                                                                          ResourceGroupEnum.InstantMessenger,
	                                                                          "AT_QUOTA_ERROR_SUBJECT"),
                                   ResourceProvider.Instance.GetResourceValue(targetBrand.Site.SiteID, targetBrand.Site.CultureInfo,
	                                                                          ResourceGroupEnum.InstantMessenger,
	                                                                          "AT_QUOTA_ERROR_MESSAGE", expansionTokens));
	    }

		#endregion

		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "ConversationInvite":
					addInvitation(replicableObject as ConversationInvite, false);
					break;

				case "ConversationInvites":
					ConversationInvites invites = replicableObject as ConversationInvites;
					// Update the entire Invites collection (even if it's empty).
					_Cache.Remove(invites.GetCacheKey());
					_Cache.Add(invites);
					break;

				case "ExternalTransactionID":
					CacheExternalTransactionID(replicableObject as ExternalTransactionID);
					break;

				default:
					throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
			}
		}

	}
}
