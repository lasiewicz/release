using System;
using System.Diagnostics;

using Matchnet.InstantMessenger.BusinessLogic;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.InstantMessenger.ServiceDefinitions;

namespace Matchnet.InstantMessenger.ServiceManagers
{
	/// <summary>
	/// Summary description for InstantMessengerSettingsSM.
	/// </summary>
	public class InstantMessengerSettingsSM : MarshalByRefObject, IServiceManager, IInstantMessengerSettingsService, IDisposable 
	{
		#region Constants

		#endregion
		
		#region Class members

		private PerformanceCounter _perfCount = null;
		private PerformanceCounter _perfSaveCount = null;
		private PerformanceCounter _perfRequestsSec = null;
		private PerformanceCounter _perfHitRate = null;
		private PerformanceCounter _perfHitRate_Base = null;

		#endregion
		
		public InstantMessengerSettingsSM()
		{
			// Bind InstantMessengerSettingsBL events to their handlers.
			InstantMessengerSettingsBL.Instance.IMSettingsAdded += 
				new InstantMessengerSettingsBL.IMSettingsAddEventHandler(InstantMessengerSettingsBL_IMSettingsAdded);
			InstantMessengerSettingsBL.Instance.IMSettingsRemoved += 
				new InstantMessengerSettingsBL.IMSettingsRemoveEventHandler(InstantMessengerSettingsBL_IMSettingsRemoved);
			InstantMessengerSettingsBL.Instance.IMSettingsRequested +=
				new InstantMessengerSettingsBL.IMSettingsRequestedEventHandler(InstantMessengerSettingsBL_IMSettingsRequested);
			InstantMessengerSettingsBL.Instance.IMSettingsSaved +=
				new InstantMessengerSettingsBL.IMSettingsSavedEventHandler(InstantMessengerSettingsBL_IMSettingsSaved);

			initPerformanceCounters();		// Initialize performance coutners

		}

		#region IInstantMessengerSettingsService implementation

		public IMSettings GetSettings(int pMemberID, int pCommunityID)
		{
			return InstantMessengerSettingsBL.Instance.GetSettings(pMemberID, pCommunityID);
		}

		public void SaveSettings(IMSettings pIMSettings)
		{
			InstantMessengerSettingsBL.Instance.SaveSettings(pIMSettings);
		}

		#endregion

		#region IDisposable implementation
		/// <summary>
		/// Dispose implementation.
		/// </summary>
		public void Dispose()
		{
			resetPerfCounters();
		}

		#endregion

		#region IServiceManager implementation

		/// <summary>
		/// Prepopulate the Matchnet.InstantMessenger cache items (not implemented at this time).
		/// </summary>
		public void PrePopulateCache()
		{
		}

		#endregion

		#region Performance Counter / Instrumentation implementation

		private void initPerformanceCounters()
		{
			_perfCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "IMSettings Items", false);
			_perfSaveCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "IMSettings Saved", false);
			_perfRequestsSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "IMSettings Requests/second", false);
			_perfHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "IMSettings Hit Rate", false);
			_perfHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "IMSettings Hit Rate_Base", false);

			resetPerfCounters();
		}

		private void resetPerfCounters()
		{
			_perfCount.RawValue = 0;
			_perfSaveCount.RawValue = 0;
			_perfRequestsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
		}

		private void InstantMessengerSettingsBL_IMSettingsRequested(bool cacheHit)
		{
			_perfRequestsSec.Increment();
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
		}

		private void InstantMessengerSettingsBL_IMSettingsAdded()
		{
			_perfCount.Increment();
		}

		private void InstantMessengerSettingsBL_IMSettingsRemoved()
		{
			_perfCount.Decrement();
		}

		private void InstantMessengerSettingsBL_IMSettingsSaved()
		{
			_perfSaveCount.Increment();
		}

		#endregion

		/// <summary>
		/// Not implemented, returns null.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

	}
}
