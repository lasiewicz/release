using System;
using System.Collections.Generic;
using System.Diagnostics;

using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.InstantMessenger.BusinessLogic;
using Matchnet.InstantMessenger.ServiceDefinitions;
using Matchnet.InstantMessenger.ValueObjects;
using Matchnet.Replication;

namespace Matchnet.InstantMessenger.ServiceManagers
{
	/// <summary>
	/// Summary description for InstantMessengerSM.
	/// </summary>
	public class InstantMessengerSM : MarshalByRefObject, IServiceManager, IInstantMessengerService, IDisposable, IReplicationRecipient
	{
		private PerformanceCounter _perfInvites = null;
		private Replicator _replicator = null;

		public InstantMessengerSM()
		{
			// Initialize performance counters
			InstantMessengerBL.Instance.InvitationAdded +=
				new InstantMessengerBL.InvitationAddEventHandler(InstantMessengerBL_InvitationAdded);
			InstantMessengerBL.Instance.InvitationRemoved +=
				new InstantMessengerBL.InvitationRemoveEventHandler(InstantMessengerBL_InvitationRemoved);

			//replication
			InstantMessengerBL.Instance.ReplicationRequested += new Matchnet.InstantMessenger.BusinessLogic.InstantMessengerBL.ReplicationEventHandler(InstantMessengerBL_ReplicationRequested);

			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}
			
			string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("INSTANTMESSENGERSVC_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, "InstantMessengerSM", machineName);
			}
			if (replicationURI != null && replicationURI != string.Empty)
			{
				_replicator = new Replicator(ServiceConstants.SERVICE_NAME);
				_replicator.SetDestinationUri(replicationURI);
				_replicator.Start();
			}
			else
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME
					, "Unable to retrieve a replication partner uri on startup."
					, EventLogEntryType.Error);
			}

			initPerformanceCounters();
		}

		#region IInstantMessengerService implementation

		public void AcceptInvitation(ConversationInvite conversationInvite)
		{
			try 
			{
				InstantMessengerBL.Instance.AcceptInvitation(conversationInvite);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure accepting IM Invitation.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure accepting IM Invitation.", ex);
			}
		}

		public ConversationInvites GetInvites(int recipientMemberID, int communityID)
		{
			try 
			{
				return InstantMessengerBL.Instance.GetInvites(recipientMemberID, communityID);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting IM Invitation(s).", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting IM Invitation(s)", ex);
			}
		}

		public void SendInvitation(ConversationInvite conversationInvite)
		{
			try 
			{
				InstantMessengerBL.Instance.SendInvitation((ConversationInvite)conversationInvite);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending IM Invitation.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending IM Invitation.", ex);
			}
		}

		public bool CacheExternalTransactionID(ExternalTransactionID externalTransactionID)
		{
			try 
			{
				return InstantMessengerBL.Instance.CacheExternalTransactionID(externalTransactionID);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure caching ExternalTransactionID.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure caching ExternalTransactionID.", ex);
			}
		}

		public void SendMissedIM(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID)
		{
			try 
			{
				InstantMessengerBL.Instance.SendMissedIM(messageSave, saveInSent, brand, transactionID);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed IM.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed IM.", ex);
			}
		}

	    public void SendMissedIM(int memberId, int targetMemberId, string messagesXml, Brand brand)
	    {
            try
            {
                InstantMessengerBL.Instance.SendMissedIM(memberId, targetMemberId, messagesXml, brand);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed IM.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed IM.", ex);
            }
	    }

        public void SendMissedIM(int senderMemberId, int targetMemberId, List<string> messages, Brand senderBrand)
        {
            try
            {
                InstantMessengerBL.Instance.SendMissedIM(senderMemberId, targetMemberId, messages, senderBrand);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed IM.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed IM.", ex);
            }
        }

	    /// <summary>
		/// Allows you to save an existing conversation invitation for the purposes of marking it as Read for example.
		/// </summary>
		public void Save(ConversationInvite conversationInvite) 
		{
			try 
			{
				InstantMessengerBL.Instance.Save(conversationInvite);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Conversation Invitation.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Conversation Invitation.", ex);
			}
		}

		#endregion

		#region Performance Counters / Instrumentation

		/// <summary>
		/// Install the performance counters for this service manager.
		/// </summary>
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
				new CounterCreationData("Invitations", "Invitations", PerformanceCounterType.NumberOfItems64),
				new CounterCreationData("IM Conversations", "IM Conversations", PerformanceCounterType.NumberOfItems64),
				new CounterCreationData("IM Messages/second", "IM Messages/second", PerformanceCounterType.RateOfCountsPerSecond32),
				});
			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
		}

		/// <summary>
		/// Uninstall the performance counters for this service manager.
		/// </summary>
		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}

		private void initPerformanceCounters()
		{
			_perfInvites = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Invitations", false);

			resetPerfCounters();
		}

		private void resetPerfCounters()
		{
			_perfInvites.RawValue = 0;
		}

		private void InstantMessengerBL_InvitationAdded()
		{
			lock (_perfInvites)
			{
				_perfInvites.Increment();
			}
		}

		private void InstantMessengerBL_InvitationRemoved()
		{
			lock (_perfInvites)
			{
				_perfInvites.Decrement();
			}
		}

		#endregion

		/// <summary>
		/// Prepopulate the Matchnet.Search cache items (not implemented at this time).
		/// </summary>
		public void PrePopulateCache()
		{
			// No implementation at this time.
		}

		/// <summary>
		/// Not implemented, returns null.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

		void IReplicationRecipient.Receive(IReplicable replicable)
		{
			try
			{
				InstantMessengerBL.Instance.CacheReplicatedObject(replicable);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure processing replication object.", ex);
			}
		}


		private void InstantMessengerBL_ReplicationRequested(IReplicable replicableObject)
		{
			if (replicableObject == null)
			{
				Trace.WriteLine("IM Service Manager handling a ReplicationRequested event with a null argument.",  "InstantMessengerSM");
			}
			else
			{
				if (_replicator != null)
				{
					try
					{
						_replicator.Enqueue(replicableObject);
					}
					catch (Exception ex)
					{
						Trace.WriteLine("Exception caught performing _replicator.Enqueue(...) : " + ex.Message, "InstantMessenger");
					}
				}
			}
		}

		#region IDisposable Members

		public void Dispose()
		{
			resetPerfCounters();
		}

		#endregion
	}
}
