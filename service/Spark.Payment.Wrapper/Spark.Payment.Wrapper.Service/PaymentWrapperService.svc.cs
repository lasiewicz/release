﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Spark.Payment.Wrapper.Engine.ServiceManagers;
using Spark.UnifiedPurchaseSystem.Lib.CustomContext;

namespace Spark.Payment.Wrapper.Service
{
    // NOTE: If you change the class name "PaymentWrapperService" here, you must also update the reference to "PaymentWrapperService" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ServiceRequestContextAttribute]
    public class PaymentWrapperService : IPaymentWrapperService
    {
        public string BillPaymentProfile(string paymentProfileID,
                            decimal amount,
                            string currency,
                            int orderID,
                            int systemID,
                            int applicationTypeID,
                            string overrideExpMonth,
                            string overrideExpYear,
                            int transactionTypeID,
                            string[] requiredValidations)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.BillPaymentProfile(paymentProfileID, amount, currency, orderID, systemID, applicationTypeID, overrideExpMonth, overrideExpYear, transactionTypeID, requiredValidations);
        }

        public string CreditTransaction(int orderID,
                            decimal amountToCredit,
                            int systemID,
                            int originalPaymentID,
                            int applicationTypeID,
                            int transactionTypeID,
                            string[] requiredValidations)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.CreditTransaction(orderID, amountToCredit, systemID, originalPaymentID, applicationTypeID, transactionTypeID, requiredValidations);
        }

        public string GetObfuscatedPaymentProfile(string paymentProfileID,
                            string systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.GetObfuscatedPaymentProfile(paymentProfileID,
                            systemID);
        }

        public string ArchivePaymentProfile(string paymentProfileID,
                            string systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.ArchivePaymentProfile(paymentProfileID,
                            systemID);
        }

        public string IsPaymentProfileValid(string paymentProfileID,
                            string systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.IsPaymentProfileValid(paymentProfileID,
                            systemID);
        }

        public string IsCreditSetToVoid(int paymentID, int systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.IsCreditSetToVoid(paymentID,
                            systemID);
        }
    }
}
