﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Spark.Payment.Wrapper.Service
{
    // NOTE: If you change the interface name "IPaymentWrapperService" here, you must also update the reference to "IPaymentWrapperService" in App.config.
    [ServiceContract(Namespace = "http://spark")]
    public interface IPaymentWrapperService
    {
        [OperationContract()]
        string BillPaymentProfile(string paymentProfileID, decimal amount, string currency, int orderID, int systemID, int applicationTypeID, string overrideExpMonth, string overrideExpYear, int transactionTypeID, string[] requiredValidations);

        [OperationContract()]
        string CreditTransaction(int orderID, decimal amountToCredit, int systemID, int originalPaymentID, int applicationTypeID, int transactionTypeID, string[] requiredValidations);

        [OperationContract()]
        string GetObfuscatedPaymentProfile(string paymentProfileID, string systemID);

        [OperationContract()]
        string ArchivePaymentProfile(string paymentProfileID, string systemID);

        [OperationContract()]
        string IsPaymentProfileValid(string paymentProfileID, string systemID);

        [OperationContract()]
        string IsCreditSetToVoid(int paymentID, int systemID);
    }
}
