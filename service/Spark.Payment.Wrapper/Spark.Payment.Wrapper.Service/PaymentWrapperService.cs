﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

//using Spark.Payment.Wrapper.Service.Contracts;
using Spark.Payment.Wrapper.Engine.ServiceManagers;

namespace Spark.Payment.Wrapper.Service
{
    // NOTE: If you change the class name "PaymentWrapperService" here, you must also update the reference to "PaymentWrapperService" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PaymentWrapperService : IPaymentWrapperService
    {
        public string BillPaymentProfile(string paymentProfileID,
                            decimal amount,
                            string currency,
                            int orderID,
                            int systemID,
                            int applicationTypeID,
                            string overrideExpMonth,
                            string overrideExpYear)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.BillPaymentProfile(paymentProfileID, amount, currency, orderID, systemID, applicationTypeID, overrideExpMonth, overrideExpYear);
        }

        public string CreditTransaction(int orderID,
                            decimal amountToCredit,
                            int systemID,
                            int originalOrderID,
                            int applicatioTypeID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.CreditTransaction(orderID, amountToCredit, systemID, originalOrderID, applicatioTypeID);
        }

        public string GetObfuscatedPaymentProfile(string paymentProfileID,
                            string systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.GetObfuscatedPaymentProfile(paymentProfileID,
                            systemID);
        }

        public string ArchivePaymentProfile(string paymentProfileID,
                            string systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.ArchivePaymentProfile(paymentProfileID,
                            systemID);
        }

        public string IsPaymentProfileValid(string paymentProfileID,
                            string systemID)
        {
            PaymentWrapperSM paymentWrapperSM = new PaymentWrapperSM();
            return paymentWrapperSM.IsPaymentProfileValid(paymentProfileID,
                            systemID);
        }
    }
}
