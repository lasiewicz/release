﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Spark.Payment.Wrapper.Engine.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;

namespace Spark.Payment.Wrapper.Engine.DataAccess
{
    internal class PaymentWrapperDAL
    {
        public void InsertPaymentToOrder(int callingSystemID, int callingSystemTypeID, int orderID, int paymentID, PaymentTransactionType paymentTransactionType, PaymentChargeStatus paymentChargeStatus)
        {
            try
            {
                TraceLog.Enter("PaymentWrapperDAL.InsertPaymentToOrder");
                TraceLog.WriteLine("Begin PaymentWrapperDAL.InsertPaymentToOrder({0}, {1}, {2}, {3}, {4}, {5})", callingSystemID, callingSystemTypeID, orderID, paymentID, paymentTransactionType, paymentChargeStatus);

                Command command = new Command("epOrder", "dbo.up_MapOrderIDPaymentID_Insert", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, paymentID);
                command.AddParameter("@CallingSystemID", SqlDbType.Int, ParameterDirection.Input, callingSystemID);
                command.AddParameter("@CallingSystemTypeID", SqlDbType.Int, ParameterDirection.Input, callingSystemTypeID);
                command.AddParameter("@ChargeTypeID", SqlDbType.Int, ParameterDirection.Input, (int)paymentTransactionType);
                command.AddParameter("@ChargeStatusID", SqlDbType.Int, ParameterDirection.Input, (int)paymentChargeStatus);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                sw.Execute(command, null, out exception);
                //Int32 returnValue = sw.Execute(command, out swEx);
                sw.Dispose();

                if (exception != null)
                {
                    TraceLog.WriteErrorLine(exception, "Error in PaymentWrapperDAL.InsertPaymentToOrder(), Error: " + exception.Message);
                    throw new DALException("PaymentWrapperDAL.InsertPaymentToOrder() error.", exception);
                }

                TraceLog.WriteLine("Order ID {0} and Charge ID {1} successfully inserted with a charge type of {2} and a charge status of {3} into the database", orderID, paymentID, paymentTransactionType, paymentChargeStatus);
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperDAL.InsertPaymentToOrder(), Error: " + ex.Message);
                throw new DALException("PaymentWrapperDAL.InsertPaymentToOrder() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperDAL.InsertPaymentToOrder()");
                TraceLog.Leave("PaymentWrapperDAL.InsertPaymentToOrder");
            }
        }

        public void UpdatePaymentToOrder(int orderID, int paymentID, PaymentTransactionType paymentTransactionType, PaymentChargeStatus paymentChargeStatus)
        {
            try
            {
                TraceLog.Enter("PaymentWrapperDAL.UpdatePaymentToOrder");
                TraceLog.WriteLine("Begin PaymentWrapperDAL.UpdatePaymentToOrder({0}, {1}, {2}, {3})", orderID, paymentID, paymentTransactionType, paymentChargeStatus);

                Command command = new Command("epOrder", "dbo.up_MapOrderIDPaymentID_Update", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, paymentID);
                command.AddParameter("@ChargeTypeID", SqlDbType.Int, ParameterDirection.Input, (int)paymentTransactionType);
                command.AddParameter("@ChargeStatusID", SqlDbType.Int, ParameterDirection.Input, (int)paymentChargeStatus);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                sw.Execute(command, null, out exception);
                //Int32 returnValue = sw.Execute(command, out swEx);
                sw.Dispose();

                if (exception != null)
                {
                    TraceLog.WriteErrorLine(exception, "Error in PaymentWrapperDAL.UpdatePaymentToOrder(), Error: " + exception.Message);
                    throw new DALException("PaymentWrapperDAL.UpdatePaymentToOrder() error.", exception);
                }

                TraceLog.WriteLine("Order ID {0} and Charge ID {1} successfully updated with a charge type of {2} and a charge status of {3} into the database", orderID, paymentID, paymentTransactionType, paymentChargeStatus);
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperDAL.UpdatePaymentToOrder(), Error: " + ex.Message);
                throw new DALException("PaymentWrapperDAL.UpdatePaymentToOrder() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperDAL.UpdatePaymentToOrder()");
                TraceLog.Leave("PaymentWrapperDAL.UpdatePaymentToOrder");
            }
        }

        public int LoadOriginalPaymentIDByOrder(int orderID)
        {
            try
            {
                Command command = new Command("epOrder", "dbo.up_paymentID_SelectByOrder", 0);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    throw new ApplicationException("Failed calling up_paymentID_SelectByOrder", exception);
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed calling up_paymentID_SelectByOrder", ex);
            }
        }
    }
}
