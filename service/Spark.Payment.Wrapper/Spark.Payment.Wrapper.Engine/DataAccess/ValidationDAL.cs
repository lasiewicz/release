﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Matchnet.Data;
using Matchnet.Data.Hydra;

namespace Spark.Payment.Wrapper.Engine.DataAccess
{
    class ValidationDAL
    {
        public bool IsDuplicateTransaction(int orderID, int systemID)
        {
            try
            {
                Command command = new Command("epOrder", "dbo.up_IsDuplicateTransaction", 0);
                command.AddParameter("@SystemID", SqlDbType.Int, ParameterDirection.Input, systemID);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);

                Exception exception;
                SyncWriter sw = new SyncWriter();
                Int32 returnValue = sw.Execute(command, out exception);
                sw.Dispose();

                if (exception != null)
                {
                    throw new ApplicationException("Failed calling IsDuplicateTransaction", exception);
                    //throw new ApplicationException("Failed calling IsDuplicateTransaction, " + exception.Message, exception);
                }

                if (returnValue == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed calling IsDuplicateTransaction", ex);
                //throw new ApplicationException("Failed calling IsDuplicateTransaction, " + ex.Message, ex);
            }
        }
    }
}
