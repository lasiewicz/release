﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects
{
    /*
    #region Payment Transaction Types

    /// <summary>
    /// Order transaction types
    /// </summary>
    public enum TransactionType
    {
        InitialBuy = 1,
        AuthorizationOnly = 6,
        PaymentProfileAuthorization = 10,
        PlanChange = 11,
        TrialPayAdjustment = 12,
        TrialPayRenewal = 13,
        ReopenAutoRenewal = 18,
        TrialTaken = 1001,
        ChargeBack = 1004,
        CheckReversal = 1005,
        TrialTermination = 1006,
        TimeAdjustment = 1007,
        CreditSales = 1008,
        CreditRenewal = 1009,
        VoidSubscription = 1010,
        VoidRenewal = 1011,
        GiftPurchase = 1013,
        GiftRedeem = 1014,
        RenewalAttempt1 = 19,
        RenewalAttempt2 = 20,
        RenewalAttempt3 = 21,
        RenewalAttempt4 = 22,
        Upgrade = 23,
        Downgrade = 24,
        Swap = 25,
        Extension = 26,
        VirtualTerminalPurchase = 34,
        VirtualTerminalCredit = 35
    }

    #endregion
    */    
 
    #region Payment Transaction Types

    /// <summary>
    /// The transaction type of the payment operation  
    /// </summary>
    public enum PaymentTransactionType : int
    {
        None = 0,
        Bill = 1,
        Authorization = 2,
        Credit = 3,
        Void = 4,
        UndeterminedCreditOrVoid = 5
    }

    #endregion

    #region Payment Charge Status Types

    /// <summary>
    /// The status of the payment operation  
    /// </summary>
    public enum PaymentChargeStatus : int
    {
        None = 0,
        Pending = 1,
        Success = 2,
        Failure = 3
    }

    #endregion
}
