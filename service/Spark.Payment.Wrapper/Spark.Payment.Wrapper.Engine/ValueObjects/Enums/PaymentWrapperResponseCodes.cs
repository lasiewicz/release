﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Enums
{
    public enum PaymentWrapperResponse
    {
        DuplicateTransaction = 5,
        UnknownException = -1000
    }
}
