﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects
{
    /*
    /// <summary>
    /// Defines system wide constants
    /// Constants cannot be serialized from the Payment service 
    /// </summary>                
    public sealed class Constants
    {
        private Constants()
        {

        }

        /// <summary>
        /// Value used to represent a NULL float
        /// </summary>
        public const float NULL_FLOAT = float.MinValue + (float)1;

        /// <summary>
        /// Value used to represent a NULL decimal
        /// </summary>
        public const decimal NULL_DECIMAL = decimal.MinValue + (decimal)1;

        /// <summary>
        /// Value used to represent a NULL double
        /// </summary>
        public const double NULL_DOUBLE = double.MinValue + (double)1;

        /// <summary>
        /// Value used to represent a NULL int
        /// </summary>
        public const int NULL_INT = int.MinValue + 1;

        /// <summary>
        /// Value used to represent a NULL long
        /// </summary>
        public const long NULL_LONG = long.MinValue + (long)1;

        /// <summary>
        /// Value used to represent a NULL string
        /// </summary>
        public const string NULL_STRING = null;

        /// <summary>
        /// Service name
        /// </summary>
        public const string SERVICE_NAME = "Spark.PaymentWrapper.Service";

        public const string SUCCESS = "SUCCESS";
        public const string FAILED = "FAILED";

        public const string INTERNALRESPONSECODE_NOTFOUND = "-1";
        public const string INTERNALRESPONSEMESSAGE_NOTFOUND = "Unknown";
    }
    */
}
