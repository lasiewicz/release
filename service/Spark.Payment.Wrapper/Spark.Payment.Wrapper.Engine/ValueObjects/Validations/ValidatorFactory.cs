﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Validations
{
    class ValidatorFactory
    {
        public List<IValidator> GetValidationEngines(ValidationType[] engineTypes)
        {
            List<IValidator> engines = new List<IValidator>();
            foreach (var e in engineTypes)
            {
                switch(e)
                {
                    case ValidationType.dupTransaction:
                        engines.Add(new DuplicateTransactionValidator());
                        break;
                    default:
                        break;
                }
            }

            return engines;
        }
    }
}
