﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Validations
{
    public enum ValidationType
    {
        /// <summary>
        /// 
        /// </summary>
        whitelist = 1,
        /// <summary>
        /// 
        /// </summary>
        format = 2,
        /// <summary>
        /// 
        /// </summary>
        dupTransaction = 3,
        /// <summary>
        /// 
        /// </summary>
        blacklist = 4,
        /// <summary>
        /// 
        /// </summary>
        binBlocking = 5,
        /// <summary>
        /// 
        /// </summary>
        velocity = 6,
        /// <summary>
        /// Charge cannot exceed the charge limit for that System ID
        /// </summary>
        maxSystemAmount = 7
    }
}
