﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Payment.Wrapper.Engine.ValueObjects.Operations;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Validations
{
    interface IValidator
    {
        ValidationResponse ValidateOperation(OperationDetails operationDetails);
    }
}
