﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Payment.Wrapper.Engine.ValueObjects;
using Spark.Payment.Wrapper.Engine.ValueObjects.Operations;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Validations
{
    public class OperationValidations
    {
        public static string[] GetRequiredValidationsForSending(TransactionType transactionType)
        {
            ValidationType[] arrValidationTypes = GetValidations(transactionType);
            List<string> validationsToSend = new List<string>();

            // Required to maintain the order of the validations 
            foreach (ValidationType vt in arrValidationTypes)
            {
                validationsToSend.Add(Enum.GetName(typeof(ValidationType), vt));
            }

            return validationsToSend.ToArray();
        }

        /// <summary>
        /// This call returns the validations mapped for each of the transaction types supported by the engine
        /// </summary>
        /// <param name="transactionType">an enum that defines the trantype</param>
        /// <returns>array of validationtypes to run for the transaction</returns>
        public static ValidationType[] GetValidations(TransactionType transactionType)
        {
            List<ValidationType> validations = new List<ValidationType>();

            switch (transactionType)
            {
                case TransactionType.InitialSubscriptionPurchase:
                    validations.Add(ValidationType.format);
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    validations.Add(ValidationType.binBlocking);
                    validations.Add(ValidationType.velocity);
                    //validations.Add(ValidationType.dupTransaction);
                    validations.Add(ValidationType.maxSystemAmount);
                    break;
                case TransactionType.Authorization:
                    validations.Add(ValidationType.format);
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    break;
                case TransactionType.VirtualTerminalPurchase:
                    validations.Add(ValidationType.format);
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    //validations.Add(ValidationType.dupTransaction);
                    validations.Add(ValidationType.maxSystemAmount);
                    break;
                case TransactionType.PaymentProfileAuthorization:
                    validations.Add(ValidationType.format);
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    validations.Add(ValidationType.binBlocking);
                    validations.Add(ValidationType.velocity);
                    //validations.Add(ValidationType.dupTransaction);
                    break;
                case TransactionType.TrialTakenInitialSubscription:
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    validations.Add(ValidationType.binBlocking);
                    validations.Add(ValidationType.velocity);
                    //validations.Add(ValidationType.dupTransaction);
                    break;
                case TransactionType.Credit:
                case TransactionType.Void:
                case TransactionType.VirtualTerminalCredit:
                case TransactionType.VirtualTerminalVoid:
                    //validations.Add(ValidationType.dupTransaction);
                    break;
                case TransactionType.GiftPurchase:
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    validations.Add(ValidationType.binBlocking);
                    validations.Add(ValidationType.velocity);
                    //validations.Add(ValidationType.dupTransaction);
                    break;
                case TransactionType.Renewal:
                case TransactionType.AdditionalNonSubscriptionPurchase:
                case TransactionType.AdditionalSubscriptionPurchase:
                    validations.Add(ValidationType.whitelist);
                    validations.Add(ValidationType.blacklist);
                    //validations.Add(ValidationType.dupTransaction);
                    break;
                default:
                    throw new ApplicationException("The system does not support this type of transaction");
            }

            return validations.ToArray<ValidationType>();
        }
    }
}
