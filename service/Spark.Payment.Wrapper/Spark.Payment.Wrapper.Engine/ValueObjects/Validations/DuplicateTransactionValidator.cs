﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Payment.Wrapper.Engine.ValueObjects.Operations;
using Spark.Payment.Wrapper.Engine.DataAccess;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Validations
{
    class DuplicateTransactionValidator : IValidator
    {
        #region IValidator Members

        public ValidationResponse ValidateOperation(OperationDetails operationDetails)
        {
            if (IsDuplicateTransaction(int.Parse(operationDetails.OrderID), int.Parse(operationDetails.SystemID)))
            {
                return new ValidationResponse { IsValid = false, Message = "This is a duplicate transaction" };
            }
            else
                return new ValidationResponse { IsValid = true, Message = "Ok" };
        }


        private bool IsDuplicateTransaction(int OrderID, int SystemID)
        {
            ValidationDAL dataAccess = new ValidationDAL();
            return dataAccess.IsDuplicateTransaction(OrderID, SystemID);
        }

        #endregion
    }
}
