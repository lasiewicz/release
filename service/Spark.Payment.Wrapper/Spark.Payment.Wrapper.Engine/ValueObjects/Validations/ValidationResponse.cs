﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Validations
{
    public class ValidationResponse
    {
        public bool IsValid { get; set; }
        public string Message { get; set; }
    }
}
