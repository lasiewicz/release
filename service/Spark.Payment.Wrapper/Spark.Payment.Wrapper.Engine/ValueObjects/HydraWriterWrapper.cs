﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;
using Matchnet.Data;
using Matchnet.Data.Hydra;

namespace Spark.Payment.Wrapper.Engine.ValueObjects
{
    public class HydraWriterWrapper
    {
        public static readonly HydraWriterWrapper Instance = new HydraWriterWrapper();
        private static HydraWriter hydraWriter;

        private HydraWriterWrapper()
        {
            hydraWriter = new HydraWriter(new string[] { "epOrder" });
            //hydraWriter = new HydraWriter(new string[] { "epOrder", "epValidation" });
        }

        public void HydraWriterStart()
        {
            hydraWriter.Start();
        }

        public void HydraWriterStop()
        {
            hydraWriter.Stop();
        }
    }
}
