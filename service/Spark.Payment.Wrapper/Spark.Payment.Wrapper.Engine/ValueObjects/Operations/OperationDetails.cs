﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Operations
{
    struct OperationDetails
    {
        public OperationType operationType { get; set; }

        public string SystemID { get; set; }

        public string OrderID { get; set; }
        
    }
}
