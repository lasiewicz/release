﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Operations
{
    enum OperationType
    {
        BillPaymentProfile,
        GetObfuscatedPaymentProfile,
        ArchivePaymentProfile,
        CreditTransaction
    }
}
