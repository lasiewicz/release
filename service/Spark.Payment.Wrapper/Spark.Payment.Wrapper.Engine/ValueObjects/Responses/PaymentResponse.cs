﻿using System;
using System.Xml.Serialization;

namespace Spark.Payment.Wrapper.Engine.ValueObjects.Responses
{
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false, Namespace="http://spark.net/schema/online")]
    public abstract class PaymentResponse
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string version;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string response;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string message;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public abstract class SuccededPaymentResponse : PaymentResponse
    {
        [System.Xml.Serialization.XmlElementAttribute("InternalResponse")]
        public InternalResponse[] internalResponse;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class BillPaymentProfilePaymentResponse : SuccededPaymentResponse
    {
        public string paymentID;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class CreatePaymentProfilePaymentResponse : SuccededPaymentResponse
    {
        public string paymentProfileID;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class CreditTransactionPaymentResponse : SuccededPaymentResponse
    {
        public string paymentID;

        public string paymentEngineOperation;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class ArchiveProfilePaymentResponse : SuccededPaymentResponse
    {
        public string isArchived;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class IsProfileValidPaymentResponse : SuccededPaymentResponse
    {
        public string isValid;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class GetPaymentProfilePaymentResponse : SuccededPaymentResponse
    {
        [System.Xml.Serialization.XmlElementAttribute("PaymentProfile")]
        public ObfuscatedCreditCardPaymentProfile[] obfuscatedCreditCardPaymentProfile;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class IsCreditSetToVoidPaymentResponse : SuccededPaymentResponse
    {
        public string isCreditSetToVoid;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute("PaymentResponse", IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class FailedPaymentResponse : PaymentResponse
    {
        [System.Xml.Serialization.XmlElementAttribute("InternalResponse")]
        public InternalResponse[] internalResponse;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    public class InternalResponse
    {
        public string internalResponseCode;

        public string internalResponseDescription;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    public class ObfuscatedCreditCardPaymentProfile
    {
        public string FirstName;

        public string LastName;
        
        public string StreetAddress;
        
        public string City;
        
        public string State;
        
        public string PostalCode;
        
        public string Country;
        
        public string CardType;
        
        public string CreditCardNumber;
        
        public string ExpMonth;
        
        public string ExpYear;

        public string PaymentType;
    }
}
