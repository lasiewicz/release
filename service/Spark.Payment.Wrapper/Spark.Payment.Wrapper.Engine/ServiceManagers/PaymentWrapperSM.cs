﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Spark.Payment.Wrapper.Engine.BusinessLogic;
using Spark.Payment.Wrapper.Engine.ValueObjects;
using Spark.Payment.Wrapper.Engine.ValueObjects.Responses;
using Spark.Payment.Wrapper.Engine.ValueObjects.Validations;
using Spark.Payment.Wrapper.Engine.ValueObjects.Enums;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.Payment.Wrapper.Engine.ServiceAdapters;

namespace Spark.Payment.Wrapper.Engine.ServiceManagers
{
    public class PaymentWrapperSM
    {
        PaymentSA _PaymentSA = new PaymentSA();

        public PaymentWrapperSM()
        {

        }

        public string BillPaymentProfile(string paymentProfileID, decimal amount, string currency, int orderID, int systemID, int applicationTypeID, string overrideExpMonth, string overrideExpYear, int transactionTypeID, string[] requiredValidations)
        {
            string currentOperation = "BillPaymentProfile";

            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = null;
            string xmlResponse = Constants.NULL_STRING;
            ValidationResponse validation = null;

            try
            {
                TraceLog.Enter("PaymentWrapperSM.BillPaymentProfile");
                TraceLog.WriteLine("Begin PaymentWrapperSM.BillPaymentProfile({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})",
                    paymentProfileID,
                    amount,
                    currency,
                    orderID,
                    systemID,
                    applicationTypeID,
                    overrideExpMonth,
                    overrideExpYear,
                    transactionTypeID,
                    requiredValidations);

                PaymentWrapperBL paymentWrapperBL = new PaymentWrapperBL(_PaymentSA);
                paymentWrapperBL.BillPaymentProfile(paymentProfileID, amount, currency, orderID, systemID, applicationTypeID, overrideExpMonth, overrideExpYear, transactionTypeID, requiredValidations, ref response, ref validation);

                if (validation.IsValid)
                {
                    if (response.InternalResponse.internalcode == "0")
                    {
                        XmlSerializationHelper<BillPaymentProfilePaymentResponse> helper = new XmlSerializationHelper<BillPaymentProfilePaymentResponse>();
                        PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                        xmlResponse = helper.SerializeObject(responseHelper.BuildBillPaymentProfileResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation, response.paymentid));
                    }
                    else
                    {
                        XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                        PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                         xmlResponse = helper.SerializeObject(responseHelper.BuildFailedPaymentResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation));
                    }
                }
                else
                {
                    XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    xmlResponse = helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.DuplicateTransaction, "Duplicate Transaction", currentOperation));
                }
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PaymentWrapperSM.BillPaymentProfile() error.", ex);
                TraceLog.WriteErrorLine(smException, "Error in PaymentWrapperSM.BillPaymentProfile(), Error: " + smException.Message);
                
                XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                xmlResponse = helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Unknown error", currentOperation));
                //return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Error: " + ex.Message.ToString(), currentOperation));
            }
            finally
            {
                _PaymentSA.CloseProxyInstance();

                TraceLog.WriteLine("End of PaymentWrapperSM.BillPaymentProfile(), XML response: {0}", xmlResponse);
                TraceLog.Leave("PaymentWrapperSM.BillPaymentProfile");
            }

            return xmlResponse;
        }

        public string CreditTransaction(int orderID, decimal amountToCredit, int systemID, int originalPaymentID, int applicationTypeID, int transactionTypeID, string[] requiredValidations)
        {
            string currentOperation = "CreditTransaction";

            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = null;
            string xmlResponse = Constants.NULL_STRING;
            ValidationResponse validation = null;

            try
            {
                TraceLog.Enter("PaymentWrapperSM.CreditTransaction");
                TraceLog.WriteLine("Begin PaymentWrapperSM.CreditTransaction({0}, {1}, {2}, {3}, {4}, {5}, {6})",
                    orderID,
                    amountToCredit,
                    systemID,
                    originalPaymentID,
                    applicationTypeID,
                    transactionTypeID,
                    requiredValidations);

                PaymentWrapperBL paymentWrapperBL = new PaymentWrapperBL(_PaymentSA);
                paymentWrapperBL.CreditTransaction(amountToCredit, systemID, orderID, originalPaymentID, applicationTypeID, transactionTypeID, requiredValidations, ref response, ref validation);

                if (response.InternalResponse.internalcode == "0")
                {
                    XmlSerializationHelper<CreditTransactionPaymentResponse> helper = new XmlSerializationHelper<CreditTransactionPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    xmlResponse = helper.SerializeObject(responseHelper.BuildCreditTransactionResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation, response.paymentid, response.transactiontype));
                }
                else
                {
                    XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    xmlResponse = helper.SerializeObject(responseHelper.BuildFailedPaymentResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation));
                }
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("PaymentWrapperSM.CreditTransaction() error.", ex);
                TraceLog.WriteErrorLine(smException, "Error in PaymentWrapperSM.CreditTransaction(), Error: " + smException.Message);

                XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                xmlResponse = helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Unknown error", currentOperation));
            }
            finally
            {
                _PaymentSA.CloseProxyInstance();

                TraceLog.WriteLine("End of PaymentWrapperSM.CreditTransaction(), XML response: {0}", xmlResponse);
                TraceLog.Leave("PaymentWrapperSM.CreditTransaction");
            }

            return xmlResponse;
        }

        public string GetObfuscatedPaymentProfile(string paymentProfileID, string systemID)
        {
            string currentOperation = "GetObfuscatedPaymentProfile";

            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = null;

            try
            {
                PaymentWrapperBL paymentWrapperBL = new PaymentWrapperBL(_PaymentSA);
                paymentWrapperBL.GetObfuscatedPaymentProfile(paymentProfileID, systemID, ref response);

                if (response.InternalResponse.internalcode == "0")
                {
                    XmlSerializationHelper<GetPaymentProfilePaymentResponse> helper = new XmlSerializationHelper<GetPaymentProfilePaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildGetProfileResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation, response.paymentprofile));
                }
                else
                {
                    XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation));
                }
            }
            catch (Exception ex)
            {
                _PaymentSA.CloseProxyInstance();

                XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Unknown error", currentOperation));
            }
        }

        public string ArchivePaymentProfile(string paymentProfileID, string systemID)
        {
            string currentOperation = "ArchivePaymentProfile";

            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = null;

            try
            {
                PaymentWrapperBL paymentWrapperBL = new PaymentWrapperBL(_PaymentSA);
                paymentWrapperBL.ArchivePaymentProfile(paymentProfileID, systemID, ref response);

                if (response.InternalResponse.internalcode == "0")
                {
                    XmlSerializationHelper<ArchiveProfilePaymentResponse> helper = new XmlSerializationHelper<ArchiveProfilePaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildArchiveProfileResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation, response.ispaymentprofilearchived));
                }
                else
                {
                    XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation));
                }
            }
            catch (Exception ex)
            {
                _PaymentSA.CloseProxyInstance();

                XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Unknown error", currentOperation));
            }
        }

        public string IsPaymentProfileValid(string paymentProfileID, string systemID)
        {
            string currentOperation = "IsPaymentProfileValid";

            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = null;

            try
            {
                PaymentWrapperBL paymentWrapperBL = new PaymentWrapperBL(_PaymentSA);
                paymentWrapperBL.IsPaymentProfileValid(paymentProfileID, systemID, ref response);

                if (response.InternalResponse.internalcode == "0")
                {
                    XmlSerializationHelper<IsProfileValidPaymentResponse> helper = new XmlSerializationHelper<IsProfileValidPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildIsProfileValidResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation, response.ispaymentprofilevalid));
                }
                else
                {
                    XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation));
                }
            }
            catch (Exception ex)
            {
                _PaymentSA.CloseProxyInstance();

                XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Unknown error", currentOperation));
            }
        }

        public string IsCreditSetToVoid(int paymentID, int systemID)
        {
            string currentOperation = "IsCreditSetToVoid";

            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = null;

            try
            {
                PaymentWrapperBL paymentWrapperBL = new PaymentWrapperBL(_PaymentSA);
                paymentWrapperBL.IsCreditSetToVoid(paymentID, systemID, ref response);

                if (response.InternalResponse.internalcode == "0")
                {
                    XmlSerializationHelper<IsCreditSetToVoidPaymentResponse> helper = new XmlSerializationHelper<IsCreditSetToVoidPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildIsCreditSetToVoidResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation, response.transactiontype));
                }
                else
                {
                    XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                    PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                    return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse(int.Parse(response.InternalResponse.internalcode), response.InternalResponse.internaldescription, currentOperation));
                }
            }
            catch (Exception ex)
            {
                _PaymentSA.CloseProxyInstance();

                XmlSerializationHelper<FailedPaymentResponse> helper = new XmlSerializationHelper<FailedPaymentResponse>();
                PaymentResponseHelper responseHelper = new PaymentResponseHelper();
                return helper.SerializeObject(responseHelper.BuildFailedPaymentResponse((int)PaymentWrapperResponse.UnknownException, "Unknown error", currentOperation));
            }
        }

        /*
        #region Helper Functions
        
        private static TransactionType ConvertTransactionTypeStringToEnum(string transactionType)
        {
            if (Enum.IsDefined(typeof(TransactionType), transactionType))
            {
                return (TransactionType)Enum.Parse(typeof(TransactionType), transactionType, true);
            }
            else
            {
                throw new ApplicationException("PaymentWrapperSM.ConvertTransactionTypeStringToEnum() error.");
            }
        }

        #endregion
        */
    }
}
