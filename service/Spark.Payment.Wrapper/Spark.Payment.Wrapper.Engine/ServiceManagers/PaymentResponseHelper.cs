﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Spark.Payment.Wrapper.Engine.ValueObjects.Responses;

namespace Spark.Payment.Wrapper.Engine.ServiceManagers
{
    internal class PaymentResponseHelper
    {
        public FailedPaymentResponse BuildFailedPaymentResponse(int internalResponseCode, string internalResponseMessage, string failedOperation)
        {
            FailedPaymentResponse response = new FailedPaymentResponse();
            response.version = "1.0";
            response.response = "FAILURE";
            response.message = failedOperation;

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public CreatePaymentProfilePaymentResponse BuildCreatePaymentProfileResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, string paymentProfileID)
        {
            CreatePaymentProfilePaymentResponse response = new CreatePaymentProfilePaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            response.paymentProfileID = paymentProfileID;

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public BillPaymentProfilePaymentResponse BuildBillPaymentProfileResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, string paymentID)
        {
            BillPaymentProfilePaymentResponse response = new BillPaymentProfilePaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            response.paymentID = paymentID;

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public CreditTransactionPaymentResponse BuildCreditTransactionResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, string paymentID, string paymentEngineOperation)
        {
            CreditTransactionPaymentResponse response = new CreditTransactionPaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            response.paymentID = paymentID;
            response.paymentEngineOperation = paymentEngineOperation;

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public ArchiveProfilePaymentResponse BuildArchiveProfileResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, string isArchived)
        {
            ArchiveProfilePaymentResponse response = new ArchiveProfilePaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            response.isArchived = isArchived;

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public IsProfileValidPaymentResponse BuildIsProfileValidResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, string isValid)
        {
            IsProfileValidPaymentResponse response = new IsProfileValidPaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            response.isValid = isValid;

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public GetPaymentProfilePaymentResponse BuildGetProfileResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, Spark.Payment.Wrapper.Engine.PaymentService.PaymentProfileBase paymentProfile)
        {
            GetPaymentProfilePaymentResponse response = new GetPaymentProfilePaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            Spark.Payment.Wrapper.Engine.PaymentService.CreditCardPaymentProfile creditCardPaymentProfile = null;
            creditCardPaymentProfile = paymentProfile as Spark.Payment.Wrapper.Engine.PaymentService.CreditCardPaymentProfile;
            if (creditCardPaymentProfile != null)
            {
                response.obfuscatedCreditCardPaymentProfile = new ObfuscatedCreditCardPaymentProfile[] { FillCreditCardPaymentProfile(creditCardPaymentProfile) };
            }

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        public IsCreditSetToVoidPaymentResponse BuildIsCreditSetToVoidResponse(int internalResponseCode, string internalResponseMessage, string currentOperation, string transactionType)
        {
            IsCreditSetToVoidPaymentResponse response = new IsCreditSetToVoidPaymentResponse();
            response.version = "1.0";
            response.response = "SUCCESS";
            response.message = currentOperation;
            if (transactionType == "Void")
            {
                response.isCreditSetToVoid = "true";
            }
            else
            {
                response.isCreditSetToVoid = "false";
            }

            InternalResponse internalResponse = new InternalResponse { internalResponseCode = internalResponseCode.ToString(), internalResponseDescription = internalResponseMessage };
            response.internalResponse = new InternalResponse[] { internalResponse };

            return response;
        }

        private ObfuscatedCreditCardPaymentProfile FillCreditCardPaymentProfile(Spark.Payment.Wrapper.Engine.PaymentService.CreditCardPaymentProfile creditCardPaymentProfile)
        {
            ObfuscatedCreditCardPaymentProfile obfuscatedCreditCardPaymentProfile = new ObfuscatedCreditCardPaymentProfile
            {
                FirstName = creditCardPaymentProfile.FirstName,
                LastName = creditCardPaymentProfile.LastName,
                CardType = creditCardPaymentProfile.CardType.ToString(),
                CreditCardNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber,
                City = creditCardPaymentProfile.City,
                State = creditCardPaymentProfile.State,
                Country = creditCardPaymentProfile.Country,
                ExpMonth = creditCardPaymentProfile.ExpMonth.ToString(),
                ExpYear = creditCardPaymentProfile.ExpYear.ToString(),
                PostalCode = creditCardPaymentProfile.PostalCode,
                StreetAddress = creditCardPaymentProfile.StreetAddress,
                PaymentType = Enum.GetName(typeof(Spark.Payment.Wrapper.Engine.PaymentService.PaymentType), creditCardPaymentProfile.PaymentProfileType)
            };

            return obfuscatedCreditCardPaymentProfile;
        }
    }
}
