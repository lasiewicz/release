﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Spark.Payment.Wrapper.Engine.BusinessLogic
{
    public class XmlSerializationHelper<T>
    {
        public string SerializeObject(T data, XmlSerializerNamespaces ns)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            {
                XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true });
                s.Serialize(writer, data, ns);
                return sw.ToString();
            }
        }

        public string SerializeObject(T data)
        {
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "http://spark.net/schema/online");

            return SerializeObject(data, ns);
        }

        public T DeserializeObject(string data)
        {
            XmlSerializer s = new XmlSerializer(typeof(T));
            T newObject;
            using (StringReader r = new StringReader(data))
            {
                XmlReader reader = XmlTextReader.Create(r);
                newObject = (T)s.Deserialize(reader);
            }

            return newObject;
        }
    }
}
