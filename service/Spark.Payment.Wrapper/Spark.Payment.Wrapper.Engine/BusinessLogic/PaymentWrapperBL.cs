﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Spark.Payment.Wrapper.Engine.PaymentService;
using Spark.Payment.Wrapper.Engine.ValueObjects;
using Spark.Payment.Wrapper.Engine.ValueObjects.Validations;
using Spark.Payment.Wrapper.Engine.ValueObjects.Operations;
using Spark.Payment.Wrapper.Engine.ValueObjects.Responses;
using Spark.Payment.Wrapper.Engine.ValueObjects.Enums;
using Spark.Payment.Wrapper.Engine.DataAccess;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.Payment.Wrapper.Engine.ServiceAdapters;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;

namespace Spark.Payment.Wrapper.Engine.BusinessLogic
{
    public class PaymentWrapperBL
    {
        PaymentSA _PaymentSA = null;

        public PaymentWrapperBL(PaymentSA paymentSA)
        {
            _PaymentSA = paymentSA;
        }

        public void BillPaymentProfile(string paymentProfileID, decimal amount, string currency, int orderID, int systemID, int applicationTypeID, string overrideExpMonth, string overrideExpYear, int transactionTypeID, string[] requiredValidations, ref Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response, ref ValidationResponse validation)
        {
            try
            {
                TraceLog.Enter("PaymentWrapperBL.BillPaymentProfile");
                TraceLog.WriteLine("Begin PaymentWrapperBL.BillPaymentProfile({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})", 
                    paymentProfileID,
                    amount,
                    currency,
                    orderID,
                    systemID,
                    applicationTypeID,
                    overrideExpMonth,
                    overrideExpYear, 
                    transactionTypeID,
                    requiredValidations,
                    response,
                    validation);

                OperationDetails operation = new OperationDetails { operationType = OperationType.BillPaymentProfile, OrderID = orderID.ToString(), SystemID = systemID.ToString() };
                TraceLog.WriteLine("OperationDetails object created: operationType={0}, OrderID={1}, SystemID={2}", operation.operationType, operation.OrderID, operation.SystemID);                 

                IValidator dupTran = new DuplicateTransactionValidator();
                validation = dupTran.ValidateOperation(operation);
                TraceLog.WriteLine("Checked for duplicate transaction, Is duplicate transaction? {0}, Validation message: {1}", validation.IsValid, validation.Message);

                if (validation.IsValid)
                {
                    try
                    {
                        response = null;

                        for (int tryCount = 1; tryCount < 3; tryCount++)
                        {
                            TraceLog.WriteLine("Attempt {0} of 2 to process the charge", tryCount);
                            response = ProcessBillPaymentProfile(paymentProfileID, amount, currency, orderID, systemID, applicationTypeID, transactionTypeID, requiredValidations);
                            if (response.InternalResponse.internalcode != "10")
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Failed calling PaymentWrapperBL.BillPaymentProfile()", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperBL.BillPaymentProfile(), Error: " + ex.Message);
                throw new BLException("PaymentWrapperBL.BillPaymentProfile() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperBL.BillPaymentProfile()");
                TraceLog.Leave("PaymentWrapperBL.BillPaymentProfile");
            }
        }

        #region Private Bill methods

        private Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse ProcessBillPaymentProfile(string paymentProfileID, decimal amount, string currency, int orderID, int systemID, int systemTypeID, int transactionTypeID, string[] requiredValidations)
        {
            //PaymentServiceClient paymentServiceProxy = new PaymentServiceClient();
            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response;

            try
            {
                TraceLog.Enter("PaymentWrapperBL.ProcessBillPaymentProfile");
                TraceLog.WriteLine("Begin PaymentWrapperBL.ProcessBillPaymentProfile({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})",
                    paymentProfileID,
                    amount,
                    currency,
                    orderID,
                    systemID,
                    systemTypeID,
                    transactionTypeID,
                    (requiredValidations != null ? "[" + String.Join(",", requiredValidations) + "]" : null));

                response = _PaymentSA.GetProxyInstance().ValidateOperation(paymentProfileID, amount, currency, orderID, systemID, systemTypeID, transactionTypeID, requiredValidations);
                TraceLog.WriteLine("Called payment service to run the required validations, Internal response code: {0}, Internal response message: {1}", response.InternalResponse.internalcode, response.InternalResponse.internaldescription);

                if (response.InternalResponse.internalcode == "0")
                {
                    response = AuthorizeProfile(paymentProfileID, amount, currency, orderID, systemID, systemTypeID, transactionTypeID);
                    TraceLog.WriteLine("Processed authorization of the charge, Internal response code: {0}, Internal response message: {1}", response.InternalResponse.internalcode, response.InternalResponse.internaldescription);

                    if (response.InternalResponse.internalcode == "0")
                    {
                        response = BillProfile(paymentProfileID, amount, currency, orderID, systemID, systemTypeID, transactionTypeID, response.authorizationID);
                        TraceLog.WriteLine("Processed billing the charge, Internal response code: {0}, Internal response message: {1}", response.InternalResponse.internalcode, response.InternalResponse.internaldescription);

                        if (response.InternalResponse.internalcode == "0")
                        {
                            _PaymentSA.GetProxyInstance().InsertVelocityLog(paymentProfileID, systemID, transactionTypeID, true);
                        }
                        else
                        {
                            _PaymentSA.GetProxyInstance().InsertVelocityLog(paymentProfileID, systemID, transactionTypeID, true);
                        }
                        TraceLog.WriteLine("Inserted the velocity log into the database");
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperBL.ProcessBillPaymentProfile(), Error: " + ex.Message);
                throw new BLException("PaymentWrapperBL.ProcessBillPaymentProfile() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperBL.ProcessBillPaymentProfile()");
                TraceLog.Leave("PaymentWrapperBL.ProcessBillPaymentProfile");
            }

            // Get the response from Bill() rather than InsertVelocityLog()
            return response;
        }

        private Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse AuthorizeProfile(string paymentProfileID, decimal amount, string currency, int orderID, int systemID, int systemTypeID, int transactionTypeID)
        {
            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse paymentResponse = null;
            int paymentID = Constants.NULL_INT;
            PaymentWrapperDAL paymentWrapperDAL = new PaymentWrapperDAL();

            try
            {
                TraceLog.Enter("PaymentWrapperBL.AuthorizeProfile");
                TraceLog.WriteLine("Begin PaymentWrapperBL.AuthorizeProfile({0}, {1}, {2}, {3}, {4}, {5}, {6})",
                    paymentProfileID,
                    amount,
                    currency,
                    orderID,
                    systemID,
                    systemTypeID,
                    transactionTypeID);

                //PaymentServiceClient paymentServiceProxy = new PaymentServiceClient();
                // get paymentID from PE
                Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = _PaymentSA.GetProxyInstance().GetNextPaymentID();

                paymentID = int.Parse(response.paymentid);
                TraceLog.WriteLine("Called payment service to get the next payment ID: {0}", paymentID);
                paymentWrapperDAL.InsertPaymentToOrder(systemID, systemTypeID, orderID, paymentID, PaymentTransactionType.Authorization, PaymentChargeStatus.Pending);
                TraceLog.WriteLine("Inserted the new order to payment record into the database");

                //string[] requiredValidations = OperationValidations.GetRequiredValidationsForSending(transactionType);
                // authorize for operation
                paymentResponse = _PaymentSA.GetProxyInstance().Authorize(paymentProfileID, amount, currency, paymentID, systemID, systemTypeID);
                TraceLog.WriteLine("Called payment service to authorize the charge, Internal response code: {0}, Internal response message: {1}", paymentResponse.InternalResponse.internalcode, paymentResponse.InternalResponse.internaldescription);

                if (paymentResponse.InternalResponse.internalcode == "0")
                {
                    // Order was successfully authorized by the gateway
                    paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.Authorization, PaymentChargeStatus.Success);
                }
                else
                {
                    // Order was not allowed authorization by the gateway
                    paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.Authorization, PaymentChargeStatus.Failure);
                }
                TraceLog.WriteLine("Updated the order to payment record in the database");         
            }
            catch (Exception ex)
            {
                // Error in trying to authorize the order 
                paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.Authorization, PaymentChargeStatus.Failure);
                TraceLog.WriteLine("Updated the order to payment record in the database");         
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperBL.AuthorizeProfile(), Error: " + ex.Message);
                throw new BLException("PaymentWrapperBL.AuthorizeProfile() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperBL.AuthorizeProfile()");
                TraceLog.Leave("PaymentWrapperBL.AuthorizeProfile");
            }

            return paymentResponse;
        }

        private Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse BillProfile(string paymentProfileID, decimal amount, string currency, int orderID, int systemID, int systemTypeID, int transactionTypeID, string authorizationID)
        {
            Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse paymentResponse = null;
            int paymentID = Constants.NULL_INT;
            PaymentWrapperDAL paymentWrapperDAL = new PaymentWrapperDAL();

            try
            {
                TraceLog.Enter("PaymentWrapperBL.BillProfile");
                TraceLog.WriteLine("Begin PaymentWrapperBL.BillProfile({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})",
                    paymentProfileID,
                    amount,
                    currency,
                    orderID,
                    systemID,
                    systemTypeID,
                    transactionTypeID,
                    authorizationID);

                //PaymentServiceClient paymentServiceProxy = new PaymentServiceClient();
                // get paymentID from PE
                Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response = _PaymentSA.GetProxyInstance().GetNextPaymentID();
                paymentID = int.Parse(response.paymentid);
                TraceLog.WriteLine("Called payment service to get the next payment ID: {0}", paymentID);
                paymentWrapperDAL.InsertPaymentToOrder(systemID, systemTypeID, orderID, paymentID, PaymentTransactionType.Bill, PaymentChargeStatus.Pending);
                TraceLog.WriteLine("Inserted the new order to payment record into the database");

                //string[] requiredValidations = OperationValidations.GetRequiredValidationsForSending(transactionType);
                // capture
                paymentResponse = _PaymentSA.GetProxyInstance().Bill(paymentProfileID, amount, 1, currency, paymentID, systemID, systemTypeID, authorizationID);
                TraceLog.WriteLine("Called payment service to bill the charge, Internal response code: {0}, Internal response message: {1}", paymentResponse.InternalResponse.internalcode, paymentResponse.InternalResponse.internaldescription);

                if (paymentResponse.InternalResponse.internalcode == "0")
                {
                    // Order was successfully charged by the gateway
                    paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.Bill, PaymentChargeStatus.Success);
                }
                else
                {
                    // Order was not allowed to be charged by the gateway
                    paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.Bill, PaymentChargeStatus.Failure);
                }
                TraceLog.WriteLine("Updated the order to payment record in the database");         
            }
            catch (Exception ex)
            {
                // Error in trying to charge the order 
                paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.Bill, PaymentChargeStatus.Failure);
                TraceLog.WriteLine("Updated the order to payment record in the database");         
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperBL.BillProfile(), Error: " + ex.Message);
                throw new BLException("PaymentWrapperBL.BillProfile() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperBL.BillProfile()");
                TraceLog.Leave("PaymentWrapperBL.BillProfile");
            }

            return paymentResponse;
        }

        #endregion


        public void CreditTransaction(decimal amount, int systemID, int orderID, int originalPaymentID, int systemTypeID, int transactionTypeID, string[] requiredValidations, ref Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response, ref ValidationResponse validation)
        {
            PaymentWrapperDAL paymentWrapperDAL = new PaymentWrapperDAL();
            int paymentID = Constants.NULL_INT;

            try
            {
                TraceLog.Enter("PaymentWrapperBL.CreditTransaction");
                TraceLog.WriteLine("Begin PaymentWrapperBL.CreditTransaction({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})",
                    amount,
                    systemID,
                    orderID,
                    originalPaymentID,
                    systemTypeID,
                    transactionTypeID,
                    requiredValidations,
                    response,
                    validation);

                OperationDetails operation = new OperationDetails { operationType = OperationType.CreditTransaction, OrderID = orderID.ToString(), SystemID = systemID.ToString() };
                TraceLog.WriteLine("OperationDetails object created: operationType={0}, OrderID={1}, SystemID={2}", operation.operationType, operation.OrderID, operation.SystemID);                 
                response = _PaymentSA.GetProxyInstance().ValidateOperation(Constants.NULL_STRING, amount, Constants.NULL_STRING, orderID, systemID, systemTypeID, transactionTypeID, requiredValidations);
                TraceLog.WriteLine("Called payment service to run the required validations, Internal response code: {0}, Internal response message: {1}", response.InternalResponse.internalcode, response.InternalResponse.internaldescription);

                if (response.InternalResponse.internalcode == "0")
                {
                    PaymentWrapperDAL dal = new PaymentWrapperDAL();

                    response = _PaymentSA.GetProxyInstance().GetNextPaymentID();
                    paymentID = int.Parse(response.paymentid);
                    TraceLog.WriteLine("Called payment service to get the next payment ID: {0}", paymentID);
                    paymentWrapperDAL.InsertPaymentToOrder(systemID, systemTypeID, orderID, paymentID, PaymentTransactionType.UndeterminedCreditOrVoid, PaymentChargeStatus.Pending);
                    TraceLog.WriteLine("Inserted the new order to payment record into the database");

                    try
                    {
                        response = _PaymentSA.GetProxyInstance().CreditTransaction(originalPaymentID, paymentID, amount, systemID, systemTypeID);
                        TraceLog.WriteLine("Called payment service to provide a credit, Internal response code: {0}, Internal response message: {1}", response.InternalResponse.internalcode, response.InternalResponse.internaldescription);

                        if (response.InternalResponse.internalcode == "0")
                        {
                            // Order was successfully credited or voided by the gateway
                            if (response.transactiontype != null)
                            {
                                paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, ConvertPaymentTransactionTypeStringToEnum(response.transactiontype), PaymentChargeStatus.Success);
                            }
                            else
                            {
                                // The transaction type can be null when attempting to credit a transaction but 
                                // the credit process ended before processing through the gateway  
                                paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.UndeterminedCreditOrVoid, PaymentChargeStatus.Success);
                            }
                        }
                        else
                        {
                            // Order was not allowed to be credited or voided by the gateway
                            if (response.transactiontype != null)
                            {
                                paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, ConvertPaymentTransactionTypeStringToEnum(response.transactiontype), PaymentChargeStatus.Failure);
                            }
                            else
                            {
                                // The transaction type can be null when attempting to credit a transaction but 
                                // the credit process ended before processing through the gateway  
                                paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.UndeterminedCreditOrVoid, PaymentChargeStatus.Failure);
                            }
                        }
                        TraceLog.WriteLine("Updated the order to payment record in the database, Payment transaction type determined by the gateway: {0}", response.transactiontype);         
                    }
                    catch(Exception ex)
                    {
                        // Error in trying to credit or void the order 
                        if (response.transactiontype != null)
                        {
                            paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, ConvertPaymentTransactionTypeStringToEnum(response.transactiontype), PaymentChargeStatus.Failure);
                        }
                        else
                        {
                            // The transaction type can be null when attempting to credit a transaction but 
                            // the credit process ended before processing through the gateway  
                            paymentWrapperDAL.UpdatePaymentToOrder(orderID, paymentID, PaymentTransactionType.UndeterminedCreditOrVoid, PaymentChargeStatus.Failure);
                        }
                        TraceLog.WriteLine("Updated the order to payment record in the database");
                        throw new BLException("Error in the credit request to the payment service after the order to payment record was inserted into the database");
                    }
                }
            }
            catch(Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperBL.CreditTransaction(), Error: " + ex.Message);
                throw new BLException("PaymentWrapperBL.CreditTransaction() error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperBL.CreditTransaction()");
                TraceLog.Leave("PaymentWrapperBL.CreditTransaction");
            }
        }

        public void GetObfuscatedPaymentProfile(string paymentProfileID, string systemID, ref Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response)
        {
            try
            {
                TraceLog.Enter("PaymentWrapperBL.GetObfuscatedPaymentProfile");
                TraceLog.WriteLine("Begin PaymentWrapperBL.GetObfuscatedPaymentProfile({0}, {1}, {2})", paymentProfileID, systemID, response);
                //PaymentServiceClient paymentServiceProxy = new PaymentServiceClient();
                response = _PaymentSA.GetProxyInstance().GetObfuscatedPaymentProfile(paymentProfileID, systemID);
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in PaymentWrapperBL.GetObfuscatedPaymentProfile(), Error: " + ex.Message);
                throw new ApplicationException("Failed calling PaymentWrapperBL.GetObfuscatedPaymentProfile()", ex);
            }
            finally
            {
                TraceLog.WriteLine("End PaymentWrapperBL.GetObfuscatedPaymentProfile()");
                TraceLog.Leave("PaymentWrapperBL.GetObfuscatedPaymentProfile");
            }
        }

        public void ArchivePaymentProfile(string paymentProfileID, string systemID, ref Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response)
        {
            try
            {
                //PaymentServiceClient paymentServiceProxy = new PaymentServiceClient();
                response = _PaymentSA.GetProxyInstance().ArchivePaymentProfile(paymentProfileID, systemID);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed calling PaymentWrapperBL.ArchivePaymentProfile()", ex);
            }
        }

        public void IsPaymentProfileValid(string paymentProfileID, string systemID, ref Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response)
        {
            try
            {
                //PaymentServiceClient paymentServiceProxy = new PaymentServiceClient();

                response = _PaymentSA.GetProxyInstance().IsPaymentProfileValid(paymentProfileID, systemID);                
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed calling PaymentWrapperBL.IsPaymentProfileValid()", ex);
            }
        }

        public void IsCreditSetToVoid(int paymentID,  int systemID, ref Spark.Payment.Wrapper.Engine.PaymentService.PaymentResponse response)
        {
            try
            {
                response = _PaymentSA.GetProxyInstance().DetermineCreditOrVoid(paymentID, systemID);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed calling PaymentWrapperBL.IsCreditSetToVoid()", ex);
            }
        }

        private static PaymentTransactionType ConvertPaymentTransactionTypeStringToEnum(string paymentTransactionType)
        {
            if (Enum.IsDefined(typeof(PaymentTransactionType), paymentTransactionType))
            {
                return (PaymentTransactionType)Enum.Parse(typeof(PaymentTransactionType), paymentTransactionType, true);
            }
            else
            {
                return PaymentTransactionType.None;
            }
        }
    }
}
