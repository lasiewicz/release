﻿using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Spark.PushNotifications.Processor.BusinessLogic;
using Spark.PushNotifications.Processor.BusinessLogic.Dispatchers;
using Spark.PushNotifications.Processor.ValueObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spark.PushNotifications.Processor.Harness
{
    public partial class UrbanAirshipDispatcher : Form
    {
        public UrbanAirshipDispatcher()
        {
            InitializeComponent();

            //bind notification type dropdown
            foreach (var value in Enum.GetValues(typeof(PushNotificationTypeID)))
            {
                ddlSNNotificationType.Items.Add(value);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtOutput.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*
             * Urban Airship JDate Dev
             * 
             * Joey
             * memberID: 141942251
             * 
             * Kenny
             * memberID: 132656906
             * 
             * Shana
             * memberID: 133009601
             * 
             * Arvin
             * memberID: 132656906
             * 
             * Stacey
             * memberID: 114818583
             * 
             * Evan
             * memberID: 140468429
             */

            if (!string.IsNullOrWhiteSpace(ddlSNNotificationType.Text))
            {
                PushNotificationTypeID notificationType = (PushNotificationTypeID)Enum.Parse(typeof(PushNotificationTypeID), ddlSNNotificationType.Text);

                string allMembers = "132656906,141942251,133009601,114818583,140468429";
                switch (notificationType)
                {
                    case PushNotificationTypeID.MessageReceived:
                        txtSNCategoryTag.Text = "JDatePushTypeActivity";
                        txtSNMemberID.Text = allMembers;
                        txtSNText.Text = "Test - You have a new message buddy";
                        txtSNDeepLink.Text = "spark-jdate://message/1242514472/?sub=true";
                        break;
                    case PushNotificationTypeID.ChatRequest:
                        txtSNCategoryTag.Text = "JDatePushTypeActivity";
                        txtSNMemberID.Text = allMembers;
                        txtSNText.Text = "Test - You have a new chat request buddy";
                        txtSNDeepLink.Text = "spark-jdate://chat/113915549/?sub=true";
                        break;
                    case PushNotificationTypeID.MutualYes:
                        txtSNCategoryTag.Text = "JDatePushTypeActivity";
                        txtSNMemberID.Text = allMembers;
                        txtSNText.Text = "Test - You have a new mutual yes buddy";
                        txtSNDeepLink.Text = "spark-jdate://mutualyes/113915549/?sub=true";
                        break;
                    case PushNotificationTypeID.PhotoApproved:
                        txtSNCategoryTag.Text = "JDatePushTypePhotos";
                        txtSNMemberID.Text = allMembers;
                        txtSNText.Text = "Test - You have a new approved photo buddy";
                        txtSNDeepLink.Text = "spark-jdate://photo/approved/?sub=true";
                        break;
                    case PushNotificationTypeID.PhotoRejected:
                        txtSNCategoryTag.Text = "JDatePushTypePhotos";
                        txtSNMemberID.Text = allMembers;
                        txtSNText.Text = "Test - You have a new rejected photo buddy";
                        txtSNDeepLink.Text = "spark-jdate://photo/rejected/?sub=true";
                        break;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var dispatcher = (new NotificationDispatcherFactory()).GetNotificationDispatcher();

                string[] memberIDs = txtSNMemberID.Text.Split(new char[]{','});
                foreach (string m in memberIDs)
                {
                    var pushNotification = new PushNotification();
                    pushNotification.MemberId = int.Parse(m.Trim());
                    pushNotification.NotificationText = txtSNText.Text;
                    pushNotification.NotificationCategoryTag = txtSNCategoryTag.Text;
                    pushNotification.DeepLink = txtSNDeepLink.Text;
                    pushNotification.BrandId = 1003;

                    NotificationUtility notificationUtility = new NotificationUtility();
                    Brand brand = notificationUtility.BrandConfigService.GetBrandByID(1003);

                    var notificationResponse = dispatcher.SendNotification(pushNotification, brand);
                    txtOutput.Text = txtOutput.Text + "\r\n" + string.Format("Urban airship response success: {0}, operation ID: {1}", notificationResponse.Success, notificationResponse.OperationID);
                }
            }
            catch(Exception ex)
            {
                txtOutput.Text = ex.Message + ", Stack: " + ex.StackTrace;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtOutput.Text = MemberAppDeviceHelper.GenerateMemberIdHash(Convert.ToInt32(txtMiscMemberID.Text));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                AppGroupID appGroup = (AppGroupID)Enum.Parse(typeof(AppGroupID), txtMiscAppGroupID.Text);
                PushNotificationAppGroupModel appGroupModel = PushNotificationMetadataSA.Instance.GetPushNotificationAppGroupModel(appGroup);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("App Group: " + appGroup.ToString());
                if (appGroupModel == null)
                {
                    sb.AppendLine("No app group model data available");
                }
                else
                {
                    foreach (var a in appGroupModel.NotificationAppGroupCategories)
                    {
                        sb.AppendLine(string.Format("Category: {0}, Tag: {1}", a.Category.Name, a.NotificationCategoryTag));
                        foreach(var b in a.NotificationAppGroupCategoryTypes)
                        {
                            sb.AppendLine(string.Format("NotificationType: {0}, DeepLink: {1}, BadgeIncrementValue: {2}, ListOrder: {3}", b.NotificationType.Name, b.DeepLink, b.BadgeIncrementValue, b.Order));
                        }
                        sb.AppendLine(" ");
                    }
                }

                txtOutput.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                txtOutput.Text = ex.Message;
            }

        }

    }
}
