﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Data.Hydra;
using Spark.PushNotifications.Processor.BusinessLogic;

namespace Spark.PushNotifications.Processor.ServiceManager
{
    public class ProcessorSM : MarshalByRefObject, IServiceManager, IBackgroundProcessor
    {
        
        public ProcessorSM()
        {

        }

        public void PrePopulateCache()
        {
            
        }

        public void Dispose()
        {
            
        }

        public void Start()
        {
            ProcessorQueueManager.Instance.Start();
        }

        public void Stop()
        {
            ProcessorQueueManager.Instance.Stop();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
