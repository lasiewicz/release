﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Spark.Managers.Managers;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Spark.PushNotifications.Processor.BusinessLogic.Interfaces
{
    public interface INotificationUtility
    {
        IMemberAppDeviceService MemberAppDeviceService { get; set; }
        ISettingsSA SettingsService { get; set; }
        IBrandConfigSA BrandConfigService { get; set; }
        IGetMember MemberService { get; set; }
        bool AreNotificationsEnabledForBrand(int brandId);
        bool IsNotificationTypeEnabledForMember(int memberId, PushNotificationTypeID notificationTypeId);
        IMember GetMember(int memberId);
    }
}
