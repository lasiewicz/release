﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.PushNotifications.Processor.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.PushNotifications.Processor.BusinessLogic.Interfaces
{
    public interface INotificationDispatcher
    {
        NotificationResponse SendNotification(PushNotification pushNotification, Brand brand);
    }
}
