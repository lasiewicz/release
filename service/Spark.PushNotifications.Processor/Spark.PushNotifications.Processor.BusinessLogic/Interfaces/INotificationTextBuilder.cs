﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.Localization;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;

namespace Spark.PushNotifications.Processor.BusinessLogic.Interfaces
{
    public interface INotificationTextBuilder
    {
        IPushNotificationMetadataSAService PushNotificationMetadataService { get; set; }
        IResourceProvider ResourceProvider { get; set; }
        INotificationUtility NotificationUtility { get; set; }
        string GetNotificationText(SystemEvent systemEvent);
    }
}
