﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.PushNotifications.Processor.BusinessLogic.Interfaces
{
    public interface INotificationDispatcherFactory
    {
        INotificationDispatcher GetNotificationDispatcher();
    }
}
