﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.SystemEvents;
using Spark.PushNotifications.Processor.ValueObjects;

namespace Spark.PushNotifications.Processor.BusinessLogic.Interfaces
{
    public interface INotificationCreator
    {
        INotificationTextBuilder NotificationTextBuilder { get; set; }
        PushNotification CreateNotification(SystemEvent systemEvent);
    }
}
