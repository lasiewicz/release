﻿using System.Text;
using log4net;
using log4net.Config;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data;
using Matchnet.Member.ServiceAdapters;
using Spark.ActivityRecording.Processor.ServiceAdapter;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.PushNotifications.Processor.ValueObjects;
using Spark.RabbitMQ.Client;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;
using Spark.Logger;
using System;
using System.Collections.Generic;

namespace Spark.PushNotifications.Processor.BusinessLogic
{
    public class ProcessorQueueManager
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ProcessorQueueManager));

        public readonly static ProcessorQueueManager Instance = new ProcessorQueueManager();
        private Subscriber _subscriber;
        private string _rabbitMQUserName = string.Empty;
        private string _rabbitMQPassword = string.Empty;
        private string _rabbitMQServer = string.Empty;
        private int _prefetchCount = 0;

        private ProcessorQueueManager()
        {
            XmlConfigurator.Configure();
        }

        public void Start()
        {
            GetSettings();
            _subscriber = new Subscriber(_rabbitMQServer, _rabbitMQUserName, _rabbitMQPassword, _prefetchCount);
            _subscriber.Subscribe<SystemEvent>("SystemEvent", ProcessQueueItem);
            _subscriber.Subscribe<IPushNotificationEvent>("IPushNotificationEvent", ProcessQueueIPushNotificationEventItem);

            Log.LogInfoMessage("Subscribed: " + _rabbitMQServer, null);
        }

        public void Stop()
        {
            if (_subscriber != null)
            {
                Log.LogInfoMessage("Stopped", null);
                _subscriber.Dispose();
            }
        }

        private void GetSettings()
        {
            _rabbitMQUserName = RuntimeSettings.GetSetting(SettingConstants.RABBITMQ_USER_ACCOUNT);
            _rabbitMQPassword = RuntimeSettings.GetSetting(SettingConstants.RABBITMQ_PASSWORD);
            _rabbitMQServer = RuntimeSettings.GetSetting(SettingConstants.PUSHNOTIFICATIONS_PROCESSOR_RABBITMQ_SERVER);
            _prefetchCount = int.Parse(RuntimeSettings.GetSetting(SettingConstants.PUSHNOTIFICATIONS_PROCESSOR_RABBITMQ_PREFETCH));
        }

        private void ProcessQueueIPushNotificationEventItem(IPushNotificationEvent pushNotificationEvent)
        {
            ProcessQueueItem(pushNotificationEvent as SystemEvent);
        }

        private void ProcessQueueItem(SystemEvent systemEvent)
        {
            if (systemEvent != null)
            {
                try
                {
                    var notificationProcessor = new NotificationProcessor();
                    notificationProcessor.CreateAndSendNotificationFromSystemEvent(systemEvent);
                }
                catch (Exception ex)
                {
                    //notifications are worth retrying later
                    Dictionary<string, string> customData = null;
                    if (systemEvent != null)
                    {
                        customData = new Dictionary<string, string> { { "memberId", systemEvent.MemberId.ToString() }, { "brandId", systemEvent.BrandId.ToString() }, { "eventCategory", systemEvent.EventCategory.ToString() }, { "eventType", systemEvent.EventType.ToString() }, { "appGroupID", systemEvent.AppGroupId.ToString() } };
                    }
                    Log.LogException("Error processing push notification in ProcessQueueItem", ex, customData);
                }
            }
        }
    }
}
