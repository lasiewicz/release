﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ServiceAdapters;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;
using Spark.PushNotifications.Processor.ValueObjects;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;


namespace Spark.PushNotifications.Processor.BusinessLogic
{
    public class NotificationCreator : INotificationCreator
    {
        private INotificationTextBuilder _notificationTextBuilder;
        private IPushNotificationMetadataSAService _pushNotificationMetadataService;
        private INotificationUtility _notificationUtility;
        
        public INotificationTextBuilder NotificationTextBuilder
        {
            get
            {
                return _notificationTextBuilder ?? new NotificationTextBuilder();
            }
            set
            {
                _notificationTextBuilder = value;
            }
        }
        
        public IPushNotificationMetadataSAService PushNotificationMetadataService
        {
            get
            {
                return _pushNotificationMetadataService ?? PushNotificationMetadataSA.Instance;
            }
            set
            {
                _pushNotificationMetadataService = value;
            }
        }

        public INotificationUtility NotificationUtility
        {
            get
            {
                return _notificationUtility ?? new NotificationUtility();
            }
            set
            {
                _notificationUtility = value;
            }
        }
        
        public PushNotification CreateNotification(SystemEvent systemEvent)
        {
            var pushNotificationEvent = systemEvent as IPushNotificationEvent;
            if (pushNotificationEvent == null)
            {
                throw new ArgumentException("SystemEvent must implement IPushNotificationEvent");
            }

            PushNotification pushNotification = null;
            switch (pushNotificationEvent.NotificationTypeId)
            {
                case PushNotificationTypeID.MessageReceived:
                case PushNotificationTypeID.ChatRequest:
                case PushNotificationTypeID.ChatRequestV2:
                case PushNotificationTypeID.ChatRequestV3:
                case PushNotificationTypeID.MutualYes:
                case PushNotificationTypeID.PhotoApproved:
                case PushNotificationTypeID.PhotoRejected:
                    pushNotification = new PushNotification();
                    pushNotification.MemberId = systemEvent.GetOperativeMemberId();
                    pushNotification.NotificationText = NotificationTextBuilder.GetNotificationText(systemEvent);
                    pushNotification.NotificationCategoryTag = GetNotificationTag(systemEvent.AppGroupId, ((IPushNotificationEvent)systemEvent).NotificationTypeId);
                    pushNotification.DeepLink = GetDeepLink(systemEvent);
                    pushNotification.IncrementBadge = GetIncrementBadge(systemEvent);
                    pushNotification.BrandId = systemEvent.BrandId;
                    break;
                default:
                    throw new Exception("Unknown notification type");
            }

            return pushNotification;
        }

        private string GetNotificationTag(AppGroupID appGroupId, PushNotificationTypeID notificationType)
        {
            var categoryTag = string.Empty;
            var notificationAppModel = PushNotificationMetadataService.GetPushNotificationAppGroupModel(appGroupId);
            if (notificationAppModel == null)
            {
                throw new Exception("Could not find notification app model for appgroup: " + appGroupId.ToString());
            }
            
            var category = PushNotificationsHelper.GetCategoryForNotificationType(notificationAppModel, notificationType);

            if(category == null)
            {
                throw new Exception("Could not find category for notification type: " + notificationType);
            }

            return category.NotificationCategoryTag;
        }

        private string GetDeepLink(SystemEvent systemEvent)
        {
            AppGroupID appGroupId = systemEvent.AppGroupId;
            PushNotificationTypeID noticationType = ((IPushNotificationEvent)systemEvent).NotificationTypeId;
            bool isSub = false;
            string deepLink = string.Empty;

            var notificationAppModel = PushNotificationMetadataService.GetPushNotificationAppGroupModel(appGroupId);
            if (notificationAppModel == null)
            {
                throw new Exception("Could not find notification app model for appgroup: " + appGroupId.ToString());
            }

            var notificationAppType = PushNotificationsHelper.GetAppTypeForNotificationType(notificationAppModel, noticationType);
            if (notificationAppType != null)
            {
                deepLink = notificationAppType.DeepLink;

                var targetMember = NotificationUtility.GetMember(systemEvent.GetOperativeMemberId());
                var brand = NotificationUtility.BrandConfigService.GetBrandByID(systemEvent.BrandId);
                isSub = targetMember.IsPayingMember(brand.Site.SiteID);

                //replace notification specific named placeholders
                switch (noticationType)
                {
                    case PushNotificationTypeID.MessageReceived:
                        EmailEvent emailEvent = systemEvent as EmailEvent;
                        deepLink = deepLink.Replace("{messagelistid}", emailEvent.MessageListID.ToString());
                        break;
                    case PushNotificationTypeID.ChatRequestV2:
                    case PushNotificationTypeID.ChatRequestV3:
                        IMRequestEvent imRequestEvent = systemEvent as IMRequestEvent;
                        deepLink = deepLink.Replace("{imid}", imRequestEvent.RoomID.ToString());
                        if (!string.IsNullOrEmpty(imRequestEvent.ResourceID))
                        {
                            deepLink = deepLink.Replace("{resourceid}", imRequestEvent.ResourceID.ToString());
                        }
                        break;
                }

                //replace common named placeholders
                deepLink = deepLink.Replace("{issub}", isSub.ToString()).Replace("{targetmemberid}", systemEvent.TargetMemberId.ToString()).Replace("{frommemberid}", systemEvent.MemberId.ToString());
            }

            return deepLink;
        }

        private int GetIncrementBadge(SystemEvent systemEvent)
        {
            int incrementBadge = 0;

            AppGroupID appGroupId = systemEvent.AppGroupId;
            PushNotificationTypeID noticationType = ((IPushNotificationEvent)systemEvent).NotificationTypeId;
            var notificationAppModel = PushNotificationMetadataService.GetPushNotificationAppGroupModel(appGroupId);
            if (notificationAppModel == null)
            {
                throw new Exception("Could not find notification app model for appgroup: " + appGroupId.ToString());
            }

            var notificationAppType = PushNotificationsHelper.GetAppTypeForNotificationType(notificationAppModel, noticationType);
            if (notificationAppType != null)
            {
                incrementBadge = notificationAppType.BadgeIncrementValue;
            }

            return incrementBadge;
        }
    }
}
