﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;
using Spark.PushNotifications.Processor.BusinessLogic.Dispatchers;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;
using Spark.Logger;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.PushNotifications.Processor.BusinessLogic
{
    public class NotificationProcessor
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(NotificationProcessor));
        
        #region Properties

        private INotificationUtility _notificationUtility;
        private INotificationCreator _notificationCreator;
        private INotificationDispatcherFactory _notificationDispatcherFactory;
        
        public INotificationUtility NotificationUtility
        {
            get
            {
                return _notificationUtility ?? new NotificationUtility();
            }
            set
            {
                _notificationUtility = value;
            }
        }

        public INotificationCreator NotificationCreator
        {
            get
            {
                return _notificationCreator ?? new NotificationCreator();
            }
            set
            {
                _notificationCreator = value;
            }
        }

        public INotificationDispatcherFactory NotificationDispatcherFactory
        {
            get
            {
                return _notificationDispatcherFactory ?? new NotificationDispatcherFactory();
            }
            set
            {
                _notificationDispatcherFactory = value;
            }
        }

        #endregion

        public void CreateAndSendNotificationFromSystemEvent(SystemEvent systemEvent)
        {
            if(systemEvent == null)
            {
                throw new ArgumentException("SystemEvent can not be null");
            }
            
            //if this event isn't a pushnotification event, just return
            IPushNotificationEvent pushNotificationEvent = systemEvent as IPushNotificationEvent;
            if(pushNotificationEvent == null) return;

            try
            {
                Brand brand = NotificationUtility.BrandConfigService.GetBrandByID(systemEvent.BrandId);
                if (!NotificationUtility.AreNotificationsEnabledForBrand(systemEvent.BrandId))
                {
                    return;
                }

                if (!NotificationUtility.IsNotificationTypeEnabledForMember(systemEvent.GetOperativeMemberId(),
                    pushNotificationEvent.NotificationTypeId))
                {
                    var customData = new Dictionary<string, string> { { "memberId", pushNotificationEvent.MemberId.ToString() }, { "brandId", pushNotificationEvent.BrandId.ToString() }, { "eventCategory", pushNotificationEvent.EventCategory.ToString() }, { "eventType", pushNotificationEvent.EventType.ToString() }, { "appGroupID", pushNotificationEvent.AppGroupId.ToString() }, { "notificationTypeID", pushNotificationEvent.NotificationTypeId.ToString() } };
                    Log.LogDebugMessage("Member has notification disabled.", customData);
                    return;
                }

                // if the target blocked the member in the past don't notify
                if (IsSenderBlockedByReceiver(pushNotificationEvent.MemberId, pushNotificationEvent.TargetMemberId, brand))
                {
                    var customData = new Dictionary<string, string> { { "memberId", pushNotificationEvent.MemberId.ToString() }, { "brandId", pushNotificationEvent.BrandId.ToString() }, { "eventCategory", pushNotificationEvent.EventCategory.ToString() }, { "eventType", pushNotificationEvent.EventType.ToString() }, { "appGroupID", pushNotificationEvent.AppGroupId.ToString() }, { "notificationTypeID", pushNotificationEvent.NotificationTypeId.ToString() } };
                    Log.LogDebugMessage("Push notification discarded. Member has been blocked by target.", customData);
                    return;
                }

                var pushNotification = NotificationCreator.CreateNotification(systemEvent);
                var pushNotificationDispatcher = NotificationDispatcherFactory.GetNotificationDispatcher();
                var notificationResponse = pushNotificationDispatcher.SendNotification(pushNotification, brand);
            }
            catch(Exception ex)
            {
                var customData = new Dictionary<string, string> { { "memberId", pushNotificationEvent.MemberId.ToString() }, { "brandId", pushNotificationEvent.BrandId.ToString() }, { "eventCategory", pushNotificationEvent.EventCategory.ToString() }, { "eventType", pushNotificationEvent.EventType.ToString() }, { "appGroupID", pushNotificationEvent.AppGroupId.ToString() }, {"notificationTypeID", pushNotificationEvent.NotificationTypeId.ToString()} };
                Log.LogException("Error in CreateAndSendNotificationFromSystemEvent", ex, customData);
            }
            
        }

        private bool IsSenderBlockedByReceiver(int senderMemberId, int receiverMemberId, Brand brand)
        {
            // you cannot block yourself
            if (senderMemberId == receiverMemberId) return false;

            // visitor, ignore 
            if (senderMemberId == Constants.NULL_INT || receiverMemberId == Constants.NULL_INT) return false;

            try
            {
                // is the feature on?
                var isProfileBlockingEnabled =
                    Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", brand.Site.Community.CommunityID));

                if (!isProfileBlockingEnabled) return false;

                // Are they blocking me?
                return IsRequestedMemberBlocked(brand,
                    senderMemberId,
                    receiverMemberId,
                    new[]
                {
                    HotListCategory.IgnoreList,
                    HotListCategory.ExcludeList,
                    HotListCategory.ExcludeFromList
                });
            }
            catch (Exception ex)
            {
                var customData = new Dictionary<string, string> { { "senderMemberId", senderMemberId.ToString() }, { "brandId", brand.BrandID.ToString() } };
                Log.LogException("Error in IsSenderBlockedByReceiver", ex, customData);
            }

            return false;
        }

        private bool IsRequestedMemberBlocked(Brand brand, 
            int memberId, 
            int targetMemberId,
            IEnumerable<HotListCategory> hotListCategory)
        {
            var list = ListSA.Instance.GetList(targetMemberId);

            return
                hotListCategory.Any(
                    listCategory => list.IsHotListed(listCategory, brand.Site.Community.CommunityID, memberId));
        }
    }
}
