﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Spark.Managers.Managers;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Spark.PushNotifications.Processor.BusinessLogic
{
    public class NotificationUtility : INotificationUtility
    {
        private IMemberAppDeviceService _MemberAppDeviceService;
        private ISettingsSA _settingsService;
        private IBrandConfigSA _brandConfigSA;
        private IGetMember _memberService;

        public IMemberAppDeviceService MemberAppDeviceService
        {
            get
            {
                return _MemberAppDeviceService ?? MemberSA.Instance;
            }
            set
            {
                _MemberAppDeviceService = value;
            }
        }

        public ISettingsSA SettingsService
        {
            get
            {
                return _settingsService ?? RuntimeSettings.Instance;
            }
            set
            {
                _settingsService = value;
            }
        }

        public IBrandConfigSA BrandConfigService
        {
            get
            {
                return _brandConfigSA ?? BrandConfigSA.Instance;
            }
            set
            {
                _brandConfigSA = value;
            }
        }

        public IGetMember MemberService
        {
            get
            {
                return _memberService ?? MemberSA.Instance;
            }
            set
            {
                _memberService = value;
            }
        }

        public bool AreNotificationsEnabledForBrand(int brandId)
        {
            if(brandId <=0)
            {
                throw new ArgumentException("BrandId must be greater than 0");
            }
            var brand = BrandConfigService.GetBrandByID(brandId);
            if(brand==null)
            {
                throw new ArgumentException("Must supply valid brandId");
            }

            var settingsManager = new SettingsManager();
            settingsManager.SettingsService = SettingsService;
            return settingsManager.GetSettingBool(SettingConstants.MOBILE_PUSH_NOTIFICATIONS_ENABLED, brand);
        }

        public bool IsNotificationTypeEnabledForMember(int memberId, PushNotificationTypeID notificationTypeId)
        {
            if (memberId <= 0)
            {
                throw new ArgumentException("MemberId must be greater than 0");
            }
            
            var enabled = false;
            var memberAppDevices = MemberAppDeviceService.GetMemberAppDevices(memberId);
            if (memberAppDevices != null)
            {
                PushNotificationTypeID notificationTypeIdPrimary = notificationTypeId;
                switch (notificationTypeId)
                {
                    case PushNotificationTypeID.ChatRequest:
                    case PushNotificationTypeID.ChatRequestV2:
                    case PushNotificationTypeID.ChatRequestV3:
                        notificationTypeIdPrimary = PushNotificationTypeID.ChatRequest;
                        break;
              
                }
                var notificationTypeValue = (long)Math.Pow(2, (long)notificationTypeIdPrimary);
                enabled = (from ad in memberAppDevices.AppDevices where ((ad.PushNotificationsFlags & notificationTypeValue) == notificationTypeValue) select ad).Count() > 0;
            }
            return enabled;
        }

        public IMember GetMember(int memberId)
        {
            return MemberService.GetIMember(memberId, MemberLoadFlags.None);
        }

    }
}
