﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.Localization;
using System.Collections.Specialized;
using Spark.Common.AccessService;
using Spark.Logger;

namespace Spark.PushNotifications.Processor.BusinessLogic
{
    public class NotificationTextBuilder : INotificationTextBuilder
    {
        #region Properties
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(NotificationTextBuilder));

        private const string SUBPOSTFIX = "_SUB";
        private const string ALLACCESSPOSTFIX = "_ALLACCESS";
        public const string ERROR_UNKNOWN_NOTIFICATION_TYPE = "Unknown notification type";
        private const string MaleLetter = "M";
        private const string FemaleLetter = "F";
        private const int GENDERID_MALE = 1;

        private IResourceProvider _resourceProvider;
        private IPushNotificationMetadataSAService _pushNotificationMetadataService;
        private INotificationUtility _notificationUtility;

        public IResourceProvider ResourceProvider
        {
            get
            {
                return _resourceProvider ?? Spark.Common.Localization.ResourceProvider.Instance;
            }
            set
            {
                _resourceProvider = value;
            }
        }

        public IPushNotificationMetadataSAService PushNotificationMetadataService
        {
            get
            {
                return _pushNotificationMetadataService ?? PushNotificationMetadataSA.Instance;
            }
            set
            {
                _pushNotificationMetadataService = value;
            }
        }

        public INotificationUtility NotificationUtility
        {
            get
            {
                return _notificationUtility ?? new NotificationUtility();
            }
            set
            {
                _notificationUtility = value;
            }
        }

        #endregion  

        public string GetNotificationText(SystemEvent systemEvent)
        {
            var notificationText = string.Empty;
            var pushNotificationEvent  = systemEvent as IPushNotificationEvent;
            if(pushNotificationEvent == null)
            {
                throw new ArgumentException("SystemEvent must implement IPushNotificationEvent");
            }

            var brand = NotificationUtility.BrandConfigService.GetBrandByID(systemEvent.BrandId);

            if (brand == null)
            {
                throw new Exception("Could not find Brand.");
            }

            var notificationAppModel = PushNotificationMetadataService.GetPushNotificationAppGroupModel(pushNotificationEvent.AppGroupId);
            if (notificationAppModel == null)
            {
                throw new Exception("Could not find NotificationAppModel");
            }

            if (string.IsNullOrEmpty(systemEvent.Message))
            {
                switch (pushNotificationEvent.NotificationTypeId)
                {
                    case PushNotificationTypeID.MessageReceived:
                        notificationText = GetEmailReceivedNotificationText(systemEvent as EmailEvent, brand, notificationAppModel);
                        break;
                    case PushNotificationTypeID.ChatRequest:
                    case PushNotificationTypeID.ChatRequestV2:
                    case PushNotificationTypeID.ChatRequestV3:
                        notificationText = GetIMRequestRecievedNotificationText(systemEvent as IMRequestEvent, brand, notificationAppModel);
                        break;
                    case PushNotificationTypeID.MutualYes:
                        notificationText = GetMutualYesNotificationText(systemEvent as MutualYesEvent, brand, notificationAppModel);
                        break;
                    case PushNotificationTypeID.PhotoApproved:
                        notificationText = GetPhotoApprovalNotificationText(systemEvent as PhotoApprovalEvent, brand, notificationAppModel);
                        break;
                    case PushNotificationTypeID.PhotoRejected:
                        notificationText = GetPhotoRejectionNotificationText(systemEvent as PhotoRejectionEvent, brand, notificationAppModel);
                        break;
                    default:
                        throw new Exception(ERROR_UNKNOWN_NOTIFICATION_TYPE);
                }
            }
            else
            {
                //event includes custom message to be used, we will just update placeholders, if any
                notificationText = systemEvent.Message;
            }

            return notificationText;
        }

        private string GetEmailReceivedNotificationText(EmailEvent emailEvent, Brand brand, PushNotificationAppGroupModel notificationAppModel)
        {
            var text = string.Empty;
           
            var notification = (from n in notificationAppModel.NotificationTypes where n.ID == PushNotificationTypeID.MessageReceived select n).FirstOrDefault();
            
            if(notification == null)
            {
                throw new Exception("NotificationAppGroupModel does not contain notification type: " + PushNotificationTypeID.MessageReceived.ToString());
            }

            var targetMember = NotificationUtility.GetMember(emailEvent.GetOperativeMemberId());
            var isTargetMemberPayingMember = targetMember.IsPayingMember(brand.Site.SiteID);
            var resourceKey = isTargetMemberPayingMember ? notification.NotificationTextResourceKey + SUBPOSTFIX : notification.NotificationTextResourceKey;
            var sendingMember = NotificationUtility.GetMember(emailEvent.MemberId);
            resourceKey += GetGenderSpecificM2MPostfix(brand, sendingMember, targetMember);

            if (isTargetMemberPayingMember)
            {
                
                var sendingUserName = sendingMember.GetUserName(brand);
                text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, 
                    ResourceGroupEnum.PushNotifications, resourceKey, new StringDictionary {{ResourceTokenConstants.UserName, sendingUserName}});
            }
            else
            {
                text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo, 
                    ResourceGroupEnum.PushNotifications, resourceKey);
            }

            return text;
        }

        private string GetGenderSpecificPostfix(Brand brand, IMember targetMember)
        {
            string postfix = string.Empty;

            if (
                Convert.ToBoolean(
                    RuntimeSettings.GetSetting(SettingConstants.ENABLE_GENDER_SPECIFIC_TEXT_IN_PUSH_NOTIFICATIOS,
                        brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))
            {
                string targetMemberLetter = IsMaleGender(targetMember, brand) ? MaleLetter : FemaleLetter;
                postfix = "_TO_" + targetMemberLetter;
            }

            return postfix;
        }

        private string GetGenderSpecificM2MPostfix(Brand brand, IMember sendingMember, IMember targetMember)
        {
            string postfix = string.Empty;

            if (
                Convert.ToBoolean(
                    RuntimeSettings.GetSetting(SettingConstants.ENABLE_GENDER_SPECIFIC_TEXT_IN_PUSH_NOTIFICATIOS,
                        brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)))
            {
                string sendingMemberGenderLetter = IsMaleGender(sendingMember, brand) ? MaleLetter : FemaleLetter;
                string targetMemberLetter = IsMaleGender(targetMember, brand) ? MaleLetter : FemaleLetter;
                postfix = "_" + sendingMemberGenderLetter + "_TO_" + targetMemberLetter;
            }

            return postfix;
        }

        private bool IsMaleGender(IMember member, Brand brand)
        {
            bool isMale = false;

            int genderMask = member.GetAttributeInt(brand, "gendermask");
            if (genderMask > 0)
            {
                if ((genderMask & GENDERID_MALE) == GENDERID_MALE)
                    isMale = true;
            }

            return isMale;
        }

        private string GetIMRequestRecievedNotificationText(IMRequestEvent imRequestEvent, Brand brand, PushNotificationAppGroupModel notificationAppModel)
        {
            var text = string.Empty;

            var notification = (from n in notificationAppModel.NotificationTypes where n.ID == imRequestEvent.NotificationTypeId select n).FirstOrDefault();

            if (notification == null)
            {
                throw new Exception("NotificationAppGroupModel does not contain notification type: " + imRequestEvent.NotificationTypeId.ToString());
            }

            var targetMember = NotificationUtility.GetMember(imRequestEvent.GetOperativeMemberId());
            var isTargetMemberPayingMember = targetMember.IsPayingMember(brand.Site.SiteID);

            var member = NotificationUtility.GetMember(imRequestEvent.MemberId);
            var isMemberAllAccess = false;
            if (member.IsPayingMember(brand.Site.SiteID))
            {
                AccessPrivilege ap = member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (ap != null && ap.EndDatePST >= DateTime.Now)
                {
                    isMemberAllAccess = true;
                }
            }

            var resourceKey = notification.NotificationTextResourceKey;
            var sendingMember = NotificationUtility.GetMember(imRequestEvent.MemberId);

            if (isTargetMemberPayingMember || isMemberAllAccess)
            {
                resourceKey = isTargetMemberPayingMember ? resourceKey + SUBPOSTFIX : resourceKey + ALLACCESSPOSTFIX;

                var sendingUserName = sendingMember.GetUserName(brand);

                resourceKey += GetGenderSpecificM2MPostfix(brand, sendingMember, targetMember);

                text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                    ResourceGroupEnum.PushNotifications, resourceKey, new StringDictionary { { ResourceTokenConstants.UserName, sendingUserName } });
            }
            else
            {
                var isSendingMemberPayingMember = targetMember.IsPayingMember(brand.Site.SiteID);
                resourceKey += GetGenderSpecificM2MPostfix(brand, sendingMember, targetMember);
                text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                    ResourceGroupEnum.PushNotifications, resourceKey);
            }

            return text;
        }

        private string GetMutualYesNotificationText(MutualYesEvent mutualYesEvent, Brand brand, PushNotificationAppGroupModel notificationAppModel)
        {
            var text = string.Empty;

            var notification = (from n in notificationAppModel.NotificationTypes where n.ID == PushNotificationTypeID.MutualYes select n).FirstOrDefault();

            if (notification == null)
            {
                throw new Exception("NotificationAppGroupModel does not contain notification type: " + PushNotificationTypeID.MutualYes.ToString());
            }

            var targetMember = NotificationUtility.GetMember(mutualYesEvent.GetOperativeMemberId());
            var isTargetMemberPayingMember = targetMember.IsPayingMember(brand.Site.SiteID);
            var resourceKey = isTargetMemberPayingMember ? notification.NotificationTextResourceKey + SUBPOSTFIX : notification.NotificationTextResourceKey;

            var sendingMember = NotificationUtility.GetMember(mutualYesEvent.MemberId);
            var sendingUserName = sendingMember.GetUserName(brand);

            resourceKey += GetGenderSpecificM2MPostfix(brand, sendingMember, targetMember);

            text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                ResourceGroupEnum.PushNotifications, resourceKey, new StringDictionary { { ResourceTokenConstants.UserName, sendingUserName } });

            if (text == resourceKey)
            {
                string culturename = (brand.Site.CultureInfo != null) ? brand.Site.CultureInfo.Name : "null cultureInfo";
                Log.LogDebugMessage(string.Format("Error in GetResourceValue key: {0}, siteID: {1}, culture name: {2}, error: ", resourceKey, brand.Site.SiteID, culturename), null);
            }

            return text;
        }

        private string GetPhotoApprovalNotificationText(PhotoApprovalEvent photoApprovalEvent, Brand brand, PushNotificationAppGroupModel notificationAppModel)
        {
            var text = string.Empty;

            var notification = (from n in notificationAppModel.NotificationTypes where n.ID == PushNotificationTypeID.PhotoApproved select n).FirstOrDefault();

            if (notification == null)
            {
                throw new Exception("NotificationAppGroupModel does not contain notification type: " + PushNotificationTypeID.PhotoApproved.ToString());
            }

            var resourceKey = notification.NotificationTextResourceKey;

            resourceKey += GetGenderSpecificPostfix(brand, NotificationUtility.GetMember(photoApprovalEvent.GetOperativeMemberId()));

            text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                    ResourceGroupEnum.PushNotifications, resourceKey);

            return text;
        }

        private string GetPhotoRejectionNotificationText(PhotoRejectionEvent photoApprovalEvent, Brand brand, PushNotificationAppGroupModel notificationAppModel)
        {
            var text = string.Empty;

            var notification = (from n in notificationAppModel.NotificationTypes where n.ID == PushNotificationTypeID.PhotoRejected select n).FirstOrDefault();
            
            if (notification == null)
            {
                throw new Exception("NotificationAppGroupModel does not contain notification type: " + PushNotificationTypeID.PhotoRejected.ToString());
            }

            var resourceKey = notification.NotificationTextResourceKey;

            resourceKey += GetGenderSpecificPostfix(brand, NotificationUtility.GetMember(photoApprovalEvent.GetOperativeMemberId()));

            text = ResourceProvider.GetResourceValue(brand.Site.SiteID, brand.Site.CultureInfo,
                    ResourceGroupEnum.PushNotifications, resourceKey);

            return text;
        }
        
    }
}
