﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;

namespace Spark.PushNotifications.Processor.BusinessLogic.Dispatchers
{
    public class NotificationDispatcherFactory : INotificationDispatcherFactory
    {
        public INotificationDispatcher GetNotificationDispatcher()
        {
            return new UrbanAirshipNotificationDispatcher();
        }
    }
}
