﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using Newtonsoft.Json;
using RestSharp;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;
using Spark.PushNotifications.Processor.ValueObjects;
using Spark.Logger;
using Spark.PushNotifications.Processor.BusinessLogic.Helpers;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.PushNotifications.Processor.BusinessLogic.Dispatchers
{
    public class UrbanAirshipNotificationDispatcher : INotificationDispatcher
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(UrbanAirshipNotificationDispatcher));
        
        private string _urbanAirshipBaseUrl = string.Empty;
        private string _urbanAirshipUsername = string.Empty;
        private string _urbanAirshipPassword = string.Empty;
        private bool _logAllResponses = false;

        public UrbanAirshipNotificationDispatcher()
        {
        }

        private void GetSettings(Brand brand)
        {
            _urbanAirshipBaseUrl = RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_BASE_URL, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            _urbanAirshipUsername = RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_USERNAME, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            _urbanAirshipPassword = RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_PASSWORD, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            _logAllResponses = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_ENABLE_RESPONSE_LOGGING, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
        }

        public NotificationResponse SendNotification(PushNotification pushNotification, Brand brand)
        {
            GetSettings(brand);

            if(pushNotification == null)
            {
                throw new ArgumentException("PushNotification can not be null");
            }
            if(pushNotification.MemberId <=0)
            {
                throw new ArgumentException("PushNotification must have valid memberId");
            }
            
            var notificationResponse = new NotificationResponse();
            var content = "";
            try
            {
                var memberAlias = MemberAppDeviceHelper.GenerateMemberIdHash(pushNotification.MemberId);
                var json = GetUANotificationString(memberAlias, pushNotification.NotificationCategoryTag,
                    pushNotification.NotificationText, pushNotification.DeepLink, GetUABadgeString(pushNotification.IncrementBadge));

                var client = new RestClient(_urbanAirshipBaseUrl);
                client.Authenticator = new HttpBasicAuthenticator(_urbanAirshipUsername, _urbanAirshipPassword);

                var request = new RestRequest("push", Method.POST);
                //request.Credentials = new NetworkCredential(_urbanAirshipUsername, _urbanAirshipPassword,);
                //request.AddHeader("Authorization", "Basic <master authorization string>");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/vnd.urbanairship+json; version=3;");
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                
                var response = client.Execute(request);
                content = response.Content; // raw content as string
                var desResponse = JsonConvert.DeserializeObject<dynamic>(content);
                notificationResponse.OperationID = desResponse.operation_id;
                notificationResponse.Success = response.StatusCode == HttpStatusCode.Accepted;
                
                if(!notificationResponse.Success)
                {
                    Log.LogError(string.Format("Failed response from UA: {0}, MemberID: {1}, json request: {2}", content, pushNotification.MemberId, json), null, null);
                }

                if(notificationResponse.Success && _logAllResponses)
                {
                    Log.LogDebugMessage(string.Format("Response from UA: {0}, MemberID: {1}, json request: {2}",
                        content, pushNotification.MemberId, json), null);
                }
            }
            catch (Exception ex)
            {
                Log.LogException("Error sending notification to Urban Airship, raw response from UA: " + content, ex, null);
                throw ex;
            }

            return notificationResponse;
        }


        public static string GetUANotificationString(string memberAlias, string categoryTag,
            string notificationText, string deepLink, string badge)
        {
            //UA Push object
            string formatString = "{"
                + "\"audience\" : {\"AND\" : [{\"alias\" : \"{alias}\" },{\"tag\" : \"{tag}\" } ]},"
                + "\"device_types\" : \"all\", "
                + "\"notification\" : {\"alert\" : \"{alertText}\", \"actions\" : {\"open\" : {\"type\" : \"deep_link\", \"content\" : \"{deepLink}\"}}, \"ios\" : {\"badge\" : \"{badge}\", \"sound\" : \"default\"}}"
                + "}";
            return formatString.Replace("{alias}", memberAlias).Replace("{tag}", categoryTag).Replace("{alertText}", notificationText).Replace("{deepLink}", deepLink).Replace("{badge}", badge);
        }

        public static string GetUABadgeString(int incrementBadge)
        {
            if (incrementBadge < 0)
            {
                return "-" + Math.Abs(incrementBadge).ToString();
            }
            else if (incrementBadge > 0)
            {
                return "+" + incrementBadge.ToString();
            }
            else
            {
                return "auto";
            }

        }

    }
   
}
