﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.PushNotifications.Processor.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "PUSHNOTIFICATIONSPROCESSOR_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Spark.PushNotifications.Processor.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "ProcessorSM";
    }
}
