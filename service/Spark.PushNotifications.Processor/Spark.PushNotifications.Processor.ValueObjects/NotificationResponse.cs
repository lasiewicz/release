﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.PushNotifications.Processor.ValueObjects
{
        public class NotificationResponse
        {
            public bool Success { get; set; }
            public string OperationID { get; set; }
        }
}
