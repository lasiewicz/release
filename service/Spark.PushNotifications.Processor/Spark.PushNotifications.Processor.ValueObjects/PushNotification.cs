﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Spark.PushNotifications.Processor.ValueObjects
{
    public class PushNotification
    {
        public int MemberId { get; set; }
        public string NotificationCategoryTag { get; set; }
        public string NotificationText { get; set; }
        public string DeepLink { get; set; }
        public int IncrementBadge { get; set; }
        public int BrandId { get; set; }
    }
}

