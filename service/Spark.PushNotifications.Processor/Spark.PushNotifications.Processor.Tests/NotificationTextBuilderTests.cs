﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Spark.PushNotifications.Processor.BusinessLogic;
using Spark.PushNotifications.Processor.ValueObjects;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Spark.PushNotifications.Processor.Tests
{
    [TestFixture]
    public class NotificationTextBuilderTests
    {
        [Test]
        public void TestGetNotificationTextEmailEvent()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = TestHelper.GetIResourceProviderMock("testText");
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.MessageReceived;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.MessageReceived;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 1;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Assert.AreEqual("testText", notificationText);

        }

        [Test]
        public void TestGetNotificationTextEmailEventRealResource()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = Spark.Common.Localization.ResourceProvider.Instance;
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.MessageReceived;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.MessageReceived;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 10000;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Console.WriteLine("Email Event Real Resource text: " + notificationText);
            Assert.AreNotEqual("RECEIVED_EMAIL", notificationText);
            Assert.AreNotEqual("RECEIVED_EMAIL", notificationText);

        }

        [Test]
        public void TestGetNotificationTextMutualYesEventRealResource()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = Spark.Common.Localization.ResourceProvider.Instance;
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            MutualYesEvent systemEvent = new MutualYesEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.MutualYes;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.BothSaidYes;
            systemEvent.MemberId = memberID;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Console.WriteLine("Mutual Yes Real Resource text: " + notificationText);
            Assert.AreNotEqual("MUTUAL_YES", notificationText);
            Assert.AreNotEqual("MUTUAL_YES_SUB", notificationText);

        }

        [Test]
        public void TestGetNotificationTextMutualYesEvent()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = TestHelper.GetIResourceProviderMock("testText");
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.MutualYes;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.BothSaidYes;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 1;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Assert.AreEqual("testText", notificationText);

        }

        [Test]
        public void TestGetNotificationTextChatRequestEvent()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = TestHelper.GetIResourceProviderMock("testText");
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.ChatRequest;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.ChatRequest;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 1;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Assert.AreEqual("testText", notificationText);

        }

        [Test]
        public void TestGetNotificationTextPhotoApprovedEvent()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = TestHelper.GetIResourceProviderMock("testText");
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.PhotoApproved;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.PhotoApproved;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 1;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Assert.AreEqual("testText", notificationText);

        }

        [Test]
        public void TestGetNotificationTextPhotoRejectedEvent()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = TestHelper.GetIResourceProviderMock("testText");
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.NotificationTypeId = PushNotificationTypeID.PhotoRejected;
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.PhotoRejected;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 1;
            systemEvent.TargetMemberId = targetMemberID;

            string notificationText = textBuilder.GetNotificationText(systemEvent);
            Assert.AreEqual("testText", notificationText);

        }

        [Test]
        public void TestGetNotificationTextInvalidEvent()
        {
            int siteID = 103;
            int communityID = 3;
            int brandID = 1003;
            int memberID = 411;
            int targetMemberID = 511;

            NotificationTextBuilder textBuilder = new NotificationTextBuilder();
            textBuilder.NotificationUtility = TestHelper.GetNotificationUtilityMock(memberID, targetMemberID, brandID, siteID, communityID);
            textBuilder.ResourceProvider = TestHelper.GetIResourceProviderMock("testText");
            textBuilder.PushNotificationMetadataService = TestHelper.GetIPushNotificationMetadataServiceMock(AppGroupID.JDateAppGroup);

            EmailEvent systemEvent = new EmailEvent();
            systemEvent.AppGroupId = AppGroupID.JDateAppGroup;
            systemEvent.BrandId = brandID;
            systemEvent.ApplicationId = 1;
            systemEvent.EventCategory = SystemEventCategory.MemberGenerated;
            systemEvent.EventType = SystemEventType.MessageReceived;
            systemEvent.MemberId = memberID;
            systemEvent.MessageListID = 1;
            systemEvent.TargetMemberId = targetMemberID;

            try
            {
                string notificationText = textBuilder.GetNotificationText(systemEvent);
                Console.WriteLine("Expected exception did not happen!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Good Expected Exception: " + ex.Message);
                Assert.AreEqual(NotificationTextBuilder.ERROR_UNKNOWN_NOTIFICATION_TYPE, ex.Message);
            }

        }
    }
}
