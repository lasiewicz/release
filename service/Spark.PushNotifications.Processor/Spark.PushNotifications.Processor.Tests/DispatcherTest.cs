﻿using System;
using NUnit.Framework;
using Spark.PushNotifications.Processor.BusinessLogic.Helpers;
using Newtonsoft.Json;
using Spark.PushNotifications.Processor.BusinessLogic.Dispatchers;


namespace Spark.PushNotifications.Processor.Tests
{
    [TestFixture]
    public class DispatcherTest
    {
        [Test]
        public void TestGetUrbanAirshipRequestJSON()
        {
            string alias = "testalias";
            string tag = "testtag";
            string deepLink = @"spark.jdate://testdeeplink";
            string notificationText = "testText";
            string badge = "auto";

            string requestStringJSON = UrbanAirshipNotificationDispatcher.GetUANotificationString(alias, tag, notificationText, deepLink, badge);
            dynamic requestObjectJSON = JsonConvert.DeserializeObject<dynamic>(requestStringJSON);

            Assert.AreEqual(notificationText, requestObjectJSON.notification.alert.ToString());
            Assert.AreEqual("deep_link", requestObjectJSON.notification.actions.open.type.ToString());
            Assert.AreEqual(deepLink, requestObjectJSON.notification.actions.open.content.ToString());
            Assert.AreEqual(alias, requestObjectJSON.audience.AND[0].alias.ToString());
            Assert.AreEqual(tag, requestObjectJSON.audience.AND[1].tag.ToString());
            Assert.AreEqual(badge, requestObjectJSON.notification.ios.badge.ToString());
        }
    }
}
