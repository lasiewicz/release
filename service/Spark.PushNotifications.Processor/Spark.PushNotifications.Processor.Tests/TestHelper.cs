﻿using Matchnet.Configuration.ServiceAdapters.Mocks;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.PushNotifications.Processor.BusinessLogic;
using Spark.PushNotifications.Processor.BusinessLogic.Interfaces;
using Spark.PushNotifications.Processor.Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.PushNotifications.Processor.Tests
{
    public class TestHelper
    {
        public static INotificationUtility GetNotificationUtilityMock(int memberID, int targetMemberID, int brandID, int siteID, int communityID)
        {
            NotificationUtility mock = new NotificationUtility();
            mock.MemberAppDeviceService = GetIMemberAppDeviceServiceMock();
            mock.MemberService = GetIGetMemberMock(memberID, targetMemberID);
            mock.SettingsService = GetISettingsSAMock();
            mock.BrandConfigService = GetIBrandConfigSAMock(brandID, siteID, communityID);

            return mock;
        }

        public static IBrandConfigSAMock GetIBrandConfigSAMock(int brandID, int siteID, int communityID)
        {
            IBrandConfigSAMock mock = new IBrandConfigSAMock();
            mock.SetBrand(GetBrand(brandID, siteID, communityID));

            return mock;
        }

        public static ISettingsSAMock GetISettingsSAMock()
        {
            ISettingsSAMock mock = new ISettingsSAMock();

            return mock;
        }

        public static IResourceProviderMock GetIResourceProviderMock(string text)
        {
            IResourceProviderMock mock = new IResourceProviderMock();
            mock.SetText(text);
            return mock;
        }

        public static IPushNotificationMetadataSAServiceMock GetIPushNotificationMetadataServiceMock(AppGroupID appGroupID)
        {
            IPushNotificationMetadataSAServiceMock mock = new IPushNotificationMetadataSAServiceMock();
            mock.SetNotificationAppGroupModel(GetPushNotificationAppGroupModel(appGroupID));

            return mock;
        }

        public static IMemberAppDeviceServiceMock GetIMemberAppDeviceServiceMock()
        {
            IMemberAppDeviceServiceMock mock = new IMemberAppDeviceServiceMock();

            return mock;
        }

        public static IGetMemberMock GetIGetMemberMock(int memberID, int targetMemberID)
        {
            IGetMemberMock mock = new IGetMemberMock();

            //member
            if (memberID > 0)
            {
                IMemberMock member = new IMemberMock();
                member.MemberID = memberID;
                member.SetIsPayingMember(true);
                member.SetUserName("memberUsername");
                mock.AddIMember(member);
            }

            //target member
            if (targetMemberID > 0)
            {
                IMemberMock member = new IMemberMock();
                member.MemberID = targetMemberID;
                member.SetIsPayingMember(true);
                member.SetUserName("targetMemberUsername");
                mock.AddIMember(member);
            }

            return mock;
        }

        public static PushNotificationAppGroupModel GetPushNotificationAppGroupModel(AppGroupID appGroupId)
        {
            //dummied up for now
            var model = new PushNotificationAppGroupModel();
            model.AppGroupId = appGroupId;
            model.NotificationAppGroupCategories = new List<PushNotificationAppGroupCategory>();
            model.NotificationTypes = new List<PushNotificationType>();

            //activity app category types
            var activityCategory = new PushNotificationAppGroupCategory();
            activityCategory.AppGroupId = appGroupId;
            activityCategory.Category = new PushNotificationCategory();
            activityCategory.Category.Id = PushNotificationCategoryId.Activity;
            activityCategory.NotificationCategoryTag = "JDatePushTypeActivity";
            activityCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            activityCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.MessageReceived, NotificationTextResourceKey = "RECEIVED_EMAIL" }, DeepLink = "spark-jdate://message/{messagelistid}/?sub={issub}" });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.MessageReceived, NotificationTextResourceKey = "RECEIVED_EMAIL" });

            activityCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.ChatRequest }, DeepLink = "spark-jdate://chat/{targetmemberid}/?sub={issub}" });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.ChatRequest, NotificationTextResourceKey = "CHAT_REQUEST" });

            activityCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 3, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.MutualYes }, DeepLink = "spark-jdate://mutualyes/{targetmemberid}/?sub={issub}" });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.MutualYes, NotificationTextResourceKey = "MUTUAL_YES" });

            //photos app category types
            var accountCategory = new PushNotificationAppGroupCategory();
            accountCategory.AppGroupId = appGroupId;
            accountCategory.Category = new PushNotificationCategory();
            accountCategory.Category.Id = PushNotificationCategoryId.Photos;
            accountCategory.NotificationCategoryTag = "JDatePushTypePhotos";
            accountCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            accountCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PhotoApproved }, DeepLink = "spark-jdate://photo/approved/?sub={issub}" });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.PhotoApproved, NotificationTextResourceKey = "PHOTO_APPROVED" });

            accountCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PhotoRejected }, DeepLink = "spark-jdate://photo/declined/?sub={issub}" });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.PhotoRejected, NotificationTextResourceKey = "PHOTO_REJECTED" });

            //promo app category types
            var promotionalCategory = new PushNotificationAppGroupCategory();
            promotionalCategory.AppGroupId = appGroupId;
            promotionalCategory.Category = new PushNotificationCategory();
            promotionalCategory.Category.Id = PushNotificationCategoryId.Promotional;
            promotionalCategory.NotificationCategoryTag = "JDatePushTypePromo";
            promotionalCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            promotionalCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PromotionalAnnouncement } });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.PromotionalAnnouncement, NotificationTextResourceKey = "PROMO_ANNOUNCEMENT" });


            model.NotificationAppGroupCategories.Add(activityCategory);
            model.NotificationAppGroupCategories.Add(accountCategory);
            model.NotificationAppGroupCategories.Add(promotionalCategory);

            return model;
        }

        public static Brand GetBrand(int brandId, int siteId, int communityId)
        {
            var community = new Community(communityId, string.Empty);
            var site = new Site(siteId, community, string.Empty, 1, 1, "en-US", 1, 1, 1, string.Empty, 1, DirectionType.ltr, 1, string.Empty, string.Empty, 1);
            return new Brand(brandId, site, string.Empty, 1, string.Empty, 1, 1, 1);
        }
    }
}
