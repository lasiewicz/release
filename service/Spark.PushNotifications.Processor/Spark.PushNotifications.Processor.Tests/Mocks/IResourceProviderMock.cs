﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spark.Common.Localization;

namespace Spark.PushNotifications.Processor.Tests.Mocks
{
    public class IResourceProviderMock : IResourceProvider
    {
        private string _text;

        public void SetText(string text)
        {
            _text = text;
        }

        public string GetResourceValue(int siteId, ResourceGroupEnum group, System.Globalization.CultureInfo cultureInfo, string key, System.Collections.Specialized.StringDictionary tokenMap)
        {
            return _text;
        }

        public string GetResourceValue(int siteId, System.Globalization.CultureInfo cultureInfo, ResourceGroupEnum group, string key, System.Collections.Specialized.StringDictionary tokenMap)
        {
            return _text;
        }

        public string GetResourceValue(int siteId, System.Globalization.CultureInfo cultureInfo, ResourceGroupEnum group, string key)
        {
            return _text;
        }
    }
}
