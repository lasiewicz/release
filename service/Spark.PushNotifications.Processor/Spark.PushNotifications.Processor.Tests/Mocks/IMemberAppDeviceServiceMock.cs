﻿using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects.MemberAppDevices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.PushNotifications.Processor.Tests.Mocks
{
    public class IMemberAppDeviceServiceMock : IMemberAppDeviceService
    {
        private MemberAppDeviceCollection _appDevices = new MemberAppDeviceCollection();

        public MemberAppDeviceCollection AppDevices
        {
            get { return _appDevices; }
            set { _appDevices = value; }
        }

        public void AddMemberAppDevice(int memberID, int appID, string deviceID, int brandID, string deviceData, long pushNotificationsFlags)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Member.ValueObjects.MemberAppDevices.MemberAppDeviceCollection GetMemberAppDevices(int memberID)
        {
            throw new NotImplementedException();
        }

        public void RemoveMemberAppDevice(int memberID, int appID, string deviceID)
        {
            throw new NotImplementedException();
        }

        public void UpdateMemberAppDeviceNotificationsFlags(int memberID, int appID, string deviceID, long pushNotificationsFlags)
        {
            throw new NotImplementedException();
        }

        public void DeleteMemberAppDevice(int memberID, int appID, string deviceID)
        {
            throw new NotImplementedException();
        }
    }
}
