﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;

namespace Spark.PushNotifications.Processor.Tests.Mocks
{
    public class IGetMemberMock : IGetMember
    {
        private Dictionary<int, IMember> _Members = new Dictionary<int, IMember>();

        public void AddIMember(IMember member)
        {
            if (member != null)
            {
                _Members[member.MemberID] = member;
            }
        }

        public IMember GetIMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            return _Members[memberID];
        }

        public Member GetMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            throw new NotImplementedException();
        }

        public IMemberDTO GetMemberDTO(int memberID, Matchnet.Member.ValueObjects.MemberDTO.MemberType type)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetMemberDTOs(ArrayList memberIDs, Matchnet.Member.ValueObjects.MemberDTO.MemberType type)
        {
            throw new NotImplementedException();
        }

        public int GetMemberIDByEmail(string emailAddress)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetMembers(ArrayList memberIDs, MemberLoadFlags memberLoadFlags)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Member.ValueObjects.Admin.ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            throw new NotImplementedException();
        }

        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            throw new NotImplementedException();
        }

    }
}
