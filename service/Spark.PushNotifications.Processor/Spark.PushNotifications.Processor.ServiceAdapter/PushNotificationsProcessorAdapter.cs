﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.RabbitMQ.Client;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;

namespace Spark.PushNotifications.Processor.ServiceAdapter
{
    /// <summary>
    /// Adapter class for enqueuing push notifications into RabbitMQ for the processor.
    /// Possibly extend to include calls directly to processor service if needed.
    /// </summary>
    public class PushNotificationsProcessorAdapter
    {
        private static Publisher _publisher;

        private PushNotificationsProcessorAdapter()
        {
            var rabbitMQUserName = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_USER_ACCOUNT);
            var rabbitMQPassword = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_PASSWORD);
            var rabbitMQServer = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.PUSHNOTIFICATIONS_PROCESSOR_RABBITMQ_SERVER);
            _publisher = new Publisher(rabbitMQServer, rabbitMQUserName, rabbitMQPassword);

        }
        public static readonly PushNotificationsProcessorAdapter Instance = new PushNotificationsProcessorAdapter();

        public void SendNotificationEvent(IPushNotificationEvent notificationEvent)
        {
            _publisher.Publish(notificationEvent);
        }

        public void SendSystemEvent(SystemEvent systemEvent)
        {
            _publisher.Publish(systemEvent);
        }

    }
}
