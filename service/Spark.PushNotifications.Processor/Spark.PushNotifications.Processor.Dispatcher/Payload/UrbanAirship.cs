﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Spark.PushNotifications.Processor.Dispatcher.Payload
{
    [JsonObject(MemberSerialization.OptIn)]
    internal class UrbanAirship
    {
        [JsonProperty (PropertyName = "audience")]
        public Dictionary<string, string> Audience { get; set; }

        [JsonProperty(PropertyName = "notification")]
        public Dictionary<string, string> Notification { get; set; }

        [JsonProperty (PropertyName = "device_types")]
        public string DeviceTypes { get; set; }

        

        public UrbanAirship(string apid, string alert)
        {
            Audience = new Dictionary<string, string>();
            Notification = new Dictionary<string, string>();
            Audience.Add("apid", apid);
            Notification.Add("alert", alert);
            DeviceTypes = "all";
        }
    }
}

