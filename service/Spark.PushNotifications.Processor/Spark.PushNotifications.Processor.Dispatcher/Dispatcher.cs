﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Newtonsoft.Json;
using RestSharp;
using Spark.PushNotifications.Processor.Dispatcher.Payload;
using Matchnet.Configuration.ValueObjects;
using Spark.PushNotifications.Processor.ValueObjects;

namespace Spark.PushNotifications.Processor.Dispatcher
{
    public class Dispatcher
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(Dispatcher));

        public readonly static Dispatcher Instance = new Dispatcher();

        private string _urbanAirshipBaseUrl = string.Empty;
        private string _urbanAirshipUsername = string.Empty;
        private string _urbanAirshipPassword = string.Empty;

        private Dispatcher()
        {
            GetSettings();
        }

        private void GetSettings()
        {
            _urbanAirshipBaseUrl = RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_BASE_URL);
            _urbanAirshipUsername = RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_USERNAME);
            _urbanAirshipPassword = RuntimeSettings.GetSetting(SettingConstants.URBAN_AIRSHIP_PASSWORD);
        }

        public NotificationResponse SendNotification(string apid, string alert)
        {
            var notificationResponse = new NotificationResponse();

            try
            {
                var request = new RestRequest("push", Method.POST);
                var client = new RestClient(_urbanAirshipBaseUrl);
                request.Credentials = new NetworkCredential(_urbanAirshipUsername, _urbanAirshipPassword);
                request.AddHeader("Authorization", "Basic <master authorization string>");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/vnd.urbanairship+json; version=3;");
                string json =
                    JsonConvert.SerializeObject(new UrbanAirship(apid, alert),
                        Formatting.None);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                var response = client.Execute(request);
                var content = response.Content; // raw content as string
                var desResponse = JsonConvert.DeserializeObject<dynamic>(content);
                notificationResponse.OperationID = desResponse.operation_id;
                notificationResponse.StatusCode = response.StatusCode.ToString();
            }
            catch (Exception ex)
            {
                _Logger.Error(string.Format(
                        "Error in PushNotificationsProcessor.SendNotification MemberId: {0} TargetMemberId: {1} BrandId: {2}, apid: {3}",
                        "MemberId", "TargetMemberId", "BrandId", apid), ex);

                throw ex;
            }

            return notificationResponse;
        }
    }
}
