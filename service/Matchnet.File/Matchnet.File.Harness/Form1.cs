using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.DirectoryServices;

using Matchnet.File.ServiceAdapters;
using Matchnet.PhotoFile.ServiceAdapters;


namespace Matchnet.File.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(88, 8);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			DirectoryEntry iis = new DirectoryEntry("IIS://localhost/W3SVC");
			foreach (DirectoryEntry child in iis.Children)
			{
				if (child.SchemaClassName == "IIsWebServer")
				{
					System.Diagnostics.Trace.WriteLine("__" + child.Path);


					Console.WriteLine(new DirectoryEntry(child.Path + "/ROOT").Properties["Path"][0]);

					/*
					foreach (DirectoryEntry c2 in child.Children)
					{
						System.Diagnostics.Trace.WriteLine("__	" + c2.Name);
					}
					System.Diagnostics.Trace.WriteLine("__	" + child.Children.Find("ROOT").ToString());
					*/
				}
			}


			/*
			System.Drawing.Image image = System.Drawing.Image.FromFile(@"C:\Documents and Settings\gpeterson\Desktop\Pics\Bobby.jpg");
			MemoryStream stream = new MemoryStream();
			image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
			FileSA.Instance.Save(@"\\BHDEVWEB01\FileRootC\Bobby.jpg",stream.ToArray());
			*/		
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			string filePath = @"\\FILE02\FileRootD\2003\02\27\16777811.jpg";
			string[] pathParts = filePath.Split('\\');

			Int32 year;
			Int32 month;
			Int32 day;
			Int32 hour = 0;

			if (pathParts[pathParts.Length - 5].Length == 4)
			{
				year = Convert.ToInt32(pathParts[pathParts.Length - 5]);
				month = Convert.ToInt32(pathParts[pathParts.Length - 4]);
				day = Convert.ToInt32(pathParts[pathParts.Length - 3]);
				hour = Convert.ToInt32(pathParts[pathParts.Length - 2]);
			}
			else if (pathParts[pathParts.Length - 4].Length == 4)
			{
				year = Convert.ToInt32(pathParts[pathParts.Length - 4]);
				month = Convert.ToInt32(pathParts[pathParts.Length - 3]);
				day = Convert.ToInt32(pathParts[pathParts.Length - 2]);
			}
			else
			{
				throw new Exception("Unable to determine file insertDate (filePath: " + filePath + ").");
			}

			DateTime insertDate = new DateTime(year, 
				month,
				day,
				hour,
				0,
				0);
	
			Console.WriteLine(insertDate);
		}
	}
}
