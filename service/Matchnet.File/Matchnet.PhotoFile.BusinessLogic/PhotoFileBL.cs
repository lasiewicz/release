#region

using System;
using System.Collections;
using System.DirectoryServices;
using System.IO;
using System.Messaging;
using System.Threading;
using System.Web;
using Amazon.CloudFront;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.PhotoFile.ValueObjects;
using Spark.Logging;
using Spark.CloudStorage;

#endregion

namespace Matchnet.PhotoFile.BusinessLogic
{
    /// <summary>
    /// Note: This class should be kept in sync or later removed.  The actual PhotoFileBL being used is in the Matchnet.PhotoFile.Web solution.
    /// </summary>
    public class PhotoFileBL
    {
        private const string QUEUE_PATH = @".\private$\FileReplication";

        private bool _runnable;
        private Hashtable _queueManagers;
        private ReaderWriterLock _queueManagersLock;
        private string _dsPath = null;
        private const string CLASS_NAME = "PhotoFileBL";

        public PhotoFileBL()
        {
            System.Diagnostics.Trace.WriteLine("__PhotoFileBL()");
            _queueManagersLock = new ReaderWriterLock();
            _queueManagers = new Hashtable();

            string siteRoot = HttpContext.Current.Server.MapPath("/");
            if (siteRoot.EndsWith("\\"))
            {
                siteRoot = siteRoot.Substring(0, siteRoot.Length - 1);
            }
            siteRoot = siteRoot.ToLower();

            System.Diagnostics.Trace.WriteLine("__siteRoot: " + siteRoot);
            DirectoryEntry iis = new DirectoryEntry("IIS://localhost/w3svc");

            foreach (DirectoryEntry child in iis.Children)
            {
                System.Diagnostics.Trace.WriteLine("__" + child.Path + ", " + child.SchemaClassName.ToString());
                if (child.SchemaClassName == "IIsWebServer")
                {
                    string currentPath = new DirectoryEntry(child.Path + "/ROOT").Properties["Path"][0].ToString().ToLower();
                    if (currentPath.EndsWith("\\"))
                    {
                        currentPath = currentPath.Substring(0, currentPath.Length - 1);
                    }
                    System.Diagnostics.Trace.WriteLine("__: " + currentPath + ", " + siteRoot);
                    if (currentPath == siteRoot)
                    {
                        System.Diagnostics.Trace.WriteLine("__MATCH");
                        _dsPath = child.Path;
                    }
                }
            }

            if (_dsPath == null)
            {
                throw new Exception("Unable to find site root in directory services.");
            }

            System.Diagnostics.Trace.WriteLine("__dsPath: " + _dsPath);
        }


        public string DSPath
        {
            set
            {
                _dsPath = value;
            }
        }


        public void Start()
        {
            TraceLog("Starting");
            _runnable = true;

            MessageQueue[] queues = MessageQueue.GetPrivateQueuesByMachine("localhost");

            _queueManagersLock.AcquireWriterLock(-1);
            try
            {
                for (Int32 queueNum = 0; queueNum < queues.Length; queueNum++)
                {
                    string path = queues[queueNum].Path;
                    if (path.IndexOf("private$\\filereplication_") > -1)
                    {
                        System.Diagnostics.Trace.WriteLine("__q: " + path);
                        QueueManager queueManager = new QueueManager(path);
                        _queueManagers.Add(path, queueManager);
                        queueManager.Start();
                    }
                }
            }
            finally
            {
                _queueManagersLock.ReleaseLock();
            }
        }


        public void Stop()
        {
            _runnable = false;

            _queueManagersLock.AcquireReaderLock(-1);
            try
            {
                IDictionaryEnumerator de = _queueManagers.GetEnumerator();
                while (de.MoveNext())
                {
                    QueueManager queueManager = de.Value as QueueManager;
                    queueManager.SignalStop();
                }

                de.Reset();
                while (de.MoveNext())
                {
                    QueueManager queueManager = de.Value as QueueManager;
                    queueManager.Stop();
                }
            }
            finally
            {
                _queueManagersLock.ReleaseLock();
            }
        }

        public void Save(Int32 fileRootID,
            string relativefilePath,
            byte[] fileData,
            string key)

        {
            // for backward compatibility only, should be removed
            Save(fileRootID, relativefilePath, string.Empty, fileData, key, Constants.NULL_INT, Constants.NULL_INT);
        }

        /// <summary>
        /// Saves original photo to the cloud
        /// </summary>
        public bool SaveOriginalFile(byte[] fileData, string relativeFileCloudPath, string key, int communityID, int siteID)
        {
            return SaveWithNoLocalCopy(fileData, relativeFileCloudPath, key, communityID, siteID,
                                       Matchnet.File.ValueObjects.FileType.MemberProfilePhotoOriginal);
        }

        /// <summary>
        /// Saves photo to local servers and to the cloud
        /// </summary>
        public bool Save(Int32 fileRootID,
            string relativefilePath,
            string relativefileCloudPath,
            byte[] fileData,
            string key,
            int communityID,
            int siteID)
        {
            auth(key);

            bool success = true;

            MessageQueueTransaction tran = new MessageQueueTransaction();
            tran.Begin();
            FileStream stream = null;
            string uncRelativePath;
            string traceMessage = string.Empty;

            System.Diagnostics.Trace.WriteLine("__Save() " + relativefilePath);

            string filePath = getUNCPath(relativefilePath,
                out uncRelativePath);
            traceMessage = string.Format("Enter Save() FileRootID: {0} FilePath: {1}", fileRootID.ToString(), filePath);
            TraceLog(traceMessage);

            try
            {
                string[] paths = FileRootSA.Instance.GetRootGroups().GetInstancePaths(fileRootID);

                for (Int32 pathNum = 0; pathNum < paths.Length; pathNum++)
                {
                    if (filePath.ToLower().IndexOf(paths[pathNum].ToLower()) == -1)
                    {
                        MessageQueue queue = GetQueue(paths[pathNum]);
                        traceMessage = string.Format("Save() Replicating File: {0}", paths[pathNum] + uncRelativePath);
                        TraceLog(traceMessage);
                        System.Diagnostics.Trace.WriteLine("__repl: " + paths[pathNum] + uncRelativePath);
                        CopyPointer pointer = new CopyPointer(filePath, paths[pathNum] + uncRelativePath);
                        queue.Send(pointer, tran);
                    }
                }

                FileInfo info = new FileInfo(filePath);
                Directory.CreateDirectory(info.Directory.ToString());
                stream = new FileStream(filePath, FileMode.Create);
                stream.Write(fileData, 0, fileData.Length);
                stream.Close();

                traceMessage = string.Format("Save() Physical File Initial Save. File: {0} Size {1}", filePath, fileData.Length.ToString());
                TraceLog(traceMessage);

                bool fileExists = System.IO.File.Exists(filePath);
                traceMessage = string.Format("Save() File Verification Result. File: {0} Exists: {1} Size: {2}", filePath, fileExists.ToString(), info.Length.ToString());
                TraceLog(traceMessage);


                bool cloudStorageEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_CLOUD_STORAGE_FOR_MEMBER_PHOTOS", communityID, siteID));

                if (cloudStorageEnabled && relativefileCloudPath != string.Empty)
                {
                    Client cloudClient = new Client(communityID, siteID, RuntimeSettings.Instance);
                    string correctedRelativeFilePath = relativefileCloudPath.Substring(0, relativefileCloudPath.LastIndexOf("/") + 1);
                    OperationResult operationResult = cloudClient.UploadFile(filePath, correctedRelativeFilePath, FileType.MemberPhoto);
                    if(operationResult != OperationResult.S3AndCloudFrontSuccess)
                    {
                        success = false;
                    }
                }

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Abort();
                success = false;
                RollingFileLogger.Instance.LogException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, ex);
                //ServiceBoundaryException boundaryEx = new ServiceBoundaryException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "Error uploading photo", ex);
                throw new BLException("Error saving file (filePath: " + filePath + ").", ex);
            }

            return success;
        }

        /// <summary>
        /// This saves the photo to the cloud based on the cloud file type; does not save photo to local servers
        /// </summary>
        public bool SaveWithNoLocalCopy(byte[] fileData, string relativeFileCloudPath, string key, int communityID, int siteID, Matchnet.File.ValueObjects.FileType fileType)
        {
            auth(key);

            bool success = true;
            string filePath = string.Empty;
            bool fileWritten = false;
            FileType cloudFileType = FileType.None;

            System.Diagnostics.Trace.WriteLine(String.Format("__SaveWithNoLocalCopy() cloudPath: {0}, fileType: {1}, siteID: {2}", relativeFileCloudPath, fileType, siteID));

            //convert file type to cloud file type
            switch (fileType)
            {
                case File.ValueObjects.FileType.MemberProfilePhoto:
                    cloudFileType = FileType.MemberPhoto;
                    break;
                case File.ValueObjects.FileType.MemberProfilePhotoOriginal:
                    cloudFileType = FileType.MemberOriginalPhoto;
                    break;
                case File.ValueObjects.FileType.MessageAttachmentPhoto:
                    cloudFileType = FileType.MessageAttachment;
                    break;
                default:
                    return false;
            }

            try
            {
                bool cloudStorageEnabled = true;
                if (cloudFileType == FileType.MemberPhoto)
                {
                    cloudStorageEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_CLOUD_STORAGE_FOR_MEMBER_PHOTOS", communityID,
                                                                 siteID));
                }

                if (cloudStorageEnabled && relativeFileCloudPath != string.Empty)
                {
                    string fileName = relativeFileCloudPath.Substring(relativeFileCloudPath.LastIndexOf('/') + 1);

                    //save file data to temporary location
                    //note: this temp directory was used originally only for "original" photos thus the setting was named this way
                    //but it is just a temp directory and will be used for all photos saved only to the cloud
                    filePath = RuntimeSettings.GetSetting("TEMP_ORIGINAL_PHOTOS_STORAGE_DIRECTORY") + fileName;

                    FileInfo info = new FileInfo(filePath);
                    Directory.CreateDirectory(info.Directory.ToString());
                    FileStream stream = new FileStream(filePath, FileMode.Create);
                    stream.Write(fileData, 0, fileData.Length);
                    stream.Close();
                    fileWritten = true;

                    //save file to the cloud (which will pick up the file from the temporary location)
                    Client cloudClient = new Client(communityID, siteID, RuntimeSettings.Instance, cloudFileType);
                    string correctedRelativeFilePath = relativeFileCloudPath.Substring(0,
                                                                                       relativeFileCloudPath.LastIndexOf
                                                                                           ("/") + 1);
                    OperationResult operationResult = cloudClient.UploadFile(filePath, correctedRelativeFilePath,
                                                                             cloudFileType);
                    if (operationResult != OperationResult.S3AndCloudFrontSuccess)
                    {
                        success = false;
                    }
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                success = false;
                RollingFileLogger.Instance.LogException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, ex);
                throw new BLException("PhotoFileBL.SaveWithNoLocalCopy(): Error saving file (fileCloudPath: " + relativeFileCloudPath + ").", ex);
            }
            finally
            {
                if (!string.IsNullOrEmpty(filePath) && fileWritten)
                {
                    FileInfo info = new FileInfo(filePath);
                    if (info.Exists) info.Delete();
                }
            }

            return success;
        }


        public void Delete(Int32 fileRootID,
            string relativefilePath,
            string key)
        {
            Delete(fileRootID, relativefilePath, key, Constants.NULL_INT, Constants.NULL_INT);
        }


        public void Delete(Int32 fileRootID,
            string relativefilePath,
            string key,
            int communityID,
            int siteID)
        {
            auth(key);

            MessageQueueTransaction tran = new MessageQueueTransaction();
            tran.Begin();
            string uncRelativePath;

            string filePath = getUNCPath(relativefilePath,
                out uncRelativePath);
            string traceMessage = string.Format("Enter Delete() FileRootID: {0} FilePath: {1}", fileRootID.ToString(), uncRelativePath);
            TraceLog(traceMessage);

            try
            {
                string[] paths = FileRootSA.Instance.GetRootGroups().GetInstancePaths(fileRootID);
                for (Int32 pathNum = 0; pathNum < paths.Length; pathNum++)
                {
                    MessageQueue queue = GetQueue(paths[pathNum]);
                    System.Diagnostics.Trace.WriteLine("__deletePointer: " + paths[pathNum] + uncRelativePath);

                    DeletePointer pointer = new DeletePointer(paths[pathNum] + uncRelativePath);
                    traceMessage = string.Format("Delete() Replicating File: {0}", paths[pathNum] + uncRelativePath);
                    queue.Send(pointer, tran);
                }

                bool cloudStorageEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_CLOUD_STORAGE_FOR_MEMBER_PHOTOS", communityID, siteID));

                if (cloudStorageEnabled && communityID != Constants.NULL_INT)
                {
                    string correctedRelativeFilePath = (relativefilePath.StartsWith("/"))
                                                               ? relativefilePath.Substring(1)
                                                               : relativefilePath;

                    try
                    {
                    Client cloudClient = new Client(communityID, siteID, RuntimeSettings.Instance);
                    correctedRelativeFilePath = correctedRelativeFilePath.Substring(correctedRelativeFilePath.IndexOf("/") + 1);
                        TraceLog(string.Format("START Photo Delete [filePath:'{0}', key:'{1}', type:{2}, community:{3}, site:{4}]",
                                relativefilePath, correctedRelativeFilePath, FileType.MemberPhoto.ToString(),
                                communityID, siteID));
                    OperationResult operationResult = cloudClient.DeleteFile(correctedRelativeFilePath, FileType.MemberPhoto);
                        TraceLog(string.Format("END Photo Delete [filePath:'{0}', key:'{1}', type:{2}, community:{3}, site:{4}, result:{5}]",
                                relativefilePath, correctedRelativeFilePath, FileType.MemberPhoto.ToString(),
                                communityID, siteID, operationResult.ToString()));
                    }
                    catch(Exception ex)
                    {
                        OperationResult result = (ex is AmazonCloudFrontException)
                                                     ? OperationResult.S3SuccessCloudFrontFailure
                                                     : OperationResult.S3AndCloudFrontFailure;
                        LogException(ex, string.Format("ERROR Photo Delete [filePath:'{0}', key:'{1}', type:{2}, community:{3}, site:{4}, result:{5}]",
                                         relativefilePath, correctedRelativeFilePath, FileType.MemberPhoto.ToString(),
                                         communityID, siteID, result.ToString()));
                    }

                }

                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Abort();
                throw new BLException("Error deleting file (fileRootID: " + fileRootID.ToString() + ", filePath: " + filePath + ").", ex);
            }
        }

        public void Hide(string relativeFilePath,
            string relativeCloudFilePath,
            string key,
            int communityID,
            int siteID)
        {
            var cloudStorageEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_CLOUD_STORAGE_FOR_MEMBER_PHOTOS", communityID, siteID));

            if (cloudStorageEnabled)
            {
                string uncRelativePath;

                string filePath = getUNCPath(relativeFilePath, out uncRelativePath);
                Client cloudClient = new Client(communityID, siteID, RuntimeSettings.Instance);
                string correctedRelativeFilePath = relativeFilePath.Substring(0, relativeFilePath.LastIndexOf("/") + 1);
                int x = 1;
            }
        }

        public void UnHide(string relativeFilePath,
            string relativeCloudFilePath,
            string key,
            int communityID,
            int siteID)
        {
            int x = 1;
        }

        private void auth(string key)
        {
            if (key != Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"))
            {
                throw new Exception("Auth keys do not match.");
            }
        }


        private string cleanPath(string path)
        {
            path = path.ToLower();
            if (path.LastIndexOf("\\") != path.Length - 1)
            {
                path = path + "\\";
            }

            return path;
        }


        private string getUNCPath(string relativePath,
            out string uncRelativePath)
        {
            relativePath = stripURLFromRelativePath(relativePath);
            
            string result = null;
            if (relativePath.IndexOf("/") == 0)
            {
                relativePath = relativePath.Substring(1);
            }

            string partition = relativePath.Split('/')[0];
            string dsPath = _dsPath + "/root/" + partition;

            DirectoryEntry vDir = new DirectoryEntry(dsPath);

            result = vDir.Properties["path"].Value.ToString();
            if (!result.EndsWith("\\"))
            {
                result = result + "\\";
            }
            uncRelativePath = relativePath.Substring(partition.Length + 1).Replace("/", "\\");
            result = result + uncRelativePath;

            System.Diagnostics.Trace.WriteLine("__p: " + result);

            if (result == null)
            {
                throw new Exception("Unable to get vdir path for " + relativePath + " (" + dsPath + ").");
            }
            // Making sure that result is in UNC form, In case of a vdir that's set to a local drive this would throw an exception -  Added by RB  20070423

            Uri uriResult = new Uri(result);
            if (!uriResult.IsUnc)
            {
                throw new Exception("Could not obtain a UNC path for " + relativePath + " (" + dsPath + ").\tPlease check the vdir setting.");
            }

            return result;
        }

        private string stripURLFromRelativePath(string pathWithURL)
        {
            var comIndex = pathWithURL.IndexOf(".com");

            if (comIndex < 0) return pathWithURL;
            return pathWithURL.Substring(comIndex + 4, pathWithURL.Length - (comIndex + 4));
        }

        public MessageQueue GetQueue(string path)
        {
            MessageQueue queue;
            path = QUEUE_PATH + path.ToLower().Replace("\\", "_");

            _queueManagersLock.AcquireReaderLock(-1);
            try
            {
                if (_queueManagers.ContainsKey(path))
                {
                    queue = new MessageQueue(path);
                }
                else
                {
                    _queueManagersLock.UpgradeToWriterLock(-1);

                    if (_queueManagers.ContainsKey(path))
                    {
                        queue = new MessageQueue(path);
                    }
                    else
                    {
                        if (!MessageQueue.Exists(path))
                        {
                            queue = MessageQueue.Create(path, true);
                        }
                        else
                        {
                            queue = new MessageQueue(path);
                        }
                        QueueManager queueManager = new QueueManager(path);
                        _queueManagers.Add(path, queueManager);
                        queueManager.Start();
                    }
                }

                queue.Formatter = new BinaryMessageFormatter();
                return queue;
            }
            finally
            {
                _queueManagersLock.ReleaseLock();
            }
        }

        private void LogException(Exception ex, string message)
        {
            RollingFileLogger.Instance.LogException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, ex, message);
        }

        private void TraceLog(string message)
        {
            RollingFileLogger.Instance.LogInfoMessage(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, message, null);
        }

        public bool SaveAdmintoolPhoto(byte[] fileData, string relativeFileCloudPath, string key)
        {
            auth(key);

            bool success = true;
            string filePath = string.Empty;
            bool fileWritten = false;
            string accessKey = RuntimeSettings.GetSetting("AWS_ADMINTOOL_ACCESSKEY");
            string secretKey = RuntimeSettings.GetSetting("AWS_ADMINTOOL_SECRETKEY");
            string bucketName = RuntimeSettings.GetSetting("AWS_ADMINTOOL_PHOTO_BUCKET_NAME");
            string cloudUrl = RuntimeSettings.GetSetting("AWS_ROOT_CLOUD_URL");
            bool appendBucketName = true;

            System.Diagnostics.Trace.WriteLine("__SaveAdmintoolPhoto() " + relativeFileCloudPath);

            try
            {
                string fileName = relativeFileCloudPath.Substring(relativeFileCloudPath.LastIndexOf('/') + 1);
                filePath = RuntimeSettings.GetSetting("TEMP_ADMINTOOL_PHOTOS_STORAGE_DIRECTORY") + fileName;

                FileInfo info = new FileInfo(filePath);
                Directory.CreateDirectory(info.Directory.ToString());
                FileStream stream = new FileStream(filePath, FileMode.Create);
                stream.Write(fileData, 0, fileData.Length);
                stream.Close();
                fileWritten = true;

                BasicClient cloudClient = new BasicClient(accessKey, secretKey, bucketName, cloudUrl, appendBucketName);
                string correctedRelativeFilePath = relativeFileCloudPath.Substring(0, relativeFileCloudPath.LastIndexOf("/") + 1);
                var operationResult = cloudClient.UploadFile(filePath, correctedRelativeFilePath);
                if (operationResult == BasicOperationResult.Failure)
                {
                    success = false;
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(Matchnet.PhotoFile.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, ex);
                throw new BLException("Error saving admintool photo (fileCloudPath: " + relativeFileCloudPath + ").", ex);
            }
            finally
            {
                if (!string.IsNullOrEmpty(filePath) && fileWritten)
                {
                    FileInfo info = new FileInfo(filePath);
                    if (info.Exists) info.Delete();
                }
            }

            return success;
        }
    }
}
