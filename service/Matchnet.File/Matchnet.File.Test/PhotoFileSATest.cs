﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Matchnet.PhotoFile.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.File.Test
{
    [TestClass]
    public class PhotoFileSATest
    {
        [TestMethod]
        public void SaveAdmintoolPhotoTest()
        {
            var imgBytes = System.IO.File.ReadAllBytes("c:\\ichigo.jpg");
            var saveResult = PhotoFileSA.Instance.SaveAdmintoolPhoto(imgBytes, "/2013/08/21/12/120209577.jpg");

            Assert.IsTrue(saveResult.Success);
        }
    }
}
