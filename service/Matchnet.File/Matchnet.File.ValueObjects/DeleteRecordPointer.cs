using System;

namespace Matchnet.File.ValueObjects
{
	[Serializable]
	public class DeleteRecordPointer
	{
		private Int32 _fileID;

		public DeleteRecordPointer(Int32 fileID)
		{
			_fileID = fileID;
		}


		public Int32 FileID
		{
			get
			{
				return _fileID;
			}
		}
	}
}
