﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.File.ValueObjects
{
    public enum FileType
    {
        None = 0,
        MemberProfilePhoto = 1,
        MemberProfilePhotoOriginal = 2,
        MessageAttachmentPhoto = 3
    }
}
