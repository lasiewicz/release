using System;
using System.IO;


namespace Matchnet.File.ValueObjects.ServiceDefinitions
{
	public interface IFileService
	{
		void GetFilePath(Int32 fileID,
			out string filePath,
			out string fileWebPath);

		FileInstance[] GetFileInstances(Int32 fileID);

		void InsertFile(Int32 fileID,
			Int32 rootID,
			string fileExtension,
			DateTime insertDate);
		
		void DeleteFile(Int32 fileID);

		void UpdateFile(Int32 fileID, long fileSize);
	}
}
