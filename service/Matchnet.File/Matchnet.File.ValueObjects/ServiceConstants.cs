using System;

namespace Matchnet.File.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "FILE_SVC";
		public const string SERVICE_NAME = "Matchnet.File.Service";
		public const string SERVICE_MANAGER_NAME = "FileSM";

		private ServiceConstants()
		{
		}
	}
}
