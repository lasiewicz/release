﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.File.ValueObjects
{
    [Serializable]
    public class CreateNewFileResult
    {
        public string WebPath { get; set; }
        public string CloudPath { get; set; }
        public int FileRootID { get; set; }
        public int FileID { get; set; }
        public bool Success { get; set; }
        public int OriginalFileSize { get; set; }
        public string OriginalFileCloudPath { get; set; }

        public CreateNewFileResult()
        {
        }
    }
}
