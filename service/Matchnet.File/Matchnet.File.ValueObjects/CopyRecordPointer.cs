using System;
using System.IO;


namespace Matchnet.File.ValueObjects
{
	[Serializable]
	public class CopyRecordPointer
	{
		private Int32 _fileID;
		private	Int32 _rootID;
		private string _extension;
		private DateTime _insertDate;

		public CopyRecordPointer(string filePath,
			Int32 rootID)
		{
			_fileID = Convert.ToInt32(Path.GetFileNameWithoutExtension(filePath));
			_rootID = rootID;
			_extension = Path.GetExtension(filePath);
			string[] pathParts = filePath.Split('\\');

			Int32 year;
			Int32 month;
			Int32 day;
			Int32 hour = 0;

			System.Diagnostics.Trace.WriteLine("__filePath: " + filePath + ", " + pathParts.Length.ToString());

			if (pathParts[pathParts.Length - 4].Length == 4)
			{
				year = Convert.ToInt32(pathParts[pathParts.Length - 4]);
				month = Convert.ToInt32(pathParts[pathParts.Length - 3]);
				day = Convert.ToInt32(pathParts[pathParts.Length - 2]);
			}
			else if (pathParts[pathParts.Length - 5].Length == 4)
			{
				year = Convert.ToInt32(pathParts[pathParts.Length - 5]);
				month = Convert.ToInt32(pathParts[pathParts.Length - 4]);
				day = Convert.ToInt32(pathParts[pathParts.Length - 3]);
				hour = Convert.ToInt32(pathParts[pathParts.Length - 2]);
			}
			else
			{
				throw new Exception("Unable to determine file insertDate (filePath: " + filePath + ").");
			}

			System.Diagnostics.Trace.WriteLine("__filePath: " + filePath + ", year: " + year.ToString() + ", month: " + month.ToString() + ", day: " + day.ToString() + ", hour: " + hour.ToString());

			try
			{
				_insertDate = new DateTime(year, 
					month,
					day,
					hour,
					0,
					0);			
			}
			catch (Exception ex)
			{
				throw new Exception("Error creating insertDate (filePath: " + filePath + ", year: " + year.ToString() + ", month: " + month.ToString() + ", day: " + day.ToString() + ", hour: " + hour.ToString() + ").", ex);
			}
		}


		public Int32 FileID
		{
			get
			{
				return _fileID;
			}
		}


		public Int32 RootID
		{
			get
			{
				return _rootID;
			}
		}


		public string Extension
		{
			get
			{
				return _extension;
			}
		}


		public DateTime InsertDate
		{
			get
			{
				return _insertDate;
			}
		}
	}
}
