using System;

namespace Matchnet.File.ValueObjects
{
	[Serializable]
	public class FileInstance : IComparable
	{
		private const string DATE_FORMAT = "yyyy/MM/dd/HH/";

		private Int32 _fileRootID;
		private bool _isPrimary;
		string _path;
		string _webPath;

		public FileInstance(Int32 fileRootID,
			bool isPrimary,
			string path,
			string webPath)
		{
			_fileRootID = fileRootID;
			_isPrimary = isPrimary;
			_path = path;
			_webPath = webPath;
		}


		public Int32 FileRootID
		{
			get
			{
				return _fileRootID;
			}
		}


		public bool IsPrimary
		{
			get
			{
				return _isPrimary;
			}
		}


		public string Path
		{
			get
			{
				return _path;
			}
		}


		public string WebPath
		{
			get
			{
				return _webPath;
			}
		}


		public int CompareTo(object obj)
		{
			if (obj.GetType().Name != "FileInstance")
			{
				return 0;
			}

			FileInstance fileInstance = obj as FileInstance;

			if (fileInstance.IsPrimary && !this.IsPrimary)
			{
				return 1;
			}
			else if (!fileInstance.IsPrimary && this.IsPrimary)
			{
				return -1;
			}

			return 0;
		}

		public static string GetFileRelativePath(Int32 fileID, string fileExtension, DateTime insertDate, Int32 saveDepth)
		{
			return GetFileRelativeWebPath(fileID, fileExtension, insertDate, saveDepth).Replace("/", @"\");
		}


		public static string GetFileRelativeWebPath(Int32 fileID, string fileExtension, DateTime insertDate, Int32 saveDepth)
		{
			string dateFormat = DATE_FORMAT;

			if (saveDepth == 3)
			{
				dateFormat = "yyyy/MM/dd/";
			}

			return insertDate.ToString(dateFormat) + fileID.ToString() + "." + fileExtension;
		}

	}
}
