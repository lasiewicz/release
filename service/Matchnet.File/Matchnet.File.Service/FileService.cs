using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.IO;

using Matchnet.RemotingServices;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.File.ServiceManagers;
using Matchnet.File.ValueObjects;


namespace Matchnet.File.Service
{
	public class FileService : RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private FileSM _fileSM = null;

		public FileService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new FileService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void RegisterServiceManagers()
		{
			_fileSM = new FileSM();
			base.RegisterServiceManager(_fileSM);
			base.RegisterServiceManagers();
		}
	}
}
