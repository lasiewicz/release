#region

using System;
using System.IO;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.File.ValueObjects.ServiceDefinitions;
using Matchnet.File.ValueObjects;
using Matchnet.PhotoFile.ServiceAdapters;
using Spark.Logging;
using System.Security.Cryptography;
using System.Text;
using ServiceConstants = Matchnet.File.ValueObjects.ServiceConstants;

#endregion

namespace Matchnet.File.ServiceAdapters
{
	public class FileSA
	{
		public static readonly FileSA Instance = new FileSA();

		private const string DATE_FORMAT = "yyyy/MM/dd/HH/";
		private const Int32 DEFAULT_SAVE_DEPTH = 4;
        private const string MESSAGE_ATTACHMENT_CLOUD_PATH_FORMAT = "/attachments/{0}/{1}/{2}.{3}";

		private FileSA()
		{
		}

        #region Public Methods
        /// <summary>
        /// Saves new member photo file
        /// </summary>
        public CreateNewFileResult SaveNewFile(byte[] fileBytes, string extension, int communityID, int siteID,
                                               int memberID, bool originalUpload)
        {
            //defaults to member photo
            return SaveNewFile(fileBytes, extension, communityID, siteID, memberID, originalUpload,
                               FileType.MemberProfilePhoto);
        }

        /// <summary>
        /// Saves new file according to file type
        /// </summary>
        public CreateNewFileResult SaveNewFile(byte[] fileBytes, string extension, int communityID, int siteID, int memberID, bool originalUpload, FileType fileType)
        {            
            RollingFileLogger.Instance.LogInfoMessage("WEB", "FileSA", String.Format("Entering SaveNewFile - memberID: {0}, siteID: {1}, fileType: {2}", memberID, siteID, fileType), null);
            CreateNewFileResult newFileResult = new CreateNewFileResult();
            DateTime insertDate = Convert.ToDateTime(DateTime.Now.ToString("g")); // no seconds. sql server smalldatetime rounds seconds

            if (!string.IsNullOrEmpty(extension))
            {
                extension = extension.Trim().Replace(".", "");
            }

            //create new file record
            Root root = CreateNewFile(newFileResult, extension, fileType, insertDate);

            var addCloudPrefix = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.PHOTO_USE_RANDOM_PREFIX_FOR_CLOUD, communityID, siteID));

            //assign paths
            AssignFilePath(newFileResult, extension, root, insertDate, fileType, addCloudPrefix);

            if (fileType == FileType.MemberProfilePhoto)
            {
                var saveNewPhotoResult = PhotoFileSA.Instance.Save(newFileResult.FileRootID,
                                                                   newFileResult.WebPath,
                                                                   newFileResult.CloudPath,
                                                                   fileBytes,
                                                                   communityID,
                                                                   siteID,
                                                                   originalUpload);


                newFileResult.Success = saveNewPhotoResult.Success;
                newFileResult.OriginalFileSize = saveNewPhotoResult.OriginalFileSize;
                newFileResult.OriginalFileCloudPath = saveNewPhotoResult.OriginalFileCloudPath;
            }
            else if (fileType == FileType.MessageAttachmentPhoto)
            {
                //message attachment photos has the following rules:
                //1. do not save to local servers
                //2. save two versions to the cloud: a. resized photo (preview), b. original photo
                //3. the cloud path will use a format other than the usually date/time folder structure
                var saveNewPhotoResult = PhotoFileSA.Instance.SaveWithNoLocalCopy(newFileResult.CloudPath,
                                                                   newFileResult.OriginalFileCloudPath,
                                                                   fileBytes,
                                                                   communityID,
                                                                   siteID,
                                                                   originalUpload,
                                                                   fileType);
                newFileResult.Success = saveNewPhotoResult.Success;
                newFileResult.OriginalFileCloudPath = saveNewPhotoResult.OriginalFileCloudPath;
            }

            RollingFileLogger.Instance.LogInfoMessage("WEB", "FileSA",
                                                          String.Format(
                                                              "Leaving SaveNewFile - memberID: {0}, siteID: {1}, fileType: {2}, success: {3}, fileID: {4}",
                                                              memberID, siteID, fileType, newFileResult.Success, newFileResult.FileID),
                                                          null);

            return newFileResult;
        }
        		
		public void GetFilePath(Int32 fileID,
			out string filePath,
			out string fileWebPath)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				getService(uri).GetFilePath(fileID,
					out filePath,
					out fileWebPath);
			}
			catch (Exception ex)
			{
				throw new SAException("GetFilePath() error (uri:" + uri + ", fileID: " + fileID.ToString() + ").",
					ex);
			}
		}

		public FileInstance[] GetFileInstances(Int32 fileID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetFileInstances(fileID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetFileInstances() error (uri:" + uri + ", fileID: " + fileID.ToString() + ").",
					ex);
			}
		}

        public void HideFile(string relativeFilePath, string relativeCloudFilePath, int communityID, int siteID)
        {
            try
            {
                PhotoFileSA.Instance.Hide(relativeFilePath, relativeCloudFilePath, communityID, siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("HideFile() error (relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }

        public void UnHideFile(string relativeFilePath, string relativeCloudFilePath, int communityID, int siteID)
        {
            try
            {
                PhotoFileSA.Instance.UnHide(relativeFilePath, relativeCloudFilePath, communityID, siteID);
 }
            catch (Exception ex)
            {
                throw new SAException("UnHideFile() error (relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }

        public void DeleteFile(Int32 fileID, int communityID, int siteID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();

                Int32 fileRootID = 0;
                string relativeFilePath = "";

                //will only be a single instance record once we de-marty
                FileInstance[] instances = this.GetFileInstances(fileID);
                for (Int32 instanceNum = 0; instanceNum < instances.Length; instanceNum++)
                {
                    FileInstance instance = instances[instanceNum];
                    /*
                    relativeFilePath = instance.Path;
                    //System.Diagnostics.Trace.WriteLine("__fileRootID: " + fileRootID.ToString() + ", " + FileRootSA.Instance.GetRootGroups().GetRoot(fileRootID).Path.Length.ToString());
                    relativeFilePath = "/Photo" + fileRootID.ToString() + "/" + relativeFilePath.Substring(FileRootSA.Instance.GetRootGroups().GetRoot(fileRootID).Path.Length).Replace("\\", "/");
                    */
                    relativeFilePath = instance.WebPath;
                    if (relativeFilePath.IndexOf("http://") != -1)
                    {
                        relativeFilePath = new Uri(relativeFilePath).AbsolutePath;
                    }

                    fileRootID = Convert.ToInt32(relativeFilePath.Split('/')[1].Substring(5));
                    break;
                }

                if (relativeFilePath.Length > 0)
                {
                    PhotoFileSA.Instance.Delete(fileRootID, relativeFilePath, communityID, siteID);
                }
                getService(uri).DeleteFile(fileID);
            }
            catch (Exception ex)
            {
                throw new SAException("DeleteFile() error (uri:" + uri + ", fileID: " + fileID.ToString() + ").",
                    ex);
            }
        }
		
		public void DeleteFile(Int32 fileID)
		{
			DeleteFile(fileID, Constants.NULL_INT, Constants.NULL_INT);
		}

		public void UpdateFile(Int32 fileRootID,
			string fileWebPath,
			byte[] fileBytes)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				getService(uri).UpdateFile(Convert.ToInt32(Path.GetFileNameWithoutExtension(fileWebPath)),
					fileBytes.Length);

				PhotoFileSA.Instance.Save(fileRootID,
					new Uri(fileWebPath).LocalPath,
					fileBytes);
			}
			catch (Exception ex)
			{
				throw new SAException("UpdateFile() error (uri:" + uri + ", fileRootID: " + fileRootID.ToString() + ", fileWebPath: " + fileWebPath + ", fileSize: " + fileBytes.Length.ToString() + ").",
					ex);
			}
		}

        #endregion

        #region Private Methods
        private Root CreateNewFile(CreateNewFileResult result, string extension, FileType fileType, DateTime insertDate)
        {            
            //get neew fileID
            result.FileID = KeySA.Instance.GetKey("FileID");
            RollingFileLogger.Instance.LogInfoMessage("WEB", "FileSA", "FileID from Key call: " + result.FileID.ToString(), null);

            //save file record
            Root root = FileRootSA.Instance.GetRootGroups().GetWritableRoot();
            result.FileRootID = root.RootGroupID;            
            InsertFile(result.FileID,
                    root.ID,
                    extension,
                    insertDate);
            
            string resultString = string.Format("RootGroupID: {0}, FileID: {1}",
                                                result.FileRootID.ToString(), result.FileID.ToString());
            RollingFileLogger.Instance.LogInfoMessage("WEB", "FileSA", "CreateNewFile - " + resultString, null);

            return root;
        }

        private void AssignFilePath(CreateNewFileResult result, string extension, Root root, DateTime insertDate, FileType fileType, bool addCloudPrefix)
        {
            try
            {
                if (fileType == FileType.MemberProfilePhoto)
                {
                    //set relative web path and cloud path
                    bool useConsolidatedPath =
                        Convert.ToBoolean(RuntimeSettings.GetSetting("FILE_SERVICE_USE_CONSOLIDATED_PATH"));

                    if (useConsolidatedPath)
                    {
                        result.WebPath = "/" + FileInstance.GetFileRelativeWebPath(result.FileID, extension, insertDate,
                                                                             DEFAULT_SAVE_DEPTH);
                    }
                    else
                    {
                        result.WebPath = root.WebPath + FileInstance.GetFileRelativeWebPath(result.FileID, extension, insertDate,
                                                                             DEFAULT_SAVE_DEPTH);
                        result.WebPath = new Uri(result.WebPath).LocalPath;
                    }


                    if (addCloudPrefix)
                    {
                        result.CloudPath = "/" + GetRandomCloudPrefix() + FileInstance.GetFileRelativeWebPath(result.FileID, extension, insertDate, DEFAULT_SAVE_DEPTH); 
                    }
                    else
                    {
                        result.CloudPath = "/" + FileInstance.GetFileRelativeWebPath(result.FileID, extension, insertDate, DEFAULT_SAVE_DEPTH);    
                    }
                    
                }
                else if (fileType == FileType.MessageAttachmentPhoto)
                {
                    //set relative cloud path
                    //original path example: /attachments/HashOfTodayAndSalt(Year/Month/Day)/original/filename.jpg
                    //preview path example: /attachments/HashOfTodayAndSalt(Year/Month/Day)/preview/filename.jpg
                    
                    string hashOfTodayAndSalt = GenerateHashForCloudPath(32, true);
                    result.CloudPath = string.Format(MESSAGE_ATTACHMENT_CLOUD_PATH_FORMAT, hashOfTodayAndSalt, "preview",
                                                     result.FileID, extension);
                    result.OriginalFileCloudPath = string.Format(MESSAGE_ATTACHMENT_CLOUD_PATH_FORMAT, hashOfTodayAndSalt, "original",
                                                     result.FileID, extension);

                }

                string resultString = string.Format("RootGroupID: {0} WebPath: {1}, FileID: {2}, CloudPath: {3}",
                                                result.FileRootID.ToString(), result.WebPath, result.FileID.ToString(), result.CloudPath);
                RollingFileLogger.Instance.LogInfoMessage("WEB", "FileSA", "AssignFilePath - " + resultString, null);
            }
            catch (Exception ex)
            {
                throw new Exception("assignFilePath() error.", ex);
            }
        }

        private string GetRandomCloudPrefix()
        {
            return Guid.NewGuid().ToString().Substring(0, 4) + "-";
        }

        public void InsertFile(Int32 fileID,
            Int32 fileRootID,
            string fileExtension,
            DateTime insertDate)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();
                getService(uri).InsertFile(fileID,
                    fileRootID,
                    fileExtension,
                    insertDate);
            }
            catch (Exception ex)
            {
                throw new SAException("InsertFile() error (uri:" + uri + ", fileID: " + fileID.ToString() + ", fileRootID: " + fileRootID.ToString() + ", fileExtension: " + fileExtension + ", insertDate: " + insertDate.ToString() + ").",
                    ex);
            }
        }

        /// <summary>
        /// This generates a daily hash used as part of the cloud path to make an non-obvious URL for security reasons.
        /// It returns a 32 length string using 0-9 and a-f characters only so it's URL safe; unless you specify a shorter length as a parameter
        /// </summary>
        /// <returns></returns>
        private string GenerateHashForCloudPath(int maxLength, bool lowercaseOnly)
        {
            string hashSalt = "GusClarkKent00";
            string daily = DateTime.Now.ToString("yyyy/MM/dd");

            //MD5 creates a hash with 16 bytes
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(daily + hashSalt);
            byte[] hash = md5.ComputeHash(inputBytes);

            //Convert 16 bytes into 32 byte hexadecimal string (0-9, A-F)
            StringBuilder sb = new StringBuilder();
            int length = (maxLength < hash.Length) ? maxLength : hash.Length;
            string hashFormat = lowercaseOnly ? "x2" : "X2";
            for (int i = 0; i < length; i++)
            {
                sb.Append(hash[i].ToString(hashFormat));
            }

            return sb.ToString();
        }

        #endregion

        #region Service Related
        private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILESVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		private IFileService getService(string uri)
		{
			return Activator.GetObject(typeof(IFileService), uri) as IFileService;
        }
        #endregion
    }
}
