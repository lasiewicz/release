using System;
using System.IO;

using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.File.BusinessLogic;
using Matchnet.File.ValueObjects.ServiceDefinitions;
using Matchnet.File.ValueObjects;


namespace Matchnet.File.ServiceManagers
{
	public class FileSM : MarshalByRefObject, IFileService, Matchnet.IBackgroundProcessor
	{
		HydraWriter _hydraWriter = null;

		public FileSM()
		{
			_hydraWriter = new HydraWriter(new string[]{"mnFile"});
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}

		public void GetFilePath(Int32 fileID,
			out string filePath,
			out string fileWebPath)
		{
			try
			{
				FileBL.Instance.GetFilePath(fileID,
					out filePath,
					out fileWebPath);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"GetFilePath() error (fileID: " + fileID.ToString() + ").",
					ex);
			}
		}


		public FileInstance[] GetFileInstances(Int32 fileID)
		{
			try
			{
				return FileBL.Instance.GetFileInstances(fileID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"GetFileInstances() error (fileID: " + fileID.ToString() + ").",
					ex);
			}
		}


		public void InsertFile(Int32 fileID,
			Int32 fileRootID,
			string fileExtension,
			DateTime insertDate)
		{
			try
			{
				FileBL.Instance.InsertFile(fileID,
					fileRootID,
					fileExtension,
					insertDate);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"InsertFile() error (fileID: " + fileID.ToString() + ", fileExtension: " + fileExtension + ", insertDate: " + insertDate.ToString() + ").", 
					ex);
			}
		}

		
		public void DeleteFile(Int32 fileID)
		{
			try
			{
				FileBL.Instance.DeleteFile(fileID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"DeleteFile() error (fileID: " + fileID.ToString() + ").",
					ex);
			}
		}


		public void UpdateFile(Int32 fileID, long fileSize)
		{
			try
			{
				FileBL.Instance.UpdateFile(fileID,
					fileSize);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"UpdateFile() error (fileID: " + fileID.ToString() + ", fileSize: " + fileSize.ToString() + ").",
					ex);
			}
		}


		#region IBackgroundProcessor Members
		public void Start()
		{
			_hydraWriter.Start();
		}


		public void Stop()
		{
			_hydraWriter.Stop();
		}
		#endregion
	}
}
