using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Exceptions;
using Matchnet.File.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;

using Matchnet.Data;


namespace Matchnet.File.BusinessLogic
{
	public class FileBL
	{
		public static readonly FileBL Instance = new FileBL();

		/// <summary>
		/// Initializes a new instance of the <see cref="FileBL"/> class.
		/// </summary>
		private FileBL()
		{
		}

		/// <summary>
		/// Gets the file path.
		/// </summary>
		/// <param name="fileID">The file ID.</param>
		/// <param name="filePath">The file path.</param>
		/// <param name="fileWebPath">The file web path.</param>
		public void GetFilePath(Int32 fileID,
			out string filePath,
			out string fileWebPath)
		{
			filePath = null;
			fileWebPath = null;

			FileInstance[] instances = GetFileInstances(fileID);

			if (instances.Length > 0)
			{
				filePath = instances[0].Path;
				fileWebPath = instances[0].WebPath;
				return;
			}
		}


		/// <summary>
		/// Gets the file instances.
		/// </summary>
		/// <param name="fileID">The file ID.</param>
		/// <returns></returns>
		public FileInstance[] GetFileInstances(Int32 fileID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnFile", "dbo.up_FileRoot_List", fileID);
				command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);				
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalRootID = Constants.NULL_INT;
				Int32 ordinalExtension = Constants.NULL_INT;
				Int32 ordinalInsertDate = Constants.NULL_INT;
				Int32 ordinalSaveDepth = Constants.NULL_INT;

				RootGroups rootGroups = FileRootSA.Instance.GetRootGroups();
				ArrayList instances = new ArrayList();

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalRootID = dataReader.GetOrdinal("RootID");
						ordinalExtension = dataReader.GetOrdinal("Extension");
						ordinalInsertDate = dataReader.GetOrdinal("InsertDate");
						ordinalSaveDepth = dataReader.GetOrdinal("SaveDepth");
						colLookupDone = true;
					}

					try
					{
						Root root = rootGroups.GetRoot(dataReader.GetInt32(ordinalRootID));
						string extension = dataReader.GetString(ordinalExtension);
						DateTime insertDate = dataReader.GetDateTime(ordinalInsertDate);
						Int32 saveDepth = (Int32)dataReader.GetByte(ordinalSaveDepth);
						string filePath = root.Path + FileInstance.GetFileRelativePath(fileID, extension, insertDate, saveDepth);
						string fileWebPath = root.WebPath + FileInstance.GetFileRelativeWebPath(fileID, extension, insertDate, saveDepth);
						instances.Add(new FileInstance(root.ID, root.IsParent, filePath, fileWebPath));
					}
					catch
					{
						//this empty catch will be removed once data is migrated
					}
				}

				instances.Sort();

				return (FileInstance[])instances.ToArray(typeof(FileInstance));
			}
			catch (Exception ex)
			{
				throw(new BLException("Error occured while getting file path (fileID: " + fileID.ToString() + ").", ex));
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}


		/// <summary>
		/// Inserts the file.
		/// </summary>
		/// <param name="fileID">The file ID.</param>
		/// <param name="fileRootID">The file root ID.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="insertDate">The insert date.</param>
		public void InsertFile(Int32 fileID,
			Int32 fileRootID,
			string fileExtension,
			DateTime insertDate)
		{
			Command command = new Command("mnFile", "dbo.up_File_Insert", fileID);
			command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
			command.AddParameter("@RootID", SqlDbType.Int, ParameterDirection.Input, fileRootID - (fileRootID % 100));
			command.AddParameter("@FileRootID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey("FileRootID"));
			command.AddParameter("@Extension", SqlDbType.VarChar, ParameterDirection.Input, fileExtension);
			command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, insertDate);
			Client.Instance.ExecuteAsyncWrite(command);
		}

		
		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="fileID">The file ID.</param>
		public void DeleteFile(Int32 fileID)
		{
			Command command = new Command("mnFile", "dbo.up_File_Delete", fileID);
			command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
			Client.Instance.ExecuteAsyncWrite(command);
		}


		/// <summary>
		/// Updates the file.
		/// </summary>
		/// <param name="fileID">The file ID.</param>
		/// <param name="fileSize">Size of the file.</param>
		public void UpdateFile(Int32 fileID, long fileSize)
		{
			try
			{
				Command command = new Command("mnFile", "dbo.up_File_Update", fileID);
				command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
				command.AddParameter("@Size", SqlDbType.Int, ParameterDirection.Input, fileSize);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch (Exception ex)
			{
				throw new BLException("Error updating file (fileID: " + fileID.ToString() + ", fileSize: " + fileSize.ToString() + ").", ex);
			}
		}
	}
}
