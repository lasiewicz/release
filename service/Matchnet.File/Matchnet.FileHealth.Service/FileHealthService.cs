using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.ServiceProcess;
using System.Threading;

using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.File;


namespace Matchnet.FileHealth.Service
{
	public class FileHealthService : System.ServiceProcess.ServiceBase
	{
		private const string SERVICE_NAME = "Matchnet.FileHealth.Service";
		
		private System.ComponentModel.Container components = null;

		private bool _runnable;
		private Thread _threadHealth;
		private string _dsPath = null;

		public FileHealthService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new FileHealthService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = SERVICE_NAME;
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		protected override void OnStart(string[] args)
		{
			string siteRoot = System.Configuration.ConfigurationSettings.AppSettings["siteRoot"];
			if (siteRoot.EndsWith("\\"))
			{
				siteRoot = siteRoot.Substring(0, siteRoot.Length - 1);
			}
			siteRoot = siteRoot.ToLower();

			System.Diagnostics.Trace.WriteLine("__siteRoot: " + siteRoot);
			DirectoryEntry iis = new DirectoryEntry("IIS://localhost/w3svc");

			foreach (DirectoryEntry child in iis.Children)
			{
				//System.Diagnostics.Trace.WriteLine("__" + child.Path + ", " + child.SchemaClassName.ToString());
				if (child.SchemaClassName == "IIsWebServer")
				{
					string currentPath = new DirectoryEntry(child.Path + "/ROOT").Properties["Path"][0].ToString().ToLower();
					if (currentPath.EndsWith("\\"))
					{
						currentPath = currentPath.Substring(0, currentPath.Length - 1);
					}
					//System.Diagnostics.Trace.WriteLine("__: " + currentPath + ", " + siteRoot);
					if (currentPath == siteRoot)
					{
						System.Diagnostics.Trace.WriteLine("__MATCH");
						_dsPath = child.Path;
					}
				}
			}

			if (_dsPath == null)
			{
				throw new Exception("Unable to find site root in directory services.");
			}

			System.Diagnostics.Trace.WriteLine("__dsPath: " + _dsPath);

			_runnable = true;
			_threadHealth = new Thread(new ThreadStart(healthCycle));
			_threadHealth.Start();
		}
 

		protected override void OnStop()
		{
			_runnable = false;
			_threadHealth.Join(10000);
		}

		private void healthCycle()
		{
			while (_runnable)
			{
				try
				{
					//System.Diagnostics.Trace.WriteLine("__healthCycle()");

					RootGroups rootGroups = FileRootSA.Instance.GetRootGroups();

					IDictionaryEnumerator de = rootGroups.Groups.GetEnumerator();

					while (de.MoveNext())
					{
						Int32 id = Convert.ToInt32(de.Key);
						ArrayList rootList = de.Value as ArrayList;

						//System.Diagnostics.Trace.WriteLine("__");
						//System.Diagnostics.Trace.WriteLine("__id: " + id.ToString());

						string vdir = new Uri((rootList[0] as Root).WebPath).AbsolutePath.Replace("/","");
						if (vdir.ToLower().IndexOf("photo") > -1)
						{
							Thread.Sleep(100);
							//System.Diagnostics.Trace.WriteLine("__v: " + _dsPath + "/root/" + vdir);
							DirectoryEntry directoryEntry = new DirectoryEntry(_dsPath + "/root/" + vdir);
							string path = cleanPath(directoryEntry.Properties["path"].Value.ToString());
							//System.Diagnostics.Trace.WriteLine("__path: " + path);

							for (Int32 rootNum = 0; rootNum < rootList.Count; rootNum++)
							{
								Root root =  rootList[rootNum] as Root;
								string rootPath = cleanPath(root.Path);

								//System.Diagnostics.Trace.WriteLine("__" + rootPath + ", isParent: " + root.IsParent.ToString());

								if (root.IsParent == true && rootPath == path && healthCheck(rootPath))
								{
									//primary root is active & passes health check
									//System.Diagnostics.Trace.WriteLine("__primary is up");
									break;
								}
								else if (root.IsParent == true && rootPath != path && healthCheck(rootPath))
								{
									//primary root is not active & passes health check
									directoryEntry.Properties["path"].Value = rootPath;
									directoryEntry.CommitChanges();
									//System.Diagnostics.Trace.WriteLine("__primary is back up");

									EventLog.WriteEntry(SERVICE_NAME,
										"Primary file root is back up (id: " + id.ToString() + ", path: " + rootPath + ")",
										EventLogEntryType.Warning);
									break;
								}
								else if (rootPath != path && healthCheck(rootPath) && !healthCheck(path))
								{
									//System.Diagnostics.Trace.WriteLine("__primary is down");
									directoryEntry.Properties["path"].Value = rootPath;
									directoryEntry.CommitChanges();

									EventLog.WriteEntry(SERVICE_NAME,
										"File root is down (id: " + id.ToString() + ", path: " + path + ")",
										EventLogEntryType.Error);
									break;
								}
							}

							directoryEntry.Close();
							directoryEntry.Dispose();
						}
					}
				}
				catch (Exception ex)
				{
					System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\n", " ").Replace("\r", " "));
					new ServiceBoundaryException(SERVICE_NAME,
						"Health check cycle error.",
						ex);
				}

				Thread.Sleep(1000);	//todo
			}
		}


		private bool healthCheck(string path)
		{
			try
			{
				//System.Diagnostics.Trace.WriteLine("__file.Exists() " + path + "health.txt");
				bool result = File.Exists(path + "health.txt");
				//System.Diagnostics.Trace.WriteLine("__" + result.ToString());
				return result;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
				return false;
			}
		}


		private string cleanPath(string path)
		{
			path = path.ToLower();
			if (path.LastIndexOf("\\") != path.Length - 1)
			{
				path = path + "\\";
			}

			return path;
		}
	}
}
