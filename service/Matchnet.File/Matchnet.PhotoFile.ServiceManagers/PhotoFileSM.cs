#region

using System;
using Matchnet.Exceptions;
using Matchnet.PhotoFile.BusinessLogic;
using Matchnet.PhotoFile.ServiceDefinitions;
using Matchnet.PhotoFile.ValueObjects;

#endregion

namespace Matchnet.PhotoFile.ServiceManagers
{
    /// <summary>
    /// Note: This class should be kept in sync or later removed.  The actual PhotoFileSM being used is in the Matchnet.PhotoFile.Web solution.
    /// </summary>
    public class PhotoFileSM : MarshalByRefObject, IPhotoFileService
    {
        private static PhotoFileBL _photoFileBL = null;

        public PhotoFileSM()
        {
        }


        public override object InitializeLifetimeService()
        {
            return null;
        }

        public static void Start()
        {
            _photoFileBL = new PhotoFileBL();
            _photoFileBL.Start();
        }


        public static void Stop()
        {
            _photoFileBL.Stop();
        }

        public bool SaveAdmintoolPhoto(byte[] fileData, string relativeFileCloudPath, string key)
        {
            try
            {
                return _photoFileBL.SaveAdmintoolPhoto(fileData,
                     relativeFileCloudPath,
                     key);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "SaveAdmintoolPhoto() error (relativeFilePath: " + relativeFileCloudPath + ").",
                    ex);
            }
        }

        public bool SaveOriginalFile(byte[] fileData, string relativeFileCloudPath, string key, int communityID, int siteID)
        {
            try
            {
                return _photoFileBL.SaveOriginalFile(fileData,
                     relativeFileCloudPath,
                     key,
                     communityID,
                     siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "SaveOriginalFile() error (relativeFilePath: " + relativeFileCloudPath + ").",
                    ex);
            }
        }

        public void Save(Int32 fileRootID,
            string relativeFilePath,
            byte[] fileData,
            string key)
        {
            try
            {
                _photoFileBL.Save(fileRootID,
                    relativeFilePath,
                    fileData,
                    key);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "Save() error (relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }


        }

        public bool Save(Int32 fileRootID,
            string relativefilePath,
            string relativefileCloudPath,
            byte[] fileData,
            string key,
            int communityID,
            int siteID)
        {
            try
            {
                return _photoFileBL.Save(fileRootID,
                     relativefilePath,
                     relativefileCloudPath,
                     fileData,
                     key,
                     communityID,
                     siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "Save() error (relativeFilePath: " + relativefilePath + ").",
                    ex);
            }
        }

        /// <summary>
        /// This saves the photo to the cloud based on the cloud file type; does not save photo to local servers
        /// </summary>
        public bool SaveWithNoLocalCopy(byte[] fileData, string relativeFileCloudPath, string key, int communityID,
                                        int siteID, int fileTypeID)
        {
            try
            {
                return _photoFileBL.SaveWithNoLocalCopy(fileData, relativeFileCloudPath, key, communityID, siteID,
                                                        (Matchnet.File.ValueObjects.FileType)Enum.Parse(typeof(Matchnet.File.ValueObjects.FileType), fileTypeID.ToString()));
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    String.Format("SaveWithNoLocalCopy() error (relativeFileCloudPath: {0}, siteID: {1}, fileTypeID: {2} ).", relativeFileCloudPath, siteID, fileTypeID),
                    ex);
            }
        }

        public void Delete(Int32 fileRootID,
            string relativeFilePath,
            string key)
        {
            try
            {
                System.Diagnostics.Trace.Write("PhotoFileSM.Delete");
                _photoFileBL.Delete(fileRootID,
                    relativeFilePath,
                    key);
                System.Diagnostics.Trace.Write("PhotoFileSM.Delete.End");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("PhotoFileSM.Delete.Err:" + ex.ToString());
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "Delete() error (relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }

        public void Delete(Int32 fileRootID,
            string relativeFilePath,
            string key,
            int communityID,
            int siteID)
        {
            try
            {
                System.Diagnostics.Trace.Write("PhotoFileSM.Delete");
                _photoFileBL.Delete(fileRootID,
                    relativeFilePath,
                    key,
                    communityID,
                    siteID);
                System.Diagnostics.Trace.Write("PhotoFileSM.Delete.End");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("PhotoFileSM.Delete.Err:" + ex.ToString());
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "Delete() error (relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }

        public void Hide(string relativeFilePath,
            string relativeCloudFilePath,
            string key,
            int communityID,
            int siteID)
        {
            _photoFileBL.Hide(relativeFilePath, relativeCloudFilePath, key, communityID, siteID);
        }

        public void UnHide(string relativeFilePath,
            string relativeCloudFilePath,
            string key,
            int communityID,
            int siteID)
        {
            _photoFileBL.Hide(relativeFilePath, relativeCloudFilePath, key, communityID, siteID);
        }


    }
}
