#region

using System;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.PhotoFile.ServiceDefinitions;
using Matchnet.PhotoFile.ValueObjects;
using Spark.Logging;
using Matchnet.File.ValueObjects;

#endregion

namespace Matchnet.PhotoFile.ServiceAdapters
{
    public class PhotoFileSA
    {
        public static readonly PhotoFileSA Instance = new PhotoFileSA();

        private PhotoFileSA()
        {
            ChannelServices.RegisterChannel(new HttpClientChannel("", new BinaryClientFormatterSinkProvider()));
        }

        /// <summary>
        /// Saves member photos locally only
        /// </summary>
        public void Save(Int32 rootID,
                         string relativeFilePath,
                         byte[] fileData)
        {
            string uri = "";

            fileData = ResizeImageBytes(fileData);
            fileData = CompressImageBytes(fileData);

            try
            {
                uri = getServiceManagerUri();
                getService(uri).Save(rootID,
                                     relativeFilePath,
                                     fileData,
                                     Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                                         "PHOTOFILESVC_KEY"));
            }
            catch (Exception ex)
            {
                throw new SAException(
                    "Save() error (uri: " + uri + ", rootID: " + rootID.ToString() + ", relativeFilePath: " +
                    relativeFilePath + ").",
                    ex);
            }
        }

        /// <summary>
        /// Saves member photos locally and to the cloud
        /// </summary>

	    public SaveNewPhotoResult SaveAdmintoolPhoto(byte[] fileData, string relativeFileCloudPath)
	    {
            string uri = "";
            SaveNewPhotoResult result = new SaveNewPhotoResult();

            try
            {
                uri = getServiceManagerUri();
                result.Success = getService(uri)
                    .SaveAdmintoolPhoto(fileData, relativeFileCloudPath, RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"));
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException("WEB", "PhotoFileSA exception", ex);
                throw new SAException(
                    "SaveAdmintoolPhoto() error (uri: " + uri + ", relativeFileCloudPath: " + relativeFileCloudPath + ").",
                    ex);
            }

	        return result;
	    }

	    public SaveNewPhotoResult Save(Int32 rootID,
                                       string relativeFilePath,
                                       string relativeCloudPath,
                                       byte[] fileData,
                                       int communityID,
                                       int siteID,
                                       bool originalUpload)
        {
            string uri = "";
            bool originalSaved;
            SaveNewPhotoResult result = new SaveNewPhotoResult();

            if (originalUpload)
            {
                try
                {
                    var originalID = KeySA.Instance.GetKey("MemberPhotoOriginalID");
                    var originalCloudPath = replaceFileIdInFilePath(relativeCloudPath, originalID.ToString());

                    uri = getServiceManagerUri();
                    originalSaved = getService(uri).SaveOriginalFile(fileData,
                                                                     originalCloudPath,
                                                                     RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"),
                                                                     communityID,
                                                                     siteID);

                    if (!originalSaved)
                    {
                        throw new Exception("Original photo file not saved");
                    }

                    result.OriginalFileCloudPath = originalCloudPath;
                    result.OriginalFileSize = fileData.Length;
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogException("WEB", "PhotoFileSA exception", ex);
                    throw new SAException(
                        "SaveOriginalFile() error (uri: " + uri + ", rootID: " + rootID.ToString() +
                        ", relativeFilePath: " + relativeFilePath + ").",
                        ex);
                }
            }

            RollingFileLogger.Instance.LogInfoMessage("WEB", "PhotoFileSA",
                                                      "Entering Save. RootID " + rootID.ToString() + " FilePath: " +
                                                      relativeFilePath, null);
            fileData = ResizeImageBytes(fileData);
            RollingFileLogger.Instance.LogInfoMessage("WEB", "PhotoFileSA", "Image resized", null);
            fileData = CompressImageBytes(fileData);
            RollingFileLogger.Instance.LogInfoMessage("WEB", "PhotoFileSA", "Image compressed", null);

            try
            {
                uri = getServiceManagerUri();
                result.Success = getService(uri).Save(rootID,
                                                      relativeFilePath,
                                                      relativeCloudPath,
                                                      fileData,
                                                      RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"),
                                                      communityID,
                                                      siteID);

                return result;
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException("WEB", "PhotoFileSA exception", ex);
                throw new SAException(
                    "Save() error (uri: " + uri + ", rootID: " + rootID.ToString() + ", relativeFilePath: " +
                    relativeFilePath + ").",
                    ex);
            }
        }

        /// <summary>
        /// Saves photo file to the cloud only
        /// </summary>
        public SaveNewPhotoResult SaveWithNoLocalCopy(string relativeCloudPath,
                                                      string relativeOriginalCloudPath,
                                                        byte[] fileData,
                                                        int communityID,
                                                        int siteID,
                                                        bool originalUpload,
                                                        FileType fileType)
        {
            string uri = "";
            SaveNewPhotoResult result = new SaveNewPhotoResult();
            string key = RuntimeSettings.GetSetting("PHOTOFILESVC_KEY");

            try
            {
                if (originalUpload && !string.IsNullOrEmpty(relativeOriginalCloudPath))
                {
                    //save original photo to the cloud
                    uri = getServiceManagerUri();
                    result.Success = getService(uri).SaveWithNoLocalCopy(fileData,
                                                                         relativeOriginalCloudPath,
                                                                         key,
                                                                         communityID,
                                                                         siteID,
                                                                         (int)fileType);

                    result.OriginalFileCloudPath = relativeOriginalCloudPath;
                    result.OriginalFileSize = fileData.Length;
                }

                if (result.Success)
                {
                    //resize and compress photo
                    fileData = ResizeImageBytes(fileData, fileType);
                    RollingFileLogger.Instance.LogInfoMessage("WEB", "PhotoFileSA", "Image resized", null);
                    fileData = CompressImageBytes(fileData, fileType);
                    RollingFileLogger.Instance.LogInfoMessage("WEB", "PhotoFileSA", "Image compressed", null);

                    //save resized photo to the cloud
                    result.Success = getService(uri).SaveWithNoLocalCopy(fileData,
                                                                            relativeCloudPath,
                                                                            key,
                                                                            communityID,
                                                                            siteID,
                                                                            (int)fileType);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException("WEB", "PhotoFileSA exception", ex);
                throw new SAException(
                    String.Format("SaveWithNoLocalCopy() error (uri: {0}, fileType: {1}, siteID: {2}, relativeCloudPath: {3}).", uri, fileType, siteID, relativeCloudPath),
                    ex);
            }

            return result;
        }

        public byte[] ResizeImageBytes(byte[] fileData)
        {
            return ResizeImageBytes(fileData, FileType.MemberProfilePhoto);
        }

        public byte[] ResizeImageBytes(byte[] fileData, FileType fileType)
		{
			try
			{
				MemoryStream stream = new MemoryStream(fileData);
				System.Drawing.Image imgPhoto = Image.FromStream(stream);

                int Width = Convert.ToInt32(RuntimeSettings.GetSetting(SettingConstants.MEMBERSVC_PHOTO_XMAX_FOR_INITIAL_UPLOAD));
				int Height = Convert.ToInt32(RuntimeSettings.GetSetting(SettingConstants.MEMBERSVC_PHOTO_YMAX_FOR_INITIAL_UPLOAD));
                if (fileType == FileType.MessageAttachmentPhoto)
                {
                    Width = Convert.ToInt32(RuntimeSettings.GetSetting(SettingConstants.MESSAGE_PHOTO_PREVIEW_XMAX));
                    Height = Convert.ToInt32(RuntimeSettings.GetSetting(SettingConstants.MESSAGE_PHOTO_PREVIEW_YMAX));
                }

				int sourceWidth = imgPhoto.Width;
				int sourceHeight = imgPhoto.Height;
				int sourceX = 0;
				int sourceY = 0;
				int destX = 0;
				int destY = 0; 

				float nPercent = 0;
				float nPercentW = 0;
				float nPercentH = 0;

				// If smaller than spec than return it back untouched.
				if ((sourceWidth <= Width) & (sourceHeight <= Height))
					return fileData;

				nPercentW = ((float)Width/(float)sourceWidth);
				nPercentH = ((float)Height/(float)sourceHeight);

				if(nPercentH < nPercentW)
				{
					nPercent = nPercentH;
					destX = System.Convert.ToInt16((Width - 
						(sourceWidth * nPercent))/2);
				}
				else
				{
					nPercent = nPercentW;
					destY = System.Convert.ToInt16((Height - 
						(sourceHeight * nPercent))/2);
				}

				int destWidth  = (int)(sourceWidth * nPercent);
				int destHeight = (int)(sourceHeight * nPercent);
			
				Bitmap bmPhoto = new Bitmap(destWidth, destHeight, 
					PixelFormat.Format32bppRgb);
				bmPhoto.SetResolution(imgPhoto.HorizontalResolution, 
					imgPhoto.VerticalResolution);

				Graphics grPhoto = Graphics.FromImage(bmPhoto);
				grPhoto.Clear(Color.White);
				grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
				grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

				grPhoto.DrawImage(imgPhoto,
					new Rectangle(sourceX,sourceY,destWidth,destHeight),
					new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
					GraphicsUnit.Pixel);

				MemoryStream finalStream = new MemoryStream();
				bmPhoto.Save(finalStream, System.Drawing.Imaging.ImageFormat.Jpeg);
				byte[] newFileData = finalStream.ToArray();
				
				stream.Close();
				finalStream.Close();
				grPhoto.Dispose();
				imgPhoto.Dispose();

				return newFileData;
			}
			catch(Exception ex)
			{
                RollingFileLogger.Instance.LogException("WEB", "PhotoFileSA resize exception", ex);
                throw new Exception("Error resizing image byte stream.", ex);
			}
		}

        public byte[] CompressImageBytes(byte[] fileData)
        {
            return CompressImageBytes(fileData, FileType.MemberProfilePhoto);
        }   

		/// <summary>
		/// Implementing image compression before it's initially saved. This will cut down on network, HD usage and load time for CS photo approval significantly.
		/// </summary>
		/// <returns></returns>
		public byte[] CompressImageBytes(byte[] fileData, FileType fileType)
		{
			try
			{
				MemoryStream stream;
				System.Drawing.Image image;

				long length;
				int compression = 90;

				EncoderParameters eps = new EncoderParameters(1);
				eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);

				ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
		
				stream = new MemoryStream(fileData);

				image = Image.FromStream(stream);
				length = stream.Length;

				System.Diagnostics.Debug.WriteLine("Image Initial Length:" + stream.Length);

				int loop = 0;
			    int maxBytes = Convert.ToInt32(RuntimeSettings.GetSetting(SettingConstants.MEMBERSVC_PHOTO_MAXBYTES_FOR_INITIAL_UPLOAD));
                if (fileType == FileType.MessageAttachmentPhoto)
                {
                    maxBytes = Convert.ToInt32(RuntimeSettings.GetSetting(SettingConstants.MESSAGE_PHOTO_PREVIEW_MAXBYTES));
                }

				while ((length > maxBytes & (compression > 0)))
				{
					eps = new EncoderParameters(1);
					eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
					stream = new MemoryStream();
					image.Save(stream, ici, eps);
					length = stream.Length;
					compression -= 2;
					//stream.Close(); //http://support.microsoft.com/?id=814675


					loop++;
				}

				System.Diagnostics.Debug.WriteLine("Image compressed Length:" + stream.Length +  " compressed " + loop + " times.");
			
				byte[] newFileData = stream.ToArray();

				stream.Close();

				image.Dispose();

				return newFileData;
			}
			catch(Exception ex)
			{
                RollingFileLogger.Instance.LogException("WEB", "PhotoFileSA compress exception", ex);
                throw new Exception("Error compressing image byte stream.", ex);
			}
		}

		public void Delete(Int32 fileRootID,
			string relativeFilePath)
		{
			string uri = "";

			try
			{
				System.Diagnostics.Trace.WriteLine("__Delete(" + fileRootID.ToString() + ", " + relativeFilePath + ")");
				uri = getServiceManagerUri();
				getService(uri).Delete(fileRootID,
					relativeFilePath,
					Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"));
			}
			catch (Exception ex)
			{
				throw new SAException("Delete() error (uri: " + uri + ", relativeFilePath: " + relativeFilePath + ").",
					ex);
			}
		}


        public void Delete(Int32 fileRootID,
            string relativeFilePath,
            int communityID,
            int siteID)
        {
            string uri = "";

            try
            {
                System.Diagnostics.Trace.WriteLine("__Delete(" + fileRootID.ToString() + ", " + relativeFilePath + ")");
                uri = getServiceManagerUri();
                getService(uri).Delete(fileRootID,
                    relativeFilePath,
                    Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"),
                    communityID, 
                    siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("Delete() error (uri: " + uri + ", relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }


        public void Hide(string relativeFilePath,
            string relativeCloudFilePath,
            int communityID,
            int siteID)
        {
            string uri = "";

            try
            {
                System.Diagnostics.Trace.WriteLine("Hide(" + relativeFilePath + ")");
                uri = getServiceManagerUri();
                getService(uri).Hide(relativeFilePath,
                    relativeCloudFilePath,
                    RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"),
                    communityID,
                    siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("Hide() error (uri: " + uri + ", relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }

        public void UnHide(string relativeFilePath,
            string relativeCloudFilePath,
            int communityID,
            int siteID)
        {
            string uri = "";

            try
            {
                System.Diagnostics.Trace.WriteLine("UnHide(" + relativeFilePath + ")");
                uri = getServiceManagerUri();
                getService(uri).UnHide(relativeFilePath,
                    relativeCloudFilePath,
                    RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"),
                    communityID,
                    siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("UnHide() error (uri: " + uri + ", relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }

        #region Private Methods
        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private string replaceFileIdInFilePath(string fullPath, string newID)
        {
            int lastSlashIndex = fullPath.LastIndexOf('/') + 1;
            int periodIndex = fullPath.LastIndexOf('.') + 1;
            return fullPath.Substring(0, lastSlashIndex) + newID + fullPath.Substring(periodIndex - 1);
        }

        #endregion

        #region Service Related
        private string getServiceManagerUri()
		{
			Int32 port = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOFILESVC_PORT"));
			string host = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOFILESVC_HOST");
			return new UriBuilder("http://", host, port, "PhotoFileSM.rem").ToString();
		}


		private IPhotoFileService getService(string uri)
		{
			try
			{
				return (IPhotoFileService)Activator.GetObject(typeof(IPhotoFileService), uri);
			}
			catch (Exception ex)
			{
				throw new Exception("Error activating remote service manager (uri: " + uri + ").", ex);
			}
        }

        #endregion
    }
}
