﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PhotoFile.ValueObjects
{
    [Serializable]
    public class SaveNewPhotoResult
    {
        public int OriginalFileSize { get; set; }
        public string OriginalFileCloudPath { get; set; }
        public bool Success { get; set; }
    }
}
