using System;

namespace Matchnet.PhotoFile.ValueObjects
{
	[Serializable]
	public class DeletePointer
	{
		private string _path;

		public DeletePointer(string path)
		{
			_path = path;
		}


		public string Path
		{
			get
			{
				return _path;
			}
		}
	}
}
