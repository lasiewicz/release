using System;

namespace Matchnet.PhotoFile.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "PHOTOFILE_SVC";
		public const string SERVICE_NAME = "Matchnet.PhotoFile.Service";
		public const string SERVICE_MANAGER_NAME = "PhotoFileSM";

		private ServiceConstants()
		{
		}
	}
}
