#region

using System;

#endregion

namespace Matchnet.PhotoFile.ServiceDefinitions
{
    public interface IPhotoFileService
    {
        void Save(Int32 fileRootID,
                  string relativeFilePath,
                  byte[] fileData,
                  string key);

        void Delete(Int32 fileRootID,
                    string relativeFilePath,
                    string key);

        void Delete(Int32 fileRootID,
                    string relativeFilePath,
                    string key,
                    int communityID,
                    int siteID);

        bool SaveOriginalFile(byte[] fileData, string relativeFileCloudPath, string key, int communityID, int siteID);

        bool Save(Int32 fileRootID,
                  string relativefilePath,
                  string relativefileCloudPath,
                  byte[] fileData,
                  string key,
                  int communityID,
                  int siteID);

	    /// <summary>
	    /// This saves the photo to the cloud based on the cloud file type; does not save photo to local servers
	    /// </summary>
	    bool SaveWithNoLocalCopy(byte[] fileData, string relativeFileCloudPath, string key, int communityID, int siteID, int fileTypeID);

        void Hide(string relativeFilePath,
                string relativeCloudFilePath,
                string key,
                int communityID,
                int siteID);

        void UnHide(string relativeFilePath,
                string relativeCloudFilePath,
                string key,
                int communityID,
                int siteID);
        bool SaveAdmintoolPhoto(byte[] fileData, string relativeFileCloudPath, string key);
    }
}
