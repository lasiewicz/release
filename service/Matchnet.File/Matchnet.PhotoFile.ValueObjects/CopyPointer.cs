using System;

namespace Matchnet.PhotoFile.ValueObjects
{
	[Serializable]
	public class CopyPointer
	{
		private string _srcPath;
		private string _destPath;

		public CopyPointer(string srcPath,
			string destPath)
		{
			_srcPath = srcPath;
			_destPath = destPath;
		}


		public string SrcPath
		{
			get
			{
				return _srcPath;
			}
		}


		public string DestPath
		{
			get
			{
				return _destPath;
			}
		}
	}
}
