using namespace System;
using namespace Matchnet::e1::Engine::ManagedAdapter;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class GSVStatistics
				{
				private:
					GsvStats _stats;
				public:
					GSVStatistics();
					~GSVStatistics();
					property Int32 NumMembers
					{
						Int32 get();
					}
					property Int32 SearchPermits
					{
						Int32 get();
					}
					property GsvStats NativeStats
					{
						GsvStats get();
					}
					String^ GetDebugTrace();
				};
			};
		};
	};
};