#include "MemberSpec.h"

using namespace System;
using namespace Matchnet::e1::Engine::ManagedAdapter;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class Result
				{
				private:
					S1Result _result;
				public:
					Result(S1Result result);
					~Result();

					property Int16 Count
					{
						Int16 get()
						{
							return _result->memNum;
						}
					}

					property Int32 NumScanned
					{
						Int32 get()
						{
							return _result->memScan;
						}
					}

					Int32 GetResult(Int32 index)
					{
						return _result->memids[index];
					}

					Int32 GetScore(Int32 index)
					{
						return _result->scores[index];
					}

					MemberSpec^ GetMemSpec(Int32 index)
					{
						MemSpec _memSpec = new MemSpecRec();
						S1ResultDetailsIdx(_result, index, _memSpec, 0);
						return gcnew MemberSpec(_memSpec);
					}

					Scores^ GetScores(Int32 index)
					{
						S1Scores _scores = new S1ScoresRec();
						S1ResultDetailsIdx(_result, index, 0, _scores);
						return gcnew Scores(_scores);
					}
				};
			};
		};
	};
};