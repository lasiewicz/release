using namespace System::Collections;

using namespace Matchnet::Search::Interfaces;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class MemberSpec
				{
				private:
					void init(MemSpec memSpec);
					MemSpec _memSpec;
					Int32 _tagCount;
					Location^ _location;
					StringWrapper^ _birthDate;
					StringWrapper^ _registerDate;
					StringWrapper^ _activeDate;
					ArrayList^ _tags;
				public:
					MemberSpec();
					MemberSpec(MemSpec memSpec);
					~MemberSpec();
					property Int32 MemberID
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 CommunityID
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 GenderMask
					{
						Int32 get();
						void set(Int32);
					}
					property Location^ MemberLocation
					{
						Location^ get();
						//void set(Location^);
					}
					property DateTime BirthDate
					{
						void set(DateTime);
						DateTime get();
					}
					property DateTime RegisterDate
					{
						void set(DateTime);
						DateTime get();
					}
					property DateTime ActiveDate
					{
						void set(DateTime);
						DateTime get();
					}
					property Int32 Height
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 Weight
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 ChildrenCount
					{
						Int32 get();
						void set(Int32);
					}
					property UpdateFlag HasPhoto
					{
						UpdateFlag get();
						void set(UpdateFlag);
					}
					property UpdateFlag IsOnline 
					{
						UpdateFlag get();
						void set(UpdateFlag);
					}
					property Int32 EmailCount
					{
						Int32 get();
						void set(Int32);
					}
					property Byte MoreChildrenFlag
					{
						Byte get();
						void set(Byte);
					}
					property Int32 ColorCode
					{
						Int32 get();
						void set(Int32);
					}
					property ArrayList^ Tags
					{
						ArrayList^ get();
					}
					//void AddTag(NameValue^ nameValue);
					void AddTag(String^ name, String^ value);
					void Load();
				};
			};
		};
	};
};