#include "StdAfx.h"
#include "MemberUpdateSpec.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;
using namespace Matchnet::Search::Interfaces;

MemberUpdateSpec::MemberUpdateSpec(void)
{
	_memUpdateSpec = new MemUpdateSpecRec();

	_memUpdateSpec->emailCount = E1_NULL_INT;
	_memUpdateSpec->hasPhoto = E1UpdateFlag_NoChange;
	_memUpdateSpec->isOnline = E1UpdateFlag_NoChange;
}

MemberUpdateSpec::~MemberUpdateSpec(void)
{
	delete _memUpdateSpec;
	delete _activeDate;
}

void MemberUpdateSpec::Load(void)
{
	MemUpdate(_memUpdateSpec);
}

void MemberUpdateSpec::MemberID::set(Int32 value)
{
	_memUpdateSpec->memId = value;
}

void MemberUpdateSpec::CommunityID::set(Int32 value)
{
	_memUpdateSpec->domId = value;
}

void MemberUpdateSpec::ActiveDate::set(DateTime value)
{
	_activeDate = StringConverter::GetE1DateString(value);
	_memUpdateSpec->activeDate = _activeDate->NativeSTR;
}

void MemberUpdateSpec::EmailCount::set(Int32 value)
{
	_memUpdateSpec->emailCount = value;
}

void MemberUpdateSpec::HasPhoto::set(UpdateFlag value)
{
	_memUpdateSpec->hasPhoto = (E1UpdateFlag) value;
}

void MemberUpdateSpec::IsOnline::set(UpdateFlag value)
{
	_memUpdateSpec->isOnline = (E1UpdateFlag) value;
}