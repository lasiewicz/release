using namespace System;
using namespace System::Collections;
using namespace Matchnet::Search::Interfaces;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				[Serializable]
				public ref class Query
				{
				private:
					S1Query _query;
					Int32 _tagCount;
					Int32 _areaCodeCount;
					Location^ _searchLocation;
					Scores^ _scores;
					ArrayList^ _tags;
					ArrayList^ _areaCodes;
				public:
					Query();
					~Query();
					property QuerySorting SortKey
					{
						QuerySorting get();
						void set(QuerySorting);
					}
					property Int32 MaxResults
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 CommunityID
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 GenderMask
					{
						Int32 get();
						void set(Int32);
					}
					property Location^ SearchLocation
					{
						Location^ get();
						void set(Location^);
					}
					property Int16 Radius
					{
						short get();
						void set(Int16);
					}
					property QueryFlag HasPhoto
					{
						QueryFlag get();
						void set(QueryFlag);
					}
					property QueryFlag IsOnline
					{
						QueryFlag get();
						void set(QueryFlag);
					}
					property Int16 AgeMinDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 AgeMaxDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 HeightMinDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 HeightMaxDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int32 WeightMinDesired
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 WeightMaxDesired
					{
						Int32 get();
						void set(Int32);
					}
					property Scores^ Blend
					{
						Scores^ get();
						void set(Scores^);
					}
					property Int16 SchoolID
					{
						Int16 get();
						void set(Int16);
					}
					property Int32 ChildrenCount
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 MoreChildrenFlag
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 ColorCode
					{
						Int32 get();
						void set(Int32);
					}
					void AddParameter(String^ name, String^ value);
					void AddAreaCode(String^ areaCode);
					property S1Query NativeQuery
					{
						S1Query get();
					}
					System::String^ GetDebugTrace(void);
					System::String^ GetString(void);
				};
			};
		};
	};
};