#ifndef INCLUDE_PAPI
	#include "papi.h"
#endif

using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				//Wrapper for unmanaged c string
				[Serializable]
				public ref class StringWrapper
				{
				private:
					IntPtr ip;
				public:
					StringWrapper(String^ string);
					~StringWrapper();
					property STR NativeSTR
					{
						STR get();
					}
				};

				//Creates a managed string from an unmanaged string
				public ref class StringConverter
				{
				public:
					static String^ STRToString(STR str)
					{
						return System::Runtime::InteropServices::Marshal::PtrToStringAnsi(static_cast<IntPtr>(str));
					}
					static String^ CSTRToString(cSTR str)
					{
						return STRToString((STR) str);
					}
					static StringWrapper^ GetE1DateString(DateTime dateTime)
					{
						return gcnew StringWrapper(dateTime.ToString("yyyy-MM-dd HH:mm"));
					}
					static void CheckString(cSTR s, String^ name)
					{
						/*if (!s)
						{
							System::Diagnostics::Trace::WriteLine(name + " was null.");
						}
						else if (strlen(s) == 0)
						{
							System::Diagnostics::Trace::WriteLine(name + " was empty.");
						}*/
					}
				};
			};
		};
	};
};