using namespace Matchnet::Search::Interfaces;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class MemberUpdateSpec
				{
				private:
					MemUpdateSpec _memUpdateSpec;
					StringWrapper^ _activeDate;
				public:
					MemberUpdateSpec(void);
					~MemberUpdateSpec(void);
					void Load(void);

					property Int32 MemberID
					{
						void set(Int32 value);
					}

					property Int32 CommunityID
					{
						void set(Int32 value);
					}

					property DateTime ActiveDate
					{
						void set(DateTime value);
					}

					property Int32 EmailCount
					{
						void set(Int32 value);
					}

					property UpdateFlag HasPhoto
					{
						void set(UpdateFlag value);
					}

					property UpdateFlag IsOnline
					{
						void set(UpdateFlag value);
					}
				};
			};
		};
	};
};