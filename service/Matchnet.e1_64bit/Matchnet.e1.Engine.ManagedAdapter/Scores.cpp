#include "stdafx.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

Scores::Scores()
{
	_s1Scores = new S1ScoresRec();
}

Scores::Scores(S1Scores s1Scores)
{
	_s1Scores = s1Scores;
}

Scores::~Scores()
{
	delete _s1Scores;
}

Byte Scores::Match::get()
{
	return _s1Scores->match;
}
void Scores::Match::set(Byte value)
{
	_s1Scores->match = value;
}

Byte Scores::Age::get()
{
	return _s1Scores->age;
}
void Scores::Age::set(Byte value)
{
	_s1Scores->age = value;
}

Byte Scores::LDG::get()
{
	return _s1Scores->ldg;
}
void Scores::LDG::set(Byte value)
{
	_s1Scores->ldg = value;
}

Byte Scores::Proximity::get()
{
	return _s1Scores->cscz;
}
void Scores::Proximity::set(Byte value)
{
	_s1Scores->cscz = value;
}

Byte Scores::Activity::get()
{
	return _s1Scores->act;
}
void Scores::Activity::set(Byte value)
{
	_s1Scores->act = value;
}

Byte Scores::Popularity::get()
{
	return _s1Scores->pop;
}
void Scores::Popularity::set(Byte value)
{
	_s1Scores->pop = value;
}

Byte Scores::NewReg::get()
{
	return _s1Scores->newreg;
}
void Scores::NewReg::set(Byte value)
{
	_s1Scores->newreg = value;
}
Byte Scores::Height::get()
{
	return _s1Scores->hgt;
}
void Scores::Height::set(Byte value)
{
	_s1Scores->hgt = value;
}

S1Scores Scores::NativeScores::get()
{
	return _s1Scores;
}
