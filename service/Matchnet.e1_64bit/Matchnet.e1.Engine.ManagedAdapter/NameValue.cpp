#include "stdafx.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

NameValue::NameValue()
{
	_memNV = new MemNVRec();
}

NameValue::NameValue(MemNV memNV)
{
	_memNV = memNV;
}

NameValue::NameValue(String^ name, String^ value)
{
	_memNV = new MemNVRec();
	this->Name = name;
	this->Value = value;
}

NameValue::~NameValue()
{
	delete _memNV;
	delete _name;
	delete _value;
}

String^ NameValue::Name::get()
{
	return StringConverter::CSTRToString(_memNV->name);
}

void NameValue::Name::set(String^ s)
{
	_name = gcnew StringWrapper(s);
	_memNV->name = _name->NativeSTR;
}

String^ NameValue::Value::get()
{
	return StringConverter::CSTRToString(_memNV->value);
}

void NameValue::Value::set(String^ s)
{
	_value = gcnew StringWrapper(s);
	_memNV->value = _value->NativeSTR;
}

MemNV NameValue::NativeNameValue::get()
{
	return _memNV;
}