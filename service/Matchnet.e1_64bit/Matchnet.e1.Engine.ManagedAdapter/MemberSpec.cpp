#include "StdAfx.h"
#include "MemberSpec.h"

using namespace System::Collections;

using namespace Matchnet::e1::Engine::ManagedAdapter;

void MemberSpec::init(MemSpec memSpec)
{
	_memSpec = memSpec;
	_memSpec->moreChildrenFlag = 2; //default to not sure
	
	_tags = gcnew ArrayList();
	_tagCount = 0;
}

MemberSpec::MemberSpec()
{
	init(new MemSpecRec());

	_memSpec->emailCount = E1_NULL_INT;
	_memSpec->hasPhoto = E1UpdateFlag_NoChange;
	_memSpec->isOnline = E1UpdateFlag_NoChange;
}

MemberSpec::MemberSpec(MemSpec memSpec)
{
	init(memSpec);

	while (_memSpec->tags[_tagCount].name != 0 && _memSpec->tags[_tagCount].value != 0)
	{
		_tags->Add(gcnew NameValue(&_memSpec->tags[_tagCount]));
		_tagCount++;
	}
}

MemberSpec::~MemberSpec()
{
	delete _memSpec;
	delete _location;
	delete _birthDate;
	delete _registerDate;
	delete _activeDate;

	for (Int32 i = 0; i < _tags->Count; i++)
	{
		delete _tags[i];
	}
	delete _tags;
}

Int32 MemberSpec::MemberID::get()
{
	return _memSpec->memId;
}
void MemberSpec::MemberID::set(Int32 value)
{
	_memSpec->memId = value;
}

Int32 MemberSpec::CommunityID::get()
{
	return _memSpec->domId;
}
void MemberSpec::CommunityID::set(Int32 value)
{
	_memSpec->domId = value;
}

Int32 MemberSpec::GenderMask::get()
{
	return _memSpec->genderMask;
}
void MemberSpec::GenderMask::set(Int32 value)
{
	_memSpec->genderMask = value;
}

Location^ MemberSpec::MemberLocation::get()
{
	if (!_location)
	{
		_location = gcnew Location(&(_memSpec->memLoc));
	}

	return _location;
}
/*void MemberSpec::MemberLocation::set(Location^ value)
{
	_location = value;
	_location->NativeLoc = &(_memSpec->memLoc);
}*/

void MemberSpec::BirthDate::set(DateTime value)
{
	_birthDate = StringConverter::GetE1DateString(value);
	_memSpec->birthDate = _birthDate->NativeSTR;
}

DateTime MemberSpec::BirthDate::get()
{
	return Convert::ToDateTime(StringConverter::STRToString(_memSpec->birthDate));
}

void MemberSpec::RegisterDate::set(DateTime value)
{
	_registerDate = StringConverter::GetE1DateString(value);
	_memSpec->registerDate = _registerDate->NativeSTR;
}

DateTime MemberSpec::RegisterDate::get()
{
	return Convert::ToDateTime(StringConverter::STRToString(_memSpec->registerDate));
}

void MemberSpec::ActiveDate::set(DateTime value)
{
	_activeDate = StringConverter::GetE1DateString(value);
	_memSpec->activeDate = _activeDate->NativeSTR;
}
DateTime MemberSpec::ActiveDate::get()
{
	return Convert::ToDateTime(StringConverter::STRToString(_memSpec->activeDate));
}

Int32 MemberSpec::Height::get()
{
	return _memSpec->height;
}
void MemberSpec::Height::set(Int32 value)
{
	_memSpec->height = value;
}

Int32 MemberSpec::Weight::get()
{
	return _memSpec->weight;
}
void MemberSpec::Weight::set(Int32 value)
{
	_memSpec->weight = value;
}

Int32 MemberSpec::ChildrenCount::get()
{
	return _memSpec->childrenCount;
}
void MemberSpec::ChildrenCount::set(Int32 value)
{
	_memSpec->childrenCount = value;
}

UpdateFlag MemberSpec::HasPhoto::get()
{
	return (UpdateFlag) _memSpec->hasPhoto;
}
void MemberSpec::HasPhoto::set(UpdateFlag value)
{
	_memSpec->hasPhoto = (E1UpdateFlag) value;
}

UpdateFlag MemberSpec::IsOnline::get()
{
	return (UpdateFlag) _memSpec->isOnline;
}
void MemberSpec::IsOnline::set(UpdateFlag value)
{
	_memSpec->isOnline = (E1UpdateFlag) value;
}

Int32 MemberSpec::EmailCount::get()
{
	return _memSpec->emailCount;
}
void MemberSpec::EmailCount::set(Int32 value)
{
	_memSpec->emailCount = value;
}

Byte MemberSpec::MoreChildrenFlag::get()
{
	return _memSpec->moreChildrenFlag;
}

void MemberSpec::MoreChildrenFlag::set(Byte value)
{
	_memSpec->moreChildrenFlag = value;
}

Int32 MemberSpec::ColorCode::get()
{
	return _memSpec->colorcode;
}


void MemberSpec::ColorCode::set(Int32 value)
{
	_memSpec->colorcode = value;
}

ArrayList^ MemberSpec::Tags::get()
{
	return _tags;
}

/*void MemberSpec::AddTag(NameValue ^nameValue)
{
	_memSpec->tags[_tagCount] = *(nameValue->NativeNameValue);
	_tagCount++;
}*/

void MemberSpec::AddTag(String^ name, String^ value)
{
	if (!name || !value || !(name->Length) || !(value->Length))
	{
		System::Diagnostics::Trace::WriteLine("Bad tag added to MemberID " + _memSpec->memId + ".");
		return;
	}

	NameValue^ nv = gcnew NameValue(name, value);
	_memSpec->tags[_tagCount] = *(nv->NativeNameValue);
	_tagCount++;
	_tags->Add(nv);
}

void MemberSpec::Load()
{
	StringConverter::CheckString(_memSpec->activeDate, "activeDate");
	StringConverter::CheckString(_memSpec->birthDate, "birthDate");
	StringConverter::CheckString(_memSpec->memLoc.regAreaCode, "regAreaCode");
	StringConverter::CheckString(_memSpec->memLoc.regCity, "regCity");
	StringConverter::CheckString(_memSpec->memLoc.regCountry, "regCountry");
	StringConverter::CheckString(_memSpec->memLoc.regState, "regState");
	StringConverter::CheckString(_memSpec->memLoc.regZip, "regZip");
	StringConverter::CheckString(_memSpec->registerDate, "registerDate");

	int i = 0;

	try
	{
		MemCreateOrUpdate(_memSpec);
	}
	catch (Exception^ ex)
	{
		throw gcnew Exception("Error creating or updating member.  Debug count: " + _memSpec->debugCount, ex);
	}
}