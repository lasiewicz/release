using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System;

namespace Matchnet.e1.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);

            try
            {
                ServiceBase[] ServicesToRun;

                // More than one user Service may run within the same process. To add
                // another service to this process, change the following line to
                // create a second service object. For example,
                //
                //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
                //
                ServicesToRun = new ServiceBase[] { new EngineService() };

                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                throw new Exception("Error starting service", ex);
            }
        }

        static void MyHandler(object sender, UnhandledExceptionEventArgs args)
        {
            System.Diagnostics.Trace.WriteLine("e1 MyHandler begin");
            Exception ex = (Exception)args.ExceptionObject;
            System.Diagnostics.Trace.WriteLine("e1 MyHandler " + ex.ToString());
            System.Diagnostics.EventLog.WriteEntry("Matchnet.e1.Service", ex.ToString().Substring(0, 4000));
            System.Diagnostics.Trace.WriteLine("e1 MyHandler end");
        }
    }
}