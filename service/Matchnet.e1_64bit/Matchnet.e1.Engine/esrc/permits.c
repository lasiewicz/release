// permits.c
// pnelson 1/10/2005
// Inter thread coordination primitives
//    The one writer must have possesion of the one Mutex
//    Searchers grab one of several search permits from an InterlockedList
//    Memory free's can be queued up waiting for all possible searchers to finish

#include "peng.h"
#include "intrin.h"

#define PKGNAME PkgName_Eng
#define PermitWrite_MutexTimeout 5000L

/****************************************************************/

#ifdef GCC

// In GCC (PCN's debug mode), stub out all the low level primitives
#include "time.h"
#define HANDLE void*
#define DWORD  US4
#ifndef FALSE
#define FALSE  0
#endif
#define WAIT_OBJECT_0    0
#define WAIT_TIMEOUT     1
#define WAIT_ABANDONED   2

typedef struct { void *next; } SLIST_ENTRY;
typedef struct { void *next; } SLIST_HEADER;
static void InitializeSListHead(SLIST_HEADER *r) { r->next = NULL; } 
static void InterlockedPushEntrySList(SLIST_HEADER *r, SLIST_ENTRY *p)
  { p->next = r->next; r->next = p; }
static void *InterlockedPopEntrySList(SLIST_HEADER *r)
  { SLIST_ENTRY *p = r->next; if (p) r->next = p->next; return p; } 
static void InterlockedFlushSList(SLIST_HEADER *r) { r->next = NULL; }

static DWORD WaitForSingleObject(HANDLE m, long timeout) { return WAIT_OBJECT_0; }
static HANDLE CreateMutex(HANDLE m, int b, HANDLE n) { return "MyMutex"; }
static void  ReleaseMutex(HANDLE m) { ; }

typedef long E1Ticks;
static void E1TicksNow(E1Ticks *t)
{
  clock_t now = clock();
  *t = now;
}

S4 E1InterlockedInc(S4 volatile*lp) { return (*lp += 1); }
S4 E1InterlockedDec(S4 volatile*lp) { return (*lp -= 1); }

#else

S4 E1InterlockedInc(S4 volatile*lp) { return InterlockedIncrement(lp); }
S4 E1InterlockedDec(S4 volatile*lp) { return InterlockedDecrement(lp); }
#pragma intrinsic(__rdtsc)
// SuperFast time measure using Pentium-specific RDTSC instruction -- from www.sysinternals.com
//extern static void __declspec(naked) E1TicksNow(E1Ticks* t);
//static void E1TicksNow(E1Ticks* t)
//{
//	/*_asm push ebp
//	_asm push eax
//	_asm push edx
//	_asm _emit 0Fh
//	_asm _emit 31h
//	_asm mov ebp, dword ptr [esp + 0x10]
//	_asm mov DWORD PTR [ebp], eax
//	_asm mov DWORD PTR [ebp + 4], edx
//	_asm pop edx
//	_asm pop eax
//	_asm pop ebp
//	_asm ret*/
//	t=__rdtsc();
//}
#endif

/****************************************************************/


// Holder for expired memory that will be freed at next opportunity
//
#define PkgIsObjFlag  (PkgName)100
#define PkgIsFncFlag  (PkgName)101

typedef struct _PermitExpire *PermitExpire;
typedef struct _PermitExpire {
  PermitExpire next;  // Next in either expire list, or in freelist when waiting for reuse
  E1Ticks   now;   // Time when memory was expired
  PkgName   pkg;   // Pkg that allocated ptr, or PkgIsObjFlag if obj, or PkgIsFncFlag if callback
  union {
	struct { 
	  void    *ptr;   // Memory to free
	  long     size;  // Size of that memory (for tracking) 
	} p;
	struct {
	  ObjMgr   xom;   // Object manager
	  ObjPtr   obj;   // Object to expire
	} o;
	struct { 
	  PermitExpireCBFnc *cb;   // Dispose method on object
	  ObjPtr obj;  // The object to expire
	} f;
  } u;
} PermitExpireRec;

// The holders are allocated in big blocks for efficiency
// 
#define PermitExpireBlockCount 256
typedef struct _PermitExpireBlock *PermitExpireBlock;
typedef struct _PermitExpireBlock {
  PermitExpireBlock next;
  PermitExpireRec   mems[PermitExpireBlockCount];
} PermitExpireBlockRec;

// State of all inter thread synchronization mechanisms
//
typedef struct _GsvPermits {

  E1Ticks now;   // The last time that the clock was checked

  // Write implemented as a Mutex
  HANDLE      writeMutex;   // The OS mutex handle
  cSTR        writeOwner;   // The name of the current owner of the Mutex
  PermitWrite writeRand;    // A random number we give to owner, to make sure return is correct

  // Search implemented as fixed number of permits 
  int              searchCount;    // How many permits/slots are allocated
  SLIST_HEADER     searchHead;     // The internal head of the interlocked list
  PermitSearchRec *searchPermits;  // The actual permits that include start time on active searches
  
  // List of expired memory waiting to be freed
  PermitExpire      expList;   // Head of list of ptrs to be freed when possible
  PermitExpireBlock expBlocks;   // List of all allocated Mem blocks
  PermitExpire      expFree;    // Freelist of PermitExpire objects for reuse
  int            memBlockCnt;    // How many blocks have we allocated

} GsvPermitsRec, *GsvPermits;

// Our global state
static GsvPermitsRec gsvPermits;

/****************************************************************/

static void xPermitExpireDispose(PermitExpire exp);

// Initialize state for Write,Search,MemExpire handling
//
int PermitInitialize(int count)
{
  int i;

  if (gsvPermits.searchPermits || gsvPermits.writeMutex) {
	PEngError(PKGNAME, NULL, "Already Initialized permits");
	return E1ErrorPermit(ERR_Permit_Inited);
  }

  // Initialize the write Mutex
  if ((gsvPermits.writeMutex = CreateMutex(NULL, FALSE, NULL)) == NULL) {
	PEngError(PKGNAME, NULL, "PermitInitizlize - Create Mutex error");
	return E1ErrorPermit(ERR_Permit_MutexCreate);
  }

  // Allocate the read permits
  if (count <= 0)
	count = E1CfgGetInt("cfg/permit/searchCount");
  if (count <= 0)
	count = 16;

  gsvPermits.searchCount   = count;
  gsvPermits.searchPermits = PEngAlloc(PKGNAME, count*sizeof(PermitSearchRec));

  // Initialize the interlock list
  InitializeSListHead(&(gsvPermits.searchHead));

  // Insert all permits on the list in reverse order
  for (i=0; i < count; i++) {
	PermitSearch permit = &(gsvPermits.searchPermits[i]);
	permit->now = 0;
	permit->seq = i;
	(void)InterlockedPushEntrySList(&(gsvPermits.searchHead), &(permit->sList));
  }
  return 0;
}

// Destroy the Permits package. 
// Engine is not useable after this
//
void PermitDestroy()
{
  PermitSearch permit;
  PermitExpire exp;
  PermitExpireBlock block;

  if (!gsvPermits.searchPermits) 
	return;

  // ??? What do do with the write Mutex?

  // Free the search permits
  InterlockedFlushSList(&(gsvPermits.searchHead));
  permit = (PermitSearch)InterlockedPopEntrySList(&(gsvPermits.searchHead));

  if (!permit) {
	free(gsvPermits.searchPermits);
	gsvPermits.searchPermits = NULL;
	gsvPermits.searchCount   = 0;
	memset(&(gsvPermits.searchHead), 0, sizeof(gsvPermits.searchHead));
  }

  // Free any pending expired memory
  while ((exp = gsvPermits.expList)) {
	gsvPermits.expList = exp->next;
	xPermitExpireDispose(exp);
  }
  // Free all the blocks of holders
  while ((block = gsvPermits.expBlocks)) {
	gsvPermits.expBlocks = block->next;
	PEngFree(PKGNAME, block, sizeof(*block));
  }
}

int PermitSearchStats(GsvStats stats)
{
  int i, cnt = gsvPermits.searchCount;
  int cumThread;
  S4  cumQuery;
  E1Ticks cumScan, cumTick;

  cumThread = cumQuery = 0;
  cumScan = cumTick = 0;
  
  // Insert all permits on the list in reverse order
  for (i=0; i < cnt; i++) {
	PermitSearch permit = &(gsvPermits.searchPermits[i]);
	if (permit->cumQuery) {
	  cumThread += 1;
	  cumQuery  += permit->cumQuery;
	  cumScan   += permit->cumScan;
	  cumTick   += permit->cumTick;
	}
  }

  stats->searchPermits = QueryDepthSList(&(gsvPermits.searchHead));
  stats->threads = cumThread;
  stats->queries = cumQuery;
  stats->scansLo = (S4)(cumScan & 0xFFFFFFFF);
  stats->scansHi = (S4)(cumScan / 0x10000 / 0x10000);
  stats->ticks   = (cumQuery > 0 ? (S4)(cumTick / cumQuery) : 0);

  return 0;
}


/****************/

// Allocate a new block of holders for freed memory
//
static int xPermitExpireBlockNew()
{
  PermitExpireBlock block = (PermitExpireBlock)PEngAlloc(PKGNAME, sizeof(PermitExpireBlockRec));
  if (block) {
	int i;
	for (i=0; i < ArrayCount(block->mems); i++) {
	  PermitExpire mem = &(block->mems[i]);
	  mem->next = gsvPermits.expFree;
	  gsvPermits.expFree = mem;
	}
	gsvPermits.memBlockCnt += 1;
	return 0;
  } else {
	return E1ErrorPermit(ERR_Permit_Alloc);
  }
}

// Fetch a holder for this mem we're expiring
//
static PermitExpire xPermitExpirePop()
{
  PermitExpire mem = NULL;

  // Find holder for this ptr while we're waiting to expire
  if (!gsvPermits.expFree) {
	  // Need more holders
	  if (xPermitExpireBlockNew()) {
		  E1ErrorPermit(ERR_Permit_Alloc);
		  return NULL; // ??? what to do if there's no place to hold it? It's lost for now
	  }
  }

  // Now know we have holders, but check again just in case
  if (gsvPermits.expFree) {
	// Pop a holder off the free list - ok cause only 1 writer
	mem = gsvPermits.expFree;
	gsvPermits.expFree = mem->next;
	// Set up expiration time
	//E1TicksNow(&(mem->now));
	mem->now=__rdtsc();
  }
  return mem;
}

// Push the holder onto the "to be expired" list
//
static void xPermitExpirePush(PermitExpire mem)
{
  mem->next = gsvPermits.expList;
  gsvPermits.expList = mem;
}

// Dispose of the memory based on its type when the time has come
//
static void xPermitExpireDispose(PermitExpire exp)
{
  switch (exp->pkg) {
  case 0:
	break;
  case PkgIsObjFlag:
	if (exp->u.o.obj && exp->u.o.xom) {
	  ObjMgrFree(exp->u.o.xom, exp->u.o.obj);
	  exp->u.o.obj = NULL;
	}
	break;
  case PkgIsFncFlag:
	if (exp->u.f.obj && exp->u.f.cb) {
	  (*(exp->u.f.cb))(exp->u.f.obj);
	  exp->u.o.obj = NULL;
	}
	break;
  default:
	if (exp->u.p.ptr) {
	  PEngFree(exp->pkg, exp->u.p.ptr, exp->u.p.size);
	  exp->u.p.ptr = NULL;
	}
	break;
  }
  exp->pkg = 0; 
}

// Can only be called by the one permit holding writer 
// Set up this ptr be freed as soon as all possible readers have finished
//
void PermitExpirePtr(PkgName pkg, void *ptr, US4 size)
{
  PermitExpire exp;
  if (ptr) {
	if ((exp = xPermitExpirePop())) {
	  exp->pkg  = pkg;
	  exp->u.p.ptr  = ptr;
	  exp->u.p.size = size;
	  xPermitExpirePush(exp);
	}
  }
}

// Can only be called by the one permit holding writer 
// Set up this obj be recycled as soon as all possible readers have finished
//
void PermitExpireObj(ObjMgr xom, ObjPtr obj)
{
  PermitExpire exp;
  if (xom && obj) {
	if ((exp = xPermitExpirePop())) {
	  exp->pkg   = PkgIsObjFlag;
	  exp->u.o.xom = xom;
	  exp->u.o.obj = obj;
	  xPermitExpirePush(exp);
	}
  }
}

// Can only be called by the one permit holding writer 
// Set up this obj be recycled as soon as all possible readers have finished
//
void PermitExpireCB(PermitExpireCBFnc *cb, ObjPtr obj)
{
  PermitExpire exp;
  if (cb && obj) {
	if ((exp = xPermitExpirePop())) {
	  exp->pkg   = PkgIsFncFlag;
	  exp->u.f.cb  = cb;
	  exp->u.f.obj = obj;
	  xPermitExpirePush(exp);
	}
  }
}

// !!! Optimize -- keep to-expire list sorted oldest to youngest

/// <summary>Free any expired memory that is not possibly in use</summary>
/// <returns>How many object handles were freed</returns>
/// Can only be called by the one permit holding writer 
///
int PermitExpireExec()
{
  int count = 0, i;
  if (gsvPermits.expList) {

	PermitExpire *root, mem;
	E1Ticks ticks, lowMark;

	// Find the time now
	//E1TicksNow(&ticks);
	ticks=__rdtsc();
	gsvPermits.now = ticks;

	// Find oldest search
	lowMark = ticks;
	for (i = 0; i < gsvPermits.searchCount; i++)
	  if (ticks = gsvPermits.searchPermits[i].now)
		if (ticks < lowMark)
		  lowMark = ticks;
	
	// Free any memory which expired before oldest search began
	root = &(gsvPermits.expList); 
	while ((mem = *root)) {
	  if (mem->now < lowMark) {
		// Goodbye to the memory
		++count;
		xPermitExpireDispose(mem);
		// Cut this holder out of the expire list, and set up root to look at next
		*root = mem->next;   
		// Add this holder to the free list
		mem->next = gsvPermits.expFree;
		gsvPermits.expFree = mem;
	  } else {
		// See if we can free the next one
		root = &(mem->next);
	  }
	}
	gsv->statsExpires += count;
  }
  return count;
}

/// <summary>If possible, free any expired memory that is no longer in use</summary>
/// <returns>How many object handles were freed</returns>
/// Grabs the write permit if needed 
///
int PermitMaintain()
{
	int retVal = 0;
	PermitWrite myPermit = (PermitWrite)0;

	if (gsvPermits.expList) {
	  // Something to delete
	  if (gsvPermits.writeRand == 0) {
		  // No one has the permit
		  if (0 == PermitWriteTake("PermitMaintain", &myPermit, 0)) {
			  // We have it
			  retVal = PermitExpireExec();
			  PermitWriteGive(myPermit);
		  }
	  }
	}
	return retVal;
}

/****************/

// Grab the Write Mutex, seqp will be set to rand number which must be returned by owner
// 
int PermitWriteTake(cSTR name, PermitWrite *seqp, int memberID)
{
  // Request ownership of mutex.
  DWORD dwWaitResult = WaitForSingleObject(gsvPermits.writeMutex, PermitWrite_MutexTimeout);

  switch (dwWaitResult) {
  case WAIT_OBJECT_0: 
	// Got it!
	//E1TicksNow(&(gsvPermits.now));
	gsvPermits.now=__rdtsc();
	gsvPermits.writeOwner = name;
	gsvPermits.writeRand  = *seqp = (long)(gsvPermits.now & 0x00FFFFFF);
	gsv->currentMemberID = memberID;
	return 0;

  case WAIT_TIMEOUT:  // Time-out.
	return E1ErrorPermit(ERR_PermitWrite_Timeout);

  case WAIT_ABANDONED:	// Earlier owner abandoned ???
	return E1ErrorPermit(ERR_PermitWrite_Abandon);

  default:
	return E1ErrorPermit(ERR_PermitWrite_Unknown);
  }
}

// Confirm that the write permit is held by this thread
//
int PermitWriteConfirm(PermitWrite seq)
{
  // Make sure this is the real owner of the Mutex
	if (!seq)
		return E1ErrorPermit(ERR_Permit_Null);
	if (seq != gsvPermits.writeRand)
		return E1ErrorPermit(ERR_PermitWrite_NotOwner);
	return 0;
}

// Return the write Mutex, freeing any expired memory while we're here
//
int PermitWriteGive(PermitWrite seq)
{
  // Make sure this is the real owner of the Mutex
  if (seq != gsvPermits.writeRand)
	return E1ErrorPermit(ERR_PermitWrite_NotOwner);

  // Free up any memory that might be waiting
  // ??? Has been moved to Maintain call
  // if (gsvPermits.expList) PermitExpireExec();
  
  // Give back the Mutex
  gsvPermits.writeRand = 0;
  gsvPermits.writeOwner = NULL;
  gsv->currentMemberID = 0;
  ReleaseMutex(gsvPermits.writeMutex);
  return 0;
}

/****************/

// Take a search permit so you can run a search
//
int PermitSearchTake(PermitSearch *permitp)
{
  PermitSearch permit = (PermitSearch)InterlockedPopEntrySList(&(gsvPermits.searchHead));
  *permitp = NULL;

  if (!permit)
	return E1ErrorPermit(ERR_PermitSearch_NoneLeft);

  if (permit->now != 0) {
	  char msgBuf[128];
	  snprintf(msgBuf, sizeof(msgBuf), "Permit(%d) already in use (0x%I64x) on Take()", permit->seq, permit->now);
	  PEngError(PKGNAME, NULL, msgBuf);
	InterlockedPushEntrySList(&(gsvPermits.searchHead), &(permit->sList));
	return E1ErrorPermit(ERR_PermitSearch_InUse);
  }

  // "->now" will be set when search begins 
  *permitp = permit;
  return 0;
}

// Give back search permit, wanr if it still seems to be in use
//
int PermitSearchGive(PermitSearch permit)
{
  int err = 0;

  if (!permit)
	return E1ErrorPermit(ERR_PermitSearch_Null);

  if (permit->now)
	err = E1ErrorPermit(WARN_PermitSearch_InUse);

  InterlockedPushEntrySList(&(gsvPermits.searchHead), &(permit->sList));
  return err;
}

// Begin a search -- flags the start time
//
int PermitSearchBeg(PermitSearch permit)
{
  if (!permit)
	return E1ErrorPermit(ERR_PermitSearch_Null);
  if (permit->now)
	return E1ErrorPermit(ERR_PermitSearch_InUse);

  // Remember the time
  //E1TicksNow(&(permit->now));
  permit->now=__rdtsc();
  gsvPermits.now = permit->now;
  return 0;
}

// End a search -- null the start time, permit can be re-used w/ Give/Take
//
int PermitSearchEnd(PermitSearch permit, S4 scans)
{
  E1Ticks rightNow;

  if (!permit)
	return E1ErrorPermit(ERR_PermitSearch_Null);
  if (permit->now == 0)
	return E1ErrorPermit(ERR_PermitSearch_NotInUse);

  //E1TicksNow(&rightNow);
  rightNow=__rdtsc();
  permit->cumQuery += 1;
  permit->cumScan += scans;
  permit->cumTick += (rightNow - permit->now);

  // No search running anymore
  permit->now = 0;
  return 0;
}

