// stx.c
// pnelson 11/13/2005
// Map strings to keys w/ fast lookup

#include "peng.h"

#define PKGNAME PkgName_Stx

#ifdef PENGDEBUG
static StxDb stxStopAtColl;
static S4    stxStopAtSeq;
#endif

/****************/

#define kStx_InstLenMax 1024

typedef struct _StxInst *StxInst;   // Instance of a key'd string
typedef struct _StxInst {
	StxInst hashNext;
	Stx   sequence;
	char   value[1]; // Will be alloced larger, this byte for null terminator 
} StxInstRec;

// How big to actually allocate it
#define StxInstAllocSize(vlen) (sizeof(StxInst) + sizeof(Stx) + 1 + (vlen))

typedef struct _StxDb {
	char   name[16];
	US4    maxSize;
	US4    curInst;    // Next one to assign

	StxInst *hashHead;  // head of hash buckets 
	StxInst **pages;    // arrays of ptrs to all records
	US2 perPage;       // how many per page
	US2 maxPage;       // how many pages
	S4  maxHash;       // how big is hash table
	US1 modHash;       // Helper to generate hashkey
} StxDbRec;
	
// Create a collection of String<>ID mappings
StxDb StxDbNew(US1 maxSizeBits, cSTR name)
{
	StxDb xstxdb = NULL;
	US1 modHash;

	// All sizes in powers of 2 
	US4 maxSize = (maxSizeBits < 7 ? 7 : maxSizeBits); // No less then 2^7 total
	US4 perPage = (maxSize < 14 ? 6 : maxSize-8);      // Pages at least 2^6
	US4 maxPage = maxSize - perPage;                   // How many pages
	US4 maxHash = maxSize - 4;                         // How big is hash table

	if (maxHash < 7) maxHash = 7;
	modHash = (US1)maxHash - 3;

	// Now real sizes
	maxSize = (1L << maxSize);
	perPage = (1L << perPage);
	maxPage = (1L << maxPage);
	maxHash = (1L << maxHash);

	{
		// Allocate memory including first page
		US4 sSize = StructSizeAlign(StxDbRec);
		US4 pSize = maxPage * sizeof(StxInst*);
		US4 hSize = maxHash * sizeof(StxInst);
		US4 xSize = perPage * sizeof(StxInst);
		US1 *data = PEngAlloc(PKGNAME, sSize+pSize+hSize+xSize);

		xstxdb = (StxDb)data; data += sSize;
		xstxdb->pages    = (StxInst**)data; data += pSize;
		xstxdb->hashHead = (StxInst*)data;  data += hSize;
		xstxdb->pages[0] = (StxInst*)data;  data += xSize;
	}

	// Load it
	str0ncpy(xstxdb->name, name, sizeof(xstxdb->name));
	xstxdb->maxSize = maxSize;
	xstxdb->perPage = (US2)perPage;
	xstxdb->maxPage = (US2)maxPage;
	xstxdb->maxHash = maxHash;
	xstxdb->modHash = modHash;   // after shifting this many bits, start over

	xstxdb->curInst = 1; // Don't give out zero as key so seed at 1

	return xstxdb;
}

void StxDbFree(StxDb xstxdb)
{
	;
}

#define valBufSize kStx_InstLenMax

// Find String if we know it
//
static Stx StxDbFindAux(StxDb xstxdb, cSTR value, STR valBuf, S4 *hashIdxp)
{
  int i; US4 ix, hashIdx;
  StxInst *root, scan;
  
  // Copy and lowercase the string
  str0ncpy(valBuf, value, valBufSize);
  strlwr(valBuf);
  
  // Generate hash key for this value
  hashIdx = 0;
  for (i=0; (ix = valBuf[i]); i++) {
	if (ix >= '0' && ix <= '9')
	  hashIdx ^= (ix - '0' + 1) * 157 * (hashIdx+1);
	else
	  hashIdx ^= ((ix-32) << (i % xstxdb->modHash));
  }
  *hashIdxp = (hashIdx %= xstxdb->maxHash);
  
  // Find it
  for (root = &(xstxdb->hashHead[hashIdx]); (scan = *root); root = &(scan->hashNext)) {
	int cmp = strcmp(valBuf, scan->value);
	if (cmp == 0) // Got it
	  return scan->sequence;
	if (cmp < 0)
	  break;
  }
  return 0;
}

Stx StxDbFind(StxDb xstxdb, cSTR value)
{
  S4 hashIdx;
  char valBuf[valBufSize];
  if (!xstxdb || !value) 
	return 0;
  return StxDbFindAux(xstxdb, value, valBuf, &hashIdx);
}


// Assign (creating if needed) an ID to this string
//
Stx StxDbFindOrNew(StxDb xstxdb, cSTR value)
{
	int vlen; S4 hashIdx; Stx xid;
	StxInst *root, xinst, scan;
	US4 curInst, curPage, curSeq, mSize;
	char valBuf[valBufSize];

	if (!xstxdb || !value) 
		return 0;

	if ((xid = StxDbFindAux(xstxdb, value, valBuf, &hashIdx)))
		return xid;

	// Create it
	vlen = strlen(valBuf);
	xinst  = PEngAlloc(PKGNAME, StxInstAllocSize(vlen));
	memcpy(xinst->value, valBuf, vlen+1);

	// Make sure there's a page of memory to store it in sequence
	curInst = xstxdb->curInst;
	curPage = curInst / xstxdb->perPage;
	curSeq  = curInst % xstxdb->perPage;

	if (curSeq == 0) {
		// Need a new page
		if (curPage >= xstxdb->maxPage) {
			snprintf(valBuf, sizeof(valBuf), "Coll(%s) can't have %ld entries", 
					 xstxdb->name, curInst); 
			PEngError(PKGNAME, valBuf, "FindOrNew");
			return 0;
		}
		mSize = xstxdb->perPage * sizeof(StxInst);
		xstxdb->pages[curPage] = PEngAlloc(PKGNAME, mSize);
	}

	// Assign an ID
	xinst->sequence = xstxdb->curInst++;
	xstxdb->pages[curPage][curSeq] = xinst;

	// Splice it in in sorted order
	for (root = &(xstxdb->hashHead[hashIdx]); (scan = *root); root = &(scan->hashNext)) {
		int cmp = strcmp(xinst->value, scan->value);
		if (cmp <= 0) 
			break;
	}
	xinst->hashNext = scan; 
	*root = xinst; 

#ifdef PENGDEBUG
	if (stxStopAtColl == xstxdb)
		if (stxStopAtSeq == xinst->sequence)
			stxStopAtColl = xstxdb;
#endif

	return xinst->sequence;
}

// Find a string by it's ID
//
cSTR StxDbById(StxDb xstxdb, Stx xid)
{
	US4 pNum = xid / xstxdb->perPage;
	US4 sNum = xid % xstxdb->perPage;
	StxInst *page, xpki;

	if (pNum >= xstxdb->maxPage)
		return NULL;
	if (!(page = xstxdb->pages[pNum]))
		return NULL;
	if (!(xpki = page[sNum]))
		return NULL;
	return xpki->value;
}

// *** DEBUG ***

S4 StxDbCount(StxDb xstxdb)
{
	if (!xstxdb) return 0;
	return xstxdb->curInst;
}

#define MAXHISTO 128
// When mode>0, perhaps show more strings
//
int StxDbXml(PXml px, StxDb xstxdb, int mode)
{
  int err; S4 i, j, histo[MAXHISTO];
  int tooLong, lenLong;
  char msgBuf[128];

  if (!px || !xstxdb) return 0;
  
  snprintf(msgBuf, sizeof(msgBuf), "StxDb name='%s' count='%ld'", 
		   xstxdb->name, xstxdb->curInst);
  err = PXmlPush(px, msgBuf, 0);
  if (err < 0)
	return err;
  if (err == 0) {

	snprintf(msgBuf, sizeof(msgBuf), 
			 "Stats maxSize='%ld' perPage='%d' maxPage='%d' maxHash='%ld' modHash='%d'",
			 xstxdb->maxSize, xstxdb->perPage, xstxdb->maxPage, xstxdb->maxHash, xstxdb->modHash);
	err |= PXmlPush0(px, msgBuf, 0);

	StructNull(histo);
	tooLong = lenLong = 0;
	for (i = 0; i < xstxdb->maxHash; i++) {
	  StxInst scan = xstxdb->hashHead[i];
	  for (j=0; scan && !err; j++, scan = scan->hashNext) {
		if (mode & 8) {
		  snprintf(msgBuf, sizeof(msgBuf), "Stx hidx='%ld' seq='%ld'", i, scan->sequence);
		  err |= PXmlSetStr(px, msgBuf, scan->value);
		}
	  }
	  if (j < MAXHISTO) {
		histo[j] += 1; 
	  } else {
		tooLong += 1;
		if (j > lenLong) lenLong = j;
	  }
	}
	err |= PXmlSetNum(px, "longLists",   tooLong);
	err |= PXmlSetNum(px, "longLongest", lenLong);

	{
	  err |= PXmlPush(px, "HistoChains", MAXHISTO);
	  for (i = 0; i < MAXHISTO && !err; i++)
		if (histo[i]) {
		  char buf[64]; 
		  snprintf(buf, sizeof(buf), "Chain len='%ld' cnt='%ld'", i, histo[i]);
		  err |= PXmlPush0(px, buf, 0);
		}
	  err |= PXmlPop(px);
	}
  }
  err |= PXmlPop(px);
  return err;
}

/****************/

int StxCheck(StxDb xdb, StxCheckFnc cFnc, E1CheckErr err)
{
	S4 idx; StxInst obj;
	if (xdb) {
		for (idx=0; idx < xdb->maxHash; idx++)
			for (obj = xdb->hashHead[idx]; obj; obj = obj->hashNext)
				if (err->errCode = (*cFnc)((cSTR)obj->value, (S4)obj->sequence, err))
					return err->errCode;
	}
	return 0;
}
