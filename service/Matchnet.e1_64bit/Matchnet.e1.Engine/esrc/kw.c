/* C code produced by gperf version 2.7.2 */
/* Command-line: gperf -a -C -G -t -T -N KWLookupAux -r -p --switch=1 -k '*' -D  */
// kw.src & kw.c == use gperf to quickly match E1 keywords
// Build kw.c from kw.src with the following:
//    cat kw.src | tr -d '\015' | gperf -a -C -G -t -T -N KWLookupAux -r -p --switch=1 -k '*' -D > kw.c
// Use _kwunit.c to test 

#include "string.h"
#ifndef strlwr
#define strlwr   _strlwr
#endif
#include "kw.h"


#define TOTAL_KEYWORDS 188
#define MIN_WORD_LENGTH 1
#define MAX_WORD_LENGTH 23
#define MIN_HASH_VALUE 44
#define MAX_HASH_VALUE 4213
/* maximum key range = 4170, duplicates = 10 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (str, len)
     register const char *str;
     register unsigned int len;
{
  static const unsigned short asso_values[] =
    {
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214,   58, 4214, 4214,   84,
        21,  241,   90, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214,  250,  212,   73,
       206,  220,  119,   48,    5,  119,  151,   87,  213,   33,
        53,  213,  146,  152,  220,  123,  228,   69,   43,   20,
        33,   96,  100, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214, 4214,
      4214, 4214, 4214, 4214, 4214, 4214
    };
  register int hval = len;

  switch (hval)
    {
      default:
      case 23:
        hval += asso_values[(unsigned char)str[22]];
      case 22:
        hval += asso_values[(unsigned char)str[21]];
      case 21:
        hval += asso_values[(unsigned char)str[20]];
      case 20:
        hval += asso_values[(unsigned char)str[19]];
      case 19:
        hval += asso_values[(unsigned char)str[18]];
      case 18:
        hval += asso_values[(unsigned char)str[17]];
      case 17:
        hval += asso_values[(unsigned char)str[16]];
      case 16:
        hval += asso_values[(unsigned char)str[15]];
      case 15:
        hval += asso_values[(unsigned char)str[14]];
      case 14:
        hval += asso_values[(unsigned char)str[13]];
      case 13:
        hval += asso_values[(unsigned char)str[12]];
      case 12:
        hval += asso_values[(unsigned char)str[11]];
      case 11:
        hval += asso_values[(unsigned char)str[10]];
      case 10:
        hval += asso_values[(unsigned char)str[9]];
      case 9:
        hval += asso_values[(unsigned char)str[8]];
      case 8:
        hval += asso_values[(unsigned char)str[7]];
      case 7:
        hval += asso_values[(unsigned char)str[6]];
      case 6:
        hval += asso_values[(unsigned char)str[5]];
      case 5:
        hval += asso_values[(unsigned char)str[4]];
      case 4:
        hval += asso_values[(unsigned char)str[3]];
      case 3:
        hval += asso_values[(unsigned char)str[2]];
      case 2:
        hval += asso_values[(unsigned char)str[1]];
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval;
}

static const E1KwRec wordlist[] =
  {
    {"v",                              KW_value},
    {"n",                              KW_name},
    {"c",                              KW_code},
    {"gsv",                            KW_gsv},
    {"hgt",                            KW_hgt},
    {"mem",                            KW_mem},
    {"wgt",                            KW_wgt},
    {"cmd",                            KW_cmd},
    {"show",                           KW_show},
    {"zip",                            KW_zip},
    {"xsl",                            KW_xsl},
    {"grp",                            KW_grp},
    {"music",                          KW_music},
    {"flng",                           KW_fltlng},
    {"check",                          KW_check},
    {"ldg",                            KW_ldg},
    {"hgtmin",                         KW_hgtmin},
    {"minhgt",                         KW_hgtmin},
    {"seq",                            KW_seq},
    {"wgtmin",                         KW_wgtmin},
    {"minwgt",                         KW_wgtmin},
    {"city",                           KW_city},
    {"age",                            KW_age},
    {"tag",                            KW_tag},
    {"blng",                           KW_boxlng},
    {"null",                           KW_null},
    {"name",                           KW_name},
    {"quit",                           KW_quit},
    {"help",                           KW_help},
    {"hgtmax",                         KW_hgtmax},
    {"maxhgt",                         KW_hgtmax},
    {"time",                           KW_time},
    {"memid",                          KW_memid},
    {"wgtmax",                         KW_wgtmax},
    {"maxwgt",                         KW_wgtmax},
    {"height",                         KW_hgt},
    {"weight",                         KW_wgt},
    {"file",                           KW_file},
    {"mode",                           KW_mode},
    {"type",                           KW_type},
    {"code",                           KW_code},
    {"coding",                         KW_coding},
    {"pets",                           KW_pets},
    {"hgt.inmin",           KW_rt_hgt_inmin},
    {"agemin",                         KW_agemin},
    {"minage",                         KW_agemin},
    {"boxlng",                         KW_boxlng},
    {"domid",                          KW_domid},
    {"cuisine",                        KW_cuisine},
    {"value",                          KW_value},
    {"rt.new",              KW_rt_new},
    {"reglng",                         KW_fltlng},
    {"flat",                           KW_fltlat},
    {"hgt.inmax",           KW_rt_hgt_inmax},
    {"heightmin",                      KW_hgtmin},
    {"minheight",                      KW_hgtmin},
    {"agemax",                         KW_agemax},
    {"maxage",                         KW_agemax},
    {"weightmin",                      KW_wgtmin},
    {"minweight",                      KW_wgtmin},
    {"fltlng",                         KW_fltlng},
    {"sapop",                          KW_sapop},
    {"resmax",                         KW_resmax},
    {"load",                           KW_load},
    {"search",                         KW_search},
    {"blat",                           KW_boxlat},
    {"def.max",             KW_rt_defmax},
    {"saact",                          KW_saact},
    {"cscz.reg2",           KW_rt_cscz_reg2},
    {"tagdb",                          KW_tagdb},
    {"heightmax",                      KW_hgtmax},
    {"maxheight",                      KW_hgtmax},
    {"country",                        KW_country},
    {"age.inmin",           KW_rt_age_inmin},
    {"weightmax",                      KW_wgtmax},
    {"maxweight",                      KW_wgtmax},
    {"zodiac",                         KW_zodiac},
    {"gender",                         KW_gender},
    {"childcnt",                       KW_childcnt},
    {"rt.ldg",              KW_rt_ldg},
    {"rtb.hgt",             KW_rtb_hgt},
    {"cscz.reg1",           KW_rt_cscz_reg1},
    {"cscz.reg4",           KW_rt_cscz_reg4},
    {"custody",                        KW_custody},
    {"reset",                          KW_reset},
    {"rt.pop",              KW_rt_pop},
    {"rtb.new",             KW_rtb_new},
    {"age.pct",             KW_rt_age_pct},
    {"noauto",                         KW_noauto},
    {"hgt.percm",           KW_rt_hgt_percm},
    {"state",                          KW_state},
    {"rt.act",              KW_rt_act},
    {"age.inmax",           KW_rt_age_inmax},
    {"rtb.cscz",            KW_rtb_cscz},
    {"maintain",                       KW_maintain},
    {"isonline",                       KW_isonline},
    {"reading",                        KW_reading},
    {"update",                         KW_update},
    {"header",							KW_header},
    {"ethnicity",                      KW_ethnicity},
    {"boxlat",                         KW_boxlat},
    {"cscz.reg3",           KW_rt_cscz_reg3},
    {"schoolid",                       KW_schoolid},
    {"hgt.outmax",          KW_rt_hgt_outmax},
    {"rtb.mol",             KW_rtb_mol},
    {"reglat",                         KW_fltlat},
    {"ranktune",            KW_ranktune},
    {"hasphoto",                       KW_hasphoto},
    {"rtb.ldg",             KW_rtb_ldg},
    {"sortkey",                        KW_sortkey},
    {"domainid",                       KW_domid},
    {"religion",                       KW_religion},
    {"rtb.pop",             KW_rtb_pop},
    {"planets",                        KW_planets},
    {"rtb.age",             KW_rtb_age},
    {"communityid",                    KW_domid},
    {"fltlat",                         KW_fltlat},
    {"memberid",                       KW_memid},
    {"rtb.act",             KW_rtb_act},
    {"distance",                       KW_distance},
    {"def.dist",            KW_rt_defdist},
    {"age.algo",            KW_rt_age_algo},
    {"delete",                         KW_delete},
    {"rtb.match",           KW_rtb_match},
    {"longitude",                      KW_fltlng},
    {"def.sort",            KW_rt_defsort},
    {"age.outmax",          KW_rt_age_outmax},
    {"bodytype",                       KW_bodytype},
    {"bodyart",                        KW_bodyart},
    {"gendermask",                     KW_gendermask},
    {"eyecolor",                       KW_eyecolor},
    {"emailcount",                     KW_emailcount},
    {"age.peryr",           KW_rt_age_peryr},
    {"rtb.sort",            KW_rtb_sort},
    {"haircolor",                      KW_haircolor},
    {"latitude",                       KW_fltlat},
    {"keepkosher",                     KW_keepkosher},
    {"majortype",                      KW_majortype},
    {"smokinghabits",                  KW_smokinghabits},
    {"incomelevel",                    KW_incomelevel},
    {"havechildren",                   KW_havechildren},
    {"relocate",                       KW_relocate},
    {"languagemask",                   KW_languagemask},
    {"areacode",                       KW_areacode},
    {"birthdate",                      KW_birthdate},
    {"childrencount",                  KW_childcnt},
    {"industrytype",                   KW_industrytype},
    {"hasphotoflag",                   KW_hasphoto},
    {"activedate",                     KW_activedate},
    {"drinkinghabits",                 KW_drinkinghabits},
    {"moviegenremask",                 KW_moviegenremask},
    {"habitattype",                    KW_habitattype},
    {"updatedate",                     KW_updatedate},
    {"depth2regionid",                 KW_state},
    {"depth1regionid",                 KW_country},
    {"depth4regionid",                 KW_zip},
    {"physicalactivity",               KW_physicalactivity},
    {"jdateethnicity",                 KW_jdateethnicity},
    {"depth3regionid",                 KW_city},
    {"jdatereligion",                  KW_jdatereligion},
    {"relocateflag",                   KW_relocate},
    {"registerdate",                   KW_registerdate},
    {"maritalstatus",                  KW_maritalstatus},
    {"educationlevel",                 KW_educationlevel},
    {"leisureactivity",                KW_leisureactivity},
    {"relationshipmask",               KW_relationshipmask},
    {"morechildrenflag",               KW_morechildrenflag},
    {"lastactivedate",                 KW_activedate},
    {"communityinsertdate",            KW_registerdate},
    {"israelareacode",                 KW_israelareacode},
    {"favoritetimeofday",              KW_favoritetimeofday},
    {"synagogueattendance",            KW_synagogueattendance},
    {"personalitytrait",               KW_personalitytrait},
    {"desiredsmokinghabits",           KW_desiredsmokinghabits},
    {"relationshipstatus",             KW_relationshipstatus},
    {"militaryservicetype",            KW_militaryservicetype},
    {"desireddrinkinghabits",          KW_desireddrinkinghabits},
    {"religionactivitylevel",          KW_religionactivitylevel},
    {"intelligenceimportance",         KW_intelligenceimportance},
    {"isrealimmigrationyear",          KW_isrealimmigrationyear},
    {"appearanceimportance",           KW_appearanceimportance},
    {"israelimmigrationdate",          KW_israelimmigrationdate},
    {"entertainmentlocation",          KW_entertainmentlocation},
    {"politicalorientation",           KW_politicalorientation},
    {"desiredjdatereligion",           KW_desiredjdatereligion},
    {"desiredmaritalstatus",           KW_desiredmaritalstatus},
    {"desirededucationlevel",          KW_desirededucationlevel},
    {"israelipersonalitytrait",        KW_israelipersonalitytrait},
	{"colorcode",        KW_colorcode},
  };

#ifdef __GNUC__
__inline
#endif
const E1KwRec *
KWLookupAux (str, len)
     register const char *str;
     register unsigned int len;
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= MIN_HASH_VALUE)
        {
          register const E1KwRec *wordptr;
          register const E1KwRec *wordendptr;
          register const E1KwRec *resword;

          switch (key - 44)
            {
              case 0:
                resword = &wordlist[0];
                goto compare;
              case 10:
                resword = &wordlist[1];
                goto compare;
              case 30:
                resword = &wordlist[2];
                goto compare;
              case 173:
                resword = &wordlist[3];
                goto compare;
              case 240:
                resword = &wordlist[4];
                goto compare;
              case 245:
                resword = &wordlist[5];
                goto compare;
              case 255:
                resword = &wordlist[6];
                goto compare;
              case 271:
                resword = &wordlist[7];
                goto compare;
              case 321:
                resword = &wordlist[8];
                goto compare;
              case 324:
                resword = &wordlist[9];
                goto compare;
              case 328:
                resword = &wordlist[10];
                goto compare;
              case 373:
                resword = &wordlist[11];
                goto compare;
              case 378:
                resword = &wordlist[12];
                goto compare;
              case 393:
                resword = &wordlist[13];
                goto compare;
              case 419:
                resword = &wordlist[14];
                goto compare;
              case 426:
                resword = &wordlist[15];
                goto compare;
              case 448:
                wordptr = &wordlist[16];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 454:
                resword = &wordlist[18];
                goto compare;
              case 463:
                wordptr = &wordlist[19];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 476:
                resword = &wordlist[21];
                goto compare;
              case 477:
                resword = &wordlist[22];
                goto compare;
              case 485:
                resword = &wordlist[23];
                goto compare;
              case 486:
                resword = &wordlist[24];
                goto compare;
              case 508:
                resword = &wordlist[25];
                goto compare;
              case 516:
                resword = &wordlist[26];
                goto compare;
              case 528:
                resword = &wordlist[27];
                goto compare;
              case 544:
                resword = &wordlist[28];
                goto compare;
              case 559:
                wordptr = &wordlist[29];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 560:
                resword = &wordlist[31];
                goto compare;
              case 572:
                resword = &wordlist[32];
                goto compare;
              case 574:
                wordptr = &wordlist[33];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 587:
                resword = &wordlist[35];
                goto compare;
              case 602:
                resword = &wordlist[36];
                goto compare;
              case 631:
                resword = &wordlist[37];
                goto compare;
              case 632:
                resword = &wordlist[38];
                goto compare;
              case 650:
                resword = &wordlist[39];
                goto compare;
              case 672:
                resword = &wordlist[40];
                goto compare;
              case 674:
                resword = &wordlist[41];
                goto compare;
              case 677:
                resword = &wordlist[42];
                goto compare;
              case 681:
                resword = &wordlist[43];
                goto compare;
              case 685:
                wordptr = &wordlist[44];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 734:
                resword = &wordlist[46];
                goto compare;
              case 738:
                resword = &wordlist[47];
                goto compare;
              case 739:
                resword = &wordlist[48];
                goto compare;
              case 756:
                resword = &wordlist[49];
                goto compare;
              case 761:
                resword = &wordlist[50];
                goto compare;
              case 764:
                resword = &wordlist[51];
                goto compare;
              case 770:
                resword = &wordlist[52];
                goto compare;
              case 792:
                resword = &wordlist[53];
                goto compare;
              case 795:
                wordptr = &wordlist[54];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 796:
                wordptr = &wordlist[56];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 810:
                wordptr = &wordlist[58];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 836:
                resword = &wordlist[60];
                goto compare;
              case 839:
                resword = &wordlist[61];
                goto compare;
              case 841:
                resword = &wordlist[62];
                goto compare;
              case 842:
                resword = &wordlist[63];
                goto compare;
              case 853:
                resword = &wordlist[64];
                goto compare;
              case 863:
                resword = &wordlist[65];
                goto compare;
              case 882:
                resword = &wordlist[66];
                goto compare;
              case 885:
                resword = &wordlist[67];
                goto compare;
              case 901:
                resword = &wordlist[68];
                goto compare;
              case 905:
                resword = &wordlist[69];
                goto compare;
              case 906:
                wordptr = &wordlist[70];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 915:
                resword = &wordlist[72];
                goto compare;
              case 918:
                resword = &wordlist[73];
                goto compare;
              case 921:
                wordptr = &wordlist[74];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 923:
                resword = &wordlist[76];
                goto compare;
              case 929:
                resword = &wordlist[77];
                goto compare;
              case 934:
                resword = &wordlist[78];
                goto compare;
              case 935:
                resword = &wordlist[79];
                goto compare;
              case 962:
                resword = &wordlist[80];
                goto compare;
              case 964:
                resword = &wordlist[81];
                goto compare;
              case 970:
                resword = &wordlist[82];
                goto compare;
              case 971:
                resword = &wordlist[83];
                goto compare;
              case 972:
                resword = &wordlist[84];
                goto compare;
              case 973:
                resword = &wordlist[85];
                goto compare;
              case 974:
                resword = &wordlist[86];
                goto compare;
              case 986:
                resword = &wordlist[87];
                goto compare;
              case 988:
                resword = &wordlist[88];
                goto compare;
              case 996:
                resword = &wordlist[89];
                goto compare;
              case 1010:
                resword = &wordlist[90];
                goto compare;
              case 1019:
                resword = &wordlist[91];
                goto compare;
              case 1029:
                resword = &wordlist[92];
                goto compare;
              case 1051:
                resword = &wordlist[93];
                goto compare;
              case 1069:
                resword = &wordlist[94];
                goto compare;
              case 1077:
                resword = &wordlist[95];
                goto compare;
              case 1079:
                resword = &wordlist[96];
                goto compare;
              case 1081:
                resword = &wordlist[97];
                goto compare;
              case 1083:
                resword = &wordlist[98];
                goto compare;
              case 1106:
                resword = &wordlist[99];
                goto compare;
              case 1111:
                resword = &wordlist[100];
                goto compare;
              case 1121:
                resword = &wordlist[101];
                goto compare;
              case 1129:
                resword = &wordlist[102];
                goto compare;
              case 1131:
                resword = &wordlist[103];
                goto compare;
              case 1140:
                resword = &wordlist[104];
                goto compare;
              case 1141:
                resword = &wordlist[105];
                goto compare;
              case 1144:
                resword = &wordlist[106];
                goto compare;
              case 1147:
                resword = &wordlist[107];
                goto compare;
              case 1148:
                resword = &wordlist[108];
                goto compare;
              case 1150:
                resword = &wordlist[109];
                goto compare;
              case 1163:
                resword = &wordlist[110];
                goto compare;
              case 1169:
                resword = &wordlist[111];
                goto compare;
              case 1186:
                resword = &wordlist[112];
                goto compare;
              case 1196:
                resword = &wordlist[113];
                goto compare;
              case 1199:
                resword = &wordlist[114];
                goto compare;
              case 1209:
                resword = &wordlist[115];
                goto compare;
              case 1213:
                resword = &wordlist[116];
                goto compare;
              case 1227:
                resword = &wordlist[117];
                goto compare;
              case 1232:
                resword = &wordlist[118];
                goto compare;
              case 1236:
                resword = &wordlist[119];
                goto compare;
              case 1243:
                resword = &wordlist[120];
                goto compare;
              case 1264:
                resword = &wordlist[121];
                goto compare;
              case 1269:
                resword = &wordlist[122];
                goto compare;
              case 1272:
                resword = &wordlist[123];
                goto compare;
              case 1334:
                resword = &wordlist[124];
                goto compare;
              case 1351:
                resword = &wordlist[125];
                goto compare;
              case 1368:
                resword = &wordlist[126];
                goto compare;
              case 1381:
                resword = &wordlist[127];
                goto compare;
              case 1388:
                resword = &wordlist[128];
                goto compare;
              case 1426:
                resword = &wordlist[129];
                goto compare;
              case 1432:
                resword = &wordlist[130];
                goto compare;
              case 1437:
                resword = &wordlist[131];
                goto compare;
              case 1443:
                resword = &wordlist[132];
                goto compare;
              case 1466:
                resword = &wordlist[133];
                goto compare;
              case 1491:
                resword = &wordlist[134];
                goto compare;
              case 1497:
                resword = &wordlist[135];
                goto compare;
              case 1507:
                resword = &wordlist[136];
                goto compare;
              case 1522:
                resword = &wordlist[137];
                goto compare;
              case 1582:
                resword = &wordlist[138];
                goto compare;
              case 1587:
                resword = &wordlist[139];
                goto compare;
              case 1595:
                resword = &wordlist[140];
                goto compare;
              case 1601:
                resword = &wordlist[141];
                goto compare;
			  case 1609:
                resword = &wordlist[188];
                goto compare;
              case 1612:
                resword = &wordlist[142];
                goto compare;
              case 1616:
                resword = &wordlist[143];
                goto compare;
              case 1653:
                resword = &wordlist[144];
                goto compare;
              case 1714:
                resword = &wordlist[145];
                goto compare;
              case 1772:
                resword = &wordlist[146];
                goto compare;
              case 1781:
                resword = &wordlist[147];
                goto compare;
              case 1803:
                resword = &wordlist[148];
                goto compare;
              case 1812:
                resword = &wordlist[149];
                goto compare;
              case 1852:
                resword = &wordlist[150];
                goto compare;
              case 1949:
                resword = &wordlist[151];
                goto compare;
              case 1989:
                resword = &wordlist[152];
                goto compare;
              case 1994:
                resword = &wordlist[153];
                goto compare;
              case 2057:
                resword = &wordlist[154];
                goto compare;
              case 2063:
                resword = &wordlist[155];
                goto compare;
              case 2153:
                resword = &wordlist[156];
                goto compare;
              case 2166:
                resword = &wordlist[157];
                goto compare;
              case 2214:
                resword = &wordlist[158];
                goto compare;
              case 2229:
                resword = &wordlist[159];
                goto compare;
              case 2235:
                resword = &wordlist[160];
                goto compare;
              case 2270:
                resword = &wordlist[161];
                goto compare;
              case 2303:
                resword = &wordlist[162];
                goto compare;
              case 2310:
                resword = &wordlist[163];
                goto compare;
              case 2311:
                resword = &wordlist[164];
                goto compare;
              case 2374:
                resword = &wordlist[165];
                goto compare;
              case 2397:
                resword = &wordlist[166];
                goto compare;
              case 2621:
                resword = &wordlist[167];
                goto compare;
              case 2759:
                resword = &wordlist[168];
                goto compare;
              case 2767:
                resword = &wordlist[169];
                goto compare;
              case 2869:
                resword = &wordlist[170];
                goto compare;
              case 2876:
                resword = &wordlist[171];
                goto compare;
              case 2898:
                resword = &wordlist[172];
                goto compare;
              case 2903:
                resword = &wordlist[173];
                goto compare;
              case 2904:
                resword = &wordlist[174];
                goto compare;
              case 2961:
                resword = &wordlist[175];
                goto compare;
              case 3133:
                resword = &wordlist[176];
                goto compare;
              case 3247:
                resword = &wordlist[177];
                goto compare;
              case 3312:
                resword = &wordlist[178];
                goto compare;
              case 3343:
                resword = &wordlist[179];
                goto compare;
              case 3359:
                resword = &wordlist[180];
                goto compare;
              case 3461:
                resword = &wordlist[181];
                goto compare;
              case 3464:
                resword = &wordlist[182];
                goto compare;
              case 3466:
                resword = &wordlist[183];
                goto compare;
              case 3550:
                resword = &wordlist[184];
                goto compare;
              case 3624:
                resword = &wordlist[185];
                goto compare;
              case 3631:
                resword = &wordlist[186];
                goto compare;
              case 4169:
                resword = &wordlist[187];
                goto compare;
            }
          return 0;
        multicompare:
          while (wordptr < wordendptr)
            {
              register const char *s = wordptr->name;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return wordptr;
              wordptr++;
            }
          return 0;
        compare:
          {
            register const char *s = resword->name;

            if (*str == *s && !strcmp (str + 1, s + 1))
              return resword;
          }
        }
    }
  return 0;
}
const char *KWLookupStr(KWEnum key)
{
	int i, cnt = sizeof(wordlist)/sizeof(wordlist[0]);
	for (i=0; i < cnt; i++)
		if (wordlist[i].kw == key)
			return wordlist[i].name;
	return NULL;
}

#define kKWBufSiz 128

KWEnum KWLookupKey(const char *str)
{
    const E1KwRec * kw;
	char buf[kKWBufSiz+2]; 
	int len;

	if (!str || !*str)
		return KW_null;

	len = strlen(str);
	if (len >= kKWBufSiz)
	  len = kKWBufSiz;
	memcpy(buf, str, len);
	buf[len] = 0;
	strlwr(buf);

	kw = KWLookupAux(buf, len);
	if (kw)
	  return kw->kw;
	else
	  return KW_null;
}
