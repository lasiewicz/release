// peng.h
// pnelson, 11/13/2005
// Internal Engine definitions

#define PENGDEBUG 1 // For now

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

#define snprintf _snprintf
#define strlwr   _strlwr

#define INCLUDE_PENG

#ifndef INCLUDE_PAPI
#include "papi.h"
#endif

typedef struct _Gsv      *Gsv;
typedef struct _PHash    *PHash;
typedef struct _PHashObj *PHashObj;
typedef struct _Tag      *Tag;
typedef struct _Grp      *Grp;
typedef struct _Ldg      *Ldg;

typedef S4                Stx;
typedef struct _StxDb    *StxDb;

typedef struct _Loc {
  US2 lng;
  US2 lat;
} LocRec, *Loc;

typedef struct _RankTune *RankTune;

/********************/
/** General macros **/
/********************/

extern long E1atol(cSTR value);

/***************/
/** Key Sizes **/
/***************/

#define kPI (float)(3.14159265)
#define kEarthCircMiles (24894)  // Ideally a multiple of 18 for clean division

#define kLoc_EWBoxes (kEarthCircMiles/6)
#define kLoc_fLngMin (-kPI)     // Longitude spans -pi to +pi around globe, wraps around
#define kLoc_fLngMax ( kPI)     // 
#define kLoc_EWPrecise     6    // Further div within box for 1 mile precision
#define kLoc_EWPreciseBits 3    // Need three bits to store 1-mile precision location

#define kLoc_NSBoxes (kEarthCircMiles/3/6)
#define kLoc_fLatMin (-(kPI/3))  // Poles are at Pi/2, latitude between arctic circles is -p1/3 to pi/3 
#define kLoc_fLatMax ( (kPI/3))  // 
#define kLoc_NSPrecise     6     // Further div within box for 1 mile precision
#define kLoc_NSPreciseBits 3     // Need three bits to store 1-mile precision location

#define kTag_HashPower   12
#define kTag_HashSize   (1L << kTag_HashPower)
#define kTag_HashShift  (kTag_HashPower - 4)
#define kTag_MaxTags    (1L << 20)
#define kStx_TagBits    20       // up to 1 million unique tag string

#define kGrp_HashPower   18
#define kGrp_HashSize   (1L << kGrp_HashPower)
#define kGrp_MaxGrps    (1L << 23)

#define kLdg_HashPower   16
#define kLdg_HashSize   (1L << kLdg_HashPower)
#define kLdg_MaxLdgs    (1L << 20)
#define kLdg_IterBandMax (kLoc_EWBoxes/50)  // Max boxes we'll look (2% of globe in each dir)

#define kMem_HashPower   18
#define kMem_HashSize   (1L << kMem_HashPower)
#define kMem_MaxMems    (1L << 24)


/****************/

#ifndef NULL
#define NULL 0
#endif

/**************/
/** Packages **/
/**************/

typedef enum {
	PkgName_Unknown,
	PkgName_Eng,
	PkgName_Cmd,
	PkgName_PHash,
	PkgName_ObjMgr,
	PkgName_SeqMgr,
	PkgName_Stx,
	PkgName_PMap,
	PkgName_Tag,
	PkgName_TagDb,
	PkgName_Grp,
	PkgName_Ldg,
	PkgName_Mem,
	PkgName_PXml,
	PkgName_S1,
	PkgName_PRes,
	PkgName_Rank,
	PkgName_Order,
	PkgName_Load
} PkgName;

#define kPkg_NameMax ((int)PkgName_Load+1)

/************/
/** Macros **/
/************/

#define StructSize(ss)       sizeof(ss)
#define StructSizeAlign(ss)  ((sizeof(ss) + 7) & ~7)
#define StructNull(ss)       memset((void*)&(ss), 0, sizeof(ss))
#define ArrayCount(sar)      (sizeof(sar)/sizeof((sar)[0]))
#define ArrayCountX(s, a)    (sizeof(((s)0)->a)/sizeof(((s)0)->a[0]))

#define minmax(v,n,x) ((v) < (n) ? (n) : ((v) > (x) ? (x) : (v)))

typedef int (*QSortCmpFnc)(const void *, const void *);

extern char *str0ncpy(char *dst, const char *src, int dstSize);
extern int  e1UtlTrimDups(void **ptrs, int cnt);

/*********************/
/** Utility methods **/
/*********************/

extern void  PEngError (PkgName pkg, cSTR obj, cSTR err);
extern void  PEngLog   (PkgName pkg, cSTR fmt, cSTR strarg, S4 intarg);
extern void  PEngLoadError(PkgName pkg, cSTR file, S4 lineNum, MemId memid, cSTR errName, cSTR data);
extern void *PEngAlloc (PkgName pkg, US4 size);
extern void  PEngFree  (PkgName pkg, void *ptr, US4 size);

extern int E1ErrorConsistency(cSTR msg);
extern int E1ErrorPermit(int err);

// Structure to carry error info in consistency checkers 
typedef struct _E1CheckErr {
	cSTR     pkg;
	cSTR     msg;
	const void *ptr;
	S4       val;
	int     errCode;
} E1CheckErrRec, *E1CheckErr;

int E1Check(E1CheckErr err);

// External PXml entry points in papi.h
extern int  PXmlPush0  (PXml px, cSTR tag, S4 seq);
extern int  PXmlPush   (PXml px, cSTR tag, S4 seq);
extern int  PXmlPop    (PXml px);
extern int  PXmlSetStr (PXml px, cSTR name, cSTR value);  /// Set string value, cleaning invalid XML from text
extern int  PXmlSetNum (PXml px, cSTR name, S4 value);
extern int  PXmlInsertStr(PXml px, cSTR str);
extern int  PXmlEnumMax(PXml px);


/**************************************/
/** Fixed object size memory manager **/
/**************************************/

typedef struct _ObjMgr *ObjMgr;
typedef struct _ObjPtr *ObjPtr;

extern ObjMgr ObjMgrCreate  (PkgName pkg, int objSize, S4 maxObjs);
extern S4     ObjMgrSequence(ObjMgr xom);
extern void   ObjMgrDestroy (ObjMgr xom);
extern ObjPtr ObjMgrNew     (ObjMgr xom, S4*);
extern void   ObjMgrFree    (ObjMgr xom, ObjPtr obj);
extern S4     ObjMgrPtrToSeq(ObjMgr xom, ObjPtr obj);
extern ObjPtr ObjMgrSeqToPtr(ObjMgr xom, S4 seq);
extern int    ObjMgrXml(PXml px, ObjMgr xom, int mode);

typedef struct _SeqMgr *SeqMgr;
typedef void   *SeqObj;
extern SeqMgr SeqMgrCreate(PkgName pkg, S4 maxObjs);
extern S4     SeqMgrSequence(SeqMgr xom);
extern void   SeqMgrDestroy(SeqMgr xom);
extern S4     SeqMgrPush(SeqMgr xom, SeqObj obj);
extern SeqObj SeqMgrLookup(SeqMgr xom, S4 seq);
extern int    SeqMgrPop(SeqMgr xom, S4 seq);
extern int    SeqMgrXml(PXml px, SeqMgr xom, int mode);

/********************/
/** Protected Hash **/
/********************/

typedef US4   PHashIdx;     /* The index into the hash table */ 
typedef void *PHashKey;     /* The value that was hashed     */

/* Base class of hashable objects 
 */ 
typedef struct _PHashObj {
	PHashObj     hashNext;
} PHashObjRec;

/* Virtual method to generate hash index from the key 
 */ 
typedef PHashIdx PHashKeyToIdxFnc(PHashKey);

/* Virtual method to compare a key to an already hashed object 
 */
typedef int      PHashKeyCmpFnc  (PHashKey, PHashObj);  

/* Hash table methods
 */
extern PHash    PHashNew(PkgName pkg, US4 maxHashIdx, 
							           PHashKeyToIdxFnc *keyToIdxFnc, 
							           PHashKeyCmpFnc   *keyCmpFnc);
extern void     PHashFree (PHash phash);
extern PHashObj PHashFind (PHash phash, PHashKey xkey);
extern PHashObj PHashPush (PHash phash, PHashKey xkey, PHashObj xobj);
extern PHashObj PHashPop  (PHash phash, PHashKey xkey, PHashObj xobj);

typedef int PHashCheckFnc(PHashObj, E1CheckErr, int depth);
extern int  PHashCheck(PHash phash, PHashCheckFnc fnc, E1CheckErr err);

/*********************/
/** Strings to Keys **/
/*********************/

extern StxDb  StxDbNew(US1 maxSizeBits, cSTR name);
extern void   StxDbFree(StxDb xpkc);
extern Stx    StxDbFindOrNew(StxDb xpkc, cSTR value);
extern Stx    StxDbFind(StxDb xpkc, cSTR value);
extern cSTR   StxDbById(StxDb xpkc, Stx xid);
extern S4     StxDbCount(StxDb xpkc);

typedef int StxCheckFnc(cSTR text, S4 seq, E1CheckErr err);
extern int  StxCheck(StxDb xpkc, StxCheckFnc fnc, E1CheckErr err);

/*********************/
/** Bulk CSV loader **/
/*********************/

extern void        LoadInitialize(void);

// Opaque handle
typedef struct _LoadPattern *LoadPattern;

// Codes for load status
typedef enum {
  LoadStatus_Ok = 0,
  LoadStatus_MemIdMissing = 1,
  LoadStatus_DomIdMissing = 2,
  LoadStatus_GenMissing   = 3,
  LoadStatus_TagsTooMany  = 4
} LoadStatusCode;
  
// Status of loading this member
typedef struct _LoadStatusInst {
  S4 lineNum; 
  S4 memId; 
  US1 domId;
  US1 gender;
  S2 status;
} LoadStatusInstRec, *LoadStatusInst;

// All the status as list of lists
typedef struct _LoadStatus *LoadStatus;
typedef struct _LoadStatus {
  LoadStatus  nextPage;
  S4 numInst;
  S4 maxInst;
  LoadStatusInstRec *datInst;
} LoadStatusRec;

#define ERR_Load_Pattern         -(((int)PkgName_Load*1000)+1)
#define ERR_Load_Permit          -(((int)PkgName_Load*1000)+2)

extern LoadPattern LoadPatternNew(cSTR fname, char *header);
extern int         LoadPatternLoad(LoadPattern xlp, MemSpec memSpec, char *data, S4 lineNum);
extern LoadStatus  LoadPatternStatus(LoadPattern xlp);
extern void        LoadPatternFree(LoadPattern xlp);

/*************************/
/** Global state holder **/
/*************************/

typedef struct _Gsv {
  S4 initialized;
  
  // All our tags
  PHash  tags;
  ObjMgr tagObjMgr;
  void  *tagDb;
  StxDb  pkTags;
  
  // All our tag groups
  PHash  grps;
  SeqMgr grpSeqMgr;
  
  // All our Location/Domain/Gender locators
  PHash  ldgs;
  ObjMgr ldgObjMgr;
  // All our Member structures
  PHash  mems;
  ObjMgr memObjMgr;
  // The String<>ID mapping tables
  StxDb  pkCountry;
  StxDb  pkState;
  StxDb  pkCity;
  StxDb  pkZip;
  // Loader state
  int    loaderInit;
  
  // Bitmap of the world to optimize "near by" scans
  void *pmap;
  
  STR pkgNames[kPkg_NameMax];
  US4 pkgMemory[kPkg_NameMax];
  US4 pkgAllocs[kPkg_NameMax];
  
  S4 statsUpserts, statsUpdates, statsDeletes, statsExpires;
  S4 statsSearchNew, statsResultFree;

  // Debugging fields
  void *dbgOutput, *logOutput, *loadOutput; 
  char lastErr[256];
  int currentMemberID; //memberID that the single writer thread is currently processing

} GsvRec;

extern Gsv gsv;
extern int GsvTodaySet();
extern STR GsvPkgNameAsStr(PkgName pkg);
extern US2 GsvGetTodayPack(US2 *);

extern int  E1CfgInitialize(cSTR source);
extern void E1CfgDestroy();
extern cSTR E1CfgGetStr(cSTR name);
extern S4   E1CfgGetInt(cSTR name);

/****************************************/
/** Permits for thread synchronization **/
/****************************************/

#define ERR_Permit_Alloc        -99
#define ERR_Permit_Null         -101
#define ERR_Permit_Inited       -102
#define ERR_Permit_MutexCreate  -103

#define ERR_PermitWrite_Timeout  -120
#define ERR_PermitWrite_Abandon  -121
#define ERR_PermitWrite_Unknown  -122
#define ERR_PermitWrite_NotOwner -123

#define ERR_PermitSearch_Null     -110
#define ERR_PermitSearch_NoneLeft -111
#define ERR_PermitSearch_InUse    -112
#define ERR_PermitSearch_NotInUse -113

#define WARN_PermitSearch_InUse    112

extern S4 E1InterlockedInc(S4 volatile*lp);
extern S4 E1InterlockedDec(S4 volatile*lp);

typedef long                  PermitWrite;

extern int  PermitInitialize(int searchCount);
extern void PermitDestroy();

extern int PermitSearchTake(PermitSearch *permit);
extern int PermitSearchGive(PermitSearch permit);
extern int PermitSearchBeg (PermitSearch permit);
extern int PermitSearchEnd (PermitSearch permit, S4 numScans);
extern int  PermitSearchStats(GsvStats stats);

extern int PermitWriteTake(cSTR name, PermitWrite *seqp, int memberID);
extern int PermitWriteGive(PermitWrite seq);
extern int PermitWriteConfirm(PermitWrite seq);

extern void PermitExpirePtr(PkgName pkg, void *ptr, US4 size);
extern void PermitExpireObj(ObjMgr xom, ObjPtr obj);

/* Virtual method to dispose of an Object
 */
typedef int PermitExpireCBFnc  (ObjPtr obj);  
extern void PermitExpireCB(PermitExpireCBFnc *cb, ObjPtr obj);

extern int  PermitExpireExec();
extern int  PermitMaintain();

/************************************/
/** Location/Domain/Gender Manager **/
/************************************/

typedef struct _LdgKey {
	LocRec   loc;
	DomId    domId;
	US1      gender;
} LdgKeyRec, *LdgKey;

typedef struct _Ldg {
	Ldg       hashNext;   // Next LDG in hash
	Mem       memChain;   // Head of chain of members at this loc/dom/gen
	US4       memCount;     // How many members
	LdgKeyRec ldgKey;     // The loc/dom/gen of this chain
} LdgRec;

typedef struct _LdgIter {
	LdgKeyRec ldgKey;    // Where we're looking
	LocRec    locBase;   // The origin
	Ldg cache;           // cache the next to return if known
	int maxDst;          // how far out should we look
	int locIdx;          // which enumerated box are we checking
	int quad;            // which wuadrant are we checking
} LdgIterRec, *LdgIter;

extern S4   LdgSequence(Ldg xldg);
extern Ldg  LdgBySequence(S4 seq);
extern Ldg  LdgFind(LdgKey lk);
extern Ldg  LdgFindOrNew(LdgKey lk);
extern Mem *LdgChainMemRoot(Ldg xldg);
extern void LdgIterInit(LdgIter iter, Loc loc, DomId domid, int gender, int maxBand);
extern Ldg  LdgIterNext(LdgIter iter, int extendOk);

/*****************************************************/
/** Map of all occupied regions to accelerate scans **/
/*****************************************************/

typedef struct _PMapIter {
	LocRec loc;   // The origin
	LocRec next;  // Point we are returning
	S4  scanLng;  // Last point we looked at
	S4  scanLat;  //   must be S4 to handle wraparound below zero
	US2 band;       // How far from center are we
	US2 maxBand;    // How far will we go
	US2 side;       // Which side of the box are we scanning 
	US2 sidx;       // where are we on that side
	// Cached Cell to minimize lookups
	void *cell;
	US2   cellEW;
	US2   cellNS;
} PMapIterRec, *PMapIter;

/*****************************/
/** Internal Object Methods **/
/*****************************/

#define kTag_TextMaxName  64
#define kTag_TextMaxValue 64
#define kTag_TextMax      (kTag_TextMaxName + kTag_TextMaxValue)
#define kTag_TextMaxs     128

typedef struct _TagKey {
	char   name [kTag_TextMaxName];
	char   value[kTag_TextMaxValue];
	Stx   nId, vId; // Ptrs to IDs of strings when available
	Tag    tag;      // Ptr to actual tag when available
} TagKeyRec, *TagKey;

//#ifdef TAG_FRIEND
typedef struct _Tag {
  Tag   hashNext;    // Next Tag in hash
  S4    sequence;    // Store explicitly since seq used when sorting tags
  Grp   grpChain;    // Head of chain of Grps that use this tag
  Stx   nxId, vxId;
  US4   memCount;     // How many members
} TagRec;
//#endif

typedef struct _TagIter {
	DomId  domid;  // Only want members from this domain
	int    gender; // Only want members with this gender
	Tag xtag;    // Base tag whose parent members we're iterating
	Grp xgrp;    // Current Grp we're walking
	Mem xmem;    // Next member to consider
	Mem cache;   // Member we just returned
} TagIterRec, *TagIter;

extern int TagInitialize();
extern Tag  TagFind(TagKey nv);
extern Tag  TagBySequence(S4 seq);
extern S4   TagSequence(Tag xtag);
extern void TagNameValue(Tag xtag, cSTR *namep, cSTR *valuep);
extern Tag  TagFindOrNew(TagKey nv);
extern Tag  TagFindOrNewStr(cSTR name, cSTR value);
extern Grp *TagChainGrpRoot(Tag xtag);
extern S4   TagCount(Tag xtag, S4 *grpCntp);

extern void TagIterInit(TagIter iter, Tag xtag, DomId domid, int gender);
extern Mem  TagIterNext(TagIter iter);

typedef enum {
  TagDb_Unknown,
  TagDb_Mask,    // codes are (multiple) bits in a mask, each representing a value
  TagDb_Id,      // codes are integers representing a value
  TagDb_Value    // codes are the actual values
} TagDbCoding;

typedef S4 TagDbId;

extern TagDbId TagDbFind(cSTR name);
extern TagDbId TagDbFindOrNew(cSTR name, TagDbCoding coding, int noAutoCreate);
extern Tag     TagDbTagAssign(TagDbId tdx, cSTR strVal, US4 code);
extern int     TagDbByCode(TagDbId tdx, US4 code, Tag *tagList);
extern S4      TagDbInternal(TagDbId tdx, S4 newData);
extern int     TagDbXml(PXml px, TagDbId tdx, int mode);

//#ifdef GRP_FRIEND
typedef struct _Grp {
  Grp     hashNext;   // Next Grp in hash
  S4      sequence;   // Sequence number of this Grp
  Tag     *tagList;   // List of actual tags in this Grp, ordered by Tag->seq
  Grp     *grpNext;   // For each tag, ptr to next group that also uses this tag
  Mem     memChain;   // Head of chain of members that use this Grp
  US4     memCount;   // How many members
  US2     tagCount;   // Count of tags in the group
  US2     allocSize;  // Put something useful in 2 byte padding
} GrpRec;
//#endif

typedef struct _GrpKey {
	Grp tagGrp; /* Ptr to actual taggrp when available */
	US2 count; US1 fixed; US1 xalign;
	TagKeyRec tagKeys[kTag_TextMaxs];
} GrpKeyRec, *GrpKey;

extern Grp  GrpBySequence(S4 seq);
extern S4   GrpSequence(Grp xgrp);
extern GrpKey  GrpKeyNewFromMemNV(MemNV nv, int num);
extern void    GrpKeyFree(GrpKey gk);
extern Grp  GrpFindOrNewFromMemNV(MemNV nv, int num);
extern Grp  GrpFindOrNewFromTags(Tag *tags, int num);
extern Grp  GrpFindOrNew(GrpKey tg);
extern Mem *GrpChainMemRoot(Grp xgrp);
extern US4  GrpCountUpdate(Grp xgrp, S4 amt);
extern Grp  GrpFollow(Grp xgrp, Tag xtag, int verbose);
extern int  GrpDispose(Grp xgrp);
extern int  GrpTagByIndex(Grp xgrp, Tag xtag);

/***********************/
/** Member defintions **/ 
/***********************/

// Lookup a member by her ID in the master hash
//
typedef struct _MemKey {
	MemId memId;
	DomId domId;
} MemKeyRec, *MemKey;

extern void MemInitialize();
extern S4  MemSequence(Mem xmem);
extern Mem MemBySequence(S4 seq);
extern S1  MemGender(S1 inp);
extern Mem MemFind(MemKey mk);

// Max Sizes for LocationString <> ID mapping tables
// Need six additional bits to store higher precision location
#define kStx_CountryBits   9 
#define kStx_StateBits    16
#define kStx_CityBits     20
#define kStx_ZipBits      21
#define kStx_AreaCodeBits 10

// Bit packed struct to store IDs of location strings in minimal space
//#pragma pack(push, 2)
typedef struct _MemCSCZMap {
  unsigned pkZip     : kStx_ZipBits;
  unsigned pkCountry : kStx_CountryBits;
  unsigned : 2;

  unsigned pkCity    : kStx_CityBits;
  unsigned pkArea    : kStx_AreaCodeBits;
  unsigned : 2;

  unsigned short pkState   : kStx_StateBits;

  unsigned char  latPrecise : kLoc_NSPreciseBits;
  unsigned char  lngPrecise : kLoc_EWPreciseBits;
  unsigned char : 2;

  unsigned char : 8;

} MemCSCZMapRec, *MemCSCZMap;

typedef union _MemDate {
  unsigned short pack;
  struct {
	unsigned short day   : 5;    // 1 based day of month
	unsigned short month : 4;    // 1 based month
	unsigned short year  : 7;    // 1900 + 128 years
  } v;
} MemDateRec, *MemDate;

typedef union _MemDateTime {
  unsigned int pack;
  struct {
	unsigned short minute  : 11;	//0-60, plus leftover bits
	unsigned short hour    : 5;	//0-23
	unsigned short day     : 5;    // 1 based day of month
	unsigned short month   : 4;    // 1 based month
	unsigned short year    : 7;    // 1900 + 128 years
  } v;
} MemDateTimeRec, *MemDateTime;


#define kMemPack_YearMin  1900  // Min year to fit in 7 bits
#define kMemPack_YearRng   127  // Min year to fit in 7 bits
#define kMemPack_HeightMin  122  // Min height in cm to fit in 7 bits
#define kMemPack_HeightRng  120  // Range of cm that fit in 7 bits
#define kMemPack_WeightMin  75  // Min weight in pounds to fit in 8 bits
#define kMemPack_WeightRng 255  // Min weight in pounds to fit in 8 bits

// Bit packed struct to store all the other vitals in 32 bits
//
typedef struct _MemVitals {
  unsigned saPop    : 4;    // popularity score
  unsigned saAct    : 4;    // activity score
  unsigned saNew    : 4;    // recently registered score
  unsigned isOnline : 1;    // is online now
  unsigned hasPhoto : 1;    // has a photo
  unsigned childCnt : 2;    // 0,1,2,3+ children
  unsigned height   : 7;    // cm above 1.22 meters
  unsigned sameSex  : 1;    // Is person seeking someone of same sex
  unsigned weight   : 8;    // pounds over 75
} MemVitalsRec, *MemVitals;

//#ifdef MEM_FRIEND
typedef struct _Mem {
  Mem    hashNext;   // Next member in hash by memberId
  MemId  memId;      // My MemId
  Ldg    ldg;        // Head of LDG chain, with loc/dom/gen
  Mem    ldgChain;   // Next member in chain at this loc/dom/gen
  Grp    grp;        // Tag group with all my attributes
  Mem    grpChain;   // Next member in chain w/ this Grp

  MemVitalsRec vitals; // Packed bitfield of Member's key info
  US1	moreChildrenFlag;//0 = no, 1 = yes, 2 = not sure
  US1   colorcode;		
  MemCSCZMapRec cscz;   // Packed bitfield of IDs of Country/State/City names & Zipcode

  MemDateRec birthPack;
  MemDateRec registerPack;
  MemDateTimeRec activePack;

} MemRec;
//#pragma pack(pop)

// Macros to pull common items out of embedded Mem structures
#define xMemMemId(mm)   ((mm)->memId)
#define xMemLoc(mm)     ((mm)->ldg->ldgKey.loc)
#define xMemDomId(mm)   ((mm)->ldg->ldgKey.domId)
#define xMemGender(mm)  ((mm)->ldg->ldgKey.gender)

//#endif

extern int  MemPackGender (int genderMask, int *sameSexp);
extern US2  MemPackDate   (cSTR dateStr);
extern US4  MemPackDateTime(cSTR dateStr);
extern STR  MemPackDateStr(cSTR name, US2 pack, char *buf, int asAttr);
extern STR  MemPackDateTimeStr(cSTR name, US4 pack, char *buf, int asAttr);
extern US2  MemPackDateAsDays(US2 pack);
extern US2  MemPackMilesAsBoxes(int miles);
extern void MemPackLoc    (Loc, MemLoc, int precise);
extern void MemPackMemToLoc(Mem, Loc, int precise);
extern void MemPackVitals (RankTune rt, MemVitals, MemSpec);
extern int  MemPackCSCZMap(MemCSCZMap, MemLoc);
extern int  MemToMemSpec(Mem xmem, MemSpec outSpec, char *xxBuf, int xxBufLen);

extern int MemMaintain();

/*********************/
/** XML serializers **/
/*********************/

extern int TagXml(PXml px, Tag xtag, int mode);
extern int GrpXml(PXml px, Grp xgrp, int mode);
extern int LdgXml(PXml px, Ldg xldg, int mode);
extern int MemXml(PXml px, Mem xmem, int mode);
extern int GrpXmlTagChain(PXml px, Grp xgrp, Tag xtag);
extern int MemXmlGrpChain(PXml px, Mem xmem, int mode);
extern int MemXmlLdgChain(PXml px, Mem xmem, int mode);
extern int GsvXml  (PXml px, int mode);
extern int StxDbXml(PXml px, StxDb xpkc, int mode);
extern int PHashXml(PXml px, PHash phash, US4 sequence, int mode);

extern int GrpPkgXml(PXml px, int mode);
extern int RankTuneXml(PXml px, DomId domid);
extern int GsvStatsXml(PXml px, GsvStats stats);

/********************/
/** Rank functions **/
/********************/

/// Number from 0-15 
typedef US1 S14bit;

extern PRes PResNew(int target);
extern int  PResInsert(PRes xres, MemId memid, US2 score);
extern int  PResLoad(PRes pres, S1Result sres);
extern void PResFree(PRes xres);
extern int  PResXml (PXml px, PRes xres, int mode);

#define kRank_AgeBandMax 6
#define kRank_BandMax   16

#define kRank_AgeVctMax 100
#define kRank_AgeMin     18
#define kRank_AgeMax     99

/// Describe how ages map to scores
typedef struct _RankTuneAge {
  /// Which algo to use
  S1Score algoSelect;  // 0,1 means favor oldest, 2 means favor center
  /// score at center of requested age band
  S1Score  inMax;
  /// score at edges of requested age band
  S1Score  inMin;
  /// max score for someone outside the requested age band
  S1Score  outMax;
  /// bands of pctDif in age, score penalty per year in that band
  US1 pctDif[kRank_AgeBandMax];
  US1 perYear[kRank_AgeBandMax];
} RankTuneAgeRec, *RankTuneAge;

/// Describe how heights map to scores
typedef struct _RankTuneHgt {
  /// score at center of requested hgt band
  S1Score  inMax;
  /// score at edges of requested hgt band
  S1Score  inMin;
  /// max score for someone outside the requested hgt band
  S1Score  outMax;
  /// deduction per cm of height difference from edge of band
  S1Score  perCm;
} RankTuneHgtRec, *RankTuneHgt;

/// Describe how overlap of Country/State/City/Zip map to score
typedef struct _RankTuneCscz {
  /// Score if only overlaps country
  S1Score inCountry;
  /// Score if also overlaps state
  S1Score inState;
  /// Score if also overlaps city
  S1Score inCity;
  /// Score if also overlaps zip
  S1Score inZip;
} RankTuneCsczRec, *RankTuneCscz;

/// Describe how distance (measured in deltaLatBoxes^2 + deltaLngBoxes^2) maps to score (15->1)
typedef struct _RankTuneLdg {
  US4 bands[kRank_BandMax];
} RankTuneLdgRec, *RankTuneLdg;

//Time (in minutes) beyond which members always receive a 0 activity score
#define MAX_ACTIVE_TIME 86400

/// Describe how active (measured in days since last login) maps to score (15->1)
typedef struct _RankTuneAct {
  US4 bands[kRank_BandMax];
} RankTuneActRec, *RankTuneAct;

/// Describe how new (measured in days since registration) maps to score (15->1)
typedef struct _RankTuneNew {
  US4 bands[kRank_BandMax];
} RankTuneNewRec, *RankTuneNew;

/// Describe how popular (measured in emails received recently) maps to score (15->1)
typedef struct _RankTunePop {
  US4 bands[kRank_BandMax];
} RankTunePopRec, *RankTunePop;

typedef struct _RankTune {

  RankTune  rtNext;  // List them all together
  DomId     domId;

  // Default values when searching
  S1SortKey       defaultSort;
  US2             defaultMax;
  US2             defaultDist;
  // Query independent scores calculated on load
  RankTuneActRec  act;
  RankTuneNewRec  newreg;
  RankTunePopRec  pop;
  // Query dependent scores calculated per scan
  RankTuneCsczRec cscz;
  RankTuneLdgRec  ldg;
  RankTuneLdgRec  ldgSquared;
  RankTuneAgeRec  age;
  RankTuneHgtRec  hgt;
  // Different built-in blends per each kind of sort
  S1ScoresRec     bySort[6];

} RankTuneRec;

extern void RankInitialize();
extern RankTune RankTuneToUse(DomId domid);
extern RankTune RankTuneToEdit(DomId domid);

// Take values from the query and turn them into useful units for the scanner
extern US2 RankPackAge(int age);
extern int RankPackHgt(int hgt);
extern int RankPackWgt(int wgt);

// Calculate scores from the ranktune and member attributes
extern S4 RankActExecAux(RankTune rt, US4 activePack, int as4bit);
extern S1Score RankActExec(RankTune rt, US4 activePack, int as4bit);
extern S1Score RankNewExec(RankTune rt, US2 registerPack, int as4bit);
extern S1Score RankPopExec(RankTune rt, S4 emailCnt, int as4bit);

// Calculate scores from the ranktune and member attributes and the query
extern S1Score RankCsczExec(RankTune rt, MemCSCZMap xscan, MemCSCZMap xmem);
extern S1Score RankLdgExec(RankTune rt, S2 miles, Loc xscan, Loc xmem);
extern S1Score RankHgtExec(RankTune rt, int vmin, int vmax, int xmem);

// Ages done by pre-calcing score for every possible age for a query, the doing lookup while scanning
extern void    RankAgeInit(RankTune rt, US1 *byAge, US1 amin, US1 amax);

extern void S1ShowScores(char *buf, int siz, int sort, S1Scores b);
