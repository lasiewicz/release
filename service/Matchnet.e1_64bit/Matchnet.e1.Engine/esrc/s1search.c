// s1search.c
// pnelson 12/11/2005
// Basic searching to emulate legacy

#define MEM_FRIEND
#define GRP_FRIEND
#define TAG_FRIEND
#include "string.h"
#include "peng.h"
#include "kw.h"

#define PKGNAME PkgName_S1

#define kS1_scanMax (200000)  // Most people we'll ever look at in a search

// Internal data to support fast query execution
//
typedef struct _S1Scan {

  S4           allocSize; // How big is this struct (for debugging)
  RankTune     rankTune;  // Rank tuning parameters

  DomId        domId;     // Must be this domain
  US1          gender;    // Must be this gender
  US1		   sameSex;   // Must be seeking people of same gender
  S2           distance;  // Requested distance in miles
  int          extendOk;  // Whether or not to expand beyond the radius distance specified
  LocRec       locQry;    // Lng Lat Box at focus of search
  LocRec       locQryPrecise;  // High precision focus of search
  LocRec       locLdgPrecise;  // High precision base of current LDG

  MemCSCZMapRec cscz;     // Packed bitfield of Country/State/City names & Zipcode

  // For area code search, map them to tags
  US2  acDigit[1+ArrayCountX(S1Query, areaCodes)];  // leave room for null sentinel
  S2   acNum;
  Tag  acTag  [ArrayCountX(S1Query, areaCodes)];

  // For SchoolId search, map it to a tag
  Tag  schoolTag;

  MemDateRec   today;     // Todays date
  int        ageMin, ageMax;
  US1  byAge[kRank_AgeVctMax];
  int        hgtMin,  hgtMax;
  int        wgtMin,  wgtMax;
  int		childrenCount;
  int		moreChildrenFlag; //0 = no, 1 = yes, 2 = either

  int        hasPhoto;     // -1 has none, 0 don't care, 1 require
  int        isOnline;    // -1 not online, 0 don't care, 1 require
  int		 colorCode;
  char        detailScratch[128]; // Scratch buffer to hold strings for detail view

  // Ptr to actual tags if they exist -- masks may expand into multiple tags
  S2     tagCnt;
  Tag    tagDat[kS1Search_TagMax*4];

  // How to blend scores
  S1SortKey    sortKey;
  S1ScoresRec  blend;

  // Some stats
  S4 scanCnt;    // How many we looked at
  S4 liveCnt;    // How many showed some sign of life
  S4 dropCnt;    // How many rejected by PRes for score too low
  S4 ldgCnt;     // How many LDG chains did we examine

  // The mask and the bit val per tag
  US4     orTarget;    // Mask with all used bits turned on

  TagDbId orBits[33];  // Which tagdb controls which bit
  US2     orMaskMax;   // largest bit in use
  US1     orMask[1];   // Which bit to flip for this tag
  // Structure will be alloced bigger to allow space for orMask to cover all tags
} S1ScanRec;


/// <summary>Compile query from API into structures optimized for fast scanning</summary>
/// <returns>an "S1Scan", the compiled query</returns>
/// <param name="xqry">the query as passed from the api, with defaults filled in and bounds checked</param>
/// <param name="rt">the tuning parameters that will guide this search</param>
///
static S1Scan S1ScanNew(S1Query xqry, RankTune rt)
{
	S4  asize;
	S1Scan xscan;
	int b, i, tagNew, tagMax; 
	S1Scores rtb;

	// Alloc struct big enough to include an orMask byte per tag
	xqry->debugCount = 120;
	tagMax = ObjMgrSequence(gsv->tagObjMgr);
	asize  = sizeof(S1ScanRec) + tagMax;
	xqry->debugCount = 121;
	xscan  = PEngAlloc(PKGNAME, asize);
	xscan->allocSize = asize;
	xscan->orMaskMax = tagMax;

	xqry->debugCount = 100;

	// Load the blend, either from a query override, or from the proper tune structure
	rtb = &(xqry->rankTuneBlend);
	if (rtb->match || rtb->ldg || rtb->cscz || rtb->age ||
		rtb->hgt || rtb->newreg || rtb->act || rtb->pop ) {
	  xscan->blend = *rtb;
	} else {
	  xscan->blend = rt->bySort[(int)xqry->sortKey];
	}

	xqry->debugCount = 101;

	// Setup processed versions of all our other builtin search params
	xscan->rankTune  = rt;
	xscan->sortKey   = xqry->sortKey;
	xscan->domId     = (xqry->domId ? xqry->domId : 1);
	xscan->gender    = MemPackGender(xqry->genderMask, &i);
	xscan->sameSex   = i;
	xscan->childrenCount = xqry->childrenCount;
	xscan->moreChildrenFlag = xqry->moreChildrenFlag;
    xscan->colorCode=xqry->colorCode;
	xqry->debugCount = 102;

	// Calculate our big box, and precise box location
	if (xqry->areaCodes[0]) {
	  // Area code search
      TagKeyRec tk; Tag xtag;
	  for (i=0; i < ArrayCount(xqry->areaCodes) && xqry->areaCodes[i]; i++) {
	    xscan->acDigit[i] = xqry->areaCodes[i];
		StructNull(tk);
		str0ncpy(tk.name, KWLookupStr(KW_areacode), sizeof(tk.name));
		snprintf(tk.value, sizeof(tk.value), "%d", xqry->areaCodes[i]);
		if ((xtag = TagFind(&tk)))
		  xscan->acTag[xscan->acNum++] = xtag;
	  }

	  xqry->debugCount = 103;

	  // Get rid of any duplicates
	  xscan->acNum = e1UtlTrimDups((void **)xscan->acTag, xscan->acNum);
	  // If this is an area code search, but we don't know any of the codes as tags, mark it
	  if (xscan->acNum == 0)
		  xscan->acNum = -1;
	  // Null out ldg and cscz since they are not meaningful
	  xscan->blend.ldg = xscan->blend.cscz = 0;
	} else if (xqry->schoolId) {

	  // Area code search
      TagKeyRec tk;

	  xqry->debugCount = 116;

	  StructNull(tk);
	  str0ncpy(tk.name, KWLookupStr(KW_schoolid), sizeof(tk.name));
	  snprintf(tk.value, sizeof(tk.value), "%d", xqry->schoolId);
      if (!(xscan->schoolTag = TagFind(&tk)))
		xscan->schoolTag = (Tag)-1;
	  // Null out ldg and cscz since they are not meaningful
	  xscan->blend.ldg = xscan->blend.cscz = 0;
	} else if (xqry->debugLocLat || xqry->debugLocLng) {

	  xqry->debugCount = 105;

	  xscan->locQry.lat = xqry->debugLocLat;
	  xscan->locQry.lng = xqry->debugLocLng;
	  xscan->locQryPrecise.lat = xscan->locQry.lat * kLoc_NSPrecise;
	  xscan->locQryPrecise.lng = xscan->locQry.lng * kLoc_EWPrecise;
	} else {

	  xqry->debugCount = 106;

	  MemPackLoc(&(xscan->locQry), &(xqry->location), 0);
	  MemPackLoc(&(xscan->locQryPrecise), &(xqry->location), 1);
	}
	xscan->distance = xqry->distance;

	xqry->debugCount = 107;

	MemPackCSCZMap(&(xscan->cscz), &(xqry->location)); //TODO: check error code and bail on search if this fails

	xqry->debugCount = 108;
	
	xscan->hasPhoto = xqry->hasPhoto;
	xscan->isOnline = xqry->isOnline;

	xqry->debugCount = 109;
	
	// Setup lookup vector for every age
	xscan->today.pack = GsvGetTodayPack(NULL);
	xscan->ageMin = xqry->ageMinDesired;
	xscan->ageMax = xqry->ageMaxDesired;
	RankAgeInit(rt, xscan->byAge, xscan->ageMin, xscan->ageMax);

	xqry->debugCount = 110;

	xscan->hgtMin = RankPackHgt(xqry->hgtMinDesired);
	xscan->hgtMax = RankPackHgt(xqry->hgtMaxDesired);
	xscan->wgtMin = RankPackWgt(xqry->wgtMinDesired);
	xscan->wgtMax = RankPackWgt(xqry->wgtMaxDesired);

	xqry->debugCount = 111;

	// Now look through all the requested tags in the query and
	//   set the proper masks on all tag instances we care about
	for (i=0; xqry->tags[i].name; i++) {
	  cSTR      name = xqry->tags[i].name;
	  cSTR     value = xqry->tags[i].value;
	  TagDbId  tdx  = TagDbFind(name);
	  S4       code = E1atol(value);
	  
	  xqry->debugCount = 112;

	  if (!tdx)
		continue; // ??? we don't know about this tag

	  // Find the bit for this tag's OR group
	  for (b=0; xscan->orBits[b] != tdx && xscan->orBits[b] != 0; b++) ;

	  xqry->debugCount = 113;

	  if (b >= 32)
		continue; // ??? too many unique tag types in this search

	  xqry->debugCount = 114;

	  // Remember that this tag group is tied to this bit
	  xscan->orBits[b] = tdx;

      xqry->debugCount = 115;

	  // Find all the tags the user requested
	  tagNew = TagDbByCode(tdx, code, &(xscan->tagDat[xscan->tagCnt]));

	  xqry->debugCount = 116;

	  // Mark those tags with the and/or bit to flip when present
	  tagMax = xscan->tagCnt + tagNew;
	  while (xscan->tagCnt < tagMax) {
		Tag tag = xscan->tagDat[xscan->tagCnt++];
		S4  seq = tag->sequence;
		if (seq >= xscan->orMaskMax)
		  continue; // ??? This tag is not in range of all tags
		else
		  xscan->orMask[seq] = b+1; // Do it one based so that zero means don't care
	  }

	  xqry->debugCount = 117;
	}

	// This is what all our bits being on looks like
	xscan->orTarget = 0;
	for (b=0; xscan->orBits[b] != 0 && b < 32; b++) 
	  xscan->orTarget |= (1L << b);

	xqry->debugCount = 118;

	
	return xscan;
}

/// <summary>Free the S1Scan when a search is done</summary>
/// <param name="xscan">the S1San returned from S1ScanNew</param>
///
static void S1ScanFree(S1Scan xscan)
{
    if (xscan)
	  PEngFree(PKGNAME, xscan, xscan->allocSize);
}

/****************/

// Routines to help with scaled, integer probability sum math
//
#define S1BlendAbs(b)    ((b) >= 0 ? (b) : ((b) == S1Blend_Require ? 0 : -(b)))
#define S1Scale          ((US2)655)   // Fit 0-100 in 16 bits
#define S1Identity       ((US2)(100 * S1Scale))
#define S1Inverse(s,b)   (US2)(S1Identity - (US2)((US4)(s) * (US4)(b) * S1Scale / 100))
#define S1InverseLarge(s,b)   (US2)(S1Identity - (US2)((US4)(s) * (US4)(b) / 100))
#define S1MultScale(a,b) (US2)(((US4)(a) * (US4)(b)) / S1Identity)
  
/// <summary>Evaluate a member against a query and produce a 16bit blended score</summary>
/// <returns>Returns a score between 0 and S1Scale*100</returns>
/// <param name="xscan">compiled representation of the query</param>
/// <param name="xmem">the member we're evaluating</param>
/// <param name="xss">array of scores we're building</param>
///
static US2 S1ScanExec(S1Scan xscan, Mem xmem, S1Scores xss)
{
  int i; S1Score ib;
  US4 orMask;
  Grp xgrp;
  S4  probSum;
  MemDateRec ageRec; US1 age;
  S1Scores blend;
  S4 actScore;
  S1Score wgtScore;

  blend = &(xscan->blend);

  // Checks are done in order of most-likely-to-reject

  // Has a photo
  if (xscan->hasPhoto) {
	switch (xscan->hasPhoto) {
	case S1Flag_MustYes: if (!xmem->vitals.hasPhoto) goto reject; break;
	case S1Flag_MustNo:  if ( xmem->vitals.hasPhoto) goto reject; break;
	}
  }

  // Is online 
  if (xscan->isOnline) {
	switch (xscan->isOnline) {
	case S1Flag_MustYes: if (!xmem->vitals.isOnline) goto reject; break;
	case S1Flag_MustNo:  if ( xmem->vitals.isOnline) goto reject; break;
	}
  }

  // Searcher and member must both be hetero or homo
  if (xscan->sameSex != xmem->vitals.sameSex)
	goto reject;

  // Age is always required
  ageRec.pack = xscan->today.pack - xmem->birthPack.pack;
  age = (int)ageRec.v.year;
  xss->age = (age < kRank_AgeMin || age > kRank_AgeMax ? 0 : xscan->byAge[age]);
  if (!xss->age)
	goto reject;
  ib = S1BlendAbs(blend->age);
  probSum = S1Inverse(xss->age, ib);

  // Maybe reject on ChildrenCount
  if (xscan->childrenCount)
  {
	if (xmem->vitals.childCnt == 0 && (xscan->childrenCount & 1) != 1)
	{
		goto reject;
	}
	else if (xmem->vitals.childCnt == 1 && (xscan->childrenCount & 2) != 2)
	{
		goto reject;
	}
	else if (xmem->vitals.childCnt == 2 && (xscan->childrenCount & 4) != 4)
	{
		goto reject;
	}
	else if (xmem->vitals.childCnt == 3 && (xscan->childrenCount & 8) != 8)
	{
		goto reject;
	}
  }
	
 
  // Maybe reject on MoreChildrenFlag
  //Hack to compare search preference mask for moreChildrenFlag to single-valued moreChildrenFlag attribute
  //xscan->moreChildrenFlag -- 1 = no, 2 = yes, 4 = not sure
  //xmem->moreChildrenFlag -- 0 = no, 1 = yes, 2 = not sure

  if (xscan->moreChildrenFlag)
  {
	if (xmem->moreChildrenFlag == 0 && (xscan->moreChildrenFlag & 1) != 1)
	{
		goto reject;
	}
	else if (xmem->moreChildrenFlag == 1 && (xscan->moreChildrenFlag & 2) != 2)
	{
		goto reject;
	}
	else if (xmem->moreChildrenFlag == 2 && (xscan->moreChildrenFlag & 4) != 4)
	{
		goto reject;
	}
  }

  if(xscan->colorCode )
  {
	    if(!xmem->colorcode)
		goto reject;
	    if((xmem->colorcode & xscan->colorCode) != xmem->colorcode)
		goto reject;
  }
  // Check And/Or tree of all tags
  orMask = 0;
  if (xscan->orTarget && (xgrp = xmem->grp)) {
	for (i=0; i<xgrp->tagCount; i++) {
	  Tag xtag = xgrp->tagList[i];
	  S4  seq  = xtag->sequence;
	  int b = xscan->orMask[seq];  // one based
	  if (b) {
		orMask |= (1L << (b-1));
		if (orMask == xscan->orTarget)
		  break;
	  }
	}
  }
  // orMask now has a bit flipped for every attr type (ie zodiac) that has at least one match
  if (orMask == xscan->orTarget) {
	// We're good -- either no tags (both are zero) or a perfect match
	xss->match = 100;
	if (ib = S1BlendAbs(blend->match))
	  probSum = S1MultScale(probSum, S1Inverse(100, ib));
  } else if (blend->match < 0) {
	// This is filter and we didn't have perfect match so reject
	goto reject;
  } else if (orMask == 0) {
	// Got nothing
	xss->match = 0;
  } else {
	// Charge 20 pts for every missing attr, then blend, ??? This should be in a RankTune
	S4 refMask = xscan->orTarget;
	xss->match = 100;
	while (refMask) {
	  if ((orMask & 1) == 0)
		if (xss->match >= 20) 
		  xss->match -= 20;
	  refMask = (refMask >> 1);
	  orMask  = (orMask  >> 1);
	}
	if (xss->match) {
	  if (ib = S1BlendAbs(blend->match))
	    probSum = S1MultScale(probSum, S1Inverse(xss->match, ib));
	}
  }

  // Calculate score for ldg distance measured in precise boxes
  if (blend->ldg) { 
    // This members precise location within the outer LDG's big box
    LocRec xmemPrecise;
	xmemPrecise.lat = xscan->locLdgPrecise.lat + xmem->cscz.latPrecise;
    xmemPrecise.lng = xscan->locLdgPrecise.lng + xmem->cscz.lngPrecise;
    // Turn distance into score, in context of our max distance
	xss->ldg = RankLdgExec(xscan->rankTune, xscan->distance, &(xscan->locQryPrecise), &xmemPrecise, xscan->extendOk);
	if (!xss->ldg && blend->ldg < 0)
		goto reject;
	if (xss->ldg && (ib = S1BlendAbs(blend->ldg)))
      probSum = S1MultScale(probSum, S1Inverse(xss->ldg, ib));
  }

  // Calculate score for cscz overlap
  if (blend->cscz) {
    xss->cscz = RankCsczExec(xscan->rankTune, &(xscan->cscz), &(xmem->cscz));
    if (!xss->cscz && blend->cscz < 0)
	  goto reject;
    if (xss->cscz && (ib = S1BlendAbs(blend->cscz)))
	  probSum = S1MultScale(probSum, S1Inverse(xss->cscz, ib));
  }

  // Calculate score for activity
  
  actScore = RankActExecAux(xscan->rankTune, xmem->activePack.pack, 0) * S1Scale / (MAX_ACTIVE_TIME / 100);
  xss->act = (S1Score) (actScore / S1Scale); // (S1Score)xmem->vitals.saAct * 6;  // normalize 0-15
  if (!actScore && blend->act < 0)
	  goto reject;
  if (actScore && (ib = S1BlendAbs(blend->act)))
	  probSum = S1MultScale(probSum, S1InverseLarge(actScore, ib));
 
  // Calculate score for popularity
  xss->pop = (S1Score)xmem->vitals.saPop * 6;  // normalize 0-15
  if (!xss->pop && blend->pop < 0)
	  goto reject;
  if (xss->pop && (ib = S1BlendAbs(blend->pop)))
	probSum = S1MultScale(probSum, S1Inverse(xss->pop, ib));

  // Calculate score for newness
  xss->newreg = (S1Score)xmem->vitals.saNew * 6;  // normalize 0-15
  if (!xss->newreg && blend->newreg < 0)
	  goto reject;
  if (xss->newreg && (ib = S1BlendAbs(blend->newreg)))
	  probSum = S1MultScale(probSum, S1Inverse(xss->newreg, ib));

  // Maybe do score for height
  if (blend->hgt && xscan->hgtMin) {
	xss->hgt = RankHgtExec(xscan->rankTune, xscan->hgtMin, xscan->hgtMax, xmem->vitals.height);
    if (!xss->hgt && blend->hgt < 0)
	  goto reject;
    if (xss->hgt && (ib = S1BlendAbs(blend->hgt)))
	  probSum = S1MultScale(probSum, S1Inverse(xss->hgt, ib));
  }

  // Maybe do score for weight
  if (xscan->wgtMin) {
	wgtScore = RankWgtExec(xscan->rankTune, xscan->wgtMin, xscan->wgtMax, xmem->vitals.weight);
	if (!wgtScore)
	  goto reject;
	ib = S1BlendAbs(100);//hard-coded weight blend
    probSum = S1MultScale(probSum, S1Inverse(wgtScore, ib));
  }

  // Maybe do MOL score
  if (blend->mol)
  {
	//MOL score is 100 for online, 0 for offline
	xss->mol = xmem->vitals.isOnline ? 100 : 0;
	if (!xss->mol && blend->mol < 0)
		goto reject;
	if (xss->mol && (ib = S1BlendAbs(blend->mol)))
		probSum = S1MultScale(probSum, S1Inverse(xss->mol, ib));
  }

  // Return final score
  probSum = S1Identity - probSum;
  return (US2)probSum;
  
 reject:
  return 0;
}

/// <summary>do the actual scan of a member, and load it into results</summary>
/// <returns>a score between 0 and S1Scale*100</returns>
/// <param name="xscan">compiled representation of the query</param>
/// <param name="xmem">the member we're evaluating</param>
/// <param name="pres">the structure that holds all matching members</param>
///
static US2 doScan(S1Scan xscan, Mem xmem, PRes pres)
{
	US2 score;
	S1ScoresRec xss;
	S1ScoresNullRec(xss);
	if ((score = S1ScanExec(xscan, xmem, &xss)) > 0) {
		xscan->liveCnt += 1;
		if (PResInsert(pres, xmem->memId, score))
			xscan->dropCnt += 1;
	}
	return score;
}

/// <summary>Iterate up the tag chains to find everyone with this tag</summary>
/// <returns>1 if iteration was aborted because search scanned too many, else 0</returns>
/// <param name="xscan">compiled representation of the query</param>
/// <param name="xtag">tag that all scanned members share</param>
/// <param name="pres">the structure that holds all matching members</param>
///
static int doTagIter(S1Scan xscan, Tag xtag, PRes pres)
{
	Mem xmem;
	TagIterRec tagIter;
	TagIterInit(&tagIter, xtag, xscan->domId, xscan->gender);
	while (xmem = TagIterNext(&tagIter)) {
		// Calculate the score
		US2 score = doScan(xscan, xmem, pres);
		// Check if we've scanned too many
		if (xscan->scanCnt++ >= kS1_scanMax)
			return 1;
	}
	return 0;
}

		/// <summary>Turn a query into a result</summary>
/// <returns>Results object</returns>
/// <param name="xqry">The query</param>
///
S1Result S1Search(S1Query xqry)
{
	S1Result     xres;
	S1Scan       xscan;
	PRes         pres;
	Ldg          xldg;
	LdgIterRec   iter;
	Mem         *root, xmem;
	RankTune     rt = NULL;
	S4  resMax;
	US2 scaled; 
	int i;
	PermitSearch myPermit = xqry->permit;

	E1InterlockedInc(&(gsv->statsSearchNew));

	// If no search permit, we're out
	if (!myPermit)
	{
		return NULL;
	}

	xqry->debugCount = 1;

	// Find the RankTune, set any defaults, check argument bounds
	rt = RankTuneToUse(xqry->domId); // ??? xqry->rankTuneName SUpport named ranktunes
	{
	  // Distance to scan, in miles
	  if (xqry->distance <= 0)
		xqry->distance = rt->defaultDist;
	  else if (xqry->distance > kS1Search_DistanceMax)
		xqry->distance = kS1Search_DistanceMax;

	  if (xqry->resMax <= 0)
		xqry->resMax = rt->defaultMax;
	  if (xqry->resMax > kS1Search_ResultMax) 
		resMax = kS1Search_ResultMax;

	  if(xqry->colorCode)
	  {	  resMax=kS1SearchColorCode_ResultMax;
		  xqry->resMax=resMax;
	  }
	  //limit areacode search to return good old 360 results
	  if(xqry->areaCodes[0])
	  {
		  xqry->resMax=kS1SearchColorCode_ResultMax;
	  }

	  // Make sure sort key is valid
	  if (!xqry->sortKey)
		xqry->sortKey = rt->defaultSort;
	  else if (xqry->sortKey <= 0 || xqry->sortKey >= ArrayCount(rt->bySort))
		xqry->sortKey = rt->defaultSort;
	}

	xqry->debugCount = 2;

	// Allocate space for results
	xres = PEngAlloc(PKGNAME, StructSize(S1ResultRec));

	xqry->debugCount = 3;

	// Map the query into our internal rep optimized for scanning
	xscan = S1ScanNew(xqry, rt);
	xres->xscan = xscan;

	xqry->debugCount = 4;

	// Create optimized result gatherer/sorter
	resMax = xqry->resMax;

	xqry->debugCount = 5;

	pres = PResNew(resMax);

	xqry->debugCount = 6;

	xres->pres = pres;

	xqry->debugCount = 7;

	xres->memMax = (US2)resMax;

	xqry->debugCount = 8;

	// Start defending any memory this search may touch 
	PermitSearchBeg(myPermit);

	xqry->debugCount = 9;

	// Several different main loops that drive finding possible members -- areacode, schoolId, location

	if (xscan->acNum) {

		xqry->debugCount = 10;

		// Iterate up the tag chains to find everyone in these area codes
		for (i=0; i < xscan->acNum; i++)
			if (doTagIter(xscan, xscan->acTag[i], pres))
				goto done;

	} else if (xscan->schoolTag)  {

		xqry->debugCount = 11;

		if (xscan->schoolTag != (Tag)-1)
			if (doTagIter(xscan, xscan->schoolTag, pres))
				goto done;

	} else {

	xqry->debugCount = 12;

	// Prepare to iterate over the Ldgs in expanding circles of loc boxes
	StructNull(iter);
	LdgIterInit(&iter, &(xscan->locQry), xscan->domId, xscan->gender, xscan->distance);

	xqry->debugCount = 13;

	xscan->extendOk = (xscan->sortKey == S1Sort_Near);

	while ((xldg = LdgIterNext(&iter, (xscan->extendOk && xscan->liveCnt < kS1Search_ExtendMin)))) {
	  xqry->debugCount = 14;
	  xscan->ldgCnt++;
	  // Establish precise outer box for this LDG
	  xscan->locLdgPrecise.lat = xldg->ldgKey.loc.lat * kLoc_NSPrecise;
	  xscan->locLdgPrecise.lng = xldg->ldgKey.loc.lng * kLoc_EWPrecise;
	  // Walk linked members
	  xqry->debugCount = 15;
	  root = LdgChainMemRoot(xldg);
	  xqry->debugCount = 16;
	  for (xmem = *root; xmem; xmem = xmem->ldgChain) {
		// Calculate the score
		scaled = doScan(xscan, xmem, pres);
		xqry->debugCount = 17;
		// Check if we've scanned too many
		if (xscan->scanCnt++ >= kS1_scanMax)
		  goto done;
	  }
	}
	}
 done:
	xqry->debugCount = 18;

	// Stop defending memory
	PermitSearchEnd(myPermit, xscan->scanCnt);

	xqry->debugCount = 19;

	// Now sort and load the results
	PResLoad(pres, xres);

	xqry->debugCount = 20;

	xres->memScan = xscan->scanCnt;
	xres->memLive = xscan->liveCnt;
	xres->ldgScan = xscan->ldgCnt;

	xqry->debugCount = 21;

	return xres;
}

/// <summary>Free a results object as returned from S1Search</summary>
/// <param name="xres">Results object</param>
///
void S1ResultFree(S1Result xres)
{
  E1InterlockedInc(&(gsv->statsResultFree));

  if (xres) {
	if (xres->xscan) 
	  S1ScanFree(xres->xscan);
	if (xres->pres)
	  PResFree(xres->pres);
	PEngFree(PKGNAME, xres, sizeof(*xres));
  }
}

/****************************************************************/

static US2 redoScores(S1Scan xscan, Mem xmem, S1Scores scores)
{
  S1ScoresNull(scores);
  xscan->locLdgPrecise.lat = xmem->ldg->ldgKey.loc.lat * kLoc_NSPrecise;
  xscan->locLdgPrecise.lng = xmem->ldg->ldgKey.loc.lng * kLoc_EWPrecise;
  return S1ScanExec(xscan, xmem, scores);
}

/// <summary>Fill a MemSpec and S1ScoresRec with description of this result</summary>
/// <returns>Combined score as % between 0 and 1</returns>
/// <param name="xres">Results object</param>
/// <param name="idx">Which result to describe (0 for first in list, ...)</param>
/// <param name="outSpec">Optional MemSpec to fill with info about member</param>
/// <param name="outScores">Optional S1Scores to fill with info about member</param>
///
float S1ResultDetailsIdx(S1Result xres, S4 idx, MemSpec outSpec, S1Scores outScores)
{
  if (!xres || !xres->xscan || idx >= xres->memNum)
	return 0.0;
  else {
	S1Scan xscan = xres->xscan;
	US2 newScr, oldScr = xres->scores[idx];
	Mem xmem;

	// Find the member
	MemKeyRec mk;
	StructNull(mk);
	mk.memId = xres->memids[idx];
	mk.domId = xscan->domId;
	xmem = MemFind(&mk);

	if (xmem && outSpec) {
	  // Get all member attributes
	  memset(outSpec, 0, sizeof(*outSpec));
	  MemToMemSpec(xmem, outSpec, xscan->detailScratch, sizeof(xscan->detailScratch));
	}
	if (xmem && outScores) {
	  // Rescan to get all scores
	  newScr = redoScores(xscan, xmem, outScores);
	}

	return (float)oldScr/(float)S1Identity;
  }
}

/****************************************************************/

void S1ShowScores(char *buf, int siz, int sort, S1Scores b)
{
  char tmp[16];
  if (!sort) tmp[0] = 0;
  else snprintf(tmp, sizeof(tmp), " sort='%d'", sort);
  snprintf(buf, siz, "blend%s cscz='%d' ldg='%d' age='%d' hgt='%d' match='%d' act='%d' new='%d' pop='%d' mol='%d'",
		   tmp, b->cscz,b->ldg,b->age,b->hgt,b->match,b->act,b->newreg,b->pop,b->mol);
}

/// <summary>Serialize the members in a results object to XML</summary>
/// <param name="xres">Results object</param>
/// <param name="px">XML string buffer to extend</param>
/// <param name="mode">Controls what to show
///    mode&1 == Show results list too
///    mode&8 == regenerate and show all subscores for member
///    mode&(8|16) == show all of member's tags
/// </param>
///
static int S1ResultXmlMem(S1Result xres, PXml px, int mode)
{
  int err, i;
  char msgBuf[512];
  MemKeyRec   mk;
  S1ScoresRec xss;
  S1Scan xscan;
  Mem    xmem;

  xscan = xres->xscan;
  StructNull(mk);
  mk.domId = xscan->domId;

  err = PXmlPush(px, "reslist", xres->memNum);
  
  for (i = 0; !err && i < xres->memNum; i++) {
	US2 newScr, oldScr = xres->scores[i];
	mk.memId = xres->memids[i];
	snprintf(msgBuf, sizeof(msgBuf), "res idx='%d' memid='%ld' score='%d' aspct='%.3f'",
			 i, mk.memId, oldScr, (float)oldScr/(float)S1Identity);
	if (!(mode & 8) || !(xmem = MemFind(&mk))) {
      err |= PXmlPush0(px, msgBuf, 0);
	} else {
	  err |= PXmlPush(px, msgBuf, 0);
	  newScr = redoScores(xscan, xmem, &xss);
	  S1ShowScores(msgBuf, sizeof(msgBuf), 0, &xss);
	  err |= PXmlPush0(px, msgBuf, 0);
	  if (mode & 16)
		err |= MemXml(px, xmem, 3);
	  err |= PXmlPop(px);
	}
  }
  err |= PXmlPop(px);
  return err;
}


/// <summary>Serialize the query into the XML</summary>
/// <param name="xres">Results object</param>
/// <param name="px">XML string buffer to extend</param>
/// <param name="mode">Controls what to show, ignored for now</param>
///
static int S1ResultXmlQry(S1Result xres, PXml px, int mode)
{
  int i, err;
  char msgBuf[512], msgLen = 0;
  S1Scan xscan = xres->xscan;

  MemDateRec today;
  today.pack = GsvGetTodayPack(NULL);

  snprintf(msgBuf, sizeof(msgBuf), 
		   "query domid='%d' gender='%d' sortkey='%d' agemin='%d' agemax='%d' hasphoto='%d' isonline='%d'",
		   xscan->domId, xscan->gender, xscan->sortKey, xscan->ageMin, xscan->ageMax, 
		   xscan->hasPhoto, xscan->isOnline);
  if ((err = PXmlPush(px, msgBuf, 0)) < 0)
	return err;

  if (xscan->acNum) {
    snprintf(msgBuf, sizeof(msgBuf), "areacodes original='%0.3d", xscan->acDigit[0]);
	msgBuf[sizeof(msgBuf)-1] = 0;
	msgLen = strlen(msgBuf);
	for (i=1; xscan->acDigit[i]; i++) {
		snprintf(msgBuf+msgLen, sizeof(msgBuf)-msgLen, ",%0.3d", xscan->acDigit[i]);
		msgLen += strlen(msgBuf+msgLen);
	}
	msgBuf[msgLen++] = '\''; msgBuf[msgLen] = 0; 
	err |= PXmlPush(px, msgBuf, xscan->acNum);
	for (i=0; i < xscan->acNum; i++)
	  err |= TagXml(px, xscan->acTag[i], 0);
	err |= PXmlPop(px);
  } else {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "location distance='%d' blat='%ld' blng='%ld' plat='%ld' plng='%ld'",
			 xscan->distance, xscan->locQry.lat, xscan->locQry.lng, 
			 xscan->locQryPrecise.lat, xscan->locQryPrecise.lng);
	err = PXmlPush0(px, msgBuf, 0);
  }

  if (1) {
	S1ShowScores(msgBuf, sizeof(msgBuf), 0, &(xscan->blend));
	err |= PXmlPush0(px, msgBuf, 0);
  }

  if (1) {
	int amin, amax;
	for (amin = kRank_AgeMin; amin < kRank_AgeMax; amin++)
	  if (xscan->byAge[amin] > 0) break;
	for (amax = kRank_AgeMax; amax > kRank_AgeMin; amax--)
	  if (xscan->byAge[amax] > 0) break;
	snprintf(msgBuf, sizeof(msgBuf), 
			 "ages today='%d-%d-%d' ageminok='%d' agemaxok='%d'",
			 1900+today.v.year, today.v.month, today.v.day, amin, amax);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  if (xscan->hgtMin) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "hgt min='%d' max='%d''", xscan->hgtMin, xscan->hgtMax);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  if (xscan->wgtMin) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "wgt min='%d' max='%d''", xscan->wgtMin, xscan->wgtMax);
	err |= PXmlPush0(px, msgBuf, 0);
  }

  // Show the tags in the query
  if (!err && xscan->tagCnt) {
	err |= PXmlPush(px, "tags", xscan->tagCnt);
	for (i = 0; !err && i < xscan->tagCnt; i++) {
	  Tag xtag = xscan->tagDat[i];
	  err |= TagXml(px, xtag, 0);
	}
	err |= PXmlPop(px);
  }

  err |= PXmlPop(px);
  return err;
}

/// <summary>Serialize a results object into XML</summary>
/// <param name="xres">Results object</param>
/// <param name="px">XML string buffer to extend</param>
/// <param name="mode">Controls what to show
///    mode==0  Just show counts
///    mode&1 == Show results list too
///    mode&2 == Show the query 
///    mode&4 == Show the details of the result gatherer
///    mode&8 == regenerate and show all subscores for member
///    mode&(8|16) == show all of member's tags
/// </param>
///
int S1ResultXml(S1Result xres, PXml px, int mode)
{
  char msgBuf[128];
  int err;
  S1Scan xscan;

  if (!xres) 
	  return -1;
  xscan = xres->xscan;

  snprintf(msgBuf, sizeof(msgBuf), "result cnt='%d' scancnt='%ld' livecnt='%ld' dropcnt='%ld' ldgcnt='%d' ortarget='%ld'",
		   xres->memNum, xscan->scanCnt, xscan->liveCnt, xscan->dropCnt, xscan->ldgCnt, xscan->orTarget);
  
  if (mode == 0)
	return PXmlPush0(px, msgBuf, 0);
  
  err = PXmlPush(px, msgBuf, xres->sequence);
  
  if (!err && (mode & 2))
	err |= S1ResultXmlQry(xres, px, mode);
  if (!err && (mode & 4) && xres->pres)
	err |= PResXml(px, (PRes)xres->pres, mode);
  if (!err && (mode & (1|8)))
	err |= S1ResultXmlMem(xres, px, mode);

  err |= PXmlPop(px);
  
  return err;
}
