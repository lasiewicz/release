// C++ interface to overall engine state
//

#include "e1cpp.h"

using namespace Matchnet::e1::Engine;

E1Gsv::E1Gsv(char *name)
{
	GsvInitialize();
}

E1Gsv::~E1Gsv()
{
	GsvDestroy();
}
