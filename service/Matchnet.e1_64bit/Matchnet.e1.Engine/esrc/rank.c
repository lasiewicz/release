// rank.c
// pnelson 11/25/2005
// Ranking algorithms
//

#include "peng.h"
#include "cmd.h"

#define PKGNAME PkgName_Rank

// How days since last active map to a 4bit "active" score
static RankTuneActRec gsvRtAct = 
  { { 0, 180, 135, 102, 76, 56, 41, 30, 22, 16, 11, 7, 4, 2, 1, 0 } };

// How days since registration map to a 4bit "new" score
static RankTuneNewRec gsvRtNew = 
  { { 0, 850, 598, 409, 276, 185, 122, 80, 52, 31, 17, 10, 7, 3, 1, 0 } };

// How recent email count maps to a 4bit "popular" score
static RankTunePopRec gsvRtPop = 
  { { 0, 1,2,4,7,11,17,25,35,48,64,84,109,140,178,224 }};

// Scores for overlapping country, and state, and city, and zip
static RankTuneCsczRec gsvRtCscz =
  { 5, 10, 75, 95 };

// How boxes away map to a 4bit "near" score (precise box size is 1 mile)
static RankTuneLdgRec gsvRtLdg = 
  { { 0, 150, 110, 80, 60, 45, 34, 26, 19, 13, 8, 5, 3, 2, 1, 0 }};

// How to build a score curve around a specified age in the query
static RankTuneAgeRec gsvRtAge = 
{ 0, 100, 90, 30, { 4,8,12,16,0 }, { 2,4,7,10,0 } };
  //{ 0, 100, 90, 30, { 0,0,0,0,0 }, { 0,0,0,0,0 } };
//  

// How to build a hgt curve around a specified height range in the query
static RankTuneHgtRec gsvRtHgt = 
  { 100, 90, 40, 4 };

//   mtch ldg  cscz age  hgt  new  act  pop
static S1ScoresRec gsvRtSortAct = 
{ -101,  -15,   25,  -10,    0,   10,   80,    4,    0 };

static S1ScoresRec gsvRtSortNear = 
{ -101,  -60,   75,  -10,    0,    5,   20,    4,   10 };

static S1ScoresRec gsvRtSortNew = 
{ -101,  -15,   25,  -10,    0,   75,   10,    4,   25 };

static S1ScoresRec gsvRtSortPop = 
{ -101,  -15,   25,  -10,    0,   10,   25,   60,   10 };

static S1ScoresRec gsvRtSortMol = 
{ -101,  -60,   75,  -50,    0,   25,    0,    4, -101 };

static RankTuneRec gsvRank;

/****************/

static void rankLdgSquared(RankTune rt)
{
	int i;
	for (i=0; i < kRank_BandMax; i++)
		rt->ldgSquared.bands[i] = (S4)rt->ldg.bands[i] * (S4)rt->ldg.bands[i];
}

const double ONE_GRAM_IN_POUNDS = 0.00220462262;


/// <summary>Boot strap our DB of rank tuning options</summary>
///
void RankInitialize()
{
  // Bootstrap the hardcoded tuning parameters
  StructNull(gsvRank);
  gsvRank.defaultSort = S1Sort_Popular;
  gsvRank.defaultMax  = 999;
  gsvRank.defaultDist = 50;

  gsvRank.age    = gsvRtAge;
  gsvRank.hgt    = gsvRtHgt;
  gsvRank.act    = gsvRtAct;
  gsvRank.newreg = gsvRtNew;
  gsvRank.pop    = gsvRtPop;
  gsvRank.cscz   = gsvRtCscz;
  // Also calc squares of ldg bands
  gsvRank.ldg    = gsvRtLdg;
  rankLdgSquared(&gsvRank);
  // Load the sorting blends
  gsvRank.bySort[(int)S1Sort_Newest]  = gsvRtSortNew;
  gsvRank.bySort[(int)S1Sort_Active]  = gsvRtSortAct;
  gsvRank.bySort[(int)S1Sort_Near]    = gsvRtSortNear;
  gsvRank.bySort[(int)S1Sort_Popular] = gsvRtSortPop;
  gsvRank.bySort[(int)S1Sort_Online]  = gsvRtSortMol;
}

/// <summary>Get RankTune for this domain -- but don't change it</summary>
/// <param name="domid">The domain</param>
/// <returns>RankTune, will never be NULL</returns>
///
RankTune RankTuneToUse(DomId domId)
{
	RankTune rtx, myrt = NULL;
	if (!gsvRank.defaultSort)
		RankInitialize();
	for (rtx = &gsvRank; rtx; rtx = rtx->rtNext)
		if (rtx->domId == 0 || rtx->domId == domId)
			myrt = rtx;
	return myrt;
}

/// <summary>Get RankTune for this domain -- can be edited</summary>
/// <param name="domid">The domain</param>
/// <returns>RankTune to edit, or NULL if RankTune not available and can't be created</returns>
///
RankTune RankTuneToEdit(DomId domId)
{
	RankTune rtx, myrt = NULL;
	if (!gsvRank.defaultSort)
		RankInitialize();
	for (rtx = &gsvRank; rtx; rtx = rtx->rtNext)
		if (rtx->domId == domId)
			myrt = rtx;
	if (!myrt) {
		PermitWrite permit = 0;
		if (0 == PermitWriteTake("RankTune", &permit, 0)) {
			// Check again now that we have a lock
			for (rtx = &gsvRank; rtx; rtx = rtx->rtNext)
				if (rtx->domId == domId)
					myrt = rtx;
			// Still don't have it, make one
			if (!myrt) {
				myrt = PEngAlloc(PKGNAME, sizeof(RankTuneRec));
				*myrt = gsvRank;  // sets myrt->rtNext to ranktune at gsvRank.rtNext
				myrt->domId = domId;
				gsvRank.rtNext = myrt;
			}
			PermitWriteGive(permit);
		}
	}
	return myrt;
}

int RankTuneSortSet(DomId domid, S1SortKey sort, S1Scores blend)
{
	RankTune rt = RankTuneToEdit(domid);
	if (rt && sort > 0 && sort < kS1Sort_Max) {
		rt->bySort[(int)sort] = *blend;
		return 0;
	} else {
		return -1;
	}
}

int RankTuneSortGet(DomId domid, S1SortKey sort, S1Scores blend)
{
	RankTune rt = RankTuneToEdit(domid);
	if (rt && sort > 0 && sort < kS1Sort_Max) {
		*blend = rt->bySort[(int)sort];
		return 0;
	} else {
		return -1;
	}
}

/****************/

/// <summary>Convert [age in years] into corresponding MemDate.pack value of birthday</summary>
/// <param name="age">age in years</param>
/// <returns>MemDate.pack of birthdate of someone who turned that age today</returns>
///
US2 RankPackAge(int age)
{
  MemDateRec tt;
  tt.pack = GsvGetTodayPack(NULL);
  tt.v.year -= age;
  return tt.pack;
}

/// <summary>Pack hgt in cms into the corresponding shifted value stored in members </summary>
/// <param name="hgt">target hgt in cms</param>
/// <returns>Shifted and bounds checked hgt for direct comparision to value stored in Mem</returns>
///
int RankPackHgt(int hgt)
{
  if (hgt == 0) return 0;
  hgt -= kMemPack_HeightMin;
  hgt = minmax(hgt, 1, kMemPack_HeightRng);
  return hgt;
}

/// <summary>Pack wgt in grams into corresponding shifted value stored in members </summary>
/// <param name="wgt">target wgt in grams</param>
/// <returns>Shifted and bounds checked wgt for direct comparision to value stored in Mem</returns>
///
//	FYI, these constants are defined in Matchnet.Constants:
//		public const double WEIGHT_MIN = 36288;  //these are in grams
//		public const double WEIGHT_MAX = 181500;
int RankPackWgt(int wgt)
{
	//first convert to pounds:
	wgt = wgt * ONE_GRAM_IN_POUNDS;

	//now pack
	if (wgt == 0) return 0;
	wgt -= kMemPack_WeightMin;
	wgt = minmax(wgt, 1, kMemPack_WeightRng);
	return wgt;
}

/****************/

/// <summary>Convert last active date to an Activity Score based on RankTune</summary>
/// <param name="activePack">MemDateTime.pack version of last active date</param>
/// <returns>Score equal to 655*[0 to 100] where higher scores are more recently active</returns>
///
S4 RankActExecAux(RankTune rt, US4 activePack, int as4bit)
{
  int scr;
  RankTuneAct ra = &(rt->act);
  US4 thenMinutes, nowMinutes;
  int agoMinutes;

  if (activePack == 0)
	  return 0;

  thenMinutes = MemPackDateTimeAsMinutes(activePack);
  (void)GsvGetMinutesPack(&nowMinutes);
  agoMinutes = nowMinutes - thenMinutes;

  if (agoMinutes > MAX_ACTIVE_TIME)
	  agoMinutes = MAX_ACTIVE_TIME;

  //ignore the bands for now... just return a linearly scaled score based on number of minutes ago:
  return MAX_ACTIVE_TIME - agoMinutes;

  // Go for the highest score, 
  /*for (scr=kRank_BandMax-1; scr > 0; scr-- )
	if (agoMinutes <= ra->bands[scr])
	  break;
  return scr * (as4bit ? 1 : 6);*/
}

S1Score RankActExec(RankTune rt, US4 activePack, int as4bit)
{
	return (S1Score) ((RankActExecAux(rt, activePack, as4bit) * 100) / MAX_ACTIVE_TIME);
}

/// <summary>Convert registration date to a "New" Score based on RankTune</summary>
/// <param name="registerPack">MemDate.pack version of registration date</param>
/// <returns>Score equal to 6*[0 to 15] where higher scores are more recently registered</returns>
///
S1Score RankNewExec(RankTune rt, US2 registerPack, int as4bit)
{
  int scr;
  RankTuneNew rn = &(rt->newreg);
  US2 thenDays, nowDays, agoDays;

  if (registerPack == 0)
	  return 0;

  thenDays = MemPackDateAsDays(registerPack);
  (void)GsvGetTodayPack(&nowDays);
  agoDays = nowDays - thenDays;

  // Go for the highest score, 
  for (scr=kRank_BandMax-1; scr > 0; scr-- )
	if (agoDays <= rn->bands[scr])
	  break;
  return scr * (as4bit ? 1 : 6);
}

/// <summary>Convert recent email count into "Popularity" score based on RankTune</summary>
/// <param name="emailCnt">Integer count of recent unique email senders</param>
/// <returns>Score equal to 6*[0 to 15] where higher scores are more popular</returns>
///
S1Score RankPopExec(RankTune rt, S4 emailCnt, int as4bit)
{
  int scr;
  RankTunePop rp = &(rt->pop);

  if (emailCnt <= 0)
	  return 0;

  // Go for the highest score, 
  for (scr=kRank_BandMax-1; scr > 0; scr-- )
	if (emailCnt >= (S4)rp->bands[scr])
	  break;
  return scr * (as4bit ? 1 : 6);
}

/// <summary>Generate a score based on overlap of country, state, city, zip</summary>
/// <param name="xscan">MemCSCZMap representing query location</param>
/// <param name="xmem">MemCSCZMap representing member location</param>
/// <returns>Score based on how much of CSCZ overlaps between query and mem</returns>
///
S1Score RankCsczExec(RankTune rt, MemCSCZMap xscan, MemCSCZMap xmem)
{
  RankTuneCscz rc = &(rt->cscz);
  S1Score scr = 0;

  if (xscan->pkCountry && (xmem->pkCountry == xscan->pkCountry)) {
	scr = rc->inCountry;
	if (xscan->pkState && (xmem->pkState == xscan->pkState)) {
	  scr = rc->inState;
	  if (xscan->pkCity && (xmem->pkCity == xscan->pkCity)) {
		scr = rc->inCity;
		if (xscan->pkZip && (xmem->pkZip == xscan->pkZip)) {
		  scr = rc->inZip;
		}
	  }
	}
  }
  return scr;
}

/// <summary>Generate a score from distance measured in *Precise* boxes from member to query</summary>
/// <param name="miles">User specified distance in miles</param>
/// <param name="xscan">PreciseLoc representing query location</param>
/// <param name="xmem">PreciseLoc representing member location</param>
/// <returns>Score equal to 6*[0 to 15] where higher scores are closer</returns>
///
S1Score RankLdgExec(RankTune rt, S2 miles, Loc xscan, Loc xmem, int extendOk)
{
  RankTuneLdg rl;
  LocRec delta;
  int scr; S4 boxes;

  // Latitude does not wrap - calc absolute distance
  if (xscan->lat > xmem->lat) 
	delta.lat = xscan->lat - xmem->lat;
  else
	delta.lat = xmem->lat - xscan->lat;
  // Longitude wraps around dateline -- calc closest distance
  if (xscan->lng > xmem->lng)
	delta.lng = xscan->lng - xmem->lng;
  else
	delta.lng = xmem->lng - xscan->lng;
  // Wrap latitude if we're too many boxes away
  boxes = kLoc_EWBoxes * kLoc_EWPrecise;
  if (delta.lng > boxes/2)
	delta.lng = (US2)boxes - delta.lng; 
  
  // Calculate hypotenuse distance, leave it squared
  boxes = ((S4)delta.lat * (S4)delta.lat) + ((S4)delta.lng * (S4)delta.lng);

  // penalize score for being beyond requested miles
  if (!extendOk && boxes > (S4) miles * miles)
  {
	  return 0;
  }
  
  // Now figure out what band we're in (using ldg^2), closer is better
  rl = &(rt->ldgSquared);
  for (scr=kRank_BandMax-1; scr > 0; scr-- )
	if (boxes <= (S4)rl->bands[scr])
	  break;

  return scr * 6;
}

/// <summary>Generate a score based on closeness of match to requested height</summary>
/// <param name="hmin">Packed version of requested min height, must be l.e. hmax</param>
/// <param name="hmax">Packed version of requested max height, must be g.e. hmin</param>
/// <param name="xmem">packed version of Member's height</param>
/// <returns>0-100 Score of this match, higher scores are closer match</returns>
///
S1Score RankHgtExec(RankTune rt, int hmin, int hmax, int xmem)
{
  RankTuneHgt rh = &(rt->hgt);
  S4 score;
  int midp = (hmax + hmin) / 2;
  int gap  = (hmax - midp);
  int srng = (rh->inMax - rh->inMin);

  if (xmem == 0 || hmin == 0) {
	// Don't know height, or don't care
	score = 0;
  } else if (xmem < hmin) {
	// Mem is shorter
	score = rh->outMax - (rh->perCm * (hmin - xmem));
  } else if (xmem > hmax) {
	// Mem is taller
	score = rh->outMax - (rh->perCm * (xmem - hmax));
  } else if (gap < 1 || xmem == midp || srng == 0) {
	// At midpt, or no gap, or no inBand blending makes us inMax
	score = rh->inMax;
  } else if (xmem == hmin || xmem == hmax) {
	// At edge makes us inMin
	score = rh->inMin;
  } else {
	// Blend based on how far we are from center
	int deltaHgt = (xmem < midp) ? (midp - xmem) : (xmem - midp);
	int deltaPct = (deltaHgt * 100) / gap;
	score = rh->inMax - (deltaPct * srng) / 100;
  }
  return (S1Score)(score < 0 ? 0 : score);
}

/// <summary>Generate a score based on closeness of match to requested weight</summary>
/// <param name="wmin">Packed version of requested min weight, must be l.e. wmax</param>
/// <param name="wmax">Packed version of requested max weight, must be g.e. wmin</param>
/// <param name="xmem">packed version of Member's height</param>
/// <returns>0-100 Score of this match, higher scores are closer match</returns>
///
S1Score RankWgtExec(RankTune rt, int wmin, int wmax, int xmem)
{
  RankTuneHgt rh = &(rt->hgt);
  S4 score = 0;

  if (wmin == 0)
  {
	  score = 0;
  }
  else if (xmem == 0)
  {
	  score = 1;
  }
  else if (wmin <= xmem && xmem <= wmax)
  {
	  score = 100;
  }

  return (S1Score)(score < 0 ? 0 : score);
}

/// <summary>Generate a score for every possible age based on query, scanning then does lookup[age]
///   Special cases:
///     min=0, max=0  ==> all scores are zero
///     min=0, max=x  ==> min is youngest possible, x is hard limit on max ("no older then")
///     min=x, max=0  ==> x is hard limit on min, max is oldest possible ("no younger then")
///     max < min     ==> if max is less then min, switch them and assume each are hard limits
///</summary>
/// <param name="byAge">pre allocated array that will be filled with scores</param>
/// <param name="amin">minimum requested age</param>
/// <param name="amax">maximum requested age</param>
/// <returns>nothing, but filsl the byAge array with scores</returns>
///
void RankAgeInit(RankTune rt, US1 *byAge, US1 amin, US1 amax)
{
  RankTuneAge ra = &(rt->age);

  int ageMin, ageMax, deltaAge, deltaPct, score, age, i, penalty;
  int midp, gap, srng;

  // amin and amax are the edges of the requested band, outside that, penalty extracted down to zero
  // ageMin and ageMax are calculated absolute limits for this query -- outside that score is zero

  // Set all to zero to begin
  memset(byAge, 0, kRank_AgeVctMax);

  // If both 0, we don't care about age
  if (!amin && !amax)
	return;

  // Handle other special cases
  ageMin = ageMax = 0;
  if (!amin && amax) {
	// If just specified max, assume min is yng possible, max is hard limit
	amin   = kRank_AgeMin;
	ageMax = amax;
  } else if (amin && !amax) {
	// If just specified min, assume max is old possible, min is hard limit
	ageMin = amin;
	amax   = kRank_AgeMax;
  } else if (amin > amax) {
	// if yng > old, switch them, and assume each is a hard boundary
	US1 tmp = amax;
	ageMax = amax = amin;
	ageMin = amin = tmp;
  }

  // Make sure our hard limits are set
  if (ageMin == 0 || ageMin < kRank_AgeMin) 
	ageMin = kRank_AgeMin;
  if (ageMax == 0 || ageMax > kRank_AgeMax) 
	ageMax = kRank_AgeMax;

  midp = (amax + amin) / 2;       // Midpt between max and min
  gap  = amax - midp;             // gap between midpt and edge
  srng = (ra->inMax - ra->inMin); // range of scores between edge and midpt

  // Now calculate score per every age between our hard limits
  for (age = kRank_AgeMin; age < kRank_AgeMax; age++) {
	if (age < ageMin || age > ageMax) 
	  score = 0;
	else if (age < amin) {
	  // Out of band younger
	  score  = 0;
	  deltaAge = amin - age;
	  deltaPct = (deltaAge * 100) / amin; // Relative to min age
	  for (i=0; i < ArrayCount(ra->pctDif) && ra->pctDif[i]; i++)
		if (deltaPct <= ra->pctDif[i]) {
		  penalty = (deltaAge * ra->perYear[i]);
		  if (penalty < ra->outMax)
			score = ra->outMax - penalty;
		  break;
		}
	} else if (age > amax) {
	  // Out of band older
	  score  = 0;
	  deltaAge = age - amax;
	  deltaPct = (deltaAge * 100) / amax; // Relative to max age
	  for (i=0; i < ArrayCount(ra->pctDif) && ra->pctDif[i]; i++)
		if (deltaPct <= ra->pctDif[i]) {
		  penalty = (deltaAge * ra->perYear[i]);
		  if (penalty < ra->outMax)
			score = ra->outMax - penalty;
		  break;
		}
	} else if (gap < 1 || age == midp || srng == 0) {
	  // We're at the max
	  score = ra->inMax;
	} else if (age == amin || age == amax) {
	  // We're at the edge
	  score = ra->inMin;
	} else {
	  // We're in the middle
	  deltaAge = (age < midp ? midp - age : age - midp);
	  deltaPct = (deltaAge * 100) / gap;
	  score = ra->inMax - (deltaPct * srng) / 100;
	}
	byAge[age] = score;
  }
}

/****************************************************************/

// A macro that shows where in structure a named slot will be
// 
#define SO(rec, fld)	(int)(&(((rec)0)->fld))

// The spec for updating a ranktune
typedef struct _CmdSpecRankTune {
  CmdSpecRec spec;
  RankTuneRec rt;
  STR         name;
  US2 defSort, defMax, defDist;
  // Grap the blends the way cmd knows how to parse the, then place them in ranktune
  US1 blndSort[kS1Sort_Max];   // which sort for each index in array
  US1 blndCscz[kS1Sort_Max];
  US1 blndLdg[kS1Sort_Max];
  US1 blndAge[kS1Sort_Max];
  US1 blndHgt[kS1Sort_Max];
  US1 blndMatch[kS1Sort_Max];
  US1 blndAct[kS1Sort_Max];
  US1 blndNew[kS1Sort_Max];
  US1 blndPop[kS1Sort_Max];
  US1 blndMol[kS1Sort_Max];
} CmdSpecRankTuneRec, *CmdSpecRankTune;

CmdSpecRec cmdSpecRankTune = {
  "ranktune",
  KW_ranktune, KW_null, sizeof(CmdSpecRankTuneRec), { 
	{ KW_name,   Arg_Str, SO(CmdSpecRankTune, name) },
	{ KW_domid,  Arg_S1,  SO(CmdSpecRankTune, rt.domId) },

	{ KW_rt_defsort, Arg_S2, SO(CmdSpecRankTune, defSort) },
	{ KW_rt_defmax,  Arg_S2, SO(CmdSpecRankTune, defMax) },
	{ KW_rt_defdist, Arg_S2, SO(CmdSpecRankTune, defDist) },

	{ KW_rt_act, Arg_S4,   SO(CmdSpecRankTune, rt.act.bands), kRank_BandMax },
	{ KW_rt_new, Arg_S4,   SO(CmdSpecRankTune, rt.newreg.bands), kRank_BandMax },
	{ KW_rt_pop, Arg_S4,   SO(CmdSpecRankTune, rt.pop.bands), kRank_BandMax },
	{ KW_rt_ldg, Arg_S4,   SO(CmdSpecRankTune, rt.ldg.bands), kRank_BandMax },

	{ KW_rt_cscz_reg1, Arg_S1, SO(CmdSpecRankTune, rt.cscz.inCountry) },
	{ KW_rt_cscz_reg2, Arg_S1, SO(CmdSpecRankTune, rt.cscz.inState) },
	{ KW_rt_cscz_reg3, Arg_S1, SO(CmdSpecRankTune, rt.cscz.inCity) },
	{ KW_rt_cscz_reg4, Arg_S1, SO(CmdSpecRankTune, rt.cscz.inZip) },

	{ KW_rt_age_algo,   Arg_S1, SO(CmdSpecRankTune, rt.age.algoSelect) },
	{ KW_rt_age_inmax,  Arg_S1, SO(CmdSpecRankTune, rt.age.inMax) },
	{ KW_rt_age_inmin,  Arg_S1, SO(CmdSpecRankTune, rt.age.inMin) },
	{ KW_rt_age_outmax, Arg_S1, SO(CmdSpecRankTune, rt.age.outMax) },
	{ KW_rt_age_pct,    Arg_S1, SO(CmdSpecRankTune, rt.age.pctDif), kRank_AgeBandMax },
	{ KW_rt_age_peryr,  Arg_S1, SO(CmdSpecRankTune, rt.age.perYear), kRank_AgeBandMax },

	{ KW_rt_hgt_inmax,  Arg_S1, SO(CmdSpecRankTune, rt.hgt.inMax) },
	{ KW_rt_hgt_inmin,  Arg_S1, SO(CmdSpecRankTune, rt.hgt.inMin) },
	{ KW_rt_hgt_outmax, Arg_S1, SO(CmdSpecRankTune, rt.hgt.outMax) },
	{ KW_rt_hgt_percm,  Arg_S1, SO(CmdSpecRankTune, rt.hgt.perCm) },

	{ KW_rtb_sort,  Arg_S1, SO(CmdSpecRankTune, blndSort), kS1Sort_Max },
	{ KW_rtb_cscz,  Arg_S1, SO(CmdSpecRankTune, blndCscz), kS1Sort_Max },
	{ KW_rtb_ldg,   Arg_S1, SO(CmdSpecRankTune, blndLdg), kS1Sort_Max },
	{ KW_rtb_age,   Arg_S1, SO(CmdSpecRankTune, blndAge), kS1Sort_Max },
	{ KW_rtb_hgt,   Arg_S1, SO(CmdSpecRankTune, blndHgt), kS1Sort_Max },
	{ KW_rtb_match, Arg_S1, SO(CmdSpecRankTune, blndMatch), kS1Sort_Max },
	{ KW_rtb_act,   Arg_S1, SO(CmdSpecRankTune, blndAct), kS1Sort_Max },
	{ KW_rtb_new,   Arg_S1, SO(CmdSpecRankTune, blndNew), kS1Sort_Max },
	{ KW_rtb_pop,   Arg_S1, SO(CmdSpecRankTune, blndPop), kS1Sort_Max },
	{ KW_rtb_mol,   Arg_S1, SO(CmdSpecRankTune, blndMol), kS1Sort_Max },
	{ 0 }
  }
};

static int rtBandCheck(US4 *band, int desc)
{
  int i;
  // OK for first element in source to be zero, but not first and second
  if (band[0] == 0 && band[1] == 0 && band[2] == 0)
	return 0;

  for (i=1; i<kRank_BandMax; i++) {
	if (band[i] == 0) break;
	if (desc) 
	  { if (band[i] > band[i-1]) break; }
	else
	  { if (band[i] < band[i-1]) break; }
  }
  return i;
}
	  
static void rtBandCopy(US4 *dst, US4 *src, int x)
{
  // Reverse them on copy since they are passed to Cmd best score first
  int s, d, i;

  s = 0;
  d = kRank_BandMax;
  for (i = 0 ; i < x; i++)
	dst[--d] = src[s++];
  for ( ; i < kRank_BandMax; i++)
	dst[--d] = 0;
}

static void rtBlendSet(S1Score val, S1Score *ptr)
{
  if (val == S1Blend_ForceNull)
	*ptr = 0;
  else if (val)
	*ptr = val;
}

int cmdExecRankTune(CmdSpec xcs, PXml px)
{
  int bx, i, errCode = 0;
  CmdSpecRankTune cs = (CmdSpecRankTune)xcs;
  RankTune src = &(cs->rt);
  RankTune dst;

  if (!(dst = RankTuneToEdit(src->domId))) {
	  PXmlPush0(px, "error type='GetRankTuneToEdit'", 0);
	  return -1;
  }

  // Error check what was submitted, and push it into main rt (for now)
  if (cs->defSort > 0 && cs->defSort < kS1Sort_Max)
	dst->defaultSort = (S1SortKey)cs->defSort;

  if (cs->defMax > 0)
	dst->defaultMax = minmax(cs->defMax, kS1Sort_Max, kS1Search_ResultMax);

  if (cs->defDist > 0)
	dst->defaultDist = minmax(cs->defDist, 1, kS1Search_DistanceMax);

  //
  if (bx = rtBandCheck(src->act.bands, 0))
	rtBandCopy(dst->act.bands, src->act.bands, bx);

  if (bx = rtBandCheck(src->pop.bands, 1))
	rtBandCopy(dst->pop.bands, src->pop.bands, bx);

  if (bx = rtBandCheck(src->newreg.bands, 0))
	rtBandCopy(dst->newreg.bands, src->newreg.bands, bx);

  if (bx = rtBandCheck(src->ldg.bands, 0)) {
	rtBandCopy(dst->ldg.bands, src->ldg.bands, bx);
	rankLdgSquared(&gsvRank);
  }

  // 
  if (src->cscz.inCountry)
	dst->cscz.inCountry = src->cscz.inCountry;

  if (src->cscz.inState)
	dst->cscz.inState = src->cscz.inState;

  if (src->cscz.inCity)
	dst->cscz.inCity = src->cscz.inCity;

  if (src->cscz.inZip)
	dst->cscz.inZip = src->cscz.inZip;

  // 
  if (src->age.inMax)
	dst->age = src->age;

  if (src->hgt.inMax)
	dst->hgt = src->hgt;

  //
  for (i=0; i<kS1Sort_Max && cs->blndSort[i]; i++) {
	int sortKey = cs->blndSort[i];
	if (sortKey > 0 && sortKey < kS1Sort_Max) {
	  rtBlendSet(cs->blndCscz[i],  &(dst->bySort[sortKey].cscz));
	  rtBlendSet(cs->blndLdg[i],   &(dst->bySort[sortKey].ldg));
	  rtBlendSet(cs->blndAge[i],   &(dst->bySort[sortKey].age));
	  rtBlendSet(cs->blndHgt[i],   &(dst->bySort[sortKey].hgt));
	  rtBlendSet(cs->blndMatch[i], &(dst->bySort[sortKey].match));
	  rtBlendSet(cs->blndAct[i],   &(dst->bySort[sortKey].act));
	  rtBlendSet(cs->blndNew[i],   &(dst->bySort[sortKey].newreg));
	  rtBlendSet(cs->blndPop[i],   &(dst->bySort[sortKey].pop));
	  rtBlendSet(cs->blndMol[i],   &(dst->bySort[sortKey].mol));
	}
  }

  // Show this one first, then all the others
  errCode = RankTuneXml(px, dst->domId);
  for (src = &gsvRank; src; src = src->rtNext)
	  if (src->domId != dst->domId)
		errCode |= RankTuneXml(px, src->domId);
  return errCode;
}

static void rankTuneBandToBuf(char *buf, int bufSiz, cSTR tag, US4 *band)
{
  int i, len = 0;
  str0ncpy(buf, tag, bufSiz);
  len = strlen(buf);
  // emit from 15 down to 1
  for (i=kRank_BandMax; i-- > 1 && (len+20<bufSiz); ) {
	snprintf(buf+len, bufSiz-len, " x%X='%d'", i, band[i]);
	len += strlen(buf+len);
  }
}

int RankTuneXml(PXml px, DomId domid)
{
  int err, i;
  char msgBuf[1024];
  RankTune rt = RankTuneToUse(domid);

  snprintf(msgBuf, sizeof(msgBuf), "ranktune domid='%d' defSort='%d' defMax='%d' defDist='%d'",
		   rt->domId, rt->defaultSort, rt->defaultMax, rt->defaultDist);

  if ((err = PXmlPush(px, msgBuf, 0)))
	return err;
  
  rankTuneBandToBuf(msgBuf, sizeof(msgBuf), "act", rt->act.bands);
  err |= PXmlPush0(px, msgBuf, 0);

  rankTuneBandToBuf(msgBuf, sizeof(msgBuf), "new", rt->newreg.bands);
  err |= PXmlPush0(px, msgBuf, 0);

  rankTuneBandToBuf(msgBuf, sizeof(msgBuf), "pop", rt->pop.bands);
  err |= PXmlPush0(px, msgBuf, 0);

  rankTuneBandToBuf(msgBuf, sizeof(msgBuf), "ldg", rt->ldg.bands);
  err |= PXmlPush0(px, msgBuf, 0);

  snprintf(msgBuf, sizeof(msgBuf), "cscz reg1='%d' reg2='%d' reg3='%d' reg4='%d'",
		   rt->cscz.inCountry, rt->cscz.inState, rt->cscz.inCity, rt->cscz.inZip);
  err |= PXmlPush0(px, msgBuf, 0);

  snprintf(msgBuf, sizeof(msgBuf), "age algo='%d' inmax='%d' inmin='%d' outmax='%d'",
		   rt->age.algoSelect, rt->age.inMax, rt->age.inMin, rt->age.outMax);
  err |= PXmlPush(px, msgBuf, 0);
  for (i=0; i < kRank_AgeBandMax && rt->age.pctDif[i]; i++) {
	snprintf(msgBuf, sizeof(msgBuf), "agecurve pctdif='%d' peryear='%d'",
			 rt->age.pctDif[i], rt->age.perYear[i]);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  err |= PXmlPop(px);

  snprintf(msgBuf, sizeof(msgBuf), "hgt inmax='%d' inmin='%d' outmax='%d' percm='%d'",
		   rt->hgt.inMax, rt->hgt.inMin, rt->hgt.outMax, rt->hgt.perCm);
  err |= PXmlPush0(px, msgBuf, 0);
  
  err |= PXmlPush(px, "blends", 0);
  for (i=1; i<kS1Sort_Max; i++) {
	S1ShowScores(msgBuf, sizeof(msgBuf), i, &(rt->bySort[i]));
	err |= PXmlPush0(px, msgBuf, 0);
  }
  err |= PXmlPop(px);

  err |= PXmlPop(px);
  return err;
}

