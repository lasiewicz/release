// memxml.c
// pnelson 12/4/2005
// Export results in XML

#define MEM_FRIEND
#include "peng.h"

#define PKGNAME PkgName_Mem

/// <summary>Serialize a Mem object into XML</summary>
/// <param name="xmem">Mem object</param>
/// <param name="px">XML string buffer to extend</param>
/// <param name="mode">Controls what to show
///    mode=0 == Just show Member object reference
///    mode&1 == show basic details (cscz, dates, vitals)
///    mode&2 == show the tags
///    mode&4 == show the LDG
/// </param>
///
int MemXml(PXml px, Mem xmem, int mode)
{
  LocRec lp;
  char msgBuf[156];
  int err;
  S4  seq = MemSequence(xmem);
  
  // Get the precise location
  MemPackMemToLoc(xmem, &lp, 1);

  snprintf(msgBuf, sizeof(msgBuf), "mem seq='%ld' memid='%ld' domid='%d' gender='%d' seekingsame='%d' plat='%d' plng='%d'",
		   seq, xMemMemId(xmem), xMemDomId(xmem), xMemGender(xmem), xmem->vitals.sameSex, lp.lat, lp.lng);

  if (mode == 0)
	return PXmlPush0(px, msgBuf, 0);

  if ((err = PXmlPush(px, msgBuf, 0)) < 0)
	return err;

  if (mode & 1) {
	cSTR s1, s2, s3, s4;
	char buf1[32], buf2[32], buf3[32];

	snprintf(msgBuf, sizeof(msgBuf), "cscz country='%s' state='%s' city='%s' zip='%s' areacode='%0.3d'",
			 ((s1 = StxDbById(gsv->pkCountry, xmem->cscz.pkCountry)) ? s1 : ""),
			 ((s2 = StxDbById(gsv->pkState,   xmem->cscz.pkState  )) ? s2 : ""),
			 ((s3 = StxDbById(gsv->pkCity,    xmem->cscz.pkCity   )) ? s3 : ""),
			 ((s4 = StxDbById(gsv->pkZip,     xmem->cscz.pkZip    )) ? s4 : ""),
			 xmem->cscz.pkArea);
	err |= PXmlPush0(px, msgBuf, 0);

	snprintf(msgBuf, sizeof(msgBuf), "dates %s %s %s",
			 MemPackDateStr("birthdate", xmem->birthPack.pack, buf1, 1),
			 MemPackDateStr("registerdate", xmem->registerPack.pack, buf2, 1),
			 MemPackDateTimeStr("activedate", xmem->activePack.pack, buf3, 1));
	err |= PXmlPush0(px, msgBuf, 0);
	
	snprintf(msgBuf, sizeof(msgBuf), "vitals hgt='%d' wgt='%d' hasphoto='%d' isonline='%d' childcnt='%d' saAct='%d' saNew='%d' saPop='%d' moreChildren='%d' colorcode='%d'",
			 (xmem->vitals.height ? (int)xmem->vitals.height + kMemPack_HeightMin : 0),
			 (xmem->vitals.weight ? (int)xmem->vitals.weight + kMemPack_WeightMin : 0),
			 xmem->vitals.hasPhoto, xmem->vitals.isOnline, xmem->vitals.childCnt,
			 xmem->vitals.saAct, xmem->vitals.saNew, xmem->vitals.saPop,
			 xmem->moreChildrenFlag, xmem->colorcode);
	err |= PXmlPush0(px, msgBuf, 0);
  }

  if (!err && xmem->grp && (mode & 2))
	err |= GrpXml(px, xmem->grp, -1);
  
  if (!err && xmem->ldg && (mode & 4))
	err |= LdgXml(px, xmem->ldg, 0);

  err |= PXmlPop(px);
  return err;
}

int MemXmlGrpChain(PXml px, Mem xmem, int mode)
{
  int i, err = 0;
  int maxChain = PXmlEnumMax(px);
  Mem scan; char buf[32];

  for (i=0, scan=xmem; scan; i++, scan=scan->grpChain) ;

  snprintf(buf, sizeof(buf), "GrpChain count='%d'", i);
  err |= PXmlPush(px, buf, 0);
  for (i=0; !err && xmem && i<maxChain; i++, xmem=xmem->grpChain) {
	S4 seq = MemSequence(xmem);
	err |= PXmlPush0(px, "Mem", seq);
  }
  err |= PXmlPop(px);
  return err;
}

int MemXmlLdgChain(PXml px, Mem xmem, int mode)
{
  int i, err = 0;
  int maxChain = PXmlEnumMax(px);
  Mem scan; char buf[32];
  
  for (i=0, scan=xmem; scan; i++, scan=scan->ldgChain) ;
  
  snprintf(buf, sizeof(buf), "LdgChain count='%d'", i);
  err |= PXmlPush(px, buf, 0);
  for (i=0; !err && xmem && i<maxChain; i++, xmem=xmem->ldgChain) {
	S4 seq = MemSequence(xmem);
	err |= PXmlPush0(px, "Mem", seq);
  }
  err |= PXmlPop(px);
  
  return err;
}
