<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<!-- XSL Transformation to map startup data into URL Commands the engine can understand
 *
 * History:
 *   v1.0 |  12/5/05 | Philip Nelson
 *   v1.1 |  2/20/06 | PNelson | Added support for Ranktune
 *
-->

<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" >

	<xsl:output method="text" indent="no" encoding="utf-8" />

<!-- The outer structure of the page -->
<xsl:template match="/e1Boot">
 <xsl:text>time</xsl:text>
 <xsl:text>&#10;</xsl:text>
 <xsl:apply-templates select="preLoad/load" />
 <xsl:apply-templates select="ranktunes/ranktune" />
</xsl:template>

<xsl:template match="load">
	<xsl:text>load?header=</xsl:text>
	<xsl:value-of select="@header"/>
  <xsl:text>&amp;file=</xsl:text>
  <xsl:value-of select="@file"/>
  <xsl:text>&amp;mode=7</xsl:text>
	<xsl:text>&#10;</xsl:text>
	<xsl:text>time</xsl:text>
	<xsl:text>&#10;</xsl:text>
</xsl:template>


  <xsl:template match="ranktune">
    <!-- Define $cmd as the prefix for all commands -->
    <xsl:variable name="cmd">
      <xsl:text>ranktune?domid=</xsl:text>
      <xsl:value-of select="@domid"/>
    </xsl:variable>
    <xsl:text>&#10;</xsl:text>

    <!-- Send in the basic attributes -->
    <xsl:value-of select="$cmd"/>
    <xsl:if test="boolean(@defSort)">
      <xsl:text>&amp;def.sort=</xsl:text><xsl:value-of select="@defSort" />
    </xsl:if>
    <xsl:if test="boolean(@defMax)">
      <xsl:text>&amp;def.max=</xsl:text><xsl:value-of select="@defMax" />
    </xsl:if>
    <xsl:if test="boolean(@defDist)">
      <xsl:text>&amp;def.dist=</xsl:text><xsl:value-of select="@defDist" />
    </xsl:if>
    <xsl:apply-templates select="cscz"/>
    <xsl:apply-templates select="age"/>
    <xsl:apply-templates select="hgt"/>
    <xsl:text>&#10;</xsl:text>

    <!-- Send in the bands where present -->
    <xsl:if test="act">
      <xsl:call-template name="band">
        <xsl:with-param name="cmd" select="$cmd" />
        <xsl:with-param name="node" select="act" />
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="new">
      <xsl:call-template name="band">
        <xsl:with-param name="cmd" select="$cmd" />
        <xsl:with-param name="node" select="new" />
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="pop">
      <xsl:call-template name="band">
        <xsl:with-param name="cmd" select="$cmd" />
        <xsl:with-param name="node" select="pop" />
      </xsl:call-template>
    </xsl:if>
    <xsl:if test="ldg">
      <xsl:call-template name="band">
        <xsl:with-param name="cmd" select="$cmd" />
        <xsl:with-param name="node" select="ldg" />
      </xsl:call-template>
    </xsl:if>

    <!-- Now do all the sort blends -->
    <xsl:apply-templates select="blends/blend">
      <xsl:with-param name="cmd" select="$cmd" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template name="band">
    <xsl:param name="cmd" />
    <xsl:param name="node" />
    <xsl:variable name="key" select="concat('&amp;rt.', name($node), '=')" />
    <xsl:value-of select="$cmd"/>
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@xF" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@xE" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@xD" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@xC" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@xB" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@xA" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x9" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x8" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x7" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x6" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x5" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x4" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x3" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x2" />
    <xsl:value-of select="$key" />    <xsl:value-of select="$node/@x1" />
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="cscz">
    <xsl:text>&amp;cscz.reg1=</xsl:text>    <xsl:value-of select="@reg1" />
    <xsl:text>&amp;cscz.reg2=</xsl:text>    <xsl:value-of select="@reg2" />
    <xsl:text>&amp;cscz.reg3=</xsl:text>    <xsl:value-of select="@reg3" />
    <xsl:text>&amp;cscz.reg4=</xsl:text>    <xsl:value-of select="@reg4" />
  </xsl:template>

  <xsl:template match="hgt">
    <xsl:text>&amp;hgt.inmax=</xsl:text>    <xsl:value-of select="@inmax" />
    <xsl:text>&amp;hgt.inmin=</xsl:text>    <xsl:value-of select="@inmin" />
    <xsl:text>&amp;hgt.outmax=</xsl:text>   <xsl:value-of select="@outmax" />
    <xsl:text>&amp;hgt.percm=</xsl:text>    <xsl:value-of select="@percm" />
  </xsl:template>

  <xsl:template match="age">
    <xsl:text>&amp;age.inmax=</xsl:text>    <xsl:value-of select="@inmax" />
    <xsl:text>&amp;age.inmin=</xsl:text>    <xsl:value-of select="@inmin" />
    <xsl:text>&amp;age.outmax=</xsl:text>   <xsl:value-of select="@outmax" />
    <xsl:for-each select="agecurve">
      <xsl:text>&amp;age.pct=</xsl:text>    <xsl:value-of select="@pctdif" />
      <xsl:text>&amp;age.peryr=</xsl:text>  <xsl:value-of select="@peryear" />
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="blend">
    <xsl:param name="cmd"/>
    <xsl:value-of select="$cmd"/>
    <xsl:text>&amp;rtb.sort=</xsl:text>    <xsl:value-of select="@sort" />
    <xsl:text>&amp;rtb.cscz=</xsl:text>    <xsl:value-of select="@cscz"/>
    <xsl:text>&amp;rtb.ldg=</xsl:text>     <xsl:value-of select="@ldg"/>
    <xsl:text>&amp;rtb.age=</xsl:text>     <xsl:value-of select="@age"/>
    <xsl:text>&amp;rtb.hgt=</xsl:text>     <xsl:value-of select="@hgt"/>
    <xsl:text>&amp;rtb.match=</xsl:text>   <xsl:value-of select="@match"/>
    <xsl:text>&amp;rtb.act=</xsl:text>     <xsl:value-of select="@act"/>
    <xsl:text>&amp;rtb.new=</xsl:text>     <xsl:value-of select="@new"/>
    <xsl:text>&amp;rtb.pop=</xsl:text>     <xsl:value-of select="@pop"/>
    <xsl:text>&amp;rtb.mol=</xsl:text>     <xsl:value-of select="@mol"/>
    <xsl:text>&#10;</xsl:text>
  </xsl:template>

</xsl:stylesheet>
