<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="no" method="html"/>

<xsl:template match="/e1">
	<html>
	<head>
	<style type="text/css">
		table { border:1 solid; width:700px; text-align: left; }
		th { font-size: 11px; text-color: gray; }
		.inpscore { font-size: 10px; text-color: gray; width: 30px; background-color: #ffece9; }
		.inpband  { font-size: 10px; text-color: gray; width: 30px; background-color: #ffece9; }
		.inpsort    { font-size: 10px; text-color: gray; width: 30px; background-color: #ffece9; }
		.inpvalue  { font-size: 10px; text-color: gray; width: 30px; background-color: #ffece9; }
		.inppair { padding: 0px 5px 0px 12px;  }
		.submit  { text-align: center; background-color: pink; }
	</style>
	<script>
function rtvalidate(frm)
{
	alert("Validate in Script Here");
}
function rtsubmit(frm)
{
  var node = document.all["rtserver"];
  var form = document.all[frm];
  if (!form) {
    alert("No Form named: " + frm);
  } else {
    if (node.value) {
      form.action = node.value;
      alert("Setting Action: " + node.value);
    }
    //rtvalidate(frm);
    form.submit();
  }
}
	</script>
	<title>RankTune</title>
	</head>    
	<body>
		<h1>Configure RankTune</h1>
    Server: <input id="rtserver" type="text" style="width: 350px;" />
		<xsl:apply-templates select="ranktune"/>
	</body>
	</html>
</xsl:template>

<xsl:template match="ranktune">
  <h1>Ranktune for Domid: <xsl:value-of select="@domid"/></h1>
  <input type="button" value="Validate" onclick="rtvalidate('form{@domid}')" />&nbsp;
  <input type="button" value="Submit" onclick="rtsubmit('form{@domid}')" />&nbsp;
  <form id="form{@domid}" name="form{@domid}" method="get" action="ranktune">
    <input type="hidden" name="xsl" value="/e1Ranktune.xsl" />
    <table border="1">
      <tr>
        <td>RankTune</td>
        <td>
          <span class="inppair">
            Domid: <input name="domid" class="inpvalue" type="text" value="{@domid}" />
          </span>
          <span class="inppair">
            DefSort: <input name="def.sort" class="inpvalue" type="text" value="{@defSort}" />
          </span>
          <span class="inppair">
            DefMax: <input name="def.max" class="inpvalue" type="text" value="{@defMax}" />
          </span>
          <span class="inppair">
            DefDist: <input name="def.dist" class="inpvalue" type="text" value="{@defDist}" />
          </span>
        </td>
      </tr>
      <tr>
        <td>Bands</td>
        <td>
          <table>
            <xsl:call-template name="bandtitle"/>
            <xsl:apply-templates select="act"/>
            <xsl:apply-templates select="new"/>
            <xsl:apply-templates select="pop"/>
            <xsl:apply-templates select="ldg"/>
          </table>
        </td>
      </tr>
      <tr>
        <td>Age</td>
        <td>
          <xsl:apply-templates select="age"/>
        </td>
      </tr>
      <tr>
        <td>Cscz</td>
        <td>
          <xsl:apply-templates select="cscz"/>
        </td>
      </tr>
      <tr>
        <td>Hgt</td>
        <td>
          <xsl:apply-templates select="hgt"/>
        </td>
      </tr>
      <xsl:apply-templates select="blends"/>
    </table>
  </form>
  <hr/>
</xsl:template>

<xsl:template match="act"><xsl:call-template name="banddata" /></xsl:template>
<xsl:template match="new"><xsl:call-template name="banddata" /></xsl:template>
<xsl:template match="pop"><xsl:call-template name="banddata" /></xsl:template>
<xsl:template match="ldg"><xsl:call-template name="banddata" /></xsl:template>

<xsl:template match="blends">
	<tr><td>Blends</td><td><table>
		<xsl:call-template name="blendtitle" />
		<xsl:call-template name="blenddata" >
			<xsl:with-param name="key"  select="'New'" />
			<xsl:with-param name="node" select="blend[@sort=1]" />
		</xsl:call-template>
		<xsl:call-template name="blenddata" >
			<xsl:with-param name="key"  select="'Active'" />
			<xsl:with-param name="node" select="blend[@sort=2]" />
		</xsl:call-template>
		<xsl:call-template name="blenddata" >
			<xsl:with-param name="key"  select="'Near'" />
			<xsl:with-param name="node" select="blend[@sort=3]" />
		</xsl:call-template>
		<xsl:call-template name="blenddata" >
			<xsl:with-param name="key"  select="'Popular'" />
			<xsl:with-param name="node" select="blend[@sort=4]" />
		</xsl:call-template>
		<xsl:call-template name="blenddata" >
			<xsl:with-param name="key"  select="'MOL'" />
			<xsl:with-param name="node" select="blend[@sort=5]" />
		</xsl:call-template>
		</table></td></tr>
</xsl:template>

<xsl:template match="cscz">
	<span class="inppair">Reg1: <input name="cscz.reg1" class="inpvalue" type="text" value="{@reg1}" /></span>
	<span class="inppair">Reg2: <input name="cscz.reg2" class="inpvalue" type="text" value="{@reg2}" /></span>
	<span class="inppair">Reg3: <input name="cscz.reg3" class="inpvalue" type="text" value="{@reg3}" /></span>
	<span class="inppair">Reg4: <input name="cscz.reg4" class="inpvalue" type="text" value="{@reg4}" /></span>
</xsl:template>

<xsl:template match="age">
<table>
<tr>
	<th>Algo</th>
	<th>InMax</th>
	<th>InMin</th>
	<th>OutMax</th>
	<xsl:for-each select="agecurve">
		<th> </th>
		<th>%Dif</th>
		<th>PerYr</th>
	</xsl:for-each>
</tr>
<tr>
	<td><input name="age.algo" class="inpvalue" type="text" value="{@algo}" /> </td>
	<td><input name="age.inmax" class="inpvalue" type="text" value="{@inmax}" /> </td>
	<td><input name="age.inmin" class="inpvalue" type="text" value="{@inmin}" /> </td>
	<td><input name="age.outmax" class="inpvalue" type="text" value="{@outmax}" /> </td>
	<xsl:for-each select="agecurve">
		<td>||</td>
		<td><input name="age.pct"   class="inpvalue" type="text" value="{@pctdif}" /> </td>
		<td><input name="age.peryr" class="inpvalue" type="text" value="{@peryear}" /> </td>
	</xsl:for-each>
</tr>
</table>
</xsl:template>

<xsl:template match="hgt">
	<span class="inppair">InMax:  <input name="hgt.inmax" class="inpvalue" type="text" value="{@inmax}" /> </span>
	<span class="inppair">InMin:  <input name="hgt.inmin" class="inpvalue" type="text" value="{@inmin}" /> </span>
	<span class="inppair">OutMax: <input name="hgt.outmax" class="inpvalue" type="text" value="{@outmax}" /> </span>
	<span class="inppair">PerCm:  <input name="hgt.percm" class="inpvalue" type="text" value="{@percm}" /> </span>
</xsl:template>

<xsl:template name="sort">
	<xsl:param name="key" />
	<td>SortKey: </td>
	<td>
		<input name="sortkey" type="radio" class="inpsort" value="1">
			<xsl:if test="$key = 1">
				<xsl:attribute name="checked">1</xsl:attribute>
			</xsl:if>
		</input> Active
		<input name="sortkey" type="radio" class="inpsort" value="2">
			<xsl:if test="$key = 2">
				<xsl:attribute name="checked">1</xsl:attribute>
			</xsl:if>
		</input> Near
		<input name="sortkey" type="radio" class="inpsort" value="3">
			<xsl:if test="$key = 3">
				<xsl:attribute name="checked">1</xsl:attribute>
			</xsl:if>
		</input> New
		<input name="sortkey" type="radio" class="inpsort" value="4">
			<xsl:if test="$key = 4">
				<xsl:attribute name="checked">1</xsl:attribute>
			</xsl:if>
		</input> Popular
		<input name="sortkey" type="radio" class="inpsort" value="5">
			<xsl:if test="$key = 4">
				<xsl:attribute name="checked">1</xsl:attribute>
			</xsl:if>
		</input> MOL
	</td>
</xsl:template>

<xsl:template name="bandtitle">
<tr>
	<th>Name</th>
	<th>xF</th>
	<th>xE</th>
	<th>xD</th>
	<th>xC</th>
	<th>xB</th>
	<th>xA</th>
	<th>x9</th>
	<th>x8</th>
	<th>x7</th>
	<th>x6</th>
	<th>x5</th>
	<th>x4</th>
	<th>x3</th>
	<th>x2</th>
	<th>x1</th>
</tr>
</xsl:template>

<xsl:template name="banddata">
	<xsl:param name="key" select="name(.)"/>
	<tr>
	<td><xsl:value-of select="$key"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@xF}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@xE}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@xD}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@xC}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@xB}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@xA}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x9}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x8}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x7}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x6}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x5}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x4}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x3}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x2}"/></td>
	<td><input name="rt.{$key}" type="text" class="inpband" value="{@x1}"/></td>
</tr>
</xsl:template>

<xsl:template name="blendtitle">
	<tr>
		<th>Name</th>
		<th>cscz</th>
		<th>ldg</th>
		<th>age</th>
		<th>hgt</th>
		<th>match</th>
		<th>act</th>
		<th>new</th>
		<th>pop</th>
		<th>mol</th>
	</tr>
</xsl:template>

<xsl:template name="blenddata">
	<xsl:param name="key" />
	<xsl:param name="node" />
	<tr id="blend_{$key}">
	<td><xsl:value-of select="$key"/><input type="hidden" name="rtb.sort" value="{$node/@sort}"/> </td>
	<td><input name="rtb.cscz" type="text" class="inpscore" value="{$node/@cscz}"/></td>
	<td><input name="rtb.ldg" type="text" class="inpscore" value="{$node/@ldg}"/></td>
	<td><input name="rtb.age" type="text" class="inpscore" value="{$node/@age}"/></td>
	<td><input name="rtb.hgt" type="text" class="inpscore" value="{$node/@hgt}"/></td>
	<td><input name="rtb.match" type="text" class="inpscore" value="{$node/@match}"/></td>
	<td><input name="rtb.act" type="text" class="inpscore" value="{$node/@act}"/></td>
	<td><input name="rtb.new" type="text" class="inpscore" value="{$node/@new}"/></td>
	<td><input name="rtb.pop" type="text" class="inpscore" value="{$node/@pop}"/></td>
	<td><input name="rtb.mol" type="text" class="inpscore" value="{$node/@mol}"/></td>
	</tr>
</xsl:template>

</xsl:stylesheet>
