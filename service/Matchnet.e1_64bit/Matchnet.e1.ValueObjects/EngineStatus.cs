using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.e1.ValueObjects
{
    [Serializable]
    public class EngineStatus
    {
        private Int32 _partitionsBulkLoaded;
        private Int32 _partitionsTotal;

        public EngineStatus(Int32 partitionsBulkLoaded, Int32 partitionsTotal)
        {
            _partitionsBulkLoaded = partitionsBulkLoaded;
            _partitionsTotal = partitionsTotal;
        }

        public Int32 PartitionsBulkLoaded
        {
            get
            {
                return _partitionsBulkLoaded;
            }
        }

        public Int32 PartitionsTotal
        {
            get
            {
                return _partitionsTotal;
            }
        }
    }
}
