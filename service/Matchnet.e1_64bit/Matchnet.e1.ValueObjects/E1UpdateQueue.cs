using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

using Matchnet.Member.ValueObjects;

namespace Matchnet.e1.ValueObjects
{
    /// <summary>
    /// Updates e1 using a single thread.
    /// </summary>
    public class E1UpdateQueue
    {
        private const Int32 DELAY_BETWEEN_CHECKS = 3; //How long to wait before re-checking the queue for items if empty (in ms)
        private const Int32 DELAY_BETWEEN_UPDATES = 3; //How long to wait between dequeueing items (in ms)

        private string _queueName; //optional -- for identifying an individual queue among multiple queues sharing an event handler
        private Int32 _queueSize; //maximum number of items that can be enqueued
        private string _serviceName; //for error logging

        private bool _isRunning = false;

        private Queue<E1Update> _loaderQueue;
        private ReaderWriterLock _loaderQueueLock = new ReaderWriterLock();
        private Int32 _lockTimeout = 1000;

        #region Events
        public delegate void MemberEnQueuedEventHandler(E1UpdateQueueEventArgs args);
        public event MemberEnQueuedEventHandler MemberEnqueued;

        public delegate void MemberDeQueuedEventHandler(E1UpdateQueueEventArgs args);
        public event MemberDeQueuedEventHandler MemberDequeued;
        #endregion

        public E1UpdateQueue(Int32 queueSize, string serviceName)
        {
            _queueName = null;
            _queueSize = queueSize;
            _serviceName = serviceName;

            _loaderQueue = new Queue<E1Update>(_queueSize);
        }

        public E1UpdateQueue(string queueName, Int32 queueSize, string serviceName)
        {
            _queueName = queueName;
            _queueSize = queueSize;
            _serviceName = serviceName;

            _loaderQueue = new Queue<E1Update>(_queueSize);
        }

        public void Start()
        {
            lock (this)
            {
                if (!_isRunning)
                {
                    _isRunning = true;
                    new Thread(new ThreadStart(workCycle)).Start();
                }
            }
        }

        public void Stop()
        {
            lock (this)
            {
                _isRunning = false;
            }
        }

        public bool IsFull
        {
            get
            {
                _loaderQueueLock.AcquireReaderLock(_lockTimeout);

                try
                {
                    return _loaderQueue.Count >= _queueSize;
                }
                finally
                {
                    _loaderQueueLock.ReleaseLock();
                }
            }
        }

        public Int32 Count
        {
            get
            {
                return _loaderQueue.Count;
            }
        }

        /// <summary>
        /// Enqueues a MemberSpec, throwing an exception if the queue is full.
        /// </summary>
        /// <param name="e1Update"></param>
        public void Enqueue(E1Update e1Update)
        {
            _loaderQueueLock.AcquireWriterLock(_lockTimeout);
            _loaderQueue.Enqueue(e1Update);
            _loaderQueueLock.ReleaseLock();

            if (MemberEnqueued != null)
            {
                E1UpdateQueueEventArgs args = new E1UpdateQueueEventArgs(_queueName, e1Update);
                MemberEnqueued(args);
            }
        }

        /// <summary>
        /// Enqueues a MemberSpec, if necessary suspending execution until space is available in the queue.
        /// </summary>
        /// <param name="memberSpec"></param>
        public void WaitEnqueue(E1Update e1Update)
        {
            while (IsFull)
            {
                Thread.Sleep(1000);
            }

            Enqueue(e1Update);
        }

        private void workCycle()
        {
            while (_isRunning)
            {
                try
                {
                    //Wait until there's an item available to dequeue
                    while (_loaderQueue.Count == 0)
                    {
                        Thread.Sleep(DELAY_BETWEEN_CHECKS);
                    }

                    _loaderQueueLock.AcquireWriterLock(_lockTimeout);
                    E1Update e1Update = _loaderQueue.Dequeue();
                    _loaderQueueLock.ReleaseLock();

                    if (MemberDequeued != null)
                    {
                        MemberDequeued(new E1UpdateQueueEventArgs(_queueName, e1Update));
                    }

                    //Throttle
                    Thread.Sleep(DELAY_BETWEEN_UPDATES);
                }
                catch (Exception ex) //make sure the thread doesn't die
                {
                    Trace.WriteLine(ex.ToString());
                    //System.Diagnostics.EventLog.WriteEntry(_serviceName, "Error dequeueing member: " + ex.ToString(), EventLogEntryType.Error);
                }
            }
        }
    }

    public class E1UpdateQueueEventArgs
    {
        private string _queueName;
        private E1Update _e1Update;

        public E1UpdateQueueEventArgs(string queueName, E1Update e1Update)
        {
            _queueName = queueName;
            _e1Update = e1Update;
        }

        public string QueueName
        {
            get
            {
                return _queueName;
            }
            set
            {
                _queueName = value;
            }
        }

        public E1Update E1Update
        {
            get
            {
                return _e1Update;
            }
            set
            {
                _e1Update = value;
            }
        }
    }
}
