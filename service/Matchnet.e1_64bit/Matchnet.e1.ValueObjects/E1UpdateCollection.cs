using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.e1.ValueObjects
{
    [Serializable]
    public class E1UpdateCollection : IEnumerable<E1Update>
    {
        private List<E1Update> _e1Updates;

        public E1UpdateCollection(Int32 size)
        {
            _e1Updates = new List<E1Update>(size);
        }

        public void Add(E1Update e1Update)
        {
            _e1Updates.Add(e1Update);
        }

        public Int32 Count
        {
            get
            {
                return _e1Updates.Count;
            }
        }

        public void Clear()
        {
            _e1Updates.Clear();
        }

        #region IEnumerable<E1Update> Members

        public IEnumerator<E1Update> GetEnumerator()
        {
            return _e1Updates.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _e1Updates.GetEnumerator();
        }

        #endregion
    }
}
