using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.e1.ValueObjects;
using Matchnet.Search.Interfaces;

namespace Matchnet.e1.ServiceAdapters
{
    public class EngineSA : SABase
    {
        private const string SERVICE_MANAGER_NAME = "EngineSM";
        public static EngineSA Instance = new EngineSA();
        string testURL = "";
        private EngineSA()
        {
        }

        public CommandResult EvaluateCommand(String command, string overrideHostName)
        {
            String uri = String.Empty;

            try
            {
                if (overrideHostName == string.Empty)
                {
                    uri = getServiceManagerUri();
                }
                else
                {
                    uri = getServiceManagerUri(overrideHostName);
                }

                base.Checkout(uri);
                try
                {
                    return getService(uri).EvaluateCommand(command);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot evaluate command (uri: " + uri + ")", ex));
            }
        }

        public CommandResult EvaluateCommand(String command)
        {
            return EvaluateCommand(command, string.Empty);
        }

        public IMatchnetQueryResults RunQuery(IMatchnetQuery query)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri).RunQuery(query);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot perform query (uri: " + uri + ")", ex));
            }
        }

        public ArrayList RunDetailedQuery(IMatchnetQuery query)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri).RunDetailedQuery(query);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot perform query (uri: " + uri + ")", ex));
            }
        }

        public void LoadMember(E1Update e1Update)
        {
            E1UpdateCollection e1UpdateCollection = new E1UpdateCollection(1);
            e1UpdateCollection.Add(e1Update);
            LoadMembers(e1UpdateCollection);
        }

        public void LoadMembers(E1UpdateCollection e1UpdateCollection)
        {
            LoadMembers(e1UpdateCollection, string.Empty);
        }

        public void LoadMembers(E1UpdateCollection e1UpdateCollection, string overrideHostName)
        {
            String uri = String.Empty;

            try
            {
                if (overrideHostName == string.Empty)
                {
                    uri = getServiceManagerUri();
                }
                else
                {
                    uri = getServiceManagerUri(overrideHostName);
                }

                base.Checkout(uri);
                try
                {
                    getService(uri).LoadMembers(e1UpdateCollection);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot load member (uri: " + uri + ")", ex));
            }
        }

        public EngineStatus GetEngineStatus(string overrideHostName)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri(overrideHostName);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetEngineStatus();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve engine status (uri: " + uri + ")", ex));
            }
        }

        public void ChangeMaintenance(string overrideHostName, bool enabled)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri(overrideHostName);
                base.Checkout(uri);
                try
                {
                    getService(uri).ChangeMaintenance(enabled);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve engine status (uri: " + uri + ")", ex));
            }
        }

        public string TestURL
        {
            get { return testURL; }
            set { testURL = value; }

        }
        private string getServiceManagerUri()
        {
            try
            {
                return getServiceManagerUri(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENGINESVC_SA_HOST_OVERRIDE"));
            }
            catch { }

            return getServiceManagerUri(string.Empty);
        }

        private string getServiceManagerUri(string overrideHostName)
        {
            try
            {
                if (!string.IsNullOrEmpty(testURL))
                {
                    return testURL;
                }
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private IEngineService getService(string uri)
        {
            try
            {
                return (IEngineService)Activator.GetObject(typeof(IEngineService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = 10;
        }
    }
}
