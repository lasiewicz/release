using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Threading;

using Matchnet.Data;
using Matchnet.e1.ValueObjects;
using Matchnet.Exceptions;

namespace Matchnet.e1.BusinessLogic
{
    public class BCPWriter
    {
        public delegate void BCPWrittenEventHandler(string fileName, bool success);
        public event BCPWrittenEventHandler BCPWritten;

        private const Int32 THREAD_COUNT = 4;
        private const string SEARCHSTORE_DB_NAME = "mnSearchStore";

        private Int32 _partitionCount;
        private string _dataPath;
        private Queue<Int32> _partitionsToBCP;

        public BCPWriter(Int32 partitionCount, string dataPath)
        {
            _partitionCount = partitionCount;
            _dataPath = dataPath;

            _partitionsToBCP = new Queue<int>(_partitionCount);

            for (Int32 i = 0; i < _partitionCount; i++)
            {
                _partitionsToBCP.Enqueue(i);
            }
        }

        public void Start()
        {
            //deleteOldFiles();

            for (Int32 i = 0; i < THREAD_COUNT; i++)
            {
                new Thread(new ThreadStart(bcpOutWorker)).Start();
            }
        }

        private void deleteOldFiles()
        {
            try
            {
                for (Int32 i = 0; i < _partitionCount; i++)
                {
                    System.IO.File.Delete(getFileName(i));
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error deleting old BCP files.  Bulk load will continue.", ex);
            }
        }

        private void bcpOutWorker()
        {
            while (_partitionsToBCP.Count > 0)
            {
                Int32 partition = Constants.NULL_INT;

                lock (_partitionsToBCP)
                {
                    if (_partitionsToBCP.Count > 0)
                    {
                        partition = _partitionsToBCP.Dequeue();
                    }
                }

                if (partition != Constants.NULL_INT)
                {
                    bcpOut(partition);
                }
            }
        }

        private void bcpOut(Int32 partition)
        {
            string fileName = getFileName(partition);
            bool success = false;

            try
            {
                System.Diagnostics.Trace.WriteLine("bcpOut " + partition);

                Command command = new Command(SEARCHSTORE_DB_NAME, "up_BCP_mnSearchStore_OUT_byPartition", 0);

                command.AddParameter("@Partition", SqlDbType.Int, ParameterDirection.Input, partition);
                command.AddParameter("@FilePath", SqlDbType.VarChar, ParameterDirection.Input, _dataPath + @"\");
                Client.Instance.ExecuteNonQuery(command);
                success = true;

                System.Diagnostics.Trace.WriteLine("bcpOut " + partition + " done");
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error performing bcp out on partition " + partition + ":" + Environment.NewLine + ex.ToString(), EventLogEntryType.Error);
            }

            BCPWritten(fileName, success);
        }

        private string getFileName(Int32 partition)
        {
            return _dataPath + @"\" + Convert.ToString(1000 + partition) + ".bcp";
        }
    }
}
