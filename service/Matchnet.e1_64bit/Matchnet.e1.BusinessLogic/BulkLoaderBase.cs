using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Matchnet.e1.ValueObjects;
using Matchnet.Exceptions;

namespace Matchnet.e1.BusinessLogic
{
    public abstract class BulkLoaderBase
    {
        #region Events
        public delegate void MemberLoadedEventHandler(Int32 count);
        public event MemberLoadedEventHandler MemberLoaded;

        public delegate void BulkLoadCompletedEventHandler();
        public event BulkLoadCompletedEventHandler BulkLoadCompleted;

        #endregion

        protected E1UpdateQueue _e1MemberQueue;

        public BulkLoaderBase(E1UpdateQueue e1MemberQueue)
        {
            _e1MemberQueue = e1MemberQueue;
        }

        public void Start()
        {
            new Thread(new ThreadStart(startBulkLoad)).Start();
        }

        private void startBulkLoad()
        {
            try
            {
                bulkLoad();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error performing bulk load.", ex);
            }
        }

        abstract protected void bulkLoad();
        abstract public Int32 PartitionsCompleted { get;}
        abstract public Int32 PartitionsTotal {get;}

        protected void onMemberLoaded(Int32 count)
        {
            if (MemberLoaded != null)
            {
                MemberLoaded(count);
            }
        }

        protected void onBulkLoadCompleted()
        {
            if (BulkLoadCompleted != null)
            {
                BulkLoadCompleted();
            }
        }
    }
}
