/*using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading;

using Matchnet.e1.ValueObjects;

namespace Matchnet.e1.BusinessLogic
{
    class BulkLoaderCSV : BulkLoaderBase
    {
        private string _fileName;

        public BulkLoaderCSV(string fileName, E1MemberQueue e1MemberQueue) : base(e1MemberQueue)
        {
            _fileName = fileName;
        }

        protected override void  bulkLoad()
        {
            StreamReader sr = new StreamReader(_fileName);

            string[] headers = sr.ReadLine().Split(',');
            Array columns = Enum.GetValues(typeof(SearchMemberColumn));
            Dictionary<SearchMemberColumn, Int32> ordinals = new Dictionary<SearchMemberColumn, int>(columns.Length);

            for (Int32 i = 0; i < headers.Length; i++)
            {
                try
                {
                    SearchMemberColumn column = (SearchMemberColumn)Enum.Parse(typeof(SearchMemberColumn), headers[i]);
                    ordinals[column] = i;
                }
                catch { }
            }

            foreach (SearchMemberColumn column in columns)
            {
                if (!ordinals.ContainsKey(column))
                {
                    ordinals[column] = 0;
                }
            }

            while (!sr.EndOfStream)
            {
                string[] row = sr.ReadLine().Split(',');
                E1Member e1Member = getE1MemberFromDataRow(row, ordinals);
                onMemberLoaded(1);
                _e1MemberQueue.WaitEnqueue(e1Member);
            }
        }

        private static E1Member getE1MemberFromDataRow(string[] row, Dictionary<SearchMemberColumn, Int32> ordinals)
        {
            E1Member e1Member = new E1Member();

            e1Member.MemberID = Conversion.CInt(row[ordinals[SearchMemberColumn.MemberID]]);
            e1Member.CommunityID = Conversion.CInt(row[ordinals[SearchMemberColumn.CommunityID]]);
            e1Member.GenderMask = Conversion.CInt(row[ordinals[SearchMemberColumn.GenderMask]]);
            e1Member.BirthDate = Conversion.CDateTime(row[ordinals[SearchMemberColumn.BirthDate]]);
            e1Member.RegisterDate = Conversion.CDateTime(row[ordinals[SearchMemberColumn.CommunityInsertDate]]);
            e1Member.ActiveDate = Conversion.CDateTime(row[ordinals[SearchMemberColumn.LastActiveDate]]);
            e1Member.HasPhoto = Conversion.CBool(row[ordinals[SearchMemberColumn.HasPhotoFlag]]);
            e1Member.Height = Conversion.CInt(row[ordinals[SearchMemberColumn.Height]]);
            //e1Member.Popularity = Conversion.CInt(row[ordinals[SearchMemberColumn.EmailCount]]);

            //TODO: These columns need to be added to SearchMember:
            //memberSpec.Weight
            //memberSpec.Activity
            //memberSpec.ChildrenCount
            //memberSpec.IsOnline
            //memberSpec.MemberLocation
            //memberSpec.RelocateFlag

            foreach (SearchMemberColumn column in Tags)
            {
                if (row[ordinals[column]] != null)
                {
                    e1Member.Tags.Add(column.ToString(), row[ordinals[column]]);
                }
            }

            return e1Member;
        }
    }
}
*/