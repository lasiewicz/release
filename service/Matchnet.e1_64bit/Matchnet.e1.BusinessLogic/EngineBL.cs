using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Timers;
using System.Xml;
using System.Xml.Xsl;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.e1.Engine.ManagedAdapter;
using Matchnet.e1.ValueObjects;
using Matchnet.e1Loader.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;

namespace Matchnet.e1.BusinessLogic
{
    public class EngineBL
    {
        #region Events
        public delegate void UpdateQueueChangedEventHandler(Int32 delta, SearchUpdateType updateType);
        public event UpdateQueueChangedEventHandler UpdateQueueChanged;

        public delegate void MemberUpdatedEventHandler(Int32 count, SearchUpdateType updateType);
        public event MemberUpdatedEventHandler MemberUpdated;

        public delegate void QueriedEventHandler(Int32 numScanned, Int32 numReturned, Int64 ticksElapsedQueryMarshal, Int64 ticksElapsedEngine, Int64 ticksElapsedResultMarshal, Int64 ticksElapsedTotal);
        public event QueriedEventHandler Queried;

        public delegate void MaintenanceEventHandler(Int32 numMembers, Int32 numSearchPermits);
        public event MaintenanceEventHandler Maintenance;
        #endregion

        #region Constants
        private const string TAGDB_XML_FILENAME = "e1tagdb.xml";
        private const string TAGDB_XSL_FILENAME = "e1tagdb.xsl";

        private const string BOOT_XML_FILENAME = "e1boot.xml";
        private const string BOOT_XSL_FILENAME = "e1boot.xsl";

        private const Int32 LOADER_QUEUE_SIZE = 10000;
        private const Int32 REGIONID_ISRAEL = 105;
        #endregion

        #region Private Members
        private GSV _gsv;
        private E1UpdateQueue _loadQueue;
        private BulkLoaderBase _bulkLoader;
        private System.Timers.Timer _maintenanceTimer;
        private bool _maintenanceEnabled = true;
        private Int32 _maintenanceInterval = 0;

        private System.Timers.Timer _statsTimer;
        private Int32 _statsInterval = 100;
        #endregion

        #region Constructor
        public static EngineBL Instance = new EngineBL();

        private EngineBL()
        {
            //Load tag DB and run commands in BOOT_XML_FILENAME above
            initializeEngine();

            //Initialize and start main loader queue -- this is a single-threaded queue that feeds directly into the engine
            _loadQueue = new E1UpdateQueue(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, LOADER_QUEUE_SIZE, string.Empty);
            _loadQueue.MemberEnqueued += new E1UpdateQueue.MemberEnQueuedEventHandler(_loadQueue_MemberEnqueued);
            _loadQueue.MemberDequeued += new E1UpdateQueue.MemberDeQueuedEventHandler(_loadQueue_MemberDequeued);
            _loadQueue.Start();

            //Initialize bulk loader
            _bulkLoader = new BulkLoaderBCP();
            _bulkLoader.MemberLoaded += new BulkLoaderBase.MemberLoadedEventHandler(bulkLoader_MemberLoaded);

            //Register this engine instance with the Loader service.  Loader will begin queueing real-time updates immediately.
            LoaderSA.Instance.AddServer(MachineName);
            LoaderSA.Instance.StopOutboundQueue(MachineName);  //Queue is stopped by default, but we explicitly stop it here in case the engine crashed last time it was running and didn't get a chance to send the stop command to the Loader.

            //Start bulk loader if not in dev mode
            // if (!Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE")))
            //{
            //Wait until bulk load completes to start real-time loader.  Don't want real-time data to be overwritten by older data from boot load.
            _bulkLoader.BulkLoadCompleted += new BulkLoaderBase.BulkLoadCompletedEventHandler(_bulkLoader_BulkLoadCompleted);
            _bulkLoader.Start();
            /*}
            else
            {
                //No bulk load for dev mode, so start real-time updates, maintenance, etc. right away.
                _bulkLoader_BulkLoadCompleted(); //
            }*/
        }
        #endregion

        #region Private Methods
        void _bulkLoader_BulkLoadCompleted()
        {
            updateMOLStatus();  //Get list of all MOL so engine has current MOL status
            startLoader(); //Start real-time updates
            setupTimers(); //Setup timer to perform engine maintenance at regular intervals
        }

        private void updateMOLStatus()
        {
            try
            {
                //Retrieve list of all MOL from MOL service
                Matchnet.MembersOnline.ValueObjects.MOLCollection mol = MembersOnlineSA.Instance.GetMembersOnline();

                if (mol != null)
                {
                    //Create a collection of E1Updates to tell the engine which members are online
                    E1UpdateCollection updates = new E1UpdateCollection(mol.GetTotalCount());

                    foreach (DictionaryEntry entry in mol)
                    {
                        Int32 communityID = (Int32)entry.Key;
                        Matchnet.MembersOnline.ValueObjects.CommunityMOLCollection memberIDs = (Matchnet.MembersOnline.ValueObjects.CommunityMOLCollection)entry.Value;

                        if (memberIDs != null)
                        {
                            foreach (Int32 memberID in memberIDs)
                            {
                                updates.Add(new E1Update(null, new SearchUpdate(memberID, communityID, true)));
                            }
                        }
                    }

                    //Feed the MOL status updates into the engine
                    LoadMembers(updates);
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting MOL status.  Error: " + ex.ToString());
            }
        }

        /// <summary>
        /// Initialize global variables, load tag db commands, setup maintenance call
        /// </summary>
        private void initializeEngine()
        {
            _gsv = new GSV();

            new Thread(new ThreadStart(runStartupCommands)).Start();
        }

        #region Loader Setup
        private void startLoader()
        {
            new Thread(new ThreadStart(startLoaderWorker)).Start();
        }

        private void startLoaderWorker()
        {
            bool success = false;

            while (!success)
            {
                try
                {
                    LoaderSA.Instance.StartOutboundQueue(MachineName);
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Failed to connect with loader.  Engine will continue without real-time updates.  Error: " + ex.ToString(), EventLogEntryType.Error);
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                }
            }
            EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Successfully connected with loader.  Engine is now receiving real-time updates.", EventLogEntryType.Information);
        }

        public void DisconnectFromLoader()
        {
            try
            {
                LoaderSA.Instance.RemoveServer(MachineName);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure disconnecting from loader.  Service will continue to shutdown.", ex);
            }
        }
        #endregion

        private void runStartupCommands()
        {
            StringBuilder sbOutput = new StringBuilder();

            try
            {
                runCommands(TAGDB_XML_FILENAME, TAGDB_XSL_FILENAME, sbOutput);
                runCommands(BOOT_XML_FILENAME, BOOT_XSL_FILENAME, sbOutput);

                EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Engine initialized");
                // Comment here
                //.  Results of tagdb/boot commands: " + sbOutput.ToString(0, Math.Min(sbOutput.Length, 32000)), EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                // Comment here
                //EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Engine failed to initialize.  Error: " + ex.ToString().Substring(0, 200) + Environment.NewLine + "Results of tagdb/boot commands completed: " + sbOutput.ToString(0, Math.Min(sbOutput.Length, 32000)), EventLogEntryType.Error);
                EventLog.WriteEntry(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Engine failed to initialize.  Error: " + ex.ToString().Substring(0, Math.Min(ex.ToString().Length, 1000)), EventLogEntryType.Error);
            }
        }

        private void runCommands(string xmlFileName, string xslFileName, StringBuilder sbOutput)
        {
            string[] tagDBCommands = xslTransform(CommandPath + @"\" + xmlFileName, CommandPath + @"\" + xslFileName).Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string command in tagDBCommands)
            {
                string commandResult = EvaluateCommand(command).Xml;

                if (sbOutput != null)
                {
                    sbOutput.Append(commandResult + Environment.NewLine);
                }
            }
        }

        private void setupTimers()
        {
            try
            {
                _maintenanceInterval = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("E1SVC_MAINTENANCE_INTERVAL"));
            }
            catch (Exception ex)
            {
                //new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving maintenance interval setting.  Using default value of one minute.", ex);
                _maintenanceInterval = 60000;
            }

            _maintenanceTimer = new System.Timers.Timer(_maintenanceInterval);
            _maintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(runMaintenance);
            _maintenanceTimer.AutoReset = true;
            _maintenanceTimer.Start();

            _statsTimer = new System.Timers.Timer(_statsInterval);
            _statsTimer.Elapsed += new ElapsedEventHandler(runStats);
            _statsTimer.AutoReset = true;
            _statsTimer.Start();
        }

        private void runMaintenance(object sender, ElapsedEventArgs e)
        {
            if (_maintenanceEnabled)
            {
                GSVStatistics stats = null;

                try
                {
                    stats = Load.Maintain();

                    if (stats != null)
                    {
                        Maintenance(stats.NumMembers, stats.SearchPermits);
                    }
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error performing maintenance call.", ex);
                }
                finally
                {
                    if (stats != null)
                    {
                        stats.Dispose();
                    }
                }
            }
        }

        private void runStats(object sender, ElapsedEventArgs e)
        {
            GSVStatistics stats = null;

            try
            {
                stats = Load.GetStats();

                if (stats != null)
                {
                    Maintenance(stats.NumMembers, stats.SearchPermits);
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Error performing maintenance call.", ex);
            }
            finally
            {
                if (stats != null)
                {
                    stats.Dispose();
                }
            }
        }
        #endregion

        #region Public Methods

        public CommandResult EvaluateCommand(String command)
        {
            XML xml = null;

            try
            {
                xml = new XML();
                xml.Reset();
                command = System.Web.HttpUtility.UrlDecode(command);
                Int32 errorCode = xml.CommandEval(command);
                return new CommandResult(xml.CommandResult, errorCode);
            }
            finally
            {
                if (xml != null)
                {
                    xml.Dispose();
                }
            }
        }

        public IMatchnetQueryResults RunQuery(IMatchnetQuery query)
        {
            Query e1Query = null;
            Result result = null;
            MatchnetQueryResults queryResults = null;

            try
            {
                Int64 ticksElapsedQueryMarshal;
                Int64 ticksElapsedEngine;
                Int64 ticksElapsedResultMarshal;
                Int64 ticksElapsedTotal;

                ticksElapsedTotal = Stopwatch.GetTimestamp();

                //Marshal Query
                ticksElapsedQueryMarshal = Stopwatch.GetTimestamp();
                e1Query = makeE1Query(query);
                e1Query.MaxResults = 360;
                ticksElapsedQueryMarshal = Stopwatch.GetTimestamp() - ticksElapsedQueryMarshal;

                //Get results from engine
                ticksElapsedEngine = Stopwatch.GetTimestamp();
                result = Matchnet.e1.Engine.ManagedAdapter.Search.DoSearch(e1Query);
                ticksElapsedEngine = Stopwatch.GetTimestamp() - ticksElapsedEngine;

                //Marshal results
                ticksElapsedResultMarshal = Stopwatch.GetTimestamp();

                if (result == null)
                {
                    queryResults = new MatchnetQueryResults(0);
                }
                else
                {
                    queryResults = new MatchnetQueryResults(result.Count);
                    for (Int32 i = 0; i < result.Count; i++)
                    {
                        MatchnetResultItem item = new MatchnetResultItem(result.GetResult(i));
                        queryResults.AddResult(item);
                    }
                    queryResults.MatchesFound = result.Count;
                }
                ticksElapsedResultMarshal = Stopwatch.GetTimestamp() - ticksElapsedResultMarshal;

                ticksElapsedTotal = Stopwatch.GetTimestamp() - ticksElapsedTotal;

                if (Queried != null)
                {
                    Queried(((result == null) ? 0 : result.NumScanned), queryResults.MatchesReturned, ticksElapsedQueryMarshal, ticksElapsedEngine, ticksElapsedResultMarshal, ticksElapsedTotal);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to run query.  Query: " + e1Query.GetString() + Environment.NewLine + "Query trace: " + e1Query.GetDebugTrace(), ex);
            }
            finally
            {
                if (e1Query != null)
                {
                    e1Query.Dispose();
                }

                if (result != null)
                {
                    result.Dispose();
                }
            }

            return queryResults;
        }

        /// <summary>
        /// For debugging only
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public ArrayList RunDetailedQuery(IMatchnetQuery query)
        {
            Result result = null;
            Query e1Query = null;
            ArrayList detailedResults = new ArrayList();

            try
            {
                e1Query = makeE1Query(query);
                e1Query.MaxResults = 720;
                result = Matchnet.e1.Engine.ManagedAdapter.Search.DoSearch(e1Query);

                for (Int32 i = 0; i < result.Count; i++)
                {
                    result.GetResult(i);
                    MemberSpec memberSpec = result.GetMemSpec(i);
                    Scores scores = result.GetScores(i);

                    DetailedQueryResult detailedQueryResult = new DetailedQueryResult();
                    detailedQueryResult.MemberID = memberSpec.MemberID;
                    detailedQueryResult.CommunityID = memberSpec.CommunityID;
                    detailedQueryResult.GenderMask = memberSpec.GenderMask;
                    detailedQueryResult.ActiveDate = memberSpec.ActiveDate;
                    detailedQueryResult.RegisterDate = memberSpec.RegisterDate;
                    detailedQueryResult.HasPhoto = (memberSpec.HasPhoto == UpdateFlag.SetYes);
                    detailedQueryResult.IsOnline = (memberSpec.IsOnline == UpdateFlag.SetYes);
                    detailedQueryResult.ColorCode = memberSpec.ColorCode;

                    detailedQueryResult.ActivityScore = scores.Activity;
                    detailedQueryResult.AgeScore = scores.Age;
                    detailedQueryResult.CsczScore = scores.Proximity;
                    detailedQueryResult.HeightScore = scores.Height;
                    detailedQueryResult.LdgScore = scores.LDG;
                    detailedQueryResult.MatchScore = scores.Match;
                    detailedQueryResult.NewScore = scores.NewReg;
                    detailedQueryResult.PopularityScore = scores.Popularity;
                    detailedQueryResult.TotalScore = result.GetScore(i);

                    Hashtable tags = detailedQueryResult.Tags;
                    foreach (NameValue nv in memberSpec.Tags)
                    {
                        if (tags.ContainsKey(nv.Name))
                        {
                            tags[nv.Name] += "," + nv.Value;
                        }
                        else
                        {
                            tags.Add(nv.Name, nv.Value);
                        }
                    }

                    detailedResults.Add(detailedQueryResult);
                }
            }
            finally
            {
                if (e1Query != null)
                {
                    e1Query.Dispose();
                }
                if (result != null)
                {
                    result.Dispose();
                }
            }

            return detailedResults;
        }

        public void LoadMembers(E1UpdateCollection e1UpdateCollection)
        {
            foreach (E1Update e1Update in e1UpdateCollection)
            {
                _loadQueue.WaitEnqueue(e1Update);
            }
        }

        public EngineStatus GetEngineStatus()
        {
            return new EngineStatus(_bulkLoader.PartitionsCompleted, _bulkLoader.PartitionsTotal);
        }

        public void ChangeMaintenance(bool enabled)
        {
            _maintenanceEnabled = enabled;
        }

        #endregion

        #region Event Handlers
        void bulkLoader_MemberLoaded(Int32 count)
        {
            //TODO: perf count this MemberUpdated(count, SearchUpdateType.Update);
        }

        void _loadQueue_MemberEnqueued(E1UpdateQueueEventArgs args)
        {
            SearchUpdateType updateType = SearchUpdateType.Update;

            if (args.E1Update.SearchUpdate != null)
            {
                updateType = args.E1Update.SearchUpdate.UpdateType;
            }

            UpdateQueueChanged(1, updateType);
        }

        void _loadQueue_MemberDequeued(E1UpdateQueueEventArgs args)
        {
            E1Member e1Member = args.E1Update.E1Member;
            SearchUpdate searchUpdate = args.E1Update.SearchUpdate;
            SearchUpdateType updateType = SearchUpdateType.Update;

            if (e1Member != null)
            {
                MemberSpec memberSpec = null;

                try
                {
                    memberSpec = convertToMemberSpec(e1Member);
                    memberSpec.Load();
                }
                finally
                {
                    if (memberSpec != null)
                    {
                        memberSpec.Dispose();
                    }
                }
            }
            else if (searchUpdate != null && searchUpdate.UpdateType == SearchUpdateType.Remove)
            {
                Load.MemberDelete(searchUpdate.MemberID, searchUpdate.CommunityID);
                updateType = searchUpdate.UpdateType;
            }
            else
            {
                MemberUpdateSpec memberUpdateSpec = null;

                try
                {
                    memberUpdateSpec = new MemberUpdateSpec();
                    memberUpdateSpec.MemberID = searchUpdate.MemberID;
                    memberUpdateSpec.CommunityID = searchUpdate.CommunityID;

                    switch (searchUpdate.UpdateType)
                    {
                        case SearchUpdateType.UpdateEmailCount:
                            memberUpdateSpec.EmailCount = searchUpdate.EmailCount;
                            break;
                        case SearchUpdateType.UpdateIsOnline:
                            memberUpdateSpec.IsOnline = searchUpdate.IsOnline ? UpdateFlag.SetYes : UpdateFlag.SetNo;
                            break;
                        case SearchUpdateType.UpdateLastActiveDate:
                            memberUpdateSpec.ActiveDate = searchUpdate.LastActiveDate;
                            break;
                    }

                    memberUpdateSpec.Load();
                }
                finally
                {
                    if (memberUpdateSpec != null)
                    {
                        memberUpdateSpec.Dispose();
                    }
                }

                updateType = searchUpdate.UpdateType;
            }

            MemberUpdated(1, updateType);
            UpdateQueueChanged(-1, updateType);
        }
        #endregion

        #region Static Conversion Methods
        private static string xslTransform(string xmlFileName, string xslFileName)
        {
            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.ProhibitDtd = false;
            XmlReader xslReader = XmlReader.Create(xslFileName, readerSettings);

            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(xslReader);

            StringBuilder result = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(result, xslt.OutputSettings);

            xslt.Transform(xmlFileName, xmlWriter);

            return result.ToString();
        }

        private static MemberSpec convertToMemberSpec(E1Member e1Member)
        {
            MemberSpec memberSpec = new MemberSpec();
            memberSpec.ActiveDate = e1Member.ActiveDate;
            memberSpec.BirthDate = e1Member.BirthDate;
            memberSpec.ChildrenCount = e1Member.ChildrenCount;
            memberSpec.CommunityID = e1Member.CommunityID;
            memberSpec.GenderMask = e1Member.GenderMask;
            memberSpec.HasPhoto = e1Member.HasPhoto;
            memberSpec.Height = e1Member.Height;
            memberSpec.IsOnline = e1Member.IsOnline;
            memberSpec.MemberID = e1Member.MemberID;
            memberSpec.MemberLocation.AreaCode = e1Member.AreaCode;
            memberSpec.MemberLocation.City = e1Member.City;
            memberSpec.MemberLocation.Country = e1Member.Country;
            memberSpec.MemberLocation.Latitude = e1Member.Latitude;
            memberSpec.MemberLocation.Longitude = e1Member.Longitude;
            memberSpec.MemberLocation.State = e1Member.State;
            memberSpec.MemberLocation.Zip = e1Member.Zip;
            memberSpec.EmailCount = e1Member.EmailCount;
            memberSpec.RegisterDate = e1Member.RegisterDate;
            memberSpec.Weight = e1Member.Weight;

            foreach (string name in e1Member.Tags.Keys)
            {
                if (name.ToLower() == "morechildrenflag")
                {
                    Int32 moreChildrenFlag = Conversion.CInt(e1Member.Tags[name]);
                    if (moreChildrenFlag >= 0)
                    {
                        memberSpec.MoreChildrenFlag = Convert.ToByte(moreChildrenFlag);
                    }
                }
                else if (name.ToLower() == "colorcode")
                {
                    Int32 colorcode = Conversion.CInt(e1Member.Tags[name]);
                    if (colorcode > 0)
                    {
                        memberSpec.ColorCode = Convert.ToByte(colorcode);
                    }
                }
                else
                {
                    memberSpec.AddTag(name, e1Member.Tags[name]);
                }
            }

            return memberSpec;
        }

        private static Query makeE1Query(IMatchnetQuery query)
        {
            Query e1Query = new Query();
            Location location = new Location();

            //TODO: Populate these once we have a UI for user-specified blends
            //Scores blend = new Scores();
            //?blend.Activity;
            //?blend.Match;
            //?blend.Popularity;
            //?blend.Total;
            //?blend.Weight;

            //Use 0 instead of HEIGHT_MIN because we don't want to reject members who have null height,
            //or members who entered heights less than current min before it was implemented
            e1Query.HeightMinDesired = 0;// Convert.ToInt16(Matchnet.Lib.ConstantsTemp.HEIGHT_MIN);
            e1Query.HeightMaxDesired = Convert.ToInt16(Matchnet.Lib.ConstantsTemp.HEIGHT_MAX);
            Int32 regionIDCountry = Constants.NULL_INT;
            string[] areaCodes = null;
            // System.Diagnostics.Trace.Write(query.ToString());
            for (Int32 i = 0; i < query.Count; i++)
            {
                object value = query[i].Value;
                Byte weight = Convert.ToByte(query[i].Weight);

                switch (query[i].Parameter)
                {
                    case QuerySearchParam.Sorting:
                        QuerySorting qs = (QuerySorting)Enum.Parse(typeof(QuerySorting), value.ToString());
                        if (qs == QuerySorting.ColorCode)
                            qs = QuerySorting.Proximity;

                        e1Query.SortKey = qs;
                        //?blend.Bias = weight;
                        break;
                    case QuerySearchParam.DomainID:
                        e1Query.CommunityID = Convert.ToInt16(value);
                        //blend.LDG = weight;
                        break;
                    case QuerySearchParam.GenderMask:
                        e1Query.GenderMask = Convert.ToInt16(value);
                        //blend.LDG = weight;
                        break;
                    case QuerySearchParam.RegionID:
                        RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(Conversion.CInt(value), (Int32)Matchnet.Language.English);
                        location.Latitude = (float)regionLanguage.Latitude;
                        location.Longitude = (float)regionLanguage.Longitude;
                        location.Country = regionLanguage.Depth1RegionID.ToString();
                        location.State = regionLanguage.StateRegionID.ToString();
                        location.City = regionLanguage.CityRegionID.ToString();
                        location.Zip = regionLanguage.PostalCodeRegionID.ToString();
                        break;
                    case QuerySearchParam.RegionIDCountry:
                        regionIDCountry = Conversion.CInt(value);
                        break;
                    case QuerySearchParam.AreaCode:
                        areaCodes = value.ToString().Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case QuerySearchParam.GeoDistance:
                        Int32 listOrder = Conversion.CInt(value);
                        e1Query.Radius = Convert.ToInt16(AttributeOptionSA.Instance.GetAttributeOptionCollection("Distance", Constants.NULL_INT).FindByListOrder(listOrder).Value);
                        //blend.Proximity = weight;
                        break;
                    case QuerySearchParam.HasPhotoFlag:
                        e1Query.HasPhoto = (QueryFlag)Conversion.CInt(value);
                        break;
                    case QuerySearchParam.AgeMin:
                        e1Query.AgeMinDesired = Convert.ToInt16(value);
                        //blend.Age = weight;
                        break;
                    case QuerySearchParam.AgeMax:
                        e1Query.AgeMaxDesired = Convert.ToInt16(value);
                        //blend.Age = weight;
                        break;
                    case QuerySearchParam.HeightMin:
                        e1Query.HeightMinDesired = Convert.ToInt16(value);
                        //blend.Height = weight;
                        break;
                    case QuerySearchParam.HeightMax:
                        //Make sure a non-zero min is specified if a max is specified since min == 0 means ignore height
                        if (e1Query.HeightMinDesired == 0)
                        {
                            e1Query.HeightMinDesired = Convert.ToInt16(Matchnet.Lib.ConstantsTemp.HEIGHT_MIN);
                        }

                        e1Query.HeightMaxDesired = Convert.ToInt16(value);
                        //blend.Height = weight;
                        break;
                    case QuerySearchParam.SchoolID:
                        e1Query.SchoolID = Convert.ToInt16(value);
                        break;
                    case QuerySearchParam.WeightMin:
                        e1Query.WeightMinDesired = Convert.ToInt32(value);
                        break;
                    case QuerySearchParam.WeightMax:
                        e1Query.WeightMaxDesired = Convert.ToInt32(value);
                        break;
                    case QuerySearchParam.ChildrenCount:
                        e1Query.ChildrenCount = Convert.ToInt32(value);
                        break;
                    case QuerySearchParam.RelocateFlag:
                        e1Query.AddParameter("relocate", value.ToString());
                        break;
                    case QuerySearchParam.MoreChildrenFlag:
                        e1Query.MoreChildrenFlag = Convert.ToInt32(value);
                        break;
                    case QuerySearchParam.ColorCode:
                        e1Query.ColorCode = Convert.ToInt32(value);
                        break;
                    default:
                        e1Query.AddParameter(query[i].Parameter.ToString(), (String)query[i].Value);
                        break;
                }
            }

            if (areaCodes != null)
            {
                addAreaCodes(e1Query, areaCodes, regionIDCountry);
            }

            e1Query.SearchLocation = location;
            //e1Query.Blend = blend;

            return e1Query;
        }

        private static void addAreaCodes(Query e1Query, string[] areaCodes, Int32 regionIDCountry)
        {
            foreach (string areaCode in areaCodes)
            {
                try
                {
                    string areaCodeString = areaCode;
                    Int32 areaCodeValue = Conversion.CInt(areaCodeString);

                    if (areaCodeValue != Constants.NULL_INT)
                    {
                        if (regionIDCountry == REGIONID_ISRAEL)
                        {
                            areaCodeString = AttributeOptionSA.Instance.GetAttributeOptionCollection("IsraelAreaCode", 10, 15, 1015)[areaCodeValue].Description;
                        }

                        e1Query.AddAreaCode(areaCodeString);
                    }
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException(Matchnet.e1.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure adding area code: " + areaCode.ToString(), ex);
                }
            }
        }

        #endregion

        #region Configuration
        private String CommandPath
        {
            get
            {
                try
                {
                    return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENGINESVC_COMMAND_PATH");
                }
                catch
                {
                    return @"C:\matchnet\data\e1\commands";
                }
            }
        }

        private String MachineName
        {
            get
            {
                return System.Net.Dns.GetHostEntry("127.0.0.1").AddressList[0].ToString();
            }
        }
        #endregion
    }
}
