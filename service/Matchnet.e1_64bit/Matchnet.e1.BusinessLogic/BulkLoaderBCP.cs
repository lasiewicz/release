using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

using Matchnet.Data;
using Matchnet.e1.Engine.ManagedAdapter;
using Matchnet.e1.ValueObjects;

namespace Matchnet.e1.BusinessLogic
{
    class BulkLoaderBCP : BulkLoaderBase
    {
        private const Int32 PARTITION_COUNT = 157;
        private const string HEADER_FILE_NAME = @"header.txt";
        private const string LOGICALDB_SEARCHSTORE = "mnSearchStore";

        private Int32 _partitionsWritten = 0;
        private Int32 _partitionsCompleted = 0;

        private Queue<string> _filesToLoad = new Queue<string>(PARTITION_COUNT);
        private XML _xml = new XML();

        public BulkLoaderBCP() : base(null)
        {
        }

        public override Int32 PartitionsCompleted
        {
            get
            {
                return _partitionsCompleted;
            }
        }

        public override Int32 PartitionsTotal
        {
            get
            {
                return PARTITION_COUNT;
            }
        }

        string _header = null;

        protected override void bulkLoad()
        {
            readHeader();

            //Start the thread that loads the BCP files into the engine from the _filesToLoad queue
            new Thread(new ThreadStart(bulkLoadFiles)).Start();

            //Now start generating the BCP files and queueing them up to be loaded by the engine
            BCPWriter bcpWriter = new BCPWriter(PARTITION_COUNT, getAbsolutePath(Environment.MachineName, DataPath, string.Empty));
            bcpWriter.BCPWritten += new BCPWriter.BCPWrittenEventHandler(bcpWriter_BCPWritten);
            bcpWriter.Start();
        }

        void bcpWriter_BCPWritten(string fileName, bool success)
        {
            Interlocked.Add(ref _partitionsWritten, 1);

            if (success)
            {
                lock (_filesToLoad)
                {
                    _filesToLoad.Enqueue(fileName);
                }
            }
        }


        private void readHeader()
        {
            StreamReader sr = null;

            try
            {
                sr = new StreamReader(DataPath + @"\" + HEADER_FILE_NAME);
                _header = sr.ReadLine();
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }

            if (_header == null)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Unable to load header file.  CSV load aborted.", EventLogEntryType.Error);
                return;
            }
        }


        private void bulkLoadFiles()
        {
            //Keep going until all files have been written and none are left to load
            for (_partitionsCompleted = 0; _partitionsCompleted < PARTITION_COUNT; _partitionsCompleted++)
            {
                //If queue is empty, sleep until BCP writer enqueues the next file to load
                while (_filesToLoad.Count == 0)
                {
                    Thread.Sleep(20);
                }

                string fileName = null;

                //Dequeue next file name to load
                lock (_filesToLoad)
                {
                    fileName = _filesToLoad.Dequeue();
                }

                try
                {
                    System.Diagnostics.Trace.WriteLine("bulkLoadFile " + fileName);

                    //Tell engine to load fileName
                    Load.LoadFile(fileName, _header, 0, _xml);

                    //Get results of file load as XML doc
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.InnerXml = _xml.CommandResult;
                    _xml.Reset();

                    //Parse out "ok" attribute from XML doc, which indicates number of members loaded succesfully
                    Int32 numOk = Conversion.CInt(doc.DocumentElement.Attributes["ok"].Value);
                    onMemberLoaded(numOk);

                    System.Diagnostics.Trace.WriteLine("bulkLoadFile " + fileName + " done");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error loading from file " + fileName + ":" + Environment.NewLine + ex.ToString(), EventLogEntryType.Error);
                }
            }

            onBulkLoadCompleted();
        }

        #region Configuration
        private string getAbsolutePath(string serverName, string pathRoot, string relativePath)
        {
            return @"\\" + serverName + @"\" + pathRoot.Replace(":", "$") + relativePath;
        }

        public static String DataPath
        {
            get
            {
                try
                {
                    return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENGINESVC_DATA_PATH");
                }
                catch
                {
                    return @"C:\matchnet\data\e1\data";
                }
            }
        }
        #endregion
    }
}
