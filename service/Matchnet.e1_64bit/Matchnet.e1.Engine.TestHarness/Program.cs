using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.e1.Engine.ManagedAdapter;
using System.IO;

namespace Matchnet.e1.Engine.TestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            //Int32 numTrials = 2000000;

            /*ManagedAdapterLoadTests.GSVStatisticsTest(numTrials);
            Console.WriteLine("GSVStatisticsTest Done.");
            ManagedAdapterLoadTests.LocationTest(numTrials);
            Console.WriteLine("LocationTest Done.");
            ManagedAdapterLoadTests.MemberSpecTest(numTrials);
            Console.WriteLine("MemberSpecTest Done.");
            ManagedAdapterLoadTests.MemberUpdateSpecTest(numTrials);
            Console.WriteLine("MemberUpdateSpecTest Done.");
            ManagedAdapterLoadTests.NameValueTest(numTrials);
            Console.WriteLine("NameValueTest Done.");*/
            //ManagedAdapterLoadTests.QueryTest(numTrials);
            //Console.WriteLine("QueryTest Done.");
            /*ManagedAdapterLoadTests.ResultTest(numTrials);
            Console.WriteLine("ResultTest Done.");
            ManagedAdapterLoadTests.ScoresTest(numTrials);
            Console.WriteLine("ScoresTest Done.");*/

            GSV gsv = new GSV();
            XML xml = new XML();
            StreamReader srHeader = new StreamReader(@"C:\matchnet\data\e1\data\header.txt");

            Load.LoadFile(@"C:\matchnet\data\e1\data\1000.bcp", srHeader.ReadLine(), 0, xml);

            Console.WriteLine();
            Console.WriteLine("Done.");
            Console.ReadLine();

            /*EvalPrint ep = new EvalPrint();
            //ep.DoCommand(@"load file=C:\Matchnet\Bedrock\Service\lib\e1\e1.Engine\Data\jd75k.csv");
            //ep.DoSearch();

            ep.DoCommand("time");

            foreach (string arg in args)
            {
                ep.DoCommandFile(arg);
            }

            ep.DoCommand("time mode=1");

            Int32 result = 0;

            while (result >= 0)
            {
                string command = Console.ReadLine();
                result = ep.DoCommand(command);
            }*/
        }
    }

    public static class ManagedAdapterLoadTests
    {
        public static void GSVStatisticsTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                GSVStatistics stats = new GSVStatistics();
                stats.Dispose();
            }
        }

        public static void LocationTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                Location location = new Location();
                location.Dispose();
            }
        }


        public static void MemberSpecTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                MemberSpec memberSpec = new MemberSpec();
                memberSpec.Dispose();
            }
        }

        public static void MemberUpdateSpecTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                MemberUpdateSpec memberUpdateSpec = new MemberUpdateSpec();
                memberUpdateSpec.Dispose();
            }
        }

        public static void NameValueTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                NameValue nameValue = new NameValue();
                nameValue.Dispose();
            }
        }

        public static void QueryTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                Query query = new Query();
                query.Dispose();
            }
        }

        public static void ResultTest(Int32 numTrials)
        {
            //for (Int32 i = 0; i < numTrials; i++)
            //{
                //Result result = new Result();
                //result.Dispose();
            //}
        }

        public static void ScoresTest(Int32 numTrials)
        {
            for (Int32 i = 0; i < numTrials; i++)
            {
                Scores scores = new Scores();
                scores.Dispose();
            }
        }
    }
}
