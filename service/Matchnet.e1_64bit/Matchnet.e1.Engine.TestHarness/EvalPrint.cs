using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Matchnet.e1.Engine.ManagedAdapter;

namespace Matchnet.e1.Engine.TestHarness
{
    class EvalPrint
    {
        private XML _xml;
        private GSV _gsv;

        public XML XML
        {
            get
            {
                return _xml;
            }
        }

        public EvalPrint()
        {
            _gsv = new GSV();
            _xml = new XML();
        }

        public Int32 DoCommand(string command)
        {
            Int32 result;
            command = command.Trim();

            if (command.StartsWith("@"))
            {
                result = DoCommandFile(command.Substring(1));
            }
            else if (command.StartsWith("!"))
            {
                result = LoadFile(command.Substring(1));
            }
            else
            {
                result = _xml.CommandEval(command);
                Console.WriteLine(_xml.CommandResult);
                _xml.Reset();
            }

          return result;
        }

        public Int32 DoCommandFile(string filename)
        {
            Int32 result = 0;

            if (!File.Exists(filename))
            {
                Console.WriteLine("File not found: " + filename);
                return -1;
            }
            
            Console.WriteLine("## Processing command file: " + filename);

            StreamReader sr = new StreamReader(filename);
            while (!sr.EndOfStream && result >= 0)
            {
                result = DoCommand(sr.ReadLine());
            }
            return result;
        }

        //MemID,DomID,GenderMask,BirthDate,HasPhoto,EducationLevel,Religion,LanguageMask,Ethnicity,SmokingHabits,DrinkingHabits,Height,MaritalStatus,JDateReligion,JDateEthnicity,SynagogueAttendance,KeepKosher,SexualIdentityType,RelationshipMask,RelationshipStatus,BodyType,Zodiac,MajorType,saPop,registerDate,ActiveDate,SchoolID,AreaCode,Country,State,City,Zip,Longitude,Latitude,IsOnline

        public Int32 LoadFile(string filename)
        {
            /*Dictionary<Int32, Int32> results = new Dictionary<Int32, Int32>();
            results[-2] = 0;
            results[-1] = 0;
            results[0] = 0;
            results[69] = 0;
            StreamReader sr = new StreamReader(filename);

            string[] spec = sr.ReadLine().Split(',');

            while (!sr.EndOfStream)
            {
                string[] values = sr.ReadLine().Split(',');

                E1Mem mem = new E1Mem();
                try
                {
                    mem.MemberID = Convert.ToInt32(values[0]);
                    mem.CommunityID = Convert.ToByte(values[1]);
                    mem.GenderMask = Convert.ToByte(values[2]);

                    for (Int32 i = 2; i < values.Length; i++)
                    {
                        mem.AddTag(spec[i], values[i]);
                    }
                    results[mem.load()]++;
                }
                catch
                {
                    results[69]++;
                }
            }

            foreach (Int32 key in results.Keys)
            {
                Console.WriteLine("{0}: {1}", key, results[key]);
            }*/

            return 0;
        }

        public void DoSearch()
        {
            Location location = new Location();
            location.Longitude = -2.066492F;
            location.Latitude = .594481F;
            location.Country = "USA";
            location.State = "California";
            location.City = "Beverly Hills";
            location.Zip = "90212";
            location.AreaCode = "323";

            Query query = new Query();
            query.CommunityID = 3;
            query.GenderMask = 1;
            query.AgeMinDesired = 67;
            query.AgeMaxDesired = 67;
            query.SearchLocation = location;
            query.Radius = 10;

            //Result result = Search.DoSearch(query);
            //Console.WriteLine(result.Count);
        }
    }
}
