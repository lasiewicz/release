﻿namespace Matchnet.Email.Harness
{
    partial class Messages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblImage = new System.Windows.Forms.Label();
            this.lblSiteID = new System.Windows.Forms.Label();
            this.lblCommunityID = new System.Windows.Forms.Label();
            this.lblToMemberID = new System.Windows.Forms.Label();
            this.lblFromMemberID = new System.Windows.Forms.Label();
            this.imgMessageAttachment = new System.Windows.Forms.PictureBox();
            this.txtMessageAttachmentOutput = new System.Windows.Forms.TextBox();
            this.btnSaveMessageAttachment = new System.Windows.Forms.Button();
            this.btnSelectImage = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.txtSiteID = new System.Windows.Forms.TextBox();
            this.txtCommunityID = new System.Windows.Forms.TextBox();
            this.txtToMemberID = new System.Windows.Forms.TextBox();
            this.txtFromMemberID = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIMHistoryPageSize = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIMHistoryStartRow = new System.Windows.Forms.TextBox();
            this.lvIMHistory = new System.Windows.Forms.ListView();
            this.chMemberID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTargetMemberID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMessageListID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMessageID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chStatusMask = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMessageInsertDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chDirectionFlag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMessageHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMessageBody = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnGetIMHistory = new System.Windows.Forms.Button();
            this.chkIMHistoryIgnoreCache = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.MemberID = new System.Windows.Forms.Label();
            this.txtIMHistoryGroupID = new System.Windows.Forms.TextBox();
            this.txtIMHistoryTargetMemberID = new System.Windows.Forms.TextBox();
            this.txtIMHistoryMemberID = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMessageMemberCommunityID = new System.Windows.Forms.TextBox();
            this.txtMessageMemberMemberID = new System.Windows.Forms.TextBox();
            this.txtMessageMemberOutput = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMessageMemberMessageListID = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button5 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgMessageAttachment)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1351, 665);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.lblImage);
            this.tabPage1.Controls.Add(this.lblSiteID);
            this.tabPage1.Controls.Add(this.lblCommunityID);
            this.tabPage1.Controls.Add(this.lblToMemberID);
            this.tabPage1.Controls.Add(this.lblFromMemberID);
            this.tabPage1.Controls.Add(this.imgMessageAttachment);
            this.tabPage1.Controls.Add(this.txtMessageAttachmentOutput);
            this.tabPage1.Controls.Add(this.btnSaveMessageAttachment);
            this.tabPage1.Controls.Add(this.btnSelectImage);
            this.tabPage1.Controls.Add(this.txtFile);
            this.tabPage1.Controls.Add(this.txtSiteID);
            this.tabPage1.Controls.Add(this.txtCommunityID);
            this.tabPage1.Controls.Add(this.txtToMemberID);
            this.tabPage1.Controls.Add(this.txtFromMemberID);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1343, 639);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Message Attachments";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblImage
            // 
            this.lblImage.AutoSize = true;
            this.lblImage.Location = new System.Drawing.Point(32, 68);
            this.lblImage.Name = "lblImage";
            this.lblImage.Size = new System.Drawing.Size(36, 13);
            this.lblImage.TabIndex = 13;
            this.lblImage.Text = "Image";
            // 
            // lblSiteID
            // 
            this.lblSiteID.AutoSize = true;
            this.lblSiteID.Location = new System.Drawing.Point(384, 15);
            this.lblSiteID.Name = "lblSiteID";
            this.lblSiteID.Size = new System.Drawing.Size(46, 13);
            this.lblSiteID.TabIndex = 12;
            this.lblSiteID.Text = "lblSiteID";
            // 
            // lblCommunityID
            // 
            this.lblCommunityID.AutoSize = true;
            this.lblCommunityID.Location = new System.Drawing.Point(268, 15);
            this.lblCommunityID.Name = "lblCommunityID";
            this.lblCommunityID.Size = new System.Drawing.Size(69, 13);
            this.lblCommunityID.TabIndex = 11;
            this.lblCommunityID.Text = "CommunityID";
            // 
            // lblToMemberID
            // 
            this.lblToMemberID.AutoSize = true;
            this.lblToMemberID.Location = new System.Drawing.Point(149, 15);
            this.lblToMemberID.Name = "lblToMemberID";
            this.lblToMemberID.Size = new System.Drawing.Size(69, 13);
            this.lblToMemberID.TabIndex = 10;
            this.lblToMemberID.Text = "ToMemberID";
            // 
            // lblFromMemberID
            // 
            this.lblFromMemberID.AutoSize = true;
            this.lblFromMemberID.Location = new System.Drawing.Point(32, 15);
            this.lblFromMemberID.Name = "lblFromMemberID";
            this.lblFromMemberID.Size = new System.Drawing.Size(79, 13);
            this.lblFromMemberID.TabIndex = 9;
            this.lblFromMemberID.Text = "FromMemberID";
            // 
            // imgMessageAttachment
            // 
            this.imgMessageAttachment.Location = new System.Drawing.Point(35, 312);
            this.imgMessageAttachment.Name = "imgMessageAttachment";
            this.imgMessageAttachment.Size = new System.Drawing.Size(100, 50);
            this.imgMessageAttachment.TabIndex = 8;
            this.imgMessageAttachment.TabStop = false;
            // 
            // txtMessageAttachmentOutput
            // 
            this.txtMessageAttachmentOutput.Location = new System.Drawing.Point(35, 153);
            this.txtMessageAttachmentOutput.Multiline = true;
            this.txtMessageAttachmentOutput.Name = "txtMessageAttachmentOutput";
            this.txtMessageAttachmentOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMessageAttachmentOutput.Size = new System.Drawing.Size(816, 141);
            this.txtMessageAttachmentOutput.TabIndex = 7;
            // 
            // btnSaveMessageAttachment
            // 
            this.btnSaveMessageAttachment.Location = new System.Drawing.Point(35, 124);
            this.btnSaveMessageAttachment.Name = "btnSaveMessageAttachment";
            this.btnSaveMessageAttachment.Size = new System.Drawing.Size(183, 23);
            this.btnSaveMessageAttachment.TabIndex = 6;
            this.btnSaveMessageAttachment.Text = "Save Message Attachment";
            this.btnSaveMessageAttachment.UseVisualStyleBackColor = true;
            this.btnSaveMessageAttachment.Click += new System.EventHandler(this.btnSaveMessageAttachment_Click);
            // 
            // btnSelectImage
            // 
            this.btnSelectImage.Location = new System.Drawing.Point(387, 81);
            this.btnSelectImage.Name = "btnSelectImage";
            this.btnSelectImage.Size = new System.Drawing.Size(121, 23);
            this.btnSelectImage.TabIndex = 5;
            this.btnSelectImage.Text = "Select Image";
            this.btnSelectImage.UseVisualStyleBackColor = true;
            this.btnSelectImage.Click += new System.EventHandler(this.btnSelectImage_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(35, 84);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(336, 20);
            this.txtFile.TabIndex = 4;
            // 
            // txtSiteID
            // 
            this.txtSiteID.Location = new System.Drawing.Point(387, 31);
            this.txtSiteID.Name = "txtSiteID";
            this.txtSiteID.Size = new System.Drawing.Size(100, 20);
            this.txtSiteID.TabIndex = 3;
            this.txtSiteID.Text = "103";
            // 
            // txtCommunityID
            // 
            this.txtCommunityID.Location = new System.Drawing.Point(271, 31);
            this.txtCommunityID.Name = "txtCommunityID";
            this.txtCommunityID.Size = new System.Drawing.Size(100, 20);
            this.txtCommunityID.TabIndex = 2;
            this.txtCommunityID.Text = "3";
            // 
            // txtToMemberID
            // 
            this.txtToMemberID.Location = new System.Drawing.Point(152, 31);
            this.txtToMemberID.Name = "txtToMemberID";
            this.txtToMemberID.Size = new System.Drawing.Size(100, 20);
            this.txtToMemberID.TabIndex = 1;
            // 
            // txtFromMemberID
            // 
            this.txtFromMemberID.Location = new System.Drawing.Point(35, 31);
            this.txtFromMemberID.Name = "txtFromMemberID";
            this.txtFromMemberID.Size = new System.Drawing.Size(100, 20);
            this.txtFromMemberID.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txtIMHistoryPageSize);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.txtIMHistoryStartRow);
            this.tabPage2.Controls.Add(this.lvIMHistory);
            this.tabPage2.Controls.Add(this.btnGetIMHistory);
            this.tabPage2.Controls.Add(this.chkIMHistoryIgnoreCache);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.MemberID);
            this.tabPage2.Controls.Add(this.txtIMHistoryGroupID);
            this.tabPage2.Controls.Add(this.txtIMHistoryTargetMemberID);
            this.tabPage2.Controls.Add(this.txtIMHistoryMemberID);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1343, 639);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "IM History";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(150, 68);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Get IM Members";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(476, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "PageSize";
            // 
            // txtIMHistoryPageSize
            // 
            this.txtIMHistoryPageSize.Location = new System.Drawing.Point(479, 33);
            this.txtIMHistoryPageSize.Name = "txtIMHistoryPageSize";
            this.txtIMHistoryPageSize.Size = new System.Drawing.Size(100, 20);
            this.txtIMHistoryPageSize.TabIndex = 23;
            this.txtIMHistoryPageSize.Text = "10000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(368, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "StartRow";
            // 
            // txtIMHistoryStartRow
            // 
            this.txtIMHistoryStartRow.Location = new System.Drawing.Point(371, 33);
            this.txtIMHistoryStartRow.Name = "txtIMHistoryStartRow";
            this.txtIMHistoryStartRow.Size = new System.Drawing.Size(100, 20);
            this.txtIMHistoryStartRow.TabIndex = 21;
            this.txtIMHistoryStartRow.Text = "1";
            // 
            // lvIMHistory
            // 
            this.lvIMHistory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chMemberID,
            this.chTargetMemberID,
            this.chMessageListID,
            this.chMessageID,
            this.chStatusMask,
            this.chMessageInsertDate,
            this.chDirectionFlag,
            this.chMessageHeader,
            this.chMessageBody});
            this.lvIMHistory.FullRowSelect = true;
            this.lvIMHistory.GridLines = true;
            this.lvIMHistory.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvIMHistory.Location = new System.Drawing.Point(21, 106);
            this.lvIMHistory.Name = "lvIMHistory";
            this.lvIMHistory.Size = new System.Drawing.Size(1301, 514);
            this.lvIMHistory.TabIndex = 20;
            this.lvIMHistory.UseCompatibleStateImageBehavior = false;
            this.lvIMHistory.View = System.Windows.Forms.View.Details;
            // 
            // chMemberID
            // 
            this.chMemberID.Text = "MemberID";
            this.chMemberID.Width = 89;
            // 
            // chTargetMemberID
            // 
            this.chTargetMemberID.Text = "TargetMemberID";
            this.chTargetMemberID.Width = 98;
            // 
            // chMessageListID
            // 
            this.chMessageListID.Text = "MessageListID";
            this.chMessageListID.Width = 110;
            // 
            // chMessageID
            // 
            this.chMessageID.Text = "MessageID";
            this.chMessageID.Width = 86;
            // 
            // chStatusMask
            // 
            this.chStatusMask.Text = "StatusMask";
            this.chStatusMask.Width = 91;
            // 
            // chMessageInsertDate
            // 
            this.chMessageInsertDate.Text = "InsertDate";
            this.chMessageInsertDate.Width = 122;
            // 
            // chDirectionFlag
            // 
            this.chDirectionFlag.Text = "DirectionFlag";
            this.chDirectionFlag.Width = 114;
            // 
            // chMessageHeader
            // 
            this.chMessageHeader.Text = "MessageHeader";
            this.chMessageHeader.Width = 181;
            // 
            // chMessageBody
            // 
            this.chMessageBody.Text = "MessageBody";
            this.chMessageBody.Width = 408;
            // 
            // btnGetIMHistory
            // 
            this.btnGetIMHistory.Location = new System.Drawing.Point(21, 68);
            this.btnGetIMHistory.Name = "btnGetIMHistory";
            this.btnGetIMHistory.Size = new System.Drawing.Size(116, 23);
            this.btnGetIMHistory.TabIndex = 19;
            this.btnGetIMHistory.Text = "Get IM History";
            this.btnGetIMHistory.UseVisualStyleBackColor = true;
            this.btnGetIMHistory.Click += new System.EventHandler(this.btnGetIMHistory_Click);
            // 
            // chkIMHistoryIgnoreCache
            // 
            this.chkIMHistoryIgnoreCache.AutoSize = true;
            this.chkIMHistoryIgnoreCache.Checked = true;
            this.chkIMHistoryIgnoreCache.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIMHistoryIgnoreCache.Location = new System.Drawing.Point(597, 33);
            this.chkIMHistoryIgnoreCache.Name = "chkIMHistoryIgnoreCache";
            this.chkIMHistoryIgnoreCache.Size = new System.Drawing.Size(90, 17);
            this.chkIMHistoryIgnoreCache.TabIndex = 18;
            this.chkIMHistoryIgnoreCache.Text = "Ignore Cache";
            this.chkIMHistoryIgnoreCache.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(254, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "CommunityID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(135, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Target MemberID";
            // 
            // MemberID
            // 
            this.MemberID.AutoSize = true;
            this.MemberID.Location = new System.Drawing.Point(18, 17);
            this.MemberID.Name = "MemberID";
            this.MemberID.Size = new System.Drawing.Size(56, 13);
            this.MemberID.TabIndex = 15;
            this.MemberID.Text = "MemberID";
            // 
            // txtIMHistoryGroupID
            // 
            this.txtIMHistoryGroupID.Location = new System.Drawing.Point(257, 33);
            this.txtIMHistoryGroupID.Name = "txtIMHistoryGroupID";
            this.txtIMHistoryGroupID.Size = new System.Drawing.Size(100, 20);
            this.txtIMHistoryGroupID.TabIndex = 14;
            this.txtIMHistoryGroupID.Text = "3";
            // 
            // txtIMHistoryTargetMemberID
            // 
            this.txtIMHistoryTargetMemberID.Location = new System.Drawing.Point(138, 33);
            this.txtIMHistoryTargetMemberID.Name = "txtIMHistoryTargetMemberID";
            this.txtIMHistoryTargetMemberID.Size = new System.Drawing.Size(100, 20);
            this.txtIMHistoryTargetMemberID.TabIndex = 13;
            this.txtIMHistoryTargetMemberID.Text = "100004579";
            // 
            // txtIMHistoryMemberID
            // 
            this.txtIMHistoryMemberID.Location = new System.Drawing.Point(21, 33);
            this.txtIMHistoryMemberID.Name = "txtIMHistoryMemberID";
            this.txtIMHistoryMemberID.Size = new System.Drawing.Size(100, 20);
            this.txtIMHistoryMemberID.TabIndex = 12;
            this.txtIMHistoryMemberID.Text = "16000206";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.txtMessageMemberCommunityID);
            this.tabPage3.Controls.Add(this.txtMessageMemberMemberID);
            this.tabPage3.Controls.Add(this.txtMessageMemberOutput);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.txtMessageMemberMessageListID);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1343, 639);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "MarkMessageMemberRemove";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(277, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "CommunityID";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(158, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "MemberID";
            // 
            // txtMessageMemberCommunityID
            // 
            this.txtMessageMemberCommunityID.Location = new System.Drawing.Point(280, 41);
            this.txtMessageMemberCommunityID.Name = "txtMessageMemberCommunityID";
            this.txtMessageMemberCommunityID.Size = new System.Drawing.Size(100, 20);
            this.txtMessageMemberCommunityID.TabIndex = 19;
            this.txtMessageMemberCommunityID.Text = "3";
            // 
            // txtMessageMemberMemberID
            // 
            this.txtMessageMemberMemberID.Location = new System.Drawing.Point(161, 41);
            this.txtMessageMemberMemberID.Name = "txtMessageMemberMemberID";
            this.txtMessageMemberMemberID.Size = new System.Drawing.Size(100, 20);
            this.txtMessageMemberMemberID.TabIndex = 18;
            this.txtMessageMemberMemberID.Text = "100004579";
            // 
            // txtMessageMemberOutput
            // 
            this.txtMessageMemberOutput.Location = new System.Drawing.Point(19, 158);
            this.txtMessageMemberOutput.Multiline = true;
            this.txtMessageMemberOutput.Name = "txtMessageMemberOutput";
            this.txtMessageMemberOutput.Size = new System.Drawing.Size(1291, 417);
            this.txtMessageMemberOutput.TabIndex = 5;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(363, 85);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(222, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "UnmarkInstantMessageMemberRemove";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(151, 85);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(206, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "MarkInstantMessageMemberRemove";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "MessageList ID";
            // 
            // txtMessageMemberMessageListID
            // 
            this.txtMessageMemberMessageListID.Location = new System.Drawing.Point(19, 41);
            this.txtMessageMemberMessageListID.Name = "txtMessageMemberMessageListID";
            this.txtMessageMemberMessageListID.Size = new System.Drawing.Size(126, 20);
            this.txtMessageMemberMessageListID.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(19, 85);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Get MessageList";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(296, 68);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(175, 23);
            this.button5.TabIndex = 26;
            this.button5.Text = "Get IM Members including Soft Removed";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Messages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1373, 677);
            this.Controls.Add(this.tabControl1);
            this.Name = "Messages";
            this.Text = "Messages";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgMessageAttachment)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblImage;
        private System.Windows.Forms.Label lblSiteID;
        private System.Windows.Forms.Label lblCommunityID;
        private System.Windows.Forms.Label lblToMemberID;
        private System.Windows.Forms.Label lblFromMemberID;
        private System.Windows.Forms.TextBox txtMessageAttachmentOutput;
        private System.Windows.Forms.Button btnSaveMessageAttachment;
        private System.Windows.Forms.Button btnSelectImage;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.TextBox txtSiteID;
        private System.Windows.Forms.TextBox txtCommunityID;
        private System.Windows.Forms.TextBox txtToMemberID;
        private System.Windows.Forms.TextBox txtFromMemberID;
        private System.Windows.Forms.CheckBox chkIMHistoryIgnoreCache;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label MemberID;
        private System.Windows.Forms.TextBox txtIMHistoryGroupID;
        private System.Windows.Forms.TextBox txtIMHistoryTargetMemberID;
        private System.Windows.Forms.TextBox txtIMHistoryMemberID;
        private System.Windows.Forms.Button btnGetIMHistory;
        private System.Windows.Forms.PictureBox imgMessageAttachment;
        private System.Windows.Forms.ListView lvIMHistory;
        private System.Windows.Forms.ColumnHeader chMessageListID;
        private System.Windows.Forms.ColumnHeader chMessageID;
        private System.Windows.Forms.ColumnHeader chStatusMask;
        private System.Windows.Forms.ColumnHeader chMessageInsertDate;
        private System.Windows.Forms.ColumnHeader chMessageHeader;
        private System.Windows.Forms.ColumnHeader chMessageBody;
        private System.Windows.Forms.ColumnHeader chDirectionFlag;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIMHistoryPageSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIMHistoryStartRow;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ColumnHeader chMemberID;
        private System.Windows.Forms.ColumnHeader chTargetMemberID;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtMessageMemberOutput;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMessageMemberMessageListID;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMessageMemberCommunityID;
        private System.Windows.Forms.TextBox txtMessageMemberMemberID;
        private System.Windows.Forms.Button button5;
    }
}