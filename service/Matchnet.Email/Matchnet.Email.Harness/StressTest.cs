using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.Email.Harness
{
	/// <summary>
	/// Summary description for StressTest.
	/// </summary>
	public class StressTest : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public StressTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(32, 40);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(24, 96);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(408, 216);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(112, 40);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(56, 32);
			this.button2.TabIndex = 2;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(256, 48);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(40, 20);
			this.textBox2.TabIndex = 3;
			this.textBox2.Text = "";
			// 
			// StressTest
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(480, 349);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.Name = "StressTest";
			this.Text = "StressTest";
			this.ResumeLayout(false);

		}
		#endregion

		Int32 numThreads = 1;
		Int32 usersPerThread = 1;
		Int32 numMessages = 1;
		Int32 myMemberID = 988272102;

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				for (int i = 0; i < numThreads; i++)
				{
					new Thread(new ThreadStart(sendMessages)).Start();
				}
			}
			catch (Exception ex)
			{
				log(ex.ToString());
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			new Thread(new ThreadStart(deleteMessages)).Start();
		}		

		private void deleteMessages()
		{
			EmailMessageCollection messages = EmailMessageSA.Instance.RetrieveMessages(988272102, 3, (Int32) SystemFolders.Inbox);
			lock (messages.SyncRoot)
			{
				foreach (EmailMessage message in messages)
				{
					ArrayList memberMailIDs = new ArrayList();
					memberMailIDs.Add(message.MemberMailID);
					EmailMessageSA.Instance.MoveToFolder(myMemberID, 3, memberMailIDs, (Int32) SystemFolders.Trash);
					EmailMessageSA.Instance.DeleteMessage(myMemberID, 3, memberMailIDs);
					Thread.Sleep(1000);
				}
			}
		}

		private void sendMessages()
		{
			for (int i = 0; i < usersPerThread; i++)
			{
				Int32 memberID = Data.Instance.GetNextMemberID();
				Int32 groupID = 3;
				Int32 targetMemberID = 988272102;

				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				while (!member.IsCommunityMember(groupID))
				{
					memberID = Data.Instance.GetNextMemberID();
					member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				}

				readMessages(memberID, groupID);

				for (i = 0; i < numMessages; i++)
				{
					sendMessage(memberID, targetMemberID, groupID);
					Thread.Sleep(1000);
				}
			}
		}

		private void readMessages(Int32 memberID, Int32 groupID)
		{
            //ICollection messages = EmailMessageSA.Instance.RetrieveMessages(memberID, groupID, (Int32) SystemFolders.Inbox, 1, 10, "insertdate desc");
            //Thread.Sleep(2000);

            //foreach(EmailMessage message in messages)
            //{
            //    EmailMessageSA.Instance.RetrieveMessage(groupID, memberID, message.MemberMailID, "insertdate desc", Direction.None, false);
            //    Thread.Sleep(1000);
            //}
		}

		private Int32 sendMessage(Int32 memberID, Int32 targetMemberID, Int32 groupID)
		{
			EmailMessage message = null;//new EmailMessage();
			//message.FromMemberID = targetMemberID;
			//message.ToMemberID = memberID;
			//message.GroupID = groupID;
			//message.Subject = "Auto-Generated Test Message";
			//message.Content = "This message was generated by an automated test script.";
			//message.MailTypeID = MailType.Email;

			for (int i = 0; i < 100; i++)
			{
				//EmailMessageSA.Instance.SendMessage(message, true, Constants.NULL_INT);
				//message = (EmailMessage) message.Clone();
				//message.MailID = Constants.NULL_INT;
				//message.MemberMailID = Constants.NULL_INT;
			}

			//message.FromMemberID = memberID;
			//message.ToMemberID = targetMemberID;

			//bool result = EmailMessageSA.Instance.SendMessage(message, true, Constants.NULL_INT);
			
			//log("Result Status: " + result.ToString() + "\tSender MemberMailID: " + message.MemberMailID + System.Environment.NewLine);

			return message.MemberMailID;
		}

		private ReaderWriterLock messageCountLock = new ReaderWriterLock();
		private ReaderWriterLock textBoxLock = new ReaderWriterLock();
		private int _messageCount;

		private int MessageCount
		{
			get
			{
				messageCountLock.AcquireReaderLock(-1);
				int retVal = _messageCount;
				messageCountLock.ReleaseReaderLock();
				return retVal;
			}
			set
			{
				messageCountLock.AcquireWriterLock(-1);
				_messageCount = value;
				textBox2.Text = "F";//_messageCount.ToString();
				messageCountLock.ReleaseWriterLock();
			}
		}

		private void log(string text)
		{
			textBoxLock.AcquireWriterLock(-1);
			textBox1.Text += text;
			textBoxLock.ReleaseWriterLock();
		}
	}
}
