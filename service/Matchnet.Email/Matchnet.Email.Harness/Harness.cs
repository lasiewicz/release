using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ServiceDefinitions;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.Harness
{
	/// <summary>
	/// Summary description for EmailHarness.
	/// </summary>
	public class EmailHarness : System.Windows.Forms.Form
	{
		#region Private Members

		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.Label lblEmailFolder;
		private System.Windows.Forms.Label lblOutput;
		private System.Windows.Forms.Label lblMemberFolderID2;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.Button btnRetrieveFolders;
		private System.Windows.Forms.Panel pnlEmailFolder;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.Button btnUpdateFolder;
		private System.Windows.Forms.Button btnEmptyFolder;
		private System.Windows.Forms.Button btnDeleteFolder;
		private System.Windows.Forms.Button btnCreateFolder;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Label lblDomainID2;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem miTease;
		private System.Windows.Forms.MenuItem miInternalMail;
		private System.Windows.Forms.MenuItem miRetrieve;
		private System.Windows.Forms.TextBox txtFolderName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cmbCommunityID;
		private System.Windows.Forms.TextBox txtMemberFolderID;
        private System.Windows.Forms.MenuItem menuItemStressTest;
		#endregion
        private Button button1;
        private MenuItem menuItem2;
        private IContainer components;

		#region Constructor
		public EmailHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new EmailHarness());
		}
		#endregion

		#region Output Button Events
		private void btnClear_Click(object sender, System.EventArgs e)
		{
			this.txtOutput.Text = "";
		}
		#endregion

		#region Email Message Button Events
		private void btnSendMessage_Click(object sender, System.EventArgs e)
		{
			try
			{
				throw new Exception("Implement for new SA interface.");
//				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.SendMessage(EmailMessage.MailType.EMAIL,
//					ToCInt(txtDomainID1.Text),
//					ToCInt(txtFromMember.Text),
//					"Test Harness From",
//					ToCInt(txtToMember.Text),
//					"Test Harness To", 
//					"Test Harness Email",
//					"Test Harness Content",
//					0,
//					System.DateTime.Now,
//					ToCInt(txtMemberMailID.Text),
//					2);
//				FormatOutput(sender.ToString(), bResult.ToString());
			}
			catch (Exception x)
			{
				FormatOutput(sender.ToString(), x.Message);
			}
		}

		private void btnSendMessageObj_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				EmailMessage oMessage = new EmailMessage();
//				oMessage.MailTypeID = EmailMessage.MailType.EMAIL;
//				oMessage.ToMemberID =  ToCInt(txtToMember.Text);
//				oMessage.FromMemberID = ToCInt(txtFromMember.Text);
//				oMessage.FromUserName = "Test Harness From";
//				oMessage.ToUserName = "Test Harness To";
//				oMessage.Subject = "Test Harness Email";
//				oMessage.Content = "Test Harness Content";
//				oMessage.ResourceID = 0;
//				oMessage.MemberMailID = ToCInt(txtMemberMailID.Text);
//				oMessage.LanguageID = 2;
//
//				throw new Exception("Implement me please!");
////				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.SendMessage(oMessage,ToCInt(txtDomainID1.Text),0,true,false);
////				FormatOutput(sender.ToString(), bResult.ToString());			
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}

		}

		private void btnRetrieveMessage_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				EmailMessage oMessage = new EmailMessage();
//				oMessage = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessage(ToCInt(txtToMember.Text),
//					ToCInt(txtMemberMailID.Text),
//					null,
//					Direction.None,
//					false);
//				FormatOutput(sender.ToString(), oMessage.Subject);
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		
		}

		private void btnRetrieveMessages_Click(object sender, System.EventArgs e)
		{
//			try
			
//			{
//				EmailMessageCollection oMessages = new EmailMessageCollection();
//				oMessages = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessages(ToCInt(txtToMember.Text),
//					ToCInt(txtDomainID.Text),
//					ToCInt(txtMemberFolderID.Text),
//					1,
//					10,
//					"fromusername desc");
//
//				System.Text.StringBuilder oBuilder = new System.Text.StringBuilder();
//				oBuilder.Append("\n");
//				foreach (EmailMessage oMessage in oMessages)
//				{
//					oBuilder.Append(oMessage.MemberMailID);
//					oBuilder.Append(": ");
//					oBuilder.Append(oMessage.Subject);
//					oBuilder.Append("\n");					
//				}
//				FormatOutput(sender.ToString(), oBuilder.ToString());			
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		}

		private void btnMarkRead_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.MarkRead(ToCInt(txtToMember.Text),
//					true,
//					txtMemberMailID.Text);
//
//				FormatOutput(sender.ToString(), bResult.ToString());
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		}

		private void btnMoveMessage_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.MoveToFolder(ToCInt(txtToMember.Text),
//					txtMemberMailID.Text,
//					ToCInt(txtMemberFolderID1.Text));
//
//				FormatOutput(sender.ToString(), bResult.ToString());
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		}

		private void btnIgnoreMessage_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.IgnoreMessage(ToCInt(txtToMember.Text),
//					ToCInt(txtDomainID1.Text),
//					txtMemberMailID.Text);
//
//				FormatOutput(sender.ToString(), bResult.ToString());
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		}

		private void btnDeleteMessage_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.DeleteMessage(ToCInt(txtToMember.Text),
//					txtMemberMailID.Text);
//
//				FormatOutput(sender.ToString(), bResult.ToString());
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		
		}

		private void btnHasMail_Click(object sender, System.EventArgs e)
		{
//			try
//			{
//				bool bResult = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.HasNewMail(ToCInt(txtToMember.Text),
//					ToCInt(txtDomainID1.Text));
//
//				FormatOutput(sender.ToString(), bResult.ToString());			
//			}
//			catch (Exception x)
//			{
//				FormatOutput(sender.ToString(), x.Message);
//			}
		}
		#endregion

		#region Email Folder Button Events
		private void btnCreateFolder_Click(object sender, System.EventArgs e)
		{
			try
			{
				//int iResult = Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.Save(ToCInt(txtMemberID.Text),
				//	ToCInt(txtDomainID2.Text),
				//	"THFolder");

				int communityID = Convert.ToInt32(cmbCommunityID.SelectedItem.ToString());
				string clientHostName = System.Environment.MachineName;

				FolderSaveResult iResult = 
					Matchnet.Email.BusinessLogic.EmailFolderBL.Instance.Save(clientHostName, 
					ToCInt(txtMemberID.Text),
					communityID,
					txtFolderName.Text);

				FormatOutput(sender.ToString(), iResult.FolderSaveResultStatus.ToString());
			}
			catch (Exception x)
			{
				FormatOutput(sender.ToString(), x.Message);
			}
		}

		private void btnUpdateFolder_Click(object sender, System.EventArgs e)
		{
			try
			{
				//int iResult = Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.Save(ToCInt(txtMemberID.Text),
				//	ToCInt(txtDomainID2.Text),
				//	"THFolder2",
				//	ToCInt(txtMemberFolderID2.Text));
				FolderSaveResult iResult = Matchnet.Email.BusinessLogic.EmailFolderBL.Instance.Save(System.Environment.MachineName,
					ToCInt(txtMemberID.Text),
					ToCInt(cmbCommunityID.SelectedItem.ToString()),
					txtFolderName.Text,
					ToCInt(txtMemberFolderID.Text));

				FormatOutput(sender.ToString(), iResult.ToString());			
			}
			catch (Exception x)
			{
				FormatOutput(sender.ToString(), x.Message);
			}
		}

		private void btnDeleteFolder_Click(object sender, System.EventArgs e)
		{
			try
			{
				Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.Delete(ToCInt(txtMemberFolderID.Text), ToCInt(txtMemberID.Text), Conversion.CInt(cmbCommunityID.SelectedValue));

				FormatOutput(sender.ToString(), "Deleted");	
			}
			catch (Exception x)
			{
				FormatOutput(sender.ToString(), x.Message);
			}
		}

		private void btnEmptyFolder_Click(object sender, System.EventArgs e)
		{
			try
			{
				/*Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.EmptyFolder(ToCInt(txtMemberID.Text),
					ToCInt(cmbCommunityID.SelectedItem.ToString()),
					ToCInt(txtMemberFolderID.Text));*/

				//FormatOutput(sender.ToString(),"Emptied Folder");				
			}
			catch (Exception x)
			{
				FormatOutput(sender.ToString(), x.Message);
			}
		}

		private void btnRetrieveFolders_Click(object sender, System.EventArgs e)
		{
			try
			{

				EmailFolderCollection oFolders;
//				oFolders = Matchnet.Email.ServiceAdapters.EmailFolderSA.Instance.RetrieveFolders(ToCInt(txtMemberID.Text),
//					ToCInt(cmbCommunityID.SelectedItem.ToString()),
//                	true);
				
				oFolders = Matchnet.Email.BusinessLogic.EmailFolderBL.Instance.RetrieveFolders(null,
					null,
					ToCInt(txtMemberID.Text),
					ToCInt(cmbCommunityID.SelectedItem.ToString()),
					true);

				System.Text.StringBuilder oBuilder = new System.Text.StringBuilder();
				oBuilder.Append("\n");
				foreach (EmailFolder oFolder in oFolders)
				{
					oBuilder.Append(oFolder.MemberFolderID);
					oBuilder.Append(": ");
					oBuilder.Append(oFolder.Description);
					oBuilder.Append(" ");
					//oBuilder.Append(oFolder.DisplayKBytes);
					oBuilder.Append("\n");					
				}
				oBuilder.Append("Total Bytes:");
				//oBuilder.Append(oFolders.TotalDisplayKBytes());

				FormatOutput(sender.ToString(), oBuilder.ToString());
			}
			catch (Exception x)
			{
				FormatOutput(sender.ToString(), x.Message);
			}
		}
		#endregion

		#region Private Methods

		private void FormatOutput(string sEvent, string s)
		{
			System.Text.StringBuilder oBuilder = new System.Text.StringBuilder();
			oBuilder.Append("\n");
			oBuilder.Append("---------------- Begin Output ----------------");
			oBuilder.Append("\n");
			oBuilder.Append(sEvent);
			oBuilder.Append(": ");
			oBuilder.Append(s);
			oBuilder.Append("\n");
			oBuilder.Append("---------------- End Output ----------------");
			oBuilder.Append("\n");
			txtOutput.Text += oBuilder.ToString();
		}

		private int ToCInt(string num)
		{
			//double dblNum;
			//Double.TryParse(num, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out dblNum);
			int iNum = int.Parse(num, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo);//(int) dblNum;
			return iNum;
		}
		#endregion
		
		#region Disposal
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.pnlEmailFolder = new System.Windows.Forms.Panel();
            this.cmbCommunityID = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFolderName = new System.Windows.Forms.TextBox();
            this.lblDomainID2 = new System.Windows.Forms.Label();
            this.btnUpdateFolder = new System.Windows.Forms.Button();
            this.btnEmptyFolder = new System.Windows.Forms.Button();
            this.btnDeleteFolder = new System.Windows.Forms.Button();
            this.btnCreateFolder = new System.Windows.Forms.Button();
            this.btnRetrieveFolders = new System.Windows.Forms.Button();
            this.txtMemberFolderID = new System.Windows.Forms.TextBox();
            this.lblMemberFolderID2 = new System.Windows.Forms.Label();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.lblEmailFolder = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.RichTextBox();
            this.lblOutput = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.miTease = new System.Windows.Forms.MenuItem();
            this.miInternalMail = new System.Windows.Forms.MenuItem();
            this.miRetrieve = new System.Windows.Forms.MenuItem();
            this.menuItemStressTest = new System.Windows.Forms.MenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.pnlEmailFolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlEmailFolder
            // 
            this.pnlEmailFolder.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnlEmailFolder.Controls.Add(this.cmbCommunityID);
            this.pnlEmailFolder.Controls.Add(this.label1);
            this.pnlEmailFolder.Controls.Add(this.txtFolderName);
            this.pnlEmailFolder.Controls.Add(this.lblDomainID2);
            this.pnlEmailFolder.Controls.Add(this.btnUpdateFolder);
            this.pnlEmailFolder.Controls.Add(this.btnEmptyFolder);
            this.pnlEmailFolder.Controls.Add(this.btnDeleteFolder);
            this.pnlEmailFolder.Controls.Add(this.btnCreateFolder);
            this.pnlEmailFolder.Controls.Add(this.btnRetrieveFolders);
            this.pnlEmailFolder.Controls.Add(this.txtMemberFolderID);
            this.pnlEmailFolder.Controls.Add(this.lblMemberFolderID2);
            this.pnlEmailFolder.Controls.Add(this.lblMemberID);
            this.pnlEmailFolder.Controls.Add(this.txtMemberID);
            this.pnlEmailFolder.Location = new System.Drawing.Point(16, 296);
            this.pnlEmailFolder.Name = "pnlEmailFolder";
            this.pnlEmailFolder.Size = new System.Drawing.Size(640, 176);
            this.pnlEmailFolder.TabIndex = 12;
            // 
            // cmbCommunityID
            // 
            this.cmbCommunityID.Items.AddRange(new object[] {
            "1",
            "3",
            "17"});
            this.cmbCommunityID.Location = new System.Drawing.Point(120, 40);
            this.cmbCommunityID.Name = "cmbCommunityID";
            this.cmbCommunityID.Size = new System.Drawing.Size(121, 21);
            this.cmbCommunityID.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(296, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 23);
            this.label1.TabIndex = 33;
            this.label1.Text = "Folder Name (Custom):";
            // 
            // txtFolderName
            // 
            this.txtFolderName.Location = new System.Drawing.Point(424, 40);
            this.txtFolderName.Name = "txtFolderName";
            this.txtFolderName.Size = new System.Drawing.Size(200, 20);
            this.txtFolderName.TabIndex = 32;
            // 
            // lblDomainID2
            // 
            this.lblDomainID2.Location = new System.Drawing.Point(0, 40);
            this.lblDomainID2.Name = "lblDomainID2";
            this.lblDomainID2.Size = new System.Drawing.Size(104, 24);
            this.lblDomainID2.TabIndex = 30;
            this.lblDomainID2.Text = "Community ID:";
            this.lblDomainID2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUpdateFolder
            // 
            this.btnUpdateFolder.BackColor = System.Drawing.SystemColors.Control;
            this.btnUpdateFolder.Location = new System.Drawing.Point(496, 96);
            this.btnUpdateFolder.Name = "btnUpdateFolder";
            this.btnUpdateFolder.Size = new System.Drawing.Size(128, 23);
            this.btnUpdateFolder.TabIndex = 29;
            this.btnUpdateFolder.Text = "Update Folder";
            this.btnUpdateFolder.UseVisualStyleBackColor = false;
            this.btnUpdateFolder.Click += new System.EventHandler(this.btnUpdateFolder_Click);
            // 
            // btnEmptyFolder
            // 
            this.btnEmptyFolder.BackColor = System.Drawing.SystemColors.Control;
            this.btnEmptyFolder.Enabled = false;
            this.btnEmptyFolder.Location = new System.Drawing.Point(16, 80);
            this.btnEmptyFolder.Name = "btnEmptyFolder";
            this.btnEmptyFolder.Size = new System.Drawing.Size(128, 23);
            this.btnEmptyFolder.TabIndex = 28;
            this.btnEmptyFolder.Text = "Empty Folder";
            this.btnEmptyFolder.UseVisualStyleBackColor = false;
            this.btnEmptyFolder.Click += new System.EventHandler(this.btnEmptyFolder_Click);
            // 
            // btnDeleteFolder
            // 
            this.btnDeleteFolder.BackColor = System.Drawing.SystemColors.Control;
            this.btnDeleteFolder.Location = new System.Drawing.Point(496, 128);
            this.btnDeleteFolder.Name = "btnDeleteFolder";
            this.btnDeleteFolder.Size = new System.Drawing.Size(128, 23);
            this.btnDeleteFolder.TabIndex = 27;
            this.btnDeleteFolder.Text = "Delete Folder";
            this.btnDeleteFolder.UseVisualStyleBackColor = false;
            this.btnDeleteFolder.Click += new System.EventHandler(this.btnDeleteFolder_Click);
            // 
            // btnCreateFolder
            // 
            this.btnCreateFolder.BackColor = System.Drawing.SystemColors.Control;
            this.btnCreateFolder.Location = new System.Drawing.Point(496, 64);
            this.btnCreateFolder.Name = "btnCreateFolder";
            this.btnCreateFolder.Size = new System.Drawing.Size(128, 23);
            this.btnCreateFolder.TabIndex = 26;
            this.btnCreateFolder.Text = "Create Folder";
            this.btnCreateFolder.UseVisualStyleBackColor = false;
            this.btnCreateFolder.Click += new System.EventHandler(this.btnCreateFolder_Click);
            // 
            // btnRetrieveFolders
            // 
            this.btnRetrieveFolders.BackColor = System.Drawing.SystemColors.Control;
            this.btnRetrieveFolders.Location = new System.Drawing.Point(160, 80);
            this.btnRetrieveFolders.Name = "btnRetrieveFolders";
            this.btnRetrieveFolders.Size = new System.Drawing.Size(128, 23);
            this.btnRetrieveFolders.TabIndex = 25;
            this.btnRetrieveFolders.Text = "Retrieve Folders";
            this.btnRetrieveFolders.UseVisualStyleBackColor = false;
            this.btnRetrieveFolders.Click += new System.EventHandler(this.btnRetrieveFolders_Click);
            // 
            // txtMemberFolderID
            // 
            this.txtMemberFolderID.Location = new System.Drawing.Point(424, 8);
            this.txtMemberFolderID.Name = "txtMemberFolderID";
            this.txtMemberFolderID.Size = new System.Drawing.Size(104, 20);
            this.txtMemberFolderID.TabIndex = 24;
            this.txtMemberFolderID.Text = "1";
            // 
            // lblMemberFolderID2
            // 
            this.lblMemberFolderID2.Location = new System.Drawing.Point(288, 8);
            this.lblMemberFolderID2.Name = "lblMemberFolderID2";
            this.lblMemberFolderID2.Size = new System.Drawing.Size(104, 24);
            this.lblMemberFolderID2.TabIndex = 23;
            this.lblMemberFolderID2.Text = "Member Folder ID:";
            this.lblMemberFolderID2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMemberID
            // 
            this.lblMemberID.Location = new System.Drawing.Point(16, 8);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(96, 24);
            this.lblMemberID.TabIndex = 18;
            this.lblMemberID.Text = "From Member ID:";
            this.lblMemberID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMemberID
            // 
            this.txtMemberID.Location = new System.Drawing.Point(120, 8);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(120, 20);
            this.txtMemberID.TabIndex = 17;
            this.txtMemberID.Text = "16000206";
            // 
            // lblEmailFolder
            // 
            this.lblEmailFolder.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailFolder.Location = new System.Drawing.Point(16, 272);
            this.lblEmailFolder.Name = "lblEmailFolder";
            this.lblEmailFolder.Size = new System.Drawing.Size(104, 16);
            this.lblEmailFolder.TabIndex = 14;
            this.lblEmailFolder.Text = "Email Folder:";
            this.lblEmailFolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(16, 40);
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(640, 216);
            this.txtOutput.TabIndex = 15;
            this.txtOutput.Text = "";
            // 
            // lblOutput
            // 
            this.lblOutput.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutput.Location = new System.Drawing.Point(16, 8);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(72, 24);
            this.lblOutput.TabIndex = 16;
            this.lblOutput.Text = "Output:";
            this.lblOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.SystemColors.Control;
            this.btnClear.Location = new System.Drawing.Point(560, 8);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(96, 23);
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.miTease,
            this.miInternalMail,
            this.miRetrieve,
            this.menuItemStressTest,
            this.menuItem2});
            this.menuItem1.Text = "Harness";
            // 
            // miTease
            // 
            this.miTease.Index = 0;
            this.miTease.Text = "Tease";
            this.miTease.Click += new System.EventHandler(this.miTease_Click);
            // 
            // miInternalMail
            // 
            this.miInternalMail.Index = 1;
            this.miInternalMail.Text = "Internal Mail (Save/Send)";
            this.miInternalMail.Click += new System.EventHandler(this.miInternalMail_Click);
            // 
            // miRetrieve
            // 
            this.miRetrieve.Index = 2;
            this.miRetrieve.Text = "Internal Mail (Retrieve)";
            this.miRetrieve.Click += new System.EventHandler(this.miRetrieve_Click);
            // 
            // menuItemStressTest
            // 
            this.menuItemStressTest.Index = 3;
            this.menuItemStressTest.Text = "Stress Test";
            this.menuItemStressTest.Click += new System.EventHandler(this.menuItemStressTest_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(298, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 30);
            this.button1.TabIndex = 31;
            this.button1.Text = "Check Member Mails";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 4;
            this.menuItem2.Text = "Messages Test";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // EmailHarness
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(672, 481);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.lblEmailFolder);
            this.Controls.Add(this.pnlEmailFolder);
            this.Menu = this.mainMenu1;
            this.Name = "EmailHarness";
            this.Text = "Email Service Test Harness";
            this.pnlEmailFolder.ResumeLayout(false);
            this.pnlEmailFolder.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private void miTease_Click(object sender, System.EventArgs e)
		{
			TeaseHarness teaseApplication = new TeaseHarness();
			teaseApplication.ShowDialog(this);
		}

		private void miInternalMail_Click(object sender, System.EventArgs e)
		{
			InternalEmailHarness iMailHarness = new InternalEmailHarness();
			iMailHarness.Show();
		}

		private void miRetrieve_Click(object sender, System.EventArgs e)
		{
			InternalMailRetrieve retrieveHarness = new InternalMailRetrieve();
			retrieveHarness.Show();
		}

		private void menuItemStressTest_Click(object sender, System.EventArgs e)
		{
			StressTest stressTest = new StressTest();
			stressTest.Show();
		}

        private void button1_Click(object sender, EventArgs e)
        {
            InternalMailRetrieve iMailHarness = new InternalMailRetrieve();
            iMailHarness.Show();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            Messages messages = new Messages();
            messages.Show();
        }

	}
}
