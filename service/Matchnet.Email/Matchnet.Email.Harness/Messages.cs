﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Email.ValueObjects;
using Application = Spark.Common.MemberLevelTracking.Application;
using System.IO;
using System.Collections;

namespace Matchnet.Email.Harness
{
    public partial class Messages : Form
    {
        public Messages()
        {
            InitializeComponent();

            txtFromMemberID.Text = "100067359";
            txtToMemberID.Text = "100066873";
        }

        private void btnSelectImage_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFile.Text = openFileDialog1.FileName;
        }

        private void btnSaveMessageAttachment_Click(object sender, EventArgs e)
        {
            try
            {
                StringBuilder sbOutput = new StringBuilder();
                int fromMemberID = int.Parse(txtFromMemberID.Text);
                int toMemberID = int.Parse(txtToMemberID.Text);
                int communityID = int.Parse(txtCommunityID.Text);
                int siteID = int.Parse(txtSiteID.Text);
                Spark.Common.MemberLevelTracking.Application app = Application.FullSite;
                AttachmentType attachmentType = AttachmentType.IMPhoto;
                int messageID = int.MinValue;

                FileInfo info = new FileInfo(txtFile.Text);
                if (info.Exists)
                {
                    sbOutput.AppendLine("File found, starting save message attachment process.");
                    sbOutput.AppendLine(string.Format("File name: {0}, File size: {1}, File extension: {2}", info.Name,
                                                      info.Length, info.Extension));

                    string extension = info.Extension;
                    FileStream fstream = info.OpenRead();
                    byte[] fileData = new byte[fstream.Length];
                    fstream.Read(fileData, 0, Convert.ToInt32(fstream.Length));

                    MessageAttachmentSaveResult result = Email.ServiceAdapters.EmailMessageSA.Instance.SaveMessageAttachment(fromMemberID, toMemberID,
                                                                                        fileData, extension,
                                                                                        attachmentType, messageID,
                                                                                        communityID, siteID, app);

                    if (result != null)
                    {
                        if (result.Status == MessageAttachmentSaveStatus.Success)
                        {
                            sbOutput.AppendLine("Successfully processed message attachment.");
                            sbOutput.AppendLine("MessageAttachmentID: " + result.MessageAttachmentID.ToString());
                            sbOutput.AppendLine("MessageID: " + result.MessageID.ToString());
                            sbOutput.AppendLine("FileID: " + result.FileID.ToString());
                            sbOutput.AppendLine("PreviewCloudPath: " + result.PreviewCloudPath);
                            sbOutput.AppendLine("OriginalCloudPath: " + result.OriginalCloudPath);
                        }
                        else
                        {
                            sbOutput.AppendLine("Error occurred while processing.");
                            sbOutput.AppendLine(string.Format("Status: {0}, Error Type: {1}", result.Status,
                                                              result.Error));
                        }
                    }
                    else
                    {
                        sbOutput.AppendLine("Error, SaveMessageAttachment() returned back null object");
                    }
                }
                else
                {
                    sbOutput.AppendLine("File not found");
                }

                txtMessageAttachmentOutput.Text = sbOutput.ToString();

            }
            catch (Exception ex)
            {
                txtMessageAttachmentOutput.Text = ex.Message;
            }
        }

        private void btnGetIMHistory_Click(object sender, EventArgs e)
        {
            int memberID = Convert.ToInt32(txtIMHistoryMemberID.Text);
            int targetMemberID = Convert.ToInt32(txtIMHistoryTargetMemberID.Text);
            int groupID = Convert.ToInt32(txtIMHistoryGroupID.Text);
            bool ignoreCache = chkIMHistoryIgnoreCache.Checked;
            int startRow = Convert.ToInt32(txtIMHistoryStartRow.Text);
            int pageSize = Convert.ToInt32(txtIMHistoryPageSize.Text);

            var emailMessageCollection = Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveInstantMessages(memberID, targetMemberID, groupID, startRow, pageSize, ignoreCache);

            lvIMHistory.Items.Clear();
            foreach (EmailMessage message in emailMessageCollection)
            {
                var lvi = new ListViewItem(new string[] { 
                    message.MessageList.MemberID.ToString()
                    , message.MessageList.TargetMemberID.ToString()
                    , message.MessageList.MessageListID.ToString()
                    , message.Message.MessageID.ToString()
                    , message.StatusMask.ToString()
                    , message.InsertDate.ToString()
                    , message.MessageList.DirectionFlag.ToString()
                    , message.Subject
					, message.Content                   
                });
                lvIMHistory.Items.Add(lvi);

            }

            lvIMHistory.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int memberID = Convert.ToInt32(txtIMHistoryMemberID.Text);
            int groupID = Convert.ToInt32(txtIMHistoryGroupID.Text);
            bool ignoreCache = chkIMHistoryIgnoreCache.Checked;

            var messageMemberCollection = Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, groupID, true, ignoreCache);

            lvIMHistory.Items.Clear();
            foreach (MessageMember message in messageMemberCollection)
            {
                var lvi = new ListViewItem(new string[] { 
                    message.MemberID.ToString()
                    , message.TargetMemberID.ToString()
                    , message.MessageListID.ToString()
                    , message.MessageID.ToString()
                    , message.StatusMask.ToString()
                    , message.InsertDate.ToString()
                    , message.DirectionFlag.ToString()
                    , ""
                    , ""
                });
                lvIMHistory.Items.Add(lvi);

            }

            lvIMHistory.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int messageListID = Convert.ToInt32(txtMessageMemberMessageListID.Text);
            int memberID = Convert.ToInt32(txtMessageMemberMemberID.Text);
            int communityID = Convert.ToInt32(txtMessageMemberCommunityID.Text);

            var messageMemberCollection = Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, communityID, false, false);

            var messageMember = messageMemberCollection.GetMessageMemberByMessageListID(messageListID);
            
            txtMessageMemberOutput.Text = "";
            if (messageMember != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("MessageListID: " + messageMember.MessageListID.ToString());
                sb.AppendLine("MemberID: " + messageMember.MemberID.ToString());
                sb.AppendLine("TargetMemberID: " + messageMember.TargetMemberID.ToString());
                sb.AppendLine("Direction: " + messageMember.DirectionFlag.ToString());
                sb.AppendLine("StatusMask: " + messageMember.StatusMask.ToString());
                sb.AppendLine("MemberFolderID: " + messageMember.MemberFolderID.ToString());
                sb.AppendLine("MailTypeID: " + messageMember.MailTypeID.ToString());

                txtMessageMemberOutput.Text = sb.ToString();
            }
            else
            {
                txtMessageMemberOutput.Text = "MessageListID " + messageListID.ToString() + " not found";
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int messageListID = Convert.ToInt32(txtMessageMemberMessageListID.Text);
            int memberID = Convert.ToInt32(txtMessageMemberMemberID.Text);
            int communityID = Convert.ToInt32(txtMessageMemberCommunityID.Text);

            ArrayList list = new ArrayList();
            list.Add(messageListID);
            bool success = Email.ServiceAdapters.EmailMessageSA.Instance.MarkInstantMessageMemberRemoved(memberID, communityID, true, list, null);

            var messageMemberCollection = Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, communityID, false, false);

            var messageMember = messageMemberCollection.GetMessageMemberByMessageListID(messageListID);

            txtMessageMemberOutput.Text = "";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Marked remove successful: " + success.ToString());

            if (messageMember != null)
            {
                sb.AppendLine("MessageListID: " + messageMember.MessageListID.ToString());
                sb.AppendLine("MemberID: " + messageMember.MemberID.ToString());
                sb.AppendLine("TargetMemberID: " + messageMember.TargetMemberID.ToString());
                sb.AppendLine("Direction: " + messageMember.DirectionFlag.ToString());
                sb.AppendLine("StatusMask: " + messageMember.StatusMask.ToString());
                sb.AppendLine("MemberFolderID: " + messageMember.MemberFolderID.ToString());
                sb.AppendLine("MailTypeID: " + messageMember.MailTypeID.ToString());

                txtMessageMemberOutput.Text = sb.ToString();
            }
            else
            {
                txtMessageMemberOutput.Text = "MessageListID " + messageListID.ToString() + " not found";
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            int messageListID = Convert.ToInt32(txtMessageMemberMessageListID.Text);
            int memberID = Convert.ToInt32(txtMessageMemberMemberID.Text);
            int communityID = Convert.ToInt32(txtMessageMemberCommunityID.Text);

            ArrayList list = new ArrayList();
            list.Add(messageListID);
            bool success = Email.ServiceAdapters.EmailMessageSA.Instance.MarkInstantMessageMemberRemoved(memberID, communityID, false, list, null);

            var messageMemberCollection = Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, communityID, false, false);

            var messageMember = messageMemberCollection.GetMessageMemberByMessageListID(messageListID);

            txtMessageMemberOutput.Text = "";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Unmarked remove successful: " + success.ToString());

            if (messageMember != null)
            {
                sb.AppendLine("MessageListID: " + messageMember.MessageListID.ToString());
                sb.AppendLine("MemberID: " + messageMember.MemberID.ToString());
                sb.AppendLine("TargetMemberID: " + messageMember.TargetMemberID.ToString());
                sb.AppendLine("Direction: " + messageMember.DirectionFlag.ToString());
                sb.AppendLine("StatusMask: " + messageMember.StatusMask.ToString());
                sb.AppendLine("MemberFolderID: " + messageMember.MemberFolderID.ToString());
                sb.AppendLine("MailTypeID: " + messageMember.MailTypeID.ToString());

                txtMessageMemberOutput.Text = sb.ToString();
            }
            else
            {
                txtMessageMemberOutput.Text = "MessageListID " + messageListID.ToString() + " not found";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int memberID = Convert.ToInt32(txtIMHistoryMemberID.Text);
            int groupID = Convert.ToInt32(txtIMHistoryGroupID.Text);
            bool ignoreCache = chkIMHistoryIgnoreCache.Checked;

            var messageMemberCollection = Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveInstantMessageMembers(memberID, groupID, false, ignoreCache);

            lvIMHistory.Items.Clear();
            foreach (MessageMember message in messageMemberCollection)
            {
                var lvi = new ListViewItem(new string[] { 
                    message.MemberID.ToString()
                    , message.TargetMemberID.ToString()
                    , message.MessageListID.ToString()
                    , message.MessageID.ToString()
                    , message.StatusMask.ToString()
                    , message.InsertDate.ToString()
                    , message.DirectionFlag.ToString()
                    , ""
                    , ""
                });
                lvIMHistory.Items.Add(lvi);

            }

            lvIMHistory.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }


    }
}
