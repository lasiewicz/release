using System;
using System.IO;
using System.Text;
using System.Collections;

namespace Matchnet.Email.Harness
{
	class FileCacher : IEnumerable
	{
		private String[] _data;
		private String _fileName;
		private Int32 _count;

		public FileCacher(string fileName)
		{
			_fileName = fileName;
			loadData();
		}

		private void loadData()
		{
			StreamReader sr = new StreamReader(System.IO.File.Open(_fileName, System.IO.FileMode.Open));
			_data = sr.ReadToEnd().Replace("\r", "").Split('\n');
			_count = _data.Length;
		}

		public Int32 Count
		{
			get
			{
				return _count;
			}
		}

		public String this[Int32 index]
		{
			get
			{
				return _data[index];
			}
		}

		public String GetRandom()
		{
			Random random = new Random();
			Int32 index = random.Next(0, Count - 1);
			return this[index];
		}
		
		#region IEnumerable Members

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _data.GetEnumerator();
		}

		#endregion
	}
}
