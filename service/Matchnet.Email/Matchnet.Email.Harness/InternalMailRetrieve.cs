using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.Harness
{
	/// <summary>
	/// Summary description for InternalMailRetrieve.
	/// </summary>
	public class InternalMailRetrieve : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblFolders;
		private System.Windows.Forms.Button btnRefreshFolderList;
		private System.Windows.Forms.ListBox lstFolders;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.Label lblCommunityID;
		private System.Windows.Forms.ComboBox cmbCommunityID;
		private System.Windows.Forms.ListView lvMessages;
		private System.Windows.Forms.ColumnHeader chSubject;
		private System.Windows.Forms.ColumnHeader chMemberID;
		private System.Windows.Forms.ColumnHeader chDate;
		private System.Windows.Forms.ColumnHeader chSize;
        private ColumnHeader chStatusMask;
        private Button btnOneFreeReply;
        private Button btnUnOneFreeReply;
        private ColumnHeader chMessageListID;
        private Button button1;
        private Button btnReleaseFraudHold;
        private TextBox tbSenderMemberID;
        private Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InternalMailRetrieve()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lblFolders = new System.Windows.Forms.Label();
            this.btnRefreshFolderList = new System.Windows.Forms.Button();
            this.lstFolders = new System.Windows.Forms.ListBox();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.lblCommunityID = new System.Windows.Forms.Label();
            this.cmbCommunityID = new System.Windows.Forms.ComboBox();
            this.lvMessages = new System.Windows.Forms.ListView();
            this.chMessageListID = new System.Windows.Forms.ColumnHeader();
            this.chSubject = new System.Windows.Forms.ColumnHeader();
            this.chMemberID = new System.Windows.Forms.ColumnHeader();
            this.chDate = new System.Windows.Forms.ColumnHeader();
            this.chSize = new System.Windows.Forms.ColumnHeader();
            this.chStatusMask = new System.Windows.Forms.ColumnHeader();
            this.btnOneFreeReply = new System.Windows.Forms.Button();
            this.btnUnOneFreeReply = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnReleaseFraudHold = new System.Windows.Forms.Button();
            this.tbSenderMemberID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblFolders
            // 
            this.lblFolders.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFolders.Location = new System.Drawing.Point(8, 56);
            this.lblFolders.Name = "lblFolders";
            this.lblFolders.Size = new System.Drawing.Size(72, 23);
            this.lblFolders.TabIndex = 0;
            this.lblFolders.Text = "Folders:";
            // 
            // btnRefreshFolderList
            // 
            this.btnRefreshFolderList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefreshFolderList.Location = new System.Drawing.Point(80, 48);
            this.btnRefreshFolderList.Name = "btnRefreshFolderList";
            this.btnRefreshFolderList.Size = new System.Drawing.Size(75, 23);
            this.btnRefreshFolderList.TabIndex = 1;
            this.btnRefreshFolderList.Text = "Refresh";
            this.btnRefreshFolderList.Click += new System.EventHandler(this.btnRefreshFolderList_Click);
            // 
            // lstFolders
            // 
            this.lstFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lstFolders.Location = new System.Drawing.Point(8, 80);
            this.lstFolders.Name = "lstFolders";
            this.lstFolders.Size = new System.Drawing.Size(144, 392);
            this.lstFolders.TabIndex = 2;
            this.lstFolders.SelectedIndexChanged += new System.EventHandler(this.lstFolders_SelectedIndexChanged);
            // 
            // lblMemberID
            // 
            this.lblMemberID.Location = new System.Drawing.Point(8, 8);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(100, 23);
            this.lblMemberID.TabIndex = 3;
            this.lblMemberID.Text = "Member ID:";
            // 
            // txtMemberID
            // 
            this.txtMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMemberID.Location = new System.Drawing.Point(112, 8);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(136, 20);
            this.txtMemberID.TabIndex = 4;
            // 
            // lblCommunityID
            // 
            this.lblCommunityID.Location = new System.Drawing.Point(272, 8);
            this.lblCommunityID.Name = "lblCommunityID";
            this.lblCommunityID.Size = new System.Drawing.Size(100, 23);
            this.lblCommunityID.TabIndex = 5;
            this.lblCommunityID.Text = "CommunityID:";
            // 
            // cmbCommunityID
            // 
            this.cmbCommunityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCommunityID.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "10",
            "11",
            "12",
            "17"});
            this.cmbCommunityID.Location = new System.Drawing.Point(376, 8);
            this.cmbCommunityID.Name = "cmbCommunityID";
            this.cmbCommunityID.Size = new System.Drawing.Size(121, 21);
            this.cmbCommunityID.TabIndex = 6;
            // 
            // lvMessages
            // 
            this.lvMessages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvMessages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chMessageListID,
            this.chSubject,
            this.chMemberID,
            this.chDate,
            this.chSize,
            this.chStatusMask});
            this.lvMessages.FullRowSelect = true;
            this.lvMessages.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvMessages.Location = new System.Drawing.Point(181, 80);
            this.lvMessages.Name = "lvMessages";
            this.lvMessages.Size = new System.Drawing.Size(858, 390);
            this.lvMessages.TabIndex = 7;
            this.lvMessages.UseCompatibleStateImageBehavior = false;
            this.lvMessages.View = System.Windows.Forms.View.Details;
            // 
            // chMessageListID
            // 
            this.chMessageListID.Text = "MessageListID";
            this.chMessageListID.Width = 91;
            // 
            // chSubject
            // 
            this.chSubject.Text = "Subject";
            this.chSubject.Width = 150;
            // 
            // chMemberID
            // 
            this.chMemberID.Text = "Member";
            this.chMemberID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chMemberID.Width = 125;
            // 
            // chDate
            // 
            this.chDate.Text = "Date";
            this.chDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chDate.Width = 125;
            // 
            // chSize
            // 
            this.chSize.Text = "Size";
            this.chSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chStatusMask
            // 
            this.chStatusMask.Text = "StatusMask";
            this.chStatusMask.Width = 317;
            // 
            // btnOneFreeReply
            // 
            this.btnOneFreeReply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOneFreeReply.Location = new System.Drawing.Point(181, 48);
            this.btnOneFreeReply.Name = "btnOneFreeReply";
            this.btnOneFreeReply.Size = new System.Drawing.Size(94, 23);
            this.btnOneFreeReply.TabIndex = 8;
            this.btnOneFreeReply.Text = "OneFreeReply";
            this.btnOneFreeReply.Click += new System.EventHandler(this.btnOneFreeReply_Click);
            // 
            // btnUnOneFreeReply
            // 
            this.btnUnOneFreeReply.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUnOneFreeReply.Location = new System.Drawing.Point(295, 48);
            this.btnUnOneFreeReply.Name = "btnUnOneFreeReply";
            this.btnUnOneFreeReply.Size = new System.Drawing.Size(101, 23);
            this.btnUnOneFreeReply.TabIndex = 9;
            this.btnUnOneFreeReply.Text = "UnOneFreeReply";
            this.btnUnOneFreeReply.Click += new System.EventHandler(this.btnUnOneFreeReply_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(422, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Refresh";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnReleaseFraudHold
            // 
            this.btnReleaseFraudHold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReleaseFraudHold.Location = new System.Drawing.Point(842, 12);
            this.btnReleaseFraudHold.Name = "btnReleaseFraudHold";
            this.btnReleaseFraudHold.Size = new System.Drawing.Size(135, 23);
            this.btnReleaseFraudHold.TabIndex = 11;
            this.btnReleaseFraudHold.Text = "Release FraudHold";
            this.btnReleaseFraudHold.Click += new System.EventHandler(this.btnReleaseFraudHold_Click);
            // 
            // tbSenderMemberID
            // 
            this.tbSenderMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSenderMemberID.Location = new System.Drawing.Point(685, 15);
            this.tbSenderMemberID.Name = "tbSenderMemberID";
            this.tbSenderMemberID.Size = new System.Drawing.Size(136, 20);
            this.tbSenderMemberID.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(579, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 12;
            this.label1.Text = "Sender MemberID";
            // 
            // InternalMailRetrieve
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1065, 491);
            this.Controls.Add(this.tbSenderMemberID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnReleaseFraudHold);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnUnOneFreeReply);
            this.Controls.Add(this.btnOneFreeReply);
            this.Controls.Add(this.lvMessages);
            this.Controls.Add(this.cmbCommunityID);
            this.Controls.Add(this.lblCommunityID);
            this.Controls.Add(this.txtMemberID);
            this.Controls.Add(this.lblMemberID);
            this.Controls.Add(this.lstFolders);
            this.Controls.Add(this.btnRefreshFolderList);
            this.Controls.Add(this.lblFolders);
            this.Name = "InternalMailRetrieve";
            this.Text = "InternalMailRetrieve";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnRefreshFolderList_Click(object sender, System.EventArgs e)
		{
			// Refresh the folder listing for the selected Member/Community
			int memberID = Conversion.CInt(txtMemberID.Text);
			int communityID = Conversion.CInt(cmbCommunityID.SelectedItem.ToString());

			if (memberID == Constants.NULL_INT || communityID == Constants.NULL_INT)
			{
				MessageBox.Show("Please enter a valid MemberID and CommunityID.");
				return;
			}

			EmailFolderCollection folders = EmailFolderSA.Instance.RetrieveFolders(memberID, communityID, false);

			if (folders != null)
			{
				lstFolders.Items.Clear();
				lstFolders.ValueMember = "MemberFolderID";
				lstFolders.DisplayMember = "Description";
				foreach (EmailFolder folder in folders)
				{
					lstFolders.Items.Add(folder);
				}
			}
		}

		private void lstFolders_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int memberID = int.Parse(txtMemberID.Text);
			int communityID = int.Parse(cmbCommunityID.SelectedItem.ToString());
            var brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsByCommunity(communityID);
            
            // this doesn't have to be exact because it's for UserName purposes as long as we get the community right
            Matchnet.Content.ValueObjects.BrandConfig.Brand brandToPassIn = null;

            foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
            {
                brandToPassIn = brand;
                break;
            }

			if (lstFolders.SelectedItem != null && brandToPassIn != null)
			{
				// Selected folder
				EmailFolder folder = (EmailFolder)lstFolders.SelectedItem;

				// retrieve the messages for this folder
				ICollection messages = 
					EmailMessageSA.Instance.RetrieveMessages(memberID, communityID, folder.MemberFolderID, 1, 1000, "InsertDate desc", brandToPassIn);

				lvMessages.Items.Clear();	// flush the list

				if (messages != null)
				{
					ListViewItem lvi;
					foreach (EmailMessage message in messages)
					{
						lvi = new ListViewItem(new string[] { message.MemberMailID.ToString()
                                                          , message.Subject
														  , message.FromMemberID.ToString()
														  , message.InsertDate.ToString()
														  , message.DisplayKBytes.ToString()
                                                          , message.StatusMask.ToString()});
						lvi.Tag = message;
						lvMessages.Items.Add(lvi);
					}
				}
			}
		}

        private void btnOneFreeReply_Click(object sender, EventArgs e)
        {
            int messageListId = int.Parse(lvMessages.SelectedItems[0].Text);
            int memberId = int.Parse(txtMemberID.Text.Trim());
            int groupId = int.Parse(cmbCommunityID.Text);

            EmailMessageSA.Instance.MarkOneFreeReply(memberId, groupId, true, new ArrayList() { messageListId });
        }

        private void btnUnOneFreeReply_Click(object sender, EventArgs e)
        {
            int messageListId = int.Parse(lvMessages.SelectedItems[0].Text);
            int memberId = int.Parse(txtMemberID.Text.Trim());
            int groupId = int.Parse(cmbCommunityID.Text);

            EmailMessageSA.Instance.MarkOneFreeReply(memberId, groupId, false, new ArrayList() { messageListId });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int messageListId = int.Parse(lvMessages.SelectedItems[0].Text);
            int memberId = int.Parse(txtMemberID.Text.Trim());
            int groupId = int.Parse(cmbCommunityID.Text);
            var messageList = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessage(memberId, groupId, messageListId);

        }

        private void btnReleaseFraudHold_Click(object sender, EventArgs e)
        {
            int senderMemberId = int.Parse(tbSenderMemberID.Text.Trim());
            int groupId = int.Parse(cmbCommunityID.Text);

            EmailMessageSA.Instance.MarkOffFraudHoldSentByMember(senderMemberId, groupId, false);
        }

	}
}
