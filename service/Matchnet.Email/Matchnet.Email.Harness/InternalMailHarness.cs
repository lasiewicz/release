using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ServiceManagers;
using Matchnet.Email.ValueObjects;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Email.Harness
{
	/// <summary>
	/// Summary description for EmailHarness.
	/// </summary>
	public class InternalEmailHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ComboBox cbServiceLevel;

		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		#region class variables
		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		#endregion

		private System.Windows.Forms.TextBox tbFromMemberID;
		private System.Windows.Forms.TextBox tbToMemberID;
		private System.Windows.Forms.Label lblFromMemberID;
		private System.Windows.Forms.Label lblToMemberID;
		private System.Windows.Forms.TextBox tbSubject;
		private System.Windows.Forms.Label lbSubject;
		private System.Windows.Forms.Label lblContent;
		private System.Windows.Forms.Button btnSend;
		private System.Windows.Forms.CheckBox cbSaveInSent;
		private System.Windows.Forms.RichTextBox rtbContent;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbMailID;
		private System.Windows.Forms.Label lblCommunity;
		private System.Windows.Forms.ComboBox cbCommunityID;
		private System.Windows.Forms.Label lblLanguage;
		private System.Windows.Forms.ComboBox cbLanguageID;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InternalEmailHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}

		private void btnSend_Click(object sender, System.EventArgs e)
		{
			//EmailMessageSM messageServiceManager = null;
			//bool resultSuccess = false;

			try
			{
				MessageSave messageSave = new MessageSave(Conversion.CInt(tbFromMemberID.Text.Trim()),
															Conversion.CInt(tbToMemberID.Text.Trim()),
															Conversion.CInt(cbCommunityID.SelectedItem.ToString()),
															MailType.Email,
															tbSubject.Text.Trim(),
															rtbContent.Text.Trim());
				btnSend.Enabled = false;

				testLevel = (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						resultSuccess = EmailMessageBL.Instance.SendMessage(aMessage, cbSaveInSent.Checked, 0);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						resultSuccess = EmailMessageSA.Instance.SendMessage(aMessage, cbSaveInSent.Checked, 0);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						messageServiceManager = new EmailMessageSM();
//						resultSuccess = messageServiceManager.SendMessage(aMessage, cbSaveInSent.Checked, 0);
//						break;
//					default:
//						resultSuccess = EmailMessageBL.Instance.SendMessage(aMessage, cbSaveInSent.Checked, 0);
//						break;
//				}
				// For new SendEmailMessage().
				MessageSendResult messageResult = null;
				switch(testLevel)
				{
					//case SERVICE_LEVEL.BUSINESS_LOGIC:
					//	sendMessageResult = EmailMessageBL.Instance.SendEmailMessage(aMessage, cbSaveInSent.Checked, 0);
					//	break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						messageResult = EmailMessageSA.Instance.SendMessage(messageSave, cbSaveInSent.Checked, Constants.NULL_INT, false);
						break;
					//case SERVICE_LEVEL.SERVICE_MANAGER:
						//messageServiceManager = new EmailMessageSM();
						//sendMessageResult = messageServiceManager.SendEmailMessage(aMessage, cbSaveInSent.Checked, 0);
						//break;
					default:
						//messageResult = EmailMessageSA.Instance.SendEmailMessage(aMessage, cbSaveInSent.Checked, 0);
						break;
				}
				//MessageBox.Show(messageResult.Status.ToString());

				if(messageResult.Status == MessageSendStatus.Success) 
				{
					MessageBox.Show("Message sent!");
				} 
				else 
				{
					MessageBox.Show("Unable to send message!", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			catch(Exception anException) 
			{
				MessageBox.Show("Error Sending Message: " + anException.Message);
			}
			finally
			{
				btnSend.Enabled = true;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.tbFromMemberID = new System.Windows.Forms.TextBox();
			this.tbToMemberID = new System.Windows.Forms.TextBox();
			this.lblFromMemberID = new System.Windows.Forms.Label();
			this.lblToMemberID = new System.Windows.Forms.Label();
			this.tbSubject = new System.Windows.Forms.TextBox();
			this.lbSubject = new System.Windows.Forms.Label();
			this.lblContent = new System.Windows.Forms.Label();
			this.btnSend = new System.Windows.Forms.Button();
			this.cbSaveInSent = new System.Windows.Forms.CheckBox();
			this.rtbContent = new System.Windows.Forms.RichTextBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tbMailID = new System.Windows.Forms.TextBox();
			this.lblCommunity = new System.Windows.Forms.Label();
			this.cbCommunityID = new System.Windows.Forms.ComboBox();
			this.lblLanguage = new System.Windows.Forms.Label();
			this.cbLanguageID = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(12, 16);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(204, 21);
			this.cbServiceLevel.TabIndex = 0;
			// 
			// tbFromMemberID
			// 
			this.tbFromMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbFromMemberID.Location = new System.Drawing.Point(116, 52);
			this.tbFromMemberID.Name = "tbFromMemberID";
			this.tbFromMemberID.TabIndex = 1;
			this.tbFromMemberID.Text = "";
			// 
			// tbToMemberID
			// 
			this.tbToMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbToMemberID.Location = new System.Drawing.Point(116, 80);
			this.tbToMemberID.Name = "tbToMemberID";
			this.tbToMemberID.TabIndex = 3;
			this.tbToMemberID.Text = "";
			// 
			// lblFromMemberID
			// 
			this.lblFromMemberID.Location = new System.Drawing.Point(20, 60);
			this.lblFromMemberID.Name = "lblFromMemberID";
			this.lblFromMemberID.Size = new System.Drawing.Size(92, 12);
			this.lblFromMemberID.TabIndex = 3;
			this.lblFromMemberID.Text = "From Member ID";
			// 
			// lblToMemberID
			// 
			this.lblToMemberID.Location = new System.Drawing.Point(32, 84);
			this.lblToMemberID.Name = "lblToMemberID";
			this.lblToMemberID.Size = new System.Drawing.Size(76, 14);
			this.lblToMemberID.TabIndex = 4;
			this.lblToMemberID.Text = "To Member ID";
			// 
			// tbSubject
			// 
			this.tbSubject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbSubject.Location = new System.Drawing.Point(116, 108);
			this.tbSubject.Name = "tbSubject";
			this.tbSubject.TabIndex = 5;
			this.tbSubject.Text = "";
			// 
			// lbSubject
			// 
			this.lbSubject.Location = new System.Drawing.Point(64, 112);
			this.lbSubject.Name = "lbSubject";
			this.lbSubject.Size = new System.Drawing.Size(43, 14);
			this.lbSubject.TabIndex = 6;
			this.lbSubject.Text = "Subject";
			// 
			// lblContent
			// 
			this.lblContent.Location = new System.Drawing.Point(64, 136);
			this.lblContent.Name = "lblContent";
			this.lblContent.Size = new System.Drawing.Size(44, 14);
			this.lblContent.TabIndex = 8;
			this.lblContent.Text = "Content";
			// 
			// btnSend
			// 
			this.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSend.Location = new System.Drawing.Point(116, 240);
			this.btnSend.Name = "btnSend";
			this.btnSend.Size = new System.Drawing.Size(52, 20);
			this.btnSend.TabIndex = 8;
			this.btnSend.Text = "Send";
			this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
			// 
			// cbSaveInSent
			// 
			this.cbSaveInSent.Location = new System.Drawing.Point(224, 112);
			this.cbSaveInSent.Name = "cbSaveInSent";
			this.cbSaveInSent.Size = new System.Drawing.Size(92, 16);
			this.cbSaveInSent.TabIndex = 6;
			this.cbSaveInSent.Text = "Save in Sent";
			// 
			// rtbContent
			// 
			this.rtbContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.rtbContent.Location = new System.Drawing.Point(116, 136);
			this.rtbContent.Name = "rtbContent";
			this.rtbContent.Size = new System.Drawing.Size(328, 92);
			this.rtbContent.TabIndex = 7;
			this.rtbContent.Text = "";
			// 
			// btnSave
			// 
			this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSave.Location = new System.Drawing.Point(180, 240);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(52, 20);
			this.btnSave.TabIndex = 9;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(268, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(44, 12);
			this.label1.TabIndex = 14;
			this.label1.Text = "Mail ID";
			// 
			// tbMailID
			// 
			this.tbMailID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbMailID.Location = new System.Drawing.Point(320, 16);
			this.tbMailID.Name = "tbMailID";
			this.tbMailID.ReadOnly = true;
			this.tbMailID.TabIndex = 13;
			this.tbMailID.Text = "";
			// 
			// lblCommunity
			// 
			this.lblCommunity.Location = new System.Drawing.Point(236, 56);
			this.lblCommunity.Name = "lblCommunity";
			this.lblCommunity.Size = new System.Drawing.Size(100, 20);
			this.lblCommunity.TabIndex = 26;
			this.lblCommunity.Text = "CommunityID:";
			// 
			// cbCommunityID
			// 
			this.cbCommunityID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbCommunityID.Items.AddRange(new object[] {
															   "1",
															   "2",
															   "10",
															   "11",
															   "17"});
			this.cbCommunityID.Location = new System.Drawing.Point(340, 52);
			this.cbCommunityID.Name = "cbCommunityID";
			this.cbCommunityID.Size = new System.Drawing.Size(121, 21);
			this.cbCommunityID.TabIndex = 2;
			// 
			// lblLanguage
			// 
			this.lblLanguage.Location = new System.Drawing.Point(236, 84);
			this.lblLanguage.Name = "lblLanguage";
			this.lblLanguage.TabIndex = 28;
			this.lblLanguage.Text = "Language ID:";
			// 
			// cbLanguageID
			// 
			this.cbLanguageID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbLanguageID.Items.AddRange(new object[] {
															  "2",
															  "8",
															  "32",
															  "262144"});
			this.cbLanguageID.Location = new System.Drawing.Point(340, 80);
			this.cbLanguageID.Name = "cbLanguageID";
			this.cbLanguageID.Size = new System.Drawing.Size(121, 21);
			this.cbLanguageID.TabIndex = 4;
			// 
			// InternalEmailHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(520, 477);
			this.Controls.Add(this.cbLanguageID);
			this.Controls.Add(this.lblLanguage);
			this.Controls.Add(this.cbCommunityID);
			this.Controls.Add(this.lblCommunity);
			this.Controls.Add(this.tbMailID);
			this.Controls.Add(this.tbSubject);
			this.Controls.Add(this.tbToMemberID);
			this.Controls.Add(this.tbFromMemberID);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.rtbContent);
			this.Controls.Add(this.cbSaveInSent);
			this.Controls.Add(this.btnSend);
			this.Controls.Add(this.lblContent);
			this.Controls.Add(this.lbSubject);
			this.Controls.Add(this.lblToMemberID);
			this.Controls.Add(this.lblFromMemberID);
			this.Controls.Add(this.cbServiceLevel);
			this.Name = "InternalEmailHarness";
			this.Text = "EmailHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//EmailMessage aMessage = null;//new EmailMessage();
			//EmailMessageSM messageServiceManager = null;
			bool resultSuccess = false;

			try
			{
				btnSave.Enabled = false;
				/*if(tbMailID.Text.Trim().Length > 0) 
				{
					aMessage.MailID = int.Parse(tbMailID.Text.Trim());
				}
				aMessage.Content = rtbContent.Text.Trim();
				aMessage.Subject = tbSubject.Text.Trim();
				aMessage.FromMemberID = int.Parse(tbFromMemberID.Text.Trim());
				aMessage.ToMemberID = int.Parse(tbToMemberID.Text.Trim());
				aMessage.GroupID = int.Parse(cbCommunityID.SelectedItem.ToString());*/
				//aMessage.LanguageID = int.Parse(cbLanguageID.SelectedItem.ToString());

				testLevel = (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						//resultSuccess = EmailMessageBL.Instance.SaveMessage(Matchnet.Constants.NULL_STRING, aMessage).Status == EmailMessageStatus.Success;
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						//resultSuccess = EmailMessageSA.Instance.SaveMessage(aMessage);
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						//messageServiceManager = new EmailMessageSM();
						//resultSuccess = messageServiceManager.SaveMessage(Matchnet.Constants.NULL_STRING, aMessage).Status == EmailMessageStatus.Success;
						break;
					default:
						//resultSuccess = EmailMessageBL.Instance.SaveMessage(Matchnet.Constants.NULL_STRING, aMessage).Status == EmailMessageStatus.Success;
						break;
				}

				if(resultSuccess) 
				{
					MessageBox.Show("Message saved.");
				} 
				else 
				{
					MessageBox.Show("Message 'SAVE' UNSuccessful");
				}
			}
			catch(Exception anException) 
			{
				MessageBox.Show("Error Saving Message: " + anException.Message);
			}
			finally
			{
				btnSave.Enabled = true;
			}
		}

	}
}
