using System;
using System.Text;

namespace Matchnet.Email.Harness
{
	class Data
	{
		public static Data Instance = new Data();

		private FileCacher _memberIDs;
		//private string _memberFileName = @"C:\Documents and Settings\tbankston\Desktop\memberids.txt"; //prod
		private string _memberFileName = @"C:\DevMemberIDs.txt"; //dev

        //private string _photoFileName = @"C:\Documents and Settings\tbankston\Desktop\DevPhotoMemberIDs.txt";
        //private FileCacher _photoMemberIDs;

		//private FileCacher _zipCodes;
		//private string _zipCodeFileName = @"C:\Documents and Settings\tbankston\Desktop\ZipCodes.txt";

		private Data()
		{
			_memberIDs = new FileCacher(_memberFileName);
			//_zipCodes = new FileCacher(_zipCodeFileName);
            //_photoMemberIDs = new FileCacher(_photoFileName);
		}

		public int GetNextMemberID()
		{
			return Convert.ToInt32(_memberIDs.GetRandom());
		}

        /*public int GetPhotoMemberID()
        {
            return Convert.ToInt32(_photoMemberIDs.GetRandom());
        }


		public string GetNextZipCode()
		{
			return _zipCodes.GetRandom();
		}*/
	}
}
