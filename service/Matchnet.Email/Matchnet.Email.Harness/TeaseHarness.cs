using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ServiceManagers;
using Matchnet.Email.ValueObjects;

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Matchnet.Email.Harness
{
	/// <summary>
	/// Summary description for TeaseHarness.
	/// </summary>
	public class TeaseHarness : System.Windows.Forms.Form
	{

		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		#region class variables
		private System.Windows.Forms.Button btnRetrieveCategories;
		private System.Windows.Forms.ListBox lbTeaseCategories;
		private System.Windows.Forms.Label lblTeaseCategories;
		private System.Windows.Forms.Label lblBrandID;
		private System.Windows.Forms.TextBox tbBrandID;
		private System.Windows.Forms.Label lblTeases;
		private System.Windows.Forms.ListBox lbTeases;
		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		private System.Windows.Forms.Label lblServiceLevel;
		private System.Windows.Forms.ComboBox cbServiceLevel;
		#endregion
		private System.Windows.Forms.Label lblCacheKey;
		private System.Windows.Forms.Label lblCacheTTL;
		private System.Windows.Forms.TextBox tbCacheKey;
		private System.Windows.Forms.TextBox tbCacheTTL;
		private System.Windows.Forms.TextBox tbIsCached;
		private System.Windows.Forms.Label lblIsCached;


		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TeaseHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Establish the test level here
			PopulateServiceLevel();
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRetrieveCategories = new System.Windows.Forms.Button();
			this.lbTeaseCategories = new System.Windows.Forms.ListBox();
			this.lblTeaseCategories = new System.Windows.Forms.Label();
			this.lblBrandID = new System.Windows.Forms.Label();
			this.tbBrandID = new System.Windows.Forms.TextBox();
			this.lblTeases = new System.Windows.Forms.Label();
			this.lbTeases = new System.Windows.Forms.ListBox();
			this.lblServiceLevel = new System.Windows.Forms.Label();
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.lblCacheKey = new System.Windows.Forms.Label();
			this.lblCacheTTL = new System.Windows.Forms.Label();
			this.tbCacheKey = new System.Windows.Forms.TextBox();
			this.tbCacheTTL = new System.Windows.Forms.TextBox();
			this.tbIsCached = new System.Windows.Forms.TextBox();
			this.lblIsCached = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnRetrieveCategories
			// 
			this.btnRetrieveCategories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRetrieveCategories.Location = new System.Drawing.Point(16, 200);
			this.btnRetrieveCategories.Name = "btnRetrieveCategories";
			this.btnRetrieveCategories.Size = new System.Drawing.Size(128, 23);
			this.btnRetrieveCategories.TabIndex = 0;
			this.btnRetrieveCategories.Text = "Get Tease Categories";
			this.btnRetrieveCategories.Click += new System.EventHandler(this.btnRetrieveCategories_Click);
			// 
			// lbTeaseCategories
			// 
			this.lbTeaseCategories.Location = new System.Drawing.Point(176, 40);
			this.lbTeaseCategories.Name = "lbTeaseCategories";
			this.lbTeaseCategories.Size = new System.Drawing.Size(176, 186);
			this.lbTeaseCategories.TabIndex = 1;
			this.lbTeaseCategories.SelectedIndexChanged += new System.EventHandler(this.lbTeaseCategories_SelectedIndexChanged);
			// 
			// lblTeaseCategories
			// 
			this.lblTeaseCategories.Location = new System.Drawing.Point(176, 20);
			this.lblTeaseCategories.Name = "lblTeaseCategories";
			this.lblTeaseCategories.Size = new System.Drawing.Size(100, 16);
			this.lblTeaseCategories.TabIndex = 2;
			this.lblTeaseCategories.Text = "Tease Categories";
			// 
			// lblBrandID
			// 
			this.lblBrandID.Location = new System.Drawing.Point(16, 20);
			this.lblBrandID.Name = "lblBrandID";
			this.lblBrandID.Size = new System.Drawing.Size(56, 16);
			this.lblBrandID.TabIndex = 3;
			this.lblBrandID.Text = "GroupID";
			// 
			// tbBrandID
			// 
			this.tbBrandID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbBrandID.Location = new System.Drawing.Point(16, 40);
			this.tbBrandID.Name = "tbBrandID";
			this.tbBrandID.Size = new System.Drawing.Size(136, 20);
			this.tbBrandID.TabIndex = 5;
			this.tbBrandID.Text = "10";
			// 
			// lblTeases
			// 
			this.lblTeases.Location = new System.Drawing.Point(364, 20);
			this.lblTeases.Name = "lblTeases";
			this.lblTeases.Size = new System.Drawing.Size(100, 16);
			this.lblTeases.TabIndex = 7;
			this.lblTeases.Text = "Teases";
			// 
			// lbTeases
			// 
			this.lbTeases.Location = new System.Drawing.Point(364, 40);
			this.lbTeases.Name = "lbTeases";
			this.lbTeases.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.lbTeases.Size = new System.Drawing.Size(448, 186);
			this.lbTeases.TabIndex = 8;
			// 
			// lblServiceLevel
			// 
			this.lblServiceLevel.Location = new System.Drawing.Point(16, 128);
			this.lblServiceLevel.Name = "lblServiceLevel";
			this.lblServiceLevel.Size = new System.Drawing.Size(76, 16);
			this.lblServiceLevel.TabIndex = 9;
			this.lblServiceLevel.Text = "Service Level";
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(16, 148);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(136, 21);
			this.cbServiceLevel.TabIndex = 10;
			// 
			// lblCacheKey
			// 
			this.lblCacheKey.Location = new System.Drawing.Point(292, 244);
			this.lblCacheKey.Name = "lblCacheKey";
			this.lblCacheKey.Size = new System.Drawing.Size(60, 16);
			this.lblCacheKey.TabIndex = 11;
			this.lblCacheKey.Text = "Cache Key";
			// 
			// lblCacheTTL
			// 
			this.lblCacheTTL.Location = new System.Drawing.Point(292, 272);
			this.lblCacheTTL.Name = "lblCacheTTL";
			this.lblCacheTTL.Size = new System.Drawing.Size(60, 16);
			this.lblCacheTTL.TabIndex = 12;
			this.lblCacheTTL.Text = "Cache TTL";
			// 
			// tbCacheKey
			// 
			this.tbCacheKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbCacheKey.Location = new System.Drawing.Point(364, 240);
			this.tbCacheKey.Name = "tbCacheKey";
			this.tbCacheKey.ReadOnly = true;
			this.tbCacheKey.Size = new System.Drawing.Size(448, 20);
			this.tbCacheKey.TabIndex = 13;
			this.tbCacheKey.Text = "";
			// 
			// tbCacheTTL
			// 
			this.tbCacheTTL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbCacheTTL.Location = new System.Drawing.Point(364, 268);
			this.tbCacheTTL.Name = "tbCacheTTL";
			this.tbCacheTTL.ReadOnly = true;
			this.tbCacheTTL.Size = new System.Drawing.Size(164, 20);
			this.tbCacheTTL.TabIndex = 14;
			this.tbCacheTTL.Text = "";
			// 
			// tbIsCached
			// 
			this.tbIsCached.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbIsCached.Location = new System.Drawing.Point(364, 296);
			this.tbIsCached.Name = "tbIsCached";
			this.tbIsCached.ReadOnly = true;
			this.tbIsCached.Size = new System.Drawing.Size(164, 20);
			this.tbIsCached.TabIndex = 16;
			this.tbIsCached.Text = "";
			// 
			// lblIsCached
			// 
			this.lblIsCached.Location = new System.Drawing.Point(308, 300);
			this.lblIsCached.Name = "lblIsCached";
			this.lblIsCached.Size = new System.Drawing.Size(44, 16);
			this.lblIsCached.TabIndex = 15;
			this.lblIsCached.Text = "Cached";
			// 
			// TeaseHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(846, 335);
			this.Controls.Add(this.tbIsCached);
			this.Controls.Add(this.lblIsCached);
			this.Controls.Add(this.tbCacheTTL);
			this.Controls.Add(this.tbCacheKey);
			this.Controls.Add(this.lblCacheTTL);
			this.Controls.Add(this.lblCacheKey);
			this.Controls.Add(this.cbServiceLevel);
			this.Controls.Add(this.lblServiceLevel);
			this.Controls.Add(this.lbTeases);
			this.Controls.Add(this.lblTeases);
			this.Controls.Add(this.tbBrandID);
			this.Controls.Add(this.lblBrandID);
			this.Controls.Add(this.lblTeaseCategories);
			this.Controls.Add(this.lbTeaseCategories);
			this.Controls.Add(this.btnRetrieveCategories);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "TeaseHarness";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "TeaseHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void displayTeaseCategories(TeaseCategoryCollection categoryResult) 
		{
			tbCacheKey.Text = "";
			tbCacheTTL.Text = "";
			tbIsCached.Text = "";
			tbCacheKey.Text = categoryResult.GetCacheKey();
			tbCacheTTL.Text = categoryResult.CacheTTLSeconds.ToString();
			tbIsCached.Text = categoryResult.CachedResult.ToString();

			lbTeaseCategories.Items.Clear();
			if(categoryResult == null || categoryResult.Count < 1) 
			{
				lbTeaseCategories.Items.Add("No Categories Found");
			} 
			else 
			{
				lbTeaseCategories.DisplayMember = "ResourceKey";
				foreach(TeaseCategory currentCategory in categoryResult) 
				{
					lbTeaseCategories.Items.Add(currentCategory);
				}
			}
		}

		private void displayTeases(TeaseCollection teasesResult) 
		{
			lbTeases.Items.Clear();
			if(teasesResult == null || teasesResult.Count < 1) 
			{
				lbTeases.Items.Add("No Teases Found");
			} 
			else 
			{
				lbTeases.DisplayMember = "ResourceKey";
				foreach(Tease currentTease in teasesResult) 
				{
					lbTeases.Items.Add(currentTease);
				}
			}
		}

		private void btnRetrieveCategories_Click(object sender, System.EventArgs e)
		{
			TeaseCategoryCollection result = null;
			try 
			{
				testLevel = (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
				btnRetrieveCategories.Enabled = false;
				lbTeaseCategories.Items.Clear();
				lbTeases.Items.Clear();

				if(tbBrandID.Text.Trim().Length > 0) 
				{
					switch(testLevel) 
					{
						case SERVICE_LEVEL.BUSINESS_LOGIC:
							result = TeaseBL.Instance.GetTeaseCategoryCollection(int.Parse(tbBrandID.Text.Trim()));
							break;
						case SERVICE_LEVEL.SERVICE_MANAGER:
							TeaseSM serviceManager = new TeaseSM();
							result = serviceManager.GetTeaseCategoryCollection(int.Parse(tbBrandID.Text.Trim()));
							break;
						case SERVICE_LEVEL.SERVICE_ADAPTER:
							result = TeaseSA.Instance.GetTeaseCategoryCollection(int.Parse(tbBrandID.Text.Trim()));
							break;
						default:
							result = TeaseBL.Instance.GetTeaseCategoryCollection(int.Parse(tbBrandID.Text.Trim()));
							break;
					} 
					displayTeaseCategories(result);
				} 
				else 
				{
					MessageBox.Show("Please specify Brand and Language IDs");
				}
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Please insert numeric Brand and Language IDs");
			}
			catch(Exception anException)
			{
				MessageBox.Show("Error Encountered: " + anException.StackTrace);
			}
			finally
			{
				btnRetrieveCategories.Enabled = true;
			}
		}

		private void lbTeaseCategories_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			TeaseCollection foundTeases = null;
			TeaseCategory selectedCategory = null;

			try 
			{
				selectedCategory = (TeaseCategory)lbTeaseCategories.SelectedItem;
				if(selectedCategory != null) 
				{
					foundTeases = TeaseBL.Instance.GetTeaseCollection(selectedCategory.TeaseCategoryID,
					   int.Parse(tbBrandID.Text.Trim()));
					displayTeases(foundTeases);
				}
			} 
			catch(Exception) 
			{
				// this shouldn't happen with a controlled value
			}
		}
	}
}
