﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ValueObjects;
using NUnit.Framework;

namespace Matchnet.Email.Tests
{
    [TestFixture]
    public class MessageAttachmentTests
    {
        /// <summary>
        /// Test parsing messages to grab photo attachmentID embedded within the message
        /// </summary>
        [Test]
        public void GetMessageAttachmentIDs_Photos()
        {
            List<MessageAttachmentMetadata> attachmentMetadatas = new List<MessageAttachmentMetadata>();

            //message with no attachment metadata
            string attachments0 = "some message";
            attachmentMetadatas = EmailMessageBL.Instance.GetEmbeddedMessageAttachmentData(attachments0);
            Assert.AreEqual(0, attachmentMetadatas.Count);

            //message with 1 photo attachment metadata
            string attachments1 = "message with photo <sparktag type=\"photo\" alt=\"some alt\" src=\"http://www.google.com/some.jpg\" id=\"111\" data-originalExt=\"jpg\"/>";
            attachmentMetadatas = EmailMessageBL.Instance.GetEmbeddedMessageAttachmentData(attachments1);
            Assert.AreEqual(1, attachmentMetadatas.Count);

            //message with 2 photo attachment metadata
            string attachments2 = "message with photo <sparktag type=\"photo\" alt=\"some alt\" src=\"http://www.google.com/some.jpg\" id=\"111\" data-originalExt=\"jpg\"/>"
                                  +
                                  "more text and another photo <sparktag type=\"photo\" alt=\"some alt\" src=\"http://www.google.com/some.jpg\" id=\"222\" data-originalExt=\"jpg\"/>";
            attachmentMetadatas = EmailMessageBL.Instance.GetEmbeddedMessageAttachmentData(attachments2);
            Assert.AreEqual(2, attachmentMetadatas.Count);

            //encoded message with 1 photo attachment metadata
            string attachments1Encoded = "encoded message with photo &lt;sparktag type=\"photo\" alt=\"some alt\" src=\"http://www.google.com/some.jpg\" id=\"111\" data-originalExt=\"jpg\"/&gt;";
            attachmentMetadatas = EmailMessageBL.Instance.GetEmbeddedMessageAttachmentData(attachments1Encoded);
            Assert.AreEqual(1, attachmentMetadatas.Count);

        }
    }
}
