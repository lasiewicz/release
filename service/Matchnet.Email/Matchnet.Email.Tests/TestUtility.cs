﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Email.Tests
{
    public class TestUtility
    {
        public Brand GetJDateBrand()
        {
            Community community = new Community(3, "JDate");
            Site site = new Site(103, community, "JDate.com", 1, 2, "en-US", 223, 7, 1, "/lib/css/jd_jc.css", 1255,
                                 DirectionType.ltr, -8, "www", "https://www.jdate.com", 3);
            Brand brand = new Brand(1003, site, "Jdate.com", 9, "(877) 453-3861", 25, 40, 50);

            return brand;
        }
    }
}
