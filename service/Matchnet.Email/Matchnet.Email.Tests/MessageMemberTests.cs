﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.RemotingClient;
using Matchnet;

namespace Matchnet.Email.Tests
{
    [TestFixture]
    public class MessageMemberTests
    {
        [Test]
        public void TestRetrieveMessageMember()
        {
            Community community = new Community(3, "JDate");
            Site site = new Site(103, community, "JDate.com", 1, 2, "en-US", 223, 7, 1, "/lib/css/jd_jc.css", 1255,
                                 DirectionType.ltr, -8, "www", "https://www.jdate.com", 3);
            Brand brand = new Brand(1003, site, "Jdate.com", 9, "(877) 453-3861", 25, 40, 50);

            string orderBy = "insertDate desc";

            int memberID = 100171610;

            int memberFolderID = 0;
            MessageMemberCollection messageMemberCollection = EmailMessageSA.Instance.RetrieveMessageMembers(memberID,
                                                                                                             community
                                                                                                                 .CommunityID,
                                                                                                             memberFolderID,
                                                                                                             1, 10,
                                                                                                             orderBy,
                                                                                                             brand);
        }

        [Test]
        public void TestGetUnreadMessageCounts()
        {
            TestUtility utility = new TestUtility();
            int memberId = 114402580;
            Dictionary<string, Dictionary<MailType, int>> unreadMessageCounts = EmailMessageSA.Instance.GetUnreadMessageCounts(memberId, utility.GetJDateBrand());
            Assert.NotNull(unreadMessageCounts);
        }
    }
}
