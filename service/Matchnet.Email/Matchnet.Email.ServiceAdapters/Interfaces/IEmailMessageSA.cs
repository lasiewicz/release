using System;
using System.Collections;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.ServiceAdapters
{
    public interface IEmailMessageSA
    {
        string CallerAppName { get; set; }

        /// <summary>
        /// Use this method to determine the correct memberID for the Ignore list.
        /// The pMemberID parameter is compared to the ToMemberID and FromMemberID attributes on EmailMessage
        /// to determine if the member is the sender or the recipient.  The opposite memberID is returned.
        /// </summary>
        /// <param name="pEmailMessage"></param>
        /// <param name="pMemberID"></param>
        /// <returns></returns>
        int GetMemberIDToBlock(EmailMessage pEmailMessage, int pMemberID);

        /// <summary>
        /// Use this method to save an email message to a sender's Draft mailbox.
        /// </summary>
        /// <param name="messageSave"></param>
        /// <returns></returns>
        bool SaveMessage(MessageSave messageSave);

        bool SaveMessage(MessageSave messageSave, out int memberMailID);

        /// <summary>
        /// Sends a message.
        /// </summary>
        /// <param name="messageSave">The email message object.</param>
        /// <param name="saveInSent">A boolean value indicating if a copy of the email message should be saved to the sender's sent folder.</param>
        /// <param name="replyMessageListID">(Optional, use Matchnet.Constants.NULL_INT if not applicable.) If replying to an email, this parameter should represent the MessageListID for the message being replied to.</param>
        /// <param name="respectQuota"></param>
        /// <returns></returns>
        MessageSendResult SendMessage(MessageSave messageSave, bool saveInSent, int replyMessageListID, bool respectQuota);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListID"></param>
        /// <returns></returns>
        EmailMessage RetrieveMessage(Int32 memberID,
                                                     Int32 groupID,
                                                     Int32 messageListID);

        EmailMessage RetrieveMessage(Int32 memberID,
                                            Int32 groupID,
                                            Int32 messageListID,
                                            bool ignoreSACache);

        /// <summary>
        /// Use this method to retrieve the specified EmailMessage.
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="memberID"></param>
        /// <param name="messageListID"></param>
        /// <param name="orderBy"></param>
        /// <param name="messageDirection"></param>
        /// <param name="updateReadFlag"></param>
        /// <returns></returns>
        EmailMessage RetrieveMessage(Int32 groupID,
                                                     Int32 memberID,
                                                     Int32 messageListID,
                                                     string orderBy,
                                                     Direction messageDirection,
                                                     bool updateReadFlag,
                                                     Brand brand);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        ICollection RetrieveMessages(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand);

        /// <summary>
        /// This is a modified version of RetrieveMessages to support merging of original system folders and vip folders.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="brand"></param>
        /// <param name="orderBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="startRow"></param>
        /// <returns></returns>
        ICollection RetrieveMessagesFromLogicalFolder(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <returns></returns>
        EmailMessageCollection RetrieveMessages(Int32 memberID, Int32 communityID, Int32 memberFolderID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="siteID"></param>
        /// <returns></returns>
        EmailMessageCollection RetrieveMessages(Int32 memberID, Int32 communityID, Int32 memberFolderID, Int32 siteID);

        /// <summary>
        /// Use this method to 'mark' messages as Read/Un-read for a member.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="read"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        bool MarkRead(Int32 memberID,
                                      Int32 groupID,
                                      bool read,
                                      ArrayList messageListIDs);

        void MarkOffFraudHoldSentByMember(int memberID,
                                                          int groupID,
                                                          int siteID);

        string MarkOffFraudHoldSentByMember(int memberID,
                                                            int groupID,
                                                            bool returnTrace);

        void MarkDeletedSentByMember(int memberID,
                                                     int groupID,
                                                     bool isDelete);

        /// <summary>
        /// Performs soft delete where DB row is updated only but the messagelist disappears from all caches
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="isDeleted"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        bool MarkDeleted(Int32 memberID,
                                         Int32 groupID,
                                         bool isDeleted,
                                         ArrayList messageListIDs,
                                         MessageListCollection messageListColFromDB);

        bool MarkOffFraudHold(Int32 memberID,
                                              Int32 groupID,
                                              ArrayList messageListIDs,
                                              MessageListCollection messageListColFromDB,
                                              StringBuilder sb);

        /// <summary>
        /// To change the OneFreeReply status of the message's StatusMask value, use this method.
        /// </summary>
        /// <param name="pMemberID"></param>
        /// <param name="pGroupId"></param>
        /// <param name="pOneFreeReply"></param>
        /// <param name="pMessageListIds"></param>
        /// <returns></returns>
        bool MarkOneFreeReply(int pMemberID,
                                              int pGroupId,
                                              bool pOneFreeReply,
                                              ArrayList pMessageListIds);

        bool MarkOneFreeReply(int pMemberID,
                              int pGroupId,
                              bool pOneFreeReply,
                              ArrayList pMessageListIds,
                              bool ignoreSACache);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListIDs"></param>
        /// <param name="memberFolderID"></param>
        /// <returns></returns>
        bool MoveToFolder(Int32 memberID, Int32 groupID, ArrayList messageListIDs, Int32 memberFolderID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        bool DeleteMessage(Int32 memberID, Int32 groupID, ArrayList messageListIDs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="memberFolderID"></param>
        /// <returns></returns>
        bool EmptyFolder(Int32 memberID, Int32 groupID, Int32 memberFolderID);

        bool MarkDraftSaved(Int32 memberID,
                                            Int32 groupID,
                                            bool saved,
                                            Int32 messageListID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="messageStatus">MessageStatus to look for</param>
        /// <param name="inverseCount">Want inverse count of the MessageStatus you passed in?</param>
        /// <returns></returns>
        int GetMessageCountByStatus(int memberID, Brand brand, int memberFolderID, MessageStatus messageStatus, bool inverseCount, bool includeFlirtAndEcards);

        ArrayList RetrieveMessagesByStatus(int memberID, Brand brand, int memberFolderID, MessageStatus messagestatus, bool messagestatusequal, string orderby, int numbertoreturn);
        Message RetrieveMessageByMessageID(Int32 memberID, Int32 messageID);

        bool CanMoveEmailsToFolder(Int32 memberID,
                                                   Int32 groupID,
                                                   ArrayList messageListIDs,
                                                   Int32 memberFolderID);

        /// <summary>
        /// Saves an attachment for a message; messageID may be null if no messageID is known yet
        /// </summary>
        MessageAttachmentSaveResult SaveMessageAttachment(int fromMemberID, int toMemberID, byte[] fileBytes, string extension,
                                                          AttachmentType attachmentType, int messageID, int communityID,
                                                          int siteID, Spark.Common.MemberLevelTracking.Application app);
    }
}