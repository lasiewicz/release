using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.ServiceAdapters.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEmailBlockedSender : IEmailMessageSA
    {
        /// <summary>
        /// Check to see if recipient member has blocked sender. If blocked, send email to sender indicating unsuccessful send.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="messageHeader"></param>
        /// <param name="messageBody"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="senderMemberId"></param>
        /// <param name="siteId"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        MessageSendResult CheckAndSendMessageForBlockedSenderInRecipientList(int groupId, string messageHeader, string messageBody, int recipientMemberId, int senderMemberId, int siteId, int communityId);
    }
}