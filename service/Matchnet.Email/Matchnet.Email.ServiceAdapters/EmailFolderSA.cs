using System;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

using Matchnet;
using Matchnet.Caching;
using Matchnet.RemotingClient;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration;
using Matchnet.Exceptions;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceDefinitions;

namespace Matchnet.Email.ServiceAdapters
{
	/// <summary>
	/// Summary description for EmailFolderSA.
	/// </summary>
	public class EmailFolderSA : SABase
	{
		#region Private Members
		private const string SERVICE_MANAGER_NAME = "EmailFolderSM";
		private const string SERVICE_CONSTANT = "EMAIL_SVC";
		private Cache _cache = Cache.Instance;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public static readonly EmailFolderSA Instance = new EmailFolderSA();

		private EmailFolderSA()
		{

		}
		#endregion


		/// <summary>
		/// 
		/// </summary>
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_CONNECTION_LIMIT"));
		}
		
		
		#region Public Methods

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="statsFlag"></param>
		/// <returns></returns>
		public EmailFolderCollection RetrieveFolders(int memberID, int communityID, bool statsFlag)
		{
			return RetrieveFolders(memberID, communityID, statsFlag, false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="statsFlag"></param>
		/// <param name="useCacheOnly"></param>
		/// <returns></returns>
		public EmailFolderCollection RetrieveFolders(int memberID, int communityID, bool statsFlag, bool useCacheOnly)
		{
			string uri = "";

			try
			{
				EmailFolderCollection folders = _cache.Get(EmailFolderCollection.GetCacheKey(memberID, communityID)) as EmailFolderCollection;

				if (folders == null && !useCacheOnly)
				{
					uri = getServiceManagerUri(memberID);

					int cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_FOLDER_CACHE_TTL_SA"));

					base.Checkout(uri);
					try
					{
						folders = getService(uri).RetrieveFolders(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
							memberID,
							communityID,
							statsFlag);
					}
					finally
					{
						base.Checkin(uri);
					}

					cacheFolders(folders);
				}

				return folders;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve folders (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Use this method to retrieve an EmailFolder for a member for a given community.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberFolderID"></param>
		/// <param name="statsFlag"></param>
		/// <returns></returns>
		public EmailFolder RetrieveFolder(int memberID, int communityID, int memberFolderID, bool statsFlag)
		{
			try
			{
				EmailFolderCollection folders = RetrieveFolders(memberID, communityID, statsFlag);
				return folders.FindByMemberFolderID(memberFolderID);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve folder.", ex));
			}
		}

		// <summary>
		// 
		// </summary>
		// <param name="memberID"></param>
		// <param name="communityID"></param>
		// <param name="memberFolderID"></param>
		/*public void EmptyFolder(int memberID, int communityID, int memberFolderID) 
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri(memberID);

				getService(uri).EmptyFolder(memberID,
					communityID, 
					memberFolderID);

			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot empty folder (uri: " + uri + ")", ex));
			}
		}*/

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberFolderID"></param>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		public void Delete(int memberFolderID, int memberID, int groupID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri(memberID);

				base.Checkout(uri);
				try
				{
					getService(uri).Delete(System.Environment.MachineName,
						memberFolderID,
						memberID,
						groupID);
				}
				finally
				{
					base.Checkin(uri);
				}

				EmailFolderCollection folders = RetrieveFolders(memberID, groupID, false);
				folders.Remove(memberFolderID);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot delete folder (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="description"></param>
		/// <returns></returns>
		public FolderSaveResult Save(int memberID, int communityID, string description)
		{
			string uri = "";

			try
			{
				FolderSaveResult result;
				uri = getServiceManagerUri(memberID);

				base.Checkout(uri);
				try
				{
					result = getService(uri).Save(System.Environment.MachineName,
						memberID,
						communityID,
						description);
				}
				finally
				{
					base.Checkin(uri);
				}
				
				if (result.FolderSaveResultStatus == FolderSaveResultStatus.Success)
				{
					EmailFolderCollection folders = RetrieveFolders(memberID, communityID, false);
					EmailFolder folder = new EmailFolder(result.MemberFolderID, memberID, communityID, description, false);
					folders.Add(folder);
				}

				return result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot save folder (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="description"></param>
		/// <param name="memberFolderID"></param>
		/// <returns></returns>
		public FolderSaveResult Save(int memberID, int communityID, string description, int memberFolderID)
		{
			string uri = "";

			try
			{
				FolderSaveResult result;
				uri = getServiceManagerUri(memberID);

				base.Checkout(uri);
				try
				{
					result = getService(uri).Save(System.Environment.MachineName,
						memberID,
						communityID,
						description,
						memberFolderID);
				}
				finally
				{
					base.Checkin(uri);
				}

				if (result.FolderSaveResultStatus == FolderSaveResultStatus.Success)
				{
					EmailFolderCollection folders = RetrieveFolders(memberID, communityID, false);
					EmailFolder folder = folders.FindByMemberFolderID(memberFolderID);
					folder.Description = description;
				}

				return result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot save folder (uri: " + uri + ")", ex));
			}
        }

        #region VIP folders helper methods
        public static Int32 GetSystemFolderIDForVIPFolder(int pFolderID)
        {
            int ret = Constants.NULL_INT;

            switch (pFolderID)
            {
                case (int)SystemFolders.VIPDraft:
                    ret = (int)SystemFolders.Draft;
                    break;
                case (int)SystemFolders.VIPInbox:
                    ret = (int)SystemFolders.Inbox;
                    break;
                case (int)SystemFolders.VIPSent:
                    ret = (int)SystemFolders.Sent;
                    break;
                case (int)SystemFolders.VIPTrash:
                    ret = (int)SystemFolders.Trash;
                    break;
                default:
                    ret = pFolderID;
                    break;
            }

            return ret;
        }

        public static bool IsVIPFolder(int pFolderID)
        {
            bool ret = false;

            switch (pFolderID)
            {
                case (int)SystemFolders.VIPDraft:
                    ret = true;
                    break;
                case (int)SystemFolders.VIPInbox:
                    ret = true;
                    break;
                case (int)SystemFolders.VIPSent:
                    ret = true;
                    break;
                case (int)SystemFolders.VIPTrash:
                    ret = true;
                    break;
                default:

                    break;
            }

            return ret;
        }

        public static int GetMatchingVIPFolderId(int pSystemFolderId)
        {
            int ret = Constants.NULL_INT;

            switch (pSystemFolderId)
            {
                case (int)SystemFolders.Inbox:
                    ret = (int)SystemFolders.VIPInbox;
                    break;
                case (int)SystemFolders.Draft:
                    ret = (int)SystemFolders.VIPDraft;
                    break;
                case (int)SystemFolders.Sent:
                    ret = (int)SystemFolders.VIPSent;
                    break;
                case (int)SystemFolders.Trash:
                    ret = (int)SystemFolders.VIPTrash;
                    break;
            }

            return ret;
        }
        #endregion
        #endregion

        #region Private Methods
        private void cacheFolders(EmailFolderCollection emailFolderCollection)
		{
			int cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_FOLDER_CACHE_TTL_SA"));
			emailFolderCollection.CacheTTLSeconds = cacheTTL;
			emailFolderCollection.CachePriority = CacheItemPriorityLevel.Normal;
			emailFolderCollection.CacheMode = CacheItemMode.Absolute;
			_cache.Insert(emailFolderCollection);
		}
		#endregion

		#region Private Service Adapter Methods
		private IEmailFolderService getService(string uri)
		{
			try
			{
				return (IEmailFolderService)Activator.GetObject(typeof(IEmailFolderService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri(int memberID)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Trim().Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
		#endregion
	}
}
