using System;

using Matchnet.Caching;
using Matchnet.RemotingClient;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;


namespace Matchnet.Email.ServiceAdapters
{
	/// <summary>
	/// Summary description for TeaseSA.
	/// </summary>
	public class TeaseSA : SABase
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "TeaseSM";
		private const string SERVICE_CONSTANT = "EMAIL_SVC";
		#endregion

		#region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly TeaseSA Instance = new TeaseSA();
		#endregion

		#region class variables
		private Cache _cache = null;
		#endregion

		#region Constructors

		private TeaseSA()
		{
			_cache = Cache.Instance;
		}
		#endregion


		/// <summary>
		/// 
		/// </summary>
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="teaseCategoryID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public TeaseCollection GetTeaseCollection(int teaseCategoryID, int groupID)
		{
			string uri = "";

			try
			{
				TeaseCollection oResult = null;

				oResult = _cache.Get(TeaseKey.GetCacheKey(teaseCategoryID, groupID)) as TeaseCollection;

				if(oResult == null) 
				{
					uri = getServiceManagerUri();

					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetTeaseCollection(teaseCategoryID, groupID);
					}
					finally
					{
						base.Checkin(uri);
					}

					oResult.CachedResult = false;
					oResult.SetCacheKey = TeaseKey.GetCacheKey(teaseCategoryID, groupID);
					CacheTease(oResult);
				} 
				else 
				{
					oResult.CachedResult = true;
				}
				
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve teases (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public TeaseCategoryCollection GetTeaseCategoryCollection(int groupID)
		{
			string uri = "";

			try 
			{
				TeaseCategoryCollection oResult = null;
				
				oResult = _cache.Get(TeaseCategoryKey.GetCacheKey(groupID)) as TeaseCategoryCollection;

				if(oResult == null) 
				{
					uri = getServiceManagerUri();

					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetTeaseCategoryCollection(groupID);
					}
					finally
					{
						base.Checkin(uri);
					}

					oResult.CachedResult = false;
					oResult.SetCacheKey = TeaseCategoryKey.GetCacheKey(groupID);
					CacheCategories(oResult);
				}
				else 
				{
					
					oResult.CachedResult = true;
				}

				oResult = FilterCategoriesByDate(oResult);
				return oResult;
			} 
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve tease categories (uri: " + uri + ")", ex));
			}
		}

		private TeaseCategoryCollection FilterCategoriesByDate(TeaseCategoryCollection unfilteredCollection)
		{
			DateTime currentDate = DateTime.Now;
			TeaseCategoryCollection filteredCollection = new TeaseCategoryCollection();
			
			foreach(TeaseCategory category in unfilteredCollection)
			{
				if(category.BeginDate == DateTime.MinValue || (currentDate >= category.BeginDate && currentDate <= category.EndDate))
				{
					filteredCollection.Add(category);
				}
			}

			return filteredCollection;
		}

		#region private Cache Routines
		private void CacheCategories(TeaseCategoryCollection pTeaseCategoryCollection)
		{
			pTeaseCategoryCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pTeaseCategoryCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEASESVC_CACHE_TTL_SA"));
			_cache.Insert(pTeaseCategoryCollection);
		}

		private void CacheTease(TeaseCollection pTeaseCollection)
		{
			pTeaseCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pTeaseCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEASESVC_CACHE_TTL_SA"));
			_cache.Insert(pTeaseCollection);
		}
		#endregion

		#region Private Service Adapter Methods
		private ITeaseService getService(string uri)
		{
			try
			{
				return (ITeaseService)Activator.GetObject(typeof(ITeaseService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
		#endregion
	}
}
