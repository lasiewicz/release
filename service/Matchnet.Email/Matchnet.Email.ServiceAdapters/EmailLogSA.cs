using System;

using Matchnet.RemotingClient;
using Matchnet.Caching;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;


namespace Matchnet.Email.ServiceAdapters
{
	/// <summary>
	/// Summary description for EmailLogSA.
	/// </summary>
	public class EmailLogSA : SABase
	{
		private const string SERVICE_MANAGER_NAME = "EmailLogSM";
		private const string SERVICE_CONSTANT = "EMAIL_SVC";

		private Cache _cache;

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static readonly EmailLogSA Instance = new EmailLogSA();

		private EmailLogSA()
		{
			_cache = Cache.Instance;
		}


		/// <summary>
		/// 
		/// </summary>
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_CONNECTION_LIMIT"));
		}


        public void ExpireCache(int memberID, int groupID)
        {
            try
            {
                _cache.Remove(MemberMailLog.GetCacheKey(memberID, groupID));
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot expire email log", ex));
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public MemberMailLog RetrieveEmailLog(int memberID, int groupID)
		{
			string uri = "";

			try
			{
				MemberMailLog memberMailLog = (MemberMailLog) _cache.Get(MemberMailLog.GetCacheKey(memberID, groupID));

				if (memberMailLog == null)
				{
					uri = getServiceManagerUri(memberID);
					base.Checkout(uri);
					try
					{
						memberMailLog = getService(uri).RetrieveEmailLog(memberID, groupID);
					}
					finally
					{
						base.Checkin(uri);
					}

					_cache.Insert(memberMailLog);
				}

				return memberMailLog;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve email log (uri: " + uri + ")", ex));
			}
		}

        public DateTime? RetrieveEmailLastSentDate(Int32 memberID, Int32 groupID)
        {
            string uri = "";
            DateTime? lastSentDate = null;

            try
            {
                uri = getServiceManagerUri(memberID);
                base.Checkout(uri);
                try
                {
                    lastSentDate = getService(uri).RetrieveEmailLastSentDate(memberID, groupID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return lastSentDate;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve email last sent date (uri: " + uri + ")", ex));
            }
        }

		#region Private Methods

		private IEmailLogService getService(string uri)
		{
			try
			{
				return (IEmailLogService)Activator.GetObject(typeof(IEmailLogService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri(int memberID)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Trim().Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
#endregion
	}
}
