


#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Matchnet.CacheSynchronization.Context;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Email.ServiceAdapters.Interfaces;
using Matchnet.Exceptions;
using Matchnet.List.ValueObjects;
using Matchnet.RemotingClient;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ReplicationActions;
using Matchnet.Email.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.EmailTracker.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Spark.CloudStorage;
using Matchnet.File.ServiceAdapters;
using Matchnet.File.ValueObjects;
using System.IO;
using System.Linq;
using Spark.Logging;
using FileType = Spark.CloudStorage.FileType;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents;
using Spark.PushNotifications.Processor.ServiceAdapter;

#endregion

namespace Matchnet.Email.ServiceAdapters
{
    /// <summary>
    /// Summary description for EmailMessageSA.
    /// </summary>
    public class EmailMessageSA : SABase, IEmailBlockedSender
    {
        #region Private Members
        private const string SERVICE_MANAGER_NAME = "EmailMessageSM";
        private const string SERVICE_CONSTANT = "EMAIL_SVC";

        private Cache _cache = Cache.Instance;
        private Matchnet.ICaching _cacheWebBucket;
        #endregion

        public string CallerAppName { get; set; }

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public static readonly EmailMessageSA Instance = new EmailMessageSA();

        private EmailMessageSA()
        {
            _cacheWebBucket = Spark.Caching.MembaseCaching.GetSingleton(RuntimeSettings.Instance.GetMembaseConfigByBucketFromSingleton("webcache"));
        }
        #endregion

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_CONNECTION_LIMIT"));
        }

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Use this method to determine the correct memberID for the Ignore list.
        /// The pMemberID parameter is compared to the ToMemberID and FromMemberID attributes on EmailMessage
        /// to determine if the member is the sender or the recipient.  The opposite memberID is returned.
        /// </summary>
        /// <param name="pEmailMessage"></param>
        /// <param name="pMemberID"></param>
        /// <returns></returns>
        public int GetMemberIDToBlock(EmailMessage pEmailMessage, int pMemberID)
        {
            if (pEmailMessage.ToMemberID == pMemberID)
            {
                return pEmailMessage.FromMemberID;
            }
            return pEmailMessage.ToMemberID;
        }

        /// <summary>
        /// Use this method to save an email message to a sender's Draft mailbox.
        /// </summary>
        /// <param name="messageSave"></param>
        /// <returns></returns>
        public bool SaveMessage(MessageSave messageSave)
        {
            int memberMailID = int.MinValue;
            return SaveMessage(messageSave, out memberMailID);
        }

        public bool SaveMessage(MessageSave messageSave, out int memberMailID)
        {
            string uri = string.Empty;
            MessageSaveResult result;

            try
            {
                uri = getServiceManagerUri(messageSave.FromMemberID);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).SaveMessage(System.Environment.MachineName, messageSave);
                }
                finally
                {
                    base.Checkin(uri);
                }

                if (result.Status == MessageSaveStatus.Failed)
                {
                    memberMailID = int.MinValue;
                    return false;
                }

                Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));

                if (messageSave.MessageListID > 0)
                {
                    CacheUtil.MessageSave(_cache, _cacheWebBucket, messageSave.GetMessageSaveAction(true, result.MessageID, MessageSaveType.Update | MessageSaveType.Draft, cacheTTL), messageSave.GroupID);
                }
                else
                {
                    CacheUtil.MessageSave(_cache, messageSave.GetMessageSaveAction(true, result.MessageID, MessageSaveType.Insert, cacheTTL));
                    if ((messageSave.MailOption & MailOption.VIP) == MailOption.VIP)
                        CacheUtil.MessageListSave(_cache, _cacheWebBucket, messageSave.GetMessageListSaveAction(true, result.MessageID, result.MessageListID, (Int32)SystemFolders.VIPDraft, MessageListSaveType.Insert));
                    else
                        CacheUtil.MessageListSave(_cache, _cacheWebBucket, messageSave.GetMessageListSaveAction(true, result.MessageID, result.MessageListID, (Int32)SystemFolders.Draft, MessageListSaveType.Insert));
                }
                memberMailID = result.MessageListID;
                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save email (uri: " + uri + ")", ex));
            }
        }
        
        /// <summary>
        /// Sends a message.
        /// </summary>
        /// <param name="messageSave">The email message object.</param>
        /// <param name="saveInSent">A boolean value indicating if a copy of the email message should be saved to the sender's sent folder.</param>
        /// <param name="replyMessageListID">(Optional, use Matchnet.Constants.NULL_INT if not applicable.) If replying to an email, this parameter should represent the MessageListID for the message being replied to.</param>
        /// <param name="respectQuota"></param>
        /// <returns></returns>
        public MessageSendResult SendMessage(MessageSave messageSave, bool saveInSent, int replyMessageListID, bool respectQuota)
        {
            try
            {
                if (replyMessageListID > 0)
                {
                    MessageListCollection messageListCollection = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, false);
                    MessageList messageList = messageListCollection[replyMessageListID];

                    if (messageList == null)
                    {
                        messageListCollection = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, true);
                        messageList = messageListCollection[replyMessageListID];
                    }

                    if (messageList == null || (messageList.FromMemberID != messageSave.ToMemberID && (messageList.MemberFolderID != (int)SystemFolders.Draft && messageList.MemberFolderID != (int)SystemFolders.VIPDraft)))
                    {
                        return new MessageSendResult(MessageSendError.InvalidReplyMessageListID);
                    }
                }
                else if (QuotaSA.Instance.IsAtQuota(messageSave.GroupID, messageSave.FromMemberID, QuotaType.SentEmail) && respectQuota)
                {
                    return new MessageSendResult(MessageSendError.ComposeLimitReached);
                }

                //IM messages
                if (messageSave.MailType == MailType.InstantMessage)
                {
                    //verify threadID exists
                    if (string.IsNullOrEmpty(messageSave.ThreadID))
                    {
                        return new MessageSendResult(MessageSendError.MissingThreadID);
                    }

                    //save last conversation, only for IM for now
                    LastConversation lastConversation = new LastConversation(messageSave);
                    _cacheWebBucket.Insert(lastConversation);
                }

                MessageSendResult sendResult = sendMessage(messageSave, saveInSent, replyMessageListID);

                if (messageSave.MailType != MailType.InstantMessage)
                {
                    // check to see if we need to send this message to member's mobile via Messmo
                    if (
                        ExternalMail.ServiceAdapters.ExternalMailSA.Instance.IsMessmoMessageRequired(
                            messageSave.ToMemberID, messageSave.GroupID))
                    {
                        ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoMessageFromEmail(sendResult);
                    }
                }

                return sendResult;
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot send email.", ex);
            }
        }

        /// <summary>
        /// Check to see if recipient member has blocked sender. If blocked, send email to sender indicating unsuccessful send.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="messageHeader"></param>
        /// <param name="messageBody"></param>
        /// <param name="recipientMemberId"></param>
        /// <param name="senderMemberId"></param>
        /// <param name="siteId"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public MessageSendResult CheckAndSendMessageForBlockedSenderInRecipientList(int groupId, string messageHeader, string messageBody, int recipientMemberId, int senderMemberId, int siteId, int communityId)
        {

            MessageSendResult messageSendResult=null;
            // If the sender is blocked by the receiver, send a blocked message to the sender and halt sending to the receiver
            List.ServiceAdapters.List list = ListSA.Instance.GetList(recipientMemberId);

            if (list.IsHotListed(HotListCategory.IgnoreList, communityId, senderMemberId))
            {
                MessageSave blockMessageSave = new MessageSave(recipientMemberId,
                                                               senderMemberId,
                                                               groupId,
                                                               siteId,
                                                               MailType.IgnoreEmail,
                                                               messageHeader,
                                                               messageBody);

                
                messageSendResult = SendMessage(blockMessageSave, false, Constants.NULL_INT, false);
            }
            return messageSendResult;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListID"></param>
        /// <returns></returns>
        public EmailMessage RetrieveMessage(Int32 memberID,
            Int32 groupID,
            Int32 messageListID)
        {
            return retrieveEmailMessage(memberID, groupID, messageListID, true, false);
        }

        public EmailMessage RetrieveMessage(Int32 memberID,
            Int32 groupID,
            Int32 messageListID,
            bool ignoreSACache)
        {
            return retrieveEmailMessage(memberID, groupID, messageListID, true, ignoreSACache);
        }

        /// <summary>
        /// Use this method to retrieve the specified EmailMessage.
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="memberID"></param>
        /// <param name="messageListID"></param>
        /// <param name="orderBy"></param>
        /// <param name="messageDirection"></param>
        /// <param name="updateReadFlag"></param>
        /// <returns></returns>
        public EmailMessage RetrieveMessage(Int32 groupID,
            Int32 memberID,
            Int32 messageListID,
            string orderBy,
            Direction messageDirection,
            bool updateReadFlag,
            Brand brand)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(memberID);
                EmailMessage message = retrieveEmailMessage(memberID, groupID, messageListID, true, false);

                if (message == null)
                {
                    return null;
                }

                if (messageDirection != Direction.None)
                {
                    ArrayList messages = retrieveMessages(memberID, groupID, message.MemberFolderID).Sort(EmailComparer.FromString(orderBy));
                    Int32 index = 0;

                    //find current message's index
                    for (Int32 i = 0; i < messages.Count; i++)
                    {
                        if (((EmailMessage)messages[i]).MemberMailID == message.MemberMailID)
                        {
                            index = i;
                            break;
                        }
                    }

                    //advance index based on direction
                    if (messageDirection == Direction.Next)
                    {
                        index += 1;
                    }
                    else if (messageDirection == Direction.Previous)
                    {
                        index -= 1;
                    }

                    //return message at new index, if in range
                    if (index >= 0 && index < messages.Count)
                    {
                        message = (EmailMessage)messages[index];
                        //If we don't have this message's content, get it from the MT
                        if (message.Content == null)
                        {
                            message = retrieveEmailMessage(memberID, groupID, message.MemberMailID, true, false);
                        }
                    }
                }

                if (message != null && updateReadFlag)
                {
                    ReplicationActionCollection actions = new ReplicationActionCollection();

                    if (memberID == message.ToMemberID)
                    {
                        IReplicationAction openedAction = markOpened(message);
                        if (openedAction != null)
                        {
                            actions.Add(openedAction);

                            string senderUri = getServiceManagerUri(message.FromMemberID);
                            if (senderUri != uri)
                            {
                                base.Checkout(senderUri);
                                try
                                {
                                    getService(senderUri).PlayAction(System.Environment.MachineName, openedAction);
                                }
                                finally
                                {
                                    base.Checkin(senderUri);
                                }
                            }
                        }
                    }

                    actions.Add(markRead(memberID, message.GroupID, true, message.MemberMailID));

                    playAction(uri, actions);
                }

                setUsernamesOnMessage(message, brand);

                return message;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve email (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public ICollection RetrieveMessages(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand)
        {
            EmailMessageCollection messages = filterECardMessages(retrieveMessages(memberID, communityID, memberFolderID), brand.Site.SiteID);
            updateRecipientMemberForHasNewMail(memberID, communityID);
            EmailComparer comparer = EmailComparer.FromString(orderBy);

            if (comparer.SortTypeValue == EmailComparer.SortType.Subject || comparer.SortTypeValue == EmailComparer.SortType.OpenDate)
            {
                //Need to retrieve full message data for all messages so that we have all the subject lines to sort by
                populateMessages(memberID, messages);
            }
            else if (comparer.SortTypeValue == EmailComparer.SortType.FromUserName || comparer.SortTypeValue == EmailComparer.SortType.ToUserName)
            {
                populateUsernames(messages, brand);
            }

            var sorted = messages.Sort(comparer);
            var paged = DoPaging(sorted, startRow, pageSize);
            //Retrieve full message data (except content) for the current page of messages
            populateMessages(memberID, paged);
            //Retrieve usernames for the current page of messages
            populateUsernames(paged, brand);

            return paged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="startRow"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public MessageMemberCollection RetrieveMessageMembers(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand)
        {

            MessageMemberCollection messageMemberCollection = retrieveMessageMembers(memberID, communityID, true);
            return messageMemberCollection;
        
        }


        /// <summary>
        /// This is a modified version of RetrieveMessages to support merging of original system folders and vip folders.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="brand"></param>
        /// <param name="orderBy"></param>
        /// <param name="pageSize"></param>
        /// <param name="startRow"></param>
        /// <returns></returns>
        public ICollection RetrieveMessagesFromLogicalFolder(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand)
        {
            if (memberFolderID > 4)
                throw new Exception("Only Inbox, Sent, Draft, Trash folder is supported.");

            SystemFolders systemFolder = (SystemFolders)memberFolderID;
            EmailMessageCollection messages = filterECardMessages(retrieveMessages(memberID, communityID, (int)systemFolder), brand.Site.SiteID);
            SystemFolders vipFolder = (SystemFolders)Enum.Parse(typeof(SystemFolders), "VIP" + systemFolder.ToString());
            EmailMessageCollection vips = filterECardMessages(retrieveMessages(memberID, communityID, (int)vipFolder), brand.Site.SiteID);

            foreach (EmailMessage message in vips)
            {
                messages.Add(message);
            }
            
            updateRecipientMemberForHasNewMail(memberID, communityID);
            EmailComparer comparer = EmailComparer.FromString(orderBy);

            if (comparer.SortTypeValue == EmailComparer.SortType.Subject || comparer.SortTypeValue == EmailComparer.SortType.OpenDate)
            {
                //Need to retrieve full message data for all messages so that we have all the subject lines to sort by
                populateMessages(memberID, messages);
            }
            else if (comparer.SortTypeValue == EmailComparer.SortType.FromUserName || comparer.SortTypeValue == EmailComparer.SortType.ToUserName)
            {
                populateUsernames(messages, brand);
            }

            var sorted = messages.Sort(comparer);
            var paged = DoPaging(sorted, startRow, pageSize);
            //Retrieve full message data (except content) for the current page of messages
            populateMessages(memberID, paged);
            //Retrieve usernames for the current page of messages
            populateUsernames(paged, brand);

            return paged;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <returns></returns>
        public EmailMessageCollection RetrieveMessages(Int32 memberID, Int32 communityID, Int32 memberFolderID)
        {
            return retrieveMessages(memberID, communityID, memberFolderID);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="siteID"></param>
        /// <returns></returns>
        public EmailMessageCollection RetrieveMessages(Int32 memberID, Int32 communityID, Int32 memberFolderID, Int32 siteID)
        {
            return filterECardMessages(retrieveMessages(memberID, communityID, memberFolderID), siteID);
        }

        /// <summary>
        /// Use this method to 'mark' messages as Read/Un-read for a member.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="read"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        public bool MarkRead(Int32 memberID,
            Int32 groupID,
            bool read,
            ArrayList messageListIDs)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri(memberID);

                IReplicationAction action = markRead(memberID, groupID, read, messageListIDs);

                playAction(uri, action);

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark email read (uri: " + uri + ")", ex));
            }
        }

        public void MarkOffFraudHoldSentByMember(int memberID,
            int groupID,
            int siteID)
        {
            bool enabled =
                Convert.ToBoolean(RuntimeSettings.GetSetting("EMAIL_FRAUD_HOLD", Constants.NULL_INT,
                                                             siteID));

            if(enabled)
                MarkOffFraudHoldSentByMember(memberID, groupID, false);
        }

        public string MarkOffFraudHoldSentByMember(int memberID,
            int groupID,
            bool returnTrace)
        {
            // need to expire the cache because EmailLog has not cache synch and it has 10 min absolute cache timeout
            EmailLogSA.Instance.ExpireCache(memberID, groupID);
            MemberMailLog mailLog = EmailLogSA.Instance.RetrieveEmailLog(memberID, groupID);

            if (mailLog == null)
                return string.Empty;

            var sb = new StringBuilder();

            // Get all emails sent by this member
            EmailLogEntryCollection col = mailLog.Sent;

            // Retrieve the last sub purchase date so that we only process emails since then
            Matchnet.Member.ServiceAdapters.Member member =
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            DateTime lastSubInitialPuchaseDate = member.GetSubscriptionLastInitialPurchaseDate(groupID);

            if(returnTrace)
                sb.AppendLine("[MemberID: " + memberID + ", LastSubInitPurDate: " + lastSubInitialPuchaseDate + "] ");

            foreach (EmailLogEntry entry in col)
            {
                if(returnTrace)
                    sb.AppendLine("[EmailLogEntry.MailID: " + entry.MailID + ", EmailLogEntry.InsertDate: " + entry.InsertDate + "] ");

                // Check the sent date first
                if (entry.InsertDate < lastSubInitialPuchaseDate)
                    continue;

                // Retrieve the recipient's MessageList collection
                MessageListCollection msgListCol = retrieveMessageListFromDB(entry.ToMemberID, groupID);

                if (msgListCol == null)
                    continue;

                // Check to see if the MessageID matches and remove if they do match
                foreach (MessageList msgList in msgListCol)
                {
                    if (msgList.MessageID == entry.MailID)
                    {
                        if(returnTrace)
                            sb.AppendLine("[MessageListID: " + msgList.MessageListID + ", MemberID: " + entry.ToMemberID + "] ");

                        MarkOffFraudHold(entry.ToMemberID, groupID, new ArrayList { msgList.MessageListID }, msgListCol, returnTrace ? sb : null);
                        break; // we found our message so break here
                    }
                }
            }

            return sb.ToString();

        }        

        public void MarkDeletedSentByMember(int memberID,
            int groupID,
            bool isDelete)
        {
            MemberMailLog mailLog = EmailLogSA.Instance.RetrieveEmailLog(memberID, groupID);

            if(mailLog == null)
                return;
            
            // Get all emails sent by this member
            EmailLogEntryCollection col = mailLog.Sent;
            ArrayList sentToMemberIDs = new ArrayList();

            MessageListCollection msgListCol = null;

            foreach (EmailLogEntry entry in col)
            {
                if (!sentToMemberIDs.Contains(entry.ToMemberID))
                {
                    sentToMemberIDs.Add(entry.ToMemberID);
                }

                // Retrieve the recipient's MessageList collection
                if (isDelete)
                {
                    msgListCol = retrieveMessageList(entry.ToMemberID, groupID, false);
                }
                else
                {
                    // in the undelete case, we don't have the MessageLists that we want in memory
                    msgListCol = retrieveMessageListFromDB(entry.ToMemberID, groupID);
                }

                if (msgListCol == null)
                    continue;
         
                // Check to see if the MessageID matches and remove if they do match
                foreach (MessageList msgList in msgListCol)
                {
                    if (msgList.MessageID == entry.MailID)
                    {
                        if (isDelete)
                        {
                            MarkDeleted(entry.ToMemberID, groupID, isDelete, new ArrayList { msgList.MessageListID }, null);
                        }
                        else
                        {
                            MarkDeleted(entry.ToMemberID, groupID, isDelete, new ArrayList { msgList.MessageListID }, msgListCol);
                        }
                        break; // we found our message so break here
                    }
                }
            }

            if (!isDelete)
                return;

            // if this was a delete we have to set BannedEmailsRemoved attribute of the recipients
            foreach (int toMemID in sentToMemberIDs)
            {
                Matchnet.Member.ServiceAdapters.Member mem = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(toMemID, MemberLoadFlags.None);
                mem.SetAttributeInt(groupID, Constants.NULL_INT, Constants.NULL_INT, "BannedEmailsRemoved", 1);
                Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(mem);
            }
        }
        
        /// <summary>
        /// Performs soft delete where DB row is updated only but the messagelist disappears from all caches
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="isDeleted"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        public bool MarkDeleted(Int32 memberID,
            Int32 groupID,
            bool isDeleted,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri(memberID);

                IReplicationAction action = markDeleted(memberID, groupID, isDeleted, messageListIDs, messageListColFromDB);

                playAction(uri, action);

                // update the new mail count because deleting and undeleting messages could change this count
                updateRecipientMemberForHasNewMail(memberID, groupID);

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark email banned (uri: " + uri + ")", ex));
            }
        }

        public bool MarkOffFraudHold(Int32 memberID,
            Int32 groupID,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB,
            StringBuilder sb)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri(memberID);

                IReplicationAction action = markOffFraudHold(memberID, groupID, messageListIDs, messageListColFromDB);

                playAction(uri, action);

                // Let's take care All Access enabling back stuff here
                // This should only happen if any action was done actually
                if(action != null)
                {
                    foreach (MessageListSaveAction mlsAction in (action as ReplicationActionCollection))
                    {
                        executeUnfraudPostProcess(memberID, groupID, new ArrayList() { mlsAction.MessageList.MessageListID }, messageListColFromDB, sb);
                    }
                }
                else
                {
                    if (sb != null)
                        sb.AppendLine("MarkOffFraudHold action count 0");
                }

                // update the new mail count because deleting and undeleting messages could change this count
                if ((action as ReplicationActionCollection) != null)
                {
                    updateRecipientMailAttributes(memberID, groupID, (action as ReplicationActionCollection).Count);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark email banned (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// To change the OneFreeReply status of the message's StatusMask value, use this method.
        /// </summary>
        /// <param name="pMemberID"></param>
        /// <param name="pGroupId"></param>
        /// <param name="pOneFreeReply"></param>
        /// <param name="pMessageListIds"></param>
        /// <returns></returns>
        public bool MarkOneFreeReply(int pMemberID,
            int pGroupId,
            bool pOneFreeReply,
            ArrayList pMessageListIds)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri(pMemberID);

                IReplicationAction action = markOneFreeReply(pMemberID, pGroupId, pOneFreeReply, pMessageListIds, false);

                playAction(uri, action);

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark OneFreeReply status (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Change the OneFreeReply status of a MessageList when SA cache won't be accurate.
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="groupId"></param>
        /// <param name="oneFreeReply"></param>
        /// <param name="messageListIds"></param>
        /// <param name="ignoreSACache"></param>
        /// <returns></returns>
        public bool MarkOneFreeReply(int memberId,
            int groupId,
            bool oneFreeReply,
            ArrayList messageListIds,
            bool ignoreSACache)
        {
            string uri = string.Empty;

            try
            {
                RollingFileLogger.Instance.EnterMethod(CallerAppName, "EmailMessageSA", "MarkOneFreeReply - ignoreSACache version");

                uri = getServiceManagerUri(memberId);

                IReplicationAction action = markOneFreeReply(memberId, groupId, oneFreeReply, messageListIds, ignoreSACache);

                RollingFileLogger.Instance.LogInfoMessage(CallerAppName, "EmailMessageSA", "Calling playAction", null);

                playAction(uri, action);

                RollingFileLogger.Instance.LeaveMethod(CallerAppName, "EmailMessageSA", "MarkOneFreeReply - ignoreSACache version");

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark OneFreeReply status (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Performs soft remove on message member so it does not show up when gathering a list of members you've had messages with based on MessageList table (e.g. list of members you've had IM conversations with)
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="isDeleted"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        public bool MarkMessageMemberRemoved(Int32 memberID,
            Int32 groupID,
            bool isRemoved,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri(memberID);

                ReplicationActionCollection actions = new ReplicationActionCollection();

                foreach (Int32 messageListID in messageListIDs)
                {
                    MessageListSaveType mSaveType = MessageListSaveType.Update;

                    IReplicationAction action = markMessageStatus(memberID, groupID, messageListID, isRemoved, MessageStatus.MemberRemove, mSaveType, true, false, messageListColFromDB);

                    actions.Add(action);
                }

                if (actions.Count > 0)
                {
                    playAction(uri, actions);                    
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark message member removed (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Performs soft remove on message member for IM history members list
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="isDeleted"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        public bool MarkInstantMessageMemberRemoved(Int32 memberID,
            Int32 groupID,
            bool isRemoved,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB)
        {
            string uri = string.Empty;

            try
            {
                if (messageListColFromDB == null || messageListColFromDB.Count <= 0)
                {
                    messageListColFromDB = new MessageListCollection(memberID, groupID);
                    MessageMemberCollection messageMemberCollection = RetrieveInstantMessageMembers(memberID, groupID, false, false);
                    foreach (int i in messageListIDs)
                    {
                        MessageMember mm = messageMemberCollection.GetMessageMemberByMessageListID(i);
                        if (mm != null)
                        {
                            MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                            messageListColFromDB.Add(ml);
                        }
                    }
                }

                if (messageListColFromDB.Count > 0)
                {
                    MarkMessageMemberRemoved(memberID, groupID, isRemoved, messageListIDs, messageListColFromDB);

                    //clear cache for IM history members list
                    _cacheWebBucket.Remove(MessageMemberCollection.GetCacheKey(memberID, groupID, MailType.InstantMessage, ConversationType.Chat, true));
                    _cacheWebBucket.Remove(MessageMemberCollection.GetCacheKey(memberID, groupID, MailType.InstantMessage, ConversationType.Chat, false));

                    return true;
                }
                else
                {
                    //message list item not found
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark instant message member removed (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Performs soft remove on message member for mail conversation inbox member list
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="isDeleted"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        public bool MarkMailConversationMemberRemoved(Int32 memberID,
            Int32 groupID,
            bool isRemoved,
            ArrayList messageListIDs,
            ConversationType conversationType,
            MessageListCollection messageListColFromDB)
        {
            string uri = string.Empty;
            ArrayList updatedMessageListIDs = new ArrayList();
            ArrayList notFoundMessageListIDs = new ArrayList();
            ArrayList notFoundMessageMember = new ArrayList();
            ArrayList notFoundMessageMemberSwap = new ArrayList();

            try
            {
                if (messageListColFromDB == null || messageListColFromDB.Count <= 0)
                {
                    messageListColFromDB = new MessageListCollection(memberID, groupID);
                    if (isRemoved)
                    {
                        MessageMemberCollection messageMemberCollection = RetrieveMailConversationMembers(memberID, groupID, ConversationType.Inbox);
                        if (messageMemberCollection != null)
                        {
                            foreach (int i in messageListIDs)
                            {
                                MessageMember mm = messageMemberCollection.GetMessageMemberByMessageListID(i);
                                if (mm != null)
                                {
                                    if (mm.LastMessageListID <= 0 || mm.LastMessageListID == mm.MessageListID)
                                    {
                                        MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                                        messageListColFromDB.Add(ml);
                                        updatedMessageListIDs.Add(mm.MessageListID);
                                    }
                                    else
                                    {
                                        //found the message member, but it's not the latest one
                                        notFoundMessageMember.Add(mm);
                                    }
                                }
                                else
                                {
                                    //did not find message member
                                    notFoundMessageListIDs.Add(i);
                                }
                            }
                        }

                        if (notFoundMessageListIDs.Count > 0 || notFoundMessageMember.Count > 0)
                        {
                            //try sent list (which may not be in the inbox possibly)
                            messageMemberCollection = RetrieveMailConversationMembers(memberID, groupID, ConversationType.Sent);
                            if (messageMemberCollection != null)
                            {
                                foreach (int i in messageListIDs)
                                {
                                    MessageMember mm = messageMemberCollection.GetMessageMemberByMessageListID(i);
                                    if (mm != null)
                                    {
                                        if (mm.LastMessageListID <= 0 || mm.MessageListID == mm.LastMessageListID)
                                        {
                                            MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                                            messageListColFromDB.Add(ml);
                                            updatedMessageListIDs.Add(mm.MessageListID);
                                            notFoundMessageListIDs.Remove(i);
                                        }
                                    }
                                }

                                notFoundMessageMemberSwap = new ArrayList();
                                foreach (MessageMember mmNotFound in notFoundMessageMember)
                                {
                                    MessageMember mm = messageMemberCollection.GetMessageMemberByTargetMemberID(mmNotFound.TargetMemberID);
                                    if (mm != null)
                                    {
                                        if (mm.LastMessageListID <= 0 || mm.MessageListID == mm.LastMessageListID)
                                        {
                                            MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                                            messageListColFromDB.Add(ml);
                                            updatedMessageListIDs.Add(mm.MessageListID);
                                        }
                                        else
                                        {
                                            notFoundMessageMemberSwap.Add(mmNotFound);
                                        }
                                    }
                                }
                                notFoundMessageMember = notFoundMessageMemberSwap;
                            }
                        }

                        if (notFoundMessageListIDs.Count > 0 || notFoundMessageMember.Count > 0)
                        {
                            //try draft list (which may not be in the inbox possibly)
                            messageMemberCollection = RetrieveMailConversationMembers(memberID, groupID, ConversationType.Draft);
                            if (messageMemberCollection != null)
                            {
                                foreach (int i in messageListIDs)
                                {
                                    MessageMember mm = messageMemberCollection.GetMessageMemberByMessageListID(i);
                                    if (mm != null)
                                    {
                                        if (mm.LastMessageListID <= 0 || mm.MessageListID == mm.LastMessageListID)
                                        {
                                            MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                                            messageListColFromDB.Add(ml);
                                            updatedMessageListIDs.Add(mm.MessageListID);
                                            notFoundMessageListIDs.Remove(i);
                                        }
                                    }
                                }

                                notFoundMessageMemberSwap = new ArrayList();
                                foreach (MessageMember mmNotFound in notFoundMessageMember)
                                {
                                    MessageMember mm = messageMemberCollection.GetMessageMemberByTargetMemberID(mmNotFound.TargetMemberID);
                                    if (mm != null)
                                    {
                                        if (mm.LastMessageListID <= 0 || mm.MessageListID == mm.LastMessageListID)
                                        {
                                            MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                                            messageListColFromDB.Add(ml);
                                            updatedMessageListIDs.Add(mm.MessageListID);
                                        }
                                        else
                                        {
                                            notFoundMessageMemberSwap.Add(mmNotFound);
                                        }
                                    }
                                }
                                notFoundMessageMember = notFoundMessageMemberSwap;
                            }
                        }
                    }
                    else
                    {
                        MessageMemberCollection messageMemberCollection = RetrieveMailConversationMembers(memberID, groupID, ConversationType.Trash);
                        if (messageMemberCollection != null)
                        {
                            foreach (int i in messageListIDs)
                            {
                                MessageMember mm = messageMemberCollection.GetMessageMemberByMessageListID(i);
                                if (mm != null)
                                {
                                    MessageList ml = new MessageList(mm.MessageListID, mm.MemberID, mm.TargetMemberID, mm.DirectionFlag, mm.GroupID, mm.MemberFolderID, mm.MessageID, mm.StatusMask, mm.MailTypeID, mm.InsertDate, mm.NumBytes);
                                    messageListColFromDB.Add(ml);
                                    updatedMessageListIDs.Add(mm.MessageListID);
                                }
                            }
                        }
                    }
                }

                if (messageListColFromDB.Count > 0)
                {
                    MarkMessageMemberRemoved(memberID, groupID, isRemoved, updatedMessageListIDs, messageListColFromDB);

                    //clear cache for mail members list
                    CacheUtil.RemoveMailConversationMembersCache(_cacheWebBucket, memberID, groupID, ConversationType.Inbox);
                    CacheUtil.RemoveMailConversationMembersCache(_cacheWebBucket, memberID, groupID, ConversationType.Draft);
                    CacheUtil.RemoveMailConversationMembersCache(_cacheWebBucket, memberID, groupID, ConversationType.Sent);
                    CacheUtil.RemoveMailConversationMembersCache(_cacheWebBucket, memberID, groupID, ConversationType.Trash);

                    return true;
                }
                else
                {
                    //message member not found
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark mail conversation inbox message member removed (uri: " + uri + ")", ex));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListIDs"></param>
        /// <param name="memberFolderID"></param>
        /// <returns></returns>
        public bool MoveToFolder(Int32 memberID, Int32 groupID, ArrayList messageListIDs, Int32 memberFolderID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(memberID);
                int inboxMsgDelta = getMsgListInboxCount(memberID, groupID, messageListIDs, memberFolderID);
                IReplicationAction action = moveToFolder(memberID, groupID, messageListIDs, memberFolderID);

                playAction(uri, action);

                updateRecipientMemberForHasNewMail(memberID, groupID);
                updateMemberInboxCount(memberID, groupID, inboxMsgDelta);

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot move to folder (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListIDs"></param>
        /// <returns></returns>
        public bool DeleteMessage(Int32 memberID, Int32 groupID, ArrayList messageListIDs)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri(memberID);
                int inboxMsgDelta = getMsgListInboxCount(memberID, groupID, messageListIDs, 0);
                IReplicationAction action = deleteMessages(memberID, groupID, messageListIDs);

                playAction(uri, action);

                updateRecipientMemberForHasNewMail(memberID, groupID);
                if (inboxMsgDelta != 0)
                {
                    updateMemberInboxCount(memberID, groupID, inboxMsgDelta);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot delete email (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="memberFolderID"></param>
        /// <returns></returns>
        public bool EmptyFolder(Int32 memberID, Int32 groupID, Int32 memberFolderID)
        {
            ArrayList messageListIDs = new ArrayList();
            MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);

            lock (messageListCollection.SyncRoot)
            {
                foreach (MessageList messageList in messageListCollection)
                {
                    if (messageList.MemberFolderID == memberFolderID)
                    {
                        messageListIDs.Add(messageList.MessageListID);
                    }
                }
            }

            return DeleteMessage(memberID, groupID, messageListIDs);
        }

        /// <summary>
        /// This will remove all chat messages between 2 members
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="messageListID"></param>
        /// <param name="conversationType"></param>
        /// <returns></returns>
        public MessageGeneralResult DeleteInstantMessages(int memberID, int targetMemberID, int groupID, int messageListID)
        {
            MessageGeneralResult generalResult = new MessageGeneralResult();
            generalResult.MemberID = memberID;
            generalResult.TargetMemberID = targetMemberID;
            generalResult.MessageListID = messageListID;

            if (memberID <= 0)
            {
                generalResult.Status = MessageGeneralResultStatus.Failed;
                generalResult.Error = MessageGeneralResultError.MemberNotFound;
                return generalResult;
            }

            if (targetMemberID <= 0)
            {
                generalResult.Status = MessageGeneralResultStatus.Failed;
                generalResult.Error = MessageGeneralResultError.TargetMemberNotFound;
                return generalResult;
            }

            //create instant messages delete action
            string uri = getServiceManagerUri(memberID);
            InstantMessagesDeleteAction action = new InstantMessagesDeleteAction(memberID, targetMemberID, messageListID, groupID);
            playAction(uri, action);

            return generalResult;

        }

        /// <summary>
        /// This will remove all messages for a conversation between 2 members
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <param name="messageListID"></param>
        /// <param name="conversationType"></param>
        /// <returns></returns>
        public MessageGeneralResult DeleteMailConversation(int memberID, int targetMemberID, int groupID, int messageListID, ConversationType conversationType, bool verifyTrashed)
        {
            MessageGeneralResult generalResult = new MessageGeneralResult();
            generalResult.MemberID = memberID;
            generalResult.TargetMemberID = targetMemberID;
            generalResult.MessageListID = messageListID;

            MessageMember messageMember = null;

            if (memberID <= 0)
            {
                generalResult.Status = MessageGeneralResultStatus.Failed;
                generalResult.Error = MessageGeneralResultError.MemberNotFound;
                return generalResult;
            }

            if (targetMemberID <= 0)
            {
                generalResult.Status = MessageGeneralResultStatus.Failed;
                generalResult.Error = MessageGeneralResultError.TargetMemberNotFound;
                return generalResult;
            }

            if (conversationType == ConversationType.None)
            {
                generalResult.Status = MessageGeneralResultStatus.Failed;
                generalResult.Error = MessageGeneralResultError.InvalidConversationType;
                return generalResult;
            }

            if (conversationType == ConversationType.Chat)
            {
                return DeleteInstantMessages(memberID, targetMemberID, groupID, messageListID);
            }

            if (verifyTrashed || conversationType == ConversationType.Trash)
            {
                //verify member is still in trash list
                MessageMemberCollection trashMessageMembers = RetrieveMailConversationMembers(memberID, groupID, ConversationType.Trash);
                if (trashMessageMembers != null)
                {
                    messageMember = trashMessageMembers.GetMessageMemberByTargetMemberID(targetMemberID);
                }

                if (messageMember == null)
                {
                    //not found in trash, likely member got a new message and it has been moved back to inbox
                    generalResult.Status = MessageGeneralResultStatus.Failed;
                    generalResult.Error = MessageGeneralResultError.MessageMemberNotFoundInTrash;
                    return generalResult;
                }
            }

            //get messages to delete
            List<int> messageList = new List<int>();
            EmailMessageList emailMessageList = null;

            if (conversationType == ConversationType.Trash)
            {
                //trash will always be the full inbox messages
                emailMessageList = RetrieveMailConversationMessages(memberID, targetMemberID, groupID, 0, 0, ConversationType.Inbox, false);                
            }
            else
            {
                emailMessageList = RetrieveMailConversationMessages(memberID, targetMemberID, groupID, 0, 0, conversationType, false);
            }

            foreach (EmailMessage emailMessage in emailMessageList)
            {
                messageList.Add(emailMessage.MemberMailID);
            }

            if (conversationType == ConversationType.Trash)
            {
                //add last sent or draft message
                if (emailMessageList != null && emailMessageList.Count > 0)
                {
                    if (emailMessageList[0].MemberMailID != messageMember.MessageListID && messageMember.MessageListID > 0)
                    {
                        messageList.Add(messageMember.MessageListID);
                    }
                }
            }

            DeleteMessage(memberID, groupID, new ArrayList(messageList));

            return generalResult;
        }

        public bool MarkDraftSaved(Int32 memberID,
            Int32 groupID,
            bool saved,
            Int32 messageListID)
        {
            string uri = "";
            try
            {
                uri = getServiceManagerUri(memberID);
                MessageList messageList = retrieveMessageList(memberID, groupID, false)[messageListID];
                IReplicationAction action = null;

                if (messageList != null)
                {
                    MessageStatus messageStatus = messageList.StatusMask;
                    bool isSaved = (messageStatus & MessageStatus.SavedToDraft) == MessageStatus.SavedToDraft;

                    messageList.StatusMask |= MessageStatus.SavedToDraft;

                    action = new MessageListSaveAction(memberID, MessageListSaveType.Update, messageList);

                }

                // if marking the message as 'Read', we need to update the member record
                // in the event that 'HasNewMail' is no longer true as a result of reading this message.
                playAction(uri, action);

                return true;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot mark message as saved to draft (uri: " + uri + ")", ex));
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, Dictionary<MailType, int>> GetUnreadMessageCounts(int memberID, Brand brand)
        {
            Dictionary<string, Dictionary<MailType, int>> unreadMessageDictionary = new Dictionary<string, Dictionary<MailType, int>>();

            int groupID = brand.Site.Community.CommunityID;
            MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);           
            foreach (MessageList messageList in messageListCollection)
            {
                if ((messageList.StatusMask & MessageStatus.New) == MessageStatus.New &&
                    (messageList.StatusMask & MessageStatus.Show) == MessageStatus.Show &&
                    (messageList.StatusMask & MessageStatus.Read) != MessageStatus.Read &&
                    (messageList.StatusMask & MessageStatus.FraudHold) != MessageStatus.FraudHold &&
                    (messageList.StatusMask & MessageStatus.MemberRemove) != MessageStatus.MemberRemove &&
                    (messageList.StatusMask & MessageStatus.Deleted) != MessageStatus.Deleted &&
                    (messageList.MailTypeID != MailType.InstantMessage))
                {
                    Dictionary<MailType,int> unreadMessages=null;
                    EmailFolder folder = EmailFolderSA.Instance.RetrieveFolder(memberID, brand.Site.Community.CommunityID, messageList.MemberFolderID, false);
                    if (!unreadMessageDictionary.ContainsKey(folder.Description))
                    {
                        unreadMessages = new Dictionary<MailType, int>();
                        unreadMessageDictionary.Add(folder.Description, unreadMessages);
                    }

                    unreadMessages = unreadMessageDictionary[folder.Description];
                    if (!unreadMessages.ContainsKey(messageList.MailTypeID))
                    {
                        unreadMessages.Add(messageList.MailTypeID, 0);
                    }
                    
                                                            
                    //only get unread messages that are in inbox
                    unreadMessages[messageList.MailTypeID]++;
                }
            }

            return unreadMessageDictionary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="memberFolderID"></param>
        /// <param name="messageStatus">MessageStatus to look for</param>
        /// <param name="inverseCount">Want inverse count of the MessageStatus you passed in?</param>
        /// <returns></returns>
        public int GetMessageCountByStatus(int memberID, Brand brand, int memberFolderID, MessageStatus messageStatus, bool inverseCount, bool includeFlirtAndEcards)
        {
            int count = 0;

            int groupID = brand.Site.Community.CommunityID;
            MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);

            bool includeInCount = false;
            foreach (MessageList messageList in messageListCollection)
            {
                includeInCount = false;
                if (messageList.MemberFolderID == memberFolderID)
                {
                    if (includeFlirtAndEcards)
                    {
                        includeInCount = true;
                    }
                    else
                    {
                        if (messageList.MailTypeID == MailType.Tease || messageList.MailTypeID == MailType.ECard)
                        {
                            includeInCount = false;
                        }
                        else
                        {
                            includeInCount = true;
                        }
                    }

                    if (inverseCount)
                    {
                        if (includeInCount && (messageList.StatusMask & messageStatus) == MessageStatus.Nothing)
                            count++;
                    }
                    else
                    {
                        if (includeInCount && (messageList.StatusMask & messageStatus) == messageStatus)
                            count++;
                    }
                }
            }

            return count;
        }

        public ArrayList RetrieveMessagesByStatus(int memberID, Brand brand, int memberFolderID, MessageStatus messagestatus, bool messagestatusequal, string orderby, int numbertoreturn)
        {
            string uri = "";

            try
            {
                int groupID = brand.Site.Community.CommunityID;
                MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);

                EmailMessageCollection messages = new EmailMessageCollection(memberID, groupID, memberFolderID);

                lock (messageListCollection.SyncRoot)
                {
                    foreach (MessageList messageList in messageListCollection)
                    {
                        if (messageList.MemberFolderID == memberFolderID)
                        {
                            if (messagestatusequal)
                            {
                                if ((messageList.StatusMask & messagestatus) == messagestatus)
                                {
                                    EmailMessage message = new EmailMessage(messageList);
                                    messages.Add(message);
                                }
                            }
                            else
                            {
                                if ((messageList.StatusMask & messagestatus) == MessageStatus.Nothing)
                                {
                                    EmailMessage message = new EmailMessage(messageList);
                                    messages.Add(message);
                                }
                            }
                        }
                    }
                }

                EmailComparer comparer = EmailComparer.FromString(orderby);

                if (comparer.SortTypeValue == EmailComparer.SortType.Subject || comparer.SortTypeValue == EmailComparer.SortType.OpenDate)
                {
                    //Need to retrieve full message data for all messages so that we have all the subject lines to sort by
                    populateMessages(memberID, messages);
                }
                else if (comparer.SortTypeValue == EmailComparer.SortType.FromUserName || comparer.SortTypeValue == EmailComparer.SortType.ToUserName)
                {
                    populateUsernames(messages, brand);
                }

                ArrayList sorted = messages.Sort(comparer);

                int returncount = sorted.Count > numbertoreturn ? numbertoreturn : sorted.Count;

                return sorted.GetRange(0, returncount);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve emails (uri: " + uri + ")", ex));
            }
        }

        #endregion

        #region IM History
        public EmailMessageList RetrieveInstantMessages(int memberID, int targetMemberID, Int32 groupID, int startRow, int pageSize, bool ignoreCache)
        {
            return RetrieveInstantMessages(memberID, targetMemberID, groupID, startRow, pageSize, "", ignoreCache);
        }

        public EmailMessageList RetrieveInstantMessages(int memberID, int targetMemberID, Int32 groupID, int startRow, int pageSize, string threadID, bool ignoreCache)
        {
            EmailMessageList emailMessageListFull = null;
            try
            {
                if (!ignoreCache)
                {
                    //check distributed cache
                    emailMessageListFull = _cacheWebBucket.Get(EmailMessageList.GetCacheKey(memberID, targetMemberID, groupID, ConversationType.Chat, MailType.InstantMessage)) as EmailMessageList;
                }

                if (emailMessageListFull != null && startRow <= 1)
                {
                    //if cached member list doesn't exist or is newer, we'll ignore message cache to support deleted messages
                    MessageMemberCollection messageMembers = _cacheWebBucket.Get(MessageMemberCollection.GetCacheKey(memberID, groupID, MailType.InstantMessage, ConversationType.Chat, true)) as MessageMemberCollection;
                    if (messageMembers == null || emailMessageListFull.CachedDate < messageMembers.CachedDate)
                    {
                        emailMessageListFull = null;

                        //refresh member list
                        if (messageMembers == null)
                        {
                            RetrieveInstantMessageMembers(memberID, groupID, true, false);
                        }
                    }
                    else //auto-refresh if new chat existed between these members
                    {
                        LastConversation lastConversation = GetInstantMessengerLastConversation(memberID, targetMemberID, groupID, true);     
                        if (lastConversation != null)
                        {
                            if (lastConversation.InsertDate > messageMembers[0].InsertDate)
                            {
                                emailMessageListFull = null;
                            }
                        }
                    }
                }

                if (emailMessageListFull == null)
                {
                    string uri = getServiceManagerUri(memberID);

                    base.Checkout(uri);
                    try
                    {
                        emailMessageListFull = getService(uri).RetrieveInstantMessages(memberID, targetMemberID, groupID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    //save to distributed cache
                    _cacheWebBucket.Insert(emailMessageListFull);
                }

                if (emailMessageListFull != null && startRow > 0 && pageSize > 0)
                {
                    //paging
                    EmailMessageList emailMessageListPage = new EmailMessageList(memberID, targetMemberID, groupID, ConversationType.Chat, MailType.InstantMessage);
                    int totalCount = 0;
                    emailMessageListPage.ReplaceList(emailMessageListFull.GetPage(startRow, pageSize, threadID, false, out totalCount));
                    emailMessageListPage.TotalCount = totalCount;

                    return emailMessageListPage;
                }

                return emailMessageListFull;
            }
            catch (Exception Ex)
            {
                throw (new SAException(string.Format("Error retrieving IM History messages. memberID: {0}, targetMemberID: {1}, groupID: {2}, ignoreCache: {3}", memberID, targetMemberID, groupID, ignoreCache), Ex));
            }            
        }

        public MessageMemberCollection RetrieveInstantMessageMembers(int memberID, Int32 groupID, bool filterRemovedMembers, bool ignoreCache)
        {
            return RetrieveInstantMessageMembers(memberID, groupID, filterRemovedMembers, Constants.NULL_INT, Constants.NULL_INT, ignoreCache);
        }

        public MessageMemberCollection RetrieveInstantMessageMembers(int memberID, Int32 groupID, bool filterRemovedMembers, int startRow, int pageSize, bool ignoreCache)
        {
            MessageMemberCollection messageMembers = null;
            try
            {
                if (!ignoreCache || startRow > 1)
                {
                    //check distributed cache
                    messageMembers = _cacheWebBucket.Get(MessageMemberCollection.GetCacheKey(memberID, groupID, MailType.InstantMessage, ConversationType.Chat, filterRemovedMembers)) as MessageMemberCollection;
                }

                if (messageMembers == null)
                {
                    string uri = getServiceManagerUri(memberID);

                    base.Checkout(uri);
                    try
                    {
                        messageMembers = getService(uri).RetrieveInstantMessageMembers(memberID, groupID, filterRemovedMembers);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    //save to distributed cache
                    _cacheWebBucket.Insert(messageMembers);
                }

                //paging
                if (messageMembers != null && startRow > 0 && pageSize > 0)
                {
                    MessageMemberCollection emailMessageListPage = new MessageMemberCollection(memberID, groupID, MailType.InstantMessage, ConversationType.Chat, filterRemovedMembers);
                    emailMessageListPage.TotalCount = messageMembers.TotalCount;
                    emailMessageListPage.ReplaceList(messageMembers.GetPage(startRow, pageSize));

                    return emailMessageListPage;
                }

                return messageMembers;
            }
            catch (Exception Ex)
            {
                throw (new SAException(string.Format("Error retrieving IM History message members. memberID: {0}, groupID: {1}, ignoreCache: {2}", memberID, groupID, ignoreCache), Ex));
            }
        }

        public LastConversation GetInstantMessengerLastConversation(int fromMemberID, int targetMemberID, int groupID, bool cacheOnly)
        {
            LastConversation lastConversation = _cacheWebBucket.Get(LastConversation.GetCacheKey(fromMemberID, targetMemberID, groupID, MailType.InstantMessage)) as LastConversation;
            if (lastConversation == null && !cacheOnly)
            {
                EmailMessageList messageList = RetrieveInstantMessages(fromMemberID, targetMemberID, groupID, 1, 1, true);
                if (messageList != null && messageList.Count > 0)
                {
                    EmailMessage emailMessage = messageList[0];
                    if (emailMessage != null)
                    {
                        lastConversation = new LastConversation(emailMessage);
                    }
                }
            }

            return lastConversation;
        }


        #endregion

        #region Threaded Conversation Mail
        public EmailMessageList RetrieveMailConversationMessages(int memberID, int targetMemberID, Int32 groupID, int startRow, int pageSize, ConversationType conversationType, bool markAsRead)
        {
            EmailMessageList emailMessageListFull = null;
            bool unreadOnly = false;

            if (pageSize <= 0)
            {
                //hard limit, consider making this a setting but clients shouldn't ever be going past this for a single call
                pageSize = 1000;
            }

            try
            {
                //chat history messages should to go chat specific method for now
                if (conversationType == ConversationType.Chat)
                {
                    return RetrieveInstantMessages(memberID, targetMemberID, groupID, startRow, pageSize, false);
                }

                if (conversationType == ConversationType.None || conversationType == ConversationType.Trash)
                {
                    conversationType = ConversationType.Inbox;
                }
                else if (conversationType == ConversationType.Unread)
                {
                    unreadOnly = true;
                    conversationType = ConversationType.Inbox;
                }

                //check distributed cache
                emailMessageListFull = _cacheWebBucket.Get(EmailMessageList.GetCacheKey(memberID, targetMemberID, groupID, conversationType, MailType.None)) as EmailMessageList;

                if (emailMessageListFull != null && startRow <= 1)
                {
                    //if cached member list doesn't exist or is newer, we'll ignore message cache to support deleted messages
                    MessageMemberCollection messageMembers = _cacheWebBucket.Get(MessageMemberCollection.GetCacheKey(memberID, groupID, MailType.None, conversationType)) as MessageMemberCollection;
                    if (messageMembers == null || emailMessageListFull.CachedDate < messageMembers.CachedDate)
                    {
                        emailMessageListFull = null;

                        //refresh member list
                        if (messageMembers == null)
                        {
                            RetrieveMailConversationMembers(memberID, groupID, conversationType);
                        }
                    }
                }

                if (emailMessageListFull == null)
                {
                    string uri = getServiceManagerUri(memberID);

                    base.Checkout(uri);
                    try
                    {
                        emailMessageListFull = getService(uri).RetrieveMailConversationMessages(memberID, targetMemberID, groupID, conversationType);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    //save to distributed cache
                    _cacheWebBucket.Insert(emailMessageListFull);
                }

                //paging
                if (emailMessageListFull != null)
                {
                    EmailMessageList emailMessageListPage = new EmailMessageList(memberID, targetMemberID, groupID, conversationType, MailType.None);
                    int totalCount = 0;
                    emailMessageListPage.ReplaceList(emailMessageListFull.GetPage(startRow, pageSize, "", unreadOnly, out totalCount));
                    emailMessageListPage.TotalCount = totalCount;

                    if (markAsRead && emailMessageListPage.Count > 0)
                    {
                        ArrayList readList = new ArrayList();
                        foreach (EmailMessage em in emailMessageListPage)
                        {
                            if ((em.StatusMask & MessageStatus.Read) != MessageStatus.Read)
                            {
                                em.StatusMask = em.StatusMask | MessageStatus.Read;
                                readList.Add(em.MemberMailID);
                            }
                        }

                        if (readList.Count > 0)
                        {
                            MarkRead(memberID, groupID, true, readList);

                            //save to distributed cache
                            _cacheWebBucket.Insert(emailMessageListFull);
                        }
                    }

                    return emailMessageListPage;
                }

                return emailMessageListFull;
            }
            catch (Exception Ex)
            {
                throw (new SAException(string.Format("Error retrieving Mail Conversation inbox messages. memberID: {0}, targetMemberID: {1}, groupID: {2}, conversationType: {3}", memberID, targetMemberID, groupID, conversationType), Ex));
            }
        }

        public MessageMemberCollection RetrieveMailConversationMembers(int memberID, Int32 groupID, ConversationType conversationType)
        {
            return RetrieveMailConversationMembers(memberID, groupID, conversationType, Constants.NULL_INT, Constants.NULL_INT);
        }

        public MessageMemberCollection RetrieveMailConversationMembers(int memberID, Int32 groupID, ConversationType conversationType, int startRow, int pageSize)
        {
            MessageMemberCollection messageMembers = null;
            bool unreadOnly = false;

            if (pageSize <= 0)
            {
                //hard limit, consider making this a setting but clients shouldn't ever be going past this for a single call
                pageSize = 1000;
            }

            try
            {
                //chat should go to chat specific method for now
                if (conversationType == ConversationType.Chat)
                {
                    return RetrieveInstantMessageMembers(memberID, groupID, true, false);
                }

                if (conversationType == ConversationType.None)
                {
                    throw new Exception("Valid conversation type needs to be provided.");
                }
                else if (conversationType == ConversationType.Unread)
                {
                    unreadOnly = true;
                    conversationType = ConversationType.Inbox;
                }

                //check distributed cache
                messageMembers = _cacheWebBucket.Get(MessageMemberCollection.GetCacheKey(memberID, groupID, MailType.None, conversationType)) as MessageMemberCollection;

                if (messageMembers == null)
                {
                    string uri = getServiceManagerUri(memberID);

                    base.Checkout(uri);
                    try
                    {
                        messageMembers = getService(uri).RetrieveMailConversationMembers(memberID, groupID, conversationType);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    //save to distributed cache
                    _cacheWebBucket.Insert(messageMembers);

                }

                //paging
                if (messageMembers != null)
                {
                    MessageMemberCollection emailMessageListPage = new MessageMemberCollection(memberID, groupID, MailType.None, conversationType);
                    int totalCount = 0;
                    emailMessageListPage.ReplaceList(messageMembers.GetPage(startRow, pageSize, unreadOnly, out totalCount));
                    emailMessageListPage.TotalCount = totalCount;

                    return emailMessageListPage;
                }

                return messageMembers;
            }
            catch (Exception Ex)
            {
                throw (new SAException(string.Format("Error retrieving Mail Conversation inbox message members. memberID: {0}, groupID: {1}, conversationType: {2}", memberID, groupID, conversationType), Ex));
            }
        }

        public void RemoveMailConversationMembersCache(int memberID, int groupID, ConversationType conversationType)
        {
            if (memberID > 0 && groupID > 0)
            {
                CacheUtil.RemoveMailConversationMembersCache(_cacheWebBucket, memberID, groupID, conversationType);
            }
        }
        #endregion

        #region Message Attachments
        public MessageAttachmentSaveResult SaveMessageAttachment(int fromMemberID, int toMemberID, byte[] fileBytes, string extension, AttachmentType attachmentType, int messageID, int communityID, int siteID, Spark.Common.MemberLevelTracking.Application app)
        {
            MessageAttachmentSaveResult result = null;
            string uri = string.Empty;

            //call File Service to create file record, save physical file to the cloud and get back web paths
            CreateNewFileResult createNewFileResult = null;
            try
            {
                if (attachmentType == AttachmentType.IMPhoto)
                {
                    //verify image
                    System.Drawing.Image image = null;
                    MemoryStream streamIn;
                    MemoryStream streamOut;
                    try
                    {
                        streamIn = new MemoryStream(fileBytes);
                        image = System.Drawing.Image.FromStream(streamIn, true);
                        streamOut = new MemoryStream();
                        image.Save(streamOut, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch (Exception ex)
                    {
                        result = new MessageAttachmentSaveResult();
                        result.Status = MessageAttachmentSaveStatus.Failed;
                        result.Error = MessageAttachmentSaveError.InvalidPhoto;
                    }
                }

                //save file
                createNewFileResult = FileSA.Instance.SaveNewFile(fileBytes,
                    extension,
                    communityID,
                    siteID, fromMemberID, true, Matchnet.File.ValueObjects.FileType.MessageAttachmentPhoto);
            }
            catch (Exception ex)
            {
                throw new SAException(String.Format("EmailMessageSA.SaveMessageAttachment(): Save attachment file error. URI: {0}, fromMemberID: {1}, toMemberID: {2}, attachmentType: {3}, siteID: {4}", uri, fromMemberID, toMemberID, attachmentType, siteID), ex);
            }

            if (createNewFileResult == null || createNewFileResult.Success == false)
            {
                result = new MessageAttachmentSaveResult();
                result.Status = MessageAttachmentSaveStatus.Failed;
                result.Error = MessageAttachmentSaveError.SavePhysicalFileError;

                return result;
            }

            //save message attachment record
            try
            {
                uri = getServiceManagerUri(fromMemberID);
                base.Checkout(uri);
                result = getService(uri)
                    .SaveMessageAttachment(fromMemberID, toMemberID, createNewFileResult.FileID, createNewFileResult.CloudPath, createNewFileResult.OriginalFileCloudPath, attachmentType, messageID,
                                            communityID, siteID, (int) app);

                //include full cloud paths for response
                Client cloudClient = new Client(communityID, siteID, RuntimeSettings.Instance, FileType.MessageAttachment);
                result.OriginalCloudPath = cloudClient.GetFullCloudImagePath(result.OriginalCloudPath,
                                                                             FileType.MessageAttachment, false);
                result.PreviewCloudPath = cloudClient.GetFullCloudImagePath(result.PreviewCloudPath,
                                                                             FileType.MessageAttachment, false);
                return result;
            }
            catch (Exception ex)
            {
                throw new SAException(String.Format("EmailMessageSA.SaveMessageAttachment(): Save attachment record error. URI: {0}, fromMemberID: {1}, toMemberID: {2}, attachmentType: {3}, siteID: {4}", uri, fromMemberID, toMemberID, attachmentType, siteID), ex);
            }
            finally
            {
                base.Checkin(uri);
            }
            
        }
        #endregion

        #region Remoting Methods

        private IEmailMessageService getService(string uri)
        {
            try
            {
                return (IEmailMessageService)Activator.GetObject(typeof(IEmailMessageService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        private string getServiceManagerUri(int memberID)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }
        #endregion

        #region Conversion Methods

        //Methods used to convert MessageList/Message objects that represent DB records
        //to EmailMessageCollection/EmailMessage objects used by the web code

        private EmailMessageCollection retrieveMessages(int memberID, int groupID, int memberFolderID)
        {
            string uri = "";

            try
            {
                MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);

                EmailMessageCollection messages = new EmailMessageCollection(memberID, groupID, memberFolderID);

                lock (messageListCollection.SyncRoot)
                {
                    foreach (MessageList messageList in messageListCollection)
                    {
                        if (messageList.MemberFolderID == memberFolderID)
                        {
                            EmailMessage message = new EmailMessage(messageList);
                            messages.Add(message);
                        }
                    }
                }

                return messages;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve emails (uri: " + uri + ")", ex));
            }
        }

        private EmailMessage retrieveEmailMessage(Int32 memberID, Int32 groupID, Int32 messageListID, bool withContent, bool ignoreSACache)
        {
            MessageListCollection messages = retrieveMessageList(memberID, groupID, ignoreSACache);
            MessageList messageList = messages[messageListID];

            if (messageList == null)
            {
                return null;
            }

            Int32 messageID = messageList.MessageID;

            Message message = retrieveMessage(memberID, messageID, withContent, ignoreSACache);

            return new EmailMessage(messageList, message);
        }

        #endregion

        #region MessageList Retrieval Methods

        private MessageListCollection retrieveCachedMessageList(Int32 memberID, Int32 groupID)
        {
            return _cache.Get(MessageListCollection.GetCacheKey(memberID, groupID)) as MessageListCollection;
        }

        private void cacheMessageList(MessageListCollection messageListCollection)
        {
            _cache.Insert(messageListCollection);
        }

        private MessageListCollection retrieveMessageListFromDB(Int32 memberID, Int32 groupID)
        {
            string uri = "";

            try
            {
                MessageListCollection messageListCollection = null;
                uri = getServiceManagerUri(memberID);
                int cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));
                base.Checkout(uri);
                try
                {
                    messageListCollection = getService(uri).RetrieveMessageListFromDB(System.Environment.MachineName,
                        Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                        memberID,
                        groupID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                // This data shouldn't be cached because some unwanted data will come back and show messages that we shouldn't show
                //messageListCollection.CacheTTLSeconds = cacheTTL;

                //cacheMessageList(messageListCollection);

                return messageListCollection;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve emails directly from DB (uri: " + uri + ")", ex));
            }
        }

        private MessageListCollection retrieveMessageList(Int32 memberID, Int32 groupID, bool ignoreSACache)
        {
            string uri = "";

            try
            {
                MessageListCollection messageListCollection = null;
                if(!ignoreSACache)
                    messageListCollection = retrieveCachedMessageList(memberID, groupID);

                if (messageListCollection == null)
                {
                    uri = getServiceManagerUri(memberID);

                    int cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));

                    base.Checkout(uri);
                    try
                    {
                        messageListCollection = getService(uri).RetrieveMessageList(System.Environment.MachineName,
                            Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                            memberID,
                            groupID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    messageListCollection.CacheTTLSeconds = cacheTTL;

                    cacheMessageList(messageListCollection);
                }

                return messageListCollection;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve emails (uri: " + uri + ")", ex));
            }
        }


        private MessageMemberCollection retrieveMessageMembers(Int32 memberID, Int32 groupID, bool ignoreSACache)
        {
            string uri = "";

            try
            {
                MessageMemberCollection messageMemberCollection = null;
                //if (!ignoreSACache)
                //    messageMemberCollection = retrieveCachedMessageList(memberID, groupID);

                if (messageMemberCollection == null)
                {
                    uri = getServiceManagerUri(memberID);

                    int cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));

                    base.Checkout(uri);
                    try
                    {
                        messageMemberCollection = getService(uri).RetrieveMessageMemberFromDB(System.Environment.MachineName,
                            Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                            memberID,
                            groupID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    messageMemberCollection.CacheTTLSeconds = cacheTTL;

                    //cacheMessageList(messageMemberCollection);
                }

                return messageMemberCollection;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve emails (uri: " + uri + ")", ex));
            }
        }

        #endregion

        #region Photo for messageMember
        public class FinalPhoto
        {
            public Int32 MemberPhotoId { get; set; }
            public Int32 FullId { get; set; }
            public Int32 ThumbId { get; set; }
            public String FullPath { get; set; }
            public String ThumbPath { get; set; }
            public String Caption { get; set; }
            public byte ListOrder { get; set; }
            public Boolean IsCaptionApproved { get; set; }
            public Boolean IsPhotoApproved { get; set; }
        }

        public enum PhotoType
        {
            Full = 1,
            Thumbnail
        }
        public static string GetPhotoUrl(Photo photo, EmailMessageSA.PhotoType photoType, Brand brand)
        {
            string virtualFilePath = string.Empty;
            bool flag = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));
            if (photo == null)
                throw new ArgumentNullException("photo");
            switch (photoType)
            {
                case EmailMessageSA.PhotoType.Full:
                    if (photo.FileWebPath != null)
                    {
                        virtualFilePath = flag ? photo.FileCloudPath : photo.FileWebPath;
                        break;
                    }
                    else
                        break;
                case EmailMessageSA.PhotoType.Thumbnail:
                    if (photo.ThumbFileWebPath != null)
                    {
                        virtualFilePath = flag ? photo.ThumbFileCloudPath : photo.ThumbFileWebPath;
                        break;
                    }
                    else
                        break;
            }
            if (string.IsNullOrEmpty(virtualFilePath))
                return virtualFilePath;
            else
                return !flag ? virtualFilePath : new Client(brand.Site.Community.CommunityID, brand.Site.SiteID, (ISettingsSA)RuntimeSettings.Instance).GetFullCloudImagePath(virtualFilePath, FileType.MemberPhoto, false);
        }

        public static List<EmailMessageSA.FinalPhoto> GetPhotos(Brand brand, int memberId, int viewerId)
        {
            try
            {
                PhotoCommunity photos = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None).GetPhotos(brand.Site.Community.CommunityID);
                List<EmailMessageSA.FinalPhoto> list = new List<EmailMessageSA.FinalPhoto>();
                for (byte index = (byte)0; (int)index < photos.Count; ++index)
                {
                    Photo photo = photos[index];
                    string photoUrl = EmailMessageSA.GetPhotoUrl(photo, EmailMessageSA.PhotoType.Full, brand);
                    if ((photo.IsApproved || viewerId == memberId) && !string.IsNullOrEmpty(photoUrl))
                    {
                        EmailMessageSA.FinalPhoto finalPhoto1 = new EmailMessageSA.FinalPhoto();
                        finalPhoto1.MemberPhotoId = photo.MemberPhotoID;
                        finalPhoto1.FullId = photo.FileID;
                        finalPhoto1.ThumbId = photo.ThumbFileID;
                        finalPhoto1.FullPath = photoUrl;
                        finalPhoto1.ThumbPath = EmailMessageSA.GetPhotoUrl(photo, EmailMessageSA.PhotoType.Thumbnail, brand);
                        finalPhoto1.Caption = photo.IsCaptionApproved || viewerId == memberId ? photo.Caption : string.Empty;
                        finalPhoto1.ListOrder = photo.ListOrder;
                        finalPhoto1.IsCaptionApproved = photo.IsCaptionApproved;
                        finalPhoto1.IsPhotoApproved = photo.IsApproved;
                        EmailMessageSA.FinalPhoto finalPhoto2 = finalPhoto1;
                        list.Add(finalPhoto2);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat(new object[4]
        {
          (object) "Error getting photos. MemberID:",
          (object) memberId,
          (object) ", ViewerID:",
          (object) viewerId
        }), ex);
            }
        }
        #endregion

        #region Message Retrieval Methods

        private Message retrieveCachedMessage(Int32 messageID, bool withContent)
        {
            Message message = _cache.Get(Message.GetCacheKey(messageID)) as Message;

            if (withContent && message != null && message.MessageBody == null)
            {
                return null;
            }

            return message;
        }

        private void cacheMessage(Message message)
        {
            _cache.Insert(message);
        }

        public Message RetrieveMessageByMessageID(Int32 memberID, Int32 messageID)
        {
            return this.retrieveMessage(memberID, messageID, true, false);
        }

        private Message retrieveMessage(Int32 memberID, Int32 messageID, bool withContent, bool ignoreSACache)
        {
            Message message = null;

            if(!ignoreSACache)
                message = retrieveCachedMessage(messageID, withContent);

            if (message == null)
            {
                //If the message wasn't already cached with content, retrieve it from the MT:
                string uri = getServiceManagerUri(memberID);
                Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));

                base.Checkout(uri);
                try
                {
                    message = getService(uri).RetrieveMessage(System.Environment.MachineName,
                        Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                        memberID,
                        messageID,
                        withContent);
                }
                finally
                {
                    base.Checkin(uri);
                }

                message.CacheTTLSeconds = cacheTTL;

                cacheMessage(message);
            }

            return message;
        }

        private MessageCollection retrieveMessages(Int32 memberID, ArrayList messageIDs, bool withContent)
        {
            string uri = getServiceManagerUri(memberID);
            MessageCollection messageCollection;
            Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));

            base.Checkout(uri);
            try
            {
                messageCollection = getService(uri).RetrieveMessages(System.Environment.MachineName,
                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                    memberID,
                    messageIDs,
                    withContent);
            }
            finally
            {
                base.Checkin(uri);
            }

            foreach (Message message in messageCollection)
            {
                message.CacheTTLSeconds = cacheTTL;
                cacheMessage(message);
            }

            return messageCollection;
        }

        private void populateMessages(Int32 memberID, ICollection emailMessages)
        {
            ArrayList messageIDs = new ArrayList(emailMessages.Count);

            lock (emailMessages.SyncRoot)
            {
                foreach (EmailMessage message in emailMessages)
                {
                    if (message.Message == null)
                    {
                        message.Message = retrieveCachedMessage(message.MailID, false);
                    }

                    if (message.Message == null)
                    {
                        messageIDs.Add(message.MailID);
                    }
                }
            }

            if (messageIDs.Count > 0)
            {
                MessageCollection messageCollection = retrieveMessages(memberID, messageIDs, false);

                lock (emailMessages.SyncRoot)
                {
                    foreach (EmailMessage message in emailMessages)
                    {
                        if (message.Message == null)
                        {
                            message.Message = messageCollection[message.MailID];
                        }
                    }
                }
            }
        }

        #endregion

        #region ReplicationAction Methods
        private MessageSendResult sendMessage(MessageSave messageSave, bool saveInSent, int replyMessageListID)
        {
            string senderUri = string.Empty;
            string recipientUri = string.Empty;

            try
            {
                Int32 senderMessageListID;
                Int32 recipientMessageListID = KeySA.Instance.GetKey("MemberMailID");
                Int32 messageID;
                bool draft = messageSave.MessageListID > 0;

                if (draft)  //Sending a draft
                {
                    senderMessageListID = messageSave.MessageListID;

                    MessageListCollection messageListCollection = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, false);
                    MessageList messageList = messageListCollection[senderMessageListID];

                    if (messageList == null)
                    {
                        messageListCollection = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, true);
                        messageList = messageListCollection[senderMessageListID];
                    }

                    messageID = messageList.MessageID;
                }
                else  //Sending a newly composed message
                {
                    senderMessageListID = KeySA.Instance.GetKey("MemberMailID");
                    messageID = KeySA.Instance.GetKey("MailID");
                }

                MessageSend messageSend = new MessageSend(messageSave,
                    senderMessageListID,
                    recipientMessageListID,
                    messageID,
                    draft);

                #region MailOption sets
                // let's determine if this is free reply to a vip message
                if (!draft && replyMessageListID > 0)
                {
                    MessageListCollection messageListCollection = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, false);
                    MessageList messageList = messageListCollection[replyMessageListID];

                    if ((messageList.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply)
                    {
                        messageSend.MessageSave.MailOption |= MailOption.FreeReplyMessage;
                    }
                }

                // FraudHold option check
                Matchnet.Member.ServiceAdapters.Member sender = MemberSA.Instance.GetMember(messageSend.MessageSave.FromMemberID, MemberLoadFlags.None);
               
                int brandID = Constants.NULL_INT;
                sender.GetLastLogonDate(messageSend.MessageSave.GroupID, out brandID);

                if (brandID != Constants.NULL_INT)
                {
                    Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
                    if (Convert.ToBoolean(RuntimeSettings.GetSetting("EMAIL_FRAUD_HOLD", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)) &&
                        !sender.GetAttributeBool(brand, "PassedFraudCheckSite"))
                    {
                         messageSend.MessageSave.MailOption |= MailOption.FraudHold;
                        
                    }
                }
                #endregion

                #region BL calls for sender and recipient
                senderUri = getServiceManagerUri(messageSave.FromMemberID);
                recipientUri = getServiceManagerUri(messageSave.ToMemberID);

                MessageSendResult result;

                if (senderUri == recipientUri)
                {
                    base.Checkout(senderUri);
                    try
                    {
                        result = getService(senderUri).SendMessage(System.Environment.MachineName,
                            messageSend,
                            saveInSent,
                            replyMessageListID,
                            true,
                            true);
                    }
                    finally
                    {
                        base.Checkin(senderUri);
                    }
                }
                else
                {
                    base.Checkout(senderUri);
                    try
                    {
                        result = getService(senderUri).SendMessage(System.Environment.MachineName,
                            messageSend,
                            saveInSent,
                            replyMessageListID,
                            true,
                            false);
                    }
                    finally
                    {
                        base.Checkin(senderUri);
                    }

                    if (result.Status == MessageSendStatus.Success)
                    {
                        base.Checkout(recipientUri);
                        try
                        {
                            result = getService(recipientUri).SendMessage(System.Environment.MachineName,
                                messageSend,
                                saveInSent,
                                replyMessageListID,
                                false,
                                true);
                        }
                        finally
                        {
                            base.Checkin(recipientUri);
                        }
                    }
                }
                #endregion

                #region Post deliver process
                if (result.Status == MessageSendStatus.Success)
                {
                    //updating inbox message caching and new mail flags not needed for IM (we don't want IM showing up in inbox)
                    if (messageSave.MailType != MailType.InstantMessage)
                    {
                        if (replyMessageListID > 0)
                        {	// this is a response, set the reply status on the original message.
                            setRepliedBit(messageSave.FromMemberID, messageSave.GroupID, replyMessageListID);
                        }

                        MessageListCollection senderMessageListCollection = retrieveCachedMessageList(messageSave.FromMemberID, messageSave.GroupID);

                        Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SA"));

                        ReplicationActionCollection actions = new ReplicationActionCollection();
                        actions.Add(messageSend.GetMessageSaveAction(true, cacheTTL));
                        if (saveInSent)
                        {
                            actions.Add(messageSend.GetMessageListSaveAction(true));
                        }

                        MessageListSaveAction recipientListAction = (MessageListSaveAction)messageSend.GetMessageListSaveAction(false);
                        addFreeReplyToListAction(replyMessageListID, messageSave, recipientListAction);
                        // check to see if this is a FraudHold send
                        if ((messageSend.MessageSave.MailOption & MailOption.FraudHold) == MailOption.FraudHold)
                        {
                            recipientListAction.MessageList.StatusMask |= MessageStatus.FraudHold;
                        }
                        actions.Add(recipientListAction);
                        CacheUtil.PlayActions(_cache, _cacheWebBucket, actions);

                        updateRecipientMemberForHasNewMail(messageSave, recipientMessageListID);
                    }
                }
                #endregion

                result.EmailMessage = new EmailMessage(messageSave.GetMessageList(true, result.MessageID, result.SenderMessageListID, (Int32)SystemFolders.Sent),
                    messageSave.GetMessage(result.MessageID, 0));

                return result;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot send email (uri: " + senderUri + ")", ex));
            }
        }

        // Handles setting the replied bit for the Member/MemberMail specified
        private void setRepliedBit(Int32 memberID, Int32 groupID, Int32 messageListID)
        {
            try
            {
                MessageListCollection messageListCollection = retrieveCachedMessageList(memberID, groupID);

                if (messageListCollection != null)
                {
                    MessageList messageList = messageListCollection[messageListID];
                    if (messageList != null && (messageList.StatusMask & MessageStatus.Replied) != MessageStatus.Replied)
                    {
                        messageList.StatusMask = (MessageStatus)messageList.StatusMask | MessageStatus.Replied;
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Error occured when setting message reply.", ex));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns>A replication action that can be used to persist this change to the MT and DB.
        /// Returns null if the cached copy of the message is already opened. </returns>
        private IReplicationAction markOpened(EmailMessage message)
        {
            try
            {
                DateTime openDate = DateTime.Now;

                Message senderMessage = retrieveCachedMessage(message.MailID, false);

                if (senderMessage != null && senderMessage.OpenDate != DateTime.MinValue)
                {
                    return null;
                }

                return new MessageOpenedAction(message.FromMemberID, message.ToMemberID, message.MailID, openDate);
            }
            catch (Exception ex)
            {
                throw new SAException("Could not mark message as opened.  MessageID: " + message.MailID, ex);
            }
        }

        private IReplicationAction markOpened(int messageListID, int memberID, int groupID)
        {
            try
            {
                DateTime openDate = DateTime.Now;

                MessageList messageList = retrieveMessageList(memberID, groupID, false)[messageListID];

                if (messageList != null && messageList.ToMemberID == memberID)
                {
                    return new MessageOpenedAction(messageList.FromMemberID, messageList.ToMemberID, messageList.MessageID, openDate);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new SAException("Could not mark message as opened.  MessageListID: " + messageListID, ex);
            }
        }
      
        private IReplicationAction markOneFreeReply(int pMemberId,
            int pGroupId,
            bool pOneFreeReply,
            ArrayList pMessageListIds,
            bool ignoreSACache)
        {
            RollingFileLogger.Instance.EnterMethod(CallerAppName, "EmailMessageSA", "markOneFreeReply - ArrayList pMessageListIds version");

            ReplicationActionCollection actions = new ReplicationActionCollection();

            foreach (Int32 messageListID in pMessageListIds)
            {
                actions.Add(markOneFreeReply(pMemberId, pGroupId, pOneFreeReply, messageListID, ignoreSACache));
            }

            RollingFileLogger.Instance.LeaveMethod(CallerAppName, "EmailMessageSA", "markOneFreeReply - ArrayList pMessageListIds version");

            return actions.Count == 0 ? null : actions;
        }

        private IReplicationAction markOneFreeReply(int pMemberId,
            int pGroupId,
            bool pOneFreeReply,
            int pMessageListId,
            bool ignoreSACache)
        {
            RollingFileLogger.Instance.EnterMethod(CallerAppName, "EmailMessageSA",
                                                   "markOneFreeReply - Single pMessageListId version");

            RollingFileLogger.Instance.LogInfoMessage(CallerAppName, "EmailMessageSA",
                                                      string.Format(
                                                          "Retrieiving MessageList with MemberID: {0} GroupID: {1} IgnoreCache: {2} MessageListID: {3}",
                                                          pMemberId, pGroupId, ignoreSACache, pMessageListId), null);

            MessageList messageList = retrieveMessageList(pMemberId, pGroupId, ignoreSACache)[pMessageListId];

            RollingFileLogger.Instance.LogInfoMessage(CallerAppName, "EmailMessageSA",
                                                      string.Format("MessageList came back {0}",
                                                                    messageList == null ? "null" : "with value"), null);

            IReplicationAction action = null;

            if (messageList != null)
            {
                MessageStatus messageStatus = messageList.StatusMask;
                bool isOneFreeReply = (messageStatus & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply;

                // do we need to update even
                if (pOneFreeReply != isOneFreeReply)
                {
                    if (pOneFreeReply)
                    {
                        messageStatus |= MessageStatus.OneFreeReply;
                    }
                    else
                    {
                        messageStatus &= ~MessageStatus.OneFreeReply;
                    }
                    messageList.StatusMask = messageStatus;

                    action = new MessageListSaveAction(pMemberId, MessageListSaveType.Update, messageList);
                }
            }

            RollingFileLogger.Instance.LeaveMethod(CallerAppName, "EmailMessageSA", "markOneFreeReply - Single pMessageListId version");

            return action;
        }

        private IReplicationAction markRead(Int32 memberID,
            Int32 groupID,
            bool read,
            ArrayList messageListIDs)
        {
            ReplicationActionCollection actions = new ReplicationActionCollection();

            foreach (Int32 messageListID in messageListIDs)
            {
                IReplicationAction actionRead = markRead(memberID, groupID, read, messageListID);
                actions.Add(actionRead);

                if (read && actionRead != null)
                {
                    //mark as open
                    actions.Add(markOpened(messageListID, memberID, groupID));
                }
            }

            if (actions.Count == 0)
            {
                return null;
            }

            return actions;
        }

        private IReplicationAction markRead(Int32 memberID,
            Int32 groupID,
            bool read,
            Int32 messageListID)
        {
            MessageList messageList = retrieveMessageList(memberID, groupID, false)[messageListID];
            IReplicationAction action = null;
            bool callEmailTracker = false;

            if (messageList != null)
            {
                MessageStatus messageStatus = messageList.StatusMask;
                bool isRead = (messageStatus & MessageStatus.Read) == MessageStatus.Read;
                if (read != isRead)
                {
                    if (read)
                    {
                        messageStatus |= MessageStatus.Read;
                        messageStatus &= ~MessageStatus.New;
                        callEmailTracker = true; 
                    }
                    else
                    {
                        messageStatus &= ~MessageStatus.Read;
                    }
                    messageList.StatusMask = messageStatus;

                    action = new MessageListSaveAction(memberID, MessageListSaveType.Update, messageList);
                }
            }

            if (action != null)
            {
                // if marking the message as 'Read', we need to update the member record
                // in the event that 'HasNewMail' is no longer true as a result of reading this message.
                updateRecipientMemberForHasNewMail(memberID, groupID);

                if (callEmailTracker)
                {
                    updateEmailTracker(messageList);
                }
            }

            return action;
        }

        private IReplicationAction markOffFraudHold(Int32 memberID,
            Int32 groupID,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB)
        {
            ReplicationActionCollection actions = new ReplicationActionCollection();

            foreach (Int32 messageListID in messageListIDs)
            {
                actions.Add(markOffFraudHold(memberID, groupID, messageListID, messageListColFromDB));
            }

            if (actions.Count == 0)
            {
                return null;
            }

            return actions;
        }

        private IReplicationAction markDeleted(Int32 memberID,
            Int32 groupID,
            bool isDeleted,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB)
        {
            ReplicationActionCollection actions = new ReplicationActionCollection();

            foreach (Int32 messageListID in messageListIDs)
            {
                actions.Add(markDeleted(memberID, groupID, messageListID, isDeleted, messageListColFromDB));
            }

            if (actions.Count == 0)
            {
                return null;
            }

            return actions;
        }

        /// <summary>
        /// Use this method to soft delete/undelete a messagelist
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListID"></param>
        /// <param name="isDeleted">Set to true to soft delete and false to soft undelete</param>
        /// <returns></returns>
        private IReplicationAction markDeleted(Int32 memberID,
            Int32 groupID,
            Int32 messageListID,
            bool isDeleted,
            MessageListCollection messageListColFromDB)
        {
            MessageListSaveType mSaveType = MessageListSaveType.Delete;
            if (!isDeleted)
            {
                mSaveType = MessageListSaveType.Undelete;
            }

            return markMessageStatus(memberID, groupID, messageListID, isDeleted, MessageStatus.Deleted, mSaveType, true, true, messageListColFromDB);
        }

        private IReplicationAction markOffFraudHold(Int32 memberID,
            Int32 groupID,
            Int32 messageListID,
            MessageListCollection messageListColFromDB)
        {

            return markMessageStatus(memberID, groupID, messageListID, false, MessageStatus.FraudHold, MessageListSaveType.Update, false, false, messageListColFromDB);
        }
        
        /// <summary>
        /// This method should be used to mark a bit position of a MessageStatus on/off
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <param name="messageListID"></param>
        /// <param name="setMessageStatusOn">Set the specified bit on/off</param>
        /// <param name="messageStatusToSet">Bit position of the MessageStatus flags</param>
        /// <param name="messageListSaveType">Insert/Update/Delete. Specify the save type so BL can handle this properly</param>
        /// <param name="markOffNewStatus">If this is set and setMessageStatusOn is true, MessageStatus.New will marked off if the update actually happens</param>
        /// <param name="callOffEmailTracker">If this is set and setMessageStatusOn is true, EmailTracker will no longer track this message</param>
        /// <param name="updateGotNewMailStatus">Modify the member attribute of HasNewMail. If calling from a batch job, the caller should update this once, but if this method is being called for one message, set this to true.</param>
        /// <returns></returns>
        private IReplicationAction markMessageStatus(Int32 memberID,
           Int32 groupID,
           Int32 messageListID,
           bool setMessageStatusOn,
           MessageStatus messageStatusToSet,
           MessageListSaveType messageListSaveType,
           bool markOffNewStatus,
           bool callOffEmailTracker,
           MessageListCollection messageListColFromDB)
        {
            MessageList messageList = null;

            if (messageListColFromDB == null)
            {
                messageList = retrieveMessageList(memberID, groupID, false)[messageListID];
            }
            else
            {
                messageList = messageListColFromDB[messageListID];
            }

            IReplicationAction action = null;

            if (messageList != null)
            {
                MessageStatus messageStatus = messageList.StatusMask;
                bool isMessageStatusOn = (messageStatus & messageStatusToSet) == messageStatusToSet;
                if (setMessageStatusOn != isMessageStatusOn)
                {
                    if (setMessageStatusOn)
                    {
                        messageStatus |= messageStatusToSet;
                        
                        if(markOffNewStatus)
                            messageStatus &= ~MessageStatus.New;
                    }
                    else
                    {
                        messageStatus &= ~messageStatusToSet;
                    }
                    messageList.StatusMask = messageStatus;

                    action = new MessageListSaveAction(memberID, messageListSaveType, messageList);
                }
            }

            if (callOffEmailTracker && setMessageStatusOn)
            {
                updateEmailTracker(messageList);
            }

            return action;
        }

        private IReplicationAction deleteMessages(Int32 memberID,
            Int32 groupID,
            ArrayList messageListIDs)
        {
            ReplicationActionCollection actions = new ReplicationActionCollection();
            MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);

            foreach (Int32 messageListID in messageListIDs)
            {
                MessageList messageList = messageListCollection[messageListID];
                if (messageList != null)
                {
                    actions.Add(new MessageListDeleteAction(memberID, groupID, messageListID));
                    actions.Add(new MessageDeleteAction(memberID, messageList.TargetMemberID, messageList.MessageID));
                }
            }

            if (actions.Count == 0)
            {
                return null;
            }

            return actions;
        }

        public bool CanMoveEmailsToFolder(Int32 memberID,
            Int32 groupID,
            ArrayList messageListIDs,
            Int32 memberFolderID)
        {
            bool canMove = true;
            MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);
            int movetofolderid = memberFolderID;
            foreach (Int32 messageListID in messageListIDs)
            {
                MessageList messageList = messageListCollection[messageListID];
                if (EmailFolderSA.IsVIPFolder(messageList.MemberFolderID))
                {
                    movetofolderid = EmailFolderSA.GetMatchingVIPFolderId(memberFolderID);
                    if (movetofolderid == Constants.NULL_INT)
                    {
                        canMove = false;
                        break;
                    }
                }
            }

            return canMove;
        }


        private IReplicationAction moveToFolder(Int32 memberID,
            Int32 groupID,
            ArrayList messageListIDs,
            Int32 memberFolderID)
        {
            ReplicationActionCollection actions = new ReplicationActionCollection();
            MessageListCollection messageListCollection = retrieveMessageList(memberID, groupID, false);
            int movetofolderid = memberFolderID;
            foreach (Int32 messageListID in messageListIDs)
            {
                MessageList messageList = messageListCollection[messageListID];
                if (EmailFolderSA.IsVIPFolder(messageList.MemberFolderID))
                {
                    movetofolderid = EmailFolderSA.GetMatchingVIPFolderId(memberFolderID);
                }
                messageList.MemberFolderID = movetofolderid;
                messageList.StatusMask &= ~MessageStatus.New;
                actions.Add(new MessageListSaveAction(memberID, MessageListSaveType.Update, messageList));
            }

            if (actions.Count == 0)
            {
                return null;
            }

            return actions;
        }

        #endregion

        #region Helper Methods
        private void executeUnfraudPostProcess(int memberID,
            int groupID,
            ArrayList messageListIDs,
            MessageListCollection messageListColFromDB,
            StringBuilder sb)
        {
            MessageListCollection msgListCol = null;

            msgListCol = messageListColFromDB ?? retrieveMessageList(memberID, groupID, false);
            
            if (msgListCol == null)
            {
                if (sb != null)
                    sb.AppendLine("executeUnfraudPostProcess MessageListCollection is null -> MemberID: " + memberID +
                                  ", GroupID: " + groupID);
                return;
            }
                
            foreach (int msgListID in messageListIDs)
            {
                MessageList msgList = msgListCol[msgListID];
                if (msgList == null)
                {
                    if (sb != null)
                        sb.AppendLine("executeUnfraudPostProcess MessageList not found -> MessageListID: " + msgListID +
                                      ", MemberID: " + memberID +
                                      ", GroupID: " + groupID);
                    continue;
                }

                // only do anything if the message hasn't been read and is an inbox item
                if ( (msgList.MemberFolderID == (int)SystemFolders.VIPInbox || msgList.MemberFolderID == (int)SystemFolders.Inbox) &&
                    (msgList.StatusMask & MessageStatus.Read) == 0)
                {
                    // next we need to know if this is an intial vip message or reply to one
                    bool isInitialSend = (msgList.StatusMask & MessageStatus.OneFreeReply) == MessageStatus.OneFreeReply;
                    int senderBrandID = Constants.NULL_INT;
                    int recBrandID = Constants.NULL_INT;

                    Matchnet.Member.ServiceAdapters.Member sender = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(msgList.FromMemberID, MemberLoadFlags.None);
                    Matchnet.Member.ServiceAdapters.Member rec = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                    if (sender == null || rec == null)
                    {
                        if(sb != null)
                        {
                            if (sender == null)
                                sb.AppendLine("executeUnfraudPostProcess sender is null -> MemberID: " +
                                              msgList.FromMemberID);
                            if(rec == null)
                                sb.AppendLine("executeUnfraudPostProcess recepient is null -> MemberID: " +
                                              memberID);
                        }
                        continue;
                    }

                    sender.GetLastLogonDate(groupID, out senderBrandID);
                    rec.GetLastLogonDate(groupID, out recBrandID);

                    if (senderBrandID == Constants.NULL_INT || recBrandID == Constants.NULL_INT)
                    {
                        if(sb != null)
                        {
                            if (senderBrandID == Constants.NULL_INT)
                                sb.AppendLine("executeUnfraudPostProcess SenderBrand is null");

                            if (recBrandID == Constants.NULL_INT)
                                sb.AppendLine("executeUnfraudPostProcess RecBrand is null");
                        }
                        continue;
                    }
                     

                    Message msg = retrieveMessage(memberID, msgList.MessageID, true, false);

                    if (sb != null && msg == null)
                        sb.AppendLine("executeUnfraudPostProcess Message is null -> MemberID: " + memberID + ", MessageID: " + msgList.MessageID); 

                    if (msgList.MemberFolderID == (int)SystemFolders.VIPInbox)
                    {
                        if (isInitialSend)
                        {
                            if (msg != null)
                                ExternalMailSA.Instance.SendAllAccessInitialEmail(sender.MemberID, senderBrandID, msgListID, memberID, recBrandID,
                                    msg.MessageHeader, msg.MessageBody);

                            // unread this message from EmailTracker so that it starts tracking this again
                            EmailTrackerSA.Instance.MarkAsRead(msgListID, false);
                        }
                        else
                        {
                            if (msg != null)
                                ExternalMailSA.Instance.SendAllAccessReplyToInitialEmail(sender.MemberID, senderBrandID, msgListID, memberID, recBrandID,
                                    msg.MessageHeader, msg.MessageBody);
                        }

                        if (sb != null)
                            sb.AppendLine("executeUnfraudPostProcess done calling ExternalMail for VIP messages");
                    }
                    else
                    {
                        EmailMessage emailMessage = new EmailMessage(msgList, msg);
                        int unreadCount = 0;
                        // obtain unread count here
                        unreadCount = GetMessageCountByStatus(memberID, BrandConfigSA.Instance.GetBrandByID(recBrandID),
                            msgList.MemberFolderID, MessageStatus.Read, true, true);

                        if (sb != null)
                            sb.AppendLine(
                                "executeUnfraudPostProcess about to call SendInternalMailNotification ->unreadCount: " +
                                unreadCount);

                        ExternalMailSA.Instance.SendInternalMailNotification(emailMessage, senderBrandID, unreadCount);
                    }
                }

            }
        }
        
        private EmailMessageCollection filterECardMessages(EmailMessageCollection msgColl, Int32 siteID)
        {

            EmailMessageCollection tempColl = new EmailMessageCollection(msgColl.MemberID, msgColl.GroupID, msgColl.MemberFolderID);
            bool blnECardEnabled = bool.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_ECARDS", msgColl.GroupID, siteID));
            // The following condition should be removed after the deployment of MPR-978 Cupid Ecards
            if (!blnECardEnabled)
            {
                blnECardEnabled = bool.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEMP_SETTING_ENABLE_ECARDS_ON_CUPID", msgColl.GroupID, siteID));
            }

            foreach (EmailMessage msg in msgColl)
            {
                // This allows Ecards to be configurable
                if (!blnECardEnabled && msgColl[msg.MemberMailID].MailTypeID == Matchnet.Email.ValueObjects.MailType.ECard)
                {
                    continue;
                }
                tempColl.Add(msg);
            }
            return tempColl;
        }

        private void updateEmailTracker(MessageList pMessageList)
        {
            if (EmailFolderSA.IsVIPFolder(pMessageList.MemberFolderID))
            {
                EmailTracker.ServiceAdapters.EmailTrackerSA.Instance.MarkAsRead(pMessageList.MessageListID);
            }
        }

        private void playAction(string uri, IReplicationAction action)
        {
            if (action != null)
            {
                ReplicationActionCollection actions = action as ReplicationActionCollection;
                if (actions != null && actions.Count == 0)
                {
                    return;
                }

                base.Checkout(uri);
                try
                {
                    getService(uri).PlayAction(System.Environment.MachineName, action);
                }
                finally
                {
                    base.Checkin(uri);
                }

                CacheUtil.PlayAction(_cache, _cacheWebBucket, action);
            }
        }

        private static ArrayList DoPaging(ArrayList list, int startRow, int pageSize)
        {
            startRow -= 1; //convert from 1-based to 0-based

            // remove items only if the list contains more items than the start index
            if (list.Count > startRow)
            {
                // e.g. for a list of items of 10 and startRow of 5(with -1),
                // this will update the list to contain only items 5,6,7,8,9,10
                list.RemoveRange(0, startRow);

                // ensure the page size doesn't exceed the list size
                if (list.Count > pageSize)
                {
                    // e.g. for a list of items of 10 and pageSize of 3,
                    // this will update the list to contain only items 8,9,10
                    list.RemoveRange(pageSize, list.Count - pageSize);
                    return list;
                }

                // there are less items than the page size indicated, return as is
                return list;
            }

            // there are less items than the start index indicated
            return new ArrayList();
        }

        private void populateUsernames(ICollection emailMessages, Brand brand)
        {
            lock (emailMessages.SyncRoot)
            {
                foreach (EmailMessage message in emailMessages)
                {
                    if (message.FromUserName == null || message.ToUserName == null)
                    {
                        setUsernamesOnMessage(message, brand);
                    }
                }
            }
        }

        private string getPhotoURL(int memberID, Brand brand)
        {

             Matchnet.Member.ServiceAdapters.Member member = ( Matchnet.Member.ServiceAdapters.Member)null;
            if (memberID > 0)
                member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            if (member == null)
                return string.Empty;
            if (member.GetUserName(brand) == null)
                return "";
            PhotoCommunity photos = member.GetPhotos(brand.Site.Community.CommunityID);

            string photoUrl = "";
            if (photos.Count > 0)
            {
                photoUrl = EmailMessageSA.GetPhotoUrl(photos[(byte)0], EmailMessageSA.PhotoType.Thumbnail, brand);
            }

            //foreach (Photo photo in photos)
            //    ((object)EmailMessageSA.GetPhotoUrl(photo, EmailMessageSA.PhotoType.Thumbnail, brand)).ToString();
            return photoUrl;
        }

        private void setUsernamesOnMessage(EmailMessage pMessage, Brand brand)
        {
            if (pMessage != null)
            {
                // setup recipient username
                pMessage.ToUserName = getUsername(pMessage.ToMemberID, brand);

                // setup sender username
                pMessage.FromUserName = getUsername(pMessage.FromMemberID, brand);
            }
        }

        private string getUsername(Int32 memberID, Brand brand)
        {
            Matchnet.Member.ServiceAdapters.Member member = null;

            if (memberID > 0)
            {
                member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            }

            if (member == null)
            {
                return string.Empty;
            }
            else if (member.GetUserName(brand) != null)
            {
                return member.GetUserName(brand);
            }
            else
            {
                return member.MemberID.ToString();
            }
        }


        /// <summary>
        /// Invoked when delivering an new message to a recipient's on-site inbox.
        /// Updates the following attributes on the Recipient Member:
        ///		- CommunityEmailCount (incremented by 1)
        ///		- LastEmailReceiveDate (DateTime.Now)
        ///		- HasNewMail (true)
        /// </summary>
        /// <param name="messageSave"></param>
        private void updateRecipientMemberForHasNewMail(MessageSave messageSave, int recipientMessageListId)
        {
            // Don't update these mail related attributes if this is a FraudHold email
            if ((messageSave.MailOption & MailOption.FraudHold) == MailOption.FraudHold)
                return;

            // Don't update these for IM messages until we integrate them into inbox
            if (messageSave.MailType == MailType.InstantMessage)
                return;

            updateRecipientMailAttributes(messageSave.ToMemberID, messageSave.GroupID, 1);

            // Send push notification
            try
            {
                // Getting the receiver brand id
                int recBrandID;
                Matchnet.Member.ServiceAdapters.Member rec = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(messageSave.ToMemberID, MemberLoadFlags.None);
                rec.GetLastLogonDate(messageSave.GroupID, out recBrandID);

                // Sending the notification
                PushNotificationsProcessorAdapter.Instance.SendSystemEvent(new EmailEvent()
                {
                    BrandId = recBrandID,
                    EventCategory = SystemEventCategory.MemberGenerated,
                    EventType = SystemEventType.MessageReceived,
                    MemberId = messageSave.FromMemberID,
                    TargetMemberId = messageSave.ToMemberID,
                    NotificationTypeId = PushNotificationTypeID.MessageReceived,
                    MessageListID = recipientMessageListId, 
                    AppGroupId= AppGroupID.JDateAppGroup
                });

            }
            catch (Exception ex)
            {
                string excformat = String.Format("Error sending push notification in updateRecipientMemberForHasNewMail, frommemberid={0}, tomemberid={1}, groupid={2}", messageSave.FromMemberID, messageSave.ToMemberID, messageSave.GroupID);
                throw (new SAException(excformat, ex));
            }
        }


        private void updateRecipientMailAttributes(int memberId, int communityId, int mailCountToAdd)
        {
            Matchnet.Member.ServiceAdapters.Member recipient = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.None);
            recipient.SetAttributeInt(communityId
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "HasNewMail"
                , 1);
            recipient.SetAttributeDate(communityId
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "LastEmailReceiveDate"
                , DateTime.Now);

            int communityMessageCount = recipient.GetAttributeInt(communityId
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "CommunityEmailCount"
                , 0);
            communityMessageCount += mailCountToAdd;	// Increment message count.

            recipient.SetAttributeInt(communityId
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "CommunityEmailCount"
                , communityMessageCount);

            MemberSA.Instance.SaveMember(recipient);
        }

        /// <summary>
        /// Invoked when a member longer has 'Unread/new' messages in their onsite in-box.
        /// Updates the following attributes on the Member:
        ///		- HasNewMail (false)
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        private void updateRecipientMemberForHasNewMail(int memberID, int groupID)
        {
            EmailMessageCollection messages = retrieveMessages(memberID, groupID, (Int32)SystemFolders.Inbox);

            Int32 hasNewMail = messages.HasNewMail() ? 1 : 0;

            Matchnet.Member.ServiceAdapters.Member recipient = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

            recipient.SetAttributeInt(groupID
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "HasNewMail"
                , hasNewMail);

            MemberSA.Instance.SaveMember(recipient);
        }
        private int getMsgListInboxCount(int memberID, int groupID, ArrayList messageListIDs, int targetFolderId)
        {
            int inboxCount = 0;
            if (targetFolderId == (int)SystemFolders.Inbox)
            {
                inboxCount += messageListIDs.Count;
            }


            foreach (int id in messageListIDs)
            {
                EmailMessage msg = retrieveEmailMessage(memberID, groupID, id, false, false);
                if (msg != null && msg.MemberFolderID == (int)SystemFolders.Inbox && targetFolderId != (int)SystemFolders.Inbox && msg.MailTypeID != MailType.InstantMessage)
                { inboxCount -= 1; }
            }

            return inboxCount;
        }

        private void updateMemberInboxCount(int memberID, int groupID, int countIncrement)
        {

            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

            int communityMessageCount = member.GetAttributeInt(groupID
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "CommunityEmailCount"
                , 0);
            communityMessageCount += countIncrement;	// Increment message count.

            if (communityMessageCount < 0) communityMessageCount = 0;
            member.SetAttributeInt(groupID
                , Constants.NULL_INT
                , Constants.NULL_INT
                , "CommunityEmailCount"
                , communityMessageCount);


            MemberSA.Instance.SaveMember(member);
        }
        private void addFreeReplyToListAction(int replyMessageListID, MessageSave messageSave, MessageListSaveAction recipientListAction)
        {

            try
            {
                if (messageSave == null || messageSave.MailOption != MailOption.VIP) return;

                if (replyMessageListID > 0)
                {
                    MessageListCollection list = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, false);
                    MessageList repyToMessage = list[replyMessageListID];
                    if (repyToMessage != null)
                    {
                        if ((repyToMessage.StatusMask & MessageStatus.OneFreeReply) != MessageStatus.OneFreeReply)
                        {
                            recipientListAction.MessageList.StatusMask = recipientListAction.MessageList.StatusMask | MessageStatus.OneFreeReply;
                        }
                    }
                }
                else
                {
                    recipientListAction.MessageList.StatusMask = recipientListAction.MessageList.StatusMask | MessageStatus.OneFreeReply;
                }
            }
            catch (Exception ex)
            {
                string excformat = String.Format("Error updating list replication action with FreeReplyStatus, frommemberid={0}, groupid={1}, replyid={2}", messageSave.FromMemberID, messageSave.GroupID, replyMessageListID);
                SAException saexc = new SAException(excformat, ex);

            }
        }

        #endregion
    }
}
