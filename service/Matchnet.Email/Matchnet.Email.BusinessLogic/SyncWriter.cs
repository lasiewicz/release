using System;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Messaging;

using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;


namespace Matchnet.Email.BusinessLogic
{
	[Transaction(TransactionOption.Required)]
	public class SyncWriter : ServicedComponent
	{
		public Int32 Write(Command command,
			out Exception exception)
		{
			bool written = false;
			Partition partition = ConnectionDispenser.Instance.GetLogicalDatabase(command.LogicalDatabaseName).GetPartition(command.Key);
			PhysicalDatabases physicalDatabases = partition.PhysicalDatabases;
			Int32 pdbCount = physicalDatabases.Count;
			SqlConnection[] connections = new SqlConnection[pdbCount];
			string connectionString = "";

			exception = null;

			Int32 returnValuePrev = 0;

			try
			{
				for (Int32 pdbNum = 0; pdbNum < pdbCount; pdbNum++)
				{
					PhysicalDatabase pdb = physicalDatabases[pdbNum];

					SqlCommand cmd = new SqlCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = command.StoredProcedureName;
					cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
					for (Int32 parameterNum = 0; parameterNum < command.Parameters.Count; parameterNum++)
					{
						Parameter parameter = command.Parameters[parameterNum];
						cmd.Parameters.Add(parameter.Name, parameter.DataType).Value = parameter.ParameterValue;
					}

					if (pdb.IsActive)
					{
						connectionString = pdb.ConnectionString;
						SqlConnection conn = new SqlConnection(pdb.ConnectionString);
						connections[pdbNum] = conn;

						cmd.Connection = conn;
						conn.Open();
						cmd.ExecuteNonQuery();
						Int32 returnValue = Convert.ToInt32(cmd.Parameters["@RETURN_VALUE"].Value);
						connectionString = "";

						if (written)
						{
							if (returnValuePrev != returnValue)
							{
								throw new Exception("Return values did not match.");
							}
						}

						returnValuePrev = returnValue;

						written = true;
					}
					else
					{
						command.SetConnectionString(pdb.ConnectionString);
						MessageQueue queue = HydraUtility.GetQueueRecovery(HydraUtility.GetKey(command.LogicalDatabaseName, partition.Offset), pdb.ServerName);
						queue.Send(command, MessageQueueTransactionType.Automatic);
					}
				}

				if (written == false)
				{
					throw new Exception("No active " + command.LogicalDatabaseName + " databases are active for offset " + partition.Offset.ToString());
				}

				ContextUtil.SetComplete();

				return returnValuePrev;
			}
			catch (Exception ex)
			{
				exception = new Exception("SyncWriter error (connectionString: " + connectionString + ").", ex);
				ContextUtil.SetAbort();
				return 0;
			}
			finally
			{
				foreach (SqlConnection conn in connections)
				{
					if (conn != null)
					{
						if (conn.State == ConnectionState.Open)
						{
							conn.Close();
						}
					}
				}
			}
		}
	}
}
