using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Email.ValueObjects;
using System.Collections.Generic;

namespace Matchnet.Email.BusinessLogic
{
	/// <summary>
	/// Summary description for EmailMessageDB.
	/// </summary>
	public class EmailMessageDB
	{
		#region Constants
		private const string LOGICALDB_IMAIL = "mnIMailNew";
		#endregion

		#region Message Methods

		internal static Message RetrieveMessage(Int32 memberID, Int32 messageID, bool withContent)
		{
			MessageListType messageListType = MessageListType.NoContent;
			if (withContent)
			{
				messageListType = MessageListType.Everything;
			}

			string messageHeader = Constants.NULL_STRING;
			string messageBody = Constants.NULL_STRING;
			DateTime insertDate = DateTime.MinValue;
			DateTime openDate = DateTime.MinValue;

			SqlDataReader reader = null;
			try
			{
				Command command = new Command(LOGICALDB_IMAIL, "up_Message_List", memberID);
				command.AddParameter("@ListType", SqlDbType.Int, ParameterDirection.Input, (Int32) messageListType);
				command.AddParameter("@MessageID",SqlDbType.Int, ParameterDirection.Input, messageID);
				reader = Client.Instance.ExecuteReader(command);
			
				if (reader.Read())
				{
					messageHeader = reader.GetString(reader.GetOrdinal("MessageHeader"));

					if (withContent)
					{
						Int32 ordinalMessageBody = reader.GetOrdinal("MessageBody");
						if (!reader.IsDBNull(ordinalMessageBody))
						{
							messageBody = reader.GetString(ordinalMessageBody);
						}
					}

					Int32 ordinalInsertDate = reader.GetOrdinal("InsertDate");
					if (!reader.IsDBNull(ordinalInsertDate))
					{
						insertDate = reader.GetDateTime(ordinalInsertDate);
					}

					Int32 ordinalOpenDate = reader.GetOrdinal("OpenDate");
					if (!reader.IsDBNull(ordinalOpenDate))
					{
						openDate = reader.GetDateTime(ordinalOpenDate);
					}
				}
				else
				{
					EventLog.WriteEntry("Matchnet.Email.Service", "Message not found.  MessageID: " + messageID + ", MemberID: " + memberID, EventLogEntryType.Warning);
				}
			}
            catch (Exception Ex)
            {
                throw (new Exception("Error populating Message object.", Ex));
            }
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
			}

			return new Message(messageID, messageHeader, messageBody, insertDate, openDate);
		}


		internal static void MessageSave(Int32 memberID, Message message, MessageSaveType saveType)
		{
			bool insert = (saveType & MessageSaveType.Insert) == MessageSaveType.Insert;
			bool update = (saveType & MessageSaveType.Update) == MessageSaveType.Update;
			bool openDate = (saveType & MessageSaveType.OpenDate) == MessageSaveType.OpenDate;
			bool draft = (saveType & MessageSaveType.Draft) == MessageSaveType.Draft;

			Command command = new Command(LOGICALDB_IMAIL, "up_Message_Save", memberID);
			command.AddParameter("@SaveType", SqlDbType.Int, ParameterDirection.Input, (Int32) saveType);
			command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, message.MessageID);

			if (openDate)
			{
				command.AddParameter("@OpenDate", SqlDbType.DateTime, ParameterDirection.Input, message.OpenDate);
			}

			if (draft || insert)
			{
				command.AddParameter("@MessageHeader", SqlDbType.NVarChar, ParameterDirection.Input, message.MessageHeader);
				command.AddParameter("@MessageBody", SqlDbType.NText, ParameterDirection.Input, message.MessageBody);
			}

			if (insert)
			{
			    if (message.InsertDate == DateTime.MinValue)
			    {
			        message.InsertDate = DateTime.Now;
			    }
			    command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, message.InsertDate);
			}

			Client.Instance.ExecuteAsyncWrite(command);
		}


		internal static void MessageSave(Int32 memberID, Int32 messageID, DateTime openDate)
		{
			Command command = new Command(LOGICALDB_IMAIL, "up_Message_Save", memberID);
			MessageSaveType saveType = (MessageSaveType.Update | MessageSaveType.OpenDate);
			command.AddParameter("@SaveType", SqlDbType.Int, ParameterDirection.Input, (Int32) saveType);
			command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageID);
			command.AddParameter("@OpenDate", SqlDbType.DateTime, ParameterDirection.Input, openDate);
			Client.Instance.ExecuteAsyncWrite(command);
		}


		internal static void MessageDelete(Int32 memberID, Int32 messageID)
		{
			Command command = new Command(LOGICALDB_IMAIL, "up_Message_Delete", memberID);
			command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageID);
			Client.Instance.ExecuteAsyncWrite(command);
		}
		#endregion

		#region MessageList Methods
		internal static bool MessageListExists(Int32 memberID, Int32 messageID)
		{
			Command command = new Command(LOGICALDB_IMAIL, "up_MessageList_List", memberID);
			command.AddParameter("@ListType", SqlDbType.Int, ParameterDirection.Input, (Int32) MessageListListType.Exists);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageID);
			command.AddParameter("@Exists", SqlDbType.Bit, ParameterDirection.Output, 0);

			return (bool) Client.Instance.ExecuteNonQuery(command)["@Exists"].Value;
		}

		internal static MessageListCollection RetrieveMessageList(Int32 memberID, Int32 groupID, bool filterDeleted)
		{
			SqlDataReader reader = null;
			MessageListCollection messageListCollection = new MessageListCollection(memberID, groupID);

			try
			{
				Command command = new Command(LOGICALDB_IMAIL, "up_MessageList_List", memberID);
				command.AddParameter("@ListType", SqlDbType.Int, ParameterDirection.Input, (Int32) MessageListListType.Retrieve);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				reader = Client.Instance.ExecuteReader(command);
			
				//Create an array of column ordinals to avoid looking them up repeatedly
				Array columns = Enum.GetValues(typeof(MessageListColumn));
				Int32[] ordinals = new Int32[columns.Length];
				foreach (MessageListColumn column in columns)
				{
					ordinals[(Int32) column] = reader.GetOrdinal(column.ToString());
				}

                if (filterDeleted)
                {
                    while (reader.Read())
                    {
                        MessageList messageList = convertToMessageList(reader, ordinals, false);
                        // only add the MessageList if it's not soft deleted or on fraud hold
                        if ( ((int)messageList.StatusMask & (int)MessageStatus.Deleted) == 0 && ((int)messageList.StatusMask & (int)MessageStatus.FraudHold) == 0)
                            messageListCollection.Add(messageList);
                    }
                }
                else
                {
                    while (reader.Read())
                    {
                        MessageList messageList = convertToMessageList(reader, ordinals, false);
                        messageListCollection.Add(messageList);
                    }
                }
				
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
			}

			return messageListCollection;
		}
        internal static MessageMemberCollection RetrieveMessageMember(Int32 memberID, Int32 groupID, bool filterDeleted)
        {
            SqlDataReader reader = null;
            MessageMemberCollection messageMemberCollection = new MessageMemberCollection(memberID, groupID);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_MessageList_Members", memberID);
                //command.AddParameter("@ListType", SqlDbType.Int, ParameterDirection.Input, (Int32)MessageListListType.Retrieve);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(MessageMemberColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (MessageMemberColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                if (filterDeleted)
                {
                    while (reader.Read())
                    {
                        MessageMember messageMember = convertToMessageMember(reader, ordinals);
                        // only add the MessageList if it's not soft removed
                        if (((int)messageMember.StatusMask & (int)MessageStatus.MemberRemove) == 0)
                            messageMemberCollection.Add(messageMember);
                    }
                }
                else
                {
                    while (reader.Read())
                    {
                        MessageMember messageMember = convertToMessageMember(reader, ordinals);
                        messageMemberCollection.Add(messageMember);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return messageMemberCollection;
        }

		internal static void MessageListSave(Int32 memberID, MessageList messageList, MessageListSaveType saveType)
		{
            if (saveType == MessageListSaveType.Delete || saveType == MessageListSaveType.Undelete)
            {
                // since these are soft delete and undelete, it should be treated as an update
                saveType = MessageListSaveType.Update;
            }

			Command command = new Command(LOGICALDB_IMAIL, "up_MessageList_Save", memberID);
			command.AddParameter("@SaveType", SqlDbType.Int, ParameterDirection.Input, (Int32) saveType);
			command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, messageList.MessageListID);
			command.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, messageList.MemberFolderID);
			command.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, (Int32) messageList.StatusMask);
			command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
			command.AddParameter("@NumBytes", SqlDbType.Int, ParameterDirection.Input, messageList.NumBytes);

			if (saveType == MessageListSaveType.Insert)
			{
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, messageList.MemberID);
				command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, messageList.TargetMemberID);
				command.AddParameter("@DirectionFlag", SqlDbType.Bit, ParameterDirection.Input, messageList.DirectionFlag);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, messageList.GroupID);
				command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageList.MessageID);
				command.AddParameter("@MessageTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32) messageList.MailTypeID);
				if (messageList.InsertDate != DateTime.MinValue)
				{
					command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, messageList.InsertDate);
				}
                command.AddParameter("@ThreadID", SqlDbType.VarChar, ParameterDirection.Input, messageList.ThreadID);
			}

			Client.Instance.ExecuteAsyncWrite(command);
		}

        internal static void MessageListSave(Int32 memberID, Int32 siteID, MessageList messageList, MessageListSaveType saveType)
        {
            Command command = new Command(LOGICALDB_IMAIL, "up_MessageList_Save", memberID);
            command.AddParameter("@SaveType", SqlDbType.Int, ParameterDirection.Input, (Int32) saveType);
            command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, messageList.MessageListID);
            command.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, messageList.MemberFolderID);
            command.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, (Int32) messageList.StatusMask);
            command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            command.AddParameter("@NumBytes", SqlDbType.Int, ParameterDirection.Input, messageList.NumBytes);

            if (saveType == MessageListSaveType.Insert)
            {
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, messageList.MemberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, messageList.TargetMemberID);
                command.AddParameter("@DirectionFlag", SqlDbType.Bit, ParameterDirection.Input, messageList.DirectionFlag);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, messageList.GroupID);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageList.MessageID);
                command.AddParameter("@MessageTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32) messageList.MailTypeID);
                if (messageList.InsertDate != DateTime.MinValue)
                {
                    command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, messageList.InsertDate);
                }
                command.AddParameter("@ThreadID", SqlDbType.VarChar, ParameterDirection.Input, messageList.ThreadID);
            }

            Client.Instance.ExecuteAsyncWrite(command);
        }
		
		internal static void MessageListDelete(Int32 memberID, Int32 messageListID)
		{
			Command command = new Command(LOGICALDB_IMAIL, "up_MessageList_Delete", memberID);
			command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, messageListID);
			Client.Instance.ExecuteAsyncWrite(command);
		}

        
		#endregion

        #region IM History
        internal static void InstantMessagesDelete(int memberID, int targetMemberID, int groupID)
        {
            //proc will automatically determine whether to delete messages
            Command command = new Command(LOGICALDB_IMAIL, "up_IMHistory_Messages_Delete", memberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        internal static EmailMessageList RetrieveInstantMessages(int memberID, int targetMemberID, Int32 groupID, bool filterDeleted)
        {
            SqlDataReader reader = null;
            EmailMessageList emailMessageList = new EmailMessageList(memberID, targetMemberID, groupID, ConversationType.Chat, MailType.InstantMessage);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_IMHistory_Message_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(EmailThreadedMessageColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (EmailThreadedMessageColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    EmailMessage emailMessage = convertToEmailThreadedMessage(reader, ordinals, true);
                    if (filterDeleted)
                    {
                        // only add the MessageList if it's not soft deleted or on fraud hold
                        if (((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.Deleted) == 0 && ((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.FraudHold) == 0)
                            emailMessageList.Add(emailMessage);
                    }
                    else
                    {
                        emailMessageList.Add(emailMessage);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return emailMessageList;
        }

        internal static MessageMemberCollection RetrieveInstantMessageMembers(Int32 memberID, Int32 groupID, bool filterRemovedMembers)
        {
            SqlDataReader reader = null;
            MessageMemberCollection messageMemberCollection = new MessageMemberCollection(memberID, groupID, MailType.InstantMessage, ConversationType.Chat, filterRemovedMembers);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_IMHistory_Member_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(MessageMemberColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (MessageMemberColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    MessageMember messageMember = convertToMessageMember(reader, ordinals);
                    if (filterRemovedMembers)
                    {
                        // only add the MessageList if it's not soft removed
                        if (((int)messageMember.StatusMask & (int)MessageStatus.MemberRemove) == 0)
                            messageMemberCollection.Add(messageMember);
                    }
                    else
                    {
                        messageMemberCollection.Add(messageMember);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return messageMemberCollection;
        }

        #endregion

        #region Threaded Conversation Mail
        internal static EmailMessageList RetrieveMailConversationInboxMessages(int memberID, int targetMemberID, Int32 groupID, ConversationType conversationType, bool filterDeleted)
        {
            SqlDataReader reader = null;
            EmailMessageList emailMessageList = new EmailMessageList(memberID, targetMemberID, groupID, conversationType, MailType.None);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Inbox_Message_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(EmailThreadedMessageColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (EmailThreadedMessageColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    EmailMessage emailMessage = convertToEmailThreadedMessage(reader, ordinals, true);
                    if (filterDeleted)
                    {
                        // only add the MessageList if it's not soft deleted or on fraud hold
                        if (((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.Deleted) == 0 && ((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.FraudHold) == 0)
                            emailMessageList.Add(emailMessage);
                    }
                    else
                    {
                        emailMessageList.Add(emailMessage);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return emailMessageList;
        }

        internal static EmailMessageList RetrieveMailConversationSentMessages(int memberID, int targetMemberID, Int32 groupID, ConversationType conversationType, bool filterDeleted)
        {
            SqlDataReader reader = null;
            EmailMessageList emailMessageList = new EmailMessageList(memberID, targetMemberID, groupID, conversationType, MailType.None);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Sent_Message_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(EmailThreadedMessageColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (EmailThreadedMessageColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    EmailMessage emailMessage = convertToEmailThreadedMessage(reader, ordinals, true);
                    if (filterDeleted)
                    {
                        // only add the MessageList if it's not soft deleted or on fraud hold
                        if (((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.Deleted) == 0 && ((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.FraudHold) == 0)
                            emailMessageList.Add(emailMessage);
                    }
                    else
                    {
                        emailMessageList.Add(emailMessage);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return emailMessageList;
        }

        internal static EmailMessageList RetrieveMailConversationDraftMessages(int memberID, int targetMemberID, Int32 groupID, ConversationType conversationType, bool filterDeleted)
        {
            SqlDataReader reader = null;
            EmailMessageList emailMessageList = new EmailMessageList(memberID, targetMemberID, groupID, conversationType, MailType.None);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Draft_Message_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(EmailThreadedMessageColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (EmailThreadedMessageColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    EmailMessage emailMessage = convertToEmailThreadedMessage(reader, ordinals, true);
                    if (filterDeleted)
                    {
                        // only add the MessageList if it's not soft deleted or on fraud hold
                        if (((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.Deleted) == 0 && ((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.FraudHold) == 0)
                            emailMessageList.Add(emailMessage);
                    }
                    else
                    {
                        emailMessageList.Add(emailMessage);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return emailMessageList;
        }

        internal static EmailMessageList RetrieveMailConversationTrashMessages(int memberID, int targetMemberID, Int32 groupID, ConversationType conversationType, bool filterDeleted)
        {
            SqlDataReader reader = null;
            EmailMessageList emailMessageList = new EmailMessageList(memberID, targetMemberID, groupID, conversationType, MailType.None);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Trash_Message_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(EmailThreadedMessageColumn));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (EmailThreadedMessageColumn column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    EmailMessage emailMessage = convertToEmailThreadedMessage(reader, ordinals, true);
                    if (filterDeleted)
                    {
                        // only add the MessageList if it's not soft deleted or on fraud hold
                        if (((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.Deleted) == 0 && ((int)emailMessage.MessageList.StatusMask & (int)MessageStatus.FraudHold) == 0)
                            emailMessageList.Add(emailMessage);
                    }
                    else
                    {
                        emailMessageList.Add(emailMessage);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return emailMessageList;
        }

        internal static MessageMemberCollection RetrieveMailConversationInboxMembers(Int32 memberID, Int32 groupID, ConversationType conversationType)
        {
            SqlDataReader reader = null;
            MessageMemberCollection messageMemberCollection = new MessageMemberCollection(memberID, groupID, MailType.None, conversationType);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Inbox_Member_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(MessageMemberColumnWithCountsV2));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (MessageMemberColumnWithCountsV2 column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    MessageMember messageMember = convertToMessageMemberWithCount(reader, ordinals);

                    // only add the MessageList if it's not soft removed
                    if (messageMember.LastMessageListID > 0)
                    {
                        if (((int)messageMember.LastMessageStatusMask & (int)MessageStatus.MemberRemove) == 0)
                        {
                            messageMemberCollection.Add(messageMember);
                        }
                    }
                    else if (((int)messageMember.StatusMask & (int)MessageStatus.MemberRemove) == 0)
                    {
                        messageMemberCollection.Add(messageMember);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return messageMemberCollection;
        }

        internal static MessageMemberCollection RetrieveMailConversationSentMembers(Int32 memberID, Int32 groupID, ConversationType conversationType)
        {
            SqlDataReader reader = null;
            MessageMemberCollection messageMemberCollection = new MessageMemberCollection(memberID, groupID, MailType.None, conversationType);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Sent_Member_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(MessageMemberColumnWithCountsV2));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (MessageMemberColumnWithCountsV2 column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    MessageMember messageMember = convertToMessageMemberWithCount(reader, ordinals);

                    // only add the MessageList if it's not soft removed
                    if (messageMember.LastMessageListID > 0)
                    {
                        if (((int)messageMember.LastMessageStatusMask & (int)MessageStatus.MemberRemove) == 0)
                        {
                            messageMemberCollection.Add(messageMember);
                        }
                    }
                    else if (((int)messageMember.StatusMask & (int)MessageStatus.MemberRemove) == 0)
                    {
                        messageMemberCollection.Add(messageMember);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return messageMemberCollection;
        }

        internal static MessageMemberCollection RetrieveMailConversationDraftMembers(Int32 memberID, Int32 groupID, ConversationType conversationType)
        {
            SqlDataReader reader = null;
            MessageMemberCollection messageMemberCollection = new MessageMemberCollection(memberID, groupID, MailType.None, conversationType);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Draft_Member_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(MessageMemberColumnWithCountsV2));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (MessageMemberColumnWithCountsV2 column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    MessageMember messageMember = convertToMessageMemberWithCount(reader, ordinals);

                    // only add the MessageList if it's not soft removed
                    if (messageMember.LastMessageListID > 0)
                    {
                        if (((int)messageMember.LastMessageStatusMask & (int)MessageStatus.MemberRemove) == 0)
                        {
                            messageMemberCollection.Add(messageMember);
                        }
                    }
                    else if (((int)messageMember.StatusMask & (int)MessageStatus.MemberRemove) == 0)
                    {
                        messageMemberCollection.Add(messageMember);
                    }
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return messageMemberCollection;
        }

        internal static MessageMemberCollection RetrieveMailConversationTrashMembers(Int32 memberID, Int32 groupID, ConversationType conversationType)
        {
            SqlDataReader reader = null;
            MessageMemberCollection messageMemberCollection = new MessageMemberCollection(memberID, groupID, MailType.None, conversationType);

            try
            {
                Command command = new Command(LOGICALDB_IMAIL, "up_Mail_Conversation_Trash_Member_List", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                reader = Client.Instance.ExecuteReader(command);

                //Create an array of column ordinals to avoid looking them up repeatedly
                Array columns = Enum.GetValues(typeof(MessageMemberColumnWithCountsV2));
                Int32[] ordinals = new Int32[columns.Length];
                foreach (MessageMemberColumnWithCountsV2 column in columns)
                {
                    ordinals[(Int32)column] = reader.GetOrdinal(column.ToString());
                }

                while (reader.Read())
                {
                    MessageMember messageMember = convertToMessageMemberWithCount(reader, ordinals);

                    // only add the MessageList if it's soft removed
                    if (((int)messageMember.StatusMask & (int)MessageStatus.MemberRemove) == (int)MessageStatus.MemberRemove)
                        messageMemberCollection.Add(messageMember);
                }

            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return messageMemberCollection;
        }


        #endregion

        #region Message Attachments
        /// <summary>
        /// This creates a MessageAttachment record with provided data
        /// Use this when you have a messageID and all attachment data
        /// </summary>
        internal static void SaveMessageAttachment(int messageAttachmentID, int messageID, AttachmentType attachmentType,
                                                                 int fileID, string previewCloudPath, string originalCloudPath, int appID, AttachmentStatus status, int memberIDForPartition)
        {
            Command command = new Command(LOGICALDB_IMAIL, "up_MessageAttachment_Insert", memberIDForPartition);
            command.AddParameter("@MessageAttachmentID", SqlDbType.Int, ParameterDirection.Input, messageAttachmentID);
            command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageID);
            command.AddParameter("@MessageAttachmentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)attachmentType);
            command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
            command.AddParameter("@PreviewCloudPath", SqlDbType.NVarChar, ParameterDirection.Input, previewCloudPath);
            command.AddParameter("@OriginalCloudPath", SqlDbType.NVarChar, ParameterDirection.Input, originalCloudPath);
            command.AddParameter("@AttachmentStatus", SqlDbType.Int, ParameterDirection.Input, (int)status);
            command.AddParameter("@ApplicationID", SqlDbType.Int, ParameterDirection.Input, appID);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        /// <summary>
        /// This creates a MessageAttachment record by copying existing attachment from interim
        /// Use this when you have a messageID and existing attachmentID so you can tie them together
        /// </summary>
        internal static void SaveMessageAttachmentFromInterim(int messageID, int messageAttachmentID, int memberIDForPartition)
        {
            Command command = new Command(LOGICALDB_IMAIL, "up_MessageAttachment_Insert_FromInterim", memberIDForPartition);
            command.AddParameter("@MessageAttachmentID", SqlDbType.Int, ParameterDirection.Input, messageAttachmentID);
            command.AddParameter("@MessageID", SqlDbType.Int, ParameterDirection.Input, messageID);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        /// <summary>
        /// This creates a MessageAttachmentInterim record with provided data
        /// Use this when you do not have a messageID, this stores attachment in an interim table which you can use to 
        /// tie to a message record later by moving this data to MessageAttachment
        /// </summary>
        internal static void SaveMessageAttachmentInterim(int messageAttachmentID, AttachmentType attachmentType,
                                                                 int fileID, string previewCloudPath, string originalCloudPath, int fromMemberID, int toMemberID, int communityID, int siteID, int appID, AttachmentStatus status, int memberIDForPartition)
        {
            Command command = new Command(LOGICALDB_IMAIL, "up_MessageAttachmentInterim_Insert", memberIDForPartition);
            command.AddParameter("@MessageAttachmentID", SqlDbType.Int, ParameterDirection.Input, messageAttachmentID);
            command.AddParameter("@MessageAttachmentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)attachmentType);
            command.AddParameter("@FromMemberID", SqlDbType.Int, ParameterDirection.Input, fromMemberID);
            command.AddParameter("@ToMemberID", SqlDbType.Int, ParameterDirection.Input, toMemberID);
            command.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, fileID);
            command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@PreviewCloudPath", SqlDbType.NVarChar, ParameterDirection.Input, previewCloudPath);
            command.AddParameter("@OriginalCloudPath", SqlDbType.NVarChar, ParameterDirection.Input, originalCloudPath);
            command.AddParameter("@AttachmentStatus", SqlDbType.Int, ParameterDirection.Input, (int)status);
            command.AddParameter("@ApplicationID", SqlDbType.Int, ParameterDirection.Input, appID);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        
        #endregion

        #region Helper Methods
        /// <summary>
		/// Determines whether the sender's and recipient's messages reside on the same mnImail partition.
		/// </summary>
		/// <param name="pMessage"></param>
		/// <returns></returns>
		internal static bool SharePartition(Int32 memberID, Int32 targetMemberID)
		{
			short toPartition = ConnectionDispenser.Instance.GetLogicalDatabase(LOGICALDB_IMAIL).GetOffset(memberID);
			short fromPartition = ConnectionDispenser.Instance.GetLogicalDatabase(LOGICALDB_IMAIL).GetOffset(targetMemberID);

			return (toPartition == fromPartition);
		}

		private static MessageList convertToMessageList(SqlDataReader reader, Int32[] ordinals, bool includeContent)
		{
			try
			{
				MessageStatus statusMask = MessageStatus.Nothing;
				if (!reader.IsDBNull(ordinals[(Int32) MessageListColumn.StatusMask]))
				{	
					statusMask = (MessageStatus) reader.GetInt32(ordinals[(Int32) MessageListColumn.StatusMask]);
				}

				DateTime insertDate = DateTime.MinValue;
				if (!reader.IsDBNull(ordinals[(Int32) MessageListColumn.InsertDate]))
				{
					insertDate = reader.GetDateTime(ordinals[(Int32) MessageListColumn.InsertDate]);
				}

				Int32 numBytes = 0;
				if (!reader.IsDBNull(ordinals[(Int32) MessageListColumn.NumBytes]))
				{
					numBytes = reader.GetInt32(ordinals[(Int32) MessageListColumn.NumBytes]);
				}
				
				return new MessageList(reader.GetInt32(ordinals[(Int32) MessageListColumn.MessageListID]),
					reader.GetInt32(ordinals[(Int32) MessageListColumn.MemberID]),
					reader.GetInt32(ordinals[(Int32) MessageListColumn.TargetMemberID]),
					reader.GetBoolean(ordinals[(Int32) MessageListColumn.DirectionFlag]),
					reader.GetInt32(ordinals[(Int32) MessageListColumn.GroupID]),
					reader.GetInt32(ordinals[(Int32) MessageListColumn.MemberFolderID]),
					reader.GetInt32(ordinals[(Int32) MessageListColumn.MessageID]),
					statusMask,
                    (MailType) reader.GetInt32(ordinals[(Int32) MessageListColumn.MessageTypeID]),
					insertDate,
					numBytes);
			}
			catch(Exception Ex)
			{
				throw(new Exception("Error populating MessageList object.", Ex));
			}
		}

        private static EmailMessage convertToEmailMessage(SqlDataReader reader, Int32[] ordinals, bool includeContent)
        {
            int messageID = reader.GetInt32(ordinals[(Int32)EmailMessageColumn.MessageID]);
            MessageStatus statusMask = MessageStatus.Nothing;
            if (!reader.IsDBNull(ordinals[(Int32)EmailMessageColumn.StatusMask]))
            {
                statusMask = (MessageStatus)reader.GetInt32(ordinals[(Int32)EmailMessageColumn.StatusMask]);
            }

            DateTime insertDate = DateTime.MinValue;
            if (!reader.IsDBNull(ordinals[(Int32)EmailMessageColumn.InsertDate]))
            {
                insertDate = reader.GetDateTime(ordinals[(Int32)EmailMessageColumn.InsertDate]);
            }

            Int32 numBytes = 0;
            if (!reader.IsDBNull(ordinals[(Int32)EmailMessageColumn.NumBytes]))
            {
                numBytes = reader.GetInt32(ordinals[(Int32)EmailMessageColumn.NumBytes]);
            }

            MessageList messageList = new MessageList(reader.GetInt32(ordinals[(Int32)EmailMessageColumn.MessageListID]),
                reader.GetInt32(ordinals[(Int32)EmailMessageColumn.MemberID]),
                reader.GetInt32(ordinals[(Int32)EmailMessageColumn.TargetMemberID]),
                reader.GetBoolean(ordinals[(Int32)EmailMessageColumn.DirectionFlag]),
                reader.GetInt32(ordinals[(Int32)EmailMessageColumn.GroupID]),
                reader.GetInt32(ordinals[(Int32)EmailMessageColumn.MemberFolderID]),
                messageID,
                statusMask,
                (MailType)reader.GetInt32(ordinals[(Int32)EmailMessageColumn.MessageTypeID]),
                insertDate,
                numBytes);

            
            string messageHeader = Constants.NULL_STRING;
            string messageBody = Constants.NULL_STRING;
            DateTime messageInsertDate = DateTime.MinValue;
            DateTime messageOpenDate = DateTime.MinValue;

            messageHeader = reader.GetString(ordinals[(Int32)EmailMessageColumn.MessageHeader]);

            if (includeContent)
            {
                if (!reader.IsDBNull(ordinals[(Int32)EmailMessageColumn.MessageBody]))
                {
                    messageBody = reader.GetString(ordinals[(Int32)EmailMessageColumn.MessageBody]);
                }
            }

            if (!reader.IsDBNull(ordinals[(Int32)EmailMessageColumn.MessageInsertDate]))
            {
                insertDate = reader.GetDateTime(ordinals[(Int32)EmailMessageColumn.MessageInsertDate]);
            }

            if (!reader.IsDBNull(ordinals[(Int32)EmailMessageColumn.OpenDate]))
            {
                messageOpenDate = reader.GetDateTime(ordinals[(Int32)EmailMessageColumn.OpenDate]);
            }

            Message message = new Message(messageID, messageHeader, messageBody, insertDate, messageOpenDate);

            return new EmailMessage(messageList, message);
        }

        private static EmailMessage convertToEmailThreadedMessage(SqlDataReader reader, Int32[] ordinals, bool includeContent)
        {
            int messageID = reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.MessageID]);
            MessageStatus statusMask = MessageStatus.Nothing;
            if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.StatusMask]))
            {
                statusMask = (MessageStatus)reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.StatusMask]);
            }

            DateTime insertDate = DateTime.MinValue;
            if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.InsertDate]))
            {
                insertDate = reader.GetDateTime(ordinals[(Int32)EmailThreadedMessageColumn.InsertDate]);
            }

            Int32 numBytes = 0;
            if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.NumBytes]))
            {
                numBytes = reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.NumBytes]);
            }

            string threadID = "";
            if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.ThreadID]))
            {
                threadID = reader.GetString(ordinals[(Int32)EmailThreadedMessageColumn.ThreadID]);
            }

            MessageList messageList = new MessageList(reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.MessageListID]),
                reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.MemberID]),
                reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.TargetMemberID]),
                reader.GetBoolean(ordinals[(Int32)EmailThreadedMessageColumn.DirectionFlag]),
                reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.GroupID]),
                reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.MemberFolderID]),
                messageID,
                statusMask,
                (MailType)reader.GetInt32(ordinals[(Int32)EmailThreadedMessageColumn.MessageTypeID]),
                insertDate,
                numBytes,
                threadID);


            string messageHeader = Constants.NULL_STRING;
            string messageBody = Constants.NULL_STRING;
            DateTime messageInsertDate = DateTime.MinValue;
            DateTime messageOpenDate = DateTime.MinValue;

            messageHeader = reader.GetString(ordinals[(Int32)EmailThreadedMessageColumn.MessageHeader]);

            if (includeContent)
            {
                if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.MessageBody]))
                {
                    messageBody = reader.GetString(ordinals[(Int32)EmailThreadedMessageColumn.MessageBody]);
                }
            }

            if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.MessageInsertDate]))
            {
                insertDate = reader.GetDateTime(ordinals[(Int32)EmailThreadedMessageColumn.MessageInsertDate]);
            }

            if (!reader.IsDBNull(ordinals[(Int32)EmailThreadedMessageColumn.OpenDate]))
            {
                messageOpenDate = reader.GetDateTime(ordinals[(Int32)EmailThreadedMessageColumn.OpenDate]);
            }

            Message message = new Message(messageID, messageHeader, messageBody, insertDate, messageOpenDate);

            return new EmailMessage(messageList, message, threadID);
        }

        private static MessageMember convertToMessageMember(SqlDataReader reader, Int32[] ordinals)
        {
            try
            {
                MessageStatus statusMask = MessageStatus.Nothing;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumn.StatusMask]))
                {
                    statusMask = (MessageStatus)reader.GetInt32(ordinals[(Int32)MessageMemberColumn.StatusMask]);
                }

                DateTime insertDate = DateTime.MinValue;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumn.InsertDate]))
                {
                    insertDate = reader.GetDateTime(ordinals[(Int32)MessageMemberColumn.InsertDate]);
                }

                Int32 numBytes = 0;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumn.NumBytes]))
                {
                    numBytes = reader.GetInt32(ordinals[(Int32)MessageMemberColumn.NumBytes]);
                }

                string body = "";
                if (ordinals.Length > (Int32)MessageMemberColumn.MessageBody)
                {
                    body = reader.GetString(ordinals[(Int32)MessageMemberColumn.MessageBody]);
                }

                return new MessageMember(reader.GetInt32(ordinals[(Int32)MessageMemberColumn.MessageListID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumn.MemberID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumn.TargetMemberID]),
                    reader.GetBoolean(ordinals[(Int32)MessageMemberColumn.DirectionFlag]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumn.GroupID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumn.MemberFolderID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumn.MessageID]),
                    statusMask,
                    (MailType)reader.GetInt32(ordinals[(Int32)MessageMemberColumn.MessageTypeID]),
                    insertDate,
                    numBytes,
                    body);
            }
            catch (Exception Ex)
            {
                throw (new Exception("Error populating MessageMember object.", Ex));
            }
        }

        private static MessageMember convertToMessageMemberWithCount(SqlDataReader reader, Int32[] ordinals)
        {
            try
            {
                MessageStatus statusMask = MessageStatus.Nothing;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCounts.StatusMask]))
                {
                    statusMask = (MessageStatus)reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.StatusMask]);
                }

                DateTime insertDate = DateTime.MinValue;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCounts.InsertDate]))
                {
                    insertDate = reader.GetDateTime(ordinals[(Int32)MessageMemberColumnWithCounts.InsertDate]);
                }

                Int32 numBytes = 0;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCounts.NumBytes]))
                {
                    numBytes = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.NumBytes]);
                }

                int unReadMessageCount = 0;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCounts.UnreadMessageCount]))
                {
                    unReadMessageCount = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.UnreadMessageCount]);
                }

                int unReadVipCount = 0;
                if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCounts.UnreadVipCount]))
                {
                    unReadVipCount = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.UnreadVipCount]);
                }

                int draftCount = Constants.NULL_INT;
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.DraftCount)
                {
                    draftCount = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCountsV2.DraftCount]);
                }

                string body = "";
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.MessageBody)
                {
                    if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCountsV2.MessageBody]))
                    {
                        body = reader.GetString(ordinals[(Int32)MessageMemberColumnWithCountsV2.MessageBody]);
                    }
                }

                int vipCount = Constants.NULL_INT;
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.VipCount)
                {
                    vipCount = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCountsV2.VipCount]);
                }

                int lastMessageListID = Constants.NULL_INT;
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.LastMessageListID)
                {
                    lastMessageListID = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCountsV2.LastMessageListID]);
                }

                int lastMessageFolderID = Constants.NULL_INT;
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.LastMessageFolderID)
                {
                    lastMessageFolderID = reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCountsV2.LastMessageFolderID]);
                }

                MessageStatus lastMessageStatusMask = MessageStatus.Nothing;
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.LastMessageStatusMask)
                {
                    lastMessageStatusMask = (MessageStatus)reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCountsV2.LastMessageStatusMask]);
                }

                DateTime lastMessageInsertDate = DateTime.MinValue;
                if (ordinals.Length > (Int32)MessageMemberColumnWithCountsV2.LastMessageInsertDate)
                {
                    if (!reader.IsDBNull(ordinals[(Int32)MessageMemberColumnWithCountsV2.LastMessageInsertDate]))
                    {
                        lastMessageInsertDate = reader.GetDateTime(ordinals[(Int32)MessageMemberColumnWithCountsV2.LastMessageInsertDate]);
                    }
                }

                var messageMember = new MessageMember(reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.MessageListID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.MemberID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.TargetMemberID]),
                    reader.GetBoolean(ordinals[(Int32)MessageMemberColumnWithCounts.DirectionFlag]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.GroupID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.MemberFolderID]),
                    reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.MessageID]),
                    statusMask,
                    (MailType)reader.GetInt32(ordinals[(Int32)MessageMemberColumnWithCounts.MessageTypeID]),
                    insertDate,
                    numBytes,
                    unReadMessageCount,
                    unReadVipCount,
                    body);

                if (draftCount > Constants.NULL_INT)
                {
                    messageMember.DraftCount = draftCount;
                }

                if (vipCount > Constants.NULL_INT)
                {
                    messageMember.VipCount = vipCount;
                }

                if (lastMessageListID > Constants.NULL_INT)
                {
                    messageMember.LastMessageListID = lastMessageListID;
                }

                if (lastMessageInsertDate != null && lastMessageInsertDate > DateTime.MinValue)
                {
                    messageMember.LastMessageInsertDate = lastMessageInsertDate;
                }

                if (lastMessageFolderID > Constants.NULL_INT)
                {
                    messageMember.LastMessageFolderID = lastMessageFolderID;
                }

                messageMember.LastMessageStatusMask = lastMessageStatusMask;

                return messageMember;
            }
            catch (Exception Ex)
            {
                throw (new Exception("Error populating MessageMember object.", Ex));
            }
        }
		#endregion
	}

	#region Enumerations
	/// <summary> 
	/// Columns returned by up_MessageList_List 
	/// Note: the integer values associated with this enum are used as array indices only -- the order of the values does not matter.
	/// </summary>
	internal enum MessageListColumn : int
	{
		MessageListID = 0,
		MemberID = 1,
		TargetMemberID = 2,
		DirectionFlag = 3,
		GroupID = 4,
		MemberFolderID = 5,
		MessageID = 6,
		StatusMask = 7,
		MessageTypeID = 8,
		UpdateDate = 9,
		InsertDate = 10,
		NumBytes = 11
	}

    /// <summary>
    /// Columns returned by any proc that pulls back both MessageList and Message together for full EmailMessage
    /// up_IMHistory_Message_List
    /// </summary>
    internal enum EmailMessageColumn : int
    {
        MessageListID = 0,
        MemberID = 1,
        TargetMemberID = 2,
        DirectionFlag = 3,
        GroupID = 4,
        MemberFolderID = 5,
        MessageID = 6,
        StatusMask = 7,
        MessageTypeID = 8,
        UpdateDate = 9,
        InsertDate = 10,
        NumBytes = 11,
        MessageHeader = 12,
        MessageBody = 13,
        MessageInsertDate = 14,
        OpenDate = 15
    }

    internal enum EmailThreadedMessageColumn : int
    {
        MessageListID = 0,
        MemberID = 1,
        TargetMemberID = 2,
        DirectionFlag = 3,
        GroupID = 4,
        MemberFolderID = 5,
        MessageID = 6,
        StatusMask = 7,
        MessageTypeID = 8,
        UpdateDate = 9,
        InsertDate = 10,
        NumBytes = 11,
        MessageHeader = 12,
        MessageBody = 13,
        MessageInsertDate = 14,
        OpenDate = 15,
        ThreadID = 16
    }

    /// <summary> 
    /// Columns returned by any proc that pulls back list of members based on MessageList
    /// up_MessageList_Members 
    /// up_IMHistory_Member_List
    /// up_Mail_Conversation_Inbox_Member_List
    /// Note: the integer values associated with this enum are used as array indices only -- the order of the values does not matter.
    /// </summary>
    internal enum MessageMemberColumn : int
    {
        MessageListID = 0,
        MemberID = 1,
        TargetMemberID = 2,
        DirectionFlag = 3,
        GroupID = 4,
        MemberFolderID = 5,
        MessageID = 6,
        StatusMask = 7,
        MessageTypeID = 8,
        UpdateDate = 9,
        InsertDate = 10,
        NumBytes = 11,
        MessageBody = 12
    }

    /// <summary> 
    /// Solely for performance reasons
    /// Columns returned by any proc that pulls back list of members based on MessageList
    /// up_Mail_Conversation_Inbox_Member_List
    /// Note: the integer values associated with this enum are used as array indices only -- the order of the values does not matter.
    /// </summary>
    internal enum MessageMemberColumnWithCounts : int
    {
        MessageListID = 0,
        MemberID = 1,
        TargetMemberID = 2,
        DirectionFlag = 3,
        GroupID = 4,
        MemberFolderID = 5,
        MessageID = 6,
        StatusMask = 7,
        MessageTypeID = 8,
        UpdateDate = 9,
        InsertDate = 10,
        NumBytes = 11,
        UnreadMessageCount = 12,
        UnreadVipCount = 13,
        MessageBody = 14
    }

    internal enum MessageMemberColumnWithCountsV2 : int
    {
        MessageListID = 0,
        MemberID = 1,
        TargetMemberID = 2,
        DirectionFlag = 3,
        GroupID = 4,
        MemberFolderID = 5,
        MessageID = 6,
        StatusMask = 7,
        MessageTypeID = 8,
        UpdateDate = 9,
        InsertDate = 10,
        NumBytes = 11,
        UnreadMessageCount = 12,
        UnreadVipCount = 13,
        MessageBody = 14,
        DraftCount = 15,
        VipCount = 16,
        LastMessageListID = 17,
        LastMessageFolderID = 18,
        LastMessageStatusMask = 19,
        LastMessageInsertDate = 20
    }

	#endregion
}
