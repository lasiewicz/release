using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Email.ValueObjects;

using System;
using System.Collections;
using System.Data;
using System.Diagnostics;

namespace Matchnet.Email.BusinessLogic
{
	/// <summary>
	/// Summary description for TeaseBL.
	/// </summary>
	public class TeaseBL : MarshalByRefObject
	{
		private Cache _cache = null;

		public TeaseCollection GetTeaseCollection(int teaseCategoryID, int groupID)
		{
			Tease tease = null;
			TeaseCollection teaseResult = null;
			int teaseID = 0;
			string teaseResourceKey = null;
			DataTable resultsTable = null;

			teaseResult = _cache.Get(TeaseKey.GetCacheKey(teaseCategoryID, groupID)) as TeaseCollection;

			if(teaseResult == null) 
			{
				Command command = new Command("mnSystem", "up_Tease_List", 0);
				command.AddParameter("@TeaseCategoryID", SqlDbType.Int, ParameterDirection.Input, teaseCategoryID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);

				resultsTable = Client.Instance.ExecuteDataTable(command);

				teaseResult = new TeaseCollection();

				foreach(DataRow row in resultsTable.Rows) 
				{
					teaseID = Convert.ToInt32(row["TeaseID"]);
					teaseResourceKey = Convert.ToString(row["ResourceKey"]);
					tease = new Tease(teaseID, teaseResourceKey);
					teaseResult.Add(tease);
				}
				teaseResult.CachedResult = false;
				teaseResult.SetCacheKey = TeaseKey.GetCacheKey(teaseCategoryID, groupID);
				CacheTease(teaseResult);
			}
			else 
			{
				teaseResult.CachedResult = true;
			}
			return teaseResult;
		}

		public TeaseCategoryCollection GetTeaseCategoryCollection(int groupID) 
		{
			TeaseCategory aTeaseCategory = null;
			TeaseCategoryCollection categoryCollection = null;
			int teaseCategoryID = 0;
			string teaseCategoryResourceKey = null;
			string teaseCategoryPromoResourceKey = null;
			DataTable resultsTable = null;
			DateTime beginDate;
			DateTime endDate;

			categoryCollection = _cache.Get(TeaseCategoryKey.GetCacheKey(groupID)) as TeaseCategoryCollection;

			if(categoryCollection == null) 
			{
				Command command = new Command("mnSystem", "up_TeaseCategory_List", 0);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);

				resultsTable = Client.Instance.ExecuteDataTable(command);
				categoryCollection = new TeaseCategoryCollection();

				foreach(DataRow row in resultsTable.Rows) 
				{
					teaseCategoryID = Convert.ToInt32(row["TeaseCategoryID"]);
					teaseCategoryResourceKey = Convert.ToString(row["ResourceKey"]);
					
					if(row["PromoResourceKey"] == DBNull.Value)
					{
						aTeaseCategory = new TeaseCategory(teaseCategoryID, teaseCategoryResourceKey);
					}
					else
					{
						teaseCategoryPromoResourceKey = Convert.ToString(row["PromoResourceKey"]);
						beginDate = Conversion.CDateTime(row["BeginDate"]);
						endDate = Conversion.CDateTime(row["EndDate"]);
						aTeaseCategory = new TeaseCategory(teaseCategoryID, teaseCategoryResourceKey, teaseCategoryPromoResourceKey, beginDate, endDate);
					}

					categoryCollection.Add(aTeaseCategory);
				}
				categoryCollection.CachedResult = false;
				categoryCollection.SetCacheKey = TeaseCategoryKey.GetCacheKey(groupID);
				CacheCategories(categoryCollection);
			} 
			else 
			{
				categoryCollection.CachedResult = true;
			}
			return categoryCollection;
		}

		/// <summary>
		/// Accepts an IReplicable object and updates the local service cache.
		/// </summary>
		/// <param name="pReplicableObject"></param>
		public void CacheReplicatedObject(IReplicable pReplicableObject)
		{
			switch (pReplicableObject.GetType().Name)
			{
				case "TeaseCategoryCollection":
					CacheCategories((TeaseCategoryCollection)pReplicableObject);
					break;
				case "TeaseCollection":
					CacheTease((TeaseCollection)pReplicableObject);
					break;
				default:
					throw new Exception("invalid replication object type received (" + pReplicableObject.GetType().ToString() + ")");
			}
		}

		#region private Cache Routines
		private void CacheCategories(TeaseCategoryCollection pTeaseCategoryCollection)
		{
			pTeaseCategoryCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pTeaseCategoryCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEASESVC_CACHE_TTL_SVC"));
			_cache.Insert(pTeaseCategoryCollection);
		}

		private void CacheTease(TeaseCollection pTeaseCollection)
		{
			pTeaseCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pTeaseCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("TEASESVC_CACHE_TTL_SVC"));
			_cache.Insert(pTeaseCollection);
		}
		#endregion

		#region singleton
		public readonly static TeaseBL Instance = new TeaseBL();
		#endregion

		#region constructor
		private TeaseBL()
		{
			_cache = Cache.Instance;
		}
		#endregion
	}
}
