using System;
using System.Collections;
using System.Data;
using System.Diagnostics;

using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ReplicationActions;


namespace Matchnet.Email.BusinessLogic
{
	/// <summary>
	/// Summary description for EmailFolderBL.
	/// </summary>
	public class EmailFolderBL : MarshalByRefObject
	{
		#region Private Members
		private const string LOGICALDB_IMAIL = "mnIMailNew";
		private const int MAX_MEMBER_FOLDER_DESCRIPTION_LENGTH = 20;
		private const int MAX_SYSTEM_FOLDER_ID = 100;
		private Cache _cache = Cache.Instance;
		private System.Web.Caching.CacheItemRemovedCallback _expireCallback;

		private enum FolderSaveType
		{
			Insert = 1,
			Update = 2
		}

		#endregion

		#region Events
		public delegate void ReplicationActionEventHandler(IReplicationAction replicationAction);
		public event ReplicationActionEventHandler ReplicationActionRequested;

		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;
		#endregion

		#region Constructors
		private EmailFolderBL()
		{
			_expireCallback = new System.Web.Caching.CacheItemRemovedCallback(this.expireCallback);
		}

		public readonly static EmailFolderBL Instance = new EmailFolderBL();
		#endregion
		
		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <param name="statsFlag"></param>
		/// <returns></returns>
		public EmailFolderCollection RetrieveFolders(string clientHostName, CacheReference cacheReference, int memberID, int groupID, bool statsFlag)
		{
			try
			{
				EmailFolderCollection emailFolderCollection = retrieveFolders(memberID, groupID, statsFlag);

				if (clientHostName != null && cacheReference != null)
				{
					emailFolderCollection.ReferenceTracker.Add(clientHostName, cacheReference);
				}

				return emailFolderCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving folders.", ex));
			}
		}

		public void Delete(string clientHostName, int memberFolderID, int memberID, int groupID)
		{
			try
			{
				EmailFolderCollection folders = retrieveFolders(memberID, groupID);
				//Need to avoid orphaning any messages in the folder
				deleteAllMessagesInFolder(clientHostName, folders.FindByMemberFolderID(memberFolderID));

				Command command = new Command(LOGICALDB_IMAIL, "up_MemberFolder_Delete", memberID);
				command.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
				Client.Instance.ExecuteAsyncWrite(command);

				delete(clientHostName, memberID, groupID, memberFolderID, true);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when deleting member folder.", ex));
			}
		}

		private void delete(string clientHostName, Int32 memberID, Int32 groupID, Int32 memberFolderID, bool replicate)
		{
			EmailFolderCollection folders = retrieveFolders(memberID, groupID);
			folders.Remove(memberFolderID);

			if (replicate)
			{
				ReplicationActionRequested(new FolderDeleteAction(memberID, groupID, memberFolderID));
			}
			SynchronizationRequested(folders.GetCacheKey(), folders.ReferenceTracker.Purge(clientHostName));
		}

		private void deleteAllMessagesInFolder(string clientHostName, EmailFolder folder)
		{
			if (folder == null)
			{
				return;
			}

			MessageListCollection messageListCollection = EmailMessageBL.Instance.RetrieveMessageList(null, null, folder.MemberID, folder.CommunityID);

			ReplicationActionCollection actions = new ReplicationActionCollection();
			foreach (MessageList messageList in messageListCollection)
			{
				if (messageList.MemberFolderID == folder.MemberFolderID)
				{
					actions.Add(new MessageListDeleteAction(messageList.MemberID, messageList.GroupID, messageList.MessageListID));
					actions.Add(new MessageDeleteAction(messageList.MemberID, messageList.TargetMemberID, messageList.MessageID));
				}
			}

			EmailMessageBL.Instance.PlayReplicationAction(clientHostName, actions, true, true);
		}

		
		public FolderSaveResult Save(string clientHostName, int memberID, int groupID, string description)
		{
			int folderID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("MemberFolderID");
			return save(clientHostName, memberID, groupID, description, folderID, FolderSaveType.Insert);
		}

		public FolderSaveResult Save(string clientHostName, int memberID, int groupID, string description, int memberFolderID)
		{
			return save(clientHostName, memberID, groupID, description, memberFolderID, FolderSaveType.Update);
		}

		private FolderSaveResult save(string clientHostName, int memberID, int groupID, string description, int memberFolderID, FolderSaveType saveType)
		{
			try
			{
				if(description.Trim().Length <= MAX_MEMBER_FOLDER_DESCRIPTION_LENGTH)
				{
					EmailFolderCollection emailFolderCollection = retrieveFolders(memberID, groupID);

					if (emailFolderCollection.ContainsDescription(description))
					{
						return new FolderSaveResult(FolderSaveResultStatus.FolderNameExists, memberFolderID);
					}
					else
					{
						//Write to the DB
						Command command = new Command(LOGICALDB_IMAIL, "up_MemberFolder_Save", memberID);
						command.AddParameter("@SaveType", SqlDbType.Int, ParameterDirection.Input, (Int32) saveType);
						command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
						command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
						command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, description);
						command.AddParameter("@MemberFolderID", SqlDbType.Int, ParameterDirection.Input, memberFolderID);
						Client.Instance.ExecuteAsyncWrite(command);

                        save(clientHostName, memberID, groupID, description, memberFolderID, true);

						return new FolderSaveResult(FolderSaveResultStatus.Success, memberFolderID);
					}
				} 
				else 
				{
					throw(new Exception("BL error occured folder description has too many letters."));
				}
			} 
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving member folder.", ex));
			}
		}

		private void save(string clientHostName, int memberID, int groupID, string description, int memberFolderID, bool replicate)
		{
			//Determine whether this is a new folder or existing folder and update the cache accordingly
			EmailFolderCollection emailFolderCollection = retrieveFolders(memberID, groupID);
			EmailFolder emailFolder = emailFolderCollection.FindByMemberFolderID(memberFolderID);
			if (emailFolder == null)
			{
				emailFolder = new EmailFolder(memberFolderID, memberID, groupID, description, false);
				emailFolderCollection.Add(emailFolder);
			}
			else
			{
				emailFolder.Description = description;
			}

			//Request replication and cache synchronization
			if (replicate)
			{
				ReplicationActionRequested(new FolderSaveAction(memberID, groupID, memberFolderID, description));
			}
			SynchronizationRequested(emailFolderCollection.GetCacheKey(), emailFolderCollection.ReferenceTracker.Purge(clientHostName));
		}

		#endregion

		#region Private Methods

		private void expireCallback(string key, object value, System.Web.Caching.CacheItemRemovedReason callbackreason) 
		{
			EmailFolderCollection folders = value as EmailFolderCollection;
			SynchronizationRequested(folders.GetCacheKey(), folders.ReferenceTracker.Purge(Constants.NULL_STRING));
			//TODO: Perform periodic GC as in List svc?
		}

		internal EmailFolderCollection retrieveFolders(int memberID, int groupID)
		{
			return retrieveFolders(memberID, groupID, false);
		}

		internal EmailFolderCollection retrieveFolders(int memberID, int groupID, bool statsFlag)
		{
			EmailFolderCollection oEmailFolderCollection = _cache.Get(EmailFolderCollection.GetCacheKey(memberID, groupID)) as EmailFolderCollection;

			//statsFlag indicates whether the caller wants to retrieve the folder sizes, messages counts, etc. along with the folder names
			//If the cached copy of the EmailFolderCollection does not include these stats and the caller wants them, we need to make a DB call to retrieve them.
			if (oEmailFolderCollection == null || (statsFlag && !oEmailFolderCollection.StatsFlag))
			{
				Command command = new Command(LOGICALDB_IMAIL, "up_MemberFolder_List", memberID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				oEmailFolderCollection = convertToEmailFolderCollection(memberID, groupID, statsFlag, dt);
				cacheFolders(oEmailFolderCollection);
			}

			return oEmailFolderCollection;
		}

		public void PlayReplicationAction(IReplicationAction replicationAction)
		{
			switch (replicationAction.GetType().Name)
			{
				case "FolderSaveAction":
					FolderSaveAction folderSaveAction = replicationAction as FolderSaveAction;
					save(null, folderSaveAction.MemberID, folderSaveAction.GroupID, folderSaveAction.Description, folderSaveAction.MemberFolderID, false);
					break;
				case "FolderDeleteAction":
					FolderDeleteAction folderDeleteAction = replicationAction as FolderDeleteAction;
					delete(null, folderDeleteAction.MemberID, folderDeleteAction.GroupID, folderDeleteAction.MemberFolderID, false);
					break;
			}
		}

		private void cacheFolders(EmailFolderCollection emailFolderCollection)
		{
			int cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_FOLDER_CACHE_TTL_SVC"));
			emailFolderCollection.CacheTTLSeconds = cacheTTL;
			emailFolderCollection.CachePriority = CacheItemPriorityLevel.Normal;
			emailFolderCollection.CacheMode = CacheItemMode.Absolute;
			_cache.Insert(emailFolderCollection, _expireCallback);
		}

		private EmailFolder convertToEmailFolder(Int32 groupID, Int32 memberID, DataRow pDataRow)
		{
			Int32 memberFolderID = Conversion.CInt(pDataRow["memberFolderID"]);
			String description = pDataRow["description"].ToString();

			EmailFolder oEmailFolder = new EmailFolder(memberFolderID,
				memberID,
				groupID,
				description,
				isSystemFolderID(memberFolderID));

			return oEmailFolder;
		}

		private EmailFolderCollection convertToEmailFolderCollection(Int32 memberID, Int32 groupID, bool statsFlag, DataTable pDataTable)
		{
			EmailFolderCollection folders = new EmailFolderCollection(memberID, groupID, statsFlag);

			//Add System folders
			Array systemFolders = Enum.GetValues(typeof(SystemFolders));
			foreach (Int32 memberFolderID in systemFolders)
			{
				string description = Enum.GetName(typeof(SystemFolders), memberFolderID);
				folders.Add(new EmailFolder(memberFolderID, memberID, groupID, description, true));
			}

			//Add custom folders from DB
			foreach (DataRow row in pDataTable.Rows)
			{
				folders.Add(convertToEmailFolder(groupID, memberID, row));
			}

			return folders;
		}

		private bool isSystemFolderID(int folderID)
		{
			return (folderID <= MAX_SYSTEM_FOLDER_ID);
		}
		#endregion
	}
}
