using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ReplicationActions;
using System.Web;
using Spark.ActivityRecording.Processor.ServiceAdapter;
using Spark.ActivityRecording.Processor.ValueObjects;

namespace Matchnet.Email.BusinessLogic
{
	/// <summary>
	/// Summary description for EmailMessageBL.
	/// </summary>
	public class EmailMessageBL : MarshalByRefObject
	{
		#region Private Members
		//private const int GC_THRESHOLD = 10000;
		private const int MAXIMUM_EMAIL_CONTENT_SIZE_BYTES = 1000;
		private const string MAXIMUM_EMAIL_FOLDER_DESCRIPTION = "20";

		private Cache _cache = Cache.Instance;
		private System.Web.Caching.CacheItemRemovedCallback _expireMessagesCallback;
	    private bool _TraceActivity = false;
		#endregion

		#region Events

		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;

		public delegate void ReplicationActionEventHandler(IReplicationAction replicationAction);
		public event ReplicationActionEventHandler ReplicationActionRequested;

		public delegate void MessageSavedEventHandler();
		public event MessageSavedEventHandler MessageSaved;

		public delegate void MessageSentEventHandler(MailType mailType);
		public event MessageSentEventHandler MessageSent;

		public delegate void MessageListRetrievedEventHandler(bool cacheHit);
		public event MessageListRetrievedEventHandler MessageListRetrieved;

		public delegate void MessageRetrievedEventHandler(bool withContent, bool cacheHit);
		public event MessageRetrievedEventHandler MessageRetrieved;

		public delegate void MessageListRemovedEventHandler();
		public event MessageListRemovedEventHandler MessageListRemoved;

		public delegate void MessageRemovedEventHandler();
		public event MessageRemovedEventHandler MessageRemoved;

		#endregion

		#region Constructors
		private EmailMessageBL()
		{
			_expireMessagesCallback = new System.Web.Caching.CacheItemRemovedCallback(this.expireMessagesCallback);
		}
		
		public readonly static EmailMessageBL Instance = new EmailMessageBL();
		#endregion

		#region Public Methods

		public MessageSaveResult SaveMessage(string clientHostName, MessageSave messageSave)
		{
			try
			{
				IReplicationAction action;
				Int32 messageID;
				Int32 messageListID;
				
				Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SVC"));

				if (messageSave.MessageListID > 0) //updating existing draft
				{
                    MessageListCollection messages = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID);
					messageListID = messageSave.MessageListID;

					MessageList messageList = messages != null ? messages[messageListID] : null;

                    if (messageList == null)
                    {
                        messages = retrieveMessageList(messageSave.FromMemberID, messageSave.GroupID, true);
                        messageList = messages != null ? messages[messageListID] : null;
                    }
						
					if (messageList == null)
					{
						return new MessageSaveResult(MessageSaveError.InvalidMessageListID);
					}

					messageID = messageList.MessageID;

					action = messageSave.GetMessageSaveAction(true, messageID, MessageSaveType.Update | MessageSaveType.Draft, cacheTTL);
				}
				else //initial save of draft
				{
					messageListID = KeySA.Instance.GetKey("MemberMailID");
					messageID = KeySA.Instance.GetKey("MailID");
					DateTime insertDate = DateTime.Now;

					ReplicationActionCollection actions = new ReplicationActionCollection();
					actions.Add(messageSave.GetMessageSaveAction(true, messageID, MessageSaveType.Insert, cacheTTL));
                    if((messageSave.MailOption & MailOption.VIP )== MailOption.VIP)
                        actions.Add(messageSave.GetMessageListSaveAction(true, messageID, messageListID, (Int32)SystemFolders.VIPDraft, MessageListSaveType.Insert));
                    else
					    actions.Add(messageSave.GetMessageListSaveAction(true, messageID, messageListID, (Int32) SystemFolders.Draft, MessageListSaveType.Insert));
					action = actions;
				}

				MessageSaved();

                if (messageSave.SiteID == Constants.NULL_INT)
                {                    
                    PlayReplicationAction(action, true, true);
                }
                else
                {
                    PlayReplicationAction(messageSave, action, true, true);
                }

				return new MessageSaveResult(messageID, messageListID);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when saving draft."
					+ "  MemberID: " + messageSave.FromMemberID
					+ ", GroupID: " + messageSave.GroupID,
					ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="messageSend"></param>
        /// <param name="saveInSent"></param>
		/// <param name="replyMessageListID"></param>
		/// <param name="sender"></param>
		/// <param name="recipient"></param>
		/// <returns></returns>
		public MessageSendResult SendMessage(string clientHostName, MessageSend messageSend, bool saveInSent, Int32 replyMessageListID, bool sender, bool recipient)
		{
			ReplicationActionCollection actions = new ReplicationActionCollection();
            bool isFreeReplyToVIP = (messageSend.MessageSave.MailOption & MailOption.FreeReplyMessage) == MailOption.FreeReplyMessage;

			if (replyMessageListID > 0)
			{
                // this block does not care for sender or recipient because the messagelist won't be found on the recipient's partition.  this block could easily
                // have if(sender) and probably behave the same.
                
                // this is a response, set the reply status on the original message.
                setRepliedBit(messageSend.MessageSave.FromMemberID, messageSend.MessageSave.GroupID, replyMessageListID, clientHostName, isFreeReplyToVIP);
			}

			Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SVC"));

			// if this is a free reply to a vip message, it will be treated as a vip message as well although it doesn't come out of anyone's balance of vip emails
            if (isFreeReplyToVIP)
            {
                messageSend.MessageSave.MailOption |= MailOption.VIP;
            }
            
            if (sender && saveInSent)
			{
				actions.Add(messageSend.GetMessageSaveAction(true, cacheTTL));

			    MessageListSaveAction messageListSaveAction = messageSend.GetMessageListSaveAction(true);
                //IM messages should be marked as read instead of new
                if (messageSend.MessageSave.MailType == MailType.InstantMessage)
                {
                    messageListSaveAction.MessageList.StatusMask |= MessageStatus.Read;
                    messageListSaveAction.MessageList.StatusMask &= ~MessageStatus.New;
                }
                actions.Add(messageListSaveAction);
			}
			if (recipient)
			{
				//Only need to save a Message record if sender did not save a copy or if
				//sender and recipient are on different DB partitions.
				if (!saveInSent || !EmailMessageDB.SharePartition(messageSend.MessageSave.FromMemberID, messageSend.MessageSave.ToMemberID))
				{
					actions.Add(messageSend.GetMessageSaveAction(false, cacheTTL));
				}
			    MessageListSaveAction messageListSaveAction = messageSend.GetMessageListSaveAction(false);

                //IM messages should be marked as read instead of new
                if (messageSend.MessageSave.MailType == MailType.InstantMessage)
                {
                    messageListSaveAction.MessageList.StatusMask |= MessageStatus.Read;
                    messageListSaveAction.MessageList.StatusMask &= ~MessageStatus.New;
                }

                // if this is a vip message (not reply to a vip message) that counted against someone's balance, then set the StatusMask with OneFreeReply status
                if ((messageSend.MessageSave.MailOption & MailOption.VIP) == MailOption.VIP && !isFreeReplyToVIP)
                {
                    // the recipient of this message gets one free reply to this message
                    messageListSaveAction.MessageList.StatusMask |= MessageStatus.OneFreeReply;
                }
                // if this is a FraudHold mail, only write to the DB and hide it from cache
                if ((messageSend.MessageSave.MailOption & MailOption.FraudHold) == MailOption.FraudHold)
                {
                    messageListSaveAction.MessageList.StatusMask |= MessageStatus.FraudHold;
                }
                actions.Add(messageListSaveAction);
			}

			//Retrieve the unread count for the recipient
			Int32 unreadCount = Constants.NULL_INT;
			if (recipient)
			{
				MessageListCollection messageListCollection = retrieveMessageList(messageSend.MessageSave.ToMemberID, messageSend.MessageSave.GroupID);
				unreadCount = messageListCollection.GetUnreadCount((Int32) SystemFolders.Inbox) + 1;
			}
			
            //complete message attachment process by tying previously uploaded attachments to the message
            //note: restrict initially to just IM messages
            //note: this only needs to happen for sender, this will save to both sender/recipient partitions
		    List<MessageAttachmentMetadata> messageAttachmentMetadataList = null;
            if (sender && messageSend.MessageSave.MailType == MailType.InstantMessage)
            {
                messageAttachmentMetadataList = GetEmbeddedMessageAttachmentData(messageSend.MessageSave.MessageBody);
                if (messageAttachmentMetadataList.Count > 0)
                {
                    foreach (MessageAttachmentMetadata i in messageAttachmentMetadataList)
                    {
                        SaveMessageAttachmentFromInterim(messageSend.MessageSave.FromMemberID,
                                                         messageSend.MessageSave.ToMemberID, messageSend.MessageID, i.AttachmentID);
                    }
                }
            }
            
            //reporting
			if (sender)
			{
                //MailLog
                //Only one mnMailLog entry is needed for each email
                EmailLogBL.Instance.InsertMailLogEntry(messageSend.MessageID, messageSend.MessageSave.GroupID,
                                                       messageSend.MessageSave.FromMemberID,
                                                       messageSend.MessageSave.ToMemberID,
                                                       MessageStatus.New | MessageStatus.Show,
                                                       messageSend.MessageSave.MailType, messageSend.MessageSave.MailOption,
                                                       messageSend.MessageSave.ThreadID);

                //Activity Recording (via RabbitMQ path) for reporting
                //for now we will only record this for IM Messages
			    if (messageSend.MessageSave.MailType == MailType.InstantMessage)
			    {
			        try
			        {
			            StringBuilder IMMessageXML = new StringBuilder();
                        IMMessageXML.Append("<IMMessage>");
                        IMMessageXML.Append("<MessageID>" + messageSend.MessageID.ToString() + "</MessageID>");
                        IMMessageXML.Append("<ThreadID>" + messageSend.MessageSave.ThreadID + "</ThreadID>");
                        if (messageAttachmentMetadataList != null && messageAttachmentMetadataList.Count > 0)
                        {
                            IMMessageXML.Append("<Attachments>");
                            IMMessageXML.Append("<Count>" + messageAttachmentMetadataList.Count.ToString() + "</Count>");
                            foreach (MessageAttachmentMetadata j in messageAttachmentMetadataList)
                            {
                                IMMessageXML.Append("<Attachment>");
                                IMMessageXML.Append("<AttachmentID>" + j.AttachmentID.ToString() + "</AttachmentID>");
                                IMMessageXML.Append("<AttachmentType>" + j.AttachmentTypeID.ToString() + "</AttachmentType>");
                                IMMessageXML.Append("<AttachmentExt>" + j.FileExtension + "</AttachmentExt>");
                                IMMessageXML.Append("</Attachment>");
                            }
                            IMMessageXML.Append("</Attachments>");
                        }
                        
                        IMMessageXML.Append("<Timestamp>" + messageSend.MessageSave.InsertDate.ToString() + "</Timestamp>");
                        IMMessageXML.Append("</IMMessage>");
			            if (_TraceActivity)
			            {
			                Trace.WriteLine(
			                    String.Format(
			                        "EmailMessageBL SaveMessage activity recording. FromMember {0}, ToMember {1}, ActionType {2}, SiteID {3}, XML {4} ",
			                        messageSend.MessageSave.FromMemberID,
			                        messageSend.MessageSave.ToMemberID,
			                        ActionType.IM,
			                        messageSend.MessageSave.SiteID,
			                        IMMessageXML.ToString()));
			            }
			            ActivityRecordingProcessorAdapter.Instance.RecordActivity(messageSend.MessageSave.FromMemberID, messageSend.MessageSave.ToMemberID,
                                                                                  (int) ActionType.IM, messageSend.MessageSave.SiteID,
                                                                                  IMMessageXML.ToString(), CallingSystem.Email);
			        }
			        catch (Exception exActivity)
			        {
			            //likely rabbitMQ is not set up on this environment
                        //RollingFileLogger.Instance.LogInfoMessage(Matchnet.Email.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "EmailMessageBL", "SendMessage activity recording exception: " + exActivity.Message, null);
                        StringBuilder errorMsg = new StringBuilder();
			            errorMsg.Append(exActivity.Message);
                        if (exActivity.InnerException != null)
                        {
                            errorMsg.Append(exActivity.InnerException.Message);
                        }
                        errorMsg.AppendLine(exActivity.StackTrace);
                        Trace.WriteLine("EmailMessageBL SaveMessage activity recording exception: " + errorMsg.ToString());
                        new ServiceBoundaryException(Matchnet.Email.ValueObjects.ServiceConstants.SERVICE_NAME,
                                             "SaveMessage activity recording Error: " + exActivity.Message, exActivity);
			        }
			    }

			    MessageSent(messageSend.MessageSave.MailType);

			}

            //mail message/messagelist actions
            if (messageSend.MessageSave.SiteID == Constants.NULL_INT)
            {                    
                PlayReplicationAction(actions, true, true);
            }
            else
            {
                PlayReplicationAction(messageSend.MessageSave, actions, true, true);
            }

            return new MessageSendResult(messageSend.MessageID, messageSend.SenderMessageListID, messageSend.RecipientMessageListID, unreadCount, messageSend.MessageSave.MailOption);
		}

		public void PlayReplicationAction(IReplicationAction replicationAction, bool replicate, bool writeToDB)
		{
			PlayReplicationAction(Constants.NULL_STRING, replicationAction, replicate, writeToDB);
		}

        public void PlayReplicationAction(MessageSave messageSave, IReplicationAction replicationAction, bool replicate, bool writeToDB)
        {
            PlayReplicationAction(messageSave, Constants.NULL_STRING, replicationAction, replicate, writeToDB);
        }

		public void PlayReplicationAction(string clientHostName, IReplicationAction replicationAction, bool replicate, bool writeToDB)
		{
			switch (replicationAction.GetType().Name)
			{
				case "ReplicationActionCollection":
					ReplicationActionCollection actions = replicationAction as ReplicationActionCollection;
					foreach (IReplicationAction action in actions)
					{
						PlayReplicationAction(clientHostName, action, false, writeToDB);
					}
					break;
				case "MessageSaveAction":
					MessageSaveAction messageSaveAction = replicationAction as MessageSaveAction;
					if (writeToDB)
					{
						EmailMessageDB.MessageSave(messageSaveAction.MemberID, messageSaveAction.Message, messageSaveAction.SaveType);
					}
					CacheUtil.MessageSave(_cache, messageSaveAction);
					synchronize(messageSaveAction.Message.MessageID, clientHostName);
					break;
				case "MessageListSaveAction":
					MessageListSaveAction messageListSaveAction = replicationAction as MessageListSaveAction;
					if (writeToDB)
					{
						EmailMessageDB.MessageListSave(messageListSaveAction.MemberID, messageListSaveAction.MessageList, messageListSaveAction.SaveType);
					}
					CacheUtil.MessageListSave(_cache, messageListSaveAction);

                    //Instant messages are cached only in couchbase in the SA level and are currently kept separate from the regular messageList (which is for the inbox)
                    if (messageListSaveAction.MessageList != null && messageListSaveAction.MessageList.MailTypeID != MailType.InstantMessage)
                    {
                        synchronize(messageListSaveAction.MemberID, messageListSaveAction.MessageList.GroupID, clientHostName);
                    }
					break;
				case "MessageDeleteAction":
					MessageDeleteAction messageDeleteAction = replicationAction as MessageDeleteAction;
					if (!EmailMessageDB.SharePartition(messageDeleteAction.MemberID, messageDeleteAction.TargetMemberID)
						|| !EmailMessageDB.MessageListExists(messageDeleteAction.TargetMemberID, messageDeleteAction.MessageID))
					{
						if (writeToDB)
						{
							EmailMessageDB.MessageDelete(messageDeleteAction.MemberID, messageDeleteAction.MessageID);
						}
						CacheUtil.MessageDelete(_cache, messageDeleteAction);
						synchronize(messageDeleteAction.MessageID, clientHostName);
					}
					break;
				case "MessageListDeleteAction":
					MessageListDeleteAction messageListDeleteAction = replicationAction as MessageListDeleteAction;
					if (writeToDB)
					{
						EmailMessageDB.MessageListDelete(messageListDeleteAction.MemberID, messageListDeleteAction.MessageListID);
					}
					CacheUtil.MessageListDelete(_cache, messageListDeleteAction);
                    synchronize(messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, clientHostName);
					break;
				case "MessageOpenedAction":
					MessageOpenedAction messageOpenedAction = replicationAction as MessageOpenedAction;
					if (writeToDB)
					{
						EmailMessageDB.MessageSave(messageOpenedAction.MemberID, messageOpenedAction.MessageID, messageOpenedAction.OpenDate);
						if (!EmailMessageDB.SharePartition(messageOpenedAction.MemberID, messageOpenedAction.TargetMemberID))
						{
							EmailMessageDB.MessageSave(messageOpenedAction.TargetMemberID, messageOpenedAction.MessageID, messageOpenedAction.OpenDate);
						}
					}
					synchronize(messageOpenedAction.MessageID, clientHostName);
					CacheUtil.MessageSave(_cache, messageOpenedAction);
					break;
                case "InstantMessagesDeleteAction":
                    InstantMessagesDeleteAction instantMessagesDeleteAction = replicationAction as InstantMessagesDeleteAction;
                    if (writeToDB)
                    {
                        EmailMessageDB.InstantMessagesDelete(instantMessagesDeleteAction.MemberID, instantMessagesDeleteAction.TargetMemberID, instantMessagesDeleteAction.GroupID);
                    }
                    break;
			}

			if (replicate)
			{
				ReplicationActionRequested(replicationAction);
			}
		}

        public void PlayReplicationAction(MessageSave messageSave, string clientHostName, IReplicationAction replicationAction, bool replicate, bool writeToDB)
        {
            //updating inbox message caching not needed for IM (we don't want IM showing up in inbox), therefore all the IM mailtype checks below
            //note: these will be revisted once we start pulling IM history and determine how that will be retrieved and cached
            switch (replicationAction.GetType().Name)
            {
                case "ReplicationActionCollection":
                    ReplicationActionCollection actions = replicationAction as ReplicationActionCollection;
                    foreach (IReplicationAction action in actions)
                    {
                        PlayReplicationAction(messageSave, clientHostName, action, false, writeToDB);
                    }
                    break;
                case "MessageSaveAction":
                    MessageSaveAction messageSaveAction = replicationAction as MessageSaveAction;
                    if (writeToDB)
                    {
                        EmailMessageDB.MessageSave(messageSaveAction.MemberID, messageSaveAction.Message, messageSaveAction.SaveType);
                    }
                    if (messageSave.MailType != MailType.InstantMessage)
                    {
                        CacheUtil.MessageSave(_cache, messageSaveAction);
                        synchronize(messageSaveAction.Message.MessageID, clientHostName);
                    }
                    break;
                case "MessageListSaveAction":
                    MessageListSaveAction messageListSaveAction = replicationAction as MessageListSaveAction;
                    if (writeToDB)
                    {
                        EmailMessageDB.MessageListSave(messageListSaveAction.MemberID, messageSave.SiteID, messageListSaveAction.MessageList, messageListSaveAction.SaveType);
                    }
                    if (messageSave.MailType != MailType.InstantMessage)
                    {
                        CacheUtil.MessageListSave(_cache, messageListSaveAction);
                        synchronize(messageListSaveAction.MemberID, messageListSaveAction.MessageList.GroupID, clientHostName);
                    }
                    break;
                case "MessageDeleteAction":
                    MessageDeleteAction messageDeleteAction = replicationAction as MessageDeleteAction;
                    if (!EmailMessageDB.SharePartition(messageDeleteAction.MemberID, messageDeleteAction.TargetMemberID)
                        || !EmailMessageDB.MessageListExists(messageDeleteAction.TargetMemberID, messageDeleteAction.MessageID))
                    {
                        if (writeToDB)
                        {
                            EmailMessageDB.MessageDelete(messageDeleteAction.MemberID, messageDeleteAction.MessageID);
                        }

                        CacheUtil.MessageDelete(_cache, messageDeleteAction);
                        synchronize(messageDeleteAction.MessageID, clientHostName);
                    }
                    break;
                case "MessageListDeleteAction":
                    MessageListDeleteAction messageListDeleteAction = replicationAction as MessageListDeleteAction;
                    if (writeToDB)
                    {
                        EmailMessageDB.MessageListDelete(messageListDeleteAction.MemberID, messageListDeleteAction.MessageListID);
                    }

                    CacheUtil.MessageListDelete(_cache, messageListDeleteAction);
                    synchronize(messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, clientHostName);
                    break;
                case "MessageOpenedAction":
                    MessageOpenedAction messageOpenedAction = replicationAction as MessageOpenedAction;
                    if (writeToDB)
                    {
                        EmailMessageDB.MessageSave(messageOpenedAction.MemberID, messageOpenedAction.MessageID, messageOpenedAction.OpenDate);
                        if (!EmailMessageDB.SharePartition(messageOpenedAction.MemberID, messageOpenedAction.TargetMemberID))
                        {
                            EmailMessageDB.MessageSave(messageOpenedAction.TargetMemberID, messageOpenedAction.MessageID, messageOpenedAction.OpenDate);
                        }
                    }
                    if (messageSave.MailType != MailType.InstantMessage)
                    {
                        synchronize(messageOpenedAction.MessageID, clientHostName);
                        CacheUtil.MessageSave(_cache, messageOpenedAction);
                    }
                    break;
            }

            if (messageSave.MailType != MailType.InstantMessage)
            {
                if (replicate)
                {
                    ReplicationActionRequested(replicationAction);
                }
            }
        }

		#endregion

		#region MessageList Retrieval

        public MessageListCollection RetrieveMessageListFromDB(string clientHostName, CacheReference cacheReference, int memberID, int groupID)
        {
            MessageListCollection messages = EmailMessageDB.RetrieveMessageList(memberID, groupID, false);

            // This data shouldn't be cached because some unwanted data will come back and show messages that we shouldn't show
            //messages.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SVC"));
            //cacheMessages(messages);

            MessageListRetrieved(false);

            return messages;
        }

        public MessageMemberCollection RetrieveMessageMemberFromDB(string clientHostName, CacheReference cacheReference, int memberID, int groupID)
        {
            MessageMemberCollection messageMembers = EmailMessageDB.RetrieveMessageMember(memberID, groupID, false);
          
            return messageMembers;
        }

		public MessageListCollection RetrieveMessageList(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID)
		{
			try
			{
				MessageListCollection messages = retrieveMessageList(memberID, groupID);

				if (clientHostName != null && cacheReference != null)
				{
					messages.ReferenceTracker.Add(clientHostName, cacheReference);
				}

				return messages;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving messages."
					+ "  MemberID: " + memberID
					+ ", GroupID: " + groupID,
					ex));
			}
		}

        private MessageListCollection retrieveMessageList(Int32 memberID, Int32 groupID)
        {
            return retrieveMessageList(memberID, groupID, false);
        }

		private MessageListCollection retrieveMessageList(Int32 memberID, Int32 groupID, bool ignoreCache)
		{
			MessageListCollection messages = ignoreCache ? null : retrieveCachedMessageList(memberID, groupID);

			bool cacheHit = true;

			if (messages == null)
			{
				cacheHit = false;
				messages = EmailMessageDB.RetrieveMessageList(memberID, groupID, true);
				messages.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SVC"));
				cacheMessages(messages);
			}

			MessageListRetrieved(cacheHit);

			return messages;
		}

		private void cacheMessages(MessageListCollection messages)
		{
			_cache.Insert(messages, _expireMessagesCallback);
		}

		private MessageListCollection retrieveCachedMessageList(Int32 memberID, Int32 groupID)
		{
			return _cache.Get(MessageListCollection.GetCacheKey(memberID, groupID)) as MessageListCollection;
		}

		#endregion

		#region Message Retrieval

		public MessageCollection RetrieveMessages(string clientHostName, CacheReference cacheReference, Int32 memberID, ArrayList messageIDs, bool withContent)
		{
			MessageCollection messages = new MessageCollection();

			foreach (Int32 messageID in messageIDs)
			{
				messages.Add(RetrieveMessage(clientHostName, cacheReference, memberID, messageID, withContent));
			}

			return messages;
		}

		public Message RetrieveMessage(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 messageID, bool withContent)
		{
			Message message = retrieveMessage(memberID, messageID, withContent);

			if (clientHostName != null && cacheReference != null)
			{
				message.ReferenceTracker.Add(clientHostName, cacheReference);
			}

			return message;
		}

		private Message retrieveMessage(Int32 memberID, Int32 messageID, bool withContent)
		{
			Message message = retrieveCachedMessage(messageID, withContent);
			bool cacheHit = true;

			if (message == null)
			{
				cacheHit = false;
				message = EmailMessageDB.RetrieveMessage(memberID, messageID, withContent);
				message.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_CACHE_TTL_SVC"));
				cacheMessage(message);
			}

			MessageRetrieved(withContent, cacheHit);

			return message;
		}

		private void cacheMessage(Message message)
		{
			_cache.Insert(message, _expireMessagesCallback);
		}

		private Message retrieveCachedMessage(Int32 messageID, bool withContent)
		{
			Message message = _cache.Get(Message.GetCacheKey(messageID)) as Message;

			if (message != null && withContent && message.MessageBody == null)
			{
				return null;
			}

			return message;
		}

		#endregion

        #region IM History
        public EmailMessageList RetrieveInstantMessages(int memberID, int targetMemberID, Int32 groupID)
        {
            try
            {
                return EmailMessageDB.RetrieveInstantMessages(memberID, targetMemberID, groupID, true);
            }
            catch (Exception Ex)
            {
                throw (new BLException(string.Format("Error retrieving IM History messages. memberID: {0}, targetMemberID: {1}, groupID: {2}", memberID, targetMemberID, groupID), Ex));
            }
        }

        public MessageMemberCollection RetrieveInstantMessageMembers(int memberID, Int32 groupID, bool filterRemovedMembers)
        {
            try
            {
                return EmailMessageDB.RetrieveInstantMessageMembers(memberID, groupID, filterRemovedMembers);
            }
            catch (Exception Ex)
            {
                throw (new BLException(string.Format("Error retrieving IM History message members. memberID: {0}, groupID: {1}", memberID, groupID), Ex));
            }
        }

        #endregion

        #region Threaded Conversation Mail
        public EmailMessageList RetrieveMailConversationMessages(int memberID, int targetMemberID, Int32 groupID, ConversationType conversationType)
        {
            try
            {
                if (conversationType == ConversationType.Inbox)
                {
                    return EmailMessageDB.RetrieveMailConversationInboxMessages(memberID, targetMemberID, groupID, conversationType, true);
                }
                else if (conversationType == ConversationType.Sent)
                {
                    return EmailMessageDB.RetrieveMailConversationSentMessages(memberID, targetMemberID, groupID, conversationType, true);
                }
                else if (conversationType == ConversationType.Draft)
                {
                    return EmailMessageDB.RetrieveMailConversationDraftMessages(memberID, targetMemberID, groupID, conversationType, true);
                }
                else if (conversationType == ConversationType.Trash)
                {
                    return EmailMessageDB.RetrieveMailConversationTrashMessages(memberID, targetMemberID, groupID, conversationType, true);
                }

                return null;
            }
            catch (Exception Ex)
            {
                throw (new BLException(string.Format("Error retrieving Mail Conversation messages. memberID: {0}, targetMemberID: {1}, groupID: {2}, conversationType: {3}", memberID, targetMemberID, groupID, conversationType), Ex));
            }
        }

        public MessageMemberCollection RetrieveMailConversationMembers(Int32 memberID, Int32 groupID, ConversationType conversationType)
        {
            try
            {
                if (conversationType == ConversationType.Inbox)
                {
                    return EmailMessageDB.RetrieveMailConversationInboxMembers(memberID, groupID, conversationType);
                }
                else if (conversationType == ConversationType.Sent)
                {
                    return EmailMessageDB.RetrieveMailConversationSentMembers(memberID, groupID, conversationType);
                }
                else if (conversationType == ConversationType.Draft)
                {
                    return EmailMessageDB.RetrieveMailConversationDraftMembers(memberID, groupID, conversationType);
                }
                else if (conversationType == ConversationType.Trash)
                {
                    return EmailMessageDB.RetrieveMailConversationTrashMembers(memberID, groupID, conversationType);
                }

                return null;
            }
            catch (Exception Ex)
            {
                throw (new BLException(string.Format("Error retrieving Mail Conversation members. memberID: {0}, groupID: {1}, conversationType: {2}", memberID, groupID, conversationType), Ex));
            }
        }


        #endregion


        #region Message Attachments
        /// <summary>
        /// Saves a new attachment for a message; messageID may be null if no messageID is known yet
        /// </summary>
        public MessageAttachmentSaveResult SaveMessageAttachment(int fromMemberID, int toMemberID, int fileID, string previewCloudPath, string originalCloudPath, AttachmentType attachmentType, int messageID, int communityID, int siteID, int appID)
        {
            MessageAttachmentSaveResult result = null;
            try
            {
                result = new MessageAttachmentSaveResult();

                //save attachment record
                int messageAttachmentID = KeySA.Instance.GetKey("MessageAttachmentID");
                if (messageID > 0)
                {
                    //save message attachment record
                    EmailMessageDB.SaveMessageAttachment(messageAttachmentID, messageID, attachmentType, fileID, previewCloudPath, originalCloudPath, appID, AttachmentStatus.SavedNoApprovalRequired, fromMemberID);
                    if (!EmailMessageDB.SharePartition(toMemberID, fromMemberID))
                    {
                        EmailMessageDB.SaveMessageAttachment(messageAttachmentID, messageID, attachmentType,
                                                             fileID, previewCloudPath,
                                                             originalCloudPath, appID,
                                                             AttachmentStatus.SavedNoApprovalRequired, toMemberID);
                    }
                }
                else
                {
                    //save message attachment interim record
                    EmailMessageDB.SaveMessageAttachmentInterim(messageAttachmentID, attachmentType, fileID, previewCloudPath, originalCloudPath, fromMemberID, toMemberID, communityID, siteID, appID, AttachmentStatus.SavedNoApprovalRequired, fromMemberID);
                    if (!EmailMessageDB.SharePartition(toMemberID, fromMemberID))
                    {
                        EmailMessageDB.SaveMessageAttachmentInterim(messageAttachmentID, attachmentType,
                                                                    fileID,
                                                                    previewCloudPath,
                                                                    originalCloudPath,
                                                                    fromMemberID, toMemberID, communityID, siteID, appID,
                                                                    AttachmentStatus.SavedNoApprovalRequired,
                                                                    toMemberID);
                    }

                }

                result.FileID = fileID;
                result.MessageID = messageID;
                result.MessageAttachmentID = messageAttachmentID;
                result.OriginalCloudPath = originalCloudPath;
                result.PreviewCloudPath = previewCloudPath;
                result.Status = MessageAttachmentSaveStatus.Success;

                //activity recording (via RabbitMQ path) for reporting
                try
                {
                    string fileExtension = "";
                    if (!string.IsNullOrEmpty(originalCloudPath) && originalCloudPath.IndexOf(".") > 0)
                    {
                        fileExtension = originalCloudPath.Substring(originalCloudPath.LastIndexOf(".") + 1);
                    }

                    StringBuilder messageAttachmentXML = new StringBuilder();
                    messageAttachmentXML.Append("<Attachment>");
                    messageAttachmentXML.Append("<AttachmentID>" + messageAttachmentID.ToString() + "</AttachmentID>");
                    //messageAttachmentXML.Append("<AttachmentSize>" + "</AttachmentSize>");
                    messageAttachmentXML.Append("<AttachmentType>" + ((int)attachmentType).ToString() + "</AttachmentType>");
                    messageAttachmentXML.Append("<AttachmentExt>" + fileExtension + "</AttachmentExt>");
                    messageAttachmentXML.Append("<ApplicationID>" + appID.ToString() + "</ApplicationID>");
                    messageAttachmentXML.Append("<Timestamp>" + DateTime.Now.ToString() + "</Timestamp>");
                    messageAttachmentXML.Append("</Attachment>");

                    if (_TraceActivity)
                    {
                        Trace.WriteLine(
                            String.Format(
                                "EmailMessageBL SaveMessageAttachment activity recording. FromMember {0}, ToMember {1}, ActionType {2}, SiteID {3}, XML {4} ",
                                fromMemberID,
                                toMemberID,
                                ActionType.MessageAttachmentUpload,
                                siteID,
                                messageAttachmentXML.ToString()));
                    }
                    ActivityRecordingProcessorAdapter.Instance.RecordActivity(fromMemberID, toMemberID, (int) ActionType.MessageAttachmentUpload, siteID, messageAttachmentXML.ToString(), CallingSystem.Email);
                }
                catch (Exception exActivity)
                {
                    //likely rabbitMQ is not set up on this environment
                    //RollingFileLogger.Instance.LogInfoMessage(Matchnet.Email.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "EmailMessageBL", "SaveMessageAttachment activity recording exception: " + exActivity.Message, null);
                    StringBuilder errorMsg = new StringBuilder();
                    errorMsg.Append(exActivity.Message);
                    if (exActivity.InnerException != null)
                    {
                        errorMsg.Append(exActivity.InnerException.Message);
                    }
                    errorMsg.AppendLine(exActivity.StackTrace);
                    Trace.WriteLine("EmailMessageBL SaveMessageAttachment activity recording exception: " + errorMsg.ToString());
                    new ServiceBoundaryException(Matchnet.Email.ValueObjects.ServiceConstants.SERVICE_NAME,
                                             "SaveMessageAttachment activity recording Error: " + exActivity.Message, exActivity);
                }
            }
            catch(Exception ex)
			{
				throw(new BLException("Email Message BL error occured when saving new message attachment"
					+ "  ToMemberID: " + toMemberID
                    + ", FromMemberID: " + fromMemberID
                    + ", SiteID: " + siteID
                    + ", FileID: " + fileID,
					ex));
			}
            return result;
        }

        /// <summary>
        /// Saves an existing interim attachment to a message
        /// </summary>
        public MessageAttachmentSaveResult SaveMessageAttachmentFromInterim(int fromMemberID, int toMemberID, int messageID,
                                                                 int messageAttachmentID)
        {
            MessageAttachmentSaveResult result = null;
            try
            {
                result = new MessageAttachmentSaveResult();

                //save message attachment record
                EmailMessageDB.SaveMessageAttachmentFromInterim(messageID, messageAttachmentID, fromMemberID);
                if (!EmailMessageDB.SharePartition(toMemberID, fromMemberID))
                {
                    EmailMessageDB.SaveMessageAttachmentFromInterim(messageID, messageAttachmentID, toMemberID);
                }

                result.MessageAttachmentID = messageAttachmentID;
                result.MessageID = messageID;
                result.Status = MessageAttachmentSaveStatus.Success;
            }
            catch (Exception ex)
            {
                throw (new BLException("Email Message BL error occured when saving message attachment"
                    + "  MessageAttachmentID: " + messageAttachmentID
                    + ", ToMemberID: " + toMemberID
                    + ", FromMemberID: " + fromMemberID,
                    ex));
            }
            return result;
        }

        /// <summary>
        /// Get message attachment metadata embedded within the message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public List<MessageAttachmentMetadata> GetEmbeddedMessageAttachmentData(string message)
        {
            List<MessageAttachmentMetadata> attachmentMetadatas = new List<MessageAttachmentMetadata>();

            try
            {
                //check for sparktag and then for the various attachment "types" first so we don't go through xml process for performance
                if (!string.IsNullOrEmpty(message) && message.IndexOf("sparktag") > 0 && message.IndexOf("photo") > 0)
                {
                    //determine if it's encoded
                    if (message.IndexOf("&lt;sparktag") >= 0)
                    {
                        message = HttpUtility.HtmlDecode(message);
                    }

                    //ensure sparktag element exists
                    if (message.IndexOf("<sparktag") >= 0)
                    {
                        //add temp root element around entire message so we can load it as xml
                        message = "<root>" + message + "</root>";

                        XPathNavigator navigator = null;
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(message);
                        navigator = doc.CreateNavigator();

                        //get photo attachment IDs
                        XPathNodeIterator iterator = navigator.Select("//sparktag[@type='photo']");
                        if (iterator.Count > 0)
                        {
                            while (iterator.MoveNext())
                            {
                                int photoAttachmentID = 0;
                                int.TryParse(iterator.Current.GetAttribute("id", ""), out photoAttachmentID);
                                if (photoAttachmentID > 0)
                                {
                                    MessageAttachmentMetadata attachmentMeta = new MessageAttachmentMetadata();
                                    attachmentMeta.AttachmentID = photoAttachmentID;
                                    attachmentMeta.AttachmentTypeID = (int)AttachmentType.IMPhoto;
                                    attachmentMeta.FileExtension = iterator.Current.GetAttribute("data-originalExt", "");
                                    attachmentMetadatas.Add(attachmentMeta);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("Email Message BL error occured parsing message for attachmentIDs"
                    + "  Message: " + message,
                    ex));
            }

            return attachmentMetadatas;
        }

        #endregion

		#region Private Methods

		private void expireMessagesCallback(string key, object value, System.Web.Caching.CacheItemRemovedReason callbackreason) 
		{
			switch (value.GetType().Name)
			{
				case "MessageListCollection":
					MessageListCollection messages = value as MessageListCollection;
					SynchronizationRequested(messages.GetCacheKey(), messages.ReferenceTracker.Purge(Constants.NULL_STRING));
					MessageListRemoved();
					break;
				case "Message":
					Message message = value as Message;
					SynchronizationRequested(message.GetCacheKey(), message.ReferenceTracker.Purge(Constants.NULL_STRING));
					MessageRemoved();
					break;
			}
		}

		private void synchronize(Int32 memberID, Int32 groupID, string clientHostName)
		{
			MessageListCollection messageListCollection = retrieveCachedMessageList(memberID, groupID);
			if (messageListCollection != null)
			{
				SynchronizationRequested(messageListCollection.GetCacheKey(), messageListCollection.ReferenceTracker.Purge(clientHostName));
			}
		}

		private void synchronize(Int32 messageID, string clientHostName)
		{
			Message message = retrieveCachedMessage(messageID, false);
			if (message != null)
			{
				SynchronizationRequested(message.GetCacheKey(), message.ReferenceTracker.Purge(clientHostName));
			}
		}

        private bool isVIPFolder(Int32 pMemberID, Int32 pGroupId, Int32 pMessageListId)
        {
            MessageListCollection messageListCollection = retrieveCachedMessageList(pMemberID, pGroupId);

            if (messageListCollection != null)
            {
                MessageList messageList = messageListCollection[pMessageListId];
                if (messageList != null)
                {
                    return (messageList.MemberFolderID == (int) SystemFolders.VIPSent ||
                            messageList.MemberFolderID == (int) SystemFolders.VIPInbox ||
                            messageList.MemberFolderID == (int) SystemFolders.VIPDraft ||
                            messageList.MemberFolderID == (int) SystemFolders.VIPTrash);
                }
            }

            return false;
        }

	    private void setRepliedBit(Int32 memberID, Int32 groupID, Int32 messageListID, string clientHostName, bool markOffFreeReply)
		{
			try
			{
				MessageListCollection messageListCollection = retrieveCachedMessageList(memberID, groupID);

				if (messageListCollection != null)
				{
					MessageList messageList = messageListCollection[messageListID];
                    bool updateMessageList = false;
					if (messageList != null && (messageList.StatusMask & MessageStatus.Replied) != MessageStatus.Replied)
					{
						messageList.StatusMask = (MessageStatus) messageList.StatusMask | MessageStatus.Replied;
                        updateMessageList = true;

					}

                    if (messageList != null && markOffFreeReply)
                    {
                        messageList.StatusMask = (MessageStatus)messageList.StatusMask & ~MessageStatus.OneFreeReply;
                        updateMessageList = true;
                    }

                    if (updateMessageList)
                    {
                        EmailMessageDB.MessageListSave(memberID, messageList, MessageListSaveType.Update);
                        ReplicationActionRequested(new MessageListSaveAction(memberID, MessageListSaveType.Update, messageList));
                        synchronize(memberID, groupID, clientHostName);
                    }
				}
			}
			catch(Exception ex)
			{
				throw(new Exception("Error occured when setting message reply.", ex));
			}
		}


		#endregion
        
    }
}
