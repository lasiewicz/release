using System;
using System.Data;

using Matchnet.Data;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

namespace Matchnet.Email.BusinessLogic
{
	/// <summary>
	/// Summary description for EmailLogBL.
	/// </summary>
	public class EmailLogBL : IEmailLogService
	{
		private const string LOGICALDB_MAILLOG = "mnMailLog";

		public static readonly EmailLogBL Instance = new EmailLogBL();

		private EmailLogBL()
		{
		}

        public DateTime? RetrieveEmailLastSentDate(Int32 memberID, Int32 groupID)
        {
            try
            {
                DateTime? lastSentDate = null;
                
                Command command = new Command(LOGICALDB_MAILLOG, "up_MailLog_List_LastSentDate", 0);
				command.AddParameter("@MemberID",SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count == 1 && dt.Rows[0]["InsertDate"] != DBNull.Value)
                {
                    lastSentDate = Convert.ToDateTime(dt.Rows[0]["InsertDate"]);
                }

                return lastSentDate;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving last email sent date.", ex));
            }
        }

		public MemberMailLog RetrieveEmailLog(Int32 memberID, Int32 groupID) 
		{
			try
			{
				Command command = new Command(LOGICALDB_MAILLOG, "up_MailLog_List", 0);
				command.AddParameter("@MemberID",SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				MemberMailLog memberMailLog = new MemberMailLog(memberID, groupID);

				foreach (DataRow row in dt.Rows)
				{
					EmailLogEntry emailLogEntry = MakeEmailLogEntryFromDataRow(row);

					memberMailLog.Both.Add(emailLogEntry);

					if (emailLogEntry.ToMemberID == memberID)
					{
						memberMailLog.Received.Add(emailLogEntry);
					}
					else
					{
						memberMailLog.Sent.Add(emailLogEntry);
					}
				}

				return memberMailLog;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when retrieving messages.", ex));
			}
		}

		public void InsertMailLogEntry(int mailID, int groupID, int fromMemberID, int toMemberID, MessageStatus statusMask, MailType type, MailOption mailOption, string ThreadID)
		{
			Command command = new Command(LOGICALDB_MAILLOG, "up_MailLog_Insert", 0);
			command.AddParameter("@MailID", SqlDbType.Int, ParameterDirection.Input, mailID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
			command.AddParameter("@FromMemberID",SqlDbType.Int, ParameterDirection.Input, fromMemberID);
			command.AddParameter("@ToMemberID",SqlDbType.Int, ParameterDirection.Input, toMemberID);
			command.AddParameter("@StatusMask", SqlDbType.Int, ParameterDirection.Input, (Int32) statusMask);
			command.AddParameter("@MailTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32) type);
            command.AddParameter("@MailOption", SqlDbType.Int, ParameterDirection.Input, (Int32)mailOption);
            command.AddParameter("@ThreadID", SqlDbType.VarChar, ParameterDirection.Input, ThreadID);
			Client.Instance.ExecuteAsyncWrite(command);
		}

		private EmailLogEntry MakeEmailLogEntryFromDataRow(DataRow row)
		{
			DateTime insertDate = DateTime.MinValue;

			if (row["InsertDate"] != DBNull.Value)
			{
				insertDate = Convert.ToDateTime(row["InsertDate"]);
			}

			return new EmailLogEntry(Conversion.CInt(row["MailID"]),
				Conversion.CInt(row["GroupID"]),
				Conversion.CInt(row["FromMemberID"]),
				Conversion.CInt(row["ToMemberID"]),
				insertDate,
                Conversion.CInt(row["StatusMask"]),
				Conversion.CInt(row["MailTypeID"]),
                Conversion.CInt(row["MailOption"])
				);
		}
	}
}
