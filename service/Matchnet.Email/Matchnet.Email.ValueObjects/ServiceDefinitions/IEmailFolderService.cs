using System;

using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IEmailFolder.
	/// </summary>
	public interface IEmailFolderService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="statsFlag"></param>
		/// <returns></returns>
		EmailFolderCollection RetrieveFolders(string clientHostName, CacheReference cacheReference, int memberID, int communityID, bool statsFlag);

		// <summary>
		// 
		// </summary>
		// <param name="memberID"></param>
		// <param name="communityID"></param>
		// <param name="memberFolderID"></param>
		//void EmptyFolder(int memberID, int communityID, int memberFolderID) ;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="memberFolderID"></param>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		void Delete(string clientHostName, int memberFolderID, int memberID, int groupID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="description"></param>
		/// <returns></returns>
		FolderSaveResult Save(string clientHostName, int memberID, int communityID, string description);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="description"></param>
		/// <param name="memberFolderID"></param>
		/// <returns></returns>
		FolderSaveResult Save(string clientHostName, int memberID, int communityID, string description, int memberFolderID); 
	}
}
