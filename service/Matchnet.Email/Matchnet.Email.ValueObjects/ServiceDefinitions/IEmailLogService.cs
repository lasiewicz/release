using System;

namespace Matchnet.Email.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IEmailLogService.
	/// </summary>
	public interface IEmailLogService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		MemberMailLog RetrieveEmailLog(Int32 memberID, Int32 groupID);
        DateTime? RetrieveEmailLastSentDate(Int32 memberID, Int32 groupID);
	}
}
