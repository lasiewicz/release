using Matchnet;
using Matchnet.Email.ValueObjects;
using System;

namespace Matchnet.Email.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for ITeaseService.
	/// </summary>
	public interface ITeaseService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="teaseCategoryID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		TeaseCollection GetTeaseCollection(int teaseCategoryID, int groupID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		TeaseCategoryCollection GetTeaseCategoryCollection(int groupID);
	}
}
