using System;
using System.Collections;

using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IEmailMessageService.
	/// </summary>
	public interface IEmailMessageService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="messageSave"></param>
		/// <returns></returns>
		MessageSaveResult SaveMessage(string clientHostName, MessageSave messageSave);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="messageSend"></param>
		/// <param name="saveInSent"></param>
		/// <param name="replyMessageListID"></param>
		/// <param name="sender"></param>
		/// <param name="recipient"></param>
		/// <returns></returns>
		MessageSendResult SendMessage(string clientHostName, MessageSend messageSend, bool saveInSent, Int32 replyMessageListID, bool sender, bool recipient);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		MessageListCollection RetrieveMessageList(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID);

        /// <summary>
        /// This should be used only when you need to operate on stuff that's not in memory
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        MessageListCollection RetrieveMessageListFromDB(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID);


        /// <summary>
        /// This should be used only when you need to operate on stuff that's not in memory
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        MessageMemberCollection RetrieveMessageMemberFromDB(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID);


		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="messageID"></param>
		/// <param name="withContent"></param>
		/// <returns></returns>
		Message RetrieveMessage(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 messageID, bool withContent);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="messageIDs"></param>
		/// <param name="withContent"></param>
		/// <returns></returns>
		MessageCollection RetrieveMessages(string clientHostName, CacheReference cacheReference, Int32 memberID, ArrayList messageIDs, bool withContent);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="action"></param>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void PlayAction(string clientHostName, IReplicationAction action);

        /// <summary>
        /// Saves an attachment for a message; messageID may be null if no messageID is known yet
        /// </summary>
        MessageAttachmentSaveResult SaveMessageAttachment(int fromMemberID, int toMemberID, int fileID, string previewCloudPath, string originalCloudPath,
                                                          AttachmentType attachmentType, int messageID, int communityID,
                                                          int siteID, int appID);

        EmailMessageList RetrieveInstantMessages(int memberID, int targetMemberID, Int32 groupID);
        MessageMemberCollection RetrieveInstantMessageMembers(int memberID, int groupID, bool filterRemovedMembers);
        EmailMessageList RetrieveMailConversationMessages(int memberID, int targetMemberID, int groupID, ConversationType conversationType);
        MessageMemberCollection RetrieveMailConversationMembers(int memberID, int groupID, ConversationType conversationType);
	}
}
