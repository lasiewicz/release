using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class EmailFolderCollection : IValueObject, ICacheable, IReplicable, ISerializable, ICollection
	{
		#region Constants
		/// <summary> </summary>
		public const string CACHE_KEY_PREFIX = "IMailFolders";
		#endregion

		#region Private Members
		private Int32 _memberID;
		private Int32 _groupID;
		private bool _statsFlag;

		private Int32 _cacheTTLSeconds;
		private CacheItemMode _cacheMode;
		private CacheItemPriorityLevel _cachePriority;

		private Hashtable _memberFolderIDs = new Hashtable();

		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public EmailFolderCollection(Int32 memberID, Int32 groupID, bool statsFlag)
		{
			_memberID = memberID;
			_groupID = groupID;
			_statsFlag = statsFlag;
		}

		/// <summary>
		/// Constructor for recreating an EmailFolderCollection from serialized data.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EmailFolderCollection(SerializationInfo info, StreamingContext context)
		{
			_memberID = info.GetInt32("memberID");
			_groupID = info.GetInt32("groupID");
			_statsFlag = info.GetBoolean("statsFlag");
			_cacheTTLSeconds = info.GetInt32("cacheTTLSeconds");
			_cacheMode = (CacheItemMode) info.GetValue("cacheMode", typeof(CacheItemMode));
			_cachePriority = (CacheItemPriorityLevel) info.GetValue("cachePriority", typeof(CacheItemPriorityLevel));
			_memberFolderIDs = (Hashtable) info.GetValue("memberFolderIDs", typeof(Hashtable));
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailFolder"></param>
		/// <returns></returns>
		public void Add(EmailFolder emailFolder)
		{
			_memberFolderIDs[emailFolder.MemberFolderID] = emailFolder;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberFolderID"></param>
		public void Remove(Int32 memberFolderID)
		{
			_memberFolderIDs.Remove(memberFolderID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberFolderID"></param>
		/// <returns></returns>
		public EmailFolder FindByMemberFolderID(Int32 memberFolderID)
		{
			return _memberFolderIDs[memberFolderID] as EmailFolder;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="description"></param>
		/// <returns></returns>
		public bool ContainsDescription(string description)
		{
			foreach (EmailFolder emailFolder in this)
			{
				if (emailFolder.Description == description)
				{
					return true;
				}
			}

			return false;
		}

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool StatsFlag
		{
			get
			{
				return _statsFlag;
			}
		}
		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
			set
			{
				_cacheMode = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return EmailFolderCollection.GetCacheKey(_memberID, _groupID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 memberID, Int32 groupID)
		{
			return CACHE_KEY_PREFIX + ":" + memberID + ":" + groupID;
		}

		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion

		#region ISerializable Members
		/// <summary>
		/// Gets data for serialization.
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("memberID", _memberID);
			info.AddValue("groupID", _groupID);
			info.AddValue("statsFlag", _statsFlag);
			info.AddValue("cacheTTLSeconds", _cacheTTLSeconds);
			info.AddValue("cacheMode", _cacheMode);
			info.AddValue("cachePriority", _cachePriority);
			info.AddValue("memberFolderIDs", _memberFolderIDs);
		}

		#endregion

		#region ICollection Members

		/// <summary>
		/// 
		/// </summary>
		public bool IsSynchronized
		{
			get
			{
				return _memberFolderIDs.Values.IsSynchronized;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Count
		{
			get
			{
				return _memberFolderIDs.Values.Count;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Array array, int index)
		{
			_memberFolderIDs.Values.CopyTo(array, index);
		}

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get
			{
				return _memberFolderIDs.Values.SyncRoot;
			}
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			ArrayList folders = new ArrayList(_memberFolderIDs.Values.Count);

			foreach(EmailFolder folder in _memberFolderIDs.Values)
			{
				folders.Add(folder);
			}

			folders.Sort();
			return folders.GetEnumerator();
		}

		#endregion
	}
}
