﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Email.ValueObjects
{
    [Serializable]
    public class LastConversation : IValueObject, ICacheable
    {
        private Int32 _fromMemberID;
        private Int32 _toMemberID;
        private Int32 _groupID;
        private MailType _mailType;
        private string _threadID = "";
        private DateTime _insertDate;

        private const string CACHE_KEY_PREFIX = "LastConversation";
        private Int32 _cacheTTLSeconds = 1800; //30 mins;
        private CacheItemMode _cacheMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Int32 FromMemberID
        {
            get { return _fromMemberID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 ToMemberID
        {
            get { return _toMemberID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 GroupID
        {
            get { return _groupID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MailType MailType
        {
            get
            {
                return _mailType;
            }
            set
            {
                _mailType = value;
            }
        }

        public string ThreadID
        {
            get { return _threadID; }
            set { _threadID = value; }
        }

        public DateTime InsertDate
        {
            get { return _insertDate; }
            set { _insertDate = value; }
        }

        #endregion

        #region Constructor
        public LastConversation(MessageSave messageSave)
        {
            _fromMemberID = messageSave.FromMemberID;
            _toMemberID = messageSave.ToMemberID;
            _groupID = messageSave.GroupID;
            _mailType = messageSave.MailType;
            _threadID = messageSave.ThreadID;
            _insertDate = (messageSave.InsertDate > DateTime.MinValue) ? messageSave.InsertDate : DateTime.Now;
        }

        public LastConversation(EmailMessage emailMessage)
        {
            _fromMemberID = emailMessage.FromMemberID;
            _toMemberID = emailMessage.ToMemberID;
            _groupID = emailMessage.GroupID;
            _mailType = emailMessage.MailTypeID;
            _threadID = emailMessage.ThreadID;
            _insertDate = emailMessage.InsertDate;
        }

        #endregion

        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return GetCacheKey(_fromMemberID, _toMemberID, _groupID, _mailType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static string GetCacheKey(Int32 fromMemberID, Int32 toMemberID, Int32 groupID, MailType mailType)
        {
            int memberID1 = fromMemberID;
            int memberID2 = toMemberID;
            if (fromMemberID > toMemberID)
            {
                memberID1 = toMemberID;
                memberID2 = fromMemberID;
            }
            return CACHE_KEY_PREFIX + string.Format(":{0}:{1}:{2}:{3}", memberID1, memberID2, groupID, ((int)mailType).ToString());
        }

        #endregion

    }
}
