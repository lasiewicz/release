using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// General collection of email messages for a member
	/// </summary>
	[Serializable]
	public class EmailMessageCollection :  IEnumerable, IValueObject, ICollection
	{
		#region Private Variables
		private Hashtable _messages;
		private Int32 _memberID;
		private Int32 _groupID;
		private Int32 _memberFolderID;

		[NonSerialized]
		private EmailFolder _emailFolder;

		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public EmailMessageCollection(Int32 memberID, Int32 groupID, Int32 memberFolderID)
		{
			_messages = new Hashtable();
			_memberID = memberID;
			_groupID = groupID;
			_memberFolderID = memberFolderID;
		}

		#endregion

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailMessage"></param>
		/// <returns></returns>
		public void Add(EmailMessage emailMessage)
		{
			lock (_messages.SyncRoot)
			{
				if (!_messages.Contains(emailMessage.MemberMailID))
				{
					_messages.Add(emailMessage.MemberMailID, emailMessage);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberMailID"></param>
		/// <returns></returns>
		public EmailMessage this[Int32 memberMailID]
		{
			get
			{
				EmailMessage message;
				lock (_messages.SyncRoot)
				{
					message = _messages[memberMailID] as EmailMessage;
				}
				return message;
			}
			set
			{
				EmailMessage message = value;

				lock (_messages.SyncRoot)
				{
					_messages[memberMailID] = message;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberMailID"></param>
		public void Remove(Int32 memberMailID)
		{
			lock (_messages.SyncRoot)
			{
				_messages.Remove(memberMailID);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="comparer"></param>
		/// <returns></returns>
		public ArrayList Sort(EmailComparer comparer)
		{
			ICollection collection;
			ArrayList result;

			lock (_messages.SyncRoot)
			{
				collection = _messages.Values;

				result = new ArrayList(collection.Count);

				foreach(EmailMessage message in collection)
				{
					result.Add(message);
				}
			}

			result.Sort(comparer);
			return result;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public bool HasNewMail()
		{
			lock (_messages.SyncRoot)
			{
				foreach (EmailMessage message in _messages.Values)
				{
					if ((message.StatusMask & MessageStatus.New) == MessageStatus.New)
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int GetDisplayKBytes()
		{
			int displayKBytes = 0;
			lock (_messages.SyncRoot)
			{
				foreach (EmailMessage message in _messages.Values)
				{
					displayKBytes += message.DisplayKBytes;
				}
			}

			return displayKBytes;
		}

		/// <summary>
		/// Determines whether usernames have been populated for the EmailMessage objects in this collection.
		/// </summary>
		/// <returns></returns>
		public bool HasUsernames()
		{
			lock (_messages.SyncRoot)
			{
				foreach (EmailMessage message in _messages.Values)
				{
					if (message.FromUserName == null)
					{
						return false;
					}
				}
			}

			return true;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get{return _memberID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 GroupID
		{
			get{return _groupID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberFolderID
		{
			get{return _memberFolderID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public EmailFolder EmailFolder
		{
			get
			{
				return _emailFolder;
			}
			set
			{
				_emailFolder = value;
			}
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _messages.Values.GetEnumerator();
		}

		#endregion

		#region ICollection Members

		/// <summary>
		/// 
		/// </summary>
		public bool IsSynchronized
		{
			get
			{
				return _messages.Values.IsSynchronized;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Count
		{
			get
			{
				return _messages.Values.Count;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int UnreadCount
		{
			get
			{
				Int32 count = 0;

				foreach (EmailMessage message in _messages.Values)
				{
					if ((message.StatusMask & MessageStatus.Read) != MessageStatus.Read)
					{
						count++;
					}
				}

				return count;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Array array, int index)
		{
			_messages.Values.CopyTo(array, index);
		}

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get
			{
				return _messages.Values.SyncRoot;
			}
		}

		#endregion
	}
}
