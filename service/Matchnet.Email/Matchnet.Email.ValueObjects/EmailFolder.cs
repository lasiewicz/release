using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class EmailFolder : IValueObject, ISerializable, IComparable
	{
		#region Private Members
		private Int32 _memberFolderID = Constants.NULL_INT;
		private Int32 _memberID = Constants.NULL_INT;
		private Int32 _communityID = Constants.NULL_INT;
		private string _description = Constants.NULL_STRING;
		private bool _systemFolder = false;
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public int MemberFolderID
		{
			get{return _memberFolderID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get{return _memberID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int CommunityID
		{
			get{return _communityID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get{return _description;}
			set{_description = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool SystemFolder
		{
			get{return _systemFolder;}
		}

		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberFolderID"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="description"></param>
		/// <param name="systemFolder"></param>
		public EmailFolder(int memberFolderID, int memberID, int communityID, string description, bool systemFolder)
		{
			_memberFolderID = memberFolderID;
			_memberID = memberID;
			_communityID = communityID;
			_description = description;
			_systemFolder = systemFolder;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberFolderID"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="description"></param>
		public EmailFolder(int memberFolderID, int memberID, int communityID, string description)
		{
			_memberFolderID = memberFolderID;
			_memberID = memberID;
			_communityID = communityID;
			_description = description;
			_systemFolder = false;
		}

		/// <summary>
		/// 
		/// </summary>
		public EmailFolder(SerializationInfo info, StreamingContext context)
		{
			_memberFolderID = info.GetInt32("memberFolderID");
			_memberID = info.GetInt32("memberID");
			_communityID = info.GetInt32("communityID");
			_description = info.GetString("description");
			_systemFolder = info.GetBoolean("systemFolder");
		}
		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("memberFolderID", _memberFolderID);
			info.AddValue("memberID", _memberID);
			info.AddValue("communityID", _communityID);
			info.AddValue("description", _description);
			info.AddValue("systemFolder", _systemFolder);
		}

		#endregion

		#region IComparable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			EmailFolder folder = obj as EmailFolder;
			return _memberFolderID.CompareTo(folder.MemberFolderID);
		}

		#endregion
	}
}
