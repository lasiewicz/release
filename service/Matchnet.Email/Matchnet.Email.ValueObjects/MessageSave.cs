using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Matchnet.Email.ValueObjects.ReplicationActions;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class MessageSave : IValueObject
	{
		#region Private Members
		private Int32 _messageListID = Constants.NULL_INT;
		private Int32 _fromMemberID = Constants.NULL_INT;
		private Int32 _toMemberID = Constants.NULL_INT;
		private Int32 _groupID = Constants.NULL_INT;
        private Int32 _siteID = Constants.NULL_INT;
        private MailType _mailType = MailType.Email;
		private string _messageHeader = Constants.NULL_STRING;
		private string _messageBody = Constants.NULL_STRING;
		private DateTime _insertDate = DateTime.Now;
	    private MailOption _mailOption = MailOption.None;
	    private string _threadID = Constants.NULL_STRING;

		#endregion

		#region Constructors

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromMemberID"></param>
        /// <param name="toMemberID"></param>
        /// <param name="groupID"></param>
        /// <param name="mailType"></param>
        /// <param name="messageHeader"></param>
        /// <param name="messageBody"></param>
        public MessageSave(Int32 fromMemberID, Int32 toMemberID, Int32 groupID, MailType mailType, string messageHeader, string messageBody)
        {
            // previously allowed empty subject that would break certain use cases such as opening the message
            if (string.IsNullOrEmpty(messageHeader))
            {
                messageHeader = Enum.GetName(typeof (MailType), mailType);
            }

            if (messageBody == null)
            {
                throw new ArgumentNullException("messageBody");
            }

            _fromMemberID = fromMemberID;
            _toMemberID = toMemberID;
            _groupID = groupID;
            _mailType = mailType;
            _messageHeader = messageHeader;
            _messageBody = messageBody;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fromMemberID"></param>
		/// <param name="toMemberID"></param>
		/// <param name="groupID"></param>
		/// <param name="mailType"></param>
		/// <param name="messageHeader"></param>
		/// <param name="messageBody"></param>
		public MessageSave(Int32 fromMemberID, Int32 toMemberID, Int32 groupID, Int32 siteID, MailType mailType, string messageHeader, string messageBody)
		{
            if (string.IsNullOrEmpty(messageHeader))
            {
                messageHeader = Enum.GetName(typeof(MailType), mailType);
            }

			if (messageBody == null)
			{
				throw new ArgumentNullException("messageBody");
			}

			_fromMemberID = fromMemberID;
			_toMemberID = toMemberID;
			_groupID = groupID;
            _siteID = siteID;
            _mailType = mailType;
			_messageHeader = messageHeader;
			_messageBody = messageBody;
		}

        /// <summary>
        /// Added threadID and insert date support in constructor
        /// </summary>
        public MessageSave(Int32 fromMemberID, Int32 toMemberID, Int32 groupID, Int32 siteID, MailType mailType, string messageHeader, string messageBody, DateTime insertDate, string threadID)
        {
            if (string.IsNullOrEmpty(messageHeader))
            {
                messageHeader = Enum.GetName(typeof(MailType), mailType);
            }

            if (messageBody == null)
            {
                throw new ArgumentNullException("messageBody");
            }

            _fromMemberID = fromMemberID;
            _toMemberID = toMemberID;
            _groupID = groupID;
            _siteID = siteID;
            _mailType = mailType;
            _messageHeader = messageHeader;
            _messageBody = messageBody;
            _insertDate = insertDate;
            _threadID = threadID;
        }

		#endregion

		#region Properties
        /// <summary>
        /// 
        /// </summary>
	    public MailOption MailOption
	    {
            get { return _mailOption; }
            set { _mailOption = value;  }
	    }

		/// <summary>
		/// 
		/// </summary>
		public Int32 MessageListID
		{
			get{return _messageListID;}
			set{_messageListID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 FromMemberID
		{
			get{return _fromMemberID;}
			set{_fromMemberID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 ToMemberID 
		{
			get{return _toMemberID;}
			set{_toMemberID = value;}
		}

		/// <summary>
		/// Represents the GroupID for this message.
		/// </summary>
		public Int32 GroupID
		{
			get{return _groupID;}
			set{_groupID = value;}
		}

        /// <summary>
        /// Represents the SiteID for this message.
        /// </summary>
        public Int32 SiteID
        {
            get{return _siteID;}
            set{_siteID = value;}
        }

		/// <summary>
		/// 
		/// </summary>
		public MailType MailType
		{
			get{return _mailType;}
			set{_mailType = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string MessageHeader
		{
			get{return _messageHeader;}
			set{_messageHeader = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string MessageBody
		{
			get{return _messageBody;}
			set{_messageBody = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get{return _insertDate;}
			set{_insertDate = value;}
		}

        /// <summary>
        /// ThreadID is used to form a relationship between messages
        /// For IM, we'll use it to group messages into a single conversation
        /// </summary>
	    public string ThreadID
	    {
            get { return _threadID; }
            set { _threadID = value; }
	    }

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageID"></param>
		/// <param name="cacheTTL"></param>
		/// <returns></returns>
		public Message GetMessage(Int32 messageID, Int32 cacheTTL)
		{
			Message message = new Message(messageID,
				_messageHeader,
				_messageBody,
				_insertDate,
				DateTime.MinValue);

			message.CacheTTLSeconds = cacheTTL;
			return message;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="messageID"></param>
		/// <param name="saveType"></param>
		/// <param name="cacheTTL"></param>
		/// <returns></returns>
		public MessageSaveAction GetMessageSaveAction(bool sender, Int32 messageID, MessageSaveType saveType, Int32 cacheTTL)
		{
			Int32 memberID = sender ? _fromMemberID : _toMemberID;

			return new MessageSaveAction(memberID, saveType, GetMessage(messageID, cacheTTL));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="messageID"></param>
		/// <param name="messageListID"></param>
		/// <param name="memberFolderID"></param>
		/// <returns></returns>
		public MessageList GetMessageList(bool sender, Int32 messageID, Int32 messageListID, Int32 memberFolderID)
		{
			Int32 memberID;
			Int32 targetMemberID;
			bool directionFlag;

			if (sender)
			{
				memberID = _fromMemberID;
				targetMemberID = _toMemberID;
				directionFlag = false;
			}
			else
			{
				memberID = _toMemberID;
				targetMemberID = _fromMemberID;
				directionFlag = true;
			}

			return new MessageList(messageListID,
				memberID,
				targetMemberID,
				directionFlag,
				_groupID,
                memberFolderID,
				messageID,
				MessageStatus.New | MessageStatus.Show,
				_mailType,
				_insertDate,
				_messageBody.Length,
                _threadID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="messageID"></param>
		/// <param name="messageListID"></param>
		/// <param name="memberFolderID"></param>
		/// <param name="saveType"></param>
		/// <returns></returns>
		public MessageListSaveAction GetMessageListSaveAction(bool sender, Int32 messageID, Int32 messageListID, Int32 memberFolderID, MessageListSaveType saveType)
		{
			Int32 memberID = sender ? _fromMemberID : _toMemberID;

			return new MessageListSaveAction(memberID,
				saveType,
				GetMessageList(sender, messageID, messageListID, memberFolderID));
		}

	}
}
