using System;
using System.Runtime.Serialization;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for SendMesageResult.
	/// </summary>
	[Serializable()]
	public class MessageResult
	{
		private EmailMessageStatus _status;
		private EmailMessageSendError _messageSendError;
		private Int32 _mailID = Constants.NULL_INT;
		private Int32 _senderMemberMailID = Constants.NULL_INT;
		private Int32 _recipientMemberMailID = Constants.NULL_INT;

		#region Constructors
		/// <summary>
		/// Overloaded successful SendMessageResult with given mailID
		/// </summary>
		/// <param name="mailID"></param>
		public MessageResult(int mailID) 
		{
			_status = EmailMessageStatus.Success;
			_mailID = mailID;
		}

		/// <summary>
		/// Default constructor for SendMessageResult, initializes a successful message result.
		/// </summary>
		public MessageResult()
		{
			_status = EmailMessageStatus.Success;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pError"></param>
		public MessageResult(EmailMessageSendError pError)
		{
			_status = EmailMessageStatus.Failed;
			_messageSendError = EmailMessageSendError.ComposeLimitReached;
		}
		#endregion

		#region Properties

		/// <summary>
		/// MailID of sent message
		/// </summary>
		public Int32 MailID 
		{
			get 
			{
				return _mailID;
			}
			set
			{
				_mailID = value;
			}
		}

		/// <summary>
		/// MemberMailID of sender's (or saver's) copy of message
		/// </summary>
		public Int32 SenderMemberMailID
		{
			get
			{
				return _senderMemberMailID;
			}
			set
			{
				_senderMemberMailID = value;
			}
		}

		/// <summary>
		/// MemberMailID of recipient's copy of message
		/// </summary>
		public Int32 RecipientMemberMailID
		{
			get
			{
				return _recipientMemberMailID;
			}
			set
			{
				_recipientMemberMailID = value;
			}
		}

		/// <summary>
		/// Indicates the status (success/fail) of this validation result.
		/// </summary>
		public EmailMessageStatus Status
		{
			get
			{
				return _status;
			}
		}

		/// <summary>
		/// Indicates the validation error for this validation result.
		/// </summary>
		public EmailMessageSendError Error
		{
			get
			{
				return _messageSendError;
			}
		}
		#endregion
	}

	#region Enumerations
	/// <summary>
	/// Status of email message send result (Success / Fail).
	/// </summary>
	public enum EmailMessageStatus
	{
		/// <summary>
		/// Indicates successful sending of email message.
		/// </summary>
		Success,
		/// <summary>
		/// Indicates failure sending email message.
		/// </summary>
		Failed
	}

	/// <summary>
	/// Message Send Error type for failed email attempts.
	/// </summary>
	public enum EmailMessageSendError
	{
		/// <summary>
		/// Indicates that the sender has reached the compose limit for a specified time period.
		/// (i.e. Members may be limited to sending 30 emails in any 24-hour period.)
		/// </summary>
		ComposeLimitReached,
		/// <summary>
		/// 
		/// </summary>
		Unspecified
	}
	#endregion
}