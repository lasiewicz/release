using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeasesKey.
	/// </summary>
	public class TeaseKey
	{
		private const string CACHE_KEY_PREFIX = "~TEASE^{0}{1}";

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int categoryID, int groupID)
		{
			return string.Format(CACHE_KEY_PREFIX, categoryID, groupID);
		}

		/// <summary>
		/// 
		/// </summary>
		public TeaseKey()
		{}
	}
}
