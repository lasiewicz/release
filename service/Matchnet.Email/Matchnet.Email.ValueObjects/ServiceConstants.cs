using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for Constants.
	/// </summary>
	public class ServiceConstants
	{
		/// <summary> </summary>
		public const string SERVICE_CONSTANT = "EMAIL_SVC";
		/// <summary> </summary>
		public const string SERVICE_NAME = "Matchnet.Email.Service";
	}
}
