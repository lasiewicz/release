using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for EmailClassification.
	/// </summary>
	public class EmailClassification
	{
		/// <summary>
		/// 
		/// </summary>
		public EmailClassification()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// Defines all external email types.  Every type of external message sent by Spark Networks as part of the application or tracked in the database is listed here.
		/// QUESTION: Where are PhotoReject emails?
		/// </summary>
		public enum EmailType 
		{
			/// <summary>
			/// Corresponds to the 'Monthly Newsletters, Dating Tips and Advice' OptIn email from Spark Networks.
			/// </summary>
			MatchNewsLetter = 1,
			/// <summary>
			/// The "You Said Yes to Each Other" YNM match notification.  
			/// </summary>
			MutualMail = 2,
			/// <summary>
			/// The "Are These People You Click With?" YNM email.
			/// </summary>
			ViralNewsLetter = 3,
			/// <summary>
			/// The notification sent to a Member's external email account when they receive a new message in their internal Spark Networks email.
			/// </summary>
			NewMailAlert = 4,
			/// <summary>
			/// PRELIMINARY: The 5-day free trial offer sent to a Member after they have been a Member for 30 days but never Subscribed.
			/// </summary>
			FreeTrial30 = 5,
			/// <summary>
			/// PRELIMINARY: The 5-day free trial offer sent to a Member after they have been a Member for 60 days but never Subscribed.
			/// </summary>
			FreeTrial60 = 6,
			/// <summary>
			/// PRELIMINARY: The 5-day free trial offer sent to a Member after they have been a Member for 90 days but never Subscribed.
			/// </summary>
			FreeTrial90 = 7,
			/// <summary>
			/// Welcome email for Members who become subscribers.
			/// </summary>
			PremiumSubscriberWelcome = 8,
			/// <summary>
			/// PRELIMINARY: A confirmation message sent to new Subscribers once their check payment has cleared and they are granted Subscriber access to
			/// the Site.
			/// </summary>
			CheckConfirmation = 9,
			/// <summary>
			/// The verification email sent to a Member's offsite email after successful registration. The Member is prompted to return to
			/// the Spark Networks Site and enter the verification code provided in the letter, thereby confirming that he/she is the true owner
			/// of the offsite email account provided at registration. 
			/// </summary>
			ActivationLetter = 10,
			/// <summary>
			/// The password reminder email initiated by the Member through "Forgot Password" page on each site.
			/// </summary>
			ForgotPassword = 11,
			/// <summary>
			/// Facelink only.  The email that is sent to album owners when another Member requests access to their private album.
			/// </summary>
			PhotoAlbumRequest = 12,
			/// <summary>
			/// Facelink only.  The email that is sent to a requesting Member when a private album approves a request for access to the album.
			/// </summary>
			PhotoAlbumApprove = 13,
			/// <summary>
			/// The "Send to Friend" functionality available when viewing any full profile of from the "Send to Friend" link within a FaceLink photo album.
			/// </summary>
			SendToFriend = 14,
			/// <summary>
			/// Corresponds to the "Email Offers From Qualified Partners (third parties) OptIn email from Spark Networks.
			/// </summary>
			CrossPromotion = 15,
			/// <summary>
			/// Once an Member photo has been approved for display, Spark Networks sends a confirmation email informing the Member that the photo as accepted and posted.
			/// </summary>
			PhotoApproval = 16,
			/// <summary>
			/// Automated response to a form request for information via the Contact Us site page.
			/// </summary>
			ContactUs = 17,
			/// <summary>
			/// ???
			/// </summary>
			EventComments = 18,
			/// <summary>
			/// Confirmation when the Member changes his/her offsite email address.
			/// </summary>
			EmailChangeConfirmation = 19,
			/// <summary>
			/// Event and other emails where identical emails are sent to all or a select portion of our user base.
			/// </summary>
			Broadcast = 20
		}

	}
}
