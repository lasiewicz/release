using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for EmailLogEntry.
	/// </summary>
	[Serializable]
	public class EmailLogEntry : IValueObject
	{
		private int _mailID;
		private int _communityID;
		private int _fromMemberID;
		private int _toMemberID;
		private DateTime _insertDate;
		private int _statusMask;
		private int _mailType;
        private int _mailOption;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="mailID"></param>
		/// <param name="communityID"></param>
		/// <param name="fromMemberID"></param>
		/// <param name="toMemberID"></param>
		/// <param name="insertDate"></param>
		/// <param name="statusMask"></param>
        /// <param name="mailType"></param>
        /// <param name="mailOption"></param>
        public EmailLogEntry(int mailID, int communityID, int fromMemberID, int toMemberID, DateTime insertDate, int statusMask, int mailType, int mailOption)
		{
			_mailID = mailID;
			_communityID = communityID;
			_fromMemberID = fromMemberID;
			_toMemberID = toMemberID;
			_insertDate = insertDate;
			_statusMask = statusMask;
			_mailType = mailType;
            _mailOption = mailOption;
		}

		/// <summary>
		/// 
		/// </summary>
		public int MailID
		{
			get { return(_mailID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int CommunityID
		{
			get { return(_communityID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int FromMemberID
		{
			get { return(_fromMemberID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int ToMemberID
		{
			get { return(_toMemberID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return(_insertDate); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int StatusMask
		{
			get { return(_statusMask); }
		}

        /// <summary>
        /// 
        /// </summary>
		public int MailType
		{
			get { return(_mailType); }
		}

        /// <summary>
        /// 
        /// </summary>
        public int MailOption
        {
            get { return (_mailOption); }
        }

		}
}
