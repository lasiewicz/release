using System;
using System.IO;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for IByteConvertable.
	/// </summary>
	public interface IByteConvertable
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="bw"></param>
		void ToBytes(BinaryWriter bw);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		Int32 GetByteCount();
	}
}
