﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Email.ValueObjects
{
    [Serializable()]
    public class MessageGeneralResult : IValueObject
    {
        public MessageGeneralResultStatus Status { get; set; }
        public MessageGeneralResultError Error { get; set; }
        public int MemberID { get; set; }
        public int TargetMemberID { get; set; }
        public int MessageListID { get; set; }

        public MessageGeneralResult()
        {
            Status = MessageGeneralResultStatus.Success;
            Error = MessageGeneralResultError.None;
        }
    }

    public enum MessageGeneralResultStatus
    {
        None = 0,
        Success = 1,
        Failed = 2
    }

    public enum MessageGeneralResultError
    {
        None = 0,
        MessageNotFound = 1,
        MessageMemberNotFound = 2,
        MemberNotFound = 3,
        TargetMemberNotFound = 4,
        InvalidConversationType = 5,
        MessageMemberNotFoundInTrash = 6,
        UnknownError = 100
    }
}
