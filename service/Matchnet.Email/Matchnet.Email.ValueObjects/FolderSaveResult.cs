using System;
using System.Runtime.Serialization;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class FolderSaveResult : ISerializable
	{
		private FolderSaveResultStatus _folderSaveResultStatus;
		private Int32 _memberFolderID;

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="folderSaveResultStatus"></param>
		/// <param name="memberFolderID"></param>
		public FolderSaveResult(FolderSaveResultStatus folderSaveResultStatus, Int32 memberFolderID)
		{
			_folderSaveResultStatus = folderSaveResultStatus;
			_memberFolderID = memberFolderID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public FolderSaveResult(SerializationInfo info, StreamingContext context)
		{
			_folderSaveResultStatus = (FolderSaveResultStatus) info.GetValue("folderSaveResultStatus", typeof(FolderSaveResultStatus));
			_memberFolderID = info.GetInt32("memberFolderID");
		}
		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public FolderSaveResultStatus FolderSaveResultStatus
		{
			get
			{
				return _folderSaveResultStatus;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberFolderID
		{
			get
			{
				return _memberFolderID;
			}
		}
		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("folderSaveResultStatus", _folderSaveResultStatus);
			info.AddValue("memberFolderID", _memberFolderID);
		}

		#endregion
	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public enum FolderSaveResultStatus : int
	{
		/// <summary> </summary>
		Success = 0,
		/// <summary> </summary>
		FolderNameExists = -1
	};
}
