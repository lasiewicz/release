using System;
using System.Collections;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for EmailLogEntryCollection.
	/// </summary>
	[Serializable]
	public class EmailLogEntryCollection : System.Collections.CollectionBase, IValueObject
	{

		/// <summary>
		/// 
		/// </summary>
		public enum SortDirection 
		{
			/// <summary></summary>
			Ascending,
			/// <summary></summary>
			Descending
		}

		/// <summary>
		/// 
		/// </summary>
		public EmailLogEntryCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public EmailLogEntry this[int index]
		{
			get{return (EmailLogEntry)base.InnerList[index];}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="emailLogEntry"></param>
		/// <returns></returns>
		public int Add(EmailLogEntry emailLogEntry)
		{
			return base.InnerList.Add(emailLogEntry);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Sort() 
		{
			base.InnerList.Sort( new EmailLogComparer(SortDirection.Descending) );
		}

		#region EmailLogComparer
		/// <summary>
		/// 
		/// </summary>
		public class EmailLogComparer : IComparer
		{
			private SortDirection dir;

			/// <summary>
			/// 
			/// </summary>
			/// <param name="pDir"></param>
			public EmailLogComparer( SortDirection pDir ) 
			{
				dir = pDir;
			}

			/// <summary>
			/// 
			/// </summary>
			/// <param name="x"></param>
			/// <param name="y"></param>
			/// <returns></returns>
			public int Compare( object x, object y ) 
			{
				switch( dir ) 
				{
					case SortDirection.Ascending :
						return ((EmailLogEntry)x).InsertDate.CompareTo(((EmailLogEntry)y).InsertDate);
					default : //descending
						return ((EmailLogEntry)y).InsertDate.CompareTo(((EmailLogEntry)x).InsertDate);
				}
			}

		}
		#endregion
	}
}
