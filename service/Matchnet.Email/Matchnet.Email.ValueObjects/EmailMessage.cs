using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class EmailMessage : IValueObject
	{
		#region Private Members
		private MessageList _messageList;
		private Message _message;
        private string _threadId;
		#endregion

		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageList"></param>
		public EmailMessage(MessageList messageList)
		{
			_messageList = messageList;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageList"></param>
		/// <param name="message"></param>
		public EmailMessage(MessageList messageList, Message message)
		{
			_messageList = messageList;
			_message = message;
		}

        public EmailMessage(MessageList messageList, Message message, string threadId)
        {
            _messageList = messageList;
            _message = message;
            _threadId = threadId;
        }

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public Message Message
		{
			get
			{
			    if (_message != null)
			    {
			        _message.MessageHeader = FillEmptyMessageHeader(_message.MessageHeader);
			    }

				return _message;
			}
			set
			{
				_message = value;

                if (_message != null)
                {
                    _message.MessageHeader = FillEmptyMessageHeader(_message.MessageHeader);
                }
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public MessageList MessageList
		{
			get
			{
				return _messageList;
			}
		}

		/// <summary>
		/// Represents the key for root Mail record associated with this EmailMessage.
		/// </summary>
		public int MailID
		{
			get
			{
				return _messageList.MessageID;
			}
		}

		/// <summary>
		/// Represents the CommunityID for this message.
		/// </summary>
		public int GroupID
		{
			get
			{
				return _messageList.GroupID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberMailID
		{
			get
			{
				return _messageList.MessageListID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public MailType MailTypeID 
		{
			get
			{
				return _messageList.MailTypeID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ToMemberID 
		{
			get
			{
				return _messageList.ToMemberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string ToUserName
		{
			get
			{
				return _messageList.ToUsername;
			}
			set
			{
				_messageList.ToUsername = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int FromMemberID
		{
			get
			{
				return _messageList.FromMemberID;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string FromUserName
		{
			get
			{
				return _messageList.FromUsername;
			}
			set 
			{
				_messageList.FromUsername = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public MessageStatus StatusMask
		{
			get
			{
				return _messageList.StatusMask;
			}
			set
			{
				_messageList.StatusMask = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get
			{
				return _messageList.InsertDate;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime OpenDate
		{
			get
			{
				return _message.OpenDate;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Subject
		{
			get
			{
			    return _message == null ? null : FillEmptyMessageHeader(_message.MessageHeader);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int NumBytes
		{
			get
			{
				return _messageList.NumBytes;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int DisplayKBytes
		{
			get
			{
				return (int)((NumBytes + 1023.9) / 1024);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Content
		{
			get
			{
				if (_message == null)
				{
					return null;
				}

				return _message.MessageBody;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MemberFolderID 
		{
			get
			{
				return _messageList.MemberFolderID;
			}
		}

        public string ThreadID
        {
            get
            {
                return _threadId;
            }
        }
		#endregion

        /// <summary>
        ///     This is to fix a bug that was introduce that allowed empty subjects which breaks
        ///     certain use cases such as not being able to open the message in the FWS mail grid.  
        /// </summary>
        /// <param name="messageHeader">a.k.a. subject</param>
        /// <returns></returns>
	    private string FillEmptyMessageHeader(string messageHeader)
	    {
            if (String.IsNullOrWhiteSpace(messageHeader))
                messageHeader = Enum.GetName(typeof(MailType), MailTypeID);

            return messageHeader;
	    }
	}

	/// <summary>
	/// 
	/// </summary>
	public class ByteConverter
	{

		/// <summary>
		/// TODO: Move this into a shared library
		/// </summary>
		/// <param name="s"></param>
		/// <param name="bw"></param>
		public static void ToBytes(string s, BinaryWriter bw)
		{
			bool isNull = (s == null);
			bw.Write(isNull);
			if (!isNull)
			{
				bw.Write(s);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="d"></param>
		/// <param name="bw"></param>
		public static void ToBytes(DateTime d, BinaryWriter bw)
		{
			bw.Write(d.Ticks);
		}

		/// <summary>
		/// TODO: Move this into a shared library
		/// </summary>
		/// <param name="br"></param>
		/// <returns></returns>
		public static string StringFromBytes(BinaryReader br)
		{
			bool isNull = br.ReadBoolean();
			if (!isNull)
			{
				return br.ReadString();
			}
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="br"></param>
		/// <returns></returns>
		public static DateTime DateTimeFromBytes(BinaryReader br)
		{
			return new DateTime(br.ReadInt64());
		}
	}

	#region Enumerations
	/// <summary>
	/// 
	/// </summary>
	public enum Direction
	{
		/// <summary>
		/// 
		/// </summary>
		None = 0,
		/// <summary>
		/// 
		/// </summary>
		Previous = 1,
		/// <summary>
		/// 
		/// </summary>
		Next = 2
	}

	/// <summary>
	/// 
	/// </summary>
	[Flags]
	public enum MessageStatus
	{
		/// <summary>
		/// 
		/// </summary>
		Nothing = 0,
		/// <summary>
		/// 
		/// </summary>
		Read = 1,
		/// <summary>
		/// 
		/// </summary>
		Replied = 2,
		/// <summary>
		/// 
		/// </summary>
		Show = 4,
		/// <summary>
		/// 
		/// </summary>
		New = 8,
		///</summary>
		///	/// 
		/// </summary>
		SavedToDraft=16,
        /// <summary>
        /// VIP email can be replied once for free
        /// </summary>
        OneFreeReply = 32,
        /// <summary>
        /// This is used to execute soft deletes where it's not deleted in the DB, but our caches should ignore
        /// </summary>
        Deleted = 64,
        /// <summary>
        /// These messages should be held until the member clears the fraud check. Should be treated similar to Deleted where
        /// these messages shouldn't appear in our cache.
        /// </summary>
        FraudHold = 128,
        /// <summary>
        /// This is used to execute soft removes of members from MessageMemberList for things like Instant Message Member list, which represents a list of members you've had messaging with
        /// </summary>
        MemberRemove = 256
	}

	/// <summary>
	/// 
	/// </summary>
	public enum MailType 
	{
        /// <summary>
        /// 
        /// </summary>
        None = 0,
		/// <summary>
		/// 
		/// </summary>
		Email = 1,
		/// <summary>
		/// 
		/// </summary>
		Tease = 2,
		/// <summary>
		/// 
		/// </summary>
		MissedIM = 3,
		/// <summary>
		/// 
		/// </summary>
		SystemAnnouncement = 4,
		/// <summary>
		/// 
		/// </summary>
		DeclineEmail = 5,
		/// <summary>
		/// 
		/// </summary>
		IgnoreEmail = 6,
		/// <summary>
		/// 
		/// </summary>
		YesEmail = 7,
		/// <summary>
		/// 
		/// </summary>
		PrivatePhotoEmail = 8,
		/// <summary>
		/// 
		/// </summary>
		ECard = 9,
		/// <summary>
		/// For Jmeter app
		/// </summary>
		CompatibilityMeterEmail = 10,
		/// <summary>
		/// 
		/// </summary>
        MissedGame = 11,
        /// <summary>
        /// 
        /// </summary>
        MissedOmnidate = 12,
        /// <summary>
        /// 
        /// </summary>
        QandAComment = 13,
        /// <summary>
        /// 
        /// </summary>
        FreeTextComment = 14,
        /// <summary>
        /// 
        /// </summary>
        PhotoComment = 15,
        /// <summary>
        /// 
        /// </summary>
        InstantMessage = 16,
        /// <summary>
        /// Ask me for my photo email
        /// </summary>
        NoPhotoRequest = 17
    }

    /// <summary>
    /// Any additional mail options tied to the EmailMessage
    /// </summary>
    [Flags]
    public enum MailOption
    {
        None = 0,
        VIP = 1,
        FreeReplyMessage = 2,
        FraudHold = 4
    }
	#endregion
}
