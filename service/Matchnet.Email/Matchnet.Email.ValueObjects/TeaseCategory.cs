using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeaseCategory.
	/// </summary>
	[Serializable()] public class TeaseCategory
	{
		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="teaseCategoryID"></param>
		/// <param name="resourceKey"></param>
		public TeaseCategory(int teaseCategoryID, string resourceKey)
		{
			this._teaseCategoryID	= teaseCategoryID;
			this._resourceKey			= resourceKey;
		}

		public TeaseCategory(int teaseCategoryID, string resourceKey, string promoResourceKey, DateTime beginDate, DateTime endDate)
		{
			this._teaseCategoryID	= teaseCategoryID;
			this._resourceKey			= resourceKey;
			this._promoResourceKey		= promoResourceKey;
			this._beginDate = beginDate;
			this._endDate = endDate;
		}

		#endregion

		#region Private Members

		private int _teaseCategoryID;
		private string _resourceKey;
		private string _promoResourceKey = string.Empty;
		private DateTime _beginDate;
		private DateTime _endDate;

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public int TeaseCategoryID
		{
			get
			{
				return this._teaseCategoryID;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string ResourceKey
		{
			get
			{
				return this._resourceKey;
			}
		}

		public string PromoResourceKey
		{
			get
			{
				return this._promoResourceKey;
			}
		}

		public DateTime BeginDate
		{
			get
			{
				return this._beginDate;
			}
		}

		public DateTime EndDate
		{
			get
			{
				return this._endDate;
			}
		}

		#endregion
	}
}
