using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for MemberMailLog.
	/// </summary>
	[Serializable]
	public class MemberMailLog : IValueObject, ICacheable
	{
		private EmailLogEntryCollection _received;
		private EmailLogEntryCollection _sent;
		private EmailLogEntryCollection _both;

		private int _memberID;
		private int _groupID;

		private int _cacheTTLSeconds = 10*60;
		private CacheItemMode _cacheMode = CacheItemMode.Absolute;
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
		private const string CACHE_KEY = "MemberMailLog";

		/// <summary>
		/// MemberMailLog Constructor
		/// </summary>
		public MemberMailLog(int memberID, int groupID)
		{
			_memberID = memberID;
			_groupID = groupID;

			_received = new EmailLogEntryCollection();
			_sent = new EmailLogEntryCollection();
			_both = new EmailLogEntryCollection();
		}

		/// <summary>
		/// Log of all messages received by a member.
		/// </summary>
		public EmailLogEntryCollection Received
		{
			get
			{
				return _received;
			}
		}

		/// <summary>
		/// Log of all messages sent by a member.
		/// </summary>
		public EmailLogEntryCollection Sent
		{
			get
			{
				return _sent;
			}
		}

		/// <summary>
		/// Log of all messages either sent or received by a member.
		/// </summary>
		public EmailLogEntryCollection Both
		{
			get
			{
				return _both;
			}
		}

		#region ICacheable Implementation

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return GetCacheKey(_memberID, _groupID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int memberID, int groupID)
		{
			return string.Format("{0}:{1}:{2}", CACHE_KEY, memberID, groupID);
		}

		#endregion
	}
}
