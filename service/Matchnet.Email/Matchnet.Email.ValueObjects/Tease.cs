using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeaseContent.
	/// </summary>
	[Serializable()] public class Tease
	{
		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="teaseID"></param>
		/// <param name="content"></param>
		public Tease(int teaseID, string content)
		{
			this._teaseID = teaseID;
			this._resourceKey = content;
		}

		#endregion

		#region Private Members

		private int _teaseID;
		private string _resourceKey;

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public int TeaseID
		{
			get
			{
				return this._teaseID;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string ResourceKey
		{
			get
			{
				return this._resourceKey;
			}
		}

		#endregion
	}
}
