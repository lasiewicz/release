using System;
using Matchnet.Caching;
using Matchnet.Email.ValueObjects.ReplicationActions;
using System.Collections.Generic;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for CacheUtil.
	/// </summary>
	public class CacheUtil
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="action"></param>
		public static void PlayAction(Cache cache, IReplicationAction action)
		{
            PlayAction(cache, null, action);
		}

        public static void PlayAction(Cache cache, Matchnet.ICaching cacheWebBucket, IReplicationAction action)
        {
            switch (action.GetType().Name)
            {
                case "ReplicationActionCollection":
                    ReplicationActionCollection replicationActionCollection = action as ReplicationActionCollection;
                    PlayActions(cache, cacheWebBucket, replicationActionCollection);
                    break;
                case "MessageSaveAction":
                    MessageSaveAction messageSaveAction = action as MessageSaveAction;
                    MessageSave(cache, cacheWebBucket, messageSaveAction);
                    break;
                case "MessageListSaveAction":
                    MessageListSaveAction messageListSaveAction = action as MessageListSaveAction;
                    MessageListSave(cache, cacheWebBucket, messageListSaveAction);
                    break;
                case "MessageDeleteAction":
                    MessageDeleteAction messageDeleteAction = action as MessageDeleteAction;
                    MessageDelete(cache, cacheWebBucket, messageDeleteAction);
                    break;
                case "MessageListDeleteAction":
                    MessageListDeleteAction messageListDeleteAction = action as MessageListDeleteAction;
                    MessageListDelete(cache, cacheWebBucket, messageListDeleteAction);
                    break;
                case "MessageOpenedAction":
                    MessageOpenedAction messageOpenedAction = action as MessageOpenedAction;
                    MessageSave(cache, cacheWebBucket, messageOpenedAction);
                    break;
                case "InstantMessagesDeleteAction":
                    InstantMessagesDeleteAction instantMessagesDeleteAction = action as InstantMessagesDeleteAction;
                    InstantMessagesDelete(cache, cacheWebBucket, instantMessagesDeleteAction);
                    break;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="actions"></param>
		public static void PlayActions(Cache cache, ReplicationActionCollection actions)
		{
            PlayActions(cache, null, actions);
		}

        public static void PlayActions(Cache cache, Matchnet.ICaching cacheWebBucket, ReplicationActionCollection actions)
        {
            int deleteActionCount = 0;
            bool clearWebBucket = true;
            foreach (IReplicationAction action in actions)
            {
                clearWebBucket = true;

                //setting clearWebBucket to false when we're doing multiple of the same action since mail in the bucket are by conversations
                //with the full list of mail, no need to constantly clear it
                switch (action.GetType().Name)
                {
                    case "MessageDeleteAction":
                    case "MessageListDeleteAction":
                        deleteActionCount++;
                        if (deleteActionCount > 1)
                        {
                            clearWebBucket = false;
                        }
                        break;
                }

                if (clearWebBucket)
                    PlayAction(cache, cacheWebBucket, action);
                else
                    PlayAction(cache, action);
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="messageSaveAction"></param>
		public static void MessageSave(Cache cache, MessageSaveAction messageSaveAction)
        {
            MessageSave(cache, null, messageSaveAction);
        }

        public static void MessageSave(Cache cache, Matchnet.ICaching cacheWebBucket, MessageSaveAction messageSaveAction)
        {
            MessageSave(cache, cacheWebBucket, messageSaveAction, Constants.NULL_INT);
        }

        public static void MessageSave(Cache cache, Matchnet.ICaching cacheWebBucket, MessageSaveAction messageSaveAction, int groupID)
		{
			cache.Remove(Message.GetCacheKey(messageSaveAction.Message.MessageID));
			cache.Add(messageSaveAction.Message);

            //only draft messages can be updated, we will clear out just the draft conversation message list
            if (cacheWebBucket != null && groupID > 0 && ((messageSaveAction.SaveType & MessageSaveType.Update) == MessageSaveType.Update) && ((messageSaveAction.SaveType & MessageSaveType.Draft) == MessageSaveType.Draft))
            {
                RemoveMailConversationMembersCache(cacheWebBucket, messageSaveAction.MemberID, groupID, ConversationType.Draft);
            }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="messageOpenedAction"></param>
		public static void MessageSave(Cache cache, MessageOpenedAction messageOpenedAction)
        {
            MessageSave(cache, null, messageOpenedAction);
        }

        public static void MessageSave(Cache cache, Matchnet.ICaching cacheWebBucket, MessageOpenedAction messageOpenedAction)
		{
			Message message = cache.Get(Message.GetCacheKey(messageOpenedAction.MessageID)) as Message;
			if (message != null)
			{
				message.OpenDate = messageOpenedAction.OpenDate;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="messageDeleteAction"></param>
		public static void MessageDelete(Cache cache, MessageDeleteAction messageDeleteAction)
        {
            MessageDelete(cache, null, messageDeleteAction);
        }

        public static void MessageDelete(Cache cache, Matchnet.ICaching cacheWebBucket, MessageDeleteAction messageDeleteAction)
		{
			cache.Remove(Message.GetCacheKey(messageDeleteAction.MessageID));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="messageListSaveAction"></param>
		public static void MessageListSave(Cache cache, MessageListSaveAction messageListSaveAction)
        {
            MessageListSave(cache, null, messageListSaveAction);
        }

        public static void MessageListSave(Cache cache, Matchnet.ICaching cacheWebBucket, MessageListSaveAction messageListSaveAction)
		{
			MessageListCollection messageListCollection = cache[MessageListCollection.GetCacheKey(messageListSaveAction.MemberID, messageListSaveAction.MessageList.GroupID)] as MessageListCollection;

			if (messageListCollection != null)
			{
                if (messageListSaveAction.SaveType == MessageListSaveType.Delete)
                {
                    messageListCollection.Remove(messageListSaveAction.MessageList.MessageListID);
                }
                else
                {
                    if( (messageListSaveAction.MessageList.StatusMask & MessageStatus.FraudHold) == 0)
                        messageListCollection[messageListSaveAction.MessageList.MessageListID] = messageListSaveAction.MessageList;
                }
			}

            if (cacheWebBucket != null)
            {
                //clear conversation member list
                //note: message list cache automatically clears by comparing how old it is relatively to the members list cache
                if (messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.VIPInbox || messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.Inbox)
                {
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Inbox);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Draft);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Sent);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Trash);

                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.TargetMemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Inbox);
                }
                else if (messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.Sent || messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.VIPSent)
                {
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Inbox);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Draft);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Sent);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Trash);
                }
                else if (messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.Draft || messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.VIPDraft)
                {
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Draft);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Trash);
                }
                else if (messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.Trash || messageListSaveAction.MessageList.MemberFolderID == (int)SystemFolders.VIPTrash)
                {
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Inbox);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Draft);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Sent);
                    RemoveMailConversationMembersCache(cacheWebBucket, messageListSaveAction.MessageList.MemberID, messageListSaveAction.MessageList.GroupID, ConversationType.Trash);
                }
            }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cache"></param>
		/// <param name="messageListDeleteAction"></param>
		public static void MessageListDelete(Cache cache, MessageListDeleteAction messageListDeleteAction)
        {
            MessageListDelete(cache, null, messageListDeleteAction);
        }

        public static void MessageListDelete(Cache cache, Matchnet.ICaching cacheWebBucket, MessageListDeleteAction messageListDeleteAction)
		{
			MessageListCollection messageListCollection = cache[MessageListCollection.GetCacheKey(messageListDeleteAction.MemberID, messageListDeleteAction.GroupID)] as MessageListCollection;

			if (messageListCollection != null)
			{
				messageListCollection.Remove(messageListDeleteAction.MessageListID);
			}

            if (cacheWebBucket != null)
            {
                //clear cache for conversation mail members list
                RemoveMailConversationMembersCache(cacheWebBucket, messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, ConversationType.Inbox);
                RemoveMailConversationMembersCache(cacheWebBucket, messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, ConversationType.Trash);
                RemoveMailConversationMembersCache(cacheWebBucket, messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, ConversationType.Sent);
                RemoveMailConversationMembersCache(cacheWebBucket, messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, ConversationType.Draft);
                RemoveMailConversationMembersCache(cacheWebBucket, messageListDeleteAction.MemberID, messageListDeleteAction.GroupID, MailType.InstantMessage, ConversationType.Chat);

            }
		}

        public static void RemoveMailConversationMembersCache(Matchnet.ICaching cacheWebBucket, int memberID, int groupID, ConversationType conversationType)
        {
            //clear cache for conversation mail members list
            RemoveMailConversationMembersCache(cacheWebBucket, memberID, groupID, MailType.None, conversationType);
        }

        public static void RemoveMailConversationMembersCache(Matchnet.ICaching cacheWebBucket, int memberID, int groupID, MailType mailType, ConversationType conversationType)
        {
            //clear cache for conversation mail members list
            cacheWebBucket.Remove(MessageMemberCollection.GetCacheKey(memberID, groupID, mailType, conversationType));
        }

        public static void RemoveMailConversationMessagesCache(Matchnet.ICaching cacheWebBucket, int memberID, int targetMemberID, int groupID, MailType mailType, ConversationType conversationType)
        {
            //clear cache for conversation mail members list
            cacheWebBucket.Remove(EmailMessageList.GetCacheKey(memberID, targetMemberID, groupID, conversationType, mailType));
        }

        public static void InstantMessagesDelete(Cache cache, Matchnet.ICaching cacheWebBucket, InstantMessagesDeleteAction instantMessagesDeleteAction)
        {
            if (cacheWebBucket != null)
            {
                //clear cache for conversation mail members and messages list
                RemoveMailConversationMembersCache(cacheWebBucket, instantMessagesDeleteAction.MemberID, instantMessagesDeleteAction.GroupID, MailType.InstantMessage, ConversationType.Chat);
                RemoveMailConversationMessagesCache(cacheWebBucket, instantMessagesDeleteAction.MemberID, instantMessagesDeleteAction.TargetMemberID, instantMessagesDeleteAction.GroupID, MailType.InstantMessage, ConversationType.Chat);
            }
        }

	}
}
