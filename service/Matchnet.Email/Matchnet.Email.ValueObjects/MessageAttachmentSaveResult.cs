﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Email.ValueObjects
{
    [Serializable()]
    public class MessageAttachmentSaveResult : IValueObject
    {
        #region Properties
        public MessageAttachmentSaveStatus Status { get; set; }
        public MessageAttachmentSaveError Error { get; set; }
        public int MessageAttachmentID { get; set; }
        public int MessageID { get; set; }
        public string OriginalCloudPath { get; set; }
        public string PreviewCloudPath { get; set; }
        public int FileID { get; set; }

        #endregion

        #region Constructor

        public MessageAttachmentSaveResult()
        {
            Status = MessageAttachmentSaveStatus.Success;
            Error = MessageAttachmentSaveError.None;
            OriginalCloudPath = "";
            PreviewCloudPath = "";
        }

        #endregion
    }

    #region Enumerations
    /// <summary>
    /// Status of saving message attachment
    /// </summary>
    public enum MessageAttachmentSaveStatus
    {
        None = 0,
        /// <summary>
        /// Indicates successful attachment saved
        /// </summary>
        Success = 1,
        /// <summary>
        /// Indicates failure saving attachment
        /// </summary>
        Failed = 2
    }

    /// <summary>
    /// Message attachment save error
    /// </summary>
    public enum MessageAttachmentSaveError
    {
        None = 0,
        Unknown = 1,
        SavePhysicalFileError = 2,
        SaveAttachmentRecordError = 3,
        InvalidPhoto = 4
    }
    #endregion
}
