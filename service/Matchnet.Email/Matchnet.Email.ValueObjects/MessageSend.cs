using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.Email.ValueObjects.ReplicationActions;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class MessageSend : IValueObject
	{
		#region Private Members
		private MessageSave _messageSave;
		private Int32 _senderMessageListID;
		private Int32 _recipientMessageListID;
		private Int32 _messageID;
		private bool _draft;

		#endregion

		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageSave"></param>
		/// <param name="senderMessageListID"></param>
		/// <param name="recipientMessageListID"></param>
		/// <param name="messageID"></param>
		/// <param name="draft"></param>
		public MessageSend(MessageSave messageSave, Int32 senderMessageListID, Int32 recipientMessageListID, Int32 messageID, bool draft)
		{
			if (messageSave == null)
			{
				throw new ArgumentNullException("messageSave");
			}

			_messageSave = messageSave;
			_senderMessageListID = senderMessageListID;
			_recipientMessageListID = recipientMessageListID;
			_messageID = messageID;
			_draft = draft;
		}

		#endregion

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public MessageSave MessageSave
		{
			get
			{
				return _messageSave;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 SenderMessageListID
		{
			get
			{
				return _senderMessageListID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 RecipientMessageListID
		{
			get
			{
				return _recipientMessageListID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MessageID
		{
			get
			{
				return _messageID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool Draft
		{
			get
			{
				return _draft;
			}
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="cacheTTL"></param>
		/// <returns></returns>
		public MessageSaveAction GetMessageSaveAction(bool sender, Int32 cacheTTL)
		{
			MessageSaveType messageSaveType = (_draft && sender) ? MessageSaveType.Update | MessageSaveType.Draft : MessageSaveType.Insert;

			return _messageSave.GetMessageSaveAction(sender, _messageID, messageSaveType, cacheTTL);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <returns></returns>
		public MessageListSaveAction GetMessageListSaveAction(bool sender)
		{
			Int32 messageListID;
			Int32 memberFolderID;
            bool markvipmail=false;
			MessageListSaveType messageListSaveType;

			if (sender)
			{
				messageListID = _senderMessageListID;
                if( (_messageSave.MailOption & MailOption.VIP) == MailOption.VIP)
                {
                    memberFolderID = (Int32) SystemFolders.VIPSent;
                }
                else
                {
                    memberFolderID = (Int32)SystemFolders.Sent;    
                }
				messageListSaveType = _draft ? MessageListSaveType.Update : messageListSaveType = MessageListSaveType.Insert;
			}
			else
			{
				messageListID = _recipientMessageListID;
                if ((_messageSave.MailOption & MailOption.VIP) == MailOption.VIP)
                {
                    memberFolderID = (Int32)SystemFolders.VIPInbox;
                    markvipmail=true;
                }
                else
                {
                    memberFolderID = (Int32)SystemFolders.Inbox;    
                }
				messageListSaveType = MessageListSaveType.Insert;
			}

			MessageListSaveAction saveaction= _messageSave.GetMessageListSaveAction(sender, _messageID, messageListID, memberFolderID, messageListSaveType);
          
            return saveaction;
		}
	}
}
