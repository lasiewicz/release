using System;

namespace Matchnet.Email.ValueObjects
{
	#region Enumerations

	/// <summary> @SaveType parameter for up_Message_Save </summary>
	[Flags]
	public enum MessageSaveType : int
	{
		/// <summary> </summary>
		Insert = 1,
		/// <summary> </summary>
		Update = 2,
		/// <summary> </summary>
		OpenDate = 4,
		/// <summary> </summary>
		Draft = 8
	}

	/// <summary> @SaveType parameter for up_MessageList_Save </summary>
	public enum MessageListSaveType : int
	{
		/// <summary> </summary>
		Insert = 1,
		/// <summary> </summary>
		Update = 2,
        /// <summary>soft delete</summary>
        Delete = 3,
        /// <summary>undelete the soft deleted items</summary>
        Undelete = 4
	}

	/// <summary> @ListType parameter for up_Message_List </summary>
	public enum MessageListType : int
	{
		/// <summary> </summary>
		NoContent = 1,
		/// <summary> </summary>
		Everything = 2
	}

	/// <summary> @ListType parameter for up_MessageList_List </summary>
	public enum MessageListListType : int
	{
		/// <summary> </summary>
		Retrieve = 1,
		/// <summary> </summary>
		Exists = 2
	}

	/// <summary>
	/// 
	/// </summary>
	public enum SystemFolders
	{
        None = 0,
		/// <summary>
		/// 
		/// </summary>
		Inbox = 1,
		/// <summary>
		/// 
		/// </summary>
		Draft = 2,
		/// <summary>
		/// 
		/// </summary>
		Sent = 3,
		/// <summary>
		/// 
		/// </summary>
		Trash = 4,
        /// <summary>
        /// 
        /// </summary>
        VIPInbox = 5,
        /// <summary>
        /// 
        /// </summary>
        VIPDraft = 6,
        /// <summary>
        /// 
        /// </summary>
        VIPSent = 7,
        /// <summary>
        /// 
        /// </summary>
        VIPTrash = 8
	}

    public enum AttachmentType
    {
        None = 0,
        IMPhoto = 1
    }

    public enum AttachmentStatus
    {
        None = 0,
        SavedNoApprovalRequired = 1
    }

    public enum ConversationType
    {
        None = 0,
        Inbox = 1,
        Sent = 2,
        Draft = 3,
        Trash = 4,
        Chat = 5,
        Unread = 6 //subset of Inbox
    }

	#endregion
}
