using System;
using System.Collections;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for EmailComparer.
	/// </summary>
	public class EmailComparer : IComparer
	{
		/// <summary>
		/// 
		/// </summary>
		public enum SortType
		{
			/// <summary></summary>
			Subject,
			/// <summary></summary>
			FromUserName,
			/// <summary></summary>
			ToUserName,
			/// <summary></summary>
			InsertDate,
			/// <summary></summary>
			DisplayKBytes,
			/// <summary></summary>
			StatusMask,
			/// <summary></summary>
			OpenDate
		}

		/// <summary>
		/// 
		/// </summary>
		public enum Direction
		{
			/// <summary></summary>
			Ascending,
			/// <summary></summary>
			Descending
		}

		private SortType _sortType;
		private Direction _direction;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sortType"></param>
		/// <param name="direction"></param>
		public EmailComparer(SortType sortType, Direction direction)
		{
			_sortType = sortType;
			_direction = direction;
		}

		/// <summary>
		/// 
		/// </summary>
		public EmailComparer()
		{
			_sortType = SortType.InsertDate;
			_direction = Direction.Descending;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj1"></param>
		/// <param name="obj2"></param>
		/// <returns></returns>
		public int Compare(object obj1, object obj2)
		{
			EmailMessage message1;
			EmailMessage message2;

			if (_direction == Direction.Ascending)
			{
				message1 = obj1 as EmailMessage;
				message2 = obj2 as EmailMessage;
			}
			else
			{
				message2 = obj1 as EmailMessage;
				message1 = obj2 as EmailMessage;
			}

			if (message1 == null || message2 == null)
			{
				return 0;
			}

			switch (_sortType)
			{
				case SortType.Subject:
					if (message1.Subject == null || message2.Subject == null)
					{
						return 0;
					}
					return message1.Subject.CompareTo(message2.Subject);
				case SortType.FromUserName:
					return message1.FromUserName.CompareTo(message2.FromUserName);
				case SortType.ToUserName:
					return message1.ToUserName.CompareTo(message2.ToUserName);
				case SortType.InsertDate:
					Int32 result = message1.InsertDate.CompareTo(message2.InsertDate);
					if (result == 0)
					{
						return message1.MemberMailID.CompareTo(message2.MemberMailID);
					}
					else
					{
						return result;
					}
				case SortType.DisplayKBytes:
					return message1.DisplayKBytes.CompareTo(message2.DisplayKBytes);
				case SortType.StatusMask:
					bool isRead1 = (message1.StatusMask & MessageStatus.Read) == MessageStatus.Read;
					bool isRead2 = (message2.StatusMask & MessageStatus.Read) == MessageStatus.Read;
					return isRead1.CompareTo(isRead2);
				case SortType.OpenDate:
					return message1.OpenDate.CompareTo(message2.OpenDate);
				default:
					return 0;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public SortType SortTypeValue
		{
			get
			{
				return _sortType;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderBy"></param>
		/// <returns></returns>
		public static EmailComparer FromString(string orderBy)
		{
			string[] parts = orderBy.Split(new char[]{' '});

			EmailComparer.SortType sortType = (EmailComparer.SortType) Enum.Parse(typeof(EmailComparer.SortType), parts[0], true);

			EmailComparer.Direction direction = EmailComparer.Direction.Descending;
			if (parts[1] == "asc")
			{
				direction = EmailComparer.Direction.Ascending;
			}

			return new EmailComparer(sortType, direction);
		}
	}
}
