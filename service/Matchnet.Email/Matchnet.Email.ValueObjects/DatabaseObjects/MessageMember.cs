using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class MessageMember : IValueObject
	{
		#region Private Members
		private Int32 _MessageListID = Constants.NULL_INT;
		private Int32 _memberID = Constants.NULL_INT;
		private Int32 _targetMemberID = Constants.NULL_INT;
		private bool _directionFlag = false;
		private Int32 _groupID = Constants.NULL_INT;
		private Int32 _memberFolderID = Constants.NULL_INT;
		private Int32 _messageID = Constants.NULL_INT;
		private MessageStatus _statusMask = MessageStatus.Nothing;
		private MailType _mailTypeID = MailType.Email;
		private DateTime _insertDate = DateTime.MinValue;
        private Int32 _numBytes = 0;
        private int _unReadMessageCount = Constants.NULL_INT;
        private int _unReadVipCount = Constants.NULL_INT;
        private int _DraftCount = Constants.NULL_INT;
        private int _vipCount = Constants.NULL_INT;
        private string _body = "";
        private int _lastMessageListID = Constants.NULL_INT;
        private MessageStatus _lastMessageStatusMask = MessageStatus.Nothing;
        private int _lastMessageFolderID = Constants.NULL_INT;
        private DateTime _lastMessageInsertDate = DateTime.MinValue;

		#endregion

		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="MessageListID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="directionFlag"></param>
		/// <param name="groupID"></param>
		/// <param name="memberFolderID"></param>
		/// <param name="messageID"></param>
		/// <param name="statusMask"></param>
		/// <param name="mailTypeID"></param>
		/// <param name="insertDate"></param>
		/// <param name="numBytes"></param>
        public MessageMember(Int32 MessageListID, Int32 memberID, Int32 targetMemberID, bool directionFlag, Int32 groupID, Int32 memberFolderID, Int32 messageID, MessageStatus statusMask, MailType mailTypeID, DateTime insertDate, Int32 numBytes, string messageBody)
		{
			_MessageListID = MessageListID;
			_memberID = memberID;
			_targetMemberID = targetMemberID;
			_directionFlag = directionFlag;
			_groupID = groupID;
			_memberFolderID = memberFolderID;
			_messageID = messageID;
			_statusMask = statusMask;
			_mailTypeID	= mailTypeID;
			_insertDate = insertDate;
            _numBytes = numBytes;
            _body = messageBody;
		}

        public MessageMember(Int32 MessageListID, Int32 memberID, Int32 targetMemberID, bool directionFlag, Int32 groupID, Int32 memberFolderID, Int32 messageID, MessageStatus statusMask, MailType mailTypeID, DateTime insertDate, Int32 numBytes, Int32 unreadMessageCount, Int32 unreadVipCount, string messageBody)
        {
            _MessageListID = MessageListID;
            _memberID = memberID;
            _targetMemberID = targetMemberID;
            _directionFlag = directionFlag;
            _groupID = groupID;
            _memberFolderID = memberFolderID;
            _messageID = messageID;
            _statusMask = statusMask;
            _mailTypeID = mailTypeID;
            _insertDate = insertDate;
            _numBytes = numBytes;
            _unReadMessageCount = unreadMessageCount;
            _unReadVipCount = unreadVipCount;
            _body = messageBody;
        }

		#endregion

		#region Properties

		/// <summary>
		/// Represents the key for root MessageMember record associated with this member's copy of the Message.
		/// </summary>
		public Int32 MessageListID
		{
			get{return _MessageListID;}
			set{_MessageListID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID 
		{
			get{return _memberID;}
			set{_memberID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 TargetMemberID
		{
			get{return _targetMemberID;}
			set{_targetMemberID = value;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public bool DirectionFlag
		{
			get{return _directionFlag;}
			set{_directionFlag = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 FromMemberID
		{
			get
			{
				return DirectionFlag ? TargetMemberID : MemberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 ToMemberID
		{
			get
			{
				return DirectionFlag ? MemberID : TargetMemberID;
			}
		}


		/// <summary>
		/// Represents the GroupID for this message.
		/// </summary>
		public Int32 GroupID
		{
			get{return _groupID;}
			set{_groupID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberFolderID
		{
			get{return _memberFolderID;	}
			set{_memberFolderID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MessageID
		{
			get{return _messageID;}
			set{_messageID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public MessageStatus StatusMask
		{
			get{return _statusMask;}
			set{_statusMask = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public MailType MailTypeID 
		{
			get{return _mailTypeID;}
			set{_mailTypeID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get{return _insertDate;}
			set{_insertDate = value;}
		}

        /// <summary>
        /// 
        /// </summary>
        public Int32 NumBytes
        {
            get { return _numBytes; }
            set { _numBytes = value; }
        }

        public bool IsAllAccess
        {
            get
            {
                if (_unReadVipCount > 0)
                {
                    return true;
                }
                else if (_memberFolderID == (int)(SystemFolders.VIPInbox))
                {
                    if (_lastMessageListID > 0 && (_lastMessageListID != _MessageListID))
                    {
                        return false;
                    }

                    return true;
                }

                return false;
            }
        }

        public int UnreadMessageCount
        {
            get { return _unReadMessageCount; }
            set { _unReadMessageCount = value; }
        }

        public int UnreadVipCount
        {
            get { return _unReadVipCount; }
            set { _unReadVipCount = value; }
        }

        public int DraftCount
        {
            get { return _DraftCount; }
            set { _DraftCount = value; }
        }

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }

        public int VipCount
        {
            get { return _vipCount; }
            set { _vipCount = value; }
        }

        public int LastMessageListID
        {
            get { return _lastMessageListID; }
            set { _lastMessageListID = value; }
        }

        public int LastMessageFolderID
        {
            get { return _lastMessageFolderID; }
            set { _lastMessageFolderID = value; }
        }

        public MessageStatus LastMessageStatusMask
        {
            get { return _lastMessageStatusMask; }
            set { _lastMessageStatusMask = value; }
        }

        public DateTime LastMessageInsertDate
        {
            get { return _lastMessageInsertDate; }
            set { _lastMessageInsertDate = value; }
        }

		#endregion

	}
}
