using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class Message : IValueObject, ISerializable, ICacheable
	{
		#region Private Members
		private Int32 _messageID = Constants.NULL_INT;
		private string _messageHeader = Constants.NULL_STRING;
		private string _messageBody = Constants.NULL_STRING;
		private DateTime _insertDate = DateTime.MinValue;
		private DateTime _openDate = DateTime.MinValue;

		private const string CACHE_KEY_PREFIX = "Message";
		private Int32 _cacheTTLSeconds;
		private CacheItemMode _cacheMode = CacheItemMode.Absolute;
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;

		[NonSerialized]
		private ReferenceTracker _referenceTracker = new ReferenceTracker();

		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageID"></param>
		/// <param name="messageHeader"></param>
		/// <param name="messageBody"></param>
		/// <param name="insertDate"></param>
		/// <param name="openDate"></param>
		public Message(Int32 messageID, string messageHeader, string messageBody, DateTime insertDate, DateTime openDate) 
		{
			_messageID = messageID;
			_messageHeader = messageHeader;
			_messageBody = messageBody;
			_insertDate = insertDate;
			_openDate = openDate;
		}
		#endregion

		#region Properties

		/// <summary>
		/// Represents the key for root Mail record associated with this EmailMessage.
		/// </summary>
		public int MessageID
		{
			get{return _messageID;}
			set{_messageID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string MessageHeader
		{
			get{return _messageHeader;}
			set{_messageHeader = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string MessageBody
		{
			get{return _messageBody;}
			set{_messageBody = value;}
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get{return _insertDate;}
			set{_insertDate = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime OpenDate
		{
			get{return _openDate;}
			set{_openDate = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				return _referenceTracker;
			}
			set
			{
				_referenceTracker = value;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected Message(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			populate(br);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="br"></param>
		public Message(BinaryReader br)
		{
			populate(br);
		}

		private void populate(BinaryReader br)
		{
			_cacheTTLSeconds = br.ReadInt32();
			_cacheMode = (CacheItemMode) br.ReadInt32();
			_cachePriority = (CacheItemPriorityLevel) br.ReadInt32();

			_messageID = br.ReadInt32();
			_messageHeader = ByteConverter.StringFromBytes(br);
			_messageBody = ByteConverter.StringFromBytes(br);
			_insertDate = ByteConverter.DateTimeFromBytes(br);
			_openDate = ByteConverter.DateTimeFromBytes(br);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public void ToBytes(BinaryWriter bw)
		{
			bw.Write(_cacheTTLSeconds);
			bw.Write((Int32) _cacheMode);
			bw.Write((Int32) _cachePriority);
			bw.Write(_messageID);
			ByteConverter.ToBytes(_messageHeader, bw);
			ByteConverter.ToBytes(_messageBody, bw);
			ByteConverter.ToBytes(_insertDate, bw);
			ByteConverter.ToBytes(_openDate, bw);
		}

		private byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			ToBytes(bw);
			return ms.ToArray();
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Int32 GetByteCount()
		{
			return 40 + _messageHeader.Length * 2 + _messageBody.Length * 2;
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return Message.GetCacheKey(_messageID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 messageID)
		{
			return CACHE_KEY_PREFIX + ":" + messageID;
		}

		#endregion
	}
}
