using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class MessageList : IValueObject, ISerializable
	{
		#region Private Members
		private Int32 _messageListID = Constants.NULL_INT;
		private Int32 _memberID = Constants.NULL_INT;
		private Int32 _targetMemberID = Constants.NULL_INT;
		private bool _directionFlag = false;
		private Int32 _groupID = Constants.NULL_INT;
		private Int32 _memberFolderID = Constants.NULL_INT;
		private Int32 _messageID = Constants.NULL_INT;
		private MessageStatus _statusMask = MessageStatus.Nothing;
		private MailType _mailTypeID = MailType.Email;
		private DateTime _insertDate = DateTime.MinValue;
		private Int32 _numBytes = 0;
	    private string _threadID = "";

		[NonSerialized]
		private string _fromUsername;
		[NonSerialized]
		private string _toUsername;
		#endregion

		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageListID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="directionFlag"></param>
		/// <param name="groupID"></param>
		/// <param name="memberFolderID"></param>
		/// <param name="messageID"></param>
		/// <param name="statusMask"></param>
		/// <param name="mailTypeID"></param>
		/// <param name="insertDate"></param>
		/// <param name="numBytes"></param>
		public MessageList(Int32 messageListID, Int32 memberID, Int32 targetMemberID, bool directionFlag, Int32 groupID, Int32 memberFolderID, Int32 messageID, MessageStatus statusMask, MailType mailTypeID, DateTime insertDate, Int32 numBytes)
		{
			_messageListID = messageListID;
			_memberID = memberID;
			_targetMemberID = targetMemberID;
			_directionFlag = directionFlag;
			_groupID = groupID;
			_memberFolderID = memberFolderID;
			_messageID = messageID;
			_statusMask = statusMask;
			_mailTypeID	= mailTypeID;
			_insertDate = insertDate;
			_numBytes = numBytes;
		}

        /// <summary>
        /// Constructor adds parameter for ThreadID
        /// </summary>
        public MessageList(Int32 messageListID, Int32 memberID, Int32 targetMemberID, bool directionFlag, Int32 groupID, Int32 memberFolderID, Int32 messageID, MessageStatus statusMask, MailType mailTypeID, DateTime insertDate, Int32 numBytes, string threadID)
        {
            _messageListID = messageListID;
            _memberID = memberID;
            _targetMemberID = targetMemberID;
            _directionFlag = directionFlag;
            _groupID = groupID;
            _memberFolderID = memberFolderID;
            _messageID = messageID;
            _statusMask = statusMask;
            _mailTypeID = mailTypeID;
            _insertDate = insertDate;
            _numBytes = numBytes;
            _threadID = threadID;
        }

		#endregion

		#region Properties

		/// <summary>
		/// Represents the key for root MessageList record associated with this member's copy of the Message.
		/// </summary>
		public Int32 MessageListID
		{
			get{return _messageListID;}
			set{_messageListID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID 
		{
			get{return _memberID;}
			set{_memberID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 TargetMemberID
		{
			get{return _targetMemberID;}
			set{_targetMemberID = value;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public bool DirectionFlag
		{
			get{return _directionFlag;}
			set{_directionFlag = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 FromMemberID
		{
			get
			{
				return DirectionFlag ? TargetMemberID : MemberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 ToMemberID
		{
			get
			{
				return DirectionFlag ? MemberID : TargetMemberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string FromUsername
		{
			get
			{
				return _fromUsername;
			}
			set
			{
				_fromUsername = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string ToUsername
		{
			get
			{
				return _toUsername;
			}
			set
			{
				_toUsername = value;
			}
		}


		/// <summary>
		/// Represents the GroupID for this message.
		/// </summary>
		public Int32 GroupID
		{
			get{return _groupID;}
			set{_groupID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberFolderID
		{
			get{return _memberFolderID;	}
			set{_memberFolderID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MessageID
		{
			get{return _messageID;}
			set{_messageID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public MessageStatus StatusMask
		{
			get{return _statusMask;}
			set{_statusMask = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public MailType MailTypeID 
		{
			get{return _mailTypeID;}
			set{_mailTypeID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get{return _insertDate;}
			set{_insertDate = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 NumBytes
		{
			get{return _numBytes;}
			set{_numBytes = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 DisplayKBytes
		{
			get
			{
				return (Int32)((_numBytes + 1023.9) / 1024);
			}
		}

        /// <summary>
        /// ThreadID is used to form a relationship between messages
        /// For IM, we'll use it to group messages into a single conversation
        /// </summary>
	    public string ThreadID
	    {
            get { return _threadID; }
            set { _threadID = value; }
	    }

	    #endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageList(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			populate(br);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="br"></param>
		public MessageList(BinaryReader br)
		{
			populate(br);
		}

		private void populate(BinaryReader br)
		{
			_messageListID = br.ReadInt32();
			_memberID = br.ReadInt32();
			_targetMemberID = br.ReadInt32();
			_directionFlag = br.ReadBoolean();
			_groupID = br.ReadInt32();
			_memberFolderID = br.ReadInt32();
			_messageID = br.ReadInt32();
			_statusMask = (MessageStatus) br.ReadInt32();
			_mailTypeID	= (MailType) br.ReadInt32();
			_insertDate = new DateTime(br.ReadInt64());
			_numBytes = br.ReadInt32();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(GetByteCount());
			BinaryWriter bw = new BinaryWriter(ms);

			ToBytes(bw);

			return ms.ToArray();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="bw"></param>
		public void ToBytes(BinaryWriter bw)
		{
			bw.Write(_messageListID);
			bw.Write(_memberID);
			bw.Write(_targetMemberID);
			bw.Write(_directionFlag);
			bw.Write(_groupID);
			bw.Write(_memberFolderID);
			bw.Write(_messageID);
			bw.Write((Int32) _statusMask);
			bw.Write((Int32) _mailTypeID);
			bw.Write(_insertDate.Ticks);
			bw.Write(_numBytes);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Int32 GetByteCount()
		{
			return 48;
		}

		#endregion
	}
}
