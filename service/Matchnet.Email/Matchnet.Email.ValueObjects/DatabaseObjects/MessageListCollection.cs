using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageListCollection :  IEnumerable, IValueObject, ICacheable, ISerializable, ICollection
	{
		#region Private Variables
		private Hashtable _messages;
		private Int32 _memberID;
		private Int32 _groupID;

		[NonSerialized]
		private ReferenceTracker _referenceTracker = new ReferenceTracker();

		private const string CACHE_KEY_PREFIX = "MessageListCollection";
		private Int32 _cacheTTLSeconds;
		private CacheItemMode _cacheMode = CacheItemMode.Absolute;
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public MessageListCollection(Int32 memberID, Int32 groupID)
		{
			_messages = new Hashtable();
			_memberID = memberID;
			_groupID = groupID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageListCollection(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);

			_memberID = br.ReadInt32();
			_groupID = br.ReadInt32();
			_cacheTTLSeconds = br.ReadInt32();
			_cachePriority = (CacheItemPriorityLevel) br.ReadInt32();
			_cacheMode = (CacheItemMode) br.ReadInt32();

			_messages = new Hashtable();
			
			Int32 messageCount = br.ReadInt32();
			for (Int32 i = 0; i < messageCount; i++)
			{
				MessageList messageList = new MessageList(br);
				Add(messageList);
			}
		}

		#endregion

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageList"></param>
		/// <returns></returns>
		public void Add(MessageList messageList)
		{
			lock (_messages.SyncRoot)
			{
				if (!_messages.Contains(messageList.MessageListID))
				{
					_messages.Add(messageList.MessageListID, messageList);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageListID"></param>
		/// <returns></returns>
		public MessageList this[Int32 messageListID]
		{
			get
			{
				MessageList messageList;
				lock (_messages.SyncRoot)
				{
					messageList = _messages[messageListID] as MessageList;
				}
				return messageList;
			}
			set
			{
				MessageList messageList = value;

				lock (_messages.SyncRoot)
				{
					_messages[messageListID] = messageList;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageListID"></param>
		public void Remove(Int32 messageListID)
		{
			lock (_messages.SyncRoot)
			{
				_messages.Remove(messageListID);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public bool HasNewMail()
		{
			lock (_messages.SyncRoot)
			{
				foreach (MessageList messageList in _messages.Values)
				{
					if (messageList.MemberFolderID == (Int32) SystemFolders.Inbox && (messageList.StatusMask & MessageStatus.New) == MessageStatus.New)
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberFolderID"></param>
		/// <returns></returns>
		public Int32 GetUnreadCount(Int32 memberFolderID)
		{
			Int32 unreadCount = 0;

			lock (_messages.SyncRoot)
			{
				foreach (MessageList messageList in _messages.Values)
				{
					if (messageList.MemberFolderID == memberFolderID && (messageList.StatusMask & MessageStatus.Read) != MessageStatus.Read)
					{
						unreadCount++;
					}
				}
			}

			return unreadCount;
		}

		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get{return _memberID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 GroupID
		{
			get{return _groupID;}
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				return _referenceTracker;
			}
			set
			{
				_referenceTracker = value;
			}
		}

		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return GetCacheKey(_memberID, _groupID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 memberID, Int32 groupID)
		{
			return CACHE_KEY_PREFIX + ":" + memberID.ToString() + ":" + groupID.ToString();
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write(_groupID);
			bw.Write(_cacheTTLSeconds);
			bw.Write((Int32) _cachePriority);
			bw.Write((Int32) _cacheMode);

			bw.Write(_messages.Count);
			lock (_messages.SyncRoot)
			{
				foreach (MessageList messageList in _messages.Values)
				{
					bw.Write(messageList.ToByteArray());
				}
			}

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _messages.Values.GetEnumerator();
		}

		#endregion

		#region ICollection Members

		/// <summary>
		/// 
		/// </summary>
		public bool IsSynchronized
		{
			get
			{
				return _messages.Values.IsSynchronized;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Count
		{
			get
			{
				return _messages.Values.Count;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Array array, int index)
		{
			_messages.Values.CopyTo(array, index);
		}

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get
			{
				return _messages.Values.SyncRoot;
			}
		}

		#endregion
	}
}
