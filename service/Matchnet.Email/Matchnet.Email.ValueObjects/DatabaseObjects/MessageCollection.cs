using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageCollection :  IEnumerable, IValueObject, ISerializable, ICollection
	{
		#region Private Variables
		private Hashtable _messages;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public MessageCollection()
		{
			_messages = new Hashtable();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageCollection(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);

			Int32 messageCount = br.ReadInt32();

			_messages = new Hashtable(messageCount);
			
			for (Int32 i = 0; i < messageCount; i++)
			{
				Message message = new Message(br);
				Add(message);
			}
		}

		#endregion

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		public void Add(Message message)
		{
			lock (_messages.SyncRoot)
			{
				if (!_messages.Contains(message.MessageID))
				{
					_messages.Add(message.MessageID, message);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageID"></param>
		/// <returns></returns>
		public Message this[Int32 messageID]
		{
			get
			{
				Message message;
				lock (_messages.SyncRoot)
				{
					message = _messages[messageID] as Message;
				}
				return message;
			}
			set
			{
				lock (_messages.SyncRoot)
				{
					_messages[messageID] = value;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="messageID"></param>
		public void Remove(Int32 messageID)
		{
			lock (_messages.SyncRoot)
			{
				_messages.Remove(messageID);
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_messages.Count);
			lock (_messages.SyncRoot)
			{
				foreach (Message message in _messages.Values)
				{
					message.ToBytes(bw);
				}
			}

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _messages.Values.GetEnumerator();
		}

		#endregion

		#region ICollection Members

		/// <summary>
		/// 
		/// </summary>
		public bool IsSynchronized
		{
			get
			{
				return _messages.Values.IsSynchronized;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Count
		{
			get
			{
				return _messages.Values.Count;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Array array, int index)
		{
			_messages.Values.CopyTo(array, index);
		}

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get
			{
				return _messages.Values.SyncRoot;
			}
		}

		#endregion
	}
}
