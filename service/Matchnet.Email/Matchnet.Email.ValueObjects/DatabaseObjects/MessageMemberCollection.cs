using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;
using System.Collections.Generic;
using System.Linq;

namespace Matchnet.Email.ValueObjects
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class MessageMemberCollection : IEnumerable<MessageMember>, IEnumerable, IValueObject, ICacheable, ICollection<MessageMember>
    {
        #region Private Variables
        private List<MessageMember> _messageMembers;
        private object _messageMembersLock = new object();
        private Int32 _memberID;
        private Int32 _groupID;
        private MailType _mailType; //only avail if this list is specific for a mail type
        private ConversationType _conversationType = ConversationType.None; //logical groupings of various mails to support conversations
        private Int32 _totalCount = 0;
        private bool _filteredRemovedMembers = true;

        [NonSerialized]
        private ReferenceTracker _referenceTracker = new ReferenceTracker();

        private const string CACHE_KEY_PREFIX = "MessageMemberCollection";
        private Int32 _cacheTTLSeconds = 1200; //20 mins;
        private CacheItemMode _cacheMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private DateTime _cachedDate = DateTime.Now;
        
        #endregion

        #region Constructors
        public MessageMemberCollection(Int32 memberID, Int32 groupID)
        {
            _messageMembers = new List<MessageMember>();
            _memberID = memberID;
            _groupID = groupID;
            _mailType = MailType.None;
        }

        public MessageMemberCollection(Int32 memberID, Int32 groupID, MailType mailType, ConversationType conversationType)
        {
            _messageMembers = new List<MessageMember>();
            _memberID = memberID;
            _groupID = groupID;
            _mailType = mailType;
            _conversationType = conversationType;
        }

        public MessageMemberCollection(Int32 memberID, Int32 groupID, MailType mailType, ConversationType conversationType, bool filteredRemovedMembers)
        {
            _messageMembers = new List<MessageMember>();
            _memberID = memberID;
            _groupID = groupID;
            _mailType = mailType;
            _filteredRemovedMembers = filteredRemovedMembers;
            _conversationType = conversationType;

        }

        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageMember"></param>
        /// <returns></returns>
        public void Add(MessageMember messageMember)
        {
            _messageMembers.Add(messageMember);
        }

        public void ReplaceList(List<MessageMember> messageMembers)
        {
            _messageMembers = messageMembers;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageListID"></param>
        /// <returns></returns>
        public MessageMember this[int index]
        {
            get
            {
                if (_messageMembers != null && index < _messageMembers.Count)
                {
                    MessageMember message;
                    lock (_messageMembersLock)
                    {
                        message = _messageMembers[index];
                    }
                    return message;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                lock (_messageMembersLock)
                {
                    _messageMembers[index] = value;
                }
            }
        }

        public MessageMember GetMessageMemberByMessageListID(int messageListID)
        {
            if (this._messageMembers != null)
            {
                foreach (MessageMember m in this._messageMembers)
                {
                    if (m.MessageListID == messageListID)
                    {
                        return m;
                    }
                }
            }
            return null;
        }

        public MessageMember GetMessageMemberByTargetMemberID(int targetMemberID)
        {
            if (this._messageMembers != null)
            {
                foreach (MessageMember m in this._messageMembers)
                {
                    if (m.TargetMemberID == targetMemberID)
                    {
                        return m;
                    }
                }
            }
            return null;
        }

        public List<MessageMember> GetPage(int startRow, int pageSize)
        {
            int totalCount;
            return GetPage(startRow, pageSize, false, out totalCount);
        }

        public List<MessageMember> GetPage(int startRow, int pageSize, bool unreadOnly, out int totalCount)
        {
            totalCount = 0;

            startRow -= 1;  //convert from 1-based to 0-based
            if (startRow < 0)
            {
                startRow = 0;
            }

            if (pageSize <= 0)
            {
                pageSize = 1;
            }

            if (_messageMembers != null)
            {
                if (unreadOnly)
                {
                    List<MessageMember> unreadMemberList = new List<MessageMember>();
                    foreach (MessageMember mm in _messageMembers)
                    {
                        if (mm.UnreadMessageCount > 0)
                        {
                            unreadMemberList.Add(mm);
                        }
                    }

                    totalCount = unreadMemberList.Count;
                    if (startRow < unreadMemberList.Count)
                    {
                        return unreadMemberList.Skip(startRow).Take(pageSize).ToList();
                    }
                }
                else
                {
                    totalCount = _messageMembers.Count;
                    if (startRow < _messageMembers.Count)
                    {
                        return _messageMembers.Skip(startRow).Take(pageSize).ToList();
                    }
                }
            }

            return null;
        }

        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Int32 MemberID
        {
            get { return _memberID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 GroupID
        {
            get { return _groupID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ReferenceTracker ReferenceTracker
        {
            get
            {
                return _referenceTracker;
            }
            set
            {
                _referenceTracker = value;
            }
        }

        public int TotalCount
        {
            get
            {
                if (_totalCount <= 0 && _messageMembers != null)
                {
                    _totalCount = _messageMembers.Count;
                }

                return _totalCount;
            }
            set
            {
                _totalCount = value;
            }
        }

        public bool FilteredRemovedMembers
        {
            get { return _filteredRemovedMembers; }
            set { _filteredRemovedMembers = value; }
        }

        public DateTime CachedDate
        {
            get { return _cachedDate; }
            set { _cachedDate = value; }
        }

        public ConversationType ConversationType
        {
            get { return _conversationType; }
            set { _conversationType = value; }
        }

        #endregion

        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return GetCacheKey(_memberID, _groupID, _mailType, _conversationType, _filteredRemovedMembers);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static string GetCacheKey(Int32 memberID, Int32 groupID, MailType mailType, ConversationType conversationType, bool filteredRemoveMembers)
        {
            return CACHE_KEY_PREFIX + string.Format(":{0}:{1}:{2}:{3}:{4}", memberID, groupID, ((int)mailType).ToString(), ((int)conversationType).ToString(), filteredRemoveMembers);
        }

        public static string GetCacheKey(Int32 memberID, Int32 groupID, MailType mailType, ConversationType conversationType)
        {
            return GetCacheKey(memberID, groupID, mailType, conversationType, true);
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _messageMembers.GetEnumerator();
        }

        public virtual IEnumerator<MessageMember> GetEnumerator()
        {
            return _messageMembers.GetEnumerator();
        }

        #endregion

        #region ICollection Members
        public virtual bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool Remove(MessageMember messageMember)
        {
            lock (_messageMembersLock)
            {
                return _messageMembers.Remove(messageMember);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Count
        {
            get
            {
                if (_messageMembers != null)
                {
                    return _messageMembers.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public virtual void CopyTo(MessageMember[] array, int index)
        {
            if (_messageMembers != null)
                _messageMembers.CopyTo(array, index);
        }

        public bool Contains(MessageMember messageMember)
        {
            if (_messageMembers != null)
            {
                foreach (MessageMember obj in _messageMembers)
                {
                    if ((obj.MemberID == messageMember.MemberID) && (obj.TargetMemberID == messageMember.TargetMemberID) && (obj.MailTypeID == messageMember.MailTypeID))
                    {
                        //if it matches return true
                        return true;
                    }
                }
            }
            //no match
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        public object SyncRoot
        {
            get
            {
                return _messageMembersLock;
            }
        }

        public virtual void Clear()
        {
            if (_messageMembers != null)
                _messageMembers.Clear();
        }


        #endregion


    }
}
