using System;
using System.Runtime.Serialization;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for SendMesageResult.
	/// </summary>
	[Serializable()]
	public class MessageSaveResult
	{
		private MessageSaveStatus _status;
		private MessageSaveError _messageSaveError;
		private Int32 _messageID = Constants.NULL_INT;
		private Int32 _messageListID = Constants.NULL_INT;

		#region Constructors
		/// <summary>
		/// Overloaded successful SaveMessageSaveResult with given messageID
		/// </summary>
		/// <param name="messageID"></param>
		/// <param name="messageListID"></param>
		public MessageSaveResult(Int32 messageID, Int32 messageListID) 
		{
			_status = MessageSaveStatus.Success;
			_messageID = messageID;
			_messageListID = messageListID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="error"></param>
		public MessageSaveResult(MessageSaveError error)
		{
			_status = MessageSaveStatus.Failed;
			_messageSaveError = error;
		}
		#endregion

		#region Properties

		/// <summary>
		/// MessageID of saved message
		/// </summary>
		public Int32 MessageID 
		{
			get 
			{
				return _messageID;
			}
		}

		/// <summary>
		/// MessageListID of saved message
		/// </summary>
		public Int32 MessageListID
		{
			get
			{
				return _messageListID;
			}
		}

		/// <summary>
		/// Indicates the status (success/fail) of this validation result.
		/// </summary>
		public MessageSaveStatus Status
		{
			get
			{
				return _status;
			}
		}

		/// <summary>
		/// Indicates the validation error for this validation result.
		/// </summary>
		public MessageSaveError Error
		{
			get
			{
				return _messageSaveError;
			}
		}
		#endregion
	}

	#region Enumerations
	/// <summary>
	/// Status of email message send result (Success / Fail).
	/// </summary>
	public enum MessageSaveStatus
	{
		/// <summary>
		/// Indicates successful sending of email message.
		/// </summary>
		Success,
		/// <summary>
		/// Indicates failure sending email message.
		/// </summary>
		Failed
	}

	/// <summary>
	/// Message Save Error type for failed email attempts.
	/// </summary>
	public enum MessageSaveError
	{
		/// <summary>
		/// Indicates that the MessageListID used to save this draft does not correspond
		/// to an existing message in the member's Drafts folder.  This only applies
		/// when saving an existing draft.
		/// </summary>
		InvalidMessageListID,
		/// <summary>
		/// 
		/// </summary>
		Unspecified
	}
	#endregion
}