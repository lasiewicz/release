﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Email.ValueObjects
{
    public class MessageAttachmentMetadata
    {
        public int AttachmentID { get; set; }
        public string FileExtension { get; set; }
        public int AttachmentTypeID { get; set; }
    }
}
