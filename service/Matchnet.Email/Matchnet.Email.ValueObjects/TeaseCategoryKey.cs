using System;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeaseCategoryKey.
	/// </summary>
	public class TeaseCategoryKey
	{
		private const string CACHE_KEY_PREFIX = "~TEASECATEGORIES^{0}";

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int groupID)
		{
			return string.Format(CACHE_KEY_PREFIX, groupID);
		}

		private TeaseCategoryKey()
		{
		}
	}
}
