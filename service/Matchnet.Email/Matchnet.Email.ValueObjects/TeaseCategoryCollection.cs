using System;
using System.Collections;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for TeaseCategoryCollection.
	/// </summary>
	[Serializable()] public class TeaseCategoryCollection : CollectionBase, ICacheable, IReplicable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private bool isCached = false;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public TeaseCategoryCollection()
		{
			isCached = false;
		}

		/// <summary>
		/// 
		/// </summary>
		public bool CachedResult 
		{
			set
			{
				isCached = value;
			}
			get
			{ 
				return isCached;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="teaseCategory"></param>
		public void Add(TeaseCategory teaseCategory)
		{
			base.InnerList.Add(teaseCategory);
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}
		#endregion

		#region IReplicable Members

		/// <summary>
		/// Returns a replication placeholder initialized with the CacheKey of this item.
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
