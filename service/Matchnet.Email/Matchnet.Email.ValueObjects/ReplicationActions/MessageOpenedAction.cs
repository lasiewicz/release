using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageOpenedAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private Int32 _targetMemberID;
		private Int32 _messageID;
		private DateTime _openDate;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="messageID"></param>
		/// <param name="openDate"></param>
		public MessageOpenedAction(Int32 memberID, Int32 targetMemberID, Int32 messageID, DateTime openDate)
		{
			_memberID = memberID;
			_targetMemberID = targetMemberID;
			_messageID = messageID;
			_openDate = openDate;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public Int32 TargetMemberID
		{
			get
			{
				return _targetMemberID;
			}
		}

		/// <summary> </summary>
		public Int32 MessageID
		{
			get
			{
				return _messageID;
			}
		}

		/// <summary> </summary>
		public DateTime OpenDate
		{
			get
			{
				return _openDate;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageOpenedAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_targetMemberID = br.ReadInt32();
			_messageID = br.ReadInt32();
			_openDate = new DateTime(br.ReadInt64());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream(20);
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write(_targetMemberID);
			bw.Write(_messageID);
			bw.Write(_openDate.Ticks);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}
