using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageSaveAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private MessageSaveType _saveType;
		private Message _message;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="saveType"></param>
		/// <param name="message"></param>
		public MessageSaveAction(Int32 memberID, MessageSaveType saveType, Message message)
		{
			_memberID = memberID;
			_saveType = saveType;
			_message = message;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public MessageSaveType SaveType
		{
			get
			{
				return _saveType;
			}
		}

		/// <summary> </summary>
		public Message Message
		{
			get
			{
				return _message;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageSaveAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_saveType = (MessageSaveType) br.ReadInt32();
			_message = new Message(br);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream(8 + _message.GetByteCount());
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write((Int32) _saveType);
			_message.ToBytes(bw);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}