using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageReadAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private Int32 _groupID;
		private Int32 _memberMailID;
		private bool _read;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <param name="memberMailID"></param>
		/// <param name="read"></param>
		public MessageReadAction(Int32 memberID, Int32 groupID, Int32 memberMailID, bool read)
		{
			_memberID = memberID;
			_groupID = groupID;
			_memberMailID = memberMailID;
			_read = read;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		/// <summary> </summary>
		public Int32 MemberMailID
		{
			get
			{
				return _memberMailID;
			}
		}

		/// <summary> </summary>
		public bool Read
		{
			get
			{
				return _read;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageReadAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_groupID = br.ReadInt32();
			_memberMailID = br.ReadInt32();
			_read = br.ReadBoolean();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write(_groupID);
			bw.Write(_memberMailID);
			bw.Write(_read);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}
