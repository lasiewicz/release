using System;
using System.IO;
using System.Runtime.Serialization;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for MessageSendAction.
	/// </summary>
	[Serializable]
	public class MessageSendAction : IReplicationAction, ISerializable
	{
		private Int32 _fromMemberMailID;
		private Int32 _toMemberMailID;
		private Int32 _mailID;
		private MailType _mailType;
		private Int32 _fromMemberID;
		private Int32 _toMemberID;
		private Int32 _groupID;
		private string _subject;
		private string _content;
		private Int32 _replyToMemberMailID;
		private DateTime _insertDate;
		private bool _saveInSent;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fromMemberMailID"></param>
		/// <param name="toMemberMailID"></param>
		/// <param name="mailID"></param>
		/// <param name="mailType"></param>
		/// <param name="fromMemberID"></param>
		/// <param name="toMemberID"></param>
		/// <param name="groupID"></param>
		/// <param name="subject"></param>
		/// <param name="content"></param>
		/// <param name="replyToMemberMailID"></param>
		/// <param name="insertDate"></param>
		/// <param name="saveInSent"></param>
		public MessageSendAction(Int32 fromMemberMailID, Int32 toMemberMailID, Int32 mailID, MailType mailType, Int32 fromMemberID, Int32 toMemberID, Int32 groupID, string subject, string content, Int32 replyToMemberMailID, DateTime insertDate, bool saveInSent)
		{
			_fromMemberMailID = fromMemberMailID;
			_toMemberMailID = toMemberMailID;
			_mailID = mailID;
			_mailType = mailType;
			_fromMemberID = fromMemberID;
			_toMemberID = toMemberID;
			_groupID = groupID;
			_subject = subject;
			_content = content;
			_replyToMemberMailID = replyToMemberMailID;
			_insertDate = insertDate;
			_saveInSent = saveInSent;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 FromMemberMailID
		{
			get
			{
				return _fromMemberMailID;
			}
		}

		/// <summary> </summary>
		public Int32 ToMemberMailID
		{
			get
			{
				return _toMemberMailID;
			}
		}

		/// <summary> </summary>
		public Int32 MailID
		{
			get
			{
				return _mailID;
			}
		}

		/// <summary> </summary>
		public MailType MailType
		{
			get
			{
				return _mailType;
			}
		}

		/// <summary> </summary>
		public Int32 FromMemberID
		{
			get
			{
				return _fromMemberID;
			}
		}

		/// <summary> </summary>
		public Int32 ToMemberID
		{
			get
			{
				return _toMemberID;
			}
		}

		/// <summary> </summary>
		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		/// <summary> </summary>
		public string Subject
		{
			get
			{
				return _subject;
			}
		}

		/// <summary> </summary>
		public string Content
		{
			get
			{
				return _content;
			}
		}

		/// <summary> </summary>
		public Int32 ReplyToMemberMailID
		{
			get
			{
				return _replyToMemberMailID;
			}
		}

		/// <summary> </summary>
		public DateTime InsertDate
		{
			get
			{
				return _insertDate;
			}
		}

		/// <summary> </summary>
		public bool SaveInSent
		{
			get
			{
				return _saveInSent;
			}
		}
		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageSendAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_fromMemberMailID = br.ReadInt32();
			_toMemberMailID = br.ReadInt32();
			_mailID = br.ReadInt32();
			_mailType = (MailType) br.ReadInt32();
			_fromMemberID = br.ReadInt32();
			_toMemberID = br.ReadInt32();
			_groupID = br.ReadInt32();
			_subject = br.ReadString();
			_content = br.ReadString();
			_replyToMemberMailID = br.ReadInt32();
			_insertDate = new DateTime(br.ReadInt64());
			_saveInSent = br.ReadBoolean();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_fromMemberMailID);
			bw.Write(_toMemberMailID);
			bw.Write(_mailID);
			bw.Write((Int32) _mailType);
			bw.Write(_fromMemberID);
			bw.Write(_toMemberID);
			bw.Write(_groupID);
			bw.Write(_subject);
			bw.Write(_content);
			bw.Write(_replyToMemberMailID);
			bw.Write(_insertDate.Ticks);
			bw.Write(_saveInSent);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}
