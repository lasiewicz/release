using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageListDeleteAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private Int32 _groupID;
		private Int32 _messageListID;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <param name="messageListID"></param>
		public MessageListDeleteAction(Int32 memberID, Int32 groupID, Int32 messageListID)
		{
			_memberID = memberID;
			_groupID = groupID;
			_messageListID = messageListID;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		/// <summary> </summary>
		public Int32 MessageListID
		{
			get
			{
				return _messageListID;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageListDeleteAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_groupID = br.ReadInt32();
			_messageListID = br.ReadInt32();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream(12);
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write(_groupID);
			bw.Write(_messageListID);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}
