using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class FolderDeleteAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private Int32 _groupID;
		private Int32 _memberFolderID;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <param name="memberFolderID"></param>
		public FolderDeleteAction(Int32 memberID, Int32 groupID, Int32 memberFolderID)
		{
			_memberID = memberID;
			_groupID = groupID;
			_memberFolderID = memberFolderID;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}

		/// <summary> </summary>
		public Int32 MemberFolderID
		{
			get
			{
				return _memberFolderID;
			}
		}
		
		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected FolderDeleteAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_groupID = br.ReadInt32();
			_memberFolderID = br.ReadInt32();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write(_groupID);
			bw.Write(_memberFolderID);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}