using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for ReplicationActionCollection.
	/// </summary>
	[Serializable]
	public class ReplicationActionCollection : IReplicationAction, ISerializable, IEnumerable
	{
		private ArrayList _actions;

		/// <summary>
		/// 
		/// </summary>
		public ReplicationActionCollection()
		{
			_actions = new ArrayList();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="replicationAction"></param>
		public void Add(IReplicationAction replicationAction)
		{
			if (replicationAction != null)
			{
				_actions.Add(replicationAction);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 Count
		{
			get
			{
				return _actions.Count;
			}
		}


		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReplicationActionCollection(SerializationInfo info, StreamingContext context)
		{
			_actions = (ArrayList) info.GetValue("actions", typeof(ArrayList));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("actions", _actions);
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _actions.GetEnumerator();
		}

		#endregion
	}
}
