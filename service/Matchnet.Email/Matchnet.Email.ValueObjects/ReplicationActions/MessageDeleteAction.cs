using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageDeleteAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private Int32 _targetMemberID;
		private Int32 _messageID;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="messageID"></param>
		public MessageDeleteAction(Int32 memberID, Int32 targetMemberID, Int32 messageID)
		{
			_memberID = memberID;
			_targetMemberID = targetMemberID;
			_messageID = messageID;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public Int32 TargetMemberID
		{
			get
			{
				return _targetMemberID;
			}
		}

		/// <summary> </summary>
		public Int32 MessageID
		{
			get
			{
				return _messageID;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageDeleteAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_targetMemberID = br.ReadInt32();
			_messageID = br.ReadInt32();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream(12);
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write(_targetMemberID);
			bw.Write(_messageID);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}
