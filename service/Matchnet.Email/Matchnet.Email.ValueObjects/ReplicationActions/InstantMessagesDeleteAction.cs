﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
    [Serializable]
    public class InstantMessagesDeleteAction : IReplicationAction
    {
        public int MemberID { get; set; }
        public int TargetMemberID { get; set; }
        public int MessageListID { get; set; }
        public int GroupID { get; set; }

        public InstantMessagesDeleteAction(int memberID, int targetMemberID, int messageListID, int groupID)
        {
            MemberID = memberID;
            TargetMemberID = targetMemberID;
            MessageListID = messageListID;
            GroupID = groupID;
        }
    }
}
