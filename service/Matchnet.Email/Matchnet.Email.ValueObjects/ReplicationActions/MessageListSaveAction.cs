using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Email.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MessageListSaveAction : IReplicationAction, ISerializable
	{
		private Int32 _memberID;
		private MessageListSaveType _saveType;
		private MessageList _messageList;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="saveType"></param>
		/// <param name="MessageList"></param>
		public MessageListSaveAction(Int32 memberID, MessageListSaveType saveType, MessageList MessageList)
		{
			_memberID = memberID;
			_saveType = saveType;
			_messageList = MessageList;
		}

		#region Properties

		/// <summary> </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary> </summary>
		public MessageListSaveType SaveType
		{
			get
			{
				return _saveType;
			}
		}

		/// <summary> </summary>
		public MessageList MessageList
		{
			get
			{
				return _messageList;
			}
		}

		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageListSaveAction(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_memberID = br.ReadInt32();
			_saveType = (MessageListSaveType) br.ReadInt32();
			_messageList = new MessageList(br);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream(8 + _messageList.GetByteCount());
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_memberID);
			bw.Write((Int32) _saveType);
			_messageList.ToBytes(bw);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}