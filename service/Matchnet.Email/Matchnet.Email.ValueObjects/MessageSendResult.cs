using System;
using System.Runtime.Serialization;

namespace Matchnet.Email.ValueObjects
{
	/// <summary>
	/// Summary description for SendMesageResult.
	/// </summary>
	[Serializable]
	public class MessageSendResult
	{
		private MessageSendStatus _status;
		private MessageSendError _messageSendError;
		private Int32 _messageID = Constants.NULL_INT;
		private Int32 _senderMessageListID = Constants.NULL_INT;
		private Int32 _recipientMessageListID = Constants.NULL_INT;
		private Int32 _recipientUnreadCount = 0;
        private MailOption _mailOptions;

		[NonSerialized]
		private EmailMessage _emailMessage;

		#region Constructors

		/// <summary>
		/// Successful MessageSendResult
		/// </summary>
		/// <param name="messageID"></param>
		/// <param name="senderMessageListID"></param>
		/// <param name="recipientMessageListID"></param>
		/// <param name="recipientUnreadCount"></param>
		public MessageSendResult(Int32 messageID, Int32 senderMessageListID, Int32 recipientMessageListID, Int32 recipientUnreadCount, MailOption mailOptions)
		{
			_status = MessageSendStatus.Success;
			_messageID = messageID;
			_senderMessageListID = senderMessageListID;
			_recipientMessageListID = recipientMessageListID;
			_recipientUnreadCount = recipientUnreadCount;
            _mailOptions = mailOptions;
		}

		/// <summary>
		/// Unsuccessful MessageSendResult
		/// </summary>
		/// <param name="error"></param>
		public MessageSendResult(MessageSendError error)
		{
			_status = MessageSendStatus.Failed;
			_messageSendError = error;
		}
		#endregion

		#region Properties

        public MailOption MailOptions
        {
            get { return _mailOptions; }
        }

		/// <summary>
		/// MessageID of sent message
		/// </summary>
		public Int32 MessageID 
		{
			get 
			{
				return _messageID;
			}
		}

		/// <summary>
		/// MessageListID of sender's copy of message
		/// </summary>
		public Int32 SenderMessageListID
		{
			get
			{
				return _senderMessageListID;
			}
		}

		/// <summary>
		/// MessageListID of recipient's copy of message
		/// </summary>
		public Int32 RecipientMessageListID
		{
			get
			{
				return _recipientMessageListID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 RecipientUnreadCount
		{
			get
			{
				return _recipientUnreadCount;
			}
		}

		/// <summary>
		/// Indicates the status (success/fail) of this validation result.
		/// </summary>
		public MessageSendStatus Status
		{
			get
			{
				return _status;
			}
		}

		/// <summary>
		/// Indicates the validation error for this validation result.
		/// </summary>
		public MessageSendError Error
		{
			get
			{
				return _messageSendError;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public EmailMessage EmailMessage
		{
			get
			{
				return _emailMessage;
			}
			set
			{
				_emailMessage = value;
			}
		}
		#endregion
	}

	#region Enumerations
	/// <summary>
	/// Status of email message send result (Success / Fail).
	/// </summary>
	public enum MessageSendStatus
	{
		/// <summary>
		/// Indicates successful sending of email message.
		/// </summary>
		Success,
		/// <summary>
		/// Indicates failure sending email message.
		/// </summary>
		Failed
	}

	/// <summary>
	/// Message Send Error type for failed email attempts.
	/// </summary>
	public enum MessageSendError
	{
		/// <summary>
		/// Indicates that the sender has reached the compose limit for a specified time period.
		/// (i.e. Members may be limited to sending 30 emails in any 24-hour period.)
		/// </summary>
		ComposeLimitReached,
		/// <summary>
		/// Indicates that the ReplyMessageListID specified by the caller was not a message belonging to the sender
		/// or that the intended recipient of the message did not match the sender of the message being replied to.
		/// </summary>
		InvalidReplyMessageListID,
		/// <summary>
		/// 
		/// </summary>
		Unspecified,
        MissingThreadID
	}
	#endregion
}