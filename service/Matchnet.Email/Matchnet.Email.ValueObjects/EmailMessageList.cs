﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Email.ValueObjects
{
    /// <summary>
    /// Threaded conversation messages between 2 members, but could be used to replace EmailMessageCollection to be general list for a member if we don't use TargetMemberID
    /// </summary>
    [Serializable]
    public class EmailMessageList : IEnumerable<EmailMessage>, IEnumerable, IValueObject, ICollection<EmailMessage>, ICacheable
    {
        #region Private Variables
        private List<EmailMessage> _messages = new List<EmailMessage>();
        private Dictionary<string, List<EmailMessage>> _threadedMessages = new Dictionary<string, List<EmailMessage>>();
        private object _messagesLock = new object();
        private Int32 _memberID;
        private Int32 _targetMemberID; //only avail if this list is specific between 2 members (used for consolidating inbox to messages between 2 members)
        private Int32 _groupID; //community id
        private MailType _mailType = MailType.None; //only avail if this list is specific for a mail type
        private ConversationType _conversationType = ConversationType.None; //logical groupings of various mails to support conversations

        private const string CACHE_KEY_PREFIX = "EmailMessageList";
        private Int32 _cacheTTLSeconds = 1200; //20 mins;
        private CacheItemMode _cacheMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private DateTime _cachedDate = DateTime.Now;
        private Int32 _totalCount = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public EmailMessageList(Int32 memberID, Int32 groupID)
        {
            _memberID = memberID;
            _groupID = groupID;
            _targetMemberID = 0;
        }

        public EmailMessageList(Int32 memberID, Int32 targetMemberID, Int32 groupID, ConversationType conversationType, MailType mailType)
        {
            _memberID = memberID;
            _groupID = groupID;
            _targetMemberID = targetMemberID;
            _conversationType = conversationType;
            _mailType = mailType;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailMessage"></param>
        /// <returns></returns>
        public void Add(EmailMessage emailMessage)
        {
            lock (_messagesLock)
            {
                _messages.Add(emailMessage);
                if (!string.IsNullOrEmpty(emailMessage.ThreadID))
                {
                    if (!_threadedMessages.ContainsKey(emailMessage.ThreadID))
                    {
                        _threadedMessages[emailMessage.ThreadID] = new List<EmailMessage>();
                    }
                    _threadedMessages[emailMessage.ThreadID].Add(emailMessage);
                }
            }
        }

        public void ReplaceList(List<EmailMessage> emailMessages)
        {
            _messages = emailMessages;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberMailID"></param>
        /// <returns></returns>
        public EmailMessage this[Int32 index]
        {
            get
            {
                if (_messages != null && index < _messages.Count)
                {
                    EmailMessage message;
                    lock (_messagesLock)
                    {
                        message = _messages[index];
                    }
                    return message;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                lock (_messagesLock)
                {
                    _messages[index] = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public List<EmailMessage> Sort(EmailComparerGeneric comparer)
        {
            if (_messages != null)
            {
                lock (_messagesLock)
                {
                    _messages.Sort(comparer);
                }

                return _messages.ToList();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool HasNewMail()
        {
            if (_messages != null)
            {
                lock (_messagesLock)
                {
                    foreach (EmailMessage message in _messages)
                    {
                        if ((message.StatusMask & MessageStatus.New) == MessageStatus.New)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetDisplayKBytes()
        {
            int displayKBytes = 0;
            if (_messages != null)
            {
                lock (_messagesLock)
                {
                    foreach (EmailMessage message in _messages)
                    {
                        displayKBytes += message.DisplayKBytes;
                    }
                }
            }

            return displayKBytes;
        }

        /// <summary>
        /// Determines whether usernames have been populated for the EmailMessage objects in this collection.
        /// </summary>
        /// <returns></returns>
        public bool HasUsernames()
        {
            if (_messages != null)
            {
                lock (_messagesLock)
                {
                    foreach (EmailMessage message in _messages)
                    {
                        if (message.FromUserName == null)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public List<EmailMessage> GetPage(int startRow, int pageSize)
        {
            int totalCount;
            return GetPage(startRow, pageSize, "", false, out totalCount);
        }

        public List<EmailMessage> GetPage(int startRow, int pageSize, string threadID, bool unreadOnly, out int totalCount)
        {
            totalCount = 0;

            startRow -= 1;  //convert from 1-based to 0-based
            if (startRow < 0)
            {
                startRow = 0;
            }

            if (pageSize <= 0)
            {
                pageSize = 1;
            }

            if (string.IsNullOrEmpty(threadID))
            {
                totalCount = _messages.Count;
                if (_messages != null && (startRow < _messages.Count))
                {
                    return _messages.Skip(startRow).Take(pageSize).ToList();
                }
            }
            else
            {
                if (_threadedMessages != null && _threadedMessages.ContainsKey(threadID))
                {
                    totalCount = _threadedMessages[threadID].Count;
                    if (startRow < _threadedMessages.Count)
                    {
                        return _threadedMessages[threadID].Skip(startRow).Take(pageSize).ToList();
                    }
                }
            }

            return null;
        }

        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Int32 MemberID
        {
            get { return _memberID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 TargetMemberID
        {
            get { return _targetMemberID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 GroupID
        {
            get { return _groupID; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ConversationType conversationType
        {
            get { return _conversationType; }
            set { _conversationType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MailType MailType
        {
            get
            {
                return _mailType;
            }
            set
            {
                _mailType = value;
            }
        }

        public int TotalCount
        {
            get
            {
                if (_totalCount <= 0 && _messages != null)
                {
                    _totalCount = _messages.Count;
                }

                return _totalCount;
            }
            set
            {
                _totalCount = value;
            }
        }

        public DateTime CachedDate
        {
            get { return _cachedDate; }
            set { _cachedDate = value; }
        }
        #endregion

        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return GetCacheKey(_memberID, _targetMemberID, _groupID, _conversationType, _mailType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static string GetCacheKey(Int32 memberID, Int32 targetMemberID, Int32 groupID, ConversationType conversationType, MailType mailType)
        {
            return CACHE_KEY_PREFIX + string.Format(":{0}:{1}:{2}:{3}:{4}", memberID, targetMemberID, groupID, ((int)conversationType).ToString(), ((int)mailType).ToString());
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _messages.GetEnumerator();
        }

        public virtual IEnumerator<EmailMessage> GetEnumerator()
        {
            return _messages.GetEnumerator();
        }

        #endregion

        #region ICollection Members
        public virtual bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberMailID"></param>
        public virtual bool Remove(EmailMessage emailMessage)
        {
            lock (_messagesLock)
            {
                return _messages.Remove(emailMessage);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Count
        {
            get
            {
                if (_messages != null)
                {
                    return _messages.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int UnreadCount
        {
            get
            {
                Int32 count = 0;
                if (_messages != null)
                {
                    foreach (EmailMessage message in _messages)
                    {
                        if ((message.StatusMask & MessageStatus.Read) != MessageStatus.Read)
                        {
                            count++;
                        }
                    }
                }

                return count;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public virtual void CopyTo(EmailMessage[] array, int index)
        {
            if (_messages != null)
                _messages.CopyTo(array, index);
        }

        public bool Contains(EmailMessage emailMessage)
        {
            if (_messages != null)
            {
                foreach (EmailMessage obj in _messages)
                {
                    if (obj.MemberMailID == emailMessage.MemberMailID)
                    {
                        //if it matches return true
                        return true;
                    }
                }
            }
            //no match
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        public object SyncRoot
        {
            get
            {
                return _messagesLock;
            }
        }

        public virtual void Clear()
        {
            if (_messages != null)
                _messages.Clear();
        }


        #endregion
    }
}
