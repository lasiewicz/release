using System;

using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

namespace Matchnet.Email.ServiceManagers
{
	/// <summary>
	/// Summary description for EmailLLogSM.
	/// </summary>
	public class EmailLogSM : MarshalByRefObject, IServiceManager, IEmailLogService, IDisposable
	{
		private const string SERVICE_MANAGER_NAME = "EmailLogSM";
		public const string SERVICE_NAME = "Matchnet.Email.Service";
		
		public EmailLogSM()
		{
		}

		public MemberMailLog RetrieveEmailLog(Int32 memberID, Int32 groupID) 
		{
			try 
			{
				return EmailLogBL.Instance.RetrieveEmailLog(memberID, groupID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving email log.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving email log.", ex);
			}	
		}

        public DateTime? RetrieveEmailLastSentDate(Int32 memberID, Int32 groupID)
        {
            try
            {
                return EmailLogBL.Instance.RetrieveEmailLastSentDate(memberID, groupID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving last sent date.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving last sent date.", ex);
            }
        }

		public override object InitializeLifetimeService()
		{
			return null;
		}
		
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#region IDisposable Members

		public void Dispose()
		{
			// TODO:  Add EmailLLogSM.Dispose implementation
		}

		#endregion
	}
}
