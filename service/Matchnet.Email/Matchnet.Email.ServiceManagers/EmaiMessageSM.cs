using System;
using System.Collections;
using System.Diagnostics;

using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceDefinitions;
using Matchnet.Replication;
using System.Runtime.Remoting.Messaging;

namespace Matchnet.Email.ServiceManagers
{
	/// <summary>
	/// 
	/// </summary>
	public class EmailMessageSM : MarshalByRefObject, IServiceManager, IEmailMessageService, IDisposable, IReplicationActionRecipient
	{
		#region Private Variables
		private const string SERVICE_MANAGER_NAME = "EmailMessageSM";
		private HydraWriter _hydraWriter;

		private Replicator _replicator;
		private Synchronizer _synchronizer;

		private PerformanceCounter _messageSavedCount;
		private PerformanceCounter _messageSendCount;
		private PerformanceCounter _emailSendCount;
		private PerformanceCounter _flirtSendCount;
		private PerformanceCounter _missedIMSendCount;

		private PerformanceCounter _messageListRetrievedCount;
		private PerformanceCounter _messageListHitRate;
		private PerformanceCounter _messageListHitRate_Base;

		private PerformanceCounter _messageRetrievedCount;
		private PerformanceCounter _messageHitRate;
		private PerformanceCounter _messageHitRate_Base;

		private PerformanceCounter _messageContentRetrievedCount;
		private PerformanceCounter _messageContentHitRate;
		private PerformanceCounter _messageContentHitRate_Base;

		private PerformanceCounter _messageListsCached;
		private PerformanceCounter _messagesCached;

		#endregion

		#region Constructors
		public EmailMessageSM()
		{
			_hydraWriter = new HydraWriter(new string[]{"mnIMailNew", "mnMailLog"});
			_hydraWriter.Start();

			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_MESSAGE_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
			}
			_replicator = new Replicator(SERVICE_MANAGER_NAME);
			_replicator.SetDestinationUri(replicationURI);
			_replicator.Start();

			_synchronizer = new Synchronizer(ServiceConstants.SERVICE_NAME);
			_synchronizer.Start();

			//EmailMessageBL.Instance.ReplicationRequested += new Matchnet.Email.BusinessLogic.EmailMessageBL.ReplicationEventHandler(EmailMessageBL_ReplicationRequested);
			EmailMessageBL.Instance.ReplicationActionRequested += new Matchnet.Email.BusinessLogic.EmailMessageBL.ReplicationActionEventHandler(EmailMessageBL_ReplicationActionRequested);
			EmailMessageBL.Instance.SynchronizationRequested += new Matchnet.Email.BusinessLogic.EmailMessageBL.SynchronizationEventHandler(EmailMessageBL_SynchronizationRequested);

			initPerfCounters();
			EmailMessageBL.Instance.MessageSaved += new EmailMessageBL.MessageSavedEventHandler(EmailMessageBL_MessageSaved);
			EmailMessageBL.Instance.MessageSent += new Matchnet.Email.BusinessLogic.EmailMessageBL.MessageSentEventHandler(EmailMessageBL_MessageSent);
			EmailMessageBL.Instance.MessageListRetrieved += new EmailMessageBL.MessageListRetrievedEventHandler(EmailMessageBL_MessageListRetrieved);
			EmailMessageBL.Instance.MessageRetrieved += new EmailMessageBL.MessageRetrievedEventHandler(EmailMessageBL_MessageRetrieved);
			EmailMessageBL.Instance.MessageRemoved += new Matchnet.Email.BusinessLogic.EmailMessageBL.MessageRemovedEventHandler(EmailMessageBL_MessageRemoved);
			EmailMessageBL.Instance.MessageListRemoved += new Matchnet.Email.BusinessLogic.EmailMessageBL.MessageListRemovedEventHandler(EmailMessageBL_MessageListRemoved);
		}

		#endregion

		#region IEmailMessageService Implementation

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="messageSave"></param>
		/// <returns></returns>
		public MessageSaveResult SaveMessage(string clientHostName, MessageSave messageSave)
		{
			return EmailMessageBL.Instance.SaveMessage(clientHostName, messageSave);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="messageSave"></param>
		/// <param name="saveInSent"></param>
		/// <param name="replyMessageListID"></param>
		/// <returns></returns>
		public MessageSendResult SendMessage(string clientHostName, MessageSend messageSend, bool saveInSent, Int32 replyMessageListID, bool sender, bool recipient)
		{
			return EmailMessageBL.Instance.SendMessage(clientHostName, messageSend, saveInSent, replyMessageListID, sender, recipient);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public MessageListCollection RetrieveMessageList(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID)
		{
			return EmailMessageBL.Instance.RetrieveMessageList(clientHostName, cacheReference, memberID, groupID);
		}

        /// <summary>
        /// This should be used only when you need to operate on stuff that's not in memory
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public MessageListCollection RetrieveMessageListFromDB(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID)
        {
            return EmailMessageBL.Instance.RetrieveMessageListFromDB(clientHostName, cacheReference, memberID, groupID);
        }


        /// <summary>
        /// This should be used only when you need to operate on stuff that's not in memory
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public MessageMemberCollection RetrieveMessageMemberFromDB(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 groupID)
        {
            return EmailMessageBL.Instance.RetrieveMessageMemberFromDB(clientHostName, cacheReference, memberID, groupID);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="messageID"></param>
		/// <param name="withContent"></param>
		/// <returns></returns>
		public Message RetrieveMessage(string clientHostName, CacheReference cacheReference, Int32 memberID, Int32 messageID, bool withContent)
		{
			return EmailMessageBL.Instance.RetrieveMessage(clientHostName, cacheReference, memberID, messageID, withContent);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="messageIDs"></param>
		/// <param name="withContent"></param>
		/// <returns></returns>
		public MessageCollection RetrieveMessages(string clientHostName, CacheReference cacheReference, Int32 memberID, ArrayList messageIDs, bool withContent)
		{
			return EmailMessageBL.Instance.RetrieveMessages(clientHostName, cacheReference, memberID, messageIDs, withContent);
		}

        [OneWay]
		public void PlayAction(string clientHostName, IReplicationAction replicationAction)
		{
			EmailMessageBL.Instance.PlayReplicationAction(clientHostName, replicationAction, true, true);
		}

        public MessageAttachmentSaveResult SaveMessageAttachment(int fromMemberID, int toMemberID, int fileID, string previewCloudPath, string originalCloudPath,
                                                                 AttachmentType attachmentType, int messageID,
                                                                 int communityID,
                                                                 int siteID, int appID)
        {
            MessageAttachmentSaveResult saveResult = null;
            try
            {
                saveResult = EmailMessageBL.Instance.SaveMessageAttachment(fromMemberID, toMemberID, fileID, previewCloudPath, originalCloudPath,
                                                                     attachmentType, messageID,
                                                                     communityID, siteID, appID);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                             "SaveMessageAttachment Error: " + ex.Message, ex);

                saveResult = new MessageAttachmentSaveResult();
                saveResult.Status = MessageAttachmentSaveStatus.Failed;
                saveResult.Error = MessageAttachmentSaveError.SaveAttachmentRecordError;
            }

            return saveResult;
        }

        public EmailMessageList RetrieveInstantMessages(int memberID, int targetMemberID, int groupID)
        {
            return EmailMessageBL.Instance.RetrieveInstantMessages(memberID, targetMemberID, groupID);
        }

        public MessageMemberCollection RetrieveInstantMessageMembers(int memberID, int groupID, bool filterRemovedMembers)
        {
            return EmailMessageBL.Instance.RetrieveInstantMessageMembers(memberID, groupID, filterRemovedMembers);
        }

        public EmailMessageList RetrieveMailConversationMessages(int memberID, int targetMemberID, int groupID, ConversationType conversationType)
        {
            return EmailMessageBL.Instance.RetrieveMailConversationMessages(memberID, targetMemberID, groupID, conversationType);
        }

        public MessageMemberCollection RetrieveMailConversationMembers(int memberID, int groupID, ConversationType conversationType)
        {
            return EmailMessageBL.Instance.RetrieveMailConversationMembers(memberID, groupID, conversationType);
        }

		#endregion

		#region Event Handlers

		private void EmailMessageBL_ReplicationRequested(IReplicable replicableObject)
		{
			_replicator.Enqueue(replicableObject);
		}

		private void EmailMessageBL_ReplicationActionRequested(IReplicationAction replicationAction)
		{
			_replicator.Enqueue(replicationAction);
		}

		private void EmailMessageBL_SynchronizationRequested(string key, Hashtable cacheReferences)
		{
			_synchronizer.Enqueue(key, cacheReferences);
		}

		#endregion


		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		public void PrePopulateCache()
		{
			// no implementation at this time
		}

		
		#region IDisposable Members
		public void Dispose()
		{
			_hydraWriter.Stop();
		}
		#endregion

		#region IReplicationActionRecipient Members

		void Matchnet.IReplicationActionRecipient.Receive(IReplicationAction replicationAction)
		{
			EmailMessageBL.Instance.PlayReplicationAction(replicationAction, false, false);
		}

		#endregion

		#region instrumentation
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														new CounterCreationData("Messages Saved/second", "Messages Saved/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Total Messages Sent/second", "Total Messages Sent/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Emails  Sent/second", "Emails Sent/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Flirts Sent/second", "Flirts Sent/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Missed IMs Sent/second", "Missed IMs Sent/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Message Lists Retrieved/second", "Message Lists Retrieved/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Message List Hit Rate", "Message List Hit Rate", PerformanceCounterType.RawFraction),
														new CounterCreationData("Message List Hit Rate_Base", "Message List Hit Rate_Base", PerformanceCounterType.RawBase),
														new CounterCreationData("Messages Retrieved/second", "Messages Retrieved/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Message Hit Rate", "Message Hit Rate", PerformanceCounterType.RawFraction),
														new CounterCreationData("Message Hit Rate_Base", "Message Hit Rate_Base", PerformanceCounterType.RawBase),
														new CounterCreationData("Message Content Retrieved/second", "Message Content Retrieved/second", PerformanceCounterType.RateOfCountsPerSecond32),
														new CounterCreationData("Message Content Hit Rate", "Message Content Hit Rate", PerformanceCounterType.RawFraction),
														new CounterCreationData("Message Content Hit Rate_Base", "Message Content Hit Rate_Base", PerformanceCounterType.RawBase),
														new CounterCreationData("Message Lists Cached", "Message Lists Cached", PerformanceCounterType.NumberOfItems32),
														new CounterCreationData("Messages Cached", "Messages Cached", PerformanceCounterType.NumberOfItems32)
			});
			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}


		private void initPerfCounters()
		{
			_messageSavedCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Messages Saved/second", false);
			_messageSendCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Total Messages Sent/second", false);
			_emailSendCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Emails  Sent/second", false);
			_flirtSendCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Flirts Sent/second", false);
			_missedIMSendCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Missed IMs Sent/second", false);

			_messageListRetrievedCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Lists Retrieved/second", false);
			_messageListHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message List Hit Rate", false);
			_messageListHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message List Hit Rate_Base", false);

			_messageRetrievedCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Messages Retrieved/second", false);
			_messageHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Hit Rate", false);
			_messageHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Hit Rate_Base", false);

			_messageContentRetrievedCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Content Retrieved/second", false);
			_messageContentHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Content Hit Rate", false);
			_messageContentHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Content Hit Rate_Base", false);

			_messageListsCached = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Message Lists Cached", false);
			_messagesCached = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Messages Cached", false);

			resetPerfCounters();
		}


		private void resetPerfCounters()
		{
			_messageSavedCount.RawValue = 0;
			_messageSendCount.RawValue = 0;
			_emailSendCount.RawValue = 0;
			_flirtSendCount.RawValue = 0;
			_missedIMSendCount.RawValue = 0;

			_messageListRetrievedCount.RawValue = 0;
			_messageListHitRate.RawValue = 0;
			_messageListHitRate_Base.RawValue = 0;

			_messageRetrievedCount.RawValue = 0;
			_messageHitRate.RawValue = 0;
			_messageHitRate_Base.RawValue = 0;

			_messageContentRetrievedCount.RawValue = 0;
			_messageContentHitRate.RawValue = 0;
			_messageContentHitRate_Base.RawValue = 0;

			_messageListsCached.RawValue = 0;
			_messagesCached.RawValue = 0;
		}

		private void EmailMessageBL_MessageSaved()
		{
			_messageSavedCount.Increment();
		}

		private void EmailMessageBL_MessageSent(MailType mailType)
		{
			switch (mailType)
			{
				case MailType.DeclineEmail:
				case MailType.Email:
				case MailType.IgnoreEmail:
				case MailType.PrivatePhotoEmail:
				case MailType.YesEmail:
					_emailSendCount.Increment();
					break;
				case MailType.MissedIM:
					_missedIMSendCount.Increment();
					break;
				case MailType.Tease:
					_flirtSendCount.Increment();
					break;
			}

			_messageSendCount.Increment();
		}

		private void EmailMessageBL_MessageListRetrieved(bool cacheHit)
		{
			_messageListRetrievedCount.Increment();
			_messageListHitRate_Base.Increment();

			if (cacheHit)
			{
				_messageListHitRate.Increment();
			}
			else
			{
				_messageListsCached.Increment();
			}
		}

		private void EmailMessageBL_MessageRetrieved(bool withContent, bool cacheHit)
		{
			if (withContent)
			{
				_messageContentRetrievedCount.Increment();
				_messageContentHitRate_Base.Increment();

				if (cacheHit)
				{
					_messageContentHitRate.Increment();
				}
				else
				{
					_messagesCached.Increment();
				}
			}
			else
			{
				_messageRetrievedCount.Increment();
				_messageHitRate_Base.Increment();

				if (cacheHit)
				{
					_messageHitRate.Increment();
				}
				else
				{
					_messagesCached.Increment();
				}
			}
		}

		private void EmailMessageBL_MessageRemoved()
		{
			_messagesCached.Decrement();
		}

		private void EmailMessageBL_MessageListRemoved()
		{
			_messageListsCached.Decrement();
		}

		#endregion
	}
}
