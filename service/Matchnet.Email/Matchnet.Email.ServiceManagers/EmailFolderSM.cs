using System;
using System.Collections;

using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.Replication;

namespace Matchnet.Email.ServiceManagers
{
	/// <summary>
	///
	/// </summary>
	public class EmailFolderSM : MarshalByRefObject, IServiceManager, IEmailFolderService, IDisposable, IReplicationActionRecipient
	{
		private const string SERVICE_CONSTANT = "EMAIL_SVC";
		private const string SERVICE_MANAGER_NAME = "EmailFolderSM";
		public const string SERVICE_NAME = "Matchnet.Email.Service";

		private Replicator _replicator = null;
		private Synchronizer _synchronizer = null;

		//TODO: Do we need perf counters?
		public EmailFolderSM()
		{
			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILSVC_FOLDER_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
			}
			_replicator = new Replicator(SERVICE_MANAGER_NAME);
			_replicator.SetDestinationUri(replicationURI);
			_replicator.Start();

			_synchronizer = new Synchronizer(SERVICE_NAME);
			_synchronizer.Start();

			EmailFolderBL.Instance.SynchronizationRequested += new Matchnet.Email.BusinessLogic.EmailFolderBL.SynchronizationEventHandler(EmailFolderBL_SynchronizationRequested);
			EmailFolderBL.Instance.ReplicationActionRequested += new Matchnet.Email.BusinessLogic.EmailFolderBL.ReplicationActionEventHandler(EmailFolderBL_ReplicationActionRequested);
		}

		#region IEmailFolderService Implementation

		public EmailFolderCollection RetrieveFolders(string clientHostName, CacheReference cacheReference, int memberID, int communityID, bool statsFlag)
		{
			try 
			{
				EmailFolderCollection oResult = null;

				oResult = EmailFolderBL.Instance.RetrieveFolders(clientHostName,
					cacheReference,
					memberID,
					communityID,
					statsFlag);

				return oResult;
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving folders.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving folders.", ex);
			}
		}

		/*public void EmptyFolder(int memberID, int communityID, int memberFolderID)
		{
			try 
			{
				EmailFolderBL.Instance.EmptyFolder(memberID,
					communityID,
					memberFolderID);

			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure emptying folders.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure emptying folders.", ex);
			}
		}*/

		public void Delete(string clientHostName, int memberFolderID, int memberID, int groupID)
		{
			try 
			{
				EmailFolderBL.Instance.Delete(clientHostName, memberFolderID, memberID, groupID);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure deleting folders.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure deleting folders.", ex);
			}		
		}

		public FolderSaveResult Save(string clientHostName, int memberID, int communityID, string description)
		{
			try 
			{
				return EmailFolderBL.Instance.Save(clientHostName,
					memberID,
					communityID, 
					description);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure saving folders.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure saving folders.", ex);
			}				
		}

		public FolderSaveResult Save(string clientHostName, int memberID, int communityID, string description, int memberFolderID)
		{
			try 
			{
				return EmailFolderBL.Instance.Save(clientHostName,
					memberID,
					communityID, 
					description, 
					memberFolderID);

			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure saving folders.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure saving folders.", ex);
			}		
		}

		#endregion
		
		public override object InitializeLifetimeService()
		{
			return null;
		}
		
		public void PrePopulateCache()
		{
			// no implementation at this time
		}

		#region IDisposable Members
		public void Dispose()
		{

		}
		#endregion

		#region Event Handlers

		private void EmailFolderBL_ReplicationActionRequested(IReplicationAction replicationAction)
		{
			_replicator.Enqueue(replicationAction);
		}

		private void EmailFolderBL_ReplicationRequested(IReplicable replicableObject)
		{
			_replicator.Enqueue(replicableObject);
		}

		private void EmailFolderBL_SynchronizationRequested(string key, Hashtable cacheReferences)
		{
			_synchronizer.Enqueue(key, cacheReferences);
		}

		#endregion

		#region IReplicationActionRecipient Members

		void IReplicationActionRecipient.Receive(IReplicationAction replicationAction)
		{
			EmailFolderBL.Instance.PlayReplicationAction(replicationAction);
		}

		#endregion

	}
}
