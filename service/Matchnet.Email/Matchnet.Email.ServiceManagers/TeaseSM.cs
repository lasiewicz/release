using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Email.BusinessLogic;
using Matchnet.Email.ServiceDefinitions;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

using System;

namespace Matchnet.Email.ServiceManagers
{
	/// <summary>
	/// Summary description for TeaseSM.
	/// </summary>
	public class TeaseSM : MarshalByRefObject, IServiceManager, ITeaseService, IDisposable
	{
		private const string SERVICE_MANAGER_NAME = "TeaseSM";
		public const string SERVICE_NAME = "Matchnet.Email.Service";

		public TeaseCollection GetTeaseCollection(int teaseCategoryID, int groupID)
		{
			try 
			{
				TeaseCollection oResult = null;
				
				oResult = TeaseBL.Instance.GetTeaseCollection(teaseCategoryID, groupID);

				return oResult;
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving teases.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving teases.", ex);
			}
		}

		public TeaseCategoryCollection GetTeaseCategoryCollection(int groupID) 
		{
			try 
			{
				TeaseCategoryCollection oResult = null;

				oResult = TeaseBL.Instance.GetTeaseCategoryCollection(groupID);

				return oResult;
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving tease categories.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving tease categories.", ex);
			}
		}

		public TeaseSM()
		{
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}
		
		public void PrePopulateCache()
		{
			// no implementation at this time
		}

		#region IDisposable Members
		public void Dispose()
		{

		}
		#endregion
	}
}
