using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Email.ServiceManagers;
using Matchnet.Email.ValueObjects;

namespace Matchnet.Email.Service
{
	public class EmailService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private EmailMessageSM _emailMessageSM = null;
		private EmailFolderSM _emailFolderSM = null;
		private TeaseSM _teaseSM = null;
		private EmailLogSM _emailLogSM = null;

		public EmailService()
		{
			InitializeComponent();
		}


		static void Main()
		{
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new EmailService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(Matchnet.Email.ValueObjects.ServiceConstants.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}


		protected override void RegisterServiceManagers()
		{
			try
			{
				_emailMessageSM = new EmailMessageSM();
				_emailFolderSM = new EmailFolderSM();
				_teaseSM = new TeaseSM();
				_emailLogSM = new EmailLogSM();
				base.RegisterServiceManager(_emailMessageSM);
				base.RegisterServiceManager(_emailFolderSM);
				base.RegisterServiceManager(_teaseSM);
				base.RegisterServiceManager(_emailLogSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service, see details:" + ex.Message);
			}
		}
	}
}

