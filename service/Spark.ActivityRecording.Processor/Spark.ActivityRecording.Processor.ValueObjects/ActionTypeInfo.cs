﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet;
using Matchnet.BaseClasses;

namespace Spark.ActivityRecording.Processor.ValueObjects
{
    public class ActionTypeInfo:BaseCacheable
    {
        public ActionTypeInfo()
            : base(CacheItemMode.Absolute, CacheItemPriorityLevel.Default, 3600)
        {
        }

        public int Id { get; set; }
        public string StoredProcedureName { get; set; }
        public bool ShallGenerateNewActivityKeyValue { get; set; }
        public string CaptionRoot { get; set; }
        public override string GetCacheKey()
        {
            return GetCacheKey(Id);
        }

        public static string GetCacheKey(int id)
        {
            return "~ActionTypeInfo^" + id;
        }
    }
}
