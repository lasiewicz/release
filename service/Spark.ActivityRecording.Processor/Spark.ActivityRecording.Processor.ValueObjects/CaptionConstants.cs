﻿namespace Spark.ActivityRecording.Processor.ValueObjects
{
    public static class CaptionConstants
    {
        public const string LOGINSESSIONID = "LoginSessionId";
        public const string ORIGINIPADDRESS = "OriginIPAddress";
        public const string LOGINSTATEVALUE = "LoginStateValue";
        public const string LOGINTEMPLATE = "LoginTemplate";
        public const string EMAILADDRESS = "EmailAddress";
        public const string LOGINFRAUDCALLSUCCESS = "LoginFraudCallSuccess";
        public const string LOGINFRAUDRESULT = "LoginFraudResult";
        public const string LOGINFRAUDFREJECTEDREASON = "LoginFraudRejectedReason";
        public const string LOGINAUTHENTICATIONRESULT = "LoginAuthenticationResult";
        public const string LOGINSTATEVALUESMATCH = "LoginStateValuesMatch";
        public const string PASSWORDRESETREQUESTSUCCESSFUL = "PasswordResetRequestSuccessful";
        public const string PASSWORDRESETSUCCESSFUL = "PasswordResetSuccessful";
        // IAP Receipt Constants
        public const string IAP_QUANTITY = "quantity";
        public const string IAP_PRODUCT_ID = "product_id";
        public const string IAP_TRANSACTION_ID = "transaction_id";
        public const string IAP_ORIGINAL_TRANSACTION_ID = "original_transaction_id";
        public const string IAP_PURCHASE_DATE = "purchase_date";
        public const string IAP_ORIGINAL_PURCHASE_DATE = "original_purchase_date";
        public const string IAP_EXPIRES_DATE = "expires_date";
        public const string IAP_CANCELLATION_DATE = "cancellation_date";
        public const string IAP_WEB_ORDER_LINE_ITEM_ID = "web_order_line_item_id";
        public const string IAP_IS_TRIAL_PERIOD = "is_trial_period";
        public const string IAP_CONTROLLER_STATUS = "iap_controller_status";
        public const string IAP_HAS_EXCEPTION = "HasException";
        public const string IAP_EXCEPTION_MESSAGE = "ExceptionMessage";

        // IAB Receipt Constants
        public const string IAB_SUBSCRIPTION_KIND = "Kind";
        public const string IAB_SUBSCRIPTION_IS_AUTORENEW = "IsAutoRenew";
        public const string IAB_SUBSCRIPTION_EXPIRATION_DATE = "ExpirationDate";
        public const string IAB_SUBSCRIPTION_START_DATE = "StartDate";
        public const string IAB_SUBSCRIPTION_HAS_EXCEPTION = "HasException";
        public const string IAB_SUBSCRIPTION_EXCEPTION_MESSAGE = "ExceptionMessage";

        // ADD or REMOVE favorites activity recording Constants
        public const string ACTION = "Action";
        public const string TIMESTAMP = "Timestamp";
        public const string COMMUNITY_ID = "CommunityId";
    }

    public static class CaptionRootConstants
    {
        public const string LOGINARRIVEDATCLIENTSYSTEM = "LoginArrivedAtClientSystem";
        public const string LOGINARRIVEDATSUA = "LoginArrivedAtSUA";
        public const string LOGINATTEMPTEDLOGIN = "LoginAttemptedLogin";
        public const string LOGINFRAUDCHECK = "LoginFraudCheck";
        public const string LOGINAUTHENTICATION = "LoginAuthentication";
        public const string LOGINSENTBACKTOCLIENT = "LoginSentBackToClient";
        public const string LOGINARRIVEDATPASSWORDRESETREQUEST = "LoginArrivedAtPasswordResetRequest";
        public const string LOGINPASSWORDRESETREQUESTSUBMITTED = "LoginPasswordResetRequestSubmitted";
        public const string LOGINARRIVEDATPASSWORDRESET = "LoginArrivedAtPasswordReset";
        public const string LOGINATTEMPTEDPASSWORDRESET = "LoginAttemptedPasswordReset";
        public const string LOGINAUTOLOGIN = "LoginAutoLogin";
        public const string IOSINAPPPURCHASERECEIPT = "IOSInAppPurchaseReceipt";
        public const string IOSINAPPPURCHASESTATUS = "IOSInAppPurchaseStatus";
        public const string ANDROIDINAPPBILLINGRECEIPT = "AndroidInAppBillingReceipt";
        public const string ANDROIDINAPPBILLINGSTATUS = "AndroidInAppBillingStatus";
        public const string DYNAMICYIELDREGISTRATION = "DynamicYieldRegistration";
        public const string APPINSTALL = "AppInstall";
        public const string INAPPPURCHASESUCCESS = "InApplicationPurchaseSuccess";
    }
}