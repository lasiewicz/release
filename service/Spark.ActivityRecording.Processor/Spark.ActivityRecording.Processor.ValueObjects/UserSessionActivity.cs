﻿using System;

namespace Spark.ActivityRecording.Processor.ValueObjects
{
    public class UserSessionActivity
    {
        // Used for insert and update
        public int SessionTrackingId { get; set; }
        public string SessionKey { get; set; }
        public SessionActivityType SessionActivityType { get; set; }

        // Used for Insert only
        public int SiteId { get; set; }
        public DateTime SessionStart { get; set; }
        public int? PromotionId { get; set; }
        public string LuggageId { get; set; }
        public int? BannerId { get; set; }
        public string ClientIp { get; set; }
        public string ReferrerUrl { get; set; }
        public int? LandingPageId { get; set; }
        public int? LandingPageTestId { get; set; }

        // Used for Update only
        public int MemberId { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime UpdateDate { get; set; }



    }


    
}
