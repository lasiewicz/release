﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.ActivityRecording.Processor.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "ACTIVITYRECORDINGPROCESSOR_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Spark.ActivityRecording.Processor.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "ProcessorSM";
    }
}
