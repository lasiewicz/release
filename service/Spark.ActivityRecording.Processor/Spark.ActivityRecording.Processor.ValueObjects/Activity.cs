﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.ActivityRecording.Processor.ValueObjects
{
    public class Activity
    {
        public int MemberId { get; set; }
        public int TargetMemberId { get; set; }
        public int SiteId { get; set; }
        public int ActionType { get; set; }
        public CallingSystem CallingSystem { get; set; }
        public string Caption { get; set; }
        public Dictionary<string, string> CaptionParameters { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
