﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.RemotingServices;
using Spark.ActivityRecording.Processor.ServiceManager;

namespace Spark.ActivityRecording.Processor.Service
{
    public partial class Processor : RemotingServiceBase
    {
        private ProcessorSM _processorSM;
        
        public Processor()
        {
            _processorSM = new ProcessorSM();
            InitializeComponent();
        }

        static void Main()
        {
            var servicesToRun = new ServiceBase[] 
                                              { 
                                                  new Processor() 
                                              };
            Run(servicesToRun);
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            _processorSM.Start();
        }

        protected override void OnStop()
        {
            base.OnStop();
            _processorSM.Stop();
        }

        protected override void RegisterServiceManagers()
        {
            RegisterServiceManager(_processorSM);
            base.RegisterServiceManagers();
        }
    }
}
