﻿#region

using System;
using System.Collections.Generic;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.RabbitMQ.Client;

#endregion

namespace Spark.ActivityRecording.Processor.ServiceAdapter
{
    /// <summary>
    ///     Adapter class for enqueuing activity recording into RabbitMQ for the processor.
    ///     Possibly extend to include calls directly to processor service if needed.
    /// </summary>
    public class ActivityRecordingProcessorAdapter
    {
        private static Publisher _publisher;

        public static readonly ActivityRecordingProcessorAdapter Instance = new ActivityRecordingProcessorAdapter();

        private bool IsUTCEnabled 
        {
            get
            {
                bool b=false;
                try
                {
                    b=Boolean.Parse(RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.USE_UTC_AR_DATETIME));
                }
                catch (Exception)
                {
                }
                return b;
            }
        }

        private ActivityRecordingProcessorAdapter()
        {
            var rabbitMQUserName = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_USER_ACCOUNT);
            var rabbitMQPassword = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.RABBITMQ_PASSWORD);
            var rabbitMQServer = RuntimeSettings.Instance.GetSettingFromSingleton(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER);
            _publisher = new Publisher(rabbitMQServer, rabbitMQUserName, rabbitMQPassword);
        }   

        public void RecordActivity(int memberId, int targetMemberId, int actionType, int siteID, string caption, CallingSystem callingSystem)
        {
            var activity = new Activity
            {
                MemberId = memberId,
                TargetMemberId = targetMemberId,
                ActionType = actionType,
                Caption = string.IsNullOrEmpty(caption) ? string.Empty : caption,
                CaptionParameters = null,
                CallingSystem = callingSystem,
                SiteId = siteID,
                Timestamp = IsUTCEnabled ? DateTime.UtcNow : DateTime.Now 
            };
            _publisher.Publish(activity);
        }

        public void RecordActivity(int memberId, int targetMemberId, int actionType, int siteID, CallingSystem callingSystem, Dictionary<string, string> captionParameters)
        {
            var activity = new Activity
            {
                MemberId = memberId,
                TargetMemberId = targetMemberId,
                ActionType = actionType,
                Caption = null,
                CaptionParameters = captionParameters,
                CallingSystem = callingSystem,
                SiteId = siteID,
                Timestamp = IsUTCEnabled ? DateTime.UtcNow : DateTime.Now 
            };
            _publisher.Publish(activity);
        }

        [Obsolete("Use correspondent method with int ActionType",false)]
        public void RecordActivity(int memberId, int targetMemberId, ActionType actionType, int siteID, string caption, CallingSystem callingSystem)
        {
           RecordActivity(memberId,targetMemberId,(int)actionType,siteID,caption,callingSystem);
        }
        
        [Obsolete("Use correspondent method with int ActionType", false)]
        public void RecordActivity(int memberId, int targetMemberId, ActionType actionType, int siteID, CallingSystem callingSystem, Dictionary<string, string> captionParameters)
        {
            RecordActivity(memberId,targetMemberId,(int)actionType,siteID,callingSystem,captionParameters);
        }

        public void RecordActivity(UserSessionActivity activity)
        {
            _publisher.Publish(activity);    
        }
    }
}