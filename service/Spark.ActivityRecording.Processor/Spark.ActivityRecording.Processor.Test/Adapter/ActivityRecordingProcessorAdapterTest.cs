﻿using System;
using System.Collections.Generic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data;
using NUnit.Framework;
using Spark.ActivityRecording.Processor.ServiceAdapter;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.Managers.Managers;
using Spark.RabbitMQ.Client;

namespace Spark.ActivityRecording.Processor.Test
{
    [TestFixture]
    public class ActivityRecordingProcessorAdapterTest
    {
        private const int memberId =101805265;
        private int targetMemberId = 23248772;
        private const int actionType = 3;
        private const int siteID = 103;
        public int Counter = 0;

        //
        private string _rabbitMQUserName = string.Empty;
        private string _rabbitMQPassword = string.Empty;
        private string _rabbitMQServer = string.Empty;
        private int _prefetchCount = 0;
        Subscriber _subscriber;

        [TestFixtureSetUp]
        public void Setup()
        {
            Counter = 0;
            var settingsManager = new SettingsManager();
            _rabbitMQUserName = settingsManager.GetSettingString(SettingConstants.RABBITMQ_USER_ACCOUNT, null);
            _rabbitMQPassword = settingsManager.GetSettingString(SettingConstants.RABBITMQ_PASSWORD, null);
            _rabbitMQServer = settingsManager.GetSettingString(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER, null);
            _prefetchCount = settingsManager.GetSettingInt(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_PREFETCH, null);
            _subscriber = new Subscriber(_rabbitMQServer, _rabbitMQUserName, _rabbitMQPassword, _prefetchCount);
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            _subscriber.Dispose();

        }

        [Test]
        public void RecordActivityTest()
        {
            Counter = 0;
            _subscriber.Subscribe<Activity>("ActivityRecording", UpdateCounter);

            var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.ACTION, "Add Favorite" }
                                            };
            for (int i = 0; i < 10; i++)
            {
                ActivityRecordingProcessorAdapter.Instance.RecordActivity(memberId, targetMemberId+i, actionType, siteID,
                    CallingSystem.Web, captionParameters);
                System.Threading.Thread.Sleep(1000);
            }

            System.Threading.Thread.Sleep(10000);
            Assert.AreEqual(10,Counter);
        }

        private void UpdateCounter(Activity activity)
        {
            Counter++;
        }

    }
}
