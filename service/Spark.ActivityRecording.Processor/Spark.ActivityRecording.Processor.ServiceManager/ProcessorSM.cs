﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Data.Hydra;
using Spark.ActivityRecording.Processor.BusinessLogic;

namespace Spark.ActivityRecording.Processor.ServiceManager
{
    public class ProcessorSM : MarshalByRefObject, IServiceManager, IBackgroundProcessor
    {
        private readonly HydraWriter _hydraWriter;
        
        public ProcessorSM()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnActivityRecording"});
        }

        public void PrePopulateCache()
        {
            
        }

        public void Dispose()
        {
            
        }

        public void Start()
        {
            _hydraWriter.Start();
            ProcessorBL.Instance.Start();
        }

        public void Stop()
        {
            ProcessorBL.Instance.Stop();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
