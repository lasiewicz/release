﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Matchnet.Data;
using Spark.ActivityRecording.Processor.ValueObjects;

namespace Spark.ActivityRecording.Processor.BusinessLogic
{
    internal class SessionActivityProcessorAction
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof (SessionActivityProcessorAction));

        public static readonly SessionActivityProcessorAction Instance = new SessionActivityProcessorAction();

        #region Helper functions
        // Probably better place for these functions is somewhere in Common. 
        // However, they are placed here because they do not form the complete solution
        // and do not follow Maybe pattern exactly. Rather, they are implemented 
        // in an imperactive way.
        // Therefore, the naming is not perfect. So, they stay here.
        private static void Maybe(int? val, Action<int> action)
        {
            if (val.HasValue) action(val.Value);
        }
        private static void Maybe(DateTime? val, Action<DateTime> action)
        {
            if (val.HasValue) action(val.Value);
        }
        private static void Maybe(string val, Action<string> action)
        {
            if (val!=null) action(val);
        }
        #endregion

        /// <summary>
        /// Records UserSessionActivity. Is used for both, inserts and updates
        /// because the order is important. If they are placed in different queues,
        /// the order is not guaranteed.
        /// </summary>
        /// <param name="activity"></param>

        public void Record(UserSessionActivity activity)
        {
            switch (activity.SessionActivityType)
            {
                    case SessionActivityType.ForInsert:
                        RecordInsert(activity);
                        break;
                    case SessionActivityType.ForUpdate:
                        RecordUpdate(activity);
                        break;
                    default:
                        _Logger.Error(string.Format(
                            "UserSessionActivity, Unknown SessionActivityType. MemberId: {0} SessionTrackingId: {1} SessionKey:{2} SiteId: {3} SessionActivityType: {4}",
                            activity.MemberId, activity.SessionTrackingId, activity.SessionKey, activity.SiteId, (int) activity.SessionActivityType));
                        break;
            }
        }

        void RecordInsert(UserSessionActivity activity)
            {
                try
                {
                    _Logger.Info(string.Format(
                        "UserSessionActivity received. MemberId: {0} SessionTrackingId: {1} SessionKey:{2} SiteId: {3} SessionActivityType: {4}",
                        activity.MemberId, activity.SessionTrackingId, activity.SessionKey, activity.SiteId, (int)activity.SessionActivityType));


                    var command = new Command("mnActivityRecording", "dbo.up_InsertSessionTrackingDetails", 0);
                    command.AddParameter("@SessionTrackingID", SqlDbType.Int, ParameterDirection.Input, activity.SessionTrackingId);
                    command.AddParameter("@SessionKey", SqlDbType.NVarChar, ParameterDirection.Input, activity.SessionKey);
                    command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, activity.SiteId);
                    command.AddParameter("@SessionStart", SqlDbType.DateTime, ParameterDirection.Input, activity.SessionStart);

                    Maybe(activity.PromotionId, x =>command.AddParameter("@PromotionID", SqlDbType.Int, ParameterDirection.Input,x));
                    Maybe(activity.LuggageId, x => command.AddParameter("@LuggageID", SqlDbType.VarChar, ParameterDirection.Input, x));
                    Maybe(activity.BannerId, x => command.AddParameter("@BannerID", SqlDbType.Int, ParameterDirection.Input,x));

                    command.AddParameter("@ClientIP", SqlDbType.VarChar, ParameterDirection.Input, activity.ClientIp);

                    Maybe(activity.ReferrerUrl, x=>command.AddParameter("@ReferrerURL", SqlDbType.VarChar, ParameterDirection.Input, x));
                    Maybe(activity.LandingPageId, x => command.AddParameter("@LandingPageID", SqlDbType.Int, ParameterDirection.Input, x));
                    Maybe(activity.LandingPageTestId, x => command.AddParameter("@LandingPageTestID", SqlDbType.Int, ParameterDirection.Input, x));

                    Client.Instance.ExecuteAsyncWrite(command);
                }
                catch (Exception ex)
                {
                    _Logger.Error(string.Format(
                            "Error in UserSessionActivity. MemberId: {0} SessionTrackingId: {1} SessionKey:{2} SiteId: {3} SessionActivityType: {4}",
                            activity.MemberId, activity.SessionTrackingId, activity.SessionKey, activity.SiteId, (int)activity.SessionActivityType), ex);
}
            }

        void RecordUpdate(UserSessionActivity activity)
        {
            try
            {
                var command = new Command("mnActivityRecording", "dbo.up_UpdateSessionTrackingDetails", 0);
                command.AddParameter("@SessionTrackingID", SqlDbType.Int, ParameterDirection.Input, activity.SessionTrackingId);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, activity.MemberId);
                
                Maybe(activity.RegistrationDate, x => command.AddParameter("@RegistrationDate", SqlDbType.DateTime, ParameterDirection.Input, x));
                
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                _Logger.Error(string.Format(
                    "Error in UserSessionActivity. MemberId: {0} SessionTrackingId: {1} SessionKey:{2} SiteId: {3} SessionActivityType: {4} RegistrationDate: {5}",
                    activity.MemberId, activity.SessionTrackingId, activity.SessionKey, activity.SiteId, (int)activity.SessionActivityType, activity.RegistrationDate), ex);
            }
        }
    }
}
