﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Data;
using Spark.ActivityRecording.Processor.ValueObjects;

namespace Spark.ActivityRecording.Processor.BusinessLogic
{
    public class ActionTypeBL
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof (ActionTypeBL));

        public static readonly ActionTypeBL Instance = new ActionTypeBL();

        public bool GetActionTypeExists(int actionTypeId)
        {
            var res = GetActionType(actionTypeId);
            return res != null && res.Id > 0;
        }

        public ActionTypeInfo GetActionType(int actionTypeId)
        {
            var res = Cache.Instance.Get(ValueObjects.ActionTypeInfo.GetCacheKey(actionTypeId)) as ValueObjects.ActionTypeInfo;
            if (res == null)
            {
                res = GetActionTypeFromDb(actionTypeId);
                if (res != null)
                {
                    Cache.Instance.Add(res); // Internal calls. No synchronization is needed. It set to expire in one hour. Acceptable for Action Types.
                }
            }
            return res;
        }

        private ValueObjects.ActionTypeInfo GetActionTypeFromDb(int actionTypeId)
        {
            if (actionTypeId <= 0)
                return null;

            var command = new Command("mnActivityRecording", "dbo.up_ActionType_Get", 0);
            command.AddParameter("@ActionTypeID", SqlDbType.Int, ParameterDirection.Input, actionTypeId);

            var reader = Client.Instance.ExecuteReader(command);

            ActionTypeInfo ret = null;
 
            while (reader.Read())
            {
                var shallGenerateKey = false;
                if (!reader.IsDBNull(2))
                {
                    shallGenerateKey = reader.GetBoolean(2);
                }

                ret = new ActionTypeInfo()
                {
                    Id = reader.GetInt32(0), 
                    StoredProcedureName = reader.GetString(1),
                    ShallGenerateNewActivityKeyValue = shallGenerateKey,
                    CaptionRoot = reader.GetString(3)
                };
            }

            return ret;
        }

    }

}
