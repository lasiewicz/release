﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.ActivityRecording.Processor.BusinessLogic
{
    static class CaptionHelper
    {
        public static string GetCaption(string captionRoot, Dictionary<string, string> parameters)
        {
            var captionBuilder = new StringBuilder();

            captionBuilder.Append("<");
            captionBuilder.Append(captionRoot);
            captionBuilder.Append(">");
            BuildCaptionBody(parameters, captionBuilder);
            captionBuilder.Append("</");
            captionBuilder.Append(captionRoot);
            captionBuilder.Append(">");

            return captionBuilder.ToString();
        }

        private static void BuildCaptionBody(Dictionary<string, string> parameters, StringBuilder stringBuilder)
        {
            foreach (var element in parameters)
            {
                stringBuilder.Append("<");
                stringBuilder.Append(element.Key);
                stringBuilder.Append(">");
                stringBuilder.Append(element.Value);
                stringBuilder.Append("</");
                stringBuilder.Append(element.Key);
                stringBuilder.Append(">");
            }
        }
    }
}
