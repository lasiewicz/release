﻿using System;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.Managers.Managers;
using Spark.RabbitMQ.Client;
using log4net;
using log4net.Config;

namespace Spark.ActivityRecording.Processor.BusinessLogic
{
    public class ProcessorBL
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ProcessorBL));

        public readonly static ProcessorBL Instance = new ProcessorBL();
        private Subscriber _subscriber;
        private string _rabbitMQUserName = string.Empty;
        private string _rabbitMQPassword = string.Empty;
        private string _rabbitMQServer = string.Empty;
        private int _prefetchCount = 0; 


        private ProcessorBL()
        {
            XmlConfigurator.Configure();
        }

        public void Start()
        {
            GetSettings();
            _subscriber = new Subscriber(_rabbitMQServer, _rabbitMQUserName, _rabbitMQPassword, _prefetchCount);
            _subscriber.Subscribe<Activity>("ActivityRecording", ActivityProcessorAction.Instance.Record);
            _subscriber.Subscribe<UserSessionActivity>("SessionActivityRecording", SessionActivityProcessorAction.Instance.Record);
            _Logger.Info("Subscribed: " + _rabbitMQServer);
        }

        public void Stop()
        {
            if(_subscriber != null)
            {
                _Logger.Info("Stopped");
                _subscriber.Dispose();
            }
        }

        private void GetSettings()
        {
            var settingsManager = new SettingsManager();
            _rabbitMQUserName = settingsManager.GetSettingString(SettingConstants.RABBITMQ_USER_ACCOUNT, null);
            _rabbitMQPassword = settingsManager.GetSettingString(SettingConstants.RABBITMQ_PASSWORD, null);
            _rabbitMQServer = settingsManager.GetSettingString(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_SERVER, null);
            _prefetchCount = settingsManager.GetSettingInt(SettingConstants.ACTIVITYRECORDING_PROCESSOR_RABBITMQ_PREFETCH, null);
        }

    }
}
