﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Spark.ActivityRecording.Processor.ValueObjects;

namespace Spark.ActivityRecording.Processor.BusinessLogic
{
    class ActivityProcessorAction
    {
        private static readonly ILog _Logger = LogManager.GetLogger(typeof(ActivityProcessorAction));

        public readonly static ActivityProcessorAction Instance = new ActivityProcessorAction();

        public void Record(Activity activity)
        {
            try
            {
                _Logger.Info(string.Format("ActivityReceived. MemberId: {0} TargetMemberId: {1} SiteId: {2} ActionType: {3} CallingSystem: {4}",
                            activity.MemberId, activity.TargetMemberId, activity.SiteId, activity.ActionType,
                            activity.CallingSystem));

                var actionType = ActionTypeBL.Instance.GetActionType(activity.ActionType);

                var procedureName = actionType.StoredProcedureName;

                Command command = new Command("mnActivityRecording", procedureName, 0);

                if (actionType.ShallGenerateNewActivityKeyValue)
                {
                    var key = KeySA.Instance.GetKey("ActivityID");
                    command.AddParameter("@ActivityID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, key);
                }

                var caption =  activity.Caption?? CaptionHelper.GetCaption(actionType.CaptionRoot, activity.CaptionParameters);

                command.AddParameter("@MemberID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, activity.MemberId);
                command.AddParameter("@TargetMemberID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, activity.TargetMemberId);
                command.AddParameter("@SiteID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, activity.SiteId);
                command.AddParameter("@ActionTypeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, (int)activity.ActionType);
                command.AddParameter("@CallingSystemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, (int)activity.CallingSystem);
                command.AddParameter("@ActivityCaption", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, caption);

                if(activity.Timestamp > DateTime.MinValue)
                    command.AddParameter("@Timestamp", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, activity.Timestamp);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                if (activity == null)
                {
                    _Logger.Error("Error in ActivityRecordingProcessor", ex);
                }
                else
                {
                    _Logger.Error(string.Format(
                            "Error in ActivityRecordingProcessor. MemberId: {0} TargetMemberId: {1} SiteId: {2} ActionType: {3} CallingSystem: {4}",
                            activity.MemberId, activity.TargetMemberId, activity.SiteId, activity.ActionType,
                            activity.CallingSystem), ex);
                }

                throw ex;
            }
        }
    }
}
