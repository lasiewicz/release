using System;
using System.Diagnostics;

using Matchnet.Exceptions;
using Matchnet.MembersOnline.BusinessLogic;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceDefinitions;
using Matchnet.Replication;


namespace Matchnet.MembersOnline.ServiceManagers
{
	public class MembersOnlineSM : MarshalByRefObject, IMembersOnlineService, IServiceManager, IDisposable, IReplicationRecipient
	{
		private MembersOnlineBL _bl;
        private MembersOnlineBLv2 _blv2;
		private Replicator _replicator;

		private PerformanceCounter _perfCountMember;
		private PerformanceCounter _perfAdditions;
		private PerformanceCounter _perfRemovals;
		private PerformanceCounter _perfUpdates;
		private PerformanceCounter _perfStatusChecks;
		private PerformanceCounter _perfSearches;

		public MembersOnlineSM()
		{
			initPerfCounters();
			_bl = new MembersOnlineBL();
			_bl.MemberAdded += new Matchnet.MembersOnline.BusinessLogic.MembersOnlineBL.MemberAddEventHandler(MembersOnlineBL_MemberAdded);
			_bl.MemberRemoved += new Matchnet.MembersOnline.BusinessLogic.MembersOnlineBL.MemberRemoveEventHandler(MembersOnlineBL_MemberRemoved);
			_bl.MemberUpdated += new Matchnet.MembersOnline.BusinessLogic.MembersOnlineBL.MemberUpdateEventHandler(MembersOnlineBL_MemberUpdated);
			_bl.SearchRequested += new Matchnet.MembersOnline.BusinessLogic.MembersOnlineBL.SearchRequestEventHandler(MembersOnlineBL_SearchRequested);
			_bl.MemberStatusRequested += new Matchnet.MembersOnline.BusinessLogic.MembersOnlineBL.MemberStatusRequestEventHandler(MembersOnlineBL_MemberStatusRequested);
			_bl.ReplicationRequested += new Matchnet.MembersOnline.BusinessLogic.MembersOnlineBL.ReplicationEventHandler(MembersOnlineBL_ReplicationRequested);
			_bl.Start();

            _blv2 = new MembersOnlineBLv2();
            _blv2.Start();

			//replication
			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			string replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, ServiceConstants.SERVICE_MANAGER_NAME, machineName);
			_replicator = new Replicator(ServiceConstants.SERVICE_NAME);
			_replicator.SetDestinationUri(replicationURI);
			_replicator.Start();
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public void Add(MemberOnlineAdd memberOnlineAdd)
		{
			try
			{
				_bl.Add(memberOnlineAdd);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error adding member.", ex);
			}
		}


		public void Remove(MemberOnlineRemove memberOnlineRemove)
		{
			try
			{
				_bl.Remove(memberOnlineRemove);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error removing member.", ex);
			}
		}


		public void Update(MemberOnlineUpdate update)
		{
			try
			{
				_bl.Update(update);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error removing member.", ex);
			}
		}

		public bool IsMemberOnline(Int32 communityID, Int32 memberID)
		{
			try
			{
                if (MOLHelper.IsUseMOLRefresherEnabled(communityID))
                {
                    return _blv2.IsMemberOnline(communityID, memberID);
                }
                else
                {
                    return _bl.IsMemberOnline(communityID, memberID);
                }
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error checking member online status.", ex);
			}
		}

		
		public QueryResult Search(Query query)
		{
			try
			{
				return _bl.Search(query);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error processing search.", ex);
			}
		}

        /// <summary>
        /// OLD - gets a collection of memberIDs for all communities; recommended to use GetMOLCommunity instead
        /// </summary>
        /// <returns></returns>
        public MOLCollection GetMembersOnline()
        {
            try
            {
                if (MOLHelper.IsUseMOLRefresherEnabled(Constants.NULL_INT))
                {
                    return _blv2.GetMembersOnline();
                }
                else
                {
                    return _bl.GetMembersOnline();
                }

            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error retrieving list of all members online.", ex);
            }
        }

        /// <summary>
        /// gets a list of members who are online for a community
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
		public MOLCommunity GetMOLCommunity(int communityID)
		{
			try
			{
                if (MOLHelper.IsUseMOLRefresherEnabled(communityID))
                {
                    return _blv2.GetMOLCommunity(communityID);
                }
                else
                {
                    return _bl.GetMOLCommunity(communityID);
                }
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error retrieving list of all members online.", ex);
			}
		}


		private void MembersOnlineBL_ReplicationRequested(IReplicable replicableObject)
		{
			_replicator.Enqueue(replicableObject);
		}


		public void Receive(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "MemberOnlineAdd":
					_bl.Add((MemberOnlineAdd)replicableObject, false);
					break;

				case "MemberOnlineRemove":
					_bl.Remove((MemberOnlineRemove)replicableObject, false);
					break;

				default:
					throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
					break;
			}
		}


		#region instrumentation
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("Member Items", "Member Items", PerformanceCounterType.NumberOfItems64),

														 new CounterCreationData("Member Additions/second", "Member Additions/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Member Removals/second", "Member Removals/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Member Updates/second", "Member Updates/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Member Status Requests/second", "Member Status Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Member Search Requests/second", "Member Search Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
													 });
			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}


		private void initPerfCounters()
		{
			_perfCountMember = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Member Items", false);
			_perfAdditions = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Member Additions/second", false);
			_perfRemovals = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Member Removals/second", false);
			_perfUpdates = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Member Updates/second", false);
			_perfStatusChecks = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Member Status Requests/second", false);
			_perfSearches = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Member Search Requests/second", false);

			resetPerfCounters();
		}


		private void resetPerfCounters()
		{
			_perfCountMember.RawValue = 0;
			_perfAdditions.RawValue = 0;
			_perfRemovals.RawValue = 0;
			_perfUpdates.RawValue = 0;
			_perfStatusChecks.RawValue = 0;
			_perfSearches.RawValue = 0;
		}


		private void MembersOnlineBL_MemberAdded()
		{
			_perfCountMember.Increment();
			_perfAdditions.Increment();
		}


		private void MembersOnlineBL_MemberRemoved()
		{
			_perfCountMember.Decrement();
			_perfRemovals.Increment();
		}


		private void MembersOnlineBL_MemberUpdated()
		{
			_perfUpdates.Increment();
		}


		private void MembersOnlineBL_MemberStatusRequested()
		{
			_perfStatusChecks.Increment();
		}


		private void MembersOnlineBL_SearchRequested()
		{
			_perfSearches.Increment();
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			resetPerfCounters();
			_bl.Stop();
			_replicator.Stop();
		}

		#endregion
	}
}
