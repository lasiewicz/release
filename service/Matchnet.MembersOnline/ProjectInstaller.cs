using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

using Matchnet.MembersOnline.ServiceManagers;
using Matchnet.MembersOnline.ValueObjects;


namespace Matchnet.MembersOnline.Service
{
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
		private System.ServiceProcess.ServiceInstaller serviceInstaller1;
		private System.ComponentModel.Container components = null;

		public ProjectInstaller()
		{
			InitializeComponent();
		}


		private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
		{
			MembersOnlineSM.PerfCounterInstall();
		}


		private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
		{
			MembersOnlineSM.PerfCounterUninstall();
		}

		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
			this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
			// 
			// serviceProcessInstaller1
			// 
			this.serviceProcessInstaller1.Password = null;
			this.serviceProcessInstaller1.Username = null;
			// 
			// serviceInstaller1
			// 
			this.serviceInstaller1.ServiceName = ServiceConstants.SERVICE_NAME;
			this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller1,
																					  this.serviceInstaller1});

			this.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_BeforeUninstall);
			this.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterInstall);
		}
		#endregion
	}
}
