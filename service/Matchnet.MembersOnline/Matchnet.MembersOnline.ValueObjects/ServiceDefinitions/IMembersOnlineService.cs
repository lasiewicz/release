using System;

using Matchnet;
using Matchnet.MembersOnline.ValueObjects;


namespace Matchnet.MembersOnline.ServiceDefinitions
{
	public interface IMembersOnlineService
	{
		void Add(MemberOnlineAdd memberOnlineAdd);

		void Remove(MemberOnlineRemove memberOnlineRemove);

		bool IsMemberOnline(Int32 communityID, Int32 memberID);

		QueryResult Search(Query query);

		MOLCollection GetMembersOnline();
        MOLCommunity GetMOLCommunity(int communityID);
	}
}