using System;
using System.Collections;

namespace Matchnet.MembersOnline.ValueObjects
{
	/// <summary>
	/// Summary description for CommunityMOLCollection.
	/// </summary>
	[Serializable]
	public class CommunityMOLCollection : IEnumerable
	{
		private ArrayList _memberIDs;

		public CommunityMOLCollection(Int32 count)
		{
			_memberIDs = new ArrayList(count);
		}

		public void Add(Int32 memberID)
		{
			_memberIDs.Add(memberID);
		}

		public Int32 Count
		{
			get
			{
				return _memberIDs.Count;
			}
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _memberIDs.GetEnumerator();
		}

		#endregion
	}
}
