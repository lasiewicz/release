using System;

namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public class MemberOnlineRemove : MemberOnlineBase
	{
		public MemberOnlineRemove(Int32 communityID,
			Int32 memberID)
		{
			base._communityID = communityID;
			base._memberID = memberID;
		}


		public new static string GetCacheKey(Int32 communityID,
			Int32 memberID)
		{
			return "MOLRemove|" + communityID.ToString() + "|" + memberID.ToString();
		}

		
		public new string GetCacheKey()
		{
			return MemberOnlineBase.GetCacheKey(_communityID, _memberID);
		}


		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
	}
}
