using System;
using System.Collections;

using Matchnet;

namespace Matchnet.MembersOnline.ValueObjects
{
	/// <summary>
	/// Summary description for MOLQueryResult.
	/// </summary>
	[Serializable]
	public class MOLQueryResult : IValueObject
	{
		private ArrayList	_memberIDList;
		private Int32		_totalMembersOnline;

		public MOLQueryResult( int pOnlineTotal )
		{
			_totalMembersOnline = pOnlineTotal;
			_memberIDList = new ArrayList();
        }

		public void AddMemberID( Int32 pMemberID ) 
		{
            _memberIDList.Add( pMemberID );
		}

		public ArrayList ToArrayList() 
		{
			return _memberIDList;
		}

		public Int32 TotalMembersOnline 
		{
			get 
			{
				return _totalMembersOnline;
			}
			set
			{
				_totalMembersOnline = value;
			}
		}

		public Int32 MembersReturned
		{
			get 
			{
				return _memberIDList.Count;
			}
		}

	}
}
