using System;
using System.Text;

using Matchnet;

namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public enum SortFieldType
	{
		HasPhoto,
		UserName,
		Age,
		Gender,
		UpdateDate,
		LogonDate,
		InsertDate
	};


	[Serializable]
	public enum SortDirectionType
	{
		Asc,
		Desc
	};


	[Serializable]
	public class Query
	{
		private Int32 _communityID;
		private Int16 _genderMask;
		private Int32 _regionID;
		private Int16 _ageMin;
		private Int16 _ageMax;
		private Int32 _languageMask;
		private SortFieldType _sortField;
		private SortDirectionType _sortDirection;

        
		public Query(Int32 communityID,
			Int16 genderMask,
			Int32 regionID,
			Int16 ageMin,
			Int16 ageMax,
			Int32 languageMask,
			SortFieldType sortField,
			SortDirectionType sortDirection)
		{
			_communityID = communityID;
			_genderMask = genderMask;
			_regionID = regionID;
			_ageMin = ageMin;
			_ageMax = ageMax;
			_languageMask = languageMask;
			_sortField = sortField;
			_sortDirection = sortDirection;
		}


		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public Int32 RegionID
		{
			get
			{
				return _regionID;
			}
		}


		public string GetFilter(string[] regionTokens)
		{
			StringBuilder sb = new StringBuilder();

			if (_genderMask > 0)
			{
				Int32 currentGender = 1;
				for (Int32 i = 1; i < 9; i++)
				{
					if ((_genderMask & currentGender) == currentGender)
					{
						if (sb.Length > 0)
						{
							sb.Append(" and ");
						}

						sb.Append("Gender");
						sb.Append(currentGender);
						sb.Append(" = 1");
					}

					currentGender = currentGender * 2;
				}
			}

			if (_regionID > 0)
			{
				if (sb.Length > 0)
				{
					sb.Append(" and ");
				}
				sb.Append("(RegionIDDepth1 = ");
				sb.Append(_regionID);
                

				for (Int32 regionTokenNum = 0; regionTokenNum < regionTokens.Length; regionTokenNum++)
				{
					sb.Append(" or RegionTokens like '%");
					sb.Append(regionTokens[regionTokenNum]);
					sb.Append("%'");
				}
				sb.Append(")");
			}

			if (_ageMin > 0)
			{
				if (sb.Length > 0)
				{
					sb.Append(" and ");
				}
				sb.Append("BirthDate <= '");
				sb.Append(DateTime.Now.AddYears(-_ageMin).ToString("MM/dd/yyyy"));
				sb.Append("'");
			}

			if (_ageMax > 0)
			{
				if (sb.Length > 0)
				{
					sb.Append(" and ");
				}
				sb.Append("BirthDate >= '");
				sb.Append(DateTime.Now.AddYears(-_ageMax).ToString("MM/dd/yyyy"));
				sb.Append("'");
			}

			if (_languageMask > 0)
			{
				Int32 currentLanguage = 1;
				for (Int32 i = 1; i < 33; i++)
				{
					if ((_languageMask & currentLanguage) == currentLanguage)
					{
						if (sb.Length > 0)
						{
							sb.Append(" and ");
						}

						sb.Append("Language");
						sb.Append(currentLanguage);
						sb.Append(" = 1");
					}

					currentLanguage = currentLanguage * 2;
				}
			}

			return sb.ToString();
		}


        public string GetFilter(string[] regionTokens,bool countrysearch)
        {
            StringBuilder sb = new StringBuilder();

            if (_genderMask > 0)
            {
                Int32 currentGender = 1;
                for (Int32 i = 1; i < 9; i++)
                {
                    if ((_genderMask & currentGender) == currentGender)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }

                        sb.Append("Gender");
                        sb.Append(currentGender);
                        sb.Append(" = 1");
                    }

                    currentGender = currentGender * 2;
                }
            }

            if (_regionID > 0)
            {
                if (sb.Length > 0)
                {
                    sb.Append(" and ");
                }
                sb.Append("(RegionIDDepth1 = ");
                sb.Append(_regionID);

                if (!countrysearch)
                {
                    for (Int32 regionTokenNum = 0; regionTokenNum < regionTokens.Length; regionTokenNum++)
                    {
                        sb.Append(" or RegionTokens like '%");
                        sb.Append(regionTokens[regionTokenNum]);
                        sb.Append("%'");
                    }
                }
                sb.Append(")");
            }

            if (_ageMin > 0)
            {
                if (sb.Length > 0)
                {
                    sb.Append(" and ");
                }
                sb.Append("BirthDate <= '");
                sb.Append(DateTime.Now.AddYears(-_ageMin).ToString("MM/dd/yyyy"));
                sb.Append("'");
            }

            if (_ageMax > 0)
            {
                if (sb.Length > 0)
                {
                    sb.Append(" and ");
                }
                sb.Append("BirthDate >= '");
                sb.Append(DateTime.Now.AddYears(-_ageMax).ToString("MM/dd/yyyy"));
                sb.Append("'");
            }

            if (_languageMask > 0)
            {
                Int32 currentLanguage = 1;
                for (Int32 i = 1; i < 33; i++)
                {
                    if ((_languageMask & currentLanguage) == currentLanguage)
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(" and ");
                        }

                        sb.Append("Language");
                        sb.Append(currentLanguage);
                        sb.Append(" = 1");
                    }

                    currentLanguage = currentLanguage * 2;
                }
            }

            return sb.ToString();
        }


		public string GetSort()
		{
			//hack b/c we are removing LogonDate, it is same as UpdateDate
			if (_sortField == SortFieldType.LogonDate)
			{
				_sortField = SortFieldType.UpdateDate;
			}

			StringBuilder sb = new StringBuilder();
			
			if (_sortField == SortFieldType.Age)
			{
				sb.Append("Birthdate ");
			}
			else
			{
				sb.Append(_sortField.ToString());
				sb.Append(" ");
			}

			sb.Append(_sortDirection.ToString());

			if (_sortField != SortFieldType.UpdateDate)
			{
				sb.Append(", UpdateDate desc");
				
			}

			return sb.ToString();
		}


		public string GetCacheKey()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("MOLResult|c");
			sb.Append(_communityID);

			if (_genderMask > 0)
			{
				sb.Append("|gm");
				sb.Append(_genderMask);
			}

			if (_regionID > 0)
			{
				sb.Append("|r");
				sb.Append(_regionID);
			}

			if (_ageMin > 0)
			{
				sb.Append("|amin");
				sb.Append(_ageMin);
			}

			if (_ageMax > 0)
			{
				sb.Append("|amax");
				sb.Append(_ageMax);
			}

			if (_languageMask > 0)
			{
				sb.Append("|l");
				sb.Append(_languageMask);
			}

			sb.Append("|sf");
			sb.Append(_sortField.ToString());
			sb.Append("|sd");
			sb.Append(_sortDirection.ToString());

			return sb.ToString();
		}
	}
}
