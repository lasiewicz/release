using System;

using Matchnet;


namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public class QueryResult : IValueObject, ICacheable
	{
		private Query _query;
		private Int32[] _memberIDs;
		private Int32 _ttl = 30;
		private Matchnet.CacheItemMode _mode = Matchnet.CacheItemMode.Absolute;
		private Matchnet.CacheItemPriorityLevel _priority = Matchnet.CacheItemPriorityLevel.Normal;

		public QueryResult(Query query, Int32[] memberIDs)
		{
			_query = query;
			_memberIDs = memberIDs;
		}

		
		public Int32[] MemberIDs
		{
			get
			{
				return _memberIDs;
			}
		}

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _ttl;
			}
			set
			{
				_ttl = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _mode;
			}
			set
			{
				_mode = value;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _priority;
			}
			set
			{
				_priority = value;
			}
		}

		public string GetCacheKey()
		{
			return QueryResult.GetCacheKey(_query);
		}

		public static string GetCacheKey(Query query)
		{
			return "R" + query.GetCacheKey();
		}

		#endregion
	}
}
