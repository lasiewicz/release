using System;

namespace Matchnet.MembersOnline.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "MEMBERSONLINE_SVC";
		public const string SERVICE_NAME = "Matchnet.MembersOnline.Service";
		public const string SERVICE_MANAGER_NAME = "MembersOnlineSM";

		private ServiceConstants()
		{
		}
	}
}
