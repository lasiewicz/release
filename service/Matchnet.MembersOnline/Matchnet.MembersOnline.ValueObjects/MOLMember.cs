﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.MembersOnline.ValueObjects
{
    [Serializable]
    public class MOLMember : IValueObject
    {
        public int CommunityId { get; set; }
        public int LastBrandId { get; set; }
        public int MemberId { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}
