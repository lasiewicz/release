using System;
using Matchnet;


namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public class MemberOnlineBase : IValueObject, ICacheable, IReplicable
	{
		private Int32 _ttl = Constants.NULL_INT;
		protected Int32 _communityID;
		protected Int32 _memberID;

		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public int CacheTTLSeconds
		{
			get
			{
				return _ttl;
			}
			set
			{
				_ttl = value;
			}
		}


		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return CacheItemPriorityLevel.Normal;
			}
			set {}
		}


		public static string GetCacheKey(Int32 communityID,
			Int32 memberID)
		{
			return "MOL|" + communityID.ToString() + "|" + memberID.ToString();
		}

		
		public string GetCacheKey()
		{
			return MemberOnlineBase.GetCacheKey(_communityID, _memberID);
		}


		ReplicationPlaceholder Matchnet.IReplicable.GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
	}
}
