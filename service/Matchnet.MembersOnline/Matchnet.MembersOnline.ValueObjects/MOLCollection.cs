using System;
using System.Collections;

namespace Matchnet.MembersOnline.ValueObjects
{
	/// <summary>
	/// Summary description for MOLCollection.
	/// </summary>
	[Serializable]
	public class MOLCollection : IEnumerable
	{
		private Hashtable _communities;

		public MOLCollection(Int32 count)
		{
			_communities = new Hashtable(count);
		}

		public void Add(Int32 communityID, CommunityMOLCollection collection)
		{
			_communities.Add(communityID, collection);
		}

		public Int32 GetTotalCount()
		{
			Int32 count = 0;

			foreach (CommunityMOLCollection memberIDs in _communities.Values)
			{
				count += memberIDs.Count;
			}

			return count;
		}

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _communities.GetEnumerator();
		}

		#endregion
	}
}
