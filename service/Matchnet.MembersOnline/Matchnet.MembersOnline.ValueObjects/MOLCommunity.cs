﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.MembersOnline.ValueObjects
{
    [Serializable]
    public class MOLCommunity : IValueObject, ICacheable, IEnumerable<MOLMember>
    {
        private Dictionary<int, MOLMember> _MOLMembers = new Dictionary<int, MOLMember>();

        //cache fields
        private const string CACHE_KEY_PREFIX = "MOLCommunity";
        private Int32 _cacheTTLSeconds = 3600;
        private CacheItemMode _cacheMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;

        //properties
        public int CommunityId { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        public MOLCommunity()
        {

        }

        public MOLCommunity(int communityId)
        {
            CommunityId = communityId;
        }

        public void AddMember(MOLMember molMember)
        {
            _MOLMembers[molMember.MemberId] = molMember;
        }

        public MOLMember GetMember(int memberId)
        {
            if (_MOLMembers.ContainsKey(memberId))
            {
                return _MOLMembers[memberId];
            }

            return null;
        }

        public bool ContainsMember(int memberId)
        {
            return _MOLMembers.ContainsKey(memberId);
        }

        public int Count()
        {
            return _MOLMembers.Keys.Count();
        }

        #region IEnumerable Members

        public IEnumerator<MOLMember> GetEnumerator()
        {
            return _MOLMembers.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _cacheMode;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return GetCacheKey(CommunityId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCacheKey(int communityId)
        {
            return CACHE_KEY_PREFIX + string.Format(":{0}", communityId);
        }

        #endregion

    }
}
