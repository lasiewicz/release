using System;

using Matchnet;


namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public class MemberOnlineUpdate : MemberOnlineBase
	{
		private string _username;
		private DateTime _birthDate;
		private Int16 _genderMask;
		private Int32 _languageMask;
		private bool _hasPhoto;
		private Int32 _regionID;
		private Int32 _regionIDDepth;
				
		public MemberOnlineUpdate(Int32 communityID,
			Int32 memberID,
			string username,
			DateTime birthDate,
			Int16 genderMask,
			Int32 languageMask,
			bool hasPhoto,
			Int32 regionID,
			Int32 regionIDDepth)
		{
			_communityID = communityID;
			_memberID = memberID;
			_username = username;
			_birthDate = birthDate;
			_genderMask = genderMask;
			_languageMask = languageMask;
			_hasPhoto = hasPhoto;
			_regionID = regionID;
			_regionIDDepth = regionIDDepth;
		}

		
		public string Username
		{
			get
			{
				return _username;
			}
		}


		public DateTime BirthDate
		{
			get
			{
				return _birthDate;
			}
		}


		public Int16 GenderMask
		{
			get
			{
				return _genderMask;
			}
		}


		public Int32 LanguageMask
		{
			get
			{
				return _languageMask;
			}
		}


		public bool HasPhoto
		{
			get
			{
				return _hasPhoto;
			}
		}


		public Int32 RegionID
		{
			get
			{
				return _regionID;
			}
		}


		public Int32 RegionIDDepth
		{
			get
			{
				return _regionIDDepth;
			}
		}


		public new static string GetCacheKey(Int32 communityID,
			Int32 memberID)
		{
			return "MOLUpdate|" + communityID.ToString() + "|" + memberID.ToString();
		}

		
		public new string GetCacheKey()
		{
			return MemberOnlineBase.GetCacheKey(_communityID, _memberID);
		}


		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
	}
}
