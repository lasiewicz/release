using System;

namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public class MemberOnlineAdd : MemberOnlineBase
	{
		private DateTime _dateTime = DateTime.Now;

		public MemberOnlineAdd(Int32 communityID,
			Int32 memberID)
		{
			base._communityID = communityID;
			base._memberID = memberID;
		}


		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}


		public new static string GetCacheKey(Int32 communityID,
			Int32 memberID)
		{
			return "MOLAdd|" + communityID.ToString() + "|" + memberID.ToString();
		}

		
		public new string GetCacheKey()
		{
			return MemberOnlineBase.GetCacheKey(_communityID, _memberID);
		}


		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
	}
}
