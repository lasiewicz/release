using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.BusinessLogic;


namespace Matchnet.MembersOnline.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button btnGetAll;
		private System.Windows.Forms.Button btnRemove;
        private TextBox textBox1;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnGetAll = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 71);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "add()";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(90, 71);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "IsOnline()";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(170, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "search()";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(170, 119);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "misc test";
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnGetAll
            // 
            this.btnGetAll.Location = new System.Drawing.Point(90, 119);
            this.btnGetAll.Name = "btnGetAll";
            this.btnGetAll.Size = new System.Drawing.Size(75, 23);
            this.btnGetAll.TabIndex = 4;
            this.btnGetAll.Text = "getAll()";
            this.btnGetAll.Click += new System.EventHandler(this.btnGetAll_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(10, 119);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "remove()";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(10, 27);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(135, 20);
            this.textBox1.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnGetAll);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			MembersOnlineSA.Instance.Add(1,Int32.Parse(textBox1.Text));
		}


		private void button2_Click(object sender, System.EventArgs e)
		{
			Console.WriteLine(MembersOnlineSA.Instance.IsMemberOnline(12, 23248462));
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			ArrayList memberIDList = MembersOnlineSA.Instance.GetMemberIDs("foo", 12, 0, 0, 0, 0, 0, SortFieldType.UpdateDate, SortDirectionType.Desc, 0, 11, Constants.NULL_INT).ToArrayList();
			Console.WriteLine(memberIDList.Count);	
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			GeoBox.Location location = GeoBox.CreateLocation(-2.057430,
				.594996);

			Console.WriteLine(Matchnet.MembersOnline.BusinessLogic.GeoBox.BestPhaseInGrid(location, 0));

			foreach (string boxName in location.Grids[0].BoxNames)
			{
				Console.WriteLine(boxName);
			}

			DataTable dt = new DataTable();
			dt.Columns.Add("foo", typeof(string));
			dt.Rows.Add(new object[]{"a"});
			dt.Rows.Add(new object[]{"b"});
			dt.Rows.Add(new object[]{"c"});
			dt.Rows.Add(new object[]{"abc"});

			DataView dv = new DataView(dt, "foo like '%c%'", "", DataViewRowState.CurrentRows);
			Console.WriteLine(dv.Count);


			/*
			SqlConnection conn = new SqlConnection("Server=devsql01;Database=brMember1;Integrated Security=SSPI;");
			SqlCommand cmd = new SqlCommand("select top 100 * from ( select distinct MemberID from MemberAttributeDate where AttributeGroupID in ( select AttributeGroupID from mnSystem..AttributeGroup where GroupID < 20 ) ) a" , conn);
			conn.Open();

			DataTable dt = new DataTable();

			try
			{
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				da.Fill(dt);
			}
			finally
			{
				conn.Close();
			}
		
			foreach (DataRow row in dt.Rows)
			{
				MembersOnlineSA.Instance.Add(17, Convert.ToInt32(row["MemberID"]));
			}

			Console.WriteLine("done");
			*/
		}

		private void btnRemove_Click(object sender, System.EventArgs e)
		{
			MembersOnlineSA.Instance.Remove(1, 23248462);
		}

		private void btnGetAll_Click(object sender, System.EventArgs e)
		{
			foreach (DictionaryEntry de in MembersOnlineSA.Instance.GetMembersOnline())
			{
				Int32 communityID = (Int32) de.Key;
				CommunityMOLCollection memberIDs = (CommunityMOLCollection) de.Value;
				foreach (Int32 memberID in memberIDs)
				{
					Console.WriteLine(communityID + ": " + memberID);
				}
			}
		}
	}
}
