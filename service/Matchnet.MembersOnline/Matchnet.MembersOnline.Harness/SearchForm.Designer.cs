﻿namespace Matchnet.MembersOnline.Harness
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.chkUseSA = new System.Windows.Forms.CheckBox();
            this.lbl = new System.Windows.Forms.Label();
            this.txtURI = new System.Windows.Forms.TextBox();
            this.txtCommunityID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGenderMask = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAgeMin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAgeMax = new System.Windows.Forms.TextBox();
            this.txtRegionID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLanguageMask = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnGetMOL = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMemberURI = new System.Windows.Forms.TextBox();
            this.pnlStatus = new System.Windows.Forms.Panel();
            this.rtfStatus = new System.Windows.Forms.RichTextBox();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnFind = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(34, 268);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(984, 571);
            this.dataGridView1.TabIndex = 0;
            // 
            // chkUseSA
            // 
            this.chkUseSA.AutoSize = true;
            this.chkUseSA.Location = new System.Drawing.Point(500, 109);
            this.chkUseSA.Name = "chkUseSA";
            this.chkUseSA.Size = new System.Drawing.Size(62, 17);
            this.chkUseSA.TabIndex = 17;
            this.chkUseSA.Text = "Use SA";
            this.chkUseSA.UseVisualStyleBackColor = true;
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Location = new System.Drawing.Point(31, 113);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(52, 13);
            this.lbl.TabIndex = 16;
            this.lbl.Text = "MOL URI";
            // 
            // txtURI
            // 
            this.txtURI.Location = new System.Drawing.Point(107, 110);
            this.txtURI.Name = "txtURI";
            this.txtURI.Size = new System.Drawing.Size(363, 20);
            this.txtURI.TabIndex = 15;
            // 
            // txtCommunityID
            // 
            this.txtCommunityID.Location = new System.Drawing.Point(107, 12);
            this.txtCommunityID.Name = "txtCommunityID";
            this.txtCommunityID.Size = new System.Drawing.Size(87, 20);
            this.txtCommunityID.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "CommunityID";
            // 
            // txtGenderMask
            // 
            this.txtGenderMask.Location = new System.Drawing.Point(355, 12);
            this.txtGenderMask.Name = "txtGenderMask";
            this.txtGenderMask.Size = new System.Drawing.Size(86, 20);
            this.txtGenderMask.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(281, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "GenderMask";
            // 
            // txtAgeMin
            // 
            this.txtAgeMin.Location = new System.Drawing.Point(535, 12);
            this.txtAgeMin.Name = "txtAgeMin";
            this.txtAgeMin.Size = new System.Drawing.Size(60, 20);
            this.txtAgeMin.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(497, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Age :";
            // 
            // txtAgeMax
            // 
            this.txtAgeMax.Location = new System.Drawing.Point(636, 12);
            this.txtAgeMax.Name = "txtAgeMax";
            this.txtAgeMax.Size = new System.Drawing.Size(60, 20);
            this.txtAgeMax.TabIndex = 22;
            // 
            // txtRegionID
            // 
            this.txtRegionID.Location = new System.Drawing.Point(819, 9);
            this.txtRegionID.Name = "txtRegionID";
            this.txtRegionID.Size = new System.Drawing.Size(86, 20);
            this.txtRegionID.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(745, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "RegionID";
            // 
            // txtLanguageMask
            // 
            this.txtLanguageMask.Location = new System.Drawing.Point(108, 38);
            this.txtLanguageMask.Name = "txtLanguageMask";
            this.txtLanguageMask.Size = new System.Drawing.Size(86, 20);
            this.txtLanguageMask.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "LanguageMask";
            // 
            // btnGetMOL
            // 
            this.btnGetMOL.Location = new System.Drawing.Point(589, 106);
            this.btnGetMOL.Name = "btnGetMOL";
            this.btnGetMOL.Size = new System.Drawing.Size(107, 27);
            this.btnGetMOL.TabIndex = 27;
            this.btnGetMOL.Text = "Get MOL";
            this.btnGetMOL.UseVisualStyleBackColor = true;
            this.btnGetMOL.Click += new System.EventHandler(this.btnGetMOL_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Member URI";
            // 
            // txtMemberURI
            // 
            this.txtMemberURI.Location = new System.Drawing.Point(108, 145);
            this.txtMemberURI.Name = "txtMemberURI";
            this.txtMemberURI.Size = new System.Drawing.Size(363, 20);
            this.txtMemberURI.TabIndex = 28;
            // 
            // pnlStatus
            // 
            this.pnlStatus.Location = new System.Drawing.Point(576, 150);
            this.pnlStatus.Name = "pnlStatus";
            this.pnlStatus.Size = new System.Drawing.Size(355, 43);
            this.pnlStatus.TabIndex = 30;
            // 
            // rtfStatus
            // 
            this.rtfStatus.Location = new System.Drawing.Point(34, 207);
            this.rtfStatus.Name = "rtfStatus";
            this.rtfStatus.Size = new System.Drawing.Size(474, 55);
            this.rtfStatus.TabIndex = 31;
            this.rtfStatus.Text = "";
            // 
            // txtMemberID
            // 
            this.txtMemberID.Location = new System.Drawing.Point(659, 231);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(87, 20);
            this.txtMemberID.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(571, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "MmeberID";
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(772, 227);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(107, 27);
            this.btnFind.TabIndex = 34;
            this.btnFind.Text = "Find Member";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 866);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.txtMemberID);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.rtfStatus);
            this.Controls.Add(this.pnlStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMemberURI);
            this.Controls.Add(this.btnGetMOL);
            this.Controls.Add(this.txtLanguageMask);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtRegionID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAgeMax);
            this.Controls.Add(this.txtAgeMin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtGenderMask);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkUseSA);
            this.Controls.Add(this.lbl);
            this.Controls.Add(this.txtURI);
            this.Controls.Add(this.txtCommunityID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "SearchForm";
            this.Text = "SearchForm";
            this.Load += new System.EventHandler(this.SearchForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox chkUseSA;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.TextBox txtURI;
        private System.Windows.Forms.TextBox txtCommunityID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGenderMask;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAgeMin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAgeMax;
        private System.Windows.Forms.TextBox txtRegionID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLanguageMask;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnGetMOL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMemberURI;
        private System.Windows.Forms.Panel pnlStatus;
        private System.Windows.Forms.RichTextBox rtfStatus;
        private System.Windows.Forms.TextBox txtMemberID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnFind;
    }
}