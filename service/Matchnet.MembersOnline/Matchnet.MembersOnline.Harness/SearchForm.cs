﻿using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

using Matchnet.MembersOnline.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceDefinitions;
using Matchnet.MembersOnline.BusinessLogic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
namespace Matchnet.MembersOnline.Harness
{
    public partial class SearchForm : Form
    {
        #region UI Properties
        List<MOLMember> _members = null;
        int CommunityID
        {
            get
            {
                try
                {
                    return Int32.Parse(txtCommunityID.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Enter CommunityID - Integer!");

                }
            }


        }
        int RegionID
        {
            get
            {
                try
                {
                    return Int32.Parse(txtRegionID.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Enter RegionID - Integer!");

                }
            }


        }
        short AgeMin
        {
            get
            {
                try
                {
                    return short.Parse(txtAgeMin.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Enter AgeMin - Integer!");

                }
            }


        }
        short AgeMax
        {
            get
            {
                try
                {
                    return short.Parse(txtAgeMax.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Enter AgeMax - Integer!");

                }
            }


        }
        int LanguageMask
        {
            get
            {
                try
                {
                    return Int32.Parse(txtLanguageMask.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Enter LanguageMask - Integer!");

                }
            }


        }
        short GenderMask
        {
            get
            {
                try
                {
                    return short.Parse(txtGenderMask.Text);
                }
                catch (Exception ex)
                {
                    throw new Exception("Enter GenderMask - Integer!");

                }
            }


        }
        #endregion


        public SearchForm()
        {
            InitializeComponent();
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            txtRegionID.Text = "0";
            txtLanguageMask.Text = "0";
            txtGenderMask.Text = "0";
            txtCommunityID.Text = "3";
            txtAgeMin.Text = "18";
            txtAgeMax.Text = "99";
            txtURI.Text = "tcp://lasvcpresence01:47000/MembersOnlineSM.rem";
            txtMemberURI.Text = "tcp://lasvcmember01:42000/MemberSM.rem";

            

        }


        private IMembersOnlineService getService(string uri)
        {
            try
            {
                return (IMembersOnlineService)Activator.GetObject(typeof(IMembersOnlineService), uri);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private IAdapterConfigurationService getConfigService(string uri)
        {
            try
            {
                return (IAdapterConfigurationService)Activator.GetObject(typeof(IAdapterConfigurationService), uri);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnGetMOL_Click(object sender, EventArgs e)
        {
            
            try{
            List<MOLMember> members=new List<MOLMember>();
             Query query = new Query(CommunityID, GenderMask, RegionID, AgeMin, AgeMax, LanguageMask, SortFieldType.HasPhoto, SortDirectionType.Asc);

            QueryResult res= getService(txtURI.Text).Search(query);
            for (int i = 0; i < res.MemberIDs.Length; i++)
            {
               // Member.ValueObjects.CachedMember cachedmember = getMemberSVC(txtURI.Text).GetCachedMember("", null, res.MemberIDs[i], false, 2);
                byte[] res1 = getMemberSVC(txtMemberURI.Text).GetCachedMemberBytes(System.Environment.MachineName, Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(300)),
               res.MemberIDs[i], false, 1);

                CachedMember cm = new CachedMember(res1, 1);
                Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.PopulateMemberObj(cm);
                MOLMember m = new MOLMember(member, CommunityID);

                m.IsOnline = getService(txtURI.Text).IsMemberOnline(CommunityID, m.MemberID);

                members.Add(m);
            }

            dataGridView1.DataSource = members;
            rtfStatus.Text = "Total:" + res.MemberIDs.Length.ToString();
            _members = members;
                
        }catch(Exception ex)
        {
            MessageBox.Show(ex.Source + "\r\n" + ex.Message);

        }
        }

        private Member.ServiceDefinitions.IMemberService getMemberSVC(string uri)
        {

            try
            {
                return (Member.ServiceDefinitions.IMemberService)Activator.GetObject(typeof(Member.ServiceDefinitions.IMemberService), uri);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            try
            {

                int memberid =Conversion.CInt( txtMemberID.Text);


                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                   int  id = Conversion.CInt(r.Cells[0].Value.ToString());
                   if (memberid == id)
                   {
                       r.Selected = true;
                       r.Visible = true;
                     
                       break;
                   }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "\r\n" + ex.Message);
            }
        }
    }
}
