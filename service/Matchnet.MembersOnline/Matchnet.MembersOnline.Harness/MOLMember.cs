﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
namespace Matchnet.MembersOnline.Harness
{
   public class MOLMember
    {

       private Member.ServiceAdapters.Member _member;
       private int _communityID;
       private int _lastbrandid=0;
       private DateTime _brandLastLogon;
        Content.ValueObjects.BrandConfig.Brand _lastbrand=null;
       public MOLMember(int memberid,int communityid)
       {
           try
           {
               _member = MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
               _communityID = communityid;
               _brandLastLogon = _member.GetLastLogonDate(_communityID, out _lastbrandid);
               _lastbrand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_lastbrandid);
           }
           catch (Exception ex) { }
       }


       public MOLMember(Member.ServiceAdapters.Member member, int communityid)
       {
           try
           {
               _member = member;
               _communityID = communityid;
               _brandLastLogon = _member.GetLastLogonDate(_communityID, out _lastbrandid);
               _lastbrand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_lastbrandid);
           }
           catch (Exception ex) { }
       }
       public int MemberID
       {
           get { try { return _member.MemberID; } catch (Exception ex) { return -1; } }
       }
       public string UserName
       {
           get { try { return _member.GetUserName(_lastbrand); } catch (Exception ex) { return "err"; } }
       }
       public DateTime LastLogonDate
       {
           get { try{return _brandLastLogon;}catch(Exception ex){return new DateTime(1900,1,1);}}
       }

       public int LastBrandID
       {
           get { try{return _lastbrandid;}catch(Exception ex){return -1;} }
       }


       public bool IsOnline { get; set; }
    }
}
