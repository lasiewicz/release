﻿using Matchnet.Configuration.ServiceAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.MembersOnline.BusinessLogic
{
    public static class MOLHelper
    {
        public static bool IsUseMOLRefresherEnabled(int communityId)
        {
            try
            {
                if (communityId > 0)
                    return Convert.ToBoolean(RuntimeSettings.Instance.GetSettingFromSingleton("USE_MOL_REFRESHER", communityId));
                else
                    return Convert.ToBoolean(RuntimeSettings.Instance.GetSettingFromSingleton("USE_MOL_REFRESHER"));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return false;
            }
        }

        public static int GetSwapIntervalSeconds()
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.Instance.GetSettingFromSingleton("MEMBERS_ONLINE_SERVICE_SWAP_INTERVAL_SECONDS"));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return 20;
            }
        }

    }
}
