using System;

using Matchnet;
using Matchnet.MembersOnline.ValueObjects;


namespace Matchnet.MembersOnline.BusinessLogic
{
	public class MemberOnlineCacheItem : MemberOnlineBase, IValueObject, ICacheable
	{
		private Int32 _ttl;

		public MemberOnlineCacheItem(Int32 communityID,
				Int32 memberID,
				Int32 ttl)
		{
			base._communityID = communityID;
			base._memberID = memberID;
			_ttl = ttl;
		}

		public int CacheTTLSeconds
		{
			get
			{
				return _ttl;
			}
			set
			{
				_ttl = value;
			}
		}


		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return CacheItemPriorityLevel.Normal;
			}
			set {}
		}


		public string GetCacheKey()
		{
			return "MOL" + base._communityID.ToString() + "|" + base.MemberID.ToString();
		}
	}
}
