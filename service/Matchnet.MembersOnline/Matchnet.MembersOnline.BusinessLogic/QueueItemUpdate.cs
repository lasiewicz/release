using System;

namespace Matchnet.MembersOnline.BusinessLogic
{
	internal class QueueItemUpdate : QueueItemBase
	{
		private string _username;
		private DateTime _birthDate;
		private Int16 _genderMask;
		private Int32 _languageMask;
		private bool _hasPhoto;
		private Int32 _regionID;
		private Int32 _regionIDDepth1;

		public QueueItemUpdate(Int32 memberID,
			string username,
			DateTime birthDate,
			Int16 genderMask,
			Int32 languageMask,
			bool hasPhoto,
			Int32 regionID,
			Int32 regionIDDepth1)
		{
			base._memberID = memberID;
			_username = username;
			_birthDate = birthDate;
			_genderMask = genderMask;
			_languageMask = languageMask;
			_hasPhoto = hasPhoto;
			_regionID = regionID;
			_regionIDDepth1 = regionIDDepth1;
		}


		public string Username
		{
			get
			{
				return _username;
			}
		}


		public DateTime BirthDate
		{
			get
			{
				return _birthDate;
			}
		}


		public Int16 GenderMask
		{
			get
			{
				return _genderMask;
			}
		}


		public Int32 LanguageMask
		{
			get
			{
				return _languageMask;
			}
		}


		public bool HasPhoto
		{
			get
			{
				return _hasPhoto;
			}
		}


		public Int32 RegionID
		{
			get
			{
				return _regionID;
			}
		}


		public Int32 RegionIDDepth1
		{
			get
			{
				return _regionIDDepth1;
			}
		}
	}
}
