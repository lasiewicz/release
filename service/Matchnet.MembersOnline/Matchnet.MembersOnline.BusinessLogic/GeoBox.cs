using System;

namespace Matchnet.MembersOnline.BusinessLogic 
{
	/// <summary>
	/// stolen from fastpush and modified, the two versions of this need to be combined into a single class somewhere
	/// but there is a crisis right now
	/// 
	/// Generates Geo Boxes - a varied size grid of locations based on long and lat.
	/// Geo Boxes are used to index person's location in a way that conservers on tokens and allows speedy
	/// retreival with minimum query terms and minimum run time cost.
	/// </summary>
	public class GeoBox 
	{

		/// <summary>
		/// Number of phases (box shifts) to create per point in a phase
		/// </summary>
		public const int MAXPHASES = 4;


		private static GeoBox.Config _Cfg = new Config(
			-Math.PI , Math.PI,
			-Math.PI , Math.PI,
			new int [] {1200}
			);

		#region data structures

		/// <summary>
		/// Configuration data for box sizes, their resolution and number of grids
		/// </summary>
		public class Config 
		{		
			// min and max latitude as found in data (+/- 2.55)
			/// <summary>
			/// Maximum Radian degrees latitude
			/// </summary>
			public double LatitudeMin;
			/// <summary>
			/// Minimum Radian degrees latitude
			/// </summary>
			public double LatitudeMax;
			/// <summary>
			/// Minimum Radian degrees longiitude
			/// </summary>
			public double LongitudeMin;
			/// <summary>
			/// Maximum Radian degrees longiitude
			/// </summary>
			public double LongitudeMax;

			// # boxes to gird the earth (more means smaller boxes) in each Grid
			/// <summary>
			/// Number of boxes on this grid. A box / cell on a grid then represents 24000 miles / BoxesPerGrid miles wide.
			/// </summary>
			public int [] BoxesPerGrid;

			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="MinLat">Minimum Latitude in Radians</param>
			/// <param name="MaxLat">Maximum Latitude in Radians</param>
			/// <param name="MinLng">Minimum Longitude in Radians</param>
			/// <param name="MaxLng">Maximum Longitude in Radians</param>
			/// <param name="BoxesPerGrid"></param>
			public Config(double MinLat, double MaxLat, double MinLng, double MaxLng, int [] BoxesPerGrid)
			{
				this.LatitudeMin = MinLat;
				this.LatitudeMax = MaxLat;
				this.LongitudeMin = MinLng;
				this.LongitudeMax = MaxLng;
				this.BoxesPerGrid = BoxesPerGrid;
			}

			/// <summary>
			/// Number of grids in this config
			/// </summary>
			public int GridCount
			{
				get 
				{
					return BoxesPerGrid.Length;
				}
			}
		}


		/// <summary>
		/// Represents a single box on a grid
		/// </summary>
		private class Box 
		{
			public int LatBoxID;
			public int LngBoxID;
			public string Name;

			public override string ToString() 
			{
				return Name;
			}

		}

		/// <summary>
		/// Represents a list of boxes in all phases on a given grid in which the long lat point can be found
		/// </summary>
		public class PhaseBoxes 
		{
			/// <summary>
			/// A list of box names. Box names are unique per the grid and within the grid.
			/// </summary>
			public string [] BoxNames = new string[GeoBox.MAXPHASES];

			/// <summary>
			/// A string representaion of this instance.
			/// </summary>
			/// <returns>a string representing all the box names in this instance</returns>
			public override string ToString() 
			{
				return string.Join(" ", BoxNames);
			}
		} 


		/// <summary>
		/// Represents a location record. A location record contains  grids of various size boxes and in each grid there are a box for each phase.
		/// </summary>
		public class  Location 
		{
			/// <summary>
			/// latitude in radians
			/// </summary>
			public double lat;  
			/// <summary>
			/// longitude in radians
			/// </summary>
			public double lng; 
			
			/// <summary>
			/// latitude normalized to number between 0..1
			/// </summary>
			public double NormalizedLat; 
			/// <summary>
			/// longitude normalized to number between 0..1
			/// </summary>
			public double NormalizedLng;
			
			/// <summary>
			/// A list of Grids, each containing boxes applicable to this long and lat
			/// </summary>
			public PhaseBoxes [] Grids = new PhaseBoxes[_Cfg.BoxesPerGrid.Length];
		}


		#endregion data Structures


		#region Methods

		/// <summary>
		/// Get the current config for the geo boxes
		/// </summary>
		/// <returns>The the GeoBox.Config for this object</returns>
		public static GeoBox.Config GetConfig()
		{
			return _Cfg;
		}


		/// <summary>
		/// Populates the location record with the boxes in the designated grid
		/// </summary>
		/// <param name="loc">The location record to populate</param>
		/// <param name="GridIndex">The zero based grid index to populate</param>
		/// <remarks>The location is passed by ref and populated in place</remarks>
		private static void PopulateGridBoxes(Location loc, int GridIndex) 
		{
			int BoxCountInGrid = _Cfg.BoxesPerGrid[GridIndex];
			PhaseBoxes boxes = new PhaseBoxes();			
			loc.Grids[GridIndex] = boxes;

			for (int PhaseIndex = 0; PhaseIndex < MAXPHASES; PhaseIndex++) 
			{
				Box box = new Box();

				// Are we phased?
				bool IsInPhaseLat = ((PhaseIndex & 1) == 1) ? true : false;
				bool IsInPhaseLng = ((PhaseIndex & 2) == 2) ? true : false;

				// Adjust our percent around the world by half a box for proper phase 
				// Since origins of each phase are shifted up, loc must be shifted down
				double latAdj = loc.NormalizedLat;
				if (IsInPhaseLat) 
				{ 
					latAdj -= ((double)0.5 / (double)BoxCountInGrid);
					if (latAdj < 0.0) latAdj += 1.0;
				}
				double lngAdj = loc.NormalizedLng;
				if (IsInPhaseLng) 
				{
					lngAdj -= ((double)0.5 / (double)BoxCountInGrid);
					if (lngAdj < 0.0) lngAdj += 1.0;
				}

				// Which box are we in
				box.LatBoxID = (int)(latAdj * (double)BoxCountInGrid) % BoxCountInGrid;
				box.LngBoxID = (int)(lngAdj * (double)BoxCountInGrid) % BoxCountInGrid;
				// Generate its name - can use fewer digits on grids w/ fewer boxes
				box.Name = string.Format("B{0}P{1}X{2}Y{3}", GridIndex, PhaseIndex, box.LngBoxID, box.LatBoxID );

				boxes.BoxNames[PhaseIndex] = box.Name;
			}
		}


		/* Populate an already allocated RbLoc object at this lat/lng based on cfg
		 * ??? Most accurate phase per grid not calculated yet */
		/// <summary>
		/// Create a location record populated according to the config
		/// </summary>
		/// <param name="Longitude">Longitude for this location</param>
		/// <param name="Latitude">Latitude for this location</param>
		/// <returns>The location computed from the long &amp; lat given</returns>
		public static Location CreateLocation(double Longitude, double Latitude) 
		{
			Location loc = new Location();

			int GridIndex;
			// Relative position on the grid of the world
			double NormalizedLat = (Latitude - _Cfg.LatitudeMin) / (_Cfg.LatitudeMax - _Cfg.LatitudeMin);
			double NormalizedLng = (Longitude - _Cfg.LongitudeMin) / (_Cfg.LongitudeMax - _Cfg.LongitudeMin);
			// Catch any bad data
			if (NormalizedLat < 0.0 || NormalizedLat >= 1.0) NormalizedLat = 0.0;
			if (NormalizedLng < 0.0 || NormalizedLng >= 1.0) NormalizedLng = 0.0;
			// Load the loc structure with our lat/lng info 
			loc.lat = Latitude; loc.NormalizedLat = NormalizedLat;
			loc.lng = Longitude; loc.NormalizedLng = NormalizedLng;
			// Do every grid, every phase
			for (GridIndex = 0; GridIndex < _Cfg.GridCount; GridIndex++)
			{
				PopulateGridBoxes(loc, GridIndex);
			}
			return loc;
		}


		// Calculate the "best" (most centered) phase based on where ref lands in phase-0 box 
		/// <summary>
		/// Calculates the best phase in a single grid
		/// </summary>
		/// <param name="loc">A location record with grids populated in it</param>
		/// <param name="GridIndex">The zero based index of the grid from which the best phase is to be returned</param>
		/// <returns>The zero based index of the best phase for this grid and in this location record</returns>
		public static int BestPhaseInGrid(Location loc, int GridIndex) 
		{
			int BoxCountInGrid = _Cfg.BoxesPerGrid[GridIndex];	
			double normBoxSize = (double)1.0 / (double)BoxCountInGrid;
			// Our normalized (0-100%) position on the earth
			double normLatRef = loc.NormalizedLat;
			double normLngRef = loc.NormalizedLng;
			// The Phase-0 box that contains this reference location
			int latBoxId = (int)(normLatRef * (double)BoxCountInGrid) % BoxCountInGrid;
			int lngBoxId = (int)(normLngRef * (double)BoxCountInGrid) % BoxCountInGrid;
			// The normalized lat long of the origin of the Phase-0 box
			double normLatBox = (double)latBoxId / (double)BoxCountInGrid;
			double normLngBox = (double)lngBoxId / (double)BoxCountInGrid;
			// The quartile within the box where the reference point lands
			int qLatRef = (int)((normLatRef - normLatBox) * 4.0 / normBoxSize);
			int qLngRef = (int)((normLngRef - normLngBox) * 4.0 / normBoxSize);
			// Choose best phase by quartile:  if (0,3) then phase shift else if (1,2) then no phase shift
			int latPhase = (qLatRef == 2 || qLatRef == 3) ? 0x0 : 0x1;
			int lngPhase = (qLngRef == 2 || qLngRef == 3) ? 0x0 : 0x2;
			int PhaseIndex = (latPhase | lngPhase);
			return PhaseIndex;
		}
		

		/// <summary>
		/// Computes the base phase for the given a location recode. The bast phase is the one that the long/lat is most close to it's center.
		/// </summary>
		/// <param name="loc">The location for which to compute the best phases</param>
		/// <returns>An array of best phases on each grid</returns>
		public static string [] BestPhases(Location loc) 
		{
			string [] best = new string[_Cfg.GridCount];
			int GridIndex;

			best[0] = string.Join(" OR ",loc.Grids[0].BoxNames);
			for (GridIndex = 1; GridIndex < _Cfg.GridCount; GridIndex++) 
			{
				int     phaseIndex = BestPhaseInGrid(loc, GridIndex);
				string  phaseName  = loc.Grids[GridIndex].BoxNames[phaseIndex];
				best[GridIndex] = phaseName;
			}
			return best;
		}


		/// <summary>
		/// Computes the base phase for the given long &amp; lat. The bast phase is the one that the long/lat is most close to it's center.
		/// </summary>
		/// <param name="Longitude">The longitude of the point to get the best phase for, in radians</param>
		/// <param name="Latitude">The latitude of the point to get the best phase for, in radians</param>
		/// <returns>An array of best phases on each grid given the long and lat supplied</returns>
		public static string [] BestPhases(double Longitude, double Latitude) 
		{
			Location loc = CreateLocation(Longitude, Latitude);
			return BestPhases(loc);
		}

		#endregion Methods

	}
}
