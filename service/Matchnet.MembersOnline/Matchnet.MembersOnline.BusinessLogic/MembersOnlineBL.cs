using System;
using System.Collections;
using System.Web.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using log4net;

namespace Matchnet.MembersOnline.BusinessLogic
{
	public class MembersOnlineBL
	{
		private readonly Caching.Cache _cache;
		private readonly CacheItemRemovedCallback _removeCallback;
		private Hashtable _communities;

		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;

		public delegate void MemberAddEventHandler();
		public event MemberAddEventHandler MemberAdded;

		public delegate void MemberRemoveEventHandler();
		public event MemberRemoveEventHandler MemberRemoved;

		public delegate void MemberUpdateEventHandler();
		public event MemberUpdateEventHandler MemberUpdated;

		public delegate void MemberStatusRequestEventHandler();
		public event MemberStatusRequestEventHandler MemberStatusRequested;

		public delegate void SearchRequestEventHandler();
		public event SearchRequestEventHandler SearchRequested;

        private static readonly ILog Log = LogManager.GetLogger(typeof(MembersOnlineBL));

		public MembersOnlineBL()
		{
            Log.DebugFormat("Starting BL");
			_cache = Matchnet.Caching.Cache.Instance;
			_removeCallback = new CacheItemRemovedCallback(this.RemoveCallback);
		}


		public void Start()
		{
			try
			{
				_communities = new Hashtable();

				Hashtable communityList = BrandConfigSA.Instance.GetCommunities();
				foreach (Community community in communityList.Values)
				{
					CommunityMembers communityMembers = new CommunityMembers(community.CommunityID);
					_communities.Add(community.CommunityID, communityMembers);
				}

				IDictionaryEnumerator de = _communities.GetEnumerator();
				while (de.MoveNext())
				{
					((CommunityMembers)de.Value).Start();
				}
			}
			catch (Exception ex)
			{
				Stop();
				throw ex;
			}
		}


		public void Stop()
		{
			try
			{
				IDictionaryEnumerator de = _communities.GetEnumerator();
				while (de.MoveNext())
				{
					try
					{
						((CommunityMembers)de.Value).Stop();
					}
					catch (Exception exInner)
					{
						new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Error stopping CommunityMembers", exInner);
					}
				}

				de.Reset();
				while (de.MoveNext())
				{
					try
					{
						((CommunityMembers)de.Value).WaitForStop();
					}
					catch (Exception exInner)
					{
						new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Error waiting for CommunityMembers stop", exInner);
					}
				}
			}
			catch (Exception ex)
			{
				new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Error stopping CommunityMembers", ex);
			}
		}


		public void Add(MemberOnlineAdd memberOnlineAdd)
		{
			Add(memberOnlineAdd, true);
		}


		public void Add(MemberOnlineAdd memberOnlineAdd, bool replicable)
		{
		    var ttl =
		        Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSONLINESVC_MEMBER_TTL", memberOnlineAdd.CommunityID)) -
		        (Int32) DateTime.Now.Subtract(memberOnlineAdd.DateTime).TotalSeconds;

		    if (ttl <= 0)
		    {
                Log.ErrorFormat("Invalid ttl value. ttl:{0} memberId:{1} molDateTime:{2} molCommunityId:{3}", ttl, memberOnlineAdd.MemberID,
                    memberOnlineAdd.DateTime, memberOnlineAdd.CommunityID);
		        ttl = 60;
		        //todo temporarily commented out to troubleshoot why this happens on prod 5/22/13
		        //throw new BLException("MemberOnlineAdd cache ttl cannot be less than 0.");
		    }
            
            // Custom value passed in from the caller. Ignore the default database setting value. e.g. MOS for shorter lifetime.
            if (memberOnlineAdd.CacheTTLSeconds == Constants.NULL_INT)
            {
                memberOnlineAdd.CacheTTLSeconds = ttl;
            }

		    if (_cache.Get(memberOnlineAdd.GetCacheKey()) == null)
		    {
		        getCommunityMembers(memberOnlineAdd.CommunityID).AddMember(memberOnlineAdd.MemberID, memberOnlineAdd.DateTime);
		        MemberAdded();
		    }

		    // If put this statement inside the above if statement, the cache item will expire since it's absolute mode and the member will
		    // appear offline until the next Add gets called.  By putting it outside the above if statement, we are implementing a virtual sliding mechanism
		    _cache.Insert(memberOnlineAdd, _removeCallback);

		    if (replicable)
		    {
		        ReplicationRequested(memberOnlineAdd);
		    }
		}


		public void Remove(MemberOnlineRemove memberOnlineRemove)
		{
			Remove(memberOnlineRemove, true);
		}

			
		public void Remove(MemberOnlineRemove memberOnlineRemove, bool replicable)
		{
			if (_cache.Remove(MemberOnlineBase.GetCacheKey(memberOnlineRemove.CommunityID, memberOnlineRemove.MemberID)) != null)
			{
				getCommunityMembers(memberOnlineRemove.CommunityID).RemoveMember(memberOnlineRemove.MemberID);
				MemberRemoved();
			}

			if (replicable)
			{
				ReplicationRequested(memberOnlineRemove);
			}
		}


		public void Update(MemberOnlineUpdate memberOnlineUpdate)
		{
			Update(memberOnlineUpdate, true);
		}

			
		public void Update(MemberOnlineUpdate memberOnlineUpdate, bool replicable)
		{
			getCommunityMembers(memberOnlineUpdate.CommunityID).UpdateMember(memberOnlineUpdate.MemberID,
				memberOnlineUpdate.Username,
				memberOnlineUpdate.BirthDate,
				memberOnlineUpdate.GenderMask,
				memberOnlineUpdate.LanguageMask,
				memberOnlineUpdate.HasPhoto,
				memberOnlineUpdate.RegionID,
				memberOnlineUpdate.RegionIDDepth);

			if (replicable)
			{
				ReplicationRequested(memberOnlineUpdate);
			}

			MemberUpdated();
		}

		
		public bool IsMemberOnline(Int32 communityID,
			Int32 memberID)
		{
			MemberStatusRequested();
			return _cache.Get(MemberOnlineBase.GetCacheKey(communityID, memberID)) != null;
		}


		public QueryResult Search(Query query)
		{
			SearchRequested();
			return getCommunityMembers(query.CommunityID).Search(query);
		}

		public MOLCollection GetMembersOnline()
		{
			Hashtable communities = BrandConfigSA.Instance.GetCommunities();
			MOLCollection molCollection = new MOLCollection(communities.Count);

			foreach (Community community in communities.Values)
			{
				molCollection.Add(community.CommunityID, getCommunityMembers(community.CommunityID).GetAllMemberIDs());
			}

			return molCollection;
		}

        /// <summary>
        /// This is gets an MOLCommunity which contains a model for member online info
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public MOLCommunity GetMOLCommunity(int communityID)
        {
            CommunityMembers communityMembers = getCommunityMembers(communityID);
            if (communityMembers != null)
            {
                return communityMembers.GetAllMemberIDsAsMOLCommunity();
            }

            return null;
        }

		private CommunityMembers getCommunityMembers(Int32 communityID)
		{
			return _communities[communityID] as CommunityMembers;
		}


		private void RemoveCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
		    try
		    {
			    if (callbackreason == CacheItemRemovedReason.Expired)
			    {
				    MemberOnlineBase item = (MemberOnlineBase)value;
				    getCommunityMembers(item.CommunityID).RemoveMember(item.MemberID);
				    MemberRemoved();
			    }
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("MOL", string.Format("Error in RemoveCallback method. Callback Reason: {3} Cache Key: {2} Exception Message: {0} Stack Trace: {1}", ex.Message, ex.StackTrace, key, callbackreason ));
            }
		}
	}
}
