using System;
using System.Collections;
using System.Data;
using System.Threading;
using System.Text;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;


namespace Matchnet.MembersOnline.BusinessLogic
{
	public class CommunityMembers
	{
		private Int32 _communityID;
		private Queue _queue;
		private Thread[] _queueThreads;
		private ReaderWriterLock _queueLock;
		private DataTable _dtInternal;
		private ReaderWriterLock _dtInternalLock;
		private DataTable _dtExternal;
		private ReaderWriterLock _dtExternalLock;
		private DataView _dvInternal;
		private Thread _threadSwap;
		private bool _runnable = false;

		public CommunityMembers(Int32 communityID)
		{
			_communityID = communityID;

			_queue = new Queue();
			_queueLock = new ReaderWriterLock();

			_dtInternal = new DataTable();
			_dtInternal.Columns.Add("MemberID", typeof(Int32)).Unique = true;
			_dtInternal.Columns.Add("Username", typeof(string));
			_dtInternal.Columns.Add("BirthDate", typeof(DateTime));
			_dtInternal.Columns.Add("Gender", typeof(Int32));

			//dataview filtering does not support bitwise operations so we have to do this
			_dtInternal.Columns.Add("Gender1", typeof(bool));
			_dtInternal.Columns.Add("Gender2", typeof(bool));
			_dtInternal.Columns.Add("Gender4", typeof(bool));
			_dtInternal.Columns.Add("Gender8", typeof(bool));
			_dtInternal.Columns.Add("Gender16", typeof(bool));
			_dtInternal.Columns.Add("Gender32", typeof(bool));
			_dtInternal.Columns.Add("Gender64", typeof(bool));
			_dtInternal.Columns.Add("Gender128", typeof(bool));

			_dtInternal.Columns.Add("Language1", typeof(bool));
			_dtInternal.Columns.Add("Language2", typeof(bool));
			_dtInternal.Columns.Add("Language4", typeof(bool));
			_dtInternal.Columns.Add("Language8", typeof(bool));
			_dtInternal.Columns.Add("Language16", typeof(bool));
			_dtInternal.Columns.Add("Language32", typeof(bool));
			_dtInternal.Columns.Add("Language64", typeof(bool));
			_dtInternal.Columns.Add("Language128", typeof(bool));
			_dtInternal.Columns.Add("Language256", typeof(bool));
			_dtInternal.Columns.Add("Language512", typeof(bool));
			_dtInternal.Columns.Add("Language1024", typeof(bool));
			_dtInternal.Columns.Add("Language2048", typeof(bool));
			_dtInternal.Columns.Add("Language4096", typeof(bool));
			_dtInternal.Columns.Add("Language8192", typeof(bool));
			_dtInternal.Columns.Add("Language16384", typeof(bool));
			_dtInternal.Columns.Add("Language32768", typeof(bool));
			_dtInternal.Columns.Add("Language65536", typeof(bool));
			_dtInternal.Columns.Add("Language131072", typeof(bool));
			_dtInternal.Columns.Add("Language262144", typeof(bool));
			_dtInternal.Columns.Add("Language524288", typeof(bool));
			_dtInternal.Columns.Add("Language1048576", typeof(bool));
			_dtInternal.Columns.Add("Language2097152", typeof(bool));
			_dtInternal.Columns.Add("Language4194304", typeof(bool));
			_dtInternal.Columns.Add("Language8388608", typeof(bool));
			_dtInternal.Columns.Add("Language16777216", typeof(bool));
			_dtInternal.Columns.Add("Language33554432", typeof(bool));
			_dtInternal.Columns.Add("Language67108864", typeof(bool));
			_dtInternal.Columns.Add("Language134217728", typeof(bool));
			_dtInternal.Columns.Add("Language268435456", typeof(bool));
			_dtInternal.Columns.Add("Language536870912", typeof(bool));
			_dtInternal.Columns.Add("Language1073741824", typeof(bool));
			_dtInternal.Columns.Add("Language2147483648", typeof(bool));

			_dtInternal.Columns.Add("HasPhoto", typeof(bool));
			_dtInternal.Columns.Add("RegionTokens", typeof(string));
			_dtInternal.Columns.Add("RegionID", typeof(Int32));
			_dtInternal.Columns.Add("RegionIDDepth1", typeof(Int32));
			_dtInternal.Columns.Add("InsertDate", typeof(DateTime));
			_dtInternal.Columns.Add("UpdateDate", typeof(DateTime));
			_dtInternalLock = new ReaderWriterLock();

			_dvInternal = new DataView(_dtInternal, "", "MemberID", DataViewRowState.CurrentRows);

			_dtExternal = _dtInternal.Clone();
			_dtExternalLock = new ReaderWriterLock();
		}


		public void Start()
		{
			if (!_runnable)
			{
				_runnable = true;

				Int32 queueThreadCount = Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSONLINESVC_QUEUE_THREAD_COUNT", _communityID));
				_queueThreads = new Thread[queueThreadCount];

				for (Int32 threadNum = 0; threadNum < queueThreadCount; threadNum++)
				{
					_queueThreads[threadNum] = new Thread(new ThreadStart(queueCycle));
					_queueThreads[threadNum].Start();
				}

				_threadSwap = new Thread(new ThreadStart(swapCycle));
				_threadSwap.Start();
			}
		}


		public void Stop()
		{
			if (_runnable)
			{
				_runnable = false;
			}
		}


		public void WaitForStop()
		{
			Stop();

			_threadSwap.Join();

			Int32 queueThreadCount = _queueThreads.Length;
			for (Int32 threadNum = 0; threadNum < queueThreadCount; threadNum++)
			{
				_queueThreads[threadNum].Join();
			}
		}


		public void AddMember(Int32 memberID, DateTime dateTime)
		{
			enqueue(new QueueItemAdd(memberID, dateTime));
		}


		public void RemoveMember(Int32 memberID)
		{
			enqueue(new QueueItemRemove(memberID));
		}

		
		public void UpdateMember(Int32 memberID,
			string username,
			DateTime birthDate,
			Int16 genderMask,
			Int32 languageMask,
			bool hasPhoto,
			Int32 regionID,
			Int32 regionIDDepth1)
		{
			enqueue(new QueueItemUpdate(memberID,
				username,
				birthDate,
				genderMask,
				languageMask,
				hasPhoto,
				regionID,
				regionIDDepth1));
		}


		public QueryResult Search(Query query)
		{
            bool countrysearch = false;
            RegionTokens tokens = getRegionTokens(query.RegionID, out countrysearch);
			string filter = query.GetFilter(tokens.Tokens,countrysearch);
			string sort = query.GetSort();

			try
			{
				DataView dv = new DataView(_dtExternal, filter, sort, DataViewRowState.CurrentRows);
			
				Int32 rowCount = dv.Count;
				Int32[] memberIDs = new Int32[rowCount];

				for (Int32 rowNum = 0; rowNum < rowCount; rowNum++)
				{
					memberIDs[rowNum] = Convert.ToInt32(dv[rowNum]["MemberID"]);
				}

				return new QueryResult(query, memberIDs);
			}
			catch (Exception ex)
			{
				throw new BLException("Error executing search.\nfilter: " + filter + "\nsort: " + sort + "\n\n", ex);
			}
		}


		public CommunityMOLCollection GetAllMemberIDs()
		{
			CommunityMOLCollection memberIDs = null;

			_dtExternalLock.AcquireReaderLock(-1);
			try
			{
				Int32 rowCount = _dtExternal.Rows.Count;
				memberIDs = new CommunityMOLCollection(rowCount);

				for (Int32 rowNum = 0; rowNum < rowCount; rowNum++)
				{
					memberIDs.Add(Conversion.CInt(_dtExternal.Rows[rowNum]["MemberID"]));
				}
			}
			finally
			{
				_dtExternalLock.ReleaseLock();
			}

			return memberIDs;
		}

        public MOLCommunity GetAllMemberIDsAsMOLCommunity()
        {
            MOLCommunity molCommunity = new MOLCommunity(_communityID);

            _dtExternalLock.AcquireReaderLock(-1);
            try
            {
                Int32 rowCount = _dtExternal.Rows.Count;

                for (Int32 rowNum = 0; rowNum < rowCount; rowNum++)
                {
                    MOLMember molMember = new MOLMember();
                    molMember.MemberId = Conversion.CInt(_dtExternal.Rows[rowNum]["MemberID"]);
                    molMember.CommunityId = _communityID;
                    molCommunity.AddMember(molMember);
                }
            }
            finally
            {
                _dtExternalLock.ReleaseLock();
            }

            return molCommunity;
        }
		

		private void enqueue(QueueItemBase queueItemBase)
		{
			_queueLock.AcquireWriterLock(-1);
			try
			{
				_queue.Enqueue(queueItemBase);
			}
			finally
			{
				_queueLock.ReleaseLock();
			}
		}


		private void addMember(Int32 memberID)
        {
            String membererrformat = "Error adding member, MemberID {0}, CommunityID {1}";
			try
			{
                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
                    MemberLoadFlags.None);

                Int32 regionID = member.GetAttributeInt(_communityID, Constants.NULL_INT, Constants.NULL_INT, "RegionID", Constants.NULL_INT);

                _dtInternalLock.AcquireWriterLock(-1);
                try
                {
                    _dvInternal.RowFilter = "MemberID = " + memberID.ToString();

                    if (_dvInternal.Count == 0)
                    {
                        int brandID = Constants.NULL_INT;

                        DateTime lastLogonDate = member.GetLastLogonDate(_communityID, out brandID);

                        Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                        if (brandID < 0)
                        {
                            //Get member info from DB. 
                            member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.IngoreSACache);
                            lastLogonDate = member.GetLastLogonDate(_communityID, out brandID);
                            brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                            System.Diagnostics.EventLog.WriteEntry("MOL",
                                                                   string.Format(
                                                                       "Retrieved Brand. MemberID: {0} BrandID: {1} ",
                                                                      member.MemberID.ToString(),
                                                                       (brand != null) ? brand.BrandID.ToString() : ""));
                        }

                        _dtInternal.Rows.Add(rowObjectArray(memberID,
                            member.GetUserName(brand),
                            member.GetAttributeDate(_communityID, Constants.NULL_INT, Constants.NULL_INT, "BirthDate", DateTime.MinValue),
                            (Int16)member.GetAttributeInt(_communityID, Constants.NULL_INT, Constants.NULL_INT, "GenderMask", 0),
                            member.GetAttributeInt(_communityID, Constants.NULL_INT, Constants.NULL_INT, "LanguageMask", Constants.NULL_INT),
                            member.HasApprovedPhoto(_communityID),
                            regionID,
                            RegionSA.Instance.RetrievePopulatedHierarchy(regionID, 2).Depth1RegionID,
                            member.GetInsertDate(_communityID),
                            lastLogonDate));
                    }
				}
				finally
				{
					_dtInternalLock.ReleaseLock();
				}
			}
			catch (Exception ex)
			{
				new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, String.Format(membererrformat,memberID,_communityID), ex);
			}
		}

		private void removeMember(Int32 memberID)
		{
			_dtInternalLock.AcquireWriterLock(-1);
			try
			{
				_dvInternal.RowFilter = "MemberID = " + memberID.ToString();
				if (_dvInternal.Count == 1)
				{
					_dvInternal[0].Delete();
				}
			}
			catch (Exception ex)
			{
                System.Diagnostics.EventLog.WriteEntry("MOL", string.Format("Error while removing member. MemberID: {0} Exception Message: {1} Stack Trace: {2}", memberID, ex.Message, ex.StackTrace));
				new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Error removing member", ex);
			}
			finally
			{
				_dtInternalLock.ReleaseLock();
			}
		}


		private void swapCycle()
		{
			DateTime lastSwapTime = DateTime.Now;
			Int32 swapInterval = getSwapInterval();

			while (_runnable)
			{
				if (DateTime.Now.Subtract(lastSwapTime).TotalSeconds >= swapInterval)
				{
					_dtExternalLock.AcquireWriterLock(-1);
					try
					{
						_dtInternalLock.AcquireWriterLock(-1);
						try
						{
							_dtExternal = _dtInternal.Copy();
						}
						finally
						{
							_dtInternalLock.ReleaseLock();
						}
					}
					catch (Exception ex)
					{
						new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Error swapping datatables.", ex);
					}
					finally
					{
						_dtExternalLock.ReleaseLock();
					}
					swapInterval = getSwapInterval();
					lastSwapTime = DateTime.Now;
				}
				else
				{
					Thread.Sleep(1000);
				}
			}
		}


		private void queueCycle()
		{
			QueueItemBase queueItem = null;
			Int32 failureCount = 0;

			while (_runnable)
			{
				if (queueItem == null)
				{
					_queueLock.AcquireWriterLock(-1);
					try
					{
						if (_queue.Count > 0)
						{
							queueItem = _queue.Dequeue() as QueueItemBase;
						}
					}
					finally
					{
						_queueLock.ReleaseLock();
					}
				}

				if (queueItem != null)
				{
					try
					{
						switch (queueItem.GetType().Name)
						{
							case "QueueItemAdd":
								addMember(queueItem.MemberID);
								break;
							case "QueueItemUpdate":
								//updateMember((QueueItemUpdate)queueItem);
								break;
							case "QueueItemRemove":
								removeMember(queueItem.MemberID);
								break;
						}

						queueItem = null;
						failureCount = 0;
					}
					catch (Exception ex)
					{
						failureCount++;

						new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing internal queue item.", ex);

						if (failureCount == 2)
						{
							queueItem = null;
						}
						else
						{
							Thread.Sleep(1000);
						}
					}
				}
				else
				{
					Thread.Sleep(1000);
				}
			}
		}


		private Int32 getSwapInterval()
		{
			try
			{
				return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSONLINESVC_SWAP_INTERVAL", _communityID));
			}
			catch (Exception ex)
			{
				new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME, "Error retreiving swap interval, defaulting to 30", ex);
				return 30;
			}
		}

		private object[] rowObjectArray(Int32 memberID,
			string username,
			DateTime birthDate,
			Int16 genderMask,
			Int32 languageMask,
			bool hasPhoto,
			Int32 regionID,
			Int32 regionIDDepth1,
			DateTime insertDate,
			DateTime updateDate)
		{
			object[] o = new object[50];

			o[0] = memberID;
			o[1] = username;
			o[2] = birthDate;
			o[3] = genderMask;

			Int32 currentGender = 1;
			for (Int32 i = 4; i < 12; i++)
			{
				o[i] = ((genderMask & currentGender) == currentGender);
				currentGender = currentGender * 2;
			}

			Int32 currentLanguage = 1;
			for (Int32 i = 12; i < 44; i++)
			{
				o[i] = ((languageMask & currentLanguage) == currentLanguage);
				currentLanguage = currentLanguage * 2;
			}

			o[44] = hasPhoto;
			o[45] = getRegionTokenList(regionID);
			o[46] = regionID;
			o[47] = regionIDDepth1;
			o[48] = insertDate;
			o[49] = updateDate;

			return o;
		}

        private RegionTokens getRegionTokens(Int32 regionID)
        {
            RegionTokens regionTokens = Matchnet.Caching.Cache.Instance.Get(RegionTokens.GetCacheKey(regionID)) as RegionTokens;
          
            if (regionTokens == null)
            {
                Region region = RegionSA.Instance.RetrieveRegionByID(regionID, 2); //eng

                if (region == null)
                {
                    new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME,
                        "Unable to load region (regionID: " + regionID.ToString() + ").");
                    return null;
                }


              
                GeoBox.Location location = GeoBox.CreateLocation((double)region.Longitude,
                    (double)region.Latitude);

                regionTokens = new RegionTokens(regionID,
                    GeoBox.BestPhaseInGrid(location, 0),
                    location.Grids[0].BoxNames);

                Matchnet.Caching.Cache.Instance.Add(regionTokens);
            }

            return regionTokens;
        }
		private RegionTokens getRegionTokens(Int32 regionID, out bool countryonlysearch)
		{
			RegionTokens regionTokens = Matchnet.Caching.Cache.Instance.Get(RegionTokens.GetCacheKey(regionID)) as RegionTokens;
            Region region = RegionSA.Instance.RetrieveRegionByID(regionID, 2); //eng
            countryonlysearch = region.Depth == 1;
			if (regionTokens == null)
			{
				if (region == null)
				{
					new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME,
						"Unable to load region (regionID: " + regionID.ToString() + ").");
					return null;
				}

              
				GeoBox.Location location = GeoBox.CreateLocation((double)region.Longitude,
					(double)region.Latitude);

				regionTokens = new RegionTokens(regionID,
					GeoBox.BestPhaseInGrid(location, 0),
					location.Grids[0].BoxNames);

				Matchnet.Caching.Cache.Instance.Add(regionTokens);
			}

			return regionTokens;
		}


		private string getPrimaryRegionToken(Int32 regionID)
		{
			return getRegionTokens(regionID).BestPhaseToken;
		}


		private string getRegionTokenList(Int32 regionID)
		{
			string[] regionTokens = getRegionTokens(regionID).Tokens;

			StringBuilder sb = new StringBuilder();

			for (Int32 regionTokenNum = 0; regionTokenNum < regionTokens.Length; regionTokenNum++)
			{
				sb.Append(regionTokens[regionTokenNum]);
				sb.Append(",");
			}

			return sb.ToString();
		}
	}
}
