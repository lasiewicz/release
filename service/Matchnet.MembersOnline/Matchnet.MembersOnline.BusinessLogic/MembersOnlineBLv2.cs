﻿using log4net;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.MembersOnline.ValueObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matchnet.MembersOnline.BusinessLogic
{
    public class MembersOnlineBLv2
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MembersOnlineBLv2));
        private Dictionary<int, MOLCommunity> _MolCommunityListV1 = new Dictionary<int, MOLCommunity>();
        private Dictionary<int, MOLCommunity> _MolCommunityListV2 = new Dictionary<int, MOLCommunity>();
        private Dictionary<int, molCommunityVersion> _CurrentMolCommunityVersion = new Dictionary<int, molCommunityVersion>();
        private Hashtable _CommunityList;
        private bool _runnable = false;
        private Thread _threadSwap;
        private Matchnet.ICaching _cacheBucket;

        private enum molCommunityVersion
        {
            V1,
            V2
        }

        public MembersOnlineBLv2()
		{
            Log.DebugFormat("Starting MembersOnlineBLv2");
            _cacheBucket = Spark.Caching.MembaseCaching.GetSingleton(RuntimeSettings.Instance.GetMembaseConfigByBucketFromSingleton("searchresults"));
            _CommunityList = BrandConfigSA.Instance.GetCommunities();
		}

        public void Start()
        {
            if (!_runnable)
            {
                _runnable = true;
                _threadSwap = new Thread(() => SwapCycle());
                _threadSwap.Start();
            }
        }


        public void Stop()
        {
            if (_runnable)
            {
                _runnable = false;
            }
        }

        public bool IsMemberOnline(int communityID, int memberID)
        {
            MOLCommunity molCommunity = GetMOLCommunity(communityID);
            if (molCommunity != null && molCommunity.ContainsMember(memberID))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// This is for backwards compatibility, just a collection of memberIDs
        /// </summary>
        /// <returns></returns>
        public MOLCollection GetMembersOnline()
        {
            MOLCollection molCollection = new MOLCollection(_CommunityList.Count);

            foreach (Community community in _CommunityList.Values)
            {
                var molCommunity = GetCurrentMOLCommunityInternal(community.CommunityID);
                CommunityMOLCollection communityMOLCollection = new CommunityMOLCollection(molCommunity.Count());
                foreach (var molmember in molCommunity)
                {
                    communityMOLCollection.Add(molmember.MemberId);
                }
                molCollection.Add(community.CommunityID, communityMOLCollection);

            }

            return molCollection;
        }

        /// <summary>
        /// This is gets an MOLCommunity which contains a model for member online info
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public MOLCommunity GetMOLCommunity(int communityID)
        {
            return GetCurrentMOLCommunityInternal(communityID);
        }

        /// <summary>
        /// Internal method for getting back the current version number to use for a particular community
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        private molCommunityVersion GetCurrentMOLCommunityVersionInternal(int communityID)
        {
            if (_CurrentMolCommunityVersion.ContainsKey(communityID))
            {
                return _CurrentMolCommunityVersion[communityID];
            }
            else
            {
                _CurrentMolCommunityVersion[communityID] = molCommunityVersion.V1;
                return molCommunityVersion.V1;
            }
        }

        /// <summary>
        /// Internal method for getting back the current MOlCommunity object that should be used
        /// </summary>
        /// <returns></returns>
        private MOLCommunity GetCurrentMOLCommunityInternal(int communityID)
        {
            var currentMolCommunities = GetCurrentMOLCommunityListInternal(communityID);
            if (currentMolCommunities != null && currentMolCommunities.ContainsKey(communityID))
            {
                return currentMolCommunities[communityID];
            }

            return null;

        }

        /// <summary>
        /// Internal method for getting back the current MOlCommunity list object that should be used
        /// This is needed to support swapping internal lists for refreshing
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, MOLCommunity> GetCurrentMOLCommunityListInternal(int communityID)
        {
            molCommunityVersion version = GetCurrentMOLCommunityVersionInternal(communityID);
            if (version == molCommunityVersion.V2)
            {
                return _MolCommunityListV2;
            }
            else
            {
                return _MolCommunityListV1;
            }

        }

        /// <summary>
        /// Internal method for getting back the next MOlCommunity list object that should be used
        /// This is needed to support swapping internal lists for refreshing
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, MOLCommunity> GetNextMOLCommunityListInternal(int communityID)
        {
            var version = GetCurrentMOLCommunityVersionInternal(communityID);
            if (version == molCommunityVersion.V2)
            {
                return _MolCommunityListV1;
            }
            else
            {
                return _MolCommunityListV2;
            }
        }
        

        private void SetNextMOLCommunitiesVersionInternal(int communityID)
        {
            var version = GetCurrentMOLCommunityVersionInternal(communityID);
            if (version == molCommunityVersion.V1)
            {
                _CurrentMolCommunityVersion[communityID] = molCommunityVersion.V2;
            }
            else
            {
                _CurrentMolCommunityVersion[communityID] = molCommunityVersion.V1;
            }
        }

        private void SwapCycle()
        {
            while (_runnable)
            {
                try
                {
                    Log.InfoFormat("Starting molV2 swap cycle.");
                    
                    foreach (Community community in _CommunityList.Values)
                    {
                        //get updated list from couchbase
                        MOLCommunity molCommunity = _cacheBucket.Get(MOLCommunity.GetCacheKey(community.CommunityID)) as MOLCommunity;

                        if (molCommunity != null)
                        {
                            bool updateCommunity = true;

                            //verify cached list from refresher is newer
                            DateTime currentMOLUpdatedDate = DateTime.MinValue;
                            DateTime cachedMOLUpdatedDate = molCommunity.LastUpdatedDate;
                            var currentMolCommunityList = GetCurrentMOLCommunityListInternal(community.CommunityID);
                            if (currentMolCommunityList.ContainsKey(community.CommunityID))
                            {
                                currentMOLUpdatedDate = currentMolCommunityList[community.CommunityID].LastUpdatedDate;
                                if (currentMOLUpdatedDate >= cachedMOLUpdatedDate)
                                {
                                    updateCommunity = false;
                                    Log.InfoFormat("molV2 swap cycle: cached data from refresher is older, will not update. community: {0}, currentUpdatedDate: {1}, cachedUpdatedDate: {2}", community.CommunityID, currentMOLUpdatedDate, cachedMOLUpdatedDate);
                                }
                            }

                            //swap
                            if (updateCommunity)
                            {
                                var nextMolCommunityList = GetNextMOLCommunityListInternal(community.CommunityID);
                                nextMolCommunityList[community.CommunityID] = molCommunity;
                                SetNextMOLCommunitiesVersionInternal(community.CommunityID);
                                Log.InfoFormat("molV2 swap cycle: cached data from refresher is newer, will update. community: {0}, new count: {1}, currentUpdatedDate: {2}, cachedUpdatedDate: {3}", community.CommunityID, molCommunity.Count(), currentMOLUpdatedDate, cachedMOLUpdatedDate);
                            }

                        }
                        else
                        {
                            Log.InfoFormat("molV2 swap cycle: cached data from refresher is null. community: {0}", community.CommunityID);
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    Log.ErrorFormat("molV2 swap cycle error. Error: {0}", ex.ToString());
                }

                //pause for next cycle
                int pauseSeconds = MOLHelper.GetSwapIntervalSeconds();
                Log.InfoFormat("Ended molV2 swap cycle. Pause Seconds: {0}", pauseSeconds);
                Thread.Sleep(pauseSeconds * 1000);
            }
        }


    }
}
