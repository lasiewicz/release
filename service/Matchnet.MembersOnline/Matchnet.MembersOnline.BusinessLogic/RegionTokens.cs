using System;

using Matchnet;


namespace Matchnet.MembersOnline.BusinessLogic
{
	public class RegionTokens : IValueObject, ICacheable
	{
		private Int32 _regionID;
		private Int32 _bestPhaseIndex;
		private string[] _tokens;

		public RegionTokens(Int32 regionID,
			Int32 bestPhaseIndex,
			string[] tokens)
		{
			_regionID = regionID;
			_bestPhaseIndex = bestPhaseIndex;
			_tokens = tokens;
		}


		public string BestPhaseToken
		{
			get
			{
				return _tokens[_bestPhaseIndex];
			}
		}


		public string[] Tokens
		{
			get
			{
				return _tokens;
			}
		}

        
		public int CacheTTLSeconds
		{
			get
			{
				return 600;
			}
			set
			{
			}
		}


		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return  Matchnet.CacheItemMode.Sliding;
			}
		}


		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return Matchnet.CacheItemPriorityLevel.Normal;
			}
			set
			{
			}
		}


		public string GetCacheKey()
		{
			return RegionTokens.GetCacheKey(_regionID);
		}


		public static string GetCacheKey(Int32 regionID)
		{
			return "^RegionTokens^" + regionID.ToString();
		}
	}
}
