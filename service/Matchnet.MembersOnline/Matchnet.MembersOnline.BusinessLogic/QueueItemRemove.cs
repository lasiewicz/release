using System;

namespace Matchnet.MembersOnline.BusinessLogic
{
	internal class QueueItemRemove : QueueItemBase
	{
		public QueueItemRemove(Int32 memberID)
		{
			base._memberID = memberID;
		}
	}
}
