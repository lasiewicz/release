using System;

namespace Matchnet.MembersOnline.BusinessLogic
{
	internal class QueueItemAdd : QueueItemBase
	{
		private DateTime _dateTime;

		public QueueItemAdd(Int32 memberID, DateTime dateTime)
		{
			base._memberID = memberID;
			_dateTime = dateTime;
		}


		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}
	}
}
