using System;

namespace Matchnet.MembersOnline.BusinessLogic
{
	internal class QueueItemBase
	{
		protected Int32 _memberID;

		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}
	}
}
