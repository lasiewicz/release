#region

using System;
using Matchnet.Exceptions;
using Matchnet.MembersOnline.ServiceManagers;
using Matchnet.MembersOnline.ValueObjects;
using log4net.Config;

#endregion

namespace Matchnet.MembersOnline.Service
{
    public class MembersOnline : RemotingServices.RemotingServiceBase
    {
        private MembersOnlineSM _membersOnlineSM;
        private System.ComponentModel.Container components = null;

        public MembersOnline()
        {
            InitializeComponent();
            XmlConfigurator.Configure();
        }

        // The main entry point for the process
        private static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] {new MembersOnline()};
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(Matchnet.MembersOnline.ValueObjects.ServiceConstants.SERVICE_NAME,
                                         "An unhandled exception occured.", e.ExceptionObject as Exception);
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig =
                Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(
                    ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _membersOnlineSM = new MembersOnlineSM();
                base.RegisterServiceManager(_membersOnlineSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(this.ServiceName, "Error occurred when starting the Windows Service",
                                                   ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            if (_membersOnlineSM != null)
            {
                _membersOnlineSM.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}