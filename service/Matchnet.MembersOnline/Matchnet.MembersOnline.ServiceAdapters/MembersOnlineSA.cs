using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.MembersOnline.ValueObjects;
using Matchnet.MembersOnline.ServiceDefinitions;
using Matchnet.Configuration;
using Matchnet.Configuration.ValueObjects;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using log4net;
using System.Diagnostics;
using log4net.Config;

namespace Matchnet.MembersOnline.ServiceAdapters
{
	public class MembersOnlineSA : SABase
	{
		private const string SEARCH_RESULTS_NAMED_CACHE = "searchresults";
		public static readonly MembersOnlineSA Instance = new MembersOnlineSA();
        private Hashtable _GetValueObjectLock = new Hashtable();
		private Matchnet.Caching.Cache _cache;
        private Matchnet.ICaching _cacheWebBucket;
        private bool _isMemberOnlinePerformnceOutputEnabled = false;

		private MembersOnlineSA()
		{
			_cache = Matchnet.Caching.Cache.Instance;
            _cacheWebBucket = Spark.Caching.MembaseCaching.GetSingleton(RuntimeSettings.Instance.GetMembaseConfigByBucketFromSingleton("searchresults"));
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSONLINESVC_SA_CONNECTION_LIMIT"));
		}


        #region Integrated Presence
        public bool IsMemberOnline(Int32 communityID, Int32 memberID)
        {
            if (_isMemberOnlinePerformnceOutputEnabled)
            {
                return IsMemberOnlineWithPerformanceOutput(communityID, memberID);
            }
            else if (IsMemberOnlineCacheEnabled(communityID))
            {
                //short term cache mol
                return IsMemberOnlineV2(communityID, memberID);
            }
            else
            {
                //no cache mol
                return IsMemberOnlineV1(communityID, memberID);
            }

        }

        public bool IsMemberOnlineWithPerformanceOutput(Int32 communityID, Int32 memberID)
        {
            var stopwatch = new Stopwatch();
            bool isOnline = false;
            stopwatch.Start();
            if (IsMemberOnlineCacheEnabled(communityID))
            {
                isOnline = IsMemberOnlineV2(communityID, memberID);
            }
            else
            {
                isOnline = IsMemberOnlineV1(communityID, memberID);
            }
            stopwatch.Stop();
            System.Diagnostics.Trace.WriteLine(string.Format("IsMemberOnline MT processing time ticks: {0}, ismemberonlinecacheenabled: {1}", stopwatch.Elapsed.Ticks, IsMemberOnlineCacheEnabled(communityID)));

            var stopwatch2 = new Stopwatch();
            stopwatch2.Start();
            MOLCommunity molC = _cacheWebBucket.Get(MOLCommunity.GetCacheKey(communityID)) as MOLCommunity;
            stopwatch2.Stop();
            System.Diagnostics.Trace.WriteLine(string.Format("IsMemberOnline Couchbase processing time ticks: {0}, molCommunity found: {1}", stopwatch2.Elapsed.Ticks, (molC != null).ToString()));
            
            return isOnline;
            
        }

        public bool IsMemberOnlineV2(Int32 communityID, Int32 memberID)
        {
            try
            {
                MOLCommunity molCommunity = GetMOLCommunity(communityID, false);
                if (molCommunity != null && molCommunity.ContainsMember(memberID))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while retreiving member online status.", ex));
            }
        }

        /// <summary>
        /// gets a list of members who are online for a community
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public MOLCommunity GetMOLCommunity(int communityID, bool ignoreCache)
        {
            string uri = "";

            try
            {
                MOLCommunity molCommunity = null;
                if (!ignoreCache)
                {
                    molCommunity = _cache.Get(MOLCommunity.GetCacheKey(communityID)) as MOLCommunity;
                }

                if (molCommunity == null)
                {
                    lock (GetServiceVOLock(MOLCommunity.GetCacheKey(communityID)))
                    {
                        molCommunity = _cache.Get(MOLCommunity.GetCacheKey(communityID)) as MOLCommunity;
                        if (molCommunity == null)
                        {
                            uri = getServiceManagerUri();
                            base.Checkout(uri);
                            try
                            {
                                molCommunity = getService(uri).GetMOLCommunity(communityID);
                            }
                            finally
                            {
                                base.Checkin(uri);
                            }

                            if (molCommunity != null)
                            {
                                molCommunity.CacheTTLSeconds = GetMOLCommunitySASeconds(communityID);
                                _cache.Insert(molCommunity);
                            }
                        }
                    }
                }

                return molCommunity;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to retrieve list of members online. (uri: " + uri + ")", ex));
            }
        }

        public int GetMOLCount(int communityID, bool ignoreCache)
        {
            MOLCommunity molCommunity = GetMOLCommunity(communityID, ignoreCache);
            if (molCommunity != null)
            {
                return molCommunity.Count();
            }
            else
            {
                return 0;
            }
        }

        //public MOLQueryResult GetMemberIDsE2(int communityID, int siteID, int startRow, Int32 pageSize, int memberID, int genderMask, int regionID, int distance, int ageMin, int ageMax, int languageMask, QuerySorting sorting)
        //{
        //    //NOTE: We will need to update FWS/API to call Matchnet.Search for MOL Searching to use E2 directly, can't create this passover within MembersOnlineSA due to assembly signing issues.
        //    MOLQueryResult molQueryResults = null;

        //    SearchPreferenceCollection prefs = new SearchPreferenceCollection();
        //    prefs.Add("CommunityID", communityID.ToString());
        //    prefs.Add("GenderMask", genderMask.ToString());
        //    prefs.Add("RegionID", regionID.ToString());
        //    prefs.Add("AgeMin", ageMin.ToString());
        //    prefs.Add("AgeMax", ageMax.ToString());
        //    prefs.Add("languagemask", languageMask.ToString());
        //    prefs.Add("SearchOrderBy", ((int)sorting).ToString());
        //    prefs.Add("Online", "1");
        //    prefs.Add("SearchRedesign30", "1");
        //    prefs.Add("Distance", distance.ToString());

        //    MatchnetQueryResults matchnetQueryResults = MemberSearchSA.Instance.Search(prefs, communityID, siteID, startRow, pageSize, memberID, Matchnet.Search.Interfaces.SearchEngineType.FAST, Matchnet.Search.Interfaces.SearchType.MembersOnline, Matchnet.Search.Interfaces.SearchEntryPoint.None, false, false, false);
        //    if (matchnetQueryResults != null)
        //    {
        //        var arrMemberIds = matchnetQueryResults.ToArrayList();
        //        molQueryResults = new MOLQueryResult(matchnetQueryResults.MatchesFound);
        //        foreach (var item in arrMemberIds)
        //        {
        //            molQueryResults.AddMemberID((int)item);
        //        }
        //    }

        //    return molQueryResults;
        //}

        #endregion

        #region Pre-integrated MOL

        public bool IsMemberOnlineV1(Int32 communityID, Int32 memberID)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri).IsMemberOnline(communityID, memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while retreiving member online status. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Use when need to override the cache TTL value manually.
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="memberID"></param>
        /// <param name="cacheTTL">In seconds</param>
        public void Add(int communityID, int memberID, int cacheTTL)
        {
            string uri = "";

            try
            {
                if (IsUpdateAllPartitionsEnabled())
                {
                    updateAllPartitions(communityID, memberID, cacheTTL);
                }
                else
                {
                    uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        var memberOnlineAdd = new MemberOnlineAdd(communityID, memberID);
                        if (cacheTTL == Constants.NULL_INT)
                        {
                            getService(uri).Add(memberOnlineAdd);
                        }
                        else
                        {
                            memberOnlineAdd.CacheTTLSeconds = cacheTTL;
                            getService(uri).Add(memberOnlineAdd);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while adding member. (uri: " + uri + ")", ex));
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="memberID"></param>
		public void Add(Int32 communityID, Int32 memberID)
		{
            Add(communityID, memberID, Constants.NULL_INT);
		}

		public void Remove(Int32 communityID, Int32 memberID)
		{
			string uri  = "";

			try
			{
				 if(IsUpdateAllPartitionsEnabled())
                {
                    removeAllPartitions(communityID,memberID);
                }
                 else{
                     uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        getService(uri).Remove(new MemberOnlineRemove(communityID, memberID));
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while removing member. (uri: " + uri + ")", ex));
			} 
		}

        public MOLQueryResult GetMemberIDs(string sessionKey, Int32 communityID, Int16 genderMask, Int32 regionID, Int16 ageMin, Int16 ageMax, Int32 languageMask, SortFieldType sortField, SortDirectionType sortDirection,
                                           Int32 startRow, Int32 pageSize, Int32 memberID)
        {
            return GetMemberIDs(sessionKey, communityID, genderMask, regionID, ageMin, ageMax, languageMask, sortField,
                                sortDirection, startRow, pageSize, memberID, null);
        }

        public MOLQueryResult GetMemberIDs(string sessionKey, Int32 communityID, Int16 genderMask, Int32 regionID, Int16 ageMin, Int16 ageMax, Int32 languageMask, SortFieldType sortField, SortDirectionType sortDirection,
                                           Int32 startRow, Int32 pageSize, Int32 memberID, List<HotListCategory> filterOutHotlist)
		{
			bool isProfileBlockingEnabled = false;
			isProfileBlockingEnabled = (memberID != Constants.NULL_INT && 
				Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", communityID)));

			QueryResult queryResult = null;
			QueryResultSession queryResultSession = null;
			Query query = new Query(communityID,
				genderMask,
				regionID,
				ageMin,
				ageMax,
				languageMask,
				sortField,
				sortDirection);

			string keyGeneric = QueryResult.GetCacheKey(query);
			string keySpecific = QueryResultSession.GetCacheKey(sessionKey, query);

			// if profile blocking is enabled, check the distributed cache first
			// if startRow == 0, this means we are on the first page. whenever a request to the first page is made, skip the cache and get the
			// new MOL from the BL.
			if(isProfileBlockingEnabled && startRow > 0)
			{
				ArrayList filteredMemberIDs =  Matchnet.DistributedCaching11.ServiceAdapters.DistributedCachingSA.Instance.GetArrayList(GetMemberSpecificCacheKey(keySpecific), SEARCH_RESULTS_NAMED_CACHE);

				if(filteredMemberIDs != null)
				{
					queryResultSession = new QueryResultSession(sessionKey, query, (int[])filteredMemberIDs.ToArray(typeof(int)));
				}
			}
            
			// if not in our distributed cache, go through the old steps
			if(queryResultSession == null)
			{
				// if we are not on the first page of MOL, check the cache for the session-specific cache item
				if (startRow > 0)
				{
					queryResultSession = _cache.Get(keySpecific) as QueryResultSession;
					if (queryResultSession != null)
					{
						queryResult = new QueryResult(queryResultSession.Query, queryResultSession.MemberIDs);
					}
				}

				// if no session-specific cache item is present, check the cache for the cache item that originally came from BL
				// if no cache hit, contact BL for new results and cache the item under both keys; session-specific & query-specific only
				if (queryResult == null)
				{
					queryResult = _cache.Get(keyGeneric) as QueryResult;

					if (queryResult == null)
					{
						queryResult = Search(query);
						_cache.Add(queryResult);
					}

					queryResultSession = new QueryResultSession(sessionKey, query, queryResult.MemberIDs);
					_cache.Add(queryResultSession);
				}

				// if profile blocking is enabled, run the results through the exclusion list and throw it in the distributed cache
				if(isProfileBlockingEnabled)
				{
					ArrayList filteredMemberIDs = RemoveExcludedMembers(queryResultSession, communityID, memberID);
					Matchnet.DistributedCaching11.ServiceAdapters.DistributedCachingSA.Instance.Put(GetMemberSpecificCacheKey(queryResultSession.GetCacheKey()), filteredMemberIDs, SEARCH_RESULTS_NAMED_CACHE);

					queryResultSession = new QueryResultSession(sessionKey, queryResultSession.Query, (int[])filteredMemberIDs.ToArray(typeof(int)));
				}

                //dynamic filter
                if (filterOutHotlist != null && filterOutHotlist.Count > 0)
                {
                    ArrayList filteredMemberIDs = null;
                    if (filterOutHotlist.Count == 1)
                    {
                        filteredMemberIDs = RemoveHotlistedMembers(queryResultSession, communityID, memberID,
                                                                             filterOutHotlist[0]);
                    }
                    else
                    {
                        filteredMemberIDs = RemoveHotlistedMembers(queryResultSession, communityID, memberID,
                                                                             filterOutHotlist);
                    }

                    queryResultSession = new QueryResultSession(sessionKey, queryResultSession.Query, (int[])filteredMemberIDs.ToArray(typeof(int)));
                }
			}

            MOLQueryResult molresult = new MOLQueryResult( queryResultSession.MemberIDs.Length );

			for( Int32 i = 0; i < pageSize; i++ ) 
			{
				Int32 rowNum = i + startRow;
				if(rowNum >= molresult.TotalMembersOnline )
				{
					break;
				}
				molresult.AddMemberID( queryResultSession.MemberIDs[rowNum]);
			}

			return molresult;
		}

        private void updateAllPartitions(int communityID, int memberID, int cacheTTL)
        {
          
            string uri = "";
            List<string> uriList = getServiceManagerUriList();
            foreach (string s in uriList)
            {
                uri = s;
                base.Checkout(uri);
                try
                {
                    var memberOnlineAdd = new MemberOnlineAdd(communityID, memberID);
                    if (cacheTTL == Constants.NULL_INT)
                    {
                        getService(uri).Add(memberOnlineAdd);
                    }
                    else
                    {
                        memberOnlineAdd.CacheTTLSeconds = cacheTTL;
                        getService(uri).Add(memberOnlineAdd);
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
        }

        private void removeAllPartitions(int communityID, int memberID)
        {

            string uri = "";
            List<string> uriList = getServiceManagerUriList();
            foreach (string s in uriList)
            {
                uri = s;
                base.Checkout(uri);
                try
                {
                    getService(uri).Remove(new MemberOnlineRemove(communityID, memberID));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
        }

		/// <summary>
		/// This is actually session-specific since the original cache key is session-specific. Named it this way
		/// to stay consistent with the rest of the projects that use the same paradigm.
		/// </summary>
		/// <param name="originalCacheKey"></param>
		/// <returns></returns>
		private string GetMemberSpecificCacheKey(string originalCacheKey)
		{			
			return "EXCL_MOL_" + originalCacheKey;
		}

		private ArrayList RemoveExcludedMembers(QueryResultSession results, int communityID, int memberID)
		{
		    return RemoveHotlistedMembers(results, communityID, memberID, HotListCategory.ExcludeListInternal);
		}

        private ArrayList RemoveHotlistedMembers(QueryResultSession pQueryResults, int pCommunityID, int pMemberID, Matchnet.List.ValueObjects.HotListCategory hotlistCategory)
        {
            ArrayList filteredList = new ArrayList();
            Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);

            if (list != null && pQueryResults != null)
            {
                for (int i = 0; i < pQueryResults.MemberIDs.Length; i++)
                {
                    if (!list.IsHotListed(hotlistCategory, pCommunityID, pQueryResults.MemberIDs[i]))
                    {
                        filteredList.Add(pQueryResults.MemberIDs[i]);
                    }
                }
            }

            return filteredList;
        }

        private ArrayList RemoveHotlistedMembers(QueryResultSession pQueryResults, int pCommunityID, int pMemberID, List<Matchnet.List.ValueObjects.HotListCategory> hotlistCategoryList)
        {
            ArrayList filteredList = new ArrayList();
            Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(pMemberID);

            if (list != null && pQueryResults != null)
            {
                for (int i = 0; i < pQueryResults.MemberIDs.Length; i++)
                {
                    bool hotlisted = false;
                    foreach (HotListCategory hotlistCategory in hotlistCategoryList)
                    {
                        if (!list.IsHotListed(hotlistCategory, pCommunityID, pQueryResults.MemberIDs[i]))
                        {
                            hotlisted = true;
                            break;
                        }
                    }

                    if (!hotlisted)
                    {
                        filteredList.Add(pQueryResults.MemberIDs[i]);
                    }
                }
            }

            return filteredList;
        }

		private QueryResult Search(Query query)
		{
			string uri  = "";

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).Search(query);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to execute search. (uri: " + uri + ")", ex));
			} 
		}

        /// <summary>
        /// OLD - gets a collection of memberIDs for all communities; recommended to use GetMOLCommunity instead
        /// </summary>
        /// <returns></returns>
		public MOLCollection GetMembersOnline()
		{
			string uri  = "";

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).GetMembersOnline();
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("Error occurred while attempting to retrieve list of members online. (uri: " + uri + ")", ex));
			} 
		}

        #endregion

        private IMembersOnlineService getService(string uri)
		{
			try
			{
				return (IMembersOnlineService)Activator.GetObject(typeof(IMembersOnlineService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


        private string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(MembersOnline.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(MembersOnline.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSONLINESVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }


        private List<string> getServiceManagerUriList()
        {
            try
            {
                List<string> uriList = new List<string>();
                PartitionList partitionlist = (PartitionList)_cache.Get(PartitionList.CACHE_KEY);
                if (partitionlist != null)
                {
                    if (partitionlist.Partitions != null && partitionlist.Partitions.Count > 0)
                        return partitionlist.Partitions;
                }

                ArrayList partitions = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartitions(MembersOnline.ValueObjects.ServiceConstants.SERVICE_CONSTANT);
               
                for (int i = 0; i < partitions.Count; i++)
                {
                    ServicePartition p = (ServicePartition)partitions[i];
                    string uri = p.ToUri(MembersOnline.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
                        
                    string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSONLINESVC_SA_HOST_OVERRIDE");
                    if (overrideHostName.Length > 0)
                    {
                        UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                        uri= "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                        uriList.Add(uri);
                        break;
                    }
                    uriList.Add(uri);
                }
                 partitionlist = new PartitionList(uriList);
                _cache.Insert(partitionlist);
                return uriList;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private bool IsUpdateAllPartitionsEnabled()
        {
            try
            {
                return Conversion.CBool( Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_MOL_PARTITIONS_UPDATE"));

            }
            catch (Exception ex)
            { return false; }
        }

        private int GetMOLCommunitySASeconds(int communityID)
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.Instance.GetSettingFromSingleton("MOLCOMMUNITY_SA_TTL_SECONDS", communityID));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return 20;
            }
        }

        private bool IsMemberOnlineCacheEnabled(int communityID)
        {
            try
            {
                if (RuntimeSettings.Instance.GetSettingFromSingleton("USE_MOL_SA_ISMEMBERONLINE_CACHE", communityID) == "true")
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                //probably missing setting
                return false;
            }
        }

        
	}


    [Serializable]
    public class PartitionList : ICacheable
    {

        private Int32 _ttl = 120;
        private Matchnet.CacheItemMode _mode = Matchnet.CacheItemMode.Absolute;
        private Matchnet.CacheItemPriorityLevel _priority = Matchnet.CacheItemPriorityLevel.Normal;
        public const string CACHE_KEY = "MOL_PARTITION_LIST";

        List<string> _partitionList = null;

        public  PartitionList(List<string> partitionList)
        {
            _partitionList = partitionList;
        }

        public List<string> Partitions { get { return _partitionList; } }
        #region ICacheable Members

        public int CacheTTLSeconds
        {
            get
            {
                return _ttl;
            }
            set
            {
                _ttl = value;
            }
        }

        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }

        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _priority;
            }
            set
            {
                _priority = value;
            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

       

        #endregion
    }
}
