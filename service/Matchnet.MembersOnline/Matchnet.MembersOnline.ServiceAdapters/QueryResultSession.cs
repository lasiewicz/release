using System;

using Matchnet;


namespace Matchnet.MembersOnline.ValueObjects
{
	[Serializable]
	public class QueryResultSession : IValueObject, ICacheable
	{
		private string _sessionKey;
		private Query _query;
		private Int32[] _memberIDs;
		private Int32 _ttl = 120;
		private Matchnet.CacheItemMode _mode = Matchnet.CacheItemMode.Sliding;
		private Matchnet.CacheItemPriorityLevel _priority = Matchnet.CacheItemPriorityLevel.Low;

		public QueryResultSession(string sessionKey, Query query, Int32[] memberIDs)
		{
			_sessionKey = sessionKey;
			_query = query;
			_memberIDs = memberIDs;
		}


		public Query Query
		{
			get
			{
				return _query;
			}
		}
		

		public Int32[] MemberIDs
		{
			get
			{
				return _memberIDs;
			}
		}

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _ttl;
			}
			set
			{
				_ttl = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _mode;
			}
			set
			{
				_mode = value;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _priority;
			}
			set
			{
				_priority = value;
			}
		}

		
		public static string GetCacheKey(string sessionKey, Query query)
		{
			return query.GetCacheKey() + "|sid" + sessionKey;
		}

		public string GetCacheKey()
		{
			return QueryResultSession.GetCacheKey(_sessionKey, _query);
		}

		#endregion
	}
}
