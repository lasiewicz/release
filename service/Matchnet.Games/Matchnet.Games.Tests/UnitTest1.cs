using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Matchnet.Games.ServiceAdapters;
using Matchnet.Games.ValueObjects;
using System.Collections;

namespace Matchnet.Games.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        #region Fields (1)

        private TestContext testContextInstance;

        #endregion Fields

        #region Constructors (1)

        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion Constructors

        #region Properties (1)

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion Properties

        #region Methods (1)

        // Public Methods (1) 

        [TestMethod]
        public void TestSendInvitation()
        {
            bool inviteFound = false;
            int initiatingMemberID = 1;
            int recipientID = 2;
            int communityID = 10;
            string gameID = "3";


            GamesSA.Instance.InitiateGame(initiatingMemberID, recipientID, communityID, gameID);
            GameInvites invites = GamesSA.Instance.GetInvites(recipientID, communityID);
            IEnumerator invitesEnum = invites.GetEnumerator();
            while (invitesEnum.MoveNext())
            {
                if (((GameInvite)invitesEnum.Current).SenderID == initiatingMemberID)
                {
                    inviteFound = true;
                    break;
                }
            }
            Assert.IsTrue(inviteFound, "Invitation was not found on the recepient GetInvites list");
        }

        [TestMethod]
        public void TestDeclineInvitation()
        {
            int initiatingMemberID = 3;
            int recipientID = 4;
            int communityID = 10;
            bool inviteFound = false;
            string gameID = "3";

            // Send invitation
            GamesSA.Instance.InitiateGame(initiatingMemberID, recipientID, communityID, gameID);
            // Test if invite exist
            inviteFound = false;
            GameInvites invites = GamesSA.Instance.GetInvites(recipientID, communityID);
            IEnumerator invitesEnum = invites.GetEnumerator();
            GameInvite invite = null;
            while (invitesEnum.MoveNext())
            {
                if (((GameInvite)invitesEnum.Current).SenderID == initiatingMemberID)
                {
                    inviteFound = true;
                    invite = (GameInvite)invitesEnum.Current;
                    break;
                }
            }
            Assert.IsTrue(inviteFound, "Error: Invitation was not found on the recepient GetInvites list");
            // Decline invitation
            GamesSA.Instance.DeclineInvitation(invite);
            // Check decline (removed from the list)
            inviteFound = false;
            invites = GamesSA.Instance.GetInvites(recipientID, communityID);
            invitesEnum = invites.GetEnumerator();
            while (invitesEnum.MoveNext())
            {
                if (((GameInvite)invitesEnum.Current).SenderID == initiatingMemberID)
                {
                    inviteFound = true;
                    break;
                }
            }
            Assert.IsFalse(inviteFound, "Error: Invitation was found on the recepient GetInvites list");
        }

        [TestMethod]
        public void TestJoinGame()
        {
            int initiatingMemberID = 5;
            int recipientID = 6;
            int communityID = 10;
            bool inviteFound = false;
            string gameID = "3";

            // Send invitation
            GamesSA.Instance.InitiateGame(initiatingMemberID, recipientID, communityID, gameID);
            // Test if invite exist
            inviteFound = false;
            GameInvites invites = GamesSA.Instance.GetInvites(recipientID, communityID);
            IEnumerator invitesEnum = invites.GetEnumerator();
            GameInvite invite = null;
            while (invitesEnum.MoveNext())
            {
                if (((GameInvite)invitesEnum.Current).SenderID == initiatingMemberID)
                {
                    inviteFound = true;
                    invite = (GameInvite)invitesEnum.Current;
                    break;
                }
            }
            Assert.IsTrue(inviteFound, "Error: Invitation was not found on the recepient GetInvites list");
            // Decline invitation
            GamesSA.Instance.JoinGame(invite);
            // Check decline (removed from the list)
            inviteFound = false;
            invites = GamesSA.Instance.GetInvites(recipientID, communityID);
            invitesEnum = invites.GetEnumerator();
            while (invitesEnum.MoveNext())
            {
                if (((GameInvite)invitesEnum.Current).SenderID == initiatingMemberID)
                {
                    inviteFound = true;
                    break;
                }
            }
            Assert.IsFalse(inviteFound, "Error: Invitation was found on the recepient GetInvites list");
        }

        [TestMethod]
        public void TestCommunityGamesList()
        {
            int communityID = 10;

            List<ValueObjects.Game> gamesList = GamesSA.Instance.GetCommunityGamesList(communityID);
            Assert.AreNotEqual(0, gamesList.Count, "Returned 0 games");
        }

        #endregion Methods



        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
    }
}
