using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Caching;
using System.Xml.Linq;
using Matchnet.Games.ValueObjects;
using Matchnet.Games.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingClient;
using Matchnet.Exceptions;

namespace Matchnet.Games.ServiceAdapters
{
    public class GamesSA : SABase
    {
        #region Fields (4)

        private Matchnet.Caching.Cache _Cache;
        public static readonly GamesSA Instance = new GamesSA();
        private const string SERVICE_CONSTANT = "GAMES_SVC";
        private const string SERVICE_MANAGER_NAME = "GamesSM";
        private const string GAMES_LIST_URL = "http://static.come2play.net/shared/api/rss/channel_rss_currently_playing.asp?channel_id=";

        #endregion Fields

        #region Constructors (1)

        private GamesSA()
        {
            _Cache = Matchnet.Caching.Cache.Instance;
        }

        #endregion Constructors

        #region Methods (12)

        // Public Methods (7) 

        public void DeclineInvitation(GameInvite gameInvite)
        {
            try
            {	// Decline is effectively the same implementation as joining, we simply need to have the
                // invitation removed from the m/t invitation cache, invoking acceptiInvitation(...) accomplishes this.
                acceptInvitation(gameInvite);

                // Now the last piece is to inform the originating member that their invitation was declined
                // by the recipient.
                //notifyParticipantOfDeclinedInvitation(conversationInvite.ConversationKey);
            }
            catch (Exception ex)
            {
                throw new SAException("Unable to decline the invitation to the Game.", ex);
            }
        }

        /// <summary>
        /// Use this method to retrieve any pending Game Conversations or Invites for the recipient
        /// member and community identified in the parameter list.  Any Game conversation that is
        /// pending this recipient's response (either decline or accept) will be returned in the collection.
        /// </summary>
        /// <param name="recipientMemberID">The member id of the recipient member.</param>
        /// <param name="communityID">The community id for this application context.</param>
        /// <returns>Either a collection of pending invites or null if none exist.</returns>
        public GameInvites GetInvites(int recipientMemberID, int communityID)
        {
            string uri = null;
            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetInvites(recipientMemberID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while attempting to get conversation invites. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to get conversation invites. (uri: " + uri + ")", ex));
            }
        }

        /// <summary>
        /// Called when User A clicks "Send" on their Instant Messenger client. Adds an invitation to a particular 
        /// user's invitation collection
        /// </summary>
        /// <param name="initiatingMemberID">The member ID of user who is trying to send an Instant Message.</param>
        /// <param name="recipientMemberID">The member ID of user that Initiating Member is trying to send a message to.</param>
        /// <param name="communityID">Community that both members belong to.</param>
        /// <param name="externalTransactionID">Optional unique transaction ID supplied by Userplane in order to help handle duplicate invitations.</param>
        /// <returns></returns>
        public Guid InitiateGame(int initiatingMemberID
            , int recipientMemberID
            , int communityID
            , string gameID)
        {
            Guid key = System.Guid.NewGuid();
            GameInvite invite = new GameInvite();
            invite.CommunityID = communityID;
            invite.InviteKey = key;
            invite.InviteDate = DateTime.Now;
            invite.SenderID = initiatingMemberID;
            invite.RecipientID = recipientMemberID;
            invite.GameID = gameID;
            sendInvitation(invite);
            return key;
        }

        public void JoinGame(GameInvite gameInvite)
        {
            try
            {
                acceptInvitation(gameInvite);	// inform the middle-tier that the recipient has joined the conversation.
            }
            catch (Exception ex)
            {
                throw new SAException("Unable to join the Game.", ex);
            }
        }

        public void JoinGame(int recipientMemberID, int initiatingMemberID, int communityID)
        {
            // get invites for this recipient
            GameInvites invites = GetInvites(recipientMemberID, communityID);

            if (invites == null)
            {
                return;
            }

            lock (invites.SyncRoot)
            {
                foreach (GameInvite invite in invites)
                {
                    // look for a match using InitiatingMember/CommunityID
                    if (invite.SenderID.Equals(initiatingMemberID))
                    {
                        // use the invite VO to JoinConversation
                        JoinGame(invite);
                        break;
                    }
                }
            }
        }

        public void Save(GameInvite gameInvite)
        {
            string uri = null;
            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    getService(uri).Save(gameInvite);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while attempting to save a Game Invitation. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to save a Game Invitation. (uri: " + uri + ")", ex));
            }
        }

        public List<ValueObjects.Game> GetCommunityGamesList(int communityID)
        {
            try
            {
                List<Game> list = new List<Game>();
                ValueObjects.Games communityGames =
                    _Cache.Get(Games.ValueObjects.Games.GetCacheKey(communityID)) as ValueObjects.Games;
                if (communityGames == null)
                {
                    string responseString = string.Empty;
                    string url = GAMES_LIST_URL + Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAMES_CHANNEL_ID", communityID);
                    HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                    request.Timeout = 3000;
                    request.KeepAlive = false;
                    request.Method = "GET";
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        using (StreamReader oResponseStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseString = oResponseStream.ReadToEnd();
                            oResponseStream.Close();
                        }
                        response.Close();
                    }
                    //return XDocument.Parse(responseString.Replace("&lt;", "<").Replace("&gt;", ">"));
                    XDocument xdoc = XDocument.Parse(responseString);

                    var q = from c in xdoc.Descendants("item")
                            select new ValueObjects.Game()
                            {
                                GameID = Int32.Parse(c.Elements("game_id").First().Value),
                                GameClassID = Int32.Parse(c.Elements("gameClass_id").First().Value),
                                Title = c.Elements("title").First().Value,
                                ImageURL = c.Elements("image").First().Value,
                                Link = c.Elements("link").First().Value,
                                Description = c.Elements("description").First().Value
                            };
                    communityGames = new ValueObjects.Games() { CommunityID = communityID, GamesList = q.ToList() };
                    _Cache.Add(communityGames);
                }
                list = communityGames.GamesList;
                return list;
            }
            catch (Exception ex)
            {
                throw new SAException("Error getting community games list.", ex);
            }
        }

        // Protected Methods (1) 

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAMESSVC_SA_CONNECTION_LIMIT"));
        }
        // Private Methods (4) 

        private void acceptInvitation(GameInvite gameInvite)
        {
            string uri = null;
            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    getService(uri).AcceptInvitation(gameInvite);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while attempting to accept an Game Invitation. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to accept an Game Invitation. (uri: " + uri + ")", ex));
            }
        }

        private IGamesService getService(string uri)
        {
            try
            {
                return (IGamesService)Activator.GetObject(typeof(IGamesService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAMESSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private void sendInvitation(GameInvite invite)
        {
            string uri = null;
            try
            {
                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    getService(uri).SendInvitation(invite);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while attempting to send invitation for Game Conversation. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to send invitation for Game Conversation. (uri: " + uri + ")", ex));
            }
        }

        #endregion Methods

        /*
        /// <summary>
        /// Temporary overload for backwards compatibility
        /// Can be removed after 1.3 has been deployed to production.
        /// </summary>
        /// <param name="messageSave"></param>
        /// <param name="saveInSent"></param>
        /// <param name="brand"></param>
        /// <param name="transactionID"></param>
        public void SendMissedGame(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID, string atQuotaErrorMessage)
        {
            Matchnet.Email.ValueObjects.MessageSave errorMessage = new Matchnet.Email.ValueObjects.MessageSave(messageSave.FromMemberID, messageSave.FromMemberID, messageSave.GroupID, Matchnet.Email.ValueObjects.MailType.MissedGame, messageSave.MessageHeader, atQuotaErrorMessage);
            SendMissedGame(messageSave, saveInSent, brand, transactionID, errorMessage);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageSave"></param>
        /// <param name="saveInSent"></param>
        /// <param name="brand"></param>
        /// <param name="transactionID"></param>
        /// <param name="atQuotaErrorMessage">Error message to display in notification if user has reached missed Game quota.</param>
        public void SendMissedGame(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID, Matchnet.Email.ValueObjects.MessageSave atQuotaErrorMessage)
        {
            string uri = null;
            try
            {
                ExternalTransactionID externalTransactionID = null;

                if (transactionID != Constants.NULL_STRING)
                {
                    externalTransactionID = new ExternalTransactionID(transactionID, UserplaneTransactionType.NotifyConnectionClosed, brand.Site.Community.CommunityID);
                }

                uri = getServiceManagerUri();

                base.Checkout(uri);
                try
                {
                    if (getService(uri).CacheExternalTransactionID(externalTransactionID))  //If this ExternalTransactionID is already cached, don't send the missed Game -- we already sent it
                    {
                        return;
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }

                new MissedGameThread(messageSave, saveInSent, brand, atQuotaErrorMessage).Start();
            }
            catch (ExceptionBase ex)
            {
                throw (new SAException("Error occurred while sending missed Game notification. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while sending missed Game notification. (uri: " + uri + ")", ex));
            }
        }
        */
    }
}
