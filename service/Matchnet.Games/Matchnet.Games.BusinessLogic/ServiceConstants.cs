using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Games.BusinessLogic
{
    public class ServiceConstants
    {
		#region Fields (2) 

        /// <summary>
        /// Service Constant (INSTANTMESSENGER_SVC)
        /// </summary>
        public const string SERVICE_CONSTANT = "GAMES_SVC";
        /// <summary>
        /// Service name (Matchnet.InstantMessenger.Service)
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.Games.Service";

		#endregion Fields 

		#region Constructors (1) 

        private ServiceConstants()
        {
        }

		#endregion Constructors 
    }
}
