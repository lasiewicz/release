using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Games.ValueObjects;
using System.Web.Caching;

namespace Matchnet.Games.BusinessLogic
{
    public class GamesBL
    {
		#region Fields (6) 

        private Matchnet.Caching.Cache _Cache;
        private int _expiredCount = 0;
        private string _expiredCountLock = "";
        private CacheItemRemovedCallback _expireInvitation;
        public const int GC_THRESHOLD = 1000;
        public static readonly GamesBL Instance = new GamesBL();

		#endregion Fields 

		#region Constructors (1) 

        private GamesBL()
		{
            _Cache = Matchnet.Caching.Cache.Instance;
			
			// Initialize call back reference for handling events related to items being removed from the cache.
			_expireInvitation = new CacheItemRemovedCallback(this.expireInvitationCallback);
		}

		#endregion Constructors 

		#region Methods (10) 

		// Public Methods (5) 

        public void AcceptInvitation(GameInvite gameInvite)
        {
            GameInvites invites = _Cache[GameInvites.GetCacheKey(gameInvite)] as GameInvites;
            if (invites != null)
            {
                // remove this invite, then replicate the invites container
                invites.Remove(gameInvite);
                ReplicationRequested(invites);
            }
        }

        public void CacheReplicatedObject(IReplicable replicableObject)
        {
            switch (replicableObject.GetType().Name)
            {
                case "GameInvite":
                    addInvitation(replicableObject as GameInvite, false);
                    break;

                case "GameInvites":
                    GameInvites invites = replicableObject as GameInvites;
                    // Update the entire Invites collection (even if it's empty).
                    _Cache.Remove(invites.GetCacheKey());
                    _Cache.Add(invites);
                    break;
              
                default:
                    throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
            }
        }

        public GameInvites GetInvites(int recipientMemberID, int communityID)
        {
            return _Cache[GameInvites.GetCacheKey(recipientMemberID, communityID)] as GameInvites;
        }

        public void Save(GameInvite gameInvite)
        {
            save(gameInvite, true);
        }

        public void SendInvitation(GameInvite gameInvite)
        {
            addInvitation(gameInvite);		// Add invitation to recipient invite queue.
        }
		// Private Methods (5) 

        private void addInvitation(GameInvite gameInvite)
        {
            addInvitation(gameInvite, true);
        }

        private void addInvitation(GameInvite gameInvite, bool replicate)
        {
            GameInvites invites = _Cache[GameInvites.GetCacheKey(gameInvite)] as GameInvites;
            if (invites != null)
            {
                invites.Add(gameInvite);
            }
            else
            {	// Construct new conversation invites collection and insert in the cache.
                invites = new GameInvites(gameInvite
                    , Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAMESSVC_INVITES_CACHE_TTL")));

                _Cache.Add(invites, _expireInvitation);
                
                InvitationAdded();	// Performance counter event raised when we add an Invites collection to the cache.
            }

            if (replicate)
            {
                ReplicationRequested(invites);
            }
        }

        private void expireInvitationCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            InvitationRemoved();
            incrementExpirationCounter();
        }

        private void incrementExpirationCounter()
        {
            lock (_expiredCountLock)
            {
                _expiredCount++;

                if (_expiredCount > GC_THRESHOLD)
                {
                    _expiredCount = 0;
                    GC.Collect();
                }
            }
        }

        private void save(GameInvite gameInvite, bool replicate)
        {
            GameInvites invites = _Cache[GameInvites.GetCacheKey(gameInvite)] as GameInvites;
            if (invites != null)
            {
                if (invites[gameInvite.InviteKey] != null)
                {
                    //only saves the invitation if it already exists.
                    invites[gameInvite.InviteKey] = gameInvite;
                }
            }
            else
            {
                //If invites collection does not already exist, we decided not to create one, although 
                //it may be necessary to change that logic if we need to :)

            }
            if (replicate)
            {
                ReplicationRequested(invites);
            }
        }

		#endregion Methods 



        #region Delegates and Event definitions

        /// <summary>
        /// Delegate signaute for handling ReplicationRequested events.
        /// </summary>
        public delegate void ReplicationEventHandler(IReplicable replicableObject);

        /// <summary>
        /// Event raised when this service encouners/alters a VO that requires replication.
        /// </summary>
        public event ReplicationEventHandler ReplicationRequested;

        /// <summary>
        /// Delegate signature for handling a InvitationAdded event.
        /// </summary>
        public delegate void InvitationAddEventHandler();

        /// <summary>
        /// Event raised when invitations are added to the invitation queue.
        /// </summary>
        public event InvitationAddEventHandler InvitationAdded;

        /// <summary>
        /// Delegate signature for handling a InvitationRemoved event.
        /// </summary>
        public delegate void InvitationRemoveEventHandler();
        /// <summary>
        /// Event raised when an invitation is removed from the invitation queue.
        /// </summary>
        public event InvitationRemoveEventHandler InvitationRemoved;

        #endregion
    }
}
