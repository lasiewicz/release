using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Games.ValueObjects.ServiceDefinitions
{
    public interface IGamesService
    {
		#region Operations (4) 

        void AcceptInvitation(GameInvite gameInvite);

        GameInvites GetInvites(int recipientMemberID, int communityID);

        void Save(GameInvite gameInvite);

        void SendInvitation(GameInvite gameInvite);

		#endregion Operations 
    }
}
