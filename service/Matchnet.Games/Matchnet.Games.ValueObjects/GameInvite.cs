using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Matchnet.Games.ValueObjects
{
    [Serializable()]
    public class GameInvite : IValueObject
    {
		#region Fields (1) 

        private bool _isRead = false;
        private bool _isAccepted = false;
        private bool _isRejected = false;

		#endregion Fields 

		#region Constructors (1) 

        //private int _imID;
        public GameInvite()
        {

        }

		#endregion Constructors 

		#region Properties (6) 

        public int CommunityID {get; set;}

        public DateTime InviteDate { get; set; }

        //private ConversationKey _conversationKey;
        public Guid InviteKey { get; set; }

        public bool IsRead
        {
            get
            {
                return _isRead;
            }
            set
            {
                _isRead = value;
            }

        }

        public bool IsRejected
        {
            get
            {
                return _isRejected;
            }
            set
            {
                _isRejected = value;
            }
        }

        public bool IsAccepted
        {
            get
            {
                return _isAccepted;
            }
            set
            {
                _isAccepted = value;
            }
        }

        public int RecipientID { get; set; }

        public int SenderID { get; set; }
        public string GameID { get; set; }

		#endregion Properties 

		#region Methods (2) 

		// Public Methods (2) 

        public override bool Equals(object obj)
        {
            GameInvite invite = obj as GameInvite;
            return (invite.InviteKey == null && InviteKey == null) || InviteKey.Equals(invite.InviteKey);
        }

        public override int GetHashCode()
        {
            return InviteKey.GetHashCode();
        }

		#endregion Methods 
    }
}
