using System;

namespace Matchnet.Games.ValueObjects
{
    [Serializable]
    public class Game
    {
        public int GameID { get; set; }
        public int GameClassID { get; set; }
        public string Title { get; set; }
        public string ImageURL { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
    }
}