using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Matchnet.Games.ValueObjects
{
    [Serializable()]
    public class GameInvites : IValueObject, IEnumerable, ICacheable, IReplicable
    {
		#region Fields (5) 

        private string _cacheKey;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private int _cacheTTLSeconds;
        private Hashtable _invites;
        public const string INVITE_CACHE_PREFIX = "~GAMEINVITE^{0}^{1}";

		#endregion Fields 

		#region Constructors (1) 

        public GameInvites(GameInvite gameInvite, int cacheTTLSeconds)
        {
            _invites = new Hashtable(1);
            Add(gameInvite);
            _cacheKey = GameInvites.GetCacheKey(gameInvite.RecipientID, gameInvite.CommunityID);
            _cacheTTLSeconds = cacheTTLSeconds;
        }

		#endregion Constructors 

		#region Properties (1) 

        public object SyncRoot
        {
            get
            {
                return _invites.Values.SyncRoot;
            }
        }

		#endregion Properties 



        #region GameInvites implementation

        public GameInvite this[Guid inviteKey]
        {
            get
            {
                lock (_invites.SyncRoot)
                {
                    return _invites[inviteKey] as GameInvite;
                }
            }
            set
            {
                lock (_invites.SyncRoot)
                {
                    _invites[inviteKey] = value;
                }
            }
        }

        /// <summary>
        /// The GameInvite will be added to this invites collection as long
        /// as the Message's Sender does NOT already have an invite in this collection.
        /// </summary>
        /// <param name="conversationInvite"></param>
        public void Add(GameInvite gameInvite)
        {
            // on the add, we are going to impose a unique constraint on 1 invite per member/recipient combo
            bool inviteAlreadyExists = false;
            lock (_invites.SyncRoot)
            {
                foreach (GameInvite invite in _invites.Values)
                {
                    if (invite.SenderID.Equals(
                        gameInvite.SenderID))
                    {
                        inviteAlreadyExists = true;
                        break;
                    }
                }
            }

            if (!inviteAlreadyExists)
            {
                lock (_invites.SyncRoot)
                {
                    this._invites.Add(gameInvite.InviteKey, gameInvite);
                }
            }
        }

        public void Remove(GameInvite gameInvite)
        {
            lock (_invites.SyncRoot)
            {
                _invites.Remove(gameInvite.InviteKey);
            }
        }

        public int Count
        {
            get
            {
                lock (_invites.SyncRoot)
                {
                    return _invites.Count;
                }
            }
        }

        //TODO: This is needed for compatibility with v1.1 of the web code.
        //It should be removed after the 1.2 release.
        public ICollection List
        {
            get
            {
                return _invites.Values;
            }
        }

        #endregion

        #region ICacheable implementation

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return Matchnet.CacheItemMode.Absolute;
            }
        }

        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        public string GetCacheKey()
        {
            return _cacheKey;
        }

        #endregion

        #region Static Methods
        public static string GetCacheKey(int recipientID, int communityID)
        {
            return String.Format(INVITE_CACHE_PREFIX, recipientID, communityID);
        }

        public static string GetCacheKey(GameInvite gameInvite)
        {
            return GameInvites.GetCacheKey(gameInvite.RecipientID, gameInvite.CommunityID);
        }
        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _invites.Values.GetEnumerator();
        }

        #endregion

        #region IReplicable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(this.GetCacheKey());
        }

        #endregion
    }
}
