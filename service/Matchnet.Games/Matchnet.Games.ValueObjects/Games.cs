using System;
using System.Collections.Generic;

namespace Matchnet.Games.ValueObjects
{
    [Serializable]
    public class Games : ICacheable
    {
        public const string CACHE_KEY = "GAMES_";
        private int _cacheTTLSeconds = 60 * 60 * 6;  // Default to 6 hours
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

        public List<Game> GamesList { get; set; }
        public int CommunityID;


        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return Matchnet.CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        public static string GetCacheKey(int communityID)
        {
            return CACHE_KEY + communityID.ToString();
        }
        public string GetCacheKey()
        {
            return GetCacheKey(CommunityID);
        }

        #endregion
    }
}