﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Games.ValueObjects.ServiceDefinitions;
using Matchnet.Games.ValueObjects;
using Matchnet.Games.BusinessLogic;
using Matchnet.Exceptions;
using Matchnet.Replication;
using System.Diagnostics;

namespace Matchnet.Games.ServiceManagers
{
    public class GamesSM : MarshalByRefObject, IServiceManager, IGamesService, IDisposable, IReplicationRecipient
    {
        private PerformanceCounter _perfInvites = null;
        private Replicator _replicator = null;

        public GamesSM()
        {
            // Initialize performance counters
            GamesBL.Instance.InvitationAdded +=
                new GamesBL.InvitationAddEventHandler(GamesBL_InvitationAdded);
            GamesBL.Instance.InvitationRemoved +=
                new GamesBL.InvitationRemoveEventHandler(GamesBL_InvitationRemoved);

            //replication
            GamesBL.Instance.ReplicationRequested += new Matchnet.Games.BusinessLogic.GamesBL.ReplicationEventHandler(GamesBL_ReplicationRequested);

            string machineName = System.Environment.MachineName;

            string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }

            string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("GAMESSVC_REPLICATION_OVERRIDE");
            if (replicationURI.Length == 0)
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, "GamesSM", machineName);
            }
            if (replicationURI != null && replicationURI != string.Empty)
            {
                _replicator = new Replicator(ServiceConstants.SERVICE_NAME);
                _replicator.SetDestinationUri(replicationURI);
                _replicator.Start();
            }
            else
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME
                    , "Unable to retrieve a replication partner uri on startup."
                    , EventLogEntryType.Error);
            }

            initPerformanceCounters();
        }

        #region IGamesService implementation

        public void AcceptInvitation(GameInvite gameInvite)
        {
            try
            {
                GamesBL.Instance.AcceptInvitation(gameInvite);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure accepting Games Invitation.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure accepting Games Invitation.", ex);
            }
        }

        public GameInvites GetInvites(int recipientMemberID, int communityID)
        {
            try
            {
                return GamesBL.Instance.GetInvites(recipientMemberID, communityID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Game Invitation(s).", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Game Invitation(s)", ex);
            }
        }

        public void SendInvitation(GameInvite gameInvite)
        {
            try
            {
                GamesBL.Instance.SendInvitation((GameInvite)gameInvite);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending Game Invitation.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending Game Invitation.", ex);
            }
        }

        /*
            public void SendMissedGame(Matchnet.Email.ValueObjects.MessageSave messageSave, bool saveInSent, Brand brand, string transactionID)
            {
                try
                {
                    InstantMessengerBL.Instance.SendMissedGame(messageSave, saveInSent, brand, transactionID);
                }
                catch (ExceptionBase ex)
                {
                    throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed Game.", ex);
                }
                catch (Exception ex)
                {
                    throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure sending missed Game.", ex);
                }
            }
         */


        /// <summary>
        /// Allows you to save an existing conversation invitation for the purposes of marking it as Read for example.
        /// </summary>
        public void Save(GameInvite gameInvite)
        {
            try
            {
                GamesBL.Instance.Save(gameInvite);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Game Invitation.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Game Invitation.", ex);
            }
        }

        #endregion

        #region Performance Counters / Instrumentation

        /// <summary>
        /// Install the performance counters for this service manager.
        /// </summary>
        public static void PerfCounterInstall()
        {
            CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
            ccdc.AddRange(new CounterCreationData[] {	
				new CounterCreationData("Invitations", "Invitations", PerformanceCounterType.NumberOfItems64),
				new CounterCreationData("Game Conversations", "Game Conversations", PerformanceCounterType.NumberOfItems64),
				new CounterCreationData("Game Messages/second", "Game Messages/second", PerformanceCounterType.RateOfCountsPerSecond32),
				});
            PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
        }

        /// <summary>
        /// Uninstall the performance counters for this service manager.
        /// </summary>
        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
        }

        private void initPerformanceCounters()
        {
            _perfInvites = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Invitations", false);

            resetPerfCounters();
        }

        private void resetPerfCounters()
        {
            _perfInvites.RawValue = 0;
        }

        private void GamesBL_InvitationAdded()
        {
            lock (_perfInvites)
            {
                _perfInvites.Increment();
            }
        }

        private void GamesBL_InvitationRemoved()
        {
            lock (_perfInvites)
            {
                _perfInvites.Decrement();
            }
        }



        #endregion

        void IReplicationRecipient.Receive(IReplicable replicable)
        {
            try
            {
                GamesBL.Instance.CacheReplicatedObject(replicable);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure processing replication object.", ex);
            }
        }

        private void GamesBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (replicableObject == null)
            {
                Trace.WriteLine("Games Service Manager handling a ReplicationRequested event with a null argument.", "GamesSM");
            }
            else
            {
                if (_replicator != null)
                {
                    try
                    {
                        _replicator.Enqueue(replicableObject);
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine("Exception caught performing _replicator.Enqueue(...) : " + ex.Message, "Games");
                    }
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            resetPerfCounters();
        }

        #endregion

        /// <summary>
        /// Prepopulate the Matchnet.Search cache items (not implemented at this time).
        /// </summary>
        public void PrePopulateCache()
        {
            // No implementation at this time.
        }

        /// <summary>
        /// Not implemented, returns null.
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
