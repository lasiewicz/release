﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.RemotingServices;
using Matchnet.Games.ServiceManagers;
using Matchnet.Games.BusinessLogic;
using Matchnet.Exceptions;

namespace Matchnet.Games.Service
{
    partial class GamesService : Matchnet.RemotingServices.RemotingServiceBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        private GamesSM _gamesSM = null;

        public GamesService()
        {
            InitializeComponent();
            
        }

        // The main entry point for the process
        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new GamesService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        //protected override void OnStart(string[] args)
        //{
        //    // TODO: Add code here to start your service.
        //}

        //protected override void OnStop()
        //{
        //    // TODO: Add code here to perform any tear-down necessary to stop your service.
        //    //base.OnStop();
        //}

        /// <summary>
        /// Register the service managers for this service.
        /// - InstantMessenger
        /// </summary>
        protected override void RegisterServiceManagers()
        {
            try
            {
                // Initialize service managers.
                _gamesSM = new GamesSM();

                // Register them.
                base.RegisterServiceManager(_gamesSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service, see details: " + ex.Message);
            }
        }
    }
}
