﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

using Matchnet.Games.BusinessLogic;
using Matchnet.Games.ServiceManagers;


namespace Matchnet.Games.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller serviceInstaller;

        public ProjectInstaller()
        {
            InitializeComponent();
            this.serviceInstaller.ServiceName = ServiceConstants.SERVICE_NAME;
        }

        private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            GamesSM.PerfCounterInstall();
        }

        private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            GamesSM.PerfCounterUninstall();
        }
    }
}
