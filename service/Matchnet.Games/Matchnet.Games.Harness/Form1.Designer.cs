﻿namespace Matchnet.Games.Harness
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRepPartner1 = new System.Windows.Forms.TextBox();
            this.txtRepPartner2 = new System.Windows.Forms.TextBox();
            this.rdbRep1 = new System.Windows.Forms.RadioButton();
            this.rdbRep2 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtInviterID = new System.Windows.Forms.TextBox();
            this.txtInviteeID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSendInvitaion = new System.Windows.Forms.Button();
            this.btnDeleteInvitation = new System.Windows.Forms.Button();
            this.btnReadInvitation = new System.Windows.Forms.Button();
            this.btnRejectInvitation = new System.Windows.Forms.Button();
            this.btnAcceptInvitation = new System.Windows.Forms.Button();
            this.txtConsole = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(669, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "The purpose of this application is to test the replication feature of Games servi" +
                "ce. Choose an instance from which the actions will be invoked:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Replication partner #1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(407, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Replication partner #2";
            // 
            // txtRepPartner1
            // 
            this.txtRepPartner1.Location = new System.Drawing.Point(155, 32);
            this.txtRepPartner1.Name = "txtRepPartner1";
            this.txtRepPartner1.Size = new System.Drawing.Size(221, 20);
            this.txtRepPartner1.TabIndex = 3;
            this.txtRepPartner1.Text = "tcp://LASVCSTAGEV201:65400/GamesSM.rem";
            // 
            // txtRepPartner2
            // 
            this.txtRepPartner2.Location = new System.Drawing.Point(525, 32);
            this.txtRepPartner2.Name = "txtRepPartner2";
            this.txtRepPartner2.Size = new System.Drawing.Size(221, 20);
            this.txtRepPartner2.TabIndex = 4;
            this.txtRepPartner2.Text = "tcp://LASVCSTAGEV202:65400/GamesSM.rem";
            // 
            // rdbRep1
            // 
            this.rdbRep1.AutoSize = true;
            this.rdbRep1.Checked = true;
            this.rdbRep1.Location = new System.Drawing.Point(17, 35);
            this.rdbRep1.Name = "rdbRep1";
            this.rdbRep1.Size = new System.Drawing.Size(14, 13);
            this.rdbRep1.TabIndex = 5;
            this.rdbRep1.TabStop = true;
            this.rdbRep1.UseVisualStyleBackColor = true;
            // 
            // rdbRep2
            // 
            this.rdbRep2.AutoSize = true;
            this.rdbRep2.Location = new System.Drawing.Point(387, 35);
            this.rdbRep2.Name = "rdbRep2";
            this.rdbRep2.Size = new System.Drawing.Size(14, 13);
            this.rdbRep2.TabIndex = 6;
            this.rdbRep2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "CommunityID: 10";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(270, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Inviter ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(522, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Invitee ID";
            // 
            // txtInviterID
            // 
            this.txtInviterID.Location = new System.Drawing.Point(326, 62);
            this.txtInviterID.Name = "txtInviterID";
            this.txtInviterID.Size = new System.Drawing.Size(165, 20);
            this.txtInviterID.TabIndex = 10;
            this.txtInviterID.Text = "111111111";
            // 
            // txtInviteeID
            // 
            this.txtInviteeID.Location = new System.Drawing.Point(581, 62);
            this.txtInviteeID.Name = "txtInviteeID";
            this.txtInviteeID.Size = new System.Drawing.Size(165, 20);
            this.txtInviteeID.TabIndex = 11;
            this.txtInviteeID.Text = "222222222";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Actions:";
            // 
            // btnSendInvitaion
            // 
            this.btnSendInvitaion.Location = new System.Drawing.Point(17, 124);
            this.btnSendInvitaion.Name = "btnSendInvitaion";
            this.btnSendInvitaion.Size = new System.Drawing.Size(116, 23);
            this.btnSendInvitaion.TabIndex = 13;
            this.btnSendInvitaion.Text = "Send Invitation";
            this.btnSendInvitaion.UseVisualStyleBackColor = true;
            this.btnSendInvitaion.Click += new System.EventHandler(this.btnSendInvitaion_Click);
            // 
            // btnDeleteInvitation
            // 
            this.btnDeleteInvitation.Location = new System.Drawing.Point(158, 124);
            this.btnDeleteInvitation.Name = "btnDeleteInvitation";
            this.btnDeleteInvitation.Size = new System.Drawing.Size(116, 23);
            this.btnDeleteInvitation.TabIndex = 14;
            this.btnDeleteInvitation.Text = "Delete Invitation";
            this.btnDeleteInvitation.UseVisualStyleBackColor = true;
            this.btnDeleteInvitation.Click += new System.EventHandler(this.btnDeleteInvitation_Click);
            // 
            // btnReadInvitation
            // 
            this.btnReadInvitation.Location = new System.Drawing.Point(302, 124);
            this.btnReadInvitation.Name = "btnReadInvitation";
            this.btnReadInvitation.Size = new System.Drawing.Size(116, 23);
            this.btnReadInvitation.TabIndex = 15;
            this.btnReadInvitation.Text = "Read Invitation";
            this.btnReadInvitation.UseVisualStyleBackColor = true;
            this.btnReadInvitation.Click += new System.EventHandler(this.btnReadInvitation_Click);
            // 
            // btnRejectInvitation
            // 
            this.btnRejectInvitation.Location = new System.Drawing.Point(589, 124);
            this.btnRejectInvitation.Name = "btnRejectInvitation";
            this.btnRejectInvitation.Size = new System.Drawing.Size(116, 23);
            this.btnRejectInvitation.TabIndex = 17;
            this.btnRejectInvitation.Text = "Reject Invitation";
            this.btnRejectInvitation.UseVisualStyleBackColor = true;
            this.btnRejectInvitation.Click += new System.EventHandler(this.btnRejectInvitation_Click);
            // 
            // btnAcceptInvitation
            // 
            this.btnAcceptInvitation.Location = new System.Drawing.Point(445, 124);
            this.btnAcceptInvitation.Name = "btnAcceptInvitation";
            this.btnAcceptInvitation.Size = new System.Drawing.Size(116, 23);
            this.btnAcceptInvitation.TabIndex = 16;
            this.btnAcceptInvitation.Text = "Accept Invitation";
            this.btnAcceptInvitation.UseVisualStyleBackColor = true;
            this.btnAcceptInvitation.Click += new System.EventHandler(this.btnAcceptInvitation_Click);
            // 
            // txtConsole
            // 
            this.txtConsole.Location = new System.Drawing.Point(15, 196);
            this.txtConsole.Multiline = true;
            this.txtConsole.Name = "txtConsole";
            this.txtConsole.ReadOnly = true;
            this.txtConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtConsole.Size = new System.Drawing.Size(835, 293);
            this.txtConsole.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Consloe";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(126, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "GameID: 13";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(485, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Invite Key (GUID)";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(581, 92);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(165, 20);
            this.txtKey.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 501);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtConsole);
            this.Controls.Add(this.btnRejectInvitation);
            this.Controls.Add(this.btnAcceptInvitation);
            this.Controls.Add(this.btnReadInvitation);
            this.Controls.Add(this.btnDeleteInvitation);
            this.Controls.Add(this.btnSendInvitaion);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtInviteeID);
            this.Controls.Add(this.txtInviterID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rdbRep2);
            this.Controls.Add(this.rdbRep1);
            this.Controls.Add(this.txtRepPartner2);
            this.Controls.Add(this.txtRepPartner1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRepPartner1;
        private System.Windows.Forms.TextBox txtRepPartner2;
        private System.Windows.Forms.RadioButton rdbRep1;
        private System.Windows.Forms.RadioButton rdbRep2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtInviterID;
        private System.Windows.Forms.TextBox txtInviteeID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSendInvitaion;
        private System.Windows.Forms.Button btnDeleteInvitation;
        private System.Windows.Forms.Button btnReadInvitation;
        private System.Windows.Forms.Button btnRejectInvitation;
        private System.Windows.Forms.Button btnAcceptInvitation;
        private System.Windows.Forms.TextBox txtConsole;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtKey;
    }
}

