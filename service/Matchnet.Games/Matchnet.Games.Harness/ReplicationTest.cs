using System;
using Matchnet.Games.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingClient;
using Matchnet.Games.ValueObjects;
using System.Threading;

namespace Matchnet.Games.Harness
{
    public class ReplicationTest : SABase
    {


        public event Form1.AddMessageHandler MessageAdd;

        public void MessageAdded(string msg)
        {
            if (MessageAdd != null) MessageAdd(msg);
        }



        public static readonly ReplicationTest Instance = new ReplicationTest();
        //public int CommunityID { get; set; }
        //public int GameID { get; set; }
        //public string SourceReplicationUri { get; set; }
        //public string TargetReplicationUri { get; set; }
        //public int InviterID { get; set; }
        //public int InviteeID { get; set; }

        public void ReadInvitation(int recipientMemberID
           , int communityID, string sKey,
            string sourceReplicationUri,
            string targetReplicationUri)
        {
            Guid key = new Guid(sKey);
            string uri = sourceReplicationUri;

            GameInvites invites = null;

            GameInvite inviteRes = null;

            invites = getService(uri).GetInvites(recipientMemberID, communityID);
            if (invites == null)
            {
                MessageAdd("No invitations were found for the invitee on the source server");
                return;
            }
            if (invites[key] == null)
            {
                MessageAdd("Invite was not found on the list on the source server.");
                return;
            }

            invites[key].IsRead = true;
            getService(uri).Save(invites[key]);

            uri = targetReplicationUri;

            invites = null;
            inviteRes = null;

            while (inviteRes == null)
            {
                invites = getService(uri).GetInvites(recipientMemberID, communityID);
                while (invites == null)
                {
                    MessageAdd("Waiting for invits list on replication partner");
                    Thread.Sleep(500);
                    invites = getService(uri).GetInvites(recipientMemberID, communityID);
                }
                if (invites[key] == null)
                {
                    MessageAdd("List was found, invite was not found on the list. Reseting list");
                }
                else
                {
                    inviteRes = invites[key];
                    MessageAdd("List was found, invite was found on the list.");
                }
            }

            MessageAdd(GetInvitationDataString(inviteRes));
        }

        public void RejectInvitation(int recipientMemberID
           , int communityID, string sKey,
            string sourceReplicationUri,
            string targetReplicationUri)
        {
            Guid key = new Guid(sKey);
            string uri = sourceReplicationUri;

            GameInvites invites = null;

            GameInvite inviteRes = null;

            invites = getService(uri).GetInvites(recipientMemberID, communityID);
            if (invites == null)
            {
                MessageAdd("No invitations were found for the invitee on the source server");
                return;
            }
            if (invites[key] == null)
            {
                MessageAdd("Invite was not found on the list on the source server.");
                return;
            }

            invites[key].IsRejected = true;
            getService(uri).Save(invites[key]);

            uri = targetReplicationUri;

            invites = null;
            inviteRes = null;

            while (inviteRes == null)
            {
                invites = getService(uri).GetInvites(recipientMemberID, communityID);
                while (invites == null)
                {
                    MessageAdd("Waiting for invits list on replication partner");
                    Thread.Sleep(500);
                    invites = getService(uri).GetInvites(recipientMemberID, communityID);
                }
                if (invites[key] == null)
                {
                    MessageAdd("List was found, invite was not found on the list. Reseting list");
                }
                else
                {
                    inviteRes = invites[key];
                    MessageAdd("List was found, invite was found on the list.");
                }
            }

            MessageAdd(GetInvitationDataString(inviteRes));
        }

        public void AcceptInvitation(int recipientMemberID
           , int communityID, string sKey,
            string sourceReplicationUri,
            string targetReplicationUri)
        {
            Guid key = new Guid(sKey);
            string uri = sourceReplicationUri;

            GameInvites invites = null;

            GameInvite inviteRes = null;

            invites = getService(uri).GetInvites(recipientMemberID, communityID);
            if (invites == null)
            {
                MessageAdd("No invitations were found for the invitee on the source server");
                return;
            }
            if (invites[key] == null)
            {
                MessageAdd("Invite was not found on the list on the source server.");
                return;
            }

            invites[key].IsAccepted = true;
            getService(uri).Save(invites[key]);

            uri = targetReplicationUri;

            invites = null;
            inviteRes = null;

            while (inviteRes == null)
            {
                invites = getService(uri).GetInvites(recipientMemberID, communityID);
                while (invites == null)
                {
                    MessageAdd("Waiting for invits list on replication partner");
                    Thread.Sleep(500);
                    invites = getService(uri).GetInvites(recipientMemberID, communityID);
                }
                if (invites[key] == null)
                {
                    MessageAdd("List was found, invite was not found on the list. Reseting list");
                }
                else
                {
                    inviteRes = invites[key];
                    MessageAdd("List was found, invite was found on the list.");
                }
            }

            MessageAdd(GetInvitationDataString(inviteRes));
        }

        public void DeleteInvitation(int recipientMemberID
           , int communityID, string sKey,
            string sourceReplicationUri,
            string targetReplicationUri)
        {
            Guid key = new Guid(sKey);
            string uri = sourceReplicationUri;

            GameInvites invites = null;

            GameInvite inviteRes = null;


            invites = getService(uri).GetInvites(recipientMemberID, communityID);
            if (invites == null)
            {
                MessageAdd("No invitations were found for the invitee on the source server");
                return;
            }
            if (invites[key] == null)
            {
                MessageAdd("Invite was not found on the list on the source server.");
                return;
            }


            getService(uri).AcceptInvitation(invites[key]);

            MessageAdd("Invitation was deleted on the source server");

            uri = targetReplicationUri;

            invites = null;
            inviteRes = null;

            while (inviteRes == null)
            {
                invites = getService(uri).GetInvites(recipientMemberID, communityID);
                while (invites == null)
                {
                    MessageAdd("Waiting for invits list on replication partner");
                    Thread.Sleep(500);
                    invites = getService(uri).GetInvites(recipientMemberID, communityID);
                }
                if (invites[key] == null)
                {
                    MessageAdd("List was found, invite was not found on the list. Success");
                    break;

                }
                else
                {
                    //inviteRes = invites[key];
                    MessageAdd("List was found, invite was found on the list. Reseting list");
                }
            }
        }

        public void InitiateGame(int initiatingMemberID
           , int recipientMemberID
           , int communityID
           , string gameID,
            string sourceReplicationUri,
            string targetReplicationUri)
        {

            Guid key = System.Guid.NewGuid();
            GameInvite invite = new GameInvite();
            invite.CommunityID = communityID;
            invite.InviteKey = key;
            invite.InviteDate = DateTime.Now;
            invite.SenderID = initiatingMemberID;
            invite.RecipientID = recipientMemberID;
            invite.GameID = gameID;

            string uri = sourceReplicationUri;

            base.Checkout(uri);
            try
            {
                getService(uri).SendInvitation(invite);
            }
            finally
            {
                base.Checkin(uri);
            }

            MessageAdd("Invitaion was sent successfully. Key= " + key.ToString());

            uri = targetReplicationUri;

            GameInvites invites = null;

            GameInvite inviteRes = null;

            while (inviteRes == null)
            {
                invites = getService(uri).GetInvites(recipientMemberID, communityID);
                while (invites == null)
                {
                    MessageAdd("Waiting for invits list on replication partner");
                    Thread.Sleep(500);
                    invites = getService(uri).GetInvites(recipientMemberID, communityID);
                }
                if (invites[key] == null)
                {
                    MessageAdd("Invite was not found on the list. Reseting list");
                }
                else
                {
                    inviteRes = invites[key];
                }
            }

            MessageAdd("Invite was found on replication partner.");
            MessageAdd(GetInvitationDataString(inviteRes));
        }

        private IGamesService getService(string uri)
        {
            return (IGamesService)Activator.GetObject(typeof(IGamesService), uri);
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte("10");
        }

        private string GetInvitationDataString(GameInvite invite)
        {
            string s =
                string.Format(
                    "Key: {0} SenderID: {1} RecipientID: {2} GameID: {3} IsRead: {4} IsAccepted: {5} IsRejected: {6}",
                    invite.InviteKey,
                    invite.SenderID,
                    invite.RecipientID,
                    invite.GameID,
                    invite.IsRead,
                    invite.IsAccepted,
                    invite.IsRejected);

            return s;
        }
    }
}