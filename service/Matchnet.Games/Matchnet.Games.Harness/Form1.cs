﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Games.ValueObjects;


namespace Matchnet.Games.Harness
{
    public partial class Form1 : Form
    {
        private const int COMMUNITY_ID = 10;
        private const int GAME_ID = 13;

        private string _SourceReplicationUri;
        private string _TargetReplicationUri;

        public delegate void AddMessageHandler(string msg);

        public Form1()
        {
            InitializeComponent();
        }

        private void WriteLineToConsole(string s)
        {
            txtConsole.Text = txtConsole.Text + Environment.NewLine + DateTime.Now.ToString("HH:mm:ss.fff") + " " + s;
        }

        private void SetReplicationRoles()
        {
            if (rdbRep1.Checked)
            {
                _SourceReplicationUri = txtRepPartner1.Text;
                _TargetReplicationUri = txtRepPartner2.Text;
            }
            else
            {
                _SourceReplicationUri = txtRepPartner2.Text;
                _TargetReplicationUri = txtRepPartner1.Text;
            }
        }



        private void btnSendInvitaion_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLineToConsole("Send Invitation test started");
                SetReplicationRoles();
                ReplicationTest.Instance.InitiateGame(int.Parse(txtInviterID.Text), int.Parse(txtInviteeID.Text),
                                                               COMMUNITY_ID, GAME_ID.ToString(), _SourceReplicationUri,
                                                               _TargetReplicationUri);
                WriteLineToConsole("Send Invitation test completed");

            }
            catch (Exception ex)
            {

                WriteLineToConsole(ex.Message);
            }
        }

        private void btnDeleteInvitation_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLineToConsole("Delete Invitation test started");
                SetReplicationRoles();
                ReplicationTest.Instance.DeleteInvitation(int.Parse(txtInviteeID.Text), COMMUNITY_ID, txtKey.Text, _SourceReplicationUri, _TargetReplicationUri);
                WriteLineToConsole("Delete Invitation test completed");

            }
            catch (Exception ex)
            {
                WriteLineToConsole(ex.Message);
            }
        }

        private void btnReadInvitation_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLineToConsole("Read Invitation test started");
                SetReplicationRoles();
                ReplicationTest.Instance.ReadInvitation(int.Parse(txtInviteeID.Text), COMMUNITY_ID, txtKey.Text, _SourceReplicationUri, _TargetReplicationUri);

                WriteLineToConsole("Read Invitation test completed");
            }
            catch (Exception ex)
            {

                WriteLineToConsole(ex.Message);
            }
        }

        private void btnAcceptInvitation_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLineToConsole("Accept Invitation test started");
                SetReplicationRoles();
                ReplicationTest.Instance.AcceptInvitation(int.Parse(txtInviteeID.Text), COMMUNITY_ID, txtKey.Text, _SourceReplicationUri, _TargetReplicationUri);

                WriteLineToConsole("Accept Invitation test completed");
            }
            catch (Exception ex)
            {

                WriteLineToConsole(ex.Message);
            }
        }

        private void btnRejectInvitation_Click(object sender, EventArgs e)
        {
            try
            {
                WriteLineToConsole("Reject Invitation test started");
                SetReplicationRoles();
                ReplicationTest.Instance.RejectInvitation(int.Parse(txtInviteeID.Text), COMMUNITY_ID, txtKey.Text, _SourceReplicationUri, _TargetReplicationUri);

                WriteLineToConsole("Reject Invitation test completed");
            }
            catch (Exception ex)
            {
                WriteLineToConsole(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ReplicationTest.Instance.MessageAdd += new AddMessageHandler(WriteLineToConsole);
        }
    }
}
