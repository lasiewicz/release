﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Spark.MingleMigration.ServiceManagers;
using Spark.MingleMigration.ValueObjects;

namespace Spark.MingleMigration.Service
{
    public partial class MingleMigrationService : RemotingServiceBase
    {
        private AttributeMapperSM _attributeMapperSM;
        private MingleMemberSM _mingleMemberSM;
        
        public MingleMigrationService()
        {
            InitializeComponent();
        }
        
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new MingleMigrationService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _attributeMapperSM = new AttributeMapperSM();
                _mingleMemberSM = new MingleMemberSM();

                base.RegisterServiceManager(_attributeMapperSM);
                base.RegisterServiceManager(_mingleMemberSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
            }
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }
    }
}
