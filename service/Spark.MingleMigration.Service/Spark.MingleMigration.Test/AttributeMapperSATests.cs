﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using NUnit.Framework;
using Spark.MingleMigration.ServiceAdapters;
using Spark.MingleMigration.ValueObjects;
using Spark.SearchPreferences;

namespace Spark.MingleMigration.Test
{
    public class AttributeMapperSATests
    {
        [Test]
        public void TestAttributesNotSuppliedAsParameter()
        {
            var result = AttributeMapperSA.Instance.MapAttributes(null, null, 9081, new MockAttributeMetadataService(),
                                                                  null);
            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.InvalidParameters);
        }

        [Test]
        public void TestInvalidAttribute()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_religion3", BHAttributeID = 1}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_religion", 4);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "Religion", DataType.Number, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeInvalid);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_religion");
        }

        [Test]
        public void TestInvalidAttributeDataTypeDate()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_birthdate", BHAttributeID = 1}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_birthdate", 4);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "birthdate", DataType.Date, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeIncorrectDataType);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_birthdate");
            Assert.IsTrue(result.Errors[0].ErrorMessage.ToLower().Contains("date"));
        }

        [Test]
        public void TestInvalidAttributeDataTypeNumber()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_height", BHAttributeID = 1}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_height", "lk");

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "height", DataType.Number, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeIncorrectDataType);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_height");
            Assert.IsTrue(result.Errors[0].ErrorMessage.ToLower().Contains("number"));
        }

        [Test]
        public void TestInvalidAttributeDataTypeBit()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_stuff", BHAttributeID = 1}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_stuff", "lk");

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "stuff", DataType.Bit, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeIncorrectDataType);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_stuff");
            Assert.IsTrue(result.Errors[0].ErrorMessage.ToLower().Contains("bool"));
        }

        [Test]
        public void TestInvalidAttributeDataTypeMask()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_religion", BHAttributeID = 1}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_religion", 1);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "religion", DataType.Mask, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeIncorrectDataType);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_religion");
            Assert.IsTrue(result.Errors[0].ErrorMessage.ToLower().Contains("mask"));
        }

        [Test]
        public void TestInvalidAttributeIfAttributeExistsButNotForGroup()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_religion", BHAttributeID = 1}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_religion", 1);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "religion", DataType.Number, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9011,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeInvalid);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_religion");
        }

        [Test]
        public void TestSingleAttributeMappingSuccess()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_religion", BHAttributeID = 1, BHHasOptions = false}, 9081);

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_religion", 5);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "religion", DataType.Number, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.IsTrue(result.Success);
            Assert.IsTrue(result.MappedAttributes.Count == 1);
            Assert.IsTrue(result.MappedAttributes.ContainsKey("religion"));
            Assert.IsTrue(Convert.ToInt32(result.MappedAttributes["religion"]) == 5);
        }

        [Test]
        public void TestMultivaluedAttributeMappingSuccess()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(
                new MingleAttribute {AttributeName = "has_religion", BHAttributeID = 1, MingleAttributeID = 1}, 9081);
            mapper.AddMingleAttribute(
                new MingleAttribute {AttributeName = "fun_facts_movie", BHAttributeID = 2, MingleAttributeID = 2}, 9081);
            mapper.AddMingleAttributeGroup(new MingleAttributeGroup
                                               {
                                                   BHAttributeGroupID = 1,
                                                   GroupID = 9081,
                                                   MingleAttributeGroupID = 1,
                                                   MingleAttributeID = 2
                                               });
            mapper.AddMingleAttributeOption(new MingleAttributeOption
                                                {BHValue = 1, Value = 1, MingleAttributeGroupID = 1});
            mapper.AddMingleAttributeOption(new MingleAttributeOption
                                                {BHValue = 8, Value = 2, MingleAttributeGroupID = 1});
            mapper.AddMingleAttributeOption(new MingleAttributeOption
                                                {BHValue = 32, Value = 4, MingleAttributeGroupID = 1});

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_religion", 5);

            var mingleMultiValued = new Dictionary<string, List<int>>();
            mingleMultiValued.Add("fun_facts_movie", new List<int> {1, 2, 4});

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "religion", DataType.Number, 0);
            attributes.AddAttribute(2, ScopeType.Site, "MovieGenreMask", DataType.Mask, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, mingleMultiValued, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.IsTrue(result.Success);
            Assert.IsTrue(result.MappedAttributes.Count == 2);
            Assert.IsTrue(result.MappedAttributes.ContainsKey("MovieGenreMask"));
            Assert.IsTrue(Convert.ToInt32(result.MappedAttributes["MovieGenreMask"]) == 41);
        }

        [Test]
        public void TestMultivaluedAttributeMappingFailsIfSingleAttributeDoesntMap()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(
                new MingleAttribute {AttributeName = "has_religion", BHAttributeID = 1, MingleAttributeID = 1}, 9081);
            mapper.AddMingleAttribute(
                new MingleAttribute {AttributeName = "fun_facts_movie", BHAttributeID = 2, MingleAttributeID = 2}, 9081);
            mapper.AddMingleAttributeGroup(new MingleAttributeGroup
                                               {
                                                   BHAttributeGroupID = 1,
                                                   GroupID = 9081,
                                                   MingleAttributeGroupID = 1,
                                                   MingleAttributeID = 2
                                               });
            mapper.AddMingleAttributeOption(new MingleAttributeOption
                                                {BHValue = 1, Value = 1, MingleAttributeGroupID = 1});
            mapper.AddMingleAttributeOption(new MingleAttributeOption
                                                {BHValue = 8, Value = 2, MingleAttributeGroupID = 1});
            mapper.AddMingleAttributeOption(new MingleAttributeOption
                                                {BHValue = 32, Value = 4, MingleAttributeGroupID = 1});

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_religion", 5);

            var mingleMultiValued = new Dictionary<string, List<int>>();
            mingleMultiValued.Add("fun_facts_movie", new List<int> {1, 2, 8});

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "religion", DataType.Number, 0);
            attributes.AddAttribute(2, ScopeType.Site, "MovieGenreMask", DataType.Mask, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService {Attributes = attributes};
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, mingleMultiValued, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.IsFalse(result.Success);
            Assert.IsTrue(result.Errors.Count == 1);
            Assert.IsTrue(result.Errors[0].AttributeName == "fun_facts_movie");
            Assert.IsTrue(result.Errors[0].ErrorMessage.Contains("Error mapping option"));
        }

        [Test]
        public void TestSearchPreferenceValidationValidMappingSingle()
        {
            var searchPreferenceMapper = new SearchPreferenceMapper();
            searchPreferenceMapper.AddSearchPreference(new MingleSearchPreference
                                                           {
                                                               MinglePreferenceName = "wants_distance",
                                                               GroupID = 9081,
                                                               IsMaskValue = false,
                                                               BHPreferenceName = "Distance"
                                                           });

            var prefs = new List<SearchPreference>();
            prefs.Add(new SearchPreference(){Name = "wants_distance", Value = "100"});
            var mappingResult = AttributeMapperSA.Instance.ValidateSearchPreferences(prefs, null, searchPreferenceMapper, null, 9081);
            Assert.IsTrue(mappingResult.Success);
            Assert.IsTrue(mappingResult.Errors.Count ==0);
        }

        [Test]
        public void TestSearchPreferenceValidationValidMappingSingleWithRelated()
        {
            var searchPreferenceMapper = new SearchPreferenceMapper();
            searchPreferenceMapper.AddSearchPreference(new MingleSearchPreference
            {
                MinglePreferenceName = "wants_distance",
                GroupID = 9081,
                IsMaskValue = false,
                BHPreferenceName = "Distance", 
                RelatedMingleAttributeName = "has_distance"
            });

            var attributeMapper = new AttributeMapper();
            attributeMapper.AddMingleAttribute(new MingleAttribute {AttributeName = "has_distance"}, 9081);

            var prefs = new List<SearchPreference>();
            prefs.Add(new SearchPreference() { Name = "wants_distance", Value = "100" });
            var mappingResult = AttributeMapperSA.Instance.ValidateSearchPreferences(prefs, null, searchPreferenceMapper,
                                                                                     attributeMapper, 9081);
            Assert.IsTrue(mappingResult.Success);
            Assert.IsTrue(mappingResult.Errors.Count == 0);
        }

        [Test]
        public void TestSearchPreferenceValidationValidMappingMultiWithRelated()
        {
            var searchPreferenceMapper = new SearchPreferenceMapper();
            searchPreferenceMapper.AddSearchPreference(new MingleSearchPreference
            {
                MinglePreferenceName = "wants_body_type",
                GroupID = 9081,
                IsMaskValue = true,
                BHPreferenceName = "Distance",
                RelatedMingleAttributeName = "has_body_type"
            });

            var attributeMapper = new AttributeMapper();
            attributeMapper.AddMingleAttribute(new MingleAttribute { AttributeName = "has_body_type", MingleAttributeID=1 }, 9081);
            attributeMapper.AddMingleAttributeGroup(new MingleAttributeGroup {MingleAttributeGroupID = 1, GroupID =9081, MingleAttributeID=1});
            attributeMapper.AddMingleAttributeOption(new MingleAttributeOption {Value=1, BHValue=2, MingleAttributeGroupID=1});
            attributeMapper.AddMingleAttributeOption(new MingleAttributeOption { Value = 2, BHValue = 4, MingleAttributeGroupID = 1 });

            //var prefs = new Dictionary<string, List<int>>() { { "wants_body_type", new List<int> {1,2} } };
            var prefs = new List<MultiValuedSearchPreference>();
            prefs.Add(new MultiValuedSearchPreference() { Name = "wants_body_type", Values = new List<int> { 1, 2 } });
            var mappingResult = AttributeMapperSA.Instance.ValidateSearchPreferences(null, prefs, searchPreferenceMapper,
                                                                                     attributeMapper, 9081);
            Assert.IsTrue(mappingResult.Success);
            Assert.IsTrue(mappingResult.Errors.Count == 0);
        }

        [Test]
        public void TestOptionValueMappedCorrectly()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute { AttributeName = "has_ethnicity", BHAttributeID = 1, MingleAttributeID=1, BHHasOptions = true }, 9081);
            mapper.AddMingleAttributeGroup(new MingleAttributeGroup { MingleAttributeGroupID = 1, GroupID = 9081, MingleAttributeID = 1 });
            mapper.AddMingleAttributeOption(new MingleAttributeOption { Value = 1, BHValue = 2, MingleAttributeGroupID = 1 });
            mapper.AddMingleAttributeOption(new MingleAttributeOption { Value = 2, BHValue = 4, MingleAttributeGroupID = 1 });

            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_ethnicity", 2);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "ethnicity", DataType.Number, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService { Attributes = attributes };
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.IsTrue(result.Success);
            Assert.IsTrue(result.MappedAttributes.Count == 1);
            Assert.IsTrue(result.MappedAttributes.ContainsKey("ethnicity"));
            Assert.IsTrue(Convert.ToInt32(result.MappedAttributes["ethnicity"]) == 4);
        }

        [Test]
        public void TestInvalidMappedOptionInSingleValued()
        {
            var mapper = new AttributeMapper();
            mapper.AddMingleAttribute(new MingleAttribute { AttributeName = "has_ethnicity", BHAttributeID = 1, MingleAttributeID = 1, BHHasOptions = true}, 9081);
            mapper.AddMingleAttributeGroup(new MingleAttributeGroup { MingleAttributeGroupID = 1, GroupID = 9081, MingleAttributeID = 1 });
            mapper.AddMingleAttributeOption(new MingleAttributeOption { Value = 1, BHValue = 2, MingleAttributeGroupID = 1 });
            
            var mingleAttributes = new Dictionary<string, object>();
            mingleAttributes.Add("has_ethnicity", 2);

            var attributes = new Attributes();
            attributes.AddAttribute(1, ScopeType.Site, "ethnicity", DataType.Number, 0);

            var mockAttributeMetadataService = new MockAttributeMetadataService { Attributes = attributes };
            var result = AttributeMapperSA.Instance.MapAttributes(mingleAttributes, null, 9081,
                                                                  mockAttributeMetadataService, mapper);

            Assert.AreEqual(result.Errors.Count, 1);
            Assert.AreEqual(result.Errors[0].ErrorType, MappingErrorType.AttributeOptionInvalid);
            Assert.AreEqual(result.Errors[0].AttributeName, "has_ethnicity");
            Assert.IsTrue(result.Errors[0].ErrorMessage.ToLower().Contains("error mapping option"));
        }
        

    }
}
    