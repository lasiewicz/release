﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.ServiceDefinitions;

namespace Spark.MingleMigration.Test
{
    public class MockAttributeMetadataService : IAttributeMetadataSA
    {
        public Attributes Attributes { get; set; }
        
        public Attributes GetAttributes()
        {
            return Attributes;
        }
    }
}
