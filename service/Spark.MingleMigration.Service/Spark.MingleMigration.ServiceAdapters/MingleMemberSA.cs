﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.RemotingClient;
using Matchnet.Search.ServiceAdapters;
using Matchnet.File.ServiceAdapters;
using Spark.Logging;
using Spark.MingleMigration.ValueObjects;
using Spark.MingleMigration.ValueObjects.ServiceDefinitions;
using Spark.SearchPreferences;
using ServiceConstants = Spark.MingleMigration.ValueObjects.ServiceConstants;
using log4net;

namespace Spark.MingleMigration.ServiceAdapters
{
    public class MingleMemberSA : SABase
    {
        public const int CACHED_MEMBER_VERSION = 5;
        public ICaching _cache;
        public static readonly MingleMemberSA Instance = new MingleMemberSA();

        private static readonly ILog Log = LogManager.GetLogger(typeof(MingleMemberSA));

        private MingleMemberSA()
        {
            _cache = Matchnet.Caching.Cache.Instance;
        }

        private ISettingsSA _settingsService = null;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public int SaveMigratedPhotoMetadata(int memberId, Brand brand, int listOrder, string caption, DateTime insertDate, bool approvedForMain, bool isMain, List<PhotoTransferFile> files)
        {
            //RollingFileLogger.Instance.LogError("MEMBER_SVC", "MingleMemberServiceSA", ex, "Error occurred while creating member. Session: " + registrationSessionID);

            var mainPhotoFile = (from file in files where file.FileType == PhotoFileType.RegularFWS select file).FirstOrDefault();
            var thumbPhotoFile = (from file in files where file.FileType == PhotoFileType.Thumbnail select file).FirstOrDefault();
            var dynamicFiles = (from file in files where file.FileType != PhotoFileType.RegularFWS && file.FileType != PhotoFileType.Thumbnail select file).ToList();

            var mainPhotoFileId = KeySA.Instance.GetKey("FileID");
            var thumbPhotoFileId = KeySA.Instance.GetKey("FileID");

            FileSA.Instance.InsertFile(mainPhotoFileId, mainPhotoFile.FileRoot, "jpg", DateTime.Now);
            FileSA.Instance.InsertFile(thumbPhotoFileId, thumbPhotoFile.FileRoot, "jpg", DateTime.Now);

            var photoFiles = new List<PhotoFile>();
            foreach (var dynamicFile in dynamicFiles)
            {
                var fileId = KeySA.Instance.GetKey("FileID");
                FileSA.Instance.InsertFile(fileId, dynamicFile.FileRoot, "jpg", DateTime.Now);

                var photoFile = new PhotoFile(dynamicFile.FileType, dynamicFile.Height, dynamicFile.Width, dynamicFile.Size, fileId, dynamicFile.FilePath, dynamicFile.CloudPath);
                photoFiles.Add(photoFile);
            }

            var photoUpdate = new PhotoUpdate(null,
                false,
                Constants.NULL_INT,
                mainPhotoFileId,
                mainPhotoFile.FilePath,
                thumbPhotoFileId,
                thumbPhotoFile.FilePath,
                (byte)listOrder,
                true,
                false,
                Constants.NULL_INT,
                Constants.NULL_INT,
                caption,
                true,
                mainPhotoFile.CloudPath,
                thumbPhotoFile.CloudPath,
                approvedForMain,
                isMain,
                true,
                mainPhotoFile.Height,
                mainPhotoFile.Width,
                mainPhotoFile.Size,
                thumbPhotoFile.Height,
                thumbPhotoFile.Width,
                thumbPhotoFile.Size);

            photoUpdate.Files = photoFiles;

            var saved = MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID,
                                                              brand.Site.Community.CommunityID, memberId,
                                                              new[] { photoUpdate });

            var member = MemberSA.Instance.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            var photos = member.GetPhotos(brand.Site.Community.CommunityID);

            if (photos == null || photos.Count == 0)
            {
                //Log.DebugFormat("Error uploading migration photo, no photos returned. MemberId: {0} List Order: {1} BrandId: {2}", memberId, listOrder, brand.BrandID);
                //success = false;
                //return null;
            }

            Photo uploadedPhoto = null;

            foreach (Photo photo in photos)
            {
                if (photo.ListOrder == listOrder)
                {
                    uploadedPhoto = photo;
                    break;
                }
            }

            return uploadedPhoto.MemberPhotoID;
        }


        public bool CreateMember(int memberId,
            Brand brand,
            string registrationSessionID,
            Int32 ipAddress,
            Dictionary<string, object> attributeValues)
        {
            string uri = "";
            var memberRecordCreated = false;

            try
            {
                //first, make sure the user exists in logon table
                uri = getServiceManagerUri();

                MemberBasicLogonInfo memberBasicLogonInfo = null;

                Checkout(uri);
                try
                {
                    memberBasicLogonInfo = getService(uri).GetMemberBasicLogonInfo(memberId, brand.Site.Community.CommunityID);
                }
                finally
                {
                    Checkin(uri);
                }

                if (memberBasicLogonInfo == null)
                {
                    throw new Exception("Member logon credentials do not exist");
                }

                Checkout(uri);
                try
                {
                    memberRecordCreated = getService(uri).CreateMemberRecord(memberId, brand);
                }
                finally
                {
                    Checkin(uri);
                }

                if (memberRecordCreated)
                {
                    var member = GetMember(memberId);

                    member.EmailAddress = memberBasicLogonInfo.EmailAddress;
                    member.SetUsername(memberBasicLogonInfo.UserName, brand, TextStatusType.Auto);

                    if (ipAddress != Constants.NULL_INT)
                    {
                        member.SetAttributeInt(brand, "RegistrationIP", ipAddress);
                    }

                    try
                    {
                        var requiredAttributeCollector = new Dictionary<string, object>();
                        SetRequiredMemberAttributes(member, brand, requiredAttributeCollector);
                        SetSuppliedMemberAttributes(member, brand, attributeValues);
                        LogRegisterAttributes(attributeValues, requiredAttributeCollector, registrationSessionID);
                    }
                    catch (Exception ex)
                    {
                        RollingFileLogger.Instance.LogError("MEMBER_SVC", "MingleMemberServiceSA", ex, "Error occurred while setting attributes. Session: " + registrationSessionID);
                        //hit an error, go ahead and save whatever attributes have been set
                        SaveMember(member, brand, false);
                        throw (new SAException("Error occurred while attempting to set new member attributes. (uri: " + uri + ")", ex));
                    }

                    SaveMember(member, brand, false);
                }

                return memberRecordCreated;
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogError("MEMBER_SVC", "MingleMemberServiceSA", ex, "Error occurred while creating member. Session: " + registrationSessionID);
                throw (new SAException("Error occurred while attempting to register new member. (uri: " + uri + ")", ex));
            }
        }

        public MemberRegisterResult SaveLogonCredentials(int memberId, string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand)
        {
            string uri = "";
            MemberRegisterResult memberRegisterResult;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    memberRegisterResult = getService(uri).SaveLogonCredentials(memberId, emailAddress.ToLower().Trim(), username.Trim(), passwordHash, passwordSalt, brand);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to save logon credentials. (uri: " + uri + ")", ex));
            }

            return memberRegisterResult;
        }


        public MingleMemberSaveResult SaveMember(Dictionary<string, object> attributeValues,
            Dictionary<string, string> approvedTextAttributeValues,
            List<SearchPreference> searchPreferences,
            List<MultiValuedSearchPreference> searchPreferencesMultiValue,
            int memberID, Brand brand, bool createMemberRecord, string password = "")
        {
            var memberSaveResult = new MingleMemberSaveResult { SaveStatus = MingleMemberSaveStatusType.Success };
            string uri = "";

            try
            {
                var member = GetMember(memberID, MemberLoadFlags.IngoreSACache);
                
                if (attributeValues != null && attributeValues.Count != 0)
                {
                    SetSuppliedMemberAttributes(member, brand, attributeValues);
                }
                if (approvedTextAttributeValues != null && approvedTextAttributeValues.Count != 0)
                {
                    SetMemberApprovedTextAttributes(member, brand, approvedTextAttributeValues);
                }

                if (!string.IsNullOrEmpty(password))
                {
                    member.Password = password;
                }

                SaveMember(member, brand, createMemberRecord);
            }
            catch (Exception ex)
            {
                memberSaveResult.SaveStatus = MingleMemberSaveStatusType.SaveMemberFailed;
                memberSaveResult.ErrorMessage = ex.Message;
                RollingFileLogger.Instance.LogException("MINGLEMEMBER_SVC", "MingleMemberServiceSA", "SaveMember", ex, memberID);
            }

            try
            {
                int? genderMask = null;

                if (attributeValues.ContainsKey(AttributeConstants.GENDERMASK))
                {
                    genderMask = Convert.ToInt32(attributeValues[AttributeConstants.GENDERMASK]);
                }

                SaveSearchPreferences(memberID, brand, searchPreferences, searchPreferencesMultiValue, genderMask);
            }
            catch (Exception ex)
            {
                memberSaveResult.SaveStatus = MingleMemberSaveStatusType.SearchPreferencesSaveFailed;
                memberSaveResult.ErrorMessage = ex.Message;
                RollingFileLogger.Instance.LogException("MINGLEMEMBER_SVC", "MingleMemberServiceSA", "SaveMember-SaveSearchPreferences", ex, memberID);
            }

            return memberSaveResult;
        }

        private MemberSaveResult SaveMember(IMember member, Brand brand, bool createMemberRecord)
        {
            var memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, Constants.NULL_STRING);
            string uri = "";

            try
            {
                if (member.MemberUpdate.IsDirty)
                {
                    uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        //If the server uses membase, no need for synchronization callback.
                        memberSaveResult = getService(uri).SaveMember(System.Environment.MachineName,
                                                                                member.MemberUpdate, CACHED_MEMBER_VERSION, brand, createMemberRecord);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                    {
                        member.MemberUpdate.Username = memberSaveResult.Username;
                        member.CommitChanges();
                    }
                }
                return memberSaveResult;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to save member. (uri: " + uri + ")", ex);
            }
        }

        private void SaveSearchPreferences(int memberID,
            Brand brand,
            List<SearchPreference> searchPreferences,
            List<MultiValuedSearchPreference> searchPreferencesMultiValue,
            int? genderMask)
        {
            if ((searchPreferences != null && searchPreferences.Count > 0) ||
                (searchPreferencesMultiValue != null && searchPreferencesMultiValue.Count > 0) ||
                genderMask.HasValue)
            {
                var searchPreferenceMapper = AttributeMapperSA.Instance.GetMingleSearchPreferenceMapper();
                var attributeMapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();
                MemberSearch memberSearch = null;
                SearchPreferenceCollection mappedSearchPreferences = null;


                var memberSearchCollection = MemberSearchPreferencesSA.Instance.GetMemberSearchCollection(memberID, brand.Site.Community.CommunityID, true);

                if (memberSearchCollection != null)
                {
                    memberSearch = memberSearchCollection.GetPrimarySearch();
                    if (memberSearch != null && memberSearch.SearchPreferenceCollection != null)
                    {
                        mappedSearchPreferences = memberSearch.SearchPreferenceCollection;
                    }
                }

                if (mappedSearchPreferences == null)
                {
                    mappedSearchPreferences = new SearchPreferenceCollection();
                }

                if (searchPreferences != null && searchPreferences.Any())
                {
                    foreach (var preference in searchPreferences)
                    {
                        var mappedPreference = searchPreferenceMapper.GetSearchPreference(preference.Name, brand.Site.SiteID);

                        if (Convert.ToInt32(preference.Value) == -1)
                        {
                            mappedSearchPreferences.Remove(mappedPreference.BHPreferenceName);
                        }
                        else
                        {
                            mappedSearchPreferences.Add(new SearchPreference
                            {
                                Name = mappedPreference.BHPreferenceName,
                                Weight = preference.Weight,
                                Value = preference.Value
                            });
                        }
                    }
                }

                if (searchPreferencesMultiValue != null && searchPreferencesMultiValue.Any())
                {
                    foreach (var preference in searchPreferencesMultiValue)
                    {
                        var mappedPreference = searchPreferenceMapper.GetSearchPreference(preference.Name, brand.Site.SiteID);

                        if (preference.Values.Count == 1 && preference.Values.First() == -1)
                        {
                            mappedSearchPreferences.Remove(mappedPreference.BHPreferenceName);
                        }
                        else
                        {
                            var mappedOptions = attributeMapper.GetMingleOptionsForAttributeAndGroup(mappedPreference.RelatedMingleAttributeName,
                                    brand.Site.SiteID);

                            int totalPreferenceValue = 0;
                            foreach (var suppliedOption in preference.Values)
                            {
                                var mappedOption = (from o in mappedOptions where o.Value == suppliedOption select o).First();
                                totalPreferenceValue = totalPreferenceValue + mappedOption.BHValue;
                            }

                            mappedSearchPreferences.Add(new SearchPreference { Name = mappedPreference.BHPreferenceName, Weight = preference.Weight, Value = totalPreferenceValue.ToString() });
                        }

                    }
                }

                if (!mappedSearchPreferences.Contains(SearchPreferenceConstants.SearchType))
                {
                    mappedSearchPreferences.Add(new SearchPreference { Name = SearchPreferenceConstants.SearchType, Weight = 0, Value = SearchTypeID.Region.ToString("d") });
                }

                if (!mappedSearchPreferences.Contains(SearchPreferenceConstants.OrderBy))
                {
                    mappedSearchPreferences.Add(new SearchPreference { Name = SearchPreferenceConstants.OrderBy, Weight = 0, Value = QuerySorting.Popularity.ToString("d") });
                }

                if (genderMask.HasValue)
                {
                    mappedSearchPreferences.Add(new SearchPreference { Name = SearchPreferenceConstants.GenderMask, Weight = 0, Value = genderMask.Value.ToString() });
                }

                memberSearch = new MemberSearch(memberID, brand.Site.Community.CommunityID, MemberSearchCollection.DEFAULTSEARCHNAME, true, mappedSearchPreferences);
                if (memberSearchCollection == null)
                {
                    memberSearchCollection = new MemberSearchCollection();
                }

                memberSearchCollection.RemovePrimary();
                memberSearchCollection.Add(memberSearch);

                MemberSearchPreferencesSA.Instance.SaveMemberSearchCollection(memberSearchCollection, memberID, brand.Site.Community.CommunityID);
            }
        }

        private void LogRegisterAttributes(Dictionary<string, object> attributeValues, Dictionary<string, object> requiredAttributeCollector, string registrationSessionID)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Member Attribute Dump");
            foreach (var attribute in attributeValues)
            {
                var value = attribute.Key.ToLower() == "password" ? string.Empty : attribute.Value.ToString();
                sb.AppendLine(attribute.Key + ": " + value);
            }
            foreach (var attribute in requiredAttributeCollector)
            {
                var value = attribute.Key.ToLower() == "password" ? string.Empty : attribute.Value.ToString();
                sb.AppendLine(attribute.Key + ": " + value);
            }

            RollingFileLogger.Instance.LogInfoMessage("MINGLEMEMBER_SVC", "MingleMemberServiceSA", sb.ToString(), registrationSessionID);
        }

        private void SetRequiredMemberAttributes(Member member, Brand brand, Dictionary<string, object> attributeValues)
        {
            DateTime now = DateTime.Now;
            member.SetAttributeDate(brand, "BrandInsertDate", now);
            attributeValues.Add("BrandInsertDate", now);

            member.SetAttributeDate(brand, "LastUpdated", now);
            attributeValues.Add("LastUpdated", now);

            member.SetAttributeDate(brand, "BrandLastLogonDateOldValue", member.GetAttributeDate(brand, "BrandLastLogonDate"));
            attributeValues.Add("BrandLastLogonDateOldValue", member.GetAttributeDate(brand, "BrandLastLogonDate"));

            member.SetAttributeDate(brand, "BrandLastLogonDate", now);
            attributeValues.Add("BrandLastLogonDate", now);

            member.SetAttributeInt(brand, "LastBrandID", brand.BrandID);
            attributeValues.Add("LastBrandID", brand.BrandID);

            Int32 brandLogonCount = member.GetAttributeInt(brand, "BrandLogonCount", 0);
            member.SetAttributeInt(brand, "BrandLogonCount", brandLogonCount + 1);
            attributeValues.Add("BrandLogonCount", brandLogonCount + 1);

            // photo requirement related logging
            bool photoRequiredSite =
                Convert.ToBoolean(SettingsService.GetSettingFromSingleton("PHOTO_REQUIRED_SITE",
                                                                          brand.Site.Community.CommunityID,
                                                                          brand.Site.SiteID));
            if (photoRequiredSite)
            {
                DateTime rebrandLogonDate = member.GetAttributeDate(brand, "RebrandFirstLogonDate", DateTime.MinValue);
                if (rebrandLogonDate == DateTime.MinValue)
                {
                    member.SetAttributeDate(brand, "RebrandFirstLogonDate", now);
                    attributeValues.Add("RebrandFirstLogonDate", now);
                    // this attribute is immutable so it's safe to set it multiple times
                }
            }

            // Let's flip the FTAApproved status to false here so that they have to go through the FTA approval check
            member.SetAttributeInt(brand, "FTAApproved", 0);
            attributeValues.Add("FTAApproved", 0);
        }

        private void SetMemberApprovedTextAttributes(Member member, Brand brand, Dictionary<string, string> attributeValues)
        {
            foreach (var attribute in attributeValues)
            {
                if (attribute.Key.ToLower() == "username")
                {
                    member.SetUsername(attribute.Value, brand, TextStatusType.Human);
                }
                member.SetAttributeText(brand, attribute.Key, attribute.Value, TextStatusType.Human);
            }
        }

        private void SetSuppliedMemberAttributes(Member member, Brand brand, Dictionary<string, object> attributeValues)
        {
            foreach (var attribute in attributeValues)
            {
                var mdAttribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attribute.Key);

                switch (mdAttribute.DataType.ToString().ToUpper())
                {
                    case "MASK":
                    case "NUMBER":
                        var intVal = Constants.NULL_INT;
                        if (attribute.Value is Int64)
                        {
                            intVal = (int)(long)(attribute.Value); // direct cast to int would fail, need to cast to long first
                        }
                        else if (attribute.Value is Int32)
                        {
                            intVal = (int)(attribute.Value);
                        }

                        member.SetAttributeInt(brand, attribute.Key, intVal);
                        break;
                    case "TEXT":
                        var mdAttributeGroup = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(mdAttribute, brand);
                        var textValue = attribute.Value as string;
                        var requiresApproval = (((int)mdAttributeGroup.Status) &
                                ((int)Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval)) ==
                                ((int)Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.RequiresTextApproval);
                        var textStatusType = requiresApproval ? TextStatusType.None : TextStatusType.Auto;

                        if (attribute.Key.ToLower() == "emailaddress")
                        {
                            member.EmailAddress = textValue;
                        }
                        else if (attribute.Key.ToLower() == "username")
                        {
                            member.SetUsername(textValue, brand, TextStatusType.Auto);
                        }
                        else
                        {
                            member.SetAttributeText(brand, attribute.Key, textValue, textStatusType);
                        }

                        break;
                    case "BIT":
                        var boolVal = ((bool)attribute.Value) ? 1 : 0;
                        member.SetAttributeInt(brand, attribute.Key, boolVal);
                        break;
                    case "DATE":
                        DateTime dateTimeAttribute;
                        DateTime.TryParse(Convert.ToString(attribute.Value), out dateTimeAttribute);
                        member.SetAttributeDate(brand, attribute.Key, dateTimeAttribute);
                        break;
                }
            }
        }

        public Member GetMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            string uri = "";
            
            try
            {
                var logCachingEvents = 
                    Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.LOG_MIGRATION_SERVICE_CACHING_UPDATES));
                
                Member member = null;
                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                CachedMember cachedMember = null;
                bool ignoreSACache = (memberLoadFlags & MemberLoadFlags.IngoreSACache) == MemberLoadFlags.IngoreSACache;

                if (!ignoreSACache)
                {
                    cachedMember = _cache.Get(CachedMember.GetCacheKey(memberID)) as CachedMember;
                }

                if (cachedMember == null || cachedMember.Version != CACHED_MEMBER_VERSION)
                {
                    Int32 cacheTTL = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = getServiceManagerUri(memberID);
                    
                    base.Checkout(uri);
                    try
                    {
                        cachedMember = new CachedMember(getService(uri).GetCachedMemberBytes(System.Environment.MachineName,
                            Matchnet.CacheSynchronization.Context.Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                            memberID, false, CACHED_MEMBER_VERSION), CACHED_MEMBER_VERSION);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (logCachingEvents)
                    {
                        Log.Debug("Retrieved member from service:" + memberID);
                    }

                    if (!ignoreSACache)
                    {
                        cachedMember.CacheTTLSeconds = cacheTTL;
                        _cache.Insert(cachedMember);
                    }
                }
                else
                {
                    if (logCachingEvents)
                    {
                        Log.Debug("Retrieved member from cache:" + memberID);
                    }
                }

                return new Member(cachedMember);
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to retrieve member from cache or middle-tier service.", ex);
            }
        }


        public Member GetMember(int memberID)
        {
            string uri = "";

            try
            {
                CachedMember cachedMember = null;

                if (cachedMember == null || cachedMember.Version != CACHED_MEMBER_VERSION)
                {
                    Int32 cacheTTL = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                    uri = getServiceManagerUri(memberID);
                    DateTime begin = DateTime.Now;
                    base.Checkout(uri);
                    try
                    {
                        cachedMember = new CachedMember(getService(uri).GetCachedMemberBytes(System.Environment.MachineName,
                            memberID,
                            false, CACHED_MEMBER_VERSION), CACHED_MEMBER_VERSION);

                        ArrayList communityIDList = cachedMember.GetCommunityIDList(AttributeMetadataSA.Instance.GetAttributes(),
                            BrandConfigSA.Instance.GetBrands());

                        for (Int32 i = 0; i < communityIDList.Count; i++)
                        {
                            Int32 communityID = (Int32)communityIDList[i];
                            if (Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MEMBERSVC_PHOTO_RELATIVEPATHS", communityID)))
                            {
                                cachedMember.Photos.GetCommunity(communityID).StripHostFromPaths();
                            }
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                    if (totalSeconds > 1)
                    {
                        System.Diagnostics.Trace.WriteLine("__GetCachedMember(" + memberID.ToString() + ") " + totalSeconds.ToString());
                    }

                    cachedMember.CacheTTLSeconds = cacheTTL;
                }

                return new Member(cachedMember);
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while attempting to get cached member. (uri: " + uri + ")", ex);
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("MINGLEMIGRATIONSVC_SA_CONNECTION_LIMIT"));
        }

        public string EncryptPassword(string password)
        {
            string passwordHash = Constants.NULL_STRING;
            if (!string.IsNullOrEmpty(password))
            {
                int workFactor = 10;
                try
                {
                    string workFactorStr = SettingsService.GetSettingFromSingleton("CRYPTO_WORK_FACTOR");
                    workFactor = Convert.ToInt32(workFactorStr);
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberBL", e, null);
                    workFactor = 10;
                }
                passwordHash = Matchnet.Security.Crypto.Instance.EncryptTextPlusSalt(password, workFactor);
            }
            return passwordHash;
        }

        public string EncryptEmail(string emailAddress)
        {
            string encryptedEmail = Constants.NULL_STRING;
            if (!string.IsNullOrEmpty(emailAddress))
            {
                string key2 = SettingsService.GetSettingFromSingleton("KEY2");
                encryptedEmail = Matchnet.Security.Crypto.Instance.EncryptText(emailAddress.ToLower(), key2);
            }
            return encryptedEmail;
        }


        internal static IMingleMember getService(string uri)
        {
            try
            {
                return (IMingleMember)Activator.GetObject(typeof(IMingleMember), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        internal static string getServiceManagerUri()
        {
            try
            {
                string uri = AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.MINGLEMEMBER_SERVICE_MANAGER_NAME);
                string overrideHostName = RuntimeSettings.GetSetting("MINGLEMIGRATIONSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        internal static string getServiceManagerUri(int memberID)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(ServiceConstants.MINGLEMEMBER_SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLEMIGRATIONSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }
    }
}
