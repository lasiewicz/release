﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Caching; 
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.MingleMigration.ValueObjects;
using Spark.MingleMigration.ValueObjects.ServiceDefinitions;
using Spark.SearchPreferences;


namespace Spark.MingleMigration.ServiceAdapters
{
    public class AttributeMapperSA: SABase//, IAttributeMapperSA  
    {
        public static readonly AttributeMapperSA Instance = new AttributeMapperSA();
        
        private AttributeMapperSA()
        {
        }

        private ISettingsSA _settingsService = null;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public AttributeMapper GetMingleAttributeMapper()
        {
            string uri = "";
            AttributeMapper attributeMapper = null;

            try
            {
                attributeMapper = Cache.Instance.Get(AttributeMapper.CACHE_KEY) as AttributeMapper;

                if (attributeMapper == null)
                {
                    uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                       attributeMapper = getService(uri).GetMingleAttributeMapper();
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                    Cache.Instance.Add(attributeMapper);
                }

                return attributeMapper;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occured when loading mingle attribute mapping data. (uri: " + uri + ")", ex));
            } 

        }

        public SearchPreferenceMapper GetMingleSearchPreferenceMapper()
        {
            string uri = "";
            SearchPreferenceMapper searchPreferenceMapper = null;

            try
            {
                searchPreferenceMapper = Cache.Instance.Get(SearchPreferenceMapper.CACHE_KEY) as SearchPreferenceMapper;

                if (searchPreferenceMapper == null)
                {
                    uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        searchPreferenceMapper = getService(uri).GetMingleSearchPreferenceMapper();
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                    Cache.Instance.Add(searchPreferenceMapper);
                }

                return searchPreferenceMapper;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occured when loading mingle search preference mapping data. (uri: " + uri + ")", ex));
            }

        }

        public AttributeMappingResult MapTextAttributes(Dictionary<string, string> attributeData, int siteID, IAttributeMetadataSA attributeMetadataService, AttributeMapper mapper)
        {
            var result = new AttributeMappingResult { Success = true };
            Hashtable bhAttributes = null;

            if (attributeData == null || attributeData.Count == 0)
            {
                result.Errors.Add(new AttributeMappingError { AttributeName = "attributeData", ErrorType = MappingErrorType.InvalidParameters, ErrorMessage = "Missing attributeData parameter." });
                return result;
            }

            if (mapper == null)
            {
                try
                {
                    mapper = GetMingleAttributeMapper();
                }
                catch (Exception ex)
                {
                    result.Errors.Add(new AttributeMappingError
                    {
                        ErrorType = MappingErrorType.GeneralError,
                        ErrorMessage = "Error retrieving AttributeMapper. Exception: " + ex.Message
                    });
                    result.Success = false;
                    return result;
                }
            }

            try
            {
                bhAttributes = attributeMetadataService.GetAttributes().GetAllAttributes();
            }
            catch (Exception ex)
            {
                result.Errors.Add(new AttributeMappingError { ErrorType = MappingErrorType.GeneralError, ErrorMessage = "Error retrieving BH attributes. Exception: " + ex.Message });
                result.Success = false;
                return result;
            }

            foreach (var attributeName in attributeData.Keys)
            {
                try
                {
                    var mappedAttribute = mapper.GetMingleAttributeByNameAndGroup(attributeName, siteID);
                    if (mappedAttribute == null)
                    {
                        result.Errors.Add(new AttributeMappingError { AttributeName = attributeName, ErrorType = MappingErrorType.AttributeInvalid, ErrorMessage = "There is no valid mapping for this attribute." });
                        result.Success = false;
                        continue;
                    }
                    var bhAttribute = bhAttributes[mappedAttribute.BHAttributeID] as Matchnet.Content.ValueObjects.AttributeMetadata.Attribute;
                    result.MappedAttributes.Add(bhAttribute.Name, attributeData[attributeName]);
                   
                }
                catch (Exception ex)
                {
                    result.Errors.Add(new AttributeMappingError { AttributeName = attributeName, ErrorType = MappingErrorType.AttributeGeneralMappingError, ErrorMessage = "Error mapping attribute. Exception: " + ex.Message });
                    result.Success = false;
                }
            }

            return result;
        }

        public AttributeMappingResult MapAttributes(Dictionary<string, object> attributeData, Dictionary<string, List<int>> attributeDataMultiValue, int siteID, IAttributeMetadataSA attributeMetadataService, AttributeMapper mapper)
        {
            var result = new AttributeMappingResult {Success = true};
            Hashtable bhAttributes = null;
            
            if (mapper == null)
            {
                try
                {
                    mapper = GetMingleAttributeMapper();
                }
                catch (Exception ex)
                {
                    result.Errors.Add(new AttributeMappingError
                                          {
                                              ErrorType = MappingErrorType.GeneralError,
                                              ErrorMessage = "Error retrieving AttributeMapper. Exception: " + ex.Message
                                          });
                    result.Success = false;
                    return result;
                }
            }

            try
            {
                bhAttributes = attributeMetadataService.GetAttributes().GetAllAttributes();
            }
            catch (Exception ex)
            {
                result.Errors.Add(new AttributeMappingError { ErrorType = MappingErrorType.GeneralError, ErrorMessage = "Error retrieving BH attributes. Exception: " + ex.Message });
                result.Success = false;
                return result;
            }

            if (attributeData != null && attributeData.Count > 0)
            {
                foreach (var attributeName in attributeData.Keys)
                {
                    try
                    {
                        var mappedAttribute = mapper.GetMingleAttributeByNameAndGroup(attributeName, siteID);
                        if (mappedAttribute == null)
                        {
                            result.Errors.Add(new AttributeMappingError
                                                  {
                                                      AttributeName = attributeName,
                                                      ErrorType = MappingErrorType.AttributeInvalid,
                                                      ErrorMessage = "There is no valid mapping for this attribute."
                                                  });
                            result.Success = false;
                            continue;
                        }
                        var bhAttribute = bhAttributes[mappedAttribute.BHAttributeID] as Matchnet.Content.ValueObjects.AttributeMetadata.Attribute;

                        switch (bhAttribute.DataType)
                        {
                            case DataType.Date:
                                DateTime dateTimeAttribute;
                                if (
                                    !(DateTime.TryParse(Convert.ToString(attributeData[attributeName]),
                                                        out dateTimeAttribute)))
                                {
                                    result.Errors.Add(new AttributeMappingError
                                                          {
                                                              AttributeName = attributeName,
                                                              ErrorType = MappingErrorType.AttributeIncorrectDataType,
                                                              ErrorMessage =
                                                                  "Invalid attribute datatype, expected DateTime."
                                                          });
                                    result.Success = false;
                                    continue;
                                }
                                break;
                            case DataType.Bit:
                                if (!(attributeData[attributeName] is Boolean))
                                {
                                    result.Errors.Add(new AttributeMappingError
                                                          {
                                                              AttributeName = attributeName,
                                                              ErrorType = MappingErrorType.AttributeIncorrectDataType,
                                                              ErrorMessage =
                                                                  "Invalid attribute datatype, expected Boolean."
                                                          });
                                    result.Success = false;
                                    continue;
                                }
                                break;
                            case DataType.Number:
                                if (!(attributeData[attributeName] is Int64) && !(attributeData[attributeName] is Int32))
                                {
                                    result.Errors.Add(new AttributeMappingError
                                                          {
                                                              AttributeName = attributeName,
                                                              ErrorType = MappingErrorType.AttributeIncorrectDataType,
                                                              ErrorMessage = "Invalid attribute datatype, expected Number."
                                                          });
                                    result.Success = false;
                                    continue;
                                }
                                break;
                            case DataType.Mask:
                                result.Errors.Add(new AttributeMappingError
                                                      {
                                                          AttributeName = attributeName,
                                                          ErrorType = MappingErrorType.AttributeIncorrectDataType,
                                                          ErrorMessage =
                                                              "Invalid attribute datatype, Mask values should be passed in multi-value attributes."
                                                      });
                                result.Success = false;
                                continue;
                        }
                        if (mappedAttribute.BHHasOptions)
                        {
                            var mappedOptions = mapper.GetMingleOptionsForAttributeAndGroup(attributeName, siteID);

                            if (mappedOptions == null)
                            {
                                result.Errors.Add(new AttributeMappingError
                                                      {
                                                          AttributeName = attributeName,
                                                          ErrorType = MappingErrorType.AttributeOptionsNotDefined,
                                                          ErrorMessage =
                                                              "There are no mapped options for this attribute and site."
                                                      });
                                result.Success = false;
                            }
                            else
                            {
                                if (Convert.ToInt32(attributeData[attributeName]) ==-1)
                                {
                                    result.MappedAttributes.Add(bhAttribute.Name, Constants.NULL_INT);
                                }
                                else
                                {
                                    //this is an attribute with options, so we need to translate the option value. And if the mapping doesn't exist, throw an exception
                                    var mappedOption =
                                        (from o in mappedOptions
                                         where o.Value == Convert.ToInt32(attributeData[attributeName])
                                         select o).FirstOrDefault();
                                    if (mappedOption == null)
                                    {
                                        result.Errors.Add(new AttributeMappingError
                                                              {
                                                                  AttributeName = attributeName,
                                                                  ErrorType = MappingErrorType.AttributeOptionInvalid,
                                                                  ErrorMessage =
                                                                      "Error mapping option value for this attribute. Option Value: " +
                                                                      attributeData[attributeName]
                                                              });
                                        result.Success = false;
                                    }
                                    else
                                    {
                                        result.MappedAttributes.Add(bhAttribute.Name, mappedOption.BHValue);
                                    }
                                }
                            }
                        }
                        else
                        {
                            result.MappedAttributes.Add(bhAttribute.Name, attributeData[attributeName]);
                        }
                    }
                    catch (Exception ex)
                    {
                        result.Errors.Add(new AttributeMappingError
                                              {
                                                  AttributeName = attributeName,
                                                  ErrorType = MappingErrorType.AttributeGeneralMappingError,
                                                  ErrorMessage = "Error mapping attribute. Exception: " + ex.Message
                                              });
                        result.Success = false;
                    }
                }
            }

            if (attributeDataMultiValue != null && attributeDataMultiValue.Count>0)
            {
                foreach (var attributeName in attributeDataMultiValue.Keys)
                {
                    var allOptionsMapped = true;
                    try
                    {
                        if (attributeDataMultiValue[attributeName] == null ||
                            attributeDataMultiValue[attributeName].Count == 0)
                        {
                            result.Errors.Add(new AttributeMappingError
                                                  {
                                                      AttributeName = attributeName,
                                                      ErrorType = MappingErrorType.InvalidParameters,
                                                      ErrorMessage = "No options supplied for multivalued attribute."
                                                  });
                            result.Success = false;
                        }

                        var mappedAttribute = mapper.GetMingleAttributeByNameAndGroup(attributeName, siteID);
                        if (mappedAttribute == null)
                        {
                            result.Errors.Add(new AttributeMappingError
                                                  {
                                                      AttributeName = attributeName,
                                                      ErrorType = MappingErrorType.AttributeInvalid,
                                                      ErrorMessage = "There is no valid mapping for this attribute."
                                                  });
                            result.Success = false;
                            continue;
                        }

                        var mappedOptions = mapper.GetMingleOptionsForAttributeAndGroup(attributeName, siteID);
                        var bhAttribute = bhAttributes[mappedAttribute.BHAttributeID] as Matchnet.Content.ValueObjects.AttributeMetadata.Attribute;

                        if (mappedOptions == null)
                        {
                            result.Errors.Add(new AttributeMappingError
                                                  {
                                                      AttributeName = attributeName,
                                                      ErrorType = MappingErrorType.AttributeOptionsNotDefined,
                                                      ErrorMessage =
                                                          "There are no mapped options for this attribute and site."
                                                  });
                            result.Success = false;    
                            continue;
                        }

                        var totalAttributeValue = 0;

                        if (attributeDataMultiValue[attributeName].Count == 1 && attributeDataMultiValue[attributeName][0] == -1)
                        {
                            result.MappedAttributes.Add(bhAttribute.Name, Constants.NULL_INT);
                        }
                        else
                        {
                            foreach (var suppliedOption in attributeDataMultiValue[attributeName])
                            {
                                var mappedOption =
                                    (from o in mappedOptions where o.Value == suppliedOption select o).FirstOrDefault();
                                if (mappedOption == null)
                                {
                                    result.Errors.Add(new AttributeMappingError
                                                          {
                                                              AttributeName = attributeName,
                                                              ErrorType = MappingErrorType.AttributeOptionInvalid,
                                                              ErrorMessage =
                                                                  "Error mapping option for this attribute. Option Value: " +
                                                                  suppliedOption.ToString()
                                                          });
                                    allOptionsMapped = false;
                                    result.Success = false;
                                }
                                else
                                {
                                    // don't map duplicate values from mingle
                                    if ((totalAttributeValue & mappedOption.BHValue) != mappedOption.BHValue)
                                        totalAttributeValue = totalAttributeValue + mappedOption.BHValue;
                                }
                            }

                            if (allOptionsMapped)
                            {
                                result.MappedAttributes.Add(bhAttribute.Name, totalAttributeValue);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        result.Errors.Add(new AttributeMappingError
                                              {
                                                  AttributeName = attributeName,
                                                  ErrorType = MappingErrorType.AttributeGeneralMappingError,
                                                  ErrorMessage = "Error mapping attribute. Exception: " + ex.Message
                                              });
                        result.Success = false;
                    }
                }
            }

            return result;
        }

        public AttributeMappingResult ReverseMapAttributes(
                    Dictionary<string, object> attributeData,
                    int siteId, 
                    IAttributeMetadataSA attributeMetadataService,
                    AttributeMapper mapper)
        {
            var result = new AttributeMappingResult
            {
                Success = true,
                MappedAttributes = new Dictionary<string, object>()
            };

            if (mapper == null)
            {
                try
                {
                    mapper = GetMingleAttributeMapper();
                }
                catch (Exception ex)
                {
                    result.Errors.Add(new AttributeMappingError
                    {
                        ErrorType = MappingErrorType.GeneralError,
                        ErrorMessage = "Error retrieving AttributeMapper. Exception: " + ex.Message
                    });
                    result.Success = false;
                    return result;
                }
            }

            Hashtable bhAttributes = null;

            try
            {
                bhAttributes = attributeMetadataService.GetAttributes().GetAllAttributes();
            }
            catch (Exception ex)
            {
                result.Errors.Add(new AttributeMappingError { ErrorType = MappingErrorType.GeneralError, ErrorMessage = "Error retrieving BH attributes. Exception: " + ex.Message });
                result.Success = false;
                return result;
            }

            if (attributeData != null && attributeData.Count > 0)
            {
                foreach (var attributeName in attributeData.Keys)
                {
                    try
                    {
                        var mingleAttribute = mapper.GetMingleAttributeByBhName(attributeName);
                        if (mingleAttribute == null)
                            throw new Exception(string.Format("Cannot map BH attribute {0} to mingle attribute.", attributeName));

                        var bhAttributeId = mapper.GetBhAttributeIdFromName(attributeName);
                        if (bhAttributeId == Constants.NULL_INT)
                            throw new Exception(string.Format("Invalid BH attribute {0}.", attributeName));

                        var bhAttribute = (Matchnet.Content.ValueObjects.AttributeMetadata.Attribute)bhAttributes[bhAttributeId];
                        if (bhAttribute == null)
                            throw new Exception(string.Format("Cannot get BH attribute {0} from cache.", attributeName));

                        switch (bhAttribute.DataType)
                        {
                            case DataType.Text:
                            case DataType.Date:
                            case DataType.Bit:
                                result.MappedAttributes.Add(mingleAttribute.AttributeName, attributeData[attributeName]);
                                break;

                            case DataType.Number:
                            case DataType.Mask: // we can process masks as long as they have only one value
                                if (mingleAttribute.BHHasOptions)
                                {
                                    var mingleAttributeGroup =
                                        mapper.GetMingleAttributeGroupBhAttribute(mingleAttribute.MingleAttributeID,
                                            siteId);
                                    if (mingleAttributeGroup != null)
                                    {
                                        var mingleAttributeOption =
                                            mapper.GetMingleAttributeOptionFromBhAttribute(
                                                mingleAttributeGroup.MingleAttributeGroupID,
                                                Convert.ToInt32(attributeData[attributeName]));

                                        if (mingleAttributeOption != null &&
                                            mingleAttributeOption.Value != Constants.NULL_INT)
                                        {
                                            // found attribute and value for mingle
                                            result.MappedAttributes.Add(mingleAttribute.AttributeName,
                                                mingleAttributeOption.Value);
                                        }
                                        else
                                        {
                                            // no option value found for mingle -> add originalBH value
                                            result.MappedAttributes.Add(mingleAttribute.AttributeName,
                                                attributeData[attributeName]);
                                        }
                                    }
                                    else
                                    {
                                        // no group found for mingle -> add originalBH value
                                        result.MappedAttributes.Add(mingleAttribute.AttributeName,
                                            attributeData[attributeName]);
                                    }
                                }
                                else
                                {
                                    // no options -> add originalBH value
                                    result.MappedAttributes.Add(mingleAttribute.AttributeName,
                                        attributeData[attributeName]);
                                }
                                break;

                            //case DataType.Mask:
                            //    throw new Exception(string.Format("Cannot map masked attribute {0}. Please use multi values instead.", attributeName));
                        }
                    }
                    catch (Exception ex)
                    {
                        result.Errors.Add(new AttributeMappingError
                        {
                            AttributeName = attributeName,
                            ErrorType = MappingErrorType.AttributeGeneralMappingError,
                            ErrorMessage = "Error mapping attribute. Exception: " + ex.Message
                        });

                        result.Success = false;
                    }
                }
            }

            return result;
        }

        public SearchPreferenceValdationResult ValidateSearchPreferences(List<SearchPreference> searchPreferences, List<MultiValuedSearchPreference> multiValuedSearchPreferences,
            SearchPreferenceMapper searchPreferenceMapper, AttributeMapper attributeMapper, int siteID)
        {
            var result = new SearchPreferenceValdationResult {Success = true};

            if(searchPreferences != null)
            {
                foreach(var preference in searchPreferences)
                {
                    var mappedPreference = searchPreferenceMapper.GetSearchPreference(preference.Name, siteID);
                    if(mappedPreference == null)
                    {
                        result.Errors.Add(new SearchPreferenceValidationError 
                        {
                            ErrorType = PreferenceValidationErrorType.PreferenceNotMapped,
                            PreferenceName = preference.Name,
                            ErrorMessage = string.Format("'{0}' is not mapped", preference.Name)
                        });
                        result.Success = false;
                        continue;
                    }
                    if(mappedPreference.IsMaskValue)
                    {
                        result.Errors.Add(new SearchPreferenceValidationError
                        {
                            ErrorType = PreferenceValidationErrorType.PreferenceIsMultiValued,
                            PreferenceName = preference.Name,
                            ErrorMessage = string.Format("'{0}' should be multivalued", preference.Name)
                        });
                        result.Success = false;
                        continue;
                    }
                    if(!string.IsNullOrEmpty(mappedPreference.RelatedMingleAttributeName))
                    {
                        var relatedAttribute = attributeMapper.GetMingleAttributeByNameAndGroup(mappedPreference.RelatedMingleAttributeName,
                                                                         siteID);
                        if(relatedAttribute == null)
                        {
                            result.Errors.Add(new SearchPreferenceValidationError
                            {
                                ErrorType = PreferenceValidationErrorType.PreferenceHasNonexistentRelatedAttribute,
                                PreferenceName = preference.Name,
                                ErrorMessage = string.Format("'{0}' has a related attribute '{1}' that is not mapped", preference.Name, mappedPreference.RelatedMingleAttributeName)
                            });
                            result.Success = false;
                        }
                    }
                }
            }

            if (multiValuedSearchPreferences != null)
            {
                foreach (var preference in multiValuedSearchPreferences)
                {
                    var mappedPreference = searchPreferenceMapper.GetSearchPreference(preference.Name, siteID);
                    if (mappedPreference == null)
                    {
                        result.Errors.Add(new SearchPreferenceValidationError
                                              {
                                                  ErrorType = PreferenceValidationErrorType.PreferenceNotMapped,
                                                  PreferenceName = preference.Name,
                                                  ErrorMessage = string.Format("'{0}' is not mapped", preference.Name)
                                              });
                        result.Success = false;
                        continue;
                    }
                    if (!mappedPreference.IsMaskValue)
                    {
                        result.Errors.Add(new SearchPreferenceValidationError
                        {
                            ErrorType = PreferenceValidationErrorType.PreferenceIsSingleValued,
                            PreferenceName = preference.Name,
                            ErrorMessage = string.Format("'{0}' should be single valued", preference.Name)
                        });
                        result.Success = false;
                        continue;
                    }
                    if (!string.IsNullOrEmpty(mappedPreference.RelatedMingleAttributeName))
                    {
                        var mappedOptions = attributeMapper.GetMingleOptionsForAttributeAndGroup(mappedPreference.RelatedMingleAttributeName,
                            siteID);

                        if(mappedOptions == null || mappedOptions.Count == 0)
                        {
                            result.Errors.Add(new SearchPreferenceValidationError
                            {
                                ErrorType = PreferenceValidationErrorType.PreferenceHasNonexistentRelatedAttribute,
                                PreferenceName = preference.Name,
                                ErrorMessage = string.Format("'{0}' has no mapped options", preference.Name)
                            });
                            result.Success = false;
                            continue;
                        }

                        if (!(preference.Values.Count == 1 && preference.Values.First() == -1))
                        {
                            foreach (var suppliedOption in preference.Values)
                            {
                                var mappedOption = (from o in mappedOptions where o.Value == suppliedOption select o).FirstOrDefault();
                                if (mappedOption == null)
                                {
                                    result.Errors.Add(new SearchPreferenceValidationError
                                                          {
                                                              ErrorType =PreferenceValidationErrorType.PreferenceHasInvalidMappedOption,
                                                              PreferenceName = preference.Name,
                                                              ErrorMessage =string.Format("'{0}' has invalid option '{1}'",preference.Name,suppliedOption.ToString())
                                                          });
                                    result.Success = false;
                                }
                            }
                        }

                    }
                }
            }

            return result;
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("MINGLEMIGRATIONSVC_SA_CONNECTION_LIMIT"));
        }

        internal static IAttributeMapper getService(string uri)
        {
            try
            {
                return (IAttributeMapper)Activator.GetObject(typeof(IAttributeMapper), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        internal static string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.ATTRIBUTEMAPPER_SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MINGLEMIGRATIONSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }
    }
}
