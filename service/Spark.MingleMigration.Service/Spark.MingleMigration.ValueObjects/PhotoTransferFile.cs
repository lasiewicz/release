﻿#region

using Matchnet.Member.ValueObjects.Photos;

#endregion

namespace Spark.MingleMigration.ValueObjects
{
    public class PhotoTransferFile
    {
        public PhotoTransferFile()
        {
            FileRoot = 0;
            FilePath = string.Empty;
        }

        public PhotoFileType FileType { get; set; }
        public int FileRoot { get; set; }
        public string FilePath { get; set; }
        public string CloudPath { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Size { get; set; }
    }
}