﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Spark.MingleMigration.ValueObjects
{
    [DataContract(Name = "MultiValuedSearchPreference")]
    public class MultiValuedSearchPreference
    {
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        [DataMember(Name = "Values")]
        public List<int> Values { get; set; }

        [DataMember(Name = "Weight")]
        public int Weight { get; set; }
    }
}
