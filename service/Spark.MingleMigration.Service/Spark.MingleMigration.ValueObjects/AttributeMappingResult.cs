﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    public class AttributeMappingResult
    {
        public bool Success { get; set; }
        public Dictionary<string, object> MappedAttributes { get; set; }
        public List<AttributeMappingError> Errors { get; set; }

        public AttributeMappingResult()
        {
            MappedAttributes = new Dictionary<string, object>();
            Errors = new List<AttributeMappingError>();
        }
    }
}
