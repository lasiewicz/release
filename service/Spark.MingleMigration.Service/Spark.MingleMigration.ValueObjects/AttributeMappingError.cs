﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    public class AttributeMappingError
    {
        public MappingErrorType ErrorType { get; set; }
        public string ErrorMessage { get; set; }
        public string AttributeName { get; set; }
    }
}
