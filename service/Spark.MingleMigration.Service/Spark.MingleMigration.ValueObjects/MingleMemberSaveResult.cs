﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public enum MingleMemberSaveStatusType : int
    {
        /// <summary>An enum indicating a success in saving the Member.</summary>
        Success = 0,
        
        SearchPreferencesSaveFailed = 1, 

        SaveMemberFailed=2,

        /// <summary>An enum indicating an email address that is in the banned list.  The Member cannot be saved.</summary>
        BannedEmailAddress = 519238,

        /// <summary>An enum indicating an email address that already exists in the system.  The Member cannot be saved. </summary>
        EmailAddressExists = 400000,

        /// <summary>An enum indicating an email address that is invalid due to length or invalid characters.  The Member cannot be saved.</summary>
        InvalidEmail = 519385

    }
    
    
    [Serializable]
    public class MingleMemberSaveResult
    {
        public MingleMemberSaveStatusType SaveStatus { get; set; }
        public string ErrorMessage { get; set; }
    }
}
