﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MingleMigration.ValueObjects
{
    public class MingleList
    {
        public int MemberId { get; set; }
        public int HotListCategory { get; set; }
        public int TargetMemberId { get; set; }
        public string ActionDate { get; set; }
        public string Comment { get; set; }
        public int Vote { get; set; }
        public bool ClickEmailSent { get; set; }
        public bool AlertEmailSent { get; set; }
        public int TeaseId { get; set; }
    }
}
