﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    public class SearchPreferenceValdationResult
    {
        public bool Success { get; set; }
        public List<SearchPreferenceValidationError> Errors { get; set; }

        public SearchPreferenceValdationResult()
        {
            Errors = new List<SearchPreferenceValidationError>();
        }
    }
}
