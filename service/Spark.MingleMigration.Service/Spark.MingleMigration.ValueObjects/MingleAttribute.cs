﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContentVO =  Matchnet.Content.ValueObjects.AttributeMetadata;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public class MingleAttribute
    {
        public int MingleAttributeID { get; set; }
        public int BHAttributeID { get; set; }
        public string AttributeName { get; set; }
        public MingleDataType DataType { get; set; }
        public bool BHHasOptions { get; set; }
        public ContentVO.Attribute BHAttribute { get; set; }
        public string BHAttributeName { get; set; }
    }
}
