﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Content.ValueObjects.ServiceDefinitions;

namespace Spark.MingleMigration.ValueObjects.ServiceDefinitions
{
    public interface IAttributeMapperSA
    {
        AttributeMapper GetMingleAttributeMapper();

        AttributeMappingResult MapAttributes(Dictionary<string, object> attributeData,
                                             Dictionary<string, List<int>> attributeDataMultiValue, int siteID,
                                             IAttributeMetadataSA attributeMetadataService,
                                             AttributeMapper mapper);
    }
}
