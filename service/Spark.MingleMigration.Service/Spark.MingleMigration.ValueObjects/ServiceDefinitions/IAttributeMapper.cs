﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects.ServiceDefinitions
{
    public interface IAttributeMapper
    {
        AttributeMapper GetMingleAttributeMapper();
        SearchPreferenceMapper GetMingleSearchPreferenceMapper();
    }
}
