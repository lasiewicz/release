﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;

namespace Spark.MingleMigration.ValueObjects.ServiceDefinitions
{
    public interface IMingleMember
    {
        MemberRegisterResult Register(string emailAddress, string username, string passwordHash,  string passwordSalt, Brand brand);
        MemberSaveResult SaveMember(string clientHostName, MemberUpdate memberUpdate, int version, Brand brand, bool createMemberRecord);
        MemberRegisterResult SaveLogonCredentials(int memberId, string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand);
        byte[] GetCachedMemberBytes(string clientHostName, Int32 memberID, bool forceLoad,int version);
        byte[] GetCachedMemberBytes(string clientHostName, Matchnet.CacheSynchronization.ValueObjects.CacheReference cacheReference, Int32 memberID, bool forceLoad, int version);
        bool CreateMemberRecord(int memberID, Brand brand);
        MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId);
    }
}
