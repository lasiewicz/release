﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public class MingleSearchPreference
    {
        public string MinglePreferenceName { get; set; }
        public int GroupID { get; set; }
        public bool IsMaskValue { get; set; }
        public string BHPreferenceName { get; set; }
        public string RelatedMingleAttributeName { get; set; }
    }
}
