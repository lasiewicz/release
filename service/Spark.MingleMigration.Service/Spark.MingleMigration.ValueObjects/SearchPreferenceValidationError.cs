﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    public class SearchPreferenceValidationError
    {
        public PreferenceValidationErrorType ErrorType { get; set; }
        public string PreferenceName { get; set; }
        public string ErrorMessage { get; set; }
    }
}
