﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public class MingleAttributeOption
    {
        public int MingleAttributeGroupID { get; set; }
        public int Value { get; set; }
        public int BHValue { get; set; }
        public string DisplayValue { get; set; }
    }
}
