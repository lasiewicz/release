﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "MINGLEMIGRATION_SVC";

        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Spark.MingleMigration.Service";

        public const string ATTRIBUTEMAPPER_SERVICE_MANAGER_NAME = "AttributeMapperSM";
        public const string MINGLEMEMBER_SERVICE_MANAGER_NAME = "MingleMemberSM";
    }
}
