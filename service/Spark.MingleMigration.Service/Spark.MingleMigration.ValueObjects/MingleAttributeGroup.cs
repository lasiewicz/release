﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public class MingleAttributeGroup
    {
        public int MingleAttributeGroupID { get; set; }
        public int MingleAttributeID { get; set; }
        public int GroupID { get; set; }
        public int BHAttributeGroupID { get; set; }
    }
}
