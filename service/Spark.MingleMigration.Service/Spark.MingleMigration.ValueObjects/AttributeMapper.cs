﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public class AttributeMapper : ICacheable
    {
        private Dictionary<int, Dictionary<string, MingleAttribute>> _mingleAttributeByNameByGroupID;
        private Dictionary<string, MingleAttribute> _mingleAttributeByName;
        private Dictionary<int, MingleAttribute> _mingleAttributeByID;
        private Dictionary<int, List<MingleAttributeGroup>> _mingleAttributeGroupsByAttributeID;
        private Dictionary<int, List<MingleAttributeOption>> _mingleOptionsByGroupID;
        private Dictionary<int, List<MingleAttribute>> _mingleAttributesByGroupID;
        private Dictionary<int, MingleAttribute> _mingleAttributesByBHID;
        private Dictionary<string, int> _bhAttributes;

        public static string CACHE_KEY = "AttributeMapper";

        public AttributeMapper()
        {
            _mingleAttributeByName = new Dictionary<string, MingleAttribute>();
            _mingleAttributeByNameByGroupID = new Dictionary<int, Dictionary<string, MingleAttribute>>();
            _mingleAttributeGroupsByAttributeID = new Dictionary<int, List<MingleAttributeGroup>>();
            _mingleOptionsByGroupID = new Dictionary<int, List<MingleAttributeOption>> ();
            _mingleAttributesByGroupID = new Dictionary<int, List<MingleAttribute>>();
            _mingleAttributeByID = new Dictionary<int, MingleAttribute>();
            _mingleAttributesByBHID = new Dictionary<int, MingleAttribute>();
            _bhAttributes = new Dictionary<string, int>();
        }

        public List<MingleAttribute> GetAllMingleAttributes()
        {
            return _mingleAttributeByID.Values.ToList();
        }

        public MingleAttribute GetMingleAttributeByID(int attributeID)
        {
            if (!_mingleAttributeByID.ContainsKey(attributeID)) return null;
            else return _mingleAttributeByID[attributeID];
        }

        public MingleAttribute GetMingleAttributeByBHID(int bhAttributeID)
        {
            if (!_mingleAttributesByBHID.ContainsKey(bhAttributeID)) return null;
            else return _mingleAttributesByBHID[bhAttributeID];
        }

        public MingleAttribute GetMingleAttributeByNameAndGroup(string name, int groupID)
        {
            if (!_mingleAttributeByName.ContainsKey(name.ToLower())) return null;
            if (!_mingleAttributeByNameByGroupID.ContainsKey(groupID)) return null;
            if (!_mingleAttributeByNameByGroupID[groupID].ContainsKey(name.ToLower())) return null;
            else return _mingleAttributeByNameByGroupID[groupID][name.ToLower()];
        }

        public List<MingleAttribute> GetMingleAttributesByGroup(int groupID)
        {
            return _mingleAttributesByGroupID.ContainsKey(groupID) ? _mingleAttributesByGroupID[groupID] : null;
        }

        public List<MingleAttributeGroup> GetAllMingleAttributeGroups()
        {
            return _mingleAttributeGroupsByAttributeID.Values.SelectMany(groupList => groupList).ToList();
        }

        public List<MingleAttributeGroup> GetMingleGroupsForAttribute(string name)
        {
            var attribute = (from a in _mingleAttributeByName where a.Key.ToLower() == name select a.Value).FirstOrDefault();
            if (attribute == null) return null;
            if (!_mingleAttributeGroupsByAttributeID.ContainsKey(attribute.MingleAttributeID)) return null;
            else return _mingleAttributeGroupsByAttributeID[attribute.MingleAttributeID];
        }

        public List<MingleAttributeGroup> GetMingleGroupsForAttribute(int attributeID)
        {
            if (!_mingleAttributeGroupsByAttributeID.ContainsKey(attributeID)) return null;
            else return _mingleAttributeGroupsByAttributeID[attributeID];
        }

        public List<MingleAttributeOption> GetAllMingleOptions()
        {
            return _mingleOptionsByGroupID.Values.SelectMany(optionList => optionList).ToList();
        }

        public List<MingleAttributeOption> GetMingleOptionsForAttributeAndGroup(string attributeName, int groupID)
        {
            var attribute = GetMingleAttributeByNameAndGroup(attributeName, groupID);
            if (attribute == null) return null;
            if(_mingleAttributeGroupsByAttributeID[attribute.MingleAttributeID] == null) return null;

            var attributeGroup = (from g in _mingleAttributeGroupsByAttributeID[attribute.MingleAttributeID] where g.GroupID == groupID select g).FirstOrDefault();
            if (attributeGroup == null) return null;
            if (!_mingleOptionsByGroupID.ContainsKey(attributeGroup.MingleAttributeGroupID)) return null;
            else return _mingleOptionsByGroupID[attributeGroup.MingleAttributeGroupID];
        }

        public void AddMingleAttribute(MingleAttribute attribute, int groupID)
        {

            if (!_mingleAttributeByNameByGroupID.ContainsKey(groupID))
            {
                _mingleAttributeByNameByGroupID.Add(groupID, new Dictionary<string, MingleAttribute>());
            }

            if (!_mingleAttributeByNameByGroupID[groupID].ContainsKey(attribute.AttributeName.ToLower()))
            {
                _mingleAttributeByNameByGroupID[groupID].Add(attribute.AttributeName.ToLower(), attribute);
            }
            if(!_mingleAttributeByName.ContainsKey(attribute.AttributeName.ToLower()))
            {
                _mingleAttributeByName.Add(attribute.AttributeName.ToLower(), attribute);
            }
            if(!_mingleAttributeByID.ContainsKey(attribute.MingleAttributeID))
            {
                _mingleAttributeByID.Add(attribute.MingleAttributeID, attribute);
            }
            if(!_mingleAttributesByBHID.ContainsKey(attribute.BHAttributeID))
            {
                _mingleAttributesByBHID.Add(attribute.BHAttributeID, attribute);
            }
        }

        public void AddMingleAttributeGroup(MingleAttributeGroup attributeGroup)
        {
            if(!_mingleAttributeGroupsByAttributeID.ContainsKey(attributeGroup.MingleAttributeID))
            {
                var groups = new List<MingleAttributeGroup> {attributeGroup};
                _mingleAttributeGroupsByAttributeID.Add(attributeGroup.MingleAttributeID, groups);
            }
            else
            {
                if(!_mingleAttributeGroupsByAttributeID[attributeGroup.MingleAttributeID].Exists(a=>a.GroupID == attributeGroup.GroupID))
                {
                    _mingleAttributeGroupsByAttributeID[attributeGroup.MingleAttributeID].Add(attributeGroup);
                }
            }

            if (!_mingleAttributesByGroupID.ContainsKey(attributeGroup.GroupID))
            {
                _mingleAttributesByGroupID.Add(attributeGroup.GroupID, new List<MingleAttribute>());
            }

            if(!_mingleAttributesByGroupID[attributeGroup.GroupID].Exists(a => a.MingleAttributeID == attributeGroup.MingleAttributeID) &&
                _mingleAttributeByID.ContainsKey(attributeGroup.MingleAttributeID))
            {
                _mingleAttributesByGroupID[attributeGroup.GroupID].Add(_mingleAttributeByID[attributeGroup.MingleAttributeID]);
            }
        }

        public void AddMingleAttributeOption(MingleAttributeOption attributeOption)
        {
            if (!_mingleOptionsByGroupID.ContainsKey(attributeOption.MingleAttributeGroupID))
            {
                var options = new List<MingleAttributeOption> { attributeOption };
                _mingleOptionsByGroupID.Add(attributeOption.MingleAttributeGroupID, options);
            }
            else
            {
                if (!_mingleOptionsByGroupID[attributeOption.MingleAttributeGroupID].Exists(a => a.Value == attributeOption.Value))
                {
                    _mingleOptionsByGroupID[attributeOption.MingleAttributeGroupID].Add(attributeOption);
                }
            }
        }

        #region reverse lookup - BH to Mingle

        public MingleAttributeGroup GetMingleAttributeGroupBhAttribute(int mingleAttributeId, int groupId)
        {
            // find the group
           return _mingleAttributeGroupsByAttributeID[mingleAttributeId].FirstOrDefault(group => group.GroupID == groupId);
        }

        public MingleAttributeOption GetMingleAttributeOptionFromBhAttribute(int mingleAttributeGroupId, int value)
        {
            // now find the option based on group
            return _mingleOptionsByGroupID[mingleAttributeGroupId].FirstOrDefault(option => option.BHValue == value);
        }

        public void AddBhAttribute(string attributeName, int attributeId)
        {
            if (!_bhAttributes.ContainsKey(attributeName))
            {
                _bhAttributes[attributeName] = attributeId;
            }
        }

        public MingleAttribute GetMingleAttributeByBhName(string bhAttributeName)
        {
            var bhAttributeId = GetBhAttributeIdFromName(bhAttributeName.ToLower());
            return bhAttributeId == Constants.NULL_INT ? null : GetMingleAttributeByBHID(bhAttributeId);
        }

        public int GetBhAttributeIdFromName(string name)
        {
            return !_bhAttributes.ContainsKey(name.ToLower()) ? Constants.NULL_INT : _bhAttributes[name.ToLower()];
        }
        #endregion

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return CacheItemPriorityLevel.Normal; }
            set { }
        }

        public int CacheTTLSeconds { get; set; }
        

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }
    }
}
