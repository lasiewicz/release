﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleMigration.ValueObjects
{
    public enum MingleDataType
    {
        None=0,
        Country=1,
        Date=2,
        Datetime=3,
        Enumeration=4,
        Floattype=5,
        Integer=6,
        Ministry=7,
        Set=8,
        Shortstring=9,
        Stringtype=10
    }

    public enum MappingErrorType
    {
        InvalidParameters=0, 
        GeneralError=1, 
        AttributeInvalid=2, 
        AttributeIncorrectDataType=3, 
        AttributeGeneralMappingError=4, 
        AttributeOptionsNotDefined=5,
        AttributeOptionInvalid=6
    }

    public enum PreferenceValidationErrorType
    {
        PreferenceNotMapped=0,
        PreferenceIsSingleValued=1,
        PreferenceIsMultiValued=2,
        PreferenceHasNoMappedOptions=3, 
        PreferenceHasNonexistentRelatedAttribute=4, 
        PreferenceHasInvalidMappedOption=5

    }
}
