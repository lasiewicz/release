﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.MingleMigration.ValueObjects
{
    [Serializable]
    public class SearchPreferenceMapper: ICacheable
    {
        public static string CACHE_KEY = "SearchPreferenceMapper";

        private Dictionary<int, Dictionary<string, MingleSearchPreference>> _mingleSearchPreferenceByNameByGroupID;
        
        public SearchPreferenceMapper()
        {
            _mingleSearchPreferenceByNameByGroupID = new Dictionary<int, Dictionary<string, MingleSearchPreference>>();
        }

        public void AddSearchPreference(MingleSearchPreference searchPreference)
        {
            if (!_mingleSearchPreferenceByNameByGroupID.ContainsKey(searchPreference.GroupID))
            {
                _mingleSearchPreferenceByNameByGroupID.Add(searchPreference.GroupID, new Dictionary<string, MingleSearchPreference>());
            }

            if (!_mingleSearchPreferenceByNameByGroupID[searchPreference.GroupID].ContainsKey(searchPreference.MinglePreferenceName.ToLower()))
            {
                _mingleSearchPreferenceByNameByGroupID[searchPreference.GroupID].Add(searchPreference.MinglePreferenceName.ToLower(), searchPreference);
            }
        }

        public MingleSearchPreference GetSearchPreference(string minglePreferenceName, int groupID)
        {
            if (!_mingleSearchPreferenceByNameByGroupID.ContainsKey(groupID)) return null;
            if (!_mingleSearchPreferenceByNameByGroupID[groupID].ContainsKey(minglePreferenceName.ToLower())) return null;
            else return _mingleSearchPreferenceByNameByGroupID[groupID][minglePreferenceName.ToLower()];
        }

        public List<MingleSearchPreference> GetAllPreferencesForGroup(int groupID)
        {
            if (!_mingleSearchPreferenceByNameByGroupID.ContainsKey(groupID)) return null;
            return _mingleSearchPreferenceByNameByGroupID[groupID].Select(entry => entry.Value).ToList();
        }

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return CacheItemPriorityLevel.Normal; }
            set { }
        }

        public int CacheTTLSeconds { get; set; }


        public string GetCacheKey()
        {
            return CACHE_KEY;
        }
    }
}
