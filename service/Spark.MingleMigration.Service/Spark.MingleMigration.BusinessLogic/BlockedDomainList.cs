﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.MingleMigration.BusinessLogic
{
    public class BlockedDomainList : ArrayList, ICacheable
    {
        public const string CACHE_KEY = "MMBlockedDomainList";

        public BlockedDomainList()
        {
        }


        #region ICacheable Members
        public int CacheTTLSeconds
        {
            get
            {
                return 600;
            }
            set
            {
            }
        }


        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Absolute;
            }
        }


        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.Normal;
            }
            set
            {
            }
        }


        public string GetCacheKey()
        {
            return CACHE_KEY;
        }
        #endregion
    }
}
