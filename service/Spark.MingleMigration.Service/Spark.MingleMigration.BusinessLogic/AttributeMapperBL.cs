﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Matchnet.Data;
using Matchnet.Exceptions;
using Spark.MingleMigration.ValueObjects;

namespace Spark.MingleMigration.BusinessLogic
{
    public class AttributeMapperBL
    {
        public readonly static AttributeMapperBL Instance = new AttributeMapperBL();

        private AttributeMapperBL()
        {

        }

        public AttributeMapper GetMingleAttributeMapper()
        {
            try
            {
                var command = new Command("mnSystem", "up_MingleAttribute_List", 0);
                var ds = Client.Instance.ExecuteDataSet(command);
                var mapper = new AttributeMapper();

                DataTable attributeTable = ds.Tables[0];
                DataTable attributeGroupTable = ds.Tables[1];
                DataTable attributeOptionTable = ds.Tables[2];

                attributeTable.ChildRelations.Add(
                    "fk_attribute_group",
                    new DataColumn[] {attributeTable.Columns["AttributeID"]},
                    new DataColumn[] {attributeGroupTable.Columns["AttributeID"]}
                    );

                attributeGroupTable.ChildRelations.Add(
                    "fk_attributegroup_option",
                    new DataColumn[] {attributeGroupTable.Columns["AttributeGroupID"]},
                    new DataColumn[] {attributeOptionTable.Columns["AttributeGroupID"]}
                    );

                foreach (DataRow attributeRow in attributeTable.Rows)
                {
                    var mingleAttribute = DataRowToMingleAttribute(attributeRow);
                    

                    DataRow[] groupRows = (from dr in attributeRow.GetChildRows("fk_attribute_group")
                                           select dr).ToArray();

                    foreach (DataRow groupRow in groupRows)
                    {
                        var mingleAttributeGroup = DataRowToMingleAttributeGroup(groupRow);
                        mapper.AddMingleAttribute(mingleAttribute, mingleAttributeGroup.GroupID);
                        mapper.AddMingleAttributeGroup(mingleAttributeGroup);

                        // add the extra reverse mapping
                        mapper.AddBhAttribute(mingleAttribute.BHAttributeName.ToLower(), mingleAttribute.BHAttributeID);

                        DataRow[] optionRows = (from dr in groupRow.GetChildRows("fk_attributegroup_option")
                                                select dr).ToArray();

                        foreach (DataRow optionRow in optionRows)
                        {
                            var mingleAttributeOption = DataRowToMingleAttributeOption(optionRow);
                            mapper.AddMingleAttributeOption(mingleAttributeOption);
                        }
                    }

                }

                return mapper;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when loading mingle attribute mapping data.", ex));
            }
        }

        public SearchPreferenceMapper GetMingleSearchPreferenceMapper()
        {
            try
            {
                var command = new Command("mnSystem", "up_MingleSearchPreference_List", 0);
                var ds = Client.Instance.ExecuteDataSet(command);
                var mapper = new SearchPreferenceMapper();

                foreach (DataRow preferenceRow in ds.Tables[0].Rows)
                {
                    var searchPreference = DataRowToMingleSearchPreference(preferenceRow);
                    mapper.AddSearchPreference(searchPreference);
                }

                return mapper;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when loading mingle search preference mapping data.", ex));
            }
        }

        private MingleSearchPreference DataRowToMingleSearchPreference(DataRow dr)
        {
            var minglePreferenceName = dr["MinglePreferenceName"].ToString();
            var groupID = Convert.ToInt32(dr["GroupID"]);
            var isMaskValue = Convert.ToBoolean(dr["IsMaskValue"]);
            var bHPreferenceName = dr["BHPreferenceName"].ToString();
            var relatedMingleAttributeName = dr["RelatedMingleAttributeName"] == DBNull.Value ? string.Empty : dr["RelatedMingleAttributeName"].ToString();
            return new MingleSearchPreference
                       {
                           BHPreferenceName = bHPreferenceName,
                           GroupID = groupID,
                           IsMaskValue = isMaskValue,
                           MinglePreferenceName = minglePreferenceName,
                           RelatedMingleAttributeName = relatedMingleAttributeName
                       };
        }

        private MingleAttribute DataRowToMingleAttribute(DataRow dr)
        {
            var attributeID = Convert.ToInt32(dr["AttributeID"]);
            var attributeName = dr["AttributeName"].ToString();
            var dataType = ParseMingleDataType(dr["DataType"].ToString());
            var bhAttributeID = Convert.ToInt32(dr["BHAttributeID"]);
            var bhHasOptions = dr["BHHasOptions"] == DBNull.Value ? false : Convert.ToBoolean(dr["BHHasOptions"]);
            var bhAttributeName = dr["BHAttributeName"].ToString();
            
            return new MingleAttribute
            {
                MingleAttributeID = attributeID,
                AttributeName = attributeName,
                DataType = dataType,
                BHAttributeID = bhAttributeID,
                BHHasOptions = bhHasOptions,
                BHAttributeName = bhAttributeName
            };
        }

        private MingleAttributeGroup DataRowToMingleAttributeGroup(DataRow dr)
        {
            var attributeGroupID = Convert.ToInt32(dr["AttributeGroupID"]);
            var attributeID = Convert.ToInt32(dr["AttributeID"]);
            var groupID = Convert.ToInt32(dr["GroupID"]);
            var bhAttributeGroupID = dr["BHAttributeGroupID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["BHAttributeGroupID"]);

            return new MingleAttributeGroup
            {
                BHAttributeGroupID = bhAttributeGroupID,
                GroupID = groupID,
                MingleAttributeID = attributeID,
                MingleAttributeGroupID = attributeGroupID
            };
        }

        private MingleAttributeOption DataRowToMingleAttributeOption(DataRow dr)
        {
            var attributeGroupId = Convert.ToInt32(dr["AttributeGroupID"]);
            var optionValue = Convert.ToInt32(dr["OptionValue"]);
            var bhOptionValue = Convert.ToInt32(dr["BHAttributeOptionValue"]);
            var displayValue = dr["DisplayValue"].ToString();

            return new MingleAttributeOption
            {
                BHValue = bhOptionValue,
                DisplayValue = displayValue,
                MingleAttributeGroupID = attributeGroupId,
                Value = optionValue
            };
        }

        private MingleDataType ParseMingleDataType(string dataType)
        {
            switch (dataType)
            {
                case "country":
                    return MingleDataType.Country;
                case "date":
                    return MingleDataType.Date;
                case "datetime":
                    return MingleDataType.Datetime;
                case "enum":
                    return MingleDataType.Enumeration;
                case "float":
                    return MingleDataType.Floattype;
                case "integer":
                    return MingleDataType.Integer;
                case "ministry":
                    return MingleDataType.Ministry;
                case "set":
                    return MingleDataType.Set;
                case "short_st":
                    return MingleDataType.Shortstring;
                case "string":
                    return MingleDataType.Stringtype;
                default:
                    return MingleDataType.None;
            }
        }
    }
}
