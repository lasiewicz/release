﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Security;
using Spark.Logging;
using log4net;
using log4net.Config;
using System.Web.Caching;

namespace Spark.MingleMigration.BusinessLogic
{
    
    public class MingleMemberBL
    {
        private const string MNLOGON_READ_SETTING_CONSTANT = "MNLOGON_READ_FROM_NEW_TABLES";
        private const int MAX_LENGTH_EMAILADDRESS = 255;
        private const Int32 MAX_ATTRIBUTES_SAVE = 20;
        private const Int32 SUB_EXP_DATE_ATTRIBUTE_ID = 501;
        private static ISettingsSA _settingsService = null;
        public Matchnet.ICaching _cache;
        private Int32 _ttl = 1200;
        private CacheItemRemovedCallback _expireMember;
        private bool _logCachingEvents = false;

        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        //public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        //public event SynchronizationEventHandler SynchronizationRequested;

        private static readonly ILog Logger = LogManager.GetLogger(typeof(MingleMemberBL));

        public readonly static MingleMemberBL Instance = new MingleMemberBL();

        private MingleMemberBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;

            _cache = Matchnet.Caching.Cache.Instance;
            _expireMember = new CacheItemRemovedCallback(this.ExpireMemberCallback);
            _ttl = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MEMBERSVC_CACHE_TTL_MEMBER_SVC"));
            _logCachingEvents = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(Matchnet.Configuration.ValueObjects.SettingConstants.LOG_MIGRATION_SERVICE_CACHING_UPDATES));
            XmlConfigurator.Configure();
        }

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private bool SaveLogonInfoFlag
        {
            get
            {
                bool b = false;
                try
                {
                    b = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("LOG_SAVE_LOGON_INFO"));
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberBL", e, null);
                }
                return b;
            }
        }

        private bool ReadFromNewmnLogonTables
        {
            get
            {
                bool b = false;
                try
                {
                    b = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(MNLOGON_READ_SETTING_CONSTANT));
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberBL", e, null);
                }
                return b;
            }
        }

        public bool StopWritingToOldTables
        {
            get
            {
                bool b = false;
                try
                {
                    b = Convert.ToBoolean(SettingsService.GetSettingFromSingleton(ServiceConstants.MNLOGON_STOP_WRITING_TO_OLD_TABLES));
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogError("Matchnet.Member.Service", "MemberBL", e, null);
                }
                return b;
            }
        }

        public void InsertCachedMemberBytes(CachedMemberBytes cachedMemberBytes, bool replicate)
        {
            if (_logCachingEvents) 
            {
                LogMessage("InsertCachedMemberBytes", string.Format("InsertingCache Replicate: {0}", replicate), cachedMemberBytes.MemberID);
            }
                        
            cachedMemberBytes.CacheTTLSeconds = _ttl;
            cachedMemberBytes.CacheMode = Matchnet.CacheItemMode.Sliding;
            _cache.Insert(cachedMemberBytes, _expireMember);
            if (replicate)
            {
                ReplicationRequested(cachedMemberBytes);
            }
        }


        public MemberRegisterResult Register(string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand)
        {
            try
            {
                Logger.Debug(string.Format("Entering Register: {0}-{1}-{2}", emailAddress, username, brand.BrandID));

                if (username == null) { username = string.Empty; }

                if (passwordHash.Length > MAX_LENGTH_EMAILADDRESS)
                {
                    Logger.Debug(string.Format("Email address too long: {0}", emailAddress));
                    return new MemberRegisterResult(RegisterStatusType.PasswordLengthExceeded, Constants.NULL_INT,
                                                    Constants.NULL_STRING);
                }

                if (!isValidEmailAddress(emailAddress))
                {
                    Logger.Debug(string.Format("Email address isn't valid long: {0}", emailAddress));
                    return new MemberRegisterResult(RegisterStatusType.EmailAddressNotAllowed, Constants.NULL_INT,
                                                    Constants.NULL_STRING);
                }

                Int32 memberID = KeySA.Instance.GetKey("MemberID");

                if (emailAddress.ToLower().IndexOf(username.ToLower()) > -1)
                {
                    username = memberID.ToString();
                }

                Logger.Debug(string.Format("Got memberID: {0} ID: {1}", emailAddress, memberID));

                RegisterStatusType registerStatus = saveLogon(memberID, emailAddress, ref username, passwordHash, passwordSalt,
                                                              brand.Site.Community.CommunityID, Constants.NULL_STRING);

                Logger.Debug(string.Format("Save Logon called: {0} Status: {1}", emailAddress, registerStatus));

                if (registerStatus == RegisterStatusType.Success)
                {
                    CreateMemberRecord(memberID, brand);
                    Logger.Debug(string.Format("Created MemberRecord: {0}", emailAddress));
                    return new MemberRegisterResult(registerStatus, memberID, username);
                }
                else
                {
                    return new MemberRegisterResult(registerStatus, 0, Constants.NULL_STRING);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error registering: {0}", emailAddress), ex);
                throw (new BLException("BL error occured when registering the member.", ex));
            }
        }

        public MemberRegisterResult SaveLogonCredentials(int memberId, string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand)
        {
            var currentEmailAddress = GetCurrentEmailAddress(memberId);
            RegisterStatusType registerStatus = saveLogon(memberId, emailAddress, ref username, passwordHash, passwordSalt,
                                                              brand.Site.Community.CommunityID, currentEmailAddress);
            return new MemberRegisterResult(registerStatus, memberId, username);

        }

        public string GetCurrentEmailAddress(int memberId)
        {
            string currentEmailAddress = Constants.NULL_STRING;
            string sp = (ReadFromNewmnLogonTables)
                            ? "up_LogonMemberCommunity_GetCurrentEmail"
                            : "dbo.up_GetCurrentEmail";
            Command command = new Command("mnLogon", sp, 0);
            command.AddParameter("@MemberId", SqlDbType.Int, ParameterDirection.Input, memberId);
            command.AddParameter("@Value", SqlDbType.NVarChar, 255, ParameterDirection.Output);
            SqlParameterCollection results = Client.Instance.ExecuteNonQuery(command);

            if (results["@Value"].Value != DBNull.Value)
            {
                currentEmailAddress = results["@Value"].Value.ToString();
            }
            return currentEmailAddress;
        }

        public MemberSaveResult SaveMember(string clientHostName, MemberUpdate memberUpdate, int version, Brand brand, bool createMemberRecord)
        {
            if (memberUpdate.MemberID < 1)
            {
                throw new ArgumentException("Must be greater than zero (" + memberUpdate.MemberID.ToString() + ").", "memberID");
            }

            Logger.Debug(string.Format("Entering SaveMember for Member {0} Brand {1}", memberUpdate.MemberID, brand.BrandID));
            
            if(createMemberRecord)
            {
                Logger.Debug(string.Format("SaveMember creating member record for {0}", memberUpdate.MemberID));
                CreateMemberRecord(memberUpdate.MemberID, brand);    
            }

            Int32 attributeNum = 1;
            Int32 attributeNumProc = 1;

            Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();
            MemberSaveResult memberSaveResult = null;
            CachedMemberBytes cachedMemberBytes = getCachedMemberBytes(clientHostName,
                    null,
                  memberUpdate.MemberID,
                  false, version);
            CachedMember cachedMember = new CachedMember(cachedMemberBytes.MemberBytes, version);
            //first versioning has no change in member attributes so we do no distinction
            Int32[] unapprovedLanguages = cachedMember.GetUnapprovedAttributeLanguages(attributes);

            if ((memberUpdate.EmailAddress != Constants.NULL_STRING ||
                memberUpdate.Username != Constants.NULL_STRING ||
                memberUpdate.PasswordHash != Constants.NULL_STRING
                // PM-218 If groupID is valid then process as usual. No groupID means it's not updating username, password, or email address.
                ) && brand.Site.Community.CommunityID != Constants.NULL_INT)
            {
                string username = memberUpdate.Username;

                var currentEmailAddress = GetCurrentEmailAddress(cachedMember.MemberID);
                if (username != Constants.NULL_STRING)
                {
                    /*if ((currentEmailAddress != null && currentEmailAddress.ToLower().IndexOf(username.ToLower()) > -1) ||
                        (memberUpdate.EmailAddress != Constants.NULL_STRING && memberUpdate.EmailAddress.ToLower().IndexOf(username.ToLower()) > -1))
                    {
                        username = memberUpdate.MemberID.ToString();
                    }*/

                    if (username.Length > 50)
                    {
                        throw new Exception("Username must not be longer than 50 characters.");
                    }
                }

                RegisterStatusType registerStatus;

                string encryptedPasswordHash = string.Empty;
                string encryptedPasswordSalt = string.Empty;

                string[] encryptedTextAndSalt = Crypto.Instance.SplitEncryptedTextAndSalt(memberUpdate.PasswordHash);
                if (null != encryptedTextAndSalt)
                {
                    encryptedPasswordHash = encryptedTextAndSalt[0];
                    encryptedPasswordSalt = encryptedTextAndSalt[1];
                }

                registerStatus = saveLogon(memberUpdate.MemberID,
                    memberUpdate.EmailAddress,
                    ref username, encryptedPasswordHash, encryptedPasswordSalt,
                    brand.Site.Community.CommunityID, currentEmailAddress);

                Logger.Debug(string.Format("Logon saved RegisterStatus {0} Status: {1}", memberUpdate.MemberID, registerStatus));

                switch (registerStatus)
                {
                    case RegisterStatusType.AlreadyRegistered:
                        memberSaveResult = new MemberSaveResult(MemberSaveStatusType.EmailAddressExists, Constants.NULL_STRING);
                        break;

                    default:
                        memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, username);

                        foreach (Matchnet.Language language in Enum.GetValues(typeof(Matchnet.Language)))
                        {
                            Hashtable textHashtable = memberUpdate.AttributesText[(int)language] as Hashtable;
                            if (textHashtable != null)
                            {
                                int attributeGroupID = Constants.NULL_INT;

                                attributeGroupID = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID,
                                    Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME).ID;

                                TextValue valUsername = textHashtable[attributeGroupID] as TextValue;

                                if (valUsername != null)
                                {
                                    valUsername.Text = username;
                                }
                            }
                        }

                        break;
                }
            }
            else
            {
                memberSaveResult = new MemberSaveResult(MemberSaveStatusType.Success, memberUpdate.Username);
            }
            
            Command commandMember = null;
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = null;

            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success &&
                (memberUpdate.AttributesText.Count > 0 || memberUpdate.AttributesInt.Count > 0 || memberUpdate.AttributesDate.Count > 0))
            {
                IDictionaryEnumerator de = memberUpdate.AttributesText.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 languageID = (Int32)de.Key;
                    Hashtable attributesText = de.Value as Hashtable;

                    Logger.Debug(string.Format("About to save {0} text attributes for {1}", attributesText.Count, memberUpdate.MemberID));
                    IDictionaryEnumerator deA = attributesText.GetEnumerator();
                    while (deA.MoveNext())
                    {
                        Int32 attributeGroupID = (Int32)deA.Key;
                        TextValue textValue = (TextValue)deA.Value;
                        string val = textValue.Text;
                        attributeNumProc = getAttributeNumProc(attributeNum);
                        AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);

                        createCommand(attributeNumProc, ref commandMember, memberUpdate.MemberID);

                        if (attributeGroup.EncryptFlag)
                        {
                            val = Crypto.Encrypt(SettingsService.GetSettingFromSingleton("KEY"), val);
                        }

                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "DataType", SqlDbType.VarChar, ParameterDirection.Input, "TEXT");
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
                        if (val != attributeGroup.DefaultValue)
                        {
                            commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ValueText", SqlDbType.NVarChar, ParameterDirection.Input, val);
                        }
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "StatusMask", SqlDbType.TinyInt, ParameterDirection.Input, textValue.TextStatus);
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ImmutableFlag", SqlDbType.Bit, ParameterDirection.Input, ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) ? 1 : 0);

                        attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                        //Logger.Debug(string.Format("Saved Text AttributeGroupID: {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, val), memberUpdate.MemberID);

                        executeCommand(attributeNumProc, commandMember);
                        attributeNum++;
                    }
                }


                #region //PM-272 7+1(8 total) Attributes - Change scope from global to community. Need to write to both global and community
                // until the scope switch happens, then this can be safely removed.
                // 'Childrencount', 'DesiredMaritalStatus', 'HaveChildren', 'MaritalStatus','RelationshipStatus','Religion','ReligionActivityLevel','BodyType'
                // AttributeGroupIDs 500, 427, 423, 451, 514, 412, 495, 407 - No need to look up every friggin time plus this is temp. Same in dev, prod
                // This block has to be removed after the deployment. Keeping it separated in its own block. Easier to clean later.
                // If any one of the 8 attributes are included then include a write for community level as well.
                if (AttributeMetadataSA.Instance.GetAttributes().GetAttribute("Religion").Scope == ScopeType.Personals)
                {
                    try
                    {
                        List<Int32> globalAttributeGroupIDs = new List<Int32>(new Int32[] { 500, 427, 423, 451, 514, 412, 495, 407 });

                        int brandID = Constants.NULL_INT;
                        cachedMember.GetLastLogonDate(out brandID, AttributeMetadataSA.Instance.GetAttributes(), BrandConfigSA.Instance.GetBrands());

                        if (brandID != Constants.NULL_INT)
                        {
                            int communityID = BrandConfigSA.Instance.GetBrandByID(brandID).Site.Community.CommunityID;
                            Hashtable hashTableAttributeInt = (Hashtable)memberUpdate.AttributesInt.Clone();
                            IDictionaryEnumerator attributesIntEnumerator = hashTableAttributeInt.GetEnumerator();
                            while (attributesIntEnumerator.MoveNext())
                            {
                                Int32 attributeGroupID = (Int32)attributesIntEnumerator.Key;
                                Int32 val = (Int32)attributesIntEnumerator.Value;

                                // Write to both scopes if there's a write for a global value.
                                if (globalAttributeGroupIDs.Contains(attributeGroupID))
                                {
                                    // Get the Attribute
                                    int attributeID = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(attributeGroupID).AttributeID;

                                    // Get the AttributeGroupID for the commnity
                                    int communityAttributeGroupID = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(communityID, attributeID).ID;

                                    // Add it to memberUpdate.AttributesInt to add an extra record in community scope.
                                    memberUpdate.AttributesInt.Add(communityAttributeGroupID, val);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw (new BLException("BL error occured saving community level attributes for PM272.", ex));
                    }
                }
                #endregion


                Logger.Debug(string.Format("About to save {0} Int attributes for {1}", memberUpdate.AttributesInt.Count, memberUpdate.MemberID));
                de = memberUpdate.AttributesInt.GetEnumerator();

                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    Int32 val = (Int32)de.Value;
                    attributeNumProc = getAttributeNumProc(attributeNum);
                    AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);
                    attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                    createCommand(attributeNumProc, ref commandMember, memberUpdate.MemberID);
                    Int32 defaultValue = Constants.NULL_INT;

                    if (attributeGroup.DefaultValue != Constants.NULL_STRING)
                    {
                        defaultValue = Convert.ToInt32(attributeGroup.DefaultValue);
                    }

                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "DataType", SqlDbType.VarChar, ParameterDirection.Input, attribute.DataType.ToString());
                    if (val != defaultValue)
                    {
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ValueInt", SqlDbType.Int, ParameterDirection.Input, val);
                    }
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ImmutableFlag", SqlDbType.Bit, ParameterDirection.Input, ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) ? 1 : 0);

                    //LogMessage("SaveMember", string.Format("Saved Int AttributeGroupID: {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, val), memberUpdate.MemberID);

                    executeCommand(attributeNumProc, commandMember);
                    attributeNum++;
                }

                Logger.Debug(string.Format("About to save {0} Date attributes for {1}", memberUpdate.AttributesDate.Count, memberUpdate.MemberID));
                de = memberUpdate.AttributesDate.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    DateTime val = (DateTime)de.Value;
                    attributeNumProc = getAttributeNumProc(attributeNum);
                    AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);
                    attribute = attributes.GetAttribute(attributeGroup.AttributeID);
                    createCommand(attributeNumProc, ref commandMember, memberUpdate.MemberID);
                    DateTime defaultValue = DateTime.MinValue;

                    if (attributeGroup.DefaultValue != Constants.NULL_STRING)
                    {
                        defaultValue = Convert.ToDateTime(attributeGroup.DefaultValue);
                    }

                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "AttributeGroupID", SqlDbType.Int, ParameterDirection.Input, attributeGroupID);
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "DataType", SqlDbType.VarChar, ParameterDirection.Input, attribute.DataType.ToString());
                    if (val != defaultValue && val != DateTime.MinValue)
                    {
                        commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ValueDate", SqlDbType.DateTime, ParameterDirection.Input, val);
                    }
                    commandMember.AddParameter("@Attribute" + attributeNumProc.ToString() + "ImmutableFlag", SqlDbType.Bit, ParameterDirection.Input, ((attributeGroup.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) == Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) ? 1 : 0);

                    //LogMessage("SaveMember", string.Format("Saving Date AttributeGroupID: {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, val), memberUpdate.MemberID);

                    executeCommand(attributeNumProc, commandMember);
                    attributeNum++;
                }

                if (attributeNumProc != MAX_ATTRIBUTES_SAVE)
                {
                    Client.Instance.ExecuteAsyncWrite(commandMember);
                }

                //update cachedMember
                de = memberUpdate.AttributesText.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 languageID = (Int32)de.Key;
                    Hashtable attributesText = de.Value as Hashtable;

                    IDictionaryEnumerator deA = attributesText.GetEnumerator();

                    //LogMessage("SaveMember", string.Format("About to update cached member with {0} text attributes for language {1}", attributesText.Count, languageID), memberUpdate.MemberID);

                    while (deA.MoveNext())
                    {
                        TextStatusType textStatus;
                        Int32 attributeGroupID = (Int32)deA.Key;
                        AttributeGroup ag = attributes.GetAttributeGroup(attributeGroupID);
                        if ((ag.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) != Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable ||
                            cachedMember.GetAttributeText(attributeGroupID, languageID, null, out textStatus) == null)
                        {
                            TextValue textValue = (TextValue)deA.Value;
                            attribute = attributes.GetAttribute(ag.AttributeID);
                            LogMessage("SaveMember", string.Format("Updating cached member with Text attribute {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, textValue.Text), memberUpdate.MemberID);

                            cachedMember.SetAttributeText(attributeGroupID, languageID, textValue.Text, textValue.TextStatus);

                            /*If a member has accounts on multiple sites within the same community, 
                                username change in one site\language should reflect on all sites\languages within the community.
                             Update cache on all sites within the community.
                            */
                            if (attribute.Name.ToLower() == "username" && textValue.TextStatus == TextStatusType.Human)
                            {
                                //get member's username on other sites/languages within the same community
                                ArrayList attributeGroupIDs =
                                    attributes.GetAttributeGroupIDs(attributes.GetAttribute("BrandInsertDate").ID);
                                Brands brands = BrandConfigSA.Instance.GetBrands();
                                var communityId = attributes.GetAttributeGroup(attributeGroupID).GroupID;

                                for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
                                {
                                    Int32 agID = (Int32)attributeGroupIDs[num];
                                    if (cachedMember.GetAttributeDate(agID, DateTime.MinValue) != DateTime.MinValue)
                                    {
                                        Site site = brands.GetBrand(attributes.GetAttributeGroup(agID).GroupID).Site;
                                        //Update username attribute for all sites in the community 
                                        if (site.Community.CommunityID == communityId && languageID != site.LanguageID)
                                        {
                                            cachedMember.SetAttributeText(attributeGroupID, site.LanguageID,
                                                                          textValue.Text, textValue.TextStatus);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //LogMessage("SaveMember", string.Format("About to update cached member with {0} Int attributes", memberUpdate.AttributesInt.Count), memberUpdate.MemberID);
                de = memberUpdate.AttributesInt.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    AttributeGroup ag = attributes.GetAttributeGroup(attributeGroupID);
                    if ((ag.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) != Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable ||
                        cachedMember.GetAttributeInt(attributeGroupID, Constants.NULL_INT) == Constants.NULL_INT)
                    {
                        attribute = attributes.GetAttribute(ag.AttributeID);
                        LogMessage("SaveMember", string.Format("Updating cached member with Int attribute {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, de.Value), memberUpdate.MemberID);

                        cachedMember.SetAttributeInt(attributeGroupID, (Int32)de.Value);
                    }
                }

                bool isSubExpDateChanged = false;
                int subExpDateSiteId = Constants.NULL_INT;
                DateTime newSubExpDate = DateTime.MinValue;

                //LogMessage("SaveMember", string.Format("About to update cached member with {0} Date attributes", memberUpdate.AttributesDate.Count), memberUpdate.MemberID);
                de = memberUpdate.AttributesDate.GetEnumerator();
                while (de.MoveNext())
                {
                    Int32 attributeGroupID = (Int32)de.Key;
                    AttributeGroup ag = attributes.GetAttributeGroup(attributeGroupID);

                    // we need to know if the SubscriptionExpirationDate attribute has changed
                    if (!isSubExpDateChanged && ag.AttributeID == SUB_EXP_DATE_ATTRIBUTE_ID)
                    {
                        subExpDateSiteId = ag.GroupID;
                        newSubExpDate = (DateTime)de.Value;
                        isSubExpDateChanged = true;
                    }

                    if ((ag.Status & Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable) != Matchnet.Content.ValueObjects.AttributeMetadata.StatusType.Immutable ||
                        cachedMember.GetAttributeDate(attributeGroupID, DateTime.MinValue) == DateTime.MinValue)
                    {
                        attribute = attributes.GetAttribute(ag.AttributeID);
                        LogMessage("SaveMember", string.Format("Updating cached member with Date attribute {0} ({1}) Value: {2}", attributeGroupID, attribute.Name, de.Value), memberUpdate.MemberID);

                        cachedMember.SetAttributeDate((Int32)de.Key, (DateTime)de.Value);
                    }
                }

                if (memberUpdate.EmailAddress != Constants.NULL_STRING)
                {
                    cachedMember.EmailAddress = memberUpdate.EmailAddress;
                }

                if (memberUpdate.Username != Constants.NULL_STRING)
                {
                    cachedMember.Username = memberSaveResult.Username;
                }

                //update notifier
                MingleMemberUpdateNotifier memberUpdateNotifier = new MingleMemberUpdateNotifier(ref cachedMember, memberUpdate, unapprovedLanguages);
                memberUpdateNotifier.SendNotifications();
                Logger.Debug(string.Format("Member Update Notifier call done: {0}", memberUpdate.MemberID));

                cachedMemberBytes.MemberBytes = cachedMember.ToByteArray();

                InsertCachedMemberBytes(cachedMemberBytes, true);
                
                /*bool synchronize = null != clientHostName;
                if (synchronize)
                {
                    SynchronizationRequested(cachedMemberBytes.GetCacheKey(), cachedMemberBytes.ReferenceTracker.Purge(clientHostName));
                }*/
            }

            return memberSaveResult;
        }

        public RegisterStatusType saveLogon(Int32 memberID, string emailAddress, ref string username, string passwordHash, string passwordSalt, Int32 groupID, string currentEmailAddress)
        {
            //this does not run through the DAL b/c it is a sync write the depends on a good retval
            string usernameNew = username;
            string encryptedEmailAddress = Constants.NULL_STRING;
            string encryptedCurrentEmailAddress = Constants.NULL_STRING;
            string key2 = SettingsService.GetSettingFromSingleton("KEY2");

            LogonActionType actionType = LogonActionType.NewUser;
            //Identify action type
            if (!string.IsNullOrEmpty(currentEmailAddress))
            {
                if (!string.IsNullOrEmpty(username))
                {
                    actionType = LogonActionType.UsernameUpdate;
                }
                else if (!string.IsNullOrEmpty(emailAddress))
                {
                    actionType = LogonActionType.EmailAddressUpdate;
                }
                else if (!string.IsNullOrEmpty(passwordHash))
                {
                    actionType = LogonActionType.PasswordUpdate;
                }
            }

            if (!string.IsNullOrEmpty(emailAddress))
            {
                try
                {
                    emailAddress = emailAddress.ToLower().Trim();
                    encryptedEmailAddress = Crypto.Instance.EncryptText(emailAddress, key2);
                }
                catch (Exception ignore)
                {
                }
            }

            if (!string.IsNullOrEmpty(currentEmailAddress))
            {
                try
                {
                    currentEmailAddress = currentEmailAddress.ToLower();
                    encryptedCurrentEmailAddress = Crypto.Instance.EncryptText(currentEmailAddress, key2);
                }
                catch (Exception ignore)
                {
                }
            }

            LogSaveLogonInfo("START", memberID, emailAddress, encryptedEmailAddress, encryptedCurrentEmailAddress, passwordHash, passwordSalt, usernameNew, groupID, false);

            PhysicalDatabases physicalDatabases = ConnectionDispenser.Instance.GetLogicalDatabase("mnLogon").GetPartition(0).PhysicalDatabases;
            RegisterStatusType registerStatus = RegisterStatusType.Success;
            for (Int32 attempts = 0; attempts < 5; attempts++)
            {
                MingleLogonTransaction lt = new MingleLogonTransaction();
                Exception ltEx;
                registerStatus = lt.RegisterWithSalt(memberID,
                                                        emailAddress,
                                                        encryptedEmailAddress,
                                                        usernameNew,
                                                        passwordHash,
                                                        passwordSalt,
                                                        encryptedCurrentEmailAddress,
                                                        groupID,
                                                        actionType,
                                                        ReadFromNewmnLogonTables,
                                                        StopWritingToOldTables,
                                                        out ltEx);
                // why was the following line commented out? it is not in Member Service where it was ported from.
                // tried uncommenting which also works but keeping it as it is.
                //lt.Dispose();

                if (ltEx != null)
                {
                    LogMessage("saveLogon", "Exception: " + ltEx.Message + ltEx.StackTrace, memberID);
                    throw ltEx;
                }

                if (registerStatus == RegisterStatusType.DuplicateUsername)
                {
                    usernameNew = username.Trim() + Guid.NewGuid().ToString().Substring(0, 4).ToUpper();
                }
                else
                {
                    break;
                }
            }

            username = usernameNew;

            LogSaveLogonInfo("END", memberID, emailAddress, encryptedEmailAddress, encryptedCurrentEmailAddress, passwordHash, passwordSalt, usernameNew, groupID, (registerStatus == RegisterStatusType.Success));

            return registerStatus;
        }

        private void SetMemberGroup(Int32 memberID,
            Int32 communityID,
            Int32 siteID,
            Int32 brandID)
        {
            Command command = new Command("mnMember", "dbo.up_MemberGroup_Save", memberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@GroupID1", SqlDbType.Int, ParameterDirection.Input, communityID);

            if (siteID != Constants.NULL_INT)
            {
                command.AddParameter("@GroupID2", SqlDbType.Int, ParameterDirection.Input, siteID);
            }
            if (brandID != Constants.NULL_INT)
            {
                command.AddParameter("@GroupID3", SqlDbType.Int, ParameterDirection.Input, brandID);
            }

            command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private Int32 getAttributeNumProc(Int32 attributeNum)
        {
            Int32 attributeNumProc = (attributeNum % MAX_ATTRIBUTES_SAVE);
            if (attributeNumProc == 0)
            {
                attributeNumProc = MAX_ATTRIBUTES_SAVE;
            }

            return attributeNumProc;
        }

        public byte[] GetCachedMemberBytes(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad, int version)
        {
            return getCachedMemberBytes(clientHostName,
                cacheReference,
                memberID,
                forceLoad, version).MemberBytes;
        }

        private CachedMemberBytes getCachedMemberBytes(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad, int version)
        {
            CachedMemberBytes cachedMemberBytes = null;
            Attributes attributes;

            try
            {
                bool overrideClientVersionRequest = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MEMBER_SERVICE_OVERRIDE_CLIENT_VERSION_REQUEST"));
                bool overrideForceLoad = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MEMBER_SERVICE_OVERRIDE_FORCE_LOAD"));

                bool ignoreCache = overrideForceLoad ? false : forceLoad;

                if (!ignoreCache)
                {
                    cachedMemberBytes = _cache.Get(CachedMemberBytes.GetCacheKey(memberID)) as CachedMemberBytes;
                }

                //based on setting, look for either current version or client requested version
                bool reloadMemberBytes = overrideClientVersionRequest
                                                ? cachedMemberBytes == null || cachedMemberBytes.Version != CachedMemberBytes.CACHED_MEMBERBYTES_VERSION
                                                : cachedMemberBytes == null || cachedMemberBytes.Version != version;


                if (reloadMemberBytes)
                {
                    if(_logCachingEvents)
                    {
                        LogMessage("GetCachedMemberBytes", "Getting Member From DB for " + (string.IsNullOrEmpty(clientHostName) ? string.Empty : clientHostName),
                            memberID);
                    }

                    CachedMember cachedMember = new CachedMember(memberID, version);
                    SqlDataReader dataReader = null;

                    try
                    {
                        try
                        {
                            Command command = new Command("mnMember", "up_Member_List_Service2", memberID);
                            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                            dataReader = Client.Instance.ExecuteReader(command);
                        }
                        catch (Exception ex)
                        {
                            throw (new BLException("BL error occured when retrieving member profile.", ex));
                        }

                        attributes = AttributeMetadataSA.Instance.GetAttributes();

                        getTextAttributes(attributes, cachedMember, dataReader);

                        //int attributes
                        dataReader.NextResult();
                        getIntAttributes(cachedMember, dataReader);

                        //date attributes
                        dataReader.NextResult();
                        getDateAttributes(cachedMember, dataReader);

                        //photos
                        dataReader.NextResult();
                        
                        //photoFiles
                        if (version >= 5)
                        {
                            dataReader.NextResult();
                        }
                    }
                    finally
                    {
                        if (dataReader != null)
                        {
                            dataReader.Close();
                        }
                    }

                    cachedMemberBytes = new CachedMemberBytes(memberID,
                        cachedMember.ToByteArray(), version);

                    InsertCachedMemberBytes(cachedMemberBytes, true);
                    /*bool synchronize = null != clientHostName;
                    if (synchronize)
                    {
                        string cacheKey = cachedMemberBytes.GetCacheKey();
                        SynchronizationRequested(cacheKey, cachedMemberBytes.ReferenceTracker.Purge(clientHostName));
                    }*/
                }
                else
                {
                    if (_logCachingEvents)
                    {
                        LogMessage("GetCachedMemberBytes", "Getting Member From Cache for " + (string.IsNullOrEmpty(clientHostName) ? string.Empty : clientHostName),
                            memberID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("GetCachedMemberBytes() error (memberID: " + memberID.ToString() + ").", ex);
            }

            if (clientHostName != null && cacheReference != null)
            {
                cachedMemberBytes.ReferenceTracker.Add(clientHostName, cacheReference);
            }

            return cachedMemberBytes;
        }

        public bool CreateMemberRecord(int memberID, Brand brand)
        {
            var created = true;

            try
            {
                Command command = new Command("mnMember", "dbo.up_Member_Save", memberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                Client.Instance.ExecuteAsyncWrite(command);

                SetMemberGroup(memberID, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error creating member record. MemberId: {0} BrandId: {1} Exception: {2}", memberID, brand.BrandID, ex);
                created = false;
            }
            
            return created;
        }

        public MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId)
        {
            try
            {
                Command command = new Command("mnLogon", "dbo.up_LogonMemberCommunity_ListbyMemberID", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (!dt.Rows.Count.Equals(1))
                    return null;

                var emailaddress = dt.Rows[0]["EmailAddress"].ToString();
                var username = dt.Rows[0]["UserName"].ToString();

                return new MemberBasicLogonInfo { MemberId = memberId, EmailAddress = emailaddress, UserName = username };
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when getting basic member logon info by member ID.", ex));
            }
        }

        private void createCommand(Int32 attributeNumProc, ref Command commandMember, Int32 memberID)
        {
            if (attributeNumProc == 1)
            {
                commandMember = new Command("mnMember", "dbo.up_MemberAttribute_Save_Multiple", memberID);
                commandMember.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                commandMember.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            }
        }


        private void executeCommand(Int32 attributeNumProc, Command commandMember)
        {
            if (attributeNumProc == MAX_ATTRIBUTES_SAVE)
            {
                Client.Instance.ExecuteAsyncWrite(commandMember);
            }
        }

        private void getTextAttributes(Attributes attributes,
            CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                bool colLookupDone = false;
                Int32 ordinalStatusMask = Constants.NULL_INT;
                Int32 ordinalValue = Constants.NULL_INT;
                Int32 ordinalLanguageID = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalStatusMask = dataReader.GetOrdinal("StatusMask");
                        ordinalValue = dataReader.GetOrdinal("Value");
                        ordinalLanguageID = dataReader.GetOrdinal("LanguageID");
                        colLookupDone = true;
                    }

                    TextStatusType textStatus = TextStatusType.None;
                    if (!dataReader.IsDBNull(ordinalStatusMask))
                    {
                        textStatus = (TextStatusType)Enum.Parse(typeof(TextStatusType), dataReader.GetByte(ordinalStatusMask).ToString());
                    }

                    Int32 attributeGroupID = dataReader.GetInt32(dataReader.GetOrdinal("AttributeGroupID"));
                    AttributeGroup attributeGroup = attributes.GetAttributeGroup(attributeGroupID);

                    if (attributeGroup != null)
                    {
                        string currentValue = dataReader.GetString(ordinalValue);
                        switch (attributeGroup.AttributeID)
                        {
                            case Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_EMAILADDRESS:
                                cachedMember.EmailAddress = currentValue;
                                break;

                            // PM-218 Always get based on attributeGroupID from GetUserName() method. This should
                            // have been the case before this project, but there maybe some unknowns.
                            //							case Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEID_USERNAME:
                            //								cachedMember.Username = currentValue;
                            //								break;
                        }

                        // PM-218 Else case would be members who signed up after this proj is launched.
                        if (attributeGroupID == Matchnet.Member.ValueObjects.ServiceConstants.ATTRIBUTEGROUPID_USERNAME)
                        {
                            cachedMember.Username = currentValue;
                        }

                        cachedMember.SetAttributeText(attributeGroupID,
                            dataReader.GetInt32(ordinalLanguageID),
                            currentValue,
                            textStatus);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured populating text member attributes.", ex));
            }
        }
        
        private void getIntAttributes(CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                bool colLookupDone = false;
                Int32 ordinalAttributeGroupID = Constants.NULL_INT;
                Int32 ordinalValue = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalAttributeGroupID = dataReader.GetOrdinal("AttributeGroupID");
                        ordinalValue = dataReader.GetOrdinal("Value");
                        colLookupDone = true;
                    }

                    cachedMember.SetAttributeInt(dataReader.GetInt32(ordinalAttributeGroupID),
                        dataReader.GetInt32(ordinalValue));
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured populating integer member attributes.", ex));
            }
        }


        private void getDateAttributes(CachedMember cachedMember,
            SqlDataReader dataReader)
        {
            try
            {
                bool colLookupDone = false;
                Int32 ordinalAttributeGroupID = Constants.NULL_INT;
                Int32 ordinalValue = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalAttributeGroupID = dataReader.GetOrdinal("AttributeGroupID");
                        ordinalValue = dataReader.GetOrdinal("Value");
                        colLookupDone = true;
                    }

                    cachedMember.SetAttributeDate(dataReader.GetInt32(ordinalAttributeGroupID),
                        dataReader.GetDateTime(ordinalValue));
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured populating date member attributes.", ex));
            }
        }

        private bool isValidEmailAddress(string emailAddress)
        {
            int positionAtSign;
            string domainName;

            if (emailAddress.Length == 0)
            {
                return false;
            }

            positionAtSign = emailAddress.IndexOf("@");
            if (positionAtSign == -1)
            {
                return false;
            }

            if (emailAddress.IndexOf(".", positionAtSign) == -1)
            {
                return false;
            }

            if (emailAddress.ToLower().IndexOf("postmaster") == 0)
            {
                return false;
            }

            if (emailAddress.Length > positionAtSign)
            {
                string key = "BlockedDomainList";

                BlockedDomainList blockedDomainList = _cache.Get(BlockedDomainList.CACHE_KEY) as BlockedDomainList;

                if (blockedDomainList == null)
                {
                    blockedDomainList = new BlockedDomainList();

                    Command command = new Command("mnLogon", "dbo.up_BlockedDomain_List", 0);
                    DataTable dt = Client.Instance.ExecuteDataTable(command);

                    Int32 rowCount = dt.Rows.Count;
                    for (Int32 rowNum = 0; rowNum < rowCount; rowNum++)
                    {
                        DataRow row = dt.Rows[rowNum];
                        blockedDomainList.Add(row["DomainName"].ToString());
                    }

                    _cache.Add(blockedDomainList);
                }

                if (blockedDomainList.Contains(emailAddress.Substring(positionAtSign + 1)))
                {
                    return false;
                }
            }

            return true;
        }

        private void ExpireMemberCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            CachedMemberBytes cachedMemberBytes = value as CachedMemberBytes;
            //SynchronizationRequested(cachedMemberBytes.GetCacheKey(),cachedMemberBytes.ReferenceTracker.Purge(Constants.NULL_STRING));
        }

        private void LogSaveLogonInfo(string step, int memberID, string emailAddress, string encryptedEmailAddress, string encryptedCachedEmailAddress, string encryptedPasswordHash, string encryptedPasswordSalt, string usernameNew, int groupID, bool savedToDb)
        {
            if (SaveLogonInfoFlag)
            {
                string message = string.Format("{0} saveLogon(): MemberId:{1}, Email: '{2}', EmailHash:'{3}', Username:'{4}', PasswordHash:'{5}', Salt:'{6}', CurrentEmailHash:'{7}', GroupId:{8}, Saved To DB: {9}", step, memberID, emailAddress, encryptedEmailAddress, usernameNew, encryptedPasswordHash, encryptedPasswordSalt, encryptedCachedEmailAddress, groupID, savedToDb);
                LogMessage("saveLogon", message, memberID);
            }
        }

        private void LogMessage(string method, string message, int memberID)
        {
            //RollingFileLogger.Instance.LogInfoMessage(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL." + method,
            //    message, memberID);
            Logger.Info(method + ": " + memberID.ToString() + ": " + message);
        }

        private void LogException(string method, Exception ex, int memberID)
        {
            //RollingFileLogger.Instance.LogException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "MemberBL." + method,
            //    ex, memberID);
            Logger.Error(method + ": " + memberID.ToString(), ex);
        }
    }
}
