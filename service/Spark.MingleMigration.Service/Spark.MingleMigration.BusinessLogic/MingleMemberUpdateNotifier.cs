﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;

namespace Spark.MingleMigration.BusinessLogic
{
    public class MingleMemberUpdateNotifier
    {
        private CachedMember _cachedMember;
        private MemberUpdate _memberUpdate;
        private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes _attributes = null;

        public MingleMemberUpdateNotifier(ref CachedMember cachedMember, MemberUpdate memberUpdate, Int32[] unapprovedLanguages)
        {
            _cachedMember = cachedMember;
            _memberUpdate = memberUpdate;
        }

        private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    _attributes = AttributeMetadataSA.Instance.GetAttributes();
                }

                return _attributes;
            }
        }

        public void SendNotifications()
        {
            //We'll use this setting to determine if we should write to the new BulkMail table or the old MatchMail table
            //this can be removed once we've completely migrated to the new tables. 
            bool UseBulkMailTables = Conversion.CBool(RuntimeSettings.GetSetting("BULKMAIL_USE_NEW_TABLES"));

            #region mnAdmin

            if (_memberUpdate.EmailAddress != Constants.NULL_STRING ||
                _memberUpdate.Username != Constants.NULL_STRING)
            {
                if (_cachedMember.EmailAddress == Constants.NULL_STRING)
                {
                    new ServiceBoundaryException(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME,
                                                 "Unable to update mnAdmin (memberID: " +
                                                 _cachedMember.MemberID.ToString() + ", emailAddress: " +
                                                 _cachedMember.EmailAddress + ", username: " + _cachedMember.Username +
                                                 ")");
                }
                else
                {
                    // PM-218
                    int groupID = Constants.NULL_INT;
                    int attributeGroupID = Constants.NULL_INT;
                    int languageID = Constants.NULL_INT;

                    // PM-218 Remove after deployed.
                    if (AttributeMetadataSA.Instance.GetAttributes().GetAttribute("Username").Scope
                        != ScopeType.Community)
                    {
                        groupID = CachedMember.GROUP_PERSONALS;
                    }
                    else
                    {
                        IDictionaryEnumerator languages = _memberUpdate.AttributesText.GetEnumerator();
                        while (languages.MoveNext())
                        {
                            languageID = (Int32) languages.Key;
                            Hashtable attributesText = languages.Value as Hashtable;
                            IDictionaryEnumerator attributeGroupIDs = attributesText.GetEnumerator();
                            while (attributeGroupIDs.MoveNext())
                            {
                                ArrayList usernameAttributeGroupIDs =
                                    Attributes.GetAttributeGroupIDs(CachedMember.ATTRIBUTEID_USERNAME);

                                if (usernameAttributeGroupIDs.Contains(attributeGroupIDs.Key))
                                {
                                    groupID = Attributes.GetAttributeGroup((int) attributeGroupIDs.Key).GroupID;
                                    attributeGroupID = (int) attributeGroupIDs.Key;
                                    break;
                                }
                            }
                        }
                    }

                    Command commandAdmin = new Command("mnAdmin", "dbo.up_Member_Save", 0);
                    commandAdmin.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input,
                                              _cachedMember.MemberID);
                    commandAdmin.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input,
                                              _cachedMember.EmailAddress);
                    commandAdmin.AddParameter("@GlobalStatusMask", SqlDbType.Int, ParameterDirection.Input,
                                              _cachedMember.GetAttributeInt(CachedMember.AG_GLOBALSTATUSMASK, 0));
                    commandAdmin.AddParameter("@AdminFlag", SqlDbType.Bit, ParameterDirection.Input, 0);
                    commandAdmin.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                    if (groupID != CachedMember.GROUP_PERSONALS)
                    {
                        TextStatusType textStatusType;
                        commandAdmin.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
                        commandAdmin.AddParameter("@Username", SqlDbType.NVarChar, ParameterDirection.Input,
                                                  _cachedMember.GetAttributeText(attributeGroupID, languageID,
                                                                                 _cachedMember.MemberID.ToString(),
                                                                                 out textStatusType));
                    }
                    else
                    {
                        commandAdmin.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input,
                                                  CachedMember.GROUP_PERSONALS);
                        commandAdmin.AddParameter("@Username", SqlDbType.NVarChar, ParameterDirection.Input,
                                                  _cachedMember.Username);
                    }
                    Client.Instance.ExecuteAsyncWrite(commandAdmin);
                }
            }

            #endregion
        }
    }
}
