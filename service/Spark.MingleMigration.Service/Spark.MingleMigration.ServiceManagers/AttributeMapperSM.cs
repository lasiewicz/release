﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Exceptions;
using Spark.MingleMigration.BusinessLogic;
using Spark.MingleMigration.ValueObjects;
using Spark.MingleMigration.ValueObjects.ServiceDefinitions;

namespace Spark.MingleMigration.ServiceManagers
{
    public class AttributeMapperSM : MarshalByRefObject, IAttributeMapper, IServiceManager, IReplicationRecipient
    {
        
        public AttributeMapper GetMingleAttributeMapper()
        {
            return AttributeMapperBL.Instance.GetMingleAttributeMapper();
        }

        public SearchPreferenceMapper GetMingleSearchPreferenceMapper()
        {
            return AttributeMapperBL.Instance.GetMingleSearchPreferenceMapper();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {
            
        }

        public void Dispose()
        {
            
        }

        public void Receive(IReplicable replicableObject)
        {
            
        }
    }
}
