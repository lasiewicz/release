﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Spark.MingleMigration.BusinessLogic;
using Spark.MingleMigration.ValueObjects;
using Spark.MingleMigration.ValueObjects.ServiceDefinitions;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;

namespace Spark.MingleMigration.ServiceManagers
{
    public class MingleMemberSM : MarshalByRefObject, IMingleMember, IServiceManager, IReplicationRecipient, IDisposable
    {
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;
        //private Synchronizer _synchronizer = null;

        public MingleMemberSM()
        {
            MingleMemberBL.Instance.ReplicationRequested += new Spark.MingleMigration.BusinessLogic.MingleMemberBL.ReplicationEventHandler(MingleMemberBL_ReplicationRequested);
            //MingleMemberBL.Instance.SynchronizationRequested += new Spark.MingleMigration.BusinessLogic.MingleMemberBL.SynchronizationEventHandler(MingleMemberBL_SynchronizationRequested);
            
            string machineName = System.Environment.MachineName;

            string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }
            
            _hydraWriter = new HydraWriter(new string[] { "mnMember", "mnLogon", "mnAdmin", "mnFile", "mnAlert", "mnQuestionAnswer" });
            _hydraWriter.Start();

            var replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Spark.MingleMigration.ValueObjects.ServiceConstants.MINGLEMEMBER_SERVICE_MANAGER_NAME, machineName);
			
			if(replicationURI!= null && replicationURI.Length != 0)
			{
                System.Diagnostics.EventLog.WriteEntry("Spark.MingleMember.Service", "Replication URI: " + replicationURI);								  
				_replicator = new Replicator(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_NAME);
				_replicator.SetDestinationUri(replicationURI);
				_replicator.Start();
			}

            //_synchronizer = new Synchronizer(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME);
            //_synchronizer.Start();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {

        }

        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

            /*if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }*/
        }

        private void MingleMemberBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (_replicator != null)
            {
                _replicator.Enqueue(replicableObject);
            }
        }

        /*private void MingleMemberBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            _synchronizer.Enqueue(key, cacheReferences);
        }*/

        public void Receive(IReplicable replicableObject)
        {
            try
            {
                if (replicableObject == null)
                    return;
                switch (replicableObject.GetType().Name)
                {
                    case "CachedMemberBytes":
                        CachedMemberBytes cachedBytes = (CachedMemberBytes)replicableObject;

                        if (cachedBytes != null)
                        {
                            MingleMemberBL.Instance.InsertCachedMemberBytes((CachedMemberBytes)replicableObject, false);
                        }
  
                        break;
                    default:
                        throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
                }
            }
            catch (Exception ex)
            { new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Exception in replication recipient: " + ex.ToString()); }
        }

        public MemberRegisterResult Register(string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand)
        {
            try
            {
                return MingleMemberBL.Instance.Register(emailAddress, username, passwordHash, passwordSalt, brand) ;
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure registering Member.", ex);
            }
        }

        public MemberRegisterResult SaveLogonCredentials(int memberId, string emailAddress, string username, string passwordHash, string passwordSalt, Brand brand)
        {
            try
            {
                return MingleMemberBL.Instance.SaveLogonCredentials(memberId, emailAddress, username, passwordHash, passwordSalt, brand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving logon credentials.", ex);
            }

        }

        public MemberSaveResult SaveMember(string clientHostName, MemberUpdate memberUpdate, int version, Brand brand, bool createMemberRecord)
        {
            try
            {
                return MingleMemberBL.Instance.SaveMember(clientHostName, memberUpdate, version, brand, createMemberRecord);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure saving Member.", ex);
            }
        }

        public bool CreateMemberRecord(int memberID, Brand brand)
        {
            try
            {
                return MingleMemberBL.Instance.CreateMemberRecord(memberID, brand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure creating member record.", ex);
            }
        }

        public MemberBasicLogonInfo GetMemberBasicLogonInfo(int memberId, int communityId)
        {
            try
            {
                return MingleMemberBL.Instance.GetMemberBasicLogonInfo(memberId, communityId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting member basic logon info.", ex);
            }
        }

        public  byte[] GetCachedMemberBytes(string clientHostName,
            Int32 memberID,
            bool forceLoad, int version)
        {
            try
            {
                return MingleMemberBL.Instance.GetCachedMemberBytes(clientHostName, null, memberID, forceLoad, version);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting cached memberbytes.", ex);
            }
        }

        public byte[] GetCachedMemberBytes(string clientHostName,
            CacheReference cacheReference,
            Int32 memberID,
            bool forceLoad, int version)
        {
            try
            {
                return MingleMemberBL.Instance.GetCachedMemberBytes(clientHostName, cacheReference, memberID, forceLoad, version);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Spark.MingleMigration.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting cached memberbytes.", ex);
            }
        }
    }
}
