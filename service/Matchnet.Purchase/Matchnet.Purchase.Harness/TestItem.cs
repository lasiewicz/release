using System;

using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Purchase.Harness
{
	public class TestItem
	{
		private string _name;
		private string _firstName;
		private string _lastName;
		private string _addressLine1;
		private string _addressLine2;
		private string _city;
		private string _state; 
		private int _countryRegionID; 
		private string _postalCode;
		private CreditCardType _creditCardType; 
		private string _creditCardNumber;
		private int _expirationMonth;
		private int _expirationYear;
		private string _cvc;
		private decimal _amount;
		private string _phone;


		public TestItem(string name,
			string firstName,
			string lastName,
			string addressLine1,
			string addressLine2,
			string city,
			string state, 
			int countryRegionID, 
			string postalCode,
			string phone,
			CreditCardType creditCardType, 
			string creditCardNumber,
			int expirationMonth,
			int expirationYear,
			string cvc,
			decimal amount)
		{
			_name = name;
			_firstName = firstName;
			_lastName = lastName;
			_addressLine1 = addressLine1;
			_addressLine2 = addressLine2;
			_city = city;
			_state = state; 
			_countryRegionID = countryRegionID; 
			_postalCode = postalCode;
			_phone = phone;
			_creditCardType = creditCardType; 
			_creditCardNumber = creditCardNumber;
			_expirationMonth = expirationMonth;
			_expirationYear = expirationYear;
			_cvc = cvc;
			_amount = amount;
		}


		public override string ToString()
		{
			return _name;
		}


		public string Name
		{
			get
			{
				return _name;
			}
		}


		public string FirstName
		{
			get
			{
				return _firstName;
			}
		}


		public string LastName
		{
			get
			{
				return _lastName;
			}
		}


		public string AddressLine1
		{
			get
			{
				return _addressLine1;
			}
		}


		public string AddressLine2
		{
			get
			{
				return _addressLine2;
			}
		}


		public string City
		{
			get
			{
				return _city;
			}
		}


		public string State
		{
			get
			{
				return _state;
			}
		}


		public int CountryRegionID
		{
			get
			{
				return _countryRegionID;
			}
		}


		public string PostalCode
		{
			get
			{
				return _postalCode;
			}
		}

		public string Phone
		{
			get
			{
				return _phone;
			}
		}

		public CreditCardType CreditCardType
		{
			get
			{
				return _creditCardType;
			}
		}


		public string CreditCardNumber
		{
			get
			{
				return _creditCardNumber;
			}
		}


		public int ExpirationMonth
		{
			get
			{
				return _expirationMonth;
			}
		}


		public int ExpirationYear
		{
			get
			{
				return _expirationYear;
			}
		}


		public string CVC
		{
			get
			{
				return _cvc;
			}
		}


		public decimal Amount
		{
			get
			{
				return _amount;
			}
		}
	}
}
