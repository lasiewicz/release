using System;

namespace Matchnet.Purchase.Harness
{
	/// <summary>
	/// Summary description for NameValue.
	/// </summary>
	public class NameValue
	{
		private string _name;
		private Int32 _value;

		public NameValue(string Name, int Value)
		{
			_name = Name;
			_value = Value;
		}

		public string Name
		{
			get{ return _name; }
			set{ _name = value; }
		}

		public Int32 Value
		{
			get{ return _value; }
			set{ _value = value; }
		}
	}
}
