using System;

namespace Matchnet.Purchase.Harness
{
	public class BrandItem
	{
		private Int32 _brandID;
		private string _name;
		private string _uri;
		private Int32 _planID;


		public BrandItem(Int32 brandID,
			string name,
			string uri,
			Int32 planID)
		{
			_brandID = brandID;
			_uri = uri;
			_name = name;
			_planID = planID;
		}


		public Int32 BrandID
		{
			get
			{
				return _brandID;
			}
		}


		public override string ToString()
		{
			return _uri;
		}

		public String Name
		{
			get
			{
				return _name;
			}
		}

		public Int32 PlanID
		{
			get
			{
				return _planID;
			}
		}
	}
}
