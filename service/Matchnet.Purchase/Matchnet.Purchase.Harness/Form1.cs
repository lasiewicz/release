using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Net;

using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceManagers;
using Matchnet.Purchase.BusinessLogic;

namespace Matchnet.Purchase.Harness
{
	public class PurchaseHarnessForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.CheckBox cbxAVSOnly;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox TextResult;
		private System.Windows.Forms.ComboBox ComboTest;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox ComboBrand;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtReasonId;
		private System.Windows.Forms.TextBox txtSiteId;
		private System.Windows.Forms.TextBox txtAdminId;
		private System.Windows.Forms.TextBox txtMemberId;
		private System.Windows.Forms.Button endSub;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtDays;
		private System.Windows.Forms.Button btnAdminAdjust;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label lblMemberSubStatus;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtSubEndDate;
		private System.Windows.Forms.TabPage PlansTest;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.ComboBox ddlRenewDurationTypeID;
		private System.Windows.Forms.ComboBox ddlInitialDurationTypeID;
		private System.Windows.Forms.TextBox tbxInitialCost;
		private System.Windows.Forms.TextBox tbxInitialDuration;
		private System.Windows.Forms.TextBox tbxRenewCost;
		private System.Windows.Forms.TextBox tbxRenewDuration;
		private System.Windows.Forms.TextBox tbxRenewAttempts;
		private System.Windows.Forms.ComboBox ddlCurrencyID;
		private System.Windows.Forms.ComboBox ddlGroupID;
		private System.Windows.Forms.Button btnInsertPlan;
		private System.Windows.Forms.ListBox lbPlanTypes;
		private System.Windows.Forms.ListBox lbPremiumTypes;
		private System.Windows.Forms.ListBox lbPaymentTypes;
		private System.Windows.Forms.DateTimePicker dtpStartDate;
		private System.Windows.Forms.Button btnValidatePurchase;
		private System.Windows.Forms.Button btnCreditCardPurchase;
		private System.Windows.Forms.Button btnCheckPurchase;
		private System.Windows.Forms.Button btnCreditTransaction;
		private System.Windows.Forms.Button btnAuthorizePmtProfile;
		private System.Windows.Forms.Button button3;
		private System.ComponentModel.Container components = null;

		public PurchaseHarnessForm()
		{
			InitializeComponent();
			initGui();

			// AVSOnly not currently used.
			cbxAVSOnly.Visible = false;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.btnAuthorizePmtProfile = new System.Windows.Forms.Button();
			this.btnCreditTransaction = new System.Windows.Forms.Button();
			this.btnCheckPurchase = new System.Windows.Forms.Button();
			this.btnCreditCardPurchase = new System.Windows.Forms.Button();
			this.btnValidatePurchase = new System.Windows.Forms.Button();
			this.cbxAVSOnly = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.TextResult = new System.Windows.Forms.TextBox();
			this.ComboTest = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.ComboBrand = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.label8 = new System.Windows.Forms.Label();
			this.txtSubEndDate = new System.Windows.Forms.TextBox();
			this.lblMemberSubStatus = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.txtDays = new System.Windows.Forms.TextBox();
			this.btnAdminAdjust = new System.Windows.Forms.Button();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtReasonId = new System.Windows.Forms.TextBox();
			this.txtSiteId = new System.Windows.Forms.TextBox();
			this.txtAdminId = new System.Windows.Forms.TextBox();
			this.txtMemberId = new System.Windows.Forms.TextBox();
			this.endSub = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.PlansTest = new System.Windows.Forms.TabPage();
			this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
			this.lbPaymentTypes = new System.Windows.Forms.ListBox();
			this.lbPremiumTypes = new System.Windows.Forms.ListBox();
			this.lbPlanTypes = new System.Windows.Forms.ListBox();
			this.btnInsertPlan = new System.Windows.Forms.Button();
			this.ddlGroupID = new System.Windows.Forms.ComboBox();
			this.ddlCurrencyID = new System.Windows.Forms.ComboBox();
			this.tbxRenewAttempts = new System.Windows.Forms.TextBox();
			this.tbxRenewDuration = new System.Windows.Forms.TextBox();
			this.tbxRenewCost = new System.Windows.Forms.TextBox();
			this.tbxInitialDuration = new System.Windows.Forms.TextBox();
			this.tbxInitialCost = new System.Windows.Forms.TextBox();
			this.ddlInitialDurationTypeID = new System.Windows.Forms.ComboBox();
			this.ddlRenewDurationTypeID = new System.Windows.Forms.ComboBox();
			this.label21 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.PlansTest.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.button3);
			this.tabPage1.Controls.Add(this.btnAuthorizePmtProfile);
			this.tabPage1.Controls.Add(this.btnCreditTransaction);
			this.tabPage1.Controls.Add(this.btnCheckPurchase);
			this.tabPage1.Controls.Add(this.btnCreditCardPurchase);
			this.tabPage1.Controls.Add(this.btnValidatePurchase);
			this.tabPage1.Controls.Add(this.cbxAVSOnly);
			this.tabPage1.Controls.Add(this.button1);
			this.tabPage1.Controls.Add(this.TextResult);
			this.tabPage1.Controls.Add(this.ComboTest);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.ComboBrand);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(752, 542);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Subs";
			// 
			// btnAuthorizePmtProfile
			// 
			this.btnAuthorizePmtProfile.Location = new System.Drawing.Point(184, 504);
			this.btnAuthorizePmtProfile.Name = "btnAuthorizePmtProfile";
			this.btnAuthorizePmtProfile.Size = new System.Drawing.Size(144, 24);
			this.btnAuthorizePmtProfile.TabIndex = 18;
			this.btnAuthorizePmtProfile.Text = "Authorize Payment Profile";
			this.btnAuthorizePmtProfile.Click += new System.EventHandler(this.btnAuthorizePmtProfile_Click);
			// 
			// btnCreditTransaction
			// 
			this.btnCreditTransaction.Location = new System.Drawing.Point(184, 464);
			this.btnCreditTransaction.Name = "btnCreditTransaction";
			this.btnCreditTransaction.Size = new System.Drawing.Size(112, 24);
			this.btnCreditTransaction.TabIndex = 17;
			this.btnCreditTransaction.Text = "Credit Transaction";
			this.btnCreditTransaction.Click += new System.EventHandler(this.btnCreditTransaction_Click);
			// 
			// btnCheckPurchase
			// 
			this.btnCheckPurchase.Location = new System.Drawing.Point(32, 504);
			this.btnCheckPurchase.Name = "btnCheckPurchase";
			this.btnCheckPurchase.Size = new System.Drawing.Size(128, 24);
			this.btnCheckPurchase.TabIndex = 16;
			this.btnCheckPurchase.Text = "Check Purchase";
			this.btnCheckPurchase.Click += new System.EventHandler(this.btnCheckPurchase_Click);
			// 
			// btnCreditCardPurchase
			// 
			this.btnCreditCardPurchase.Location = new System.Drawing.Point(32, 464);
			this.btnCreditCardPurchase.Name = "btnCreditCardPurchase";
			this.btnCreditCardPurchase.Size = new System.Drawing.Size(128, 24);
			this.btnCreditCardPurchase.TabIndex = 15;
			this.btnCreditCardPurchase.Text = "Credit Card Purchase";
			this.btnCreditCardPurchase.Click += new System.EventHandler(this.btnCreditCardPurchase_Click);
			// 
			// btnValidatePurchase
			// 
			this.btnValidatePurchase.Location = new System.Drawing.Point(32, 424);
			this.btnValidatePurchase.Name = "btnValidatePurchase";
			this.btnValidatePurchase.Size = new System.Drawing.Size(104, 23);
			this.btnValidatePurchase.TabIndex = 14;
			this.btnValidatePurchase.Text = "Validate Purchase";
			this.btnValidatePurchase.Click += new System.EventHandler(this.btnValidatePurchase_Click);
			// 
			// cbxAVSOnly
			// 
			this.cbxAVSOnly.Location = new System.Drawing.Point(584, 8);
			this.cbxAVSOnly.Name = "cbxAVSOnly";
			this.cbxAVSOnly.Size = new System.Drawing.Size(88, 24);
			this.cbxAVSOnly.TabIndex = 13;
			this.cbxAVSOnly.Text = "AVS Only";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(672, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 23);
			this.button1.TabIndex = 12;
			this.button1.Text = "hit it!";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// TextResult
			// 
			this.TextResult.Location = new System.Drawing.Point(8, 40);
			this.TextResult.Multiline = true;
			this.TextResult.Name = "TextResult";
			this.TextResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.TextResult.Size = new System.Drawing.Size(720, 368);
			this.TextResult.TabIndex = 11;
			this.TextResult.Text = "";
			// 
			// ComboTest
			// 
			this.ComboTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ComboTest.ItemHeight = 13;
			this.ComboTest.Location = new System.Drawing.Point(232, 8);
			this.ComboTest.Name = "ComboTest";
			this.ComboTest.Size = new System.Drawing.Size(344, 21);
			this.ComboTest.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(200, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(26, 16);
			this.label2.TabIndex = 9;
			this.label2.Text = "test:";
			// 
			// ComboBrand
			// 
			this.ComboBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ComboBrand.ItemHeight = 13;
			this.ComboBrand.Location = new System.Drawing.Point(48, 8);
			this.ComboBrand.Name = "ComboBrand";
			this.ComboBrand.Size = new System.Drawing.Size(144, 21);
			this.ComboBrand.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(36, 16);
			this.label1.TabIndex = 8;
			this.label1.Text = "brand:";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.label8);
			this.tabPage2.Controls.Add(this.txtSubEndDate);
			this.tabPage2.Controls.Add(this.lblMemberSubStatus);
			this.tabPage2.Controls.Add(this.button2);
			this.tabPage2.Controls.Add(this.label7);
			this.tabPage2.Controls.Add(this.txtDays);
			this.tabPage2.Controls.Add(this.btnAdminAdjust);
			this.tabPage2.Controls.Add(this.txtResult);
			this.tabPage2.Controls.Add(this.label6);
			this.tabPage2.Controls.Add(this.label5);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Controls.Add(this.label3);
			this.tabPage2.Controls.Add(this.txtReasonId);
			this.tabPage2.Controls.Add(this.txtSiteId);
			this.tabPage2.Controls.Add(this.txtAdminId);
			this.tabPage2.Controls.Add(this.txtMemberId);
			this.tabPage2.Controls.Add(this.endSub);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(752, 542);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Sub Tools";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(352, 168);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(120, 16);
			this.label8.TabIndex = 16;
			this.label8.Text = "Subscription End Date";
			// 
			// txtSubEndDate
			// 
			this.txtSubEndDate.AccessibleName = "";
			this.txtSubEndDate.Location = new System.Drawing.Point(360, 192);
			this.txtSubEndDate.Name = "txtSubEndDate";
			this.txtSubEndDate.TabIndex = 15;
			this.txtSubEndDate.Text = "";
			// 
			// lblMemberSubStatus
			// 
			this.lblMemberSubStatus.Location = new System.Drawing.Point(496, 216);
			this.lblMemberSubStatus.Name = "lblMemberSubStatus";
			this.lblMemberSubStatus.Size = new System.Drawing.Size(152, 23);
			this.lblMemberSubStatus.TabIndex = 14;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(480, 184);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(184, 23);
			this.button2.TabIndex = 13;
			this.button2.Text = "Get Member Subscription Status";
			this.button2.Click += new System.EventHandler(this.button2_Click_1);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(368, 96);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(96, 16);
			this.label7.TabIndex = 12;
			this.label7.Text = "Number of days";
			// 
			// txtDays
			// 
			this.txtDays.AccessibleName = "";
			this.txtDays.Location = new System.Drawing.Point(360, 120);
			this.txtDays.Name = "txtDays";
			this.txtDays.TabIndex = 11;
			this.txtDays.Text = "";
			// 
			// btnAdminAdjust
			// 
			this.btnAdminAdjust.Location = new System.Drawing.Point(480, 120);
			this.btnAdminAdjust.Name = "btnAdminAdjust";
			this.btnAdminAdjust.Size = new System.Drawing.Size(184, 23);
			this.btnAdminAdjust.TabIndex = 10;
			this.btnAdminAdjust.Text = "TrialPay-Adjust subscription";
			this.btnAdminAdjust.Click += new System.EventHandler(this.button2_Click);
			// 
			// txtResult
			// 
			this.txtResult.Location = new System.Drawing.Point(32, 80);
			this.txtResult.Multiline = true;
			this.txtResult.Name = "txtResult";
			this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtResult.Size = new System.Drawing.Size(264, 56);
			this.txtResult.TabIndex = 9;
			this.txtResult.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(384, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 23);
			this.label6.TabIndex = 8;
			this.label6.Text = "ReasonId";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(288, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 16);
			this.label5.TabIndex = 7;
			this.label5.Text = "SiteId";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(152, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 16);
			this.label4.TabIndex = 6;
			this.label4.Text = "Admin MemberId";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(56, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 16);
			this.label3.TabIndex = 5;
			this.label3.Text = "MemberId";
			// 
			// txtReasonId
			// 
			this.txtReasonId.Location = new System.Drawing.Point(368, 40);
			this.txtReasonId.Name = "txtReasonId";
			this.txtReasonId.TabIndex = 4;
			this.txtReasonId.Text = "";
			// 
			// txtSiteId
			// 
			this.txtSiteId.Location = new System.Drawing.Point(256, 40);
			this.txtSiteId.Name = "txtSiteId";
			this.txtSiteId.TabIndex = 3;
			this.txtSiteId.Text = "";
			// 
			// txtAdminId
			// 
			this.txtAdminId.AccessibleName = "adminId";
			this.txtAdminId.Location = new System.Drawing.Point(144, 40);
			this.txtAdminId.Name = "txtAdminId";
			this.txtAdminId.TabIndex = 2;
			this.txtAdminId.Text = "";
			// 
			// txtMemberId
			// 
			this.txtMemberId.AccessibleDescription = "";
			this.txtMemberId.AccessibleName = "";
			this.txtMemberId.Location = new System.Drawing.Point(32, 40);
			this.txtMemberId.Name = "txtMemberId";
			this.txtMemberId.TabIndex = 1;
			this.txtMemberId.Text = "";
			// 
			// endSub
			// 
			this.endSub.Location = new System.Drawing.Point(488, 40);
			this.endSub.Name = "endSub";
			this.endSub.Size = new System.Drawing.Size(120, 23);
			this.endSub.TabIndex = 0;
			this.endSub.Text = "End Subscription";
			this.endSub.Click += new System.EventHandler(this.endSub_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.PlansTest);
			this.tabControl1.ItemSize = new System.Drawing.Size(42, 18);
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(760, 568);
			this.tabControl1.TabIndex = 0;
			// 
			// PlansTest
			// 
			this.PlansTest.Controls.Add(this.dtpStartDate);
			this.PlansTest.Controls.Add(this.lbPaymentTypes);
			this.PlansTest.Controls.Add(this.lbPremiumTypes);
			this.PlansTest.Controls.Add(this.lbPlanTypes);
			this.PlansTest.Controls.Add(this.btnInsertPlan);
			this.PlansTest.Controls.Add(this.ddlGroupID);
			this.PlansTest.Controls.Add(this.ddlCurrencyID);
			this.PlansTest.Controls.Add(this.tbxRenewAttempts);
			this.PlansTest.Controls.Add(this.tbxRenewDuration);
			this.PlansTest.Controls.Add(this.tbxRenewCost);
			this.PlansTest.Controls.Add(this.tbxInitialDuration);
			this.PlansTest.Controls.Add(this.tbxInitialCost);
			this.PlansTest.Controls.Add(this.ddlInitialDurationTypeID);
			this.PlansTest.Controls.Add(this.ddlRenewDurationTypeID);
			this.PlansTest.Controls.Add(this.label21);
			this.PlansTest.Controls.Add(this.label20);
			this.PlansTest.Controls.Add(this.label19);
			this.PlansTest.Controls.Add(this.label18);
			this.PlansTest.Controls.Add(this.label17);
			this.PlansTest.Controls.Add(this.label16);
			this.PlansTest.Controls.Add(this.label15);
			this.PlansTest.Controls.Add(this.label14);
			this.PlansTest.Controls.Add(this.label13);
			this.PlansTest.Controls.Add(this.label12);
			this.PlansTest.Controls.Add(this.label11);
			this.PlansTest.Controls.Add(this.label10);
			this.PlansTest.Controls.Add(this.label9);
			this.PlansTest.Location = new System.Drawing.Point(4, 22);
			this.PlansTest.Name = "PlansTest";
			this.PlansTest.Size = new System.Drawing.Size(752, 542);
			this.PlansTest.TabIndex = 2;
			this.PlansTest.Text = "Plans";
			// 
			// dtpStartDate
			// 
			this.dtpStartDate.Location = new System.Drawing.Point(152, 112);
			this.dtpStartDate.Name = "dtpStartDate";
			this.dtpStartDate.TabIndex = 32;
			// 
			// lbPaymentTypes
			// 
			this.lbPaymentTypes.Location = new System.Drawing.Point(432, 336);
			this.lbPaymentTypes.Name = "lbPaymentTypes";
			this.lbPaymentTypes.Size = new System.Drawing.Size(288, 134);
			this.lbPaymentTypes.TabIndex = 31;
			// 
			// lbPremiumTypes
			// 
			this.lbPremiumTypes.Location = new System.Drawing.Point(432, 168);
			this.lbPremiumTypes.Name = "lbPremiumTypes";
			this.lbPremiumTypes.Size = new System.Drawing.Size(288, 134);
			this.lbPremiumTypes.TabIndex = 30;
			// 
			// lbPlanTypes
			// 
			this.lbPlanTypes.Location = new System.Drawing.Point(432, 16);
			this.lbPlanTypes.Name = "lbPlanTypes";
			this.lbPlanTypes.Size = new System.Drawing.Size(288, 134);
			this.lbPlanTypes.TabIndex = 29;
			// 
			// btnInsertPlan
			// 
			this.btnInsertPlan.Location = new System.Drawing.Point(328, 504);
			this.btnInsertPlan.Name = "btnInsertPlan";
			this.btnInsertPlan.TabIndex = 28;
			this.btnInsertPlan.Text = "Insert";
			this.btnInsertPlan.Click += new System.EventHandler(this.btnInsertPlan_Click);
			// 
			// ddlGroupID
			// 
			this.ddlGroupID.Location = new System.Drawing.Point(176, 16);
			this.ddlGroupID.Name = "ddlGroupID";
			this.ddlGroupID.Size = new System.Drawing.Size(121, 21);
			this.ddlGroupID.TabIndex = 27;
			this.ddlGroupID.Text = "comboBox1";
			// 
			// ddlCurrencyID
			// 
			this.ddlCurrencyID.Location = new System.Drawing.Point(176, 64);
			this.ddlCurrencyID.Name = "ddlCurrencyID";
			this.ddlCurrencyID.Size = new System.Drawing.Size(121, 21);
			this.ddlCurrencyID.TabIndex = 26;
			this.ddlCurrencyID.Text = "comboBox1";
			// 
			// tbxRenewAttempts
			// 
			this.tbxRenewAttempts.Location = new System.Drawing.Point(176, 443);
			this.tbxRenewAttempts.Name = "tbxRenewAttempts";
			this.tbxRenewAttempts.TabIndex = 21;
			this.tbxRenewAttempts.Text = "3";
			// 
			// tbxRenewDuration
			// 
			this.tbxRenewDuration.Location = new System.Drawing.Point(176, 396);
			this.tbxRenewDuration.Name = "tbxRenewDuration";
			this.tbxRenewDuration.TabIndex = 20;
			this.tbxRenewDuration.Text = "1";
			// 
			// tbxRenewCost
			// 
			this.tbxRenewCost.Location = new System.Drawing.Point(176, 301);
			this.tbxRenewCost.Name = "tbxRenewCost";
			this.tbxRenewCost.TabIndex = 19;
			this.tbxRenewCost.Text = "29.99";
			// 
			// tbxInitialDuration
			// 
			this.tbxInitialDuration.Location = new System.Drawing.Point(176, 206);
			this.tbxInitialDuration.Name = "tbxInitialDuration";
			this.tbxInitialDuration.TabIndex = 18;
			this.tbxInitialDuration.Text = "6";
			// 
			// tbxInitialCost
			// 
			this.tbxInitialCost.Location = new System.Drawing.Point(176, 159);
			this.tbxInitialCost.Name = "tbxInitialCost";
			this.tbxInitialCost.TabIndex = 17;
			this.tbxInitialCost.Text = "149.99";
			// 
			// ddlInitialDurationTypeID
			// 
			this.ddlInitialDurationTypeID.Location = new System.Drawing.Point(176, 253);
			this.ddlInitialDurationTypeID.Name = "ddlInitialDurationTypeID";
			this.ddlInitialDurationTypeID.Size = new System.Drawing.Size(121, 21);
			this.ddlInitialDurationTypeID.TabIndex = 15;
			this.ddlInitialDurationTypeID.Text = "comboBox2";
			// 
			// ddlRenewDurationTypeID
			// 
			this.ddlRenewDurationTypeID.Location = new System.Drawing.Point(176, 348);
			this.ddlRenewDurationTypeID.Name = "ddlRenewDurationTypeID";
			this.ddlRenewDurationTypeID.Size = new System.Drawing.Size(121, 21);
			this.ddlRenewDurationTypeID.TabIndex = 14;
			this.ddlRenewDurationTypeID.Text = "comboBox1";
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(320, 168);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(112, 23);
			this.label21.TabIndex = 12;
			this.label21.Text = "PremiumTypeMask";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(320, 344);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(120, 23);
			this.label20.TabIndex = 11;
			this.label20.Text = "PaymentTypeMask";
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(16, 112);
			this.label19.Name = "label19";
			this.label19.TabIndex = 10;
			this.label19.Text = "StartDate";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(16, 16);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(56, 23);
			this.label18.TabIndex = 9;
			this.label18.Text = "GroupID";
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(16, 448);
			this.label17.Name = "label17";
			this.label17.TabIndex = 8;
			this.label17.Text = "RenewAttempts";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(336, 16);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(80, 23);
			this.label16.TabIndex = 7;
			this.label16.Text = "PlanTypeMask";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(16, 400);
			this.label15.Name = "label15";
			this.label15.TabIndex = 6;
			this.label15.Text = "RenewDuration";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(16, 352);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(120, 23);
			this.label14.TabIndex = 5;
			this.label14.Text = "RenewDurationTypeID";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(16, 304);
			this.label13.Name = "label13";
			this.label13.TabIndex = 4;
			this.label13.Text = "RenewCost";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(16, 208);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(120, 23);
			this.label12.TabIndex = 3;
			this.label12.Text = "InitialDuration";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(16, 256);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(120, 23);
			this.label11.TabIndex = 2;
			this.label11.Text = "InitialDurationTypeID";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(16, 160);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(120, 23);
			this.label10.TabIndex = 1;
			this.label10.Text = "InitialCost";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(16, 64);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(64, 23);
			this.label9.TabIndex = 0;
			this.label9.Text = "CurrencyID";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(640, 504);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(96, 23);
			this.button3.TabIndex = 19;
			this.button3.Text = "Initiate Encore";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// PurchaseHarnessForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(770, 576);
			this.Controls.Add(this.tabControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "PurchaseHarnessForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "PurchaseHarnessForm";
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.PlansTest.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new PurchaseHarnessForm());
		}


		private void initGui()
		{
			#region ComboBrand Setup
			ComboBrand.Items.Add(new BrandItem(1001, "americansingles.com", "americansingles.com", 360));
			ComboBrand.Items.Add(new BrandItem(1003, "jdate.com", "jdate.com", 202));
			ComboBrand.Items.Add(new BrandItem(1004, "jdate.co.il", "jdate.co.il", 350));
			// 322, 324, 326, 328 are all acceptable AS 5DFT PlanIDs.
			ComboBrand.Items.Add(new BrandItem(1001, "americansingles.com - 5DFT" ,"americansingles.com", 322));
			ComboBrand.Items.Add(new BrandItem(1105, "jdate.fr" ,"jdate.fr", 733));
			ComboBrand.DisplayMember = "Name";
			ComboBrand.SelectedIndex = 0;
			#endregion

			#region ComboTest Setup
			ComboTest.Items.Add(new TestItem("test01",
				"John",
				"Smith",
				"1 Main St.",
				"",
				"Burlington",
				"MA",
				223,
				"01803-3747",
				"323-836-3000",
				CreditCardType.Visa,
				"4457010000000009",
				1,
				2009,
				"111",
				100.10M));

			ComboTest.Items.Add(new TestItem("test02",
				"Mike",
				"Hammer",
				"2 Main St. Apt. 222",
				"Apt. 222",
				"Riverside",
				"RI",
				223,
				"02915",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112010000000003",
				2,
				2009,
				"111",
				200.20M));

			ComboTest.Items.Add(new TestItem("test03",
				"Eileen",
				"Jones",
				"3 Main St.",
				"",
				"Bloomfield",
				"CT",
				223,
				"06002",
				"323-836-3000",
				CreditCardType.Discover,
				"6011010000000003",
				3,
				2009,
				"111",
				300.30M));

			ComboTest.Items.Add(new TestItem("test04",
				"Bob",
				"Black",
				"4 Main St.",
				"",
				"Laurel",
				"MD",
				223,
				"20708",
				"323-836-3000",
				CreditCardType.AmericanExpress,
				"375001000000005",
				4,
				2009,
				"111",
				400.40M));

			ComboTest.Items.Add(new TestItem("test05",
				"Bob",
				"Black",
				"4 Main St.",
				"",
				"Laurel",
				"MD",
				223,
				"20708",
				"323-836-3000",
				CreditCardType.Visa,
				"4457010200000007",
				5,
				2009,
				"111",
				500.50M));

			ComboTest.Items.Add(new TestItem("test06",
				"Joe",
				"Green",
				"6 Main St.",
				"",
				"Derry",
				"NH",
				223,
				"03038",
				"323-836-3000",
				CreditCardType.Visa,
				"4457010100000008",
				6,
				2009,
				"111",
				600.60M));

			ComboTest.Items.Add(new TestItem("test07",
				"Jane",
				"Murray",
				"7 Main St.",
				"",
				"Amesbury",
				"MA",
				223,
				"01913",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112010100000002",
				7,
				2009,
				"111",
				700.70M));

			ComboTest.Items.Add(new TestItem("test08",
				"Mark",
				"Johnson",
				"8 Main St.",
				"",
				"Manchester",
				"NH",
				223,
				"03101",
				"323-836-3000",
				CreditCardType.Discover,
				"6011010100000002",
				8,
				2009,
				"111",
				800.80M));

			ComboTest.Items.Add(new TestItem("test09",
				"James",
				"Miller",
				"9 Main St.",
				"",
				"Boston",
				"MA",
				223,
				"02134",
				"323-836-3000",
				CreditCardType.AmericanExpress,
				"375001010000003",
				9,
				2009,
				"111",
				900.90M));

			ComboTest.Items.Add(new TestItem("CVV test01",
				"John",
				"Smith",
				"1 Main St.",
				"",
				"Burlington",
				"MA",
				223,
				"01803-3747",
				"323-836-3000",
				CreditCardType.Visa,
				"4356102000000956",
				1,
				2009,
				"123",
				100.10M));

			ComboTest.Items.Add(new TestItem("CVV test02",
				"John",
				"Smith",
				"1 Main St.",
				"",
				"Burlington",
				"MA",
				223,
				"01803-3747",
				"323-836-3000",
				CreditCardType.Visa,
				"4356102000000956",
				1,
				2009,
				"456",
				100.10M));

			ComboTest.Items.Add(new TestItem("CVV test03",
				"John",
				"Smith",
				"1 Main St.",
				"",
				"Burlington",
				"MA",
				223,
				"01803-3747",
				"323-836-3000",
				CreditCardType.Visa,
				"4356102000000956",
				1,
				2009,
				"789",
				100.10M));

			ComboTest.Items.Add(new TestItem("CVV test04",
				"John",
				"Smith",
				"1 Main St.",
				"",
				"Burlington",
				"MA",
				223,
				"01803-3747",
				"323-836-3000",
				CreditCardType.Visa,
				"4356102000000956",
				1,
				2009,
				"289",
				100.10M));

			ComboTest.Items.Add(new TestItem("CVV test05",
				"John",
				"Smith",
				"1 Main St.",
				"",
				"Burlington",
				"MA",
				223,
				"01803-3747",
				"323-836-3000",
				CreditCardType.Visa,
				"4841020582904077",
				1,
				2009,
				"567",
				100.10M));

			ComboTest.Items.Add(new TestItem("AVS test01",
				"Avs",
				"test1",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.Visa,
				"4457000300000007",
				1,
				2008,
				"123",
				11.00M));

			ComboTest.Items.Add(new TestItem("AVS test02",
				"Avs",
				"test2",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.Visa,
				"4457000100000009",
				1,
				2008,
				"123",
				22.01M));

			ComboTest.Items.Add(new TestItem("AVS test03",
				"Avs",
				"test3",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.Visa,
				"4457003100000003",
				1,
				2008,
				"123",
				33.02M));

			ComboTest.Items.Add(new TestItem("AVS test04",
				"Avs",
				"test4",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.Visa,
				"4457000400000006",
				1,
				2008,
				"123",
				44.10M));

			ComboTest.Items.Add(new TestItem("AVS test05",
				"Avs",
				"test5",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.Visa,
				"4457000200000008",
				1,
				2008,
				"123",
				55.11M));

			ComboTest.Items.Add(new TestItem("AVS test06",
				"Avs",
				"test6",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112000100000003",
				1,
				2008,
				"123",
				66.12M));

			ComboTest.Items.Add(new TestItem("AVS test07",
				"Avs",
				"test7",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112002100000009",
				1,
				2008,
				"123",
				77.13M));

			ComboTest.Items.Add(new TestItem("AVS test08",
				"Avs",
				"test8",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112002200000008",
				1,
				2008,
				"123",
				88.14M));

			ComboTest.Items.Add(new TestItem("AVS test09",
				"Avs",
				"test9",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112000200000002",
				1,
				2008,
				"123",
				99.20M));

			ComboTest.Items.Add(new TestItem("AVS test10",
				"Avs",
				"test10",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112000300000001",
				1,
				2008,
				"123",
				110.30M));

			ComboTest.Items.Add(new TestItem("AVS test11",
				"Avs",
				"test11",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112000400000000",
				1,
				2008,
				"123",
				111.31M));

			ComboTest.Items.Add(new TestItem("AVS test12",
				"Avs",
				"test12",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.Visa,
				"6011000100000003",
				1,
				2008,
				"123",
				112.32M));

			ComboTest.Items.Add(new TestItem("AVS test13",
				"Avs",
				"test13",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112000500000009",
				1,
				2008,
				"123",
				113.33M));

			ComboTest.Items.Add(new TestItem("AVS test14",
				"Avs",
				"test14",
				"8383 Wilshire Blvd.",
				"",
				"Beverly Hills",
				"CA",
				223,
				"90211",
				"323-836-3000",
				CreditCardType.MasterCard,
				"5112000600000008",
				1,
				2008,
				"123",
				114.34M));

			ComboTest.SelectedIndex = 0;
			#endregion

			setupPlanTab();
		}


		private void setupPlanTab()
		{
			#region CurrencyID setup
			ddlCurrencyID.Items.Add(new NameValue("USD", 1));
			ddlCurrencyID.Items.Add(new NameValue("EU", 2));
			ddlCurrencyID.Items.Add(new NameValue("CAD", 3));
			ddlCurrencyID.Items.Add(new NameValue("GBP", 4));
			ddlCurrencyID.Items.Add(new NameValue("AUD", 5));
			ddlCurrencyID.Items.Add(new NameValue("NIS", 6));
			ddlCurrencyID.Items.Add(new NameValue("VBV", 7));
			ddlCurrencyID.DisplayMember = "Name";
			ddlCurrencyID.ValueMember = "Value";
			ddlCurrencyID.SelectedIndex = 0;
			#endregion

			#region RenewDurationTypeID setup
			ddlRenewDurationTypeID.Items.Add(new NameValue("Miniute", 1));
			ddlRenewDurationTypeID.Items.Add(new NameValue("Hour", 2));
			ddlRenewDurationTypeID.Items.Add(new NameValue("Day", 3));
			ddlRenewDurationTypeID.Items.Add(new NameValue("Week", 4));
			ddlRenewDurationTypeID.Items.Add(new NameValue("Month", 5));
			ddlRenewDurationTypeID.Items.Add(new NameValue("Year", 6));
			ddlRenewDurationTypeID.DisplayMember = "Name";
			ddlRenewDurationTypeID.ValueMember = "Value";
			ddlRenewDurationTypeID.SelectedIndex = 0;
			#endregion

			#region GroupID setup
			// Not using the PlanIDs that are in the contructors here
			ddlGroupID.Items.Add(new NameValue("AS", 1001));
			ddlGroupID.Items.Add(new NameValue("JD", 1003));
			ddlGroupID.Items.Add(new NameValue("JDIL", 1004));
			ddlGroupID.Items.Add(new NameValue("JDFR", 1005));
			ddlGroupID.DisplayMember = "Name";
			ddlGroupID.ValueMember = "Value";
			ddlGroupID.SelectedIndex = 0;
			#endregion

			#region InitialDurationTypeID setup
			ddlInitialDurationTypeID.Items.Add(new NameValue("Miniute", 1));
			ddlInitialDurationTypeID.Items.Add(new NameValue("Hour", 2));
			ddlInitialDurationTypeID.Items.Add(new NameValue("Day", 3));
			ddlInitialDurationTypeID.Items.Add(new NameValue("Week", 4));
			ddlInitialDurationTypeID.Items.Add(new NameValue("Month", 5));
			ddlInitialDurationTypeID.Items.Add(new NameValue("Year", 6));
			ddlInitialDurationTypeID.DisplayMember = "Name";
			ddlInitialDurationTypeID.ValueMember = "Value";
			ddlInitialDurationTypeID.SelectedIndex = 0;
			#endregion

			#region lbPlanTypes setup
            lbPlanTypes.Items.Add(new NameValue("Regular", 1));
			lbPlanTypes.Items.Add(new NameValue("One Time Only", 2));
			lbPlanTypes.Items.Add(new NameValue("Auth Only", 4));
			lbPlanTypes.Items.Add(new NameValue("Terminate Immediately", 8));
			lbPlanTypes.Items.Add(new NameValue("Free Trial Welcome", 16));
			lbPlanTypes.Items.Add(new NameValue("Registration", 32));
			lbPlanTypes.Items.Add(new NameValue("Installments", 64));
			lbPlanTypes.Items.Add(new NameValue("Promotional Plan", 128));
			lbPlanTypes.Items.Add(new NameValue("Premium Plan", 256));
			lbPlanTypes.Items.Add(new NameValue("Site Default", 512));
			lbPlanTypes.DisplayMember = "Name";
            lbPlanTypes.ValueMember = "Value";
			lbPlanTypes.SelectionMode = SelectionMode.MultiExtended;
			#endregion

			#region lbPremiumTypes
			lbPremiumTypes.Items.Add(new NameValue("HighlightedProfile", 1));
			lbPremiumTypes.Items.Add(new NameValue("SpotedlightMember", 2));
			lbPremiumTypes.Items.Add(new NameValue("none", 0));
			lbPremiumTypes.DisplayMember = "Name";
			lbPremiumTypes.ValueMember = "Value";
			lbPremiumTypes.SelectionMode = SelectionMode.MultiExtended;
			#endregion

			#region lbPaymentTypes
			lbPaymentTypes.Items.Add(new NameValue("Credit Card", 1));
			lbPaymentTypes.Items.Add(new NameValue("Check", 2));
			lbPaymentTypes.Items.Add(new NameValue("DirectDebit", 4));
			lbPaymentTypes.DisplayMember = "Name";
			lbPaymentTypes.ValueMember = "Value";
			lbPaymentTypes.SelectionMode = SelectionMode.MultiExtended;
			#endregion

			dtpStartDate.Value = DateTime.Now;
		}

		private void hitIt()
		{
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand;
			Int32 memberID;
			BrandItem brandItem = (BrandItem)ComboBrand.Items[ComboBrand.SelectedIndex];
			TestItem testItem = (TestItem)ComboTest.Items[ComboTest.SelectedIndex];
			SubscriptionResult subscriptionResult;

			//			// Verify that a 5DFT was selected if AuthOnly was selected.
			//			if (cbxAVSOnly.Checked && !brandItem.Name.EndsWith("5DFT"))
			//			{
			//				MessageBox.Show("You must selected a 5DFT Brand type in order to successfully run AuthOnly transactions.");
			//				return;
			//			}

			// If this is an AVSOnly, use a 0.00 test amount.
			//			Decimal amount = 0.00M;
			//			if (!cbxAVSOnly.Checked)
			//				amount = testItem.Amount;
			// An amount of 0 means that the Plan's amount will be used.
			Decimal amount = 0;

			//get brand
			try
			{
				string uri = brandItem.ToString();
				log("getting brand (uri: " + uri + ")");
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
				brand = BrandConfigSA.Instance.GetBrandByURI(uri);
				log("got brand (brandID: " + brand.BrandID.ToString() + ")");
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
			}
			catch (Exception ex)
			{
				log(ex.ToString());
				return;
			}


			//register a new member
			try
			{
				log("registering member");
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
				string username = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
				string password = username;
				string emailAddress = username + "@" + username + ".com";
				IPHostEntry host = Dns.GetHostByName(Dns.GetHostName());
				string ip = host.AddressList[0].ToString();
				int ipAddress=BitConverter.ToInt32(System.Net.IPAddress.Parse(ip).GetAddressBytes(),0);
				MemberRegisterResult registerResult = MemberSA.Instance.Register(brand, emailAddress, username, password, ipAddress);
				if (registerResult.RegisterStatus != RegisterStatusType.Success)
				{
					throw new Exception("Error registering member (registerStatus: " + registerResult.RegisterStatus.ToString() + ")");
				}
				memberID = registerResult.MemberID;
				log("registered member (memberID: " + memberID + ")");
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
			}
			catch (Exception ex)
			{
				log(ex.ToString());
				return;
			}

			//purchase
			try
			{
				log("submitting credit card purchase");
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
				
				PurchaseSM objPurchaseSM = new Matchnet.Purchase.ServiceManagers.PurchaseSM();
				objPurchaseSM.Start();

				subscriptionResult = objPurchaseSM.BeginCreditCardPurchase(memberID,
					Constants.NULL_INT,
					brandItem.PlanID,
					Constants.NULL_INT,
					brand.Site.SiteID,
					brand.BrandID,
					testItem.FirstName,
					testItem.LastName,
					"",
					testItem.Phone,
					testItem.AddressLine1,
					testItem.City,
					testItem.State,
					testItem.CountryRegionID,
					testItem.PostalCode,
					testItem.CreditCardType,
					testItem.CreditCardNumber,
					testItem.ExpirationMonth,
					testItem.ExpirationYear,
					testItem.CVC,
					Constants.NULL_INT,
					Constants.NULL_INT,
					Constants.NULL_INT,
					"127.0.0.1");

				/*
				subscriptionResult = PurchaseSA.Instance.BeginCreditCardPurchase(memberID,
					Constants.NULL_INT,
					brandItem.PlanID,
					Constants.NULL_INT,
					brand.Site.SiteID,
					brand.BrandID,
					testItem.FirstName,
					testItem.LastName,
					"",
					testItem.Phone,
					testItem.AddressLine1,
					testItem.City,
					testItem.State,
					testItem.CountryRegionID,
					testItem.PostalCode,
					testItem.CreditCardType,
					testItem.CreditCardNumber,
					testItem.ExpirationMonth,
					testItem.ExpirationYear,
					testItem.CVC,
					Constants.NULL_INT,
					Constants.NULL_INT,
					Constants.NULL_INT,
					"127.0.0.1");
				*/
				log("submitted credit card purchase (memberTranID: " + subscriptionResult.MemberTranID.ToString() + ")");
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
			}
			catch (Exception ex)
			{
				log(ex.ToString());
				return;
			}


			//get status
			try
			{
				while (subscriptionResult.MemberTranStatus != MemberTranStatus.Failure && subscriptionResult.MemberTranStatus != MemberTranStatus.Success)
				{
					System.Threading.Thread.Sleep(2000);
					log("checking transaction status");
					Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
					subscriptionResult = PurchaseSA.Instance.GetMemberTranStatus(memberID, subscriptionResult.MemberTranID);
				}
				
				log("transaction status: " + subscriptionResult.MemberTranStatus.ToString());
				Matchnet.Purchase.BusinessLogic.Logger.Instance.Log(Matchnet.Purchase.BusinessLogic.Logger.LoggerType.RenewalFailureNotification, "");
			}
			catch (Exception ex)
			{
				log(ex.ToString());
				return;
			}
		}

		private void clearLog()
		{
			TextResult.Text = "";
		}

		private void log(string logText)
		{
			TextResult.Text = TextResult.Text + DateTime.Now + " " + logText + "\r\n";
			
			this.Refresh();
			Application.DoEvents();
		}

		private void endSub_Click(object sender, System.EventArgs e)
		{
			SubscriptionResult subscriptionResult;
			try
			{
				txtResult.Text = "";
				subscriptionResult = PurchaseSA.Instance.EndSubscription(Convert.ToInt32(txtMemberId.Text), Convert.ToInt32(txtAdminId.Text), Convert.ToInt32(txtSiteId.Text), Convert.ToInt32(txtReasonId.Text));
				txtResult.Text ="MemberTranResult " +  (subscriptionResult.ReturnValue==0);
			}
			catch (Exception ex)
			{
				this.txtResult.Text = ex.Message;
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				button1.Enabled = false;
				this.Cursor = Cursors.WaitCursor;
				clearLog();
				hitIt();
			}
			catch (Exception ex)
			{
				log(ex.ToString());
			}
			finally
			{
				log("*******************done*******************");
				this.Cursor = Cursors.Default;
				button1.Enabled = true;
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			int retVal=Purchase.ServiceAdapters.Admin.PurchaseAdminSA.Instance.UpdateSubscription(Int32.Parse(txtMemberId.Text),Int32.Parse(txtSiteId.Text),Int32.Parse( txtAdminId.Text),Int32.Parse( txtDays.Text), Matchnet.DurationType.Day, Matchnet.Purchase.ValueObjects.TranType.TrialPayAdjustment,0,false,0);
		
		}

		private void button2_Click_1(object sender, System.EventArgs e)
		{
			DateTime dtEndDate= DateTime.MinValue;
			if (txtSubEndDate.Text.Length>0)
			{
				dtEndDate=DateTime.Parse(txtSubEndDate.Text);
			}
			lblMemberSubStatus.Text= PurchaseSA.Instance.GetMemberSubStatus(Int32.Parse(txtMemberId.Text),Int32.Parse(txtSiteId.Text),dtEndDate).Status.ToString();
		}
		
		private void btnInsertPlan_Click(object sender, System.EventArgs e)
		{
			try
			{
				Plan plan = new Plan();

				plan.CurrencyType = (CurrencyType)((NameValue)ddlCurrencyID.SelectedItem).Value;
				plan.StartDate = dtpStartDate.Value;
				plan.InitialCost = Convert.ToDecimal(tbxInitialCost.Text);
				plan.InitialDuration = Convert.ToInt32(tbxInitialDuration.Text);
				plan.InitialDurationType = (DurationType)((NameValue)ddlInitialDurationTypeID.SelectedItem).Value;
				plan.RenewCost = Convert.ToDecimal(tbxRenewCost.Text);
				plan.RenewDurationType = (DurationType)((NameValue)ddlRenewDurationTypeID.SelectedItem).Value;
				plan.RenewDuration = Convert.ToInt32(tbxRenewDuration.Text);
				plan.RenewAttempts = Convert.ToInt32(tbxRenewAttempts.Text);
				plan.PlanTypeMask = (PlanType)GetMaskValue(lbPlanTypes);
				plan.PremiumTypeMask = (PremiumType)GetMaskValue(lbPremiumTypes);
				plan.PaymentTypeMask = (PaymentType)GetMaskValue(lbPaymentTypes);

				int planId = PlanSA.Instance.InsertPlan(((NameValue)ddlGroupID.SelectedItem).Value, plan);
				MessageBox.Show("Plan was created. PlanID: " + planId);
			}
			catch(Exception ex)
			{
				MessageBox.Show("Exception: " + ex.Message);
			}
		}

		private Int32 GetMaskValue(ListBox lbTarget)
		{
			Int32 maskVal = 0;
			
			foreach(Object obj in lbTarget.SelectedItems)
			{
				NameValue nameValue = (NameValue)obj;
				maskVal = maskVal | nameValue.Value;
			}

			return maskVal;
		}

		private void btnValidatePurchase_Click(object sender, System.EventArgs e)
		{
			int returnValue = Constants.NULL_INT;
			PurchaseBL purchaseBL = new PurchaseBL();
			SubscriptionResult sr = purchaseBL.UPSValidatePlanPurchase(72,
								100066132,
								0,
								103,
								1003,
								10.00m,
								5.00m,
								PurchaseMode.Upgrade,
								PaymentType.CreditCard,
								121663324);
		}

		private void btnCreditCardPurchase_Click(object sender, System.EventArgs e)
		{
			Matchnet.Data.Hydra.HydraWriter hydraWriter;
			hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnSubscription", "mnAdmin", "mnLookup", "mnChargeStage"});
			hydraWriter.Start();

			try
			{
				PurchaseBL purchaseBL = new PurchaseBL();
				Int32 returnVal = purchaseBL.UPSDoCreditCardPurchase(100066132,
					100066132,
					new int[]{72, 100},
					Constants.NULL_INT,
					103,
					1003,
					CreditCardType.Visa,
					Constants.NULL_INT,
					Constants.NULL_INT,
					Constants.NULL_INT,
					"127.0.0.1",
					1346,
					10.00m,
					PurchaseMode.Upgrade, 
					true,
					"John",
					"Talner",
					20.0m,
					1,
					DurationType.Day,
					"5636",
					1000967, // Get after UPS is called 
					1, // Get after UPS is called
					//string resourceConstant, // Get after UPS is called
					2, // Get after UPS is called
					//string responseCode, // Get after UPS is called
					//string AVSCode, // Get after UPS is called
					//string CVVCode, // Get after UPS is called
					//bool purchaseModeValidForCredit, // Get after UPS is called
					1001720);
			}
			catch (Exception ex)
			{
				string message = ex.Message;
			}
			finally
			{
				hydraWriter.Stop();
			}
		}

		private void btnCheckPurchase_Click(object sender, System.EventArgs e)
		{
			Matchnet.Data.Hydra.HydraWriter hydraWriter;
			hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnSubscription", "mnAdmin", "mnLookup", "mnChargeStage"});
			hydraWriter.Start();

			try
			{
				PurchaseBL purchaseBL = new PurchaseBL();
				Int32 returnVal = purchaseBL.UPSDoCheckPurchase(100066132,
					100066132,
					new int[] {72, 100},
					Constants.NULL_INT,
					103,
					1003,
					Constants.NULL_INT,
					Constants.NULL_INT,
					Constants.NULL_INT,
					"127.0.0.1",
					1346,
					10.00m,
					PurchaseMode.Upgrade, 
					true,
					"John",
					"Talner",
					20.0m,
					1,
					DurationType.Day,
					"5636",
					1000966, // Get after UPS is called 
					1, // Get after UPS is called
					//string resourceConstant, // Get after UPS is called
					2, // Get after UPS is called
					//string responseCode, // Get after UPS is called
					//string AVSCode, // Get after UPS is called
					//string CVVCode, // Get after UPS is called
					//bool purchaseModeValidForCredit, // Get after UPS is called
					1001720);
			}
			catch (Exception ex)
			{
				string message = ex.Message;
			}
			finally
			{
				hydraWriter.Stop();
			}		
		}

		private void btnCreditTransaction_Click(object sender, System.EventArgs e)
		{
			Matchnet.Data.Hydra.HydraWriter hydraWriter;
			hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnSubscription", "mnAdmin", "mnLookup", "mnChargeStage"});
			hydraWriter.Start();

			try
			{
				PurchaseBL purchaseBL = new PurchaseBL();
				int sr = purchaseBL.UPSDoCredit(TranType.AdministrativeAdjustment,
					100066132,
					100066132,
					1.00m,
					CreditCardType.Visa,
					20,
					DurationType.Minute,
					CurrencyType.USDollar,
					"John",
					"Talner",
					"5636",
					1000968, // Get after UPS is called 
					1000967,
					1, // Get after UPS is called
					//string resourceConstant, // Get after UPS is called
					2, // Get after UPS is called
					//string responseCode, // Get after UPS is called
					//string AVSCode, // Get after UPS is called
					//string CVVCode, // Get after UPS is called
					//bool purchaseModeValidForCredit, // Get after UPS is called
					1001720);
			}
			catch (Exception ex)
			{
				string message = ex.Message;
			}
			finally
			{
				hydraWriter.Stop();
			}		
		}

		private void btnAuthorizePmtProfile_Click(object sender, System.EventArgs e)
		{
			Matchnet.Data.Hydra.HydraWriter hydraWriter;
			hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnSubscription", "mnAdmin", "mnLookup", "mnChargeStage"});
			hydraWriter.Start();

			try
			{
				PurchaseBL purchaseBL = new PurchaseBL();
				Int32 returnVal = purchaseBL.UPSDoAuthPmtProfile(100066132,
					100066132,
					103,
					CreditCardType.Visa,
					Constants.NULL_INT,
					Constants.NULL_INT,
					"127.0.0.1",
					1015306,
					1000966,
					1,
					2,
					1001720);
			}
			catch (Exception ex)
			{
				string message = ex.Message;
			}
			finally
			{
				hydraWriter.Stop();
			}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			TextResult.Text = "";
			try
			{
				//initiate encore
				TextResult.Text = PurchaseSA.Instance.GenerateEncryptedSalesFile().ToString();
			}
			catch (Exception ex)
			{
				TextResult.Text = ex.Message;
			}

		}		
	}
}
