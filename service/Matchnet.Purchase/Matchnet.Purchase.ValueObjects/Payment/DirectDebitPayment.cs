using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class DirectDebitPayment  : Payment, IValueObject
	{
		#region Private Members

		private string streetName = Constants.NULL_STRING;
		private string houseNumber = Constants.NULL_STRING;
		private string bankName = Constants.NULL_STRING;
		private string bankCode = Constants.NULL_STRING;
		private string directDebitAccountNumber = Constants.NULL_STRING;

		#endregion

		#region Constructors

		public DirectDebitPayment()
		{
		}

		public DirectDebitPayment(
			int memberID,
			int siteID,
			int memberPaymentID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			int countryRegionID,
			string postalCode,
			PaymentType paymentType,
			CurrencyType currencyType,
			string streetName,
			string houseNumber,
			string bankName,
			string bankCode,
			string directDebitAccountNumber) : base (
			memberID,
			siteID,
			memberPaymentID,
			firstName,
			lastName,
			phone,
			addressLine1,
			city,
			state,
			countryRegionID,
			postalCode,
			paymentType,
			currencyType)
		{
			this.streetName = streetName;
			this.houseNumber = houseNumber;
			this.bankName = bankName;
			this.bankCode = bankCode;
			this.directDebitAccountNumber = directDebitAccountNumber;
		}

		#endregion

		#region Properties

		public string StreetName
		{
			get{return streetName;}
			set{streetName = value;}
		}	

		public string HouseNumber
		{
			get{return houseNumber;}
			set{houseNumber = value;}
		}	

		public string BankName
		{
			get{return bankName;}
			set{bankName = value;}
		}	

		public string BankCode
		{
			get{return bankCode;}
			set{bankCode = value;}
		}	

		public string DirectDebitAccountNumber
		{
			get{return directDebitAccountNumber;}
			set{directDebitAccountNumber = value;}
		}	

		#endregion
	}
}
