using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	
	#region PaymentType Enum

	[Flags]
	public enum PaymentType : int
	{
		None = 0,
		CreditCard = 1,
		Check = 2,
		DirectDebit = 4,
		PaypalLitle = 8,
		SMS = 16,
		PaypalDirect = 32,
		Manual = 64,
		ElectronicFundsTransfer = 128,
		PaymentReceived = 256
	};

	#endregion

	[Serializable]
	public class Payment
	{
		#region Private Members

		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		private int memberPaymentID = Constants.NULL_INT;
		private string firstName = Constants.NULL_STRING;
		private string lastName = Constants.NULL_STRING;
		private string phone = Constants.NULL_STRING;
		private string addressLine1 = Constants.NULL_STRING;
		private string city = Constants.NULL_STRING;
		private string state = Constants.NULL_STRING;
		private int countryRegionID = Constants.NULL_INT;
		private string postalCode = Constants.NULL_STRING;
		private PaymentType paymentType = PaymentType.None;
		private CurrencyType currencyType;

		#endregion
		
		#region Constructors

		public Payment()
		{
		}
		
		public Payment(
			int memberID,
			int siteID,
			int memberPaymentID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			int countryRegionID,
			string postalCode,
			PaymentType paymentType,
			CurrencyType currencyType)
		{
			this.memberID = memberID;
			this.siteID = siteID;
			this.memberPaymentID = memberPaymentID;
			this.firstName = firstName;
			this.lastName = lastName;
			this.phone = phone;
			this.addressLine1 = addressLine1;
			this.city = city;
			this.state = state;
			this.countryRegionID = countryRegionID;
			this.postalCode = postalCode;
			this.paymentType = paymentType;
			this.currencyType = currencyType;
		}

		#endregion

		#region Properties

		public int MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		public int SiteID
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public int MemberPaymentID
		{
			get { return memberPaymentID; }
			set { memberPaymentID = value; }
		}

		public string FirstName
		{
			get { return firstName; }
			set { firstName = value; }
		}

		public string LastName
		{
			get { return lastName; }
			set { lastName = value; }
		}

		public string Phone 
		{
			get { return phone; }
			set { phone = value; }
		}

		public string AddressLine1 
		{
			get { return addressLine1; }
			set { addressLine1 = value; }
		}

		public string City 
		{
			get { return city; }
			set { city = value; }
		}

		public string State
		{
			get { return state; }
			set { state = value; }
		}

		public int CountryRegionID
		{
			get { return countryRegionID; }
			set { countryRegionID = value; }
		}

		public string PostalCode
		{
			get { return postalCode; }
			set { postalCode = value; }
		}

		public PaymentType PaymentType
		{
			get { return paymentType; }
			set { paymentType = value; }
		}

		public CurrencyType CurrencyType
		{
			get { return currencyType; }
			set { currencyType = value; }
		}

		#endregion
	}
}
