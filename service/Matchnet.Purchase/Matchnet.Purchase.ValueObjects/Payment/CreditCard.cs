using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class CreditCard : IValueObject
	{
		
		#region Private Members
		
		private int creditCardID = Constants.NULL_INT;
		private string content = Constants.NULL_STRING;
		private string resourceConstant = Constants.NULL_STRING;
		#endregion

		#region Constructors

		public CreditCard(
			int creditCardID, 
			string content,
			string resourceConstant)
		{
			this.creditCardID = creditCardID;
			this.content = content;
			this.resourceConstant = resourceConstant;
		}

		#endregion

		#region Properties

		public int CreditCardID
		{
			get { return creditCardID; }
			set { creditCardID = value; }
		}

		public string Content
		{
			get { return content; }
			set { content = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		#endregion
	}
}
