using System;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class PaypalPayment : Payment, IValueObject
	{
		private string _Email = String.Empty;
		private string _FirstName = String.Empty;
		private string _LastName = String.Empty;
		private string _Country = String.Empty;
		private string _PaypalToken = String.Empty;
		private string _PayerStatus = String.Empty;
		private string _PayerID = String.Empty;
		private string _BillingAgreementID = String.Empty;

		public PaypalPayment()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string Email
		{
			get { return this._Email; }
			set { this._Email = value; }
		}

		public string FirstName
		{
			get { return this._FirstName; }
			set { this._FirstName = value; }
		}

		public string LastName
		{
			get { return this._LastName; }
			set { this._LastName = value; }
		}

		public string Country
		{
			get { return this._Country; }
			set { this._Country = value; }
		}

		public string PaypalToken
		{
			get { return this._PaypalToken; }
			set { this._PaypalToken = value; }
		}

		public string PayerStatus
		{
			get { return this._PayerStatus; }
			set { this._PayerStatus = value; }
		}

		public string PayerID
		{
			get { return this._PayerID; }
			set { this._PayerID = value; }
		}

		public string BillingAgreementID
		{
			get { return this._BillingAgreementID; }
			set { this._BillingAgreementID = value; }
		}

	}
}
