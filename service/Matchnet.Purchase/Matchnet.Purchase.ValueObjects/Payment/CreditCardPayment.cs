using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{

	#region CreditCardType Enum

	public enum CreditCardType : int
	{
		None = 0,
		AmericanExpress = 1,
		CarteBlanche = 2,
		DinersClub = 4,
		Switch = 5,
		Discover = 8,
		Solo = 9,
		DirectDebit = 10,
		Delta = 11,
		Electron = 12,
		MasterCard = 32,
		Visa = 64,
		YoterClubLeumiCard = 68,
		LeumiCardStudentCard = 70,
		YoterClubIsraCard = 72,
		IsraCardCampusCardStudent = 74,
		VisaCalSoldiersClub = 76,
		IsraCard = 128
	};

	#endregion
	
	[Serializable]
	public class CreditCardPayment : Payment, IValueObject
	{
		#region Private Members

		private string creditCardNumber = Constants.NULL_STRING;
		private int expirationMonth = Constants.NULL_INT;
		private int expirationYear = Constants.NULL_INT;
		private CreditCardType creditCardType;
		private string cvc = Constants.NULL_STRING;
		private string israeliID = Constants.NULL_STRING;

		#endregion

		#region Constructors

		public CreditCardPayment()
		{
		}

		public CreditCardPayment(
			int memberID,
			int siteID,
			int memberPaymentID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			int countryRegionID,
			string postalCode,
			PaymentType paymentType,
			CurrencyType currencyType,
			string creditCardNumber,
			int expirationMonth,
			int expirationYear,
			CreditCardType creditCardType,
			string cvc,
			string israeliID) : base (
			memberID,
			siteID,
			memberPaymentID,
			firstName,
			lastName,
			phone,
			addressLine1,
			city,
			state,
			countryRegionID,
			postalCode,
			paymentType,
			currencyType)
		{
			this.creditCardNumber = creditCardNumber;
			this.expirationMonth = expirationMonth;
			this.expirationYear = expirationYear;
			this.creditCardType = creditCardType;
			this.cvc = cvc;
			this.israeliID = israeliID;
		}

		#endregion
		
		#region Properties

		public string CreditCardNumber
		{
			get{return creditCardNumber;}
			set{creditCardNumber = value;}
		}

		public int ExpirationMonth
		{
			get{return expirationMonth;}
			set{expirationMonth = value;}
		}

		public int ExpirationYear
		{
			get{return expirationYear;}
			set{expirationYear = value;}
		}

		public CreditCardType CreditCardType
		{
			get{return creditCardType;}
			set{creditCardType = value;}
		}

		public string CVC 
		{
			get{return cvc;}
			set{cvc = value;}
		}

		public string IsraeliID 
		{
			get{return israeliID;}
			set{israeliID = value;}
		}

		#endregion
	}
}
