using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	public enum BankAccountType : int
	{
		Checking = 10,
		Savings = 20
	};

	[Serializable]
	public class CheckPayment : Payment, IValueObject
	{
		#region Private Members

		private string addressLine2 = Constants.NULL_STRING;
		private string driversLicenseNumber = Constants.NULL_STRING;
		private string driversLicenseState = Constants.NULL_STRING;
		private string bankName = Constants.NULL_STRING;
		private string bankRoutingNumber = Constants.NULL_STRING;
		private string checkingAccountNumber = Constants.NULL_STRING;
		private int checkNumber = Constants.NULL_INT;
		private BankAccountType bankAccountType;

		#endregion

		#region Constructors

		public CheckPayment()
		{
		}

		public CheckPayment(
			int memberID,
			int siteID,
			int memberPaymentID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			int countryRegionID,
			string postalCode,
			PaymentType paymentType,
			CurrencyType currencyType,
			string addressLine2,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			int checkNumber,
			BankAccountType bankAccountType) : base (
			memberID,
			siteID,
			memberPaymentID,
			firstName,
			lastName,
			phone,
			addressLine1,
			city,
			state,
			countryRegionID,
			postalCode,
			paymentType,
			currencyType)
		{
			this.addressLine2 = addressLine2;
			this.driversLicenseNumber = driversLicenseNumber;
			this.driversLicenseState = driversLicenseState;
			this.bankName = bankName;
			this.bankRoutingNumber = bankRoutingNumber;
			this.checkingAccountNumber = checkingAccountNumber;
			this.checkNumber = checkNumber;
			this.bankAccountType = bankAccountType;
		}

		#endregion

		#region Properties

		public string AddressLine2
		{
			get{return addressLine2;}
			set{addressLine2 = value;}
		}

		public string DriversLicenseNumber
		{
			get{return driversLicenseNumber;}
			set{driversLicenseNumber = value;}
		}

		public string DriversLicenseState
		{
			get{return driversLicenseState;}
			set{driversLicenseState = value;}
		}

		public string BankName
		{
			get{return bankName;}
			set{bankName = value;}
		}

		public string BankRoutingNumber
		{
			get{return bankRoutingNumber;}
			set{bankRoutingNumber = value;}
		}

		public string CheckingAccountNumber
		{
			get{return checkingAccountNumber;}
			set{checkingAccountNumber = value;}
		}

		public int CheckNumber
		{
			get{return checkNumber;}
			set{checkNumber = value;}
		}

		public BankAccountType BankAccountType
		{
			get{return bankAccountType;}
			set{bankAccountType = value;}
		}

		#endregion
	}
}
