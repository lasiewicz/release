using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class MemberPayment : IValueObject
	{
		private Payment payment = null;
		private int memberPaymentID = Constants.NULL_INT;
		private int memberID = Constants.NULL_INT;
		private PaymentType paymentType;
		private int domainID = Constants.NULL_INT;
		private DateTime insertDate = DateTime.MinValue;

		public MemberPayment(Payment payment,
			int memberID,
			PaymentType paymentType,
			int domainID,
			DateTime insertDate) : this(payment,
			Constants.NULL_INT,
			memberID,
			paymentType,
			domainID,
			insertDate)
		{
		}

		public MemberPayment(Payment payment,
			int memberPaymentID,
			int memberID,
			PaymentType paymentType,
			int domainID,
			DateTime insertDate)
		{
			this.payment = payment;
			this.memberPaymentID = memberPaymentID;
			this.memberID = memberID;
			this.paymentType = paymentType;
			this.domainID = domainID;
			this.insertDate = insertDate;
		}
	}
}
