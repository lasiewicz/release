using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class CreditCardCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members

		private int _cacheTTLSeconds = 60 * 10; 
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
		private int siteID = Constants.NULL_INT;

		#endregion

		#region Constructors

		public CreditCardCollection(int siteID)
		{
			this.siteID = siteID;
		}

		#endregion

		#region Properties
		
		public int SiteID
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public CreditCard this[int index]
		{
			get { return (CreditCard)base.InnerList[index]; }
		}

		#endregion

		#region CollectionBase Implementation

		public int Add(CreditCard creditCard)
		{
			return base.InnerList.Add(creditCard);
		}

		public CreditCard FindByID(int creditCardID)
		{
			foreach(CreditCard creditCard in this)
			{
				if(creditCard.CreditCardID  == creditCardID)
				{
					return creditCard;
				}
			}
			return null;
		}

		#endregion
		
		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return _cacheTTLSeconds; }
			set { _cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return _cacheItemMode; }
			set { _cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return _cachePriority; }
			set { _cachePriority = value; }
		}

		public static string GetCacheKey(int siteID)
		{
			return "~CREDITCARDCOLLECTION^" + siteID.ToString();
		}

		public string GetCacheKey()
		{
			return "~CREDITCARDCOLLECTION^" + siteID.ToString();
		}

		#endregion
	}
}
