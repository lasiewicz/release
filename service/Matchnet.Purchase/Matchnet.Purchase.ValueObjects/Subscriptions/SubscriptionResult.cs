using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class SubscriptionResult : IValueObject
	{
		#region Private Members

		private bool result = false;
		private int returnValue = Constants.NULL_INT;
		private string resourceConstant = Constants.NULL_STRING;
		private int memberTranID = Constants.NULL_INT;
		private MemberTranStatus memberTranStatus = MemberTranStatus.None;
		private int memberPaymentID = Constants.NULL_INT;
		private int planID = Constants.NULL_INT;
		private decimal amount = Constants.NULL_DECIMAL;
		private int duration = Constants.NULL_INT;
		private PaymentVerificationRequest paReq = null;
		private PlanDisplayValidation planDisplayValidationResult = PlanDisplayValidation.None;
		
		#endregion
		
		#region Constructors

		public SubscriptionResult()
		{
		}

		#endregion
		
		#region Properties
		
		public bool Result
		{
			get { return result; }
			set { result = value; }
		}

		public int ReturnValue
		{
			get { return returnValue; }
			set { returnValue = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public int MemberTranID 
		{
			get { return memberTranID; }
			set { memberTranID = value; }
		}

		public MemberTranStatus MemberTranStatus
		{
			get { return memberTranStatus; }
			set { memberTranStatus = value; }
		}

		public int MemberPaymentID
		{
			get { return memberPaymentID; }
			set { memberPaymentID = value; }
		}

		public int PlanID
		{
			get { return planID; }
			set { planID = value; }
		}

		public decimal Amount
		{
			get { return amount; }
			set { amount = value; }
		}

		public int Duration 
		{
			get { return duration; }
			set { duration = value; }
		}

		public PaymentVerificationRequest PaReq
		{
			get { return paReq; }
			set { paReq = value; }
		}

		/// <summary>
		/// Set/Get the server side validation of selected plan from UI
		/// </summary>
		public PlanDisplayValidation PlanDisplayValidationResult
		{
			get { return planDisplayValidationResult;}
			set { planDisplayValidationResult = value;}
		}

		#endregion
	}
}
