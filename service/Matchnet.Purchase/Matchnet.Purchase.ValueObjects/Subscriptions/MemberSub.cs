using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	#region FreeTrialGroupType Enums

	public enum FreeTrialGroupType : int
	{
		None = 0,
		Group1 = 1,
		Group2 = 2,
		Group3 = 3,
		Group4 = 4
	}

	#endregion

	#region ResourceConstants Enums
		
	public enum ResourceConstants : int
	{
		MNCHARGE___DATABASEERROR_13082 = 13082,
		INVALIDMEMBERPAYMENT = 518773,
		CHARGE_PENDING = 13201,
		YOU_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_CHARGES_IN_A_24_HOUR_PERIOD = 521538,
		YOU_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_FAILURES_IN_A_24_HOUR_PERIOD = 601668,
		INVALID_PACKAGE = 519137,
		CLOSED_PLAN = 13203,
		DISCOUNT_NOT_AVAILABLE = 600226,
		NO_CURRENT_SUBSCRIPTION = 519035,
		INVALID_PLANDISCOUNT = 601634,
		YOU_HAVE_EXCEEDED_THE_MAXIMUM_NUMBER_OF_AUTHORIZATION_IN_A_24_HOUR_PERIOD = 521539,
		PLAN_PURCHASE_VALIDATION_REMAINING_CREDIT_CHANGED = 10010,
		PLAN_PURCHASE_VALIDATION_PURCHASE_MODE_CHANGED = 10011,
		PLAN_PURCHASE_VALIDATION_NEGATIVE_PURCHASE_AMOUNT = 10012,
		PLAN_PURCHASE_VALIDATION_INVALID_CREDIT_DISCOUNT = 10013, //Credited amount is greater than cost of the plan

	}

	#endregion

	[Serializable]
	public class MemberSub : IValueObject
	{
		#region Private Members

		private int memberSubID = Constants.NULL_INT;
		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		private int memberPaymentID = Constants.NULL_INT;
		private int planID = Constants.NULL_INT;
		private string planResourceConstant = Constants.NULL_STRING;
		private int discountID = Constants.NULL_INT;
		private DateTime discountStartDate = DateTime.MinValue;
		private int discountRemaining = Constants.NULL_INT;
		private string discountResourceConstant = Constants.NULL_STRING;
		private DateTime renewDate = DateTime.MinValue;
		private DateTime endDate = DateTime.MinValue;
		private int renewAttempts = Constants.NULL_INT;
		private DateTime updateDate = DateTime.MinValue;
		private PaymentType paymentType = PaymentType.None;
		private MemberSubExtended[] memberSubExtended;
		
		#endregion

		#region Constructors

		public MemberSub(){;}

		public MemberSub(
			int memberID,
			int siteID,
			int memberPaymentID,
			int planID,
			string planResourceConstant,
			int discountID,
			DateTime discountStartDate,
			int discountRemaining,
			string discountResourceConstant,
			DateTime renewDate,
			DateTime endDate,
			int renewAttempts,
			DateTime updateDate,
			PaymentType paymentType) : this (
			Constants.NULL_INT,
			memberID,
			siteID,
			memberPaymentID,
			planID,
			planResourceConstant,
			discountID,
			discountStartDate,
			discountRemaining,
			discountResourceConstant,
			renewDate,
			endDate,
			renewAttempts,
			updateDate,
			paymentType)			
		{
		}
		
		public MemberSub(
			int memberSubID,
			int memberID,
			int siteID,
			int memberPaymentID,
			int planID,
			string planResourceConstant,
			int discountID,
			DateTime discountStartDate,
			int discountRemaining,
			string discountResourceConstant,
			DateTime renewDate,
			DateTime endDate,
			int renewAttempts,
			DateTime updateDate,
			PaymentType paymentType)
		{
			this.memberSubID = memberSubID;
			this. memberID = memberID;
			this.siteID = siteID;
			this.memberPaymentID = memberPaymentID;
			this.planID = planID;
			this.planResourceConstant = planResourceConstant;
			this.discountID = discountID;
			this.discountStartDate = discountStartDate;
			this.discountRemaining = discountRemaining;
			this.discountResourceConstant = discountResourceConstant;
			this.renewDate = renewDate;
			this.endDate = endDate;
			this.renewAttempts = renewAttempts;
			this.updateDate = updateDate;
			this.paymentType = paymentType;
		}

		#endregion
		
		#region Properties
		
		public int MemberSubID
		{
			get { return memberSubID; }
			set { memberSubID = value; }
		}

		public int MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		public int SiteID
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public int MemberPaymentID
		{
			get { return memberPaymentID; }
			set { memberPaymentID = value; }
		}

		public int PlanID
		{
			get { return planID; }
			set { planID = value; }
		}

		public string PlanResourceConstant
		{
			get { return planResourceConstant; }
			set { planResourceConstant = value; }
		}

		public int DiscountID
		{
			get { return discountID; }
			set { discountID = value; }
		}

		public DateTime DiscountStartDate
		{
			get { return discountStartDate; }
			set { discountStartDate = value; }
		}

		public int DiscountRemaining
		{
			get { return discountRemaining; }
			set { discountRemaining = value; }
		}

		public string DiscountResourceConstant
		{
			get { return discountResourceConstant; }
			set { discountResourceConstant = value; }
		}

		public DateTime RenewDate
		{
			get { return renewDate; }
			set { renewDate = value; }
		}

		public DateTime EndDate
		{
			get { return endDate; }
			set { endDate = value; }
		}
		
		public int RenewAttempts
		{
			get { return renewAttempts; }
			set { renewAttempts = value; }
		}

		public DateTime UpdateDate
		{
			get { return updateDate; }
			set { updateDate = value; }
		}
		
		public PaymentType PaymentType
		{
			get { return paymentType; }
			set { paymentType = value; }
		}

		public MemberSubExtended[] MemberSubExtended
		{
			get { return memberSubExtended;}
			set { memberSubExtended = value;}
		}

		#endregion
	}
}
