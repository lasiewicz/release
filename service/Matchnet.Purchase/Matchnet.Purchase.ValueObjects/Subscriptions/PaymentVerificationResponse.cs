using System;
using System.Collections;
using System.Collections.Specialized;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class PaymentVerificationResponse
	{
		#region Private Members

		private bool approved = false;
		private int errorNumber = Constants.NULL_INT;
		private string errorDescription = Constants.NULL_STRING;
		private string xid = Constants.NULL_STRING;
		private string cavv = Constants.NULL_STRING;
		private string eciFlag = Constants.NULL_STRING;
		private string signatureVerification = Constants.NULL_STRING;
		private string paRes = Constants.NULL_STRING;
		private string resourceConstant = Constants.NULL_STRING;
		private string xmlRequest = Constants.NULL_STRING;
		private int memberTranID = Constants.NULL_INT;
		private MemberTranStatus memberTranStatus = MemberTranStatus.Failure;
		private int siteID = Constants.NULL_INT;
		private int memberID = Constants.NULL_INT;
		private string primaryUrl = Constants.NULL_STRING;
		private string secondaryUrl = Constants.NULL_STRING;
		private SiteMerchant siteMerchant = null;

		#endregion

		#region Constructors

		public PaymentVerificationResponse()
		{
		}

		#endregion

		#region Properties

		public bool Approved
		{
			get { return approved; }
			set { approved = value; }
		}

		public int ErrorNumber
		{
			get { return errorNumber; }
			set { errorNumber = value; }
		}
		public string ErrorDescription
		{
			get { return errorDescription; }
			set { errorDescription = value; }
		}

		public string Xid
		{
			get { return xid; }
			set { xid = value; }
		}

		public string Cavv
		{
			get { return cavv; }
			set { cavv = value; }
		}

		public string EciFlag
		{
			get { return eciFlag; }
			set { eciFlag = value; }
		}

		public string SignatureVerification
		{ 
			get { return signatureVerification; }
			set { signatureVerification = value; }
		}

		public string PaRes
		{
			get { return paRes; } 
			set { paRes = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public string XmlRequest
		{
			get { return xmlRequest; }
			set { xmlRequest = value; }
		}

		public int MemberTranID
		{
			get { return memberTranID; }
			set { memberTranID = value; }
		}

		public MemberTranStatus MemberTranStatus
		{
			get { return memberTranStatus; }
			set { memberTranStatus = value; }
		}

		public int SiteID
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public int MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		public string PrimaryUrl
		{
			get { return primaryUrl; }
			set { primaryUrl = value; }
		}

		public string SecondaryUrl
		{
			get { return secondaryUrl; }
			set { secondaryUrl = value; }
		}

		public SiteMerchant SiteMerchant
		{
			get { return siteMerchant; }
			set { siteMerchant = value; }
		}
		
		#endregion
	}
}
