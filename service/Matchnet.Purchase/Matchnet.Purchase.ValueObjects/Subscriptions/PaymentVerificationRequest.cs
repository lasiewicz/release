using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml.Serialization;

using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class PaymentVerificationRequest : IValueObject
	{
		#region Private Members

		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		private int brandID = Constants.NULL_INT;
		private decimal amount = Constants.NULL_DECIMAL;
		private int memberTranID = Constants.NULL_INT;
		private MemberTranStatus memberTranStatus = MemberTranStatus.Failure;
		private string creditCardNumber = Constants.NULL_STRING;
		private int expirationMonth = Constants.NULL_INT;
		private int expirationYear = Constants.NULL_INT;
		private bool enrolled = false;
		private string errorDescription = Constants.NULL_STRING;
		private int errorNumber = Constants.NULL_INT;
		private string paReq = Constants.NULL_STRING;
		private string acsUrl = Constants.NULL_STRING;
		private string primaryUrl = Constants.NULL_STRING;
		private string secondaryUrl = Constants.NULL_STRING;
		private string callBackUrl = Constants.NULL_STRING;
		private string resourceConstant = Constants.NULL_STRING;
		private string xmlRequest = Constants.NULL_STRING;
		private SiteMerchant siteMerchant = null;

		#endregion

		#region Constructors

		public PaymentVerificationRequest()
		{
		}

		#endregion

		#region Properties

		public int MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		public int SiteID
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public int BrandID
		{
			get { return brandID; }
			set { brandID = value; }
		}

		public decimal Amount
		{
			get { return amount; }
			set { amount = value; }
		}

		public int MemberTranID
		{
			get { return memberTranID; }
			set { memberTranID = value; }
		}

		public MemberTranStatus MemberTranStatus
		{
			get { return memberTranStatus; }
			set { memberTranStatus = value; }
		}

		public string CreditCardNumber
		{
			get { return creditCardNumber; }
			set { creditCardNumber = value; }
		}

		public int ExpirationMonth
		{
			get { return expirationMonth; }
			set { expirationMonth = value; }
		}

		public int ExpirationYear
		{
			get { return expirationYear; }
			set { expirationYear = value; }
		}

		public bool Enrolled
		{
			get { return enrolled; }
			set { enrolled = value; }
		}

		public string ErrorDescription
		{
			get { return errorDescription; }
			set { errorDescription = value; }
		}

		public int ErrorNumber
		{
			get { return errorNumber; }
			set { errorNumber = value; }
		}

		public string PaReq
		{
			get { return paReq; }
			set { paReq = value; }
		}

		public string AcsUrl
		{
			get { return acsUrl; }
			set { acsUrl = value; }
		}
		
		public string PrimaryUrl
		{
			get { return primaryUrl; }
			set { primaryUrl = value; }
		}

		public string SecondaryUrl
		{
			get { return secondaryUrl; }
			set { secondaryUrl = value; }
		}

		public string CallBackUrl
		{
			get { return callBackUrl; }
			set { callBackUrl = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public string XmlRequest
		{
			get { return xmlRequest; }
			set { xmlRequest = value; }
		}

		public SiteMerchant SiteMerchant
		{
			get { return siteMerchant; }
			set { siteMerchant = value; }
		}

		#endregion
	}
}
