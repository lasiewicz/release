using System;

namespace Matchnet.Purchase.ValueObjects
{

	#region Subscription Status Enum definition
		public enum SubscriptionStatus : int
		{
			NonSubscriberNeverSubscribed=1, //Non-sub, never subbed
			NonSubscriberExSubscriber=2,//Non-sub, ex-sub
			SubscribedFTS=3,//Sub, FTS
			SubscribedWinBack=4,//Sub, Win Back
			RenewalSubscribed=5,//Renewal Subscriber
			RenewalWinBack=6,//Renewal Win Back
			SubscriberFreeTrial=7,
			NonSubscriberFreeTrial=8,
			PremiumSubscriber = 9,
            PausedSubscriber = 14
		}
	#endregion

	/// <summary>
	/// Summary description for MemberSubscriptionStatus.
	/// </summary>
	
	[Serializable()]
	public class MemberSubscriptionStatus : ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private SubscriptionStatus _memberStatus= 0;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public SubscriptionStatus Status
		{
			get {return _memberStatus;}
			set {_memberStatus = value;}
		}

		

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}

