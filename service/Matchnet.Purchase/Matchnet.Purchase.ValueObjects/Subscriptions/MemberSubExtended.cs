using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Represents a MemberSubExtended data table item, used for a la carte.
	/// </summary>
	[Serializable]
	public class MemberSubExtended : IValueObject
	{
		private int memberSubID = Constants.NULL_INT;
		private DateTime endDate = DateTime.MinValue;
		private int planID = Constants.NULL_INT;

		public MemberSubExtended()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public MemberSubExtended(int pMemberSubID, int pPlanID, DateTime pEndDate)
		{
			memberSubID = pMemberSubID;
			planID = pPlanID;
			endDate = pEndDate;
		}

		public int MemberSubID
		{
			get { return memberSubID; }
			set { memberSubID = value; }
		}

		public int PlanID
		{
			get { return planID; }
			set { planID = value; }
		}

		public DateTime EndDate
		{
			get { return endDate; }
			set { endDate = value; }
		}
	}
}
