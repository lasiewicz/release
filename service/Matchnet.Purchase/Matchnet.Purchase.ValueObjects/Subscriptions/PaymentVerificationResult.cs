using System;
using System.Collections.Specialized;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class PaymentVerificationResult
	{
		#region Private Members

		private bool enrolled = false;
		private string errorDescription = Constants.NULL_STRING;
		private int errorNumber = Constants.NULL_INT;
		private string paReq = Constants.NULL_STRING;
		private string acsUrl = Constants.NULL_STRING;
		private string callBackUrl = Constants.NULL_STRING;
		private string resourceConstant = Constants.NULL_STRING;
		private NameValueCollection nameValueCollection = null;

		#endregion

		public PaymentVerificationResult()
		{
		}

		#region Properties

		public bool Enrolled
		{
			get { return enrolled; }
			set { enrolled = value; }
		}

		public string ErrorDescription
		{
			get { return errorDescription; }
			set { errorDescription = value; }
		}

		public int ErrorNumber
		{
			get { return errorNumber; }
			set { errorNumber = value; }
		}

		public string PaReq
		{
			get { return paReq; }
			set { paReq = value; }
		}

		public string AcsUrl
		{
			get { return acsUrl; }
			set { acsUrl = value; }
		}

		public string CallBackUrl
		{
			get { return callBackUrl; }
			set { callBackUrl = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public NameValueCollection NameValueCollection
		{
			get { return nameValueCollection; }
			set { nameValueCollection = value; }
		}
		
		#endregion
	}
}
