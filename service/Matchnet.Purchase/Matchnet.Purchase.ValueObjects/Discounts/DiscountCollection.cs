using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	#region DiscountType
	
	public enum DiscountType : int
	{
		None = 0,
		PercentageDiscount = 1,
		MoneyDiscount = 2,
		AbsolutePrice = 3
	}

	#endregion

	[Serializable]
	public class DiscountCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members

		private int _cacheTTLSeconds = 3600; 
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
		
		#endregion

		#region Constructors

		public DiscountCollection()
		{
		}

		#endregion		
		
		#region CollectionBase Implementation

		public Discount this[int index]
		{
			get{return (Discount)base.InnerList[index];}
		}

		public int Add(Discount discount)
		{
			return base.InnerList.Add(discount);
		}

		public Discount FindByID(int discountID)
		{
			foreach (Discount discount in this)
			{
				if (discount.DiscountID == discountID)
				{
					return discount;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return _cacheTTLSeconds; }
			set { _cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return _cacheItemMode; }
			set { _cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return _cachePriority; }
			set { _cachePriority = value; }
		}

		public static string GetCacheKey(int siteID)
		{
			return "~DISCOUNTCOLLECTION^";
		}

		public string GetCacheKey()
		{
			return "~DISCOUNTCOLLECTION^";
		}

		#endregion
	}
}
