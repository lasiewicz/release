using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class Discount : IValueObject
	{
		#region Private Members

		private Int32 discountID = Constants.NULL_INT;
		private DiscountType discountType;
		private decimal amount = Constants.NULL_DECIMAL;
		private DurationType durationType;
		private string resourceConstant = Constants.NULL_STRING;
		private Int32 duration = Constants.NULL_INT;
		
		#endregion

		#region Constructors
		
		public Discount(
			int discountID,
			DiscountType discountType,
			decimal amount,
			DurationType durationType,
			string resourceConstant) : this (
			discountID,
			discountType,
			amount,
			durationType,
			resourceConstant,
			Constants.NULL_INT)
		{
		}

		public Discount(
			int discountID,
			DiscountType discountType,
			decimal amount,
			DurationType durationType,
			string resourceConstant,
			int duration)
		{
			this.discountID = discountID;
			this.discountType = discountType;
			this.amount = amount;
			this.durationType = durationType;
			this.resourceConstant = resourceConstant;
			this.duration = duration;
		}

		#endregion

		#region Properties

		public int DiscountID
		{
			get { return  discountID; }
			set { discountID = value; }
		}

		public DiscountType DiscountType
		{
			get { return discountType; }
			set { discountType = value; }
		}

		public decimal Amount
		{
			get { return amount; }
			set { amount = value; }
		}

		public DurationType DurationType
		{
			get { return durationType; }
			set { durationType = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public int Duration 
		{
			get { return duration; }
			set { duration = value; }
		}

		#endregion
	}
}
