using System;

namespace Matchnet.Purchase.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_NAME = "Matchnet.Purchase.Service";
		public const string SERVICE_CONSTANT = "PURCHASE_SVC";
		public const string SERVICE_MANAGER_NAME_PURCHASE = "PurchaseSM";
		public const string SERVICE_MANAGER_NAME_PLAN = "PlanSM";
	}
}
