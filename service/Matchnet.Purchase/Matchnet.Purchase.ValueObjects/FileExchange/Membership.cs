using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for Membership.
	/// </summary>
	[Serializable]
	public class Membership : IValueObject
	{
		#region Private Members
		private string m_EncoreMembershipID = string.Empty;
		private string m_VendorVariableField = string.Empty;
		private DateTime m_EncoreMembershipStartDate = DateTime.MinValue;
		private DateTime m_EncoreMembershipEndDate = DateTime.MinValue;
		private string m_MailCode = string.Empty;
		private DateTime m_InsertDate = DateTime.MinValue;
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public string EncoreMembershipID
		{
			get { return m_EncoreMembershipID; }
			set { m_EncoreMembershipID = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string VendorVariableField
		{
			get { return m_VendorVariableField; }
			set { m_VendorVariableField = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime EncoreMembershipStartDate
		{
			get { return m_EncoreMembershipStartDate; }
			set { m_EncoreMembershipStartDate = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime EncoreMembershipEndDate
		{
			get { return m_EncoreMembershipEndDate; }
			set { m_EncoreMembershipEndDate = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string MailCode
		{
			get { return m_MailCode; }
			set { m_MailCode = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return m_InsertDate; }
			set { m_InsertDate = value; }
		}
		#endregion

		public Membership()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
