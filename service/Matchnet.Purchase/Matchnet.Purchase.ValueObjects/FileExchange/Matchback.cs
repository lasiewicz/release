using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for Matchback.
	/// </summary>
	[Serializable]
	public class Matchback : IValueObject			
	{
		#region Private Members
		private string m_ClientOrderID = string.Empty;
		private string m_Email = string.Empty;
		private string m_Zip = string.Empty;
		private string m_OrderDate = string.Empty;
		private string m_MailCode = string.Empty;
		private string m_Filler = string.Empty;
		private DateTime m_InsertDate =  DateTime.MinValue;
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public string ClientOrderID
		{
			get { return m_ClientOrderID; }
			set { m_ClientOrderID = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string Email
		{
			get { return m_Email; }
			set { m_Email = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string Zip
		{
			get { return m_Zip; }
			set { m_Zip = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string OrderDate
		{
			get { return m_OrderDate; }
			set { m_OrderDate = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string MailCode
		{
			get { return m_MailCode; }
			set { m_MailCode = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public string Filler
		{
			get { return m_Filler; }
			set { m_Filler = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return m_InsertDate; }
			set { m_InsertDate = value; }
		}
		#endregion

		public Matchback()
		{
			//
			// TODO: Add constructor logic here
			//
		}


	}
}
