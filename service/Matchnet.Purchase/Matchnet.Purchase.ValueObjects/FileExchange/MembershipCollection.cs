using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for MembershipCollection.
	/// </summary>
	[Serializable]
	public class MembershipCollection : CollectionBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public MembershipCollection()
		{
		}
		/// <summary>
		/// 
		/// </summary>
		public Membership this[int index]
		{
			get { return (Membership)base.InnerList[index]; }
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="membership"></param>
		/// <returns></returns>
		public int Add(Membership membership)
		{
			return base.InnerList.Add(membership);
		}
	}
}
