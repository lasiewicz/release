using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for MatchbackCollection.
	/// </summary>
	[Serializable]
	public class MatchbackCollection : CollectionBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public MatchbackCollection()
		{
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchback this [int index]
		{
			get { return (Matchback)base.InnerList[index]; }
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="matchback"></param>
		/// <returns></returns>
		public int Add(Matchback matchback)
		{
			return base.InnerList.Add(matchback);
		}
	}
}
