using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class TransactionReason : IValueObject
	{
		#region Private Members

		private int transactionReasonID = Constants.NULL_INT;
		private TransactionReasonType transactionReasonType ;
		private string description = string.Empty;
		private string resourceConstant = Constants.NULL_STRING;
		private int listOrder = Constants.NULL_INT;
		
		#endregion
		
		#region Constructors
		
		public TransactionReason(
			int transactionReasonID,
			TransactionReasonType transactionReasonType,
			string description,
			string resourceConstant,
			int listOrder)
		{
			this.transactionReasonID = transactionReasonID;
			this.transactionReasonType = transactionReasonType;
			this.description = description;
			this.resourceConstant = resourceConstant;
			this.listOrder = listOrder;
		}

		#endregion

		#region Properties

		public int TransactionReasonID
		{
			get { return transactionReasonID; }
			set { transactionReasonID = value; }
		}

		public TransactionReasonType TransactionReasonType
		{
			get { return transactionReasonType; }
			set { transactionReasonType = value; }
		}

		public string Description
		{ 
			get { return description; }
			set { description = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public int ListOrder
		{
			get { return listOrder; }
			set { listOrder = value; }
		}
		
		#endregion
	}
}
