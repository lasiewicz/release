using System;
using System.Collections;
using Matchnet;
//using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Purchase.ValueObjects
{
	
	#region Transaction Reason Types

	public enum TransactionReasonType : int
	{
		All = 0,
		InternalUse = 1,
		GoodReason = 2,
		BadReason = 3,
		NeedsHelp = 4,
		ConsumerIssue = 5,
		ProductIssue = 6
	};
	
	#endregion
	
	[Serializable]
	public class TransactionReasonCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members

		//private int cacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.SerivceAdapters.RuntimeSettings.GetSetting("SUBSCRIPTIONSVC_CACHE_TTL_TRANSACTIONREASONS"));
		private int cacheTTLSeconds = 3600;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Sliding;

		#endregion

		#region Constructors

		public TransactionReasonCollection()
		{
		}

		#endregion

		#region CollectionBase Implementation

		public TransactionReason this[int index]
		{
			get { return (TransactionReason)base.InnerList[index]; }
			
		}

		public int Add(TransactionReason transactionReason)
		{
			return base.InnerList.Add(transactionReason);
		}

		public TransactionReason FindByID(int transactionReasonID)
		{
			foreach(TransactionReason transactionReason in this)
			{
				if(transactionReason.TransactionReasonID  == transactionReasonID)
				{
					return transactionReason;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		public static string GetCacheKey(TransactionReasonType transactionReasonType)
		{
			return "~TRANSACTIONREASONCOLLECTION^";
		}

		public string GetCacheKey()
		{
			return "~TRANSACTIONREASONCOLLECTION^";
		}

		#endregion
	}
}
