using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class Property : IValueObject, ISerializable
	{
		private string name;
		private string value;

		public Property()
		{
		}

		public Property(
			SerializationInfo info,
			StreamingContext c)
		{
			name = info.GetString("name");
			value = info.GetString("value");
		}

		public Property(
			string name, 
			string value) 
		{
			this.name = name;
			this.value = value;
		}

		public string Name 
		{
			get { return name; }
		}

		public string Value 
		{
			get { return value; }
		}
		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("name", name);
			info.AddValue("value", value);
		}

		#endregion
	}
}
