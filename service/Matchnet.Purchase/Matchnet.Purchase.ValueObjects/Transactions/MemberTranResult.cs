using System;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class MemberTranResult
	{
		private MemberTran _memberTran;
		private string _resourceConstant;
		private MemberTranStatus _memberTranStatus;
		private string _responseCode;
		private string _responseCodeAVS;
		private string _responseCodeCVV;
		private Int32 _providerID;

		//09252008 TL MPR-539, Adding response/request data fields to save to new ChargeLogXML in mnChargeStage
		private string _requestDataScrubbed = "";
		private string _responseData = "";
		private bool _logRequestResponseData = false;


		public MemberTranResult(MemberTran memberTran,
			string resourceConstant,
			MemberTranStatus memberTranStatus,
			string responseCode,
			string responseCodeAVS,
			string responseCodeCVV,
			Int32 providerID)
		{
			_memberTran = memberTran;
			_resourceConstant = resourceConstant;
			_memberTranStatus = memberTranStatus;
			_responseCode = responseCode;
			_responseCodeAVS = responseCodeAVS;
			_responseCodeCVV = responseCodeCVV;
			_providerID = providerID;
		}

		public MemberTranResult(MemberTran memberTran,
			string resourceConstant,
			MemberTranStatus memberTranStatus,
			string responseCode,
			string responseCodeAVS,
			string responseCodeCVV,
			Int32 providerID,
			string responseData,
			string requestData)
		{
			_memberTran = memberTran;
			_resourceConstant = resourceConstant;
			_memberTranStatus = memberTranStatus;
			_responseCode = responseCode;
			_responseCodeAVS = responseCodeAVS;
			_responseCodeCVV = responseCodeCVV;
			_providerID = providerID;
			_responseData = responseData;
			_requestDataScrubbed = requestData;
		}


		public MemberTran MemberTran
		{
			get
			{
				return _memberTran;
			}
		}


		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
		}


		public MemberTranStatus MemberTranStatus
		{
			get
			{
				return _memberTranStatus;
			}
		}


		public string ResponseCode
		{
			get
			{
				return _responseCode;
			}
		}


		public string ResponseCodeAVS
		{
			get
			{
				return _responseCodeAVS;
			}
		}


		public string ResponseCodeCVV
		{
			get
			{
				return _responseCodeCVV;
			}
		}


		public Int32 ProviderID
		{
			get
			{
				return _providerID;
			}
		}

		/// <summary>
		/// Request data sent to provider, this should already be scrubbed of all sensitive information (e.g. cc, cvv, pw)
		/// </summary>
		public string RequestDataScrubbed
		{
			get {return _requestDataScrubbed;}
		}

		/// <summary>
		/// Response data returned from provider
		/// </summary>
		public string ResponseData
		{
			get {return _responseData;}
		}

		/// <summary>
		/// Indicates whether there is a need to log request/response data due to an exception.
		/// This is currently used to determine whether to send request/response data when generating
		/// a response to send to Purchase, in order to also log request/response data in mnChargeStage.
		/// </summary>
		public bool logRequestResponseData
		{
			get
			{
				return _logRequestResponseData;
			}
			set
			{
				_logRequestResponseData = value;
			}
		}
	}
}
