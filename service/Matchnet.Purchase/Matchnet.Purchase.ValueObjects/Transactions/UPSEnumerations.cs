using System;

namespace Matchnet.Purchase.ValueObjects
{
	#region Original Purchase Action Type For UPS

	/// <summary>
	///	The call that would have been made to the existing purchase service before calling UPS    
	/// </summary>
	public enum OriginalPurchaseActionTypeForUPS : int
	{
		None = 0,
		BeginCreditCardPurchase = 1,
		BeginCheckPurchase = 2,
		BeginCredit = 3,
		BeginAuthPmtProfile = 4,
		BeginRenewal = 5,
		BeginPaypalPurchase = 6,
		BeginUpdateSubscription = 7
	};
	
	#endregion

}
