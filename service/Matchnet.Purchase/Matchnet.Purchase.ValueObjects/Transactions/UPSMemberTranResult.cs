using System;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for UPSMemberTranResult.
	/// </summary>
	public class UPSMemberTranResult
	{
		private UPSMemberTran _upsMemberTran;
		private int _commonResponseCode = Constants.NULL_INT;
		private MemberTranStatus _memberTranStatus;
		private Int32 _providerID;
		private Int32 _orderID;
		private Int32 _chargeID;

		public UPSMemberTran UPSMemberTran
		{
			get { return this._upsMemberTran;}
			set { this._upsMemberTran = value;}
		}

		public int CommonResponseCode
		{
			get { return this._commonResponseCode;}
			set { this._commonResponseCode = value;}
		}

		public MemberTranStatus MemberTranStatus
		{
			get { return this._memberTranStatus;}
			set { this._memberTranStatus = value;}
		}

		public int ProviderID
		{
			get { return this._providerID;}
			set { this._providerID = value;}
		}

		public int OrderID
		{
			get { return this._orderID;}
			set { this._orderID = value;}
		}

		public int ChargeID
		{
			get { return this._chargeID;}
			set { this._chargeID = value;}
		}

		public UPSMemberTranResult()
		{

		}

		public UPSMemberTranResult(UPSMemberTran upsMemberTran, int commonResponseCode, MemberTranStatus memberTranStatus, int providerID, int orderID, int chargeID)
		{
			this._upsMemberTran = upsMemberTran;
			this._commonResponseCode = commonResponseCode;
			this._memberTranStatus = memberTranStatus;
			this._providerID = providerID;
			this._orderID = orderID;
			this._chargeID = chargeID;
		}
	}
}
