using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Xml.Serialization;

using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class MemberTran : IValueObject
	{
		#region Private Members

		private int memberTranID = Constants.NULL_INT;
		private int chargeID = Constants.NULL_INT;
		private int referenceMemberTranID = Constants.NULL_INT;
		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		private CurrencyType currencyType;
		private string resourceConstant = Constants.NULL_STRING;
		private TranType tranType;
		private string tranTypeResourceConstant = Constants.NULL_STRING;
		private int memberSubID = Constants.NULL_INT;
		private int planID = Constants.NULL_INT;
		private Plan plan = null;
		private string planResourceConstant = Constants.NULL_STRING;
		private int discountID = Constants.NULL_INT;
		private int memberPaymentID = Constants.NULL_INT;
		private Payment payment = null;
		private string paymentTypeDescription = string.Empty;
		private string paymentTypeResourceConstant = Constants.NULL_STRING;
		private MemberTranStatus memberTranStatus = MemberTranStatus.None;
		private string memberTranStatusDescription = string.Empty;
		private string memberTranStatusResourceConstant = Constants.NULL_STRING;
		private int adminMemberID = Constants.NULL_INT;
		private decimal amount = Constants.NULL_DECIMAL;
		private int duration = Constants.NULL_INT;
		private DurationType durationType;
		private string durationTypeResourceConstant = Constants.NULL_STRING;
		private int transactionReasonID = Constants.NULL_INT;
		private DateTime insertDate = DateTime.MinValue;
		private DateTime updateDate = DateTime.MinValue;
		private string accountNumber = string.Empty;
		private string discountResourceConstant = Constants.NULL_STRING;
		private PaymentType paymentType;
		private SiteMerchant siteMerchant = null;
		private int minutes = Constants.NULL_INT;
		private bool savePayment = true;
		private PaymentVerificationRequest paReq = null;
		private PaymentVerificationResponse paRes = null;
		private RequestType requestType = RequestType.None;
		private byte _attempts = 0;
		private bool _ignoreCVV = false;
		private DateTime startDate = DateTime.MinValue;
		private DateTime endDate = DateTime.MinValue;
		private PurchaseMode purchaseMode = PurchaseMode.None;
		private bool usedToCreditPurchase = false;
		private decimal creditAmount = Constants.NULL_DECIMAL; //represents the total credit amount to apply; only has a value during processing a purchase
		private int promoID = Constants.NULL_INT; 
		private TimeAdjustOverrideType timeAdjustOverrideType = TimeAdjustOverrideType.None;
		private bool reusePreviousPayment = false;
		private string planIDList = Constants.NULL_STRING;
		private string legacyPaymentTypeResourceConstant = Constants.NULL_STRING;
		private string legacyLastFourAccountNumber = Constants.NULL_STRING;
		private CreditCardType creditCardType;
		private int _OrderID = Constants.NULL_INT;

		private MemberTranCreditCollection memberTranCreditCollection = null;

		#endregion
		
		#region Constructors
		
		public MemberTran()
		{
		}

		public MemberTran(
			int memberTranID)
		{
			this.memberTranID = memberTranID;
		}

		public MemberTran(
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant) : this(
			Constants.NULL_INT,
			Constants.NULL_INT,
			referenceMemberTranID,
			memberID,
			siteID,
			currencyType,
			resourceConstant,
			tranType,
			tranTypeResourceConstant,
			memberSubID,
			planID,
			null,
			planResourceConstant,
			discountID,
			memberPaymentID,
			null,
			paymentTypeDescription,
			paymentTypeResourceConstant,
			memberTranStatus,
			memberTranStatusDescription,
			memberTranStatusResourceConstant,
			adminMemberID,
			amount,
			duration,
			durationType,
			durationTypeResourceConstant,
			transactionReasonID,
			insertDate,
			updateDate,
			accountNumber,
			discountResourceConstant,
			PaymentType.None,
			null,
			Constants.NULL_INT,
			DateTime.MinValue,
			DateTime.MinValue,
			false,
			PurchaseMode.None,
			Constants.NULL_INT,
			TimeAdjustOverrideType.None,
			false)
		{
		}

		public MemberTran(
			int memberTranID,
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant) : this(
			memberTranID,
			Constants.NULL_INT,
			referenceMemberTranID,
			memberID,
			siteID,
			currencyType,
			resourceConstant,
			tranType,
			tranTypeResourceConstant,
			memberSubID,
			planID,
			null,
			planResourceConstant,
			discountID,
			memberPaymentID,
			null,
			paymentTypeDescription,
			paymentTypeResourceConstant,
			memberTranStatus,
			memberTranStatusDescription,
			memberTranStatusResourceConstant,
			adminMemberID,
			amount,
			duration,
			durationType,
			durationTypeResourceConstant,
			transactionReasonID,
			insertDate,
			updateDate,
			accountNumber,
			discountResourceConstant,
			PaymentType.None,
			null,
			Constants.NULL_INT,
			DateTime.MinValue,
			DateTime.MinValue,
			false,
			PurchaseMode.None,
			Constants.NULL_INT,
			TimeAdjustOverrideType.None,			 
			false
			)
		{
		}

		public MemberTran(
			int memberTranID,
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant,
			DateTime startDate,
			DateTime endDate,
			bool usedToCreditPurchase,
			PurchaseMode purchaseMode) : this(
			memberTranID,
			Constants.NULL_INT,
			referenceMemberTranID,
			memberID,
			siteID,
			currencyType,
			resourceConstant,
			tranType,
			tranTypeResourceConstant,
			memberSubID,
			planID,
			null,
			planResourceConstant,
			discountID,
			memberPaymentID,
			null,
			paymentTypeDescription,
			paymentTypeResourceConstant,
			memberTranStatus,
			memberTranStatusDescription,
			memberTranStatusResourceConstant,
			adminMemberID,
			amount,
			duration,
			durationType,
			durationTypeResourceConstant,
			transactionReasonID,
			insertDate,
			updateDate,
			accountNumber,
			discountResourceConstant,
			PaymentType.None,
			null,
			Constants.NULL_INT,
			startDate,
			endDate,
			usedToCreditPurchase,
			purchaseMode,
			Constants.NULL_INT,
			TimeAdjustOverrideType.None,
			false)
		{
		}

		public MemberTran(
			int memberTranID,
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant,
			DateTime startDate,
			DateTime endDate,
			bool usedToCreditPurchase,
			PurchaseMode purchaseMode,
			int methodPromoID,
			TimeAdjustOverrideType timeAdjustOverrideType) : this(
			memberTranID,
			Constants.NULL_INT,
			referenceMemberTranID,
			memberID,
			siteID,
			currencyType,
			resourceConstant,
			tranType,
			tranTypeResourceConstant,
			memberSubID,
			planID,
			null,
			planResourceConstant,
			discountID,
			memberPaymentID,
			null,
			paymentTypeDescription,
			paymentTypeResourceConstant,
			memberTranStatus,
			memberTranStatusDescription,
			memberTranStatusResourceConstant,
			adminMemberID,
			amount,
			duration,
			durationType,
			durationTypeResourceConstant,
			transactionReasonID,
			insertDate,
			updateDate,
			accountNumber,
			discountResourceConstant,
			PaymentType.None,
			null,
			Constants.NULL_INT,
			startDate,
			endDate,
			usedToCreditPurchase,
			purchaseMode,
			methodPromoID,
			timeAdjustOverrideType,
			false)
		{
		}

		public MemberTran(
			int memberTranID,
			int chargeID,
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			Plan plan,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			Payment payment,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant,
			PaymentType paymentType,
			SiteMerchant siteMerchant,
			int minutes)
			: this(
			memberTranID,
			chargeID,
			referenceMemberTranID,
			memberID,
			siteID,
			currencyType,
			resourceConstant,
			tranType,
			tranTypeResourceConstant,
			memberSubID,
			planID,
			plan,
			planResourceConstant,
			discountID,
			memberPaymentID,
			payment,
			paymentTypeDescription,
			paymentTypeResourceConstant,
			memberTranStatus,
			memberTranStatusDescription,
			memberTranStatusResourceConstant,
			adminMemberID,
			amount,
			duration,
			durationType,
			durationTypeResourceConstant,
			transactionReasonID,
			insertDate,
			updateDate,
			accountNumber,
			discountResourceConstant,
			paymentType,
			siteMerchant,
			minutes,
			DateTime.MinValue,
			DateTime.MinValue,
			false,
			PurchaseMode.None,
			Constants.NULL_INT,
			TimeAdjustOverrideType.None,
			false)
		{

		}
	
		public MemberTran(
			int memberTranID,
			int chargeID,
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			Plan plan,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			Payment payment,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant,
			PaymentType paymentType,
			SiteMerchant siteMerchant,
			int minutes,
			int methodPromoID)
			: this(
			memberTranID,
			chargeID,
			referenceMemberTranID,
			memberID,
			siteID,
			currencyType,
			resourceConstant,
			tranType,
			tranTypeResourceConstant,
			memberSubID,
			planID,
			plan,
			planResourceConstant,
			discountID,
			memberPaymentID,
			payment,
			paymentTypeDescription,
			paymentTypeResourceConstant,
			memberTranStatus,
			memberTranStatusDescription,
			memberTranStatusResourceConstant,
			adminMemberID,
			amount,
			duration,
			durationType,
			durationTypeResourceConstant,
			transactionReasonID,
			insertDate,
			updateDate,
			accountNumber,
			discountResourceConstant,
			paymentType,
			siteMerchant,
			minutes,
			DateTime.MinValue,
			DateTime.MinValue,
			false,
			PurchaseMode.None,
			methodPromoID,
			TimeAdjustOverrideType.None,
			false)
		{

		}

		public MemberTran(
			int memberTranID,
			int chargeID,
			int referenceMemberTranID,
			int memberID,
			int siteID,
			CurrencyType currencyType,
			string resourceConstant,
			TranType tranType,
			string tranTypeResourceConstant,
			int memberSubID,
			int planID,
			Plan plan,
			string planResourceConstant,
			int discountID,
			int memberPaymentID,
			Payment payment,
			string paymentTypeDescription,
			string paymentTypeResourceConstant,
			MemberTranStatus memberTranStatus,
			string memberTranStatusDescription,
			string memberTranStatusResourceConstant,
			int adminMemberID,
			decimal amount,
			int duration,
			DurationType durationType,
			string durationTypeResourceConstant,
			int transactionReasonID,
			DateTime insertDate,
			DateTime updateDate,
			string accountNumber,
			string discountResourceConstant,
			PaymentType paymentType,
			SiteMerchant siteMerchant,
			int minutes,
			DateTime startDate,
			DateTime endDate,
			bool usedToCreditPurchase,
			PurchaseMode purchaseMode,
			int methodPromoID,
			TimeAdjustOverrideType timeAdjustOverrideType,
			bool reusePreviousPayment)
		{
			this.memberTranID = memberTranID;
			this.chargeID = chargeID;
			this.referenceMemberTranID = referenceMemberTranID;
			this.memberID = memberID;
			this.siteID = siteID;
			this.currencyType = currencyType;
			this.resourceConstant = resourceConstant;
			this.tranType = tranType;
			this.tranTypeResourceConstant = tranTypeResourceConstant;
			this.memberSubID = memberSubID;
			this.planID = planID;
			this.plan = plan;
			this.planResourceConstant = planResourceConstant;
			this.discountID = discountID;
			this.memberPaymentID = memberPaymentID;
			this.payment = payment;
			this.paymentTypeDescription = paymentTypeDescription;
			this.paymentTypeResourceConstant = paymentTypeResourceConstant;
			this.memberTranStatus = memberTranStatus;
			this.memberTranStatusDescription = memberTranStatusDescription;
			this.memberTranStatusResourceConstant = memberTranStatusResourceConstant;
			this.adminMemberID = adminMemberID;
			this.amount = amount;
			this.duration = duration;
			this.durationType = durationType;
			this.durationTypeResourceConstant = durationTypeResourceConstant;
			this.transactionReasonID = transactionReasonID;
			this.insertDate = insertDate;
			this.updateDate = updateDate;
			this.accountNumber = accountNumber;
			this.discountResourceConstant = discountResourceConstant;
			this.paymentType = paymentType;
			this.siteMerchant = siteMerchant;
			this.minutes = minutes;
			this.startDate = startDate;
			this.endDate = endDate;
			this.usedToCreditPurchase = usedToCreditPurchase;
			this.purchaseMode = purchaseMode;
			this.promoID = methodPromoID;
			this.timeAdjustOverrideType = timeAdjustOverrideType;
			this.reusePreviousPayment = reusePreviousPayment;
		}
		
		#endregion

		#region Properties
		
		public int MemberTranID
		{
			get { return memberTranID; }
			set { memberTranID = value; }
		}

		public int ChargeID
		{
			get { return chargeID; }
			set { chargeID = value; }
		}

		public int ReferenceMemberTranID
		{
			get { return referenceMemberTranID; }
			set { referenceMemberTranID = value; }
		}

		public int MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		public int SiteID
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public CurrencyType CurrencyType
		{
			get { return currencyType; }
			set { currencyType = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public TranType TranType
		{
			get { return tranType; }
			set { tranType = value; }
		}

		public string TranTypeResourceConstant
		{
			get { return tranTypeResourceConstant; }
			set { tranTypeResourceConstant = value; }
		}
		
		public int MemberSubID
		{
			get { return memberSubID; }
			set { memberSubID = value; }
		}

		public int PlanID
		{
			get { return planID; }
			set { planID = value; }
		}

		public Plan Plan
		{
			get { return plan; }
			set { plan = value; }
		}

		public string PlanResourceConstant
		{
			get { return planResourceConstant; }
			set { planResourceConstant = value; }
		}

		public int DiscountID
		{
			get { return discountID; }
			set { discountID = value; }
		}
		
		public int MemberPaymentID
		{
			get { return memberPaymentID; }
			set { memberPaymentID = value; }
		}

		public Payment Payment
		{
			get { return payment; }
			set { payment = value; }
		}

		public string PaymentTypeDescription
		{
			get { return paymentTypeDescription; }
			set { paymentTypeDescription = value; }
		}

		public string PaymentTypeResourceConstant
		{
			get { return paymentTypeResourceConstant; }
			set { paymentTypeResourceConstant = value; }
		}

		public MemberTranStatus MemberTranStatus
		{
			get { return memberTranStatus; }
			set { memberTranStatus = value; }
		}
		
		public string MemberTranStatusDescription
		{
			get { return memberTranStatusDescription; }
			set { memberTranStatusDescription = value; }
		}

		public string MemberTranStatusResourceConstant
		{
			get { return memberTranStatusResourceConstant; }
			set { memberTranStatusResourceConstant = value; }
		}

		public int AdminMemberID
		{
			get { return adminMemberID; }
			set { adminMemberID = value; }
		}

		public decimal Amount
		{
			get { return amount; }
			set { amount = value; }
		}

		public int Duration
		{
			get { return duration; }
			set { duration = value; }
		}

		public DurationType DurationType 
		{
			get { return durationType; }
			set { durationType = value; }
		}
		
		public string DurationTypeResourceConstant
		{
			get { return durationTypeResourceConstant; }
			set { durationTypeResourceConstant = value; }
		}

		public int TransactionReasonID
		{
			get { return transactionReasonID; }
			set { transactionReasonID = value; }
		}

		public DateTime InsertDate
		{
			get { return insertDate; }
			set { insertDate = value; }
		}

		public DateTime UpdateDate
		{
			get { return updateDate; }
			set { updateDate = value; }
		}

		public string AccountNumber
		{
			get { return accountNumber; }
			set { accountNumber = value; }
		}

		public string DiscountResourceConstant
		{
			get { return discountResourceConstant; }
			set { discountResourceConstant = value; }
		}

		public PaymentType PaymentType
		{
			get { return paymentType; }
			set { paymentType = value; }
		}

		public SiteMerchant SiteMerchant
		{
			get { return siteMerchant; }
			set { siteMerchant = value; }
		}

		public int Minutes
		{
			get { return minutes; }
			set { minutes = value; }
		}

		public bool SavePayment
		{
			get { return savePayment; }
			set { savePayment = value; }
		}

		public PaymentVerificationRequest PaReq
		{
			get { return paReq; }
			set { paReq = value; }
		}

		public PaymentVerificationResponse PaRes
		{
			get { return paRes; }
			set { paRes = value; }
		}

		public RequestType RequestType
		{
			get { return requestType; }
			set { requestType = value; }
		}

		public byte Attempts
		{
			get
			{
				return _attempts;
			}
			set
			{
				_attempts = value;
			}
		}


		public bool IgnoreCVV
		{
			get
			{
				return _ignoreCVV;
			}
			set
			{
				_ignoreCVV = value;
			}
		}

		public DateTime StartDate
		{
			get { return this.startDate; }
			set { this.startDate = value; }
		}

		public DateTime EndDate
		{
			get { return this.endDate; }
			set { this.endDate = value; }
		}

		public PurchaseMode PurchaseMode
		{
			get { return this.purchaseMode; }
			set { this.purchaseMode = value; }
		}

		public bool UsedToCreditPurchase
		{
			get { return this.usedToCreditPurchase; }
			set { this.usedToCreditPurchase = value; }
		}

		public int PromoID
		{
			get { return this.promoID; }
			set { this.promoID = value; }
		}

		public TimeAdjustOverrideType TimeAdjustOverrideType
		{
			get { return this.timeAdjustOverrideType; }
			set { this.timeAdjustOverrideType = value; }
		}

		public MemberTranCreditCollection MemberTranCreditCollection
		{
			get { return this.memberTranCreditCollection; }
			set { this.memberTranCreditCollection = value; }
		}

		/// <summary>
		/// Represents the total credit amount to apply; only has a value during processing a purchase
		/// </summary>
		public decimal CreditAmount
		{
			get { return this.creditAmount;}
			set { this.creditAmount = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool ReusePreviousPayment
		{
			get { return this.reusePreviousPayment;}
			set { this.reusePreviousPayment = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string PlanIDList
		{
			get { return this.planIDList;}
			set { this.planIDList = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string LegacyPaymentTypeResourceConstant
		{
			get { return this.legacyPaymentTypeResourceConstant;}
			set { this.legacyPaymentTypeResourceConstant = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string LegacyLastFourAccountNumber
		{
			get { return this.legacyLastFourAccountNumber;}
			set { this.legacyLastFourAccountNumber = value;}
		}

		public CreditCardType CreditCardType
		{
			get
			{
				return this.creditCardType;
			}
			set
			{
				this.creditCardType = value;
			}
		}

		public int OrderID
		{
			get { return this._OrderID;}
			set { this._OrderID = value;}
		}

		#endregion

		#region Static methods
		/// <summary>
		/// This function determines what the start date will be for a transaction (to be passed into the creation of a transaction)
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="memberSub"></param>
		/// <returns></returns>
		public static DateTime DetermineTransactionStartDate(MemberTran mt, DateTime nowDate, MemberSub memberSub)
		{
			DateTime startDate = nowDate; //Default: reset start date to be today

			if (memberSub != null) //null memberSub indicates a new subscriber with no subscription
			{
				if (mt.plan != null
					&& (mt.plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
				{
					// Primary plan in the MemberTran is an ala carte item
					// Ala carte items always begin now  
					startDate = nowDate;
				}
				else if (mt.TranType == TranType.InitialBuy || mt.TranType == TranType.Renewal)
				{
					//determine if startDate needs to begin at the end of their subscription(their existing renew date)
					if (memberSub.RenewDate > DateTime.Now && !Plan.IsPurchaseModeValidForCredit(mt.PurchaseMode))
					{
						//start date should be at the end of current subscription(s), which is the renew date.
						startDate = memberSub.RenewDate;
					}
				}
				else if (mt.TranType == TranType.AdministrativeAdjustment)
				{
					//a. Credit transactions where the sub priviledge end date is in the past
					//c. Credit transactions where there is no time adjustments made
					//Do Nothing, start date is the default.

					//b. Credit transactions where the sub priviledge end date is in the future with positive/negative time duration
					if (memberSub.RenewDate > DateTime.Now)
					{
						//only if admin specifies a time duration
						if (mt.Duration != Constants.NULL_INT)
						{
							startDate = memberSub.RenewDate;
						}
					}
				}
			}

			return startDate;
		}

		/// <summary>
		/// This function determines what the end date will be for a transaction
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="mt"></param>
		public static DateTime DetermineTransactionEndDate(DateTime startDate, MemberTran mt, MemberSub memberSub)
		{
			DateTime endDate = startDate; //Default: end date is the same as start date

			if (mt.plan != null
				&& (mt.plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
			{
				// Primary plan in the MemberTran is an ala carte item
				// Ala carte items always ends until the end of the base subscription date  
				// Cannot just renew an ala carte item without renewing a base subscription so the 
				// primary plan will be a plan that includes a base subscription. 
				endDate = memberSub.RenewDate;
				//endDate = MemberTran.AddDurationToDate(mt, startDate, false);
			}
			else if (mt.TranType == TranType.InitialBuy || mt.TranType == TranType.Renewal)
			{
				endDate = MemberTran.AddDurationToDate(mt, startDate, false);
			}
			else if (mt.TranType == TranType.AdministrativeAdjustment)
			{
				//Credit transactions with positive/negative time duration
				if (mt.Duration != Constants.NULL_INT)
				{
					endDate = MemberTran.AddDurationToDate(mt, startDate, true);
				}

			}

			return endDate;

		}

		/// <summary>
		/// Determines a date based on addition of duration
		/// </summary>
		/// <param name="memberTran"></param>
		/// <param name="dteInitialExpDate"></param>
		/// <param name="renewalMode"></param>
		/// <param name="AbsoluteDuration">Indicates whether to always treat duration as a positive number</param>
		/// <returns></returns>
		public static DateTime AddDurationToDate(MemberTran memberTran,DateTime dteInitialExpDate, bool absoluteDuration)
		{
			DateTime dteExpirationDate = dteInitialExpDate;
		
			DurationType durationType = (memberTran.TranType == TranType.Renewal) ? memberTran.Plan.RenewDurationType : memberTran.DurationType;
			int durationToAdd = (memberTran.TranType == TranType.Renewal) ? memberTran.Plan.RenewDuration : memberTran.Duration;
			
			if (durationToAdd != Constants.NULL_INT)
			{
				if (absoluteDuration)
				{
					durationToAdd = Math.Abs(durationToAdd);
				}

				switch (durationType)
				{
					case DurationType.Minute: 
						dteExpirationDate = dteInitialExpDate.AddMinutes(durationToAdd);
						break;
					case DurationType.Hour: 
						dteExpirationDate = dteInitialExpDate.AddHours(durationToAdd);
						break;
					case DurationType.Day: 
						dteExpirationDate = dteInitialExpDate.AddDays(durationToAdd);
						break;
					case DurationType.Week: 
						dteExpirationDate = dteInitialExpDate.AddDays(durationToAdd * 7);
						break;
					case DurationType.Month: 
						dteExpirationDate = dteInitialExpDate.AddMonths(durationToAdd);
						break;
					case DurationType.Year: 
						dteExpirationDate = dteInitialExpDate.AddYears(durationToAdd);
						break;
				}
			}

			return dteExpirationDate;
		}
		#endregion
	}
}
