using System;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for UPSMemberTranInfo.
	/// </summary>
	public class UPSMemberTranInfo
	{
		public int _siteID = Constants.NULL_INT;
		public int _memberTranID = Constants.NULL_INT;
		public int _memberPaymentID = Constants.NULL_INT;
		public int _planID = Constants.NULL_INT;
		public PaymentType _paymentType = PaymentType.None;
		
		public UPSMemberTranInfo()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public UPSMemberTranInfo(
			int siteID,
			int memberTranID,
			int memberPaymentID,
			int planID,
			PaymentType paymentType)
		{
			_siteID = siteID;
			_memberTranID = memberTranID;
			_memberPaymentID = memberPaymentID;
			_planID = planID;
			_paymentType = paymentType; 
		}

		public int SiteID
		{
			get { return _siteID;}
			set { _siteID = value;}
		}

		public int MemberTranID
		{
			get { return _memberTranID;}
			set { _memberTranID = value;}
		}

		public int MemberPaymentID
		{
			get { return _memberPaymentID;}
			set { _memberPaymentID = value;}
		}

		public int PlanID
		{
			get { return _planID;}
			set { _planID = value;}
		}

		public PaymentType PaymentTypeUsed
		{
			get { return _paymentType;}
			set { _paymentType = value;}
		}
	}
}
