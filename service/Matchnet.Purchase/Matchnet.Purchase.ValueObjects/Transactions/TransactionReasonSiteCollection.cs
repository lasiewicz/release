using System;
using System.Collections;
using Matchnet;
//using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class TransactionReasonSiteCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members

		//private int cacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.SerivceAdapters.RuntimeSettings.GetSetting("SUBSCRIPTIONSVC_CACHE_TTL_TRANSACTIONREASONS"));
		private int cacheTTLSeconds = 3600;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Sliding;
		private int siteID = Constants.NULL_INT;

		#endregion

		#region Constructors

		public TransactionReasonSiteCollection(int siteID)
		{
			this.siteID = siteID;
		}

		#endregion

		#region CollectionBase Implementation

		public TransactionReason this[int index]
		{
			get { return (TransactionReason)base.InnerList[index]; }
			
		}

		public int Add(TransactionReason transactionReason)
		{
			return base.InnerList.Add(transactionReason);
		}

		public TransactionReason FindByID(int transactionReasonID)
		{
			foreach(TransactionReason transactionReason in this)
			{
				if(transactionReason.TransactionReasonID  == transactionReasonID)
				{
					return transactionReason;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		public static string GetCacheKey(int siteID)
		{
			return "~TRANSACTIONREASONSITECOLLECTION^" + siteID.ToString();
		}

		public string GetCacheKey()
		{
			return "~TRANSACTIONREASONSITECOLLECTION^" + siteID.ToString();
		}

		#endregion
	}
}
