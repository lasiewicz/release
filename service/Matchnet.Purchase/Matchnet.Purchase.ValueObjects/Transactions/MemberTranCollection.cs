using System;
using System.Collections;

namespace Matchnet.Purchase.ValueObjects
{
	#region MemberTranStatus

	public enum MemberTranStatus : int
	{ 
		None = 0,
		Pending = 1, 
		Success = 2, 
		Failure = 3,
		AwaitingApproval = 4,
		VerifiedByVisaPending = 5,
		VerificationPending = 6,
		AuthenticationPending = 7
	};

	#endregion

	#region TranType

	public enum TranType : int
	{
		InitialBuy = 1,
		Renewal = 2,
		Termination = 3,
		AdministrativeAdjustment = 4,
		ReceivedCheck = 5,
		AuthorizationOnly = 6,
		Discount = 7,
		Void = 8,
		AuthorizationNoTran = 9,
		AuthPmtProfile = 10,
		TrialPayAdjustment = 12,
		ReopenAutoRenewal = 18
	}

	#endregion

	#region RequestType

	public enum RequestType : int
	{
		None = 0,
		VerificationRequest = 1,
		VerificationResponse = 2,
		ProviderRequest = 3
	}

	#endregion

	#region Purchase Mode

	/// <summary>
	///	Based on the comparison of what the member has to what is being purchased  
	/// </summary>
	public enum PurchaseMode : int
	{
		None = 0,
		New = 1,
		Upgrade = 2,
		Downgrade = 3,
		Extension = 4,
		Swap = 5
	};
	
	#endregion

	#region Admin Time Adjustment Type Mask

	/// <summary>
	///	Specifies which adjustments should be made regardless of the plan type  
	/// </summary>
	[Flags]
	public enum TimeAdjustOverrideType : int
	{
		None = 0,
		Standard = 1,
		HighlightedProfile = 2,
		SpotlightMember = 4,
		JMeter = 8,
        AllAccess = 16,
        AllAccessEmail = 32,
        ReadReceipt = 64
	}

	#endregion

	#region AutoRenewal Type
	/// <summary>
	/// Specifies which subscription type is targeted for auto-renewal transaction
	/// This is needed to support the ability to turn off/on auto-renewal for a la carte only items.
	/// </summary>
	[Flags]
	public enum AutoRenewalType : int
	{
		None = 0,
		BaseSubsubscription = 1,
		HighlightedProfile = 2,
		SpotlightMember = 4
	}
	#endregion
    
	[Serializable]
	public sealed class MemberTranCollection : CollectionBase, IValueObject
	{
		#region Private Members

		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		
		#endregion

		#region Constructors

		public MemberTranCollection(int memberID, int siteID)
		{
			this.memberID = memberID;
			this.siteID = siteID;
		}

		#endregion

		#region CollectionBase Implementation

		public MemberTran this[int index]
		{
			get{return (MemberTran)base.InnerList[index];}
		}

		public int Add(MemberTran memberTran)
		{
			return base.InnerList.Add(memberTran);
		}

		public MemberTran FindByID(int memberTranID)
		{
			foreach(MemberTran memberTran in this)
			{
				if(memberTran.MemberTranID == memberTranID)
				{
					return memberTran;
				}
			}
			return null;
		}

		#endregion
	}
}

