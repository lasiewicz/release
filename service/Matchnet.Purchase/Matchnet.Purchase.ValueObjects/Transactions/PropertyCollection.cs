using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class PropertyCollection : IValueObject
	{
		#region Private Members

		private ArrayList properties = new ArrayList();

		#endregion

		#region Constructors
		
		public PropertyCollection()
		{
		}

		#endregion


		public void Add(Property property)
		{
			properties.Add( property );
		}

		public ArrayList Properties
		{
			get { return properties; }
			set { properties = value; }
		}
	}
}
