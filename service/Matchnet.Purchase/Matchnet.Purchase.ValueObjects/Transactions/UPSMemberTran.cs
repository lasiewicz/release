using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// A wrapper around MemberTran to add additional behaviors for UPS Integration
	/// </summary>
	[Serializable]
	public class UPSMemberTran : IValueObject
	{
		private MemberTran _MemberTran;
		private int[] _PackageID = null;
		private string _PaymentProfileID = ""; //this is actually a guid
		private int _RegionID = Constants.NULL_INT;
		private int _BrandID = Constants.NULL_INT;
		private DateTime _OriginalInsertDate = DateTime.MinValue;

		public MemberTran MemberTran
		{
			get { return _MemberTran;}
			set { _MemberTran = value;}
		}

		public int[] PackageID
		{
			get { return _PackageID;}
			set { _PackageID = value;}
		}

		public string PaymentProfileID
		{
			get { return _PaymentProfileID;}
			set { _PaymentProfileID = value;}
		}

		public int RegionID
		{
			get { return _RegionID;}
			set { _RegionID = value;}
		}

		public int BrandID
		{
			get { return _BrandID;}
			set { _BrandID = value;}
		}

		public DateTime OriginalInsertDate
		{
			get { return _OriginalInsertDate;}
			set { _OriginalInsertDate = value;}
		}

		public UPSMemberTran()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public UPSMemberTran(MemberTran mt)
		{
			_MemberTran = mt;
		}
	}
}
