using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// The MemberTran that was used to calculate remaining credit for another MemberTran.  
	/// </summary>
	[Serializable] 
	public class MemberTranCredit : IValueObject
	{
		#region Private Members

		// The memberTranID is actually the CreditedFromTranID in the MemberTranCredit table.  
		private int memberTranID = Constants.NULL_INT;	
		private decimal creditAmount = Constants.NULL_DECIMAL;
		private DateTime insertDate = DateTime.MinValue;

		#endregion
		
		public MemberTranCredit()
		{

		}

		public MemberTranCredit(int memberTranID, decimal creditAmount, DateTime insertDate)
		{
			this.memberTranID = memberTranID;
			this.creditAmount = creditAmount;
			this.insertDate = insertDate;
		}
	
		#region Properties

		public int MemberTranID
		{
			get {return this.memberTranID;}
			set {this.memberTranID = value;}
		}

		public decimal CreditAmount
		{
			get {return this.creditAmount;}
			set {this.creditAmount = value;}
		}

		public DateTime InsertDate
		{
			get {return this.insertDate;}
			set {this.insertDate = value;}
		}

		#endregion
	}
}
