using System;
using System.Collections;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// The collection of MemberTran that was used to calculate the total remaining credits for another MemberTran.  
	/// </summary>
	[Serializable]
	public sealed class MemberTranCreditCollection : CollectionBase, IValueObject
	{
		#region Private Members

		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		
		#endregion

		#region Constructors

		public MemberTranCreditCollection(int memberID, int siteID)
		{
			this.memberID = memberID;
			this.siteID = siteID;
		}

		#endregion

		#region Properties

		public decimal TotalCreditedAmount
		{
			get
			{
				decimal totalCreditedAmount = 0.0m;

				foreach (MemberTranCredit memberTranCredit in this.InnerList)
				{
					totalCreditedAmount += memberTranCredit.CreditAmount;
				}

				// Do not return a negative remaining credit amount since a member should not owe more than
				// he originally paid for
				// If the remaining credit amount is negative, set it to 0.  
				return (totalCreditedAmount < 0.0m ? 0.0m : Math.Round(totalCreditedAmount, 2)); 				
			}
		}

		#endregion

		#region CollectionBase Implementation

		public MemberTranCredit this[int index]
		{
			get
			{
				return (MemberTranCredit) base.InnerList[index];
			}
		}

		public int Add(MemberTranCredit memberTranCredit)
		{
			return base.InnerList.Add(memberTranCredit);
		}

		public MemberTranCredit FindByID(int memberTranID)
		{
			foreach (MemberTranCredit memberTranCredit in this.InnerList)
			{
				if (memberTranCredit.MemberTranID == memberTranID)
				{
					return memberTranCredit;
				}
			}

			return null;
		}
		
		#endregion
		
	}
}
