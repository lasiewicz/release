using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class SaveOfferCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members

		//private int cacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.SerivceAdapters.RuntimeSettings.GetSetting("SUBSCRIPTIONSVC_CACHE_TTL_SAVEOFFERS"));
		private int cacheTTLSeconds = 3600;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Sliding;
		private int siteID = Constants.NULL_INT;

		#endregion

		#region Constructors

		public SaveOfferCollection(int siteID)
		{
			this.siteID = siteID;
		}

		#endregion
		
		#region CollectionBase Implementation

		public SaveOffer this[int index]
		{
			get { return (SaveOffer)base.InnerList[index]; }
		}

		public int Add(SaveOffer saveOffer)
		{
			return base.InnerList.Add(saveOffer);
		}

		public SaveOffer FindByID(int saveOfferID)
		{
			foreach(SaveOffer saveOffer in this)
			{
				if(saveOffer.SaveOfferID == saveOfferID)
				{
					return saveOffer;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		public static string GetCacheKey(int siteID)
		{
			return "~SAVEOFFERCOLLECTION^"+ siteID.ToString();
		}

		public string GetCacheKey()
		{
			return "~SAVEOFFERCOLLECTION^"+ siteID.ToString();
		}

		#endregion
	}
}
