using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class SaveOffer : IValueObject
	{
		#region Private Members
		
		private int saveOfferID = Constants.NULL_INT;
		private TransactionReasonType transactionReasonType;
		private int planID = Constants.NULL_INT;
		private int discountID = Constants.NULL_INT;
		private string resourceConstant = Constants.NULL_STRING;
		private int tier = Constants.NULL_INT;
		
		#endregion
		
		#region Constructors

		public SaveOffer(
			int saveOfferID,
			TransactionReasonType transactionReasonType,
			int planID,
			int discountID,
			string resourceConstant,
			int tier)
		{
			this.saveOfferID = saveOfferID;
			this.transactionReasonType = transactionReasonType;
			this.planID = planID;
			this.discountID = discountID;
			this.resourceConstant = resourceConstant;
			this.tier = tier;
		}

		#endregion
		
		#region Properties

		public int SaveOfferID
		{
			get { return saveOfferID; }
			set { saveOfferID = value; }
		}

		public TransactionReasonType TransactionReasonType
		{
			get { return transactionReasonType; }
			set { transactionReasonType = value; }
		}

		public int PlanID
		{
			get { return planID; }
			set { planID = value; }
		}

		public int DiscountID
		{
			get { return discountID; }
			set { discountID = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public int Tier
		{
			get { return tier; }
			set { tier = value; }
		}
		
		#endregion
	}
}
