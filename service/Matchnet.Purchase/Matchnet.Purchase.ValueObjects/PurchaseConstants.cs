using System;

namespace Matchnet.Purchase.ValueObjects
{
	public class PurchaseConstants
	{
		private PurchaseConstants()
		{
		}

		public const string QUEUEPATH_PURCHASE_INBOUND = @".\private$\Purchase_Inbound";
		public const string QUEUEPATH_PURCHASE_OUTBOUND = @".\private$\Purchase_Outbound";

		public const string SOOPER_HASH = "00025554F494";

		// Transaction Reason ID
		public const int TRANSACTION_REASON_ID_CREDIT = 24;

		// Attribute IDs
		public const int EMAIL_ADDRESS_ATTRIBUTE_ID = 12;
		public const int FIRST_NAME_ATTRIBUTE_ID = 3;
		public const int LAST_NAME_ATTRIBUTE_ID = 4;
		public const int MEMBERTRAN_ID_ATTRIBUTE_ID = 168;
		public const int ISRAELI_ID_ATTRIBUTE_ID = 198;
		public const int PHONE_ATTRIBUTE_ID = 22;
		public const int ADDRESS_LINE1_ATTRIBUTE_ID = 30;
		public const int ADDRESS_LINE2_ATTRIBUTE_ID = 31;
		public const int CITY_ATTRIBUTE_ID = 6;
		public const int STATE_ATTRIBUTE_ID = 7;
		public const int COUNTRY_REGION_ID_ATTRIBUTE_ID = 8;
		public const int POSTAL_CODE_ATTRIBUTE_ID = 9;
		public const int CREDIT_CARD_TYPE_ATTRIBUTE_ID = 33;
		public const int CARD_CARD_NUMBER_ATTRIBUTE_ID = 10;
		public const int CHECKING_ACCOUNT_NUMBER_ATTRIBUTE = 18;
		public const int DIRECT_DEBIT_ACCOUNT_NUMBER_ATTRIBUTE_ID = 192;
		public const int CREDIT_CARD_EXPIRATION_MONTH_ATTRIBUTE_ID = 26;
		public const int CREDIT_CARD_EXPIRATION_YEAR_ATTRIBUTE_ID = 27;
		public const int CVC_ATTRIBUTE_ID = 196;
		public const int DRIVERS_LICENSE_NUMBER_ATTRIBUTE_ID = 20;
		public const int DRIVERS_LICENSE_STATE_ATTRIBUTE_ID = 21;
		public const int BANK_NAME_ATTRIBUTE_ID = 13;
		public const int BANK_CODE_ATTRIBUTE_ID = 190;
		public const int BANK_ROUTING_NUMBER_ATTRIBUTE_ID = 19;
		public const int BANK_ACCOUNT_TYPE_ATTRIBUTE_ID = 234;
		public const int CHECK_NUMBER_ATTRIBUTE_ID = 17;
		public const int AMOUNT_ATTRIBUTE_ID = 25;
		public const int MEMBER_ID_ATTRIBUTE_ID = 2;


		// Provider IDs
		public const int JETPAY_PROVIDER_ID = 6000;
		public const int GLOBAL_COLLECT_PROVIDER_ID = 7000;
		public const int TRANZILA_PROVIDER_ID = 8000;
		public const int FIRST_DATA_PROVIDER_ID = 9000;
		public const int OPTIMAL_PAYMENTS_PROVIDER_ID = 11000;
		public const int LITLE_PROVIDER_ID = 12000;

		// Region
		public const int REGION_ID_USA = 223;

		// MemberTran
		public const string MEMBERTRAN_MESSAGE_ID = "MemberTranMessageID";

		// Paying Attribute
		public const string ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE = "SubscriptionExpirationDate";
		public const string ATTRIBUTE_SUBSCRIPTION_LAST_INITIAL_PURCHASE_DATE = "SubscriptionLastInitialPurchaseDate";
        public const string ATTRIBUTE_SUBSCRIPTION_LAST_INITIAL_PURCHASE_DATE_AFTER_LAPSE = "SubscriptionLastInitialPurchaseDateAfterLapse";

		// Premium services
		public const string ATTRIBUTE_HIGHLIGHTED_EXPIRATION_DATE = "HighlightedExpirationDate";
		public const string ATTRIBUTE_SPOTLIGHT_EXPIRATION_DATE = "SpotlightExpirationDate";
		public const string ATTRIBUTE_JMETER_EXPIRATION_DATE = "JMeterExpirationDate";
        public const string ATTRIBUTE_READRECEIPT_EXPIRATION_DATE = "ReadReceiptExpirationDate";

		// Subscription status of the member on a site
		public const string ATTRIBUTE_SUBSCRIPTION_STATUS = "SubscriptionStatus";

		// Sites
		public const int AS_SITE_ID = 101;
		public const int CA_SITE_ID = 13;
		public const int JD_SITE_ID = 103;
	}
}
