using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Matchnet;


namespace Matchnet.Purchase.ValueObjects
{
	#region Currency Types
	
	public enum CurrencyType : int
	{
		None = 0,
		USDollar = 1,
		Euro = 2,
		CanadianDollar = 3,
		Pound = 4,
		AustralianDollar = 5,
		Shekels = 6,
		VerifiedByVisa = 7
	}
	
	#endregion

	#region Plan Types

	[Flags]
	public enum PlanType : int	
	{
		None = 0,
		Regular = 1,
		OneTimeOnly = 2,
		AuthOnly = 4,
		TerminateImmediately = 8,
		FreeTrialWelcome = 16,
		Registration = 32,
		Installment = 64,
		PromotionalPlan = 128,
        PremiumPlan = 256,
		SiteDefault = 512,
		ALaCarte = 1024,
		Discount = 2048
	}
	
	#endregion

	#region Premium Plan Type Mask

	[Flags]
	public enum PremiumType : int	
	{
		None = 0,
		HighlightedProfile=1,
		SpotlightMember=2,
		JMeter = 4,
		ColorCode = 8,
        ServiceFee = 16,
        AllAccess = 32,
        AllAccessEmail = 64,
        ReadReceipt = 128
	}

	#endregion

	#region Plan Display Validation

	/// <summary>
	/// Returned type from server side validation
	/// </summary>
	public enum PlanDisplayValidation : int
	{
		None = 0,
		PurchaseModeChanged = 1,
		RemainingCreditChanged = 2,
		PurchaseAllowedWithRemainingCreditChanged = 3
	};
	
	#endregion

	[Serializable]
	public sealed class PlanCollection : CollectionBase, IValueObject, ICacheable, ICloneable
	{
		#region Private Members
		
		//private int cacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.SerivceAdapters.RuntimeSettings.GetSetting("SUBSCRIPTIONSVC_CACHE_TTL_PLANS"));
		private int cacheTTLSeconds = 6000;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Absolute;
		private int brandID = Matchnet.Constants.NULL_INT;
		private PlanCollectionDisplayMetaData planCollectionDisplayMetaData = null;

		#endregion


		#region Constructors
		
		public PlanCollection(int brandID)
		{
			this.brandID = brandID;
			planCollectionDisplayMetaData = new PlanCollectionDisplayMetaData();
		}

		#endregion

		#region Properties

		public PlanCollectionDisplayMetaData DisplayMetaData
		{
			get { return this.planCollectionDisplayMetaData; }
			set { this.planCollectionDisplayMetaData = value; }			
		}

		#endregion

		#region CollectionBase Implementation

		public Plan this[int index]
		{
			get{return (Plan)base.InnerList[index];}
			
		}

		public int Add(Plan plan)
		{
			return base.InnerList.Add(plan);
		}

		public Plan FindByID(int planID)
		{
			foreach(Plan plan in this)
			{
				if(plan.PlanID == planID)
				{
					return plan;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		public static string GetCacheKey(int brandID)
		{
			return "~PLANCOLLECTION^" + brandID.ToString();
		}

		public string GetCacheKey()
		{
			return "~PLANCOLLECTION^" + brandID.ToString();
		}

		#endregion

		#region

		public Object Clone()
		{
			using (Stream objStream = new MemoryStream())
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(objStream, this);
				objStream.Seek(0, SeekOrigin.Begin);
				return formatter.Deserialize(objStream);
			}			
		}

		#endregion

	}
}
