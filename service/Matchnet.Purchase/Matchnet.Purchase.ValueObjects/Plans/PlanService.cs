using System;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// PlanService definition type
	/// </summary>
	public enum PlanServiceDefinition : int
	{
		Basic_Subscription = 1,
		SpotLight = 1000,
		Profile_Highlighting = 1001,
		JMeter = 1002,
		ColorCode = 1003,
        ServiceFee = 1004,
        AllAccess  = 1005,
        AllAccessEmail = 1006,
        ReadReceipt = 1007,
		Discount = 2000
	};

	/// <summary>
	/// Summary description for PlanService.
	/// </summary>
	/// 
	[Serializable]
	public class PlanService : IValueObject
	{
		private PlanServiceDefinition _planServiceDefinition;
        private decimal _basePrice = Constants.NULL_DECIMAL;
		private decimal _renewalPrice = Constants.NULL_DECIMAL;
        private int _disburseCount = Constants.NULL_INT;
        private int _disburseDuration = Constants.NULL_INT;
        private int _diburseDurationTypeID = 0;

		/// <summary>
		/// Empty c'tor to satisfy serialization in the case of xml serialization
		/// </summary>
		public PlanService()
		{}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="PlanSvcDef"></param>
		/// <param name="BasePrice"></param>
		/// <param name="RenewalPrice"></param>
		public PlanService(PlanServiceDefinition PlanSvcDef, decimal BasePrice, decimal RenewalPrice)
		{
			_planServiceDefinition = PlanSvcDef;
			_basePrice = BasePrice;
			_renewalPrice = RenewalPrice;
		} 

		#region Properties
		/// <summary>
		/// PlanServiceDefinition type
		/// </summary>
		public PlanServiceDefinition PlanSvcDefinition
		{
			get { return _planServiceDefinition; }
		}
		/// <summary>
		/// BasePrice
		/// </summary>
		public decimal BasePrice
		{
			get { return _basePrice; }
		}
		/// <summary>
		/// RenewalPrice
		/// </summary>
		public decimal RenewalPrice
		{
			get { return _renewalPrice; }
		}

        /// <summary>
        /// DisburseCount - used for count based items
        /// </summary>
        public int DisburseCount
        {
            get { return _disburseCount; }
            set { _disburseCount = value; }
        }

        /// <summary>
        /// DisburseDuration - used for count based items
        /// </summary>
        public int DisburseDuration
        {
            get { return _disburseDuration; }
            set { _disburseDuration = value; }
        }

        /// <summary>
        /// DisburseDurationTypeID - used for count based items
        /// </summary>
        public int DisburseDurationTypeID
        {
            get { return _diburseDurationTypeID; }
            set { _diburseDurationTypeID = value; }
        }

		#endregion
	}
}
