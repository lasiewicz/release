using System;
using Matchnet;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class Plan : IValueObject , ICloneable
	{
		#region Private Members

		private int planID = Constants.NULL_INT;
		private CurrencyType currencyType;
		private decimal initialCost = Constants.NULL_DECIMAL;
		private int initialDuration = Constants.NULL_INT;
		private DurationType initialDurationType;
		private decimal renewCost = Constants.NULL_DECIMAL;
		private int renewDuration = Constants.NULL_INT;
		private DurationType renewDurationType;
        private int freeTrialDuration = Constants.NULL_INT;
        private DurationType freeTrialDurationType;
		private PlanType planTypeMask = PlanType.None;
		private int renewAttempts = Matchnet.Constants.NULL_INT;
		private DateTime updateDate = DateTime.MinValue;
		private bool bestValueFlag = false;
		private PaymentType paymentTypeMask = PaymentType.None;
		private PaymentType planResourcePaymentTypeID = PaymentType.None;
		private DateTime startDate = DateTime.MinValue;
		private DateTime endDate = DateTime.MinValue;
		//private PlanDisplayMetaData planDisplayMetaData = null;
		private PremiumType premiumTypeMask=PremiumType.None;
		private decimal creditAmount = Constants.NULL_DECIMAL;
		private PurchaseMode purchaseMode = PurchaseMode.None;
		private PlanServiceCollection planServices = null;
		private string planResourceConstant = Constants.NULL_STRING;

		#endregion

		#region Constructors

		public Plan()
		{
		}

		public Plan(
			int planID,
			CurrencyType currencyType,
			decimal initialCost,
			int initialDuration,
			DurationType initialDurationType,
			decimal renewCost,
			int renewDuration,
			DurationType renewDurationType,
			PlanType planTypeMask,
			int renewAttempts,
			DateTime updateDate,
			bool bestValueFlag,
			PaymentType paymentTypeMask,
			PaymentType planResourcePaymentTypeID,
			DateTime startDate,
			DateTime endDate) : this (planID,
			currencyType,
			initialCost,
			initialDuration,
			initialDurationType,
			renewCost,
			renewDuration,
			renewDurationType,
			planTypeMask,
			renewAttempts,
			updateDate,
			bestValueFlag,
			paymentTypeMask,
			planResourcePaymentTypeID,
			startDate,
			endDate,
			Constants.NULL_DECIMAL,
			PurchaseMode.None) 
		{

		}

		public Plan(
			int planID,
			CurrencyType currencyType,
			decimal initialCost,
			int initialDuration,
			DurationType initialDurationType,
			decimal renewCost,
			int renewDuration,
			DurationType renewDurationType,
			PlanType planTypeMask,
			int renewAttempts,
			DateTime updateDate,
			bool bestValueFlag,
			PaymentType paymentTypeMask,
			PaymentType planResourcePaymentTypeID,
			DateTime startDate,
			DateTime endDate,
			decimal creditAmount,
			PurchaseMode purchaseMode)
		{
			this.planID = planID;
			this.currencyType = currencyType;
			this.initialCost = initialCost;
			this.initialDuration = initialDuration;
			this.initialDurationType = initialDurationType;
			this.renewCost = renewCost;
			this.renewDuration = renewDuration;
			this.renewDurationType = renewDurationType;
			this.planTypeMask = planTypeMask;
			this.renewAttempts = renewAttempts;
			this.updateDate = updateDate;
			this.bestValueFlag = bestValueFlag;
			this.paymentTypeMask = paymentTypeMask;
			this.planResourcePaymentTypeID = planResourcePaymentTypeID;
			this.startDate = startDate;
			this.endDate = endDate;
			this.creditAmount = creditAmount;
			this.purchaseMode = purchaseMode;
		}

		#endregion
		
		#region Properties

		public int PlanID
		{
			get { return planID; }
			set { planID = value; }
		}

		public CurrencyType CurrencyType
		{
			get { return currencyType; }
			set { currencyType = value; }
		}

		public decimal InitialCost
		{
			get { return initialCost; }
			set { initialCost = value; }
		}
	
		public int InitialDuration
		{
			get { return initialDuration; }
			set { initialDuration = value; }
		}

		public DurationType InitialDurationType
		{
			get { return initialDurationType; }
			set { initialDurationType = value; }
		}

		public decimal RenewCost
		{
			get { return renewCost; }
			set { renewCost = value; }
		}

		public int RenewDuration
		{
			get { return renewDuration; }
			set { renewDuration = value; }
		}

		public DurationType RenewDurationType
		{
			get { return renewDurationType; }
			set { renewDurationType = value; }
		}

        public int FreeTrialDuration
        {
            get { return freeTrialDuration; }
            set { freeTrialDuration = value; }
        }

        public DurationType FreeTrialDurationType
        {
            get { return freeTrialDurationType; }
            set { freeTrialDurationType = value; }
        }

		public PlanType PlanTypeMask
		{
			get { return planTypeMask; }
			set { planTypeMask = value; }
		}

		public int RenewAttempts
		{
			get { return renewAttempts; }
			set { renewAttempts = value; }
		}

		public DateTime UpdateDate
		{
			get { return updateDate; }
			set { updateDate = value; }
		}

		public bool BestValueFlag
		{
			get { return bestValueFlag; }
			set { bestValueFlag = value; }
		}

		public PaymentType PaymentTypeMask
		{
			get { return paymentTypeMask; }
			set { paymentTypeMask = value; }
		}

		public PaymentType PlanResourcePaymentTypeID
		{
			get { return planResourcePaymentTypeID; }
			set { planResourcePaymentTypeID = value; }
		}

		public DateTime StartDate
		{
			get { return startDate; }
			set { startDate = value; }
		}

		public DateTime EndDate
		{
			get { return endDate; }
			set { endDate = value; }
		}

		public PremiumType PremiumTypeMask
		{
			get { return this.premiumTypeMask; }
			set { this.premiumTypeMask = value; }			
		}

		public decimal CreditAmount
		{
			get { return this.creditAmount; }
			set { this.creditAmount = value; }			
		}

		public PurchaseMode PurchaseMode
		{
			get { return this.purchaseMode; }
			set { this.purchaseMode = value; }			
		}

		public PlanServiceCollection PlanServices
		{
			get { return planServices; }
			set { planServices = value; }
		}

		public string PlanResourceConstant
		{
			get { return planResourceConstant; }
			set { planResourceConstant = value; }
		}

		/*
		public DateTime InsertDate
		{
			get { return insertDate; }
			set { insertDate = value; }
		}
		*/

		/// <summary>
		/// Determines if the purchase mode is eligible for a credit  
		/// </summary>
		/// <param name="purchaseMode">Purchase mode to be checked</param>
		/// <returns name="bool">True if the purchase mode is eligible for a credit.  Otherwise return false if the purchase mode is not eligible for a credit.</returns>
		public static bool IsPurchaseModeValidForCredit(PurchaseMode purchaseMode)
		{
			bool isEligibleForCredit = false;

			if (purchaseMode == PurchaseMode.Upgrade
				|| purchaseMode == PurchaseMode.Swap)
			{
				isEligibleForCredit = true;
			}

			return isEligibleForCredit;
		}

		#endregion

		#region ICloneable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Object Clone()
		{
			using (Stream objStream = new MemoryStream())
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(objStream, this);
				objStream.Seek(0, SeekOrigin.Begin);
				return formatter.Deserialize(objStream);
			}
		}

		#endregion
	}
}
