using System;
using System.Collections;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Summary description for PlanServiceCollection.
	/// </summary>
	/// 
	[Serializable]
	public class PlanServiceCollection : CollectionBase, IValueObject	
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		public PlanServiceCollection()
		{			
		}

		#region CollectionBase
		/// <summary>
		/// Indexer
		/// </summary>
		public PlanService this[int index]
		{
			get{return (PlanService)base.InnerList[index];}
			
		}

		/// <summary>
		/// Add
		/// </summary>
		/// <param name="planService"></param>
		/// <returns></returns>
		public int Add(PlanService planService)
		{
			return base.InnerList.Add(planService);
		}
		#endregion
	}
}
