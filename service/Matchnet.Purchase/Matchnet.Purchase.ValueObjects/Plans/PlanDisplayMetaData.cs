using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Describes how this plan in a particular plan collection will render
	/// in an application.  This includes row ordering, column ordering, 
	/// plan row header, and plan column header.  A single plan can be in different
	/// order and group across different promotions and default plans by brand.       
	/// </summary>
	[Serializable]
	public class PlanDisplayMetaData
	{
		#region Private Members

		private int listOrder = Constants.NULL_INT;
		private int columnGroup = Constants.NULL_INT;
		private string rowHeaderResourceConstant = Constants.NULL_STRING;
		private string columnHeaderResourceConstant = Constants.NULL_STRING;
		private bool enabledForMember = true;

		#endregion

		#region Constructors

		public PlanDisplayMetaData()
		{

		}

		public PlanDisplayMetaData(
			int listOrder, 
			int columnGroup, 
			string rowHeaderResourceConstant, 
			string columnHeaderResourceConstant) : this (listOrder,
			columnGroup,
			rowHeaderResourceConstant,
			columnHeaderResourceConstant,
			true)
		{

		}

		public PlanDisplayMetaData(
			int listOrder, 
			int columnGroup, 
			string rowHeaderResourceConstant, 
			string columnHeaderResourceConstant,
			bool enabledForMember)
		{
			this.listOrder = listOrder;
			this.columnGroup = columnGroup;
			this.rowHeaderResourceConstant = rowHeaderResourceConstant;
			this.columnHeaderResourceConstant = columnHeaderResourceConstant;
			this.enabledForMember = enabledForMember;
		}

		#endregion

		#region Properties

		public int ListOrder
		{
			get { return listOrder; }
			set { listOrder = value; }
		}

		public int ColumnGroup
		{
			get { return this.columnGroup; }
			set { this.columnGroup = value; }
		}

		public string RowHeaderResourceConstant
		{
			get { return this.rowHeaderResourceConstant; }
			set { this.rowHeaderResourceConstant = value; }
		}

		public string ColumnHeaderResourceConstant
		{
			get { return this.columnHeaderResourceConstant; }
			set { this.columnHeaderResourceConstant = value; }
		}

		public bool EnabledForMember
		{
			get { return this.enabledForMember; }
			set { this.enabledForMember = value; }
		}

		#endregion		

	}
}
