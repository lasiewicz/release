using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	/// <summary>
	/// Outlines the shape of the plan collection when rendered in an application
	/// by describing all the groups across and all the rows to be filled in these
	/// groups.    
	/// </summary>
	[Serializable]
	public class PlanCollectionDisplayMetaData
	{
		#region Private Members
		
		private ArrayList colAllPlaceholders = null;
		private ArrayList rowAllPlaceholders = null;		

		#endregion

		#region Constructors

		public PlanCollectionDisplayMetaData()
		{
			colAllPlaceholders = new ArrayList();
			rowAllPlaceholders = new ArrayList();
		}

		#endregion

		#region Properties

		public ArrayList AllColumnPlaceholders
		{
			get 
			{
				return this.colAllPlaceholders;
			}
			set 
			{
				this.colAllPlaceholders = value;
			}
		}

		public ArrayList AllRowPlaceholders
		{
			get 
			{
				return this.rowAllPlaceholders;
			}
			set 
			{
				this.rowAllPlaceholders = value;
			}
		}

		#endregion


	}
}
