using System;
using Matchnet;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Purchase.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IPlanService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <returns></returns>
		PlanCollection GetPlans(Int32 brandID,PlanType planTypeMask,PaymentType paymentTypeMask);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="initialDuration"></param>
		/// <param name="durationType"></param>
		/// <param name="premiumType"></param>
		/// <returns></returns>
		PlanCollection GetPlans(Int32 brandID,PlanType planTypeMask,PaymentType paymentTypeMask, int initialDuration, DurationType durationType, PremiumType premiumType);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="planID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		Plan GetPlan(Int32 planID, 	Int32 brandID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="planID"></param>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <returns></returns>
		Plan GetPlan(Int32 planID,Int32 brandID,PlanType planTypeMask,PaymentType paymentTypeMask);
//
//		PlanCollection GetMemberDisplayedPlans(Int32 brandID,
//			PlanType planTypeMask,
//			PaymentType paymentTypeMask, Int32 memberID);
//
//		PlanCollection SetupPlanCollectionForDisplay(PlanCollection sourcePlans, 
//			int brandID, Int32 bestValuePlanID, Int32 memberID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="planID"></param>
		/// <returns></returns>
		string GetPlanDescription(Int32 planID);
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="freeTrialGroupType"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		Plan GetFreeTrialGroupPlan(FreeTrialGroupType freeTrialGroupType, Int32 brandID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="plan"></param>
		/// <returns></returns>
		Int32 InsertPlan(Int32 brandID, Plan plan);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="plan"></param>
		/// <param name="remainingCreditAmount"></param>
		/// <returns></returns>
		bool IsPurchaseAllowedWithRemainingCredit(Plan plan, decimal remainingCreditAmount);
	}
}
