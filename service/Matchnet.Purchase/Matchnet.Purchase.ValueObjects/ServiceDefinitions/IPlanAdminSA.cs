using System;

using Matchnet;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Purchase.ServiceDefinitions
{
	public interface IPlanAdminService
	{
		PlanCollection GetPlans(Int32 brandID);

		PlanCollection GetPlans(Int32 brandID, PaymentType paymentTypeMask);

//		PlanCollection GetAdminDisplayedPlans(Int32 brandID,
//			PaymentType paymentTypeMask,
//			Int32 memberID);
//
//		//08202008 TL - Adding overloaded method to support ESP project, specifically to allow Admin version of the subscription pages to view plans with member specific UI metadata
//		PlanCollection GetAdminDisplayedPlans(Int32 brandID,
//			PaymentType paymentTypeMask,
//			Int32 memberID,
//			bool showSiteDefaultPlansOnly,
//			bool allowAllColumnOrders);

		Int32 InsertPlan(Int32 brandID,
			CurrencyType currencyType,
			string resourceConstant,
			decimal initialCost,
			Int32 initialDuration,
			DurationType initialDurationType,
			decimal renewCost,
			Int32 renewDuration,
			DurationType renewDurationType,
			PlanType planTypeMask,
			Int32 renewAttempts,
			bool bestValueFlag,
			Int32 listOrder,
			PaymentType paymentTypeMask,
			DateTime startDate,
			DateTime endDate);

		void UpdatePlan(Int32 planID,
			Int32 brandID,
			CurrencyType currencyType,
			string resourceConstant,
			decimal initialCost,
			Int32 initialDuration,
			DurationType initialDurationType,
			decimal renewCost,
			int renewDuration,
			DurationType renewDurationType,
			PlanType planTypeMask,
			Int32 renewAttempts,
			bool bestValueFlag,
			Int32 listOrder,
			PaymentType paymentTypeMask,
			DateTime startDate,
			DateTime endDate);

		void InsertPlanResource(Int32 planID, 
			PaymentType paymentType, 
			string resourceConstant);

		void UpdatePlanResource(Int32 planID, 
			PaymentType paymentType, 
			string resourceConstant);
	}
}
