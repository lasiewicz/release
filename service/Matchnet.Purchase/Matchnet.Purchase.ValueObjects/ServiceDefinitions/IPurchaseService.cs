using System;
using System.Collections.Specialized;

using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Purchase.ServiceDefinitions
{
	public interface IPurchaseService
	{
		MemberSubscriptionStatus GetMemberSubStatus(int memberid, int siteid, DateTime memberSubEndDate);
		SubscriptionResult BeginAuthPmtProfile(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 referenceMemberPaymentId);

		SubscriptionResult BeginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string addressLine2,
			string city,
			string state,
			string postalCode,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			Int32 checkNumber,
			Int32 conversionMemberID,
			BankAccountType bankAccountType,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress);

		SubscriptionResult BeginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string addressLine2,
			string city,
			string state,
			string postalCode,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			Int32 checkNumber,
			Int32 conversionMemberID,
			BankAccountType bankAccountType,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID);

		SubscriptionResult BeginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string addressLine2,
			string city,
			string state,
			string postalCode,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			Int32 checkNumber,
			Int32 conversionMemberID,
			BankAccountType bankAccountType,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID,
			bool reusePreviousPayment);

		SubscriptionResult BeginCreditCardPurchase(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress);
		/// <summary>
		/// Overload method.
		/// </summary>		
		 SubscriptionResult BeginCreditCardPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 brandID,
			string firstName, 
			string lastName, 
			string israeliID, 
			string phone, 
			string addressLine1, 
			string city, 
			string state, 
			Int32 countryRegionID, 
			string postalCode, 
			CreditCardType creditCardType, 
			string creditCardNumber, 
			Int32 expirationMonth, 
			Int32 expirationYear, 
			string cvc, 
			Int32 conversionMemberID, 
			Int32 sourceID, 
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID);		

		SubscriptionResult BeginCreditCardPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 brandID,
			string firstName, 
			string lastName, 
			string israeliID, 
			string phone, 
			string addressLine1, 
			string city, 
			string state, 
			Int32 countryRegionID, 
			string postalCode, 
			CreditCardType creditCardType, 
			string creditCardNumber, 
			Int32 expirationMonth, 
			Int32 expirationYear, 
			string cvc, 
			Int32 conversionMemberID, 
			Int32 sourceID, 
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode);

		SubscriptionResult BeginCreditCardPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 brandID,
			string firstName, 
			string lastName, 
			string israeliID, 
			string phone, 
			string addressLine1, 
			string city, 
			string state, 
			Int32 countryRegionID, 
			string postalCode, 
			CreditCardType creditCardType, 
			string creditCardNumber, 
			Int32 expirationMonth, 
			Int32 expirationYear, 
			string cvc, 
			Int32 conversionMemberID, 
			Int32 sourceID, 
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			bool reusePreviousPayment
			);

		Int32 BeginCreditCardVerification(Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear);
			
		Int32 BeginCredit(TranType tranType,
			Int32 memberID,
			Int32 adminMemberID,
			Int32 siteID,
			Int32 merchantID,
			Int32 referenceMemberTranID,
			Int32 memberPaymentID,
			Int32 planID,
			decimal amount,
			Int32 minutes,
			Int32 duration,
			DurationType durationType,
			CurrencyType currencyType);
			
		SubscriptionResult BeginRenewalPurchase(Int32 memberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 communityID,
			Int32 brandID,
			Int32 adminMemberID, 
			Int32 memberPaymentID,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID);

		SubscriptionResult BeginRenewalPurchase(Int32 memberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 communityID,
			Int32 brandID,
			Int32 adminMemberID, 
			Int32 memberPaymentID,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID);

		SubscriptionResult EndSubscription(Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID, 
			Int32 transactionReasonID);

		SubscriptionResult EndALaCarteOnly(int memberID, 
			int adminMemberID,
			int siteID, 
			int planID);

		bool HasAcceptedFreeTrial(Int32 planID, 
			Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID,
			Int32 brandID);

		bool HasSubscriptionEnded(Int32 memberID,
			Int32 siteID);

		bool IsEligibleForSaveOffer(Int32 memberID,
			Int32 siteID);
	
		MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID);
			
		NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID);

		NameValueCollection GetMemberPaymentByMemberTranID(
			Int32 memberID,
			Int32 memberTranID);

		NameValueCollection UPSGetMemberPaymentByOrderID(
			Int32 memberID, 
			Int32 memberTranID, 
			Int32 siteID);

		SaveOfferCollection GetSaveOffers(Int32 siteID);
			
		SiteMerchantCollection GetSiteMerchants(Int32 siteID,
			CurrencyType currencyType,
			PaymentType paymentType);
			
		MemberTranCollection GetMemberTransactions(Int32 memberID, 
			Int32 siteID);

		SubscriptionResult GetMemberTranStatus(Int32 memberID,
			Int32 memberTranID);

		MemberSub GetSubscription(Int32 memberID, 
			Int32 siteID);

		TransactionReasonCollection GetTransactionReasons();

		TransactionReasonSiteCollection GetTransactionReasonsSite(Int32 siteID);

		Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID);

		CreditCardCollection GetCreditCardTypes(Int32 siteID);

		DiscountCollection GetDiscounts();
	
		bool RenderCVC(Int32 siteID,
			CurrencyType currencyType,
			PaymentType paymentType);

		void SaveComplete(MemberTranResult memberTranResult);

		SubscriptionResult SaveDiscount(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 siteID, 
			Int32 discountID);

		void UpdateMemberPayment(Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		MemberTranCreditCollection GetRemainingCredit(Int32 memberID, 
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="plan"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		PurchaseMode GetPurchaseMode(Int32 memberID, 
			Plan plan, 
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		Int32 UPSProcessLegacyData(Int32 memberID,
			Int32 adminMemberID,
			Int32[] planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			CreditCardType creditCardType,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode, 
			bool reusePreviousPayment,
			string firstName,
			string lastName,
			TranType tranType,
			Int32 referenceMemberPaymentID,
			CreditCardType referenceCreditCardType,
			decimal amount,
			Int32 duration,
			DurationType durationType,
			CurrencyType currencyType,
			string lastFourAccountNumber,
			Int32 orderID,  
			Int32 referenceOrderID,  
			int commonResponseCode,  
			Int32 memberTranStatusID,
			DateTime originalInsertDate,    
			Int32 chargeID,
			OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS);

		Int32 UPSProcessLegacyData(Int32 memberID,
			Int32 adminMemberID,
			Int32[] planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			CreditCardType creditCardType,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode, 
			bool reusePreviousPayment,
			string firstName,
			string lastName,
			TranType tranType,
			Int32 referenceMemberPaymentID,
			CreditCardType referenceCreditCardType,
			decimal amount,
			Int32 duration,
			DurationType durationType,
			CurrencyType currencyType,
			string lastFourAccountNumber,
			Int32 orderID,  
			Int32 referenceOrderID,  
			int commonResponseCode,  
			Int32 memberTranStatusID,
			DateTime originalInsertDate,      
			Int32 chargeID,
			OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS,
			Int32 paymentTypeID);

        void UPSLegacyDataSave(string upsLegacyData, int id);

		string UPSLegacyDataGet(int upsLegacyDataID);

		bool HasValidCCOnFile(int memberID, int brandID);
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		int GenerateEncryptedSalesFile();
		/// <summary>
		/// 
		/// </summary>
		void ProcessMembershipFile();
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		MatchbackCollection GetMatbackRequest();
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		int StartMatchbackFileGeneration(string outputFileName);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="file"></param>
		/// <param name="lineID"></param>
		void InsertMatchbackFile(byte[] file, int lineID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mbCol"></param>
		void MarkMatchbackRequestAsComplete(MatchbackCollection mbCol);
	}
}
