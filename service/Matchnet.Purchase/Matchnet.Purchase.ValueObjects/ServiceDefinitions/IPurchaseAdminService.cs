using System;
using System.Collections.Specialized;

using Matchnet;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Purchase.ServiceDefinitions
{
	public interface IPurchaseAdminService
	{
		SubscriptionResult RemovePaymentInfo(Int32 memberID, 
			Int32 siteID);


		Int32[] GetMemberIDsByAccountNumber(string accountNumber);
			
		Int32 GetMemberIDByMemberTranID(Int32 memberTranID);
			
		CreditMaximum GetCreditMaximum(Int32 memberTranID);
			
		NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID);
			
		Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID);

		Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID);

		Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand);

		Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
			TimeAdjustOverrideType timeAdjustOverrideType);

		int UpdateALaCarteOnly(int memberID, 
			int siteID, 
			int adminMemberID,  
			TranType adjustmentType,
			int transactionReasonID, 
			bool reOpenFlag, 
			int planID,
			int planIDToReplace,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
			bool insertTransactionRecordFlag);

	}
}
