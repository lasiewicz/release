using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class Region : IValueObject
	{
		private Int32 _regionID = Matchnet.Constants.NULL_INT;
		private string _isoCountryCode2 = Matchnet.Constants.NULL_STRING;
		private string _isoCountryCode3 = Matchnet.Constants.NULL_STRING;

		public Region(Int32 regionID)
		{
			_regionID = regionID;
		}


		public Region(Int32 regionID,
			string isoCountryCode2,
			string isoCountryCode3)
		{
			_regionID = regionID;
			_isoCountryCode2 = isoCountryCode2;
			_isoCountryCode3 = isoCountryCode3;
		}


		public Int32 RegionID
		{
			get
			{
				return _regionID; 
			}
			set
			{
				_regionID = value;
			}
		}


		public string IsoCountryCode2
		{
			get
			{
				return _isoCountryCode2;
			}
			set
			{
				_isoCountryCode2 = value;
			}
		}


		public string IsoCountryCode3
		{
			get
			{
				return _isoCountryCode3;
			}
			set
			{
				_isoCountryCode3 = value;
			}
		}
	}
}
