using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public sealed class RegionCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members
		
		private int cacheTTLSeconds = 3600;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Sliding;
		
		#endregion

		#region Constructors
		
		public RegionCollection()
		{
		}

		#endregion

		#region CollectionBase Implementation

		public Region this[int index]
		{
			get{return (Region)base.InnerList[index];}
			
		}

		public int Add(Region region)
		{
			return base.InnerList.Add(region);
		}

		public Region FindByID(int regionID)
		{
			foreach(Region region in this)
			{
				if(region.RegionID == regionID)
				{
					return region;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		public static string GetCacheKey(int nullInt)
		{
			return "~PROVIDERREGIONCOLLECTION^";
		}

		public string GetCacheKey()
		{
			return "~PROVIDERREGIONCOLLECTION^";
		}

		#endregion
	}
}
