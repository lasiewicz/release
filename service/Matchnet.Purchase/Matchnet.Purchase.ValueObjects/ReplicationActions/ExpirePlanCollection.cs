using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Purchase.ValueObjects.ReplicationActions
{
	/// <summary>
	/// Summary description for ExpirePlanCollection.
	/// </summary>
	[Serializable]
	public class ExpirePlanCollection : IReplicationAction, ISerializable
	{
		private string _cacheKey;

		public string CacheKey
		{
			get { return _cacheKey; }
			set { _cacheKey = value; }
		}

		public ExpirePlanCollection(string cacheKey)
		{
			_cacheKey = cacheKey;
		}

		#region ISerializable Members
        protected ExpirePlanCollection(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			_cacheKey= br.ReadString();
		}

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_cacheKey);

			info.AddValue("bytearray", ms.ToArray());
		}
		#endregion
	}
}
