using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class SiteMerchant : IValueObject
	{
		#region Private Members

		private int providerID = Constants.NULL_INT;
		private int merchantID = Constants.NULL_INT;
		private string agentObject = Constants.NULL_STRING;
		
		#endregion

		#region Constructors

		public SiteMerchant()
		{
		}

		public SiteMerchant(
			int providerID,
			int merchantID,
			string agentObject)
		{
			this.providerID = providerID;
			this.merchantID = merchantID;
			this.agentObject = agentObject;
		}

		#endregion

		#region Properties

		public int ProviderID
		{
			get { return providerID; }
			set { providerID = value; }
		}

		public int MerchantID
		{
			get { return merchantID; }
			set { merchantID = value; }
		}

		public string AgentObject
		{
			get { return agentObject; }
			set { agentObject = value; }
		}

		public string AgentAssembly
		{
			get 
			{ 
				if ( agentObject != null )
				{
					return agentObject.Substring( 1, agentObject.IndexOf("]", 0) - 1 );
				}
				else
				{
					return null;
				}
			}
		}

		public string AgentType
		{
			get
			{
				if ( agentObject != null )
				{
					return agentObject.Substring( agentObject.IndexOf("]", 0) + 1 );
				}
				else
				{
					return null;
				}
			}
		}

		#endregion
	}
}
