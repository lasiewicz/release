using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	#region StatusType Enum
			
	public enum StatusType
	{
		Success = 2,
		Failure = 4,
		FailureRetry = 8
	}

	#endregion

	[Serializable]
	public sealed class SiteMerchantCollection : CollectionBase, IValueObject, ICacheable
	{

		#region Private Members

		//private int cacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.SerivceAdapters.RuntimeSettings.GetSetting("SUBSCRIPTIONSVC_CACHE_TTL_PRIVATELABELMERCHANTS"));
		private int cacheTTLSeconds = 3600;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Sliding;
		private int siteID = Constants.NULL_INT;
		private CurrencyType currencyType;
		private PaymentType paymentType = PaymentType.None;

		#endregion

		#region Constructors

		public SiteMerchantCollection(
			int siteID,
			CurrencyType currencyType,
			PaymentType paymentType)
		{
			this.siteID = siteID;
			this.currencyType = currencyType;
			this.paymentType = paymentType;
		}
		
		#endregion
		
		#region CollectionBase Implementation

		public SiteMerchant this[int index]
		{
			get{return (SiteMerchant)base.InnerList[index];}
		}

		public int Add(SiteMerchant siteMerchant)
		{
			return base.InnerList.Add(siteMerchant);
		}

		public SiteMerchant FindByID(int providerID)
		{
			foreach(SiteMerchant siteMerchant in this)
			{
				if(siteMerchant.ProviderID == providerID)
				{
					return siteMerchant;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		public static string GetCacheKey(
			int siteID, 
			CurrencyType currencyType, 
			PaymentType paymentType)

		{
			return "~MERCHANTSITECOLLECTION^" + siteID.ToString() + ":" + ((int)currencyType).ToString() + ":" + ((int)paymentType).ToString();
		}

		public string GetCacheKey()
		{
			return "~MERCHANTSITECOLLECTION^" + siteID.ToString() + ":" + ((int)currencyType).ToString() + ":" + ((int)paymentType).ToString();
		}

		#endregion

	}
}
