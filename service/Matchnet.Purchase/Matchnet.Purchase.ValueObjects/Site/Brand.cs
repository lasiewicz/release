using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class Brand : IValueObject
	{
		#region Private Members

		private int brandID = Matchnet.Constants.NULL_INT;
		private string uri = null;
		
		#endregion

		#region Constructors

		public Brand()
		{
		}

		public Brand(int brandID, string uri)
		{
			this.brandID = brandID;
			this.uri = uri;
		}

		#endregion

		#region Properties

		public int BrandID
		{
			get { return brandID; }
			set { brandID = value; }
		}

		public string URI
		{
			get { return uri; }
			set { uri = value; }
		}

		public override string ToString()
		{
			return uri;
		}

		#endregion

	}
}
