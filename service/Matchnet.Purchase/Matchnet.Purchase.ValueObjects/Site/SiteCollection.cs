using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class SiteCollection : CollectionBase, IValueObject
	{
		#region Constructors
		
		public SiteCollection()
		{
		}

		#endregion

		#region CollectionBase Implementation

		public Site this[int index]
		{
			get{return (Site)base.InnerList[index];}
		}

		public int Add(Site site)
		{
			return base.InnerList.Add(site);
		}

		public Site FindByID(int siteID)
		{
			foreach(Site site in this)
			{
				if(site.SiteID == siteID)
				{
					return site;
				}
			}
			return null;
		}

		#endregion
	}
}
