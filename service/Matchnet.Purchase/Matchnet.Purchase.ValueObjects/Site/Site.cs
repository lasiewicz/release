using System;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class Site : IValueObject
	{
		#region Private Members

		private int siteID = Matchnet.Constants.NULL_INT;
		private string siteName = null;
		
		#endregion
		
		#region Constructors
		
		public Site()
		{
		}

		public Site(int siteID, string siteName)
		{
			this.siteID = siteID;
			this.siteName = siteName;
		}

		#endregion

		#region Properties
		
		public int SiteID 
		{
			get { return siteID; }
			set { siteID = value; }
		}

		public string SiteName 
		{
			get { return siteName; }
			set { siteName = value; }
		}

		public override string ToString()
		{
			return siteName;
		}

		#endregion
	}
}
