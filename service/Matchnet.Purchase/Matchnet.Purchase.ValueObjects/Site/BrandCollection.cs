using System;
using System.Collections;
using Matchnet;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class BrandCollection : CollectionBase, IValueObject
	{
		#region Constructors
		
		public BrandCollection()
		{
		}

		#endregion

		#region CollectionBase Implementation

		public Brand this[int index]
		{
			get{return (Brand)base.InnerList[index];}
		}

		public int Add(Brand brand)
		{
			return base.InnerList.Add(brand);
		}

		public Brand FindByID(int brandID)
		{
			foreach(Brand brand in this)
			{
				if(brand.BrandID == brandID)
				{
					return brand;
				}
			}
			return null;
		}

		#endregion
	}
}
