using System;

namespace Matchnet.Purchase.ValueObjects
{
	[Serializable]
	public class CreditMaximum
	{
		private decimal _maxAmount;
		private Int32 _maxMinutes;

		public CreditMaximum()
		{
		}


		public CreditMaximum(decimal maxAmount,
			Int32 maxMinutes)
		{
			_maxAmount = maxAmount;
			_maxMinutes= maxMinutes;
		}


		public decimal MaxAmount
		{
			get
			{
				return _maxAmount;
			}
			set
			{
				_maxAmount = value;
			}
		}


		public Int32 MaxMinutes
		{
			get
			{
				return _maxMinutes;
			}
			set
			{
				_maxMinutes = value;
			}
		}

	}
}
