using System;
using System.Collections.Specialized;

using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Purchase.BusinessLogic;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceDefinitions;


namespace Matchnet.Purchase.ServiceManagers
{
	public class PurchaseSM : MarshalByRefObject, IPurchaseService, IPurchaseAdminService, IBackgroundProcessor
	{
		private HydraWriter _hydraWriter;
		private PurchaseBL _purchaseBL;

		public PurchaseSM()
		{
			_hydraWriter = new HydraWriter(new string[]{"mnSubscription", "mnAdmin", "mnLookup", "mnFileExchange"});
			_purchaseBL = new PurchaseBL();
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void Start()
		{
			_hydraWriter.Start();
			_purchaseBL.Start();
		}


		public void Stop()
		{
			_purchaseBL.Stop();
			_hydraWriter.Stop();
		}

		public MemberSubscriptionStatus GetMemberSubStatus(int memberid, int siteid,  DateTime memberSubEndDate)
		{
			//return _purchaseBL.GetMemberSubStatus( memberid, siteid, memberSubEndDate);

			return PurchaseBL.GetMemberSubStatus( memberid, siteid, memberSubEndDate);
		}

        public PurchaseMode GetPurchaseMode(Int32 memberID,
            Plan plan,
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                return PurchaseBL.GetPurchaseMode(memberID, plan, brand);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPurchaseMode() error.", ex);
            }
        }

        public void UPSLegacyDataSave(string upsLegacyData, int id)
        {
            try
            {
                _purchaseBL.UPSLegacyDataSave(upsLegacyData, id);
                return;
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UPSLegacyDataSave() error.", ex);
            }
        }

        public string UPSLegacyDataGet(int upsLegacyDataID)
        {
            try
            {
                return _purchaseBL.UPSLegacyDataGet(upsLegacyDataID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UPSLegacyDataGet() error.", ex);
            }
        }

        public TransactionReasonCollection GetTransactionReasons()
        {
            try
            {
                return _purchaseBL.GetTransactionReasons();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetTransactionReasons() error.", ex);
            }
        }

        public TransactionReasonSiteCollection GetTransactionReasonsSite(Int32 siteID)
        {
            try
            {
                return _purchaseBL.GetTransactionReasonsSite(siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetTransactionReasonsSite() error.", ex);
            }
        }

        public CreditCardCollection GetCreditCardTypes(Int32 siteID)
        {
            try
            {
                return _purchaseBL.GetCreditCardTypes(siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting credit card types.", ex);
            }
        }
        
        #region Obsolete
        [Obsolete]
        public NameValueCollection UPSGetMemberPaymentByOrderID(Int32 memberID, Int32 orderID, Int32 siteID)
        {
            return null;
            /*
            try
            {
                return _purchaseBL.UPSGetMemberPaymentByOrderID(memberID,
                    orderID, siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberPaymentByMemberTranID() error.", ex);
            }
             * */
        }

        [Obsolete]
        public SaveOfferCollection GetSaveOffers(Int32 siteID)
        {
            return null;
            /*
            try
            {
                return _purchaseBL.GetSaveOffers(siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetSaveOffers() error.", ex);
            }
             * */
        }

        [Obsolete]
        public bool HasAcceptedFreeTrial(Int32 planID,
            Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 brandID)
        {
            return false;
            /*
            try
            {
                return _purchaseBL.HasAcceptedFreeTrial(planID,
                    memberID,
                    adminMemberID,
                    siteID,
                    brandID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "HasAcceptedFreeTrial() error.", ex);
            }
             * */
        }

        [Obsolete]
        public bool IsEligibleForSaveOffer(Int32 memberID,
            Int32 siteID)
        {
            return false;
            /*
            try
            {
                return _purchaseBL.IsEligibleForSaveOffer(memberID,
                    siteID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "IsEligibleForSaveOffer() error (memberID: " + memberID.ToString() + ", siteID: " + siteID.ToString() + ").",
                    ex);
            }
             * */
        }

        [Obsolete]
        public DiscountCollection GetDiscounts()
        {
            return null;
            /*
            try
            {
                return _purchaseBL.GetDiscounts();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetDiscounts() error.", ex);
            }
             * */
        }

        [Obsolete]
        public SubscriptionResult SaveDiscount(Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 discountID)
        {
            return null;
            /*
            try
            {
                return _purchaseBL.SaveDiscount(memberID,
                    adminMemberID,
                    siteID,
                    discountID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SaveDiscount() error.", ex);
            }
             * */
        }

        [Obsolete]
		public SubscriptionResult BeginAuthPmtProfile(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 referenceMemberPaymentId)
		{
            return null;

            /* Removed as part of DB Cleanup efforts */
            /*
			return _purchaseBL.BeginAuthPmtProfile(
			memberID,
			adminMemberID,
			siteID,
			brandID,
			firstName,
			lastName,
			israeliID,
			phone,
			addressLine1,
			city,
			state,
			countryRegionID,
			postalCode,
			creditCardType,
			creditCardNumber,
			expirationMonth,
			expirationYear,
			cvc,
			conversionMemberID,
			sourceID,
			purchaseReasonTypeID,
			ipAddress,
			referenceMemberPaymentId);
             * */
		}

        [Obsolete]
		public SubscriptionResult BeginCheckPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 brandID,
			string firstName, 
			string lastName, 
			string phone, 
			string addressLine1, 
			string addressLine2, 
			string city, 
			string state, 
			string postalCode, 
			string driversLicenseNumber, 
			string driversLicenseState, 
			string bankName, 
			string bankRoutingNumber, 
			string checkingAccountNumber, 
			Int32 checkNumber, 
			Int32 conversionMemberID, 
			BankAccountType bankAccountType, 
			Int32 sourceID, 
			Int32 purchaseReasonTypeID,
			string ipAddress)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginCheckPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					phone,
					addressLine1,
					addressLine2,
					city,
					state,
					postalCode,
					driversLicenseNumber,
					driversLicenseState,
					bankName,
					bankRoutingNumber,
					checkingAccountNumber,
					checkNumber,
					conversionMemberID,
					bankAccountType,
					sourceID,
					purchaseReasonTypeID,
					ipAddress);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "BeginCheckPurchase() error.", ex);
			}
             * */
		}

        [Obsolete]
		public SubscriptionResult BeginCheckPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 brandID,
			string firstName, 
			string lastName, 
			string phone, 
			string addressLine1, 
			string addressLine2, 
			string city, 
			string state, 
			string postalCode, 
			string driversLicenseNumber, 
			string driversLicenseState, 
			string bankName, 
			string bankRoutingNumber, 
			string checkingAccountNumber, 
			Int32 checkNumber, 
			Int32 conversionMemberID, 
			BankAccountType bankAccountType, 
			Int32 sourceID, 
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginCheckPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					phone,
					addressLine1,
					addressLine2,
					city,
					state,
					postalCode,
					driversLicenseNumber,
					driversLicenseState,
					bankName,
					bankRoutingNumber,
					checkingAccountNumber,
					checkNumber,
					conversionMemberID,
					bankAccountType,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					UICreditAmount,
					purchaseMode,
					promoID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "BeginCheckPurchase() error.", ex);
			}
             * */
		}

        [Obsolete]
		public SubscriptionResult BeginCheckPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 brandID,
			string firstName, 
			string lastName, 
			string phone, 
			string addressLine1, 
			string addressLine2, 
			string city, 
			string state, 
			string postalCode, 
			string driversLicenseNumber, 
			string driversLicenseState, 
			string bankName, 
			string bankRoutingNumber, 
			string checkingAccountNumber, 
			Int32 checkNumber, 
			Int32 conversionMemberID, 
			BankAccountType bankAccountType, 
			Int32 sourceID, 
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID,
			bool reusePreviousPayment)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.BeginCheckPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					phone,
					addressLine1,
					addressLine2,
					city,
					state,
					postalCode,
					driversLicenseNumber,
					driversLicenseState,
					bankName,
					bankRoutingNumber,
					checkingAccountNumber,
					checkNumber,
					conversionMemberID,
					bankAccountType,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					UICreditAmount,
					purchaseMode,
					promoID,
					reusePreviousPayment);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "BeginCheckPurchase() error.", ex);
			}
             */
		}

        [Obsolete]
		public SubscriptionResult BeginCreditCardPurchase(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.BeginCreditCardPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					israeliID,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardType,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					cvc,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					ipAddress);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning credit card purchase.", ex);
			}
             */
		}

        [Obsolete]
		public SubscriptionResult BeginCreditCardPurchase(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.BeginCreditCardPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					israeliID,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardType,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					cvc,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					promoID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning credit card purchase(Overloaded version).", ex);
			}
             */
		}

        [Obsolete]
		public SubscriptionResult BeginCreditCardPurchase(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.BeginCreditCardPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					israeliID,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardType,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					cvc,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					promoID,
					UICreditAmount,
					purchaseMode);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning credit card purchase(Overloaded version).", ex);
			}
             * */
		}

        [Obsolete]
		public SubscriptionResult BeginCreditCardPurchase(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			bool reusePreviousPayment
			)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginCreditCardPurchase(memberID,
					adminMemberID,
					planID,
					discountID,
					siteID,
					brandID,
					firstName,
					lastName,
					israeliID,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardType,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					cvc,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					promoID,
					UICreditAmount,
					purchaseMode,
					reusePreviousPayment);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning credit card purchase(Overloaded version).", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 BeginCreditCardVerification(Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginCreditCardVerification(memberID,
					siteID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardType,
					creditCardNumber,
					expirationMonth,
					expirationYear);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "BeginCreditCardVerification() error.", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 BeginCredit(TranType tranType,
			Int32 memberID,
			Int32 adminMemberID,
			Int32 siteID,
			Int32 merchantID,
			Int32 referenceMemberTranID,
			Int32 memberPaymentID,
			Int32 planID,
			decimal amount,
			Int32 minutes,
			Int32 duration,
			DurationType durationType,
			CurrencyType currencyType)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginCredit(tranType,
					memberID,
					adminMemberID,
					siteID,
					merchantID,
					referenceMemberTranID,
					memberPaymentID,
					planID,
					amount,
					minutes,
					duration,
					durationType,
					currencyType);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning credit.", ex);
			}
             * */
		}

	    [Obsolete]
		public SubscriptionResult BeginRenewalPurchase(Int32 memberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 communityID,
			Int32 brandID,
			Int32 adminMemberID, 
			Int32 memberPaymentID,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginRenewalPurchase(memberID, 
					planID, 
					discountID, 
					siteID, 
					communityID,
					brandID,
					adminMemberID, 
					memberPaymentID,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					promoID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning renewal purchase.", ex);
			}
             * */
		}

        [Obsolete]
		public SubscriptionResult BeginRenewalPurchase(Int32 memberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 communityID,
			Int32 brandID,
			Int32 adminMemberID, 
			Int32 memberPaymentID,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.BeginRenewalPurchase(memberID, 
					planID, 
					discountID, 
					siteID, 
					communityID,
					brandID,
					adminMemberID, 
					memberPaymentID,
					conversionMemberID,
					sourceID,
					purchaseReasonTypeID,
					ipAddress,
					UICreditAmount,
					purchaseMode,
					promoID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occured while beginning renewal purchase.", ex);
			}
             * */
		}		
		
        [Obsolete]
		public SubscriptionResult EndSubscription(Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID, 
			Int32 transactionReasonID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.EndSubscription(memberID, 
					adminMemberID,
					siteID, 
					transactionReasonID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "EndSubscription() error.", ex);
			}
             * */
		}

        [Obsolete]
		public SubscriptionResult EndALaCarteOnly(int memberID, 
			int adminMemberID,
			int siteID, 
			int planID)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.EndALaCarteOnly(memberID, 
					adminMemberID,
					siteID,
					planID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "EndALaCarteOnly() error.", ex);
			}
             * */
		}

		[Obsolete]
		public MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID)
		{
            return MemberTranStatus.None;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetChargeStatus(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetChargeStatus() error.", ex);
			}
             * */
		}

        [Obsolete]
		public NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetChargeLog(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetChargeLog() error.", ex);
			}
             * */
		}
			
		[Obsolete]
		public CreditMaximum GetCreditMaximum(Int32 memberTranID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetCreditMaximum(memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetCreditMaximum() error.", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 GetMemberIDByMemberTranID(Int32 memberTranID)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetMemberIDByMemberTranID(memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberIDByMemberTranID() error.", ex);
			}
             * */
		}

		[Obsolete]
		public Int32[] GetMemberIDsByAccountNumber(string accountNumber)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetMemberIDsByAccountNumber(accountNumber);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberIDsByAccountNumber() error.", ex);
			}
             * */
		}

	    [Obsolete]
		public NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.GetMemberPayment(memberID, 
					communityID, 
					memberPaymentID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberPayment() error.", ex);
			}
             * */
		}

        [Obsolete]
		public NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.GetMemberPaymentByMemberTranID(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberPaymentByMemberTranID() error.", ex);
			}
             * */
		}
		
		[Obsolete]
		public SiteMerchantCollection GetSiteMerchants(Int32 siteID,
			CurrencyType currencyType,
			PaymentType paymentType)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetSiteMerchants(siteID, 
					currencyType,
					paymentType);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetSiteMerchants() error.", ex);
			}
             * */
		}
			
		[Obsolete]
		public MemberTranCollection GetMemberTransactions(Int32 memberID, 
			Int32 siteID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetMemberTransactions(memberID, 
					siteID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberTranHistory() error.", ex);
			}
             * */
		}

        [Obsolete]
		public MemberSub GetSubscription(Int32 memberID, 
			Int32 siteID)
		{
            return null;
            /*
			try
			{
				return PurchaseBL.GetSubscription(memberID, 
					siteID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetSubscription() error.", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.GetSuccessfulPayment(memberID, 
					siteID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetSuccessfulPayment() error.", ex);
			}
             * */
		}

		[Obsolete]
		public bool HasSubscriptionEnded(Int32 memberID,
			Int32 siteID)
		{
			return false;
		}

	    [Obsolete]
		public SubscriptionResult GetMemberTranStatus(Int32 memberID, 
			Int32 memberTranID)
		{
            return null;
            /*
			try
			{
				return _purchaseBL.GetMemberTranStatus(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberTranStatus() error.", ex);
			}
             * */
		}

		[Obsolete]
		public bool RenderCVC(Int32 siteID, 
			CurrencyType currencyType, 
			PaymentType paymentType)
		{
            return false;
            /*
			try
			{
				return _purchaseBL.RenderCVC(siteID,
					currencyType,
					paymentType);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure determining CVC render status.", ex);
			}
             * */
		}

        [Obsolete]
		public void SaveComplete(MemberTranResult memberTranResult)
		{
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				_purchaseBL.SaveComplete(memberTranResult);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SaveComplete() error (memberTranID: " + memberTranResult.MemberTran.MemberTranID.ToString() + ").", ex);
			}
             * */
		}

		[Obsolete]
		public SubscriptionResult RemovePaymentInfo(Int32 memberID, 
			Int32 siteID)
		{
            return null;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.RemovePaymentInfo(memberID, 
					siteID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemovePaymentInfo() error (memberID: " + memberID.ToString() + ").", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.UpdateSubscription(memberID, 
					siteID, 
					adminMemberID, 
					duration, 
					durationType, 
					TranType.AdministrativeAdjustment, // Hard coded parameter to comply with previous calls
					transactionReasonID, 
					reOpenFlag, 
					planID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UpdateSubscription() error (memberID: " + memberID.ToString() + ").", ex);
			}
             * */
		}
		
        [Obsolete]
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.UpdateSubscription(memberID, 
					siteID, 
					adminMemberID, 
					duration, 
					durationType,
					adjustmentType,
					transactionReasonID, 
					reOpenFlag, 
					planID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UpdateSubscription() error (memberID: " + memberID.ToString() + ").", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand)
		{
            return Int32.MinValue;
            /*
			try
			{
				return _purchaseBL.UpdateSubscription(memberID, 
					siteID, 
					adminMemberID, 
					duration, 
					durationType,
					adjustmentType,
					transactionReasonID, 
					reOpenFlag, 
					planID,
					targetBrand);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UpdateSubscription() error (memberID: " + memberID.ToString() + ").", ex);
			}
             * */
		}

        [Obsolete]
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
			TimeAdjustOverrideType timeAdjustOverrideType)
		{
            return Int32.MinValue;
            /* Removed as part of DB Cleanup efforts */
            /*
			try
			{
				return _purchaseBL.UpdateSubscription(memberID, 
					siteID, 
					adminMemberID, 
					duration, 
					durationType,
					adjustmentType,
					transactionReasonID, 
					reOpenFlag, 
					planID,
					targetBrand,
					timeAdjustOverrideType);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UpdateSubscription() error (memberID: " + memberID.ToString() + ").", ex);
			}
             * */
		}

        [Obsolete]
		public int UpdateALaCarteOnly(int memberID, 
			int siteID, 
			int adminMemberID,  
			TranType adjustmentType,
			int transactionReasonID, 
			bool reOpenFlag, 
			int planID,
			int planIDToReplace,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
			bool insertTransactionRecordFlag)
		{
            return Int32.MinValue;
            /*
			try
			{
				return _purchaseBL.UpdateALaCarteOnly(memberID,
					siteID,
					adminMemberID,
					adjustmentType,
					transactionReasonID,
					reOpenFlag,
					planID,
					planIDToReplace,
					targetBrand,
					insertTransactionRecordFlag);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UpdateALaCarteOnly() error (memberID: " + memberID.ToString() + ").", ex);
			}
             * */
		}

        [Obsolete]
		public void UpdateMemberPayment(Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType)
		{
            /*
			try
			{
				_purchaseBL.UpdateMemberPayment(memberID,
					siteID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					creditCardType);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UpdateMemberPayment() error.", ex);
			}
             * */
		}

        [Obsolete]
		public MemberTranCreditCollection GetRemainingCredit(Int32 memberID, 
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
            return null;
            /*
			try
			{
				return PurchaseBL.GetRemainingCredit(memberID, brand);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetRemainingCredit() error.", ex);
			}
             * */
		}

        [Obsolete]
		public bool HasValidCCOnFile(int memberID, int brandID)
		{
            return false;
            /*
			try
			{
				return _purchaseBL.HasValidCCOnFile(memberID, brandID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "HasValidCCOnFile() error.", ex);
			}
             * */
		}
		
		/// <summary>
		/// A hole to call Payment to generate the Encore Sales File
		/// </summary>
		/// <returns></returns>
        [Obsolete]
		public int GenerateEncryptedSalesFile()
		{
            return Int32.MinValue;
            /*
			try
			{
				return _purchaseBL.GenerateEncryptedSalesFile();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GenerateEncryptedSalesFile() error.", ex);
			}
             * */
		}
		/// <summary>
		/// A hole to call Payment to process the Encore Membership File
		/// </summary>
        [Obsolete]
		public void ProcessMembershipFile()
		{
            /*
			try
			{
				_purchaseBL.ProcessMembershipFile();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "ProcessMembershipFile() error.", ex);
			}
             * */
		}

        [Obsolete]
		public MatchbackCollection GetMatbackRequest()
		{
            return null;
            /*
			try
			{
				return _purchaseBL.GetMatbackRequest();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMatbackRequest() error.", ex);
			}
             * */
		}

        [Obsolete]
		public int StartMatchbackFileGeneration(string outputFileName)
		{
            return Int32.MinValue;
            /*
			try
			{
				return _purchaseBL.StartMatchbackFileGeneration(outputFileName);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "StartMatchbackFileGeneration() error.", ex);
			}
             * */
		}

        [Obsolete]
		public void InsertMatchbackFile(byte[] file, int lineID)
		{
            /*
			try
			{
				_purchaseBL.InsertMatchbackFile(file, lineID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "InsertMatchbackFile() error.", ex);
			}
             * */
		}

        [Obsolete]
		public void MarkMatchbackRequestAsComplete(MatchbackCollection mbCol)
		{
            /*
			try
			{
				_purchaseBL.MarkMatchbackRequestAsComplete(mbCol);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "MarkMatchbackRequestAsComplete() error.", ex);
			}
             * */
        }

        [Obsolete]
        public Int32 UPSProcessLegacyData(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            CreditCardType creditCardType,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            TranType tranType,
            Int32 referenceMemberPaymentID,
            CreditCardType referenceCreditCardType,
            decimal amount,
            Int32 duration,
            DurationType durationType,
            CurrencyType currencyType,
            string lastFourAccountNumber,
            Int32 orderID,
            Int32 referenceOrderID,
            int commonResponseCode,
            Int32 memberTranStatusID,
            DateTime originalInsertDate,
            Int32 chargeID,
            OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS)
        {
            try
            {
                return UPSProcessLegacyData(memberID,
                    adminMemberID,
                    planID,
                    discountID,
                    siteID,
                    brandID,
                    creditCardType,
                    conversionMemberID,
                    sourceID,
                    purchaseReasonTypeID,
                    ipAddress,
                    promoID,
                    UICreditAmount,
                    purchaseMode,
                    reusePreviousPayment,
                    firstName,
                    lastName,
                    tranType,
                    referenceMemberPaymentID,
                    referenceCreditCardType,
                    amount,
                    duration,
                    durationType,
                    currencyType,
                    lastFourAccountNumber,
                    orderID,
                    referenceOrderID,
                    commonResponseCode,
                    memberTranStatusID,
                    originalInsertDate,
                    chargeID,
                    originalPurchaseActionTypeForUPS,
                    0);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UPSProcessLegacyData() error.", ex);
            }
        }

        [Obsolete]
        public Int32 UPSProcessLegacyData(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            CreditCardType creditCardType,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            TranType tranType,
            Int32 referenceMemberPaymentID,
            CreditCardType referenceCreditCardType,
            decimal amount,
            Int32 duration,
            DurationType durationType,
            CurrencyType currencyType,
            string lastFourAccountNumber,
            Int32 orderID,
            Int32 referenceOrderID,
            int commonResponseCode,
            Int32 memberTranStatusID,
            DateTime originalInsertDate,
            Int32 chargeID,
            OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS,
            Int32 paymentTypeID)
        {
            return Int32.MinValue;
            /*
            try
            {
                return _purchaseBL.UPSProcessLegacyData(memberID,
                            adminMemberID,
                            planID,
                            discountID,
                            siteID,
                            brandID,
                            creditCardType,
                            conversionMemberID,
                            sourceID,
                            purchaseReasonTypeID,
                            ipAddress,
                            promoID,
                            UICreditAmount,
                            purchaseMode,
                            reusePreviousPayment,
                            firstName,
                            lastName,
                            tranType,
                            referenceMemberPaymentID,
                            referenceCreditCardType,
                            amount,
                            duration,
                            durationType,
                            currencyType,
                            lastFourAccountNumber,
                            orderID,
                            referenceOrderID,
                            commonResponseCode,
                            memberTranStatusID,
                            originalInsertDate,
                            chargeID,
                            originalPurchaseActionTypeForUPS,
                            paymentTypeID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "UPSProcessLegacyData() error.", ex);
            }
             * */
        }

        #endregion
    }
}
