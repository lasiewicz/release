using System;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Purchase.BusinessLogic;
using Matchnet.Purchase.ServiceDefinitions;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Replication;


namespace Matchnet.Purchase.ServiceManagers
{
	public class PlanSM : MarshalByRefObject, IPlanService, IPlanAdminService, IReplicationActionRecipient, IBackgroundProcessor
	{
		private Replicator _replicator = null;
		private HydraWriter _hydraWriter;

		public PlanSM()
		{
			_hydraWriter = new HydraWriter(new string[]{"mnSystem"});

			// Initialize the replicator
			PlanBL.Instance.ReplicationRequested += new PlanBL.ReplicationEventHandler(PlanBL_ReplicationRequested);
			
			string replicationURI = string.Empty;
			string machineName = System.Environment.MachineName;
            string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }

			// Check to see if there is a replication override
			replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PURCHASESVC_REPLICATION_OVERRIDE");
			if(replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, ServiceConstants.SERVICE_MANAGER_NAME_PLAN, machineName);
			}

            if (!String.IsNullOrEmpty(replicationURI))
            {
                // Although the replicator is related to PlanSM, I'm just using the svc constant for Purchase
                _replicator = new Replicator(ServiceConstants.SERVICE_NAME);
                _replicator.SetDestinationUri(replicationURI);
                System.Diagnostics.Trace.WriteLine("PlanSM contructor, replicator setup completed.");
            }
            else
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "PlanSM constructor, replicator partner URL is null. Replicator not started.", System.Diagnostics.EventLogEntryType.Warning);
            }
		}

		private void PlanBL_ReplicationRequested(IReplicationAction replicationAction)
		{
			System.Diagnostics.Debug.WriteLine("_replicator.Enqueue.");
			_replicator.Enqueue(replicationAction);
		}

		/// <summary>
		/// Insert a plan for a given brand ID.
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="plan"></param>
		public Int32 InsertPlan(Int32 brandID, Plan plan)
		{
			try
			{
                return PlanBL.Instance.InsertPlan(brandID, plan);				
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting plan.", ex);
			}
		}

		#region IBackgroundProcessor Members

		public void Start()
		{
			if (_hydraWriter != null)
				_hydraWriter.Start();
			
			_replicator.Start();
		}

		public void Stop()
		{
			if (_hydraWriter != null)
				_hydraWriter.Stop();

			_replicator.Stop();
		}

		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}


		public PlanCollection GetPlans(Int32 brandID,
			PlanType planTypeMask,
			PaymentType paymentTypeMask)
		{
			try
			{
				return PlanBL.Instance.GetPlans(brandID,
					planTypeMask,
					paymentTypeMask);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPlans() error.", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="initialDuration"></param>
		/// <param name="durationType"></param>
		/// <param name="premiumType"></param>
		/// <returns></returns>
		public PlanCollection GetPlans(Int32 brandID,
			PlanType planTypeMask,
			PaymentType paymentTypeMask,
			int initialDuration,
			DurationType durationType,
			PremiumType premiumType)
		{
			try
			{
				return PlanBL.Instance.GetPlans(brandID,
					planTypeMask,
					paymentTypeMask,
					initialDuration,
					durationType,
					premiumType);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPlans() error.", ex);
			}
		}

//		/// <summary>
//		/// Gets a list of plans for admins with UI metadata.  Plans are filtered to only return SiteDefault plans, and
//		/// plan start/end dates are not taken into account so admins may view plans that are technically expired.
//		/// </summary>
//		/// <param name="brand"></param>
//		/// <param name="paymentTypeMask"></param>
//		/// <param name="memberID"></param>
//		/// <returns></returns>
//		public PlanCollection GetAdminDisplayedPlans(Int32 brandID,
//			PaymentType paymentTypeMask,
//			Int32 memberID)
//		{
//			//this version is kept to work as the original GetAdminDisplayedPlans; showSiteDefaultPlans = true, allowAllColumnOrders = false
//			return GetAdminDisplayedPlans(brandID, paymentTypeMask, memberID, true, false);
//		}

//		/// <summary>
//		/// Gets a list of plans for admins with UI metadata.  Plans may be filtered to only return SiteDefaults or ColumnOrders > 99
//		/// depending on the corresponding parameters.  Plan start/end dates are not taken into account so admins may view plans that 
//		/// are technically expired.
//		/// </summary>
//		/// <param name="brand"></param>
//		/// <param name="paymentTypeMask"></param>
//		/// <param name="memberID"></param>
//		/// <param name="showSiteDefaultPlansOnly">Determines whether to filter plans to only return SiteDefaults</param>
//		/// <param name="allowAllColumnOrders">Determines whether to filter plans with ColumnOrders > 99</param>
//		/// <returns></returns>
//		public PlanCollection GetAdminDisplayedPlans(Int32 brandID,
//			PaymentType paymentTypeMask,
//			Int32 memberID,
//			bool showSiteDefaultPlansOnly,
//			bool allowAllColumnOrders)
//		{
//			try
//			{
//				return PlanBL.Instance.GetAdminDisplayedPlans(brandID, paymentTypeMask, memberID, showSiteDefaultPlansOnly, allowAllColumnOrders);
//			}
//			catch(Exception ex) 
//			{
//				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetAdminDisplayedPlans() error.", ex);
//			}
//		}

//		public PlanCollection GetMemberDisplayedPlans(Int32 brandID,
//			PlanType planTypeMask,
//			PaymentType paymentTypeMask,
//			Int32 memberID)
//		{
//			try
//			{
//				return PlanBL.Instance.GetMemberDisplayedPlans(brandID,
//					planTypeMask,
//					paymentTypeMask,
//					memberID);
//			}
//			catch(Exception ex) 
//			{
//				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberDisplayedPlans() error.", ex);
//			}
//		}

		public string GetPlanDescription(Int32 planID)
		{
			try
			{
				return PlanBL.Instance.GetPlanDescription(planID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPlanDescription() error.", ex);
			}
		}

			
		public Plan GetPlan(Int32 planID, 
			Int32 brandID)
		{
			try
			{
				return PlanBL.Instance.GetPlans(brandID).FindByID(planID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPlan() error.", ex);
			}
		}
		/// <summary>
		/// Overload method 
		/// </summary>
		/// <param name="planID"></param>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <returns></returns>
		public Plan GetPlan(Int32 planID,Int32 brandID,	PlanType planTypeMask,PaymentType paymentTypeMask)
		{
			try
			{
				return PlanBL.Instance.GetPlans(brandID,planTypeMask,paymentTypeMask).FindByID(planID);

			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPlan()(overload) error.", ex);
			}
		}

		public Plan GetFreeTrialGroupPlan(FreeTrialGroupType freeTrialGroupType, 
			Int32 brandID)
		{
			try
			{
				return PlanBL.Instance.GetFreeTrialGroupPlan(freeTrialGroupType,
					brandID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetFreeTrialGroupPlan() error.", ex);
			}
		}

		/// <summary>
		/// This is not implemented.
		/// </summary>
		/// <param name="planID"></param>
		/// <param name="paymentType"></param>
		/// <param name="resourceConstant"></param>
		public void InsertPlanResource(Int32 planID, 
			PaymentType paymentType, 
			string resourceConstant)
		{
			try
			{
				//PlanBL.Instance.InsertPlanResource(planID, paymentType, resourceConstant);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting plan resource.", ex);
			}
		}

		/// <summary>
		/// This is not implemented.
		/// </summary>
		/// <param name="planID"></param>
		/// <param name="brandID"></param>
		/// <param name="currencyType"></param>
		/// <param name="resourceConstant"></param>
		/// <param name="initialCost"></param>
		/// <param name="initialDuration"></param>
		/// <param name="initialDurationType"></param>
		/// <param name="renewCost"></param>
		/// <param name="renewDuration"></param>
		/// <param name="renewDurationType"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="renewAttempts"></param>
		/// <param name="bestValueFlag"></param>
		/// <param name="listOrder"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		public void UpdatePlan(Int32 planID, 
			Int32 brandID, 
			CurrencyType currencyType, 
			string resourceConstant, 
			decimal initialCost, 
			Int32 initialDuration, 
			DurationType initialDurationType, 
			decimal renewCost, 
			Int32 renewDuration, 
			DurationType renewDurationType, 
			PlanType planTypeMask, 
			Int32 renewAttempts, 
			bool bestValueFlag, 
			Int32 listOrder, 
			PaymentType paymentTypeMask, 
			DateTime startDate, 
			DateTime endDate)
		{
			try
			{
				/*
				PlanBL.Instance.UpdatePlan(
					planID,
					brandID,
					currencyType,
					resourceConstant,
					initialCost,
					initialDuration,
					initialDurationType,
					renewCost,
					renewDuration,
					renewDurationType,
					planTypeMask,
					renewAttempts,
					bestValueFlag,
					listOrder,
					paymentTypeMask,
					startDate,
					endDate);
				*/
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure updating plan.", ex);
			}
		}


		public void UpdatePlanResource(Int32 planID, 
			PaymentType paymentType, 
			string resourceConstant)
		{
			try
			{
				//PlanBL.Instance.UpdatePlanResource(planID, paymentType, resourceConstant);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure updating plan resource.", ex);
			}
		}
		
		/// <summary>
		/// This is not implemented.
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="currencyType"></param>
		/// <param name="resourceConstant"></param>
		/// <param name="initialCost"></param>
		/// <param name="initialDuration"></param>
		/// <param name="initialDurationType"></param>
		/// <param name="renewCost"></param>
		/// <param name="renewDuration"></param>
		/// <param name="renewDurationType"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="renewAttempts"></param>
		/// <param name="bestValueFlag"></param>
		/// <param name="listOrder"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <returns></returns>
		public Int32 InsertPlan(Int32 brandID, 
			CurrencyType currencyType, 
			string resourceConstant, 
			decimal initialCost, 
			Int32 initialDuration, 
			DurationType initialDurationType, 
			decimal renewCost, 
			Int32 renewDuration, 
			DurationType renewDurationType, 
			PlanType planTypeMask, 
			Int32 renewAttempts, 
			bool bestValueFlag, 
			Int32 listOrder, 
			PaymentType paymentTypeMask, 
			DateTime startDate, 
			DateTime endDate)
		{
			try
			{
				/*
				return PlanBL.Instance.InsertPlan(
					brandID,
					currencyType,
					resourceConstant,
					initialCost,
					initialDuration,
					initialDurationType,
					renewCost,
					renewDuration,
					renewDurationType,
					planTypeMask,
					renewAttempts,
					bestValueFlag,
					listOrder,
					paymentTypeMask,
					startDate,
					endDate);
				*/

				return Constants.NULL_INT;
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting plan.", ex);
			}
		}

		PlanCollection IPlanAdminService.GetPlans(Int32 brandID)
		{
			try
			{
				return PlanBL.Instance.GetAdminPlans(brandID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Admin plans.", ex);
			}
		}

		PlanCollection IPlanAdminService.GetPlans(Int32 brandID, PaymentType paymentTypeMask)
		{
			try
			{
				return PlanBL.Instance.GetAdminPlans(brandID, paymentTypeMask);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Admin plans by paymenttype.", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="plan"></param>
		/// <param name="remainingCreditAmount"></param>
		/// <returns></returns>
		public bool IsPurchaseAllowedWithRemainingCredit(Plan plan, decimal remainingCreditAmount)
		{
			try
			{
				return PlanBL.Instance.IsPurchaseAllowedWithRemainingCredit(plan, remainingCreditAmount);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "IsPurchaseAllowedWithRemainingCredit() error.", ex);
			}
		}

//		public PlanCollection SetupPlanCollectionForDisplay(PlanCollection sourcePlans, 
//			Int32 brandID, 
//			Int32 bestValuePlanID,
//			Int32 memberID)
//		{
//			try
//			{
//				// Currently used by promo engine
//				// Promo engine returns a promotional plan collection.
//				// This should not filter promotional plans.    
//				// This plan collection will need to have plan collection display meta data
//				// in order to display.  In addition, promotions have different best value 
//				// plans within its plan collection so set one plan as the best value plan.  
//				PlanBL.Instance.ResetBestValuePlanInPlanCollection(sourcePlans, bestValuePlanID);
//				return PlanBL.Instance.PreparePlanCollectionForDisplay(sourcePlans, brandID, false, memberID, false, false);
//			}
//			catch(Exception ex) 
//			{
//				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetupPlanCollectionForDisplay() error.", ex);
//			}
//		}
		#region IReplicationActionRecipient Members

		public void Receive(IReplicationAction replicationAction)
		{
			try
			{
				PlanBL.Instance.PlayReplicationAction(replicationAction);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure processing replication object - (PlanCollection).", ex);
			}
		}

		#endregion

		
	}
}
