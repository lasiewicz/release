using System;
using System.Collections.Specialized;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceDefinitions;


namespace Matchnet.Purchase.ServiceAdapters
{
	public class PurchaseSA : SABase
	{
		#region constants
			private const string MEMBER_SUB_STATUS_CACHE_KEY_PREFIX = "~MEMBERSUBSTATUS^{0}{1}";
		#endregion
		
		private Cache _cache = null;		
		public static readonly PurchaseSA Instance = new PurchaseSA();
		
		
		private PurchaseSA()
		{
			_cache = Cache.Instance;
		}		

		public MemberSubscriptionStatus GetMemberSubStatus(int memberid, int siteid,  DateTime memberSubEndDate)
		{
			return GetMemberSubStatus(memberid, siteid, memberSubEndDate, true);
		}
		
		/// <summary>
		/// Returns an object which contains the subscription status of member 
		/// </summary>
		/// <param name="memberid"></param>
		/// <param name="siteid"></param>
		/// <param name="memberSubEndDate"></param>
		/// <returns></returns>
		public MemberSubscriptionStatus GetMemberSubStatus(int memberid, int siteid,  DateTime memberSubEndDate, bool useCache)
		{
			MemberSubscriptionStatus oResult = null;
			string uri = "";

			try 
			{	
				// validate input parameters
				// try to retrieve from cache
				if (useCache)
				{
					oResult = _cache.Get(GetMemberSubStatusKey(memberid,siteid)) as MemberSubscriptionStatus;
				}

				if(oResult == null) 
				{
					uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);					
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetMemberSubStatus(memberid,siteid,memberSubEndDate);
					}
					finally
					{
						base.Checkin(uri);
					}

					//if(oResult != null && useCache) 
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetMemberSubStatusKey(memberid,siteid);
						CacheMemberSubStatus(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve member subscription status(uri: " + uri + "). MemID="+ memberid.ToString()+ " ,SiteID=" + siteid.ToString() , ex));
			}
		}
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PURCHASESVC_SA_CONNECTION_LIMIT"));
		}

		public bool HasAcceptedFreeTrial(Int32 planID, 
			Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID,
			Int32 brandID)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				base.Checkout(uri);
				try
				{
					return getService(uri).HasAcceptedFreeTrial(
						planID, 
						memberID, 
						adminMemberID,
						siteID,
						brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("HasAcceptedFreeTrial() error (uri: " + uri + ").", ex);
			}
		}
		
		public TransactionReasonCollection GetTransactionReasons()
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				return getService(uri).GetTransactionReasons();
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot get transation reasons (uri: " + uri + ")", ex);
			}
		}

		public TransactionReasonSiteCollection GetTransactionReasonsSite(Int32 siteID)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				return getService(uri).GetTransactionReasonsSite(siteID);
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot get site transation reasons (uri: " + uri + ")", ex);
			}
		}

		public CreditCardCollection GetCreditCardTypes(Int32 siteID)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				return getService(uri).GetCreditCardTypes(siteID);
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot get credit card types (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="plan"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public PurchaseMode GetPurchaseMode(Int32 memberID, 
			Plan plan, 
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				return getService(uri).GetPurchaseMode(memberID, plan, brand);
			}
			catch (Exception ex)
			{
				throw new SAException("GetPurchaseMode() error (uri: " + uri + ").", ex);
			}
		}

        public void UPSLegacyDataSave(string upsLegacyData, int id)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                getService(uri).UPSLegacyDataSave(upsLegacyData, id);
                return;
			}
			catch (Exception ex)
			{
				throw new SAException("UPSLegacyDataSave() error (uri: " + uri + ").", ex);
			}
		}

		/// <summary>
		/// UPSLegacyDataGet
		/// </summary>
		/// <param name="upsLegacyDataID"></param>
		/// <returns></returns>
		public string UPSLegacyDataGet(int upsLegacyDataID)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				return getService(uri).UPSLegacyDataGet(upsLegacyDataID);
			}
			catch (Exception ex)
			{
				throw new SAException("UPSLegacyDataGet() error (uri: " + uri + ").", ex);
			}
		}

        private IPurchaseService getService(string uri)
        {
            try
            {
                return (IPurchaseService)Activator.GetObject(typeof(IPurchaseService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        #region Deprecated
        [Obsolete("Obsolete - Do not use")]
        public Int32 UPSProcessLegacyData(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            CreditCardType creditCardType,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            TranType tranType,
            Int32 referenceMemberPaymentID,
            CreditCardType referenceCreditCardType,
            decimal amount,
            Int32 duration,
            DurationType durationType,
            CurrencyType currencyType,
            string lastFourAccountNumber,
            Int32 orderID,
            Int32 referenceOrderID,
            int commonResponseCode,
            Int32 memberTranStatusID,
            DateTime originalInsertDate,
            Int32 chargeID,
            OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS)
        {
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return UPSProcessLegacyData(memberID,
                            adminMemberID,
                            planID,
                            discountID,
                            siteID,
                            brandID,
                            creditCardType,
                            conversionMemberID,
                            sourceID,
                            purchaseReasonTypeID,
                            ipAddress,
                            promoID,
                            UICreditAmount,
                            purchaseMode,
                            reusePreviousPayment,
                            firstName,
                            lastName,
                            tranType,
                            referenceMemberPaymentID,
                            referenceCreditCardType,
                            amount,
                            duration,
                            durationType,
                            currencyType,
                            lastFourAccountNumber,
                            orderID,
                            referenceOrderID,
                            commonResponseCode,
                            memberTranStatusID,
                            originalInsertDate,
                            chargeID,
                            originalPurchaseActionTypeForUPS,
                            0);
            }
            catch (Exception ex)
            {
                throw new SAException("UPSProcessLegacyData() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 UPSProcessLegacyData(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            CreditCardType creditCardType,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            TranType tranType,
            Int32 referenceMemberPaymentID,
            CreditCardType referenceCreditCardType,
            decimal amount,
            Int32 duration,
            DurationType durationType,
            CurrencyType currencyType,
            string lastFourAccountNumber,
            Int32 orderID,
            Int32 referenceOrderID,
            int commonResponseCode,
            Int32 memberTranStatusID,
            DateTime originalInsertDate,
            Int32 chargeID,
            OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS,
            Int32 paymentTypeID)
        {
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UPSProcessLegacyData(memberID,
                    adminMemberID,
                    planID,
                    discountID,
                    siteID,
                    brandID,
                    creditCardType,
                    conversionMemberID,
                    sourceID,
                    purchaseReasonTypeID,
                    ipAddress,
                    promoID,
                    UICreditAmount,
                    purchaseMode,
                    reusePreviousPayment,
                    firstName,
                    lastName,
                    tranType,
                    referenceMemberPaymentID,
                    referenceCreditCardType,
                    amount,
                    duration,
                    durationType,
                    currencyType,
                    lastFourAccountNumber,
                    orderID,
                    referenceOrderID,
                    commonResponseCode,
                    memberTranStatusID,
                    originalInsertDate,
                    chargeID,
                    originalPurchaseActionTypeForUPS,
                    paymentTypeID);
            }
            catch (Exception ex)
            {
                throw new SAException("UPSProcessLegacyData() error (uri: " + uri + ").", ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        [Obsolete("Do not use, call UPS instead")]
        public MemberTranCreditCollection GetRemainingCredit(Int32 memberID,
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetRemainingCredit(memberID, brand);
            }
            catch (Exception ex)
            {
                throw new SAException("GetRemainingCredit() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public MemberTranCollection GetMemberTransactions(Int32 memberID,
            Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberTransactions(
                        memberID,
                        siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("GetMemberTranHistory() (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public MemberSub GetSubscription(Int32 memberID,
            Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetSubscription(
                    memberID,
                    siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("GetSubscription() (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public bool HasSubscriptionEnded(Int32 memberID,
            Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).HasSubscriptionEnded(memberID,
                        siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot determine subscription status (uri: " + uri + ")", ex);
            }
        }	

        [Obsolete("Obsolete - Do not use")]
        public Int32 GetSuccessfulPayment(Int32 memberID,
            Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetSuccessfulPayment(
                    memberID,
                    siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot get successful payment (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public DiscountCollection GetDiscounts()
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetDiscounts();
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot get discounts (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public bool RenderCVC(Int32 siteID,
            CurrencyType currencyType,
            PaymentType paymentType)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).RenderCVC(siteID,
                    currencyType,
                    paymentType);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot determin CVC render status (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public void SaveComplete(MemberTranResult memberTranResult)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                getService(uri).SaveComplete(memberTranResult);
            }
            catch (Exception ex)
            {
                throw new SAException("SaveComplete error (uri: " + uri + ", memberTranID: " + memberTranResult.MemberTran.MemberTranID.ToString() + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult SaveDiscount(Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 discountID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).SaveDiscount(memberID,
                    adminMemberID,
                    siteID,
                    discountID);
            }
            catch (Exception ex)
            {
                throw new SAException("SaveDiscount() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult SaveSaveOffer(Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 saveOfferID)
        {
            throw new SAException("Obsolete - Do not use");
            try
            {
                SaveOffer saveOffer = GetSaveOffers(siteID).FindByID(saveOfferID);

                if (saveOffer != null)
                {
                    return SaveDiscount(
                        memberID,
                        adminMemberID,
                        siteID,
                        saveOffer.DiscountID);
                }
                else
                {
                    throw new Exception("Cannot find save offer ID:" + saveOfferID.ToString());
                }
            }
            catch (Exception ex)
            {
                throw new SAException("SaveSaveOffer() error.", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public void UpdateMemberPayment(Int32 memberID,
            Int32 siteID,
            string firstName,
            string lastName,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear,
            CreditCardType creditCardType)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                getService(uri).UpdateMemberPayment(memberID,
                    siteID,
                    firstName,
                    lastName,
                    phone,
                    addressLine1,
                    city,
                    state,
                    countryRegionID,
                    postalCode,
                    creditCardNumber,
                    expirationMonth,
                    expirationYear,
                    creditCardType);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateMemberPayment() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult EndSubscription(Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 transactionReasonID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).EndSubscription(
                    memberID,
                    adminMemberID,
                    siteID,
                    transactionReasonID);
            }
            catch (Exception ex)
            {
                throw new SAException("EndSubscription error (uri: " + uri + ")", ex);
            }
        }

        /// <summary>
        /// New API as part of UPS Integration to enable/disable renewal for A La Carte item (MemberSubExtended table)
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="siteID"></param>
        /// <param name="planID"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult EndALaCarteOnly(int memberID,
            int adminMemberID,
            int siteID,
            int planID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).EndALaCarteOnly(
                    memberID,
                    adminMemberID,
                    siteID,
                    planID);
            }
            catch (Exception ex)
            {
                throw new SAException("EndALaCarteOnly error (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public MemberTranStatus GetChargeStatus(Int32 memberID,
            Int32 memberTranID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetChargeStatus(memberID, memberTranID);
            }
            catch (Exception ex)
            {
                throw new SAException("GetChargeStatus() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult GetMemberTranStatus(Int32 memberID,
            Int32 memberTranID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberTranStatus(
                        memberID,
                        memberTranID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("GetMemberTranStatus() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SaveOfferCollection GetSaveOffers(Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetSaveOffers(siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("GetSaveOffers() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SiteMerchantCollection GetSiteMerchants(Int32 siteID,
            CurrencyType currencyType,
            PaymentType paymentType)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetSiteMerchants(siteID,
                    currencyType,
                    paymentType);
            }
            catch (Exception ex)
            {
                throw new SAException("GetMemberTranHistory() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public bool HasValidCCOnFile(int memberID, int brandID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).HasValidCCOnFile(memberID, brandID);
            }
            catch (Exception ex)
            {
                throw new SAException("HasValidCCOnFile() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
            Int32 memberTranID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberPaymentByMemberTranID(
                        memberID,
                        memberTranID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot get member payment by member tran ID (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public NameValueCollection GetMemberPayment(Int32 memberID,
            Int32 communityID,
            Int32 memberPaymentID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetMemberPayment(memberID,
                        communityID,
                        memberPaymentID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot get member payment (uri: " + uri + ")", ex);
            }
        }

        /// <summary>
        /// Method used for displaying payment information for UPS Subscription Confirmation page.
        /// </summary>
        /// <param name="memberID">Member ID</param>
        /// <param name="memberTranID">UPS Order ID</param>
        /// <param name="siteID">Site ID</param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public NameValueCollection UPSGetMemberPaymentByOrderID(Int32 memberID, Int32 orderID, Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).UPSGetMemberPaymentByOrderID(memberID, orderID, siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot get member payment (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginAuthPmtProfile(
            Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string israeliID,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            CreditCardType creditCardType,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear,
            string cvc,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 referenceMemberPaymentId)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginAuthPmtProfile(memberID,
                        adminMemberID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        israeliID,
                        phone,
                        addressLine1,
                        city,
                        state,
                        countryRegionID,
                        postalCode,
                        creditCardType,
                        creditCardNumber,
                        expirationMonth,
                        expirationYear,
                        cvc,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        referenceMemberPaymentId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("BeginCheckPurchase() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCheckPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string phone,
            string addressLine1,
            string addressLine2,
            string city,
            string state,
            string postalCode,
            string driversLicenseNumber,
            string driversLicenseState,
            string bankName,
            string bankRoutingNumber,
            string checkingAccountNumber,
            Int32 checkNumber,
            Int32 conversionMemberID,
            BankAccountType bankAccountType,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCheckPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        phone,
                        addressLine1,
                        addressLine2,
                        city,
                        state,
                        postalCode,
                        driversLicenseNumber,
                        driversLicenseState,
                        bankName,
                        bankRoutingNumber,
                        checkingAccountNumber,
                        checkNumber,
                        conversionMemberID,
                        bankAccountType,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("BeginCheckPurchase() error (uri: " + uri + ").", ex);
            }
        }

        /// <summary>
        /// Overload method for BeginCheckPurchase()
        /// Provides credit amount and purchase mode
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="planID"></param>
        /// <param name="discountID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phone"></param>
        /// <param name="addressLine1"></param>
        /// <param name="addressLine2"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <param name="driversLicenseNumber"></param>
        /// <param name="driversLicenseState"></param>
        /// <param name="bankName"></param>
        /// <param name="bankRoutingNumber"></param>
        /// <param name="checkingAccountNumber"></param>
        /// <param name="checkNumber"></param>
        /// <param name="conversionMemberID"></param>
        /// <param name="bankAccountType"></param>
        /// <param name="sourceID"></param>
        /// <param name="purchaseReasonTypeID"></param>
        /// <param name="ipAddress"></param>
        /// <param name="UICreditAmount"></param>
        /// <param name="purchaseMode"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCheckPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string phone,
            string addressLine1,
            string addressLine2,
            string city,
            string state,
            string postalCode,
            string driversLicenseNumber,
            string driversLicenseState,
            string bankName,
            string bankRoutingNumber,
            string checkingAccountNumber,
            Int32 checkNumber,
            Int32 conversionMemberID,
            BankAccountType bankAccountType,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            Int32 promoID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCheckPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        phone,
                        addressLine1,
                        addressLine2,
                        city,
                        state,
                        postalCode,
                        driversLicenseNumber,
                        driversLicenseState,
                        bankName,
                        bankRoutingNumber,
                        checkingAccountNumber,
                        checkNumber,
                        conversionMemberID,
                        bankAccountType,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        UICreditAmount,
                        purchaseMode,
                        promoID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("BeginCheckPurchase() error (uri: " + uri + ").", ex);
            }
        }


        /// <summary>
        /// Overload method for BeginCheckPurchase()
        /// Provides credit amount and purchase mode
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="planID"></param>
        /// <param name="discountID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phone"></param>
        /// <param name="addressLine1"></param>
        /// <param name="addressLine2"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <param name="driversLicenseNumber"></param>
        /// <param name="driversLicenseState"></param>
        /// <param name="bankName"></param>
        /// <param name="bankRoutingNumber"></param>
        /// <param name="checkingAccountNumber"></param>
        /// <param name="checkNumber"></param>
        /// <param name="conversionMemberID"></param>
        /// <param name="bankAccountType"></param>
        /// <param name="sourceID"></param>
        /// <param name="purchaseReasonTypeID"></param>
        /// <param name="ipAddress"></param>
        /// <param name="UICreditAmount"></param>
        /// <param name="purchaseMode"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCheckPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string phone,
            string addressLine1,
            string addressLine2,
            string city,
            string state,
            string postalCode,
            string driversLicenseNumber,
            string driversLicenseState,
            string bankName,
            string bankRoutingNumber,
            string checkingAccountNumber,
            Int32 checkNumber,
            Int32 conversionMemberID,
            BankAccountType bankAccountType,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            Int32 promoID,
            bool reusePreviousPayment
            )
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCheckPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        phone,
                        addressLine1,
                        addressLine2,
                        city,
                        state,
                        postalCode,
                        driversLicenseNumber,
                        driversLicenseState,
                        bankName,
                        bankRoutingNumber,
                        checkingAccountNumber,
                        checkNumber,
                        conversionMemberID,
                        bankAccountType,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        UICreditAmount,
                        purchaseMode,
                        promoID,
                        reusePreviousPayment);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("BeginCheckPurchase() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string israeliID,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            CreditCardType creditCardType,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear,
            string cvc,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCreditCardPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        israeliID,
                        phone,
                        addressLine1,
                        city,
                        state,
                        countryRegionID,
                        postalCode,
                        creditCardType,
                        creditCardNumber,
                        expirationMonth,
                        expirationYear,
                        cvc,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin credit card purchase (uri: " + uri + ")", ex);
            }
        }

        /// <summary>
        /// Overload method for BeginCreditCardPurchase(), this method captures the PromoID into MemberTran table.	3/27/2008-RB
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="planID"></param>
        /// <param name="discountID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="israeliID"></param>
        /// <param name="phone"></param>
        /// <param name="addressLine1"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="countryRegionID"></param>
        /// <param name="postalCode"></param>
        /// <param name="creditCardType"></param>
        /// <param name="creditCardNumber"></param>
        /// <param name="expirationMonth"></param>
        /// <param name="expirationYear"></param>
        /// <param name="cvc"></param>
        /// <param name="conversionMemberID"></param>
        /// <param name="sourceID"></param>
        /// <param name="purchaseReasonTypeID"></param>
        /// <param name="ipAddress"></param>
        /// <param name="promoID"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string israeliID,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            CreditCardType creditCardType,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear,
            string cvc,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCreditCardPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        israeliID,
                        phone,
                        addressLine1,
                        city,
                        state,
                        countryRegionID,
                        postalCode,
                        creditCardType,
                        creditCardNumber,
                        expirationMonth,
                        expirationYear,
                        cvc,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin credit card purchase.(overload version) (uri: " + uri + ")", ex);
            }
        }

        /// <summary>
        /// Overload method for BeginCreditCardPurchase()
        /// Provides credit amount and purchase mode
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="planID"></param>
        /// <param name="discountID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="israeliID"></param>
        /// <param name="phone"></param>
        /// <param name="addressLine1"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="countryRegionID"></param>
        /// <param name="postalCode"></param>
        /// <param name="creditCardType"></param>
        /// <param name="creditCardNumber"></param>
        /// <param name="expirationMonth"></param>
        /// <param name="expirationYear"></param>
        /// <param name="cvc"></param>
        /// <param name="conversionMemberID"></param>
        /// <param name="sourceID"></param>
        /// <param name="purchaseReasonTypeID"></param>
        /// <param name="ipAddress"></param>
        /// <param name="promoID"></param>
        /// <param name="UICreditAmount"></param>
        /// <param name="purchaseMode"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string israeliID,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            CreditCardType creditCardType,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear,
            string cvc,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCreditCardPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        israeliID,
                        phone,
                        addressLine1,
                        city,
                        state,
                        countryRegionID,
                        postalCode,
                        creditCardType,
                        creditCardNumber,
                        expirationMonth,
                        expirationYear,
                        cvc,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID,
                        UICreditAmount,
                        purchaseMode);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin credit card purchase.(overload version) (uri: " + uri + ")", ex);
            }
        }

        /// <summary>
        /// Overload method for BeginCreditCardPurchase()
        /// Provides credit amount and purchase mode
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="planID"></param>
        /// <param name="discountID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="israeliID"></param>
        /// <param name="phone"></param>
        /// <param name="addressLine1"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="countryRegionID"></param>
        /// <param name="postalCode"></param>
        /// <param name="creditCardType"></param>
        /// <param name="creditCardNumber"></param>
        /// <param name="expirationMonth"></param>
        /// <param name="expirationYear"></param>
        /// <param name="cvc"></param>
        /// <param name="conversionMemberID"></param>
        /// <param name="sourceID"></param>
        /// <param name="purchaseReasonTypeID"></param>
        /// <param name="ipAddress"></param>
        /// <param name="promoID"></param>
        /// <param name="UICreditAmount"></param>
        /// <param name="purchaseMode"></param>
        /// <param name="reusePreviousPayment"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            string firstName,
            string lastName,
            string israeliID,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            CreditCardType creditCardType,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear,
            string cvc,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment
            )
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCreditCardPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        firstName,
                        lastName,
                        israeliID,
                        phone,
                        addressLine1,
                        city,
                        state,
                        countryRegionID,
                        postalCode,
                        creditCardType,
                        creditCardNumber,
                        expirationMonth,
                        expirationYear,
                        cvc,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID,
                        UICreditAmount,
                        purchaseMode,
                        reusePreviousPayment);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin credit card purchase.(overload version) (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 BeginCreditCardVerification(Int32 memberID,
            Int32 siteID,
            string firstName,
            string lastName,
            string phone,
            string addressLine1,
            string city,
            string state,
            Int32 countryRegionID,
            string postalCode,
            CreditCardType creditCardType,
            string creditCardNumber,
            Int32 expirationMonth,
            Int32 expirationYear)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCreditCardVerification(memberID,
                        siteID,
                        firstName,
                        lastName,
                        phone,
                        addressLine1,
                        city,
                        state,
                        countryRegionID,
                        postalCode,
                        creditCardType,
                        creditCardNumber,
                        expirationMonth,
                        expirationYear);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("BeginCreditCardVerification error (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 BeginCredit(TranType tranType,
            Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 merchantID,
            Int32 referenceMemberTranID,
            Int32 memberPaymentID,
            Int32 planID,
            decimal amount,
            Int32 minutes,
            Int32 duration,
            DurationType durationType,
            CurrencyType currencyType)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginCredit(tranType,
                        memberID,
                        adminMemberID,
                        siteID,
                        merchantID,
                        referenceMemberTranID,
                        memberPaymentID,
                        planID,
                        amount,
                        minutes,
                        duration,
                        durationType,
                        currencyType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin credit card purchase (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginRenewalPurchase(Int32 memberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 communityID,
            Int32 brandID,
            Int32 adminMemberID,
            Int32 memberPaymentID,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginRenewalPurchase(
                        memberID,
                        planID,
                        discountID,
                        siteID,
                        communityID,
                        brandID,
                        adminMemberID,
                        memberPaymentID,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin renewal purchase (uri: " + uri + ")", ex);
            }
        }

        /// <summary>
        /// Overload method for BeginRenewalPurchase()
        /// Provides credit amount and purchase mode
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="planID"></param>
        /// <param name="discountID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <param name="brandID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="memberPaymentID"></param>
        /// <param name="conversionMemberID"></param>
        /// <param name="sourceID"></param>
        /// <param name="purchaseReasonTypeID"></param>
        /// <param name="ipAddress"></param>
        /// <param name="UICreditAmount"></param>
        /// <param name="purchaseMode"></param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult BeginRenewalPurchase(Int32 memberID,
            Int32 planID,
            Int32 discountID,
            Int32 siteID,
            Int32 communityID,
            Int32 brandID,
            Int32 adminMemberID,
            Int32 memberPaymentID,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            Int32 promoID)
        {
            string uri = null;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).BeginRenewalPurchase(
                        memberID,
                        planID,
                        discountID,
                        siteID,
                        communityID,
                        brandID,
                        adminMemberID,
                        memberPaymentID,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        UICreditAmount,
                        purchaseMode,
                        promoID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot begin renewal purchase (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public bool IsEligibleForSaveOffer(Int32 memberID,
            Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                base.Checkout(uri);
                try
                {
                    return getService(uri).IsEligibleForSaveOffer(memberID,
                        siteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("IsEligibleForSaveOffer() error (uri: " + uri + ", memberID: " + memberID.ToString() + ", siteID: " + siteID.ToString() + ").", ex);
            }
        }

        #region Encore Related
        /// <summary>
        /// A hole to call Payment to generate the Encore Sales File
        /// </summary>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public int GenerateEncryptedSalesFile()
        {
            string uri = null;
            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);
                return getService(uri).GenerateEncryptedSalesFile();
            }
            catch (Exception ex)
            {
                throw new SAException("GenerateEncryptedSalesFile() error (uri: " + uri + ").", ex);
            }
        }
        /// <summary>
        /// A hole to call Payment to process the Encore Membership File
        /// </summary>
        [Obsolete("Obsolete - Do not use")]
        public void ProcessMembershipFile()
        {
            string uri = null;
            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);
                getService(uri).ProcessMembershipFile();
            }
            catch (Exception ex)
            {
                throw new SAException("ProcessMembershipFile() error (uri: " + uri + ").", ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public MatchbackCollection GetMatbackRequest()
        {
            string uri = null;
            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);
                return getService(uri).GetMatbackRequest();
            }
            catch (Exception ex)
            {
                throw new SAException("GetMatbackRequest() error (uri: " + uri + ").", ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public int StartMatchbackFileGeneration(string outputFileName)
        {
            string uri = null;
            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);
                return getService(uri).StartMatchbackFileGeneration(outputFileName);
            }
            catch (Exception ex)
            {
                throw new SAException("StartMatchbackFileGeneration() error (uri: " + uri + ").", ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="lineID"></param>
        [Obsolete("Obsolete - Do not use")]
        public void InsertMatchbackFile(byte[] file, int lineID)
        {
            string uri = null;
            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);
                getService(uri).InsertMatchbackFile(file, lineID);
            }
            catch (Exception ex)
            {
                throw new SAException("InsertMatchbackFile() error (uri: " + uri + ").", ex);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mbCol"></param>
        [Obsolete("Obsolete - Do not use")]
        public void MarkMatchbackRequestAsComplete(MatchbackCollection mbCol)
        {
            string uri = null;
            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);
                getService(uri).MarkMatchbackRequestAsComplete(mbCol);
            }
            catch (Exception ex)
            {
                throw new SAException("MarkMatchbackRequestAsComplete() error (uri: " + uri + ").", ex);
            }
        }

        #endregion

        #endregion

        #region caching methods

        /// <summary>
		/// This method returns appropriate cache key for a specific member 
		/// </summary>
		/// <param name="memberid"></param>
		/// <param name="siteid"></param>
		/// <returns></returns>
		private string GetMemberSubStatusKey(int memberid, int siteid) 
		{
			return string.Format(MEMBER_SUB_STATUS_CACHE_KEY_PREFIX, memberid.ToString(), siteid.ToString());
		}

		/// <summary>
		/// This method caches member subscription status object
		/// </summary>
		/// <param name="oMemberSubsStatus"></param>
		private void CacheMemberSubStatus(MemberSubscriptionStatus oMemberSubsStatus) 
		{
			oMemberSubsStatus.CachePriority = CacheItemPriorityLevel.Normal;
			oMemberSubsStatus.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBER_SUBSCRIPTION_STATUS_CACHE_TTL_PURCHASE_SA"));
			_cache.Insert(oMemberSubsStatus);
		}
		#endregion


	}
}
