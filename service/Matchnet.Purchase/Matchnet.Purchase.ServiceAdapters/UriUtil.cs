using System;

using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Purchase.ServiceAdapters
{
	public class UriUtil
	{
		private UriUtil()
		{
		}


		public static string GetServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PURCHASE_SERVICE_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}
				
				return uri;
			}
			catch(Exception ex)
			{
				throw new Exception("Cannot get configuration settings for remote service manager.", ex);
			}
		}
	}
}
