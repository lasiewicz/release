using System;
using System.Collections.Specialized;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceDefinitions;


namespace Matchnet.Purchase.ServiceAdapters.Admin
{
	public class PurchaseAdminSA
	{
		public static readonly PurchaseAdminSA Instance = new PurchaseAdminSA();

		private PurchaseAdminSA()
		{
		}

		private IPurchaseAdminService getService(string uri)
		{
			try
			{
				return (IPurchaseAdminService)Activator.GetObject(typeof(IPurchaseAdminService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
        }

        #region Deprecated
        [Obsolete("Obsolete - Do not use")]
        public SubscriptionResult RemovePaymentInfo(Int32 memberID,
            Int32 siteID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).RemovePaymentInfo(memberID,
                    siteID);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot remove payment information from subscription (uri: " + uri + ")", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public NameValueCollection GetChargeLog(Int32 memberID,
            Int32 memberTranID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = "";

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetChargeLog(memberID,
                    memberTranID);
            }
            catch (Exception ex)
            {
                throw new SAException("GetChargeLog() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 GetMemberIDByMemberTranID(Int32 memberTranID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = "";

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetMemberIDByMemberTranID(memberTranID);
            }
            catch (Exception ex)
            {
                throw new SAException("GetMemberIDByMemberTranID() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32[] GetMemberIDsByAccountNumber(string accountNumber)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = "";

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetMemberIDsByAccountNumber(accountNumber);
            }
            catch (Exception ex)
            {
                throw new SAException("GetMemberIDsByAccountNumber() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public CreditMaximum GetCreditMaximum(Int32 memberTranID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = "";

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).GetCreditMaximum(memberTranID);
            }
            catch (Exception ex)
            {
                throw new SAException("GetCreditMaximum() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 UpdateSubscription(Int32 memberID,
            Int32 siteID,
            Int32 adminMemberID,
            Int32 duration,
            DurationType durationType,
            Int32 transactionReasonID,
            bool reOpenFlag,
            Int32 planID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UpdateSubscription(memberID,
                    siteID,
                    adminMemberID,
                    duration,
                    durationType,
                    TranType.AdministrativeAdjustment,
                    transactionReasonID,
                    reOpenFlag,
                    planID);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateSubscription() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 UpdateSubscription(Int32 memberID,
            Int32 siteID,
            Int32 adminMemberID,
            Int32 duration,
            DurationType durationType,
            Int32 transactionReasonID,
            bool reOpenFlag,
            Int32 planID,
            Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UpdateSubscription(memberID,
                    siteID,
                    adminMemberID,
                    duration,
                    durationType,
                    TranType.AdministrativeAdjustment,
                    transactionReasonID,
                    reOpenFlag,
                    planID,
                    targetBrand);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateSubscription() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 UpdateSubscription(Int32 memberID,
            Int32 siteID,
            Int32 adminMemberID,
            Int32 duration,
            DurationType durationType,
            TranType adjustmentType,
            Int32 transactionReasonID,
            bool reOpenFlag,
            Int32 planID)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UpdateSubscription(memberID,
                    siteID,
                    adminMemberID,
                    duration,
                    durationType,
                    adjustmentType,
                    transactionReasonID,
                    reOpenFlag,
                    planID);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateSubscription() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public Int32 UpdateSubscription(Int32 memberID,
            Int32 siteID,
            Int32 adminMemberID,
            Int32 duration,
            DurationType durationType,
            TranType adjustmentType,
            Int32 transactionReasonID,
            bool reOpenFlag,
            Int32 planID,
            Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UpdateSubscription(memberID,
                    siteID,
                    adminMemberID,
                    duration,
                    durationType,
                    adjustmentType,
                    transactionReasonID,
                    reOpenFlag,
                    planID,
                    targetBrand);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateSubscription() error (uri: " + uri + ").", ex);
            }
        }

        /// <summary>
        /// 08202008 TL - Overloaded to add the TimeAdjustOverrideType, used to override whether to update premium service expiration regardless of the plan type
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="siteID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="duration"></param>
        /// <param name="durationType"></param>
        /// <param name="adjustmentType"></param>
        /// <param name="transactionReasonID"></param>
        /// <param name="reOpenFlag"></param>
        /// <param name="planID"></param>
        /// <param name="targetBrand"></param>
        /// <param name="timeAdjustOverrideType">Value other than TimeAdjustOverrideType.None will indicate an override to update premium service expiration regardless of the plan type</param>
        /// <returns></returns>
        [Obsolete("Obsolete - Do not use")]
        public Int32 UpdateSubscription(Int32 memberID,
            Int32 siteID,
            Int32 adminMemberID,
            Int32 duration,
            DurationType durationType,
            TranType adjustmentType,
            Int32 transactionReasonID,
            bool reOpenFlag,
            Int32 planID,
            Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
            TimeAdjustOverrideType timeAdjustOverrideType)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UpdateSubscription(memberID,
                    siteID,
                    adminMemberID,
                    duration,
                    durationType,
                    adjustmentType,
                    transactionReasonID,
                    reOpenFlag,
                    planID,
                    targetBrand,
                    timeAdjustOverrideType);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateSubscription() error (uri: " + uri + ").", ex);
            }
        }

        [Obsolete("Obsolete - Do not use")]
        public int UpdateALaCarteOnly(int memberID,
            int siteID,
            int adminMemberID,
            TranType adjustmentType,
            int transactionReasonID,
            bool reOpenFlag,
            int planID,
            int planIDToReplace,
            Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
            bool insertTransactionRecordFlag)
        {
            throw new SAException("Obsolete - Do not use");
            string uri = string.Empty;

            try
            {
                uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

                return getService(uri).UpdateALaCarteOnly(memberID,
                    siteID,
                    adminMemberID,
                    adjustmentType,
                    transactionReasonID,
                    reOpenFlag,
                    planID,
                    planIDToReplace,
                    targetBrand,
                    insertTransactionRecordFlag);
            }
            catch (Exception ex)
            {
                throw new SAException("UpdateALaCarteOnly() error (uri: " + uri + ").", ex);
            }

        }
        #endregion
    }
}
