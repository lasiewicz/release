using System;

using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceDefinitions;


namespace Matchnet.Purchase.ServiceAdapters
{
	public class PlanSA : SABase
	{
		public static readonly PlanSA Instance = new PlanSA();
		
		private PlanSA()
		{
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PURCHASESVC_SA_CONNECTION_LIMIT"));
		}

		public Int32 InsertPlan(Int32 brandID, Plan plan)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).InsertPlan(
						brandID, 
						plan);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("InsertPlan() error (uri: " + uri + ").", ex);
			}
		}
		
		public PlanCollection GetPlans(Int32 brandID, 
			PlanType planTypeMask, 
			PaymentType paymentTypeMask)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlans(
						brandID, 
						planTypeMask, 
						paymentTypeMask);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("GetPlans() error (uri: " + uri + ").", ex);
			}
		}

		
		public PlanCollection GetPlans(Int32 brandID, 
			PlanType planTypeMask, 
			PaymentType paymentTypeMask,
			int initialDuration,
			DurationType durationType,
			PremiumType premiumType)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlans(
						brandID, 
						planTypeMask, 
						paymentTypeMask,
						initialDuration,
						durationType,
						premiumType);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("GetPlans() error (uri: " + uri + ").", ex);
			}
		}
		
//		public PlanCollection GetMemberDisplayedPlans(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, 
//			PlanType planTypeMask, 
//			PaymentType paymentTypeMask,
//			Int32 memberID)
//		{
//			string uri = null;
//
//			try
//			{
//				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);
//
//				base.Checkout(uri);
//				try
//				{
//					return getService(uri).GetMemberDisplayedPlans(
//						brand.BrandID, 
//						planTypeMask, 
//						paymentTypeMask,
//						memberID);																
//				}
//				finally
//				{
//					base.Checkin(uri);
//				}
//			}
//			catch (Exception ex)
//			{
//				throw new SAException("GetMemberDisplayedPlans() error (uri: " + uri + ").", ex);
//			}
//		}
		
		public Plan GetPlan(Int32 planID,
			Int32 brandID)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlan(planID, 
						brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("GetPlan() error (uri: " + uri + ").", ex);
			}
		}

		public Plan GetPlan(Int32 planID,Int32 brandID,	PlanType planTypeMask,PaymentType paymentTypeMask)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlan(planID,brandID,planTypeMask,paymentTypeMask);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("GetPlan() (overload) error (uri: " + uri + ").", ex);
			}
		}

		public string GetPlanDescription(Int32 planID)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlanDescription(planID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("GetPlanDescription() error (uri: " + uri + ").", ex);
			}
		}


		public Plan GetFreeTrialGroupPlan(FreeTrialGroupType freeTrialGroupType, 
			Int32 brandID)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetFreeTrialGroupPlan(freeTrialGroupType, brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("GetFreeTrialGroupPlan() error (uri: " + uri + ")", ex);
			}
		}

//		public PlanCollection SetupPlanCollectionForDisplay(PlanCollection sourcePlans, 
//			Int32 brandID, 
//			Int32 bestValuePlanID,
//			Int32 memberID)
//		{
//			string uri = null;
//
//			try
//			{
//				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PLAN);
//
//				base.Checkout(uri);
//				try
//				{
//					return getService(uri).SetupPlanCollectionForDisplay(
//						sourcePlans,
//						brandID, 
//						bestValuePlanID,
//						memberID);																
//				}
//				finally
//				{
//					base.Checkin(uri);
//				}
//			}
//			catch (Exception ex)
//			{
//				throw new SAException("SetupPlanCollectionForDisplay() error (uri: " + uri + ").", ex);
//			}
//		}

		public bool IsPurchaseAllowedWithRemainingCredit(Plan plan, decimal remainingCreditAmount)
		{
			string uri = null;

			try
			{
				uri = UriUtil.GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME_PURCHASE);

				return getService(uri).IsPurchaseAllowedWithRemainingCredit(plan, remainingCreditAmount);
			}
			catch (Exception ex)
			{
				throw new SAException("IsPurchaseAllowedWithRemainingCredit() error (uri: " + uri + ").", ex);
			}
		}
		
		private IPlanService getService(string uri)
		{
			try
			{
				return (IPlanService)Activator.GetObject(typeof(IPlanService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}




	}
}
