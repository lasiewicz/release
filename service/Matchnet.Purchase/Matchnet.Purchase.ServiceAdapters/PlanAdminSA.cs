using System;

using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Purchase.ServiceDefinitions;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Purchase.ServiceAdapters
{
	public class PlanAdminSA : SABase
	{

		private const string SERVICE_MANAGER_NAME = "PlanSM";
		
		public static readonly PlanAdminSA Instance = new PlanAdminSA();

		private PlanAdminSA()
		{
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PURCHASESVC_SA_CONNECTION_LIMIT"));
		}

		
		public void InsertPlanResource(Int32 planID, 
			PaymentType paymentType, 
			string resourceConstant)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					getService(uri).InsertPlanResource(planID, paymentType, resourceConstant);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot insert plan resource (uri: " + uri + ")", ex));
			}
		}


		public void UpdatePlan(Int32 planID, 
			Int32 brandID, 
			CurrencyType currencyType, 
			string resourceConstant, 
			decimal initialCost, 
			Int32 initialDuration, 
			DurationType initialDurationType, 
			decimal renewCost, 
			Int32 renewDuration, 
			DurationType renewDurationType, 
			PlanType planTypeMask, 
			Int32 renewAttempts, 
			bool bestValueFlag, 
			Int32 listOrder, 
			PaymentType paymentTypeMask, 
			DateTime startDate, 
			DateTime endDate)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					getService(uri).UpdatePlan(planID,
						brandID,
						currencyType,
						resourceConstant,
						initialCost,
						initialDuration,
						initialDurationType,
						renewCost,
						renewDuration,
						renewDurationType,
						planTypeMask,
						renewAttempts,
						bestValueFlag,
						listOrder,
						paymentTypeMask,
						startDate,
						endDate);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot update plan (uri: " + uri + ")", ex));
			}
		}


		public void UpdatePlanResource(Int32 planID, 
			PaymentType paymentType, 
			string resourceConstant)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					getService(uri).UpdatePlanResource(
						planID,
						paymentType,
						resourceConstant);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot update plan resource (uri: " + uri + ")", ex));
			}
		}


		public Int32 InsertPlan(Int32 brandID, 
			CurrencyType currencyType, 
			string resourceConstant, 
			decimal initialCost, 
			Int32 initialDuration, 
			DurationType initialDurationType, 
			decimal renewCost, 
			Int32 renewDuration, 
			DurationType renewDurationType, 
			PlanType planTypeMask, 
			Int32 renewAttempts, 
			bool bestValueFlag, 
			Int32 listOrder, 
			PaymentType paymentTypeMask, 
			DateTime startDate, 
			DateTime endDate)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					return getService(uri).InsertPlan(
						brandID,
						currencyType,
						resourceConstant,
						initialCost,
						initialDuration,
						initialDurationType,
						renewCost,
						renewDuration,
						renewDurationType,
						planTypeMask,
						renewAttempts,
						bestValueFlag,
						listOrder,
						paymentTypeMask,
						startDate,
						endDate);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot insert plan (uri: " + uri + ")", ex));
			}
		}


		public PlanCollection GetPlans(Int32 brandID)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlans(brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin plans (uri: " + uri + ")", ex));
			}
		}

		public PlanCollection GetPlans(Int32 brandID, PaymentType paymentTypeMask)
		{
			string uri = string.Empty ;

			try
			{
				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					return getService(uri).GetPlans(brandID, paymentTypeMask);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin plans by payment type (uri: " + uri + ")", ex));
			}
		}

//		/// <summary>
//		/// Gets a list of plans for admins with UI metadata.  Plans are filtered to only return SiteDefault plans, and
//		/// plan start/end dates are not taken into account so admins may view plans that are technically expired.
//		/// </summary>
//		/// <param name="brand"></param>
//		/// <param name="paymentTypeMask"></param>
//		/// <param name="memberID"></param>
//		/// <returns></returns>
//		public PlanCollection GetAdminDisplayedPlans(Matchnet.Content.ValueObjects.BrandConfig.Brand brand,
//			PaymentType paymentTypeMask,
//			Int32 memberID)
//		{
//			string uri = string.Empty ;
//
//			try
//			{
//				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);
//
//				base.Checkout(uri);
//				try
//				{
//					return getService(uri).GetAdminDisplayedPlans(
//						brand.BrandID, 
//						paymentTypeMask,
//						memberID);																
//				}
//				finally
//				{
//					base.Checkin(uri);
//				}
//			}
//			catch (Exception ex)
//			{
//				throw new SAException("GetAdminDisplayedPlans() error (uri: " + uri + ").", ex);
//			}
//		}

//		/// <summary>
//		/// Gets a list of plans for admins with UI metadata.  Plans may be filtered to only return SiteDefaults or ColumnOrders > 99
//		/// depending on the corresponding parameters.  Plan start/end dates are not taken into account so admins may view plans that 
//		/// are technically expired.
//		/// </summary>
//		/// <param name="brand"></param>
//		/// <param name="paymentTypeMask"></param>
//		/// <param name="memberID"></param>
//		/// <param name="showSiteDefaultPlansOnly">Determines whether to filter plans to only return SiteDefaults</param>
//		/// <param name="allowAllColumnOrders">Determines whether to filter plans with ColumnOrders > 99</param>
//		/// <returns></returns>
//		public PlanCollection GetAdminDisplayedPlans(Matchnet.Content.ValueObjects.BrandConfig.Brand brand,
//			PaymentType paymentTypeMask,
//			Int32 memberID,
//			bool showSiteDefaultPlansOnly,
//			bool allowAllColumnOrders)
//		{
//			string uri = string.Empty ;
//
//			try
//			{
//				uri = UriUtil.GetServiceManagerUri(SERVICE_MANAGER_NAME);
//
//				base.Checkout(uri);
//				try
//				{
//					return getService(uri).GetAdminDisplayedPlans(
//						brand.BrandID, 
//						paymentTypeMask,
//						memberID,
//						showSiteDefaultPlansOnly,
//						allowAllColumnOrders);																
//				}
//				finally
//				{
//					base.Checkin(uri);
//				}
//			}
//			catch (Exception ex)
//			{
//				throw new SAException("GetAdminDisplayedPlans() error (uri: " + uri + ").", ex);
//			}
//		}

		private IPlanAdminService getService(string uri)
		{
			try
			{
				return (IPlanAdminService)Activator.GetObject(typeof(IPlanAdminService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

	}
}
