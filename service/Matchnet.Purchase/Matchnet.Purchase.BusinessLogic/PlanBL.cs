using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ValueObjects.ReplicationActions;

using System.Collections;

namespace Matchnet.Purchase.BusinessLogic
{
	public class PlanBL
	{
		public static readonly PlanBL Instance = new PlanBL();

		private Matchnet.Caching.Cache _cache = Matchnet.Caching.Cache.Instance;

		#region Events
		/// <summary>
		/// This event handles the cache synchronization between service instances
		/// </summary>
		public delegate void ReplicationEventHandler(IReplicationAction replicationAction);
		/// <summary>
		/// 
		/// </summary>
		public event ReplicationEventHandler ReplicationRequested;
		#endregion

		private PlanBL()
		{
		}

		/// <summary>
		/// Save a plan then raise the SynchronizationRequested event so that all the caches at the BL level are updated.
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="plan"></param>
		public Int32 InsertPlan(Int32 brandID, Plan plan)
		{
			Int32 planId = Constants.NULL_INT;
			TransactionalWriter tw = new TransactionalWriter();
			try
			{
				#region Write is done through TransactionalWriter now
//				// We need to get key values for both Plan and PlanGroup tables.
//				// Tables themselves do not enforce this unique constraint
//				planId = KeySA.Instance.GetKey("PlanID");
//				plan.PlanID = planId;
//				Int32 planGroupId = KeySA.Instance.GetKey("PlanGroupID");
//				
//				Command command = new Command("mnSystem", "dbo.up_Plan_Save", 0);
//
//				// Although we don't need to insert anything into PlanResource table going forward, we must still do it
//				// to support the older plans that exist already.  Depending on the PaymentType, we will insert 1 or 2
//				// records.
//				Int32 planResourceId = Constants.NULL_INT;
//				if(plan.PaymentTypeMask == (PaymentType.Check | PaymentType.CreditCard))
//				{
//					planResourceId = KeySA.Instance.GetKey("PlanResourceID");
//					command.AddParameter("@PlanResourceID_1", SqlDbType.Int, ParameterDirection.Input, planResourceId);
//
//					planResourceId = KeySA.Instance.GetKey("PlanResourceID");
//					command.AddParameter("@PlanResourceID_2", SqlDbType.Int, ParameterDirection.Input, planResourceId);
//				}
//				else
//				{
//					planResourceId = KeySA.Instance.GetKey("PlanResourceID");
//					command.AddParameter("@PlanResourceID_1", SqlDbType.Int, ParameterDirection.Input, planResourceId);
//				}				
//				
//				command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, plan.PlanID);
//				command.AddParameter("@PlanGroupID", SqlDbType.Int, ParameterDirection.Input, planGroupId);
//				command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)plan.CurrencyType);
//				command.AddParameter("@InitialCost", SqlDbType.Decimal, ParameterDirection.Input, plan.InitialCost);
//				command.AddParameter("@InitialDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)plan.InitialDurationType);
//				command.AddParameter("@InitialDuration", SqlDbType.Int, ParameterDirection.Input, plan.InitialDuration);
//				command.AddParameter("@RenewCost", SqlDbType.Decimal, ParameterDirection.Input, plan.RenewCost);
//				command.AddParameter("@RenewDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)plan.RenewDurationType);
//				command.AddParameter("@RenewDuration", SqlDbType.Int, ParameterDirection.Input, plan.RenewDuration);
//				command.AddParameter("@PlanTypeMask", SqlDbType.Int, ParameterDirection.Input, (int)plan.PlanTypeMask);
//				command.AddParameter("@RenewAttempts", SqlDbType.Int, ParameterDirection.Input, plan.RenewAttempts);
//				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, brandID);
//				command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, plan.StartDate);
//				command.AddParameter("@PaymentTypeMask", SqlDbType.Int, ParameterDirection.Input, (int)plan.PaymentTypeMask);
//				command.AddParameter("@PremiumTypeMask", SqlDbType.Int, ParameterDirection.Input, (int)plan.PremiumTypeMask);
//
//				Client.Instance.ExecuteNonQuery(command);
				#endregion

				// We need to return PlanID, so get the key for it here
				planId = KeySA.Instance.GetKey("PlanID");
				plan.PlanID = planId;

				if (tw.PersistPlan(plan, brandID))
				{
					// Bust cache and replicate to the partner
					string cacheKey = PlanCollection.GetCacheKey(brandID);
					// Expire mine first
					ExpireCachedPlanCollection(cacheKey);
					// Replicate this action on the partner
					if(cacheKey.Length > 0)
					{
						ExpirePlanCollection actionItem = new ExpirePlanCollection(cacheKey);
						ReplicationRequested(actionItem);
					}
				}
				else
				{
					// Since the insert didn't go through, this PlanID is no longer valid
					planId = Constants.NULL_INT;
				}

			}
			catch (Exception ex)
			{
				throw new BLException("InsertPlan() error. (brandID: " + brandID.ToString() + ", planID: " + plan.PlanID + ")", ex);
			}

            return planId;
		}

		public PlanCollection GetPlans(Int32 brandID)
		{
			try
			{

				PlanCollection planCollection = _cache.Get(PlanCollection.GetCacheKey(brandID)) as PlanCollection;
				
				if (planCollection == null)
				{
					Command command = new Command("mnSystem", "dbo.up_Plan_List_Services", 0);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, brandID);
					DataTable dt = Client.Instance.ExecuteDataTable(command);
				
					planCollection = new PlanCollection(brandID);
					int currentPlanID = Constants.NULL_INT;
					int lastPlanID = Constants.NULL_INT;
					PaymentType lastPmtType = PaymentType.None;
					Plan plan = null;
				
					for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
					{
						DataRow row = dt.Rows[rowNum];

						PaymentType paymentType = (PaymentType)Enum.Parse(typeof(PaymentType), row["PaymentTypeID"].ToString());

						if ( paymentType == PaymentType.CreditCard || paymentType == PaymentType.Check )
						{
							currentPlanID = Convert.ToInt32(row["PlanID"]);
							// Create a plan object if PlanID or PaymentType changed
							if(currentPlanID != lastPlanID || paymentType != lastPmtType)
							{
								plan = new Plan(Convert.ToInt32(row["PlanID"]),
									(CurrencyType)Enum.Parse(typeof(CurrencyType), row["CurrencyID"].ToString()),
									Convert.ToDecimal(row["InitialCost"]),
									Convert.ToInt32(row["InitialDuration"]),
									(Matchnet.DurationType)Enum.Parse(typeof(Matchnet.DurationType), 
									row["InitialDurationTypeID"].ToString()),
									Convert.ToDecimal(row["RenewCost"]),
									Convert.ToInt32(row["RenewDuration"]),
									(Matchnet.DurationType)Enum.Parse(typeof(Matchnet.DurationType), 
									row["RenewDurationTypeID"].ToString()),
									(PlanType)Enum.Parse(typeof(PlanType), row["PlanTypeMask"].ToString()),
									Convert.ToInt32(row["RenewAttempts"]),
									Convert.ToDateTime(row["UpdateDate"]),
									Convert.ToBoolean(row["BestValueFlag"]),
									(PaymentType)Enum.Parse(typeof(PaymentType), row["PaymentTypeMask"].ToString()),
									paymentType,
									Convert.ToDateTime(row["StartDate"]),
									row["EndDate"] != DBNull.Value ? Convert.ToDateTime(row["EndDate"]) : DateTime.MinValue);

								plan.PremiumTypeMask=row["PremiumTypeMask"] != DBNull.Value ? (PremiumType)Enum.Parse(typeof(PremiumType), row["PremiumTypeMask"].ToString()) : PremiumType.None;	
								plan.PlanResourceConstant = row["ResourceConstant"] != DBNull.Value ? row["ResourceConstant"].ToString() : Constants.NULL_STRING;

								/*
								if (row["insertDate"] != null)
									plan.InsertDate = Convert.ToDateTime(row["insertDate"]);
								*/
								plan.PlanServices = new PlanServiceCollection();
								if(row["PlanServiceDefinitionID"] == DBNull.Value)
								{
									// For plans that do not have entries in the PlanServices table, just assume the entire amount is for the
									// stadard subscription
									plan.PlanServices.Add(new PlanService(PlanServiceDefinition.Basic_Subscription,
										Convert.ToDecimal(row["InitialCost"]), Convert.ToDecimal(row["RenewCost"])));


								}
								else
								{
									plan.PlanServices.Add(new PlanService( (PlanServiceDefinition)Enum.Parse(typeof(PlanServiceDefinition), row["PlanServiceDefinitionID"].ToString()), 
										Convert.ToDecimal(row["BasePrice"]),  Convert.ToDecimal(row["RenewalPrice"])));
								}
								
								planCollection.Add(plan);
							}
							else
							{
								// if PlanID and PaymentType are the same, just update the PlanServices collection
								plan.PlanServices.Add(new PlanService( (PlanServiceDefinition)Enum.Parse(typeof(PlanServiceDefinition), row["PlanServiceDefinitionID"].ToString()), 
									Convert.ToDecimal(row["BasePrice"]),  Convert.ToDecimal(row["RenewalPrice"])));
							}

							lastPlanID = currentPlanID;
                            lastPmtType = paymentType;
						}
					}

					_cache.Add(planCollection);
				}
			
				return planCollection;
			}
			catch (Exception ex)
			{
				throw new BLException("GetPlans() error. (brandID: " + brandID.ToString() + ")", ex);
			}
		}


		public string GetPlanDescription(Int32 planID)
		{
			try
			{
				Command command = new Command("mnSystem", "dbo.up_Plan_List_Single", 0);
				command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
				DataTable dt = Client.Instance.ExecuteDataTable(command);
				
				if (dt.Rows.Count == 1)
				{
					return dt.Rows[0]["ResourceConstant"].ToString();
				}
				else
				{
					return "";
				}
			
			}
			catch (Exception ex)
			{
				throw new BLException("GetPlanDescription() error. (planID: " + planID.ToString() + ")", ex);
			}
		}

		public PlanCollection GetPlans(Int32 brandID,
			PaymentType paymentTypeMask)
		{
			try
			{
				return filterPlans(GetPlans(brandID), brandID, paymentTypeMask, false);
			}
			catch (Exception ex)
			{
				throw new BLException("GetPlans() error (brand: " + brandID.ToString() + ", paymentTypeMask: " + ((Int32)paymentTypeMask).ToString(), ex);
			}
		}
		
		public PlanCollection GetPlans(Int32 brandID,
			PlanType planTypeMask,
			PaymentType paymentTypeMask)
		{
			try
			{
				return filterPlans(GetPlans(brandID), brandID, planTypeMask, paymentTypeMask, true);
			}
			catch (Exception ex)
			{
				throw new BLException("GetPlans() error (brand: " + brandID.ToString() + ", planMask: " + ((Int32)planTypeMask).ToString() + ", paymentTypeMask: " + ((Int32)paymentTypeMask).ToString(), ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="initialDuration"></param>
		/// <param name="durationType"></param>
		/// <param name="premiumType"></param>
		/// <returns></returns>
		public PlanCollection GetPlans(Int32 brandID,
			PlanType planTypeMask,
			PaymentType paymentTypeMask,
			int initialDuration,
			DurationType durationType,
			PremiumType premiumType)
		{
			try
			{
				return filterPlans(GetPlans(brandID), brandID, planTypeMask, paymentTypeMask, true, initialDuration, durationType, premiumType);
			}
			catch (Exception ex)
			{
				throw new BLException("GetPlans() error (brand: " + brandID.ToString() + ", planMask: " + ((Int32)planTypeMask).ToString() + ", paymentTypeMask: " + ((Int32)paymentTypeMask).ToString(), ex);
			}
		}

//		/// <summary>
//		/// 08182008 TL - Added showSiteDefaultPlansOnly and allowAllColumnOrders parameters so that the admin UI pages that needs to see all plans
//		/// can use these params.  Admin UI used to call GetPlans() that that method does not prepare display metadata info for a user which is now
//		/// a necessity for the admin pages.
//		/// </summary>
//		/// <param name="brandID"></param>
//		/// <param name="paymentTypeMask"></param>
//		/// <param name="memberID"></param>
//		/// <param name="showSiteDefaultPlansOnly"></param>
//		/// <param name="allowAllColumnOrders"></param>
//		/// <returns></returns>
//		public PlanCollection GetAdminDisplayedPlans(Int32 brandID,
//			PaymentType paymentTypeMask,
//			Int32 memberID,
//			bool showSiteDefaultPlansOnly,
//			bool allowAllColumnOrders)
//		{
//			try
//			{
//				PlanCollection plansToDisplay = GetPlans(brandID, paymentTypeMask);
//				if (paymentTypeMask == PaymentType.Check)
//				{
//					// Remove plans that do not belong on the subscribe by check page  
//					plansToDisplay = applyFilterForPaymentByChecks(plansToDisplay, brandID);
//				}
//				if (plansToDisplay != null)
//				{
//					// Gets only the site default plans and populates the plan collection
//					// meta data
//					//plansToDisplay = PreparePlanCollectionForDisplay(plansToDisplay, brandID, true, memberID, false);
//					plansToDisplay = PreparePlanCollectionForDisplay(plansToDisplay, brandID, showSiteDefaultPlansOnly, memberID, allowAllColumnOrders, true);
//					//plansToDisplay = PreparePlanCollectionForDisplay(plansToDisplay, brandID, showSiteDefaultPlansOnly, memberID, allowAllColumnOrders);
//				}
//
//				return plansToDisplay;
//			}
//			catch (Exception ex)
//			{
//				throw new BLException("GetAdminDisplayedPlans() error (brand: " + brandID.ToString() + ", paymentTypeMask: " + ((Int32)paymentTypeMask).ToString(), ex);
//			}
//		}

//		public PlanCollection GetMemberDisplayedPlans(Int32 brandID,
//			PlanType planTypeMask,
//			PaymentType paymentTypeMask,
//			Int32 memberID)
//		{
//			try
//			{
//				PlanCollection plansToDisplay = GetPlans(brandID, planTypeMask, paymentTypeMask);
//				if (paymentTypeMask == PaymentType.Check)
//				{
//					// Remove plans that do not belong on the subscribe by check page  
//					plansToDisplay = applyFilterForPaymentByChecks(plansToDisplay, brandID);
//				}
//				if (plansToDisplay != null)
//				{
//					// Gets only the site default plans and populates the plan collection
//					// meta data
//					plansToDisplay = PreparePlanCollectionForDisplay(plansToDisplay, brandID, true, memberID, false, true);
//					//plansToDisplay = PreparePlanCollectionForDisplay(plansToDisplay, brandID, true, memberID, false);
//				}
//
//				return plansToDisplay;
//			}
//			catch (Exception ex)
//			{
//				throw new BLException("GetMemberDisplayedPlans() error (brand: " + brandID.ToString() + ", planMask: " + ((Int32)planTypeMask).ToString() + ", paymentTypeMask: " + ((Int32)paymentTypeMask).ToString(), ex);
//			}
//		}

		public Plan GetFreeTrialGroupPlan(FreeTrialGroupType freeTrialGroupType, 
			Int32 brandID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSystem", "dbo.up_Plan_List_FreeTrialGroup", 0);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, brandID);
				command.AddParameter("@FreeTrialGroupID", SqlDbType.Int, ParameterDirection.Input, (Int32)freeTrialGroupType);
				dataReader = Client.Instance.ExecuteReader(command);
				
				bool colLookupDone = false;
				Int32 ordinalPlanID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalPlanID = dataReader.GetOrdinal("PlanID");
						colLookupDone = true;
					}

					return GetPlans(brandID).FindByID(dataReader.GetInt32(ordinalPlanID));
				}

				return null;
			}
			catch (Exception ex)
			{
				throw new BLException("GetFreeTrialGroupPlan() error (freeTrialGroupType: " + freeTrialGroupType.ToString() + ", brandID: " + brandID + ").", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		public PlanCollection GetAdminPlans(Int32 brandID)
		{
			try
			{
				return GetPlans(brandID);
			}
			catch (Exception ex)
			{
				throw new BLException("GetAdminPlans() error (brandID: " + brandID + ").", ex);
			}
		}

		//  Allow Admin plan collection to be retrieved by payment type
		public PlanCollection GetAdminPlans(Int32 brandID, PaymentType paymentTypeMask)
		{
			try
			{
				return filterPlans(GetPlans(brandID), brandID, paymentTypeMask, false);
			}
			catch (Exception ex)
			{
				throw new BLException("GetAdminPlans() error (brandID: " + brandID + ", paymentTypeMask: " + paymentTypeMask + ").", ex);
			}
		}

		
		public Plan GetPlan(Int32 planID, 
			Int32 brandID)
		{
			try
			{
				return GetPlans(brandID).FindByID(planID);
			}
			catch (Exception ex)
			{
				throw new BLException("GetPlan() error (planID: " + planID.ToString() + ", brandID: " + brandID.ToString() + ").", ex);
			}
		}

		/// <summary>
		/// 08182008 TL - Added includeActivePlanOnly parameter to determine whether to check for plan expiration and to maintain
		/// consistency with the functionality of the overloaded filterPlans function.
		/// </summary>
		/// <param name="sourcePlans"></param>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="includeActivePlanOnly"></param>
		/// <returns></returns>
		private PlanCollection filterPlans(PlanCollection sourcePlans,
			Int32 brandID,
			PlanType planTypeMask,
			PaymentType paymentTypeMask,
			bool includeActivePlanOnly)
		{
			PlanCollection filteredPlans = null;

			if (sourcePlans != null)
			{
				bool addPlan = false;
				PaymentType paymentTypeID = PaymentType.None;

				switch (paymentTypeMask)
				{
					case PaymentType.Check:
						paymentTypeID = PaymentType.Check;
						break;

					default:
						paymentTypeID = PaymentType.CreditCard;
						break;
				}

				foreach (Plan plan in sourcePlans)
				{
					if ((plan.PlanTypeMask & planTypeMask) > 0 &&
						(plan.PaymentTypeMask & paymentTypeMask) > 0 && 
						(plan.PlanResourcePaymentTypeID == paymentTypeID))
					{
						addPlan = true;
					
						if (includeActivePlanOnly)
						{
							if (plan.StartDate < DateTime.Now && (plan.EndDate == DateTime.MinValue || plan.EndDate > DateTime.Now))
							{
								addPlan = true;
							}
							else
							{
								addPlan = false;
							}
						}
					}
					else
					{
						addPlan = false;
					}

					if (addPlan)
					{
						if (filteredPlans == null)
						{
							filteredPlans = new PlanCollection(brandID);
						}
						filteredPlans.Add(plan);
					}
				}
			}
			
			return filteredPlans;
		}
		
		/// <summary>
		/// Initially written for Subcription Admin project.  Plan.PlanResourcePaymentTypeID is not referenced in this method since this property is being
		/// phased out.
		/// </summary>
		/// <param name="sourcePlans"></param>
		/// <param name="brandID"></param>
		/// <param name="planTypeMask"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="includeActivePlanOnly"></param>
		/// <param name="initialDuration"></param>
		/// <param name="durationType"></param>
		/// <param name="premiumType"></param>
		/// <returns></returns>
		private PlanCollection filterPlans(PlanCollection sourcePlans,
			Int32 brandID,
			PlanType planTypeMask,
			PaymentType paymentTypeMask,
			bool includeActivePlanOnly,
			int initialDuration,
			DurationType durationType,
			PremiumType premiumType)
		{
			PlanCollection filteredPlans = null;

			if (sourcePlans != null)
			{
				bool addPlan = false;
				
				int lastPlanID = Constants.NULL_INT;
				foreach (Plan plan in sourcePlans)
				{
					// GetPlans(brandID) actually returns 2 plans for each plan due to the  multiple entries
					//in the PlanGroup table (1 for check and 1 for credit).  The PaymentTypeMask values for the repeated items
					// are the same, so we can just remember the last PlanID and skip if needed.
					// Previous filterPlans method used Plan.PlanResourcePaymentTypeID to filter out the doubles, but we are
					// phasing out that property so I won't make reference to that property in this method. (mcho)
					if(lastPlanID != plan.PlanID)
					{
						if ((plan.PlanTypeMask & planTypeMask) == planTypeMask &&
							(plan.PaymentTypeMask & paymentTypeMask) == paymentTypeMask &&
							plan.InitialDuration == initialDuration &&
							plan.InitialDurationType == durationType)
						{					
							// This means PremiumType is not important
							if(premiumType == PremiumType.None)
								addPlan = true;
							else
							{
								// Looking for specific PremiumType here
								if(plan.PremiumTypeMask == premiumType )
									addPlan = true;
								else
									addPlan = false;
							}

							if (addPlan && includeActivePlanOnly)
							{
								if (plan.StartDate < DateTime.Now && (plan.EndDate == DateTime.MinValue || plan.EndDate > DateTime.Now))
								{
									addPlan = true;
								}
								else
								{
									addPlan = false;
								}
							}
						}
						else
							addPlan= false;

						lastPlanID = plan.PlanID;
					}
					else
					{
						addPlan = false;
					}

					if (addPlan)
					{
						if (filteredPlans == null)
						{
							filteredPlans = new PlanCollection(brandID);
						}
						filteredPlans.Add(plan);
					}
				}
			}
			
			return filteredPlans;
		}

		/// <summary>
		/// 08182008 TL - Added includeActivePlanOnly parameter to determine whether to check for plan expiration and to maintain
		/// consistency with the functionality of the overloaded filterPlans function.
		/// </summary>
		/// <param name="sourcePlans"></param>
		/// <param name="brandID"></param>
		/// <param name="paymentTypeMask"></param>
		/// <param name="includeActivePlanOnly"></param>
		/// <returns></returns>
		private PlanCollection filterPlans(PlanCollection sourcePlans,
			Int32 brandID,
			PaymentType paymentTypeMask,
			bool includeActivePlanOnly)
		{
			PlanCollection filteredPlans = null;

			if (sourcePlans != null)
			{
				bool addPlan = false;
				PaymentType paymentTypeID = PaymentType.None;

				switch (paymentTypeMask)
				{
					case PaymentType.Check:
						paymentTypeID = PaymentType.Check;
						break;

					default:
						paymentTypeID = PaymentType.CreditCard;
						break;
				}

				foreach (Plan plan in sourcePlans)
				{
					if ((plan.PaymentTypeMask & paymentTypeMask) > 0 && 
						(plan.PlanResourcePaymentTypeID == paymentTypeID))
					{
						addPlan = true;

						if (includeActivePlanOnly)
						{
							if (plan.StartDate < DateTime.Now && (plan.EndDate == DateTime.MinValue || plan.EndDate > DateTime.Now))
							{
								addPlan = true;
							}
							else
							{
								addPlan = false;
							}
						}
					}
					else
					{
						addPlan = false;
					}

					if (addPlan)
					{
						if (filteredPlans == null)
						{
							filteredPlans = new PlanCollection(brandID);
						}
						filteredPlans.Add(plan);
					}
				}
			}
			
			return filteredPlans;
		}

		public PlanCollection applyFilterForPaymentByChecks(PlanCollection sourcePlans, Int32 brandID)
		{
			PlanCollection filteredPlans = sourcePlans;

			// Do not display any plans that have a duration of a month or less
			// Assume that there will not be 5 week plans or 40 day plans for example
			if (filteredPlans != null)
			{
				PlanCollection filteredPlansByPaymentType = null;
				bool addPlan = false;

				foreach (Plan plan in filteredPlans)
				{
					switch (plan.InitialDurationType)
					{
						case DurationType.Minute: 
						case DurationType.Hour: 
						case DurationType.Day:
						case DurationType.Week: 
							// Do not display on subscribe by credit card if the plan duration is less than a month
							addPlan = false;
							break;
						case DurationType.Month: 
							if (plan.InitialDuration > 1)
							{
								// Plan duration is longer than a month
								addPlan = true;
							}
							else
							{
								// Plan duration is less than a month
								addPlan = false;
							}
							break;
						case DurationType.Year: 
							// Plan duration is longer than a month
							addPlan = true;
							break;
					}

					if (addPlan)
					{
						if (filteredPlansByPaymentType == null)
						{
							filteredPlansByPaymentType = new PlanCollection(brandID);
						}

						filteredPlansByPaymentType.Add(plan);
					}
				}


				// This plan collection only has the plans with duration of greater than a month  
				filteredPlans = filteredPlansByPaymentType;
			}

			return filteredPlans;
		}

		#region Plan display methods

		/*
		public PlanCollection PreparePlanCollectionForDisplay(PlanCollection sourcePlans, Int32 brandID)
		{
			// Used when plans are retrieved and displayed by promo engine.  
			// This should not filter promotional plans.  
			return PreparePlanCollectionForDisplay(sourcePlans, brandID, false);
		}

		public PlanCollection PreparePlanCollectionForDisplay(PlanCollection sourcePlans, Int32 brandID, bool showSiteDefaultPlansOnly)
		{
			// Used to display site default plans
			return PreparePlanCollectionForDisplay(sourcePlans, brandID, showSiteDefaultPlansOnly, Constants.NULL_INT, false);			
		}
		*/
//
//		/// <summary>
//		/// Sets up the collection of plans for display
//		/// </summary>
//		/// <param name="sourcePlans">Original plan collection</param>
//		/// <param name="brandID">Current Brand ID</param>
//		/// <param name="showSiteDefaultPlansOnly">Show only the site default plans only</param>
//		/// <param name="memberID">Current Member ID</param>
//		/// <param name="allowAllColumnOrders">Show plans with any column order</param>
//		/// <param name="clonePlanCollection">Requires cloning the original plan collection</param>
//		/// <returns name="PlanDisplayValidation">Returns the plan collection for display</returns>
//		public PlanCollection PreparePlanCollectionForDisplay(PlanCollection sourcePlans, Int32 brandID, bool showSiteDefaultPlansOnly, Int32 memberID, bool allowAllColumnOrders, bool clonePlanCollection)
//		{
//			/*
//				Does 2 things
//				Filters out all plans that will not be displayed to the customers
//				Any plan with ColumnOrder >= 99 will be removed
//				If showSiteDefaultPlansOnly is true, then only show plans with a plan type
//				of site default.  However, if showSiteDefaultPlansOnly is false, then possibly 
//				include all plan types.    
//				Also populates the column placeholder array and the row placeholder array
//				and sorts both arrays				
//			*/			
//
//			if (clonePlanCollection)
//			{
//				sourcePlans = (PlanCollection) sourcePlans.Clone();
//			}
//
//			PlanCollection arrangedPlans = null;
//			decimal remainingCredit = Constants.NULL_DECIMAL;
//			bool checkedRemainingCredit = false;
//			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = null;
//
//			if (sourcePlans != null)
//			{
//				foreach (Plan plan in sourcePlans)
//				{
//					if (plan.DisplayMetaData != null)
//					{
//						if (((plan.DisplayMetaData.ColumnGroup < 99 && !allowAllColumnOrders) 
//								|| (allowAllColumnOrders))  
//							&& ((showSiteDefaultPlansOnly && ((plan.PlanTypeMask & PlanType.SiteDefault) == PlanType.SiteDefault))
//								|| (!showSiteDefaultPlansOnly)))
//						{
//							if (arrangedPlans == null)
//							{
//								arrangedPlans = new PlanCollection(brandID);
//							}
//
//							if (!checkedRemainingCredit)
//							{
//								brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
//
//								// Calculate the remaining credit for a member just once
//								remainingCredit = (PurchaseBL.GetRemainingCredit(memberID, brand)).TotalCreditedAmount;
//
//								checkedRemainingCredit = true;
//							}
//
//							// Set the remaining credit for the plan
//							plan.CreditAmount = remainingCredit;
//
//							// Set the purchase mode for the plan
//							plan.PurchaseMode = PurchaseBL.GetPurchaseMode(memberID, plan, brand); 
//
//							if (Plan.IsPurchaseModeValidForCredit(plan.PurchaseMode))
//							{
//								plan.DisplayMetaData.EnabledForMember = this.IsPurchaseAllowedWithRemainingCredit(plan, remainingCredit);
//							}
//							else
//							{
//								// Member is not upgrading his plan so no remaining credit will be applied
//								plan.DisplayMetaData.EnabledForMember = true;
//							}
//
//							if (!arrangedPlans.DisplayMetaData.AllColumnPlaceholders.Contains(plan.DisplayMetaData.ColumnGroup))
//							{
//								arrangedPlans.DisplayMetaData.AllColumnPlaceholders.Add(plan.DisplayMetaData.ColumnGroup);								
//							}
//
//							if (!arrangedPlans.DisplayMetaData.AllRowPlaceholders.Contains(plan.DisplayMetaData.ListOrder))
//							{
//								arrangedPlans.DisplayMetaData.AllRowPlaceholders.Add(plan.DisplayMetaData.ListOrder);								
//							}
//										
//							arrangedPlans.Add(plan);
//						}		
//					}
//				}
//			}
//			
//			if (arrangedPlans != null)
//			{
//				arrangedPlans.DisplayMetaData.AllColumnPlaceholders.Sort();
//				arrangedPlans.DisplayMetaData.AllRowPlaceholders.Sort();
//			}
//
//			return arrangedPlans;
//		}
//
//		public void ResetBestValuePlanInPlanCollection(PlanCollection sourcePlans, Int32 planID)
//		{
//			// Allows overriding the best value plan returned from promo engine
//			// Without this, the best value plan would be set when getting the
//			// site default plans.  
//			foreach (Plan plan in sourcePlans)
//			{
//				if (plan != null)
//				{
//					if (plan.PlanID == planID)
//					{
//						plan.BestValueFlag = true;
//					}
//					else
//					{
//						plan.BestValueFlag = false;
//					}
//				}
//			}
//		}

		/// <summary>
		/// Check if the member is allowed to purchase the selected plan given his remaining credit amount     
		/// </summary>
		/// <param name="plan">Plan being checked</param>
		/// <param name="remainingCreditAmount">Credit amount for the member being checked</param>
		/// <returns name="bool">Returns true if the remaining credit amount does not exceed the purchase rules for a particular payment type or false if the remaining credit amount does exceed the purchase rules for a particular payment type</returns>
		public bool IsPurchaseAllowedWithRemainingCredit(Plan plan, decimal remainingCreditAmount)
		{
			bool isPurchaseAllowedWithRemainingCredit = false;

			// Member is upgrading his plan so allow remaining credit to be applied  
			if (plan.PlanResourcePaymentTypeID == PaymentType.CreditCard)
			{
				// For credit card payment allow the member to upgrade to this plan only
				// if the cost of the plan is greater than the remaining credit
				isPurchaseAllowedWithRemainingCredit = (plan.InitialCost > remainingCreditAmount);
			}
			else if (plan.PlanResourcePaymentTypeID == PaymentType.Check)
			{
				// For check payment allow the member to upgrade to this plan only
				// if the net cost of the plan after applying the remaining credit 
				// is greater than $100.00
				isPurchaseAllowedWithRemainingCredit = ((plan.InitialCost - remainingCreditAmount) > 100.00m);
			}

			return isPurchaseAllowedWithRemainingCredit;
		}

		/// <summary>
		/// Check if the purchase mode and remaining credit plan display information is out of date as well as whether 
		/// the plan should really be enabled or not     
		/// </summary>
		/// <param name="memberID">Member ID being checked</param>
		/// <param name="originalCreditAmount">Remaining credit plan display information</param>
		/// <param name="originalPurchaseMode">Purchase mode plan display information</param>
		/// <param name="brandID">Brand ID being checked</param>
		/// <param name="planID">Plan ID being checked</param>
		/// <param name="paymentType">Type of payment in the display</param>
		/// <returns name="PlanDisplayValidation">Returns any plan display validation error</returns>
		public PlanDisplayValidation IsPlanDisplayOutdated(Int32 memberID, decimal originalCreditAmount, PurchaseMode originalPurchaseMode, Int32 brandID, Int32 planID, PaymentType paymentType)
		{
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
            decimal mostRecentCreditAmount = 0;// PurchaseBL.GetRemainingCredit(memberID, brand).TotalCreditedAmount;		
			
			return IsPlanDisplayOutdated(memberID, originalCreditAmount, mostRecentCreditAmount,originalPurchaseMode, brandID, planID, paymentType);
		}

		/// <summary>
		/// Check if the purchase mode and remaining credit plan display information is out of date as well as whether 
		/// the plan should really be enabled or not     
		/// </summary>
		/// <param name="memberID">Member ID being checked</param>
		/// <param name="originalCreditAmount">Remaining credit plan display information</param>
		/// <param name="newCreditAmount">Most recent credit amount for the member</param>
		/// <param name="originalPurchaseMode">Purchase mode plan display information</param>
		/// <param name="brandID">Brand ID being checked</param>
		/// <param name="planID">Plan ID being checked</param>
		/// <param name="paymentType">Type of payment in the display</param>
		/// <returns name="PlanDisplayValidation">Returns any plan display validation error</returns>
		public PlanDisplayValidation IsPlanDisplayOutdated(Int32 memberID, decimal originalCreditAmount, decimal newCreditAmount, PurchaseMode originalPurchaseMode, Int32 brandID, Int32 planID, PaymentType paymentType)
		{
			PlanDisplayValidation planDisplayValidation = PlanDisplayValidation.None;

			Plan plan = this.GetPlans(brandID, paymentType).FindByID(planID);
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
 
			// Get most recent purchase mode 
			PurchaseMode mostRecentPurchaseMode = PurchaseBL.GetPurchaseMode(memberID, plan, brand);  
			if (mostRecentPurchaseMode != originalPurchaseMode)
			{
				// Original display of selected plan is outdated because the purchase mode against this plan has changed
				planDisplayValidation = PlanDisplayValidation.PurchaseModeChanged;
			}

			if (planDisplayValidation == PlanDisplayValidation.None)
			{
				decimal mostRecentCreditAmount = newCreditAmount;

				if (mostRecentCreditAmount != originalCreditAmount)
				{
					// The original display the selected plan is outdated because the unused credit amount has changed  
					planDisplayValidation = PlanDisplayValidation.RemainingCreditChanged;
				}
			}

			if (planDisplayValidation == PlanDisplayValidation.None)
			{
				// Purchase mode for the member is the same
				// Remaining credit for the member is the same
				// However, the plan selected for purchase may have changed where the member is no longer allowed
				// to purchase this plan with his remaining credit
				// For example, the selected Plan ID was tampered although the purchase mode and remaining credit
				// stayed the same.  In this case, the remaining credit may be more than the cost of the plan if 
				// the plan was supposed to be a 6 month purchase but is now a 1 month purchase.
				// Or the initial cost of the plan may have changed  
				// This check is essential to prevent crediting a member for a new purchase  
				if (Plan.IsPurchaseModeValidForCredit(originalPurchaseMode))
				{	
					if (!this.IsPurchaseAllowedWithRemainingCredit(plan, originalCreditAmount))
					{						
						planDisplayValidation = PlanDisplayValidation.PurchaseAllowedWithRemainingCreditChanged;
					}
				}
			}

			return planDisplayValidation;
		}

		#endregion
		
		/// <summary>
		/// When the replicator figures out the partner URI, this method will be called to bust the cache.
		/// </summary>
		/// <param name="replicationAction"></param>
		public void PlayReplicationAction(IReplicationAction replicationAction)
		{
			if(replicationAction is ExpirePlanCollection)
			{
				ExpirePlanCollection expirePlanCollectionAction = replicationAction as ExpirePlanCollection;
				ExpireCachedPlanCollection(expirePlanCollectionAction.CacheKey);
			}
		}
		
		/// <summary>
		/// Expires a PlanCollection from the current cache
		/// </summary>
		/// <param name="cacheKey"></param>
		public void ExpireCachedPlanCollection(string cacheKey)
		{
			_cache.Remove(cacheKey);
		}
	}
}
