using System;
using System.Data;
using System.EnterpriseServices;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.Purchase.BusinessLogic
{
	/// <summary>
	/// Summary description for TransactionalWriter.
	/// </summary>
	[Transaction(TransactionOption.Required)]
	public class TransactionalWriter : ServicedComponent
	{
		/// <summary>
		/// Empty constructor
		/// </summary>
		public TransactionalWriter()
		{			
		}

		/// <summary>
		/// Insert a new plan into the DB
		/// </summary>
		/// <param name="plan"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public bool PersistPlan(Plan plan, int brandID)
		{
			bool result = false;

			if (plan != null)
			{
				Command command;
				SyncWriter sw;
				Exception ex;

				try
				{
					#region Task 1 - Save Plan
					command = null;
					sw = null;
					ex = null;

					// We need to get key value PlanGroup table.
					// Tables themselves do not enforce this unique constraint
					int planGroupId = KeySA.Instance.GetKey("PlanGroupID");
				
					command = new Command("mnSystem", "dbo.up_Plan_Save", 0);
					
					// Although we don't need to insert anything into PlanResource table going forward, we must still do it
					// to support the older plans that exist already.  Depending on the PaymentType, we will insert 1 or 2
					// records.
					int planResourceId = Constants.NULL_INT;
					if(plan.PaymentTypeMask == (PaymentType.Check | PaymentType.CreditCard))
					{
						planResourceId = KeySA.Instance.GetKey("PlanResourceID");
						command.AddParameter("@PlanResourceID_1", SqlDbType.Int, ParameterDirection.Input, planResourceId);

						planResourceId = KeySA.Instance.GetKey("PlanResourceID");
						command.AddParameter("@PlanResourceID_2", SqlDbType.Int, ParameterDirection.Input, planResourceId);
					}
					else
					{
						planResourceId = KeySA.Instance.GetKey("PlanResourceID");
						command.AddParameter("@PlanResourceID_1", SqlDbType.Int, ParameterDirection.Input, planResourceId);
					}				
				
					command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, plan.PlanID);
					command.AddParameter("@PlanGroupID", SqlDbType.Int, ParameterDirection.Input, planGroupId);
					command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)plan.CurrencyType);
					command.AddParameter("@InitialCost", SqlDbType.Decimal, ParameterDirection.Input, plan.InitialCost);
					command.AddParameter("@InitialDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)plan.InitialDurationType);
					command.AddParameter("@InitialDuration", SqlDbType.Int, ParameterDirection.Input, plan.InitialDuration);
					command.AddParameter("@RenewCost", SqlDbType.Decimal, ParameterDirection.Input, plan.RenewCost);
					command.AddParameter("@RenewDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)plan.RenewDurationType);
					command.AddParameter("@RenewDuration", SqlDbType.Int, ParameterDirection.Input, plan.RenewDuration);
					command.AddParameter("@PlanTypeMask", SqlDbType.Int, ParameterDirection.Input, (int)plan.PlanTypeMask);
					command.AddParameter("@RenewAttempts", SqlDbType.Int, ParameterDirection.Input, plan.RenewAttempts);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, brandID);
					command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, plan.StartDate);
					command.AddParameter("@PaymentTypeMask", SqlDbType.Int, ParameterDirection.Input, (int)plan.PaymentTypeMask);
					command.AddParameter("@PremiumTypeMask", SqlDbType.Int, ParameterDirection.Input, (int)plan.PremiumTypeMask);

					sw = new SyncWriter();
					sw.Execute(command, null, out ex);
					sw.Dispose();

					if (ex != null)
						throw ex;
					#endregion

					#region Task 2 - Save the Price breakdowns (PlanService table rows)
					if (plan.PlanServices != null)
					{
						for (int i=0; i < plan.PlanServices.Count; i++)
						{
							command = null;
							sw = null;
							ex = null;

							command = new Command("mnSystem", "up_PlanService_Insert", 0);
                            command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, plan.PlanID);
							command.AddParameter("@PlanServiceDefinitionID", SqlDbType.Int, ParameterDirection.Input, (int)plan.PlanServices[i].PlanSvcDefinition);
							command.AddParameter("@BasePrice", SqlDbType.Money, ParameterDirection.Input, plan.PlanServices[i].BasePrice);
							command.AddParameter("@RenewalPrice", SqlDbType.Money, ParameterDirection.Input, plan.PlanServices[i].RenewalPrice);

							sw = new SyncWriter();
							sw.Execute(command, null, out ex);
							sw.Dispose();

							if (ex != null)
								throw ex;
						}
					}
					#endregion

					ContextUtil.SetComplete();
					
					result = true;
				}
				catch (Exception exception)
				{
					ContextUtil.SetAbort();
					throw exception;
				}
				finally
				{}
			}
			else
			{
				System.Diagnostics.EventLog.WriteEntry("PlanBL.TransactionalWriter.PersistPlan()", "failed saving Plan. Plan object is null!", System.Diagnostics.EventLogEntryType.Warning); 
			}
			
			return result;
		}
	}
}
