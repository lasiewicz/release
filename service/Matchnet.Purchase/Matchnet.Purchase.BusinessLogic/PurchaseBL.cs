using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Diagnostics;
using System.Text;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Queuing;
using Matchnet.Security;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.Purchase.BusinessLogic
{
	public class PurchaseBL
	{
		public const string PRIMARYKEY_MEMBERTRANID = "MemberTranID";
		public const string PRIMARYKEY_MEMBERPAYMENTID = "MemberPaymentID";
		public const string PRIMARYKEY_MEMBERSUBID = "MemberSubID";
		public const string PRIMARYKEY_CHARGEID = "ChargeID";
		public const string PRIMARYKEY_PURCHASEREASONID = "PurchaseReasonID";
		public const string PRIMARYKEY_ADMINPAYMENTATTRIBUTE_ID = "AdminPaymentAttributeID";
		public const string PRIMARYKEY_CSLOOKUP_ID = "CSLookupID";
		public const string PRIMARYKEY_UPSLEGACYDATAID = "UPSLegacyDataID";

		private const byte QUEUEPROCESSOR_THREAD_COUNT = 4;

		private Matchnet.Queuing.Processor _queueProcessorOutbound;
		private Matchnet.Queuing.Processor _queueProcessorInbound;

		private Matchnet.Caching.Cache _cache;

		private static ArrayList _upsHardDeclineCodes;
		private static object _upsSyncBlock = new object();

		public PurchaseBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}

		public void Start()
		{
			//calling getqueue here will create the queues if they do not exist
			Matchnet.Queuing.Util.GetQueue(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, true, true);
			Matchnet.Queuing.Util.GetQueue(PurchaseConstants.QUEUEPATH_PURCHASE_INBOUND, true, true);

			_queueProcessorOutbound = new Matchnet.Queuing.Processor(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND,
				QUEUEPROCESSOR_THREAD_COUNT,
				true,
				new ProcessorWorkerOutbound(),
				Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME);
			_queueProcessorOutbound.Start();

			_queueProcessorInbound = new Matchnet.Queuing.Processor(PurchaseConstants.QUEUEPATH_PURCHASE_INBOUND,
				QUEUEPROCESSOR_THREAD_COUNT,
				true,
				new ProcessorWorkerInbound(),
				Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME);
			_queueProcessorInbound.Start();
		}


		public void Stop()
		{
			_queueProcessorOutbound.Stop();
			_queueProcessorInbound.Stop();

			_queueProcessorOutbound.Join();
			_queueProcessorInbound.Join();
		}

        /// <summary>
        /// Returns member subscription status
        /// </summary>
        /// <param name="memberid"></param>
        /// <param name="siteid"></param>
        /// <param name="memberSubEndDate"></param>
        /// <returns></returns>
        public static MemberSubscriptionStatus GetMemberSubStatus(int memberid, int siteid, DateTime memberSubEndDate)
        {
            MemberSubscriptionStatus oResult = null;

            #region Status via Legacy Data
            //SqlDataReader dataReader = null;
            //try
            //{
            //    Command command = new Command("mnSubscription", "up_Member_Sub_Status", 0);				
            //    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberid);
            //    command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteid);
            //    if (memberSubEndDate!=DateTime.MinValue)
            //    {
            //        command.AddParameter("@SubExpirationDate", SqlDbType.DateTime, ParameterDirection.Input,  memberSubEndDate);
            //    }
            //    else
            //    {
            //        command.AddParameter("@SubExpirationDate", SqlDbType.DateTime, ParameterDirection.Input,  DBNull.Value);
            //    }

            //    bool hasActivePremiumService = (PremiumServiceSA.Instance.GetMemberActivePremiumServices(memberid, PremiumServiceUtil.getBrand(siteid)).Count > 0 ? true : false);
            //    if (hasActivePremiumService)
            //    {
            //        // Member has at least one premium service that is still active
            //        command.AddParameter("@IsPremiumMember", SqlDbType.Int, ParameterDirection.Input,  1);
            //    }
            //    else
            //    {
            //        // Member has no active premium services
            //        command.AddParameter("@IsPremiumMember", SqlDbType.Int, ParameterDirection.Input,  0);
            //    }					

            //    dataReader = Client.Instance.ExecuteReader(command);

            //    while (dataReader.Read())
            //    {
            //        int status=dataReader.GetInt16(0);
            //        if (status>0)
            //        {
            //            oResult= new MemberSubscriptionStatus();
            //            oResult.Status= (SubscriptionStatus) status;

            //        }

            //    }

            //    return oResult;
            //}
            //catch (Exception ex)
            //{
            //    throw new BLException("GetMemberSubStatus() error (memberID:" + memberid.ToString()+"siteID: " + siteid.ToString() + ").", ex);
            //}
            //finally
            //{
            //    if (dataReader != null)
            //    {
            //        dataReader.Close();
            //    }
            //}
            #endregion

            #region Status via UPS Access
            Spark.Common.Adapter.AccessServiceAdapter accessServiceAdapter = new Spark.Common.Adapter.AccessServiceAdapter();
            try
            {
                Spark.Common.AccessService.SubscriberStatus subscriberstatus = accessServiceAdapter.GetProxyInstanceForBedrock().GetSubscriberStatus(memberid, siteid);

                oResult = new MemberSubscriptionStatus();
                switch (subscriberstatus)
                {
                    case Spark.Common.AccessService.SubscriberStatus.NonSubscriberExSubscriber:
                        oResult.Status = SubscriptionStatus.NonSubscriberExSubscriber;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.NonSubscriberFreeTrial:
                        oResult.Status = SubscriptionStatus.NonSubscriberFreeTrial;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.NonSubscriberNeverSubscribed:
                        oResult.Status = SubscriptionStatus.NonSubscriberNeverSubscribed;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.RenewalSubscriber:
                        oResult.Status = SubscriptionStatus.RenewalSubscribed;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.RenewalWinBack:
                        oResult.Status = SubscriptionStatus.RenewalWinBack;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.SubscriberFreeTrial:
                        oResult.Status = SubscriptionStatus.SubscriberFreeTrial;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.SubscriberFTS:
                        oResult.Status = SubscriptionStatus.SubscribedFTS;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.SubscriberWinBack:
                        oResult.Status = SubscriptionStatus.SubscribedWinBack;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.PremiumRenewalSubscriber:
                    case Spark.Common.AccessService.SubscriberStatus.PremiumRenewalWinBack:
                    case Spark.Common.AccessService.SubscriberStatus.PremiumSubscriber:
                    case Spark.Common.AccessService.SubscriberStatus.PremiumSubscriberFTS:
                    case Spark.Common.AccessService.SubscriberStatus.PremiumSubscriberWinBack:
                        oResult.Status = SubscriptionStatus.PremiumSubscriber;
                        break;
                    case Spark.Common.AccessService.SubscriberStatus.PausedSubscriber:
                        oResult.Status = SubscriptionStatus.PausedSubscriber;
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new BLException("GetMemberSubStatus() error (memberID:" + memberid.ToString() + "siteID: " + siteid.ToString() + ").", ex);
            }
            finally
            {
                accessServiceAdapter.CloseProxyInstance();
            }

            return oResult;
            #endregion


        }

        /// <summary>
        /// Compares a member against a plan and classifies the purchase type  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="plan">Plan being checked</param>
        /// <param name="brand">Brand</param>
        /// <returns name="PurchaseMode">Returns the purchase type</returns>
        public static PurchaseMode GetPurchaseMode(Int32 memberID, Plan plan, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            PurchaseMode purchaseMode = PurchaseMode.None;

            try
            {
                if ((plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                {
                    // Only new ala carte items will be offered to the member on the ala carte page so
                    // any ala carte purchases is a new purchase.  All renewals including ala carte
                    // renewals have PurchaseMode set to None.
                    // The PurchaseMode for an ala carte item needs to be set before displaying 
                    // this ala carte item to the member on the ala carte page
                    return PurchaseMode.New;
                }
                else if ((plan.PlanTypeMask & PlanType.Discount) == PlanType.Discount)
                {
                    // Set the PurchaseMode to None for all discount plans
                    return PurchaseMode.None;
                }

                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                if (member != null)
                {
                    DateTime currentDate = DateTime.Now;

                    DateTime dtSubEndDate = member.GetAttributeDate(brand, PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE, DateTime.MinValue);
                    if (dtSubEndDate != DateTime.MinValue)
                    {
                        if (dtSubEndDate <= currentDate)
                        {
                            // Subscription date exists but is expired
                            // Member is now an ex-subscriber
                            purchaseMode = PurchaseMode.New;
                        }
                        else
                        {
                            // Subscription expiration date is still valid
                            // Check what premium services the member has and compare this to the premium services 
                            // that the plan offers to classify the purchase type
                            Spark.Common.AccessService.AccessPrivilege accessPrivilege = null;
                            PremiumType memberHasPremiumTypeMask = PremiumType.None;
                            DateTime dtHighlightedEndDate = DateTime.MinValue;
                            accessPrivilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                            if (accessPrivilege != null)
                            {
                                dtHighlightedEndDate = accessPrivilege.EndDatePST;
                            }

                            DateTime dtSpotlightEndDate = DateTime.MinValue;
                            accessPrivilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                            if (accessPrivilege != null)
                            {
                                dtSpotlightEndDate = accessPrivilege.EndDatePST;
                            }

                            DateTime dtJMeterEndDate = DateTime.MinValue;
                            accessPrivilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.JMeter, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                            if (accessPrivilege != null)
                            {
                                dtJMeterEndDate = accessPrivilege.EndDatePST;
                            }

                            DateTime dtReadReceiptEndDate = DateTime.MinValue;
                            accessPrivilege = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.ReadReceipt, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                            if (accessPrivilege != null)
                            {
                                dtReadReceiptEndDate = accessPrivilege.EndDatePST;
                            }

                            if (dtHighlightedEndDate != DateTime.MinValue)
                            {
                                if (dtHighlightedEndDate > currentDate)
                                {
                                    // Member has highlight profile 
                                    memberHasPremiumTypeMask = (memberHasPremiumTypeMask | PremiumType.HighlightedProfile);
                                }
                            }

                            if (dtSpotlightEndDate != DateTime.MinValue)
                            {
                                if (dtSpotlightEndDate > currentDate)
                                {
                                    // Member has spotlight member 
                                    memberHasPremiumTypeMask = (memberHasPremiumTypeMask | PremiumType.SpotlightMember);
                                }
                            }

                            if (dtJMeterEndDate != DateTime.MinValue)
                            {
                                if (dtJMeterEndDate > currentDate)
                                {
                                    // Member has JMeter premium service
                                    memberHasPremiumTypeMask = (memberHasPremiumTypeMask | PremiumType.JMeter);
                                }
                            }

                            if (dtReadReceiptEndDate != DateTime.MinValue)
                            {
                                if (dtReadReceiptEndDate > currentDate)
                                {
                                    // Member has ReadReceipt premium service
                                    memberHasPremiumTypeMask = (memberHasPremiumTypeMask | PremiumType.ReadReceipt);
                                }
                            }

                            // Compare now what the member has to what the plan offers
                            if (memberHasPremiumTypeMask == plan.PremiumTypeMask)
                            {
                                // Extension mode
                                // 1) Member is standard and plan is standard
                                // 2) Member is premium and the plan has the same premium services
                                purchaseMode = PurchaseMode.Extension;
                            }
                            else
                            {
                                Int16 memberPremiumVal = Convert.ToInt16(memberHasPremiumTypeMask);
                                Int16 planPremiumVal = Convert.ToInt16(plan.PremiumTypeMask);

                                if ((memberHasPremiumTypeMask == PremiumType.None && planPremiumVal > 0)
                                    || (((plan.PremiumTypeMask & memberHasPremiumTypeMask) == memberHasPremiumTypeMask)
                                    && (planPremiumVal > memberPremiumVal))
                                    )
                                {
                                    // Upgrade mode
                                    // 1) Member is standard and plan is premium
                                    // 2) Member has 1 premium service and plan has same premium service and additional premium service
                                    purchaseMode = PurchaseMode.Upgrade;
                                }
                                else if ((plan.PremiumTypeMask == PremiumType.None && memberPremiumVal > 0)
                                    || (((memberHasPremiumTypeMask & plan.PremiumTypeMask) == plan.PremiumTypeMask)
                                    && (memberPremiumVal > planPremiumVal))
                                    )
                                {
                                    // Downgrade mode
                                    // 1) Member is premium and plan is standard
                                    // 2) Plan has 1 premium service and member has same premium service and additional premium service
                                    purchaseMode = PurchaseMode.Downgrade;
                                }
                                else if ((memberPremiumVal > 0 && planPremiumVal > 0)
                                    && (
                                    ((memberHasPremiumTypeMask & plan.PremiumTypeMask) == PremiumType.None)
                                    ||
                                    (Convert.ToInt32(memberHasPremiumTypeMask & plan.PremiumTypeMask) > 0)
                                    )
                                    )
                                {
                                    // Swap mode
                                    // 1) Plan has 1 type of premium service and member has a different type of premium service and both do not intersect
                                    // 2) Plan and member have the same premium service but both have another premium service that neither share with each other  
                                    purchaseMode = PurchaseMode.Swap;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Subscription expiration date does not exists
                        // Member cannot have a premium expiration date since premium privilege time cannot go past standard
                        // privilege time
                        // Member is a new subscriber
                        purchaseMode = PurchaseMode.New;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("GetPurchaseMode() error (memberID:" + memberID.ToString() + ", planID: " + plan.PlanID.ToString() + ").", ex);
            }

            return purchaseMode;
        }

        public TransactionReasonCollection GetTransactionReasons()
        {
            SqlDataReader dataReader = null;

            try
            {
                TransactionReasonCollection transactionReasonCollection = _cache.Get(TransactionReasonCollection.GetCacheKey(TransactionReasonType.All)) as TransactionReasonCollection;

                if (transactionReasonCollection == null)
                {
                    transactionReasonCollection = new TransactionReasonCollection();

                    Command command = new Command("mnSystem", "dbo.up_TransactionReason_List_ByType", 0);
                    dataReader = Client.Instance.ExecuteReader(command);

                    bool colLookupDone = false;
                    Int32 ordinalTransactionReasonID = Constants.NULL_INT;
                    Int32 ordinalTransactionReasonTypeID = Constants.NULL_INT;
                    Int32 ordinalDescription = Constants.NULL_INT;
                    Int32 ordinalResourceConstant = Constants.NULL_INT;
                    Int32 ordinalListOrder = Constants.NULL_INT;

                    while (dataReader.Read())
                    {
                        if (!colLookupDone)
                        {
                            ordinalTransactionReasonID = dataReader.GetOrdinal("TransactionReasonID");
                            ordinalTransactionReasonTypeID = dataReader.GetOrdinal("TransactionReasonTypeID");
                            ordinalDescription = dataReader.GetOrdinal("Description");
                            ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
                            ordinalListOrder = dataReader.GetOrdinal("ListOrder");
                            colLookupDone = true;
                        }

                        TransactionReason transactionReason = new TransactionReason(dataReader.GetInt32(ordinalTransactionReasonID),
                            (TransactionReasonType)Enum.Parse(typeof(TransactionReasonType), dataReader.GetInt32(ordinalTransactionReasonTypeID).ToString()),
                            dataReader.GetString(ordinalDescription),
                            dataReader.GetString(ordinalResourceConstant),
                            dataReader.GetInt32(ordinalListOrder));

                        transactionReasonCollection.Add(transactionReason);
                    }

                    _cache.Add(transactionReasonCollection);
                }

                return transactionReasonCollection;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured when getting transaction reasons.", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

        public TransactionReasonSiteCollection GetTransactionReasonsSite(Int32 siteID)
        {
            SqlDataReader dataReader = null;

            try
            {
                TransactionReasonSiteCollection transactionReasonSiteCollection = _cache.Get(TransactionReasonSiteCollection.GetCacheKey(siteID)) as TransactionReasonSiteCollection;

                if (transactionReasonSiteCollection == null)
                {
                    Command command = new Command("mnSystem", "dbo.up_TransactionReason_List", 0);
                    command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
                    dataReader = Client.Instance.ExecuteReader(command);

                    transactionReasonSiteCollection = new TransactionReasonSiteCollection(siteID);

                    bool colLookupDone = false;
                    Int32 ordinalTransactionReasonID = Constants.NULL_INT;
                    Int32 ordinalTransactionReasonTypeID = Constants.NULL_INT;
                    Int32 ordinalDescription = Constants.NULL_INT;
                    Int32 ordinalResourceConstant = Constants.NULL_INT;
                    Int32 ordinalListOrder = Constants.NULL_INT;

                    while (dataReader.Read())
                    {
                        if (!colLookupDone)
                        {
                            ordinalTransactionReasonID = dataReader.GetOrdinal("TransactionReasonID");
                            ordinalTransactionReasonTypeID = dataReader.GetOrdinal("TransactionReasonTypeID");
                            ordinalDescription = dataReader.GetOrdinal("Description");
                            ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
                            ordinalListOrder = dataReader.GetOrdinal("ListOrder");
                            colLookupDone = true;
                        }

                        TransactionReason transactionReason = new TransactionReason(dataReader.GetInt32(ordinalTransactionReasonID),
                            (TransactionReasonType)Enum.Parse(typeof(TransactionReasonType), dataReader.GetInt32(ordinalTransactionReasonTypeID).ToString()),
                            dataReader.GetString(ordinalDescription),
                            dataReader.GetString(ordinalResourceConstant),
                            dataReader.GetInt16(ordinalListOrder));

                        transactionReasonSiteCollection.Add(transactionReason);
                    }

                    _cache.Add(transactionReasonSiteCollection);
                }

                return transactionReasonSiteCollection;
            }
            catch (Exception ex)
            {
                throw new BLException("GetTransactionReasonsSite() error (siteID: " + siteID.ToString() + ").", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

        private Int32 validatePlan(Int32 planID,
            Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 brandID)
        {

            return this.validatePlan(planID, memberID, adminMemberID, siteID, brandID, null, Constants.NULL_DECIMAL, Constants.NULL_DECIMAL, PurchaseMode.None, PaymentType.None);
        }

        private Int32 validatePlan(Int32 planID,
            Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 brandID,
            SubscriptionResult subRes,
            decimal UICreditAmount,
            decimal RefreshedCreditAmount,
            PurchaseMode purchaseMode,
            PaymentType paymentType)
        {
            int returnValue = Constants.RETURN_OK;

            //ensure plan exists and is active
            Command command = new Command("mnSubscription", "dbo.up_Plan_Validate", 0);
            command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, brandID);
            command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
            if (adminMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@AdminFlag", SqlDbType.Bit, ParameterDirection.Input, adminMemberID);
            }
            returnValue = Convert.ToInt32(Client.Instance.ExecuteNonQuery(command)["@ReturnValue"].Value);

            //PurchaseMode.None indicates that it came from a non-ESPremium enabled site, using one of the older overloads for BeginCreditCardXXX() that doesn't have PurchaseMode.
            if (returnValue == Constants.RETURN_OK && subRes != null && paymentType != PaymentType.None && purchaseMode != PurchaseMode.None)
            {
                //determine whether plan's display has changed since being displayed to user
                subRes.PlanDisplayValidationResult = PlanBL.Instance.IsPlanDisplayOutdated(memberID, UICreditAmount, RefreshedCreditAmount, purchaseMode, brandID, planID, paymentType);

                if (subRes.PlanDisplayValidationResult == PlanDisplayValidation.PurchaseModeChanged)
                    returnValue = (int)ResourceConstants.PLAN_PURCHASE_VALIDATION_PURCHASE_MODE_CHANGED;
                else if (subRes.PlanDisplayValidationResult == PlanDisplayValidation.RemainingCreditChanged)
                    returnValue = (int)ResourceConstants.PLAN_PURCHASE_VALIDATION_REMAINING_CREDIT_CHANGED;
                else if (subRes.PlanDisplayValidationResult == PlanDisplayValidation.PurchaseAllowedWithRemainingCreditChanged)
                    returnValue = (int)ResourceConstants.PLAN_PURCHASE_VALIDATION_NEGATIVE_PURCHASE_AMOUNT;

            }


            return returnValue;

        }

        public CreditCardCollection GetCreditCardTypes(
            Int32 siteID)
        {
            try
            {
                CreditCardCollection creditCardCollection = _cache.Get(CreditCardCollection.GetCacheKey(siteID)) as CreditCardCollection;

                if (creditCardCollection == null)
                {
                    SqlDataReader dataReader = null;
                    try
                    {
                        Command command = new Command("mnSystem", "dbo.up_CreditCard_List", 0);
                        command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
                        dataReader = Client.Instance.ExecuteReader(command);

                        creditCardCollection = new CreditCardCollection(siteID);

                        bool colLookupDone = false;
                        Int32 ordinalCreditCardID = Constants.NULL_INT;
                        Int32 ordinalContent = Constants.NULL_INT;
                        Int32 ordinalResourceConstant = Constants.NULL_INT;

                        while (dataReader.Read())
                        {
                            if (!colLookupDone)
                            {
                                ordinalCreditCardID = dataReader.GetOrdinal("CreditCardID");
                                ordinalContent = dataReader.GetOrdinal("Content");
                                ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
                                colLookupDone = true;
                            }

                            CreditCard creditCard = new CreditCard(
                                dataReader.GetInt32(ordinalCreditCardID),
                                dataReader.GetString(ordinalContent),
                                dataReader.GetString(ordinalResourceConstant));

                            creditCardCollection.Add(creditCard);
                        }

                        _cache.Add(creditCardCollection);
                    }
                    finally
                    {
                        if (dataReader != null)
                        {
                            dataReader.Close();
                        }
                    }
                }

                return creditCardCollection;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while retrieving credit card types.", ex);
            }
        }

        #region UPS Integration
        /// <summary>
        /// Used for the UPS integration 
        /// Checks for any validation errors when a member purchases a plan
        /// Since the plan has already been purchased through UPS, this valiation is only used to
        /// write the details of the validation error to the event log if there is a validation 
        /// error
        /// </summary>
        /// <param name="planID"></param>
        /// <param name="memberID"></param>
        /// <param name="adminMemberID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="UICreditAmount"></param>
        /// <param name="refreshedCreditAmount"></param>
        /// <param name="purchaseMode"></param>
        /// <param name="paymentType"></param>
        /// <param name="memberTranID"></param>
        /// <returns></returns>
        public SubscriptionResult UPSValidatePlanPurchase(Int32 planID,
            Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 brandID,
            decimal UICreditAmount,
            decimal refreshedCreditAmount,
            PurchaseMode purchaseMode,
            PaymentType paymentType,
            Int32 memberTranID)
        {
            SubscriptionResult subRes = new SubscriptionResult();

            //Validate Plan
            Int32 returnValue = validatePlan(planID, memberID, adminMemberID, siteID, brandID, subRes, UICreditAmount, refreshedCreditAmount, purchaseMode, PaymentType.CreditCard);
            if (returnValue != Constants.RETURN_OK)
            {
                StringBuilder errorLog = new StringBuilder();
                errorLog.Append(String.Format("Error in server validation for {0} purchase", Enum.GetName(typeof(PaymentType), paymentType)));
                errorLog.Append(String.Format("\n\tplanID: {0}", Convert.ToString(planID)));
                errorLog.Append(String.Format("\n\tmemberID: {0}", Convert.ToString(memberID)));
                errorLog.Append(String.Format("\n\tsiteID: {0}", Convert.ToString(siteID)));
                errorLog.Append(String.Format("\n\tbrandID: {0}", Convert.ToString(brandID)));
                errorLog.Append(String.Format("\n\tUICreditAmount: {0}", Convert.ToString(UICreditAmount)));
                errorLog.Append(String.Format("\n\trefreshedCreditAmount: {0}", Convert.ToString(refreshedCreditAmount)));
                errorLog.Append(String.Format("\n\tpurchaseMode: {0}", Convert.ToString(purchaseMode)));
                errorLog.Append(String.Format("\n\tpaymentType: {0}", Convert.ToString(paymentType)));
                errorLog.Append(String.Format("\n\tmemberTranID: {0}", Convert.ToString(memberTranID)));
                switch (returnValue)
                {
                    case 10010:
                        errorLog.Append(String.Format("\n\tValidation error type: {0}", "PLAN PURCHASE VALIDATION REMAINING CREDIT CHANGED"));
                        break;
                    case 10011:
                        errorLog.Append(String.Format("\n\tValidation error type: {0}", "PLAN PURCHASE VALIDATION PURCHASE MODE CHANGED"));
                        break;
                    case 10012:
                        errorLog.Append(String.Format("\n\tValidation error type: {0}", "PLAN PURCHASE VALIDATION NEGATIVE PURCHASE AMOUNT"));
                        break;
                    default:
                        break;
                }

                bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);
                if (IsValidForCredit)
                {
                    if (refreshedCreditAmount != Constants.NULL_DECIMAL)
                    {
                        Plan plan = PlanBL.Instance.GetPlans(brandID).FindByID(planID);

                        if (refreshedCreditAmount > plan.InitialCost)
                        {
                            errorLog.Append(String.Format("\n\tValidation error type: {0}", "PLAN PURCHASE VALIDATION INVALID CREDIT DISCOUNT"));
                        }
                    }
                }

                // Write to event log
                EventLog.WriteEntry(ValueObjects.ServiceConstants.SERVICE_NAME, errorLog.ToString(), EventLogEntryType.Error);
            }

            subRes.ReturnValue = returnValue;
            subRes.ResourceConstant = "";// getResourceConstant(returnValue);
            //subRes.MemberPaymentID = memberPaymentID;
            //subRes.MemberTranID = memberTranID;

            return subRes;
        }

        public void UPSLegacyDataSave(string upsLegacyData, int id)
        {
            Command command = new Command("mnSubscription", "dbo.up_UPSLegacyData_Insert", 0);
            command.AddParameter("@UPSLegacyDataID", SqlDbType.Int, ParameterDirection.Input, id);
            command.AddParameter("@UPSLegacyData", SqlDbType.Text, ParameterDirection.Input, upsLegacyData);
            //command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

            SyncWriter sw = new SyncWriter();
            Exception ex;
            Int32 returnValue = sw.Execute(command,
                out ex);
            sw.Dispose();
            if (ex != null)
            {
                throw ex;
            }

            return;
        }

        public string UPSLegacyDataGet(int upsLegacyDataID)
        {
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("mnSubscription", "dbo.up_UPSLegacyData_Select", 0);
                command.AddParameter("@UPSLegacyDataID", SqlDbType.Int, ParameterDirection.Input, upsLegacyDataID);
                dataReader = Client.Instance.ExecuteReader(command);

                while (dataReader.Read())
                {
                    return dataReader.GetString(dataReader.GetOrdinal("UPSLegacyData"));
                }

                return Constants.NULL_STRING;
            }
            catch (Exception ex)
            {
                throw new Exception("UPSLegacyDataGet() error (upsLegacyDataID: " + upsLegacyDataID.ToString() + ").", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

        public MemberTran UPSPopulateMemberTranOnCallBack(MemberTran memberTran)
        {
            return null;
        }

        private int UPSGetPrimaryPlanID(int[] planID, int brandID)
        {
            int legacyPlanIDFound = Constants.NULL_INT;
            int alaCartePlanIDFound = Constants.NULL_INT;
            int discountPlanIDFound = Constants.NULL_INT;

            if (planID.Length == 1)
            {
                return planID[0];
            }
            else
            {
                foreach (int singlePlanID in planID)
                {
                    Plan plan = PlanBL.Instance.GetPlans(brandID).FindByID(singlePlanID);

                    if (plan != null)
                    {
                        if ((plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                        {
                            // An ala carte item exists in the order
                            if (alaCartePlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                alaCartePlanIDFound = singlePlanID;
                            }
                        }
                        else if ((plan.PlanTypeMask & PlanType.Discount) == PlanType.Discount)
                        {
                            // A discount item exists in the order
                            if (discountPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                discountPlanIDFound = singlePlanID;
                            }
                        }
                        else
                        {
                            // The primary PlanID will be the item that is not an ala carte 
                            // or discount package  
                            if (legacyPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                legacyPlanIDFound = singlePlanID;
                            }
                        }
                    }
                }

                if (legacyPlanIDFound != Constants.NULL_INT)
                {
                    // Return any PlanID as the primary PlanID as long as this is not an ala carte 
                    // package or a discount package
                    return legacyPlanIDFound;
                }
                else if (alaCartePlanIDFound != Constants.NULL_INT)
                {
                    // If the member only purchased an ala carte package and no base subscription packages,
                    // than return the first ala carte package found
                    return alaCartePlanIDFound;
                }
                else
                {
                    // If the order did not have any base subscriptions or ala carte packages, than there
                    // no primary PlanID was found
                    // Do not return the discount PlanID since a discount cannot exists without a base 
                    // subscription or ala carte purchase  
                    return Constants.NULL_INT;
                }
            }
        }

        private string UPSGetDelimitedPlanIDList(int[] planIDList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int planID in planIDList)
            {
                if (sb.Length > 0)
                {
                    sb.Append(";");
                }
                sb.Append(Convert.ToString(planID));
            }

            return sb.ToString();
        }

        private static bool UPSDoesOrderContainALaCartePlan(int[] planID, int brandID)
        {
            foreach (int singlePlanID in planID)
            {
                Plan plan = PlanBL.Instance.GetPlans(brandID).FindByID(singlePlanID);

                if (plan != null)
                {
                    if ((plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                    {
                        return true;
                    }
                }
            }

            return false;

        }

        private static Plan UPSGetALaCartePlanFromList(int[] planID, int brandID, PremiumType premiumType)
        {
            Plan plan = null;

            foreach (int singlePlanID in planID)
            {
                plan = PlanBL.Instance.GetPlans(brandID).FindByID(singlePlanID);

                if (plan != null)
                {
                    if ((plan.PremiumTypeMask & premiumType) == premiumType)
                    {
                        return plan;
                    }
                }
            }

            return null;
        }

        private static Plan UPSGetALaCartePlanFromMemberSub(MemberSub memberSub, int brandID, PremiumType premiumType)
        {
            Plan plan = null;

            if (memberSub != null && memberSub.MemberSubExtended != null)
            {
                foreach (MemberSubExtended mse in memberSub.MemberSubExtended)
                {
                    plan = PlanBL.Instance.GetPlans(brandID).FindByID(mse.PlanID);

                    if (plan != null)
                    {
                        if ((plan.PremiumTypeMask & premiumType) == premiumType)
                        {
                            return plan;
                        }
                    }
                }
            }

            return null;
        }

        #endregion

        #region Obsolete
        /*
         * 
                public Int32 UPSProcessLegacyData(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planID,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            CreditCardType creditCardType,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            TranType tranType,
            Int32 referenceMemberPaymentID,
            CreditCardType referenceCreditCardType,
            decimal headerAmount,
            Int32 headerDuration,
            DurationType headerDurationType,
            CurrencyType currencyType,
            string lastFourAccountNumber,
            Int32 orderID,
            Int32 referenceOrderID,
            int commonResponseCode,
            Int32 memberTranStatusID,
            DateTime originalInsertDate,
            Int32 chargeID,
            OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS,
            Int32 paymentTypeID)
        {
            // Get the following from the mnSubscription database using the purchaseID
            //      PromoID
            //		OriginalPurchaseActionTypeForUPS
            // Get the following from the epOrder database using the orderID
            //		ChargeID	
            //		MemberTranStatus (same as ChargeStatus)
            //		CommonResponseCode
            //Int32 memberID = 0;
            //Int32 adminMemberID = 0;
            //Int32 planID = 0;
            //Int32 discountID = 0;
            //Int32 siteID = 0;
            //Int32 brandID = 0;
            //CreditCardType creditCardType = CreditCardType.Visa;
            //Int32 conversionMemberID = 0;
            //Int32 sourceID = 0;
            //Int32 purchaseReasonTypeID = 0;
            //string ipAddress = "";
            //Int32 promoID = 0;
            //decimal UICreditAmount = 0.0m;
            //PurchaseMode purchaseMode = PurchaseMode.None;
            //bool reusePreviousPayment = false;
            //string firstName = "";
            //string lastName = "";
            //int commonResponseCode = 0;
            ////string resourceConstant = "";
            //Int32 memberTranStatusID = 0;
            ////string responseCode = "";
            //Int32 providerID = 0;
            ////string AVSCode = "";
            ////string CVVCode = "";
            ////bool purchaseModeValidForCredit = false;
            //Int32 chargeID = 0;

            //OriginalPurchaseActionTypeForUPS originalPurchaseActionTypeForUPS = (OriginalPurchaseActionTypeForUPS) Enum.Parse(typeof(OriginalPurchaseActionTypeForUPS), purchaseActionTypeForUPS);

            PaymentType paymentType = (PaymentType)Enum.Parse(typeof(PaymentType), paymentTypeID.ToString());

            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = null;
            if (brandID < 0)
            {
                // Occurs when migrating data where there is no UPS legacy data for the order to provide the brand ID     
                brand = PremiumServiceUtil.getBrand(siteID);
                brandID = brand.BrandID;
            }

            switch (originalPurchaseActionTypeForUPS)
            {
                case OriginalPurchaseActionTypeForUPS.BeginCreditCardPurchase:
                    return UPSDoCreditCardPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        creditCardType,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID,
                        UICreditAmount,
                        purchaseMode,
                        reusePreviousPayment,
                        firstName,
                        lastName,
                        headerAmount,
                        headerDuration,
                        headerDurationType,
                        lastFourAccountNumber,
                        orderID,
                        commonResponseCode,
                        memberTranStatusID,
                        originalInsertDate,
                        chargeID);
                    break;
                case OriginalPurchaseActionTypeForUPS.BeginCheckPurchase:
                    return UPSDoCheckPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID,
                        UICreditAmount,
                        purchaseMode,
                        reusePreviousPayment,
                        firstName,
                        lastName,
                        headerAmount,
                        headerDuration,
                        headerDurationType,
                        lastFourAccountNumber,
                        orderID,
                        commonResponseCode,
                        memberTranStatusID,
                        originalInsertDate,
                        chargeID,
                        paymentType);
                    break;
                case OriginalPurchaseActionTypeForUPS.BeginCredit:
                    return UPSDoCredit(tranType,
                        memberID,
                        adminMemberID,
                        headerAmount,
                        referenceCreditCardType,
                        headerDuration,
                        headerDurationType,
                        currencyType,
                        firstName,
                        lastName,
                        lastFourAccountNumber,
                        orderID,
                        referenceOrderID,
                        commonResponseCode,
                        memberTranStatusID,
                        originalInsertDate,
                        chargeID);
                    break;
                case OriginalPurchaseActionTypeForUPS.BeginAuthPmtProfile:
                    return UPSDoAuthPmtProfile(memberID,
                        adminMemberID,
                        siteID,
                        creditCardType,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        referenceMemberPaymentID,
                        orderID,
                        commonResponseCode,
                        memberTranStatusID,
                        originalInsertDate,
                        chargeID);
                    break;
                case OriginalPurchaseActionTypeForUPS.BeginRenewal:
                    return UPSDoRenewal(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        creditCardType,
                        firstName,
                        lastName,
                        headerAmount,
                        headerDuration,
                        headerDurationType,
                        lastFourAccountNumber,
                        orderID,
                        commonResponseCode,
                        memberTranStatusID,
                        originalInsertDate,
                        chargeID,
                        paymentType);
                    break;
                case OriginalPurchaseActionTypeForUPS.BeginPaypalPurchase:
                    return UPSDoPaypalPurchase(memberID,
                        adminMemberID,
                        planID,
                        discountID,
                        siteID,
                        brandID,
                        conversionMemberID,
                        sourceID,
                        purchaseReasonTypeID,
                        ipAddress,
                        promoID,
                        UICreditAmount,
                        purchaseMode,
                        reusePreviousPayment,
                        firstName,
                        lastName,
                        headerAmount,
                        headerDuration,
                        headerDurationType,
                        lastFourAccountNumber,
                        orderID,
                        commonResponseCode,
                        memberTranStatusID,
                        originalInsertDate,
                        chargeID,
                        paymentType);
                    break;
                case OriginalPurchaseActionTypeForUPS.BeginUpdateSubscription:
                    return UPSUpdateSubscription(memberID,
                        siteID,
                        adminMemberID,
                        headerDuration,
                        headerDurationType,
                        planID,
                        brand,
                        (TimeAdjustOverrideType)promoID, // Really used for time adjustment override type ID
                        orderID,
                        commonResponseCode, // Really used for the transaction reason ID     
                        memberTranStatusID,
                        originalInsertDate);
                default:
                    break;
            }

            return Constants.NULL_INT;
        }

        public Int32 UPSDoCreditCardPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planIDList,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            CreditCardType creditCardType,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            decimal headerAmount, // Total amount includes amounts from multiple items as well as discounts  			
            int headerDuration, // For multiple item purchases in the same order
            DurationType headerDurationType, // For multiple purchases in the same order
            string lastFourAccountNumber,
            Int32 orderID, // Get after UPS is called 
            int commonResponseCode, // Get after UPS is called
            //string resourceConstant, // Get after UPS is called
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate,
            //string responseCode, // Get after UPS is called
            //string AVSCode, // Get after UPS is called
            //string CVVCode, // Get after UPS is called
            //bool purchaseModeValidForCredit, // Get after UPS is called
            Int32 chargeID) // Get after UPS is called
        {
            try
            {
                // The PurchaseMode is sent from the subscription page or the ala carte page and depends
                // on what the user selected
                // For an ala carte purchase the PurchaseMode is set to New
                // Renewals always have a PurchaseMode set to None
                bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

                MemberTranStatus memberTranStatus = MemberTranStatus.Pending;
                SubscriptionResult subRes = new SubscriptionResult();
                Int32 returnValue = Constants.RETURN_OK;

                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
                MemberTranCreditCollection tranCredits = null;// PurchaseBL.GetRemainingCredit(memberID, brand);
                decimal refreshedCreditAmount = Constants.NULL_DECIMAL;
                if (tranCredits != null)
                {
                    // Only used in server validation
                    refreshedCreditAmount = tranCredits.TotalCreditedAmount;
                }

                // Get the primary PlanID to use that will be saved in the new MemberTran
                int planID = UPSGetPrimaryPlanID(planIDList, brandID);
                if (planID == Constants.NULL_INT)
                {
                    throw new ApplicationException("Primary plan does not exists");
                }

                // Get plan object and initialize with values
                Plan plan = (Plan)PlanBL.Instance.GetPlans(brandID).FindByID(planID).Clone();
                plan.InitialCost = headerAmount;
                // The headerDuration and headerDurationType for an a la carte only purchase
                // is dynamic and will be provided by the calling application
                plan.InitialDuration = headerDuration;
                plan.InitialDurationType = headerDurationType;

                //decimal amount = plan.InitialCost;
                // Still populate the memberPaymentID field in MemberTran although this will not 
                // be used for anything since a new orderID field will be added to MemberTran
                // to find the user payment information  
                Int32 memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                // Create a new credit card payment and add to memberTran
                // Only used to store the credit card type
                CreditCardPayment creditCardPayment = new CreditCardPayment();
                creditCardPayment.CreditCardType = creditCardType;
                creditCardPayment.FirstName = firstName;
                creditCardPayment.LastName = lastName;

                // NEED TO CHECK THAT THE MEMBERTRAN HAS BOTH A DURATION AND DURATIONTYPE    
                // USED IN PREMIUMSERVICEUTIL  
                // THERE ARE 2 CHARGEID FOR AN ORDER
                // USE THE CHARGID FROM THE AUTHORIZATION   
                MemberTran mt = new MemberTran(memberTranID,
                    chargeID, // Only used in the PremiumServiceUtil.setPremiumServiceMember(), not actually saved in the database  	
                    Constants.NULL_INT,
                    memberID,
                    siteID,
                    plan.CurrencyType,
                    null,
                    (plan.PlanTypeMask & PlanType.AuthOnly) == PlanType.AuthOnly ? TranType.AuthorizationOnly : TranType.InitialBuy,
                    null,
                    Constants.NULL_INT,
                    plan.PlanID,
                    plan,
                    null,
                    discountID,
                    memberPaymentID,
                    creditCardPayment, // Set Payment to the CreditCardPayment that just stores the credit card type  
                    null,
                    null,
                    MemberTranStatus.None,
                    null,
                    null,
                    adminMemberID,
                    plan.InitialCost,
                    plan.InitialDuration,
                    plan.InitialDurationType,
                    null,
                    Constants.NULL_INT,
                    DateTime.Now,
                    DateTime.Now,
                    Constants.NULL_STRING,
                    null,
                    PaymentType.CreditCard,
                    null, // Set Merchant to null  
                    Matchnet.Duration.GetMinutes(plan.InitialDurationType, plan.InitialDuration));

                mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;
                mt.PurchaseMode = purchaseMode;
                mt.ReusePreviousPayment = reusePreviousPayment;
                mt.LegacyLastFourAccountNumber = lastFourAccountNumber;

                if (IsValidForCredit)
                {
                    mt.CreditAmount = UICreditAmount;
                    mt.MemberTranCreditCollection = tranCredits; //set collection of transactions to be used for crediting for later storage into MemberTranCredited table
                }
                //mt.Amount = totalAmount;

                UPSMemberTran upsMemberTran = new UPSMemberTran(mt);
                upsMemberTran.PackageID = planIDList;
                upsMemberTran.BrandID = brandID;
                upsMemberTran.OriginalInsertDate = originalInsertDate;
                //upsMemberTran.OrderID = orderID;

                returnValue = UPSBeginCreditCardPurchase(memberID,
                    adminMemberID,
                    planID,
                    UPSGetDelimitedPlanIDList(planIDList),
                    discountID,
                    siteID,
                    conversionMemberID,
                    0,//getPurchaseReasonID(purchaseReasonTypeID, sourceID),
                    purchaseReasonTypeID,
                    sourceID,
                    memberTranID,
                    memberPaymentID,
                    memberTranStatus,
                    upsMemberTran,
                    ipAddress,
                    creditCardType,
                    promoID,
                    reusePreviousPayment,
                    headerAmount,
                    headerDuration,
                    headerDurationType);

                // ProviderID will not be provided by the payment service in UPS
                // This was only used in saving to the ProviderID field in MemberTran as well
                // as renewal recycling 
                // Use GroupID in renewal recycling instead  
                Int32 providerID = Constants.NULL_INT;

                // POPULATE THE MEMBERTRAN OBJECT WITH THE REST OF THE DATA FROM UPS
                UPSMemberTranResult upsMemberTranResult = new UPSMemberTranResult(upsMemberTran,
                    commonResponseCode,
                    (MemberTranStatus)memberTranStatusID,
                    providerID,
                    orderID,
                    chargeID);
                UPSSaveComplete(upsMemberTranResult);

                SubscriptionResult valSubRes = UPSValidatePlanPurchase(planID,
                    memberID,
                    adminMemberID,
                    siteID,
                    brandID,
                    UICreditAmount,
                    refreshedCreditAmount,
                    purchaseMode,
                    PaymentType.CreditCard,
                    memberTranID);

                subRes.ReturnValue = returnValue;
                subRes.ResourceConstant = "";// getResourceConstant(returnValue);
                // The MemberPaymentID in this case will be the OrderID provided  
                subRes.MemberPaymentID = memberPaymentID;
                subRes.MemberTranID = memberTranID;

                //return subRes;

                return memberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while processing a UPS credit card purchase.", ex);
            }
        }


        public Int32 UPSDoPaypalPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planIDList,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            decimal headerAmount, // Total amount includes amounts from multiple items as well as discounts  			
            int headerDuration, // For multiple item purchases in the same order
            DurationType headerDurationType, // For multiple purchases in the same order
            string lastFourAccountNumber,
            Int32 orderID, // Get after UPS is called 
            int commonResponseCode, // Get after UPS is called
            //string resourceConstant, // Get after UPS is called
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate,
            //string responseCode, // Get after UPS is called
            //string AVSCode, // Get after UPS is called
            //string CVVCode, // Get after UPS is called
            //bool purchaseModeValidForCredit, // Get after UPS is called
            Int32 chargeID, // Get after UPS is called
            PaymentType paymentType)
        {
            try
            {
                // The PurchaseMode is sent from the subscription page or the ala carte page and depends
                // on what the user selected
                // For an ala carte purchase the PurchaseMode is set to New
                // Renewals always have a PurchaseMode set to None
                bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

                MemberTranStatus memberTranStatus = MemberTranStatus.Pending;
                SubscriptionResult subRes = new SubscriptionResult();
                Int32 returnValue = Constants.RETURN_OK;

                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
                MemberTranCreditCollection tranCredits = null;// PurchaseBL.GetRemainingCredit(memberID, brand);
                decimal refreshedCreditAmount = Constants.NULL_DECIMAL;
                if (tranCredits != null)
                {
                    // Only used in server validation
                    refreshedCreditAmount = tranCredits.TotalCreditedAmount;
                }

                // Get the primary PlanID to use that will be saved in the new MemberTran
                int planID = UPSGetPrimaryPlanID(planIDList, brandID);
                if (planID == Constants.NULL_INT)
                {
                    throw new ApplicationException("Primary plan does not exists");
                }

                // Get plan object and initialize with values
                Plan plan = (Plan)PlanBL.Instance.GetPlans(brandID).FindByID(planID).Clone();
                plan.InitialCost = headerAmount;
                // The headerDuration and headerDurationType for an a la carte only purchase
                // is dynamic and will be provided by the calling application
                plan.InitialDuration = headerDuration;
                plan.InitialDurationType = headerDurationType;

                //decimal amount = plan.InitialCost;
                // Still populate the memberPaymentID field in MemberTran although this will not 
                // be used for anything since a new orderID field will be added to MemberTran
                // to find the user payment information  
                Int32 memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                // Create a new paypal payment and add to memberTran
                PaypalPayment paypalPayment = new PaypalPayment();
                paypalPayment.PaymentType = paymentType;
                paypalPayment.FirstName = firstName;
                paypalPayment.LastName = lastName;

                // NEED TO CHECK THAT THE MEMBERTRAN HAS BOTH A DURATION AND DURATIONTYPE    
                // USED IN PREMIUMSERVICEUTIL  
                // THERE ARE 2 CHARGEID FOR AN ORDER
                // USE THE CHARGID FROM THE AUTHORIZATION   
                MemberTran mt = new MemberTran(memberTranID,
                    chargeID, // Only used in the PremiumServiceUtil.setPremiumServiceMember(), not actually saved in the database  	
                    Constants.NULL_INT,
                    memberID,
                    siteID,
                    plan.CurrencyType,
                    null,
                    (plan.PlanTypeMask & PlanType.AuthOnly) == PlanType.AuthOnly ? TranType.AuthorizationOnly : TranType.InitialBuy,
                    null,
                    Constants.NULL_INT,
                    plan.PlanID,
                    plan,
                    null,
                    discountID,
                    memberPaymentID,
                    paypalPayment,
                    null,
                    null,
                    MemberTranStatus.None,
                    null,
                    null,
                    adminMemberID,
                    plan.InitialCost,
                    plan.InitialDuration,
                    plan.InitialDurationType,
                    null,
                    Constants.NULL_INT,
                    DateTime.Now,
                    DateTime.Now,
                    Constants.NULL_STRING,
                    null,
                    paymentType,
                    null, // Set Merchant to null  
                    Matchnet.Duration.GetMinutes(plan.InitialDurationType, plan.InitialDuration));

                mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;
                mt.PurchaseMode = purchaseMode;
                mt.ReusePreviousPayment = reusePreviousPayment;
                mt.LegacyLastFourAccountNumber = lastFourAccountNumber;

                if (IsValidForCredit)
                {
                    mt.CreditAmount = UICreditAmount;
                    mt.MemberTranCreditCollection = tranCredits; //set collection of transactions to be used for crediting for later storage into MemberTranCredited table

                }
                //mt.Amount = totalAmount;

                UPSMemberTran upsMemberTran = new UPSMemberTran(mt);
                upsMemberTran.PackageID = planIDList;
                upsMemberTran.BrandID = brandID;
                upsMemberTran.OriginalInsertDate = originalInsertDate;
                //upsMemberTran.OrderID = orderID;

                returnValue = UPSBeginPaypalPurchase(memberID,
                    adminMemberID,
                    planID,
                    UPSGetDelimitedPlanIDList(planIDList),
                    discountID,
                    siteID,
                    conversionMemberID,
                    0,//getPurchaseReasonID(purchaseReasonTypeID, sourceID),
                    purchaseReasonTypeID,
                    sourceID,
                    memberTranID,
                    memberPaymentID,
                    memberTranStatus,
                    upsMemberTran,
                    ipAddress,
                    paymentType,
                    promoID,
                    reusePreviousPayment,
                    headerAmount,
                    headerDuration,
                    headerDurationType);

                // ProviderID will not be provided by the payment service in UPS
                // This was only used in saving to the ProviderID field in MemberTran as well
                // as renewal recycling 
                // Use GroupID in renewal recycling instead  
                Int32 providerID = Constants.NULL_INT;

                // POPULATE THE MEMBERTRAN OBJECT WITH THE REST OF THE DATA FROM UPS
                UPSMemberTranResult upsMemberTranResult = new UPSMemberTranResult(upsMemberTran,
                    commonResponseCode,
                    (MemberTranStatus)memberTranStatusID,
                    providerID,
                    orderID,
                    chargeID);
                UPSSaveComplete(upsMemberTranResult);

                SubscriptionResult valSubRes = UPSValidatePlanPurchase(planID,
                    memberID,
                    adminMemberID,
                    siteID,
                    brandID,
                    UICreditAmount,
                    refreshedCreditAmount,
                    purchaseMode,
                    PaymentType.CreditCard, //Paypal plans are same as credit card
                    memberTranID);

                subRes.ReturnValue = returnValue;
                subRes.ResourceConstant = "";// getResourceConstant(returnValue);
                // The MemberPaymentID in this case will be the OrderID provided  
                subRes.MemberPaymentID = memberPaymentID;
                subRes.MemberTranID = memberTranID;

                //return subRes;

                return memberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while processing a UPS credit card purchase.", ex);
            }
        }

        public Int32 UPSDoRenewal(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planIDList,
            Int32 discountID,
            Int32 siteID,
            //Int32 brandID,
            CreditCardType creditCardType,
            //Int32 conversionMemberID,
            //Int32 sourceID,
            //Int32 purchaseReasonTypeID,
            //string ipAddress,
            //Int32 promoID,
            //decimal UICreditAmount,
            //PurchaseMode purchaseMode, 
            //bool reusePreviousPayment,
            string firstName,
            string lastName,
            decimal headerAmount, // Total amount includes amounts from multiple items as well as discounts  			
            int headerDuration, // For multiple item purchases in the same order
            DurationType headerDurationType, // For multiple purchases in the same order,
            string lastFourAccountNumber,
            Int32 orderID, // Get after UPS is called 
            int commonResponseCode, // Get after UPS is called
            //string resourceConstant, // Get after UPS is called
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate,
            //string responseCode, // Get after UPS is called
            //string AVSCode, // Get after UPS is called
            //string CVVCode, // Get after UPS is called
            //bool purchaseModeValidForCredit, // Get after UPS is called
            Int32 chargeID, // Get after UPS is called
            PaymentType paymentType)
        {
            try
            {
                Int32 conversionMemberID = Constants.NULL_INT;
                Int32 sourceID = Constants.NULL_INT;
                Int32 purchaseReasonTypeID = Constants.NULL_INT;
                string ipAddress = String.Empty;
                Int32 promoID = Constants.NULL_INT;
                // Set CreditAmount to 0 for renewals
                decimal UICreditAmount = 0.0m;
                // Set PurchaseMode to None for renewals
                PurchaseMode purchaseMode = PurchaseMode.None;
                // Set ReusePreviousPayment to false for renewals
                bool reusePreviousPayment = false;

                //bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

                MemberTranStatus memberTranStatus = MemberTranStatus.Pending;
                SubscriptionResult subRes = new SubscriptionResult();
                Int32 returnValue = Constants.RETURN_OK;

                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = PremiumServiceUtil.getBrand(siteID);
                Int32 brandID = brand.BrandID;

                decimal refreshedCreditAmount = Constants.NULL_DECIMAL;

                // Get the primary PlanID to use that will be saved in the new MemberTran
                int planID = UPSGetPrimaryPlanID(planIDList, brandID);
                if (planID == Constants.NULL_INT)
                {
                    throw new ApplicationException("Primary plan does not exists");
                }
                //get plan object and initialize with values
                Plan plan = (Plan)PlanBL.Instance.GetPlans(brandID).FindByID(planID).Clone();
                plan.InitialCost = headerAmount;
                // The headerDuration and headerDurationType for an a la carte only purchase
                // is dynamic and will be provided by the calling application
                plan.InitialDuration = headerDuration;
                plan.InitialDurationType = headerDurationType;

                //decimal amount = plan.InitialCost;
                // Still populate the memberPaymentID field in MemberTran although this will not 
                // be used for anything since a new orderID field will be added to MemberTran
                // to find the user payment information  
                Int32 memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                Matchnet.Purchase.ValueObjects.Payment payment = null;
                if (paymentType == PaymentType.PaypalLitle)
                {
                    // Create a new paypal payment and add to memberTran
                    PaypalPayment paypalPayment = new PaypalPayment();
                    paypalPayment.PaymentType = paymentType;
                    paypalPayment.FirstName = firstName;
                    paypalPayment.LastName = lastName;
                    payment = paypalPayment;
                }
                else if (paymentType == PaymentType.ElectronicFundsTransfer)
                {
                    // Create a new electronic funds transfer payment and add to memberTran
                    ElectronicFundsTransferPayment electronicFundsTransferPayment = new ElectronicFundsTransferPayment();
                    electronicFundsTransferPayment.PaymentType = paymentType;
                    electronicFundsTransferPayment.FirstName = firstName;
                    electronicFundsTransferPayment.LastName = lastName;
                    payment = electronicFundsTransferPayment;
                }
                else
                {
                    // Create a new credit card payment and add to memberTran
                    // Only used to store the credit card type
                    CreditCardPayment creditCardPayment = new CreditCardPayment();
                    creditCardPayment.CreditCardType = creditCardType;
                    creditCardPayment.FirstName = firstName;
                    creditCardPayment.LastName = lastName;
                    payment = creditCardPayment;
                    paymentType = PaymentType.CreditCard;
                }


                // NEED TO CHECK THAT THE MEMBERTRAN HAS BOTH A DURATION AND DURATIONTYPE    
                // USED IN PREMIUMSERVICEUTIL  
                // THERE ARE 2 CHARGEID FOR AN ORDER
                // USE THE CHARGID FROM THE AUTHORIZATION   
                MemberTran mt = new MemberTran(memberTranID,
                    chargeID, // Only used in the PremiumServiceUtil.setPremiumServiceMember(), not actually saved in the database  	
                    Constants.NULL_INT,
                    memberID,
                    siteID,
                    plan.CurrencyType,
                    null,
                    TranType.Renewal,
                    null,
                    Constants.NULL_INT,
                    plan.PlanID,
                    plan,
                    null,
                    discountID,
                    memberPaymentID,
                    payment, // Set Payment to the CreditCardPayment that just stores the credit card type  
                    null,
                    null,
                    MemberTranStatus.None,
                    null,
                    null,
                    adminMemberID,
                    plan.InitialCost,
                    plan.InitialDuration,
                    plan.InitialDurationType,
                    null,
                    Constants.NULL_INT,
                    DateTime.Now,
                    DateTime.Now,
                    Constants.NULL_STRING,
                    null,
                    paymentType,
                    null, // Set Merchant to null  
                    Matchnet.Duration.GetMinutes(plan.InitialDurationType, plan.InitialDuration));

                mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;
                // Set PurchaseMode to None for renewals
                mt.PurchaseMode = purchaseMode;
                // Set ReusePreviousPayment to false for renewals
                mt.ReusePreviousPayment = reusePreviousPayment;
                // Set CreditAmount to 0 for renewals
                mt.CreditAmount = UICreditAmount;
                mt.LegacyLastFourAccountNumber = lastFourAccountNumber;

                UPSMemberTran upsMemberTran = new UPSMemberTran(mt);
                upsMemberTran.PackageID = planIDList;
                upsMemberTran.BrandID = brandID;
                upsMemberTran.OriginalInsertDate = originalInsertDate;
                //upsMemberTran.OrderID = orderID;

                if (paymentType == PaymentType.PaypalLitle)
                {
                    returnValue = UPSBeginPaypalPurchase(memberID,
                        adminMemberID,
                        planID,
                        UPSGetDelimitedPlanIDList(planIDList),
                        discountID,
                        siteID,
                        conversionMemberID,
                        0,//getPurchaseReasonID(purchaseReasonTypeID, sourceID),
                        purchaseReasonTypeID,
                        sourceID,
                        memberTranID,
                        memberPaymentID,
                        memberTranStatus,
                        upsMemberTran,
                        ipAddress,
                        paymentType,
                        promoID,
                        reusePreviousPayment,
                        headerAmount,
                        headerDuration,
                        headerDurationType);
                }
                else if (paymentType == PaymentType.ElectronicFundsTransfer)
                {
                    returnValue = UPSBeginCheckPurchase(memberID,
                        adminMemberID,
                        planID,
                        UPSGetDelimitedPlanIDList(planIDList),
                        discountID,
                        siteID,
                        conversionMemberID,
                        0,//getPurchaseReasonID(purchaseReasonTypeID, sourceID),
                        purchaseReasonTypeID,
                        sourceID,
                        memberTranID,
                        memberPaymentID,
                        upsMemberTran,
                        ipAddress,
                        promoID,
                        reusePreviousPayment,
                        headerAmount,
                        headerDuration,
                        headerDurationType,
                        paymentType);
                }
                else
                {
                    returnValue = UPSBeginCreditCardPurchase(memberID,
                        adminMemberID,
                        planID,
                        UPSGetDelimitedPlanIDList(planIDList),
                        discountID,
                        siteID,
                        conversionMemberID,
                        0,//getPurchaseReasonID(purchaseReasonTypeID, sourceID),
                        purchaseReasonTypeID,
                        sourceID,
                        memberTranID,
                        memberPaymentID,
                        memberTranStatus,
                        upsMemberTran,
                        ipAddress,
                        creditCardType,
                        promoID,
                        reusePreviousPayment,
                        headerAmount,
                        headerDuration,
                        headerDurationType);
                }

                // ProviderID will not be provided by the payment service in UPS
                // This was only used in saving to the ProviderID field in MemberTran as well
                // as renewal recycling 
                // Use GroupID in renewal recycling instead  
                Int32 providerID = Constants.NULL_INT;

                // POPULATE THE MEMBERTRAN OBJECT WITH THE REST OF THE DATA FROM UPS
                UPSMemberTranResult upsMemberTranResult = new UPSMemberTranResult(upsMemberTran,
                    commonResponseCode,
                    (MemberTranStatus)memberTranStatusID,
                    providerID,
                    orderID,
                    chargeID);
                UPSSaveComplete(upsMemberTranResult);

                SubscriptionResult valSubRes = UPSValidatePlanPurchase(planID,
                    memberID,
                    adminMemberID,
                    siteID,
                    brandID,
                    UICreditAmount,
                    refreshedCreditAmount,
                    purchaseMode,
                    PaymentType.CreditCard,
                    memberTranID);

                subRes.ReturnValue = returnValue;
                subRes.ResourceConstant = "";// getResourceConstant(returnValue);
                // The MemberPaymentID in this case will be the OrderID provided  
                subRes.MemberPaymentID = memberPaymentID;
                subRes.MemberTranID = memberTranID;

                //return subRes;

                return memberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while processing a UPS renewal purchase.", ex);
            }
        }

        private Int32 UPSBeginCreditCardPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            string planIDList,
            Int32 discountID,
            Int32 siteID,
            Int32 conversionMemberID,
            Int32 purchaseReasonID,
            Int32 purchaseReasonTypeID,
            Int32 sourceID,
            Int32 memberTranID,
            Int32 memberPaymentID,
            MemberTranStatus memberTranStatus,
            UPSMemberTran UPSMemberTran,
            string ipAddress,
            CreditCardType creditCardType,
            Int32 promoID,
            bool reusePreviousPayment,
            decimal headerAmount,
            int headerDuration,
            DurationType headerDurationType)
        {
            Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_CreditCard_V20", 0);

            if (adminMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            }

            if (discountID == Constants.NULL_INT)
            {
                discountID = 0;
            }

            if (conversionMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@ConversionMemberID", SqlDbType.Int, ParameterDirection.Input, conversionMemberID);
            }

            if (sourceID != Constants.NULL_INT)
            {
                command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
            }
            else
            {
                command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, 0);
            }

            System.Diagnostics.Trace.WriteLine("__purchaseReasonTypeID: " + purchaseReasonTypeID.ToString());
            if (purchaseReasonTypeID != Constants.NULL_INT)
            {
                command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
            }

            System.Diagnostics.Trace.WriteLine("__purchaseReasonID: " + purchaseReasonID.ToString());
            if (purchaseReasonID != Constants.NULL_INT)
            {
                command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonID);
            }

            if (ipAddress != Constants.NULL_STRING)
            {
                command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
            }
            if (promoID != Constants.NULL_INT)
            {
                command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promoID);
            }
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
            command.AddParameter("@PlanIDList", SqlDbType.NVarChar, ParameterDirection.Input, planIDList);
            command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
            command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
            command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranStatus);
            command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (Int32)creditCardType);
            command.AddParameter("@LastFourAccountNumber", SqlDbType.NVarChar, ParameterDirection.Input, UPSMemberTran.MemberTran.LegacyLastFourAccountNumber);

            if (UPSMemberTran.MemberTran.TranType == TranType.Renewal)
            {
                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, "RenewalService@spark.net");
            }
            else
            {
                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);
            }

            //TL 08082008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
            if (UPSMemberTran.MemberTran.CreditAmount != Constants.NULL_DECIMAL && UPSMemberTran.MemberTran.CreditAmount > 0)
                command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, UPSMemberTran.MemberTran.CreditAmount);

            command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)UPSMemberTran.MemberTran.PurchaseMode);

            MemberSub memberSub = null;// PurchaseBL.GetSubscription(UPSMemberTran.MemberTran.MemberID, UPSMemberTran.MemberTran.SiteID);
            DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
            if (UPSMemberTran.OriginalInsertDate != DateTime.MinValue)
            {
                // The insert date is overrided with the original insert date of the transaction    
                updateDate = UPSMemberTran.OriginalInsertDate;
            }
            DateTime startDate = MemberTran.DetermineTransactionStartDate(UPSMemberTran.MemberTran, updateDate, memberSub);
            DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, UPSMemberTran.MemberTran, memberSub);
            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
                command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
            }

            command.AddParameter("@EZBuy", SqlDbType.Bit, ParameterDirection.Input, reusePreviousPayment);
            //command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, UPSMemberTran.OrderID);

            command.AddParameter("@HeaderAmount", SqlDbType.Decimal, ParameterDirection.Input, headerAmount);
            command.AddParameter("@HeaderDuration", SqlDbType.Int, ParameterDirection.Input, headerDuration);
            command.AddParameter("@HeaderDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)headerDurationType);
            command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)UPSMemberTran.MemberTran.TranType);

            SyncWriter sw = new SyncWriter();
            Exception swEx;
            Int32 returnValue = sw.Execute(command,
                out swEx);
            sw.Dispose();

            if (swEx != null)
            {
                throw swEx;
            }

            return returnValue;
        }


        private Int32 UPSBeginPaypalPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            string planIDList,
            Int32 discountID,
            Int32 siteID,
            Int32 conversionMemberID,
            Int32 purchaseReasonID,
            Int32 purchaseReasonTypeID,
            Int32 sourceID,
            Int32 memberTranID,
            Int32 memberPaymentID,
            MemberTranStatus memberTranStatus,
            UPSMemberTran UPSMemberTran,
            string ipAddress,
            PaymentType paymentType,
            Int32 promoID,
            bool reusePreviousPayment,
            decimal headerAmount,
            int headerDuration,
            DurationType headerDurationType)
        {
            Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_Paypal_V20", 0);

            if (adminMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            }

            if (discountID == Constants.NULL_INT)
            {
                discountID = 0;
            }

            if (conversionMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@ConversionMemberID", SqlDbType.Int, ParameterDirection.Input, conversionMemberID);
            }

            if (sourceID != Constants.NULL_INT)
            {
                command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
            }
            else
            {
                command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, 0);
            }

            System.Diagnostics.Trace.WriteLine("__purchaseReasonTypeID: " + purchaseReasonTypeID.ToString());
            if (purchaseReasonTypeID != Constants.NULL_INT)
            {
                command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
            }

            System.Diagnostics.Trace.WriteLine("__purchaseReasonID: " + purchaseReasonID.ToString());
            if (purchaseReasonID != Constants.NULL_INT)
            {
                command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonID);
            }

            if (ipAddress != Constants.NULL_STRING)
            {
                command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
            }
            if (promoID != Constants.NULL_INT)
            {
                command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promoID);
            }
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
            command.AddParameter("@PlanIDList", SqlDbType.NVarChar, ParameterDirection.Input, planIDList);
            command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
            command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
            command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranStatus);
            command.AddParameter("@PaymentType", SqlDbType.Int, ParameterDirection.Input, (Int32)paymentType);
            command.AddParameter("@LastFourAccountNumber", SqlDbType.NVarChar, ParameterDirection.Input, UPSMemberTran.MemberTran.LegacyLastFourAccountNumber);

            if (UPSMemberTran.MemberTran.TranType == TranType.Renewal)
            {
                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, "RenewalService@spark.net");
            }
            else
            {
                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);
            }

            //TL 08082008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
            if (UPSMemberTran.MemberTran.CreditAmount != Constants.NULL_DECIMAL && UPSMemberTran.MemberTran.CreditAmount > 0)
                command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, UPSMemberTran.MemberTran.CreditAmount);

            command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)UPSMemberTran.MemberTran.PurchaseMode);

            MemberSub memberSub = null;// PurchaseBL.GetSubscription(UPSMemberTran.MemberTran.MemberID, UPSMemberTran.MemberTran.SiteID);
            DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
            if (UPSMemberTran.OriginalInsertDate != DateTime.MinValue)
            {
                // The insert date is overrided with the original insert date of the transaction    
                updateDate = UPSMemberTran.OriginalInsertDate;
            }
            DateTime startDate = MemberTran.DetermineTransactionStartDate(UPSMemberTran.MemberTran, updateDate, memberSub);
            DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, UPSMemberTran.MemberTran, memberSub);
            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
                command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
            }

            command.AddParameter("@EZBuy", SqlDbType.Bit, ParameterDirection.Input, reusePreviousPayment);
            //command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, UPSMemberTran.OrderID);

            command.AddParameter("@HeaderAmount", SqlDbType.Decimal, ParameterDirection.Input, headerAmount);
            command.AddParameter("@HeaderDuration", SqlDbType.Int, ParameterDirection.Input, headerDuration);
            command.AddParameter("@HeaderDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)headerDurationType);
            command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)UPSMemberTran.MemberTran.TranType);

            SyncWriter sw = new SyncWriter();
            Exception swEx;
            Int32 returnValue = sw.Execute(command,
                out swEx);
            sw.Dispose();

            if (swEx != null)
            {
                throw swEx;
            }

            return returnValue;
        }

        public Int32 UPSDoCheckPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32[] planIDList,
            Int32 discountID,
            Int32 siteID,
            Int32 brandID,
            Int32 conversionMemberID,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 promoID,
            decimal UICreditAmount,
            PurchaseMode purchaseMode,
            bool reusePreviousPayment,
            string firstName,
            string lastName,
            decimal headerAmount, // Total amount includes amounts from multiple items as well as discounts  			
            int headerDuration, // For multiple item purchases in the same order
            DurationType headerDurationType, // For multiple purchases in the same order
            string lastFourAccountNumber,
            Int32 orderID, // Get after UPS is called 
            int commonResponseCode, // Get after UPS is called
            //string resourceConstant, // Get after UPS is called
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate,
            //string responseCode, // Get after UPS is called
            //string AVSCode, // Get after UPS is called
            //string CVVCode, // Get after UPS is called
            //bool purchaseModeValidForCredit, // Get after UPS is called
            Int32 chargeID, // Get after UPS is called
            PaymentType paymentType)
        {
            try
            {
                // The PurchaseMode is sent from the subscription page or the ala carte page and depends
                // on what the user selected
                // For an ala carte purchase the PurchaseMode is set to New
                // Renewals always have a PurchaseMode set to None
                bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

                MemberTranStatus memberTranStatus = MemberTranStatus.Pending;
                SubscriptionResult subRes = new SubscriptionResult();
                Int32 returnValue = Constants.RETURN_OK;

                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
                MemberTranCreditCollection tranCredits = null;// PurchaseBL.GetRemainingCredit(memberID, brand);
                decimal refreshedCreditAmount = Constants.NULL_DECIMAL;
                if (tranCredits != null)
                {
                    refreshedCreditAmount = tranCredits.TotalCreditedAmount;
                }

                // Get the primary PlanID to use that will be saved in the new MemberTran
                int planID = UPSGetPrimaryPlanID(planIDList, brandID);
                if (planID == Constants.NULL_INT)
                {
                    throw new ApplicationException("Primary plan does not exists");
                }
                //get plan object and initialize with values
                Plan plan = (Plan)PlanBL.Instance.GetPlans(brandID).FindByID(planID).Clone();
                plan.InitialCost = headerAmount;
                // The headerDuration and headerDurationType for an a la carte only purchase
                // is dynamic and will be provided by the calling application
                plan.InitialDuration = headerDuration;
                plan.InitialDurationType = headerDurationType;

                //decimal amount = plan.InitialCost;
                // Still populate the memberPaymentID field in MemberTran although this will not 
                // be used for anything since a new orderID field will be added to MemberTran
                // to find the user payment information  
                Int32 memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                Matchnet.Purchase.ValueObjects.Payment payment = null;
                if (paymentType == PaymentType.Check)
                {
                    // Create a new check payment and add to memberTran
                    // Only used to store the first name and last name
                    payment = new CheckPayment();
                    payment.PaymentType = paymentType;
                    payment.FirstName = firstName;
                    payment.LastName = lastName;
                }
                else if (paymentType == PaymentType.ElectronicFundsTransfer)
                {
                    // Create a new check payment and add to memberTran
                    // Only used to store the first name and last name
                    payment = new ElectronicFundsTransferPayment();
                    payment.PaymentType = paymentType;
                    payment.FirstName = firstName;
                    payment.LastName = lastName;
                }

                // NEED TO CHECK THAT THE MEMBERTRAN HAS BOTH A DURATION AND DURATIONTYPE    
                // USED IN PREMIUMSERVICEUTIL  
                MemberTran mt = new MemberTran(memberTranID,
                    chargeID, // Only used in the PremiumServiceUtil.setPremiumServiceMember(), not actually saved in the database  	
                    Constants.NULL_INT,
                    memberID,
                    siteID,
                    plan.CurrencyType,
                    null,
                    TranType.InitialBuy,
                    null,
                    Constants.NULL_INT,
                    plan.PlanID,
                    plan,
                    null,
                    discountID,
                    memberPaymentID,
                    payment, // Set Payment to either the CheckPayment or ElectronicFundsTransferPayment    
                    null,
                    null,
                    MemberTranStatus.None,
                    null,
                    null,
                    adminMemberID,
                    plan.InitialCost,
                    plan.InitialDuration,
                    plan.InitialDurationType,
                    null,
                    Constants.NULL_INT,
                    DateTime.Now,
                    DateTime.Now,
                    Constants.NULL_STRING,
                    null,
                    PaymentType.Check,
                    null, // Set Merchant to null  
                    Matchnet.Duration.GetMinutes(plan.InitialDurationType, plan.InitialDuration));

                mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;
                mt.PurchaseMode = purchaseMode;
                mt.ReusePreviousPayment = reusePreviousPayment;
                mt.LegacyLastFourAccountNumber = lastFourAccountNumber;

                if (IsValidForCredit)
                {
                    mt.CreditAmount = UICreditAmount;
                    mt.MemberTranCreditCollection = tranCredits; //set collection of transactions to be used for crediting for later storage into MemberTranCredited table

                }

                UPSMemberTran upsMemberTran = new UPSMemberTran(mt);
                upsMemberTran.PackageID = planIDList;
                upsMemberTran.BrandID = brandID;
                upsMemberTran.OriginalInsertDate = originalInsertDate;
                //upsMemberTran.OrderID = orderID;

                returnValue = UPSBeginCheckPurchase(memberID,
                    adminMemberID,
                    planID,
                    UPSGetDelimitedPlanIDList(planIDList),
                    discountID,
                    siteID,
                    conversionMemberID,
                    0,//getPurchaseReasonID(purchaseReasonTypeID, sourceID),
                    purchaseReasonTypeID,
                    sourceID,
                    memberTranID,
                    memberPaymentID,
                    upsMemberTran,
                    ipAddress,
                    promoID,
                    reusePreviousPayment,
                    headerAmount,
                    headerDuration,
                    headerDurationType,
                    paymentType);

                // ProviderID will not be provided by the payment service in UPS
                // This was only used in saving to the ProviderID field in MemberTran as well
                // as renewal recycling 
                // Use GroupID in renewal recycling instead  
                Int32 providerID = Constants.NULL_INT;

                // POPULATE THE MEMBERTRAN OBJECT WITH THE REST OF THE DATA FROM UPS
                UPSMemberTranResult upsMemberTranResult = new UPSMemberTranResult(upsMemberTran,
                    commonResponseCode,
                    (MemberTranStatus)memberTranStatusID,
                    providerID,
                    orderID,
                    chargeID);
                UPSSaveComplete(upsMemberTranResult);

                SubscriptionResult valSubRes = UPSValidatePlanPurchase(planID,
                    memberID,
                    adminMemberID,
                    siteID,
                    brandID,
                    UICreditAmount,
                    refreshedCreditAmount,
                    purchaseMode,
                    PaymentType.Check,
                    memberTranID);

                subRes.ReturnValue = returnValue;
                subRes.ResourceConstant = "";// getResourceConstant(returnValue);
                // The MemberPaymentID in this case will be the OrderID provided  
                subRes.MemberPaymentID = memberPaymentID;
                subRes.MemberTranID = memberTranID;

                //return subRes;

                return memberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while processing a UPS check purchase.", ex);
            }
        }

        private Int32 UPSBeginCheckPurchase(Int32 memberID,
            Int32 adminMemberID,
            Int32 planID,
            string planIDList,
            Int32 discountID,
            Int32 siteID,
            Int32 conversionMemberID,
            Int32 purchaseReasonID,
            Int32 purchaseReasonTypeID,
            Int32 sourceID,
            Int32 memberTranID,
            Int32 memberPaymentID,
            UPSMemberTran UPSMemberTran,
            string ipAddress,
            Int32 promoID,
            bool reusePreviousPayment,
            decimal headerAmount,
            int headerDuration,
            DurationType headerDurationType,
            PaymentType paymentType)
        {
            MemberTran memberTran = UPSMemberTran.MemberTran;

            Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_CheckingAccount_V20", 0);

            if (adminMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
            }

            if (discountID == Constants.NULL_INT)
            {
                discountID = 0;
            }

            if (conversionMemberID != Constants.NULL_INT)
            {
                command.AddParameter("@ConversionMemberID", SqlDbType.Int, ParameterDirection.Input, conversionMemberID);
            }

            if (sourceID != Constants.NULL_INT)
            {
                command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
            }

            if (purchaseReasonTypeID != Constants.NULL_INT)
            {
                command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
            }

            if (purchaseReasonID != Constants.NULL_INT)
            {
                command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonID);
            }

            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
            command.AddParameter("@PlanIDList", SqlDbType.NVarChar, ParameterDirection.Input, planIDList);
            command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
            command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
            command.AddParameter("@LastFourAccountNumber", SqlDbType.NVarChar, ParameterDirection.Input, UPSMemberTran.MemberTran.LegacyLastFourAccountNumber);
            command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
            command.AddParameter("@PaymentType", SqlDbType.Int, ParameterDirection.Input, (Int32)paymentType);
            command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);

            //TL 08082008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
            if (memberTran.CreditAmount != Constants.NULL_DECIMAL && memberTran.CreditAmount > 0)
                command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, memberTran.CreditAmount);

            command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.PurchaseMode);

            MemberSub memberSub = null;// PurchaseBL.GetSubscription(memberTran.MemberID, memberTran.SiteID);
            DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
            if (UPSMemberTran.OriginalInsertDate != DateTime.MinValue)
            {
                // The insert date is overrided with the original insert date of the transaction    
                updateDate = UPSMemberTran.OriginalInsertDate;
            }
            DateTime startDate = MemberTran.DetermineTransactionStartDate(memberTran, updateDate, memberSub);
            DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, memberTran, memberSub);
            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
                command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
            }

            // Supporting PromoID entry for check purchases too now
            if (promoID != Constants.NULL_INT)
            {
                command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promoID);
            }

            command.AddParameter("@EZBuy", SqlDbType.Bit, ParameterDirection.Input, reusePreviousPayment);

            command.AddParameter("@HeaderAmount", SqlDbType.Decimal, ParameterDirection.Input, headerAmount);
            command.AddParameter("@HeaderDuration", SqlDbType.Int, ParameterDirection.Input, headerDuration);
            command.AddParameter("@HeaderDurationTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)headerDurationType);
            command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)UPSMemberTran.MemberTran.TranType);

            SyncWriter sw = new SyncWriter();
            Exception swEx;
            Int32 returnValue = sw.Execute(command,
                out swEx);
            sw.Dispose();

            if (swEx != null)
            {
                throw swEx;
            }

            return returnValue;
        }

        public Int32 UPSDoCredit(TranType tranType,
            Int32 memberID,
            Int32 adminMemberID,
            //Int32 merchantID,
            decimal amount,
            CreditCardType referenceCreditCardType, // NEED TO FIND THIS WHEN DOING A CREDIT AGAINST AN ORDER IN UPS
            //Int32 minutes,
            Int32 duration,
            DurationType durationType,
            CurrencyType currencyType,
            string firstName,
            string lastName,
            string lastFourAccountNumber,
            Int32 orderID, // Get after UPS is called 
            Int32 referenceOrderID, // The ReferenceOrderID for the new order   
            int commonResponseCode, // Get after UPS is called
            //string resourceConstant, // Get after UPS is called
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate,
            //string responseCode, // Get after UPS is called
            //string AVSCode, // Get after UPS is called
            //string CVVCode, // Get after UPS is called
            //bool purchaseModeValidForCredit, // Get after UPS is called
            Int32 chargeID)
        {
            try
            {
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                // NEED TO FIND THIS WHEN DOING A CREDIT AGAINST AN ORDER IN UPS
                Int32 siteID = Constants.NULL_INT;
                Int32 referenceMemberTranID = Constants.NULL_INT;
                Int32 referenceMemberPaymentID = Constants.NULL_INT;
                Int32 referencePlanID = Constants.NULL_INT;
                PaymentType referencePaymentType = PaymentType.None;

                // Get the details of the original transaction that this credit has been
                // applied to  
                UPSMemberTranInfo upsMemberTranInfo = null;// UPSGetMemberTranInfo(referenceOrderID);
                if (upsMemberTranInfo != null)
                {
                    siteID = upsMemberTranInfo.SiteID;
                    referenceMemberTranID = upsMemberTranInfo.MemberTranID;
                    referenceMemberPaymentID = upsMemberTranInfo.MemberPaymentID;
                    referencePlanID = upsMemberTranInfo.PlanID;
                    referencePaymentType = upsMemberTranInfo.PaymentTypeUsed;
                }
                else
                {
                    throw new BLException("BL error occured while trying to get the transaction details for OrderID: " + orderID.ToString());
                }

                Matchnet.Purchase.ValueObjects.Payment payment = null;
                if (referencePaymentType == PaymentType.CreditCard)
                {
                    // Create a new credit card payment and add to memberTran
                    // Only used to store the credit card type
                    CreditCardPayment creditCardPayment = new CreditCardPayment();
                    creditCardPayment.CreditCardType = referenceCreditCardType;
                    creditCardPayment.FirstName = firstName;
                    creditCardPayment.LastName = lastName;
                    creditCardPayment.PaymentType = referencePaymentType;
                    payment = creditCardPayment;
                }
                else
                {
                    payment = new Matchnet.Purchase.ValueObjects.Payment();
                    payment.FirstName = firstName;
                    payment.LastName = lastName;
                    payment.PaymentType = referencePaymentType;
                }

                MemberTran mt = new MemberTran(memberTranID,
                    chargeID,
                    referenceMemberTranID,
                    memberID,
                    siteID,
                    currencyType,
                    null,
                    tranType,
                    null,
                    Matchnet.Constants.NULL_INT,
                    referencePlanID,
                    null,
                    null,
                    Constants.NULL_INT,
                    referenceMemberPaymentID,
                    payment,
                    null,
                    null,
                    MemberTranStatus.None,
                    null,
                    null,
                    adminMemberID,
                    amount,
                    duration,
                    durationType,
                    null,
                    PurchaseConstants.TRANSACTION_REASON_ID_CREDIT,
                    DateTime.Now,
                    DateTime.Now,
                    Constants.NULL_STRING, // Set AccountNumber to Constants.NULL_STRING
                    null,
                    referencePaymentType,
                    null, // Set Merchant to null 
                    Matchnet.Constants.NULL_INT);

                mt.SavePayment = false;
                mt.RequestType = RequestType.ProviderRequest;
                // make sure neg numbers for months and amount
                if (mt.Amount > 0)
                {
                    mt.Amount = -mt.Amount;
                }
                if (mt.Duration > 0)
                {
                    mt.Duration = -mt.Duration;
                }
                mt.LegacyLastFourAccountNumber = lastFourAccountNumber;

                UPSMemberTran upsMemberTran = new UPSMemberTran(mt);
                upsMemberTran.OriginalInsertDate = originalInsertDate;

                // ProviderID will not be provided by the payment service in UPS
                // This was only used in saving to the ProviderID field in MemberTran as well
                // as renewal recycling 
                // Use GroupID in renewal recycling instead  
                Int32 providerID = Constants.NULL_INT;

                // POPULATE THE MEMBERTRAN OBJECT WITH THE REST OF THE DATA FROM UPS
                UPSMemberTranResult upsMemberTranResult = new UPSMemberTranResult(upsMemberTran,
                    commonResponseCode,
                    (MemberTranStatus)memberTranStatusID,
                    providerID,
                    orderID,
                    chargeID);

                UPSSaveComplete(upsMemberTranResult);

                return mt.MemberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while processing a UPS credit for memberID: " + memberID.ToString(), ex);
            }
        }

        public Int32 UPSDoAuthPmtProfile(Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            CreditCardType creditCardType,
            Int32 sourceID,
            Int32 purchaseReasonTypeID,
            string ipAddress,
            Int32 referenceMemberPaymentID, // NEED TO PROVIDE THIS
            Int32 orderID, // Get after UPS is called 
            int commonResponseCode, // Get after UPS is called
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate,
            Int32 chargeID) // Get after UPS is called      
        {
            try
            {
                SubscriptionResult subRes = new SubscriptionResult();

                Int32 memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                // Create a new credit card payment and add to memberTran
                // Only used to store the credit card type
                CreditCardPayment creditCardPayment = new CreditCardPayment();
                creditCardPayment.CreditCardType = creditCardType;

                MemberTran mt = new MemberTran(
                    memberTranID, //int memberTranID,
                    chargeID, //int chargeID,
                    referenceMemberPaymentID, //int referenceMemberTranID, HACK,use this field as referenceMemberPaymentId since its not being used
                    memberID, //int memberID,
                    siteID, //int siteID,
                    CurrencyType.USDollar, //CurrencyType currencyType,
                    null, //string resourceConstant,
                    TranType.AuthPmtProfile, //TranType tranType,
                    null, //string tranTypeResourceConstant,
                    Matchnet.Constants.NULL_INT, //int memberSubID,
                    Constants.NULL_INT, //int planID,
                    null, //Plan plan,
                    null, //string planResourceConstant,
                    Constants.NULL_INT, //int discountID,
                    memberPaymentID, //int memberPaymentID,
                    creditCardPayment, //Payment payment,
                    null, //string paymentTypeDescription,
                    null, //string paymentTypeResourceConstant,
                    MemberTranStatus.Pending, //MemberTranStatus memberTranStatus,
                    null, //string memberTranStatusDescription,
                    null, //string memberTranStatusResourceConstant,
                    adminMemberID, //int adminMemberID,
                    1, //decimal amount,
                    1, //int duration,
                    DurationType.Month, //DurationType durationType,
                    null, //string durationTypeResourceConstant,
                    Constants.NULL_INT, //int transactionReasonID,
                    DateTime.Now, //DateTime insertDate,
                    DateTime.Now, //DateTime updateDate,
                    Constants.NULL_STRING, //string accountNumber, 
                    null, //string discountResourceConstant,
                    PaymentType.CreditCard, //PaymentType paymentType,
                    null, //SiteMerchant siteMerchant,
                    Constants.NULL_INT//int minutes
                    );

                mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;

                UPSMemberTran upsMemberTran = new UPSMemberTran(mt);
                upsMemberTran.OriginalInsertDate = originalInsertDate;
                //upsMemberTran.OrderID = orderID;

                Int32 returnValue = UPSProcessMemberAuthPmtProfileSaveInit(
                    upsMemberTran,
                    1, // 1 dollar for now
                    purchaseReasonTypeID,
                    sourceID,
                    ipAddress);

                // ProviderID will not be provided by the payment service in UPS
                // This was only used in saving to the ProviderID field in MemberTran as well
                // as renewal recycling 
                // Use GroupID in renewal recycling instead  
                Int32 providerID = Constants.NULL_INT;

                // POPULATE THE MEMBERTRAN OBJECT WITH THE REST OF THE DATA FROM UPS
                UPSMemberTranResult upsMemberTranResult = new UPSMemberTranResult(upsMemberTran,
                    commonResponseCode,
                    (MemberTranStatus)memberTranStatusID,
                    providerID,
                    orderID,
                    chargeID);
                UPSSaveCompletAuthPmtProfile(upsMemberTranResult);

                subRes.ReturnValue = returnValue;
                subRes.ResourceConstant = "";// getResourceConstant(returnValue);
                subRes.MemberPaymentID = memberPaymentID;
                subRes.MemberTranID = memberTranID;
                subRes.MemberTranStatus = upsMemberTranResult.MemberTranStatus;

                //return subRes;

                return memberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while processing a UPS payment profile authorization for memberID: " + memberID.ToString(), ex);
            }

        }

        private Int32 UPSProcessMemberAuthPmtProfileSaveInit(
            UPSMemberTran upsMemberTran
            , decimal amount
            , Int32 purchaseReasonTypeID
            , Int32 sourceID
            , string ipAddress
            )
        {
            try
            {
                MemberTran memberTran = upsMemberTran.MemberTran;

                Command command = new Command("mnSubscription", "dbo.up_Member_Auth_Pmt_Profile_Save_Init_V20", 0);

                #region Put all the DB param list together
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
                command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.TranType);
                if (memberTran.AdminMemberID != Constants.NULL_INT)
                {
                    command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.AdminMemberID);
                }
                command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, amount);
                if (memberTran.TransactionReasonID != Constants.NULL_INT)
                {
                    command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, memberTran.TransactionReasonID);
                }

                if (upsMemberTran.OriginalInsertDate != DateTime.MinValue)
                {
                    // The insert date is overrided with the original insert date of the transaction    
                    command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, upsMemberTran.OriginalInsertDate);
                }
                else if (memberTran.InsertDate != DateTime.MinValue)
                {
                    command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, memberTran.InsertDate);
                }

                int PurchaseReasonID = getPurchaseReasonID(purchaseReasonTypeID, sourceID);

                if (PurchaseReasonID != Constants.NULL_INT)
                {
                    command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, PurchaseReasonID);
                }

                if (purchaseReasonTypeID != Constants.NULL_INT)
                {
                    command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
                }
                if (sourceID != Constants.NULL_INT)
                {
                    command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
                }

                command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
                command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberPaymentID);
                if ((int)memberTran.MemberTranStatus != Constants.NULL_INT)
                {
                    command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.MemberTranStatus);
                }

                if (ipAddress != null)
                {
                    command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
                }

                if ((int)memberTran.Payment.PaymentType != Constants.NULL_INT)
                {
                    command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTran.Payment).CreditCardType);
                }

                command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberTran.MemberID, MemberLoadFlags.None).EmailAddress);

                #endregion
                SyncWriter sw = new SyncWriter();
                Exception swEx;
                Int32 returnValue = sw.Execute(command,
                    out swEx);
                sw.Dispose();

                if (swEx != null)
                {
                    throw swEx;
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                throw new BLException(this.ToString() + ".UPSProcessMemberAuthPmtProfileSaveInit()", ex);
            }
        }

        public Int32 UPSUpdateSubscription(Int32 memberID,
            Int32 siteID,
            Int32 adminMemberID,
            Int32 headerDuration,
            DurationType headerDurationType,
            //TranType adjustmentType, -- Only migrate time adjustment transactions 
            //Int32 transactionReasonID, -- Use the common response code for this
            //bool reOpenFlag, -- Not migrating renewal terminations nor reopening of renewals      
            Int32[] planIDList,
            Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
            TimeAdjustOverrideType timeAdjustOverrideType,
            Int32 orderID, // Get after UPS is called 
            int commonResponseCode, // Get after UPS is called, used to set the transaction reason ID  
            Int32 memberTranStatusID, // Get after UPS is called
            DateTime originalInsertDate)
        {
            try
            {

                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);
                Int32 memberSubID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERSUBID);

                // Get the primary PlanID to use that will be saved in the new MemberTran
                int planID = UPSGetPrimaryPlanID(planIDList, targetBrand.BrandID);
                if (planID == Constants.NULL_INT)
                {
                    throw new ApplicationException("Primary plan does not exists");
                }

                // Only used for data migration of time adjustments
                // So the only transaction type used will be administrative adjustments  
                // Not migrating transactions for changing renewal plans, renewal terminiations, nor reopening renewals  
                TranType adjustmentType = TranType.AdministrativeAdjustment;

                Command command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Adjust_V20", 0);

                if (planID != Constants.NULL_INT)
                {
                    command.AddParameter("@UpdatedPlanID", SqlDbType.Int, ParameterDirection.Input, planID);
                }
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, headerDuration);
                command.AddParameter("@DurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)headerDurationType);
                command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)adjustmentType);
                command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, commonResponseCode);
                //command.AddParameter("@ReOpenFlag", SqlDbType.Bit, ParameterDirection.Input, reOpenFlag);
                command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
                command.AddParameter("@MemberSubID", SqlDbType.Int, ParameterDirection.Input, memberSubID);
                command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

                //09162008 TL - Creating a temporary MemberTran to calculate start/end dates
                MemberTran mt = new MemberTran();
                mt.TranType = adjustmentType;
                mt.Duration = headerDuration;
                mt.DurationType = headerDurationType;

                //09162008 TL - Passing in params to insert into new MemberTranExtended table
                DateTime updateDate = DateTime.Now; //passing in update date which is used internally in proc to determine renew date, in order for it to sync up with our member tran extended end date
                if (originalInsertDate != DateTime.MinValue)
                {
                    // The insert date is overrided with the original insert date of the transaction    
                    updateDate = originalInsertDate;
                }
                MemberSub memberSub = null;// PurchaseBL.GetSubscription(memberID, siteID);
                DateTime startDate = MemberTran.DetermineTransactionStartDate(mt, updateDate, memberSub);
                DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, mt, memberSub);
                if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
                {
                    command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
                    command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                    command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
                }

                command.AddParameter("@TimeAdjustOverrideTypeID", SqlDbType.Int, ParameterDirection.Input, (int)timeAdjustOverrideType);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
                command.AddParameter("@RenewDate", SqlDbType.SmallDateTime, ParameterDirection.Output);

                SyncWriter sw = new SyncWriter();
                Exception ex;
                Int32 returnValue = sw.Execute(command, out ex);
                sw.Dispose();

                if (ex != null)
                {
                    throw ex;
                }

                //get newly created member transaction
                mt = GetMemberTransactions(memberID, siteID).FindByID(memberTranID);

                if (returnValue == Constants.RETURN_OK)
                {

                    //update member attributes
                    DateTime renewDate = DateTime.MinValue;
                    if (command.Parameters["@RenewDate"].ParameterValue != DBNull.Value)
                    {
                        renewDate = (DateTime)command.Parameters["@RenewDate"].ParameterValue;
                    }

                    //DateTime renewDate = GetSubscription(memberID, siteID).RenewDate;

                    if (renewDate != DateTime.MinValue)
                    {
                        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                        //member.SetAttributeDate(Constants.NULL_INT, siteID, Constants.NULL_INT, PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE, renewDate );

                        //update member standard subscription
                        member.SetAttributeDate(Constants.NULL_INT,
                            siteID,
                            Constants.NULL_INT,
                            PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
                            renewDate);

                        member.SetAttributeInt(Constants.NULL_INT,
                            siteID,
                            Constants.NULL_INT,
                            PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_STATUS,
                            Convert.ToInt16(PurchaseBL.GetMemberSubStatus(member.MemberID, siteID, renewDate).Status));

                        MemberSaveResult result = MemberSA.Instance.SaveMember(member);
                        if (result.SaveStatus != MemberSaveStatusType.Success)
                        {
                            throw new BLException("Unable to save subscription expiration attribute for MemberID " + mt.MemberID.ToString());
                        }

                        // If this member is a Messmo user, we have to let Messmo know of this subscription end date change.
                        int communityID = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).Community.CommunityID;
                        bool siteMessmoEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_UI_ENABLED",
                            communityID, siteID));

                        if (siteMessmoEnabled)
                        {
                            int messmoCapable = member.GetAttributeInt(communityID, Constants.NULL_INT, Constants.NULL_INT, "MessmoCapable", 0);

                            if (messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending ||
                                messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable)
                            {
                                int languageID = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).LanguageID;
                                string messmoSubID = member.GetAttributeText(communityID, Constants.NULL_INT, Constants.NULL_INT, languageID,
                                    "MessmoSubscriberID", string.Empty);

                                Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoUserTransStatusRequestBySiteID(siteID,
                                    DateTime.Now, messmoSubID, Matchnet.HTTPMessaging.Constants.MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL, renewDate);
                            }
                        }

                        //update member premium subscription
                        PremiumServiceUtil.setPremiumServiceMember(MemberTranStatus.Success, mt, targetBrand, DateTime.MinValue, timeAdjustOverrideType);

                    }
                }

                return memberTranID;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while updating subscription.", ex);
            }
        }

        private void UPSSaveComplete(UPSMemberTranResult upsMemberTranResult)
        {
            UPSMemberTran upsMemberTran = upsMemberTranResult.UPSMemberTran;
            MemberTran memberTran = upsMemberTranResult.UPSMemberTran.MemberTran;
            MemberSub memberSub = null;
            Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType timeAdjustOverrideType = Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.None;
            bool isALaCarteOnly = false;
            bool isALaCarteCreditOnly = false;
            Command command = null;

            Trace.WriteLine("__" + memberTran.TranType.ToString());
            Trace.WriteLine("__" + upsMemberTranResult.MemberTranStatus.ToString());

            //Update Tran record
            if (memberTran.TranType != TranType.AdministrativeAdjustment
                && memberTran.TranType != TranType.Void
                && memberTran.TranType != TranType.AuthorizationNoTran)
            {
                command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Complete_V20", 0);
                command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
                command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)upsMemberTranResult.MemberTranStatus);
                command.AddParameter("@NewMemberTranID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(PurchaseBL.PRIMARYKEY_MEMBERTRANID));
                command.AddParameter("@NewMemberSubID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(PurchaseBL.PRIMARYKEY_MEMBERTRANID));
                //command.AddParameter("@ResponseCode", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResponseCode);
                if (memberTran.PaymentType == PaymentType.CreditCard)
                {
                    command.AddParameter("@CreditCardTypeID", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTran.Payment).CreditCardType);
                }
                //command.AddParameter("@AVSCode", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResponseCodeAVS);
                //command.AddParameter("@CVVCode", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResponseCodeCVV);
                if (upsMemberTranResult.ProviderID != Constants.NULL_INT)
                {
                    command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, upsMemberTranResult.ProviderID);
                }
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, upsMemberTranResult.OrderID);
                command.AddParameter("@CommonResponseCode", SqlDbType.Int, ParameterDirection.Input, upsMemberTranResult.CommonResponseCode);
                if (upsMemberTran.OriginalInsertDate != DateTime.MinValue)
                {
                    // The insert date is overrided with the original insert date of the transaction    
                    command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, upsMemberTran.OriginalInsertDate);
                }
                else
                {
                    command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                }
                //08192008 TL - ESP Project, Adding new parameter - PurchaseModeValidForCredit - to specify whether this transaction should reset the renew date, instead of extending it
                if (Plan.IsPurchaseModeValidForCredit(memberTran.PurchaseMode))
                    command.AddParameter("@PurchaseModeValidForCredit", SqlDbType.Bit, ParameterDirection.Input, Convert.ToBoolean(1));

                command.AddParameter("@RenewDate", SqlDbType.SmallDateTime, ParameterDirection.Output);

                //08212009 TL - Updated to support A La Carte
                //timeAdjustOverrideType will be used to also update Premium features purchased as A La Carte
                timeAdjustOverrideType = Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.None;
                if (PurchaseBL.UPSDoesOrderContainALaCartePlan(upsMemberTran.PackageID, upsMemberTran.BrandID))
                {
                    memberSub = null;// PurchaseBL.GetSubscription(memberTran.MemberID, memberTran.SiteID);
                    //check primary plan to see if this order is a la carte only, no base subscription
                    isALaCarteOnly = ((memberTran.Plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte);
                    command.AddParameter("@IsALaCarteOnly", SqlDbType.Bit, ParameterDirection.Input, isALaCarteOnly);

                    Plan highlightPlan = PurchaseBL.UPSGetALaCartePlanFromList(upsMemberTran.PackageID, upsMemberTran.BrandID, PremiumType.HighlightedProfile);
                    Plan existingHighlightPlan = PurchaseBL.UPSGetALaCartePlanFromMemberSub(memberSub, upsMemberTran.BrandID, PremiumType.HighlightedProfile);
                    Plan spotlightPlan = PurchaseBL.UPSGetALaCartePlanFromList(upsMemberTran.PackageID, upsMemberTran.BrandID, PremiumType.SpotlightMember);
                    Plan existingSpotlightPlan = PurchaseBL.UPSGetALaCartePlanFromMemberSub(memberSub, upsMemberTran.BrandID, PremiumType.SpotlightMember);
                    Plan allAccessPlan = PurchaseBL.UPSGetALaCartePlanFromList(upsMemberTran.PackageID, upsMemberTran.BrandID, PremiumType.AllAccess);
                    Plan existingAllAccessPlan = PurchaseBL.UPSGetALaCartePlanFromMemberSub(memberSub, upsMemberTran.BrandID, PremiumType.AllAccess);

                    if (highlightPlan != null)
                    {
                        command.AddParameter("@HighlightALaCartePlanID", SqlDbType.Int, ParameterDirection.Input, highlightPlan.PlanID);
                        timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.HighlightedProfile);
                    }
                    if (existingHighlightPlan != null)
                    {
                        command.AddParameter("@HighlightALaCartePlanIDToReplace", SqlDbType.Int, ParameterDirection.Input, existingHighlightPlan.PlanID);
                    }
                    if (spotlightPlan != null)
                    {
                        command.AddParameter("@SpotlightALaCartePlanID", SqlDbType.Int, ParameterDirection.Input, spotlightPlan.PlanID);
                        timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.SpotlightMember);
                    }
                    if (existingSpotlightPlan != null)
                    {
                        command.AddParameter("@SpotlightALaCartePlanIDToReplace", SqlDbType.Int, ParameterDirection.Input, existingSpotlightPlan.PlanID);
                    }
                    if (allAccessPlan != null)
                    {
                        command.AddParameter("@AllAccessALaCartePlanID", SqlDbType.Int, ParameterDirection.Input, allAccessPlan.PlanID);
                        timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.AllAccess);
                    }
                    if (existingAllAccessPlan != null)
                    {
                        command.AddParameter("@AllAccessALaCartePlanIDToReplace", SqlDbType.Int, ParameterDirection.Input, existingAllAccessPlan.PlanID);
                    }


                    command.AddParameter("@TimeAdjustOverrideTypeID", SqlDbType.Int, ParameterDirection.Input, (int)timeAdjustOverrideType);
                }

            }
            else if (memberTran.TranType == TranType.AuthorizationNoTran && upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success)
            {
                Trace.WriteLine("__dbo.up_MemberSub_Save_Payment_Update");
                command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Payment_Update", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
                command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberPaymentID);

            }
            else if (memberTran.TranType != TranType.AuthorizationNoTran)
            {
                //Administrative adjustments (credit tool)
                command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Adjust_V20", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
                command.AddParameter("@ReferenceMemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.ReferenceMemberTranID);
                command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.AdminMemberID);
                command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, memberTran.Duration);
                command.AddParameter("@DurationTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.DurationType);
                command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, memberTran.Amount);
                command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, memberTran.TransactionReasonID);
                command.AddParameter("@MemberSubID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberSubID);
                command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
                command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)upsMemberTranResult.MemberTranStatus);
                command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, upsMemberTranResult.ProviderID);
                if (memberTran.PaymentType == PaymentType.CreditCard)
                {
                    command.AddParameter("@CreditCardTypeID", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTran.Payment).CreditCardType);
                }
                command.AddParameter("@PaymentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.PaymentType);
                if (upsMemberTran.MemberTran.LegacyLastFourAccountNumber != Constants.NULL_STRING)
                {
                    command.AddParameter("@LastFourAccountNumber", SqlDbType.NVarChar, ParameterDirection.Input, upsMemberTran.MemberTran.LegacyLastFourAccountNumber);
                }

                if (memberTran.Plan != null)
                {
                    //TL 08212008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
                    if (memberTran.Plan.CreditAmount != Constants.NULL_DECIMAL && memberTran.Plan.CreditAmount > 0)
                        command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, memberTran.Plan.CreditAmount);
                }
                command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.PurchaseMode);

                DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
                if (upsMemberTran.OriginalInsertDate != DateTime.MinValue)
                {
                    // The insert date is overrided with the original insert date of the transaction    
                    updateDate = upsMemberTran.OriginalInsertDate;
                }

                memberSub = null;// PurchaseBL.GetSubscription(memberTran.MemberID, memberTran.SiteID);
                DateTime startDate = MemberTran.DetermineTransactionStartDate(memberTran, updateDate, memberSub);
                DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, memberTran, memberSub);
                if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
                {
                    command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
                    command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
                    command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
                }

                // For the credit tool, get the time adjust override type from the plan that was used in the transaction  
                Matchnet.Purchase.ValueObjects.Plan planToCheckForAdjustOverrideType = memberTran.Plan;
                if (planToCheckForAdjustOverrideType == null)
                {
                    planToCheckForAdjustOverrideType = PlanBL.Instance.GetPlans(PremiumServiceUtil.getBrand(memberTran.SiteID).BrandID).FindByID(memberTran.PlanID);
                }

                //TODO: Need to get "planlistID" to support crediting UPS Orders with multiple packages, whenever we start offering that
                //check primary plan to see if this order is a la carte only, no base subscription
                isALaCarteCreditOnly = ((planToCheckForAdjustOverrideType.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte);
                command.AddParameter("@IsALaCarteOnly", SqlDbType.Bit, ParameterDirection.Input, isALaCarteCreditOnly);

                if (Math.Abs(memberTran.Duration) > 0)
                {
                    timeAdjustOverrideType = Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.Standard;

                    if ((planToCheckForAdjustOverrideType.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile) == Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile)
                    {
                        timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.HighlightedProfile);
                    }
                    if ((planToCheckForAdjustOverrideType.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember) == Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember)
                    {
                        timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.SpotlightMember);
                    }
                    if ((planToCheckForAdjustOverrideType.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.AllAccess) == Matchnet.Purchase.ValueObjects.PremiumType.AllAccess)
                    {
                        timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.AllAccess);
                    }
                }

                command.AddParameter("@TimeAdjustOverrideTypeID", SqlDbType.Int, ParameterDirection.Input, (int)timeAdjustOverrideType);
                command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, upsMemberTranResult.OrderID);
                command.AddParameter("@CommonResponseCode", SqlDbType.Int, ParameterDirection.Input, upsMemberTranResult.CommonResponseCode);
                command.AddParameter("@RenewDate", SqlDbType.SmallDateTime, ParameterDirection.Output);

            }

            if (command != null)
            {
                SyncWriter sw = new SyncWriter();
                Exception ex;
                sw.Execute(command, out ex);
                sw.Dispose();

                if (ex != null)
                {
                    throw ex;
                }
            }

            //Update Member attributes
            if (memberTran.TranType != TranType.AuthorizationNoTran)
            {
                DateTime renewDate = DateTime.MinValue;
                if (command.Parameters["@RenewDate"].ParameterValue != DBNull.Value)
                {
                    renewDate = (DateTime)command.Parameters["@RenewDate"].ParameterValue;
                }

                Trace.WriteLine("__renewDate: " + renewDate.ToString() + " (" + memberTran.TranType.ToString() + ")");

                if ((upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success
                    || memberTran.TranType == TranType.Renewal
                    || memberTran.TranType == TranType.AdministrativeAdjustment
                    || memberTran.TranType == TranType.Void) && renewDate != DateTime.MinValue)
                {
                    //Update subscription expiration and status
                    Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTran.MemberID,
                        MemberLoadFlags.None);

                    // we need to know if the member's subscription has lapsed before we update the sub expiration date
                    DateTime oldSubExpDate = member.GetAttributeDate(Constants.NULL_INT,
                        memberTran.SiteID,
                        Constants.NULL_INT,
                        PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
                        DateTime.MinValue);

                    bool subLapsed = (oldSubExpDate < DateTime.Now);

                    member.SetAttributeDate(Constants.NULL_INT,
                        memberTran.SiteID,
                        Constants.NULL_INT,
                        PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
                        renewDate);

                    // Used for targeting in GAM, Google Ad Manager
                    if (memberTran.TranType == TranType.InitialBuy)
                    {
                        member.SetAttributeDate(Constants.NULL_INT,
                            memberTran.SiteID,
                            Constants.NULL_INT,
                            PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_LAST_INITIAL_PURCHASE_DATE,
                            DateTime.Now);

                        // If InitialBuy takes place after the sub has lapsed, we need to update another Attribute related to this
                        if (subLapsed)
                        {
                            member.SetAttributeDate(Constants.NULL_INT,
                                memberTran.SiteID,
                                Constants.NULL_INT,
                                PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_LAST_INITIAL_PURCHASE_DATE_AFTER_LAPSE,
                                DateTime.Now);
                        }
                    }

                    if (upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success && memberTran.TranType == TranType.InitialBuy)
                    {
                        bool checkFraud = Conversion.CBool(RuntimeSettings.GetSetting("ENABLE_IM_FRAUD_CHECK", Constants.NULL_INT, memberTran.SiteID));
                        if (checkFraud)
                        {
                            member.SetAttributeInt(Constants.NULL_INT, memberTran.SiteID, Constants.NULL_INT, "PassedFraudCheckSite", 0);
                        }
                    }


                    MemberSaveResult result = MemberSA.Instance.SaveMember(member);

                    if (result.SaveStatus != MemberSaveStatusType.Success)
                    {
                        throw new BLException("Unable to save subscription expiration attribute for MemberID " + memberTran.MemberID.ToString());
                    }

                    //Update premium services expiration dates; member attributes
                    //Premium Service expiration dates should reset to base subscription expiration date if
                    //1. Purchase Mode is valid for credit
                    //2. Purchase is only for A La Carte
                    if (Plan.IsPurchaseModeValidForCredit(memberTran.PurchaseMode) || isALaCarteOnly)
                    {
                        //specify premium service dates to be the same as the subscription expiration date
                        PremiumServiceUtil.setPremiumServiceMember(upsMemberTranResult.MemberTranStatus, memberTran, null, renewDate, timeAdjustOverrideType);
                    }
                    else
                    {
                        PremiumServiceUtil.setPremiumServiceMember(upsMemberTranResult.MemberTranStatus, memberTran, null, DateTime.MinValue, timeAdjustOverrideType);
                    }

                    //Update status
                    //Status needs to be updated after premium services has been applied
                    member = MemberSA.Instance.GetMember(memberTran.MemberID,
                        MemberLoadFlags.None);

                    member.SetAttributeInt(Constants.NULL_INT,
                        memberTran.SiteID,
                        Constants.NULL_INT,
                        PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_STATUS,
                        Convert.ToInt16(PurchaseBL.GetMemberSubStatus(member.MemberID, memberTran.SiteID, renewDate).Status));

                    result = MemberSA.Instance.SaveMember(member);

                    if (result.SaveStatus != MemberSaveStatusType.Success)
                    {
                        throw new BLException("Unable to save subscription status attribute for MemberID " + memberTran.MemberID.ToString());
                    }
                }

            }

            //08122008 TL
            //Insert into MemberTranCredit for a successful transaction;
            if (upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success
                && memberTran.MemberTranCreditCollection != null
                && memberTran.MemberTranCreditCollection.Count > 0)
            {
                Command[] commandArray = new Command[memberTran.MemberTranCreditCollection.Count];
                int c = 0;
                foreach (MemberTranCredit mtc in memberTran.MemberTranCreditCollection)
                {
                    command = new Command("mnSubscription", "dbo.up_MemberTranCredit_Save", 0);
                    command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
                    command.AddParameter("@CreditedFromTranID", SqlDbType.Int, ParameterDirection.Input, mtc.MemberTranID);
                    command.AddParameter("@CreditAmount", SqlDbType.Money, ParameterDirection.Input, (mtc.CreditAmount == Constants.NULL_DECIMAL) ? 0m : Decimal.Round(mtc.CreditAmount, 4));

                    commandArray[c] = command;
                    c++;
                }

                if (commandArray.Length > 0)
                {
                    SyncWriter sw2 = new SyncWriter();
                    Exception ex2;
                    sw2.Execute(commandArray, out ex2);
                    sw2.Dispose();

                    if (ex2 != null)
                    {
                        throw ex2;
                    }
                }
            }

            //save to mnChargeStage for reporting
            //if (memberTranResult.MemberTran.Payment != null)
            //{
            //    Trace.WriteLine("__avsCode: " + memberTranResult.ResponseCodeAVS);
            //    Trace.WriteLine("__cvvCode: " + memberTranResult.ResponseCodeCVV);
            //    Command commandChargeStage = new Command("mnChargeStage", "dbo.up_MemberPaymentFlat_Save", 0);
            //    commandChargeStage.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberPaymentID);
            //    commandChargeStage.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberID);
            //    commandChargeStage.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.FirstName);
            //    commandChargeStage.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.LastName);
            //    commandChargeStage.AddParameter("@Phone", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.Phone);
            //    commandChargeStage.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.AddressLine1);
            //    commandChargeStage.AddParameter("@AddressLine2", SqlDbType.NVarChar, ParameterDirection.Input, null);
            //    commandChargeStage.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.City);
            //    commandChargeStage.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.State);
            //    commandChargeStage.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.PostalCode);
            //    commandChargeStage.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.Payment.CountryRegionID);
            //    commandChargeStage.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            //    if (memberTranResult.MemberTran.Payment is CreditCardPayment)
            //    {
            //        commandChargeStage.AddParameter("@CreditCardExpirationMonth", SqlDbType.TinyInt, ParameterDirection.Input, ((CreditCardPayment)memberTranResult.MemberTran.Payment).ExpirationMonth);
            //        commandChargeStage.AddParameter("@CreditCardExpirationYear", SqlDbType.Int, ParameterDirection.Input, ((CreditCardPayment)memberTranResult.MemberTran.Payment).ExpirationYear);
            //        commandChargeStage.AddParameter("@CreditCardHash", SqlDbType.VarChar, ParameterDirection.Input, Crypto.Hash(memberTranResult.MemberTran.AccountNumber + PurchaseConstants.SOOPER_HASH));
            //    }

            //    Client.Instance.ExecuteAsyncWrite(commandChargeStage);
            //}

            //save request/response XML to mnChargeStage for reporting
            if (memberTranResult.logRequestResponseData)
            {
                this.LogRequestResponseXML(memberTranResult.ResponseData, memberTranResult.RequestDataScrubbed, memberTranResult);
            }

            //09252008 TL - THERE SHOULD BE NO CALL TO PREMIUMSERVICEUTIL HERE, it is called earlier above based on specific conditions
            //PremiumServiceUtil.highLightMember(memberTranResult.MemberTranStatus, memberTranResult.MemberTran, null);
            //Trace.WriteLine("before setPremiumServiceMember");
            //PremiumServiceUtil.setPremiumServiceMember(memberTranResult.MemberTranStatus, memberTranResult.MemberTran, null);
            //Trace.WriteLine("after setPremiumServiceMember");
            //highLightMember(memberTranResult);


            if (upsMemberTran.OriginalInsertDate != DateTime.MinValue)
            {
                // Do not send email if migrating data from another system    
                return;
            }

            #region Send subscription confirmation email
            Matchnet.Content.ValueObjects.BrandConfig.Site site =
                BrandConfigSA.Instance.GetBrandsBySite(memberTran.SiteID).GetSite(memberTran.SiteID);

            // Only send out email on successful initial buy OR free trial
            if (upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success &&
                memberTran != null &&
                ((memberTran.TranType == TranType.InitialBuy) ||
                (memberTran.TranType == TranType.AuthorizationOnly)))
            {
                // is this site configured to send out this alert?
                bool send = Convert.ToBoolean(RuntimeSettings.GetSetting(
                    "ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", site.Community.CommunityID, site.SiteID));

                if (send)
                {
                    int brandID = Constants.NULL_INT;

                    Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTran.MemberID,
                        MemberLoadFlags.None);

                    member.GetLastLogonDate(site.Community.CommunityID, out brandID);

                    int creditCardTypeID = Constants.NULL_INT;

                    if (memberTran.PaymentType == PaymentType.CreditCard)
                    {
                        CreditCardPayment creditCardPayment = (CreditCardPayment)memberTran.Payment;

                        if (creditCardPayment != null)
                        {

                            creditCardTypeID = (int)creditCardPayment.CreditCardType;
                        }
                    }

                    // determine plan type for this email 
                    int planType = Constants.NULL_INT;
                    if ((memberTran.Plan.PlanTypeMask & PlanType.Installment) == PlanType.Installment)
                    {
                        planType = (int)PlanType.Installment;
                    }
                    else if ((memberTran.Plan.PlanTypeMask & PlanType.FreeTrialWelcome) == PlanType.FreeTrialWelcome)
                    {
                        planType = (int)PlanType.FreeTrialWelcome;
                    }
                    else if ((memberTran.Plan.PlanTypeMask & PlanType.OneTimeOnly) == PlanType.OneTimeOnly)
                    {
                        planType = (int)PlanType.OneTimeOnly;
                    }
                    else
                    {
                        planType = (int)PlanType.Regular;
                    }

                    // Do not send email when it's an initial buy from a Free Trial. Sub confirm email will be sent out on initial auth Free Trial.
                    if ((planType == (int)PlanType.FreeTrialWelcome) && (memberTran.TranType == TranType.InitialBuy))
                    {
                        return;
                    }

                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand =
                        Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

                    ExternalMailSA.Instance.SendSubscriptionConfirmation(
                        member.MemberID,
                        site.SiteID,
                        brandID,
                        member.GetUserName(brand),
                        member.EmailAddress,
                        memberTran.Payment.FirstName,
                        memberTran.Payment.LastName,
                        String.Empty,
                        String.Empty,
                        String.Empty,
                        String.Empty,
                        memberTran.InsertDate,
                        planType,
                        (int)memberTran.Plan.CurrencyType,
                        memberTran.Plan.InitialCost,
                        memberTran.Plan.InitialDuration,
                        memberTran.Plan.InitialDurationType,
                        memberTran.Plan.RenewCost,
                        memberTran.Plan.RenewDuration,
                        memberTran.Plan.RenewDurationType,
                        String.Empty,
                        creditCardTypeID,
                        Convert.ToString(memberTran.MemberTranID));
                }
            }
            else if (upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success &&
                memberTran == null)
            {
                throw new ServiceBoundaryException(Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME, "Error sending subscription confirmation e-mail.  MemberTran was null.");
            }
            #endregion

            #region Send email to user if renewal failed.

            // Check if current transaction is for subscription renewal and if the current site has this (failed email notification) enabled.
            //if (memberTran != null)
            //{
            //    if (memberTran.TranType == TranType.Renewal && Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_SUBSCRIPTION_RENEWAL_FAILURE_EMAIL", site.Community.CommunityID, site.SiteID)))
            //    {
            //        if (upsMemberTranResult.MemberTranStatus == MemberTranStatus.Failure && upsMemberTranResult.CommonResponseCode != Constants.NULL_INT)
            //        {
            //            if (upsMemberTranResult.CommonResponseCode != Constants.NULL_INT)
            //            {
            //                string commonResponseCodeForSearch = Convert.ToString(upsMemberTranResult.CommonResponseCode);

            //                if (UPSHardDeclineCodes.BinarySearch(commonResponseCodeForSearch) >= 0)
            //                {
            //                    Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTran.MemberID, MemberLoadFlags.None);

            //                    int brandID = Constants.NULL_INT;
            //                    member.GetLastLogonDate(site.Community.CommunityID, out brandID);
            //                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

            //                    ExternalMailSA.Instance.SendRenewalFailureEmail(site.SiteID, member.GetUserName(brand), member.EmailAddress);

            //                    Logger.Instance.Log(Logger.LoggerType.RenewalFailureNotification, site.SiteID.ToString() + "\t" + member.MemberID.ToString() + "\t" + member.EmailAddress.ToString() + "\t" + commonResponseCodeForSearch);
            //                }
            //            }
            //        }
            //    }
            //}

            #endregion
        }

        private void UPSSaveCompletAuthPmtProfile(UPSMemberTranResult upsMemberTranResult)
        {
            try
            {
                MemberTran memberTran = upsMemberTranResult.UPSMemberTran.MemberTran;

                UPSProcessMemberAuthPmtProfileSaveComplete(memberTran.MemberTranID,
                    (Int32)upsMemberTranResult.MemberTranStatus,
                    upsMemberTranResult.OrderID,
                    upsMemberTranResult.CommonResponseCode,
                    upsMemberTranResult.ProviderID,
                    DateTime.Now);

                //saveChargeStage(memberTranResult);

                if (upsMemberTranResult.MemberTranStatus == MemberTranStatus.Success)
                {
                    if (upsMemberTranResult.UPSMemberTran.OriginalInsertDate != DateTime.MinValue)
                    {
                        // Do not send email if migrating data from another system    
                        return;
                    }

                    UPSSendPaymentProfileConfirmation(upsMemberTranResult);
                }
            }
            catch (Exception ex)
            {
                string message = this.ToString() + ".saveCompletAuthPmtProfile() - " + ex.Message;
                throw new Exception(message, ex);
            }
            finally
            {
            }

        }

        public void UPSProcessMemberAuthPmtProfileSaveComplete(Int32 MemberTranID,
            Int32 MemberTranStatusID,
            Int32 OrderID,
            Int32 CommonResponseCode,
            Int32 ProviderID,
            DateTime UpdateDate)
        {
            Command command = null;
            SyncWriter sw = null;
            try
            {
                #region build up the command object

                command = new Command("mnSubscription", "dbo.up_Member_Auth_Pmt_Profile_Save_Complete_V20", 0);

                command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, MemberTranID);
                command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, MemberTranStatusID);
                if (OrderID != Constants.NULL_INT)
                {
                    command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, OrderID);
                }
                if (CommonResponseCode != Constants.NULL_INT)
                {
                    command.AddParameter("@CommonResponseCode", SqlDbType.Int, ParameterDirection.Input, CommonResponseCode);
                }
                if (ProviderID != Constants.NULL_INT)
                {
                    command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, ProviderID);
                }
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, UpdateDate);

                #endregion

                sw = new SyncWriter();
                Exception ex;
                sw.Execute(command,
                    out ex);

                if (ex != null)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                string message = this.ToString() + ".UPSProcessMemberAuthPmtProfileSaveComplete()";
                Exception tempEx = ex;
                while (tempEx != null)
                {
                    message = message + " " + tempEx.Message;
                    tempEx = tempEx.InnerException;
                }
                throw new Exception(message, ex);
            }
            finally
            {
                command = null;
                if (sw != null)
                {
                    sw.Dispose();
                    sw = null;
                }
            }
        }

        private void UPSSendPaymentProfileConfirmation(UPSMemberTranResult upsMemberTranResult)
        {
            try
            {
                MemberTran memberTran = upsMemberTranResult.UPSMemberTran.MemberTran;

                Matchnet.Content.ValueObjects.BrandConfig.Site site = BrandConfigSA.Instance.GetBrandsBySite(memberTran.SiteID).GetSite(memberTran.SiteID);

                // is this site configured to send out this alert?
                bool send = Convert.ToBoolean(RuntimeSettings.GetSetting(
                    "ENABLE_PAYMENTPROFILE_CONFIRMATION_EMAIL", site.Community.CommunityID, site.SiteID));

                if (send)
                {
                    int brandID = Constants.NULL_INT;

                    Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTran.MemberID,
                        MemberLoadFlags.None);

                    member.GetLastLogonDate(site.Community.CommunityID, out brandID);

                    ExternalMailSA.Instance.SendPaymentProfileConfirmation(
                        member.MemberID,//int memberID,
                        site.SiteID,//int siteID,
                        brandID,//int brandID,
                        member.EmailAddress,//string emailAddress,
                        String.Empty,//string firstName,
                        String.Empty,//string lastName,
                        memberTran.InsertDate,//DateTime dateOfPurchase,
                        String.Empty,//string last4CC,
                        memberTran.MemberTranID.ToString()//string confirmationNumber
                        );
                }
            }
            catch (Exception ex)
            {
                //We're not throwing the exception here because its not worth to re-attempt the transaction
                //just because of failure due to sending emails.

                string errorMessage = this.ToString() + ".UPSSendPaymentProfileConfirmation()";
                Exception tempEx = ex;
                while (tempEx != null)
                {
                    errorMessage = errorMessage + " - " + tempEx.Message;
                    tempEx = tempEx.InnerException;
                }
                EventLog.WriteEntry(Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME, errorMessage);
            }
        }

        private ArrayList UPSHardDeclineCodes
        {
            get
            {
                if (_upsHardDeclineCodes == null)
                {
                    lock (_upsSyncBlock)
                    {
                        if (_upsHardDeclineCodes == null)
                        {
                            string[] codeValues = { "309", "101", "215", "401", "311", "216", "217", "102", "104", "105", "1209", "1204", "313", "222", "221", "214", "212", "1211" };
                            _upsHardDeclineCodes = new ArrayList(codeValues.Length);
                            _upsHardDeclineCodes.AddRange(codeValues);
                            _upsHardDeclineCodes.Sort();
                        }
                    }
                }
                return _upsHardDeclineCodes;
            }
        }

        public DiscountCollection GetDiscounts()
        {
            SqlDataReader dataReader = null;

            try
            {
                DiscountCollection discountCollection = _cache.Get(DiscountCollection.GetCacheKey(Constants.NULL_INT)) as DiscountCollection;

                if (discountCollection == null)
                {
                    Command command = new Command("mnSystem", "dbo.up_Discount_List", 0);
                    dataReader = Client.Instance.ExecuteReader(command);

                    discountCollection = new DiscountCollection();

                    bool colLookupDone = false;
                    Int32 ordinalDiscountID = Constants.NULL_INT;
                    Int32 ordinalDiscountType = Constants.NULL_INT;
                    Int32 ordinalAmount = Constants.NULL_INT;
                    Int32 ordinalDurationType = Constants.NULL_INT;
                    Int32 ordinalResourceConstant = Constants.NULL_INT;

                    while (dataReader.Read())
                    {
                        if (!colLookupDone)
                        {
                            ordinalDiscountID = dataReader.GetOrdinal("DiscountID");
                            ordinalDiscountType = dataReader.GetOrdinal("DiscountType");
                            ordinalAmount = dataReader.GetOrdinal("Amount");
                            ordinalDurationType = dataReader.GetOrdinal("DurationType");
                            ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
                            colLookupDone = true;
                        }

                        Discount discount = new Discount(dataReader.GetInt32(ordinalDiscountID),
                            (DiscountType)Enum.Parse(typeof(DiscountType), dataReader.GetString(ordinalDiscountType).Replace(" ", "")),
                            dataReader.GetDecimal(ordinalAmount),
                            (DurationType)Enum.Parse(typeof(DurationType), dataReader.GetString(ordinalDurationType)),
                            dataReader.GetString(ordinalResourceConstant));

                        discountCollection.Add(discount);
                    }

                    _cache.Add(discountCollection);

                }

                return discountCollection;
            }
            catch (Exception ex)
            {
                throw new BLException("BL error occured while getting discounts.", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

        public SubscriptionResult SaveDiscount(Int32 memberID,
            Int32 adminMemberID,
            Int32 siteID,
            Int32 discountID)
        {
            try
            {
                SubscriptionResult subRes = new SubscriptionResult();
                Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                Command command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Discount_Update", 0);
                if (adminMemberID != Constants.NULL_INT)
                {
                    command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
                }
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
                command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
                command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
                command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

                SyncWriter sw = new SyncWriter();
                Exception ex;
                Int32 returnValue = sw.Execute(command,
                    out ex);
                sw.Dispose();
                if (ex != null)
                {
                    throw ex;
                }

                if (returnValue == Constants.RETURN_OK)
                {
                    MemberTran mt = GetMemberTransactions(memberID, siteID).FindByID(memberTranID);
                    if (mt != null)
                    {
                        saveMemberTranToReportDB(mt,
                            Constants.NULL_STRING,
                            Constants.NULL_INT,
                            Constants.NULL_INT,
                            Constants.NULL_INT,
                            Constants.NULL_INT,
                            Constants.NULL_INT,
                            Constants.NULL_STRING);
                    }
                }

                subRes.ReturnValue = returnValue;
                subRes.ResourceConstant = getResourceConstant(returnValue);

                return subRes;
            }
            catch (Exception ex)
            {
                throw new BLException("SaveDiscount() error.", ex);
            }
        }

        public SubscriptionResult BeginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string addressLine2,
			string city,
			string state,
			string postalCode,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			Int32 checkNumber,
			Int32 conversionMemberID,
			BankAccountType bankAccountType,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress)
		{
			return this.BeginCheckPurchase(memberID, adminMemberID, planID, discountID, siteID, brandID, firstName, lastName, phone, addressLine1, addressLine2,
				city, state, postalCode, driversLicenseNumber, driversLicenseState, bankName, bankRoutingNumber, checkingAccountNumber,
				checkNumber, conversionMemberID, bankAccountType, sourceID, purchaseReasonTypeID, ipAddress, Constants.NULL_DECIMAL, PurchaseMode.None, Constants.NULL_INT);
		}

		/// <summary>
		/// 08042008 TL - Overloaded method to support additional parameters for transactions with crediting.
		/// </summary>
		/// <returns>SubscriptionResult</returns>
		public SubscriptionResult BeginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string addressLine2,
			string city,
			string state,
			string postalCode,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			Int32 checkNumber,
			Int32 conversionMemberID,
			BankAccountType bankAccountType,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID)
		{
			return this.BeginCheckPurchase(memberID, adminMemberID, planID, discountID, siteID, brandID, firstName, lastName, phone, addressLine1, addressLine2,
				city, state, postalCode, driversLicenseNumber, driversLicenseState, bankName, bankRoutingNumber, checkingAccountNumber,
				checkNumber, conversionMemberID, bankAccountType, sourceID, purchaseReasonTypeID, ipAddress, Constants.NULL_DECIMAL, PurchaseMode.None, Constants.NULL_INT, false);
		}

		/// <summary>
		/// 02092008 MM - Added new parameter for one-click functionality
		/// </summary>
		/// <returns>SubscriptionResult</returns>
		public SubscriptionResult BeginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string addressLine2,
			string city,
			string state,
			string postalCode,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankRoutingNumber,
			string checkingAccountNumber,
			Int32 checkNumber,
			Int32 conversionMemberID,
			BankAccountType bankAccountType,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID, 
			bool reusePreviousPayment
			)
		{
			try
			{
				if (siteID == 102)
				{
					throw new Exception("purchase not allowed");
				}

				bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

				SubscriptionResult subRes = new SubscriptionResult();
				Int32 memberPaymentID = Constants.NULL_INT;
				Int32 memberTranID = Constants.NULL_INT;
				Int32 chargeID = Constants.NULL_INT;

				//Get collection of MemberTran records as MemberTranCredit objects that will be used for determining credit
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
				MemberTranCreditCollection tranCredits = PurchaseBL.GetRemainingCredit(memberID, brand);
				decimal refreshedCreditAmount = Constants.NULL_DECIMAL;
				if (tranCredits != null)
					refreshedCreditAmount = tranCredits.TotalCreditedAmount;

				//Validate Plan
				Int32 returnValue = validatePlan(planID, memberID, adminMemberID, siteID, brandID, subRes, UICreditAmount, refreshedCreditAmount, purchaseMode, PaymentType.Check);

				if (returnValue == Constants.RETURN_OK)
				{
					//get plan object and initialize with values
					Plan plan = PlanBL.Instance.GetPlans(brandID).FindByID(planID);

					memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
					memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);
					chargeID = KeySA.Instance.GetKey(PRIMARYKEY_CHARGEID);
					MemberTran memberTran = getMemberTran(plan,
						PaymentType.Check,
						TranType.InitialBuy,
						chargeID,
						memberTranID,
						memberID,
						adminMemberID,
						discountID,
						siteID,
						memberPaymentID,
						firstName,
						lastName,
						null,
						phone,
						addressLine1,
						addressLine2,
						null,
						null,
						city,
						state,
						PurchaseConstants.REGION_ID_USA,
						postalCode,
						CreditCardType.AmericanExpress,
						checkingAccountNumber,
						Constants.NULL_INT,
						Constants.NULL_INT,
						Constants.NULL_STRING,
						driversLicenseNumber,
						driversLicenseState,
						bankName,
						null,
						bankRoutingNumber,
						checkNumber,
						bankAccountType);

					memberTran.RequestType = RequestType.ProviderRequest;
					memberTran.PurchaseMode = purchaseMode;
					memberTran.ReusePreviousPayment = reusePreviousPayment;

					if (IsValidForCredit)
					{
						memberTran.CreditAmount = UICreditAmount;
						memberTran.MemberTranCreditCollection = tranCredits; //set collection of transactions to be used for crediting for later storage into MemberTranCredited table
						
						if (UICreditAmount != Constants.NULL_DECIMAL)
						{
							if (UICreditAmount < plan.InitialCost)
							{
								memberTran.Amount = plan.InitialCost - UICreditAmount; //set updated amount to charge customer, will be used in Payment Service
							}
							else
								//ensures credited (discount) is not greater than plan's cost in case it escaped check from UI
							{
								returnValue = (int)ResourceConstants.PLAN_PURCHASE_VALIDATION_INVALID_CREDIT_DISCOUNT;
							}
						}
					}

					returnValue = beginCheckPurchase(memberID,
						adminMemberID,
						planID,
						discountID,
						siteID,
						getPurchaseReasonID(purchaseReasonTypeID, sourceID),
						purchaseReasonTypeID,
						sourceID,
						conversionMemberID,
						memberTranID,
						memberPaymentID,
						memberTran,
						ipAddress,
						promoID,
						reusePreviousPayment);
					
					if (returnValue == Constants.RETURN_OK)
					{
						saveToAdminDB(memberID, phone);
						saveToLookUpDB(memberID, bankRoutingNumber + checkingAccountNumber);
					}
				}

				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant(returnValue);
				subRes.MemberPaymentID = memberPaymentID;
				subRes.MemberTranID = memberTranID;

				return subRes;
			}
			catch (Exception ex)
			{
				throw new BLException("BeginCheckPurchase() error.", ex);
			}

		}

		public SubscriptionResult BeginAuthPmtProfile(
			Int32 memberID,
			Int32 adminMemberID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 referenceMemberPaymentId)
		{

			try
			{
				SubscriptionResult subRes = new SubscriptionResult();

				Int32 memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
				Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);
				Int32 chargeID = KeySA.Instance.GetKey(PRIMARYKEY_CHARGEID);

				Matchnet.Purchase.ValueObjects.Payment payment = null;
				
				SiteMerchant merchant = null;
				switch(siteID)
				{
					case 103:
					case 101:
					case 9171:
						merchant = GetSiteMerchants(siteID, CurrencyType.USDollar, PaymentType.CreditCard)[0];
						break;
					case 107:
					case 6:
						merchant = GetSiteMerchants(siteID, CurrencyType.Pound, PaymentType.CreditCard)[0];
						break;
					case 13:
						merchant = GetSiteMerchants(siteID, CurrencyType.CanadianDollar, PaymentType.CreditCard)[0];
						break;
					case 15:
					case 4:
					case 19:
						merchant = GetSiteMerchants(siteID, CurrencyType.Shekels, PaymentType.CreditCard)[0];
						break;
					case 105:
						merchant = GetSiteMerchants(siteID, CurrencyType.Euro, PaymentType.CreditCard)[0];
						break;
					default:
						throw new Exception("Payment profile currently not setup for this site.");
				}

				#region create PaymentType
				payment = new CreditCardPayment(memberID,
					siteID,
					memberPaymentID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					PaymentType.CreditCard,
					CurrencyType.USDollar,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					creditCardType,
					cvc,
					israeliID);
				#endregion

				#region create MemberTran
				MemberTran memberTran = new MemberTran(
					memberTranID//int memberTranID,
					,chargeID//int chargeID,
					,referenceMemberPaymentId//int referenceMemberTranID, HACK,use this field as referenceMemberPaymentId since its not being used
					,memberID//int memberID,
					,siteID//int siteID,
					,CurrencyType.USDollar//CurrencyType currencyType,
					,null//string resourceConstant,
					,TranType.AuthPmtProfile//TranType tranType,
					,null//string tranTypeResourceConstant,
					,Matchnet.Constants.NULL_INT//int memberSubID,
					,Constants.NULL_INT//int planID,
					,null//Plan plan,
					,null//string planResourceConstant,
					,Constants.NULL_INT//int discountID,
					,memberPaymentID//int memberPaymentID,
					,payment//Payment payment,
					,null//string paymentTypeDescription,
					,null//string paymentTypeResourceConstant,
					,MemberTranStatus.Pending//MemberTranStatus memberTranStatus,
					,null//string memberTranStatusDescription,
					,null//string memberTranStatusResourceConstant,
					,adminMemberID//int adminMemberID,
					,1//decimal amount,
					,1//int duration,
					,DurationType.Month//DurationType durationType,
					,null//string durationTypeResourceConstant,
					,Constants.NULL_INT//int transactionReasonID,
					,DateTime.Now//DateTime insertDate,
					,DateTime.Now//DateTime updateDate,
					,creditCardNumber//string accountNumber,
					,null//string discountResourceConstant,
					,PaymentType.CreditCard//PaymentType paymentType,
					,merchant//SiteMerchant siteMerchant,
					,Constants.NULL_INT//int minutes
					//,DateTime.MinValue // start date
					//,DateTime.MinValue // end date
					//,false // used to credit purchase
					//,PurchaseMode.None // purchase mode
					);

				memberTran.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;

				#endregion

				Int32 returnValue = processMemberAuthPmtProfileSaveInit(
					memberTran
					,1 // 1 dollar for now
					,purchaseReasonTypeID
					,sourceID
					,ipAddress
					);

				if (returnValue == Constants.RETURN_OK)
				{
					saveToAdminDB(memberID, memberTran.Payment.Phone);
					
					//this is an update to an existing creditcard
					if(creditCardNumber != null)
					{
						saveToLookUpDB(memberID, creditCardNumber);
					}
				}

				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant(returnValue);
				subRes.MemberPaymentID = memberPaymentID;
				subRes.MemberTranID = memberTranID;
				subRes.MemberTranStatus = MemberTranStatus.Pending;

				return subRes;
			}
			catch(Exception ex)
			{
				throw new BLException(this.ToString()+ ".BeginAuthPmtProfile()",ex);
			}

		}

		/// <summary>
		/// process to save to database and send to outgoing queue
		/// </summary>
		/// <param name="memberTran"></param>
		/// <param name="Amount"></param>
		/// <param name="purchaseReasonTypeID"></param>
		/// <param name="sourceID"></param>
		/// <param name="ipAddress"></param>
		/// <returns></returns>
		private Int32 processMemberAuthPmtProfileSaveInit(
			MemberTran memberTran
			,decimal Amount
			,Int32 purchaseReasonTypeID
			,Int32 sourceID
			,string ipAddress
			)
		{
			try
			{
				Command command = new Command("mnSubscription", "dbo.up_Member_Auth_Pmt_Profile_Save_Init", 0);

				#region Put all the DB param list together
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
				command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.TranType);
				if (memberTran.AdminMemberID != Constants.NULL_INT)
				{
					command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.AdminMemberID);
				}
				command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, Amount);
				if (memberTran.TransactionReasonID != Constants.NULL_INT)
				{
					command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, memberTran.TransactionReasonID);
				}
				if (memberTran.InsertDate != DateTime.MinValue)
				{
					command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, memberTran.InsertDate);
				}

				int PurchaseReasonID = getPurchaseReasonID(purchaseReasonTypeID, sourceID);

				if (PurchaseReasonID != Constants.NULL_INT)
				{
					command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, PurchaseReasonID);
				}

				if (purchaseReasonTypeID != Constants.NULL_INT)
				{
					command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
				}
				if (sourceID != Constants.NULL_INT)
				{
					command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
				}

				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
				command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberPaymentID);
				if ((int)memberTran.MemberTranStatus != Constants.NULL_INT)
				{
					command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.MemberTranStatus);
				}

				if (ipAddress != null)
				{
					command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
				}

				if ((int)memberTran.Payment.PaymentType != Constants.NULL_INT)
				{
					command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTran.Payment).CreditCardType );
				}

				command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberTran.MemberID, MemberLoadFlags.None).EmailAddress);

				#endregion
				SyncWriter sw = new SyncWriter();
				Exception swEx;
				Int32 returnValue = sw.Execute(command,
					new MessageSendTask[]{new MessageSendTask(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, memberTran)},
					out swEx);
				sw.Dispose();

				if (swEx != null)
				{
					throw swEx;
				}

				return returnValue;
			}
			catch(Exception ex)
			{
				throw new BLException(this.ToString()+".processMemberAuthPmtProfileSaveInit()",ex);
			}
		}
		
		public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress)
		{
			return this.BeginCreditCardPurchase(memberID, adminMemberID, planID, discountID, siteID, brandID, firstName, lastName, israeliID, phone, addressLine1,
				city, state, countryRegionID, postalCode, creditCardType, creditCardNumber, expirationMonth, expirationYear, cvc, conversionMemberID, 
				sourceID, purchaseReasonTypeID, ipAddress, Constants.NULL_INT);
		}


		/// <summary>
		/// Overload method for BeginCreditCardPurchase(), this method captures the PromoID into MemberTran table.	3/27/2008-RB
		/// </summary>
		/// <returns>SubscriptionResult</returns>
		public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID)
		{
			return this.BeginCreditCardPurchase(memberID, adminMemberID, planID, discountID, siteID, brandID, firstName, lastName, israeliID, phone, addressLine1,
				city, state, countryRegionID, postalCode, creditCardType, creditCardNumber, expirationMonth, expirationYear, cvc, conversionMemberID, 
				sourceID, purchaseReasonTypeID, ipAddress, promoID, Constants.NULL_DECIMAL, PurchaseMode.None);
		}

		/// <summary>
		/// 08042008 TL - Overload method for BeginCreditCardPurchase(), this method adds additional parameters to support crediting towards a purchase.
		/// </summary>
		/// <returns>SubscriptionResult</returns>
		public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode)
		{
			return this.BeginCreditCardPurchase(memberID, adminMemberID, planID, discountID, siteID, brandID, firstName, lastName, israeliID, phone, addressLine1,
				city, state, countryRegionID, postalCode, creditCardType, creditCardNumber, expirationMonth, expirationYear, cvc, conversionMemberID, 
				sourceID, purchaseReasonTypeID, ipAddress, promoID, Constants.NULL_DECIMAL, PurchaseMode.None, false);
		}

		public SubscriptionResult BeginCreditCardPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 brandID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID,
			decimal UICreditAmount,
			PurchaseMode purchaseMode, 
			bool reusePreviousPayment)
		{
			try
			{
				if (siteID == 102)
				{
					throw new Exception("purchase not allowed");
				}

				bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

				Int32 memberPaymentID = Constants.NULL_INT;
				Int32 memberTranID = Constants.NULL_INT;
				Int32 chargeID = Constants.NULL_INT;
				MemberTranStatus memberTranStatus = MemberTranStatus.Pending;
				SubscriptionResult subRes = new SubscriptionResult();

				//Get collection of MemberTran records as MemberTranCredit objects that will be used for determining credit
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
				MemberTranCreditCollection tranCredits = PurchaseBL.GetRemainingCredit(memberID, brand);
				decimal refreshedCreditAmount = Constants.NULL_DECIMAL; 
				if (tranCredits != null)
					refreshedCreditAmount = tranCredits.TotalCreditedAmount;

				//Validate Plan
				Int32 returnValue = validatePlan(planID, memberID, adminMemberID, siteID, brandID, subRes, UICreditAmount, refreshedCreditAmount, purchaseMode, PaymentType.CreditCard);
				
				if (returnValue == Constants.RETURN_OK)
				{
					//get plan object and initialize with values
					Plan plan = PlanBL.Instance.GetPlans(brandID).FindByID(planID);
					
					decimal amount = plan.InitialCost;
					memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
					memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);
					chargeID = KeySA.Instance.GetKey(PRIMARYKEY_CHARGEID);
		
					MemberTran mt = getMemberTran(plan,
						PaymentType.CreditCard,
						(plan.PlanTypeMask & PlanType.AuthOnly) == PlanType.AuthOnly ? TranType.AuthorizationOnly : TranType.InitialBuy,
						chargeID,
						memberTranID,
						memberID,
						adminMemberID,
						discountID,
						siteID,
						memberPaymentID,
						firstName,
						lastName,
						israeliID,
						phone,
						addressLine1,
						null,
						null,
						null,
						city,
						state,
						countryRegionID,
						postalCode,
						creditCardType,
						creditCardNumber,
						expirationMonth,
						expirationYear,
						cvc,
						null,
						null,
						null,
						null,
						null,
						Constants.NULL_INT,
						BankAccountType.Checking);

					mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;
					mt.PurchaseMode = purchaseMode;
					mt.ReusePreviousPayment = reusePreviousPayment;
					
					if (IsValidForCredit)
					{
						mt.CreditAmount = UICreditAmount;
						mt.MemberTranCreditCollection = tranCredits; //set collection of transactions to be used for crediting for later storage into MemberTranCredited table
						
						if (UICreditAmount != Constants.NULL_DECIMAL)
						{
							if (UICreditAmount < plan.InitialCost)
							{
								mt.Amount = plan.InitialCost - UICreditAmount; //set updated amount to charge customer, will be used in Payment Service
							}
							else
								//ensures credited (discount) is not greater than plan's cost in case it escaped check from UI
							{
								returnValue = (int)ResourceConstants.PLAN_PURCHASE_VALIDATION_INVALID_CREDIT_DISCOUNT;
							}
						}
					}

					if (returnValue == Constants.RETURN_OK)
					{
						returnValue = beginCreditCardPurchase(memberID,
							adminMemberID,
							planID,
							discountID,
							siteID,
							conversionMemberID,
							getPurchaseReasonID(purchaseReasonTypeID, sourceID),
							purchaseReasonTypeID,
							sourceID,
							memberTranID,
							memberPaymentID,
							memberTranStatus,
							mt,
							ipAddress,
							creditCardType,
							promoID,
							reusePreviousPayment);
					}

					if (returnValue == Constants.RETURN_OK)
					{
						saveToLookUpDB(memberID, creditCardNumber);
						saveToAdminDB(memberID, phone);
					}
				}

				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant(returnValue);
				subRes.MemberPaymentID = memberPaymentID;
				subRes.MemberTranID = memberTranID;

				return subRes;
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured while beginning credit card purchase.", ex);
			}
		}


		public Int32 BeginCreditCardVerification(Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear)
		{
			try
			{
				Int32 memberPaymentID = Constants.NULL_INT;
				Int32 memberTranID = Constants.NULL_INT;
				Int32 chargeID = Constants.NULL_INT;
				SubscriptionResult subRes = new SubscriptionResult();

				memberPaymentID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID);
				memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);
				chargeID = KeySA.Instance.GetKey(PRIMARYKEY_CHARGEID);

				Matchnet.Purchase.ValueObjects.Payment payment = null;
				SiteMerchant merchant = GetSiteMerchants(siteID, CurrencyType.USDollar, PaymentType.CreditCard)[0];
 
				payment = new CreditCardPayment(memberID,
					siteID,
					memberPaymentID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					PaymentType.CreditCard,
					CurrencyType.USDollar,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					creditCardType,
					null,
					null);
			
				MemberTran memberTran = new MemberTran(memberTranID,
					chargeID,
					Constants.NULL_INT,
					memberID,
					siteID,
					CurrencyType.USDollar,
					null,
					TranType.AuthorizationNoTran,
					null,
					Matchnet.Constants.NULL_INT,
					Constants.NULL_INT,
					null,
					null,
					Constants.NULL_INT,
					memberPaymentID,
					payment,
					null,
					null,
					MemberTranStatus.None,
					null,
					null,
					Constants.NULL_INT,
					1,
					1,
					DurationType.Month,
					null,
					Constants.NULL_INT,
					DateTime.Now,
					DateTime.Now,
					creditCardNumber,
					null,
					PaymentType.CreditCard,
					merchant,
					Constants.NULL_INT);

				memberTran.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;
				Matchnet.Queuing.Util.GetQueue(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, true, true).Send(memberTran, MessageQueueTransactionType.Single);

				return memberTranID;
			}
			catch (Exception ex)
			{
				throw new BLException("BeginCreditCardVerification error.", ex);
			}
		}

		
		public Int32 BeginCredit(TranType tranType,
			Int32 memberID,
			Int32 adminMemberID,
			Int32 siteID,
			Int32 merchantID,
			Int32 referenceMemberTranID,
			Int32 memberPaymentID,
			Int32 planID,
			decimal amount,
			Int32 minutes,
			Int32 duration,
			DurationType durationType,
			CurrencyType currencyType)
		{
			try
			{
				//MemberPaymentInfo mpi = PaymentSA.Instance.GetMemberPaymentInfo(memberID, memberPaymentID);

                //if (mpi == null)
                //{
                //    throw new Exception("null mpi (memberID: " + memberID.ToString() + ", memberPaymentID: " + memberPaymentID.ToString() + ")");
                //}

                NameValueCollection nvc = null;// PaymentSA.Instance.GetMemberPayment(memberID, mpi.GroupID, memberPaymentID);

				MemberTran memberTran = getMemberTran(currencyType,
					PaymentType.CreditCard,
					KeySA.Instance.GetKey(PRIMARYKEY_CHARGEID),
					KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID),
					referenceMemberTranID,
					memberID,
					adminMemberID,
					Constants.NULL_INT,
					siteID,
					memberPaymentID,
					planID,
					amount,
					tranType,
					duration,
					durationType,
					nvc["FirstName"],
					nvc["LastName"],
					nvc["IsraeliID"],
					nvc["Phone"],
					nvc["AddressLine1"],
					nvc["City"],
					nvc["State"],
					Convert.ToInt32(nvc["Country"]),
					nvc["PostalCode"],
					(CreditCardType)Enum.Parse(typeof(CreditCardType), nvc["CreditCardType"]),
					null,
					Convert.ToInt32(nvc["CreditCardExpirationMonth"]),
					Convert.ToInt32(nvc["CreditCardExpirationYear"]),
					null);

				memberTran.SavePayment = false;

				memberTran.RequestType = RequestType.ProviderRequest;

				MessageQueue queue = Matchnet.Queuing.Util.GetQueue(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, true, true);
				queue.Send(memberTran, MessageQueueTransactionType.Single);

				return memberTran.MemberTranID;
			}
			catch(Exception ex)
			{
				throw new BLException("BL error occured while executing credit for memberID: " + memberID.ToString() + " merchantID: " + merchantID.ToString() + " memberPaymentID: " + memberPaymentID.ToString(), ex);
			}
		}

		
		public SubscriptionResult BeginRenewalPurchase(Int32 memberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 communityID,
			Int32 brandID,
			Int32 adminMemberID, 
			Int32 memberPaymentID,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			Int32 promoID)
		{
			return this.BeginRenewalPurchase(memberID, planID, discountID, siteID, communityID, brandID, adminMemberID, memberPaymentID,
				conversionMemberID, sourceID, purchaseReasonTypeID, ipAddress, Constants.NULL_DECIMAL, PurchaseMode.None, promoID);
		}
		

		public SubscriptionResult BeginRenewalPurchase(Int32 memberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 communityID,
			Int32 brandID,
			Int32 adminMemberID, 
			Int32 memberPaymentID,
			Int32 conversionMemberID,
			Int32 sourceID,
			Int32 purchaseReasonTypeID,
			string ipAddress,
			decimal UICreditAmount,
			PurchaseMode purchaseMode,
			Int32 promoID)
		{
			try
			{
				if (siteID == 102)
				{
					throw new Exception("purchase not allowed");
				}

				bool IsValidForCredit = Plan.IsPurchaseModeValidForCredit(purchaseMode);

				Int32 memberTranID = Constants.NULL_INT;
				Int32 chargeID = Constants.NULL_INT;
				MemberTranStatus memberTranStatus = MemberTranStatus.Pending;
				SubscriptionResult subRes = new SubscriptionResult();

				//Get collection of MemberTran records as MemberTranCredit objects that will be used for determining credit
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
				MemberTranCreditCollection tranCredits = PurchaseBL.GetRemainingCredit(memberID, brand);
				decimal refreshedCreditAmount = Constants.NULL_DECIMAL;
				if (tranCredits != null)
					refreshedCreditAmount = tranCredits.TotalCreditedAmount;

				//Validate Plan
				Int32 returnValue = validatePlan(planID, memberID, adminMemberID, siteID, brandID, subRes, UICreditAmount, refreshedCreditAmount, purchaseMode, PaymentType.CreditCard);

				if (returnValue == Constants.RETURN_OK)
				{
					Plan plan = PlanBL.Instance.GetPlans(brandID).FindByID(planID);

					decimal amount = plan.InitialCost;
					chargeID = KeySA.Instance.GetKey(PRIMARYKEY_CHARGEID);
					memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

                    NameValueCollection nvc = null;// PaymentSA.Instance.GetMemberPayment(memberID,
                         //communityID,
                         //memberPaymentID);

					Int32 expirationMonth = Convert.ToInt32(getPaymentAttribute(nvc, "CreditCardExpirationMonth"));
					Int32 expirationYear = Convert.ToInt32(getPaymentAttribute(nvc, "CreditCardExpirationYear"));

					CreditCardType creditCardType = (CreditCardType)Enum.Parse(typeof(CreditCardType), getPaymentAttribute(nvc, "CreditCardType").ToString());

					MemberTran mt = getMemberTran(plan,
						PaymentType.CreditCard,
						TranType.InitialBuy,
						chargeID,
						memberTranID,
						memberID,
						adminMemberID,
						discountID,
						siteID,
						memberPaymentID,
						getPaymentAttribute(nvc, "FirstName"),
						getPaymentAttribute(nvc, "LastName"),
						getPaymentAttribute(nvc, "IsraeliID"),
						getPaymentAttribute(nvc, "Phone"),
						getPaymentAttribute(nvc, "AddressLine1"),
						null,
						null,
						null,
						getPaymentAttribute(nvc, "City"),
						getPaymentAttribute(nvc, "State"),
						Convert.ToInt32(getPaymentAttribute(nvc, "Country")),
						getPaymentAttribute(nvc, "PostalCode"),
						creditCardType,
						Constants.NULL_STRING,
						expirationMonth,
						expirationYear,
						null,
						null,
						null,
						null,
						null,
						null,
						Constants.NULL_INT,
						BankAccountType.Checking); 

					mt.SavePayment = false;
					mt.RequestType = RequestType.ProviderRequest;
					mt.PurchaseMode = purchaseMode;

					if (IsValidForCredit)
					{
						mt.CreditAmount = UICreditAmount;
						mt.MemberTranCreditCollection = tranCredits; //set collection of transactions to be used for crediting for later storage into MemberTranCredited table
						
						if (UICreditAmount != Constants.NULL_DECIMAL)
						{
							if (UICreditAmount < plan.InitialCost)
							{
								mt.Amount = plan.InitialCost - UICreditAmount; //set updated amount to charge customer, will be used in Payment Service
							}
							else
								//ensures credited (discount) is not greater than plan's cost in case it escaped check from UI
							{
								returnValue = (int)ResourceConstants.PLAN_PURCHASE_VALIDATION_INVALID_CREDIT_DISCOUNT;
							}
						}
					}

					returnValue = beginRenewalPurchase(memberID,
						adminMemberID,
						planID,
						discountID,
						siteID,
						conversionMemberID,
						getPurchaseReasonID(purchaseReasonTypeID, sourceID),
						purchaseReasonTypeID,
						sourceID,
						memberTranID,
						memberPaymentID,
						memberTranStatus,
						mt,
						ipAddress,
						creditCardType,
						promoID);
				}

				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant(returnValue);
				subRes.MemberTranID = memberTranID;

				return subRes;
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured while beginning renewal purchase.", ex);
			}
		}
		
		
		public SubscriptionResult EndSubscription(Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID, 
			Int32 transactionReasonID)
		{
			try
			{	
				Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

				Int32 returnValue = endSubscription(
					memberID,
					adminMemberID,
					siteID,
					transactionReasonID,
					memberTranID);

				if (returnValue == Constants.RETURN_OK)
				{
					MemberTran memberTran = GetMemberTransactions(memberID, siteID).FindByID(memberTranID);
					if (memberTran != null)
					{
						saveMemberTranToReportDB(memberTran,
							Constants.NULL_STRING,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_STRING);
					}
				}

				SubscriptionResult subRes = new SubscriptionResult();
				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant( returnValue);
				subRes.MemberTranID = memberTranID;

				return subRes;
			}
			catch (Exception ex)
			{
				throw new BLException("EndSubscription error() (memberID: " + memberID.ToString() + ", adminMemberID: " + adminMemberID.ToString()+ ", siteID: " + siteID.ToString() + ", transactionReasonID: " + transactionReasonID.ToString() + ").", ex);
			}
		}

		public SubscriptionResult EndALaCarteOnly(int memberID, 
			int adminMemberID,
			int siteID, 
			int planID)
		{
			try
			{	
				Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

				Int32 returnValue = endALaCarteOnly(
					memberID,
					adminMemberID,
					siteID,
					memberTranID,
					planID);

				if (returnValue == Constants.RETURN_OK)
				{
					MemberTran memberTran = GetMemberTransactions(memberID, siteID).FindByID(memberTranID);
					if (memberTran != null)
					{
						saveMemberTranToReportDB(memberTran,
							Constants.NULL_STRING,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_STRING);
					}
				}

				SubscriptionResult subRes = new SubscriptionResult();
				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant( returnValue);
				subRes.MemberTranID = memberTranID;

				return subRes;
			}
			catch (Exception ex)
			{
				throw new BLException("EndALaCarteOnly error() (memberID: " + memberID.ToString() + ", adminMemberID: " + adminMemberID.ToString()+ ", siteID: " + siteID.ToString() + ", planID: " + planID.ToString() + ").", ex);
			}
		}		
		
		public NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID)
		{
            return null;
            //PaymentSA.Instance.GetChargeLog(memberID, memberTranID);
		}
		

		public MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID)
		{
            return MemberTranStatus.None; 
            //PaymentSA.Instance.GetChargeStatus(memberID, memberTranID);
		}
			

		public Int32 GetMemberIDByMemberTranID(Int32 memberTranID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberTran_List_ByID", 0);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				dataReader = Client.Instance.ExecuteReader(command);

				while (dataReader.Read())
				{
					return dataReader.GetInt32(dataReader.GetOrdinal("MemberID"));
				}

				return Constants.NULL_INT;
			}
			catch (Exception ex)
			{
				throw new Exception("GetMemberIDByMemberTranID() error (memberTranID: " + memberTranID.ToString() + ").", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}


		public Int32[] GetMemberIDsByAccountNumber(string accountNumber)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnLookup", "dbo.up_CSLookup_List", 0);
				command.AddParameter("@Hash", SqlDbType.VarChar, ParameterDirection.Input, Crypto.Hash(accountNumber + PurchaseConstants.SOOPER_HASH));
				command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, 1000);
				command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output);
				dataReader = Client.Instance.ExecuteReader(command);

				ArrayList memberIDs = new ArrayList();

				Int32 ordinal = dataReader.GetOrdinal("MemberID");
				while (dataReader.Read())
				{
					memberIDs.Add(dataReader.GetInt32(ordinal));
				}

				return (Int32[])memberIDs.ToArray(typeof(Int32));
			}
			catch (Exception ex)
			{
				throw new Exception("GetMemberIDsByAccountNumber() error.", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}
			
		public NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID)
		{
            return new NameValueCollection();
            //return PaymentSA.Instance.GetMemberPayment(memberID, 
            //    communityID, 
            //    memberPaymentID);
		}


		public NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID)
		{
            return null;
            //return PaymentSA.Instance.GetMemberPaymentByMemberTranID(memberID, 
            //    memberTranID);
		}

		/// <summary>
		/// Method for the new UPS Subscription Confirmation Page.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="orderID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public NameValueCollection UPSGetMemberPaymentByOrderID(Int32 memberID, Int32 orderID, Int32 siteID)
		{
			NameValueCollection nameValueCollection = new NameValueCollection();

			UPSMemberTranInfo upsMemberTranInfo =  UPSGetMemberTranInfo(orderID);

			if (upsMemberTranInfo != null)
			{
				MemberTranCollection memberTranCollection = this.GetMemberTransactions(memberID, siteID);

                if (memberTranCollection != null)
                {
                    MemberTran memberTran = memberTranCollection.FindByID(upsMemberTranInfo.MemberTranID);

                    nameValueCollection.Add("MemberID", memberTran.MemberID.ToString());
                    nameValueCollection.Add("MemberTranStatus", ((int)memberTran.MemberTranStatus).ToString());
                    nameValueCollection.Add("CreditCardNumber", memberTran.LegacyLastFourAccountNumber);
                    nameValueCollection.Add("CreditCardType", memberTran.CreditCardType.ToString());
                    nameValueCollection.Add("MemberTranID", memberTran.MemberTranID.ToString());
                    nameValueCollection.Add("PaymentTypeID", ((int)memberTran.PaymentType).ToString());

                    MemberSub ms = GetSubscription(memberTran.MemberID, siteID);
                    if (ms != null)
                    {
                        int communityID = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).Community.CommunityID;

                        NameValueCollection msnvc = GetMemberPayment(memberTran.MemberID, communityID, ms.MemberPaymentID);

                        if (msnvc != null)
                        {
                            nameValueCollection.Add("Phone", msnvc.Get("Phone"));
                        }
                    }
                }
			}
			
			return nameValueCollection;
		}

		public static MemberSub GetSubscription(Int32 memberID, 
			Int32 siteID)
		{
			MemberSub memberSub = null;
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberSub_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				dataReader = Client.Instance.ExecuteReader(command);

				//MemberPaymentInfo mpi = null;
				PaymentType paymentType = PaymentType.None;

				bool colLookupDone = false;
				Int32 ordinalMemberPaymentID = Constants.NULL_INT;
				Int32 ordinalMemberSubID = Constants.NULL_INT;
				Int32 ordinalPlanID = Constants.NULL_INT;
				Int32 ordinalPlanResourceConstant = Constants.NULL_INT;
				Int32 ordinalDiscountID = Constants.NULL_INT;
				Int32 ordinalDiscountStartDate = Constants.NULL_INT;
				Int32 ordinalDiscountRemaining = Constants.NULL_INT;
				Int32 ordinalDiscountResourceConstant = Constants.NULL_INT;
				Int32 ordinalRenewDate = Constants.NULL_INT;
				Int32 ordinalEndDate = Constants.NULL_INT;
				Int32 ordinalRenewAttempts = Constants.NULL_INT;
				Int32 ordinalUpdateDate = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalMemberPaymentID = dataReader.GetOrdinal("MemberPaymentID");
						ordinalMemberSubID = dataReader.GetOrdinal("MemberSubID");
						ordinalPlanID = dataReader.GetOrdinal("PlanID");
						ordinalPlanResourceConstant = dataReader.GetOrdinal("PlanResourceConstant");
						ordinalDiscountID = dataReader.GetOrdinal("DiscountID");
						ordinalDiscountStartDate = dataReader.GetOrdinal("DiscountStartDate");
						ordinalDiscountRemaining = dataReader.GetOrdinal("DiscountRemaining");
						ordinalDiscountResourceConstant = dataReader.GetOrdinal("DiscountResourceConstant");
						ordinalRenewDate = dataReader.GetOrdinal("RenewDate");
						ordinalEndDate = dataReader.GetOrdinal("EndDate");
						ordinalRenewAttempts = dataReader.GetOrdinal("RenewAttempts");
						ordinalUpdateDate = dataReader.GetOrdinal("UpdateDate");
						colLookupDone = true;
					}
									
					Int32 memberPaymentID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalMemberPaymentID))
					{
						memberPaymentID = dataReader.GetInt32(ordinalMemberPaymentID);
					}

					if ( memberPaymentID != Constants.NULL_INT )
					{
						//mpi = PaymentSA.Instance.GetMemberPaymentInfo(memberID, memberPaymentID);
					}

                    //if (mpi != null)
                    //{
                    //    paymentType = mpi.PaymentTypeID != Constants.NULL_INT ? (PaymentType)Enum.Parse(typeof(PaymentType), mpi.PaymentTypeID.ToString()) : PaymentType.None;
                    //}

					memberSub = new MemberSub(dataReader.GetInt32(ordinalMemberSubID),
						memberID,
						siteID,
						memberPaymentID,
						dataReader.GetInt32(ordinalPlanID),
						dataReader.GetString(ordinalPlanResourceConstant),
						dataReader.GetInt32(ordinalDiscountID),
						!dataReader.IsDBNull(ordinalDiscountStartDate) ? dataReader.GetDateTime(ordinalDiscountStartDate) : DateTime.MinValue,
						dataReader.GetInt32(ordinalDiscountRemaining),
						dataReader.GetString(ordinalDiscountResourceConstant),
						dataReader.GetDateTime(ordinalRenewDate),
						!dataReader.IsDBNull(ordinalEndDate) ? dataReader.GetDateTime(ordinalEndDate) : DateTime.MinValue,
						dataReader.GetInt32(ordinalRenewAttempts),
						dataReader.GetDateTime(ordinalUpdateDate),
						paymentType);
				}

				if (memberSub != null && memberSub.MemberSubID != Constants.NULL_INT)
				{
					memberSub.MemberSubExtended = GetMemberSubExtended(memberSub.MemberSubID);
				}
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured when retrieving subscription.", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}

			return memberSub; 
		}

		public static MemberSubExtended[] GetMemberSubExtended(int memberSubID)
		{
			SqlDataReader dataReaderSE = null;
			MemberSubExtended[] arrMemberSubExtended = null;
			try
			{
				//Get MemberSubExtended information
				Command commandSE = new Command("mnSubscription", "dbo.up_MemberSubExtended_ListByMemberSubID", 0);
				commandSE.AddParameter("@MemberSubID", SqlDbType.Int, ParameterDirection.Input, memberSubID);
				dataReaderSE = Client.Instance.ExecuteReader(commandSE);

				bool colLookupDoneSE = false;
				Int32 ordinalMemberSubIDSE = Constants.NULL_INT;
				Int32 ordinalPlanIDSE = Constants.NULL_INT;
				Int32 ordinalEndDateSE = Constants.NULL_INT;

				ArrayList memberSubExtendeds = new ArrayList();
				while (dataReaderSE.Read())
				{
					if (!colLookupDoneSE)
					{
						ordinalMemberSubIDSE = dataReaderSE.GetOrdinal("MemberSubID");
						ordinalPlanIDSE = dataReaderSE.GetOrdinal("PlanID");
						ordinalEndDateSE = dataReaderSE.GetOrdinal("EndDate");
						colLookupDoneSE = true;
					}

					memberSubExtendeds.Add(new MemberSubExtended(dataReaderSE.GetInt32(ordinalMemberSubIDSE),
						dataReaderSE.GetInt32(ordinalPlanIDSE), 
						!dataReaderSE.IsDBNull(ordinalEndDateSE) ? dataReaderSE.GetDateTime(ordinalEndDateSE) : DateTime.MinValue));

				}

				arrMemberSubExtended = new MemberSubExtended[memberSubExtendeds.Count];
				for (int j = 0; j < memberSubExtendeds.Count; j++)
				{
					arrMemberSubExtended[j] = (MemberSubExtended)memberSubExtendeds[j];
				}
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured when retrieving subscription extended.", ex);
			}
			finally
			{
				if (dataReaderSE != null)
				{
					dataReaderSE.Close();
				}
			}

			return arrMemberSubExtended;
		}

		public Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID)
		{
            return Constants.NULL_INT;
            //return PaymentSA.Instance.GetSuccessfulPayment(memberID, 
            //    siteID);
		}
		
		public bool HasSubscriptionEnded(Int32 memberID,
			Int32 siteID)
		{
			try
			{
				MemberSub ms = GetSubscription(memberID, siteID);
				if(ms != null)
				{
					return (ms.EndDate != DateTime.MinValue);
				}
				else 
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured while getting subscription status.", ex);
			}
		}


		public bool HasAcceptedFreeTrial(Int32 planID, 
			Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID,
			Int32 brandID)
		{
			try
			{
				if (validatePlan(planID, memberID, adminMemberID, siteID, brandID) == Constants.RETURN_OK)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				throw new BLException("HasAcceptedFreeTrial() error.", ex);
			}
		}


		public bool IsEligibleForSaveOffer(Int32 memberID,
			Int32 siteID)
		{
			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberTran_List_SaveOffer", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
				return Convert.ToInt32(Client.Instance.ExecuteNonQuery(command)["@ReturnValue"].Value) == 0;

			}
			catch (Exception ex)
			{
				throw new BLException("IsEligibleForSaveOffer() error (memberID: " + memberID.ToString() + ", siteID: " + siteID.ToString() + ").", ex);
			}
		}

		
		public SubscriptionResult GetMemberTranStatus(Int32 memberID,
			Int32 memberTranID)
		{
			Int32 siteID = Constants.NULL_INT;
			SubscriptionResult subRes = new SubscriptionResult();
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberTran_List_Status", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalResourceConstant = Constants.NULL_INT;
				Int32 ordinalMemberTranStatusID = Constants.NULL_INT;
				Int32 ordinalMemberPaymentID = Constants.NULL_INT;
				Int32 ordinalPlanID = Constants.NULL_INT;
				Int32 ordinalAmount = Constants.NULL_INT;
				Int32 ordinalDuration = Constants.NULL_INT;
				Int32 ordinalGroupID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
						ordinalMemberTranStatusID = dataReader.GetOrdinal("MemberTranStatusID");
						ordinalMemberPaymentID = dataReader.GetOrdinal("MemberPaymentID");
						ordinalPlanID = dataReader.GetOrdinal("PlanID");
						ordinalAmount = dataReader.GetOrdinal("Amount");
						ordinalDuration = dataReader.GetOrdinal("Duration");
						ordinalGroupID = dataReader.GetOrdinal("GroupID");
						colLookupDone = true;
					}
				
					subRes.MemberTranID = memberTranID;
					subRes.MemberTranStatus = (MemberTranStatus)Enum.Parse(typeof(MemberTranStatus), dataReader.GetInt32(ordinalMemberTranStatusID).ToString());
					subRes.PlanID = dataReader.GetInt32(ordinalPlanID);
					subRes.Amount = dataReader.GetDecimal(ordinalAmount);
					subRes.Duration = dataReader.GetInt32(ordinalDuration);
					siteID = dataReader.GetInt32(ordinalGroupID);

					if (!dataReader.IsDBNull(ordinalResourceConstant))
					{
						subRes.ResourceConstant = dataReader.GetString(ordinalResourceConstant);
					}

					if (!dataReader.IsDBNull(ordinalMemberPaymentID))
					{
						subRes.MemberPaymentID = dataReader.GetInt32(ordinalMemberPaymentID);
					}
				}
				
				return subRes;
			}
			catch (Exception ex)
			{
				throw (new BLException("BL error occured while getting member transaction status.", ex));
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		public MemberTranCollection GetMemberTransactions(Int32 memberID, 
			Int32 siteID)
		{
            //NO MORE MEMBERTRAN
            return null;


			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberTran_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				dataReader = Client.Instance.ExecuteReader(command);
				
				MemberTranCollection memberTranCollection = null;
                //MemberPaymentInfo mpi = null;

				bool colLookupDone = false;
				Int32 ordinalReferenceMemberTranID = Constants.NULL_INT;
				Int32 ordinalResourceConstant = Constants.NULL_INT;
				Int32 ordinalMemberSubID = Constants.NULL_INT;
				Int32 ordinalMemberPaymentID = Constants.NULL_INT;
				Int32 ordinalMemberTranStatusID = Constants.NULL_INT;
				Int32 ordinalAdminMemberID = Constants.NULL_INT;
				Int32 ordinalTransactionReasonID = Constants.NULL_INT;
				Int32 ordinalMemberTranID = Constants.NULL_INT;
				Int32 ordinalMemberID = Constants.NULL_INT;
				Int32 ordinalGroupID = Constants.NULL_INT;
				Int32 ordinalTranTypeID = Constants.NULL_INT;
				Int32 ordinalTranTypeResourceConstant = Constants.NULL_INT;
				Int32 ordinalPlanID = Constants.NULL_INT;
				Int32 ordinalPlanResourceConstant = Constants.NULL_INT;
				Int32 ordinalDiscountID = Constants.NULL_INT;
				Int32 ordinalMemberTranStatusDescription = Constants.NULL_INT;
				Int32 ordinalMemberTranStatusResourceConstant = Constants.NULL_INT;
				Int32 ordinalAmount = Constants.NULL_INT;
				Int32 ordinalCurrencyID = Constants.NULL_INT;
				Int32 ordinalDuration = Constants.NULL_INT;
				Int32 ordinalDurationTypeID = Constants.NULL_INT;
				Int32 ordinalDurationTypeResourceConstant = Constants.NULL_INT;
				Int32 ordinalInsertDate = Constants.NULL_INT;
				Int32 ordinalUpdateDate = Constants.NULL_INT;
				Int32 ordinalDiscountResourceConstant = Constants.NULL_INT;
				Int32 ordinalStartDate = Constants.NULL_INT;
				Int32 ordinalEndDate = Constants.NULL_INT;
				Int32 ordinalTotalCreditedAmount = Constants.NULL_INT;
				Int32 ordinalPurchaseModeID = Constants.NULL_INT;
				Int32 ordinalUsedToCreditPurchase = Constants.NULL_INT;
				Int32 ordinalCreditedFromTranID = Constants.NULL_INT;
				Int32 ordinalCreditAmount = Constants.NULL_INT;
				Int32 ordinalPromoID = Constants.NULL_INT;
				Int32 ordinalTimeAdjustOverrideTypeID = Constants.NULL_INT;
				Int32 ordinalPlanIDList = Constants.NULL_INT;
				Int32 ordinalLegacyPaymentTypeResourceConstant = Constants.NULL_INT;
				Int32 ordinalLegacyLastFourAccountNumber = Constants.NULL_INT;
				Int32 ordinalCreditCardType = Constants.NULL_INT;
				Int32 ordinalOrderID = Constants.NULL_INT;
				Int32 ordinalPaymentTypeID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalReferenceMemberTranID = dataReader.GetOrdinal("ReferenceMemberTranID");
						ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
						ordinalMemberSubID = dataReader.GetOrdinal("MemberSubID");
						ordinalMemberPaymentID = dataReader.GetOrdinal("MemberPaymentID");
						ordinalMemberTranStatusID = dataReader.GetOrdinal("MemberTranStatusID");
						ordinalAdminMemberID = dataReader.GetOrdinal("AdminMemberID");
						ordinalTransactionReasonID = dataReader.GetOrdinal("TransactionReasonID");
						ordinalMemberTranID = dataReader.GetOrdinal("MemberTranID");
						ordinalMemberID = dataReader.GetOrdinal("MemberID");
						ordinalGroupID = dataReader.GetOrdinal("GroupID");
						ordinalTranTypeID = dataReader.GetOrdinal("TranTypeID");
						ordinalTranTypeResourceConstant = dataReader.GetOrdinal("TranTypeResourceConstant");
						ordinalPlanID = dataReader.GetOrdinal("PlanID");
						ordinalPlanResourceConstant = dataReader.GetOrdinal("PlanResourceConstant");
						ordinalDiscountID = dataReader.GetOrdinal("DiscountID");
						ordinalMemberTranStatusDescription = dataReader.GetOrdinal("MemberTranStatusDescription");
						ordinalMemberTranStatusResourceConstant = dataReader.GetOrdinal("MemberTranStatusResourceConstant");
						ordinalAmount = dataReader.GetOrdinal("Amount");
						ordinalCurrencyID = dataReader.GetOrdinal("CurrencyID");
						ordinalDuration = dataReader.GetOrdinal("Duration");
						ordinalDurationTypeID = dataReader.GetOrdinal("DurationTypeID");
						ordinalDurationTypeResourceConstant = dataReader.GetOrdinal("DurationTypeResourceConstant");
						ordinalInsertDate = dataReader.GetOrdinal("InsertDate");
						ordinalUpdateDate = dataReader.GetOrdinal("UpdateDate");
						ordinalDiscountResourceConstant = dataReader.GetOrdinal("DiscountResourceConstant");
						ordinalStartDate = dataReader.GetOrdinal("StartDate");
						ordinalEndDate = dataReader.GetOrdinal("EndDate");
						ordinalTotalCreditedAmount = dataReader.GetOrdinal("TotalCreditedAmount");
						ordinalPurchaseModeID = dataReader.GetOrdinal("PurchaseModeID");
						ordinalUsedToCreditPurchase = dataReader.GetOrdinal("UsedToCreditPurchase");
						ordinalPromoID = dataReader.GetOrdinal("PromoID");
						ordinalTimeAdjustOverrideTypeID = dataReader.GetOrdinal("TimeAdjustOverrideTypeID");
						ordinalPlanIDList = dataReader.GetOrdinal("PlanIDList");
						ordinalLegacyPaymentTypeResourceConstant = dataReader.GetOrdinal("LegacyPaymentTypeResourceConstant");
						ordinalLegacyLastFourAccountNumber = dataReader.GetOrdinal("LegacyLastFourAccountNumber");
						ordinalCreditCardType = dataReader.GetOrdinal("CreditCardType");
						ordinalOrderID = dataReader.GetOrdinal("OrderID");
						ordinalPaymentTypeID = dataReader.GetOrdinal("PaymentTypeID");
						colLookupDone = true;

						memberTranCollection = new MemberTranCollection(memberID, siteID);
					}


					Int32 referenceMemberTranID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalReferenceMemberTranID))
					{
						referenceMemberTranID = dataReader.GetInt32(ordinalReferenceMemberTranID);
					}

					string resourceConstant = Constants.NULL_STRING;
					if (!dataReader.IsDBNull(ordinalResourceConstant))
					{
						resourceConstant = dataReader.GetString(ordinalResourceConstant);
					}

					Int32 memberSubID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalMemberSubID))
					{
						memberSubID = dataReader.GetInt32(ordinalMemberSubID);
					}

					Int32 memberPaymentID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalMemberPaymentID))
					{
						memberPaymentID = dataReader.GetInt32(ordinalMemberPaymentID);
					}

					MemberTranStatus memberTranStatus = MemberTranStatus.None;
					if (!dataReader.IsDBNull(ordinalMemberTranStatusID))
					{
						memberTranStatus = (MemberTranStatus)Enum.Parse(typeof(MemberTranStatus), dataReader.GetInt32(ordinalMemberTranStatusID).ToString());
					}

					Int32 adminMemberID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalAdminMemberID))
					{
						adminMemberID = dataReader.GetInt32(ordinalAdminMemberID);
					}
					
					Int32 transactionReasonID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalTransactionReasonID))
					{
						transactionReasonID = dataReader.GetInt32(ordinalTransactionReasonID);
					}

					DateTime startDate = DateTime.MinValue;
					if (!dataReader.IsDBNull(ordinalStartDate))
					{
						startDate = dataReader.GetDateTime(ordinalStartDate);
					}

					DateTime endDate = DateTime.MinValue;
					if (!dataReader.IsDBNull(ordinalEndDate))
					{
						endDate = dataReader.GetDateTime(ordinalEndDate);
					}					

					decimal totalCreditedAmount = Constants.NULL_DECIMAL;
					if (!dataReader.IsDBNull(ordinalTotalCreditedAmount))
					{
						totalCreditedAmount = dataReader.GetDecimal(ordinalTotalCreditedAmount);
					}

					Int32 purchaseModeID = 0;
					if (!dataReader.IsDBNull(ordinalPurchaseModeID))
					{
						purchaseModeID = dataReader.GetInt32(ordinalPurchaseModeID);
					}

					Int32 timeAdjustOverrideTypeID = 0;
					if (!dataReader.IsDBNull(ordinalTimeAdjustOverrideTypeID))
					{
						timeAdjustOverrideTypeID = dataReader.GetInt32(ordinalTimeAdjustOverrideTypeID);
					}

					if (memberPaymentID != Constants.NULL_INT)
					{
						//mpi = PaymentSA.Instance.GetMemberPaymentInfo(memberID, memberPaymentID);
					}

                    //if (mpi == null)
                    //{
                    //    mpi = new MemberPaymentInfo();
                    //    mpi.Description = string.Empty;
                    //    mpi.ResourceConstant = string.Empty;
                    //    mpi.AccountNumber = string.Empty;
                    //}
					Int32 promoID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalPromoID))
					{
						promoID = dataReader.GetInt32(ordinalPromoID);
					}

					string planIDList = Constants.NULL_STRING;
					if (!dataReader.IsDBNull(ordinalPlanIDList))
					{
						planIDList = dataReader.GetString(ordinalPlanIDList);
					}

					string legacyPaymentTypeResourceConstant = Constants.NULL_STRING;
					if (!dataReader.IsDBNull(ordinalLegacyPaymentTypeResourceConstant))
					{
						legacyPaymentTypeResourceConstant = dataReader.GetString(ordinalLegacyPaymentTypeResourceConstant);
					}

					string legacyLastFourAccountNumber = Constants.NULL_STRING;
					if (!dataReader.IsDBNull(ordinalLegacyLastFourAccountNumber))
					{
						legacyLastFourAccountNumber = dataReader.GetString(ordinalLegacyLastFourAccountNumber);
					}

					int creditCardType = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalCreditCardType))
					{
						creditCardType = dataReader.GetInt32(ordinalCreditCardType);
					}

					Int32 orderID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalOrderID))
					{
						orderID = dataReader.GetInt32(ordinalOrderID);
					}

					PaymentType paymentType = PaymentType.None;
					if (!dataReader.IsDBNull(ordinalPaymentTypeID))
					{
						paymentType = (PaymentType)Enum.ToObject(typeof(PaymentType), dataReader.GetInt32(ordinalPaymentTypeID));
					}

					MemberTran memberTran = new MemberTran(dataReader.GetInt32(ordinalMemberTranID),
						referenceMemberTranID,
						dataReader.GetInt32(ordinalMemberID),
						dataReader.GetInt32(ordinalGroupID),	
						(CurrencyType)Enum.Parse(typeof(CurrencyType), dataReader.GetInt32(ordinalCurrencyID).ToString()),	
						resourceConstant,
						(TranType)Enum.Parse(typeof(TranType), dataReader.GetInt32(ordinalTranTypeID).ToString()),	
						dataReader.GetString(ordinalTranTypeResourceConstant),
						memberSubID,
						dataReader.GetInt32(ordinalPlanID),
						dataReader.GetString(ordinalPlanResourceConstant),
						dataReader.GetInt32(ordinalDiscountID),
						memberPaymentID,
                        "",
                        "",
                        //mpi.Description,
                        //mpi.ResourceConstant,
						memberTranStatus,
						dataReader.GetString(ordinalMemberTranStatusDescription),
						dataReader.GetString(ordinalMemberTranStatusResourceConstant),
						adminMemberID,
						dataReader.GetDecimal(ordinalAmount),
						dataReader.GetInt32(ordinalDuration),
						(DurationType)Enum.Parse(typeof(DurationType), dataReader.GetInt32(ordinalDurationTypeID).ToString()),
						dataReader.GetString(ordinalDurationTypeResourceConstant),
						transactionReasonID,
						dataReader.GetDateTime(ordinalInsertDate),
						dataReader.GetDateTime(ordinalUpdateDate),
                        "",
						//mpi.AccountNumber,
						dataReader.GetString(ordinalDiscountResourceConstant),
						startDate,
						endDate,
						(dataReader.GetInt32(ordinalUsedToCreditPurchase) == 1 ? true : false),
						(PurchaseMode)Enum.Parse(typeof(PurchaseMode), purchaseModeID.ToString()),
						promoID,
						(TimeAdjustOverrideType) Enum.Parse(typeof(TimeAdjustOverrideType), timeAdjustOverrideTypeID.ToString())
						);

					memberTran.PlanIDList = planIDList;
					memberTran.LegacyPaymentTypeResourceConstant = legacyPaymentTypeResourceConstant;
					memberTran.LegacyLastFourAccountNumber = legacyLastFourAccountNumber;
					memberTran.CreditCardType = (CreditCardType)creditCardType;
					memberTran.OrderID = orderID;
					memberTranCollection.Add(memberTran);
					memberTran.PaymentType = paymentType;

					//mpi = null; 
				}

				// Get the MemberTranCredit if any and add it to the MemberTranCreditCollection in MemberTran
				if (dataReader.NextResult())
				{
					colLookupDone = false;

					MemberTran creditAppliedMemberTran = null;

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalMemberTranID = dataReader.GetOrdinal("MemberTranID");
							ordinalCreditedFromTranID = dataReader.GetOrdinal("CreditedFromTranID");
							ordinalCreditAmount = dataReader.GetOrdinal("CreditAmount");
							ordinalInsertDate = dataReader.GetOrdinal("InsertDate");

							colLookupDone = true;
						}

						creditAppliedMemberTran = memberTranCollection.FindByID(dataReader.GetInt32(ordinalMemberTranID));
						if (creditAppliedMemberTran.MemberTranCreditCollection == null)
						{
							creditAppliedMemberTran.MemberTranCreditCollection = new MemberTranCreditCollection(memberID, siteID);
						}						
						MemberTranCredit currentMemberTranCredit = new MemberTranCredit(dataReader.GetInt32(ordinalCreditedFromTranID), dataReader.GetDecimal(ordinalCreditAmount), dataReader.GetDateTime(ordinalInsertDate));						
						creditAppliedMemberTran.MemberTranCreditCollection.Add(currentMemberTranCredit);
					}
				}

				return memberTranCollection;
			}
			catch (Exception ex)
			{
				throw (new BLException("BL error occured while getting member transactions.", ex));
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		public UPSMemberTranInfo UPSGetMemberTranInfo(Int32 orderID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberTran_GetByOrderID", 0);
				command.AddParameter("@OrderID", SqlDbType.Int, ParameterDirection.Input, orderID);
				dataReader = Client.Instance.ExecuteReader(command);
				
				UPSMemberTranInfo upsMemberTranInfo = null;

				bool colLookupDone = false;
				Int32 ordinalMemberPaymentID = Constants.NULL_INT;
				Int32 ordinalMemberTranID = Constants.NULL_INT;
				Int32 ordinalGroupID = Constants.NULL_INT;
				Int32 ordinalPlanID = Constants.NULL_INT;
				Int32 ordinalPaymentTypeID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalMemberPaymentID = dataReader.GetOrdinal("MemberPaymentID");
						ordinalMemberTranID = dataReader.GetOrdinal("MemberTranID");
						ordinalGroupID = dataReader.GetOrdinal("GroupID");
						ordinalPlanID = dataReader.GetOrdinal("PlanID");
						ordinalPaymentTypeID = dataReader.GetOrdinal("PaymentTypeID");

						colLookupDone = true;
					}

					Int32 memberPaymentID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalMemberPaymentID))
					{
						memberPaymentID = dataReader.GetInt32(ordinalMemberPaymentID);
					}

					Int32 memberTranID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalMemberTranID))
					{
						memberTranID = dataReader.GetInt32(ordinalMemberTranID);
					}

					Int32 siteID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalGroupID))
					{
						siteID = dataReader.GetInt32(ordinalGroupID);
					}

					Int32 planID = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalPlanID))
					{
						planID = dataReader.GetInt32(ordinalPlanID);
					}

					Int32 paymentTypeID = 0;
					if (!dataReader.IsDBNull(ordinalPaymentTypeID))
					{
						paymentTypeID = dataReader.GetInt32(ordinalPaymentTypeID);
					}

					upsMemberTranInfo = new UPSMemberTranInfo(
						siteID,
						memberTranID,
						memberPaymentID,
						planID,
						(PaymentType)Enum.Parse(typeof(PaymentType), paymentTypeID.ToString()));
				}

				return upsMemberTranInfo;
			}
			catch (Exception ex)
			{
				throw (new BLException("BL error occured while getting UPS MemberTranInfo", ex));
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		public SaveOfferCollection GetSaveOffers(Int32 siteID)
		{
			SqlDataReader dataReader = null;

			try
			{
				SaveOfferCollection saveOfferCollection = _cache.Get(SaveOfferCollection.GetCacheKey(siteID)) as SaveOfferCollection;
			
				if (saveOfferCollection == null)
				{
					Command command = new Command("mnSystem", "dbo.up_SaveOffer_List", 0);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
					dataReader = Client.Instance.ExecuteReader(command);

					bool colLookupDone = false;
					Int32 ordinalSaveOfferID = Constants.NULL_INT;
					Int32 ordinalTransactionReasonTypeID = Constants.NULL_INT;
					Int32 ordinalPlanID = Constants.NULL_INT;
					Int32 ordinalDiscountID = Constants.NULL_INT;
					Int32 ordinalResourceConstant = Constants.NULL_INT;
					Int32 ordinalTier = Constants.NULL_INT;

					saveOfferCollection = new SaveOfferCollection(siteID);

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalSaveOfferID = dataReader.GetOrdinal("SaveOfferID");
							ordinalTransactionReasonTypeID = dataReader.GetOrdinal("TransactionReasonTypeID");
							ordinalPlanID = dataReader.GetOrdinal("PlanID");
							ordinalDiscountID = dataReader.GetOrdinal("DiscountID");
							ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
							ordinalTier = dataReader.GetOrdinal("Tier");
							colLookupDone = true;
						}

						SaveOffer saveOffer = new SaveOffer(dataReader.GetInt32(ordinalSaveOfferID),
							(TransactionReasonType)Enum.Parse(typeof(TransactionReasonType), dataReader.GetInt32(ordinalTransactionReasonTypeID).ToString()),
							!dataReader.IsDBNull(ordinalPlanID) ? dataReader.GetInt32(ordinalPlanID) : Constants.NULL_INT,
							dataReader.GetInt32(ordinalDiscountID),
							dataReader.GetString(ordinalResourceConstant),
							dataReader.GetInt32(ordinalTier));

						saveOfferCollection.Add(saveOffer);
					}

					_cache.Add(saveOfferCollection);
				}

				return saveOfferCollection;
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured while getting save offers.", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		public SiteMerchantCollection GetSiteMerchants(Int32 siteID,
			CurrencyType currencyType,
			PaymentType paymentType)
		{
			SiteMerchantCollection siteMerchantCollection = _cache.Get(SiteMerchantCollection.GetCacheKey(siteID, currencyType, paymentType)) as SiteMerchantCollection;
				
			if (siteMerchantCollection == null)
			{
				Command command = new Command("mnSystem", "dbo.up_MerchantGroup_List", 0);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)currencyType);
				command.AddParameter("@PaymentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)paymentType);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				siteMerchantCollection = new SiteMerchantCollection(siteID, currencyType, paymentType);
				for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
				{
					DataRow row = dt.Rows[rowNum];
					SiteMerchant siteMerchant = new SiteMerchant(
						Convert.ToInt32(row["ProviderID"]),
						Convert.ToInt32(row["MerchantID"]),
						row["AgentObject"].ToString());

					siteMerchantCollection.Add(siteMerchant);
				}

				_cache.Add(siteMerchantCollection);
			}
			
			return siteMerchantCollection;
		}


		private MemberTran getMemberTran(Plan plan,
			PaymentType paymentType,
			TranType tranType,
			Int32 chargeID,
			Int32 memberTranID, 
			Int32 memberID, 
			Int32 adminMemberID,
			Int32 discountID,
			Int32 siteID,
			Int32 memberPaymentID,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string addressLine2,
			string streetName,
			string houseNumber,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string accountNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc,
			string driversLicenseNumber,
			string driversLicenseState,
			string bankName,
			string bankCode,
			string bankRoutingNumber,
			Int32 checkNumber,
			BankAccountType bankAccountType)
		{
			Matchnet.Purchase.ValueObjects.Payment payment = null;
			SiteMerchant merchant = GetSiteMerchants(siteID, plan.CurrencyType, paymentType)[0];
 
			switch (paymentType)
			{
				case PaymentType.CreditCard:
					payment = new CreditCardPayment(memberID,
						siteID,
						memberPaymentID,
						firstName,
						lastName,
						phone,
						addressLine1,
						city,
						state,
						countryRegionID,
						postalCode,
						paymentType,
						plan.CurrencyType,
						accountNumber,
						expirationMonth,
						expirationYear,
						creditCardType,
						cvc,
						israeliID);
					break;

				case PaymentType.Check:
					payment = new CheckPayment(memberID,
						siteID,
						memberPaymentID,
						firstName,
						lastName,
						phone,
						addressLine1,
						city,
						state,
						countryRegionID,
						postalCode,
						paymentType,
						plan.CurrencyType,
						addressLine2,
						driversLicenseNumber,
						driversLicenseState,
						bankName,
						bankRoutingNumber,
						accountNumber,
						checkNumber,
						bankAccountType);
					break;

				default:
					throw new BLException( "Payment type not defined for MemberID: " + memberID.ToString() + " SiteID: " + siteID.ToString() );
			}
			
			return new MemberTran(
				memberTranID,
				chargeID,
				Constants.NULL_INT,
				memberID,
				siteID,
				plan.CurrencyType,
				null,
				tranType,
				null,
				Constants.NULL_INT,
				plan.PlanID,
				plan,
				null,
				discountID,
				memberPaymentID,
				payment,
				null,
				null,
				MemberTranStatus.None,
				null,
				null,
				adminMemberID,
				plan.InitialCost,
				plan.InitialDuration,
				plan.InitialDurationType,
				null,
				Constants.NULL_INT,
				DateTime.Now,
				DateTime.Now,
				accountNumber,
				null,
				paymentType,
				merchant,
				Matchnet.Duration.GetMinutes(plan.InitialDurationType, plan.InitialDuration));
		}


		private MemberTran getMemberTran(CurrencyType currencyType,
			PaymentType paymentType,
			Int32 chargeID,
			Int32 memberTranID, 
			Int32 referenceMemberTranID,
			Int32 memberID, 
			Int32 adminMemberID,
			Int32 discountID,
			Int32 siteID,
			Int32 memberPaymentID,
			Int32 planID,
			decimal amount,
			TranType tranType,
			Int32 duration,
			DurationType durationType,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			string accountNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			string cvc)
		{
			Matchnet.Purchase.ValueObjects.Payment payment = null;
			SiteMerchant merchant = GetSiteMerchants(siteID, currencyType, paymentType)[0];
 
			//hack for FT credits
			if (tranType == TranType.AdministrativeAdjustment && siteID == 101 && System.Math.Abs(amount) == 1)
			{
				merchant = new SiteMerchant(12000, 12101, "[Matchnet.PaymentProvider.LitleProvider]Matchnet.PaymentProvider.LitleProvider.LitleProvider");
			}

			payment = new CreditCardPayment(memberID,
				siteID,
				memberPaymentID,
				firstName,
				lastName,
				phone,
				addressLine1,
				city,
				state,
				countryRegionID,
				postalCode,
				paymentType,
				currencyType,
				accountNumber,
				expirationMonth,
				expirationYear,
				creditCardType,
				cvc,
				israeliID);
			
			return new MemberTran(memberTranID,
				chargeID,
				referenceMemberTranID,
				memberID,
				siteID,
				currencyType,
				null,
				tranType,
				null,
				Matchnet.Constants.NULL_INT,
				planID,
				null,
				null,
				discountID,
				memberPaymentID,
				payment,
				null,
				null,
				MemberTranStatus.None,
				null,
				null,
				adminMemberID,
				amount,
				duration,
				durationType,
				null,
				PurchaseConstants.TRANSACTION_REASON_ID_CREDIT,
				DateTime.Now,
				DateTime.Now,
				accountNumber,
				null,
				paymentType,
				merchant,
				Matchnet.Constants.NULL_INT);
		}

		
		private Int32 getPurchaseReasonID(Int32 purchaseReasonTypeID,
			Int32 sourceID)
		{
			Int32 purchaseReasonID = Constants.NULL_INT;

			if (purchaseReasonTypeID != Constants.NULL_INT)
			{
				purchaseReasonID = KeySA.Instance.GetKey(PRIMARYKEY_PURCHASEREASONID);
			}

			return purchaseReasonID;
		}


		private string getResourceConstant(int returnValue)
		{
			if ( returnValue != Constants.RETURN_OK )
			{
				if ( ! Enum.IsDefined(typeof(ResourceConstants), returnValue) )
				{
					throw new BLException("BL error.  Return value does not map to a resource constant enumeration.");
				}

				return Enum.GetName(typeof(ResourceConstants), returnValue);
			}
			else
			{
				return Constants.NULL_STRING;
			}
		}


		private void saveMemberTranToReportDB(MemberTran memberTran,
			string responseCode,
			Int32 purchaseReasonTypeID,
			Int32 sourceID,
			Int32 promotionID,
			Int32 promoterID,
			Int32 depth1RegionID,
			string luggage)
		{
			Command command = new Command("mnSubscription", "dbo.up_MemberTranEx_Save", 0);
			command.AddParameter("@MemberTranExID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
			if ( memberTran.ReferenceMemberTranID != Constants.NULL_INT )
			{
				command.AddParameter("@ReferenceMemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.ReferenceMemberTranID);
			}

			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberID);
			if ( memberTran.SiteID != Constants.NULL_INT )
			{
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
			}
			if ( memberTran.ResourceConstant != Constants.NULL_STRING )
			{
				command.AddParameter("@Resourceconstant", SqlDbType.VarChar, ParameterDirection.Input, memberTran.ResourceConstant);
			}
			if ( responseCode != Constants.NULL_STRING )
			{
				command.AddParameter("@ResponseCode", SqlDbType.NVarChar, ParameterDirection.Input, responseCode);
			}
			if ( memberTran.CurrencyType != CurrencyType.None )
			{
				command.AddParameter("@CurrencyID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.CurrencyType);
			}
			if ( memberTran.PaymentType != PaymentType.None )
			{
				command.AddParameter("@PaymentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.PaymentType);
			}

			command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.TranType);
			if ( memberTran.PaymentType == PaymentType.CreditCard )
			{
				command.AddParameter("@CreditCardTypeID", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTran.Payment).CreditCardType);
			}
			if ( memberTran.MemberSubID != Constants.NULL_INT )
			{
				command.AddParameter("@MemberSubID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberSubID);
			}
			if ( memberTran.PlanID != Constants.NULL_INT )
			{
				command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, memberTran.PlanID);
			}
			if ( memberTran.DiscountID != Constants.NULL_INT )
			{
				command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, memberTran.DiscountID);
			}
			if ( memberTran.MemberPaymentID != Constants.NULL_INT )
			{
				command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberPaymentID);
			}
			if ( memberTran.MemberTranStatus != MemberTranStatus.None )
			{
				command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.MemberTranStatus);
			}
			if ( memberTran.AdminMemberID != Constants.NULL_INT )
			{
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.AdminMemberID);
			}
			if ( memberTran.Amount != Constants.NULL_DECIMAL )
			{
				command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, memberTran.Amount);
			}
			if ( memberTran.Duration != Constants.NULL_INT )
			{
				command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, memberTran.Duration);
			}
			command.AddParameter("@DurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)memberTran.DurationType);
			if ( memberTran.TransactionReasonID != Constants.NULL_INT )
			{
				command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, memberTran.TransactionReasonID);
			}
			if ( memberTran.InsertDate != DateTime.MinValue )
			{
				command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, memberTran.InsertDate);
			}
			if ( memberTran.UpdateDate != DateTime.MinValue )
			{
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, memberTran.UpdateDate);
			}
			if ( purchaseReasonTypeID != Constants.NULL_INT )
			{
				command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
			}
			if ( sourceID != Constants.NULL_INT )
			{
				command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
			}
			if ( promotionID != Constants.NULL_INT )
			{
				command.AddParameter("@PromotionID", SqlDbType.Int, ParameterDirection.Input, promotionID);
			}
			if ( promoterID != Constants.NULL_INT )
			{
				command.AddParameter("@PromoterID", SqlDbType.Int, ParameterDirection.Input, promoterID);
			}
			if ( depth1RegionID != Constants.NULL_INT )
			{
				command.AddParameter("@Depth1RegionID", SqlDbType.Int, ParameterDirection.Input, depth1RegionID);
			}
			if ( luggage != Constants.NULL_STRING )
			{
				command.AddParameter("@Luggage", SqlDbType.VarChar, ParameterDirection.Input, luggage);
			}

			Client.Instance.ExecuteAsyncWrite(command);
		}

		
		private void saveToAdminDB(Int32 memberID,
			string phone)
		{
			Command command = new Command("mnAdmin", "dbo.up_AdminPaymentAttribute_Save", 0);
			command.AddParameter("@AdminPaymentAttributeID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(PRIMARYKEY_ADMINPAYMENTATTRIBUTE_ID));
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@PhoneNumber", SqlDbType.VarChar, ParameterDirection.Input, phone);
			Client.Instance.ExecuteAsyncWrite(command);
		}


		private void saveToLookUpDB(Int32 memberID,
			string creditCardNumber)
		{
			Command command = new Command("mnLookup", "dbo.up_mnLookup_Facility_Save", 0);
			command.AddParameter("@CSLookUpID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(PRIMARYKEY_CSLOOKUP_ID));
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@CreditCardNumber", SqlDbType.NVarChar, ParameterDirection.Input, Crypto.Hash(creditCardNumber.Trim() + PurchaseConstants.SOOPER_HASH));
			Client.Instance.ExecuteAsyncWrite(command);
		}

		public Int32 beginCheckPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 purchaseReasonID,
			Int32 purchaseReasonTypeID,
			Int32 sourceID,
			Int32 conversionMemberID,
			Int32 memberTranID,
			Int32 memberPaymentID,
			MemberTran memberTran,
			string ipAddress,
			Int32 promoID, 
			bool reusePreviousPayment)
		{
			Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_CheckingAccount", 0);
		
			if (adminMemberID != Constants.NULL_INT)
			{
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			}

			if (discountID == Constants.NULL_INT)
			{
				discountID = 0;
			}

			if (conversionMemberID != Constants.NULL_INT)
			{
				command.AddParameter("@ConversionMemberID", SqlDbType.Int, ParameterDirection.Input, conversionMemberID);
			}

			if (sourceID != Constants.NULL_INT)
			{
				command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
			}

			if (purchaseReasonTypeID != Constants.NULL_INT)
			{
				command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
			}

			if (purchaseReasonID != Constants.NULL_INT)
			{
				command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonID);
			}

			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
			command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
			command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
			command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);
			

			//TL 08082008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
			if (memberTran.CreditAmount != Constants.NULL_DECIMAL && memberTran.CreditAmount > 0)
				command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, memberTran.CreditAmount);

			command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.PurchaseMode);
			
			MemberSub memberSub = PurchaseBL.GetSubscription(memberTran.MemberID, memberTran.SiteID);
			DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
			DateTime startDate = MemberTran.DetermineTransactionStartDate(memberTran, updateDate, memberSub);
			DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, memberTran, memberSub);
			if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
			{
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
				command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
				command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
			}

			// Supporting PromoID entry for check purchases too now
			if (promoID != Constants.NULL_INT)
			{
				command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promoID);
			}

			command.AddParameter("@EZBuy", SqlDbType.Bit, ParameterDirection.Input, reusePreviousPayment);
			
			SyncWriter sw = new SyncWriter();
			Exception swEx;
			Int32 returnValue = sw.Execute(command,
				new MessageSendTask[]{new MessageSendTask(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, memberTran)},
				out swEx);
			sw.Dispose();

			if (swEx != null)
			{
				throw swEx;
			}

			return returnValue;
		}

		
		private Int32 beginCreditCardPurchase(Int32 memberID,
			Int32 adminMemberID,
			Int32 planID,
			Int32 discountID,
			Int32 siteID,
			Int32 conversionMemberID,
			Int32 purchaseReasonID,
			Int32 purchaseReasonTypeID,
			Int32 sourceID,
			Int32 memberTranID,
			Int32 memberPaymentID,
			MemberTranStatus memberTranStatus,
			MemberTran memberTran,
			string ipAddress,
			CreditCardType creditCardType,
			Int32 promoID,
			bool reusePreviousPayment)
		{
			Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_CreditCard", 0);
			if (adminMemberID != Constants.NULL_INT)
			{
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			}

			if (discountID == Constants.NULL_INT)
			{
				discountID = 0;
			}

			if (conversionMemberID != Constants.NULL_INT)
			{
				command.AddParameter("@ConversionMemberID", SqlDbType.Int, ParameterDirection.Input, conversionMemberID);
			}

			if (sourceID != Constants.NULL_INT)
			{
				command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
			}
			else
			{
				command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, 0);
			}

			System.Diagnostics.Trace.WriteLine("__purchaseReasonTypeID: " + purchaseReasonTypeID.ToString());
			if (purchaseReasonTypeID != Constants.NULL_INT)
			{
				command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
			}

			System.Diagnostics.Trace.WriteLine("__purchaseReasonID: " + purchaseReasonID.ToString());
			if (purchaseReasonID != Constants.NULL_INT)
			{
				command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonID);
			}

			if (ipAddress != Constants.NULL_STRING)
			{
				command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
			}
			if (promoID != Constants.NULL_INT)
			{
				command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promoID);
			}
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
			command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
			command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranStatus);
			command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (Int32)creditCardType);
			command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);
			

			//TL 08082008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
			if (memberTran.CreditAmount != Constants.NULL_DECIMAL && memberTran.CreditAmount > 0)
				command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, memberTran.CreditAmount);

			command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.PurchaseMode);

			MemberSub memberSub = PurchaseBL.GetSubscription(memberTran.MemberID, memberTran.SiteID);
			DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
			DateTime startDate = MemberTran.DetermineTransactionStartDate(memberTran, updateDate, memberSub);
			DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, memberTran, memberSub);
			if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
			{
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
				command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
				command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
			}

			command.AddParameter("@EZBuy", SqlDbType.Bit, ParameterDirection.Input, reusePreviousPayment);

			SyncWriter sw = new SyncWriter();
			Exception swEx;
			Int32 returnValue = sw.Execute(command,
				new MessageSendTask[]{new MessageSendTask(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, memberTran)},
				out swEx);
			sw.Dispose();

			if (swEx != null)
			{
				throw swEx;
			}

			return returnValue;
		}


		public Int32 beginRenewalPurchase(Int32 memberID, 
			Int32 adminMemberID, 
			Int32 planID, 
			Int32 discountID, 
			Int32 siteID, 
			Int32 conversionMemberID,
			Int32 purchaseReasonID,
			Int32 purchaseReasonTypeID,
			Int32 sourceID,
			Int32 memberTranID,
			Int32 memberPaymentID,
			MemberTranStatus memberTranStatus,
			MemberTran mt,
			string ipAddress,
			CreditCardType creditCardType,
			Int32 promoID)
		{
			Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_MemberPayment", 0);

			if ( discountID == Constants.NULL_INT )
			{
				discountID = 0;
			}

			if ( adminMemberID != Constants.NULL_INT )
			{
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			}

			if ( conversionMemberID != Constants.NULL_INT )
			{
				command.AddParameter("@ConversionMemberID", SqlDbType.Int, ParameterDirection.Input, conversionMemberID);
			}

			if ( sourceID != Constants.NULL_INT )
			{
				command.AddParameter("@SourceID", SqlDbType.Int, ParameterDirection.Input, sourceID);
			}

			if ( purchaseReasonTypeID != Constants.NULL_INT )
			{
				command.AddParameter("@PurchaseReasonTypeID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonTypeID);
			}

			if ( purchaseReasonID != Constants.NULL_INT )
			{
				command.AddParameter("@PurchaseReasonID", SqlDbType.Int, ParameterDirection.Input, purchaseReasonID);
			}

			if (ipAddress != Constants.NULL_STRING)
			{
				command.AddParameter("@IPAddress", SqlDbType.VarChar, ParameterDirection.Input, ipAddress);
			}

			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)TranType.InitialBuy);
			command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
			command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, discountID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
			command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranStatus);
			command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (Int32)creditCardType);
			command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None).EmailAddress);

			//TL 08082008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
			if (mt.CreditAmount != Constants.NULL_DECIMAL && mt.CreditAmount > 0)
				command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, mt.CreditAmount);

			command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)mt.PurchaseMode);

			MemberSub memberSub = PurchaseBL.GetSubscription(mt.MemberID, mt.SiteID);
			DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
			DateTime startDate = MemberTran.DetermineTransactionStartDate(mt, updateDate, memberSub);
			DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, mt, memberSub);
			if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
			{
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
				command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
				command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
			}

			// Renewal purchase also needs to support PromoID
			if (promoID != Constants.NULL_INT)
			{
				command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promoID);	
			}

			SyncWriter sw = new SyncWriter();
			Exception swEx;
			Int32 returnValue = sw.Execute(command,
				new MessageSendTask[]{new MessageSendTask(PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND, mt)},
				out swEx);
			sw.Dispose();

			if (swEx != null)
			{
				throw swEx;
			}

			return returnValue;
		}

		
		private Int32 endSubscription(Int32 memberID, 
			Int32 adminMemberID,
			Int32 siteID, 
			Int32 transactionReasonID,
			Int32 memberTranID)
		{
			DateTime updateDate = DateTime.Now;

			SyncWriter sw = new SyncWriter();
			Exception swEx;
			Command command;

			command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Terminate", 0);
			
			if ( adminMemberID != Constants.NULL_INT )
			{
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			}
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, transactionReasonID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
			command.AddParameter("@SubscriptionExpirationDate", SqlDbType.DateTime, ParameterDirection.Output);

			Int32 returnValue = sw.Execute(command,
				out swEx);
			sw.Dispose();

			if (swEx != null)
			{
				throw swEx;
			}

			if (command.Parameters["@SubscriptionExpirationDate"].ParameterValue != DBNull.Value)
			{
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID,
					MemberLoadFlags.None);

				member.SetAttributeDate(Matchnet.Constants.NULL_INT,
					siteID,
					Matchnet.Constants.NULL_INT,
					PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
					(DateTime)command.Parameters["@SubscriptionExpirationDate"].ParameterValue);

				member.SetAttributeInt(Constants.NULL_INT,
					siteID,
					Constants.NULL_INT,
					PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_STATUS,
					Convert.ToInt16(PurchaseBL.GetMemberSubStatus(member.MemberID, siteID, (DateTime)command.Parameters["@SubscriptionExpirationDate"].ParameterValue).Status));

				MemberSaveResult result = MemberSA.Instance.SaveMember(member);

				if(result.SaveStatus == MemberSaveStatusType.Success)
				{
					// If this member is a Messmo user, we have to let Messmo know of this subscription end date change.
					int communityID = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).Community.CommunityID;
					bool siteMessmoEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_UI_ENABLED",
						communityID, siteID));

					if(siteMessmoEnabled)
					{
						int messmoCapable = member.GetAttributeInt(communityID, Constants.NULL_INT,	Constants.NULL_INT, "MessmoCapable", 0);

						if(messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending ||
							messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable)
						{
							int languageID = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).LanguageID;
							string messmoSubID = member.GetAttributeText(communityID, Constants.NULL_INT, Constants.NULL_INT, languageID,
								"MessmoSubscriberID", string.Empty);
							DateTime renewDate = (DateTime)command.Parameters["@SubscriptionExpirationDate"].ParameterValue;
							
							Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoUserTransStatusRequestBySiteID(siteID,
								DateTime.Now, messmoSubID, Matchnet.HTTPMessaging.Constants.MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL, renewDate);
						}
					}
				}
			}

			return returnValue;
		}

		
		private int endALaCarteOnly(int memberID, 
			int adminMemberID,
			int siteID, 
			int memberTranID,
			int planID)
		{
			DateTime updateDate = DateTime.Now;

			SyncWriter sw = new SyncWriter();
			Exception swEx;
			Command command;

			//this is new for UPS Integration to terminate an a la carte item
			command = new Command("mnSubscription", "dbo.up_MemberSubExtended_Save_Terminate", 0);
			
			if ( adminMemberID != Constants.NULL_INT )
			{
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
			}
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);

			int returnValue = sw.Execute(command, out swEx);
			sw.Dispose();

			if (swEx != null)
			{
				throw swEx;
			}

			return returnValue;
		}

		
		public CreditMaximum GetCreditMaximum(Int32 memberTranID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnSubscription", "dbo.up_MemberTran_List_Credit", 0);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				dataReader = Client.Instance.ExecuteReader(command);

				CreditMaximum creditMaximum = new CreditMaximum();

				while (dataReader.Read())
				{
					creditMaximum.MaxAmount = dataReader.GetDecimal(dataReader.GetOrdinal("MaxAmount"));
					creditMaximum.MaxMinutes = dataReader.GetInt32(dataReader.GetOrdinal("MaxMinutes"));
					return creditMaximum;
				}

				throw new Exception("MemberTran not found.");
			}
			catch (Exception ex)
			{
				throw new BLException("GetCreditMaximum() error (memberTranID: " + memberTranID.ToString() + ").", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}
		
		public SubscriptionResult RemovePaymentInfo(Int32 memberID, 
			Int32 siteID)
		{
			try
			{
				SubscriptionResult subRes = new SubscriptionResult();

				Command command = new Command("mnSubscription", "dbo.up_MemberSub_RemovePayment", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

				SyncWriter sw = new SyncWriter();
				Exception ex;
				Int32 returnValue = sw.Execute(command, out ex);
				sw.Dispose();

				if (ex != null)
				{
					throw ex;
				}

				subRes.ReturnValue = returnValue;
				subRes.ResourceConstant = getResourceConstant(returnValue);

				return subRes;
			}
			catch (Exception ex)
			{
				throw new BLException("RemovePaymentInfo() error (memberID: " + memberID.ToString() + ", siteID: " + siteID.ToString() + ").", ex);
			}
		}


		public void UpdateMemberPayment(Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType)
		{

            //PaymentSA.Instance.UpdateMemberPayment(KeySA.Instance.GetKey(PRIMARYKEY_MEMBERPAYMENTID),
            //    memberID,
            //    siteID,
            //    firstName,
            //    lastName,
            //    phone,
            //    addressLine1,
            //    city,
            //    state,
            //    countryRegionID,
            //    postalCode,
            //    creditCardNumber,
            //    expirationMonth,
            //    expirationYear,
            //    creditCardType);
		}
		
		/// <summary>
		/// This is an overload of above method that accepts a new parameter of type of TranType, 
		/// which in above overload of this method is always considerred AdministrativeAdjustment.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="duration"></param>
		/// <param name="durationType"></param>
		/// <param name="adjustmentType"></param>
		/// <param name="transactionReasonID"></param>
		/// <param name="reOpenFlag"></param>
		/// <param name="planID"></param>
		/// <returns></returns>
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID)
		{
			return UpdateSubscription(memberID, 
				siteID, 
				adminMemberID, 
				duration, 
				durationType, 
				adjustmentType,
				transactionReasonID, 
				reOpenFlag, 
				planID,
				null);
		}

		/// <summary>
		/// This is an overload of above method that accepts a new parameter of type of targetBrand, 
		/// which in above overload of this method is always an admin adjustment that requires checking
		/// to see whether premium services is activated  
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="duration"></param>
		/// <param name="durationType"></param>
		/// <param name="adjustmentType"></param>
		/// <param name="transactionReasonID"></param>
		/// <param name="reOpenFlag"></param>
		/// <param name="planID"></param>
		/// <param name="targetBrand"></param>
		/// <returns></returns>
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand)
		{
			return UpdateSubscription(memberID, 
				siteID, 
				adminMemberID, 
				duration, 
				durationType, 
				adjustmentType,
				transactionReasonID, 
				reOpenFlag, 
				planID,
				targetBrand,
				TimeAdjustOverrideType.None); //no override for updating premium service even for non-premium plans
		}

		/// <summary>
		/// This is an overload of above method for updating a member's subscription information (e.g. Web Admin tool).  It includes a new parameter
		/// to specify whether you would like to override the updating of premium service expiration dates regardless of the plan type.  
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="duration"></param>
		/// <param name="durationType"></param>
		/// <param name="adjustmentType"></param>
		/// <param name="transactionReasonID"></param>
		/// <param name="reOpenFlag"></param>
		/// <param name="planID"></param>
		/// <param name="targetBrand"></param>
		/// <param name="timeAdjustOverrideType">A value other than "None" indicates an override for updating premium service dates regardless of the plan type.</param>
		/// <returns></returns>
		public Int32 UpdateSubscription(Int32 memberID, 
			Int32 siteID, 
			Int32 adminMemberID, 
			Int32 duration, 
			DurationType durationType, 
			TranType adjustmentType,
			Int32 transactionReasonID, 
			bool reOpenFlag, 
			Int32 planID,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
			TimeAdjustOverrideType timeAdjustOverrideType)
		{
			try
			{
				Int32 memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);
				Int32 memberSubID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERSUBID);

				Command command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Adjust", 0);

				if ( planID != Constants.NULL_INT )
				{
					command.AddParameter("@UpdatedPlanID", SqlDbType.Int, ParameterDirection.Input, planID);
				}
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
				command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, duration);
				command.AddParameter("@DurationTypeID", SqlDbType.Int, ParameterDirection.Input, (int)durationType);
				command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)adjustmentType);
				command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, transactionReasonID);
				command.AddParameter("@ReOpenFlag", SqlDbType.Bit, ParameterDirection.Input, reOpenFlag);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				command.AddParameter("@MemberSubID", SqlDbType.Int, ParameterDirection.Input, memberSubID);
				command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);
				
				//09162008 TL - Creating a temporary MemberTran to calculate start/end dates
				MemberTran mt = new MemberTran();
				mt.TranType = adjustmentType;
				mt.Duration = duration;
				mt.DurationType = durationType;

				//09162008 TL - Passing in params to insert into new MemberTranExtended table
				DateTime updateDate = DateTime.Now; //passing in update date which is used internally in proc to determine renew date, in order for it to sync up with our member tran extended end date
				MemberSub memberSub = PurchaseBL.GetSubscription(memberID, siteID);
				DateTime startDate = MemberTran.DetermineTransactionStartDate(mt, updateDate, memberSub);
				DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, mt, memberSub);
				if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
				{
					command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
					command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
					command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
				}

				command.AddParameter("@TimeAdjustOverrideTypeID", SqlDbType.Int, ParameterDirection.Input, (int)timeAdjustOverrideType);
				command.AddParameter("@RenewDate", SqlDbType.SmallDateTime, ParameterDirection.Output);

				SyncWriter sw = new SyncWriter();
				Exception ex;
				Int32 returnValue = sw.Execute(command, out ex);
				sw.Dispose();

				if (ex != null)
				{
					throw ex;
				}

				//get newly created member transaction
				mt = GetMemberTransactions(memberID, siteID).FindByID(memberTranID);

				if (returnValue == Constants.RETURN_OK)
				{
					if (mt != null)
					{
						saveMemberTranToReportDB(mt,
							Constants.NULL_STRING,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_STRING);
					}

					//update member attributes
					DateTime renewDate = DateTime.MinValue;
					if (command.Parameters["@RenewDate"].ParameterValue != DBNull.Value)
					{
						renewDate = (DateTime)command.Parameters["@RenewDate"].ParameterValue;
					}
					
					//DateTime renewDate = GetSubscription(memberID, siteID).RenewDate;

					if (renewDate != DateTime.MinValue)
					{
						Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
						//member.SetAttributeDate(Constants.NULL_INT, siteID, Constants.NULL_INT, PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE, renewDate );

						//update member standard subscription
						member.SetAttributeDate(Constants.NULL_INT,
							siteID,
							Constants.NULL_INT,
							PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
							renewDate);

						member.SetAttributeInt(Constants.NULL_INT,
							siteID,
							Constants.NULL_INT,
							PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_STATUS,
							Convert.ToInt16(PurchaseBL.GetMemberSubStatus(member.MemberID, siteID, renewDate).Status));
						
						MemberSaveResult result = MemberSA.Instance.SaveMember(member);
						if (result.SaveStatus != MemberSaveStatusType.Success)
						{
							throw new BLException("Unable to save subscription expiration attribute for MemberID " + mt.MemberID.ToString());
						}

						// If this member is a Messmo user, we have to let Messmo know of this subscription end date change.
						int communityID = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).Community.CommunityID;
						bool siteMessmoEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_UI_ENABLED",
							communityID, siteID));

						if(siteMessmoEnabled)
						{
							int messmoCapable = member.GetAttributeInt(communityID, Constants.NULL_INT,	Constants.NULL_INT, "MessmoCapable", 0);

							if(messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending ||
								messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable)
							{
								int languageID = BrandConfigSA.Instance.GetBrandsBySite(siteID).GetSite(siteID).LanguageID;
								string messmoSubID = member.GetAttributeText(communityID, Constants.NULL_INT, Constants.NULL_INT, languageID,
									"MessmoSubscriberID", string.Empty);
							
								Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoUserTransStatusRequestBySiteID(siteID,
									DateTime.Now, messmoSubID, Matchnet.HTTPMessaging.Constants.MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL, renewDate);
							}
						}

						//update member premium subscription
						PremiumServiceUtil.setPremiumServiceMember(MemberTranStatus.Success, mt, targetBrand, DateTime.MinValue, timeAdjustOverrideType); 

					}
				}

				return returnValue;
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured while updating subscription.", ex);
			}
		}

		/// <summary>
		/// This method is similar to UpdateSubscription but for a la carte
		/// and will primarily be used initially to reopen auto-renewal for a la carte items.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="adjustmentType"></param>
		/// <param name="transactionReasonID"></param>
		/// <param name="reOpenFlag"></param>
		/// <param name="planID">this represents the new planID or existing planID to be updated</param>
		/// <param name="planIDToReplace">this represents an existing planID that should be removed and replaced with the new PlanID</param>
		/// <param name="targetBrand"></param>
		/// <param name="insertTransactionRecordFlag">this flag indicates whether this update should be recorded with a new member transaction record</param>
		/// <returns></returns>
		public int UpdateALaCarteOnly(int memberID, 
			int siteID, 
			int adminMemberID,  
			TranType adjustmentType,
			int transactionReasonID, 
			bool reOpenFlag, 
			int planID,
			int planIDToReplace,
			Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand,
			bool insertTransactionRecordFlag)
		{
			try
			{
				int memberTranID = KeySA.Instance.GetKey(PRIMARYKEY_MEMBERTRANID);

				Command command = new Command("mnSubscription", "dbo.up_MemberSubExtended_Save_Adjust", 0);

				command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, planID);
				if (planIDToReplace != Constants.NULL_INT)
				{
					command.AddParameter("@PlanIDToReplace", SqlDbType.Int, ParameterDirection.Input, planIDToReplace);
				}
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
				command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)adjustmentType);
				command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, transactionReasonID);
				command.AddParameter("@ReOpenFlag", SqlDbType.Bit, ParameterDirection.Input, reOpenFlag);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
				command.AddParameter("@InsertMemberTran", SqlDbType.Int, ParameterDirection.Input, insertTransactionRecordFlag);
				command.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue);

				SyncWriter sw = new SyncWriter();
				Exception ex;
				Int32 returnValue = sw.Execute(command, out ex);
				sw.Dispose();

				if (ex != null)
				{
					throw ex;
				}

				//get newly created member transaction
				MemberTran mt = GetMemberTransactions(memberID, siteID).FindByID(memberTranID);

				if (returnValue == Constants.RETURN_OK)
				{
					if (mt != null)
					{
						saveMemberTranToReportDB(mt,
							Constants.NULL_STRING,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_INT,
							Constants.NULL_STRING);
					}
				}

				return returnValue;
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured in UpdateALaCarteOnly(); PlanID:" + planID.ToString() + ",MemberID:" + memberID.ToString(), ex);
			}
		}
		

		public bool RenderCVC(Int32 siteID,
			CurrencyType currencyType,
			PaymentType paymentType)
		{
			try
			{
				SiteMerchantCollection siteMerchantCollection = GetSiteMerchants(siteID, currencyType, paymentType);
				
				if ( siteMerchantCollection != null )
				{	// I added Litle and we should remove Jetpay after Litle is live; this way we support jdate on either provider for now.
					if ( siteMerchantCollection.FindByID(PurchaseConstants.GLOBAL_COLLECT_PROVIDER_ID) != null ||
						siteMerchantCollection.FindByID(PurchaseConstants.TRANZILA_PROVIDER_ID) != null ||
						( siteMerchantCollection.FindByID(PurchaseConstants.JETPAY_PROVIDER_ID) != null && 
						siteID != PurchaseConstants.JD_SITE_ID ) || 
						(siteMerchantCollection.FindByID(PurchaseConstants.LITLE_PROVIDER_ID) != null) || 
						(siteMerchantCollection.FindByID(PurchaseConstants.OPTIMAL_PAYMENTS_PROVIDER_ID) !=null && 
						siteID != PurchaseConstants.AS_SITE_ID))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				throw new BLException("BL error occured while determining CVC rendering status.", ex);
			}
		}


		public void SaveComplete(MemberTranResult memberTranResult)
		{
			try
			{
				MessageQueue queue = new MessageQueue(PurchaseConstants.QUEUEPATH_PURCHASE_INBOUND);
				queue.Formatter = new BinaryMessageFormatter();
				queue.Send(memberTranResult, MessageQueueTransactionType.Single);
			}
			catch (Exception ex)
			{
				throw new BLException("SaveComplete() error (memberTranID: " + memberTranResult.MemberTran.MemberTranID.ToString() + ").", ex);
			}
		}

		#region Encore Processing
		/// <summary>
		/// A hole to call Payment to generate the Encore Sales File
		/// </summary>
		/// <returns></returns>
		public int GenerateEncryptedSalesFile()
        {
            //12282011 TL - Deprecated old Encore related DB Tables, if we ever implement
            //Encore or related, it will require a new design and DB process
            return Constants.NULL_INT;

			try
			{
				//09152009 TL - Encore Integration with Unified Purchase System
				if (Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ENCORE_ENABLED").ToLower() == "true")
				{
					string tickeyKey = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPARKWS_SUBSCRIPTION_INTERNAL_ACCESS_KEY");
					string sparkWSURL = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPARKWS_SUBSCRIPTION_URL");
                    SparkWS.Subscription.Subscription sparkWSSubscription = new Matchnet.Purchase.BusinessLogic.SparkWS.Subscription.Subscription();
                    sparkWSSubscription.Url = sparkWSURL;
					return sparkWSSubscription.GenerateEncryptedSalesFile(tickeyKey);
				}
				else
				{
                    return Constants.NULL_INT;
                    //return PaymentSA.Instance.GenerateEncryptedSalesFile();
				}
			}
			catch (Exception ex)
			{
				throw new BLException("GenerateEncryptedSalesFile() error", ex);
			}
		}
		/// <summary>
		/// A hole to call Payment to process the Encore Membership File
		/// </summary>
		public void ProcessMembershipFile()
		{
            //12282011 TL - Deprecated old Encore related DB Tables, if we ever implement
            //Encore or related, it will require a new design and DB process
            return;

			SqlDataReader dataReader = null;
			DateTime SQL_MIN_DATE = new DateTime(1900, 1, 1);
			try
			{
				Command command = new Command("mnFileExchange", "dbo.up_GetMembershipEntries", 0);
				dataReader = Client.Instance.ExecuteReader(command);

				#region Column Ordinals
				bool colLookupDone = false;
				int ordinalEncoreMembershipID = Constants.NULL_INT;
				int ordinalVendorVariableField = Constants.NULL_INT;
				int ordinalEncoreMembershipStartDate= Constants.NULL_INT;
				int ordinalEncoreMembershipEndDate= Constants.NULL_INT;
				int ordinalMailCode= Constants.NULL_INT;
				int ordinalInsertDate= Constants.NULL_INT;
				#endregion

				bool eligibleForRebateCredit = false;
				int memberID = Constants.NULL_INT;
				int brandID = Constants.NULL_INT;
				MemberTran memberTran = null;
				string memberIDPlusBrandID = string.Empty;

				while(dataReader.Read())
				{
					#region column lookup
					if(!colLookupDone)
					{
						ordinalEncoreMembershipID = dataReader.GetOrdinal("MembershipID");
						ordinalVendorVariableField = dataReader.GetOrdinal("VendorVariableField");
						ordinalEncoreMembershipStartDate = dataReader.GetOrdinal("MembershipStartDate");
						ordinalEncoreMembershipEndDate= dataReader.GetOrdinal("MembershipEndDate");
						ordinalMailCode = dataReader.GetOrdinal("MailCode");
						ordinalInsertDate = dataReader.GetOrdinal("InsertDate");
							
					}
					#endregion

					// Get the memberID, then check for $5 credit eligibility before anything else
					eligibleForRebateCredit = false;
					memberIDPlusBrandID = dataReader.GetString(ordinalVendorVariableField);
					string[] splits = memberIDPlusBrandID.Trim().Split('_');
					
					if(splits.Length == 2)
					{
						if(IntTryParse(splits[0], out memberID) && IntTryParse(splits[1], out brandID))
						{
							if(Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_REBATE_ENABLE")))
							{
								// eligibility should be determined before we make updates to the member attributes
								eligibleForRebateCredit = IsEligibleForRebateCredit(memberID, brandID,
									dataReader.GetDateTime(ordinalEncoreMembershipStartDate), out memberTran, false);
							}
						
							// Do all the member attributes updates regardless of the $5 credit eligibility
							//************************************************************************************************
							Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, 
								Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

							if(member != null)
							{
								Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

								// update the Encore member ID if member attr value is null or different
								if(!dataReader.IsDBNull(ordinalEncoreMembershipID))
								{
									int encoreMembershipID = dataReader.GetInt32(ordinalEncoreMembershipID);
									if(member.GetAttributeText(brand, "EncoreMembershipID", string.Empty) != encoreMembershipID.ToString())
									{
										member.SetAttributeText(brand, "EncoreMembershipID", encoreMembershipID.ToString(), Member.ValueObjects.TextStatusType.None);
									}
								}

								if(!dataReader.IsDBNull(ordinalEncoreMembershipStartDate))
								{
									DateTime attrStartDate = member.GetAttributeDate(brand, "JRewardStartDate", SQL_MIN_DATE);
									DateTime mbFileStartDate = dataReader.GetDateTime(ordinalEncoreMembershipStartDate);

									if(attrStartDate == SQL_MIN_DATE)
									{
										// member attribute not set yet. only set the attribute if it's not Encore's null value SQL_MIN_DATE
										if(mbFileStartDate != SQL_MIN_DATE)
										{
											member.SetAttributeDate(brand, "JRewardStartDate", mbFileStartDate);
										}
									}
									else
									{
										// member attribute already set
										// if Encore sent us their NULL value, remove it from the our DB
										if(mbFileStartDate == SQL_MIN_DATE)
										{
											// this will delete the attribute from the database
											member.SetAttributeDate(brand, "JRewardStartDate", DateTime.MinValue);
										}
										else
										{
											// Encore sent us a valid value.  only update if it is different than the current attribute value
											if(attrStartDate != mbFileStartDate)
											{
												member.SetAttributeDate(brand, "JRewardStartDate", mbFileStartDate);
											}
										}
									}
									
								}

								if(!dataReader.IsDBNull(ordinalEncoreMembershipEndDate))
								{
									DateTime attrEndDate = member.GetAttributeDate(brand, "JRewardEndDate", SQL_MIN_DATE);
									DateTime mbFileEndDate = dataReader.GetDateTime(ordinalEncoreMembershipEndDate);

									if(attrEndDate == SQL_MIN_DATE)
									{
										// member attribute not set yet. only set the attribute if it's not Encore's null value SQL_MIN_DATE
										if(mbFileEndDate != SQL_MIN_DATE)
										{
											member.SetAttributeDate(brand, "JRewardEndDate", mbFileEndDate);
										}
									}
									else
									{
										// member attribute already set
										// if Encore sent us their NULL value, remove it from the our DB
										if(mbFileEndDate == SQL_MIN_DATE)
										{
											// this will delete the attribute from the database
											member.SetAttributeDate(brand, "JRewardEndDate", DateTime.MinValue);
										}
										else
										{
											// Encore sent us a valid value.  only update if it is different than the current attribute value
											if(attrEndDate != mbFileEndDate)
											{
												member.SetAttributeDate(brand, "JRewardEndDate", mbFileEndDate);
											}
										}
									}
								
								}

								Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
							}

							// give user $5 credit if eligible. this sits here after the member attr updates to ensure that we only give
							// $5 credit once. if we give credit first and then the member attribute updates fail, then we could possibly
							// repeat this.
							if(eligibleForRebateCredit)
							{
								GiveRebateCredit(memberTran);
							}
						}
					}
					
					// Update the record as processed
					Command updateCmd = new Command("mnFileExchange", "dbo.up_UpdateMembershipProcessed", 0);
					updateCmd.AddParameter("@IsProcessed", SqlDbType.Bit, ParameterDirection.Input, 1);
					updateCmd.AddParameter("@MembershipID", SqlDbType.Int, ParameterDirection.Input, dataReader.GetInt32(ordinalEncoreMembershipID));

					Client.Instance.ExecuteAsyncWrite(updateCmd);
				}

			}
			catch(Exception ex)
			{
				throw new BLException("ProcessMembershipFile() error", ex);
			}
			finally
			{
				if(dataReader != null)
				{
					dataReader.Close();
				}
			}
		}
		/// <summary>
		/// This hits the database directly! Do not use if the load is going to be heavy.
		/// Checks to see if the member has a valid CC on file that was used for the last member transaction.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public bool HasValidCCOnFile(int memberID, int brandID)
		{
			MemberTran mt = null;
			try
			{
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

				mt = GetLastValidCCMemberTran(memberID, brand);
			}
			catch(Exception ex)
			{
				throw new BLException(string.Format("HasValidCCOnFile() error. MemberID:{0} BrandID: {1}", memberID.ToString(),
					brandID.ToString(), ex));
			}

			return (mt != null);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public MatchbackCollection GetMatbackRequest()
		{
            //12282011 TL - Deprecated old Encore related DB Tables, if we ever implement
            //Encore or related, it will require a new design and DB process
            return null;

			SqlDataReader dataReader = null;
			MatchbackCollection mbCol = new MatchbackCollection();
			try
			{
				Command command = new Command("mnFileExchange", "dbo.up_GetMatchbackEntries", 0);
				dataReader = Client.Instance.ExecuteReader(command);
				
				#region Column Ordinals
				bool colLookupDone = false;
				Int32 ordinalClientOrderID = Constants.NULL_INT;
				Int32 ordinalEmail = Constants.NULL_INT;
				Int32 ordinalZip = Constants.NULL_INT;
				Int32 ordinalOrderDate = Constants.NULL_INT;
				Int32 ordinalEncoreMailCode = Constants.NULL_INT;
				Int32 ordinalFiller = Constants.NULL_INT;
				Int32 ordinalInsertDate = Constants.NULL_INT;
				#endregion

				while(dataReader.Read())
				{
					#region Column Lookup
					if(!colLookupDone)
					{
						ordinalClientOrderID = dataReader.GetOrdinal("ClientOrderID");
						ordinalEmail = dataReader.GetOrdinal("CustomerEmailAddress");
						ordinalZip = dataReader.GetOrdinal("CustomerZipCode");
						ordinalOrderDate = dataReader.GetOrdinal("OrderDate");
						ordinalEncoreMailCode = dataReader.GetOrdinal("MailCode");
						ordinalFiller = dataReader.GetOrdinal("Filler");
						ordinalInsertDate = dataReader.GetOrdinal("InsertDate");
						colLookupDone = true;
					}
					#endregion

					Matchback mb = new Matchback();
					mb.ClientOrderID = dataReader.GetString(ordinalClientOrderID);
					mb.Email = dataReader.GetString(ordinalEmail);
					mb.Zip = dataReader.GetString(ordinalZip);
					mb.OrderDate = dataReader.GetString(ordinalOrderDate);
					mb.MailCode = dataReader.GetString(ordinalEncoreMailCode);
					mb.Filler = dataReader.GetString(ordinalFiller);
					mb.InsertDate = dataReader.GetDateTime(ordinalInsertDate);

					mbCol.Add(mb);
				}
	
			}
			catch(Exception ex)
			{
				throw new BLException("GetMatbackRequest() error", ex);
			}
			finally
			{
				if(dataReader != null)
				{
					dataReader.Close();
				}
			}

			return mbCol;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public int StartMatchbackFileGeneration(string outputFileName)
		{
            //12282011 TL - Deprecated old Encore related DB Tables, if we ever implement
            //Encore or related, it will require a new design and DB process
            return Constants.NULL_INT;

			int lineID = Constants.NULL_INT;

			try
			{
				int processID = Convert.ToInt32(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCOREEXPORT_PROCESSID"));
				lineID = KeySA.Instance.GetKey("FileTransferID");

				// Update the FileTransfer table to let it know the file generation task has started
				Command fileTransCommand = new Command("mnFileExchange", "dbo.up_InsertFileTransfer", 0);
				fileTransCommand.AddParameter("@LineID", SqlDbType.Int, ParameterDirection.Input, lineID);
				fileTransCommand.AddParameter("@FileName", SqlDbType.NVarChar, ParameterDirection.Input, outputFileName + ".PGP");
				fileTransCommand.AddParameter("@ProcessID", SqlDbType.Int, ParameterDirection.Input, processID);
				fileTransCommand.AddParameter("@ExchangeStatusID", SqlDbType.Int, ParameterDirection.Input, (int)ExchangeStatus.ProcessStarted);
				
				Client.Instance.ExecuteAsyncWrite(fileTransCommand);
			}
			catch(Exception ex)
			{
				throw new BLException("StartMatchbackFileGeneration() error", ex);
			}

			return lineID;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="file"></param>
		/// <param name="lineID"></param>
		public void InsertMatchbackFile(byte[] file, int lineID)
		{
            //12282011 TL - Deprecated old Encore related DB Tables, if we ever implement
            //Encore or related, it will require a new design and DB process
            return;

			try
			{
				// Put the BLOB in the DB then update the ExchangeStatus			
				Command command = new Command("mnFileExchange", "dbo.up_UpdateFileTransferBLOB", 0);
				command.AddParameter("@LineID", SqlDbType.Int, ParameterDirection.Input, lineID);
				command.AddParameter("@FileBLOB", SqlDbType.Image, ParameterDirection.Input, file );
				command.AddParameter("@ExchangeStatusID", SqlDbType.Int, ParameterDirection.Input, (int)ExchangeStatus.FileReady);
				
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				throw new BLException("InsertMatchbackFile() error", ex);
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mbCol"></param>
		public void MarkMatchbackRequestAsComplete(MatchbackCollection mbCol)
		{
            //12282011 TL - Deprecated old Encore related DB Tables, if we ever implement
            //Encore or related, it will require a new design and DB process
            return;

			try
			{
				Command updateCommand;
				foreach(Matchback mb in mbCol)
				{
					updateCommand = null;

					updateCommand = new Command("mnFileExchange", "dbo.up_UpdateMatchbackProcessed", 0);
					updateCommand.AddParameter("@IsProcessed", SqlDbType.Bit, ParameterDirection.Input, 1);
					updateCommand.AddParameter("@ClientOrderID", SqlDbType.NVarChar, ParameterDirection.Input, mb.ClientOrderID);

					Client.Instance.ExecuteAsyncWrite(updateCommand);
				}
			}
			catch(Exception ex)
			{
				throw new BLException("EndMatchbackFileGeneration() error", ex);
			}
		}

		/// <summary>
		/// Determines if the member has a valid CC on file. This means:
		/// 1. Last successful member tran was of credit card payment type
		/// 2. Credit card hasn't expired
		/// Valid MemberTran object is returned if found
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <returns>Valid MemberTran object is returned if found & NULL if this evaluates false</returns>
		private MemberTran GetLastValidCCMemberTran(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			MemberTran mtTemp = null;
			MemberTran mtRet = null;
			
			// membertrans come order with recent ones at top, find the first (last) intial purchase or renewal tran type
			MemberTranCollection memTrans = GetMemberTransactions(memberID, brand.Site.SiteID);
			int paymentID = Constants.NULL_INT;

			if(memTrans != null)
			{
				foreach(MemberTran mTran in memTrans)
				{
					if( (mTran.TranType == TranType.InitialBuy || mTran.TranType == TranType.Renewal) && mTran.MemberTranStatus == MemberTranStatus.Success)
					{
						mtTemp = mTran;
						paymentID = mTran.MemberPaymentID;
						break;
					}
				}	
			}

			// initial buy and renewal membertrans should have a paymentID. If it doesn't, then just skip it.
			// also check to see if this is of CC payment type
			if(paymentID != Constants.NULL_INT)
			{
				// Check to see if this payment is a credit payment
				//MemberPaymentInfo mpi = PaymentSA.Instance.GetMemberPaymentInfo(memberID, mtTemp.MemberPaymentID);

                //if(mpi != null)
                //{
                //    if(mpi.PaymentTypeID == (int)PaymentType.CreditCard)
                //    {
                //        NameValueCollection nvCol = GetMemberPayment(memberID, brand.Site.Community.CommunityID, paymentID);
                //        // check the expiration date
                //        int ccExpMonth;
                //        int ccExpYear;
						
                //        if(IntTryParse(nvCol["CreditCardExpirationMonth"], out ccExpMonth) && IntTryParse(nvCol["CreditCardExpirationYear"], out ccExpYear))
                //        {
                //            int days = DateTime.DaysInMonth(ccExpYear, ccExpMonth);
                //            DateTime expDate = new DateTime(ccExpYear, ccExpMonth, days);
                //            expDate = expDate.AddDays(1);

                //            if(expDate > DateTime.Now)
                //            {
                //                // VALID!
                //                mtRet = mtTemp;
                //            }
                //        }
                //    }
				//}
			}

			return mtRet;
		}
		/// <summary>
		/// This is specifically for ProcessMembershipFile() to use. Do not use this method unless you know the load is going to be very light.
		/// Is member eligible for rebate credit related to Encore?
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brandID"></param>
		/// <param name="membershipStartDate"></param>
		/// <param name="memberTran"></param>
		/// <param name="ignoreActiveSub"></param>
		/// <returns></returns>
		private bool IsEligibleForRebateCredit(int memberID, int brandID, DateTime membershipStartDate, out MemberTran memberTran, bool ignoreActiveSub)
		{			
			bool isActiveSub = false;;
			bool validCConFile = false;
			bool outsideMaryland = false;
			bool firstTimeBuyer = false;
			bool openAmountGreaterThanRebateAmount = false;
			memberTran = null;
			
			Matchnet.Member.ServiceAdapters.Member member =	MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
			if(member != null)
			{
				// active subscriber?
				if(ignoreActiveSub)
				{
					isActiveSub = true;
				}
				else
				{
					isActiveSub = member.IsPayingMember(brand.Site.SiteID);
				}
				
				// valid credit card on file?
				if(isActiveSub)
				{
					memberTran = GetLastValidCCMemberTran(memberID, brand);
					if(memberTran != null)
					{
						validCConFile = true;
					}
					else
					{
						string message = string.Format("Invalid credit card on file. MemberID: {0}, BrandID: {1}", memberID, brand.BrandID.ToString());
						//EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
					}
				}

				// Outside Maryland check & within USA
				if(validCConFile)
				{
					int regionID = member.GetAttributeInt(brand, "RegionID", Constants.NULL_INT);
					if(regionID != Constants.NULL_INT)
					{
						Matchnet.Content.ValueObjects.Region.RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);
						
						outsideMaryland = (region.StateRegionID != 1554) && (region.CountryRegionID == 223);
					}
				}

				// Successful purchase and first time buyer check
				if(outsideMaryland)
				{
					DateTime memberAttrStartDate = member.GetAttributeDate(brand, "JRewardStartDate", DateTime.MinValue);

					firstTimeBuyer = (memberAttrStartDate == DateTime.MinValue) && (membershipStartDate > DateTime.MinValue);
				}

				// Remaining credit amount check
				if(firstTimeBuyer)
				{
					// Only do the remaining credit check if the setting is enabled
					if(Convert.ToBoolean(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_REBATE_ENABLE")))
					{
						// Get the rebate amount
						decimal rebateAmount = Convert.ToDecimal(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_REBATE_AMOUNT"));

						CreditMaximum credMax = GetCreditMaximum(memberTran.MemberTranID);
						openAmountGreaterThanRebateAmount = credMax.MaxAmount >= rebateAmount;

						if(!openAmountGreaterThanRebateAmount)
						{
                            //EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME,
                            //    string.Format("Rebate amount exceeds the maximum open credit amount. MemberID: {0}, MemberTranID: {1}, Credit Max: {2}, Rebate Amount: {3}", memberID, memberTran.MemberTranID, credMax.MaxAmount.ToString(), rebateAmount.ToString()),
                            //    EventLogEntryType.Warning); 
						}
					}
					else
					{
						openAmountGreaterThanRebateAmount = true;
					}
				}	
				
			}	
			
			// DO A FINAL BOOL EXPRESSION HERE
			return isActiveSub && validCConFile && outsideMaryland && firstTimeBuyer && openAmountGreaterThanRebateAmount;
		}
		private bool IntTryParse(string valueString, out int outValue)
		{
			outValue = 0;
			bool isSuccess = true;

			try
			{
				outValue = int.Parse(valueString);
			}
			catch(FormatException)
			{
				isSuccess = false;
			}
			catch(OverflowException)
			{
				isSuccess = false;
			}

			return isSuccess;
		}
		private void GiveRebateCredit(MemberTran memberTran)
		{
			decimal rebateAmount = Convert.ToDecimal(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_REBATE_AMOUNT"));

			BeginCredit(TranType.AdministrativeAdjustment, memberTran.MemberID, Constants.NULL_INT, memberTran.SiteID,
				Constants.NULL_INT, memberTran.MemberTranID, memberTran.MemberPaymentID, memberTran.PlanID, rebateAmount, 0, 0, DurationType.None, memberTran.CurrencyType);            			
		}
		private string getPaymentAttribute(NameValueCollection nvc, string attributeName)
		{
			foreach (string key in nvc.Keys)
			{
				if (key == attributeName)
				{
					return nvc[key];
				}
			}

			return string.Empty;
		}
		
		#endregion

        /// <summary>
        /// Calculates any unused credit amount for a member on a particular site  
        /// </summary>
        /// <param name="memberID">Member ID being checked</param>
        /// <param name="brand">Current brand</param>
        /// <returns name="MemberTranCreditCollection">Returns a collection of MemberTranCredit</returns>
        public static MemberTranCreditCollection GetRemainingCredit(Int32 memberID,
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("mnSubscription", "dbo.up_MemberTran_GetUnusedCreditsList", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, brand.Site.SiteID);
                dataReader = Client.Instance.ExecuteReader(command);

                MemberTranCreditCollection memberTranCreditCollection = new MemberTranCreditCollection(memberID, brand.Site.SiteID);

                bool colLookupDone = false;
                Int32 ordinalMemberTranID = Constants.NULL_INT;
                Int32 ordinalReferenceMemberTranID = Constants.NULL_INT;
                Int32 ordinalTranTypeID = Constants.NULL_INT;
                Int32 ordinalAmount = Constants.NULL_INT;
                Int32 ordinalDuration = Constants.NULL_INT;
                Int32 ordinalDurationTypeID = Constants.NULL_INT;
                Int32 ordinalStartDate = Constants.NULL_INT;
                Int32 ordinalEndDate = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalMemberTranID = dataReader.GetOrdinal("MemberTranID");
                        ordinalReferenceMemberTranID = dataReader.GetOrdinal("ReferenceMemberTranID");
                        ordinalTranTypeID = dataReader.GetOrdinal("TranTypeID");
                        ordinalAmount = dataReader.GetOrdinal("Amount");
                        ordinalDuration = dataReader.GetOrdinal("Duration");
                        ordinalDurationTypeID = dataReader.GetOrdinal("DurationTypeID");
                        ordinalStartDate = dataReader.GetOrdinal("StartDate");
                        ordinalEndDate = dataReader.GetOrdinal("EndDate");

                        colLookupDone = true;
                    }

                    Int32 tranTypeID = Constants.NULL_INT;
                    tranTypeID = dataReader.GetInt32(ordinalTranTypeID);

                    Int32 referenceMemberTranID = Constants.NULL_INT;
                    if (!dataReader.IsDBNull(ordinalReferenceMemberTranID))
                    {
                        referenceMemberTranID = dataReader.GetInt32(ordinalReferenceMemberTranID);
                    }

                    DateTime startDate = DateTime.MinValue;
                    if (!dataReader.IsDBNull(ordinalStartDate))
                    {
                        startDate = dataReader.GetDateTime(ordinalStartDate);
                    }

                    DateTime endDate = DateTime.MinValue;
                    if (!dataReader.IsDBNull(ordinalEndDate))
                    {
                        endDate = dataReader.GetDateTime(ordinalEndDate);
                    }

                    DateTime currentDate = DateTime.Now;
                    Int32 totalPlanDays = Constants.NULL_INT;
                    decimal tranAmount = dataReader.GetDecimal(ordinalAmount);
                    decimal pricePerDay = Constants.NULL_DECIMAL;
                    Int32 remainingDays = Constants.NULL_INT;
                    decimal amountToCredit = Constants.NULL_DECIMAL;
                    MemberTranCredit memberTranCredit = null;

                    if (tranTypeID == 1 || tranTypeID == 2)
                    {
                        // TranTypeID 1 is Initial Buy
                        // TranTypeID 2 is Renewal
                        // Get remaining credit for initial buy or renewal transactions
                        if (currentDate > startDate && currentDate < endDate)
                        {
                            // Crediting remaining time on plan that is currently active
                            // Get total duration purchased in days
                            TimeSpan span = endDate.Subtract(startDate);
                            totalPlanDays = span.Days;

                            // If the start date and end date for a transaction is on the same day,
                            // then the transaction is valid for one day.  
                            if (totalPlanDays == 0)
                            {
                                totalPlanDays = 1;
                            }

                            // Calculate cost per day
                            pricePerDay = (tranAmount) / Convert.ToDecimal(totalPlanDays);

                            // Get remaining days left on this plan
                            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
                            // member.  
                            // Round 2.1 to 3 days
                            // Round 2.9 to 3 days
                            span = endDate.Subtract(currentDate);
                            remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

                            amountToCredit = (pricePerDay) * (Convert.ToDecimal(remainingDays));

                            memberTranCredit = new MemberTranCredit(dataReader.GetInt32(ordinalMemberTranID), amountToCredit, currentDate);
                            memberTranCreditCollection.Add(memberTranCredit);
                        }
                        else if (currentDate < startDate && currentDate < endDate)
                        {
                            // Crediting purchased plans that have not yet started
                            memberTranCredit = new MemberTranCredit(dataReader.GetInt32(ordinalMemberTranID), tranAmount, currentDate);
                            memberTranCreditCollection.Add(memberTranCredit);
                        }
                    }
                    else if (tranTypeID == 4)
                    {
                        // TranTypeID 4 is Administrative Adjustment
                        // Get remaining credit for credit tool adjustments
                        if (Math.Abs(tranAmount) > 0.0m)
                        {
                            // Get remaining credit for credit tool adjustments where money was credited
                            // Ignore credit tool adjustments where there were only time adjustments and money was not credited
                            memberTranCredit = new MemberTranCredit(dataReader.GetInt32(ordinalMemberTranID), tranAmount, currentDate);
                            memberTranCreditCollection.Add(memberTranCredit);
                        }
                    }
                }

                return memberTranCreditCollection;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured while getting remaining credit.", ex));
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }
        */
        #endregion

	}
	

	public enum ExchangeStatus
	{
		ProcessStarted = 0,
		FileReady = 1,
		FileTransferred = 2,
		FileProcessed = 3
	};
}
