using System;
using System.Diagnostics;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using ServiceConstants=Matchnet.Purchase.ValueObjects.ServiceConstants;
using Site=Matchnet.Content.ValueObjects.BrandConfig.Site;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;

namespace Matchnet.Purchase.BusinessLogic
{
	/// <summary>
	/// Summary description for PremiumServiceHelper.
	/// </summary>
	public class PremiumServiceUtil
	{
		public PremiumServiceUtil()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void highLightMember(MemberTranStatus memberTranStatus, MemberTran memberTran, Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrandByAdmin)
		{
			setPremiumServiceMember(memberTranStatus, memberTran, targetBrandByAdmin);
		}		

		public static void setPremiumServiceMember(MemberTranStatus memberTranStatus, MemberTran memberTran, Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrandByAdmin)
		{
			setPremiumServiceMember(memberTranStatus, memberTran, targetBrandByAdmin, DateTime.MinValue);
		}	

		public static void setPremiumServiceMember(MemberTranStatus memberTranStatus, MemberTran memberTran, Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrandByAdmin, DateTime premiumServicesResetDate)
		{
			setPremiumServiceMember(memberTranStatus, memberTran, targetBrandByAdmin, premiumServicesResetDate, TimeAdjustOverrideType.None);
		}	
	

		/// <summary>
		/// 08122008 TL - Overloaded method to account for a "reset date" parameter in cases of upgrade (or any purchases where we apply a credit for prior transactions).
        /// 07152014 TL - This method is no longer used, it was part of the callback for updating matchnet and the old membertran database tables.
		/// </summary>
		/// <param name="memberTranStatus"></param>
		/// <param name="memberTran"></param>
		/// <param name="targetBrandByAdmin"></param>
		/// <param name="premiumServicesResetDate">Represents the new expiration date, due to a purchase mode with a credit applied for prior transactions; This is ignored if date is DateTime.MinValue.</param>
		/// <param name="timeAdjustOverrideTypeMask">Type of web admin time adjustment</param>
		public static void setPremiumServiceMember(MemberTranStatus memberTranStatus, MemberTran memberTran, Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrandByAdmin, DateTime premiumServicesResetDate, TimeAdjustOverrideType timeAdjustOverrideTypeMask)
		{
			//bool renewalMode = false;
			//int brandid=0;
			//bool creditTool=false;
			//bool webAdmin=false;
			//bool initialMemberBuy=false;
			//bool renewal=false;
			Content.ValueObjects.BrandConfig.Brand transactionBrand=null;
			Purchase.ValueObjects.Plan transactionPlan=null;
			System.Text.StringBuilder strLog = new System.Text.StringBuilder();
			DateTime dteBaseExpirationDate=DateTime.MinValue;
			PlanType transactionPlanType = PlanType.None;
			PremiumType transactionPremiumType = PremiumType.None;

			try
			{
				// If targetBrandByAdmin is null, then the subscription is handled by the member
				// If targetBrandByAdmin is not null, then the subscription is handled by an admin using the web tool rather than the credit tool    
				if (memberTran == null || memberTranStatus != MemberTranStatus.Success)
				{
					// Just an extra check to make sure that there is a valid memberTran and that it's successful
					// This occurs when an admin selects a plan without adjusting time and
					// saving this for the member
					return;
				}
				else if (memberTran.TranType != TranType.InitialBuy && memberTran.TranType != TranType.AdministrativeAdjustment && memberTran.TranType != TranType.Renewal && memberTran.TranType != TranType.Void)
				{
					return;
				}
				else
				{
					//initialMemberBuy=(memberTran.Plan != null);
					//targetBrand set at webadmin
					//webAdmin=((!initialMemberBuy) && (targetBrandByAdmin != null));
					//renewal= (memberTran.TranType == TranType.Renewal); //used to get correct duration from plan
					//creditTool=(!initialMemberBuy) && (!webAdmin) && (!renewal);
					
					transactionBrand=targetBrandByAdmin;
					if(transactionBrand==null)
					{
						transactionBrand=getBrand(memberTran.SiteID);
					}

					if(transactionBrand ==null)
					{return;}

					transactionPlan=memberTran.Plan;
					if(transactionPlan==null && memberTran.PlanID != 0 && memberTran.PlanID != Constants.NULL_INT)
					{
						transactionPlan=PlanBL.Instance.GetPlans(transactionBrand.BrandID).FindByID(memberTran.PlanID);
					}

					if (transactionPlan != null)
					{
						transactionPlanType = transactionPlan.PlanTypeMask;
						transactionPremiumType = transactionPlan.PremiumTypeMask;
					}
				}				
				
				//if (memberTranStatus != MemberTranStatus.Success || transactionPlan == null)
				//{return;}

				strLog.Append("In setPremiumServiceMember() for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\n");
				Trace.WriteLine("__In setPremiumServiceMember() for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\n");
				
				//Get Member
				Site site = transactionBrand.Site;
				Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberTran.MemberID, MemberLoadFlags.None);

				Trace.WriteLine("__In setPremiumServiceMember() set the site to " + site.SiteID.ToString());					
				Trace.WriteLine("__In setPremiumServiceMember() set the Member ID to " + objMember.MemberID.ToString());										
				
				Trace.WriteLine("__In setPremiumServiceMember(), PlanType is " + Convert.ToString(transactionPlanType) + ", TranType is " + Convert.ToString(memberTran.TranType));	
				Trace.WriteLine("__In setPremiumServiceMember(), PremiumPlanType is " + Convert.ToString(transactionPremiumType));																						

				// 7/9/2008 JRho
				// Voiding a transaction using the credit tool should also adjust the highlighted expiration date
				// The memberTran.TranType is TranType.Void in this case
				// The behavior on stage was different from production where the TranType was AdministrativeAdjustment on stage but Void in production
				
				strLog.Append("In setPremiumServiceMember() where site ID is " + site.SiteID.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\n");
				Trace.WriteLine("__In setPremiumServiceMember() where site ID is " + site.SiteID.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID);

				DateTime dteExpirationDate = DateTime.MinValue;
				DateTime dteHighlightOriginal = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "HighlightedExpirationDate", DateTime.MinValue, true);
				DateTime dteSpotlightOriginal = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "SpotlightExpirationDate", DateTime.MinValue, true);
				DateTime dteJMeterOriginal = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "JMeterExpirationDate", DateTime.MinValue, true);
                DateTime dteSubscriptionExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE, DateTime.MinValue, true);		

				// FOR ALL THE OTHER SITES INCREMENT THE TIME PURCHASED TO THE
				// REMAINING TIME FOR THE PREMIUM SERVICE
				// 3 CASES
				// 1. PURCHASED FOR THE FIRST TIME SO HIGHLIGHTEDEXPIRATIONDATE ATTRIBUTE DOES NOT EXISTS
				// 2. PURCHASED AGAIN BUT THE PREVIOUS PURCHASE ALREADY EXPIRED
				// 3. PURCHASED AGAIN BUT THE PREVIOUS PURCHASE HAS NOT EXPIRED  
				#region HighlightProfile
				//Get Expiration Date for Highlight service
				if(
					((transactionPlan != null) && ((transactionPlan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile) && (timeAdjustOverrideTypeMask == TimeAdjustOverrideType.None))
					||
					((timeAdjustOverrideTypeMask & TimeAdjustOverrideType.HighlightedProfile) == TimeAdjustOverrideType.HighlightedProfile)
				  )
				{					
					if (premiumServicesResetDate != DateTime.MinValue)
					{
						// For the following case
						// 1) Initial purchases that are upgrades only
						//This indicates that the new expiration date should be this value passed in.
						dteExpirationDate = premiumServicesResetDate;

						//Highlighting should be turned on since user is upgrading (or purchasing a new premium plan, with all prior/current plans credited back)
						objMember.SetAttributeInt(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "HighlightedFlag", 1);
					}
					else
					{
						// For the following cases
						// 1) Initial purchases that are not upgrades
						// 2) Administrative adjustments  
						dteBaseExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "HighlightedExpirationDate", DateTime.MinValue, true);							

						if (dteBaseExpirationDate == DateTime.MinValue
							|| dteBaseExpirationDate <= DateTime.Now)
						{
							// Set the base expiration date in case 1 and 2
							// Do not add duration to the highlightedexpirationdate if it 
							// has already expired 
							dteBaseExpirationDate = DateTime.Now;
							// Only turn on the highlight profile for first time subscribers
							// or previous subscribers whose subscription has ended before  
							// Otherwise, do not adjust the HighlightedFlag setting if the member
							// has it turned off.  
							objMember.SetAttributeInt(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "HighlightedFlag", 1);		
						}

						strLog.Append("In setPremiumServiceMember() where previous HighlightedExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\n");
						Trace.WriteLine("__In setPremiumServiceMember() where previous HighlightedExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID);

						dteExpirationDate = MemberTran.AddDurationToDate(memberTran,dteBaseExpirationDate,false);
					}
					
				}
				else
				{
					if ((transactionPlan != null) && (Plan.IsPurchaseModeValidForCredit(memberTran.PurchaseMode)) && (timeAdjustOverrideTypeMask == TimeAdjustOverrideType.None))
					{
						// For upgrade or swap purchases, any existing highlighted profile premium service
						// is removed.  The next step will add the appropriate time for this premium service
						// only if the member actually purchased this particular premium service.  
						dteExpirationDate = DateTime.MinValue;
					}
					else
					{
						dteExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "HighlightedExpirationDate", DateTime.MinValue, true);							
					}
				}

				//set updated date
				dteExpirationDate = CheckPremiumDateAgainstSubscriptionDate(dteExpirationDate, dteSubscriptionExpirationDate);
				objMember.SetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "HighlightedExpirationDate", dteExpirationDate);

				#endregion

				#region JMeter
				//Get Expiration Date for JMeter service
				if(
					((transactionPlan != null) && ((transactionPlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter) && (timeAdjustOverrideTypeMask == TimeAdjustOverrideType.None))
					||
					((timeAdjustOverrideTypeMask & TimeAdjustOverrideType.JMeter) == TimeAdjustOverrideType.JMeter)
					)
				{					
					if (premiumServicesResetDate != DateTime.MinValue)
					{
						// For the following case
						// 1) Initial purchases that are upgrades only
						//This indicates that the new expiration date should be this value passed in.
						dteExpirationDate = premiumServicesResetDate;

						//JMeter should be turned on since user is upgrading (or purchasing a new premium plan, with all prior/current plans credited back)
						objMember.SetAttributeInt(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "JMeterFlag", 1);
					}
					else
					{
						// For the following cases
						// 1) Initial purchases that are not upgrades
						// 2) Administrative adjustments  
						dteBaseExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "JMeterExpirationDate", DateTime.MinValue, true);							

						if (dteBaseExpirationDate == DateTime.MinValue
							|| dteBaseExpirationDate <= DateTime.Now)
						{
							// Set the base expiration date in case 1 and 2
							// Do not add duration to the JMeterexpirationdate if it 
							// has already expired 
							dteBaseExpirationDate = DateTime.Now;
							// Only turn on the JMeter for first time subscribers
							// or previous subscribers whose subscription has ended before  
							// Otherwise, do not adjust the JMeterFlag setting if the member
							// has it turned off.  
							objMember.SetAttributeInt(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "JMeterFlag", 1);		
						}

						strLog.Append("In setPremiumServiceMember() where previous JMeterExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\n");
						Trace.WriteLine("__In setPremiumServiceMember() where previous JMeterExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID);

						dteExpirationDate = MemberTran.AddDurationToDate(memberTran,dteBaseExpirationDate,false);
					}
					
				}
				else
				{
					if ((transactionPlan != null) && (Plan.IsPurchaseModeValidForCredit(memberTran.PurchaseMode)) && (timeAdjustOverrideTypeMask == TimeAdjustOverrideType.None))
					{
						// For upgrade or swap purchases, any existing JMeter profile premium service
						// is removed.  The next step will add the appropriate time for this premium service
						// only if the member actually purchased this particular premium service.  
						dteExpirationDate = DateTime.MinValue;
					}
					else
					{
						dteExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "JMeterExpirationDate", DateTime.MinValue, true);							
					}
				}

				//set updated date
				dteExpirationDate = CheckPremiumDateAgainstSubscriptionDate(dteExpirationDate, dteSubscriptionExpirationDate);
				objMember.SetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "JMeterExpirationDate", dteExpirationDate);

				#endregion

				#region SpotlightMember
				//Get Expiration Date for Spotlight service
				if(
					((transactionPlan != null) && ((transactionPlan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember) && (timeAdjustOverrideTypeMask == TimeAdjustOverrideType.None))
					||
					((timeAdjustOverrideTypeMask & TimeAdjustOverrideType.SpotlightMember) == TimeAdjustOverrideType.SpotlightMember)
				  )
				{
					if (premiumServicesResetDate != DateTime.MinValue)
					{
						//This indicates that the new expiration date should be this value passed in.
						dteExpirationDate = premiumServicesResetDate;
					}
					else
					{
						dteBaseExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "SpotlightExpirationDate", DateTime.MinValue, true);							
						if (dteBaseExpirationDate == DateTime.MinValue
							|| dteBaseExpirationDate <= DateTime.Now)
						{	
							dteBaseExpirationDate = DateTime.Now;
				
						}

						strLog.Append("In setPremiumServiceMember() where previous SpotlightExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\n");
						Trace.WriteLine("__In setPremiumServiceMember() where previous SpotlightExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID);

						dteExpirationDate = MemberTran.AddDurationToDate(memberTran,dteBaseExpirationDate,false);
					}					
				}
				else
				{
					if ((transactionPlan != null) && (Plan.IsPurchaseModeValidForCredit(memberTran.PurchaseMode)) && (timeAdjustOverrideTypeMask == TimeAdjustOverrideType.None))
					{
						// For upgrade or swap purchases, any existing highlighted profile premium service
						// is removed.  The next step will add the appropriate time for this premium service
						// only if the member actually purchased this particular premium service.  
						dteExpirationDate = DateTime.MinValue;
					}
					else
					{
						dteExpirationDate = objMember.GetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "SpotlightExpirationDate", DateTime.MinValue, true);							
					}
				}	

				//set updated date
				dteExpirationDate = CheckPremiumDateAgainstSubscriptionDate(dteExpirationDate, dteSubscriptionExpirationDate);
				objMember.SetAttributeDate(Constants.NULL_INT, site.SiteID, Constants.NULL_INT, "SpotlightExpirationDate", dteExpirationDate);

				//only updating spotlight date if it changed
				if (dteSpotlightOriginal != dteExpirationDate)
				{
					SpotlightMemberSA.Instance.SaveMember(transactionBrand.BrandID,objMember.MemberID,dteExpirationDate,true,true);
				}
				#endregion

				//Save Member
				MemberSaveResult objMemberSaveResult = MemberSA.Instance.SaveMember(objMember);                        
				if (objMemberSaveResult.SaveStatus != MemberSaveStatusType.Success)
				{
					strLog.Append("Unable to set the SpotlightExpirationDate attribute for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".");
					Trace.WriteLine("__In highLightMember(): Unable to set the HighlightedExpirationDate attribute for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID);
				}                   
				else
				{
					strLog.Append("Successfuly setPremiumServiceMember attribute for Member ID " + memberTran.MemberID + " where HighlightedExpirationDate is " + dteExpirationDate.ToString() + " and the HighlightedFlag is 1.");
					Trace.WriteLine("__In highLightMember(): Successfuly set the ExpirationDate attribute for Member ID " + memberTran.MemberID + " where HighlightedExpirationDate is " + dteExpirationDate.ToString() + " and the HighlightedFlag is 1.");
				}			
			}
			catch(Exception ex)
			{
				Matchnet.Exceptions.ServiceBoundaryException serviceExc=new Matchnet.Exceptions.ServiceBoundaryException(ServiceConstants.SERVICE_NAME,"Unable to setPremiumServiceMember attribute for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + ".\r\n" + strLog,ex,false);
				Trace.WriteLine("__Error in setPremiumServiceMember(): Unable to setPremiumServiceMember attribute for Member ID " + memberTran.MemberID + " after purchasing the Premium Plan ID " + memberTran.PlanID.ToString() + ". The Charge ID is " + memberTran.ChargeID + "., Error message: " + ex.Message.ToString());
			}
		}	
	
		public static Matchnet.Content.ValueObjects.BrandConfig.Brand getBrand(int siteid)
		{
			Matchnet.Content.ValueObjects.BrandConfig.Brands  brands=BrandConfigSA.Instance.GetBrandsBySite(siteid);
			Matchnet.Content.ValueObjects.BrandConfig.Brand lowestBrand=null;
			int lowestBrandID=Constants.NULL_INT;
			foreach(Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
			{
				if (lowestBrandID == Constants.NULL_INT)
				{
					lowestBrandID = brand.BrandID;
				}
				else if (brand.BrandID < lowestBrandID)
				{
					// The assumption is that the lowest brand ID for all the brands
					// of a site will provide the default brand ID for the site 
					// In the case that the lowest brand ID is not the default brand 
					// ID for the site, then the correct plan will still be retrieved.
					// However, a collection of plans for a new brand ID will be cached.  
					lowestBrandID = brand.BrandID;
				}
			}
			lowestBrand=Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(lowestBrandID);
			return lowestBrand;
			

		}


		/// <summary>
		/// Performs a check to ensure that the premium service expiration date does not extend pass the subscription expiration date
		/// </summary>
		/// <param name="premiumDate"></param>
		/// <param name="subscriptionDate"></param>
		/// <returns></returns>
		private static DateTime CheckPremiumDateAgainstSubscriptionDate(DateTime premiumDate, DateTime subscriptionDate)
		{
			DateTime returnDate = premiumDate;

			if (subscriptionDate == DateTime.MinValue)
			{
				returnDate = DateTime.MinValue;
			}
			else if (premiumDate > subscriptionDate)
			{
				return subscriptionDate;
			}

			return returnDate;
		}
	}
}
