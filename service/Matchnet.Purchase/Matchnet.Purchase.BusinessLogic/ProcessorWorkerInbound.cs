using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase;
using Matchnet.Queuing;
using Matchnet.Security;
using ServiceConstants=Matchnet.Purchase.ValueObjects.ServiceConstants;
using Site=Matchnet.Content.ValueObjects.BrandConfig.Site;

namespace Matchnet.Purchase.BusinessLogic
{
	public class ProcessorWorkerInbound : IProcessorWorker
	{
		private static ArrayList _hardDeclineCodes;
		private static object _syncBlock = new object();
		
		private ArrayList HardDeclineCodes
		{
			get
			{
				if(_hardDeclineCodes == null)
				{
					lock(_syncBlock)
					{
						if(_hardDeclineCodes == null)
						{
							string[] codeValues = { "140", "301", "302", "303", "304", "305", "307", "320", "330", "340", "322", "323", "327", "326", "328", "351", "352", "321" };
							_hardDeclineCodes = new ArrayList(codeValues.Length);
							_hardDeclineCodes.AddRange(codeValues);			
							_hardDeclineCodes.Sort();
						}
					}
				}
				return _hardDeclineCodes;
			}
		}
		
		public void ProcessMessage(object messageBody)
		{
			MemberTranResult memberTranResult = messageBody as MemberTranResult;

			if (memberTranResult == null)
			{
				throw new Exception("Message body was not a MemberTranResult (type: " + messageBody.GetType().ToString() + ").");
			}
			
			if(memberTranResult.MemberTran.TranType == TranType.AuthPmtProfile)
			{
				saveCompletAuthPmtProfile(memberTranResult);
			}
			else
			{
				saveComplete(memberTranResult);
			}
		}

		public void saveChargeStage(MemberTranResult memberTranResult)
		{
			try
			{
				#region prepare command to write to mnChargeStage
				//save to mnChargeStage for reporting
				if (memberTranResult.MemberTran.Payment != null)
				{
					Trace.WriteLine("__avsCode: " + memberTranResult.ResponseCodeAVS);
					Trace.WriteLine("__cvvCode: " + memberTranResult.ResponseCodeCVV);
					Command commandChargeStage = new Command("mnChargeStage", "dbo.up_MemberPaymentFlat_Save", 0);
					commandChargeStage.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberPaymentID);
					commandChargeStage.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberID);
					commandChargeStage.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.FirstName);
					commandChargeStage.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.LastName);
					commandChargeStage.AddParameter("@Phone", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.Phone);
					commandChargeStage.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.AddressLine1);
					commandChargeStage.AddParameter("@AddressLine2", SqlDbType.NVarChar, ParameterDirection.Input, null);
					commandChargeStage.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.City);
					commandChargeStage.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.State);
					commandChargeStage.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.PostalCode);
					commandChargeStage.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.Payment.CountryRegionID);
					commandChargeStage.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
					if (memberTranResult.MemberTran.Payment is CreditCardPayment)
					{
						commandChargeStage.AddParameter("@CreditCardExpirationMonth", SqlDbType.TinyInt, ParameterDirection.Input, ((CreditCardPayment)memberTranResult.MemberTran.Payment).ExpirationMonth);
						commandChargeStage.AddParameter("@CreditCardExpirationYear", SqlDbType.Int, ParameterDirection.Input, ((CreditCardPayment)memberTranResult.MemberTran.Payment).ExpirationYear);
						commandChargeStage.AddParameter("@CreditCardHash", SqlDbType.VarChar, ParameterDirection.Input, Crypto.Hash(memberTranResult.MemberTran.AccountNumber + PurchaseConstants.SOOPER_HASH));
					}
					#endregion

					Client.Instance.ExecuteAsyncWrite(commandChargeStage);
				}

				//save request/response XML to mnChargeStage for reporting
				if (memberTranResult.logRequestResponseData)
				{
					this.LogRequestResponseXML(memberTranResult.ResponseData, memberTranResult.RequestDataScrubbed, memberTranResult);
				}

			}
			catch(Exception ex)
			{
				string message = this.ToString() + ".saveChargeStage()";
				Exception tempEx = ex;
				while(tempEx != null)
				{
					message = message + " " + tempEx.Message;
					tempEx = tempEx.InnerException;
				}
				throw new Exception(message,ex);
			}
		}

		/// <summary>
		/// This method will log response/request XML to the mnChargeStage.ChargeLogXML database for logging
		/// </summary>
		private void LogRequestResponseXML(string responseXML, string requestXML, MemberTranResult memberTranResult)
		{
			try
			{
				if (memberTranResult != null)
				{
					Command command = new Command("mnChargeStage", "dbo.up_ChargeLogXML_Insert", 0);
					command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.ChargeID);
					command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberTranID);
					command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberPaymentID);
					command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberID);
					command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.ProviderID);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.SiteID);
					command.AddParameter("@XMLRequest", SqlDbType.NVarChar, ParameterDirection.Input, requestXML);
					command.AddParameter("@XMLResponse", SqlDbType.NVarChar, ParameterDirection.Input, responseXML);

					Client.Instance.ExecuteAsyncWrite(command);
				}
			}
			catch (Exception ex)
			{

				new ServiceBoundaryException(Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME,
					"Error in PurchaseWorkerInbound.LogRequestResponseXML: " + ex.Message + ", memberTranID:" + memberTranResult.MemberTran.MemberTranID.ToString() + ").\r\n",
					null);
			}
		}

		public void processMemberAuthPmtProfileSaveComplete(Int32 MemberTranID,
														string ResourceConstant,
														Int32 MemberTranStatusID,
														string ResponseCode,
														string AVSCode,
														string CVVCode,
														Int32 ProviderID,
														DateTime UpdateDate,
														Int32 CreditCardType)
		{
			Command command = null;
			SyncWriter sw = null;
			try
			{
				#region build up the command object

				command = new Command("mnSubscription", "dbo.up_Member_Auth_Pmt_Profile_Save_Complete", 0);
				
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, MemberTranID);
				if(ResourceConstant != null)
				{
					command.AddParameter("@ResourceConstant", SqlDbType.VarChar, ParameterDirection.Input, ResourceConstant);
				}
				command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, MemberTranStatusID);
				if(ResponseCode != null)
				{
					command.AddParameter("@ResponseCode", SqlDbType.VarChar, ParameterDirection.Input, ResponseCode);
				}
				if(AVSCode != null)
				{
					command.AddParameter("@AVSCode", SqlDbType.VarChar, ParameterDirection.Input, AVSCode);
				}
				if(CVVCode != null)
				{
					command.AddParameter("@CVVCode", SqlDbType.VarChar, ParameterDirection.Input, CVVCode);
				}
				if(ProviderID != Constants.NULL_INT)
				{
					command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, ProviderID);
				}
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, UpdateDate);

				#endregion

				sw = new SyncWriter();
				Exception ex;
				sw.Execute(command,
					out ex);

				if (ex != null)
				{
					throw ex;
				}
			}
			catch(Exception ex)
			{
				string message = this.ToString() + ".processMemberAuthPmtProfileSaveComplete()";
				Exception tempEx = ex;
				while(tempEx != null)
				{
					message = message + " " + tempEx.Message;
					tempEx = tempEx.InnerException;
				}
				throw new Exception(message,ex);
			}
			finally
			{
				command = null;
				if(sw != null)
				{
					sw.Dispose();
					sw = null;
				}
			}
		}

		private void saveCompletAuthPmtProfile(MemberTranResult memberTranResult)
		{
			try
			{
				this.processMemberAuthPmtProfileSaveComplete(memberTranResult.MemberTran.MemberTranID
																,memberTranResult.ResourceConstant
																,(int)memberTranResult.MemberTranStatus
																,memberTranResult.ResponseCode
																,memberTranResult.ResponseCodeAVS
																,memberTranResult.ResponseCodeCVV
																,memberTranResult.ProviderID
																,DateTime.Now
																,(int)memberTranResult.MemberTran.PaymentType);

				saveChargeStage(memberTranResult);

				if(memberTranResult.MemberTranStatus == MemberTranStatus.Success)
				{
					SendPaymentProfileConfirmation(memberTranResult);
				}
			}
			catch(Exception ex)
			{
				string message = this.ToString() + ".saveCompletAuthPmtProfile() - " + ex.Message;
				throw new Exception(message,ex);
			}
			finally
			{
			}

		}

		private void SendPaymentProfileConfirmation(MemberTranResult memberTranResult)
		{
			try
			{
				Site site = BrandConfigSA.Instance.GetBrandsBySite(memberTranResult.MemberTran.SiteID).GetSite(memberTranResult.MemberTran.SiteID);

				// is this site configured to send out this alert?
				bool send = Convert.ToBoolean(RuntimeSettings.GetSetting(
					"ENABLE_PAYMENTPROFILE_CONFIRMATION_EMAIL", site.Community.CommunityID, site.SiteID));

				if (send)
				{
					int brandID = Constants.NULL_INT;

					Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTranResult.MemberTran.MemberID,
						MemberLoadFlags.None);

					member.GetLastLogonDate(site.Community.CommunityID, out brandID);

					// get CC info
					string last4CC = string.Empty;

					if (memberTranResult.MemberTran.PaymentType == PaymentType.CreditCard)
					{
						CreditCardPayment creditCardPayment = (CreditCardPayment) memberTranResult.MemberTran.Payment;

						if (creditCardPayment != null)
						{
							if (creditCardPayment.CreditCardNumber != null)
							{
								last4CC = creditCardPayment.CreditCardNumber.Substring(creditCardPayment.CreditCardNumber.Length - 3);
							}
						}
					}

					ExternalMailSA.Instance.SendPaymentProfileConfirmation(
						member.MemberID,//int memberID,
						site.SiteID,//int siteID,
						brandID,//int brandID,
						member.EmailAddress,//string emailAddress,
						memberTranResult.MemberTran.Payment.FirstName,//string firstName,
						memberTranResult.MemberTran.Payment.LastName,//string lastName,
						memberTranResult.MemberTran.InsertDate,//DateTime dateOfPurchase,
						last4CC,//string last4CC,
						memberTranResult.MemberTran.MemberTranID.ToString()//string confirmationNumber
						);
				}
			}
			catch(Exception ex)
			{
				//We're not throwing the exception here because its not worth to re-attempt the transaction
				//just because of failure due to sending emails.

				string errorMessage = this.ToString() + ".SendPaymentProfileConfirmation()";
				Exception tempEx = ex;
				while(tempEx != null)
				{
					errorMessage = errorMessage + " - " + tempEx.Message;
					tempEx = tempEx.InnerException;
				}
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME,errorMessage);
			}
		}

		/*
        private void highLightMember(MemberTranResult memberTranResult)
        {
			System.Text.StringBuilder strLog = new System.Text.StringBuilder();
			strLog.Append("In highLightMember() for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".\n");

            try
            {
                if (memberTranResult.MemberTranStatus == MemberTranStatus.Success && memberTranResult.MemberTran.Plan != null)
                {
                    if (((memberTranResult.MemberTran.Plan.PlanTypeMask & PlanType.PremiumPlan) == PlanType.PremiumPlan) && memberTranResult.MemberTran.TranType == TranType.InitialBuy)
                    {
                        // Member has purchased a premium plan  
                        // Just set the expiration date for the HighlightedExpirationDate attribute
                        // to a year from today
                        // If the plan is good for 3 months, then after 3 months, the member mini profile
                        // will not be highlighted even if the HighlightedExpirationDate attribute is still 
                        // valid for another 9 months.  The member must also be currently subscribed to
                        // have his mini profiles highlighted.                         
                        Site site = BrandConfigSA.Instance.GetBrandsBySite(memberTranResult.MemberTran.SiteID).GetSite(memberTranResult.MemberTran.SiteID);                       
                        int intBrandID = Constants.NULL_INT;

						Member.ServiceAdapters.Member objMember = MemberSA.Instance.GetMember(memberTranResult.MemberTran.MemberID, MemberLoadFlags.None);
						DateTime lastLogonDate = objMember.GetLastLogonDate(site.Community.CommunityID, out intBrandID);
						Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(intBrandID);
						
						DateTime dteExpirationDate = DateTime.MinValue;

						strLog.Append("In highLightMember() where site ID is " + site.SiteID.ToString() + " and last logon brand ID is " + intBrandID.ToString() + " and last logon date is " + lastLogonDate.ToString() + " for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".\n");

						if (site.SiteID == 4)
						{
							// FOR JDATE.CO.IL ONLY
							dteExpirationDate = DateTime.Now.AddYears(1);                                                         
						}
						else
						{
							// FOR ALL THE OTHER SITES INCREMENT THE TIME PURCHASED TO THE
							// REMAINING TIME FOR THE PREMIUM SERVICE
							dteExpirationDate = objMember.GetAttributeDate(brand, "HighlightedExpirationDate", DateTime.MinValue);							
							if (dteExpirationDate == DateTime.MinValue)
							{
								dteExpirationDate = DateTime.Now;
								// Only turn on the highlight profile for first time subscribers
								// Otherwise, do not adjust the HighlightedFlag setting if the member
								// has it turned off.  
								objMember.SetAttributeInt(brand, "HighlightedFlag", 1);		
							}

							strLog.Append("In highLightMember() where previous HighlightedExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".\n");

							DurationType durationType = memberTranResult.MemberTran.DurationType;
							int durationToAdd = memberTranResult.MemberTran.Duration;

							strLog.Append("In highLightMember() where  duration type is " + durationType.ToString() + " and duration is " + durationToAdd.ToString() + "\n");

							switch (durationType)
							{
								case DurationType.Minute: 
									dteExpirationDate = dteExpirationDate.AddMinutes(durationToAdd);
									break;
								case DurationType.Hour: 
									dteExpirationDate = dteExpirationDate.AddHours(durationToAdd);
									break;
								case DurationType.Day: 
									dteExpirationDate = dteExpirationDate.AddDays(durationToAdd);
									break;
								case DurationType.Week: 
									dteExpirationDate = dteExpirationDate.AddDays(durationToAdd * 7);
									break;
								case DurationType.Month: 
									dteExpirationDate = dteExpirationDate.AddMonths(durationToAdd);
									break;
								case DurationType.Year: 
									dteExpirationDate = dteExpirationDate.AddYears(durationToAdd);
									break;
							}
						}

						strLog.Append("In highLightMember() where new HighlightedExpirationDate is " + dteExpirationDate.ToString() + " for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".\n");

						objMember.SetAttributeDate(brand, "HighlightedExpirationDate", dteExpirationDate);

                        MemberSaveResult objMemberSaveResult = MemberSA.Instance.SaveMember(objMember);                        
						if (objMemberSaveResult.SaveStatus != MemberSaveStatusType.Success)
						{
							strLog.Append("Unable to set the HighlightedExpirationDate attribute for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".");
							//EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Unable to set the HighlightedExpirationDate attribute for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".");
						}                   
						else
						{
							strLog.Append("Successfuly set the HighlightedExpirationDate attribute for Member ID " + memberTranResult.MemberTran.MemberID + " where HighlightedExpirationDate is " + dteExpirationDate.ToString() + " and the HighlightedFlag is 1.");
						}

						EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, strLog.ToString());
					}
                }
            }
            catch(Exception ex)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Unable to set the HighlightedExpirationDate attribute for Member ID " + memberTranResult.MemberTran.MemberID + " after purchasing the Premium Plan ID " + memberTranResult.MemberTran.PlanID.ToString() + ". The Charge ID is " + memberTranResult.MemberTran.ChargeID + ".");
            }
        }
		*/


		private void saveComplete(MemberTranResult memberTranResult) 
		{
			Command command = null;

			Trace.WriteLine("__" + memberTranResult.MemberTran.TranType.ToString());
			Trace.WriteLine("__" + memberTranResult.MemberTranStatus.ToString());

			//Update Tran record
			if (memberTranResult.MemberTran.TranType != TranType.AdministrativeAdjustment
				&& memberTranResult.MemberTran.TranType != TranType.Void
				&& memberTranResult.MemberTran.TranType != TranType.AuthorizationNoTran)
			{
				command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Complete", 0);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberTranID);
				command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranResult.MemberTranStatus);
				command.AddParameter("@NewMemberTranID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(PurchaseBL.PRIMARYKEY_MEMBERTRANID));
				command.AddParameter("@NewMemberSubID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(PurchaseBL.PRIMARYKEY_MEMBERTRANID));
				command.AddParameter("@ResponseCode", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResponseCode);
				if (memberTranResult.MemberTran.PaymentType == PaymentType.CreditCard)
				{
					command.AddParameter("@CreditCardTypeID", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTranResult.MemberTran.Payment).CreditCardType);
				}
				command.AddParameter("@AVSCode", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResponseCodeAVS);
				command.AddParameter("@CVVCode", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResponseCodeCVV);
				if (memberTranResult.ProviderID != Constants.NULL_INT)
				{
					command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.ProviderID);
				}
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

				//08192008 TL - ESP Project, Adding new parameter - PurchaseModeValidForCredit - to specify whether this transaction should reset the renew date, instead of extending it
				if (Plan.IsPurchaseModeValidForCredit(memberTranResult.MemberTran.PurchaseMode))
					command.AddParameter("@PurchaseModeValidForCredit", SqlDbType.Bit, ParameterDirection.Input, Convert.ToBoolean(1));

				command.AddParameter("@RenewDate", SqlDbType.SmallDateTime, ParameterDirection.Output);
				if (memberTranResult.ResourceConstant != null)
				{
					command.AddParameter("@ResourceConstant", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.ResourceConstant);
				}
				
			}
			else if (memberTranResult.MemberTran.TranType == TranType.AuthorizationNoTran && memberTranResult.MemberTranStatus == MemberTranStatus.Success)
			{
				Trace.WriteLine("__dbo.up_MemberSub_Save_Payment_Update");
				command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Payment_Update", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.SiteID);
				command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberPaymentID);

			}
			else if (memberTranResult.MemberTran.TranType != TranType.AuthorizationNoTran)
			{
				//Administrative adjustments (credit tool)
				command = new Command("mnSubscription", "dbo.up_MemberSub_Save_Adjust", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.SiteID);
				command.AddParameter("@ReferenceMemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.ReferenceMemberTranID);
				command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.AdminMemberID);
				command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.Duration);
				command.AddParameter("@DurationTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranResult.MemberTran.DurationType);
				command.AddParameter("@Amount", SqlDbType.Decimal, ParameterDirection.Input, memberTranResult.MemberTran.Amount);
				command.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.TransactionReasonID);
				command.AddParameter("@MemberSubID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberSubID);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberTranID);
				command.AddParameter("@MemberTranStatusID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranResult.MemberTranStatus);
				command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.ProviderID);
				if (memberTranResult.MemberTran.PaymentType == PaymentType.CreditCard)
				{
					command.AddParameter("@CreditCardTypeID", SqlDbType.Int, ParameterDirection.Input, (int)((CreditCardPayment)memberTranResult.MemberTran.Payment).CreditCardType);
				}
				if (memberTranResult.MemberTran.Plan != null)
				{
					//TL 08212008 Adding 4 params for ESP project to record amount credited, purchase mode, start/end dates
					if (memberTranResult.MemberTran.Plan.CreditAmount != Constants.NULL_DECIMAL && memberTranResult.MemberTran.Plan.CreditAmount > 0)
						command.AddParameter("@TotalCreditedAmount", SqlDbType.Money, ParameterDirection.Input, memberTranResult.MemberTran.Plan.CreditAmount);
				}
				command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranResult.MemberTran.PurchaseMode);
			
				DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
                MemberSub memberSub = null;// PurchaseBL.GetSubscription(memberTranResult.MemberTran.MemberID, memberTranResult.MemberTran.SiteID);
				DateTime startDate = MemberTran.DetermineTransactionStartDate(memberTranResult.MemberTran, updateDate, memberSub);
				DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, memberTranResult.MemberTran, memberSub);
				if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
				{
					command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
					command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
					command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
				}

				// For the credit tool, get the time adjust override type from the plan that was used in the transaction  
				Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType timeAdjustOverrideType = Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.None;
				Matchnet.Purchase.ValueObjects.Plan planToCheckForAdjustOverrideType = memberTranResult.MemberTran.Plan;
				if (planToCheckForAdjustOverrideType == null)
				{
					planToCheckForAdjustOverrideType = PlanBL.Instance.GetPlans(PremiumServiceUtil.getBrand(memberTranResult.MemberTran.SiteID).BrandID).FindByID(memberTranResult.MemberTran.PlanID);
				}
				if (Math.Abs(memberTranResult.MemberTran.Duration) > 0)
				{
					timeAdjustOverrideType = Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.Standard;

					if ((planToCheckForAdjustOverrideType.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile) == Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile)
					{
						timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.HighlightedProfile);
					}
					if ((planToCheckForAdjustOverrideType.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember) == Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember)
					{
						timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.SpotlightMember);
					}
				}
				/*
				// The time adjust override type for a credit tool adjustment is none since no specific time adjustment can be specified  
				// The time is only adjusted from the plan used in the transaction  
				if ((memberTranResult.MemberTran.Plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile) == Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile)
				{
					timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.HighlightedProfile);
				}
				if ((memberTranResult.MemberTran.Plan.PremiumTypeMask & Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember) == Matchnet.Purchase.ValueObjects.PremiumType.SpotlightMember)
				{
					timeAdjustOverrideType = (timeAdjustOverrideType | Matchnet.Purchase.ValueObjects.TimeAdjustOverrideType.SpotlightMember);
				}
				*/
				command.AddParameter("@TimeAdjustOverrideTypeID", SqlDbType.Int, ParameterDirection.Input, (int)timeAdjustOverrideType);

				command.AddParameter("@RenewDate", SqlDbType.SmallDateTime, ParameterDirection.Output);

			}

			if (command != null)
			{
				SyncWriter sw = new SyncWriter();
				Exception ex;
				sw.Execute(command, out ex);
				sw.Dispose();

				if (ex != null)
				{
					throw ex;
				}
			}

			//Update Member attributes
			if (memberTranResult.MemberTran.TranType != TranType.AuthorizationNoTran)
			{
				DateTime renewDate = DateTime.MinValue;
				if (command.Parameters["@RenewDate"].ParameterValue != DBNull.Value)
				{
					renewDate = (DateTime)command.Parameters["@RenewDate"].ParameterValue;
				}

				Trace.WriteLine("__renewDate: " + renewDate.ToString() + " (" + memberTranResult.MemberTran.TranType.ToString() + ")");

				if ((memberTranResult.MemberTranStatus == MemberTranStatus.Success
					|| memberTranResult.MemberTran.TranType == TranType.Renewal
					|| memberTranResult.MemberTran.TranType == TranType.AdministrativeAdjustment
					|| memberTranResult.MemberTran.TranType == TranType.Void) && renewDate != DateTime.MinValue)
				{
					//Update subscription expiration and status
					Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTranResult.MemberTran.MemberID,
						MemberLoadFlags.None);

                    // we need to know if the member's subscription has lapsed before we update the sub expiration date
                    DateTime oldSubExpDate = member.GetAttributeDate(Constants.NULL_INT,
                        memberTranResult.MemberTran.SiteID,
                        Constants.NULL_INT,
                        PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
                        DateTime.MinValue);

                    bool subLapsed = (oldSubExpDate < DateTime.Now);

					member.SetAttributeDate(Constants.NULL_INT,
						memberTranResult.MemberTran.SiteID,
						Constants.NULL_INT,
						PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_EXPIRATION_DATE,
						renewDate);

					member.SetAttributeInt(Constants.NULL_INT,
						memberTranResult.MemberTran.SiteID,
						Constants.NULL_INT,
						PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_STATUS,
						Convert.ToInt16(PurchaseBL.GetMemberSubStatus(member.MemberID, memberTranResult.MemberTran.SiteID, renewDate).Status));

					int communityID = BrandConfigSA.Instance.GetBrandsBySite(memberTranResult.MemberTran.SiteID).GetSite(memberTranResult.MemberTran.SiteID).Community.CommunityID;
					// Used for targeting in GAM, Google Ad Manager
					if (memberTranResult.MemberTran.TranType == TranType.InitialBuy)
					{
						member.SetAttributeDate(Constants.NULL_INT,
							memberTranResult.MemberTran.SiteID,
							Constants.NULL_INT,
							PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_LAST_INITIAL_PURCHASE_DATE,
							DateTime.Now);

                        // If InitialBuy takes place after the sub has lapsed, we need to update another Attribute related to this
                        if (subLapsed)
                        {
                            member.SetAttributeDate(Constants.NULL_INT,
                                memberTranResult.MemberTran.SiteID,
                                Constants.NULL_INT,
                                PurchaseConstants.ATTRIBUTE_SUBSCRIPTION_LAST_INITIAL_PURCHASE_DATE_AFTER_LAPSE,
                                DateTime.Now);
                        }

						// if this is a Photo Requirement site, then we must write to PhotoStatusMask indicating this member is required to
						// have a photo since he subscribed under new terms and conditions
						if(Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTO_REQUIRED_SITE",
							communityID, memberTranResult.MemberTran.SiteID)))
						{
							int brandID;
							member.GetLastLogonDate(communityID, out brandID);

							if(brandID != Constants.NULL_INT)
							{
								int photoStatusMask = member.GetAttributeInt(communityID, memberTranResult.MemberTran.SiteID, brandID, "PhotoStatusMask", 0);
								photoStatusMask = photoStatusMask | (int)Matchnet.Member.ValueObjects.PhotoStatusMask.PhotoRequired;
								member.SetAttributeInt(communityID, memberTranResult.MemberTran.SiteID, brandID, "PhotoStatusMask", photoStatusMask);
							}
						}

					}
					
					MemberSaveResult result = MemberSA.Instance.SaveMember(member);

					if (result.SaveStatus != MemberSaveStatusType.Success)
					{
						throw new BLException("Unable to save subscription expiration attribute for MemberID " + memberTranResult.MemberTran.MemberID.ToString());
					}

					// If this member is a Messmo user, we have to let Messmo know of this subscription end date change.
					bool siteMessmoEnabled = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_UI_ENABLED",
						communityID, memberTranResult.MemberTran.SiteID));

					if(siteMessmoEnabled)
					{
						int messmoCapable = member.GetAttributeInt(communityID, Constants.NULL_INT,	Constants.NULL_INT, "MessmoCapable", 0);

						if(messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Pending ||
							messmoCapable == (int)Matchnet.HTTPMessaging.Constants.MessmoCapableStatus.Capable)
						{
							int languageID = BrandConfigSA.Instance.GetBrandsBySite(memberTranResult.MemberTran.SiteID).GetSite(memberTranResult.MemberTran.SiteID).LanguageID;
							string messmoSubID = member.GetAttributeText(communityID, Constants.NULL_INT, Constants.NULL_INT, languageID,
								"MessmoSubscriberID", string.Empty);
							
							ExternalMailSA.Instance.SendMessmoUserTransStatusRequestBySiteID(memberTranResult.MemberTran.SiteID,
								DateTime.Now, messmoSubID, Matchnet.HTTPMessaging.Constants.MESSMO_USER_TRANS_STATUS_SET_VALID_UNTIL, renewDate);
						}
					}
					
					//Update premium services expiration dates; member attributes
					if (Plan.IsPurchaseModeValidForCredit(memberTranResult.MemberTran.PurchaseMode))
					{
						//specify premium service dates to be the same as the subscription expiration date
						PremiumServiceUtil.setPremiumServiceMember(memberTranResult.MemberTranStatus, memberTranResult.MemberTran, null, renewDate);
					}
					else
					{
						PremiumServiceUtil.setPremiumServiceMember(memberTranResult.MemberTranStatus, memberTranResult.MemberTran, null);
					}
				}

			}

			//08122008 TL
			//Insert into MemberTranCredit for a successful transaction;
			if (memberTranResult.MemberTranStatus == MemberTranStatus.Success 
				&& memberTranResult.MemberTran.MemberTranCreditCollection != null 
				&& memberTranResult.MemberTran.MemberTranCreditCollection.Count > 0)
			{
				Command[] commandArray = new Command[memberTranResult.MemberTran.MemberTranCreditCollection.Count];
				int c = 0;
				foreach (MemberTranCredit mtc in memberTranResult.MemberTran.MemberTranCreditCollection)
				{
					command = new Command("mnSubscription", "dbo.up_MemberTranCredit_Save", 0);
					command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberTranID);
					command.AddParameter("@CreditedFromTranID", SqlDbType.Int, ParameterDirection.Input, mtc.MemberTranID);
					command.AddParameter("@CreditAmount", SqlDbType.Money, ParameterDirection.Input, (mtc.CreditAmount == Constants.NULL_DECIMAL) ? 0m : Decimal.Round(mtc.CreditAmount, 4));

					commandArray[c] = command;
					c++;
				}

				if (commandArray.Length > 0)
				{
					SyncWriter sw2 = new SyncWriter();
					Exception ex2;
					sw2.Execute(commandArray, out ex2);
					sw2.Dispose();

					if (ex2 != null)
					{
						throw ex2;
					}
				}
			}

			//save to mnChargeStage for reporting
			if (memberTranResult.MemberTran.Payment != null)
			{
				Trace.WriteLine("__avsCode: " + memberTranResult.ResponseCodeAVS);
				Trace.WriteLine("__cvvCode: " + memberTranResult.ResponseCodeCVV);
				Command commandChargeStage = new Command("mnChargeStage", "dbo.up_MemberPaymentFlat_Save", 0);
				commandChargeStage.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberPaymentID);
				commandChargeStage.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.MemberID);
				commandChargeStage.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.FirstName);
				commandChargeStage.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.LastName);
				commandChargeStage.AddParameter("@Phone", SqlDbType.VarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.Phone);
				commandChargeStage.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.AddressLine1);
				commandChargeStage.AddParameter("@AddressLine2", SqlDbType.NVarChar, ParameterDirection.Input, null);
				commandChargeStage.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.City);
				commandChargeStage.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.State);
				commandChargeStage.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, memberTranResult.MemberTran.Payment.PostalCode);
				commandChargeStage.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, memberTranResult.MemberTran.Payment.CountryRegionID);
				commandChargeStage.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
				if (memberTranResult.MemberTran.Payment is CreditCardPayment)
				{
					commandChargeStage.AddParameter("@CreditCardExpirationMonth", SqlDbType.TinyInt, ParameterDirection.Input, ((CreditCardPayment)memberTranResult.MemberTran.Payment).ExpirationMonth);
					commandChargeStage.AddParameter("@CreditCardExpirationYear", SqlDbType.Int, ParameterDirection.Input, ((CreditCardPayment)memberTranResult.MemberTran.Payment).ExpirationYear);
					commandChargeStage.AddParameter("@CreditCardHash", SqlDbType.VarChar, ParameterDirection.Input, Crypto.Hash(memberTranResult.MemberTran.AccountNumber + PurchaseConstants.SOOPER_HASH));
				}

				Client.Instance.ExecuteAsyncWrite(commandChargeStage);
			}

			//save request/response XML to mnChargeStage for reporting
			if (memberTranResult.logRequestResponseData)
			{
				this.LogRequestResponseXML(memberTranResult.ResponseData, memberTranResult.RequestDataScrubbed, memberTranResult);
			}

			//09252008 TL - THERE SHOULD BE NO CALL TO PREMIUMSERVICEUTIL HERE, it is called earlier above based on specific conditions
			//PremiumServiceUtil.highLightMember(memberTranResult.MemberTranStatus, memberTranResult.MemberTran, null);
			//Trace.WriteLine("before setPremiumServiceMember");
			//PremiumServiceUtil.setPremiumServiceMember(memberTranResult.MemberTranStatus, memberTranResult.MemberTran, null);
			//Trace.WriteLine("after setPremiumServiceMember");
			//highLightMember(memberTranResult);

			#region Send subscription confirmation email
			Site site =  
				BrandConfigSA.Instance.GetBrandsBySite(memberTranResult.MemberTran.SiteID).GetSite(memberTranResult.MemberTran.SiteID);
            
			// Only send out email on successful initial buy OR free trial
			if (memberTranResult.MemberTranStatus == MemberTranStatus.Success &&
				memberTranResult.MemberTran != null &&
				((memberTranResult.MemberTran.TranType == TranType.InitialBuy) ||
				(memberTranResult.MemberTran.TranType == TranType.AuthorizationOnly)))
			{			
				// is this site configured to send out this alert?
				bool send = Convert.ToBoolean(RuntimeSettings.GetSetting(
					"ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", site.Community.CommunityID, site.SiteID));

				if (send)
				{
					int brandID = Constants.NULL_INT;

					Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTranResult.MemberTran.MemberID,
						MemberLoadFlags.None);

					member.GetLastLogonDate(site.Community.CommunityID, out brandID);

					// get CC info
					string last4CC = string.Empty;
					int creditCardTypeID = Constants.NULL_INT;

					if (memberTranResult.MemberTran.PaymentType == PaymentType.CreditCard)
					{
						CreditCardPayment creditCardPayment = (CreditCardPayment) memberTranResult.MemberTran.Payment;

						if (creditCardPayment != null)
						{
							if (creditCardPayment.CreditCardNumber != null)
							{
								last4CC = creditCardPayment.CreditCardNumber.Substring(creditCardPayment.CreditCardNumber.Length - 3);
							}

							creditCardTypeID = (int) creditCardPayment.CreditCardType;
						}
					}

					// determine plan type for this email 
					int planType = Constants.NULL_INT;
					if ((memberTranResult.MemberTran.Plan.PlanTypeMask & PlanType.Installment) == PlanType.Installment)
					{
						planType = (int) PlanType.Installment;
					}
					else if ((memberTranResult.MemberTran.Plan.PlanTypeMask & PlanType.FreeTrialWelcome) == PlanType.FreeTrialWelcome)
					{
						planType = (int) PlanType.FreeTrialWelcome;
					}
					else if ((memberTranResult.MemberTran.Plan.PlanTypeMask & PlanType.OneTimeOnly) == PlanType.OneTimeOnly)
					{
						planType = (int) PlanType.OneTimeOnly;
					}
					else
					{
						planType = (int) PlanType.Regular;
					}

					// Do not send email when it's an initial buy from a Free Trial. Sub confirm email will be sent out on initial auth Free Trial.
					if ((planType == (int) PlanType.FreeTrialWelcome) && (memberTranResult.MemberTran.TranType == TranType.InitialBuy))
					{
						return;
					}

					Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
																	 
					ExternalMailSA.Instance.SendSubscriptionConfirmation(
						member.MemberID,
						site.SiteID,
						brandID,
						member.GetUserName(brand),
						member.EmailAddress,
						memberTranResult.MemberTran.Payment.FirstName,
						memberTranResult.MemberTran.Payment.LastName,
						memberTranResult.MemberTran.Payment.AddressLine1,
						memberTranResult.MemberTran.Payment.City,
						memberTranResult.MemberTran.Payment.State,
						memberTranResult.MemberTran.Payment.PostalCode,
						memberTranResult.MemberTran.InsertDate,
						planType,
						(int) memberTranResult.MemberTran.Plan.CurrencyType,
						memberTranResult.MemberTran.Plan.InitialCost,
						memberTranResult.MemberTran.Plan.InitialDuration,
						memberTranResult.MemberTran.Plan.InitialDurationType,
						memberTranResult.MemberTran.Plan.RenewCost,
						memberTranResult.MemberTran.Plan.RenewDuration,
						memberTranResult.MemberTran.Plan.RenewDurationType,
						last4CC,
						creditCardTypeID,
						memberTranResult.MemberTran.AccountNumber);
				}
			}
			else if (memberTranResult.MemberTranStatus == MemberTranStatus.Success &&
				memberTranResult.MemberTran == null)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error sending subscription confirmation e-mail.  MemberTran was null.");
			}
			#endregion
			
			#region Send email to user if renewal failed.
			
			// Check if current transaction is for subscription renewal and if the current site has this (failed email notification) enabled.
			if (memberTranResult.MemberTran != null)
			{
				if (memberTranResult.MemberTran.TranType == TranType.Renewal && Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_SUBSCRIPTION_RENEWAL_FAILURE_EMAIL", site.Community.CommunityID, site.SiteID)))
				{
					if (memberTranResult.MemberTranStatus == MemberTranStatus.Failure && memberTranResult.ResponseCode != null &&  memberTranResult.ResponseCode != string.Empty)
					{
						if (HardDeclineCodes.BinarySearch(memberTranResult.ResponseCode) >= 0)
						{
							Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberTranResult.MemberTran.MemberID, MemberLoadFlags.None);
						
							int brandID = Constants.NULL_INT;
							member.GetLastLogonDate(site.Community.CommunityID, out brandID);
							Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

							ExternalMailSA.Instance.SendRenewalFailureEmail(site.SiteID, member.GetUserName(brand), member.EmailAddress);
						
							Logger.Instance.Log(Logger.LoggerType.RenewalFailureNotification, site.SiteID.ToString() + "\t" + member.MemberID.ToString() + "\t" + member.EmailAddress.ToString() + "\t" + memberTranResult.ResponseCode.ToString());
						}
					}
				}			
			}			

			#endregion
		}
	}
}
