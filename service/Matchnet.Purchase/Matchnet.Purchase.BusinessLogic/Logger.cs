using System;
using System.IO;

namespace Matchnet.Purchase.BusinessLogic
{	
	/// <summary>
	/// 
	/// </summary>
	public class Logger : IDisposable
	{
		/// <summary>
		/// 
		/// </summary>
		public enum LoggerType
		{
			/// <summary>
			/// 
			/// </summary>
			RenewalFailureNotification	
		}
		
		private const Int32 LOG_FLUSH_INTERVAL = 60; //In seconds

		private StreamWriter _logFileWriter;
		private DateTime _lastFlushTime;

		/// <summary>
		/// Logging is turned on through app.config setting, LocalLogging.
		/// This should be turned on for debugging usage only.
		/// </summary>
		public static Logger Instance = new Logger();
		
		private Logger()
		{
			string year = DateTime.Now.Year.ToString();
			string month = DateTime.Now.Month.ToString();
			string day = DateTime.Now.Day.ToString();
			
			string logFileName = "Matchnet.Purchase.Log-" + year + "-" + month + "-" + day + ".txt";
			string logFilePath = System.Environment.CurrentDirectory + "\\Logs\\" + logFileName;
			
			// create the logging directory
			if (!Directory.Exists(System.Environment.CurrentDirectory + "\\Logs"))
			{
				Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\Logs");
			}
			
			// if the file doesn't exist, create it
			if (!System.IO.File.Exists(logFilePath)) 
			{
				FileStream fs = System.IO.File.Create(logFilePath);
				fs.Close();
			}

			_logFileWriter = System.IO.File.AppendText(logFilePath);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="loggerType"></param>
		/// <param name="text"></param>
		public void Log(LoggerType loggerType, string text)
		{
			if (System.Configuration.ConfigurationSettings.AppSettings["LocalLogging"] != "true")
				return;
			
			lock (_logFileWriter)
			{
				_logFileWriter.WriteLine(System.DateTime.Now + "\t" + loggerType.ToString() + text);

				if (DateTime.Now.Subtract(_lastFlushTime).TotalSeconds > LOG_FLUSH_INTERVAL)
				{
					_logFileWriter.Flush();
					_lastFlushTime = DateTime.Now;
				}
			}
		}

		#region IDisposable Members

		/// <summary>
		/// 
		/// </summary>
		public void Dispose()
		{
			if (_logFileWriter != null)
			{
				_logFileWriter.Close();
			}
		}

		#endregion
	}
}
