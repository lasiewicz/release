using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.RemotingServices;
using Matchnet.Purchase.ServiceManagers;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Purchase.Service
{
	public class PurchaseService : RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private PurchaseSM _purchaseSM;
		private PlanSM _planSM;


		public PurchaseService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PurchaseService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void RegisterServiceManagers()
		{
			_purchaseSM = new PurchaseSM();
			_planSM = new PlanSM();
			
			base.RegisterServiceManager(_purchaseSM);
			base.RegisterServiceManager(_planSM);
			base.RegisterServiceManagers ();
		}
	}
}
