﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.2032
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace Matchnet.StormpostExport.Harness {
    using System;
    using System.Data;
    using System.Xml;
    using System.Runtime.Serialization;
    
    
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.ToolboxItem(true)]
    public class MemberAttribute : DataSet {
        
        private MemberAttributeIntDataTable tableMemberAttributeInt;
        
        public MemberAttribute() {
            this.InitClass();
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        protected MemberAttribute(SerializationInfo info, StreamingContext context) {
            string strSchema = ((string)(info.GetValue("XmlSchema", typeof(string))));
            if ((strSchema != null)) {
                DataSet ds = new DataSet();
                ds.ReadXmlSchema(new XmlTextReader(new System.IO.StringReader(strSchema)));
                if ((ds.Tables["MemberAttributeInt"] != null)) {
                    this.Tables.Add(new MemberAttributeIntDataTable(ds.Tables["MemberAttributeInt"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else {
                this.InitClass();
            }
            this.GetSerializationData(info, context);
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        [System.ComponentModel.Browsable(false)]
        [System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public MemberAttributeIntDataTable MemberAttributeInt {
            get {
                return this.tableMemberAttributeInt;
            }
        }
        
        public override DataSet Clone() {
            MemberAttribute cln = ((MemberAttribute)(base.Clone()));
            cln.InitVars();
            return cln;
        }
        
        protected override bool ShouldSerializeTables() {
            return false;
        }
        
        protected override bool ShouldSerializeRelations() {
            return false;
        }
        
        protected override void ReadXmlSerializable(XmlReader reader) {
            this.Reset();
            DataSet ds = new DataSet();
            ds.ReadXml(reader);
            if ((ds.Tables["MemberAttributeInt"] != null)) {
                this.Tables.Add(new MemberAttributeIntDataTable(ds.Tables["MemberAttributeInt"]));
            }
            this.DataSetName = ds.DataSetName;
            this.Prefix = ds.Prefix;
            this.Namespace = ds.Namespace;
            this.Locale = ds.Locale;
            this.CaseSensitive = ds.CaseSensitive;
            this.EnforceConstraints = ds.EnforceConstraints;
            this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
            this.InitVars();
        }
        
        protected override System.Xml.Schema.XmlSchema GetSchemaSerializable() {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            this.WriteXmlSchema(new XmlTextWriter(stream, null));
            stream.Position = 0;
            return System.Xml.Schema.XmlSchema.Read(new XmlTextReader(stream), null);
        }
        
        internal void InitVars() {
            this.tableMemberAttributeInt = ((MemberAttributeIntDataTable)(this.Tables["MemberAttributeInt"]));
            if ((this.tableMemberAttributeInt != null)) {
                this.tableMemberAttributeInt.InitVars();
            }
        }
        
        private void InitClass() {
            this.DataSetName = "MemberAttribute";
            this.Prefix = "";
            this.Namespace = "http://tempuri.org/MemberAttribute.xsd";
            this.Locale = new System.Globalization.CultureInfo("en-US");
            this.CaseSensitive = false;
            this.EnforceConstraints = true;
            this.tableMemberAttributeInt = new MemberAttributeIntDataTable();
            this.Tables.Add(this.tableMemberAttributeInt);
        }
        
        private bool ShouldSerializeMemberAttributeInt() {
            return false;
        }
        
        private void SchemaChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e) {
            if ((e.Action == System.ComponentModel.CollectionChangeAction.Remove)) {
                this.InitVars();
            }
        }
        
        public delegate void MemberAttributeIntRowChangeEventHandler(object sender, MemberAttributeIntRowChangeEvent e);
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class MemberAttributeIntDataTable : DataTable, System.Collections.IEnumerable {
            
            private DataColumn columnAttributeValue;
            
            private DataColumn columnAttributeName;
            
            internal MemberAttributeIntDataTable() : 
                    base("MemberAttributeInt") {
                this.InitClass();
            }
            
            internal MemberAttributeIntDataTable(DataTable table) : 
                    base(table.TableName) {
                if ((table.CaseSensitive != table.DataSet.CaseSensitive)) {
                    this.CaseSensitive = table.CaseSensitive;
                }
                if ((table.Locale.ToString() != table.DataSet.Locale.ToString())) {
                    this.Locale = table.Locale;
                }
                if ((table.Namespace != table.DataSet.Namespace)) {
                    this.Namespace = table.Namespace;
                }
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
                this.DisplayExpression = table.DisplayExpression;
            }
            
            [System.ComponentModel.Browsable(false)]
            public int Count {
                get {
                    return this.Rows.Count;
                }
            }
            
            internal DataColumn AttributeValueColumn {
                get {
                    return this.columnAttributeValue;
                }
            }
            
            internal DataColumn AttributeNameColumn {
                get {
                    return this.columnAttributeName;
                }
            }
            
            public MemberAttributeIntRow this[int index] {
                get {
                    return ((MemberAttributeIntRow)(this.Rows[index]));
                }
            }
            
            public event MemberAttributeIntRowChangeEventHandler MemberAttributeIntRowChanged;
            
            public event MemberAttributeIntRowChangeEventHandler MemberAttributeIntRowChanging;
            
            public event MemberAttributeIntRowChangeEventHandler MemberAttributeIntRowDeleted;
            
            public event MemberAttributeIntRowChangeEventHandler MemberAttributeIntRowDeleting;
            
            public void AddMemberAttributeIntRow(MemberAttributeIntRow row) {
                this.Rows.Add(row);
            }
            
            public MemberAttributeIntRow AddMemberAttributeIntRow(int AttributeValue, string AttributeName) {
                MemberAttributeIntRow rowMemberAttributeIntRow = ((MemberAttributeIntRow)(this.NewRow()));
                rowMemberAttributeIntRow.ItemArray = new object[] {
                        AttributeValue,
                        AttributeName};
                this.Rows.Add(rowMemberAttributeIntRow);
                return rowMemberAttributeIntRow;
            }
            
            public System.Collections.IEnumerator GetEnumerator() {
                return this.Rows.GetEnumerator();
            }
            
            public override DataTable Clone() {
                MemberAttributeIntDataTable cln = ((MemberAttributeIntDataTable)(base.Clone()));
                cln.InitVars();
                return cln;
            }
            
            protected override DataTable CreateInstance() {
                return new MemberAttributeIntDataTable();
            }
            
            internal void InitVars() {
                this.columnAttributeValue = this.Columns["AttributeValue"];
                this.columnAttributeName = this.Columns["AttributeName"];
            }
            
            private void InitClass() {
                this.columnAttributeValue = new DataColumn("AttributeValue", typeof(int), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnAttributeValue);
                this.columnAttributeName = new DataColumn("AttributeName", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnAttributeName);
            }
            
            public MemberAttributeIntRow NewMemberAttributeIntRow() {
                return ((MemberAttributeIntRow)(this.NewRow()));
            }
            
            protected override DataRow NewRowFromBuilder(DataRowBuilder builder) {
                return new MemberAttributeIntRow(builder);
            }
            
            protected override System.Type GetRowType() {
                return typeof(MemberAttributeIntRow);
            }
            
            protected override void OnRowChanged(DataRowChangeEventArgs e) {
                base.OnRowChanged(e);
                if ((this.MemberAttributeIntRowChanged != null)) {
                    this.MemberAttributeIntRowChanged(this, new MemberAttributeIntRowChangeEvent(((MemberAttributeIntRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowChanging(DataRowChangeEventArgs e) {
                base.OnRowChanging(e);
                if ((this.MemberAttributeIntRowChanging != null)) {
                    this.MemberAttributeIntRowChanging(this, new MemberAttributeIntRowChangeEvent(((MemberAttributeIntRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleted(DataRowChangeEventArgs e) {
                base.OnRowDeleted(e);
                if ((this.MemberAttributeIntRowDeleted != null)) {
                    this.MemberAttributeIntRowDeleted(this, new MemberAttributeIntRowChangeEvent(((MemberAttributeIntRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleting(DataRowChangeEventArgs e) {
                base.OnRowDeleting(e);
                if ((this.MemberAttributeIntRowDeleting != null)) {
                    this.MemberAttributeIntRowDeleting(this, new MemberAttributeIntRowChangeEvent(((MemberAttributeIntRow)(e.Row)), e.Action));
                }
            }
            
            public void RemoveMemberAttributeIntRow(MemberAttributeIntRow row) {
                this.Rows.Remove(row);
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class MemberAttributeIntRow : DataRow {
            
            private MemberAttributeIntDataTable tableMemberAttributeInt;
            
            internal MemberAttributeIntRow(DataRowBuilder rb) : 
                    base(rb) {
                this.tableMemberAttributeInt = ((MemberAttributeIntDataTable)(this.Table));
            }
            
            public int AttributeValue {
                get {
                    try {
                        return ((int)(this[this.tableMemberAttributeInt.AttributeValueColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tableMemberAttributeInt.AttributeValueColumn] = value;
                }
            }
            
            public string AttributeName {
                get {
                    try {
                        return ((string)(this[this.tableMemberAttributeInt.AttributeNameColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tableMemberAttributeInt.AttributeNameColumn] = value;
                }
            }
            
            public bool IsAttributeValueNull() {
                return this.IsNull(this.tableMemberAttributeInt.AttributeValueColumn);
            }
            
            public void SetAttributeValueNull() {
                this[this.tableMemberAttributeInt.AttributeValueColumn] = System.Convert.DBNull;
            }
            
            public bool IsAttributeNameNull() {
                return this.IsNull(this.tableMemberAttributeInt.AttributeNameColumn);
            }
            
            public void SetAttributeNameNull() {
                this[this.tableMemberAttributeInt.AttributeNameColumn] = System.Convert.DBNull;
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class MemberAttributeIntRowChangeEvent : EventArgs {
            
            private MemberAttributeIntRow eventRow;
            
            private DataRowAction eventAction;
            
            public MemberAttributeIntRowChangeEvent(MemberAttributeIntRow row, DataRowAction action) {
                this.eventRow = row;
                this.eventAction = action;
            }
            
            public MemberAttributeIntRow Row {
                get {
                    return this.eventRow;
                }
            }
            
            public DataRowAction Action {
                get {
                    return this.eventAction;
                }
            }
        }
    }
}
