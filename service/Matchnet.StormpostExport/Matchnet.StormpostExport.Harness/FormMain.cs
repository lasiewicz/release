using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.StormpostExport.ValueObjects.QueueItem;
using Matchnet.StormpostExport.ValueObjects;
using Matchnet.StormpostExport.BusinessLogic;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Data;

namespace Matchnet.StormpostExport.Harness
{
	/// <summary>
	/// Summary description for FormMain.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		private Matchnet.StormpostExport.Harness.Queue _dsQueueItems;
		private System.Windows.Forms.Button Run;
		private System.Windows.Forms.Button Stop;
		private System.Windows.Forms.DataGrid Before;
		private System.Windows.Forms.DataGrid After;
		private System.Windows.Forms.StatusBar StatusBar;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.Button btnPopulateGrid;
		private System.Windows.Forms.Button btnQueueProcessSelected;
		private System.Windows.Forms.DataGrid QueueItemsGrid;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormMain()
		{
			InitializeComponent();
			//
			// Required for Windows Form Designer support
			//
			//InitializeGridData();
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormMain());
		}

		private void InitializeGridData(int MemberID)
		{
			try 
			{
				_dsQueueItems.Clear();
				_dsQueueItems.QueueData.AddQueueDataRow(39,MemberID,1,1,-2,-2,1, DateTime.Now); // Self Suspended Flag (Set as slef suspended)
				_dsQueueItems.QueueData.AddQueueDataRow(40,MemberID,1,2,-2,-2,1, DateTime.Now); // Self Suspended Flag (clear it)
				_dsQueueItems.QueueData.AddQueueDataRow(42,MemberID,2,1,44,5,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(43,MemberID,2,1,46,7,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(44,MemberID,1,1,3,3,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(45,MemberID,1,1,4,4,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(46,MemberID,1,1,26,5,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(47,MemberID,1,1,27,6,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(48,MemberID,1,1,28,7,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(49,MemberID,1,1,29,8,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(50,MemberID,1,1,30,9,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(51,MemberID,1,1,31,10,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(52,MemberID,1,2,-1,-1,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(53,MemberID,1,2,2,2,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(54,MemberID,1,2,3,3,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(55,MemberID,1,2,4,4,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(56,MemberID,1,2,26,5,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(57,MemberID,1,2,27,6,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(58,MemberID,1,2,28,7,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(59,MemberID,1,2,29,8,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(60,MemberID,1,2,30,9,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(61,MemberID,1,2,31,10,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(62,MemberID,1,1,-1,-1,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(63,MemberID,1,1,2,2,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(64,MemberID,1,1,3,3,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(65,MemberID,1,1,4,4,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(66,MemberID,1,1,26,5,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(67,MemberID,1,1,27,6,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(68,MemberID,1,1,28,7,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(69,MemberID,1,1,29,8,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(70,MemberID,1,1,30,9,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(71,MemberID,1,1,31,10,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(72,MemberID,1,2,-1,-1,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(73,MemberID,1,2,2,2,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(74,MemberID,1,2,3,3,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(75,MemberID,1,2,4,4,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(76,MemberID,1,2,26,5,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(77,MemberID,1,2,27,6,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(78,MemberID,1,2,28,7,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(79,MemberID,1,2,29,8,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(80,MemberID,1,2,30,9,1, DateTime.Now);
				_dsQueueItems.QueueData.AddQueueDataRow(81,MemberID,1,2,31,10,1, DateTime.Now);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.ToString());
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Run = new System.Windows.Forms.Button();
			this.Stop = new System.Windows.Forms.Button();
			this.QueueItemsGrid = new System.Windows.Forms.DataGrid();
			this.btnQueueProcessSelected = new System.Windows.Forms.Button();
			this.Before = new System.Windows.Forms.DataGrid();
			this.After = new System.Windows.Forms.DataGrid();
			this.StatusBar = new System.Windows.Forms.StatusBar();
			this.txtMemberID = new System.Windows.Forms.TextBox();
			this.btnPopulateGrid = new System.Windows.Forms.Button();
			this._dsQueueItems = new Matchnet.StormpostExport.Harness.Queue();
			((System.ComponentModel.ISupportInitialize)(this.QueueItemsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Before)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.After)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._dsQueueItems)).BeginInit();
			this.SuspendLayout();
			//
			//_dsQueueItems 
			//
			this._dsQueueItems.DataSetName = "QueueItemsData";
			this._dsQueueItems.Locale = new System.Globalization.CultureInfo("en-US");
			// 
			// Run
			// 
			this.Run.Location = new System.Drawing.Point(648, 8);
			this.Run.Name = "Run";
			this.Run.TabIndex = 7;
			this.Run.Text = "&Run";
			this.Run.Click += new System.EventHandler(this.Run_Click);
			// 
			// Stop
			// 
			this.Stop.Location = new System.Drawing.Point(736, 8);
			this.Stop.Name = "Stop";
			this.Stop.TabIndex = 8;
			this.Stop.Text = "&Stop";
			this.Stop.Click += new System.EventHandler(this.Stop_Click);
			// 
			// QueueItemsGrid
			// 
			this.QueueItemsGrid.AlternatingBackColor = System.Drawing.Color.Lavender;
			this.QueueItemsGrid.BackColor = System.Drawing.Color.WhiteSmoke;
			this.QueueItemsGrid.BackgroundColor = System.Drawing.Color.LightGray;
			this.QueueItemsGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.QueueItemsGrid.CaptionBackColor = System.Drawing.Color.LightSteelBlue;
			this.QueueItemsGrid.CaptionForeColor = System.Drawing.Color.MidnightBlue;
			this.QueueItemsGrid.DataMember = "";
			this.QueueItemsGrid.DataSource = this._dsQueueItems.QueueData;
			this.QueueItemsGrid.FlatMode = true;
			this.QueueItemsGrid.Font = new System.Drawing.Font("Tahoma", 8F);
			this.QueueItemsGrid.ForeColor = System.Drawing.Color.MidnightBlue;
			this.QueueItemsGrid.GridLineColor = System.Drawing.Color.Gainsboro;
			this.QueueItemsGrid.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None;
			this.QueueItemsGrid.HeaderBackColor = System.Drawing.Color.MidnightBlue;
			this.QueueItemsGrid.HeaderFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.QueueItemsGrid.HeaderForeColor = System.Drawing.Color.WhiteSmoke;
			this.QueueItemsGrid.LinkColor = System.Drawing.Color.Teal;
			this.QueueItemsGrid.Location = new System.Drawing.Point(16, 72);
			this.QueueItemsGrid.Name = "QueueItemsGrid";
			this.QueueItemsGrid.ParentRowsBackColor = System.Drawing.Color.Gainsboro;
			this.QueueItemsGrid.ParentRowsForeColor = System.Drawing.Color.MidnightBlue;
			this.QueueItemsGrid.SelectionBackColor = System.Drawing.Color.CadetBlue;
			this.QueueItemsGrid.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
			this.QueueItemsGrid.Size = new System.Drawing.Size(800, 232);
			this.QueueItemsGrid.TabIndex = 0;
			// 
			// btnQueueProcessSelected
			// 
			this.btnQueueProcessSelected.Enabled = false;
			this.btnQueueProcessSelected.Location = new System.Drawing.Point(24, 40);
			this.btnQueueProcessSelected.Name = "btnQueueProcessSelected";
			this.btnQueueProcessSelected.Size = new System.Drawing.Size(216, 24);
			this.btnQueueProcessSelected.TabIndex = 9;
			this.btnQueueProcessSelected.Text = "Process Selected Grid Item";
			this.btnQueueProcessSelected.Click += new System.EventHandler(this.QueueItemProcessSelected_Click);
			// 
			// Before
			// 
			this.Before.AlternatingBackColor = System.Drawing.Color.White;
			this.Before.BackColor = System.Drawing.Color.White;
			this.Before.BackgroundColor = System.Drawing.Color.Ivory;
			this.Before.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Before.CaptionBackColor = System.Drawing.Color.DarkSlateBlue;
			this.Before.CaptionFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Before.CaptionForeColor = System.Drawing.Color.Lavender;
			this.Before.CaptionText = "Before";
			this.Before.DataMember = "";
			this.Before.FlatMode = true;
			this.Before.Font = new System.Drawing.Font("Tahoma", 8F);
			this.Before.ForeColor = System.Drawing.Color.Black;
			this.Before.GridLineColor = System.Drawing.Color.Wheat;
			this.Before.HeaderBackColor = System.Drawing.Color.CadetBlue;
			this.Before.HeaderFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.Before.HeaderForeColor = System.Drawing.Color.Black;
			this.Before.LinkColor = System.Drawing.Color.DarkSlateBlue;
			this.Before.Location = new System.Drawing.Point(16, 312);
			this.Before.Name = "Before";
			this.Before.ParentRowsBackColor = System.Drawing.Color.Ivory;
			this.Before.ParentRowsForeColor = System.Drawing.Color.Black;
			this.Before.SelectionBackColor = System.Drawing.Color.Wheat;
			this.Before.SelectionForeColor = System.Drawing.Color.DarkSlateBlue;
			this.Before.Size = new System.Drawing.Size(392, 280);
			this.Before.TabIndex = 10;
			// 
			// After
			// 
			this.After.AlternatingBackColor = System.Drawing.Color.White;
			this.After.BackColor = System.Drawing.Color.White;
			this.After.BackgroundColor = System.Drawing.Color.Ivory;
			this.After.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.After.CaptionBackColor = System.Drawing.Color.DarkSlateBlue;
			this.After.CaptionFont = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.After.CaptionForeColor = System.Drawing.Color.Lavender;
			this.After.CaptionText = "After";
			this.After.DataMember = "";
			this.After.FlatMode = true;
			this.After.Font = new System.Drawing.Font("Tahoma", 8F);
			this.After.ForeColor = System.Drawing.Color.Black;
			this.After.GridLineColor = System.Drawing.Color.Wheat;
			this.After.HeaderBackColor = System.Drawing.Color.CadetBlue;
			this.After.HeaderFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.After.HeaderForeColor = System.Drawing.Color.Black;
			this.After.LinkColor = System.Drawing.Color.DarkSlateBlue;
			this.After.Location = new System.Drawing.Point(424, 312);
			this.After.Name = "After";
			this.After.ParentRowsBackColor = System.Drawing.Color.Ivory;
			this.After.ParentRowsForeColor = System.Drawing.Color.Black;
			this.After.SelectionBackColor = System.Drawing.Color.Wheat;
			this.After.SelectionForeColor = System.Drawing.Color.DarkSlateBlue;
			this.After.Size = new System.Drawing.Size(392, 280);
			this.After.TabIndex = 11;
			// 
			// StatusBar
			// 
			this.StatusBar.Location = new System.Drawing.Point(0, 599);
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Size = new System.Drawing.Size(832, 22);
			this.StatusBar.TabIndex = 14;
			// 
			// txtMemberID
			// 
			this.txtMemberID.Location = new System.Drawing.Point(24, 8);
			this.txtMemberID.Name = "txtMemberID";
			this.txtMemberID.Size = new System.Drawing.Size(104, 20);
			this.txtMemberID.TabIndex = 15;
			this.txtMemberID.Text = "{memberid}";
			this.txtMemberID.TextChanged += new System.EventHandler(this.txtMemberID_TextChanged);
			this.txtMemberID.Enter += new System.EventHandler(this.txtMemberID_Enter);
			// 
			// btnPopulateGrid
			// 
			this.btnPopulateGrid.Enabled = false;
			this.btnPopulateGrid.Location = new System.Drawing.Point(136, 8);
			this.btnPopulateGrid.Name = "btnPopulateGrid";
			this.btnPopulateGrid.Size = new System.Drawing.Size(104, 24);
			this.btnPopulateGrid.TabIndex = 16;
			this.btnPopulateGrid.Text = "Populate Grid";
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(832, 621);
			this.Controls.Add(this.btnPopulateGrid);
			this.Controls.Add(this.txtMemberID);
			this.Controls.Add(this.StatusBar);
			this.Controls.Add(this.After);
			this.Controls.Add(this.Before);
			this.Controls.Add(this.btnQueueProcessSelected);
			this.Controls.Add(this.QueueItemsGrid);
			this.Controls.Add(this.Stop);
			this.Controls.Add(this.Run);
			this.Name = "FormMain";
			this.Text = "FormMain";
			((System.ComponentModel.ISupportInitialize)(this.QueueItemsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Before)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.After)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._dsQueueItems)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void Run_Click(object sender, System.EventArgs e)
		{
			QueueItemsBL.Instance.Run();
		}

		private void Stop_Click(object sender, System.EventArgs e)
		{
			QueueItemsBL.Instance.Stop();
		}

		private void QueueItemProcessSelected_Click(object sender, System.EventArgs e)
		{
			QueueItem item = PopulateItemFromGridSelection();

			// Before
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(item.MemberID, MemberLoadFlags.None);
			
			Before.DataSource = getMemberAttributeInts(member);
			Before.Refresh();
			
			// Process
			try
			{
				QueueItemsBL.Instance.ProcessItem(item);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			// After
			member
				= MemberSA.Instance.GetMember(item.MemberID, MemberLoadFlags.None);

			After.DataSource = getMemberAttributeInts(member);
			After.Refresh();

			StatusBar.Text = "Executed @ " + DateTime.Now.ToString();
		}

		private QueueItem PopulateItemFromGridSelection()
		{
			return new QueueItem( 
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].QueueID ,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].MemberID ,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].CommunityID,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].ActionID ,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].ListID ,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].ListTypeID ,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].StatusID ,
				_dsQueueItems.QueueData[QueueItemsGrid.CurrentRowIndex].InsertDate
				);
		}

		private MemberAttributeInts getMemberAttributeInts(Matchnet.Member.ServiceAdapters.Member member)
		{
			MemberAttributeInts memberAttributeInts = 
				new MemberAttributeInts();

			memberAttributeInts.Add(new MemberAttributeInt("GotAClickEmailPeriod", 
				member.GetAttributeInt(1, 0, 0, "GotAClickEmailPeriod", 0)));
			
			memberAttributeInts.Add(new MemberAttributeInt("GlobalStatusMask", 
				member.GetAttributeInt(1, 0, 0, "GlobalStatusMask", 0)));

			memberAttributeInts.Add(new MemberAttributeInt("MatchNewsletterPeriod", 
				member.GetAttributeInt(1, 0, 0, "MatchNewsletterPeriod", 0)));

			memberAttributeInts.Add(new MemberAttributeInt("SelfSuspendedFlag", 
				member.GetAttributeInt(1, 0, 0, "SelfSuspendedFlag", 0)));

			return memberAttributeInts;
		}

		private void txtMemberID_Enter(object sender, System.EventArgs e)
		{
			btnPopulateGrid.Enabled = false;
			btnQueueProcessSelected.Enabled = false;
			txtMemberID.Text = "";
		}

		private void txtMemberID_TextChanged(object sender, System.EventArgs e)
		{
			try 
			{
				if (txtMemberID.Text != string.Empty)
				{
					InitializeGridData(Int32.Parse(txtMemberID.Text));
					btnPopulateGrid.Enabled = true;
					btnQueueProcessSelected.Enabled = true;

				}
			}
			catch 
			{}

		}
	}

	public class MemberAttributeInt
	{
		private String _attributeName;
		private Int32 _attributeValue;

		public String AttributeName
		{
			get
			{
				return _attributeName;
			}
			set
			{
				_attributeName = value;
			}
		}
		public Int32 AttributeValue
		{
			get
			{
				return _attributeValue;
			}
			set
			{
				_attributeValue = value;
			}
		}

		public MemberAttributeInt(string attributeName, int attributeValue)
		{
			_attributeName = attributeName;
			_attributeValue = attributeValue;
		}
	}


	public class MemberAttributeInts : System.Collections.CollectionBase
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public MemberAttributeInts()
		{
		}

		/// <summary>
		/// Indexer for getting item an index
		/// </summary>
		public MemberAttributeInt this[int index]
		{
			get 
			{ 
				return ((MemberAttributeInt)base.InnerList[index]); 
			}
		}

		/// <summary>
		/// Add a queue item to this list
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public int Add(MemberAttributeInt item)
		{
			return base.InnerList.Add(item);
		}
	}
}
