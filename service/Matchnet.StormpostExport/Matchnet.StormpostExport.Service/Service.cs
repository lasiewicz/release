using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.ServiceProcess;
using System.Diagnostics;
using System.Threading;

using Matchnet.StormpostExport.BusinessLogic;

namespace Matchnet.StormpostExport.Service
{
	public class Service : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private Thread _runningThread;

		public Service()
		{
			InitializeComponent();
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new Service() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);

		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) 
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			_runningThread = new Thread(new ThreadStart(QueueItemsBL.Instance.Run));
			_runningThread.Start();
			Metrics.Instance.ResetCounters();
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			_runningThread = null;
		}
	}
}
