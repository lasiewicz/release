using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

using Matchnet.StormpostExport.BusinessLogic;

namespace Matchnet.StormpostExport.Service
{
	/// <summary>
	/// Summary description for Installer1.
	/// </summary>
	[RunInstaller(true)]
	public class Installer : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceInstaller serviceInstaller;
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Installer()
		{
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();
			this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			// 
			// serviceProcessInstaller
			// 
			this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
			this.serviceProcessInstaller.Password = null;
			this.serviceProcessInstaller.Username = null;
			this.serviceInstaller.ServiceName = MetricsInstaller.COUNTER_CATEGORY;

			// 
			// Installer
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceInstaller,
																					  this.serviceProcessInstaller});

			// Add events for metrics
			this.AfterUninstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterUninstall);
			this.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterInstall);
		}

		#endregion

		private void ProjectInstaller_AfterInstall(object sender, System.Configuration.Install.InstallEventArgs e) 
		{
			MetricsInstaller.Install();
		}

		private void ProjectInstaller_AfterUninstall(object sender, System.Configuration.Install.InstallEventArgs e) 
		{
			MetricsInstaller.Uninstall();
		}

	}
}
