using System;
using System.Data;

namespace Matchnet.StormpostExport.ValueObjects.QueueItem
{
	/// <summary>
	/// Encapsulates a QueueItem which is a row from the Queue table in the stormpost export DB
	/// </summary>
	public class QueueItem
	{
		#region Private Variables
		private int _QueueItemID;
		private int _MemberID;
		private int _CommunityID;
		private int _ActionID;
		private int _ListID;
		private int _ListTypeID;
		private int _StatusID;
		private DateTime _InsertDate;
		#endregion

		#region Public Properties

		/// <summary>
		/// Queue ID from the identity column
		/// </summary>
		public int QueueItemID 
		{
			get 
			{
				return _QueueItemID;
			}
			set 
			{
				_QueueItemID = value;
			}
		}

		/// <summary>
		/// MemberID
		/// </summary>
		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}
			set 
			{
				_MemberID = value;
			}
		}

		/// <summary>
		/// CommunityID
		/// </summary>
		public int CommunityID 
		{
			get 
			{
				return _CommunityID;
			}
			set 
			{
				_CommunityID = value;
			}
		}

		/// <summary>
		/// ActionID to take
		/// </summary>
		public int ActionID 
		{
			get 
			{
				return _ActionID;
			}
			set 
			{
				_ActionID = value;
			}
		}

		/// <summary>
		/// ListID
		/// </summary>
		public int ListID 
		{
			get 
			{
				return _ListID;
			}
			set 
			{
				_ListID = value;
			}
		}

		/// <summary>
		/// ListTypeID
		/// </summary>
		public int ListTypeID 
		{
			get 
			{
				return _ListTypeID;
			}
			set 
			{
				_ListTypeID = value;
			}
		}

		/// <summary>
		/// StatusID
		/// </summary>
		public int StatusID 
		{
			get 
			{
				return _StatusID;
			}
			set 
			{
				_StatusID = value;
			}
		}

		/// <summary>
		/// Date item was exprted
		/// </summary>
		public DateTime InsertDateTime 
		{
			get 
			{
				return _InsertDate;
			}
			set 
			{
				_InsertDate = value;
			}
		}

		#endregion

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="QueueItemID"></param>
		/// <param name="MemberID"></param>
		/// <param name="CommunityID"></param>
		/// <param name="ActionID"></param>
		/// <param name="ListID"></param>
		/// <param name="ListTypeID"></param>
		/// <param name="StatusID"></param>
		/// <param name="InsertDate"></param>
		public QueueItem(int QueueItemID, int MemberID, int CommunityID, int ActionID, int ListID, int ListTypeID, int StatusID, DateTime InsertDate )
		{	
			_QueueItemID = QueueItemID;
			_MemberID = MemberID;
			_CommunityID  = CommunityID;
			_ActionID = ActionID;
			_ListID  = ListID;
			_ListTypeID  = ListTypeID;
			_StatusID  = StatusID;
			_InsertDate	 = InsertDateTime;		
		}

		/// <summary>
		/// A string representation of this instance
		/// </summary>
		/// <returns></returns>
		public override string ToString() 
		{
			return string.Format("QID:{0}\tMID:{1}\tCmt:{2}\tAct:{3}\tLst:{4}\tLstTyp:{5}\tSt:{6}\tIns:{7}\t",
				_QueueItemID,
				_MemberID,
				_CommunityID,
				_ActionID,
				_ListID,
				_ListTypeID,
				_StatusID,
				_InsertDate);
		}
	}
}
