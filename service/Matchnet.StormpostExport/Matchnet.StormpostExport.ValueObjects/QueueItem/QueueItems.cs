using System;
using System.Collections;

namespace Matchnet.StormpostExport.ValueObjects.QueueItem
{
	/// <summary>
	/// A collection of Queue Items
	/// </summary>
	public class QueueItems : CollectionBase
	{
		/// <summary>
		/// Constructor
		/// </summary>
		public QueueItems()
		{
		}

		/// <summary>
		/// Indexer for getting item an index
		/// </summary>
		public QueueItem this[int index]
		{
			get 
			{ 
				return ((QueueItem)base.InnerList[index]); 
			}
		}

		/// <summary>
		/// Add a queue item to this list
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public int Add(QueueItem item)
		{
			return base.InnerList.Add(item);
		}
	}
}
