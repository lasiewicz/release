using System;

namespace Matchnet.StormpostExport.ValueObjects 
{

	/// <summary>
	/// List Content Type / frequency.
	/// </summary>
	/// <remarks>These values are declared in the Stormpost export database in the ListType table.</remarks>
	public enum ListType: int 
	{
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		SELF_SUSPENDED = -2,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		BAD_EMAIL_ADDRESS = -1,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		TEST = 0,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		NEWSLETTER = 2,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		EVENTS = 3,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		OFFERS = 4,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		MATCHMAIL_DAILY = 5,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		MATCHMAIL_BIWEEKLY = 6,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		MATCHMAIL_WEEKLY = 7,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		VIRALMAIL_DAILY = 8,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		VIRALMAIL_BIWEEKLY = 9,
		/// <summary>
		/// List Type mapping value for specific attribute(s)
		/// </summary>
		VIRALMAIL_WEEKLY = 10,

		MATCHMAIL_NEVER = 11,

		VIRALMAIL_NEVER = 12
	}

	/*	 
		SELECT AttributeID, AttributeName, DataType, [Default]
		FROM Attribute
		WHERE AttributeName IN ('GlobalStatusMask', 'GotAClickEmailPeriod', 'MatchNewsletterPeriod', 'NewsLetterMask')
		
		268	GlobalStatusMask		MASK	0
		204	GotAClickEmailPeriod	NUMBER	3
		344	MatchNewsletterPeriod	NUMBER	7
		346	NewsletterMask			MASK	7
	*/
	/// <summary>
	/// Attribute names from mnShared.dbo.Attribute
	/// </summary>
	public class AttributeName 
	{
		/// <summary>
		/// Name of attribute
		/// </summary>
		public const string SELF_SUSPENDED_FLAG = "SelfSuspendedFlag";
		/// <summary>
		/// Name of attribute
		/// </summary>
		public const string GLOBAL_STATUS_MASK	= "GlobalStatusMask";
		/// <summary>
		/// Name of attribute
		/// </summary>
		public const string VIRAL_MAIL			= "GotAClickEmailPeriod";
		/// <summary>
		/// Name of attribute
		/// </summary>
		public const string MATCH_MAIL			= "MatchNewsletterPeriod";
		/// <summary>
		/// Name of attribute
		/// </summary>
		public const string NEWSLETTER_MASK		= "NewsLetterMask";
	}

	/// <summary>
	/// The newslettermask attribute
	/// </summary>
	[Flags]
	public enum NewsLetter: int 
	{
		/// <summary>
		/// none
		/// </summary>
		NONE = 0,
		/// <summary>
		/// news and tips
		/// </summary>
		NEWSANDTIPS = 1,
		/// <summary>
		///  events etc
		/// </summary>
		EVENTS = 2,
		/// <summary>
		/// 3rd party email commecial content
		/// </summary>
		SPECIALOFFERS = 4
	}

	/// <summary>
	/// Frequency of automated mails (applies to match mail and ynm votes / click
	/// </summary>
	public enum MailFrequency: int 
	{
		/// <summary>
		/// Never
		/// </summary>
		NONE = 0,
		/// <summary>
		///  Every 1 day
		/// </summary>
		DAILY = 1,
		/// <summary>
		/// Every 3 days
		/// </summary>
		BIWEEKLY = 3,
		/// <summary>
		/// Every 7 days
		/// </summary>
		WEEKLY = 7
	}

	/// <summary>
	/// Action type relates to a list, where a list is the notion that a person is either enrolled or unsubscribed from the particular type or mailing.
	/// </summary>
	public enum ActionType : int
	{
		/// <summary>
		/// Add the item to the list
		/// </summary>
		ADD = 1,
		/// <summary>
		/// Remove the item from the list
		/// </summary>
		REMOVE = 2
	}

	/// <summary>
	/// Bitmask operation supporting member attribute save optimization to flip bits on/off etc
	/// </summary>
	public enum MaskOperationType: int 
	{
		/// <summary>
		/// value = newval
		/// </summary>
		VERBATIM = 0,	
		/// <summary>
		/// value = newval | oldval
		/// </summary>
		OR = 101,		
		/// <summary>
		/// value = newval &amp; oldval
		/// </summary>
		AND = 100,		
		/// <summary>
		/// value = ~newval &amp; oldval
		/// </summary>
		CLEARBITS = 103 
	}
}
