using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;


namespace Matchnet.StormpostExport.BusinessLogic
{
	internal class SKYDBClient
	{
		public static readonly SKYDBClient Instance = new SKYDBClient();
		private SqlConnection _connection = new SqlConnection();
		private SqlCommand _command = new SqlCommand();

		private SKYDBClient()
		{
			_connection.ConnectionString = 
				RuntimeSettings.GetSetting("STORMPOST_DB_CONNECTION_STRING");
		}

		public DataTable ExecuteDataTable(Command command, string storedProcedureName)
		{
			SqlCommand cmd = CreateSqlCommand(command, storedProcedureName);
			cmd.Connection = _connection;
			SqlDataAdapter adapter = new SqlDataAdapter(cmd);
			DataTable dt = new DataTable();

			try
			{
				_connection.Open();
				adapter.Fill(dt);
				return dt;
			}
			catch(SqlException sex)
			{
				throw new Exception("There was a SQL exception while executing datatable on SKYDB", sex);
			}
			finally
			{
				_connection.Close();
			}
		}

		public SqlParameterCollection ExecuteNonQuery(Command command, string storedProcedureName)
		{
			SqlCommand cmd = CreateSqlCommand(command, storedProcedureName);
			cmd.Connection = _connection;

			try
			{
				_connection.Open();
				cmd.ExecuteNonQuery();
				return cmd.Parameters;
			}
			catch(SqlException sex)
			{
				throw new Exception("There was a SQL exception while executing non query on SKYDB", sex);
			}
			finally
			{
				_connection.Close();
			}

		}

		private static SqlCommand CreateSqlCommand(Command command, string storedProcedureName)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandText = storedProcedureName;
			sqlCommand.CommandType = CommandType.StoredProcedure;

			Int32 parameterCount = command.Parameters.Count;

			for (Int32 parameterNum = 0; parameterNum < parameterCount; parameterNum++)
			{
				Parameter parameter = command.Parameters[parameterNum];

				SqlParameter sqlParameter = new SqlParameter(parameter.Name, parameter.DataType);
				sqlParameter.Direction = parameter.ParameterDirection;
				sqlParameter.Value = parameter.ParameterValue;

				sqlCommand.Parameters.Add(sqlParameter);
			}

			return sqlCommand;
		}

	}
}
