using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;

using Matchnet;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.StormpostExport.ValueObjects;
using Matchnet.StormpostExport.ValueObjects.QueueItem;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.StormpostExport.BusinessLogic
{
	/// <summary>
	/// Business logic for queue item processing
	/// </summary>
	public class QueueItemsBL 
	{
		/// <summary>
		/// The single instance of the class
		/// </summary>
		public readonly static QueueItemsBL Instance = new QueueItemsBL();
		private static int _SleepTime; // sleep between batches in seconds
		private static bool _IsRunnable;

		/// <summary>
		/// Constructor
		/// </summary>
		private QueueItemsBL() 
		{
			_IsRunnable = true;
			_SleepTime = 1;
		}

		/// <summary>
		/// Stops processing of batches
		/// </summary>
		public void Stop()
		{
			_IsRunnable = false;
		}

		/// <summary>
		/// Start sporcessing batches repetitively until stopped
		/// </summary>
		public void Run()
		{
			while (_IsRunnable)
			{
				if ( !ProcessBatch() )
				{
					if (_SleepTime < 64)
					{
						_SleepTime *= 2;
					}
					Thread.Sleep(_SleepTime * 1000);
				}
				else
				{
					if (_SleepTime > 1)
					{
						_SleepTime /= 2;
					}
				}
			}
		}


		/// <summary>
		/// Processes a batch of queue items.
		/// Note that underlying SQL does NOT account for multiple threads!!! this service should only run ONE place.
		/// </summary>
		/// <returns>True - If it process any items</returns>
		public bool ProcessBatch() 
		{
			try 
			{
				QueueItems items = GetNextBatch();

				if (items != null)
				{
					for ( int i = 0 ; i < items.Count; i++)	
					{
						try
						{
							Metrics.pcItemTotal.Increment();

							ProcessItem(items[i]);

						}
						catch (Exception ex)
						{
							Metrics.pcItemErrors.Increment();
							SaveQueueError(items[i]);
							throw new Exception("Process Item: ", ex);
						}
					}

					if (items.Count > 0) 
					{
						return true;
					}
					else 
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				/// TODO: Time out keeps occuring, handle that error as a warning and not kill the service.

				Metrics.Instance.WriteError("Process Batch Error: " + ex.ToString());
				
				//Environment.Exit(1);

				
				return false;
			}

		}

		/// <summary>
		/// Process single item. Enacts attribute change to the backend databases
		/// </summary>
		/// <param name="item"> The member item to be processed</param>
		public void ProcessItem(QueueItem item) 
		{
			switch((ListType)item.ListTypeID) 
			{
				case ListType.NEWSLETTER:
					SetNewsLetterMask(item, NewsLetter.NEWSANDTIPS );
					break;
				case ListType.EVENTS:
					SetNewsLetterMask(item, NewsLetter.EVENTS);
					break;
				case ListType.OFFERS:
					SetNewsLetterMask(item, NewsLetter.SPECIALOFFERS);
					break;

				case ListType.SELF_SUSPENDED:
					SetSelfSuspendedFlag(item);
					break;

				case ListType.BAD_EMAIL_ADDRESS:
					SetBadEmailAddress(item);
					break;

				case ListType.MATCHMAIL_NEVER:
				case ListType.MATCHMAIL_DAILY:
				case ListType.MATCHMAIL_BIWEEKLY:
				case ListType.MATCHMAIL_WEEKLY:
					SetMailFrequency(item);
					break;

				case ListType.VIRALMAIL_NEVER:
				case ListType.VIRALMAIL_DAILY:
				case ListType.VIRALMAIL_BIWEEKLY:
				case ListType.VIRALMAIL_WEEKLY:
					SetMailFrequency(item);
					break;

				default:
					throw new Exception("Unknown mail list type ID. " + item.ToString());
			}

			DeleteQueueItem(item);
		}

		#region Processing different lists
		/// <summary>
		/// Sets the NewsLetterMask mask for the memebr according to the data provided.
		/// </summary>
		/// <param name="item">The memebr item to be processed</param>
		/// <param name="newsletter">The newsletter type to set</param>
		private void SetNewsLetterMask(QueueItem item, NewsLetter newsletter) 
		{
			if (item.ActionID == (int)ActionType.ADD)
				saveMemberAttribute(
					item.MemberID, 
					item.CommunityID, 
					AttributeName.NEWSLETTER_MASK,
					(int)newsletter,
					MaskOperationType.OR);
			else
				saveMemberAttribute(
					item.MemberID, 
					item.CommunityID, 
					AttributeName.NEWSLETTER_MASK,
					(int)newsletter,
					MaskOperationType.CLEARBITS);
		}


		/// <summary>
		/// Sets global status mask to indicate Email address is bad / good. 
		/// </summary>
		/// <param name="item">The memebr being affected</param>
		/// <remarks>Side effect of email being deemed bad is that the EmailVerified is also unset, so that a member has to re-verify if they fix their email address</remarks>
		private void SetBadEmailAddress(QueueItem item) 
		{
			// ADD means "This is a bad email address". "Remove" therefore means " this is a not a bad email address".	
			if (item.ActionID == (int)ActionType.ADD) 
			{
				saveMemberAttribute(item.MemberID, item.CommunityID, AttributeName.GLOBAL_STATUS_MASK, (int)GlobalStatusMask.BouncedEmail, MaskOperationType.OR);
				saveMemberAttribute(item.MemberID, item.CommunityID, AttributeName.GLOBAL_STATUS_MASK, (int)GlobalStatusMask.VerifiedEmail, MaskOperationType.CLEARBITS);				
			}
			else 
			{
				saveMemberAttribute(item.MemberID, item.CommunityID, AttributeName.GLOBAL_STATUS_MASK, (int)GlobalStatusMask.BouncedEmail, MaskOperationType.CLEARBITS);
			}
		}

		/// <summary>
		/// Sets self suspended flag to indicate no emails no more of any kind - not interested in continuing to use the site.
		/// </summary>
		/// <param name="item">The memebr being affected</param>
		/// <remarks>This is used for AOL Complain Loop</remarks>
		private void SetSelfSuspendedFlag(QueueItem item) 
		{
			// ADD means "Set to self suspended". "Remove" therefore means "Un suspend me".	
			if (item.ActionID == (int)ActionType.ADD) 
			{
				saveMemberAttribute(item.MemberID, item.CommunityID, AttributeName.SELF_SUSPENDED_FLAG, 1, MaskOperationType.VERBATIM);
			}
			else 
			{
				saveMemberAttribute(item.MemberID, item.CommunityID, AttributeName.SELF_SUSPENDED_FLAG, 0, MaskOperationType.VERBATIM);
				
			}
			Metrics.pcItemSuccess.Increment();
		}


		/// <summary>
		/// Changes Mail Frequency
		/// </summary>
		/// <param name="item"></param>
		private void SetMailFrequency(QueueItem item) 
		{
			MailFrequency frequency = MailFrequency.NONE;
			string AttributeAffected = String.Empty;

			switch((ListType) item.ListTypeID) 
			{
				case ListType.MATCHMAIL_NEVER:
					frequency = MailFrequency.NONE;
					AttributeAffected = AttributeName.MATCH_MAIL;
					break;
				case ListType.MATCHMAIL_DAILY:
					frequency = MailFrequency.DAILY;
					AttributeAffected = AttributeName.MATCH_MAIL;
					break;
				case ListType.MATCHMAIL_BIWEEKLY:
					frequency = MailFrequency.BIWEEKLY;
					AttributeAffected = AttributeName.MATCH_MAIL;
					break;
				case ListType.MATCHMAIL_WEEKLY:
					frequency = MailFrequency.WEEKLY;
					AttributeAffected = AttributeName.MATCH_MAIL;
					break;


				case ListType.VIRALMAIL_NEVER:
					frequency = MailFrequency.NONE;
					AttributeAffected = AttributeName.VIRAL_MAIL;
					break;
				case ListType.VIRALMAIL_DAILY:
					frequency = MailFrequency.DAILY;
					AttributeAffected = AttributeName.VIRAL_MAIL;
					break;
				case ListType.VIRALMAIL_BIWEEKLY:
					frequency = MailFrequency.BIWEEKLY;
					AttributeAffected = AttributeName.VIRAL_MAIL;
					break;
				case ListType.VIRALMAIL_WEEKLY:
					frequency = MailFrequency.WEEKLY;
					AttributeAffected = AttributeName.VIRAL_MAIL;
					break;
				default:
					throw new Exception("Unknown mail list type ID. " + item.ToString());;
			}

			// Mail frequency change is always verbatim. 
			if (item.ActionID == (int)ActionType.REMOVE) 
			{
				return;
			}

			saveMemberAttribute(item.MemberID, item.CommunityID, AttributeAffected, (int)frequency);
		}

		#endregion

		#region DB calls
		private void saveMemberAttribute(int memberID, int communityID, string attributeName, int attributeValue) 
		{
			saveMemberAttribute(memberID, communityID, attributeName, attributeValue, MaskOperationType.VERBATIM);
		}

		/// <summary>
		/// Saves a member attribute to MemberX database(s)
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="attributeName"></param>
		/// <param name="attributeValue">Value to be set</param>
		private void saveMemberAttribute(int memberID, int communityID, string attributeName, int attributeValue, MaskOperationType operation) 
		{
			int newAttributeValue = 0;
			int oldAttributeValue = 0;
			try 
			{
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

				if (operation != MaskOperationType.VERBATIM)
					oldAttributeValue = member.GetAttributeInt(communityID, Constants.NULL_INT, Constants.NULL_INT, attributeName, 0);

				switch (operation)
				{
					case MaskOperationType.VERBATIM:
						newAttributeValue = attributeValue;
						break;
					case MaskOperationType.OR:
						newAttributeValue = attributeValue | oldAttributeValue;
						break;
					case MaskOperationType.CLEARBITS:
						newAttributeValue = ~attributeValue & oldAttributeValue;
						break;
					case MaskOperationType.AND:
						newAttributeValue = attributeValue & oldAttributeValue;
						break;
				}

				member.SetAttributeInt(communityID ,Constants.NULL_INT,Constants.NULL_INT,attributeName, newAttributeValue);

				MemberSaveResult saveResult = MemberSA.Instance.SaveMember(member);

				if (saveResult.SaveStatus != MemberSaveStatusType.Success)
				{
					throw new Exception("Member save failed. Save result status: " + saveResult.SaveStatus);
				}

				Metrics.pcItemSuccess.Increment();
				
			}
			catch(Exception ex) 
			{
				Metrics.Instance.WriteError("Error updating member attribute. [" + "MemberID:" + memberID + ", DomainID:" + communityID + ", AttributeName:" + attributeName + ", AttributeValue:" + attributeValue);

				throw new Exception("Error updating member attribute. [" + "MemberID:" + memberID +
					", CommunityID:" + communityID + ", AttributeName:" + attributeName + ", AttributeValue:" + attributeValue, ex);
			}
		}


		/// <summary>
		/// Deletes item from the notification Queue at StormPost
		/// </summary>
		/// <param name="item">Processed queue</param>
		public void DeleteQueueItem(QueueItem item) 
		{
			try
			{
				Command command = new Command();
				command.AddParameter("@QueueID",SqlDbType.Int,ParameterDirection.Input,item.QueueItemID);
				SKYDBClient.Instance.ExecuteNonQuery(command, "up_Queue_Delete");
				
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("Failed DeleteQueueItem " + item.ToString());

				throw new Exception("Failed DeleteQueueItem " + item.ToString(), ex);
			}
		}

		/// <summary>
		/// Saves a queue item to the error queue table "QueueError" and removes it from the Queue table.
		/// </summary>
		/// <param name="item">The queue item to be moved to the error queue</param>
		public void SaveQueueError(QueueItem item) 
		{
			try
			{
				Command command = new Command();
				command.AddParameter("@QueueID",SqlDbType.Int,ParameterDirection.Input,item.QueueItemID);
				SKYDBClient.Instance.ExecuteNonQuery(command, "up_QueueError_Save");
				
			}
			catch (Exception ex)
			{
				Metrics.Instance.WriteError("Failed SaveQueueError " + item.ToString());
				throw new Exception("Failed SaveQueueError " + item.ToString(), ex);
			}
		}


		/// <summary>
		/// Get next batch of Bedrock community members to update from the  SkyList DB
		/// </summary>
		/// <returns></returns>
		private QueueItems GetNextBatch() 
		{
			try
			{
				QueueItems items = new QueueItems();

				Command command = new Command();
				command.AddParameter("@Bedrock", SqlDbType.Bit, ParameterDirection.Input, 1);
				DataTable dt = SKYDBClient.Instance.ExecuteDataTable(command, "up_Queue_ListBatch");
				
				foreach( DataRow dr in dt.Rows)
				{
					QueueItem item = new QueueItem(		
						Convert.ToInt32(dr["QueueID"]),
						Convert.ToInt32(dr["MemberID"]),
						Convert.ToInt32(dr["CommunityID"]),
						Convert.ToInt32(dr["ActionID"]),
						Convert.ToInt32(dr["ListID"]),
						Convert.ToInt32(dr["ListTypeID"]),
						Convert.ToInt32(dr["StatusID"]),
						Convert.ToDateTime(dr["InsertDate"])
						);
					items.Add(item);
				}
				
				Metrics.pcBatchesTotal.Increment();
				Metrics.pcLastBatchSize.RawValue = items.Count;

				return items;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				throw new Exception("StormpostExport.BusinessLogic.GetNextBatch() exception: ",ex);
			}
		}


		#endregion

	}
}

