using System;
using System.Diagnostics;

using Matchnet.StormpostExport.ValueObjects;

namespace Matchnet.StormpostExport.BusinessLogic
{
	
	/// <summary>
	/// Performance counters etc
	/// </summary>
	public class Metrics 
	{

		/// <summary>
		/// The single instance of this class.
		/// </summary>
		public static readonly Metrics Instance = new Metrics();

		/// <summary>
		/// Items total (rows from the Queue table. Each representing a single attribute or bit in a mask
		/// </summary>
		public static PerformanceCounter pcItemTotal;
		/// <summary>
		/// Errors total
		/// </summary>
		public static PerformanceCounter pcItemErrors;
		/// <summary>
		/// Successfully processed items
		/// </summary>
		public static PerformanceCounter pcItemSuccess;
		/// <summary>
		/// Last batch size
		/// </summary>
		public static PerformanceCounter pcLastBatchSize;
		/// <summary>
		/// Number of batches total
		/// </summary>
		public static PerformanceCounter pcBatchesTotal;

		private Metrics() 
		{
			if (PerformanceCounterCategory.Exists(MetricsInstaller.COUNTER_CATEGORY))
			{
				pcItemTotal = new PerformanceCounter(MetricsInstaller.COUNTER_CATEGORY,"Processed Total",false);
				pcItemErrors = new PerformanceCounter(MetricsInstaller.COUNTER_CATEGORY,"Error Items Total",false);
				pcItemSuccess = new PerformanceCounter(MetricsInstaller.COUNTER_CATEGORY,"Success Items Total",false);
				pcLastBatchSize = new PerformanceCounter(MetricsInstaller.COUNTER_CATEGORY,"Last Batch Size",false);
				pcBatchesTotal = new PerformanceCounter(MetricsInstaller.COUNTER_CATEGORY,"Batches Total",false);
			}
			else 
			{
				throw new Exception("Unable to instantiate counters. PerformanceCounterCategory does not exist");
			}	
		}


		/// <summary>
		/// Reset all performance counters.
		/// </summary>
		public void ResetCounters()
		{
			pcItemErrors.RawValue = 0L;
			pcItemSuccess.RawValue = 0L;
			pcItemTotal.RawValue = 0L;
			pcLastBatchSize.RawValue = 0L;
			pcBatchesTotal.RawValue = 0L;
		}

		/// <summary>
		/// Adds an information type event log entry
		/// </summary>
		/// <param name="message">message content</param>
		public void WriteInformation(string message) 
		{
			EventLog.WriteEntry(MetricsInstaller.EVENT_LOG_SOURCE , message, EventLogEntryType.Information);
		}


		/// <summary>
		/// Adds an error type event log entry
		/// </summary>
		/// <param name="message">message content</param>
		public void WriteError(string message) 
		{
			EventLog.WriteEntry(MetricsInstaller.EVENT_LOG_SOURCE, message, EventLogEntryType.Error);
		}

		public void WriteWarning(string message)
		{
			EventLog.WriteEntry(MetricsInstaller.EVENT_LOG_SOURCE, message, EventLogEntryType.Warning);
		}
	}		
	/// <summary>
	/// Install / Uninstall counters
	/// </summary>
	public class MetricsInstaller
	{
		/// <summary>
		/// Used by metrics
		/// </summary>
		public const string COUNTER_CATEGORY = "Matchnet.StormpostExport.Service";
		/// <summary>
		/// Used by metrics and service
		/// </summary>
		public const string EVENT_LOG_SOURCE = "Matchnet.StormpostExport.Service";

		/// <summary>
		/// Install
		/// </summary>
		public static void Install()
		{
			Debug.WriteLine(COUNTER_CATEGORY);
			if (!PerformanceCounterCategory.Exists(COUNTER_CATEGORY))
			{
				CounterCreationDataCollection CountersToCreate = new CounterCreationDataCollection();
				//	PerformanceCounter pcItemTotal;
				CountersToCreate.Add(new CounterCreationData("Processed Total","# items processed total",PerformanceCounterType.NumberOfItems32));
				//	PerformanceCounter pcItemErrors;
				CountersToCreate.Add(new CounterCreationData("Error Items Total","# items processed that produced an error",PerformanceCounterType.NumberOfItems32));
				//	PerformanceCounter pcItemSuccess;
				CountersToCreate.Add(new CounterCreationData("Success Items Total","# items processed with success",PerformanceCounterType.NumberOfItems32));
				//	PerformanceCounter pcLastBatchSize;
				CountersToCreate.Add(new CounterCreationData("Last Batch Size","# of items present in the last batch processed",PerformanceCounterType.NumberOfItems32));
				//	PerformanceCounter pcBatchesTotal
				CountersToCreate.Add(new CounterCreationData("Batches Total","# of batches total",PerformanceCounterType.NumberOfItems32));
					
				// Create the category and pass the collection to it.
				PerformanceCounterCategory.Create(COUNTER_CATEGORY, "Counters for the service that runs a Matchnet Query agains the FAST system", CountersToCreate);
			}

			// Event log prep
			if(!EventLog.SourceExists(EVENT_LOG_SOURCE))
			{
				EventLog.CreateEventSource( EVENT_LOG_SOURCE,"Application");
				Debug.WriteLine("Created Event Source -> " + EVENT_LOG_SOURCE );
			}

		}

		/// <summary>
		///  Uninstall
		/// </summary>
		public static void Uninstall()
		{
			if (PerformanceCounterCategory.Exists(COUNTER_CATEGORY))
			{
				PerformanceCounterCategory.Delete(COUNTER_CATEGORY);
			}

			if(EventLog.SourceExists(EVENT_LOG_SOURCE))
			{
				EventLog.DeleteEventSource(EVENT_LOG_SOURCE);
			}
		}
	}

	
}
