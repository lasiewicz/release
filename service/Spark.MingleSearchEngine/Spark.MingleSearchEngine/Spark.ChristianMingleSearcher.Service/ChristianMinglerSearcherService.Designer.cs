﻿using Matchnet.Configuration.ServiceAdapters;
using Matchnet.InitialConfiguration;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.ChristianMingleSearcher.Service
{
    partial class ChristianMinglerSearcherService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ChristianMinglerSearcherService.SERVICE_NAME;
            string searcherConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, GetCommunity().ToUpper());
            base.ServiceInstanceConfig = RuntimeSettings.GetServiceInstanceConfig(searcherConstant, InitializationSettings.MachineName);        
        }

        #endregion
    }
}
