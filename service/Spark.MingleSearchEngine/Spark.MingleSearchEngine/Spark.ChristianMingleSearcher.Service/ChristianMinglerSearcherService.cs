﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Spark.MingleSearchEngine.ServiceManager;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.ChristianMingleSearcher.Service
{
    public partial class ChristianMinglerSearcherService : RemotingServiceBase
    {
        public static string SERVICE_NAME = string.Format(ServiceConstants.SERVICE_SEARCHER_NAME, GetCommunity());
        SearcherSM _searcherSM;

        public ChristianMinglerSearcherService()
        {
            InitializeComponent();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _searcherSM = new SearcherSM(GetCommunity());
                base.RegisterServiceManager(_searcherSM);
            }
            catch (Exception ex)
            {
                Trace.Write(SERVICE_NAME + " - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }

        public static string GetCommunity()
        {
            string community = "ChristianMingle";
            return community;
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                //System.Threading.Thread.Sleep(300000);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
