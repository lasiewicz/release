﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleSearchEngine.ValueObjects
{
    [Serializable]
    public class MingleSearchMemberUpdate
    {
        private UpdateReasonEnum _UpdateReason = UpdateReasonEnum.none;
        private UpdateModeEnum _UpdateMode = UpdateModeEnum.none;

        public UpdateReasonEnum UpdateReason
        {
            get { return _UpdateReason; }
            set { _UpdateReason = value; }
        }

        public UpdateModeEnum UpdateMode
        {
            get { return _UpdateMode; }
            set { _UpdateMode = value; }
        }

        public int MemberID { get; set; }
        public int CommunityID { get; set; }

    }
}
