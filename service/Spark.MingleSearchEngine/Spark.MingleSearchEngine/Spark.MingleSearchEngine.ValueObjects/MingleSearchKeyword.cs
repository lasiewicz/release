﻿using System;
using System.Data;
using Matchnet;

namespace Spark.MingleSearchEngine.ValueObjects
{
    #region class SearchKeyword
    [Serializable]
    public class MingleSearchKeyword
    {

        #region private
        int _userId;
        int _communityid;
        DateTime _lastLogin;
        double _longitude;
        double _latitude;
        int _locationBhRegionid;
        DateTime _lastUpdateYmdt;
        string _anythingElse;
        string _bottomThreeThings;
        string _bravery;
        string _car;
        string _christianMeaning;
        string _collegeMajor;
        string _coreColor;
        string _favoriteCalling;
        string _favoriteScripture;
        string _firstDate;
        string _fiveYears;
        string _greeting;
        string _howFound;
        string _improvement;
        string _lifeImportant;
        string _ministryOther;
        string _movie;
        string _occupation;
        string _pastRelationships;
        string _perfectDay;
        string _postalCode;
        string _qow;
        string _realLocation;
        string _scripture;
        string _serviceProject;
        string _tenMillion;
        string _topThreeThings;
        string _tvShow;
        string _userName;
        string _whyMilitary;
        string _whyKnowMe;

        private UpdateReasonEnum _UpdateReason = UpdateReasonEnum.none;
        private UpdateModeEnum _UpdateMode = UpdateModeEnum.none;
        #endregion
        #region properties

        //for compatibility with e1loader updates
        public UpdateTypeEnum UpdateType { get; set; }
        public UpdateReasonEnum UpdateReason
        {
            get { return _UpdateReason; }
            set { _UpdateReason = value; }
        }
        public UpdateModeEnum UpdateMode
        {
            get { return _UpdateMode; }
            set { _UpdateMode = value; }
        }

        public int CommunityID
        {
            get { return _communityid; }
            set { _communityid = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public int UserID
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public DateTime LastLogin
        {
            get { return _lastLogin; }
            set { _lastLogin = value; }
        }

        public int LocationBHRegionID
        {
            get { return _locationBhRegionid; }
            set { _locationBhRegionid = value; }
        }

        public DateTime LastUpdateYMDT
        {
            get { return _lastUpdateYmdt; }
            set { _lastUpdateYmdt = value; }
        }

        public string AnythingElse
        {
            get { return _anythingElse; }
            set { _anythingElse = value; }
        }

        public string BottomThreeThings
        {
            get { return _bottomThreeThings; }
            set { _bottomThreeThings = value; }
        }

        public string Bravery
        {
            get { return _bravery; }
            set { _bravery = value; }
        }

        public string Car
        {
            get { return _car; }
            set { _car = value; }
        }

        public string ChristianMeaning
        {
            get { return _christianMeaning; }
            set { _christianMeaning = value; }
        }

        public string CollegeMajor
        {
            get { return _collegeMajor; }
            set { _collegeMajor = value; }
        }

        public string CoreColor
        {
            get { return _coreColor; }
            set { _coreColor = value; }
        }

        public string FavoriteCalling
        {
            get { return _favoriteCalling; }
            set { _favoriteCalling = value; }
        }

        public string FavoriteScripture
        {
            get { return _favoriteScripture; }
            set { _favoriteScripture = value; }
        }

        public string FirstDate
        {
            get { return _firstDate; }
            set { _firstDate = value; }
        }

        public string FiveYears
        {
            get { return _fiveYears; }
            set { _fiveYears = value; }
        }

        public string Greeting
        {
            get { return _greeting; }
            set { _greeting = value; }
        }

        public string HowFound
        {
            get { return _howFound; }
            set { _howFound = value; }
        }

        public string Improvement
        {
            get { return _improvement; }
            set { _improvement = value; }
        }

        public string LifeImportant
        {
            get { return _lifeImportant; }
            set { _lifeImportant = value; }
        }

        public string MinistryOther
        {
            get { return _ministryOther; }
            set { _ministryOther = value; }
        }

        public string Movie
        {
            get { return _movie; }
            set { _movie = value; }
        }

        public string Occupation
        {
            get { return _occupation; }
            set { _occupation = value; }
        }

        public string PastRelationships
        {
            get { return _pastRelationships; }
            set { _pastRelationships = value; }
        }

        public string PerfectDay
        {
            get { return _perfectDay; }
            set { _perfectDay = value; }
        }

        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }

        public string Qow
        {
            get { return _qow; }
            set { _qow = value; }
        }

        public string RealLocation
        {
            get { return _realLocation; }
            set { _realLocation = value; }
        }

        public string Scripture
        {
            get { return _scripture; }
            set { _scripture = value; }
        }

        public string ServiceProject
        {
            get { return _serviceProject; }
            set { _serviceProject = value; }
        }

        public string TenMillion
        {
            get { return _tenMillion; }
            set { _tenMillion = value; }
        }

        public string TopThreeThings
        {
            get { return _topThreeThings; }
            set { _topThreeThings = value; }
        }

        public string TvShow
        {
            get { return _tvShow; }
            set { _tvShow = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public string WhyMilitary
        {
            get { return _whyMilitary; }
            set { _whyMilitary = value; }
        }

        public string WhyKnowMe
        {
            get { return _whyKnowMe; }
            set { _whyKnowMe = value; }
        }

        #endregion


        public static MingleSearchKeyword GenerateSearchKeyword(int memberId, int communityId, DateTime lastActiveDate, DateTime updateDate)
        {
            MingleSearchKeyword searchKeyword = new MingleSearchKeyword();
            searchKeyword.UserID = memberId;
            searchKeyword.CommunityID = communityId;
            searchKeyword.LastLogin = lastActiveDate;
            searchKeyword.LastUpdateYMDT = updateDate;

            return searchKeyword;
        }

        public MingleSearchKeyword Populate(IDataReader dr)
        {
            //columns for keyword search
            if (!dr.IsDBNull(dr.GetOrdinal("userid"))) UserID = dr.GetInt32(dr.GetOrdinal("userid"));
            if (!dr.IsDBNull(dr.GetOrdinal("communityid"))) CommunityID = dr.GetInt32(dr.GetOrdinal("communityid"));
            if (!dr.IsDBNull(dr.GetOrdinal("location_bhregionid"))) LocationBHRegionID = dr.GetInt32(dr.GetOrdinal("location_bhregionid"));
            if (!dr.IsDBNull(dr.GetOrdinal("location_long"))) Longitude = dr.GetDouble(dr.GetOrdinal("location_long"));
            if (!dr.IsDBNull(dr.GetOrdinal("location_lat"))) Latitude = dr.GetDouble(dr.GetOrdinal("location_lat"));
            if (!dr.IsDBNull(dr.GetOrdinal("LastLogin"))) LastLogin = dr.GetDateTime(dr.GetOrdinal("LastLogin"));
            if (!dr.IsDBNull(dr.GetOrdinal("LastUpdateYMDT"))) LastUpdateYMDT = dr.GetDateTime(dr.GetOrdinal("LastUpdateYMDT"));
            if (!dr.IsDBNull(dr.GetOrdinal("anythingelse"))) AnythingElse = dr.GetString(dr.GetOrdinal("anythingelse"));
            if (!dr.IsDBNull(dr.GetOrdinal("bottomthreethings"))) BottomThreeThings = dr.GetString(dr.GetOrdinal("bottomthreethings"));
            if (!dr.IsDBNull(dr.GetOrdinal("bravery"))) Bravery = dr.GetString(dr.GetOrdinal("bravery"));
            if (!dr.IsDBNull(dr.GetOrdinal("car"))) Car = dr.GetString(dr.GetOrdinal("car"));
            if (!dr.IsDBNull(dr.GetOrdinal("christian_meaning"))) ChristianMeaning = dr.GetString(dr.GetOrdinal("christian_meaning"));
            if (!dr.IsDBNull(dr.GetOrdinal("college_major"))) CollegeMajor = dr.GetString(dr.GetOrdinal("college_major"));
            if (!dr.IsDBNull(dr.GetOrdinal("core_color"))) CoreColor = dr.GetString(dr.GetOrdinal("core_color"));
            if (!dr.IsDBNull(dr.GetOrdinal("favorite_calling"))) FavoriteCalling = dr.GetString(dr.GetOrdinal("favorite_calling"));
            if (!dr.IsDBNull(dr.GetOrdinal("favorite_scripture"))) FavoriteScripture = dr.GetString(dr.GetOrdinal("favorite_scripture"));
            if (!dr.IsDBNull(dr.GetOrdinal("first_date"))) FirstDate = dr.GetString(dr.GetOrdinal("first_date"));
            if (!dr.IsDBNull(dr.GetOrdinal("five_years"))) FiveYears = dr.GetString(dr.GetOrdinal("five_years"));
            if (!dr.IsDBNull(dr.GetOrdinal("greeting"))) Greeting = dr.GetString(dr.GetOrdinal("greeting"));
            if (!dr.IsDBNull(dr.GetOrdinal("how_found"))) HowFound = dr.GetString(dr.GetOrdinal("how_found"));
            if (!dr.IsDBNull(dr.GetOrdinal("improvement"))) Improvement = dr.GetString(dr.GetOrdinal("improvement"));
            if (!dr.IsDBNull(dr.GetOrdinal("lifeimportant"))) LifeImportant = dr.GetString(dr.GetOrdinal("lifeimportant"));
            if (!dr.IsDBNull(dr.GetOrdinal("ministry_other"))) MinistryOther = dr.GetString(dr.GetOrdinal("ministry_other"));
            if (!dr.IsDBNull(dr.GetOrdinal("movie"))) Movie = dr.GetString(dr.GetOrdinal("movie"));
            if (!dr.IsDBNull(dr.GetOrdinal("occupation"))) Occupation = dr.GetString(dr.GetOrdinal("occupation"));
            if (!dr.IsDBNull(dr.GetOrdinal("past_relationships"))) PastRelationships = dr.GetString(dr.GetOrdinal("past_relationships"));
            if (!dr.IsDBNull(dr.GetOrdinal("perfectday"))) PerfectDay = dr.GetString(dr.GetOrdinal("perfectday"));
            if (!dr.IsDBNull(dr.GetOrdinal("postal_code"))) PostalCode = dr.GetString(dr.GetOrdinal("postal_code"));
            if (!dr.IsDBNull(dr.GetOrdinal("qow"))) Qow = dr.GetString(dr.GetOrdinal("qow"));
            if (!dr.IsDBNull(dr.GetOrdinal("real_location"))) RealLocation = dr.GetString(dr.GetOrdinal("real_location"));
            if (!dr.IsDBNull(dr.GetOrdinal("scripture"))) Scripture = dr.GetString(dr.GetOrdinal("scripture"));
            if (!dr.IsDBNull(dr.GetOrdinal("service_project"))) ServiceProject = dr.GetString(dr.GetOrdinal("service_project"));
            if (!dr.IsDBNull(dr.GetOrdinal("tenmillion"))) TenMillion = dr.GetString(dr.GetOrdinal("tenmillion"));
            if (!dr.IsDBNull(dr.GetOrdinal("topthreethings"))) TopThreeThings = dr.GetString(dr.GetOrdinal("topthreethings"));
            if (!dr.IsDBNull(dr.GetOrdinal("tv_show"))) TvShow = dr.GetString(dr.GetOrdinal("tv_show"));
            if (!dr.IsDBNull(dr.GetOrdinal("username"))) UserName = dr.GetString(dr.GetOrdinal("username"));
            if (!dr.IsDBNull(dr.GetOrdinal("why_military"))) WhyMilitary = dr.GetString(dr.GetOrdinal("why_military"));
            if (!dr.IsDBNull(dr.GetOrdinal("whyknowme"))) WhyKnowMe = dr.GetString(dr.GetOrdinal("whyknowme"));
            return this;
        }
    }

    #endregion


}
