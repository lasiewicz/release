﻿using System;
using System.Data;
using Matchnet;

namespace Spark.MingleSearchEngine.ValueObjects
{
    [Serializable]
    public enum UpdateTypeEnum:int
    {
        none=0,
        all=1,
        emailcount=2,
        lastactivedate=3,
        hasphoto=4,
        delete=5,
        add=6,
        partial=7
    }

    [Serializable]
    public enum UpdateReasonEnum : int
    {
        none=0,
        adminSuspend=1,
        adminUnsuspend = 2,
        selfSuspend=3,
        selfUnsuspend = 4,
        hide = 5,
        unHide = 6,
        searchDataChanged=7,
        logon=8,
        updateChangedToRemove =9
    }

    [Serializable]
    public enum UpdateModeEnum : int
    {
        none=0,
        add=1,
        update=2,
        remove=3
    }

    #region class SearchMember
    [Serializable]
    public class MingleSearchMember
    {

        #region private
        int _userId;
        int _communityid;
        int _gender;
        DateTime _birthdate;
        DateTime _joinDate;
        DateTime _lastLogin;
        int _photoFlag;
        int _education;
        int _religion;
        long _language;
        int _ethnicity;
        int _smoke;
        int _drink;
        int _height;
        int _maritalstatus;
        int _bodytype;
        double _longitude;
        double _latitude;
        int _locationBhRegionid;
        DateTime _lastUpdateYmdt;
        int _weight;
        int _children;
        int _aboutChildren;
        int _childrenAtHome;
        decimal _colorB;
        decimal _colorR;
        decimal _colorW;
        decimal _colorY;
        int _premiumServicesFlags;
        int _churchactivity;
        int _ministryid;
        int _eyes;
        int _hair;
        int _intention;
        int _wantsEthnicity;
        int _wantsSmoke;
        int _wantsDrink;
        int _wantsReligion;
        int _wantsMaritalStatus;
        int _wantsBodyType;
        int _wantsChurchActivity;
        int _wantsEducation;
        int _importanceEthnicity;
        int _importanceSmoke;
        int _importanceDrink;
        int _importanceReligion;
        int _importanceMaritalStatus;
        int _importanceBodyType;
        int _importanceChurchActivity;
        int _importanceEducation;
 
        private UpdateReasonEnum _UpdateReason = UpdateReasonEnum.none;
        private UpdateModeEnum _UpdateMode = UpdateModeEnum.none;
        #endregion
        #region properties

        //for compatibility with e1loader updates
        public UpdateTypeEnum UpdateType { get; set; }
        public UpdateReasonEnum UpdateReason
        {
            get { return _UpdateReason; }
            set { _UpdateReason = value; }
        }
        public UpdateModeEnum UpdateMode
        {
            get { return _UpdateMode; }
            set { _UpdateMode = value; }
        }

        public int UserID
        { 
            get { return _userId;}
            set {_userId = value;} 
        }

        public int CommunityID
        {
            get { return _communityid; }
            set { _communityid = value; }
        }

        public int Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public DateTime Birthdate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        public DateTime JoinDate
        {
            get { return _joinDate; }
            set { _joinDate = value; }
        }

        public DateTime LastLogin
        {
            get { return _lastLogin; }
            set { _lastLogin = value; }
        }

        public int PhotoFlag
        {
            get { return _photoFlag; }
            set { _photoFlag = value; }
        }

        public int Education
        {
            get { return _education; }
            set { _education = value; }
        }

        public int Religion
        {
            get { return _religion; }
            set { _religion = value; }
        }

        public long Language
        {
            get { return _language; }
            set { _language = value; }
        }

        public int Ethnicity
        {
            get { return _ethnicity; }
            set { _ethnicity = value; }
        }

        public int Smoke
        {
            get { return _smoke; }
            set { _smoke = value; }
        }

        public int Drink
        {
            get { return _drink; }
            set { _drink = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public int MaritalStatus
        {
            get { return _maritalstatus; }
            set { _maritalstatus = value; }
        }

        public int BodyType
        {
            get { return _bodytype; }
            set { _bodytype = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public int LocationBHRegionID
        {
            get { return _locationBhRegionid; }
            set { _locationBhRegionid = value; }
        }

        public DateTime LastUpdateYMDT
        {
            get { return _lastUpdateYmdt; }
            set { _lastUpdateYmdt = value; }
        }

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public int Children
        {
            get { return _children; }
            set { _children = value; }
        }

        public int AboutChildren
        {
            get { return _aboutChildren; }
            set { _aboutChildren = value; }
        }

        public int ChildrenAtHome
        {
            get { return _childrenAtHome; }
            set { _childrenAtHome = value; }
        }

        public decimal ColorB
        {
            get { return _colorB; }
            set { _colorB = value; }
        }

        public decimal ColorR
        {
            get { return _colorR; }
            set { _colorR = value; }
        }

        public decimal ColorW
        {
            get { return _colorW; }
            set { _colorW = value; }
        }

        public decimal ColorY
        {
            get { return _colorY; }
            set { _colorY = value; }
        }

        public int PremiumServicesFlags
        {
            get { return _premiumServicesFlags; }
            set { _premiumServicesFlags = value; }
        }

        public int Churchactivity
        {
            get { return _churchactivity; }
            set { _churchactivity = value; }
        }

        public int Ministryid
        {
            get { return _ministryid; }
            set { _ministryid = value; }
        }

        public int Eyes
        {
            get { return _eyes; }
            set { _eyes = value; }
        }

        public int Hair
        {
            get { return _hair; }
            set { _hair = value; }
        }

        public int Intention
        {
            get { return _intention; }
            set { _intention = value; }
        }

        public int WantsEthnicity
        {
            get { return _wantsEthnicity; }
            set { _wantsEthnicity = value; }
        }

        public int WantsSmoke
        {
            get { return _wantsSmoke; }
            set { _wantsSmoke = value; }
        }

        public int WantsDrink
        {
            get { return _wantsDrink; }
            set { _wantsDrink = value; }
        }

        public int WantsReligion
        {
            get { return _wantsReligion; }
            set { _wantsReligion = value; }
        }

        public int WantsMaritalStatus
        {
            get { return _wantsMaritalStatus; }
            set { _wantsMaritalStatus = value; }
        }

        public int WantsBodyType
        {
            get { return _wantsBodyType; }
            set { _wantsBodyType = value; }
        }

        public int WantsChurchActivity
        {
            get { return _wantsChurchActivity; }
            set { _wantsChurchActivity = value; }
        }

        public int WantsEducation
        {
            get { return _wantsEducation; }
            set { _wantsEducation = value; }
        }

        public int ImportanceEthnicity
        {
            get { return _importanceEthnicity; }
            set { _importanceEthnicity = value; }
        }

        public int ImportanceSmoke
        {
            get { return _importanceSmoke; }
            set { _importanceSmoke = value; }
        }

        public int ImportanceDrink
        {
            get { return _importanceDrink; }
            set { _importanceDrink = value; }
        }

        public int ImportanceReligion
        {
            get { return _importanceReligion; }
            set { _importanceReligion = value; }
        }

        public int ImportanceMaritalStatus
        {
            get { return _importanceMaritalStatus; }
            set { _importanceMaritalStatus = value; }
        }

        public int ImportanceBodyType
        {
            get { return _importanceBodyType; }
            set { _importanceBodyType = value; }
        }

        public int ImportanceChurchActivity
        {
            get { return _importanceChurchActivity; }
            set { _importanceChurchActivity = value; }
        }

        public int ImportanceEducation
        {
            get { return _importanceEducation; }
            set { _importanceEducation = value; }
        }
        #endregion

        public static MingleSearchMember GenerateSearchMember(int userId, int communityId, int gender, DateTime birthDate, DateTime joinDate, DateTime lastLogonDate, int photo, int education, int religion, int language, int ethnicity, int smoke, int drink, int height, int maritalStatus, int bodyType, int locationBHRegionID, double longitude, double latitude, DateTime updateDate, int weight, int childrenCount,
        int aboutChildren, int childrenAtHome, int churchActivity, decimal colorB, decimal colorR, decimal colorW, decimal colorY, int eyes, int hair, int ministryID, int premiumServicesFlags,
        int wantsEthnicity, int wantsSmoke, int wantsDrink, int wantsReligion, int wantsMaritalStatus, int wantsBodyType, int wantsChurchActivity, int wantsEducation, int importanceEthnicity, int importanceSmoke, int importanceDrink, int importanceReligion, int importanceMaritalStatus, int importanceBodyType, int importanceChurchActivity, int importanceEducation)
        {
            MingleSearchMember searchMember = new MingleSearchMember();
            searchMember.AboutChildren = aboutChildren;
            searchMember.Birthdate = birthDate;
            searchMember.BodyType = bodyType;
            searchMember.Children = childrenCount;
            searchMember.ChildrenAtHome = childrenAtHome;
            searchMember.Churchactivity = churchActivity;
            searchMember.ColorB = colorB;
            searchMember.ColorR = colorR;
            searchMember.ColorW = colorW;
            searchMember.ColorY = colorY;
            searchMember.CommunityID = communityId;
            searchMember.Drink = drink;
            searchMember.Education = education;
            searchMember.Ethnicity = ethnicity;
            searchMember.Eyes = eyes;
            searchMember.Gender = gender;
            searchMember.Hair = hair;
            searchMember.Height = height;
            searchMember.JoinDate = joinDate;
            searchMember.Language = language;
            searchMember.Latitude = latitude;
            searchMember.LastLogin = lastLogonDate;
            searchMember.LastUpdateYMDT = updateDate;
            searchMember.LocationBHRegionID = locationBHRegionID;
            searchMember.Longitude = longitude;
            searchMember.MaritalStatus = maritalStatus;
            searchMember.Ministryid = ministryID;
            searchMember.PhotoFlag = photo;
            searchMember.PremiumServicesFlags = premiumServicesFlags;
            searchMember.Religion = religion;
            searchMember.Smoke = smoke;
            searchMember.UserID = userId;
            searchMember.Weight = weight;
            // attributes used for match percentage calculation
            searchMember.WantsEthnicity = wantsEthnicity;
            searchMember.WantsSmoke = wantsSmoke;
            searchMember.WantsDrink = wantsDrink;
            searchMember.WantsReligion = wantsReligion;
            searchMember.WantsMaritalStatus = wantsMaritalStatus;
            searchMember.WantsBodyType = wantsBodyType;
            searchMember.WantsChurchActivity = wantsChurchActivity;
            searchMember.WantsEducation = wantsEducation;
            searchMember.ImportanceEthnicity = importanceEthnicity;
            searchMember.ImportanceSmoke = importanceSmoke;
            searchMember.ImportanceDrink = importanceDrink;
            searchMember.ImportanceReligion = importanceReligion;
            searchMember.ImportanceMaritalStatus = importanceMaritalStatus;
            searchMember.ImportanceBodyType = importanceBodyType;
            searchMember.ImportanceChurchActivity = importanceChurchActivity;
            searchMember.ImportanceEducation = importanceEducation;

            return searchMember;
        }


        public MingleSearchMember Populate(IDataReader dr)
        {
            //columns for search
            if (!dr.IsDBNull(dr.GetOrdinal("userid"))) UserID = dr.GetInt32(dr.GetOrdinal("userid"));
            if (!dr.IsDBNull(dr.GetOrdinal("communityid"))) CommunityID = dr.GetInt32(dr.GetOrdinal("communityid"));
            if (!dr.IsDBNull(dr.GetOrdinal("gender"))) Gender = dr.GetByte(dr.GetOrdinal("gender"));
            if (!dr.IsDBNull(dr.GetOrdinal("birthdate"))) Birthdate = dr.GetDateTime(dr.GetOrdinal("birthdate"));
            if (!dr.IsDBNull(dr.GetOrdinal("joindate"))) JoinDate = dr.GetDateTime(dr.GetOrdinal("joindate"));
            if (!dr.IsDBNull(dr.GetOrdinal("LastLogin"))) LastLogin = dr.GetDateTime(dr.GetOrdinal("LastLogin"));
            if (!dr.IsDBNull(dr.GetOrdinal("photo"))) PhotoFlag = dr.GetInt32(dr.GetOrdinal("photo"));
            if (!dr.IsDBNull(dr.GetOrdinal("education"))) Education = dr.GetInt32(dr.GetOrdinal("education"));
            if (!dr.IsDBNull(dr.GetOrdinal("religion"))) Religion = dr.GetInt32(dr.GetOrdinal("religion"));
            if (!dr.IsDBNull(dr.GetOrdinal("language"))) Language = dr.GetInt64(dr.GetOrdinal("language"));
            if (!dr.IsDBNull(dr.GetOrdinal("ethnicity"))) Ethnicity = dr.GetInt32(dr.GetOrdinal("ethnicity"));
            if (!dr.IsDBNull(dr.GetOrdinal("smoke"))) Smoke = dr.GetInt32(dr.GetOrdinal("smoke"));
            if (!dr.IsDBNull(dr.GetOrdinal("drink"))) Drink = dr.GetInt32(dr.GetOrdinal("drink"));
            if (!dr.IsDBNull(dr.GetOrdinal("height"))) Height = dr.GetByte(dr.GetOrdinal("height"));
            if (!dr.IsDBNull(dr.GetOrdinal("marital_status"))) MaritalStatus = dr.GetInt32(dr.GetOrdinal("marital_status"));
            if (!dr.IsDBNull(dr.GetOrdinal("body_type"))) BodyType = dr.GetInt32(dr.GetOrdinal("body_type"));
            if (!dr.IsDBNull(dr.GetOrdinal("location_bhregionid"))) LocationBHRegionID = dr.GetInt32(dr.GetOrdinal("location_bhregionid"));
            if (!dr.IsDBNull(dr.GetOrdinal("location_long"))) Longitude = dr.GetDouble(dr.GetOrdinal("location_long"));
            if (!dr.IsDBNull(dr.GetOrdinal("location_lat"))) Latitude = dr.GetDouble(dr.GetOrdinal("location_lat"));
            if (!dr.IsDBNull(dr.GetOrdinal("LastUpdateYMDT"))) LastUpdateYMDT = dr.GetDateTime(dr.GetOrdinal("LastUpdateYMDT"));
            if (!dr.IsDBNull(dr.GetOrdinal("children"))) Children = dr.GetByte(dr.GetOrdinal("children"));
            if (!dr.IsDBNull(dr.GetOrdinal("children_at_home"))) ChildrenAtHome = dr.GetByte(dr.GetOrdinal("children_at_home"));
            if (!dr.IsDBNull(dr.GetOrdinal("about_children"))) AboutChildren = dr.GetByte(dr.GetOrdinal("about_children"));
            if (!dr.IsDBNull(dr.GetOrdinal("color_b"))) ColorB = dr.GetDecimal(dr.GetOrdinal("color_b"));
            if (!dr.IsDBNull(dr.GetOrdinal("color_r"))) ColorR = dr.GetDecimal(dr.GetOrdinal("color_r"));
            if (!dr.IsDBNull(dr.GetOrdinal("color_w"))) ColorW = dr.GetDecimal(dr.GetOrdinal("color_w"));
            if (!dr.IsDBNull(dr.GetOrdinal("color_y"))) ColorY = dr.GetDecimal(dr.GetOrdinal("color_y"));
            if (!dr.IsDBNull(dr.GetOrdinal("premiumservicesflags"))) PremiumServicesFlags = dr.GetInt32(dr.GetOrdinal("premiumservicesflags"));
            if (!dr.IsDBNull(dr.GetOrdinal("church_activity"))) Churchactivity = dr.GetInt32(dr.GetOrdinal("church_activity"));
            if (!dr.IsDBNull(dr.GetOrdinal("ministryid"))) Ministryid = dr.GetInt32(dr.GetOrdinal("ministryid"));
            if (!dr.IsDBNull(dr.GetOrdinal("eyes"))) Eyes = dr.GetByte(dr.GetOrdinal("eyes"));
            if (!dr.IsDBNull(dr.GetOrdinal("hair"))) Hair = dr.GetByte(dr.GetOrdinal("hair"));
            if (!dr.IsDBNull(dr.GetOrdinal("intention"))) Intention = dr.GetByte(dr.GetOrdinal("intention"));
            //columns for match percentage calculation
            if (!dr.IsDBNull(dr.GetOrdinal("wantsethnicity"))) WantsEthnicity = dr.GetInt32(dr.GetOrdinal("wantsethnicity"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantssmoke"))) WantsSmoke = dr.GetInt32(dr.GetOrdinal("wantssmoke"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantsdrink"))) WantsDrink = dr.GetInt32(dr.GetOrdinal("wantsdrink"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantsreligion"))) WantsReligion = dr.GetInt32(dr.GetOrdinal("wantsreligion"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantsmaritalstatus"))) WantsMaritalStatus = dr.GetInt32(dr.GetOrdinal("wantsmaritalstatus"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantsbodytype"))) WantsBodyType = dr.GetInt32(dr.GetOrdinal("wantsbodytype"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantschurchactivity"))) WantsChurchActivity = dr.GetInt32(dr.GetOrdinal("wantschurchactivity"));
            if (!dr.IsDBNull(dr.GetOrdinal("wantseducation"))) WantsEducation = dr.GetInt32(dr.GetOrdinal("wantseducation"));
            if (!dr.IsDBNull(dr.GetOrdinal("importanceethnicity"))) ImportanceEthnicity = dr.GetByte(dr.GetOrdinal("importanceethnicity"));
            if (!dr.IsDBNull(dr.GetOrdinal("importancesmoke"))) ImportanceSmoke = dr.GetByte(dr.GetOrdinal("importancesmoke"));
            if (!dr.IsDBNull(dr.GetOrdinal("importancedrink"))) ImportanceDrink = dr.GetByte(dr.GetOrdinal("importancedrink"));
            if (!dr.IsDBNull(dr.GetOrdinal("importancereligion"))) ImportanceReligion = dr.GetByte(dr.GetOrdinal("importancereligion"));
            if (!dr.IsDBNull(dr.GetOrdinal("importancemaritalstatus"))) ImportanceMaritalStatus = dr.GetByte(dr.GetOrdinal("importancemaritalstatus"));
            if (!dr.IsDBNull(dr.GetOrdinal("importancebodytype"))) ImportanceBodyType = dr.GetByte(dr.GetOrdinal("importancebodytype"));
            if (!dr.IsDBNull(dr.GetOrdinal("importancechurchactivity"))) ImportanceChurchActivity = dr.GetByte(dr.GetOrdinal("importancechurchactivity"));
            if (!dr.IsDBNull(dr.GetOrdinal("importanceeducation"))) ImportanceEducation = dr.GetByte(dr.GetOrdinal("importanceeducation"));
            return this;
        }
    }

    #endregion


}
