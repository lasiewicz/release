﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.MingleSearchEngine.ValueObjects
{
    public enum IProcessorType
    {
        MemberIndexProcessor = 1,
        KeywordIndexProcessor = 2,
        AdminIndexProcessor = 3
    }

    public enum SearchResultType
    {
        TerseResult=1,
        DetailedResult=2
    }

    public enum COMMUNITY_ID : int
    {
        Spark = 1,
        Jdate = 3,
        Corp = 8,
        Cupid = 10,
        College = 12,
        Mingle = 20,
        ItalianSingles = 21,
        InterRacialSingles = 22,
        BBWPersonalsPlus = 23,
        BlackSingles = 24,
        ChristianMingle = 25
    }

    public class ServiceConstants
    {
        public const string SERVICE_INDEXER_CONST = "MINGLESEARCHINDEXER_SVC";
        public const string SERVICE_INDEXER_NAME = "Spark.MingleSearchIndexer.Service";

        public const string SERVICE_SEARCHER_CONST = "{0}SEARCHER_SVC";
        public const string SERVICE_SEARCHER_NAME = "Spark.{0}Searcher.Service";
        public const string SEARCHER_SA_HOST_OVERRIDE = "{0}SEARCHER_SA_HOST_OVERRIDE";

        public const string SERVICE_SEARCHER_MANAGER_CONST = "SearcherSM";

        public const string SERVICE_INDEXER_MANAGER_CONST = "IndexerSM";
        public const string INDEX_DIR_LOCK_FILE_FORMAT = "{0}.lock";
        public const string SYSTEM_DB_NAME = "mnSystem";
        public const string MINGLE_DB_NAME = "Mingle";


        public static string GetCommunityName(int communityId)
        {
            string communityName = Enum.Parse(typeof(COMMUNITY_ID), communityId.ToString()).ToString().ToUpper();
            return communityName;
        }

        public static int GetCommunityId(string communityName)
        {
            int communityId = Convert.ToInt32(Enum.Parse(typeof(COMMUNITY_ID), communityName));
            return communityId;
        }
    }
}
