﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Net;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.Spatial;
using Lucene.Net.Spatial.BBox;
using Lucene.Net.Util;
using Matchnet;
using Spark.Logging;
using Spark.MingleSearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.MingleSearchEngine.BusinessLogic.Documents
{
    [Serializable]
    public class SearchDocument
    {
        private Document _document;
        private DocumentMetadata _documentMetadata;
        private Dictionary<string, Field> _documentFields = new Dictionary<string, Field>();
        private Dictionary<string, NumericField> _documentNumericFields = new Dictionary<string, NumericField>();
        private Dictionary<string, Dictionary<object, NumericField>> _documentMaskNumericFields = new Dictionary<string, Dictionary<object, NumericField>>();

        public int ID { get; set; }
        private static object _lockObject = new object();

        private SpatialContext _context;
        private SpatialStrategy _strategy;
        private Dictionary<string, AbstractField[]> _spatialShapeFields = null;

        public string CommunityName { get; set; }

        public SpatialContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public SpatialStrategy Strategy
        {
            get { return _strategy; }
            set { _strategy = value; }
        }

        public Dictionary<string, AbstractField[]> SpatialShapeFields
        {
            get { return _spatialShapeFields; }
            set { _spatialShapeFields = value; }
        }

        private void AddSpatialShapeFields(string latLongKey, AbstractField [] fields)
        {
            lock (_lockObject)
            {
                if (null != SpatialShapeFields && !SpatialShapeFields.ContainsKey(latLongKey))
                {
                    SpatialShapeFields.Add(latLongKey, fields);
                }
            }
        }

        private bool HasSpatialShapeFields(string latLongKey)
        {
            bool b = false;
            lock (_lockObject)
            {
                if (null != SpatialShapeFields)
                {
                    b = SpatialShapeFields.ContainsKey(latLongKey);
                }
            }
            return b;
        }


        public SearchDocument(DocumentMetadata metadata)
        {
            _document = new Document();
            _documentMetadata = metadata;
        }

        public Document LuceneDocument
        {
            get { return _document; }
            set { _document = value; }
        }

        #region Lucence Indexing Optimization
        private Field GetField(string fieldName, string value, Field.Index fieldIndex, Field.Store fieldStore, float boost, bool useTermVectors)
        {
            Field f=null;
            if (_documentFields.ContainsKey(fieldName))
            {
                f = _documentFields[fieldName];
                if (null != f && string.IsNullOrEmpty(f.StringValue))
                {
                    f.SetValue(value);
                    if (boost > 0)
                    {
                        f.Boost=boost;
                    }
                }
            }

            if (null == f)
            {
                if (useTermVectors)
                {
                    f = new Field(fieldName, value, fieldStore, fieldIndex, Field.TermVector.YES);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                else
                {
                    f = new Field(fieldName, value, fieldStore, fieldIndex);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                if(!_documentFields.ContainsKey(fieldName)) _documentFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, int value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f=null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && int.MinValue.Equals((int) f.NumericValue))
                {
                    f.SetIntValue(value);
                    if (boost > 0)
                    {
                        f.Boost=boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetIntValue(value);
                if (boost > 0)
                {
                        f.Boost=boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && double.MinValue.Equals((double) f.NumericValue))
                {
                    f.SetDoubleValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, int precisionLevel, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && double.MinValue.Equals((double)f.NumericValue))
                {
                    f.SetDoubleValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, precisionLevel, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, long value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && long.MinValue.Equals((long) f.NumericValue))
                {
                    f.SetLongValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetLongValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, int value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && int.MinValue.Equals((int)f.NumericValue))
                    {
                        f.SetIntValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetIntValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName)) _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && double.MinValue.Equals((double)f.NumericValue))
                    {
                        f.SetDoubleValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName)) _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, long value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && long.MinValue.Equals((long)f.NumericValue))
                    {
                        f.SetLongValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetLongValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName)) _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        public void ClearAllFieldValues()
        {
            foreach (Field f in _documentFields.Values)
            {
                f.SetValue(string.Empty);
                if (f.Boost > 0)
                {
                    f.Boost=0f;
                }
                LuceneDocument.RemoveFields(f.Name);
            }

            foreach (NumericField nf in _documentNumericFields.Values)
            {
                if (nf.NumericValue is int) nf.SetIntValue(int.MinValue);
                if (nf.NumericValue is double) nf.SetDoubleValue(double.MinValue);
                if (nf.NumericValue is long) nf.SetLongValue(long.MinValue);
                if (nf.Boost > 0)
                {
                    nf.Boost=0f;
                }
                LuceneDocument.RemoveFields(nf.Name);
            }

            foreach (Dictionary<object, NumericField> dnf in _documentMaskNumericFields.Values)
            {
                foreach (NumericField nf in dnf.Values)
                {
                    if (nf.NumericValue is int) nf.SetIntValue(int.MinValue);
                    if (nf.NumericValue is double) nf.SetDoubleValue(double.MinValue);
                    if (nf.NumericValue is long) nf.SetLongValue(long.MinValue);
                    if (nf.Boost > 0)
                    {
                        nf.Boost=0f;
                    }
                    LuceneDocument.RemoveFields(nf.Name);
                }
            }
        }
        #endregion

        public void Populate(DataRow rs, DocumentMetadata docMetadata)
        {
            try
            {
                for (int i = 0; i < docMetadata.Fields.Count; i++)
                {
                    SearchField fld = docMetadata.Fields[i];
                    if (fld == null)
                        continue;

                    if (fld.FieldType != DataType.LOCATION && fld.FieldType != DataType.AREACODE && fld.FieldType != DataType.TXT)
                    {
                        Add(fld, rs[fld.Name].ToString());
                    }
                    else if (fld.FieldType == DataType.TXT)
                    {
                        Add(fld, rs[fld.Name].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(string.Format(ServiceConstants.SERVICE_INDEXER_CONST, CommunityName.ToUpper()), "Document", "Populate()", ex, null);
            }
        }

        public void Populate(MingleSearchKeyword keyword, DocumentMetadata docMetadata)
        {
            try
            {
                Add(docMetadata.GetField("UserID"), keyword.UserID.ToString());
                Add(docMetadata.GetField("CommunityID"), keyword.CommunityID.ToString());
                Add(docMetadata.GetField("LastLogin"), keyword.LastLogin.ToString());
                Add(docMetadata.GetField("LocationBHRegionID"), keyword.LocationBHRegionID.ToString());
                Add(docMetadata.GetField("LastUpdateYMDT"), keyword.LastUpdateYMDT.ToString());
                // Converting radian lat long to degrees because it is what is expected by Spatial.net
                AddLocation(DistanceUtils.ToDegrees(keyword.Latitude), DistanceUtils.ToDegrees(keyword.Longitude));
                Add(docMetadata.GetField("anythingelse"), keyword.AnythingElse);
                Add(docMetadata.GetField("bottomthreethings"), keyword.BottomThreeThings);
                Add(docMetadata.GetField("bravery"), keyword.Bravery);
                Add(docMetadata.GetField("car"), keyword.Car);
                Add(docMetadata.GetField("christianmeaning"), keyword.ChristianMeaning);
                Add(docMetadata.GetField("collegemajor"), keyword.CollegeMajor);
                Add(docMetadata.GetField("corecolor"), keyword.CoreColor);
                Add(docMetadata.GetField("favoritecalling"), keyword.FavoriteCalling);
                Add(docMetadata.GetField("favoritescripture"), keyword.FavoriteScripture);
                Add(docMetadata.GetField("firstdate"), keyword.FirstDate);
                Add(docMetadata.GetField("fiveyears"), keyword.FiveYears);
                Add(docMetadata.GetField("greeting"), keyword.Greeting);
                Add(docMetadata.GetField("howfound"), keyword.HowFound);
                Add(docMetadata.GetField("improvement"), keyword.Improvement);
                Add(docMetadata.GetField("lifeimportant"), keyword.LifeImportant);
                Add(docMetadata.GetField("ministryother"), keyword.MinistryOther);
                Add(docMetadata.GetField("movie"), keyword.Movie);
                Add(docMetadata.GetField("occupation"), keyword.Occupation);
                Add(docMetadata.GetField("pastrelationships"), keyword.PastRelationships);
                Add(docMetadata.GetField("perfectday"), keyword.PerfectDay);
                Add(docMetadata.GetField("postalcode"), keyword.PostalCode);
                Add(docMetadata.GetField("qow"), keyword.Qow);
                Add(docMetadata.GetField("reallocation"), keyword.RealLocation);
                Add(docMetadata.GetField("scripture"), keyword.Scripture);
                Add(docMetadata.GetField("serviceproject"), keyword.ServiceProject);
                Add(docMetadata.GetField("tenmillion"), keyword.TenMillion);
                Add(docMetadata.GetField("topthreethings"), keyword.TopThreeThings);
                Add(docMetadata.GetField("tvshow"), keyword.TvShow);
                Add(docMetadata.GetField("username"), keyword.UserName);
                Add(docMetadata.GetField("whymilitary"), keyword.WhyMilitary);
                Add(docMetadata.GetField("whyknowme"), keyword.WhyKnowMe);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", "Populate(SearchKeyword)", ex, CommunityName);
                throw ex;
            }
        }

        public void Populate(MingleSearchMember member)
        {
            Populate(member, _documentMetadata);
        }

        public void Populate(MingleSearchMember member, DocumentMetadata docMetadata)
        {
            try
            {
                Add(docMetadata.GetField("UserID"), member.UserID.ToString());
                Add(docMetadata.GetField("CommunityID"), member.CommunityID.ToString());
                Add(docMetadata.GetField("Gender"), member.Gender.ToString());
                Add(docMetadata.GetField("BodyType"), member.BodyType.ToString());
                Add(docMetadata.GetField("Birthdate"), member.Birthdate.ToString());
                Add(docMetadata.GetField("JoinDate"), member.JoinDate.ToString());
                Add(docMetadata.GetField("LastLogin"), member.LastLogin.ToString());
                Add(docMetadata.GetField("Photo"), member.PhotoFlag.ToString());
                Add(docMetadata.GetField("Education"), member.Education.ToString());
                Add(docMetadata.GetField("Religion"), member.Religion.ToString());
                Add(docMetadata.GetField("Language"), member.Language.ToString());
                Add(docMetadata.GetField("Ethnicity"), member.Ethnicity.ToString());
                Add(docMetadata.GetField("Smoke"), member.Smoke.ToString());
                Add(docMetadata.GetField("Drink"), member.Drink.ToString());
                Add(docMetadata.GetField("Height"), member.Height.ToString());
                Add(docMetadata.GetField("MaritalStatus"), member.MaritalStatus.ToString());
                Add(docMetadata.GetField("LocationBHRegionID"), member.LocationBHRegionID.ToString());
                Add(docMetadata.GetField("LastUpdateYMDT"), member.LastUpdateYMDT.ToString());
                Add(docMetadata.GetField("Children"), member.Children.ToString());
                Add(docMetadata.GetField("AboutChildren"), member.AboutChildren.ToString());
                Add(docMetadata.GetField("ChildrenAtHome"), member.ChildrenAtHome.ToString());
                Add(docMetadata.GetField("ColorB"), member.ColorB.ToString());
                Add(docMetadata.GetField("ColorR"), member.ColorR.ToString());
                Add(docMetadata.GetField("ColorW"), member.ColorW.ToString());
                Add(docMetadata.GetField("ColorY"), member.ColorY.ToString());
                // Converting radian lat long to degrees because it is what is expected by Spatial.net
                AddLocation(DistanceUtils.ToDegrees(member.Latitude),DistanceUtils.ToDegrees(member.Longitude));
                Add(docMetadata.GetField("PremiumServicesFlags"), member.PremiumServicesFlags.ToString());
                Add(docMetadata.GetField("Churchactivity"), member.Churchactivity.ToString());
                Add(docMetadata.GetField("Ministryid"), member.Ministryid.ToString());
                Add(docMetadata.GetField("Eyes"), member.Eyes.ToString());
                Add(docMetadata.GetField("Hair"), member.Hair.ToString());
                Add(docMetadata.GetField("Intention"), member.Intention.ToString());
                //fields for match percentage calculation
                Add(docMetadata.GetField("wantsethnicity"), member.WantsEthnicity.ToString());
                Add(docMetadata.GetField("wantssmoke"), member.WantsSmoke.ToString());
                Add(docMetadata.GetField("wantsdrink"), member.WantsDrink.ToString());
                Add(docMetadata.GetField("wantsreligion"), member.WantsReligion.ToString());
                Add(docMetadata.GetField("wantsmaritalstatus"), member.WantsMaritalStatus.ToString());
                Add(docMetadata.GetField("wantsbodytype"), member.WantsBodyType.ToString());
                Add(docMetadata.GetField("wantschurchactivity"), member.WantsChurchActivity.ToString());
                Add(docMetadata.GetField("wantseducation"), member.WantsEducation.ToString());
                Add(docMetadata.GetField("importanceethnicity"), member.ImportanceEthnicity.ToString());
                Add(docMetadata.GetField("importancesmoke"), member.ImportanceSmoke.ToString());
                Add(docMetadata.GetField("importancedrink"), member.ImportanceDrink.ToString());
                Add(docMetadata.GetField("importancereligion"), member.ImportanceReligion.ToString());
                Add(docMetadata.GetField("importancemaritalstatus"), member.ImportanceMaritalStatus.ToString());
                Add(docMetadata.GetField("importancebodytype"), member.ImportanceBodyType.ToString());
                Add(docMetadata.GetField("importancechurchactivity"), member.ImportanceChurchActivity.ToString());
                Add(docMetadata.GetField("importanceeducation"), member.ImportanceEducation.ToString());
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", "Populate(SearchMember)", ex, CommunityName);
                throw ex;
            }
        }
        
        public void Add(SearchField fld, string value)
        {
            if (fld.IsDocumentID)
            {

                int id = Conversion.CInt(value);
                if (id > 0)
                {
                    int val = Conversion.CInt(value);
                    _document.Add(GetNumericField("id", (int)val, true, Field.Store.YES, fld.Boost)); //new Field("id", value, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    ID = id;
                    return;
                }
            }
            switch (fld.FieldType)
            {
                case DataType.LONG:
                case DataType.INT:
                case DataType.DOUBLE:
                    addNumeric(fld, value);
                    break;
                case DataType.MASK:
                    addMask(fld, value);
                    break;
                case DataType.DATE:
                    addDate(fld, value);
                    break;
                case DataType.TXT:
                    addText(fld, value);
                    break;
            }
        }

        public void AddLocation(double lat, double lng)
        {
            try
            {
                string latLongKey = lat.ToString() + "," + lng.ToString();
                AbstractField[] shapeFields = null;
                if (HasSpatialShapeFields(latLongKey))
                {
                    shapeFields = SpatialShapeFields[latLongKey];
                }
                else
                {
                    Shape shape = null;
                    if (this.Strategy is BBoxStrategy)
                    {
                        shape = Context.MakeRectangle(DistanceUtils.NormLonDEG(lng), DistanceUtils.NormLonDEG(lng), DistanceUtils.NormLatDEG(lat), DistanceUtils.NormLatDEG(lat));
                    }
                    else
                    {
                        shape = Context.MakePoint(DistanceUtils.NormLonDEG(lng), DistanceUtils.NormLatDEG(lat));
                    }
                    shapeFields = Strategy.CreateIndexableFields(shape);
                    AddSpatialShapeFields(latLongKey, shapeFields);
                }

                //Potentially more than one shape in this field is supported by some
                // strategies; see the javadocs of the SpatialStrategy impl to see.
                foreach (AbstractField f in shapeFields)
                {
                    //For PointVectorStrategy, AbstractField is always a NumericField
                    if (f is NumericField)
                    {
                        NumericField nf = (NumericField)f;
                        double val = Double.Parse(nf.StringValue);
                        NumericField field = GetNumericField(f.Name, Utils.PrecisionLevel, val, f.IsIndexed, Field.Store.YES, f.Boost);
                        field.OmitNorms = true;
                        field.OmitTermFreqAndPositions = true;
                        _document.Add(field);
                    }
                }
                //add lat long values to index too, VERY IMPORTANT - DO NOT ALTER THIS CODE
                _document.Add(GetField("latitude", NumericUtils.DoubleToPrefixCoded(lat), Field.Index.NOT_ANALYZED, Field.Store.YES, 0f, false));
                _document.Add(GetField("longitude", NumericUtils.DoubleToPrefixCoded(lng), Field.Index.NOT_ANALYZED, Field.Store.YES, 0f, false));
            }
            catch (Exception e)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", string.Format("AddLocation({0},{1})", lat.ToString(), lng.ToString()), e, null);
                throw e;
            }
        }


        private void addNumeric(SearchField fld, String value)
        {
            try
            {
                switch (fld.FieldType)
                {
                    case DataType.INT:
                        {
                            int val = Conversion.CInt(value);
                            _document.Add(GetNumericField(fld.Name, (int)val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.DOUBLE:
                        {
                            double val = Conversion.CDouble(value);
                            if (val > 0)
                                _document.Add(GetNumericField(fld.Name, (double)val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.LONG:
                        {
                            long val = Conversion.CInt(value);
                            _document.Add(GetNumericField(fld.Name, (long)val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.MASK:
                        {
                            int val = Conversion.CInt(value);
                            _document.Add(GetMaskNumericField(fld.Name, (int)val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void addText(SearchField fld, string value)
        {
            if (null != value)
            {
                Field field = GetField(fld.Name, value, fld.getLuceneIndex(), fld.getLuceneStore(), fld.Boost, true);
                _document.Add(field);
            }
        }

        private void addDate(SearchField fld, String value)
        {
            addDate(fld, Conversion.CDateTime(value));
        }

        private void addDate(SearchField fld, DateTime value)
        {
            _document.Add(GetNumericField(fld.Name, value.Ticks, true, fld.getLuceneStore(), fld.Boost));
        }

        private void addMask(SearchField fld, String value)
        {
            Int32 mask = Conversion.CInt(value);
            List<int> intValuesFromMask = Utils.GetIntValuesFromMask(mask);
            foreach (int i in intValuesFromMask)
            {
                addNumeric(fld,i.ToString());
            }
        }
    }
}
