﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net;
using System.Xml.Serialization;

namespace Spark.MingleSearchEngine.BusinessLogic.Documents
{
    public enum IndexOption:int
    {
        NO=0,
        NOT_ANALYZED_NO_NORMS=1,
        ANALYZED = 2,
        NOT_ANALYZED = 4,
        ANALYZED_NO_NORMS=8
    }

    public enum StoreOption : int
    {
        YES = 1,
        NO = 2,
        COMPRESS=4
    }

    public enum DataType : int
    {
        TXT=0,
        INT=1,
        MASK=2,
        DATE=4,
        DOUBLE=8,
        LONG=16,
        LOCATION=32,
        AREACODE=64
    }

    [Serializable]
    public class SearchField
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
        [XmlAttribute("Index")]
        public IndexOption Index { get; set; }
        [XmlAttribute("Store")]
        public StoreOption Store { get; set; }
        [XmlAttribute("FieldType")]
        public DataType FieldType { get; set; }
        [XmlAttribute("Boost")]
        public float Boost { get; set; }

        [XmlAttribute("IsDocumentID")]
        public bool IsDocumentID { get; set; }
       
        public Lucene.Net.Documents.Field.Store  getLuceneStore()
        {
            switch(Store)
            {
                case StoreOption.YES:
                    return Lucene.Net.Documents.Field.Store.YES;                   
                case StoreOption.NO:
                    return Lucene.Net.Documents.Field.Store.NO;
                default:
                    return Lucene.Net.Documents.Field.Store.NO;
                   
            }
        }
       
        public Lucene.Net.Documents.Field.Index getLuceneIndex()
        {
            switch(Index)
            {
                case IndexOption.ANALYZED:
                    return Lucene.Net.Documents.Field.Index.ANALYZED;
                case IndexOption.NO:
                    return Lucene.Net.Documents.Field.Index.NO;
                case IndexOption.ANALYZED_NO_NORMS:
                    return Lucene.Net.Documents.Field.Index.ANALYZED_NO_NORMS;
                case IndexOption.NOT_ANALYZED:
                    return Lucene.Net.Documents.Field.Index.NOT_ANALYZED;
                case IndexOption.NOT_ANALYZED_NO_NORMS:
                    return Lucene.Net.Documents.Field.Index.NOT_ANALYZED_NO_NORMS;
                default:
                    return Lucene.Net.Documents.Field.Index.NOT_ANALYZED_NO_NORMS;
                   
            }
        }
    }
}
