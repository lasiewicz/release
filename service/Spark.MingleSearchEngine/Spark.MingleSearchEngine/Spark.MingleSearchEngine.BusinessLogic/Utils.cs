﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Lucene.Net.Documents;
using Lucene.Net.Spatial;
using Lucene.Net.Spatial.Vector;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Exceptions;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Caching;
using Matchnet.Data;
using Spark.Logging;
using System.Data;
using Spark.MingleSearchEngine.BusinessLogic.Documents;
using Spark.MingleSearchEngine.BusinessLogic.Indexer;
using Spark.MingleSearchEngine.BusinessLogic.Searcher;
using Spark.MingleSearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using ServiceConstants = Spark.MingleSearchEngine.ValueObjects.ServiceConstants;

namespace Spark.MingleSearchEngine.BusinessLogic
{
   public class Utils
    {
    #region Settings helpful
       private static Dictionary<int, int> brands = new Dictionary<int, int>();
       private static Dictionary<int, int> sites = new Dictionary<int, int>();      
       private static object _lockObject = new object();
       private static RegionAreaCodeDictionary _areaCodeDictionary = null;
       private static Dictionary<int, RegionLanguage> _regionLanguages = new Dictionary<int, RegionLanguage>();
       private static Dictionary<string, AbstractField[]> _spatialShapeFields = new Dictionary<string, AbstractField[]>();
       private static object _metadataLockObject = new object();
       private static Dictionary<int, DocumentMetadata> _documentMetadata = new Dictionary<int, DocumentMetadata>();
       public const int MAX_ROLLING_DIRS = 3;
       private static SpatialContext _spatialContext;
       private static SpatialStrategy _spatialStrategy;

       private static ISettingsSA _settingsService = null;

       public static ISettingsSA SettingsService
       {
           get
           {
               if (null == _settingsService) return RuntimeSettings.Instance;
               return _settingsService;
           }
           set { _settingsService = value; }
       }

       public static RegionAreaCodeDictionary RegionAreaCodeDictionary
       {
           get
           {
               if (null == _areaCodeDictionary)
               {
                   lock (_lockObject)
                   {
                       if (null == _areaCodeDictionary)
                       {
                           _areaCodeDictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();
                       }
                   }
               }
               return _areaCodeDictionary;
           }
       }

       public static Dictionary<int, RegionLanguage> RegionLanguages { get { return _regionLanguages; } }

       public static Dictionary<string, AbstractField[]> SpatialShapeFields
       {
           get { return _spatialShapeFields; }
       }

       public static SpatialContext SpatialContext
       {
           get
           {
               if (null == _spatialContext)
               {
                   lock (_lockObject)
                   {
                       if(null==_spatialContext) _spatialContext = SpatialContext.GEO;
                   }
               }
               return _spatialContext;
           }
       }

       public static SpatialStrategy SpatialStrategy
       {
           get
           {
               if (null == _spatialStrategy)
               {
                   lock (_lockObject)
                   {
                       if (null == _spatialStrategy)
                       {
//                           GeohashPrefixTree geohashPrefixTree = new GeohashPrefixTree(SpatialContext, PrecisionLevel);
//                           _spatialStrategy = new RecursivePrefixTreeStrategy(geohashPrefixTree, "geoField");

                           // using PVS for optimizer
                           PointVectorStrategy pointVectorStrategy = new SparkPointVectorStrategy(SpatialContext, "geoField");
                           pointVectorStrategy.SetPrecisionStep(PrecisionLevel);
                           _spatialStrategy = pointVectorStrategy;
                       }
                   }
               }
               return _spatialStrategy;
           }
       }

       public static int PrecisionLevel
       {
           get
           {
               int precisionLevel = 11;
               return precisionLevel;
           }
       }

       public static DocumentMetadata GetDocMetadata(int communityID)
       {
           if (!_documentMetadata.ContainsKey(communityID))
           {
               lock (_metadataLockObject)
               {
                   if (!_documentMetadata.ContainsKey(communityID))
                   {
                       string metaFilePath = AppDomain.CurrentDomain.BaseDirectory + "/" + GetSetting("SEARCHER_COMMUNITY_METADATA_FILE", communityID, "SearchDocument.xml");
                       DocumentMetadata documentMetaData = Serialization.FromXmlFile<DocumentMetadata>(metaFilePath);
                       _documentMetadata.Add(communityID, documentMetaData);
                   }
               }
           }

           return _documentMetadata[communityID];
       }

       public static string GetSetting(string name, string defaultvalue)
       {
           try
           {
               return SettingsService.GetSettingFromSingleton(name);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static string GetSetting(string name, int communityid, string defaultvalue)
       {
           try
           {
               return SettingsService.GetSettingFromSingleton(name,communityid);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static int GetSetting(string name, int defaultvalue)
       {
           try
           {
             return  Conversion.CInt(SettingsService.GetSettingFromSingleton(name),defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static double GetSetting(string name, double defaultvalue)
       {
           try
           {
               return Conversion.CDouble(SettingsService.GetSettingFromSingleton(name), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }
       
       public static DateTime GetSetting(string name, DateTime defaultvalue)
       {
           try
           {
               return Conversion.CDateTime(SettingsService.GetSettingFromSingleton(name), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static bool GetSetting(string name, bool defaultvalue)
       {
           try
           {
               return Conversion.CBool(SettingsService.GetSettingFromSingleton(name), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static bool GetSetting(string name, int communityid, bool defaultvalue)
       {
           try
           {
               return Conversion.CBool(SettingsService.GetSettingFromSingleton(name, communityid), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static int GetSetting(string name, int communityId, int defaultvalue)
       {
           try
           {
               return Conversion.CInt(SettingsService.GetSettingFromSingleton(name, communityId), defaultvalue);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static int GetMaxResults(int communityId)
       {
           int maxResults = GetSetting("SEARCHER_MAX_RESULTS", communityId, 1000);
           return maxResults;
       }

       public static List<int> GetIntValuesFromMask(int maskValue)
       {
           List<int> results = new List<int>();
           BitArray arr = new BitArray(BitConverter.GetBytes(maskValue));
           int idx = 0;
           foreach (bool b in arr.Cast<bool>())
           {
               if (b)
               {
                   results.Add((int)Math.Pow(2,idx));
               }
               idx++;
           }
           return results;
       }

       public static AttributeOptionCollection GetIntValuesFromMask(string attributeName, int communityId)
       {
           AttributeOptionCollection optionCollection = null;
           CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(attributeName, communityId)) as CacheAttributeOptionCollection;
           if (null == cachedOptionCollection)
           {
               if ("colorcode".Equals(attributeName.ToLower()))
               {
                   //0 = no, 1 = white, 2 = blue, 4 = yellow, 8 = red
                   AttributeOptionCollection colorCodes = new AttributeOptionCollection();
                   colorCodes.Add(new AttributeOption(1, 999999, 1, 1, "white", String.Empty));
                   colorCodes.Add(new AttributeOption(2, 999999, 2, 2, "blue", String.Empty));
                   colorCodes.Add(new AttributeOption(3, 999999, 4, 3, "yellow", String.Empty));
                   colorCodes.Add(new AttributeOption(4, 999999, 8, 4, "red", String.Empty));
                   optionCollection = colorCodes;
               }
               else
               {
                   optionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, GetBrandIdsByCommunity()[communityId]);
               }

               if (null != optionCollection)
               {
                   cachedOptionCollection = new CacheAttributeOptionCollection(optionCollection, attributeName, communityId);
                   Cache.Instance.Add(cachedOptionCollection);
               }
           }
           else
           {
               optionCollection = cachedOptionCollection.OptionCollection;
           }
           return optionCollection;
       }

       public static AttributeOptionCollection GetAttributeOptionCollection(string attributeName, int communityId)
       {
           CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(attributeName, communityId)) as CacheAttributeOptionCollection;
           if (null == cachedOptionCollection)
           {
               AttributeOptionCollection optionCollection=AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, Constants.NULL_INT);
               cachedOptionCollection = new CacheAttributeOptionCollection(optionCollection, attributeName, communityId);
               Cache.Instance.Add(cachedOptionCollection);
           }
           return cachedOptionCollection.OptionCollection;           
       }

       public static AttributeOptionCollection GetAttributeOptionCollection(string attributeName, int communityId, int siteId, int brandId)
       {
           CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(attributeName, communityId)) as CacheAttributeOptionCollection;
           if (null == cachedOptionCollection)
           {
               AttributeOptionCollection optionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, communityId, siteId, brandId);
               if (null != optionCollection)
               {
                   cachedOptionCollection = new CacheAttributeOptionCollection(optionCollection, attributeName, communityId);
                   Cache.Instance.Add(cachedOptionCollection);
               }
           }
           return cachedOptionCollection.OptionCollection;
       }

       public static double[] ConvertRadiansToDegrees(double latRadians, double lngRadians)
       {
           double [] degrees = new double[2];
           degrees[0] = DistanceUtils.ToDegrees(latRadians);
           degrees[1] = DistanceUtils.ToDegrees(lngRadians);
           return degrees;
       }

       //gets current index directory for searcher server if swapping directory index is enabled - ENABLE_INDEX_DIR_SWAP_UPDATES
       public static string GetCurrentIndexDirectory(int communityid, IProcessorType iProcessorType, string server, string caller)
       {
           string path = "";

           SqlDataReader rs = null;
           try
           {
               //yeah, no caching for now
               //but it shouldn't be big load
               Command command = new Command(ServiceConstants.SYSTEM_DB_NAME, "[up_GetCurrentIndexDir]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityid);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, server);
               command.AddParameter("@IProcessorTypeID", SqlDbType.Int, ParameterDirection.Input, (int)iProcessorType);
               rs = Client.Instance.ExecuteReader(command);
               if (rs.Read())
               {
                   path = rs["path"].ToString();
               }               
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, caller, "GetCurrentIndexDirectory", ex, communityid.ToString());
               throw (new BLException(ex));
           }
           finally
           {
               rs.Dispose();
           }
           return path;
       }

       public static void UpdateIndexDirectory(int communityId, IProcessorType iProcessorType, string server, string indexPath, string caller)
       {
           try
           {
               Command command = new Command(ServiceConstants.SYSTEM_DB_NAME, "[up_InsertCurrentIndexDir]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, server);
               command.AddParameter("@Path", SqlDbType.VarChar, ParameterDirection.Input, indexPath);
               command.AddParameter("@IProcessorTypeID", SqlDbType.Int, ParameterDirection.Input, (int)iProcessorType);
               Client.Instance.ExecuteAsyncWrite(command);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, caller, "updateIndexDirectory", ex, null);
           }
       }

       public static int CalculateAge(DateTime birthDate)
       {
           // cache the current time
           DateTime now = DateTime.Today; // today is fine, don't need the timestamp from now
           // get the difference in years
           int years = now.Year - birthDate.Year;
           // subtract another year if we're before the
           // birth day in the current year
           if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
               --years;
           return years;
       }

       public static Dictionary<int, int> GetBrandIdsByCommunity()
       {
           if (brands.Count == 0)
           {
               brands.Add(1, 101);
               brands.Add(3, 1003);
               brands.Add(10, 1015);
               brands.Add(21, 9161);
               brands.Add(23, 9041);
               brands.Add(24, 9051);
           }
           return brands;
       }

       public static Dictionary<int, int> GetSiteIdsByCommunity()
       {
           if (sites.Count == 0)
           {
               sites.Add(1, 101);
               sites.Add(3, 103);
               sites.Add(10, 15);
               sites.Add(21, 9161);
               sites.Add(23, 9041);
               sites.Add(24, 9051);
               sites.Add(25, 9081);
           }
           return brands;
       }

       public static double GetRadiusOffsetBuffer(double radius)
       {
           double offset = (radius + 1.5) / 10;
           return radius + offset;
       }
    #endregion

       public static List<string> getSearcherServers(int communityId)
       {
           List<string> servers = new List<string>();

           try
           {
               string searcherConstant = String.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
               ArrayList partitions = AdapterConfigurationSA.GetServicePartitions(searcherConstant);
               for (int i = 0; null != partitions && i < partitions.Count; i++)
               {
                   ServicePartition p = partitions[i] as ServicePartition;
                   if (null != p)
                   {
                       for (int k = 0; k < p.Count; k++)
                       {
                           ServiceInstance si = p[k];
                           if (servers.Find(delegate(string s) { return s == si.HostName.ToLower(); }) == null)
                           {
                               servers.Add(si.HostName.ToLower());
                           }
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "getSearcherServers()", ex, null);
           }

           return servers;
       }

       public static string GetUNCTargetPath(string indexPath, int rollingIndexDirId, string server)
       {
           string uncPath = indexPath.Replace("/", "\\").Replace(":\\\\", ":\\");
           string targetpath = "\\\\{0}\\" + uncPath.Replace(":\\", "$\\") + rollingIndexDirId.ToString();
           string serverpath = String.Format(targetpath, server);
           return serverpath;
       }

       public static string IndexDirectoryInUse(CommunityIndexConfig c)
       {
           List<string> servers = getSearcherServers(c.CommunityID);
           string directoryInUse = String.Empty;
           foreach (string server in servers)
           {
               string indexDirectoryInUseForServer = IndexDirectoryInUseForServer(c, server);
               if(!String.IsNullOrEmpty(indexDirectoryInUseForServer))
               {
                   directoryInUse = indexDirectoryInUseForServer;
               }
           }
           return directoryInUse;
       }

       public static string IndexDirectoryInUseForServer(CommunityIndexConfig c, string server)
       {
           string directoryInUse=String.Empty;
               string uncPath = GetUNCTargetPath(c.IndexPath, c.RollingIndexDirectoryID, server);
               if (Directory.Exists(uncPath))
               {
                   string[] files = Directory.GetFiles(uncPath);
                   //don't overwrite an index if it is in use
                   foreach(string fileName in files)
                   {
                       if (fileName.EndsWith(String.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, server)))
                       {
                           directoryInUse = uncPath;
                   }
               }
           }
           return directoryInUse;
       }

       public static void CopyIndex(string sourcepath, string targetpath)
       {
           try
           {
               if (!Directory.Exists(targetpath))
               {
                   Directory.CreateDirectory(targetpath);
               }

               DirectoryInfo dirinfo = new DirectoryInfo(sourcepath);
               foreach (FileInfo f in dirinfo.GetFiles())
               {
                   string dest = targetpath + "\\" + f.Name;
                   f.CopyTo(dest, true);
               }
           }
           catch (Exception ex)
           {
               BLException blex = new BLException("Error copying index dir: " + sourcepath + " to " + targetpath + ", Inner Exception: " + ex.ToString(), ex);
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "CopyIndex", blex, null);
           }
       }

       public static void CleanDir(string path)
       {
           try
           {
               if (Directory.Exists(path))
               {
                   RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "Cleaning Index Dir: " + path, null);

                   DirectoryInfo dirinfo = new DirectoryInfo(path);
                   CleanDir(dirinfo);
                   RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "Finish Cleaning Index Dir: " + path, null);
               }

           }
           catch (Exception ex)
           {
               BLException blex = new BLException("Error cleaning index dir: " + path + ", Inner Exception: " + ex.ToString(), ex);
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "CleanDir", blex, null);
           }
       }

       private static void CleanDir(DirectoryInfo dirinfo)
       {
           foreach (FileInfo f in dirinfo.GetFiles())
           {
               f.Delete();
           }
           foreach (DirectoryInfo dir in dirinfo.GetDirectories())
           {
               dir.Delete(true);
           }
       }

       public static SqlDataReader GetCommunitySearchMembers(CommunityIndexConfig config)
       {
           SqlDataReader dataReader = null;
           try
           {               
               string getCommunityDataCommand = "[up_Mingle_OUT_byCommunity]";
               Command command = new Command(ServiceConstants.MINGLE_DB_NAME, getCommunityDataCommand, 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, config.CommunityID);
               command.DBTimeoutInSecs = GetSetting("INDEXER_DB_TIMEOUT", 90);
               if (!config.EnableRollingUpdates)
               {
                   if (config.LastIndexDate > DateTime.MinValue)
                   {
                       command.AddParameter("@LastIndexDate", SqlDbType.DateTime, ParameterDirection.Input, config.LastIndexDate);
                   }
               }
               if (config.LastActiveDays > 0)
               {
                   command.AddParameter("@LastActiveDays", SqlDbType.Int, ParameterDirection.Input, config.LastActiveDays);
               }

               dataReader = Client.Instance.ExecuteReader(command);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "GetCommunitySearchMembers()", ex, config);
           }
           return dataReader;
       }

       public static void updateLastIndexDate(CommunityIndexConfig c)
       {
           DateTime lastindexdate = DateTime.MinValue;
           try
           {
               int communityId = c.CommunityID;
               string caller = IndexerBL.CLASS_NAME;
               IProcessorType iProcessorType = c.IProcessorType;
               string dbName = ServiceConstants.SYSTEM_DB_NAME;
               Command command = new Command(dbName, "[up_InsertLastIndexDate]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, c.CommunityID);
               command.AddParameter("@IndexDate", SqlDbType.DateTime, ParameterDirection.Input, c.LastIndexDate);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, Environment.MachineName);
               command.AddParameter("@Rollingdirid", SqlDbType.Int, ParameterDirection.Input, c.RollingIndexDirectoryID);
               Client.Instance.ExecuteAsyncWrite(command);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "getLastIndexDate", ex, null);
           }
       }

       public static void CopyIndexToServer(object m)
       {
           string sourcepath = String.Empty;
           string serverPath = String.Empty;
           try
           {
               Hashtable map = m as Hashtable;
               CommunityIndexConfig config = map["config"] as CommunityIndexConfig;
               string server = map["server"] as string;
               serverPath = GetUNCTargetPath(config.IndexPath, config.RollingIndexDirectoryID, server);
               sourcepath = map["sourcepath"] as string;
               Stopwatch stopWatch = new Stopwatch();
               RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "Copying Index Dir: " + sourcepath + " to " + serverPath, null);
               CleanDir(serverPath as string);
               CopyIndex(sourcepath as string, serverPath as string);
               stopWatch.Stop();
               RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "Total Copy Time: " + ((stopWatch.ElapsedMilliseconds / 1000) / 60) + " minutes. Finished Copying Index Dir: " + sourcepath + " to " + serverPath, null);
               UpdateIndexDirectory(config.CommunityID, config.IProcessorType, server, serverPath, IndexerBL.CLASS_NAME);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, String.Format("CopyIndexToServer: [srcpath:{0}, destpath:{1}]", sourcepath, serverPath), ex, null);
           }
       }

       public static DateTime getLastIndexDate(int communityid, out int rollingdirid, IProcessorType iProcessorType)
       {
           DateTime lastindexdate = DateTime.MinValue;
           SqlDataReader rs = null;
           rollingdirid = 0;
           try
           {
               string caller = IndexerBL.CLASS_NAME;
               string dbName = ServiceConstants.SYSTEM_DB_NAME;
               Command command = new Command(dbName, "[up_GetLastIndexDate]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityid);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, Environment.MachineName);
               SqlParameterCollection sqlParams;
               rs = Client.Instance.ExecuteReader(command);

               if (rs.Read())
               {
                   lastindexdate = Conversion.CDateTime(rs["indexdate"]);
                   rollingdirid = Conversion.CInt(rs["rollingdirid"]);
               }
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "getLastIndexDate", ex, null);
           }
           finally
           {
               if (rs != null)
               { rs.Dispose(); }
           }
           return lastindexdate;
       }

       public static string CalculateIndexPath(CommunityIndexConfig c)
       {
           for (int i = 0; i < MAX_ROLLING_DIRS; i++)
           {
               string indexDirectoryInUse = IndexDirectoryInUse(c);
               if (!String.IsNullOrEmpty(indexDirectoryInUse))
               {
                   c.RollingIndexDirectoryID = (c.RollingIndexDirectoryID < MAX_ROLLING_DIRS) ? c.RollingIndexDirectoryID + 1 : 1;
               }
               else
               {
                   break;
               }
           }
           string path = c.IndexStagePath + c.RollingIndexDirectoryID.ToString();
           return path;
       }

       public static QueryBuilder GetTestQuery(int communityId, int siteId)
       {
           int genderVal = 1;
           int minAgeVal = 18;
           int maxAgeVal = 75;
           double radiansLat = 0.594637;
           double radiansLng = -2.066426;
           int radiusMI = 2000;

           SearchParameterCollection searchParameters = new SearchParameterCollection()
               .Add(new SearchParameter("maxResults", 2000, SearchParameterType.MaxResults))
               .Add(new SearchParameter("communityid",communityId,SearchParameterType.Int))
               .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
               .Add(new SearchParameter("birthdate", minAgeVal+"|"+maxAgeVal,SearchParameterType.AgeRangeFilter))
               .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
           searchParameters.Sort = SearchSorting.VerifyIndex;

           var queryBuilder = QueryBuilderFactory.Instance.GetQueryBuilder(searchParameters, communityId);
           return queryBuilder;
       }

       public static QueryBuilder GetTestKeywordQuery(int communityId, int siteId)
       {
           int genderVal = 1;
           double radiansLat = 0.594637;
           double radiansLng = -2.066426;
           int radiusMI = 2000;

           SearchParameterCollection searchParameters = new SearchParameterCollection()
               .Add(new SearchParameter("maxResults", 2000, SearchParameterType.MaxResults))
               .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
               .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
               .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
           searchParameters.Sort = SearchSorting.VerifyIndex;
           var queryBuilder = QueryBuilderFactory.Instance.GetQueryBuilder(searchParameters, communityId);
           return queryBuilder;
       }
    }

   public class CacheAttributeOptionCollection : ICacheable
   {
       private AttributeOptionCollection _optionCollection = null;
       private int _communityId;
       private string _attributeName;
       private int _cacheTTLSeconds = 540;
       private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
       private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
       const string CACHEKEYFORMAT = "AttributeOption-{0}-{1}";

       public AttributeOptionCollection OptionCollection
       {
           get { return _optionCollection; }
           set { _optionCollection = value; }
       }

       public int CommunityId
       {
           get { return _communityId; }
           set { _communityId = value; }
       }

       public string AttributeName
       {
           get { return _attributeName; }
           set { _attributeName = value; }
       }

       public CacheAttributeOptionCollection(AttributeOptionCollection collection, string attributeName, int commmunityId)
       {
           this._communityId = commmunityId;
           this._attributeName = attributeName;
           this._optionCollection = collection;
       }

       #region ICacheable Members

       public CacheItemMode CacheMode
       {
           get { return _cacheItemMode; }
           set { _cacheItemMode = value; }
       }

       public CacheItemPriorityLevel CachePriority
       {
           get { return _cachePriority; }
           set { _cachePriority = value; }
       }

       public int CacheTTLSeconds
       {
           get { return _cacheTTLSeconds; }
           set { _cacheTTLSeconds = value; }
       }

       public string GetCacheKey()
       {
           return GetCacheKeyString(_attributeName, _communityId);
       }

       public static string GetCacheKeyString(string attributeName, int communityId)
       {
           return string.Format(CACHEKEYFORMAT, attributeName, communityId);
       }


       #endregion
   }

   public static class Extensions
   {
       public static IList<TResult> GetBitsAs<TResult>(this BitArray bits) where TResult : struct
       {
           return GetBitsAs<TResult>(bits, 0);
       }

       /// <summary>
       /// Gets the bits from an BitArray as an IList combined to the given type.
       /// </summary>
       /// <typeparam name="TResult">The type of the result.</typeparam>
       /// <param name="bits">An array of bit values, which are represented as Booleans.</param>
       /// <param name="index">The zero-based index in array at which copying begins.</param>
       /// <returns>An read-only IList containing all bits combined to the given type.</returns>
       public static IList<TResult> GetBitsAs<TResult>(this BitArray bits, int index) where TResult : struct
       {
           var instance = default(TResult);
           var type = instance.GetType();
           int sizeOfType = Marshal.SizeOf(type);

           int arraySize = (int)Math.Ceiling(((bits.Count - index) / 8.0) / sizeOfType);
           var array = new TResult[arraySize];

           bits.CopyTo(array, index);

           return array;
       }
   }
}
