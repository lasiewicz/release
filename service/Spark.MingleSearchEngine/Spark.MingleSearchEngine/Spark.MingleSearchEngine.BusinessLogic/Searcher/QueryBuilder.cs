﻿using System.Collections.Generic;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Prefix;
using Spatial4n.Core.Shapes;

namespace Spark.MingleSearchEngine.BusinessLogic.Searcher
{
    public class QueryBuilder
    {
		public Filter spatialFilter = null;        
		public Dictionary<int,int> matchPercentages = new Dictionary<int, int>();
        public Dictionary<string, int> has = new Dictionary<string, int>();
        public Dictionary<string, int> wants = new Dictionary<string, int>();
        public Dictionary<string, double> importances = new Dictionary<string, double>();
        public List<Query> queries = new List<Query>();
        public List<Filter> filters = new List<Filter>();
        public List<SortField> sortFields = new List<SortField>();
        private bool _useSearchRedesign30 = false;
        private int maxNumberOfMatches = int.MinValue;

        public int CommunityId { get; set; }
        public Shape SpatialShape { get; set; }
        public int MaxNumberOfMatches
        {
            get { return maxNumberOfMatches; }
            set { maxNumberOfMatches = value; }
        }

        public void AddQuery(Query q)
        {
            queries.Add(q);
        }

        public void ClearQueries()
        {
            queries.Clear();
        }

        public void AddFilter(Filter f)
        {
            filters.Add(f);
        }

        public void AddFilterFirst(Filter f)
        {
            List<Filter> newfilters = new List<Filter>();
            newfilters.Add(f);
            foreach (Filter _f in filters)
            {
                newfilters.Add(_f);
            }
            filters = null;
            filters=newfilters;
        }

        public void AddSortField(SortField s)
        {
            sortFields.Add(s);
        }

        public void AddWants(string name, int value)
        {
            wants.Add(name, value);
        }

        public void AddImportance(string name, double value)
        {
            importances.Add(name, value);
        }

        public void AddHas(string name, int value)
        {
            has.Add(name,value);
        }

        public void AddMatchPercentage(int memberId, int matchPercentage)
        {
            matchPercentages.Add(memberId, matchPercentage);
        }
    }
}
