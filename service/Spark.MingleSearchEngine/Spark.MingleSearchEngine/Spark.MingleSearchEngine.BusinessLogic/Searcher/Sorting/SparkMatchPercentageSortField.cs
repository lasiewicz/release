﻿using System;
using System.Collections.Generic;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Util;

namespace Spark.MingleSearchEngine.BusinessLogic.Searcher.Sorting
{
    public class SparkMatchPercentageSortField : SortField
  	{
		private QueryBuilder _queryBuilder;

		public SparkMatchPercentageSortField(string field, bool reverse, QueryBuilder queryBuilder)
			: base(field, CUSTOM, reverse)
		{
			_queryBuilder=queryBuilder;
		}

		public override FieldComparator GetComparator(int numHits, int sortPos)
		{
			return new MatchPercentageFieldComparatorSource.MatchPercentageFieldComparator(_queryBuilder, numHits);
		}

		public override FieldComparatorSource ComparatorSource
		{
			get
			{
                return new MatchPercentageFieldComparatorSource(_queryBuilder);
			}
		} 
	}

	public class MatchPercentageFieldComparatorSource : FieldComparatorSource
	{
		private QueryBuilder _queryBuilder;

        public MatchPercentageFieldComparatorSource(QueryBuilder queryBuilder)
		{
			this._queryBuilder = queryBuilder;
		}

		public override FieldComparator NewComparator(string fieldname, int numHits, int sortPos, bool reversed)
		{
			return new MatchPercentageFieldComparator(_queryBuilder, numHits);
		}

		public class MatchPercentageFieldComparator : FieldComparator
		{
		    public const string ATTR_NAME_HASETHNICITY = "ethnicity";
		    public const string ATTR_NAME_HASSMOKE = "smoke";
		    public const string ATTR_NAME_HASDRINK = "drink";
		    public const string ATTR_NAME_HASRELIGION = "religion";
		    public const string ATTR_NAME_HASMARITALSTATUS = "maritalstatus";
		    public const string ATTR_NAME_HASBODYTYPE = "bodytype";
		    public const string ATTR_NAME_HASCHURCHACTIVITY = "churchactivity";
		    public const string ATTR_NAME_HASEDUCATION = "education";

            public const string ATTR_NAME_WANTSETHNICITY = "wantsethnicity";
            public const string ATTR_NAME_WANTSSMOKE = "wantssmoke";
            public const string ATTR_NAME_WANTSDRINK = "wantsdrink";
            public const string ATTR_NAME_WANTSRELIGION = "wantsreligion";
            public const string ATTR_NAME_WANTSMARITALSTATUS = "wantsmaritalstatus";
            public const string ATTR_NAME_WANTSBODYTYPE = "wantsbodytype";
            public const string ATTR_NAME_WANTSCHURCHACTIVITY = "wantschurchactivity";
            public const string ATTR_NAME_WANTSEDUCATION = "wantseducation";

            public const string ATTR_NAME_IMPORTANCEETHNICITY = "importanceethnicity";
            public const string ATTR_NAME_IMPORTANCESMOKE = "importancesmoke";
            public const string ATTR_NAME_IMPORTANCEDRINK = "importancedrink";
            public const string ATTR_NAME_IMPORTANCERELIGION = "importancereligion";
            public const string ATTR_NAME_IMPORTANCEMARITALSTATUS = "importancemaritalstatus";
            public const string ATTR_NAME_IMPORTANCEBODYTYPE = "importancebodytype";
            public const string ATTR_NAME_IMPORTANCECHURCHACTIVITY = "importancechurchactivity";
            public const string ATTR_NAME_IMPORTANCEEDUCATION = "importanceeducation";
		    public const string ATTR_NAME_ID = "id";
            private static readonly string[] _attrNames = new string[]
		                                                      {
		                                                          ATTR_NAME_HASETHNICITY, ATTR_NAME_HASSMOKE, ATTR_NAME_HASDRINK, ATTR_NAME_HASRELIGION, ATTR_NAME_HASMARITALSTATUS, ATTR_NAME_HASBODYTYPE,
		                                                          ATTR_NAME_HASCHURCHACTIVITY, ATTR_NAME_HASEDUCATION
		                                                      };
            private Dictionary<string, CacheReferenceHolder> caches = new Dictionary<string, CacheReferenceHolder>(); 

		    private readonly double[] values;
			private double bottom;
			private QueryBuilder queryBuilder;
		    private int[] ids, w_ethnicity, h_ethnicity, i_ethnicity, w_smoke, h_smoke, i_smoke, w_drink, h_drink, i_drink, w_religion, h_religion, i_religion,
                w_maritalstatus, h_maritalstatus, i_maritalstatus, w_bodytype, h_bodytype, i_bodytype, w_churchactivity, h_churchactivity, i_churchactivity, w_education,
                h_education, i_education;

            private IndexReader currentIndexReader;
		    private bool UseFieldCache = false;

		    public MatchPercentageFieldComparator(QueryBuilder queryBuilder, int numHits)
			{
				values = new double[numHits];
				this.queryBuilder = queryBuilder;
                this.UseFieldCache = Utils.GetSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", true);
			}

			public override int Compare(int slot1, int slot2)
			{
				double a = values[slot1];
				double b = values[slot2];
				if (a > b)
					return 1;
				if (a < b)
					return -1;

				return 0;
			}

			public override void SetBottom(int slot)
			{
				bottom = values[slot];
			}

			public override int CompareBottom(int doc)
			{
                var v2 = (UseFieldCache) ? CalculateMatchPercentageWithCaches(doc) : CalculateMatchPercentage(doc);
				if (bottom > v2)
				{
					return 1;
				}

				if (bottom < v2)
				{
					return -1;
				}

				return 0;
			}

			public override void Copy(int slot, int doc)
			{
                values[slot] = (UseFieldCache) ? CalculateMatchPercentageWithCaches(doc) : CalculateMatchPercentage(doc);
			}

			private double CalculateMatchPercentage(int doc)
			{
                
			    int matchPercent = 0;
			    var document = currentIndexReader.Document(doc);
			    if (document == null) return double.NaN;
			    int userId = Int32.Parse(document.GetField(ATTR_NAME_ID).StringValue);

                //TODO: don't provide match percentage if searcher gender == result gender

                if (queryBuilder.matchPercentages.ContainsKey(userId))
                {
                    matchPercent = queryBuilder.matchPercentages[userId];
                }
                else
                {
                    double seventyFivePercentSubTotal = 1;
                    double twentyFivePercentSubTotal = 1;
                    foreach (string attrName in _attrNames)
                    {
                        //calculate match of searcher wants and importance vs result member has
                        int resultMemberHas = (null != document.GetField(attrName)) ? Int32.Parse(document.GetField(attrName).StringValue) : 1;
                        double searcherMemberImportance = (queryBuilder.importances.ContainsKey(attrName))?queryBuilder.importances[attrName] : 1;
                        int searcherMemberWants = (queryBuilder.wants.ContainsKey(attrName)) ? queryBuilder.wants[attrName] : 1;
                        if ((ATTR_NAME_HASETHNICITY.Equals(attrName) && (searcherMemberWants & resultMemberHas) != searcherMemberWants) ||
                        ((resultMemberHas & searcherMemberWants) != resultMemberHas))
                        {
                            seventyFivePercentSubTotal *= 1 - ((searcherMemberImportance * searcherMemberImportance) / 100);
                        }
                        else
                        {
                            // multiply by 1 (not needed in actual code)
                        }

                        //calculate match of result member wants and importance vs searcher has
                        int resultMemberWants = (null != document.GetField("wants" + attrName)) ? Int32.Parse(document.GetField("wants" + attrName).StringValue) : 1;
                        int resultMemberImportance = (null != document.GetField("importance" + attrName)) ? Int32.Parse(document.GetField("importance" + attrName).StringValue) : 1;
                        int searcherMemberHas = (queryBuilder.has.ContainsKey(attrName)) ? queryBuilder.has[attrName] : 1;
                        if ((searcherMemberHas & resultMemberWants) != searcherMemberHas)
                        {
                            twentyFivePercentSubTotal *= 1 - ((resultMemberImportance*resultMemberImportance)/100);
                        }
                        else
                        {
                            // multiply by 1 (not needed in actual code)
                        }
                    }

                    matchPercent = (int) Math.Floor((75*seventyFivePercentSubTotal) + (25*twentyFivePercentSubTotal));
                    if (matchPercent < 50)
                    {                        
//                        Spark.Logging.RollingFileLogger.Instance.LogInfoMessage("CHRISTIANMINGLESEARCHER_SVC",
//                                                                                "MatchPercentageSortField",
//                                                                                string.Format("UserId:{0}, MatchPercentage:{1}\n",userId, matchPercent), null);
                        matchPercent = 0;
                    }
                    queryBuilder.matchPercentages.Add(userId, matchPercent);
                }
			    return matchPercent;
			}

            private double CalculateMatchPercentageWithCaches(int doc)
            {
                int matchPercent = 0;
                if (ids != null && ids.Length > doc && ids[doc] > -1)
                {
                    int userId = ids[doc];

                    //TODO: don't provide match percentage if searcher gender == result gender
                    if (queryBuilder.matchPercentages.ContainsKey(userId))
                    {
                        matchPercent = queryBuilder.matchPercentages[userId];
                    }
                    else
                    {
                        double seventyFivePercentSubTotal = 1;
                        double twentyFivePercentSubTotal = 1;
                        foreach (string attrName in _attrNames)
                        {
                            CacheReferenceHolder cache = caches[attrName];
                            //calculate match of searcher wants and importance vs result member has
                            int resultMemberHas = (cache.has != null && cache.has.Length > doc && cache.has[doc] > -1) ? cache.has[doc] : 1;
                            double searcherMemberImportance = (queryBuilder.importances.ContainsKey(attrName)) ? queryBuilder.importances[attrName] : 1;
                            int searcherMemberWants = (queryBuilder.wants.ContainsKey(attrName)) ? queryBuilder.wants[attrName] : 1;
                            if ((ATTR_NAME_HASETHNICITY.Equals(attrName) &&
                                 (searcherMemberWants & resultMemberHas) != searcherMemberWants) ||
                                ((resultMemberHas & searcherMemberWants) != resultMemberHas))
                            {
                                seventyFivePercentSubTotal *= 1 - ((searcherMemberImportance*searcherMemberImportance)/100);
                            }
                            else
                            {
                                // multiply by 1 (not needed in actual code)
                            }

                            //calculate match of result member wants and importance vs searcher has
                            int resultMemberWants = (cache.wants != null && cache.wants.Length > doc && cache.wants[doc] > -1) ? cache.wants[doc] : 1;
                            int resultMemberImportance = (cache.importance != null && cache.importance.Length > doc && cache.importance[doc] > -1) ? cache.importance[doc] : 1;
                            int searcherMemberHas = (queryBuilder.has.ContainsKey(attrName)) ? queryBuilder.has[attrName] : 1;
                            if ((searcherMemberHas & resultMemberWants) != searcherMemberHas)
                            {
                                twentyFivePercentSubTotal *= 1 - ((resultMemberImportance*resultMemberImportance)/100);
                            }
                            else
                            {
                                // multiply by 1 (not needed in actual code)
                            }
                        }

                        matchPercent = (int) Math.Floor((75*seventyFivePercentSubTotal) + (25*twentyFivePercentSubTotal));
                        if (matchPercent < 50)
                        {
                            //                        Spark.Logging.RollingFileLogger.Instance.LogInfoMessage("CHRISTIANMINGLESEARCHER_SVC",
                            //                                                                                "MatchPercentageSortField",
                            //                                                                                string.Format("UserId:{0}, MatchPercentage:{1}\n",userId, matchPercent), null);
                            matchPercent = 0;
                        }
                        queryBuilder.matchPercentages.Add(userId, matchPercent);
                    }
                }
                return matchPercent;
            }

			public override void SetNextReader(IndexReader reader, int docBase)
			{
                ids = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_ID);
                h_ethnicity = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASETHNICITY);
                w_ethnicity = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSETHNICITY);
                i_ethnicity = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCEETHNICITY);
                h_smoke = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASSMOKE);
                w_smoke = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSSMOKE);
                i_smoke = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCESMOKE);
                h_drink = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASDRINK);
                w_drink = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSDRINK);
                i_drink = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCEDRINK);
                h_religion = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASRELIGION);
                w_religion = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSRELIGION);
                i_religion = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCERELIGION);
                h_maritalstatus = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASMARITALSTATUS);
                w_maritalstatus = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSMARITALSTATUS);
                i_maritalstatus = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCEMARITALSTATUS);
                h_bodytype = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASBODYTYPE);
                w_bodytype = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSBODYTYPE);
                i_bodytype = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCEBODYTYPE);
                h_churchactivity = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASCHURCHACTIVITY);
                w_churchactivity = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSCHURCHACTIVITY);
                i_churchactivity = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCECHURCHACTIVITY);
                h_education = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_HASEDUCATION);
                w_education = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_WANTSEDUCATION);
                i_education = FieldCache_Fields.DEFAULT.GetInts(reader, ATTR_NAME_IMPORTANCEEDUCATION);

                caches.Clear(); 
                if (!caches.ContainsKey(ATTR_NAME_HASETHNICITY)) caches.Add(ATTR_NAME_HASETHNICITY, new CacheReferenceHolder(ref w_ethnicity, ref h_ethnicity, ref i_ethnicity));
                if (!caches.ContainsKey(ATTR_NAME_HASSMOKE)) caches.Add(ATTR_NAME_HASSMOKE, new CacheReferenceHolder(ref w_smoke, ref h_smoke, ref i_smoke));
                if (!caches.ContainsKey(ATTR_NAME_HASDRINK)) caches.Add(ATTR_NAME_HASDRINK, new CacheReferenceHolder(ref w_drink, ref h_drink, ref i_drink));
                if (!caches.ContainsKey(ATTR_NAME_HASRELIGION)) caches.Add(ATTR_NAME_HASRELIGION, new CacheReferenceHolder(ref w_religion, ref h_religion, ref i_religion));
                if (!caches.ContainsKey(ATTR_NAME_HASMARITALSTATUS)) caches.Add(ATTR_NAME_HASMARITALSTATUS, new CacheReferenceHolder(ref w_maritalstatus, ref h_maritalstatus, ref i_maritalstatus));
                if (!caches.ContainsKey(ATTR_NAME_HASBODYTYPE)) caches.Add(ATTR_NAME_HASBODYTYPE, new CacheReferenceHolder(ref w_bodytype, ref h_bodytype, ref i_bodytype));
                if (!caches.ContainsKey(ATTR_NAME_HASCHURCHACTIVITY)) caches.Add(ATTR_NAME_HASCHURCHACTIVITY, new CacheReferenceHolder(ref w_churchactivity, ref h_churchactivity, ref i_churchactivity));
                if (!caches.ContainsKey(ATTR_NAME_HASEDUCATION)) caches.Add(ATTR_NAME_HASEDUCATION, new CacheReferenceHolder(ref w_education, ref h_education, ref i_education));

			    this.currentIndexReader = reader;
			}

			public override IComparable this[int slot]
			{
				get { return values[slot]; }
			}
		}

        class CacheReferenceHolder
        {
            public int[] wants, has, importance;
            public CacheReferenceHolder(ref int[] _wants, ref int[] _has, ref int[] _importance)
            {
                wants = _wants;
                has = _has;
                importance = _importance;
            }
        }
	}
}
    