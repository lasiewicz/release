﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Util;
using Lucene.Net.Store;
using Spark.Logging;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.BusinessLogic.Searcher
{
    public class Searcher
    {
#if DEBUG
        private bool _Use_NUnit = false;
        public bool USE_NUNIT
        {
            get { return _Use_NUnit; }
            set { _Use_NUnit = value; }
        }

        private bool _NUnit_UseNRT = false;
        public bool NUNIT_UseNRT
        {
            get { return _NUnit_UseNRT; }
            set { _NUnit_UseNRT = value; }
        }

        public string NUNIT_IndexPath { get; set; }
        public bool NUNIT_DeleteOldIndex { get; set; }
#endif
        private IndexSearcher _indexSearcher = null;
        private IndexReader _indexReader = null;
        private Directory _indexDirectory = null;
        private IndexWriter _indexWriter = null;
        private System.IO.FileStream _lockFile = null;
        private string _indexPath;
        private int _communityId;
        private IndexSearcher _oldIndexSearcher = null;
        private IndexReader _oldIndexReader = null;
        private Directory _oldIndexDirectory = null;
        private IndexWriter _oldIndexWriter = null;
        private System.IO.FileStream _oldLockFile = null;
        private string _serviceConstant = string.Empty;
        private string _machineName = string.Empty;
        private IProcessorType _iProcessorType;

        public static readonly string CLASS_NAME = "Searcher";

        private IProcessorType IProcessorType
        {
            get { return _iProcessorType; }
        }

        private string MachineName
        {
            get
            {
                if (string.IsNullOrEmpty(_machineName)) return System.Environment.MachineName;
                return _machineName;
            }
            set { _machineName = value; }
        }

        public IndexSearcher IndexSearcher
        {
            get { return _indexSearcher; }
        }

        public string IndexPath
        {
            get { return _indexPath; }
        }

        public IndexWriter IndexWriter
        {
            get { return _indexWriter; }
        }

        public Searcher(int communityId, IProcessorType iProcessorType)
        {
            _communityId = communityId;
            _iProcessorType = iProcessorType;
        }

        public void Init()
        {
            _serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(_communityId).ToUpper());
            _indexPath = GetIndexPath();
            _indexDirectory = CreateIndexDirectory(_indexPath);

            _indexWriter = CreateIndexWriter(_indexDirectory, _indexPath);
            _indexReader = CreateIndexReader(_indexDirectory, _indexWriter);
            _indexSearcher = CreateIndexSearcher(_indexReader);
            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, string.Format("Initialized Searcher. ProcessorType:{0}, IndexPath: {1}",_iProcessorType, _indexPath), null);
        }

        public void Dispose()
        {
            try
            {
                CloseOldSearcher();
                
                if (null!=_indexDirectory && null!=_lockFile && _indexDirectory.FileExists(_lockFile.Name))
                {
                    _lockFile.Close();
                    _indexDirectory.DeleteFile(_lockFile.Name);
                }

                if (_indexSearcher != null)
                    _indexSearcher.Close();

                if (_indexReader != null)
                    _indexReader.Close();

                if (_indexWriter != null)
                    _indexWriter.Close();

                if (_indexDirectory != null)
                    _indexDirectory.Close();
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "Dispose()", ex, null);
            }
            finally
            {
                _indexPath = null;
                _indexDirectory = null;
                _lockFile = null;
                _indexReader = null;
                _indexSearcher = null;
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "Searcher.Dispose()", string.Format("Disposed {0}-{1} on {2}", _communityId,_iProcessorType, MachineName), null);
            }
        }

        private int GetCloseWaitTime()
        {
            string maxResultsStr = Utils.GetSetting("SEARCHER_CLOSE_WAIT_TIME", "3000");
            return Convert.ToInt32(maxResultsStr);
        }

        private IndexSearcher CreateIndexSearcher(IndexReader indexReader)
        {
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            return indexSearcher;
        }

        private IndexReader CreateIndexReader(Directory directory, IndexWriter indexWriter)
        {
            IndexReader indexReader = null;
            if (indexWriter != null)
            {
                indexReader = indexWriter.GetReader();
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Created NRT IndexReader", null);
            }
            else
            {
                indexReader = IndexReader.Open(directory, true);
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Created Regular IndexReader", null);
            }

            return indexReader;
        }

        private Directory CreateIndexDirectory(string indexPath)
        {
            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Opening indexPath=" + indexPath, null);
            Directory dir = null;
            if (!string.IsNullOrEmpty(indexPath))
            {
                System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(indexPath);
                try
                {
                    string lockFilePath = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, _indexPath + "/" + MachineName.ToLower());
                    if (!System.IO.File.Exists(lockFilePath))
                    {
                        _lockFile = System.IO.File.Create(lockFilePath);
                    }
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "GetIndexDirectory", ex, null);
                }

                dir = FSDirectory.Open(dirinfo);
            }
            return dir;
        }

        private IndexWriter CreateIndexWriter(Directory directory, string dirIndexPath)
        {
            IndexWriter indexWriter = null;
            try
            {
                if (directory != null)
                {
                    indexWriter = new IndexWriter(directory, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), false, IndexWriter.MaxFieldLength.UNLIMITED);
                    indexWriter.UseCompoundFile = true;
                    indexWriter.MergeFactor = Int32.Parse(Utils.GetSetting("INDEXER_MERGE_FACTOR", _communityId, "30"));
                    indexWriter.SetRAMBufferSizeMB(Int32.Parse(Utils.GetSetting("INDEXER_MAX_MB_BUFFER_SIZE", _communityId, "48")));

                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Created IndexWriter for " + dirIndexPath, null);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "CreateIndexWriter", ex, null);
            }
            return indexWriter;
        }

        private string GetIndexPath()
        {
            string indexPath = null;
            try
            {
                indexPath = Utils.GetCurrentIndexDirectory(_communityId, IProcessorType, MachineName, CLASS_NAME);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "GetIndexDirectory", ex, null);
            }

            if (string.IsNullOrEmpty(indexPath))
            {
                //if no indexpath is found and current index path exists, return current one
                if (!string.IsNullOrEmpty(_indexPath))
                {
                    indexPath = _indexPath;
                }
                else
                {
                    //if no current index path then create one using setting
                    string searcherCommunityDirKey = "SEARCHER_COMMUNITY_DIR";
                    switch (_iProcessorType)
                    {
                        case IProcessorType.KeywordIndexProcessor:
                            searcherCommunityDirKey = "KEYWORD_SEARCHER_COMMUNITY_DIR";
                            break;
                        case IProcessorType.MemberIndexProcessor:
                        default:
                            searcherCommunityDirKey = "SEARCHER_COMMUNITY_DIR";
                            break;
                    }
                    indexPath = Utils.GetSetting(searcherCommunityDirKey, _communityId, "c:\\Matchnet\\Index\\" + _communityId.ToString() + "\\");
                }
            }

#if DEBUG
            if (USE_NUNIT)
            {
                if (!string.IsNullOrEmpty(NUNIT_IndexPath))
                {
                    indexPath = NUNIT_IndexPath;
                }
            }
#endif
            return indexPath;
        }

        public void Update()
        {
            Directory newDir = null;
            try
            {
                string newIndexPath = GetIndexPath();
                if (!string.IsNullOrEmpty(newIndexPath))
                {
                    if (newIndexPath != _indexPath)
                    {
                        System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(newIndexPath);
                        newDir = FSDirectory.Open(dirinfo);
                    }

                    if (null != newDir)
                    {
                        Update(newDir, newIndexPath);
                    }
                    else
                    {
                        UpdateNRT();
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "Update() exception with indexPath=" + _indexPath, ex, null);
            }
            
        }

        public void Update(Directory newDir, string newIndexPath)
        {
            try
            {
                if (null != newDir)
                {
                    Directory newDirectory = newDir;
                    //if null == newDirectory then we are closing/reopening current index dir
                    //to free up objects for the garbage collector
                    if (newDirectory != null)
                    {
                        IndexWriter newWriter = CreateIndexWriter(newDirectory, newIndexPath);
                        IndexReader newReader = CreateIndexReader(newDirectory, newWriter);
                        IndexSearcher newSearcher = new IndexSearcher(newReader);
                        if (VerifySearchIndex(newSearcher))
                        {
                            //if index is verified set new index path
                            _indexPath = newIndexPath;

                            //retain pointers to old searcher
                            _oldIndexDirectory = _indexDirectory;
                            _oldIndexWriter = _indexWriter;
                            _oldIndexReader = _indexReader;
                            _oldIndexSearcher = _indexSearcher;
                            _oldLockFile = _lockFile;

                            //set new searcher
                            _indexDirectory = newDirectory;
                            _indexWriter = newWriter;
                            _indexReader = newReader;
                            _indexSearcher = newSearcher;
                            //write lock file in new index dir
                            string lockFilePath = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT,
                                                                _indexPath + "/" +
                                                                System.Environment.MachineName.ToLower());
                            if (!System.IO.File.Exists(lockFilePath))
                            {
                                _lockFile = System.IO.File.Create(lockFilePath);
                            }

                            //close old searcher after wait time
                            Thread closeThread = new Thread(CleanupOldSearcher);
                            closeThread.Start();
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME,
                                                                      "UpdateSearcher() Updated Searcher for indexPath=" +
                                                                      _indexPath, null);
                        }
                        else
                        {
                            try
                            {
                                if (null != newSearcher)
                                {
                                    newSearcher.Close();
                                }

                                if (null != newReader)
                                {
                                    newReader.Close();
                                }

                                if (null != newWriter)
                                {
                                    newWriter.Close();
                                }

                                if (null != newDirectory)
                                {
                                    newDirectory.Close();
                                }

                                //set db index path to current working index
                                if (!string.IsNullOrEmpty(_indexPath))
                                {
                                    Utils.UpdateIndexDirectory(_communityId, IProcessorType, MachineName, _indexPath,
                                                               CLASS_NAME);
                                }
                            }
                            finally
                            {
                                newSearcher = null;
                                newReader = null;
                                newWriter = null;
                                newDirectory = null;
                            }
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "UpdateSearcher() Invalid index at indexPath=" + newIndexPath, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "UpdateSearcher() exception with indexPath=" + _indexPath, ex, null);
            }
        }

        public void UpdateNRT()
        {
            try
            {
                if (!_indexReader.IsCurrent() || _indexWriter == null)
                {
                    if (_indexWriter == null)
                    {
                        _indexWriter = CreateIndexWriter(_indexDirectory, _indexPath);
                    }

                    if (_indexWriter != null)
                    {
                        //getting updated NRT Reader
                        IndexReader newReader = CreateIndexReader(_indexDirectory, _indexWriter);
                        IndexSearcher newSearcher = new IndexSearcher(newReader);

                        //retain pointers to old searcher
                        _oldIndexReader = _indexReader;
                        _oldIndexSearcher = _indexSearcher;

                        //set new searcher
                        _indexReader = newReader;
                        _indexSearcher = newSearcher;

                        //close old searcher after wait time
                        Thread closeThread = new Thread(CleanupOldSearcher);
                        closeThread.Start();

                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "UpdateNRT() - Updated Searcher and Reader for NRT", null);
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "UpdateNRT()", ex, null);
            }
        }

        public bool VerifySearchIndex(IndexSearcher newIndexSearcher)
        {
            bool isValid = false;
            TopDocs topDocs = null;
            try
            {
                switch (this.IProcessorType)
                {
                    case IProcessorType.KeywordIndexProcessor:
                        QueryBuilder kqb = Utils.GetTestKeywordQuery(_communityId, 0);
                        topDocs = Search(kqb, newIndexSearcher);
                        break;
                    case IProcessorType.MemberIndexProcessor:
                    default:
                        QueryBuilder qb = Utils.GetTestQuery(_communityId, 0);
                        topDocs = Search(qb, newIndexSearcher);
                        break;
                }
                isValid = (null != topDocs && topDocs.ScoreDocs.Length > 0);
#if DEBUG
                if (USE_NUNIT)
                {
                    isValid = true;
                }
#endif
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "VerifySearchIndex", ex, null);
                isValid = false;
            }
            return isValid;
        }


        public TopDocs Search(QueryBuilder qb)
        {
            return Search(qb, _indexSearcher);
        }

        public TopDocs Search(QueryBuilder qb, IndexSearcher searcher)
        {
            TopDocs topDocs = null;
            int maxResults = (qb.MaxNumberOfMatches > 0) ? qb.MaxNumberOfMatches : Utils.GetMaxResults(_communityId);

            BooleanQuery booleanQuery = new BooleanQuery();
            foreach (Query q in qb.queries)
            {
                booleanQuery.Add(q, Occur.MUST);
            }

            if (qb.filters.Count > 0)
            {
                BooleanFilter boolFilter = new BooleanFilter();
                foreach (Filter filter in qb.filters)
                {
                    boolFilter.Add(new FilterClause(filter, Occur.MUST));
                }
                Filter searchFilter = new CachingWrapperFilter(boolFilter);

                if (qb.sortFields.Count > 0)
                {
                    topDocs = searcher.Search(booleanQuery, searchFilter, maxResults, new Sort(qb.sortFields.ToArray<SortField>()));
                }
                else
                {
                    topDocs = searcher.Search(booleanQuery, searchFilter, maxResults);
                }
            }
            else
            {
                topDocs = searcher.Search(booleanQuery, maxResults);
            }
            return topDocs;
        }


        private void CleanupOldSearcher(object o)
        {
            Thread.Sleep(GetCloseWaitTime());
            CloseOldSearcher();
        }

        private void CloseOldSearcher()
        {
            try
            {
                if (null != _oldIndexSearcher)
                {
                    //close old searcher
                    _oldIndexSearcher.Close();
                }

                if (null != _oldIndexReader)
                {
                    _oldIndexReader.Close();
                }

                if (null != _oldIndexWriter)
                {
                    _oldIndexWriter.Close();
                }

                if (null != _oldIndexDirectory)
                {
                    //remove old lock file 
                    if (null != _oldLockFile && _oldIndexDirectory.FileExists(_oldLockFile.Name))
                    {
                        _oldLockFile.Close();
                        _oldIndexDirectory.DeleteFile(_oldLockFile.Name);
                    }
                    _oldIndexDirectory.Close();
                }
            }
            catch (Exception e)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "CloseOldSearcher()", e, null);
            }
            finally
            {
                _oldIndexSearcher = null;
                _oldIndexReader = null;
                _oldIndexWriter = null;
                _oldIndexDirectory = null;
                _oldLockFile = null;
            }
        }

    }
}
