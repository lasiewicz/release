﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Spatial;
using Lucene.Net.Store;
using Spark.Logging;
using Spark.MingleSearchEngine.BusinessLogic.Documents;
using Spark.MingleSearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Shapes;

namespace Spark.MingleSearchEngine.BusinessLogic.Indexer
{
    public class KeywordIndexProcessor : IProcessor
    {
        public static string CLASS_NAME = "KeywordIndexProcessor";
        private int _maxSize = 100000;
        private List<MingleSearchKeyword> _keywords = null;
        private long count = 0;
        private long elapsedProcessingTime = 0;
        private long elapsedIndexWriterTime = 0;
        private IndexWriter _writer;
        private DocumentMetadata _documentMetadata;
        private SearchDocument keyworddoc;
        private IProcessorType _processorType = IProcessorType.KeywordIndexProcessor;
        private SpatialContext _spatialContext = null;
        private SpatialStrategy _spatialStrategy = null;
        private Dictionary<string, AbstractField[]> _spatialShapeFields = null;
        Stopwatch processingTime = new Stopwatch();
        Stopwatch indexWriterTime = new Stopwatch();

        public bool IsProcessing { get; set; }
        public bool IsFull { get; set; }

        public DocumentMetadata DocumentMetadata
        {
            get { return _documentMetadata; }
            set { _documentMetadata = value; }
        }

        public IndexWriter Writer
        {
            get { return _writer; }
            set { _writer = value; }
        }

        public int MaxSize
        {
            get { return _maxSize; }
            set { _maxSize = value; }
        }

        public int CommunityId { get; set; }

        public Directory IndexDir { get; set; }

        public SpatialContext SpatialContext
        {
            get { return _spatialContext; }
            set { _spatialContext = value; }
        }

        public SpatialStrategy SpatialStrategy
        {
            get { return _spatialStrategy; }
            set { _spatialStrategy = value; }
        }

        public Dictionary<string, AbstractField[]> SpatialShapeFields
        {
            get { return _spatialShapeFields; }
            set { _spatialShapeFields = value; }
        }

        public decimal AverageProcessingTime
        {
            get { return (count == 0) ? 0 : (decimal)elapsedProcessingTime / (decimal)count; }
        }

        public decimal AverageIndexWriterTime
        {
            get { return (count == 0) ? 0 : (decimal)elapsedIndexWriterTime / (decimal)count; }
        }

        public bool IsInitialized
        {
            get
            {
                return (null != Writer)
                       && (null != DocumentMetadata)
                       && (null != SpatialContext)
                       && (null != SpatialStrategy)
                       && (null != SpatialShapeFields);
            }

        }

        public void Initialize(Hashtable context)
        {
            IndexWriter indexWriter = context["indexWriter"] as IndexWriter;
            DocumentMetadata documentMetadata = context["documentMetadata"] as DocumentMetadata;
            int maxSize = (int)context["maxSize"];
            SpatialContext spatialContext = context["spatialContext"] as SpatialContext;
            SpatialStrategy spatialStrategy = context["spatialStrategy"] as SpatialStrategy;
            Dictionary<string, AbstractField[]> spatialShapeFields = context["spatialShapeFields"] as Dictionary<string, AbstractField[]>;

            this.Writer = indexWriter;
            this.DocumentMetadata = documentMetadata;
            this.SpatialContext = spatialContext;
            this.SpatialStrategy = spatialStrategy;
            this.SpatialShapeFields = spatialShapeFields;
            this.MaxSize = maxSize;
        }

        public KeywordIndexProcessor(DocumentMetadata documentMetadata, IndexWriter writer, Directory indexDir, int maxSize)
        {
            this.DocumentMetadata = documentMetadata;
            this.Writer = writer;
            this.IndexDir = indexDir;
            this.MaxSize = maxSize;
            keyworddoc = new SearchDocument(DocumentMetadata);
        }

        public void AddDataRow(IDataReader iDataReader)
        {
            if (IsFull)
            {
                throw new ConstraintException("Processor is Full!!");
            }
            if (null == _keywords)
            {
                _keywords = new List<MingleSearchKeyword>(MaxSize);
            }
            processingTime.Start();
            MingleSearchKeyword keyword = new MingleSearchKeyword().Populate(iDataReader);
            keyword.UpdateType = UpdateTypeEnum.all;
            _keywords.Add(keyword);
            processingTime.Stop();
            elapsedProcessingTime += processingTime.ElapsedMilliseconds;
            processingTime.Reset();
            if (_keywords.Count >= MaxSize)
            {
                count = _keywords.Count;
                IsFull = true;
                IsFull = true;
                //start processing
                this.IsProcessing = true;
                Thread t = new Thread(new ThreadStart(this.Run));
                t.IsBackground = true;
                t.Start();
            }
        }

        public void Run()
        {
            IsProcessing = true;
            IndexProcessorFactory.Instance.LogActiveProcessorCount(CommunityId, _processorType);
            count = _keywords.Count;
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                      string.Format("Total Processing Time: {0}s, Avg Processing Time: {1}ms, Total Keywords Processed: {2} for communityId={3} and processor type={4}\n",
                                                          (elapsedProcessingTime / 1000),
                                                          AverageProcessingTime,
                                                          _keywords.Count,
                                                          CommunityId, _processorType), null);
            try
            {
                foreach (MingleSearchKeyword keyword in _keywords)
                {
                    if (IsProcessing)
                    {

                        indexWriterTime.Start();
                        IndexKeyword(keyword, DocumentMetadata, Writer);
                        indexWriterTime.Stop();
                        elapsedIndexWriterTime += indexWriterTime.ElapsedMilliseconds;
                        indexWriterTime.Reset();
                    }
                    if (!IsProcessing)
                    {
                        break;
                    }
                }

                if (null != IndexDir)
                {
                    try
                    {
                        Writer.Optimize();
                    }
                    catch (Exception e)
                    {
                        RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, e);
                    }
                    finally
                    {
                        Writer.Close();
                    }
                }
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                          string.Format(
                                                              "Total Index Write Time: {0}min, Avg Index Write Time: {1}ms for communityId={2} and processor type={3}\n",                                                              
                                                              ((elapsedIndexWriterTime / 1000) / 60), 
                                                              AverageIndexWriterTime, CommunityId, _processorType), null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, ex);
            }
            CleanUp();
        }

        private void CleanUp()
        {
            _keywords.Clear();
            Writer = null;
            DocumentMetadata = null;
            count = 0;
            elapsedProcessingTime = 0;
            elapsedIndexWriterTime = 0;
            IsProcessing = false;
            IsFull = false;
            SpatialStrategy = null;
            SpatialContext = null;
            SpatialShapeFields = null;
        }

        public void IndexKeyword(MingleSearchKeyword searchKeyword, DocumentMetadata docMetadata, IndexWriter writer)
        {
            try
            {
                keyworddoc.CommunityName = ServiceConstants.GetCommunityName(this.CommunityId);
                keyworddoc.Context = SpatialContext;
                keyworddoc.Strategy = SpatialStrategy;
                keyworddoc.SpatialShapeFields = SpatialShapeFields;

                if (searchKeyword.UserID <= 0 || searchKeyword.CommunityID <= 0)
                    return;
                if (searchKeyword.UpdateType == UpdateTypeEnum.all)
                {
                    keyworddoc.Populate(searchKeyword, docMetadata);
                }
                if (searchKeyword.UpdateType != UpdateTypeEnum.delete)
                    writer.UpdateDocument(new Term("id", searchKeyword.UserID.ToString()), keyworddoc.LuceneDocument);
                else
                    writer.DeleteDocuments(new Term("id", searchKeyword.UserID.ToString()));
                //clear field values
                keyworddoc.ClearAllFieldValues();

                //RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Member {0} written to index.", member.MemberID),null);             
            }
            catch (Lucene.Net.Store.AlreadyClosedException ace)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Already closed writer: {0}, {1}, {2},{3}", this.Writer.ToString(), this.IndexDir, this.CommunityId, this.count), ace, null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexKeyword", ex, null);
            }

        }

        public void Dispose()
        {
            this.IsProcessing = false;
            this.CleanUp();
        }
    }
}
