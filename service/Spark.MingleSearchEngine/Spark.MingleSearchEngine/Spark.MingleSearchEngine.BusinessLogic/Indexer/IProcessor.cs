﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Spatial;
using Lucene.Net.Store;
using Spark.MingleSearchEngine.BusinessLogic.Documents;
using Spatial4n.Core.Context;
using Spatial4n.Core.Shapes;

namespace Spark.MingleSearchEngine.BusinessLogic.Indexer
{
    public interface IProcessor : IDisposable
    {
        DocumentMetadata DocumentMetadata { get; set; }
        IndexWriter Writer { get; set; }
        Directory IndexDir { get; set; }
        SpatialContext SpatialContext { get; set; }
        SpatialStrategy SpatialStrategy { get; set; }
        Dictionary<string, AbstractField[]> SpatialShapeFields { get; set; }
        int MaxSize { get; set; }
        int CommunityId { get; set; }
        bool IsInitialized { get;  }
        bool IsProcessing { get; set; }
        bool IsFull { get; set; }

        void Initialize(Hashtable context);
        void AddDataRow(IDataReader iDataReader);
        void Run();
    }
}
