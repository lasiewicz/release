using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Matchnet;
using Matchnet.Exceptions;
using Quartz;
using Spark.Logging;
using Spark.MingleSearchEngine.BusinessLogic.Documents;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.BusinessLogic.Indexer
{
    public class IndexJob:IJob{
        #region IJob Members

        public void Execute(IJobExecutionContext context)
        {
            SqlDataReader memberDataReader=null;
            try
            {
                CommunityIndexConfig cic = (CommunityIndexConfig)context.JobDetail.JobDataMap["config"];
                IProcessorFactory iProcessorFactory = (IProcessorFactory)context.JobDetail.JobDataMap["iProcessorFactory"];

                //calculate path
                cic.IncrementRollingIndexDirectoryID();
                string path = Utils.CalculateIndexPath(cic);
                Utils.CleanDir(path);
                context.JobDetail.JobDataMap["path"] = path;

                FSDirectory directory = FSDirectory.Open(path);
                IndexWriter indexWriter = new IndexWriter(directory, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);
                indexWriter.UseCompoundFile = cic.UseCompoundFile;
                indexWriter.MergeFactor = cic.MergeFactor;
                indexWriter.SetRAMBufferSizeMB(cic.MaxMBBufferSize);
                

                DocumentMetadata documentMetadata = Serialization.FromXmlFile<DocumentMetadata>(cic.MetadataFile);

                DateTime indexStartTime = DateTime.Now;
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                int totalRowCount = 0;
                memberDataReader = Utils.GetCommunitySearchMembers(cic);

                stopWatch.Stop();
                if (null != memberDataReader && memberDataReader.HasRows)
                {
                    memberDataReader.Read();
                    totalRowCount = (memberDataReader.IsDBNull(memberDataReader.GetOrdinal("TotalActiveMembers"))) ? 50000 : memberDataReader.GetInt32(memberDataReader.GetOrdinal("TotalActiveMembers"));
                    memberDataReader.NextResult();

                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME,
                                                              string.Format("Retrieved {0} members in {1} minutes for {2} index communityId={3}",
                                                                            totalRowCount, (((double)stopWatch.ElapsedMilliseconds / (double)1000) / (double)60), cic.IProcessorType, cic.CommunityID), null);
                    stopWatch.Reset();
                    stopWatch.Start();
                    int maxProcessorSize = (totalRowCount / cic.IndexerThreads) + 1;
                    while (memberDataReader.Read())
                    {
                        IProcessor mip = iProcessorFactory.GetIProcessor(cic.CommunityID, documentMetadata, indexWriter, maxProcessorSize, cic.IProcessorType);
//                        IProcessor mip = iProcessorFactory.GetIProcessor(cic.CommunityID, documentMetadata, (Directory)null, maxProcessorSize, cic.IProcessorType);
                        mip.AddDataRow(memberDataReader);
                    }
                    //start last processor if it isn't started
                    iProcessorFactory.StartProcessors(cic.CommunityID, cic.IProcessorType);

                    //stop indexing if processors are all done or if time limit is reached (indexing interval +80 minutes)
                    int processingTimeLimit = cic.RollingIndexInterval + 80;
                    DateTime nextindexdate = DateTime.Now.AddMinutes(Conversion.CDouble(processingTimeLimit.ToString()));
                    while (!iProcessorFactory.AreAllDone(cic.CommunityID, cic.IProcessorType))// && DateTime.Now < nextindexdate)
                    {
                        Thread.Sleep(500);
                    }
                    stopWatch.Stop();
                    if (iProcessorFactory.AreAllDone(cic.CommunityID, cic.IProcessorType))
                    {
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME,
                                                                  string.Format("Processed {0} members in {1} minutes for {2} index communityId={3}",
                                                                                totalRowCount, (((double)stopWatch.ElapsedMilliseconds / (double)1000) / (double)60), cic.IProcessorType, cic.CommunityID), null);
                        stopWatch.Reset();
                        //optimize index
                        stopWatch.Start();
//                        List<IndexWriter> indexWriters = ((IndexProcessorFactory)iProcessorFactory).GetIndexWriters(cic.CommunityID, cic.IProcessorType);
                        try
                        {
//                            foreach (IndexWriter writer in indexWriters)
//                            {
//                                indexWriter.AddIndexes(writer.GetReader());
//                            }
//                            indexWriter.Commit();
                            indexWriter.Optimize();
                        }
                        catch (Exception iwe)
                        {
                            RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, iwe);
                        }
                        finally
                        {
                            indexWriter.Dispose();                            
//                            foreach (IndexWriter writer in indexWriters)
//                            {
//                                writer.Dispose();                                
//                            }
//                            iProcessorFactory.ResetCommunity(cic.CommunityID, cic.IProcessorType);
                        }

                        stopWatch.Stop();
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME,
                                                                  string.Format("{0} index optimized in {1} minutes for communityId={2}",
                                                                                cic.IProcessorType, (((double)stopWatch.ElapsedMilliseconds / (double)1000) / (double)60), cic.CommunityID),
                                                                  null);
                        //update the last index date
                        //cic.NextIndexDate = cic.LastIndexDate;
                        cic.LastIndexDate = indexStartTime;
                        Utils.updateLastIndexDate(cic);
#if DEBUG
//                        if (!IndexerBL.Instance.USE_NUNIT)
//                        {
#endif
                            //copy index to search servers
                            Thread copyThread = new Thread(copyIndex);
                            copyThread.IsBackground = true;
                            copyThread.Start(context.JobDetail.JobDataMap);
#if DEBUG
//                        }
#endif
                    }
                    else
                    {

                        RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME,
                                                                     string.Format("{0} indexing failed to complete in {1} minutes for communityId={2}", cic.IProcessorType, processingTimeLimit, cic.CommunityID),
                                                                     null);
                        //clear out the unfinished processors for this community
                        iProcessorFactory.ResetCommunity(cic.CommunityID, cic.IProcessorType);
                    }
                }
                else
                {

                    RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME,
                                                            string.Format("No members found for {0} index communityId={1}", cic.IProcessorType, cic.CommunityID),
                                                            new BLException(""),
                                                            null);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "IndexCommunityWithThreads", ex, null);
                throw (new BLException(ex));
            }
            finally
            {
                if (null != memberDataReader && !memberDataReader.IsClosed)
                {
                    memberDataReader.Close();
                }
            }          
        }
        #endregion

        private void copyIndex(object c)
        {
            try
            {
                JobDataMap context = (JobDataMap)c;
                CommunityIndexConfig config = (CommunityIndexConfig)context["config"];
                string path = (string)context["path"];
                if ((null != config) && Utils.GetSetting("INDEXER_COPY_INDEX", config.CommunityID, true))
                {
                    List<string> servers = Utils.getSearcherServers(config.CommunityID);
                    foreach (string server in servers)
                    {
                        Hashtable h = new Hashtable();
                        string indexDirectoryInUseForServer = string.Empty;
                        //loop through the index directories and find an open one
                        for (int i = 0; i < Utils.MAX_ROLLING_DIRS; i++)
                        {
                            indexDirectoryInUseForServer = Utils.IndexDirectoryInUseForServer(config, server);
                            if (string.IsNullOrEmpty(indexDirectoryInUseForServer))
                            {
                                break;
                            }
                            else
                            {
                                config.IncrementRollingIndexDirectoryID();
                            }
                        }
                        //copy index to open dir
                        if (string.IsNullOrEmpty(indexDirectoryInUseForServer))
                        {
                            //copy each server index using different thread.
                            if (context.ContainsKey("config"))
                            {
                                h["config"]=config;
                            }
                            h["server"] = server;
                            h["sourcepath"] = path;

                            Thread copy = new Thread(Utils.CopyIndexToServer);
                            copy.Start(h);
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, string.Format("Lock file exists for community:{0}. {1} Index not copied to {2}", config.CommunityID, config.IProcessorType, indexDirectoryInUseForServer), null);
                        }
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, string.Format("Copying {0} index turned off for community:{1}", config.IProcessorType, config.CommunityID), null);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, IndexerBL.CLASS_NAME, "copyIndex", ex, null);
            }
        }
    }
}