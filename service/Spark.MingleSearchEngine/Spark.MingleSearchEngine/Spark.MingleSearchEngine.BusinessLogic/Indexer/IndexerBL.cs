﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Quartz;
using Quartz.Impl;
using Lucene.Net;
using Spark.Logging;
using Spark.MingleSearchEngine.BusinessLogic.Documents;
using Spark.MingleSearchEngine.ValueObjects;


namespace Spark.MingleSearchEngine.BusinessLogic.Indexer
{
    public class IndexerBL
    {
        #region debug code
        #if DEBUG
        private bool _useNUnit = false;
        Dictionary<int, StringBuilder> _communityStringBuilders = new Dictionary<int, StringBuilder>();
        Dictionary<IProcessorType,IProcessorType> _iProcessorTypesToIndex = new Dictionary<IProcessorType, IProcessorType>();

        public bool USE_NUNIT
        {
            get { return _useNUnit; }
            set { _useNUnit = value; }
        }

        private StringBuilder GetCommunityStringBuilder(int communityId)
        {
            lock (_communityStringBuilders)
            {
                if (!_communityStringBuilders.ContainsKey(communityId))
                {
                    _communityStringBuilders[communityId] = new StringBuilder();
                }
            }
            return _communityStringBuilders[communityId];
        }

        public void AddProcessorTypeToIndex(IProcessorType iProcessorType)
        {
            if (!_iProcessorTypesToIndex.ContainsKey(iProcessorType))
            {
                _iProcessorTypesToIndex.Add(iProcessorType,iProcessorType);
            }
        }

        public void ClearProcessorTypesToIndex()
        {
            _iProcessorTypesToIndex.Clear();
        }
        #endif
        #endregion

        public static readonly string CLASS_NAME = "IndexerBL";
        string APP_NAME = "Spark.MingleSearchEngine";
        public static readonly IndexerBL Instance = new IndexerBL();
        private ISchedulerFactory _schedulerFactory = null;
        private IProcessorFactory _iProcessorFactory = null;
        private IScheduler _scheduler;

        public ISchedulerFactory SchedulerFactory
        {
            get
            {
                if (null == _schedulerFactory)
                {
                    _schedulerFactory = new StdSchedulerFactory();
                }
                return _schedulerFactory;
            }
            set { _schedulerFactory = value; }
        }

        public IProcessorFactory IProcessorFactory
        {
            get
            {
                if (null == _iProcessorFactory)
                {
                    _iProcessorFactory = IndexProcessorFactory.Instance;
                }
             
                return _iProcessorFactory;
            }
            set { _iProcessorFactory = value; }
        }

        private IndexerBL(){}



        public void Start()
        {
            List<CommunityIndexConfig> communityIndexConfigs = GetCommunityIndexConfigs();
            _scheduler = SchedulerFactory.GetScheduler();
            _scheduler.Start();

            //TODO: Figure out how to use xml for these configs
            foreach (CommunityIndexConfig communityIndexConfig in communityIndexConfigs)
            {                
#if DEBUG
                if (_iProcessorTypesToIndex.Count == 0 || _iProcessorTypesToIndex.ContainsKey(communityIndexConfig.IProcessorType))
                {
#endif
                    JobBuilder jobBuilder = JobBuilder.Create<IndexJob>();
                    jobBuilder.WithIdentity(new JobKey(ServiceConstants.GetCommunityName(communityIndexConfig.CommunityID) +communityIndexConfig.IProcessorType, "indexers"));
                    JobDataMap jobDataMap = new JobDataMap();
                    jobDataMap.Add("iProcessorFactory", IProcessorFactory);
                    jobDataMap.Add("config", communityIndexConfig);
//                    jobDataMap.Add("path", path);
                    jobBuilder.UsingJobData(jobDataMap);

                    IJobDetail jobDetail = jobBuilder.Build();

                    TriggerBuilder triggerBuilder = TriggerBuilder.Create();
                    triggerBuilder.ForJob(jobDetail);
//                    triggerBuilder.WithCronSchedule(communityIndexConfig.CronExpression);
                    triggerBuilder.StartNow();
                    ITrigger trigger = triggerBuilder.Build();
                    _scheduler.ScheduleJob(jobDetail, trigger);
#if DEBUG
                }
#endif
            }
        }

        public void Stop()
        {
            IProcessorFactory.Dispose();
            _scheduler.Shutdown();
        }

        public CommunityIndexConfig GetCommunityConfig(int communityId, IProcessorType iProcessorType)
        {
            CommunityIndexConfig config = null;
            List<CommunityIndexConfig> _communityConfigs = GetCommunityIndexConfigs();
            if (null != _communityConfigs)
            {
                config = _communityConfigs.Find(delegate(CommunityIndexConfig c) { return c.CommunityID == communityId && c.IProcessorType == iProcessorType; });
            }
            return config;
        }

        private List<CommunityIndexConfig> GetCommunityIndexConfigs()
        {
            CommunityIndexConfigCollection _cachedCommunityConfigs = Cache.Instance.Get(CommunityIndexConfigCollection.GetCacheKeyString()) as CommunityIndexConfigCollection;
            if (null == _cachedCommunityConfigs)
            {
                initConfigs();
                _cachedCommunityConfigs = Cache.Instance.Get(CommunityIndexConfigCollection.GetCacheKeyString()) as CommunityIndexConfigCollection;
            }
            List<CommunityIndexConfig> _communityConfigs = _cachedCommunityConfigs.CommunityIndexConfigs;
            return _communityConfigs;
        }

        private void initConfigs()
        {
            string communitylist = string.Empty;
            try
            {
                communitylist = Utils.GetSetting("MINGLE_SEARCHER_COMMUNITY_LIST", "");
                List<CommunityIndexConfig> _communityConfigs = new List<CommunityIndexConfig>();
                if (String.IsNullOrEmpty(communitylist))
                {
                    RollingFileLogger.Instance.LogWarningMessage(APP_NAME, CLASS_NAME, "Found no communities for searcher", null);
                    return;
                }

                string[] communityList = communitylist.Split(new char[] { ';' });
                if (communityList == null || communityList.Length == 0)
                {
                    RollingFileLogger.Instance.LogWarningMessage(APP_NAME, CLASS_NAME, "Cannot get communities for searcher:" + communitylist, null);
                    return;
                }

                for (int i = 0; i < communityList.Length; i++)
                {
                    int communityid = Conversion.CInt(communityList[i]);
                    CommunityIndexConfig config = createCommunityConfigForMemberIndex(communityid);
                    _communityConfigs.Add(config);
//                    CommunityIndexConfig keywordConfig = createCommunityConfigForKeywordIndex(communityid);
//                    _communityConfigs.Add(keywordConfig);
                    // todo: mike this is not ready so commenting it out
                    //CommunityIndexConfig adminConfig = createCommunityConfigForAdminIndex(communityid);
                    //_communityConfigs.Add(adminConfig);
                }

                CommunityIndexConfigCollection cachedCommunityIndexConfigs = new CommunityIndexConfigCollection(_communityConfigs);
                Cache.Instance.Add(cachedCommunityIndexConfigs);
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Refeshed configs in cache.", null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "initConfigs", ex, communitylist);
            }
        }

        private CommunityIndexConfig createCommunityConfigForMemberIndex(int communityid)
        {
            CommunityIndexConfig config = new CommunityIndexConfig();
            config.IProcessorType = IProcessorType.MemberIndexProcessor;
            string indexPath = Utils.GetSetting("SEARCHER_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\Index\\" + communityid.ToString() + "\\");
            string indexStagePath = Utils.GetSetting("INDEXER_STAGE_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\IndexStage\\" + communityid.ToString() + "\\");

            config.CommunityID = communityid;
            config.IndexPath = indexPath;
            config.IndexStagePath = indexStagePath;
            config.MetadataFile = AppDomain.CurrentDomain.BaseDirectory + "/" + Utils.GetSetting("SEARCHER_COMMUNITY_METADATA_FILE", communityid, "SearchDocument.xml");
            config.IndexerThreads = Conversion.CInt(Utils.GetSetting("INDEXER_COMMUNITY_THREADS", communityid, "1"));
            config.MergeFactor = Conversion.CInt(Utils.GetSetting("INDEXER_MERGE_FACTOR", communityid, "30"));
            config.MaxMergeDocs = Conversion.CInt(Utils.GetSetting("INDEXER_MAX_MERGE_DOCS", communityid, "100"));
            config.LastActiveDays = Conversion.CInt(Utils.GetSetting("INDEXER_ACTIVE_DAYS", communityid, "180"));
            config.EnableRollingUpdates = Conversion.CBool(Utils.GetSetting("ENABLE_INDEX_DIR_SWAP_UPDATES", communityid, "false"));
            config.RollingIndexInterval = Conversion.CInt(Utils.GetSetting("INDEXER_SWAPPING_INTERVAL", communityid, "1"));
            config.UseCompoundFile = Conversion.CBool(Utils.GetSetting("INDEXER_USE_COMPOUND_FILE", communityid, "false"));
            config.MaxMBBufferSize = Conversion.CInt(Utils.GetSetting("INDEXER_MAX_MB_BUFFER_SIZE", communityid, "48"));
//            config.CronExpression = Utils.GetSetting("INDEXER_CRON_EXPRESSION", communityid, "0 0 "+((communityid*20)%23)+" ? * MON,WED,FRI");
//            config.CronExpression = Utils.GetSetting("INDEXER_CRON_EXPRESSION", communityid, "0 0/10 * * * ?");
            int id = 0;
            config.LastIndexDate = Utils.getLastIndexDate(communityid, out id, config.IProcessorType);
            config.RollingIndexDirectoryID = (id < Utils.MAX_ROLLING_DIRS) ? id + 1 : 1;
            for (int i = 0; i < Utils.MAX_ROLLING_DIRS; i++)
            {
                if (!string.IsNullOrEmpty(Utils.IndexDirectoryInUse(config)))
                {
                    config.RollingIndexDirectoryID = (id < Utils.MAX_ROLLING_DIRS) ? id + 1 : 1;
                }
                else
                {
                    break;
                }
            }
            //config.NextIndexDate = DateTime.Now;

            config.Metadata = Serialization.FromXmlFile<DocumentMetadata>(config.MetadataFile);

            return config;
        }

        private CommunityIndexConfig createCommunityConfigForKeywordIndex(int communityid)
        {
            CommunityIndexConfig config = new CommunityIndexConfig();
            config.IProcessorType = IProcessorType.KeywordIndexProcessor;
            string indexPath = Utils.GetSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\KeywordIndex\\" + communityid.ToString() + "\\");
            string indexStagePath = Utils.GetSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\KeywordIndexStage\\" + communityid.ToString() + "\\");

            config.CommunityID = communityid;
            config.IndexPath = indexPath;
            config.IndexStagePath = indexStagePath;
            config.MetadataFile = AppDomain.CurrentDomain.BaseDirectory + "/" + Utils.GetSetting("KEYWORD_SEARCHER_COMMUNITY_METADATA_FILE", communityid, "KeywordSearchDocument.xml");
            config.IndexerThreads = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_COMMUNITY_THREADS", communityid, "1"));
            config.MergeFactor = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_MERGE_FACTOR", communityid, "30"));
            config.MaxMergeDocs = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_MAX_MERGE_DOCS", communityid, "100"));
            config.LastActiveDays = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_ACTIVE_DAYS", communityid, "180"));
            config.EnableRollingUpdates = Conversion.CBool(Utils.GetSetting("ENABLE_KEYWORD_INDEX_DIR_SWAP_UPDATES", communityid, "false"));
            config.RollingIndexInterval = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_SWAPPING_INTERVAL", communityid, "1"));
            config.UseCompoundFile = Conversion.CBool(Utils.GetSetting("KEYWORD_INDEXER_USE_COMPOUND_FILE", communityid, "false"));
            config.MaxMBBufferSize = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_MAX_MB_BUFFER_SIZE", communityid, "48"));
//            config.CronExpression = Utils.GetSetting("KEYWORD_INDEXER_CRON_EXPRESSION", communityid, "0 0 " + ((communityid * 20) / 23) + " ? * MON,WED,FRI");
//            config.CronExpression = Utils.GetSetting("KEYWORD_INDEXER_CRON_EXPRESSION", communityid, "0 0/10 * * * ?");
            int id = 0;
            config.LastIndexDate = Utils.getLastIndexDate(communityid, out id, config.IProcessorType);
            config.RollingIndexDirectoryID = (id < Utils.MAX_ROLLING_DIRS) ? id + 1 : 1;
            for (int i = 0; i < Utils.MAX_ROLLING_DIRS; i++)
            {
                if (!string.IsNullOrEmpty(Utils.IndexDirectoryInUse(config)))
                {
                    config.RollingIndexDirectoryID = (id < Utils.MAX_ROLLING_DIRS) ? id + 1 : 1;
                }
                else
                {
                    break;
                }
            }
            //config.NextIndexDate = DateTime.Now;

            config.Metadata = Serialization.FromXmlFile<DocumentMetadata>(config.MetadataFile);

            return config;
        }
    }

    public class CommunityIndexConfig
    {
//        public Dictionary<int, CartesianTierPlotter> Tiers;
        public int CommunityID { get; set; }
        public string IndexPath { get; set; }
        public string IndexStagePath { get; set; }
        public string MetadataFile { get; set; }
        public int IndexerThreads { get; set; }
        public DateTime LastIndexDate { get; set; }
        public int MergeFactor { get; set; }
        public int MaxMergeDocs { get; set; }
        public int LastActiveDays { get; set; }
        public DocumentMetadata Metadata { get; set; }
        public int RollingIndexDirectoryID { get; set; }
        public bool EnableRollingUpdates { get; set; }
        public int RowBlockDateRangeInDays { get; set; }
        public int ProcTimeLimitPerRowBlockInMinutes { get; set; }
        public bool UseRowBlocks { get; set; }
        public int RollingIndexInterval { get; set; }
        public bool UseCompoundFile { get; set; }
        public int MaxMBBufferSize { get; set; }
        public IProcessorType IProcessorType { get; set; }
        public string CronExpression { get; set; }

        public Stopwatch Stopwatch { get; set; }

        public Counter MemberCount { get; set; }

        public void IncrementRollingIndexDirectoryID()
        {
            RollingIndexDirectoryID = RollingIndexDirectoryID + 1;
            if (RollingIndexDirectoryID > Utils.MAX_ROLLING_DIRS)
            {
                RollingIndexDirectoryID = 1;
            }
        }

        public override string ToString()
        {
            try
            {
                return
                    string.Format(
                        "Community Index Config, communityid:{0}, index path:{1}, metadata:{2},  active days: {3}, last index date :{4}, dir id:{5}, max buffer size:{6} MB, use compound file:{7}, processor type:{8}",
                        CommunityID, IndexPath, MetadataFile, LastActiveDays, LastIndexDate,
                        RollingIndexDirectoryID, MaxMBBufferSize, UseCompoundFile, IProcessorType);
            }
            catch
            {
                return base.ToString();
            }
        }

    }

    public class Counter
    {
        private int _count = 0;

        public int Count { get { return _count; } }

        public void Increment()
        {
            _count++;
        }

        public void Reset()
        {
            _count = 0;
        }

        public override string ToString()
        {
            return _count.ToString();
        }
    }

    public class CommunityIndexConfigCollection : ICacheable
    {

        private List<CommunityIndexConfig> _communityIndexConfigs = null;
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        private const string CACHEKEYFORMAT = "CommunityIndexConfigs";

        public List<CommunityIndexConfig> CommunityIndexConfigs
        {
            get { return _communityIndexConfigs; }
            set { _communityIndexConfigs = value; }
        }

        public CommunityIndexConfigCollection(List<CommunityIndexConfig> configs)
        {
            this._communityIndexConfigs = configs;
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEYFORMAT;
        }
        #endregion
    }

}
