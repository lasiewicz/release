﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.MingleSearchEngine.ServiceInterfaceNRT;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.ServiceAdaptersNRT
{
    public class SearcherNRTSA : SABase
    {
        public static readonly SearcherNRTSA Instance = new SearcherNRTSA();
        private ISettingsSA _settingsService = null;

        public string TestURI { get; set; }

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private SearcherNRTSA() { }

        public void NRTUpdateMember(MingleSearchMemberUpdate searchMemberUpdate, MingleSearchMember searchMember)
        {
            try
            {
                if (searchMemberUpdate != null && searchMemberUpdate.CommunityID > 0 && searchMemberUpdate.MemberID > 0)
                {
                    if (searchMemberUpdate.UpdateMode == UpdateModeEnum.remove)
                    {
                        NRTRemoveMember(searchMemberUpdate);
                    }
                    else
                    {
                        //Update all instances in all partitioons used by the community
                        List<string> uriList = getServiceManagerUriAllInstances(searchMemberUpdate.CommunityID);
                        foreach (string uri in uriList)
                        {
                            base.Checkout(uri);
                            try
                            {
                                if (searchMember != null)
                                {
                                    getService(uri).NRTUpdateMember(searchMember);
                                }
                            }
                            catch (Exception e)
                            {
                                //ignore, these API calls are OneWay, if service instance is down, we'll skip it
                            }
                            finally
                            {
                                base.Checkin(uri);
                            }
                        }
                    }
                }
                else
                {
                    //throw new Exception("NRTUpdateMember(): Missing community ID or MemberID");
                }
            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
        }


        public void NRTRemoveMember(MingleSearchMemberUpdate searchMemberUpdate)
        {
            try
            {
                if (searchMemberUpdate != null && searchMemberUpdate.CommunityID > 0)
                {
                    List<string> uriList = getServiceManagerUriAllInstances(searchMemberUpdate.CommunityID);
                    foreach (string uri in uriList)
                    {
                        base.Checkout(uri);
                        try
                        {
                            searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
                            getService(uri).NRTRemoveMember(searchMemberUpdate);
                        }
                        catch (Exception e)
                        {
                            //ignore, these API calls are OneWay, if service instance is down, we'll skip it
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }
                    }
                }
                else
                {
                    throw new Exception("NRTRemoveMember(): Missing community ID");
                }
            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
        }

        private ISearcherNRT getService(string uri)
        {
            try
            {
                return (ISearcherNRT)Activator.GetObject(typeof(ISearcherNRT), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri(int communityId)
        {
            try
            {
                if (!string.IsNullOrEmpty(TestURI))
                {
                    return TestURI;
                }
                string communityNameUpper = ServiceConstants.GetCommunityName(communityId).ToUpper();
                string serviceSearcherConst = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, communityNameUpper);
                string uri = AdapterConfigurationSA.GetServicePartition(serviceSearcherConst, PartitionMode.Random).ToUri(ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST);
                string overrideHostName = GetSetting(string.Format(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, communityNameUpper), string.Empty);

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private List<string> getServiceManagerUriAllInstances(int communityId)
        {
            try
            {
                List<string> uriList = new List<string>();
                if (!string.IsNullOrEmpty(TestURI))
                {
                    uriList.Add(TestURI);
                }
                else
                {
                    string communityNameUpper = ServiceConstants.GetCommunityName(communityId).ToUpper();
                    string serviceSearcherConst = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, communityNameUpper);

                    ArrayList servicePartitionList = AdapterConfigurationSA.GetServicePartitions(serviceSearcherConst);
                    foreach (Matchnet.Configuration.ValueObjects.ServicePartition servicePartition in servicePartitionList)
                    {
                        for (int i = 0; i < servicePartition.Count; i++)
                        {
                            Matchnet.Configuration.ValueObjects.ServiceInstance serviceInstance = servicePartition[i];
                            uriList.Add("tcp://" + serviceInstance.HostName + ":" + servicePartition.Port + "/" + ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST + ".rem");
                        }

                    }

                    string uri = "";
                    if (uriList.Count > 0)
                    {
                        uri = uriList[0];
                    }
                    string overrideHostName = GetSetting(string.Format(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, communityNameUpper), "");
                    if (overrideHostName.Length > 0 && !string.IsNullOrEmpty(uri))
                    {
                        uriList.Clear();
                        UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                        uriList.Add("tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path);
                    }
                }
                return uriList;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            try
            {
                base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("MINGLESEARCHER_SA_CONNECTION_LIMIT"));
            }
            catch (Exception ex)
            {
                base.MaxConnections = 50;
            }
        }

        public string GetSetting(string name, string defaultvalue)
        {
            try
            {
                return SettingsService.GetSettingFromSingleton(name);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

        public string GetSetting(string name, int communityID, string defaultvalue)
        {
            try
            {
                return SettingsService.GetSettingFromSingleton(name, communityID);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }


    }
}
