﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.Exceptions;
using Spark.MingleSearchEngine.ServiceManager;

namespace Spark.MingleSearchIndexer.Service
{
    public partial class MingleSearchIndexer : Matchnet.RemotingServices.RemotingServiceBase
    {
        public const string SERVICE_NAME = "Spark.MingleSearchIndexer.Service";
        IndexerSM _indexerSM;

        public MingleSearchIndexer()
        {
            InitializeComponent();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _indexerSM = new IndexerSM();
                base.RegisterServiceManager(_indexerSM);
            }
            catch (Exception ex)
            {
                Trace.Write(SERVICE_NAME + " - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
//                System.Threading.Thread.Sleep(300000);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
