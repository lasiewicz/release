﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.MingleSearchEngine.BusinessLogic;
using Spark.MingleSearchEngine.BusinessLogic.Indexer;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.BusinessLogic.Indexer
{
    [TestFixture]
    public class MingleSearchEngineIndexerTests : AbstractSparkUnitTest
    {

        [TestFixtureSetUp]
        public void StartUp()
        {
            IndexerBL.Instance.USE_NUNIT = true;
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            Spark.Logging.Utils.SettingsService = _mockSettingsService;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            IndexerBL.Instance.USE_NUNIT = false;
            _mockSettingsService = null;
            Utils.SettingsService = null;
            Spark.Logging.Utils.SettingsService = null;
        }

        private Hashtable GetHashConfig(int communityId, IProcessorType iProcessorType)
        {
            Hashtable h = new Hashtable();
            CommunityIndexConfig cic = IndexerBL.Instance.GetCommunityConfig(communityId, iProcessorType);
            h["config"] = cic;
            h["path"] = (!cic.EnableRollingUpdates) ? cic.IndexPath : cic.IndexPath + cic.RollingIndexDirectoryID;
            return h;
        }

        [Test]
        public void TestChristianMingleIndex()
        {
            IndexerBL.Instance.ClearProcessorTypesToIndex();
            IndexProcessorFactory.Instance.ResetCommunity(25, IProcessorType.MemberIndexProcessor);
            IndexerBL.Instance.AddProcessorTypeToIndex(IProcessorType.MemberIndexProcessor);
            IndexerBL.Instance.Start();
            System.Threading.Thread.Sleep((40 * 60) * 1000);
            while (!IndexerBL.Instance.IProcessorFactory.AreAllDone(25, IProcessorType.MemberIndexProcessor))
            {
                System.Threading.Thread.Sleep((20 * 60) * 1000);
            }
            IndexerBL.Instance.ClearProcessorTypesToIndex();
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestChristianMingleKeywordIndex()
        {
            IndexerBL.Instance.ClearProcessorTypesToIndex();
            IndexProcessorFactory.Instance.ResetCommunity(25, IProcessorType.KeywordIndexProcessor);
            IndexerBL.Instance.AddProcessorTypeToIndex(IProcessorType.KeywordIndexProcessor);
            IndexerBL.Instance.Start();
            System.Threading.Thread.Sleep((20 * 60) * 1000);
            while (!IndexerBL.Instance.IProcessorFactory.AreAllDone(25, IProcessorType.KeywordIndexProcessor))
            {
                System.Threading.Thread.Sleep((5 * 60) * 1000);
            }
            IndexerBL.Instance.ClearProcessorTypesToIndex();            
        }

    }
}
