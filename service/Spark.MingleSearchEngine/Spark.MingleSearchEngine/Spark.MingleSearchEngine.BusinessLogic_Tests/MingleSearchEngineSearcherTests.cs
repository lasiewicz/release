﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using NUnit.Framework;
using Spark.MingleSearchEngine.BusinessLogic.Searcher;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.BusinessLogic
{
    [TestFixture]
    public class MingleSearchEngineSearcherTests : AbstractSparkUnitTest
    {

        protected const string newChristianMingleIndexPath = "D:/matchnet/index/ChristianMingle2";

        [TestFixtureSetUp]
        public void StartUp()
        {
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            SearchersSetup();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            _mockSettingsService = null;
            Utils.SettingsService = null;
            SearchersTeardown();
        }

        [Test]
        public void TestChristianMingleGenderAndAgeRangeAndSpatialSearch()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.Proximity;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            ResetAvgSearchTimes();
            for (int i = 0; i < 100; i++)
            {
                results = searcherBl.RunQuery(searchParameterCollection, communityId);
            }

            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);
            print(string.Format("Avg Search Time:{0}", avgSearchTime));

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];                
                int age = (int)result["Age"];                
                int gender = (int)result["gender"];
                double distanceMi = (result.ContainsKey("Distance"))?(double)result["Distance"]:double.NaN;
                DateTime lastLogin = (DateTime)result["lastlogin"];
                int matchPercent = (result.ContainsKey("matchPercentage"))?(int)result["matchPercentage"] : 0;
                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                print(string.Format("UserId:{0}, Gender:{1}, Age:{2}, LoginDate:{3}, MatchPercent:{4}, Distance:{5}", userId, gender, age, lastLogin.ToString(), matchPercent, distanceMi));
            }
        }

        [Test]
        public void TestChristianMingleGenderAndAgeRangeSearch()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter));
            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];
                int age = (int)result["Age"];
                int gender = (int)result["gender"];
                DateTime lastLogin = (DateTime)result["lastlogin"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);

                print(string.Format("UserId:{0}, Gender:{1}, Age:{2}, LoginDate:{3}", userId, gender, age, lastLogin.ToString()));
            }
        }

        [Test]
        public void TestChristianMingleQuickSearch()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 75;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 3000, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height","48|72",SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "1|" + int.MaxValue, SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.Proximity;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            StringBuilder sb = new StringBuilder("UserId:{0}, Age:{1}, LoginDate:{2}, Distance:{3}, MatchPercentage:{4}, ");
            for (int i = 0; i < searchParameterCollection.SearchParameters.Count; i++)
            {
                SearchParameter sp = searchParameterCollection.SearchParameters[i];
                if (sp.Name.ToLower().Equals("location") || sp.Name.ToLower().Equals("maxresults") || sp.Name.ToLower().Equals("birthdate"))
                {
                    continue;
                }
                sb.Append(sp.Name + ": {" + (5+i) + "}, ");
            }
            string paramStrFormat = sb.ToString(0, sb.Length - 2);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];
                int age = (int)result["Age"];
                int gender = (int)result["gender"];
                double distanceMi = (double)result["Distance"];
                DateTime lastLogin = (DateTime)result["lastlogin"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                object[] values = new object[searchParameterCollection.SearchParameters.Count+4];
                values[0] = userId;
                values[1] = age;
                values[2] = lastLogin.ToString();
                values[3] = distanceMi;
                values[4] = (int)result["matchPercentage"];

                for (int i = 0; i < searchParameterCollection.SearchParameters.Count; i++)
                {
                    SearchParameter sp = searchParameterCollection.SearchParameters[i];
                    if (sp.Name.ToLower().Equals("location") || sp.Name.ToLower().Equals("maxresults") || sp.Name.ToLower().Equals("birthdate"))
                    {
                        continue;
                    }
                    values[(5+i)] = result[sp.Name];
                }
                
                print(string.Format(paramStrFormat, values));
            }
        }

        [Test]
        public void TestChristianMingleQuickSearchMatchPercentSort()
        {
            int communityId = 25;
            int genderVal = 0;
            int minAgeVal = 18;
            int maxAgeVal = 75;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 500;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 3000, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "1|" + int.MaxValue, SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("ethnicity", 4, SearchParameterType.Has))
                .Add(new SearchParameter("bodytype", 8, SearchParameterType.Has))
                .Add(new SearchParameter("education", 32, SearchParameterType.Has))
                .Add(new SearchParameter("maritalstatus", 4, SearchParameterType.Has))
                .Add(new SearchParameter("religion", 1, SearchParameterType.Has))
                .Add(new SearchParameter("smoke", 1, SearchParameterType.Has))
                .Add(new SearchParameter("drink", 2, SearchParameterType.Has))
                .Add(new SearchParameter("ethnicity", 4, SearchParameterType.Wants))
                .Add(new SearchParameter("bodytype", 64, SearchParameterType.Wants))
                .Add(new SearchParameter("education", 34, SearchParameterType.Wants))
                .Add(new SearchParameter("maritalstatus", 5, SearchParameterType.Wants))
                .Add(new SearchParameter("religion", 139297, SearchParameterType.Wants))
                .Add(new SearchParameter("smoke", 15, SearchParameterType.Wants))
                .Add(new SearchParameter("drink", 31, SearchParameterType.Wants))
                .Add(new SearchParameter("ethnicity", 3, SearchParameterType.Importance))
                .Add(new SearchParameter("bodytype", 1, SearchParameterType.Importance))
                .Add(new SearchParameter("education", 3, SearchParameterType.Importance))
                .Add(new SearchParameter("maritalstatus", 0, SearchParameterType.Importance))
                .Add(new SearchParameter("religion", 2, SearchParameterType.Importance))
                .Add(new SearchParameter("smoke", 2, SearchParameterType.Importance))
                .Add(new SearchParameter("drink", 1, SearchParameterType.Importance))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.MatchPercent;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            StringBuilder sb = new StringBuilder("UserId:{0}, Age:{1}, LoginDate:{2}, Distance:{3}, MatchPercentage:{4}, Gender:{5}, ");
            for (int i = 0; i < searchParameterCollection.SearchParameters.Count; i++)
            {
                SearchParameter sp = searchParameterCollection.SearchParameters[i];
                if (sp.Name.ToLower().Equals("location") || sp.Name.ToLower().Equals("maxresults") || sp.Name.ToLower().Equals("birthdate"))
                {
                    continue;
                }
                sb.Append(sp.Name + ": {" + (6 + i) + "}, ");
            }
            string paramStrFormat = sb.ToString(0, sb.Length - 2);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];
                int age = (int)result["Age"];
                int gender = (int)result["gender"];
                double distanceMi = (double)result["Distance"];
                DateTime lastLogin = (DateTime)result["lastlogin"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                object[] values = new object[searchParameterCollection.SearchParameters.Count + 5];
                values[0] = userId;
                values[1] = age;
                values[2] = lastLogin.ToString();
                values[3] = distanceMi;
                values[4] = (int)result["matchPercentage"];
                values[5] = gender;
                for (int i = 0; i < searchParameterCollection.SearchParameters.Count; i++)
                {
                    SearchParameter sp = searchParameterCollection.SearchParameters[i];
                    if (sp.Name.ToLower().Equals("location") || sp.Name.ToLower().Equals("maxresults") || sp.Name.ToLower().Equals("birthdate"))
                    {
                        continue;
                    }
                    values[(6 + i)] = result[sp.Name];
                }

                print(string.Format(paramStrFormat, values));
            }
        }

        [Test]
        public void TestChristianMingleQuickSearchMatchPercentSortTerseResult()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 75;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 3000, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "1|" + int.MaxValue, SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("ethnicity", 4, SearchParameterType.Has))
                .Add(new SearchParameter("bodytype", 8, SearchParameterType.Has))
                .Add(new SearchParameter("education", 32, SearchParameterType.Has))
                .Add(new SearchParameter("maritalstatus", 4, SearchParameterType.Has))
                .Add(new SearchParameter("religion", 1, SearchParameterType.Has))
                .Add(new SearchParameter("smoke", 1, SearchParameterType.Has))
                .Add(new SearchParameter("drink", 2, SearchParameterType.Has))
                .Add(new SearchParameter("ethnicity", 4, SearchParameterType.Wants))
                .Add(new SearchParameter("bodytype", 64, SearchParameterType.Wants))
                .Add(new SearchParameter("education", 34, SearchParameterType.Wants))
                .Add(new SearchParameter("maritalstatus", 5, SearchParameterType.Wants))
                .Add(new SearchParameter("religion", 139297, SearchParameterType.Wants))
                .Add(new SearchParameter("smoke", 15, SearchParameterType.Wants))
                .Add(new SearchParameter("drink", 31, SearchParameterType.Wants))
                .Add(new SearchParameter("ethnicity", 3, SearchParameterType.Importance))
                .Add(new SearchParameter("bodytype", 1, SearchParameterType.Importance))
                .Add(new SearchParameter("education", 3, SearchParameterType.Importance))
                .Add(new SearchParameter("maritalstatus", 0, SearchParameterType.Importance))
                .Add(new SearchParameter("religion", 2, SearchParameterType.Importance))
                .Add(new SearchParameter("smoke", 2, SearchParameterType.Importance))
                .Add(new SearchParameter("drink", 1, SearchParameterType.Importance))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.MatchPercent;
            searchParameterCollection.ResultType = SearchResultType.TerseResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];
                int matchPercent = (int) result["matchPercentage"];

                print(string.Format("UserId:{0}, MatchPercentage:{1}", userId, matchPercent));
            }
        }


        [Test]
        public void TestMingleNRTRemoveMember_AdminSuspend()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594637;
            double radiansLng = -2.066426;
            int radiusMI = 100;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 160, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
//                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
//                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
//                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
//                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
//                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
//                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
//                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "0|" + int.MaxValue, SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
    
            //set test members
            var mingleMembers = GetMingleSearchMembers(genderVal, communityId);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.MingleSearchEngine.ValueObjects/SearchDocument.xml", searcherBl, mingleMembers);
            System.Threading.Thread.Sleep(2500);

            //get initial results count
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            int initialResultsCount = results.Count;
            Assert.AreEqual(mingleMembers.Count, initialResultsCount);

            //remove member NRT
            int memberID = 103820222;
            MingleSearchMemberUpdate searchMemberUpdate = new MingleSearchMemberUpdate();
            searchMemberUpdate.CommunityID = communityId;
            searchMemberUpdate.MemberID = memberID;
            searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
            searchMemberUpdate.UpdateReason = UpdateReasonEnum.adminSuspend;

            searcherBl.NRTRemoveMember(searchMemberUpdate);

            //refresh NRT Reader
            searcherBl.GetSearcher(communityId, IProcessorType.MemberIndexProcessor).UpdateNRT();
            System.Threading.Thread.Sleep(2500);

            //check results count
            ArrayList finalResults = searcherBl.RunQuery(searchParameterCollection, communityId);
            int finalResultsCount = finalResults.Count;
            print("SparkNRTRemoveMember Test - MemberID:" + memberID.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(initialResultsCount - 1, finalResultsCount);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, searchParameterCollection);
        }

        [Test]
        public void TestMingleNRTUpdateMember_LoginDate()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594637;
            double radiansLng = -2.066426;
            int radiusMI = 100;
            int memberID = 103939807;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 160, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
//                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
//                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
//                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
//                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
//                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
//                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
//                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "0|"+int.MaxValue, SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
    
            //set test members
            var mingleMembers = GetMingleSearchMembers(genderVal, communityId);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.MingleSearchEngine.ValueObjects/SearchDocument.xml", searcherBl, mingleMembers);
            System.Threading.Thread.Sleep(2500);

            //get initial results count
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            int initialResultsCount = results.Count;
            int initialMostRecentActiveMember = (int)((Hashtable)results[0])["userid"];
            Assert.AreEqual(mingleMembers.Count, initialResultsCount);

            //update member NRT
            MingleSearchMember searchMember = MingleSearchMember.GenerateSearchMember(memberID, communityId, genderVal, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 70, 8, -21474, 3443821, -2.066127, 0.594557, DateTime.Now, 102, 0, 2, 0, 0, 55.02m, 8.39m, 16.84m, 19.74m, 4, 13, 4, -1, 0, 0, 17, 5245953, 0, 15, 8, 0, 0, 3, 2, 3, 0, 2, 3, 0);
            searchMember.UpdateReason = UpdateReasonEnum.logon;
            searchMember.UpdateMode = UpdateModeEnum.update;
            searcherBl.NRTUpdateMember(searchMember);

            //refresh NRT Reader
            searcherBl.GetSearcher(communityId, IProcessorType.MemberIndexProcessor).UpdateNRT();
            System.Threading.Thread.Sleep(2500);

            //check results count and most recent active member
            ArrayList finalResults = searcherBl.RunQuery(searchParameterCollection, communityId);
            int finalResultsCount = finalResults.Count;
            int finalMostRecentActiveMember = (int)((Hashtable)finalResults[0])["userid"];

            print("SparkNRTUpdateMember Test - InitialMostRecentMemberID:" + initialMostRecentActiveMember.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", MemberIDToUpdate: " + memberID.ToString() + ", FinalMostRecentMemberID: " + finalMostRecentActiveMember.ToString() + ",  FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(initialResultsCount, finalResultsCount);
            Assert.AreEqual(memberID, finalMostRecentActiveMember);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, searchParameterCollection);
        }


        [Test]
        public void SearchChristianMingleChangePathTest()
        {
            int communityId = 25;
            int genderVal = 0;
            int minAgeVal = 18;
            int maxAgeVal = 90;
            double radiansLat = 0.594637;
            double radiansLng = -2.066426;
            int radiusMI = 2000;

            SearchParameter genderParam = new SearchParameter("gender", genderVal, SearchParameterType.Int);
            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 160, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(genderParam)
                //                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
                //                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
                //                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
                //                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
                //                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
                //                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
                //                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "0|" + int.MaxValue, SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);
            long millis = stopWatch.ElapsedMilliseconds;
            //Assert.Less(millis, 3000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}\n\n\n", millis, results.Count, searchParameterCollection.ToString()));

            var searcher = searcherBl.GetSearcher(communityId, IProcessorType.MemberIndexProcessor);
            string originalIndexPath = searcher.IndexPath;
            searcher.NUNIT_IndexPath = newChristianMingleIndexPath;
            searcher.Update();

            searchParameterCollection.Remove(genderParam).Add(new SearchParameter("gender", 1, SearchParameterType.Int));
            stopWatch.Start();
            ArrayList results2 = searcherBl.RunQuery(searchParameterCollection, communityId);
            stopWatch.Stop();


            millis = stopWatch.ElapsedMilliseconds;
            Assert.AreEqual(searcher.NUNIT_IndexPath, searcher.IndexPath);
            Assert.NotNull(results2);
            Assert.GreaterOrEqual(results2.Count, 1);
            //Assert.Less(millis, 13000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results2.Count, searchParameterCollection.ToString()));

            //wait, ensure close old searcher finishes
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }

            //reset back to original index path
            searcher.NUNIT_IndexPath = originalIndexPath;
            searcher.Update();
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }

        }



        private List<MingleSearchMember> GetMingleSearchMembers(int genderVal, int communityId)
        {
            List<MingleSearchMember> mingleMembers = new List<MingleSearchMember>();
            //beverly hills (zip=90211)
            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(103939806, communityId, genderVal,
                                                                      DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"),
                                                                      DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 70, 8, -21474,
                                                                      3443821, -2.066127, 0.594557, DateTime.Now, 102, 0, 2, 0,
                                                                      0, 55.02m, 8.39m, 16.84m, 19.74m, 4, 13, 4, -1, 3071, 3, 19, 16776703, 7, 31, 8, 1023, 2, 2, 2, 2, 2, 1, 3, 2));
            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(103939807, communityId, genderVal,
                                                                      DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"),
                                                                      DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 70, 8, -21474,
                                                                      3443821, -2.066127, 0.594557, DateTime.Now, 102, 0, 2, 0,
                                                                      0, 55.02m, 8.39m, 16.84m, 19.74m, 4, 13, 4, -1, 0, 0, 17, 5245953, 0, 15, 8, 0, 0, 3, 2, 3, 0, 2, 3, 0));
            //hollywood 5 miles away (zip=90028)
            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(103820222, communityId, genderVal,
                                                                      DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"),
                                                                      DateTime.Now, 2, 8, 8, 2, 256, 2, 2, 55, 2, -21474,
                                                                      9805694, -2.065197, 0.595144, DateTime.Now, 102, 0, 2, 0,
                                                                      0, 0.0m, 0.0m, 0.0m, 0.0m, 6, 1, 14, -1, 0, 1, 23, 0, 7, 29, 0, 0, 0, 3, 2, 2, 2, 2, 2, 2));
            //pasadena 20 miles away (zip=91101)
            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(23247846, communityId, genderVal,
                                                                      DateTime.Parse("1974-12-10"), DateTime.Parse("2003-06-12"),
                                                                      DateTime.Now, 3, 128, 131072, 2, 2, 4, 4, 49, 8, 0,
                                                                      3445194, -2.061917, 0.595984, DateTime.Now, 102, 0, 2, 0,
                                                                      0, 0.0m, 0.0m, 0.0m, 0.0m, 6, 1, 14, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
            return mingleMembers;
        }

    }
}
