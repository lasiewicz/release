﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Spark.MingleSearchEngine.BusinessLogic;
using Spark.MingleSearchEngine.BusinessLogic.Searcher;
using Spark.MingleSearchEngine.ServiceAdaptersNRT;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.ServiceAdapters
{
    [TestFixture]
    public class MingleSearchEngineSearcherSATests : AbstractSparkUnitTest
    {
        private SearcherSA _searcherSA = null;
        private SearcherNRTSA _searcherNRTSA = null;

        [TestFixtureSetUp]
        public void StartUp()
        {
            _searcherSA = SearcherSA.Instance;
            _searcherNRTSA = SearcherNRTSA.Instance;
//            SetupMockSettingsService();
//            _searcherSA.SettingsService = _mockSettingsService;
//            _searcherNRTSA.SettingsService = _mockSettingsService;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
//            _searcherSA.SettingsService = null;
//            _searcherNRTSA.SettingsService = null;
//            _mockSettingsService = null;
            _searcherSA = null;
            _searcherNRTSA = null;

        }

        [Test]
        public void TestChristianMingleGenderAndAgeRangeAndSpatialSearch()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.Proximity;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            ArrayList results = SearcherSA.Instance.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];                
                int age = (int)result["Age"];                
                int gender = (int)result["gender"];
                double distanceMi = (double)result["Distance"];
                DateTime lastLogin = (DateTime)result["lastlogin"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                print(string.Format("UserId:{0}, Gender:{1}, Age:{2}, LoginDate:{3}, Distance:{4}", userId, gender, age,
                                    lastLogin.ToString(), distanceMi));
            }
        }

        [Test]
        public void TestChristianMingleGenderAndAgeRangeSearch()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter));
            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            ArrayList results = SearcherSA.Instance.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];
                int age = (int)result["Age"];
                int gender = (int)result["gender"];
                DateTime lastLogin = (DateTime)result["lastlogin"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);

                print(string.Format("UserId:{0}, Gender:{1}, Age:{2}, LoginDate:{3}", userId, gender, age, lastLogin.ToString()));
            }
        }

        [Test]
        public void TestChristianMingleQuickSearch()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 160, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height","48|72",SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "1|1000", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.Proximity;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            ArrayList results = SearcherSA.Instance.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            StringBuilder sb = new StringBuilder("UserId:{0}, Age:{1}, LoginDate:{2}, Distance:{3}, ");
            for (int i = 0; i < searchParameterCollection.SearchParameters.Count; i++)
            {
                SearchParameter sp = searchParameterCollection.SearchParameters[i];
                if (sp.Name.ToLower().Equals("location") || sp.Name.ToLower().Equals("maxresults") || sp.Name.ToLower().Equals("birthdate"))
                {
                    continue;
                }
                sb.Append(sp.Name + ": {" + (4+i) + "}, ");
            }
            string paramStrFormat = sb.ToString(0, sb.Length - 2);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["userid"];
                int age = (int)result["Age"];
                int gender = (int)result["gender"];
                double distanceMi = (double)result["Distance"];
                DateTime lastLogin = (DateTime)result["lastlogin"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                object[] values = new object[searchParameterCollection.SearchParameters.Count+3];
                values[0] = userId;
                values[1] = age;
                values[2] = lastLogin.ToString();
                values[3] = distanceMi;

                for (int i = 0; i < searchParameterCollection.SearchParameters.Count; i++)
                {
                    SearchParameter sp = searchParameterCollection.SearchParameters[i];
                    if (sp.Name.ToLower().Equals("location") || sp.Name.ToLower().Equals("maxresults") || sp.Name.ToLower().Equals("birthdate"))
                    {
                        continue;
                    }
                    values[(4+i)] = result[sp.Name];
                }
                
                print(string.Format(paramStrFormat, values));
            }

        }

//        [Test]
//        public void TestSparkNRTRemoveMember_AdminSuspend()
//        {
//            int communityId = 25;
//            int genderVal = 1;
//            int minAgeVal = 18;
//            int maxAgeVal = 55;
//            double radiansLat = 0.594637;
//            double radiansLng = -2.066426;
//            int radiusMI = 100;
//
//            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
//                .Add(new SearchParameter("maxResults", 160, SearchParameterType.MaxResults))
//                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
//                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
//                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
//                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
//                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
//                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
//                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
//                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
//                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
//                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
//                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
//                .Add(new SearchParameter("photo", "0|1000", SearchParameterType.IntRangeFilter))
//                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
//            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
//            searchParameterCollection.ResultType = SearchResultType.DetailedResult;
//    
//            //set test members
//            List<MingleSearchMember> mingleMembers = new List<MingleSearchMember>();
//            //beverly hills (zip=90211)
//            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(103939806, communityId, genderVal, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 70, 8, -21474, 3443821, -2.066127, 0.594557, DateTime.Now, 102, 0, 2, 0, 0, 55.02m, 8.39m, 16.84m, 19.74m, 4, 13, 4, -1));
//            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(103939807, communityId, genderVal, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 70, 8, -21474, 3443821, -2.066127, 0.594557, DateTime.Now, 102, 0, 2, 0, 0, 55.02m, 8.39m, 16.84m, 19.74m, 4, 13, 4, -1));
//            //hollywood 5 miles away (zip=90028)
//            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(103820222, communityId, genderVal, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 2, 8, 8, 2, 256, 2, 2, 55, 2, -21474, 9805694, -2.065197, 0.595144, DateTime.Now, 102, 0, 2, 0, 0, 0.0m, 0.0m, 0.0m, 0.0m, 6, 1, 14, -1));
//            //pasadena 20 miles away (zip=91101)
//            mingleMembers.Add(MingleSearchMember.GenerateSearchMember(23247846, communityId, genderVal, DateTime.Parse("1974-12-10"), DateTime.Parse("2003-06-12"), DateTime.Now, 3, 128, 131072, 2, 2, 4, 4, 49, 8, 0, 3445194, -2.061917, 0.595984, DateTime.Now, 102, 0, 2, 0, 0, 0.0m, 0.0m, 0.0m, 0.0m, 6, 1, 14, -1));
//
//            //create ram directory
//            SetupTestDataInWriter(communityId, "../../../Spark.MingleSearchEngine.ValueObjects/SearchDocument.xml", searcherBl, mingleMembers);
//
//            //get initial results count
//            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
//            int initialResultsCount = results.Count;
//            Assert.AreEqual(mingleMembers.Count, initialResultsCount);
//
//            //remove member NRT
//            int memberID = 103820222;
//            MingleSearchMemberUpdate searchMemberUpdate = new MingleSearchMemberUpdate();
//            searchMemberUpdate.CommunityID = communityId;
//            searchMemberUpdate.MemberID = memberID;
//            searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
//            searchMemberUpdate.UpdateReason = UpdateReasonEnum.adminSuspend;
//
//            searcherBl.NRTRemoveMember(searchMemberUpdate);
//
//            //refresh NRT Reader
//            searcherBl.GetSearcher(communityId, IProcessorType.MemberIndexProcessor).UpdateNRT();
//
//            //check results count
//            ArrayList finalResults = searcherBl.RunQuery(searchParameterCollection, communityId);
//            int finalResultsCount = finalResults.Count;
//            print("SparkNRTRemoveMember Test - MemberID:" + memberID.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", FinalResults:" + finalResultsCount.ToString());
//            Assert.AreEqual(initialResultsCount - 1, finalResultsCount);
//
//            //Regenerate SearchBL to use default FS index
//            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
//            CreateAndInitializeSearcherBL(communityId, searcherBL, searchParameterCollection);
//        }

        [Test]
        public void TestSparkNRTUpdateMember_LoginDate()
        {
            int communityId = 25;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594637;
            double radiansLng = -2.066426;
            int radiusMI = 100;
            int memberID = 103939807;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 160, SearchParameterType.MaxResults))
                .Add(new SearchParameter("height", "48|72", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
//                .Add(new SearchParameter("ethnicity", 3071, SearchParameterType.Mask))
//                .Add(new SearchParameter("bodytype", 255, SearchParameterType.Mask))
//                .Add(new SearchParameter("education", 1023, SearchParameterType.Mask))
//                .Add(new SearchParameter("maritalstatus", 7, SearchParameterType.Mask))
//                .Add(new SearchParameter("religion", 16776703, SearchParameterType.Mask))
//                .Add(new SearchParameter("smoke", 15, SearchParameterType.Mask))
//                .Add(new SearchParameter("drink", 31, SearchParameterType.Mask))
                .Add(new SearchParameter("photo", "0|1000", SearchParameterType.IntRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.LastLogonDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            //get initial results count
            ArrayList results = _searcherSA.RunQuery(searchParameterCollection, communityId);
            int initialResultsCount = results.Count;
            int initialMostRecentActiveMember = (int)((Hashtable)results[0])["userid"];

            //update member NRT
            MingleSearchMember searchMember = MingleSearchMember.GenerateSearchMember(memberID, communityId, genderVal, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 70, 8, -21474, 3443821, -2.066127, 0.594557, DateTime.Now, 102, 0, 2, 0, 0, 55.02m, 8.39m, 16.84m, 19.74m, 4, 13, 4, -1, 0, 0, 17, 5245953, 0, 15, 8, 0, 0, 3, 2, 3, 0, 2, 3, 0);
            searchMember.UpdateReason = UpdateReasonEnum.logon;
            searchMember.UpdateMode = UpdateModeEnum.update;
            MingleSearchMemberUpdate mingleSearchMemberUpdate = new MingleSearchMemberUpdate();
            mingleSearchMemberUpdate.CommunityID = communityId;
            mingleSearchMemberUpdate.MemberID = searchMember.UserID;
            mingleSearchMemberUpdate.UpdateMode = searchMember.UpdateMode;
            mingleSearchMemberUpdate.UpdateReason = searchMember.UpdateReason;
            _searcherNRTSA.NRTUpdateMember(mingleSearchMemberUpdate, searchMember);

            int waitTime = Utils.GetSetting("SEARCHER_INDEX_UPDATE_INTERVAL", 60000);
            System.Threading.Thread.Sleep(waitTime);

            //check results count and most recent active member
            ArrayList finalResults = _searcherSA.RunQuery(searchParameterCollection, communityId);

            //remove the member to cleanup the index
            MingleSearchMemberUpdate removeMember = new MingleSearchMemberUpdate();
            removeMember.CommunityID = communityId;
            removeMember.MemberID = searchMember.UserID;
            removeMember.UpdateMode = UpdateModeEnum.remove;
            removeMember.UpdateReason = UpdateReasonEnum.updateChangedToRemove;
            _searcherNRTSA.NRTRemoveMember(removeMember);

            //run assserts
            int finalResultsCount = finalResults.Count;
            int finalMostRecentActiveMember = (int)((Hashtable)finalResults[0])["userid"];

            print("SparkNRTUpdateMember Test - InitialMostRecentMemberID:" + initialMostRecentActiveMember.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", MemberIDToUpdate: " + memberID.ToString() + ", FinalMostRecentMemberID: " + finalMostRecentActiveMember.ToString() + ",  FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(memberID, finalMostRecentActiveMember);
            Assert.AreNotEqual(initialMostRecentActiveMember, finalMostRecentActiveMember);


        }

    }
}
