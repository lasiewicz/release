﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Spark.MingleSearchEngine.ValueObjects;

namespace Spark.MingleSearchEngine.ServiceInterfaceNRT
{
    [ServiceContract(Namespace = "spark.net")]
    public interface ISearcherNRT
    {
        [OperationContract]
        void NRTUpdateMember(MingleSearchMember searchMember);

        [OperationContract]
        void NRTRemoveMember(MingleSearchMemberUpdate searchMemberUpdate);
    }
}
