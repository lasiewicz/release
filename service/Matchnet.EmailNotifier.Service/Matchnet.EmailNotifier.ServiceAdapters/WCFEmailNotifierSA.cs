﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.WCFClient;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.Service.ServiceContracts;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
namespace Matchnet.EmailNotifier.ServiceAdapters
{
    public class WCFEmailNotifierSA : WCFClientBase<IEmailNotifierSvc>
    {
        const string SERVICE_CONST = "EMAILNOTIFIER_IIS_SVC";
        const string SERVICE_NAME = "EmailNotifierSvc";

        public static WCFEmailNotifierSA Instance = new WCFEmailNotifierSA();
        public string TestURI { get; set; }
        private WCFEmailNotifierSA():base()
        {
           
            _settings= WCFClientHelper.GetServiceSettings(SERVICE_CONST);
            
            _cache.Insert(_settings);
            
        }
        public ScheduledEvent SaveScheduledEvent(ScheduledEvent schedule)
        {
            WcfServiceBasicHttpClient<IEmailNotifierSvc> _client = null;
            ScheduledEvent ev = null;
            bool newEvent = false;
            try
            {
                if (schedule == null)
                    return null;

                if (!ValidateEventEmail(schedule.EmailAddress))
                    return null;
                if (String.IsNullOrEmpty(schedule.ScheduleGUID))
                {
                    schedule.ScheduleGUID = Guid.NewGuid().ToString();
                    newEvent = true;
                }
               

                _client = GetClient(schedule.ScheduleGUID);
                if(newEvent)
                    return _client.Instance.SaveScheduledEvent(schedule);
                else
                    return _client.Instance.SaveScheduledEventAsync(schedule);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("EmailNotifierSA.SavecheduledEvent" + schedule.ToString() + ", exception" + ex.ToString());

                throw new Matchnet.Exceptions.SAException("Error saving ScheduledEvent: " + schedule.ToString(), ex);
            }
            finally
            {
                if(_client != null)
                    _client.Dispose();
            }
            
        }
      

        public ScheduledEvent GetScheduledEvent(string guid)
        {
            WcfServiceBasicHttpClient<IEmailNotifierSvc> _client = null;
            ScheduledEvent ev = null;
            try
            {


                _client = GetClient(guid);
                

                return _client.Instance.GetScheduledEvent(guid);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("EmailNotifierSA.GetScheduledEvent, exception" + ex.ToString());

                throw new Matchnet.Exceptions.SAException("Error GetScheduledEvent ", ex);
            }
            finally
            {
                if (_client != null)
                    _client.Dispose();
            }

        }


        public ScheduledEvent GetScheduledEventByMember(int groupID, int memberID, string schedulename)
        {

            WcfServiceBasicHttpClient<IEmailNotifierSvc> _client = null;
            ScheduledEvent ev = null;
            try
            {
                _client = GetClient(Guid.NewGuid().ToString());
                return _client.Instance.GetScheduledEventByMember(groupID, memberID, schedulename);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("EmailNotifierSA.GetScheduledEvent, exception" + ex.ToString());

                throw new Matchnet.Exceptions.SAException("Error GetScheduledEvent ", ex);
            }
            finally
            {
                if (_client != null)
                    _client.Dispose();
            }
        }


        public void DeleteScheduledEvent(string guid)
        {
            WcfServiceBasicHttpClient<IEmailNotifierSvc> _client = null;
            ScheduledEvent ev = null;
            try
            {


                _client = GetClient(guid);


                 _client.Instance.DeleteScheduledEventAsync(guid);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("EmailNotifierSA.DeleteScheduledEvent, exception" + ex.ToString());

                throw new Matchnet.Exceptions.SAException("Error DeleteScheduledEvent ", ex);
            }
            finally
            {
                if (_client != null)
                    _client.Dispose();
            }

        }


        private WcfServiceBasicHttpClient<IEmailNotifierSvc> GetClient(string guid)
        {
            try
            {
                string urioverride = TestURI;
                //try
                //{
                //    if (String.IsNullOrEmpty(urioverride))
                //        urioverride = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILNOTIFIER_IIS_SA_HOST_OVERRIDE");
                    
                //}
                //catch (Exception settingExc)
                //{ }

                
                if (!String.IsNullOrEmpty(urioverride))
                {
                    return base.GetProxy(SERVICE_CONST, SERVICE_NAME, urioverride);

                }
                else
                {
                    return base.GetProxy(SERVICE_CONST, SERVICE_NAME, PartitionMode.Calculated, new Guid(guid));
                }

            
            }
            catch (Exception ex)
            { throw new Matchnet.Exceptions.SAException(ex); }

        }

        private bool ValidateEventEmail(string email)
        {
            try
            {
                if (String.IsNullOrEmpty(email))
                    return false;

                string match = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

                System.Text.RegularExpressions.Regex regValidation = new System.Text.RegularExpressions.Regex(match);
                return regValidation.IsMatch(email);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("EmailNotifierSA.ValidateEventEmail" + email + ", exception" + ex.ToString());

                throw new Matchnet.Exceptions.SAException("Error ValidateEventEmail: " + email, ex);

            }

        }


       
       

    }
}
