﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.EmailNotifier.ValueObjects.QueueMessage;
using Spark.Logging;
namespace Matchnet.EmailNotifier.BusinessLogic.QueueProcessors
{
    public class ScheduledProcessor
    {
        const string CLASS_NAME = "ScheduledProcessor";
        public const string QUEUE_PATH = @".\private$\EmailScheduler";
        bool _runnable;

        private MessageQueue emailSchedulerQueue;
        private Thread[] threads;

        public void Start()
        {
            const string functionName = "Start";
            try
            {
                startThreads();
            }
            catch (Exception ex)
            {

               // PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error starting ScheduleProcessor", ex, null);

            }

        }


        public void Stop()
        {
            const string functionName = "Stop";
            try
            {
                stopThreads();
            }
            catch (Exception ex)
            {

                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error stopping ScheduleProcessor", ex, null);

            }

        }

        private void startThreads()
        {
            const string functionName = "startThreads";
            try
            {
                _runnable = true;
                RollingFileLogger.Instance.EnterMethod(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "startThreads");
               // emailSchedulerQueue = new MessageQueue(QUEUE_PATH);
                //emailSchedulerQueue.Formatter = new BinaryMessageFormatter();
                Int16 threadCount = 1;
                try
                {
                     threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILNOTIFIERSVC_SCHEDULER_THREAD_COUNT"));
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Configuration value for threads num not found (duh), starting QueueProcessor anyway. Find time and insert that EMAILNOTIFIERSVC_THREAD_COUNT setting, please", ex, null);
                }
                threads = new Thread[threadCount];

                for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
                {
                    Thread t = new Thread(new ThreadStart(processQueue));
                    t.Name = "ProcessEmailScheduleThread" + threadNum.ToString();
                    t.Start();
                    threads[threadNum] = t;
                  
                }
                RollingFileLogger.Instance.LeaveMethod(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "startThreads");
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error starting ScheduleProcessor threads", ex, null);
                RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "startThreads",ex,null);
            }

        }


        private void stopThreads()
        {
            const string functionName = "stopThreads";
            _runnable = false;
            try
            {
                Int16 threadCount = (Int16)threads.Length;

                for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
                {
                    threads[threadNum].Join(10000);
                }

               

            }
            catch (Exception ex)
            {
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error stopping ScheduleProcessor threads", ex, null);
            }

        }

        public static void Enqueue(IQueueMessage message)
        {
            const string functionName = "Enqueue";
            try
            {
                if (!MessageQueue.Exists(QUEUE_PATH))
                {
                    MessageQueue.Create(QUEUE_PATH, true);
                }

                MessageQueue queue = new MessageQueue(QUEUE_PATH);
                queue.Formatter = new BinaryMessageFormatter();
                MessageQueueTransaction trans = new MessageQueueTransaction();

                try
                {
                    trans.Begin();
                    queue.Send(message, trans);

                    trans.Commit();
                }
                finally
                {
                    trans.Dispose();
                }

            }
            catch (Exception ex)
            {
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error enqueueing", ex, null);
            }
        }

        private void processQueue()
        {
            const string functionName = "processQueue";
            try
            {
                RollingFileLogger.Instance.EnterMethod(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "processQueue");
                _runnable = true;
                while (_runnable)
                {
                    
                    processQueueTransaction();
                }
            //    _runnable = false;
                RollingFileLogger.Instance.LogWarningMessage(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME,"processQueue - exit loop",null);

            }

            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "processQueue", ex,null);
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error in processQueue", ex, null);
            }

        }

        private void processQueueTransaction()
        {
            MessageQueueTransaction tran = null;

            try
            {
                RollingFileLogger.Instance.EnterMethod(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "processQueueTransaction");
                tran = new MessageQueueTransaction();
                tran.Begin();
                RollingFileLogger.Instance.LogInfoMessage(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "processQueueTransaction - receiving",null);
                IQueueMessage msg = receive(tran);

                processMessage(msg, tran);
                tran.Commit();
                tran.Dispose();
                tran = null;
                RollingFileLogger.Instance.LeaveMethod(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "processQueueTransaction");
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "processQueueTransaction - before attemptrollback", ex, null);
                attemptRollback(tran);
               
                Thread.Sleep(5000);
            }
        }

        private void processMessage(IQueueMessage msg, MessageQueueTransaction tran)
        {

            try
            {
                if (msg == null)
                { throw (new Exception("Queue message is null")); }

                ScheduledEventMessage savemsg = (ScheduledEventMessage)msg;

                if (savemsg == null)
                    return;

                switch (savemsg.MessageType)
                {
                    case(QueueMessageType.save):
                        EmailSchedulerBL.Instance.SaveScheduledEvent(savemsg.ScheduledEmailEvent);
                        break;

                    case (QueueMessageType.delete):
                        EmailSchedulerBL.Instance.DeleteScheduledEvent(savemsg.EventGuid);
                        break;

                }
            }
            catch (Exception ex)
            {
                attemptResend(tran, msg, ex);
            }
        }

        private IQueueMessage receive(MessageQueueTransaction tran)
        {
            IQueueMessage msg = null;
            try
            {
                if (!MessageQueue.Exists(QUEUE_PATH))
                {
                    MessageQueue.Create(QUEUE_PATH, true);
                }

                MessageQueue queue = new MessageQueue(QUEUE_PATH);
                queue.Formatter = new BinaryMessageFormatter();
                msg = (IQueueMessage)queue.Receive(tran).Body;
                return msg;
            }
            catch (MessageQueueException mex)
            { throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex); }
        }

        private void attemptResend(MessageQueueTransaction tran, IQueueMessage msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();

                    msg.Errors.Add(ex.Source + ", " + ex.Message);
                    msg.Attempts += 1;
                    msg.LastAttemptDateTime = DateTime.Now;

                    Enqueue(msg);
                }
            }
            catch (Exception e)
            {
                RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "attemptResend", ex, null);
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error in attemptResend", ex, null);
               // throw (e);
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                    tran = null;
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ValueObjects.ServiceConstants.SERVICE_NAME, CLASS_NAME, "attemptRollback", ex, null);
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error in attemptRollback", ex, null);
            }
        }


    }
}
