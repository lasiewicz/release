﻿using System;

using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using System.Threading;
using Matchnet.Exceptions;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.ValueObjects.QueueMessage;
using Spark.Logging;
namespace Matchnet.EmailNotifier.BusinessLogic.QueueProcessors
{
    public class DistributorProcessor
    {
        
        const string MODULE_NAME = "DistributorProcessor";
        public const string QUEUE_PATH = @"FormatName:DIRECT=OS:{0}\private$\EmailNotification";
        bool _runnable;

       // private MessageQueue emailSchedulerQueue;
        private Thread _thread;
        List<string> _queues = null;

        public string ServiceName = ValueObjects.ServiceConstants.SERVICE_NAME;
        public DistributorProcessor(string[] queuepathes)
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "DistributorProcessor");
            _queues = new List<string>();
            for (int i = 0; i < queuepathes.Length; i++)
            {

                _queues.Add(String.Format(QUEUE_PATH,queuepathes[i]));
                RollingFileLogger.Instance.LogInfoMessage(ServiceName, MODULE_NAME, "Adding queue - " + queuepathes[i],null);
            }
            RollingFileLogger.Instance.LeaveMethod(ServiceName, MODULE_NAME, "DistributorProcessor");
        }
        public void Start()
        {
            const string functionName = "Start";
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "Start");
                startThreads();
                RollingFileLogger.Instance.LeaveMethod(ServiceName, MODULE_NAME, "Start");
            }
            catch (Exception ex)
            {

                // PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error starting DistributorProcessor", ex, null);

            }

        }


        public void Stop()
        {
            const string functionName = "Stop";
            try
            {
                stopThreads();
            }
            catch (Exception ex)
            {

                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error stopping DistributorProcessor", ex, null);

            }

        }

        private void startThreads()
        {
            const string functionName = "startThreads";
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "StartThreads");
              //  emailSchedulerQueue = new MessageQueue(QUEUE_PATH);
               // emailSchedulerQueue.Formatter = new BinaryMessageFormatter();
                Int16 threadCount = 1;
              //it's distributor only 1 thread
                _thread = new Thread(new ThreadStart(processBatch));
                _thread.Name = "DistributorThread";

                _thread.Start();
                
                _runnable = true;
                RollingFileLogger.Instance.LeaveMethod(ServiceName, MODULE_NAME, "StartThreads");
                
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error starting DistributorProcessor threads", ex, null);
            }

        }


        private void stopThreads()
        {
            const string functionName = "stopThreads";
            _runnable = false;
            try
            {
               _thread.Join(10000);
                

            }
            catch (Exception ex)
            {
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error stopping DistributorProcessor threads", ex, null);
            }

        }

        public  void Enqueue(IQueueMessage message)
        {
            
            try
            {
                int num = DateTime.Now.Millisecond;
                int queuenum = num % _queues.Count;
               
                MessageQueue queue = new MessageQueue(_queues[queuenum]);
                queue.Formatter = new BinaryMessageFormatter();
                MessageQueueTransaction trans = new MessageQueueTransaction();

                try
                {
                    trans.Begin();
                    queue.Send(message, trans);

                    trans.Commit();
                }
                finally
                {
                    trans.Dispose();
                }

            }
            catch (Exception ex)
            {
                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error enqueueing in EmailNotifierProcessor", ex, null);
            }
        }

        private void processBatch()
        {
          
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "processBatch", null);
                _runnable = true;
                while (_runnable)
                { 
                    processBatchEvents();
                }
                RollingFileLogger.Instance.LogWarningMessage(ServiceName, MODULE_NAME, "processBatch - exiting loop", null);
            }

            catch (Exception ex)
            {

                new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error in EmailNotifierProcessor process Queue", ex, null);
            }

        }

        private void processBatchEvents()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "processBatchEvents", null);
                int batchsize=20;
                try{
                
                    batchsize=Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILNOTIFIER_BATCH_SIZE"));
                }
                catch(Exception ex){
                    RollingFileLogger.Instance.LogWarningMessage(ServiceName, MODULE_NAME, "In processBatchEvents settings EMAILNOTIFIER_BATCH_SIZE does not exist", null);
                }

                DataTable dt = EmailNotifierBL.Instance.GetScheduledBatch(batchsize);


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ScheduledEvent ev = EmailNotifierBL.Instance.PopulateEvent(dt.Rows[i]);
                    //mainly to check if registration completed and we do have member with this email
                    if (!Validate(ev))
                    {
                        EmailNotifierBL.Instance.DeleteSchedule(ev.EmailScheduleID);
                        continue;
                    }
                    ScheduledEventMessage msg = new ScheduledEventMessage(ev);
                    msg.MessageType = QueueMessageType.send;
                    Enqueue(msg);
                    }

                if (dt.Rows.Count == 0)
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceName, MODULE_NAME, "processBatchEvents - no batch, going to sleep", null);
                    Thread.Sleep(5000);
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceName, MODULE_NAME, "processBatchEvents - got batch", null);
                }
                RollingFileLogger.Instance.LeaveMethod(ServiceName, MODULE_NAME, "processBatchEvents", null);
            }
            catch (Exception ex)
            {
                new BLException("Error happened processing Schedule Batch", ex);
                RollingFileLogger.Instance.LogException(ServiceName, MODULE_NAME, "processBatchEvents - exception",ex, null);
                Thread.Sleep(5000);
            }
        }

        private void processMessage(IQueueMessage msg, MessageQueueTransaction tran)
        {

            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "processMessage", null);
                if (msg == null)
                { 
                    throw (new Exception("Queue message is null")); 
                }

                ScheduledEventMessage savemsg = (ScheduledEventMessage)msg;

                if (savemsg == null)
                    return;

                switch (savemsg.MessageType)
                {
                    case (QueueMessageType.send):
                        RollingFileLogger.Instance.LogInfoMessage(ServiceName, MODULE_NAME, "processMessage - SendNotification", null);
                        SendNotification(savemsg.ScheduledEmailEvent);
                        break;
                    default:
                        BLException blLog = new BLException("Unknown Notification Message type:" + savemsg.MessageType.ToString() + ", " + savemsg.ScheduledEmailEvent.ToString());
                        break;
                }
                RollingFileLogger.Instance.LeaveMethod(ServiceName, MODULE_NAME, "processBatchEvents", null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceName, MODULE_NAME, "processMessage - exception", ex, null);
                attemptResend(tran, msg, ex);
            }
        }

        private void SendNotification(ScheduledEvent scheduledEvent)
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceName, MODULE_NAME, "SendNotification", null);
                if (scheduledEvent == null) return;


                bool sent = ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendScheduledEmail(scheduledEvent.GroupID, scheduledEvent.MemberID, scheduledEvent.EmailScheduleID, scheduledEvent.ScheduleName, scheduledEvent.ScheduleGUID,scheduledEvent.EmailAddress, scheduledEvent.ScheduleDetails.Details);
                if (sent)
                    EmailNotifierBL.Instance.UpdateSchedule(scheduledEvent);
                RollingFileLogger.Instance.LeaveMethod(ServiceName, MODULE_NAME, "SendNotification", null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceName, MODULE_NAME, "SendNotification - exception", ex, null);
                throw new BLException("Error sending Notification in SendNotification:" + scheduledEvent.ToString(), ex);

            }

        }

     

        private void attemptResend(MessageQueueTransaction tran, IQueueMessage msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();

                    msg.Errors.Add(ex.Source + ", " + ex.Message);
                    msg.Attempts += 1;
                    msg.LastAttemptDateTime = DateTime.Now;

                    Enqueue(msg);
                }
            }
            catch (Exception e)
            {
                string message = "Exception while trying to resend msg: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                RollingFileLogger.Instance.LogException(ServiceName, MODULE_NAME, "attemptResend - " + message, ex, null);
                throw (new Exception(message));
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                    tran = null;
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                RollingFileLogger.Instance.LogException(ServiceName, MODULE_NAME, "attemptRollback - " + message, ex, null);
            }
        }

        private bool Validate(ScheduledEvent ev)
        {bool valid=true;
            try
            {
                if (ev == null)
                    return false;


                if (String.IsNullOrEmpty(ev.EmailAddress) || ev.ScheduleID < 0)
                    return false;

                if (ev.ScheduleName.ToLower() == "registration")
                {
                    valid = !EmailNotifierBL.Instance.IfMemberExists(ev);

                }

                return valid;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
    }
}
