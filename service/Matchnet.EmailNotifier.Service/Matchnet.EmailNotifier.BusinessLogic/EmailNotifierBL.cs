﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.Data;
using Matchnet.Member.ServiceAdapters;
using Spark.Logging;
using Spark.Common;
using Spark.Common.Adapter;
using Spark.Common.DiscountService;

namespace Matchnet.EmailNotifier.BusinessLogic
{
    public class EmailNotifierBL
    {
        public const string LOGICAL_DBNAME = "mnAlertSaveMM";
        public static EmailNotifierBL Instance = new EmailNotifierBL();
       public const string CLASS_NAME="EmailNotifierBL";
        private EmailNotifierBL(){}




        public DataTable GetScheduledBatch(int batchsize)
        {
            List<ScheduledEvent> scheduledEventsList = new List<ScheduledEvent>();
            try
            {
                Command comm = new Command();
                comm.LogicalDatabaseName = "mnAlertWrite";
                comm.StoredProcedureName = "up_EmailSchedule_GetScheduledBatch";
                comm.AddParameter("@batchsize", SqlDbType.Int, ParameterDirection.Input, batchsize);


                DataTable dt = Client.Instance.ExecuteDataTable(comm);
           

                return dt;

            }
            catch (Exception ex)
            {
                throw new Matchnet.Exceptions.BLException("Error retrieving schedule batch", ex);
            }

        }
        public ScheduledEvent PopulateEvent(DataRow rs)
        {
           try
            {
             
                    ScheduledEvent s = new ScheduledEvent();
                    s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                    s.EmailScheduleID = (int)(rs["emailscheduleid"] != Convert.DBNull ? rs["emailscheduleid"] : 0);
                    s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                    s.ScheduleGUID = (string)(rs["scheduleguid"] != Convert.DBNull ? rs["scheduleguid"] : "");
                    s.MemberID = (int)(rs["memberid"] != Convert.DBNull ? rs["memberid"] : 0);
                    s.EmailAddress = (string)(rs["emailaddress"] != Convert.DBNull ? rs["emailaddress"] : "");
                    s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                    s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                    s.NextAttemptDate = (DateTime)(rs["nextattemptdate"] != Convert.DBNull ? rs["nextattemptdate"] : new DateTime());
                    s.LastAttemptDate = (DateTime)(rs["lastattemptdate"] != Convert.DBNull ? rs["lastattemptdate"] : new DateTime());
                    s.SentCount = (int)(rs["sentcount"] != Convert.DBNull ? rs["sentcount"] : 0);
                    string details = (string)(rs["details"] != Convert.DBNull ? rs["details"] : "");

                    ScheduleDetail detail = new ScheduleDetail();
                    detail.FromJSonString(details);
                    s.ScheduleDetails = detail;
                    EmailSchedule emailschedule = EmailSchedulerBL.Instance.GetEmailSchedules().Find(s.ScheduleID,s.GroupID);
                    s.ScheduleName = emailschedule.ScheduleName;
                    s.InsertDate = (DateTime)(rs["insertdate"] != Convert.DBNull ? rs["insertdate"] : new DateTime());

                    return s;
            }
            catch (Exception ex)
            {
                throw new Matchnet.Exceptions.BLException("Error PopulateEvent from schedule batch", ex);
            }

        }


        public void DeleteSchedule(int emailScheduleID)
        {
            List<ScheduledEvent> scheduledEventsList = new List<ScheduledEvent>();
            try
            {
                Command comm = new Command();
                comm.LogicalDatabaseName = "mnAlert";
                comm.StoredProcedureName = "up_EmailSchedule_DeleteSchedule";
                comm.AddParameter("@emailscheduleid", SqlDbType.Int, ParameterDirection.Input, emailScheduleID);

                Client.Instance.ExecuteAsyncWrite(comm);


               

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME, "DeleteSchedule", ex, null);
                throw new Matchnet.Exceptions.BLException("Error retrieving schedule batch", ex);
              
            }

        }


        public void UpdateSchedule(ScheduledEvent scheduledEvent)
        {
   
            try
            {
                int scheduleid=scheduledEvent.ScheduleID;
                EmailSchedule schedule = EmailSchedulerBL.Instance.GetEmailSchedules().Find(scheduleid, scheduledEvent.GroupID);
                if (schedule == null)
                    return;
                scheduledEvent.SentCount += 1;
                if (scheduledEvent.SentCount < schedule.RepeatTimes + schedule.GraceRepeatTimes + 1)
                {
                    scheduledEvent.LastAttemptDate = DateTime.Now;
                    scheduledEvent.NextAttemptDate = EmailSchedulerBL.Instance.GetNextDate(schedule, false);

                }
                else
                {
                    DeleteSchedule(scheduledEvent.EmailScheduleID);
                    return;
                }
                Command comm = new Command();
                comm.LogicalDatabaseName = LOGICAL_DBNAME;
                comm.StoredProcedureName = "up_EmailSchedule_UpdateSchedule";
                comm.AddParameter("@EmailScheduleID", SqlDbType.Int, ParameterDirection.Input, scheduledEvent.EmailScheduleID);
                comm.AddParameter("@NextAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, scheduledEvent.NextAttemptDate);
                comm.AddParameter("@LastAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, scheduledEvent.LastAttemptDate);
                comm.AddParameter("@SentCount", SqlDbType.Int, ParameterDirection.Input, scheduledEvent.SentCount);
               

                Client.Instance.ExecuteAsyncWrite(comm);


            }
            catch (Exception ex)
            {//decide not to throw, if we throw we keep requeueing so we may sent tonns of emails,
                //if not throwing and failed to update the email will just stack in limbo, whateverb
               new Matchnet.Exceptions.BLException("Error in UpdateSchedule " + scheduledEvent.ToString(), ex);
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME, "UpdateSchedule", ex, scheduledEvent);
            }

        }


          public void UpdateScheduleForFailedEmail(ScheduledEvent scheduledEvent)
        {
            //with my authoritative decision  in case if externalmailsa returns fails
            //not to create race condition but reschedule attempt
            try
            {
                int scheduleid=scheduledEvent.ScheduleID;
                EmailSchedule schedule = EmailSchedulerBL.Instance.GetEmailSchedules().Find(scheduleid, scheduledEvent.GroupID);
                if (schedule == null)
                    return;
                
               
                scheduledEvent.LastAttemptDate = DateTime.Now;
                scheduledEvent.NextAttemptDate = EmailSchedulerBL.Instance.GetNextDate(schedule, scheduledEvent.SentCount==0);

                Command comm = new Command();
                comm.LogicalDatabaseName = LOGICAL_DBNAME;
                comm.StoredProcedureName = "up_EmailSchedule_UpdateSchedule";
                comm.AddParameter("@EmailScheduleID", SqlDbType.Int, ParameterDirection.Input, scheduledEvent.EmailScheduleID);
                comm.AddParameter("@NextAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, scheduledEvent.NextAttemptDate);
                comm.AddParameter("@LastAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, scheduledEvent.LastAttemptDate);
                comm.AddParameter("@SentCount", SqlDbType.Int, ParameterDirection.Input, scheduledEvent.SentCount);
               

                Client.Instance.ExecuteAsyncWrite(comm);


            }
            catch (Exception ex)
            {//decide not to throw, if we throw we keep requeueing so we may sent tonns of emails,
                //if not throwing and failed to update the email will just stack in limbo, whateverb
                new Matchnet.Exceptions.BLException("Error in UpdateScheduleForFailedEmail " + scheduledEvent.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME, "UpdateScheduleForFailedEmail", ex, scheduledEvent);
            }

        }
        public bool IfMemberExists(ScheduledEvent sEv)
        {
            try
            {
               int memberid= Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByEmail(sEv.EmailAddress);
               return memberid > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void SendNotification(ScheduledEvent scheduledEvent)
        {
            try
            {bool update=false;

            if (scheduledEvent == null)
            {
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification - scheduledEvent is null???",scheduledEvent);
                return;
            }
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification - enter", scheduledEvent);
                EmailSchedule schedule = EmailSchedulerBL.Instance.GetEmailSchedules().Find(scheduledEvent.ScheduleID, scheduledEvent.GroupID);
                if (schedule == null)
                    return;
                //repeat times + initial
                if (scheduledEvent.SentCount < schedule.RepeatTimes  + 1)
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification - got scheduledEvent, strying to send",scheduledEvent);
                    try
                    {
                        update = ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendScheduledEmail(scheduledEvent.GroupID, scheduledEvent.MemberID, scheduledEvent.EmailScheduleID, scheduledEvent.ScheduleName, scheduledEvent.ScheduleGUID, scheduledEvent.EmailAddress, scheduledEvent.ScheduleDetails.Details);
                    }
                    catch (Exception ex)
                    {
                        RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification", ex, scheduledEvent);
                        update = false;
                    }
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification - got scheduledEvent, sent returned:" + update.ToString(), scheduledEvent);
                  }
                else
                {//we give us some grace period before we delete scheduled event
                    update = true;
                }
                if (update)
                {
                    UpdateSchedule(scheduledEvent);
                    ProcessPostNotificationHandling(scheduledEvent);
                }
                else
                {
                    UpdateScheduleForFailedEmail(scheduledEvent);
                }

                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification");
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME, "SendNotification", ex, scheduledEvent);
               
                throw new Matchnet.Exceptions.BLException("Error sending Notification in SendNotification:" + scheduledEvent.ToString(), ex);

            }

        }

        private void ProcessPostNotificationHandling(ScheduledEvent scheduledEvent)
        {
            switch(scheduledEvent.ScheduleName.ToLower())
            {
                case "bogo":
                    int giftNotificationID = Convert.ToInt32(scheduledEvent.ScheduleDetails.Details["GiftNotificationID"].ToString());
                    UpdateGiftNotificationStatus(giftNotificationID);
                    break;
            }
        }

        private void UpdateGiftNotificationStatus(int giftNotificationID)
        {
            try
            {
                DiscountServiceAdapter adapter = new DiscountServiceAdapter();
                GiftResponse response = adapter.GetProxyInstanceForBedrock().UpdateGiftNotificationStatus(giftNotificationID,
                                                                                  GiftNotificationStatus.Sent,
                                                                                  Constants.NULL_INT);
                if(response.InternalResponse.responsecode !="0")
                {
                    throw new ApplicationException("GiftResponse invalid: " + response.InternalResponse.responsecode);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME, "UpdateGiftNotificationStatus",ex,giftNotificationID);
                throw new Matchnet.Exceptions.BLException("Error updating GiftNotification in UpdateGiftNotificationStatus:" + giftNotificationID.ToString(), ex);
            }
            
        }

        private bool Send(ScheduledEvent scheduledEvent)
        {
            try
            {
                return ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendScheduledEmail(scheduledEvent.GroupID, scheduledEvent.MemberID, scheduledEvent.EmailScheduleID, scheduledEvent.ScheduleName, scheduledEvent.ScheduleGUID, scheduledEvent.EmailAddress, scheduledEvent.ScheduleDetails.Details);
            }
            catch (Exception ex)
            {
                Matchnet.Exceptions.BLException bl = new Matchnet.Exceptions.BLException("Exception while sending scheduled email",ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST, CLASS_NAME,"Send", ex,scheduledEvent);
                return false;
            }
        }
    }
}
