﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Caching;
using Matchnet.Data;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.Exceptions;

using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.CacheSynchronization.Tracking;
using Matchnet.EmailNotifier.ValueObjects.QueueMessage;
using Spark.Logging;
namespace Matchnet.EmailNotifier.BusinessLogic
{
    public class EmailSchedulerBL
    {

    public const string CLASS_NAME = "EmailSchedulerBL";
    #region synchronization
        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        public event SynchronizationEventHandler SynchronizationRequested;

        public string ServiceName = "";
        private CacheItemRemovedCallback expireEmailSchedule;

        private void ExpireScheduledEventCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            //MemberRemoved();

            ScheduledEvent scheduledevent = value as ScheduledEvent;
            if (scheduledevent != null)
            {
                ReplicationRequested(scheduledevent);
                SynchronizationRequested(scheduledevent.GetCacheKey(),
                    scheduledevent.ReferenceTracker.Purge(Constants.NULL_STRING));
                // IncrementExpirationCounter();
            }
        }


     #endregion
    public const string LOGICAL_DBNAME="mnAlertSaveMM";

    private Matchnet.Caching.Cache _cache;
    public readonly static EmailSchedulerBL Instance = new EmailSchedulerBL();

    private EmailSchedulerBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
            expireEmailSchedule = new CacheItemRemovedCallback(ExpireScheduledEventCallback);
            RollingFileLogger.Instance.LogInfoMessage(ServiceName, CLASS_NAME, AppDomain.CurrentDomain.FriendlyName,null);
		}
    public EmailScheduleList GetEmailSchedules()
    {
        EmailScheduleList list = null;
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME,"GetEmailSchedules");
            list = (EmailScheduleList)_cache.Get(EmailScheduleList.CACHE_KEY_PREFIX);
            if (list != null)
            {
                RollingFileLogger.Instance.LogInfoMessage(ServiceName, CLASS_NAME, "GetEmailSchedules - got from cache, returning",null);
                return list;
            }


            Command comm = new Command();
            comm.LogicalDatabaseName = LOGICAL_DBNAME;
            comm.StoredProcedureName = "up_EmailSchedule_List";

            DataTable dt=  Client.Instance.ExecuteDataTable(comm);

            for (int i = 0; i < dt.Rows.Count;i++ )
            {
                DataRow rs = dt.Rows[i];
                EmailSchedule s = new EmailSchedule();
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.ScheduleName = (string)rs["schedulename"];
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.InitialInterval = (int)(rs["initialinterval"] != Convert.DBNull ? rs["initialinterval"] : 0);
                s.InitialIntervalType = (IntervalTypeEnum)(rs["intervaltypeid"] != Convert.DBNull ? rs["intervaltypeid"] : 0);
                s.RepeatTimes = (int)(rs["repeattimes"] != Convert.DBNull ? rs["repeattimes"] : 0);
                s.GraceRepeatTimes = (int)(rs["gracerepeattimes"] != Convert.DBNull ? rs["gracerepeattimes"] : 0);
                s.RepeatInterval = (int)(rs["repeatinterval"] != Convert.DBNull ? rs["repeatinterval"] : 0);
                s.RepeatIntervalType = (IntervalTypeEnum)(rs["repeatintervaltypeid"] != Convert.DBNull ? rs["repeatintervaltypeid"] : 0);
                if (list == null)
                    list = new EmailScheduleList();

                list.Add(s);
                RollingFileLogger.Instance.LogInfoMessage(ServiceName, CLASS_NAME, "GetEmailSchedules - Adding Schedule",s);

            }

            _cache.Insert(list);
          
            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "GetEmailSchedules",list);
            return list;


        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME,"GetEmailSchedules", ex,null);
            return null;
        }


    }

    public ScheduledEvent GetScheduledEvent(int groupid, string emailaddress, string schedulename)
    {
        ScheduledEvent s = null;
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "GetScheduledEvent", new string[] { groupid.ToString(), emailaddress, schedulename });



            Command comm = new Command();
            comm.LogicalDatabaseName = LOGICAL_DBNAME;
            comm.StoredProcedureName = "up_EmailScheduler_ListByEmail";
            EmailSchedule schedule = GetEmailSchedules().Find(schedulename, groupid);

            comm.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailaddress);
            comm.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupid);
            comm.AddParameter("@ScheduleID", SqlDbType.Int, ParameterDirection.Input, schedule.ScheduleID);

            DataTable dt = Client.Instance.ExecuteDataTable(comm);

            if (dt.Rows.Count > 0)
            {
                int i = 0;
                DataRow rs = dt.Rows[i];
                s = new ScheduledEvent();
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.ScheduleGUID = (string)(rs["scheduleguid"] != Convert.DBNull ? rs["scheduleguid"] : "");
                s.MemberID = (int)(rs["memberid"] != Convert.DBNull ? rs["memberid"] : 0);
                s.EmailAddress = (string)(rs["emailaddress"] != Convert.DBNull ? rs["emailaddress"] : "");
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.NextAttemptDate = (DateTime)(rs["nextattemptdate"] != Convert.DBNull ? rs["nextattemptdate"] : new DateTime());
                s.LastAttemptDate = (DateTime)(rs["lastattemptdate"] != Convert.DBNull ? rs["lastattemptdate"] : new DateTime());
                s.SentCount = (int)(rs["sentcount"] != Convert.DBNull ? rs["sentcount"] : 0);
                s.ScheduleName = schedulename;
                string details = (string)(rs["details"] != Convert.DBNull ? rs["details"] : "");

                ScheduleDetail detail = new ScheduleDetail();
                detail.FromJSonString(details);

                s.InsertDate = (DateTime)(rs["insertdate"] != Convert.DBNull ? rs["insertdate"] : new DateTime());

                InsertCache(s, true, true);

            }

            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "GetScheduledEvent", s);
            return s;

        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "GetScheduledEvent", ex, s);
            return null;
        }
    }

        
    public ScheduledEvent GetScheduledEvent(int groupID, int memberID, string schedulename)
    {
        ScheduledEvent s = null;
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "GetScheduledEvent", new string[] { groupID.ToString(), memberID.ToString(), schedulename });
          


            Command comm = new Command();
            comm.LogicalDatabaseName = LOGICAL_DBNAME;
            comm.StoredProcedureName = "up_EmailScheduler_ListByMemberID";
            EmailSchedule schedule = GetEmailSchedules().Find(schedulename, groupID);

            comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            comm.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
            comm.AddParameter("@ScheduleID", SqlDbType.Int, ParameterDirection.Input, schedule.ScheduleID);

            DataTable dt = Client.Instance.ExecuteDataTable(comm);
            
            if (dt.Rows.Count > 0)
            {
                int i = 0;
                DataRow rs = dt.Rows[i];
                s = new ScheduledEvent();
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.ScheduleGUID = (string)(rs["scheduleguid"] != Convert.DBNull ? rs["scheduleguid"] : "");
                s.MemberID = (int)(rs["memberid"] != Convert.DBNull ? rs["memberid"] : 0);
                s.EmailAddress = (string)(rs["emailaddress"] != Convert.DBNull ? rs["emailaddress"] : "");
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.NextAttemptDate = (DateTime)(rs["nextattemptdate"] != Convert.DBNull ? rs["nextattemptdate"] : new DateTime());
                s.LastAttemptDate = (DateTime)(rs["lastattemptdate"] != Convert.DBNull ? rs["lastattemptdate"] : new DateTime());
                s.SentCount = (int)(rs["sentcount"] != Convert.DBNull ? rs["sentcount"] : 0);
                s.ScheduleName = schedulename;
                string details = (string)(rs["details"] != Convert.DBNull ? rs["details"] : "");

                ScheduleDetail detail = new ScheduleDetail();
                detail.FromJSonString(details);

                s.InsertDate = (DateTime)(rs["insertdate"] != Convert.DBNull ? rs["insertdate"] : new DateTime());

                InsertCache(s, true, true);

            }
            
            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "GetScheduledEvent",s);
            return s;

        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "GetScheduledEvent", ex, s);
            return null;
        }
    }

    public ScheduledEvent GetScheduledEvent(String guid)
    {
        ScheduledEvent s = null;
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "GetScheduledEvent 2",new string[]{guid});
           
            s = (ScheduledEvent)_cache.Get(guid.ToUpper());
            if (s != null)
                return s;

            Command comm = new Command();
            comm.LogicalDatabaseName = LOGICAL_DBNAME;
            comm.StoredProcedureName = "up_EmailScheduler_List";
            comm.AddParameter("@ScheduleGuid", SqlDbType.VarChar, ParameterDirection.Input, guid);

            
            DataTable dt = Client.Instance.ExecuteDataTable(comm);
            if (dt.Rows.Count > 0)
            {
                int i = 0;
                DataRow rs = dt.Rows[i];
                s = new ScheduledEvent();
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.ScheduleGUID = (string)(rs["scheduleguid"] != Convert.DBNull ? rs["scheduleguid"] : "");
                s.MemberID = (int)(rs["memberid"] != Convert.DBNull ? rs["memberid"] : 0);
                s.EmailAddress = (string)(rs["emailaddress"] != Convert.DBNull ? rs["emailaddress"] : "");
                s.GroupID = (int)(rs["groupid"] != Convert.DBNull ? rs["groupid"] : 0);
                s.ScheduleID = (int)(rs["scheduleid"] != Convert.DBNull ? rs["scheduleid"] : 0);
                s.NextAttemptDate = (DateTime)(rs["nextattemptdate"] != Convert.DBNull ? rs["nextattemptdate"] : new DateTime());
                s.LastAttemptDate = (DateTime)(rs["lastattemptdate"] != Convert.DBNull ? rs["lastattemptdate"] : new DateTime());
                s.SentCount = (int)(rs["sentcount"] != Convert.DBNull ? rs["sentcount"] : 0);
                string details = (string)(rs["details"] != Convert.DBNull ? rs["details"] : "");

                ScheduleDetail detail = new ScheduleDetail();
                detail.FromJSonString(details);
                s.ScheduleDetails = detail;
                EmailSchedule emailschedule = GetEmailSchedules().Find(s.ScheduleID,s.GroupID);
                s.ScheduleName = emailschedule.ScheduleName;
                s.InsertDate = (DateTime)(rs["insertdate"] != Convert.DBNull ? rs["insertdate"] : new DateTime());
                InsertCache(s, true, true);

            }

            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "GetScheduledEvent 2",s);
            return s;

        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "GetScheduledEvent 2", ex, s);
            return null;
        }
    }

    public ScheduledEvent SaveScheduledEvent(ScheduledEvent scheduledevent)
    {
        
        try
           {
               RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "SaveScheduledEvent", new Object[] { scheduledevent });
            if (scheduledevent == null)
            {
                RollingFileLogger.Instance.LogWarningMessage(ServiceName, CLASS_NAME, "SaveScheduledEvent - null scheduled event param",null);
                return scheduledevent;
            }
            
            if (!ValidateEvent(scheduledevent))
                throw (new BLException("Not valid event:" + scheduledevent.ToString()));

           
            EmailSchedule schedule = GetEmailSchedules().Find(scheduledevent.ScheduleName, scheduledevent.GroupID);

            if (schedule == null)
                throw (new BLException("Schedule for event not found:" + scheduledevent.ToString()));

            scheduledevent.ScheduleID = schedule.ScheduleID;
              ScheduledEvent ev=null;
            //we dont know if we have it in database
              if (scheduledevent.EmailScheduleID <= 0)
              {
                  ev = GetScheduledEvent(scheduledevent.GroupID,scheduledevent.EmailAddress,scheduledevent.ScheduleName);
                  if (ev != null)
                  {
                      scheduledevent.ScheduleGUID = ev.ScheduleGUID;
                      scheduledevent.EmailScheduleID = ev.EmailScheduleID;
                  }
                  else
                  {
                      scheduledevent.EmailScheduleID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("EmailScheduleID");

                  }
                }
             
          
            Command comm = new Command();
            comm.LogicalDatabaseName = LOGICAL_DBNAME;
            comm.StoredProcedureName = "up_EmailSchedule_Save";

            comm.AddParameter("@EmailScheduleID", SqlDbType.Int, ParameterDirection.Input, scheduledevent.EmailScheduleID);
            comm.AddParameter("@ScheduleGUID", SqlDbType.VarChar, ParameterDirection.Input, scheduledevent.ScheduleGUID);
            comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, scheduledevent.MemberID);
            comm.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, scheduledevent.EmailAddress);
            comm.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, scheduledevent.GroupID);
            comm.AddParameter("@ScheduleID", SqlDbType.Int, ParameterDirection.Input, scheduledevent.ScheduleID);

            if (schedule.InitialIntervalType == IntervalTypeEnum.absolute)
            {
                if(scheduledevent.NextAttemptDate == DateTime.MinValue)
                {
                    //if interval is absolute than a specific time must be specified on the event, otherwise email will never be sent
                    throw new ApplicationException("Event interval type is absolute but no nextattemptdate specified");
                }
            }
            else
            {
                scheduledevent.NextAttemptDate = GetNextDate(schedule, true);    
            }
            

            comm.AddParameter("@NextAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, scheduledevent.NextAttemptDate);
            if(scheduledevent.ScheduleDetails != null)
                comm.AddParameter("@Details", SqlDbType.NText, ParameterDirection.Input,  scheduledevent.ScheduleDetails.ToJSonString());

            Client.Instance.ExecuteAsyncWrite(comm);
            InsertCache(scheduledevent, true, true);

            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "SaveScheduledEvent",  scheduledevent );
            return scheduledevent;
        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME,"SaveScheduledEvent", ex,scheduledevent);
            throw new BLException("Error saving email schedule: " + scheduledevent.ToString(),ex);
        }
    }

  
    public void DeleteScheduledEvent(string guid)
    {
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "DeleteScheduledEvent",new string[]{guid});
            if(String.IsNullOrEmpty(guid))
                return;
            Command comm = new Command();
            comm.LogicalDatabaseName = LOGICAL_DBNAME;
            comm.StoredProcedureName = "up_EmailSchedule_Delete";

            comm.AddParameter("@ScheduleGUID", SqlDbType.VarChar, ParameterDirection.Input, guid);
            Client.Instance.ExecuteAsyncWrite(comm);

            _cache.Remove(ScheduledEvent.GetCacheKey(guid));
            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "DeleteScheduledEvent");

        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "DeleteScheduledEvent", ex, guid);
            throw new BLException("atchnet.EmailNotifier.Service AddScheduledEvent exception: " + guid);
        }
    }

    public bool ValidateEvent(ScheduledEvent schedule)
    {

        if (schedule == null) return false;

        if(String.IsNullOrEmpty(schedule.EmailAddress)) return false;

        if(schedule.GroupID <=0) return false;

        return true;

    }

    public void  CreateSchedule(ScheduledEvent schedule)
    {
        RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "CreateSchedule", new Object[] { schedule });
        EmailSchedule emailschedule = GetEmailSchedules().Find(schedule.ScheduleName, schedule.GroupID);
        if (emailschedule == null)
            return;

        schedule.ScheduleID = schedule.ScheduleID;
        schedule.NextAttemptDate = GetNextDate(emailschedule, true);

        RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "CreateSchedule");
    }


    public DateTime GetNextDate(EmailSchedule schedule, bool initial)
    {
        DateTime nextdate;
        IntervalTypeEnum intervaltype;
        int interval;
        if (initial)
        {
            intervaltype = schedule.InitialIntervalType;
            interval = schedule.InitialInterval;
        }
        else
        {
            intervaltype = schedule.RepeatIntervalType;
            interval = schedule.RepeatInterval;
        }

        switch (intervaltype)
        {

            case (IntervalTypeEnum.minute):
                nextdate = DateTime.Now.AddMinutes(interval);
                break;
            
            case( IntervalTypeEnum.day):
                nextdate = DateTime.Now.AddDays(interval);
                break;
            
            case (IntervalTypeEnum.hour):
                nextdate = DateTime.Now.AddHours(interval);
                break;
            case (IntervalTypeEnum.month):
            default:
                nextdate = DateTime.Now.AddMonths(interval);
                break;

        }

        return nextdate;
    }


    public void InsertCache(ScheduledEvent scheduledevent, bool replicate, bool synchronize)
    {
       
        try
        {
            if (scheduledevent == null)
                return;

            _cache.Insert(scheduledevent);
            if (replicate && ReplicationRequested != null)
            {   
                ReplicationRequested(scheduledevent);
            }
            if (synchronize && SynchronizationRequested != null)
            {
                SynchronizationRequested(scheduledevent.GetCacheKey(),
                scheduledevent.ReferenceTracker.Purge(Constants.NULL_STRING));
            }
        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "InsertCache", ex,scheduledevent);
        }
    }


    public ScheduledEvent Enqueue(ScheduledEvent scheduledevent)
    {   
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "Enqueue", new Object[] { scheduledevent });
            if (scheduledevent == null) return null;

            if (scheduledevent.EmailScheduleID <= 0 || String.IsNullOrEmpty(scheduledevent.ScheduleGUID))
            {
                scheduledevent = SaveScheduledEvent(scheduledevent);
                return scheduledevent;
            }

            ScheduledEventMessage msg = new ScheduledEventMessage(scheduledevent);
            msg.MessageType = QueueMessageType.save;
            QueueProcessors.ScheduledProcessor.Enqueue(msg);
            InsertCache(scheduledevent, true, true);
            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "Enqueue", scheduledevent );
            return scheduledevent;

        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "Enqueue", ex, scheduledevent);
            throw new BLException("Error Enqueue email schedule: " + scheduledevent.ToString(),ex);
        }
    }

    public void Enqueue(string guid)
    {
        try
        {
            RollingFileLogger.Instance.EnterMethod(ServiceName, CLASS_NAME, "Enqueue", new string[] { guid });
            if (String.IsNullOrEmpty(guid)) return;

         
            ScheduledEventMessage msg = new ScheduledEventMessage(guid);
            msg.MessageType = QueueMessageType.delete;
            QueueProcessors.ScheduledProcessor.Enqueue(msg);
            _cache.Remove(ScheduledEvent.GetCacheKey(guid));
            RollingFileLogger.Instance.LeaveMethod(ServiceName, CLASS_NAME, "Enqueue");
            

        }
        catch (Exception ex)
        {
            RollingFileLogger.Instance.LogException(ServiceName, CLASS_NAME, "Enqueue", ex, guid);
            throw new BLException("Error Enqueue  delete email schedule: " + guid, ex);
        }
    }
    }
}

