﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using Matchnet;
using AjaxPro;
namespace Matchnet.EmailNotifier.ValueObjects
{

    [Serializable]
    [KnownType(typeof(ScheduleDetail))]
    public class ScheduleDetail
    {

        Hashtable _details = null;
        [DataMember]
        public Hashtable Details { get { return _details; } set { _details = value; } }

        public void Add(string key, string value)
        {
            try
            {
                if (_details == null)
                    _details = new Hashtable();


                _details[key] = value;
            }
            catch (Exception ex)
            { }


        }


        public string Get(string key)
        {
            try
            {
                if (_details != null)
                    return _details[key].ToString();
                else
                    return String.Empty;
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }
        public override string ToString()
        {
            StringBuilder bld = new StringBuilder();
            try
            {

         
                if (_details != null)
                {

                    foreach (string key in _details.Keys)
                    {
                        bld.Append(key + "=" + _details[key].ToString() + ";");

                    }

                }
              
                return bld.ToString();

            }
            catch (Exception ex)
            {
                if (bld != null)
                    return bld.ToString();
                else
                    return "Exception in ScheduledDetail.ToString()";
            }
        }


        public void FromJSonString(string jsonstr)
        {
            _details = (Hashtable)JavaScriptDeserializer.DeserializeFromJson(jsonstr, typeof(Hashtable));

        }


        public string  ToJSonString()
        {
           //_details = (Hashtable)JavaScriptDeserializer.DeserializeFromJson(jsonstr, typeof(Hashtable));
            StringBuilder bld=new StringBuilder();
            if(_details != null)
                JavaScriptSerializer.Serialize(_details, bld);

            return bld.ToString();

        }

    }
}
