﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using System.ServiceModel;
using Matchnet;
using Spark.WCFReplication;
using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.EmailNotifier.ValueObjects
{
    [Serializable]
    [KnownType(typeof(ScheduledEvent))]
    [KnownType(typeof(IReplicable))]
    [DataContract(Namespace = ServiceConstants.CONTRACT_NAMESPACE)]
    public class ScheduledEvent:IValueObject, ICacheable,IReplicable
    {

        private int _cacheTTLSeconds = 300;
        public const string CACHE_KEY_PREFIX = "ScheduledEvent";
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

        [DataMember(Name = "scheduleguid", EmitDefaultValue = false, Order = 1)]
        public String ScheduleGUID { get; set; }

        [DataMember(Name = "groupid", EmitDefaultValue = false, Order = 2)]
        public int GroupID { get; set; }


        [DataMember(Name = "schedulename", EmitDefaultValue = false, Order = 3)]
        public string ScheduleName { get; set; }


        [DataMember(Name = "emailaddress", EmitDefaultValue = false, Order = 4)]
        public String EmailAddress { get; set; }


        [DataMember(Name = "memberid", EmitDefaultValue = true, Order = 5)]
        public int MemberID { get; set; }


        [DataMember(Name = "scheduledetails", EmitDefaultValue = false, Order = 6)]
        public ScheduleDetail ScheduleDetails { get; set; }


        [DataMember(Name = "scheduleid", EmitDefaultValue = false, Order = 7)]
        public int ScheduleID { get; set; }

        [DataMember(Name = "emailscheduleid", EmitDefaultValue = false, Order = 7)]
        public int EmailScheduleID { get; set; }

        [DataMember(Name = "nextattemptdate", EmitDefaultValue = false, Order = 8)]
        public DateTime NextAttemptDate { get; set; }

        [DataMember(Name = "lastattemptdate", EmitDefaultValue = false, Order = 9)]
        public DateTime LastAttemptDate { get; set; }

        [DataMember(Name = "sentcount", EmitDefaultValue = false, Order = 10)]
        public int SentCount { get; set; }

        [DataMember(Name = "insertdate", EmitDefaultValue = false, Order = 11)]
        public DateTime InsertDate { get; set; }

        public override string ToString()
        { StringBuilder bld = new StringBuilder();
            try
            {
               
                bld.Append("ScheduleID: " + ScheduleID.ToString());
                bld.Append(";GroupID: " + GroupID.ToString());
                bld.Append(";ScheduleName: " + ScheduleName.ToString());
                bld.Append(";EmailAddress: " + EmailAddress.ToString());
                bld.Append(";MemberID: " + MemberID.ToString());
                bld.Append(";ScheduleGuid: " + ScheduleGUID.ToString());
                bld.Append(";NextAttemptDate: " + NextAttemptDate.ToString());
                bld.Append(";LastAttemptDate: " + LastAttemptDate.ToString());
                bld.Append(";SentCount: " + SentCount.ToString());
                bld.Append(";InsertDate: " + InsertDate.ToString());

                if (ScheduleDetails != null)
                {
                    bld.Append("\r\nScheduleDetails: " + ScheduleDetails.ToString() + "\r\n");
                   

                }

                return bld.ToString();

            }
            catch (Exception ex) 
            {
                if (bld != null)
                    return bld.ToString();
                else
                    return "Exception in ScheduledEvent.ToString()";
            }
        }
        #region ICacheable Members
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public static string GetCacheKey(string guid)
        {
            return String.Format("{0}_{1}",CACHE_KEY_PREFIX, guid).ToUpper();
        }

        public string GetCacheKey()
        {
            return String.Format("{0}_{1}", CACHE_KEY_PREFIX, ScheduleGUID).ToUpper();
        }
        #endregion
        #region IReplicable and ReferenceTracking Members
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }
}
