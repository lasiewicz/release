﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using Matchnet.EmailNotifier.ValueObjects;
namespace Matchnet.EmailNotifier.Service.ServiceContracts
{
    [ServiceContract(Namespace="spark.net")]
    public interface IEmailNotifierSvc
    {
       

        [OperationContract]
        ScheduledEvent SaveScheduledEvent(ScheduledEvent scheduledevent);

        [OperationContract]
        ScheduledEvent SaveScheduledEventAsync(ScheduledEvent scheduledevent);
        [OperationContract]
        ScheduledEvent GetScheduledEvent(int groupid, string emailaddress, string schedulename);

        [OperationContract]
        ScheduledEvent GetScheduledEventByMember(int groupID, int memberID, string schedulename);

        [OperationContract(Name = "GetScheduledEventByGuid")]
        ScheduledEvent GetScheduledEvent(string guid);
        [OperationContract]
        void DeleteScheduledEvent(string guid);
        [OperationContract]
        void DeleteScheduledEventAsync(string guid);
        
    }
}
