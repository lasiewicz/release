﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Matchnet;
using System.Reflection;
using Matchnet.EmailNotifier.ValueObjects;
using Spark.WCFReplication;
namespace Matchnet.EmailNotifier.Service.ServiceContracts
{
    [ServiceKnownType("GetReplicationKnownTypes", typeof(ReplicationTypeContainer))]
    [ServiceContract(Namespace = "http://spark")]
    public interface IEmailNotifierReplicationRecipient:IWCFReplicationRecipient
    {
      
    }

    public class ReplicationTypeContainer
    {
        public static IEnumerable<Type> GetReplicationKnownTypes(ICustomAttributeProvider p)
        {
            System.Collections.Generic.List<System.Type> dynamicyTypes =
            new System.Collections.Generic.List<System.Type>();


            // Here I'm adding my static types.
            // Usually I could generate the type on the fly and append it here.
            dynamicyTypes.Add(typeof(ScheduledEvent));

            return dynamicyTypes;
        }

    }
}