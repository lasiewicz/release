﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.EmailNotifier.ValueObjects
{
    public enum IntervalTypeEnum : int
    {
        minute=1,
        hour=2,
        day=3,
        month=4, 
        absolute=5

    }
    [Serializable]
    public class EmailSchedule:IValueObject
    {
        public int ScheduleID { get; set; }
        public string ScheduleName { get; set; }
        public int GroupID { get; set; }
        public int InitialInterval { get; set; }
        public IntervalTypeEnum InitialIntervalType { get; set; }
        public int RepeatTimes { get; set; }
        public int RepeatInterval { get; set; }
        public int GraceRepeatTimes { get; set; }
        public IntervalTypeEnum RepeatIntervalType { get; set; }


        public override string ToString()
        { StringBuilder bld = new StringBuilder();
            try
            {
               
                bld.Append(String.Format("ScheduleID={1},",ScheduleID));
                bld.Append(String.Format("ScheduleName={1},",ScheduleName));
                bld.Append(String.Format("GroupID={1},",GroupID));
                bld.Append(String.Format("InitialIntervalType={1},",InitialIntervalType));
                bld.Append(String.Format("RepeatTimes={1},", RepeatTimes));
                bld.Append(String.Format("RepeatInterval={1},", RepeatInterval));
                bld.Append(String.Format("RepeatIntervalType={1},", RepeatIntervalType));
                bld.Append(String.Format("GraceRepeatTimes={1},", GraceRepeatTimes));
                return base.ToString() + "," + bld.ToString();



            }
            catch (Exception ex)
            {
                return base.ToString() + "," + bld.ToString();
            }
        }

    }


    [Serializable]
    public class EmailScheduleList : IValueObject,ICacheable
    {
        private int _cacheTTLSeconds = 300;
        public const string CACHE_KEY_PREFIX = "EmailScheduleList";
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;

        string cacheKey=CACHE_KEY_PREFIX;

        List<EmailSchedule> _schedules = null;

        public void Add(EmailSchedule schedule)
        {
            try
            {
                if (_schedules == null)
                    _schedules = new List<EmailSchedule>();


                _schedules.Add(schedule);
            }
            catch (Exception EX)
            { }


        }

        public EmailSchedule Find(string name, int groupid)
        {

            EmailSchedule schedule = _schedules.Find(delegate(EmailSchedule s) { return s.ScheduleName.ToLower().Trim() == name.ToLower().Trim() && s.GroupID == groupid; });
            return schedule;
        }

        public EmailSchedule Find(int scheduleid,int groupid)
        {

            EmailSchedule schedule = _schedules.Find(delegate(EmailSchedule s) { return s.ScheduleID == scheduleid && s.GroupID == groupid; });
            return schedule;
        }
        #region ICacheable Members
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }


        public string GetCacheKey()
        {
            return CACHE_KEY_PREFIX;
        }
        #endregion
    }
}
