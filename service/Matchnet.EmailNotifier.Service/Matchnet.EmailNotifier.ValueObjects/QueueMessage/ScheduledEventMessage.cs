﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.EmailNotifier.ValueObjects.QueueMessage
{
    public enum QueueMessageType
    {
        save,
        delete,
        send
    }
    [Serializable]
    public class ScheduledEventMessage:IQueueMessage
    {
        ScheduledEvent _scheduledEvent = null;
        string _guid = null;

        public ScheduledEvent ScheduledEmailEvent { get { return _scheduledEvent; } set { _scheduledEvent = value; } }
        public String EventGuid { get { return _guid; } set { _guid = value; } }
        public ScheduledEventMessage(string guid)
        {
            _guid = guid;
            errors = new List<string>();
        }

        public ScheduledEventMessage(ScheduledEvent scheduledevent)
        {
            _scheduledEvent = scheduledevent;
            errors = new List<string>();
        }

        
        #region IQueueMember

        int attempts;
        DateTime lastAttemptDateTime;
        List<string> errors;
        QueueMessageType messageType;
        public int Attempts
        {
            get { return (attempts); }
            set { attempts = value; }
        }

        public DateTime LastAttemptDateTime
        {
            get { return (lastAttemptDateTime); }
            set { lastAttemptDateTime = value; }
        }

        public List<string> Errors
        {
            get { return (errors); }
            set { errors = value; }
        }

        public QueueMessageType MessageType
        {
            get { return (messageType); }
            set { messageType = value; }
        }
        #endregion


    }
}
