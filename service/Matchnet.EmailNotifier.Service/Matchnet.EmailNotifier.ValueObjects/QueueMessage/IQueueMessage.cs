﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.EmailNotifier.ValueObjects.QueueMessage
{
    public interface IQueueMessage
    {
        int Attempts { get; set; }
        DateTime LastAttemptDateTime { get; set; }
        List<string> Errors { get; set; }
        QueueMessageType MessageType { get; set; }
    }
}
