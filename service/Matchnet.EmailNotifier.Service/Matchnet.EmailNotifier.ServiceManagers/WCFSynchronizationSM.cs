﻿using System;
using System.Collections;
using System.Diagnostics;

using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.EmailNotifier.BusinessLogic;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.Service.ServiceContracts;

using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Replication;
using Spark.WCFReplication;
using System.Reflection;
using Spark.Logging;
namespace Matchnet.EmailNotifier.ServiceManagers
{
    public class WCFSynchronizationSM
    {
        private string CLASS_NAME = "WCFSynchronizationSM";
        public static readonly WCFSynchronizationSM Instance = new WCFSynchronizationSM();

        private WCFReplicator _replicator = null;
        private Synchronizer _synchronizer = null;

        private WCFSynchronizationSM()
        {

            try
            {

                EmailSchedulerBL.Instance.ServiceName = ServiceConstants.SERVICE_CONST_HTTP;
            }
            catch (Exception ex)
            {

            }
        }
        public void InitializeSynchronizationReplication()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication");
                _synchronizer = new Synchronizer("Matchnet.EmailNotifier.IISHost");
                _synchronizer.Start();
                EmailSchedulerBL.Instance.SynchronizationRequested += new EmailSchedulerBL.SynchronizationEventHandler(EmailSchedulerBL_SynchronizationRequested);
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - started synchronization",null);
                string replicationURI = "";

                try
                {
                   replicationURI= Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILNOTIFIER_IIS_SVC_REPLICATION_OVERRIDE");
                   RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - getting replicationURI override :" + replicationURI, null);
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - getting replicationURI override",ex, null);
                }
                string machineName = System.Environment.MachineName;
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - machinename :" + machineName, null);
                    replicationURI = GetReplicationPartner(machineName);
                    

                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - getting replicationURI:" + replicationURI, null);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - starting replicator to replicationURI:" + replicationURI, null);
                    replicationURI = replicationURI.ToLower().Replace("emailnotifiersvc.rem", "WCFReplicationRecipientSvc.svc");
                    _replicator = new WCFReplicator("WCFReplicationRecipientSvc",Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_CONST_HTTP, "emailnotifier_iis");
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                    
                    EmailSchedulerBL.Instance.ReplicationRequested += new EmailSchedulerBL.ReplicationEventHandler(EmailSchedulerBL_ReplicationRequested);

                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication - replicationURI is emptu", null);
                }

                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication");

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "InitializeSynchronizationReplication." ,ex,null);
                new ServiceBoundaryException("Matchnet.EmailNotifier.IISHost", "Unable to start synchronizer/replicator", ex);
            }
        }


        public void StopSynchronizationReplication()
        {
            try
            {

                if (_replicator != null)
                {
                    _replicator.Stop();
                }

                if (_synchronizer != null)
                {
                    _synchronizer.Stop();
                }


            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable ToString start synchronizer/replicator", ex);
            }
        }

        private string GetReplicationPartner(string machineName)
        {
            try
            {
                Matchnet.Configuration.ValueObjects.ServicePartitions partitions = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions();
                if (partitions == null || partitions.Count == 0)
                {
                    RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "GetReplicationPartner - no partitions found? MachineName=" + machineName, null);
                    return "";
                }
                else
                {
                    RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "GetReplicationPartner -  found partitions. MachineName=" + machineName, null);
                }

                string uri = partitions.GetReplicationPartnerUri(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_CONST_HTTP, "EmailNotifierSvc", machineName);
                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "GetReplicationPartner", uri);
                return uri;
            }
            catch (Exception ex)
            { RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONST_HTTP, CLASS_NAME, "GetReplicationPartner.", ex, null); return ""; }

        }

        #region synchronization/replication events

        private void EmailSchedulerBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicableObject);
        }


        private void EmailSchedulerBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if (_synchronizer != null)
                _synchronizer.Enqueue(key, cacheReferences);
        }
        #endregion
     
    }

  
}
