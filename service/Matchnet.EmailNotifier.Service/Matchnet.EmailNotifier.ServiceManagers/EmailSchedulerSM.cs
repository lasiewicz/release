﻿using System;
using System.Collections;
using System.Diagnostics;

using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.EmailNotifier.BusinessLogic;
using Matchnet.EmailNotifier.BusinessLogic.QueueProcessors;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.Service.ServiceContracts;

using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Replication;
using Spark.Logging;

namespace Matchnet.EmailNotifier.ServiceManagers
{
    public class EmailSchedulerSM : MarshalByRefObject, IEmailNotifierSvc, IServiceManager, IDisposable,IReplicationRecipient
    {
        private HydraWriter _hydraWriter = null;

        private PerformanceCounter _perfHitRateSec;
        private PerformanceCounter _perfScheduleRequestsSec;
        private PerformanceCounter _perfSaveScheduleRequestsSec;
        private PerformanceCounter _perfDeleteScheduleRequestsSec;

        private PerformanceCounter _perfCacheHitRate;
        private PerformanceCounter _perfCacheHitRate_Base;

        ScheduledProcessor _queueProcessor = null;
        EmailNotificationProcessor _queueNotifierProcessor = null;
        DistributorProcessor _distributorProcessor=null;
        private Replicator _replicator = null;
        private Synchronizer _synchronizer = null;
        public EmailSchedulerSM()
		{
		try{

           RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_CONST,"EmailSchedulerSM", "constructor");
			initPerfCounters();
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "Starting hydra",null);
			_hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnAlertSaveMM"});
			_hydraWriter.Start();
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "InitializeSynchronizationReplication", null);
            InitializeSynchronizationReplication();
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "Initialize ScheduledProcessor", null);
            _queueProcessor = new ScheduledProcessor();
            _queueProcessor.Start();

            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "Initialize EmailNotificationProcessor", null);
            _queueNotifierProcessor = new EmailNotificationProcessor();
            _queueNotifierProcessor.Start();
            EmailSchedulerBL.Instance.ServiceName = ServiceConstants.SERVICE_CONST;
            StartDistributor();
            RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "constructor");
           
			
		}
		catch(Exception ex)
		{
            System.Diagnostics.Trace.WriteLine("In EmailSchedulerSM exception block:" + ex.Message);
		}
        }


        public void InitializeSynchronizationReplication()
        {
            try
            {
                _synchronizer = new Synchronizer("Matchnet.EmailNotifier.Service");
                _synchronizer.Start();
                EmailSchedulerBL.Instance.SynchronizationRequested += new EmailSchedulerBL.SynchronizationEventHandler(EmailSchedulerBL_SynchronizationRequested);

                string replicationURI = ""; ;
                try
                {
                    replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAILNOTIFIER_SVC_REPLICATION_OVERRIDE");
                }
                catch (Exception ex)
                {

                }
                string machineName = "";
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", machineName);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                {
                    _replicator = new Replicator(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME);
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                    EmailSchedulerBL.Instance.ReplicationRequested += new EmailSchedulerBL.ReplicationEventHandler(EmailSchedulerBL_ReplicationRequested);
                   
                }
                else
                {
                    System.Diagnostics.Trace.WriteLine("Matchnet.EmailNotifier.EmailSchedulerSM -In InitializeSynchronizationReplication, replicationURI is empty");
                }


            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Unable to start synchronizer/replicator", ex);
            }
        }

        public void StartDistributor()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "StartDistributor");
                string queuepathes = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAIL_NOTIFICATION_QUEUES_LIST");

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "StartDistributor. Configured QueuePaths:" + queuepathes, null);
                string[] paths = queuepathes.Split(';');
                if (paths[0].ToLower() == Environment.MachineName.ToLower())
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "StartDistributor. Configured QueuePaths:" + queuepathes + ", main distributor:" + paths[0], null);
                    _distributorProcessor = new DistributorProcessor(paths);
                    _distributorProcessor.Start();

                }


                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_CONST, "EmailSchedulerSM", "StartDistributor",null);
            }
            catch (Exception ex)
            { new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Exception caught while startign DistributorProcessor", ex); }

        }
        #region synchronization/replication events

        private void EmailSchedulerBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicableObject);
        }


        private void EmailSchedulerBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if (_synchronizer != null)
                _synchronizer.Enqueue(key, cacheReferences);
        }
        #endregion

        #region IReplicationRecipient

        public void Receive(IReplicable replicableObject)
        {
            try
            {
                if (replicableObject != null)
                    EmailSchedulerBL.Instance.InsertCache((ScheduledEvent)replicableObject, false, false);
            }
            catch (Exception ex)
            {

            }
        }


        #endregion
        #region Service Implementation

        public ScheduledEvent SaveScheduledEvent(ScheduledEvent scheduledevent)
        {

          return  EmailSchedulerBL.Instance.SaveScheduledEvent(scheduledevent);
        }

        public ScheduledEvent SaveScheduledEventAsync(ScheduledEvent scheduledevent)
        {

            return EmailSchedulerBL.Instance.Enqueue(scheduledevent);
        }

        public ScheduledEvent GetScheduledEvent( int groupid, string emailaddress, string schedulename)
        {

            return EmailSchedulerBL.Instance.GetScheduledEvent(groupid, emailaddress, schedulename);
        }

        public ScheduledEvent GetScheduledEventByMember(int groupID, int memberID, string schedulename)
        {
            return EmailSchedulerBL.Instance.GetScheduledEvent(groupID, memberID, schedulename);
        }

        public ScheduledEvent GetScheduledEvent( string scheduleguid)
        {

            return EmailSchedulerBL.Instance.GetScheduledEvent(scheduleguid);
        }

        public void DeleteScheduledEvent(string guid)
        {

                EmailSchedulerBL.Instance.DeleteScheduledEvent(guid);
        }

        public void DeleteScheduledEventAsync(string guid)
        {

            EmailSchedulerBL.Instance.Enqueue(guid);
        }
        #endregion

        #region IServiceManager
        public void PrePopulateCache()
        {
        }
        public override object InitializeLifetimeService()
        { return null; }
        #endregion
        #region performance counters
        public static void PerfCounterInstall()
        {
            try
            {
                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());
                PerformanceCounterCategory.Create(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
               
            }
            catch (Exception ex)
            {; }
        }


        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME);
        }

        public static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] {	
				new CounterCreationData("Email Schedule Save Requests/second", "Email Schedule Save Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("Email Schedule Requests/second", "Email Schedule Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("PhotoStore Delete Requests/second", "PhotoStore Delete Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
				new CounterCreationData("Email Schedule Delete Requests/second", "Email Schedule Delete Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
			    new CounterCreationData("Email Schedule Cache Hit Rate", "Email Schedule Cache Hit Rate", PerformanceCounterType.RawFraction),
                new CounterCreationData("Email Schedule Cache Hit Rate_Base", "Email Schedule Cache Hit Rate_Base", PerformanceCounterType.RawBase)
				};

            //PerformanceCounterCategory.Create(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
        }


     
        private void initPerfCounters()
        {
            _perfSaveScheduleRequestsSec = new PerformanceCounter(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Email Schedule Save Requests/second", false);
            _perfScheduleRequestsSec = new PerformanceCounter(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Email Schedule Requests/second", false);
            _perfDeleteScheduleRequestsSec = new PerformanceCounter(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Email Schedule Delete Requests/second", false);

            _perfHitRateSec = new PerformanceCounter(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Email Schedule Hit Rate/second", false);
            _perfCacheHitRate = new PerformanceCounter(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Email Schedule Cache Hit Rate", false);
            _perfCacheHitRate_Base = new PerformanceCounter(Matchnet.EmailNotifier.ValueObjects.ServiceConstants.SERVICE_NAME, "Email Schedule Cache  Hit Rate_Base", false);

        }


        private void resetPerfCounters()
        {
            _perfSaveScheduleRequestsSec.RawValue = 0;
            _perfScheduleRequestsSec.RawValue = 0;
            _perfDeleteScheduleRequestsSec.RawValue = 0;
            
            _perfHitRateSec.RawValue = 0;


        }
        private void EmailSchedulerBL_ScheduleSaveRequested()
        {
            _perfSaveScheduleRequestsSec.Increment();
            _perfHitRateSec.Increment();
        }

        private void EmailSchedulerBL_ScheduleRequested(bool cacheHit)
        {
           
            _perfHitRateSec.Increment();
            _perfCacheHitRate_Base.Increment();
            if (cacheHit)
            {
                _perfCacheHitRate.Increment();
            }
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

            if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }
            if (_queueProcessor != null)
                _queueProcessor.Stop();

            if (_queueNotifierProcessor != null)
                _queueNotifierProcessor.Stop();


            if (_distributorProcessor != null)
                _distributorProcessor.Stop();
            resetPerfCounters();
        }
        #endregion
    }
}
