﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.EmailNotifier.ServiceAdapters;
using Matchnet.EmailNotifier.ValueObjects;
using System.Threading;
namespace Harness
{
    public partial class Form1 : Form
    {
        List<Thread> _threads;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            ScheduledEvent schedule = new ScheduledEvent();
            schedule.ScheduleGUID = Guid.NewGuid().ToString();
            
            schedule.ScheduleName = "registration";
            schedule.EmailAddress = "skener@spark.net";
            schedule.GroupID = 1003;
            schedule.ScheduleDetails = new ScheduleDetail();
            schedule.ScheduleDetails.Add("key1","data1");
            schedule.ScheduleDetails.Add("key2", "data2");
            schedule.ScheduleDetails.Add("key3", "data3");
            schedule.ScheduleDetails.Add("key4", "data4");
            

            WCFEmailNotifierSA.Instance.SaveScheduledEvent(schedule);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                _threads = new List<Thread>();
                int numthreads = Int32.Parse(textBox1.Text);
                for (int i = 0; i < numthreads; i++)
                {
                    Thread t = new Thread(AddEvent);
                    t.Start();



                }


            }
            catch (Exception ex)
            { }
        }

        private void AddEvent()
        {
            ScheduledEvent schedule = new ScheduledEvent();
            schedule.ScheduleGUID = new Guid().ToString();

            schedule.ScheduleName = "registrationcapture";
            schedule.EmailAddress = "skener@spark.net";
            for (int i = 0; i < 100; i++)
            {
               


                WCFEmailNotifierSA.Instance.SaveScheduledEvent(schedule);

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ScheduledEvent schedule = WCFEmailNotifierSA.Instance.GetScheduledEvent(txtGUID.Text);
                rtfEvent.Text = schedule.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
