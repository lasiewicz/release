﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.RemotingServices;
using Matchnet.Exceptions;
using Matchnet.EmailNotifier.ServiceManagers;
using Matchnet.EmailNotifier.ValueObjects;


namespace Matchnet.EmailNotifier.ServiceHost
{
    public partial class EmailNotifierService : Matchnet.RemotingServices.RemotingServiceBase
    {
     
        private EmailSchedulerSM _schedulerSM;
        public EmailNotifierService()
        {
            InitializeComponent();
        }

      

        protected override void RegisterServiceManagers()
        {
            try
            {
                Trace.Write("Matchnet.EmailNotifier.ServiceHost - RegisterServiceManagers");
                _schedulerSM = new EmailSchedulerSM();
                //base.RegisterServiceManager(_schedulerSM);
                Trace.Write("Matchnet.EmailNotifier.ServiceHost -Finished RegisterServiceManagers");
            }
            catch (Exception ex)
            {
                Trace.Write("Matchnet.EmailNotifier.ServiceHost - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }


        protected override void OnStart(string[] args)
        {
            try
            {
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
