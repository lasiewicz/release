﻿using System;
using Matchnet.EmailNotifier.ValueObjects;
namespace Matchnet.EmailNotifier.ServiceHost
{
    partial class EmailNotifierService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            try
            {
              
                components = new System.ComponentModel.Container();
               this.ServiceName ="Matchnet.EmailNotifier.Service";
                base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONST, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
            }
            catch (Exception ex)
            {
                throw new Matchnet.Exceptions.ServiceBoundaryException("Matchnet.EmailNotifier.Service","Exception while initializing service" , ex);
            }
        }

        #endregion
    }
}
