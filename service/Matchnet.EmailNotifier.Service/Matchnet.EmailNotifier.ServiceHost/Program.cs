﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.EmailNotifier.ValueObjects;
namespace Matchnet.EmailNotifier.ServiceHost
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new EmailNotifierService() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
         
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException("Matchnet.EmailNotifier.Service", "An unhandled exception occured.", e.ExceptionObject as Exception);
        }
    }
}
