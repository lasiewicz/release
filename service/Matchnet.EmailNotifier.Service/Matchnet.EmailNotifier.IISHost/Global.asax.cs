﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Matchnet.EmailNotifier.ServiceManagers;

namespace Matchnet.EmailNotifier.IISHost
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
           WCFSynchronizationSM.Instance.InitializeSynchronizationReplication();
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {
           
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
          
           
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
          
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.IISHost Application_Error");
        }

        protected void Session_End(object sender, EventArgs e)
        {
          
        }

        protected void Application_End(object sender, EventArgs e)
        {
            
            WCFSynchronizationSM.Instance.StopSynchronizationReplication();
        }
    }
}