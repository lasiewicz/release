﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Spark.WCFReplication;
using Matchnet.EmailNotifier.Service.ServiceContracts;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet.EmailNotifier.BusinessLogic;
using System.Reflection;
namespace Matchnet.EmailNotifier.Service
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    [ServiceKnownType("GetReplicationKnownTypes", typeof(ReplicationRecipientKnownTypes))]
    public class WCFReplicationRecipientSvc :IEmailNotifierReplicationRecipient
    {
        #region IWCFReplicationRecipient

        public void Receive(IReplicable replicableObject)
        {
            try
            {
                if (replicableObject != null)
                    EmailSchedulerBL.Instance.InsertCache((ScheduledEvent)replicableObject, false, false);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

    }
    public class ReplicationRecipientKnownTypes 
    {
        public static System.Collections.Generic.IEnumerable<Type> GetReplicationKnownTypes(ICustomAttributeProvider p)
        {
            System.Collections.Generic.List<System.Type> dynamicyTypes =
            new System.Collections.Generic.List<System.Type>();


            // Here I'm adding my static types.
            // Usually I could generate the type on the fly and append it here.
            dynamicyTypes.Add(typeof(Matchnet.EmailNotifier.ValueObjects.ScheduledEvent));

            return dynamicyTypes;
        }
    }
}
