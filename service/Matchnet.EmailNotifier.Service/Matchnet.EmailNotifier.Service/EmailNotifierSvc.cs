﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Matchnet.EmailNotifier.Service.ServiceContracts;
using Matchnet.EmailNotifier.ValueObjects;


using Matchnet.EmailNotifier.BusinessLogic;

namespace Matchnet.EmailNotifier.Service
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
     public class EmailNotifierSvc : IEmailNotifierSvc
     { 
        public ScheduledEvent SaveScheduledEvent(ScheduledEvent scheduledevent)
        {
            try
            {
                if (scheduledevent != null)
                {
                    return EmailSchedulerBL.Instance.SaveScheduledEvent(scheduledevent);
                    System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service AddScheduledEvent:" + scheduledevent.ToString());
                }
                else
                {
                    System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service AddScheduledEvent: received null");
                    return null;
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service AddScheduledEvent exception:" + ex.ToString());
                return null;
            }
        }


        public ScheduledEvent SaveScheduledEventAsync(ScheduledEvent scheduledevent)
        {
            try
            {
                if (scheduledevent != null)
                {
                    return EmailSchedulerBL.Instance.Enqueue(scheduledevent);
                    System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service AddScheduledEvent:" + scheduledevent.ToString());
                }
                else
                {
                    System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service AddScheduledEvent: received null");
                    return null;
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service AddScheduledEvent exception:" + ex.ToString());
                return null;
            }
        }


        public ScheduledEvent GetScheduledEvent( int groupid, string email, string schedulename)
        {
            try
            {
               return EmailSchedulerBL.Instance.GetScheduledEvent(groupid,email,schedulename);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service DeleteScheduledEvent exception:" + ex.ToString());
                return null;
            }
        }

        public ScheduledEvent GetScheduledEventByMember(int groupID, int memberID, string schedulename)
        {
            try
            {
                return EmailSchedulerBL.Instance.GetScheduledEvent(groupID, memberID, schedulename);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service DeleteScheduledEvent exception:" + ex.ToString());
                return null;
            }
        }


        public ScheduledEvent GetScheduledEvent(string guid)
        {
            try
            {
                return EmailSchedulerBL.Instance.GetScheduledEvent(guid);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service DeleteScheduledEvent exception:" + ex.ToString());
                return null;
            }
        }

        public void DeleteScheduledEvent(string  guid)
        {
            try
            {
                EmailSchedulerBL.Instance.DeleteScheduledEvent(guid);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.Write("Matchnet.EmailNotifier.Service DeleteScheduledEvent exception:" + ex.ToString());
            }
        }

        public void DeleteScheduledEventAsync(string guid)
        {

            EmailSchedulerBL.Instance.Enqueue(guid);
        }


      
    }
}
