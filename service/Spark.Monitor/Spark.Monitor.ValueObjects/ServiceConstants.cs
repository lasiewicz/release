using System;

namespace Spark.Monitor.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_NAME = "Spark.Monitor.Service";
		public const string NSCA_EXE_PATH = @"C:\NSCA\send_nsca.exe";
		public const string NSCA_DROP_PATH = @"C:\NSCA\Drop\";
		public const string NSCA_CONFIG_PATH = @"C:\NSCA\send_nsca.cfg";
		public const string IRONPORT_SERVER_PARTIAL_URL = @"matchnet.com:13171/xml/hoststatus?hostname=";
	}

	public enum NagiosStatuses
	{
		OK = 0,
		WARNING = 1,
		CRITICAL = 2,
		UNKNOWN = -1
	}
}
