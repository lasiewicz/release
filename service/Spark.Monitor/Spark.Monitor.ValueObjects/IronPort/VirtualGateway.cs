using System;

namespace Spark.Monitor.ValueObjects.IronPort
{
	/// <summary>
	/// XML node from ironpot
	/// </summary>
	public class VirtualGateway
	{
		private string name = String.Empty;
		private int activeRecipients;
		private string status = String.Empty;

		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		public int ActiveRecipients
		{
			get
			{
				return this.activeRecipients;
			}
			set
			{
				this.activeRecipients = value;
			}
		}

		public string Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}

		public VirtualGateway(string name, int activeRecipients, string status)
		{
			this.name = name;
			this.activeRecipients = activeRecipients;
			this.status = status;
		}
	}

	public class VirtualGateways : System.Collections.CollectionBase 
	{
		public VirtualGateways()
		{
		}

		public void Add(VirtualGateway virtualGateway)
		{
			base.InnerList.Add(virtualGateway);
		}

		public VirtualGateway this[int index]
		{
			get
			{
				return (VirtualGateway) base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}
	}
}
