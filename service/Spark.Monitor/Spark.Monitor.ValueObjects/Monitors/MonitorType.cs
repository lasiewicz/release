using System;

namespace Spark.Monitor.ValueObjects
{
	public enum MonitorType
	{
		IronPort,
		Unknown = 999
	}
}
