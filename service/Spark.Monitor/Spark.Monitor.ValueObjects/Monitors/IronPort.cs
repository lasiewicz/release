using System;
using System.Collections;

namespace Spark.Monitor.ValueObjects.Monitors
{
	public class IronPort
	{
		private Spark.Monitor.ValueObjects.IronPort.VirtualGateways virtualGateways =
			new Spark.Monitor.ValueObjects.IronPort.VirtualGateways();
		private string name = String.Empty;
		private System.Collections.ArrayList hosts = new ArrayList();

		/// <summary>
		/// Contains list of XML attributes with key, Status and values
		/// </summary>
		public Spark.Monitor.ValueObjects.IronPort.VirtualGateways VirtualGateways
		{
			get
			{
				return this.virtualGateways;
			}
			set
			{
				this.virtualGateways = value;
			}
		}


		/// <summary>
		/// Name of the Irontport server
		/// </summary>
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}
		public System.Collections.ArrayList Hosts
		{
			get
			{
				return this.hosts;
			}
			set
			{
				this.hosts = value;
			}
		}

		public string GetURL(string host)
		{
			return @"http://" + this.name + @"." + Spark.Monitor.ValueObjects.ServiceConstants.IRONPORT_SERVER_PARTIAL_URL
				+ host;
		}

		public IronPort(string name)
		{
			this.name = name;
		}
	}

	public class IronPorts : System.Collections.CollectionBase
	{
		public IronPorts()
		{
		}

		
		public void Add(IronPort ironPort)
		{
			base.InnerList.Add(ironPort);
		}

		public IronPort this[int index]
		{
			get
			{
				return (IronPort) base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}
	}
}
