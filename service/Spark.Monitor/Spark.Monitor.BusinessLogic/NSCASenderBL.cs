using System;
using System.IO;
using System.Text;

using System.Configuration;

namespace Spark.Monitor.BusinessLogic
{
	/// <summary>
	/// Sends properly formatted log files to Nagios using nsca_send.exe program on the local server 
	/// </summary>
	public class NSCASenderBL
	{
		public static readonly NSCASenderBL Instance = new NSCASenderBL();

		FileSystemWatcher watcher;

		private NSCASenderBL()
		{
			System.IO.Directory.Delete(Spark.Monitor.ValueObjects.ServiceConstants.NSCA_DROP_PATH, true);
			System.IO.Directory.CreateDirectory(Spark.Monitor.ValueObjects.ServiceConstants.NSCA_DROP_PATH);

			watcher = new FileSystemWatcher(Spark.Monitor.ValueObjects.ServiceConstants.NSCA_DROP_PATH, "*.*");

			watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite | NotifyFilters.CreationTime |
				NotifyFilters.Size;

			watcher.EnableRaisingEvents = true;
			watcher.IncludeSubdirectories = true;

			watcher.Created += new FileSystemEventHandler(watcher_Changed);
		}

		// New log file has been dropped into the direcotry
		private void watcher_Changed(object sender, FileSystemEventArgs e)
		{
			Send(e.FullPath);
		}

		private void Send(string fullPath)
		{
			FileInfo fileInfo = new FileInfo(fullPath);

			string nagiosServerIP = ConfigurationSettings.AppSettings.Get("NAGIOS_SERVER_IP");

			switch (fileInfo.Name.ToLower())
			{
				case "ironport_output.txt":
					System.Diagnostics.Process process = new System.Diagnostics.Process();
					process.StartInfo.FileName = "cmd.exe";
					process.StartInfo.Arguments = "/C " + Spark.Monitor.ValueObjects.ServiceConstants.NSCA_EXE_PATH +
						" -H " + nagiosServerIP +
						" -c \"" + Spark.Monitor.ValueObjects.ServiceConstants.NSCA_CONFIG_PATH + 
						"\" < \"" + fullPath + "\"";

					process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
					process.StartInfo.CreateNoWindow = true;
					process.StartInfo.UseShellExecute = false;
					process.StartInfo.RedirectStandardOutput = true;
					
					process.Start();

				    #if DEBUG
					System.Diagnostics.Debug.WriteLine(process.StartInfo.FileName + process.StartInfo.Arguments);

					StreamReader outputReader = process.StandardOutput;
					string output = outputReader.ReadToEnd();
					System.Diagnostics.Debug.WriteLine(output);
					outputReader.Close();
					#endif
					
					if (!process.WaitForExit(5000))
					{
						throw new Exception("Timeout! running sending log file to send_nsca.");
					}
					process.Close();
					process.Dispose();
					File.Delete(fullPath);
					break;
				default:
					throw new Exception("Invalid file name");
			}
		}

		public void Run()
		{
		}
	}
}
