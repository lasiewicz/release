using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Xml;
using System.Threading;

using System.Configuration;

using Spark.Monitor.ValueObjects;

namespace Spark.Monitor.BusinessLogic
{
	public class MonitorBL
	{
		public static readonly MonitorBL Instance = new MonitorBL();
		private bool runnable = true;
		string appPath = String.Empty;

		public bool Runnable
		{
			get
			{
				return this.runnable;
			}
			set
			{
				this.runnable = value;
			}
		}

		private MonitorBL()
		{
			// Get exact apppath. System.Environment returns 
			System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
			System.IO.FileInfo fileInfo = new System.IO.FileInfo(assembly.Location);
			appPath = fileInfo.Directory.ToString();

			Thread senderThread = new Thread(new ThreadStart(NSCASenderBL.Instance.Run));
			senderThread.Name = "NSCASenderBLThread";
			senderThread.Start();
		}

		public void Run()
		{
			try
			{
				while (runnable)
				{
					TestIronPorts();

					Thread.Sleep(System.Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CHECK_INTERVAL_SECONDS")) * 1000);
				}
			}
			catch(Exception ex)
			{
				#if DEBUG
				throw ex;
				#else
				System.Diagnostics.EventLog.WriteEntry(Spark.Monitor.ValueObjects.ServiceConstants.SERVICE_NAME,
					ex.ToString(), System.Diagnostics.EventLogEntryType.Error);

				RecoverFromException();

				Thread.Sleep(System.Convert.ToInt32(ConfigurationSettings.AppSettings.Get("CHECK_INTERVAL_SECONDS")) * 1000);
				#endif
			}
		}

		private void RecoverFromException()
		{
			// Delete any junked files
			File.Delete(appPath + @"\IronPort_Output.txt");
			File.Delete(Spark.Monitor.ValueObjects.ServiceConstants.NSCA_DROP_PATH + @"\IronPort_Output.txt");
		}

		private void TestIronPorts()
		{
			string errorMessage = String.Empty;

			#region Read in hosts for each ironport server
			// Each line - First Col contains name of Ironport server, rest of them are sites to be passed into the xml querystring
			TextReader textReader = new StreamReader(appPath + @"\IronPort.txt");
			
			string line = String.Empty;

			Monitor.ValueObjects.Monitors.IronPorts ironPorts = new Spark.Monitor.ValueObjects.Monitors.IronPorts();

			while ((line = textReader.ReadLine()) != null)
			{
				string[] items = line.Split(new char[] {'\t'});

				string host = items[0];

				Monitor.ValueObjects.Monitors.IronPort ironPort = new Spark.Monitor.ValueObjects.Monitors.IronPort(host);

				for (int i = 1; i < items.Length; i++)
				{
					ironPort.Hosts.Add(items[i]);
				}

				ironPorts.Add(ironPort);
			}

			textReader.Close();
			#endregion

			#region Test each Ironport monitor
			foreach (Spark.Monitor.ValueObjects.Monitors.IronPort ironPort in ironPorts)
			{
				foreach (object host in ironPort.Hosts)
				{	
					try
					{
						XmlTextReader reader = new XmlTextReader(ironPort.GetURL((string)host));

						while (reader.Read())
						{
							switch (reader.NodeType)
							{
								case XmlNodeType.Element:
									if (reader.Name == "virtual_gateway")
									{
										Spark.Monitor.ValueObjects.IronPort.VirtualGateway gateWay = 
											new Spark.Monitor.ValueObjects.IronPort.VirtualGateway(
											reader.GetAttribute("name"),
											System.Convert.ToInt32(reader.GetAttribute("active_recips")),
											reader.GetAttribute("status"));

										ironPort.VirtualGateways.Add(gateWay);
									}
									break;
								default:
									break;
							}
						}

						reader.Close();
					}
					catch(Exception ex)
					{
						errorMessage = "Error connecting to ironport:" + ironPort.GetURL((string)host) + ex.ToString();
					}
				}
				
			}
			#endregion

			#region Write to file
			File.Delete(appPath + @"\IronPort_Output.txt");

			StreamWriter writer = new StreamWriter(appPath + @"\IronPort_Output.txt");

			StringBuilder logLine = new StringBuilder();
			StringBuilder description = new StringBuilder();

			foreach (Monitor.ValueObjects.Monitors.IronPort ironPort in ironPorts)
			{
				bool hasError = false;

				logLine = new StringBuilder();
				description = new StringBuilder(); // only used when there's down status

				if (errorMessage == String.Empty)
				{
					logLine.Append(ironPort.Name + "\t" + 
						ConfigurationSettings.AppSettings.Get("NAGIOS_SERVICE_NAME") + "\t");
						

					foreach (Spark.Monitor.ValueObjects.IronPort.VirtualGateway virtualGateway in ironPort.VirtualGateways)
					{
						#if DEBUG
						System.Diagnostics.Debug.WriteLine(virtualGateway.Name + ":" + virtualGateway.Status);
						#endif

						// We only care about down status in the log file
						if (virtualGateway.Status.ToLower() == "down")
						{
							hasError = true;
							description.Append("[" + virtualGateway.Name + ", AR:" + virtualGateway.ActiveRecipients + "] ");
						}
					}

					if (!hasError)
					{
						string hosts = String.Empty;

						foreach(object host in ironPort.Hosts)
						{
							hosts += ((string) host) + ", ";
						}

						logLine.Append((int) Spark.Monitor.ValueObjects.NagiosStatuses.OK + "\t");
						logLine.Append(hosts);
						logLine.Append(" Check is NORMAL");
					}
					else
					{
						logLine.Append((int) Spark.Monitor.ValueObjects.NagiosStatuses.CRITICAL + "\t");
						logLine.Append(description.ToString());
					}

					writer.WriteLine(logLine.ToString());
				}
				else
				{
					logLine.Append(ironPort.Name + "\t" + 
						ConfigurationSettings.AppSettings.Get("NAGIOS_SERVICE_NAME") + "\t" +
						(int) Spark.Monitor.ValueObjects.NagiosStatuses.UNKNOWN + "\t" + errorMessage);

					writer.WriteLine(logLine.ToString());
				}
			}

			writer.Close();

			File.Delete(Spark.Monitor.ValueObjects.ServiceConstants.NSCA_DROP_PATH 
				+ @"\IronPort_Output.txt");

			File.Move(appPath + @"\IronPort_Output.txt", Spark.Monitor.ValueObjects.ServiceConstants.NSCA_DROP_PATH 
				+ @"\IronPort_Output.txt");
			#endregion
		}
	}
}
