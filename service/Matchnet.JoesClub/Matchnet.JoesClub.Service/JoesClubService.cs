using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.JoesClub.ServiceManagers;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.BusinessLogic;


namespace Matchnet.JoesClub
{
	public class JoesClubService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
        private PageHandlerSM _pageHandlerSM;
        private HostSM _hostSM;
        private VenueSM _venueSM;
        private MemberSM _memberSM;

		public JoesClubService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new JoesClubService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);

            // start CronThread
            CronThread.singleton.start();
		}


		protected override void RegisterServiceManagers()
		{
			try
			{
                _pageHandlerSM = new PageHandlerSM();
                base.RegisterServiceManager(_pageHandlerSM);
                
                _hostSM = new HostSM();
                base.RegisterServiceManager(_hostSM);

                _venueSM = new VenueSM();
                base.RegisterServiceManager(_venueSM);

                _memberSM = new MemberSM();
                base.RegisterServiceManager(_memberSM);
            }
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
			}
		}

	
		protected override void Dispose( bool disposing )
		{
			if(disposing)
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}
	}
}
