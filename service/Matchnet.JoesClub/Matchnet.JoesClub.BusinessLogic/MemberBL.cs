using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;

using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberBL
{
    private static Log _log = new Log("MemberBL");

    public static MemberBL Instance = new MemberBL();

    
    private MemberBL()
    {
    }


    /**
     * Load the Member
     */
    public MemberVO load(int id)
    {
        Member member = (Member) MemberMgr.singleton.load(id);
        if (member == null)
            return null;

        MemberVO memberVO = VOConvert.convert(member);

        return memberVO;
    }


    /**
     * delete the Member
     */
    public void delete(int id)
    {
        MemberMgr.singleton.delete(id);
    }


    /**
     * save the Member
     */
    public int save(MemberVO memberVO)
    {
        Member member = VOConvert.convert(memberVO);
        member.save();
        memberVO.id = member.id;
        return member.id;
    }


    /**
     * Gets a dataset of all the members
     */
    public DataSet getDataSet()
    {
        return DbUtils.getDataSet("GetAllMembers", "members", null);
    }


    /**
     * Gets a dataset of all the members with the given name
     */
	public DataSet getDataSetByName(string name)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createStringParam("@name", "%" + name + "%") };

        return DbUtils.getDataSet("GetMembersByName", "members", sqlParameters);
    }


    /**
     * Gets a dataset of all the members with the given email
     */
	public DataSet getDataSetByEmail(string email)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createStringParam("@email", "%" + email + "%") };

        return DbUtils.getDataSet("GetMembersByEmail", "members", sqlParameters);
    }


    /**
     * return ArrayList[PhoneInterviewVO]
     */
    public ArrayList loadPhoneInterviewsForMember(int memberId)
    {
        Member member = (Member) MemberMgr.singleton.load(memberId);

        ArrayList phoneInterviewVOs = new ArrayList();
        foreach (PhoneInterview pi in member.phoneInterviews)
            phoneInterviewVOs.Add(VOConvert.convert(pi));

        return phoneInterviewVOs;
    }


    /**
     * return ArrayList[MemberVO]
     */
    public ArrayList loadAll()
    {
        ArrayList members = MemberMgr.singleton.loadAll();
        ArrayList memberVOs = new ArrayList();
        foreach (Member member in members)
        {
            MemberVO memberVO = VOConvert.convert(member);
            memberVOs.Add(memberVO);
        }

        return memberVOs;
    }


    /**
     * return ArrayList[BlockedMemberVO]
     */
    public ArrayList getBlockedMembers(int memberId)
    {
        return BlockedMembers.loadBlockedMemberVOs(memberId);
    }


    /**
     * blockedMemberIds = ArrayList[int]
     */
    public void saveBlockedMembers(int memberId, ArrayList blockedMemberIds)
    {
        BlockedMembers bms = new BlockedMembers(memberId);
        bms.setBlockedMembers(blockedMemberIds);
        bms.save();
    }


    public int numDinAttend(MemberVO member)
    {
        return VOConvert.convert(member).numDinAttend;
    }


    public double getPercentFulfillment(MemberVO member)
    {
        return VOConvert.convert(member).getPercentFulfillment();
    }


    public EventVO getLastEventAttended(MemberVO member)
    {
        Event ev = VOConvert.convert(member).getLastEventAttended();
        return VOConvert.convert(ev);
    }


    public int getNumNoShows(MemberVO member)
    {
        return VOConvert.convert(member).getNumNoShows();
    }


    public ArrayList getVenuesAttended(MemberVO member)
    {
        ArrayList venues = VOConvert.convert(member).getVenuesAttended();
        return VOConvert.convert(venues); // venues --> venueVOs
    }

    public bool sparkIdAlreadyUsed(MemberVO member, int sparkId)
    {
        return VOConvert.convert(member).sparkIdAlreadyUsed(sparkId);
    }


    public bool emailAddressAlreadyUsed(MemberVO member, string emailaddr)
    {
        return VOConvert.convert(member).emailAddressAlreadyUsed(emailaddr);
    }

}
}
