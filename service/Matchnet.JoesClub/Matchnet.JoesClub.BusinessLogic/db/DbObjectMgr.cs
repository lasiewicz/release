using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the persistence for a DbObject class
 */  
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class DbObjectMgr
{
    private static Log _log = new Log("DbObjectMgr");

    #region abstract and default methods
    /**
     * return the id column name for the table
     */
    public abstract string getIdColumn();


    /**
     * load the object at the reader's current row
     */
    public virtual DbObject load(SqlDataReader reader, object owner)
    {
        return load(reader);
    }


    /**
     * load the object at the reader's current row
     */
    public virtual DbObject load(SqlDataReader reader)
    {
        throw new MethodNotSupportedException("load()");
    }


    /**
     * Return the db table for the object
     */
    public abstract string getTableName();


    /**
     * Create an object of the correct type containing just the id.
     */
    public virtual DbObject createEmptyDbObject(int id)
    {
        throw new NotSupportedException();
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public virtual void insert(SqlConnection conn, DbObject dbObject)
    {
        throw new NotSupportedException();
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public virtual void update(SqlConnection conn, DbObject dbObject)
    {
        throw new NotSupportedException();
    }


    /**
     * Called after DbObjectMgr loads the object.  Subclasses can override
     * this to do more work such as loading associated objects. 
     */
    public virtual void afterLoad(DbObject dbObject) {}

    
    /**
     * Get the name of the "Load" stored proc
     */
    protected virtual string getLoadStoredProcName()
    {
        return "Get" + getTableName();
    }

    
    /**
     * Get the name of the "LoadAll" stored proc
     */
    protected virtual string getLoadAllStoredProcName()
    {
        return "GetAll" + getTableName() + "s";
    }
    #endregion


    /**
     * Delete the object
     */
    public virtual void delete(int id)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@" + getIdColumn(), id) };
        DbUtils.runStoredProc("Delete" + getTableName(), sqlParameters);
    }

    
    /**
     * load instances of the object using the given where clause
     * ArrayList[DbObject]
     */
    public virtual DbObject load(int id)
    {
        // try the cache
        DbObject dbObject = DbObjectCache.get(this, id);
        if (dbObject != null)
            return dbObject;

        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@" + getIdColumn(), id) };

        ArrayList dbObjects = load(getLoadStoredProcName(), sqlParameters);

        if (dbObjects.Count == 0)
            return null;
        else
            return (DbObject) dbObjects[0];
    }
    

    /**
     * load all instances of the object
     */
    public virtual ArrayList loadAll()
    {
        return load(getLoadAllStoredProcName(), null);
    }

    
    /**
     * load instances of the object using the given where clause
     * ArrayList[DbObject]
     */
    public ArrayList load(string storedProc, SqlParameter[] sqlParameters)
    {
        return load(storedProc, sqlParameters, null);
    }

    
    /**
     * load instances of the object using the given where clause
     * ArrayList[DbObject]
     */
    public ArrayList load(string storedProc, SqlParameter[] sqlParameters,
        object owner)
    {
        return load(storedProc, sqlParameters, owner, 0);
    }

    
    /**
     * load instances of the object using the given where clause
     * ArrayList[DbObject]
     */
    private ArrayList load(string storedProc, SqlParameter[] sqlParameters,
        object owner, int tries)
    {
        lock (this)
            {
            SqlConnection conn = null;
            SqlDataReader reader = null;
            SqlCommand command = null;

            try
            {
                conn = DbUtils.getConnection();
                command = new SqlCommand(storedProc, conn);
                command.CommandType = CommandType.StoredProcedure;

                // add params
                if (sqlParameters != null)
                    foreach (SqlParameter sqlParameter in sqlParameters)
                        command.Parameters.Add(sqlParameter);

                // execute   
                reader = command.ExecuteReader();

                ArrayList dbObjects = new ArrayList();
                Hashtable dbObjectsInCache = new Hashtable(); // Hashtable[DbObject-->""]
                while (reader.Read())
                {
                    // we don't want multiple instances of the same object 
                    // floating around, so try the cache
                    int id = DbUtils.getIdFromReader(reader, getIdColumn());
                    DbObject dbObject = DbObjectCache.get(this, id);
                    if (dbObject == null)
                    {
                        dbObject = load(reader, owner);
                        DbObjectCache.add(dbObject);
                    }
                    else
                        dbObjectsInCache[dbObject] = "";

                    dbObjects.Add(dbObject);
                }

                // call afterLoad for each DbObject in case the subclass
                // needs to do more work such as loading associated objects.
                // Close reader first because you can't have >1 reader open.
                reader.Close();
                reader = null;
                foreach (DbObject dbObject in dbObjects)
                    if (!dbObjectsInCache.ContainsKey(dbObject))
                        afterLoad(dbObject);

                return dbObjects;
            }

            // if we get a ConcurrencyException, try again
            catch (ConcurrencyException ce)
            {
                _log.error(ce);
                if (tries > 3)
                    throw ce;

                DbUtils.cleanUp(ref conn, ref command, ref reader);
                return load(storedProc, sqlParameters, owner, ++tries);
            }

            finally
            {
                DbUtils.cleanUp(ref conn, ref command, ref reader);
            }
        }
    }


    /**
     * select count(1) using the given where clause and params
     */
    public int getCountWhere(string storedProc, SqlParameter[] sqlParameters)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, storedProc);

            // add params
            if (sqlParameters != null)
                for (int i = 0; i < sqlParameters.Length; i++)
                    command.Parameters.Add(sqlParameters[i]);
    
            reader = command.ExecuteReader();
            reader.Read();
            return (int) reader[0];
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * save the object
     */
    public virtual void save(DbObject dbObject)
    {
        lock (this)
        {
            SqlConnection conn = null;

            try
            {
                conn = DbUtils.getConnection();
                // create new db object
                if (dbObject.id == DbConstants.NoId)
                    insert(conn, dbObject);

                // update existing db object
                else
                    update(conn, dbObject);
            }

            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }
    }
}

}