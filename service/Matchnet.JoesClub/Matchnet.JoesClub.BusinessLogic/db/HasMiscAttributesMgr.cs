using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the HasMiscAttributes table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
abstract public class HasMiscAttributesMgr : DbObjectMgr
{
    private static Log _log = new Log("HasMiscAttributesMgr");


    /**
     * Save the DbObject and the MiscAttributes
     */
    public override void save(DbObject dbObject)
    {
        base.save(dbObject);
        saveMiscAttributes((HasMiscAttributes) dbObject);
    }


    /**
     * save the MiscAttributes
     */
    protected void saveMiscAttributes(HasMiscAttributes hma)
    {
        foreach (MiscAttribute miscAttribute in hma.miscAttributes.Values)
            miscAttribute.save();
    }


    /**
     * Called after DbObjectMgr loads the object.  
     */
    public override void afterLoad(DbObject dbObject)
    {
        loadMiscAttributes((HasMiscAttributes) dbObject);
    }


    /**
     * Load the MiscAttributes
     */
    protected void loadMiscAttributes(HasMiscAttributes hma)
    {
        ArrayList aMiscAttributes = MiscAttributeMgr.singleton.load(hma);
        Hashtable hMiscAttributes = new Hashtable();
        foreach (MiscAttribute miscAttribute in aMiscAttributes)
            hMiscAttributes[miscAttribute.name] = miscAttribute;
        hma.miscAttributes = hMiscAttributes;
    }


    /**
     * Delete the object
     */
    public override void delete(int id)
    {
        // delete the MiscAttributes
        MiscAttributeMgr.singleton.delete(id, getTableName());

        base.delete(id);
    }
}

}