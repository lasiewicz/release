using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;

namespace Matchnet.JoesClub.BusinessLogic
{
public class MiscAttribute : DbObject
{
    private static Log _log = new Log("MiscAttribute");

    /**
    * ValueType enum
    */
    public enum ValueType : int
    {
	    String   = 1,
	    Int      = 2,
	    Bool     = 3,
	    DateTime = 4,
	    Double   = 5
    }

    private string _name;
    private HasMiscAttributes _owner;
    private ValueType _valueType;
    private string _stringValue;
    private int _intValue;
    private bool _boolValue;
    private DateTime _dateTimeValue;
    private double _doubleValue;
    private bool _changed;

    public string name { get { return _name; }}
    public HasMiscAttributes owner { get { return _owner; }}
    public ValueType valueType { get { return _valueType; }}
    public string stringValue { get { return _stringValue; }}
    public int intValue { get { return _intValue; }}
    public bool boolValue { get { return _boolValue; }}
    public DateTime dateTimeValue { get { return _dateTimeValue; }}
    public double doubleValue { get { return _doubleValue; }}
    public bool changed { get { return _changed; }}


    /**
     * Constructor called when loading from db
     */
    public MiscAttribute(
        int id,
        string name,
        HasMiscAttributes owner,
        ValueType valueType,
        string stringValue,
        int intValue,
        bool boolValue,
        DateTime dateTimeValue,
        double doubleValue) : base(id)
    {
        _name = name;
        _owner = owner;
        _valueType = valueType;
        _stringValue = stringValue;
        _intValue = intValue;
        _boolValue = boolValue;
        _dateTimeValue = dateTimeValue;
        _doubleValue = doubleValue;
        _changed = false;
    }


    /**
     * Constructor for in-memory String MiscAttributes
     */
    public MiscAttribute(
        string name,
        HasMiscAttributes owner,
        string stringValue) : this(name, owner, ValueType.String)
    {
        _stringValue = stringValue;
    }


    /**
     * Constructor for in-memory Int MiscAttributes
     */
    public MiscAttribute(
        string name,
        HasMiscAttributes owner,
        int intValue) : this(name, owner, ValueType.Int)
    {
        _intValue = intValue;
    }


    /**
     * Constructor for in-memory Bool MiscAttributes
     */
    public MiscAttribute(
        string name,
        HasMiscAttributes owner,
        bool boolValue) : this(name, owner, ValueType.Bool)
    {
        _boolValue = boolValue;
    }


    /**
     * Constructor for in-memory DateTime MiscAttributes
     */
    public MiscAttribute(
        string name,
        HasMiscAttributes owner,
        DateTime dateTimeValue) : this(name, owner, ValueType.DateTime)
    {
        _dateTimeValue = dateTimeValue;
    }


    /**
     * Constructor for in-memory DateTime MiscAttributes
     */
    public MiscAttribute(
        string name,
        HasMiscAttributes owner,
        double doubleValue) : this(name, owner, ValueType.Double)
    {
        _doubleValue = doubleValue;
    }


    /**
     * Constructor for in-memory MiscAttributes
     */
    private MiscAttribute(
        string name,
        HasMiscAttributes owner,
        ValueType valueType) : base(DbConstants.NoId)
    {
        _name = name;
        _owner = owner;
        _valueType = valueType;

        _stringValue = null;
        _intValue = CommonConstants.NoValueInt;
        _boolValue = false;
        _dateTimeValue = CommonConstants.NoValueDate;
        _doubleValue = CommonConstants.NoValueDouble;

        _changed = true;
    }


    /**
     * Get the value, checking the type
     */
    public string getStringValue()
    {
        checkValueType(ValueType.String);
        return _stringValue;
    }


    /**
     * Get the value, checking the type
     */
    public int getIntValue()
    {
        checkValueType(ValueType.Int);
        return _intValue;
    }


    /**
     * Get the value, checking the type
     */
    public bool getBoolValue()
    {
        checkValueType(ValueType.Bool);
        return _boolValue;
    }


    /**
     * Get the value, checking the type
     */
    public DateTime getDateTimeValue()
    {
        checkValueType(ValueType.DateTime);
        return _dateTimeValue;
    }


    /**
     * Get the value, checking the type
     */
    public double getDoubleValue()
    {
        checkValueType(ValueType.Double);
        return _doubleValue;
    }


    /**
     * Set the value, checking the type
     */
    public void setValue(string val)
    {
        checkValueType(ValueType.String);
        _stringValue = val;
        _changed = true;
    }


    /**
     * Set the value, checking the type
     */
    public void setValue(int val)
    {
        checkValueType(ValueType.Int);
        _intValue = val;
        _changed = true;
    }


    /**
     * Set the value, checking the type
     */
    public void setValue(bool val)
    {
        checkValueType(ValueType.Bool);
        _boolValue = val;
        _changed = true;
    }


    /**
     * Set the value, checking the type
     */
    public void setValue(DateTime val)
    {
        checkValueType(ValueType.DateTime);
        _dateTimeValue = val;
        _changed = true;
    }


    /**
     * Set the value, checking the type
     */
    public void setValue(double val)
    {
        checkValueType(ValueType.Double);
        _doubleValue = val;
        _changed = true;
    }


    /**
     * Constructor for in-memory String MiscAttributes
     */
    private void checkValueType(ValueType valueType)
    {
        if (valueType != _valueType)
            _log.error("checkValueType() valueType=" + valueType + 
                       " _valueType=" + _valueType);
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return MiscAttributeMgr.singleton;
    }
}


}