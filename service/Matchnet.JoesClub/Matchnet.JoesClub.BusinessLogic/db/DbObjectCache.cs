using System.Collections;
using System.Threading;
using Matchnet.JoesClub.ValueObjects;

/**
 * A simple db object cache
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class DbObjectCache
{
    private static Log _log = new Log("DbObjectCache");

    // we create DbObjectCache per thread
    // Hashtable[Thread --> DbObjectCache]
    private static Hashtable _caches = new Hashtable();

    private Hashtable _objects;


    #region construction
    /**
     * Creates a DbObjectCache for the current thread
     */
    public static void create()
    {
        _caches[Thread.CurrentThread] = new DbObjectCache();
    }


    /**
     * Destroys a DbObjectCache for the current thread
     */
    public static void destroy()
    {
        _caches[Thread.CurrentThread] = null;
    }


    /**
     * Constructor
     */
    private DbObjectCache()
    {
        _objects = new Hashtable();
    }
    #endregion


    /**
     * Adds an object to the cache
     */
    public static void add(DbObject dbObject)
    {
        DbObjectCache dbObjectCache = (DbObjectCache) _caches[Thread.CurrentThread];
        if (dbObjectCache == null)
            return;
        dbObjectCache._add(dbObject);
    }


    /**
     * Adds an object to the cache
     */
    private void _add(DbObject dbObject)
    {
        if (dbObject.id == DbConstants.NoId)
            _log.error("The dbObject has no id: " + dbObject);

        _objects[getKey(dbObject)] = dbObject;
    }


    /**
     * Gets an object from the cache
     */
    public static DbObject get(DbObjectMgr dbObjectMgr, int id)
    {
        DbObjectCache dbObjectCache = (DbObjectCache) _caches[Thread.CurrentThread];
        if (dbObjectCache == null)
            return null;
        return dbObjectCache._get(dbObjectMgr, id);
    }


    /**
     * Gets an object from the cache
     */
    private DbObject _get(DbObjectMgr dbObjectMgr, int id)
    {
        DbObject dbObject = (DbObject) _objects[getKey(dbObjectMgr, id)];

        string hitOrMiss = dbObject == null ? "miss" : "hit";
        _log.debug("get() " + hitOrMiss + " " + 
            dbObjectMgr.getTableName() + " " + id);
        
        return dbObject;
    }


    /**
     * Returns a unique key for the DbObject
     */
    private object getKey(DbObject dbObject)
    {
        return getKey(dbObject.getDbObjectMgr(), dbObject.id);
    }


    /**
     * Returns a unique key for the DbObjectMgr and id.
     */
    private object getKey(DbObjectMgr dbObjectMgr, int id)
    {
        return dbObjectMgr.getTableName() + id;
    }
}

}