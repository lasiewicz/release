using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class DbUtils
{
    private static Log _log = new Log("DbUtils");


    /**
     * Get a SqlConnection.
     */  
    public static SqlConnection getConnection()
    {
        _log.debug("createConnection()");
        SqlConnection conn = null;
        try
        {
            string sc = ConfigurationSettings.AppSettings["ConnectionString"];
            conn = new SqlConnection(sc);
            conn.Open();
            return conn;
        }

        catch (Exception e)
        {
            if (conn != null)
                conn.Close();
            throw e;
        }
    }


    /**
     * Gets an int from the reader.  A missing value is treated as an error.
     */
    public static int getIntFromReader(SqlDataReader reader, string column)
    {
        object obj = reader[column];
        if (obj is DBNull)
            _log.error("getIntFromReader() Missing value: " + column);
        return (int) obj;
    }


    /**
     * Gets an int from the reader
     */
    public static int getIntFromReader(SqlDataReader reader, string column,
        int defaultValue)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return defaultValue;
        return (int) obj;
    }


    /**
     * Gets an id int from the reader
     */
    public static int getIdFromReader(SqlDataReader reader, string column)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return DbConstants.NoId;
        if (obj is int)
            return (int) obj;
        return (int)(decimal) obj;
    }


    /**
     * Gets a double from the reader
     */
    public static double getDoubleFromReader(SqlDataReader reader, string column)
    {
        return getDoubleFromReader(reader, column, CommonConstants.NoValueDouble);
    }


    /**
     * Gets a double from the reader
     */
    public static double getDoubleFromReader(SqlDataReader reader, string column,
        double defaultValue)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return defaultValue;
        else if (obj is double)
            return (double) obj;
        else 
            return (double)(decimal) obj;
    }


    /**
     * Gets an bool from the reader.  A missing value is treated as an error.
     */
    public static bool getBoolFromReader(SqlDataReader reader, string column)
    {
        object obj = reader[column];
        if (obj is DBNull)
            _log.error("getBoolFromReader() Missing value: " + column);
        return (bool) obj;
    }


    /**
     * Gets an bool from the reader
     */
    public static bool getBoolFromReader(SqlDataReader reader, string column,
        bool defaultValue)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return defaultValue;
        else 
            return (bool) obj;
    }


    /**
     * Gets an string from the reader
     */
    public static string getStringFromReader(SqlDataReader reader, string column)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return null;
        else
            return (string) obj;
    }


    /**
     * Gets a DateTime from the reader
     */
    public static DateTime getDateTimeFromReader(SqlDataReader reader, string column)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return CommonConstants.DateTimeNone;
        else
            return (DateTime) obj;
    }


    /**
     * Gets a DateTime from the reader
     */
    public static DateTime getDateTimeFromReader(SqlDataReader reader, int column)
    {
        object obj = reader[column];
        if (obj is DBNull)
            return CommonConstants.DateTimeNone;
        else
            return (DateTime) obj;
    }


    /**
     * Gets a couple from the reader
     */  
    public static DbObject getDbObjectFromReaderAndHash(
        SqlDataReader reader, 
        Hashtable dbObjects,
        string column)
    {
        int id = (int)(decimal) reader[column];
        DbObject dbObject = (DbObject) dbObjects[id];
        if (dbObject == null)
            _log.error("load() bad dbObject id: " + id);
        return dbObject;
    }



    /**
     * Create an int SqlParameter
     */
    public static SqlParameter createIntParam(string name, int iValue)
    {
        SqlParameter p = new SqlParameter(name, SqlDbType.Int);
        p.Value = iValue;
        return p;
    }


    /**
     * Create an bool SqlParameter
     */
    public static SqlParameter createBoolParam(string name, bool bValue)
    {
        SqlParameter p = new SqlParameter(name, SqlDbType.Bit);
        p.Value = bValue;
        return p;
    }


    /**
     * Create an double SqlParameter
     */
    public static SqlParameter createDoubleParam(string name, double dValue)
    {
        SqlParameter p = new SqlParameter(name, SqlDbType.Decimal);
        p.Value = dValue;
        return p;
    }


    /**
     * Create an DateTime SqlParameter
     */
    public static SqlParameter createDateTimeParam(string name, DateTime dValue)
    {
        object obj = DBNull.Value;
        if (dValue != CommonConstants.DateTimeNone)
            obj = dValue;

        SqlParameter p = new SqlParameter(name, SqlDbType.DateTime);
        p.Value = obj;
        return p;
    }


    /**
     * Create an ID SqlParameter
     */
    public static SqlParameter createIdParam(string name, DbObject dbObject)
    {
        object id = DBNull.Value;
        if (dbObject != null)
            id = dbObject.id;
        SqlParameter p = new SqlParameter(name, SqlDbType.Int);
        p.Value = id;
        return p;
    }


    /**
     * Create an ID SqlParameter
     */
    public static SqlParameter createIdParam(string name, int id)
    {
        return createIntParam(name, id);
    }


    /**
     * set an ID command Parameter
     */
    public static void setIdParam(string name, DbObject dbObject, SqlCommand command)
    {
        object id = DBNull.Value;
        if (dbObject != null)
            id = dbObject.id;
        command.Parameters.Add(name, SqlDbType.Int).Value = id;
    }


    /**
     * set an ID command Parameter
     */
    public static SqlParameter createIdOutParam(string name)
    {
		SqlParameter idParam = new SqlParameter(name, SqlDbType.Int, 4);
		idParam.Direction = ParameterDirection.Output;
		idParam.Value = DbConstants.NoId;
        return idParam;
    }



    /**
     * set a DateTime command Parameter
     */
    public static void setDateTimeParam(string name, DateTime dt, SqlCommand command)
    {
        object obj = DBNull.Value;
        if (dt != CommonConstants.DateTimeNone)
            obj = dt;
        command.Parameters.Add(name, SqlDbType.DateTime).Value = obj;
    }


    /**
     * Create a string SqlParameter
     */
    public static SqlParameter createStringParam(string name, string sValue)
    {
        object oValue = getStringForDbParam(sValue);
        SqlParameter p = new SqlParameter(name, SqlDbType.VarChar);
        p.Value = oValue;
        return p;
    }


    /**
     * set a string command Parameter
     */
    public static void setStringParam(string name, string sValue, SqlCommand command)
    {
        command.Parameters.Add(createStringParam(name, sValue));
    }


    /**
     * get a string for a command Parameter
     */
    public static object getStringForDbParam(string s)
    {
        if (s == null)
            return DBNull.Value;
        return s;
    }


    /**
     * get the dbObject's id, checking for null.
     */
    public static int getId(DbObject dbObject)
    {
        return dbObject == null ? DbConstants.NoId : dbObject.id;
    }


    /**
     * Execute sql which doesn't return a value.
     */
    public static void runStoredProc(string storedProc, SqlParameter[] sqlParameters)
    {
        SqlDataReader reader = null;
        SqlConnection conn = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand(storedProc, conn);
            command.CommandType = CommandType.StoredProcedure;

            // add params
            if (sqlParameters != null)
                for (int i = 0; i < sqlParameters.Length; i++)
                    command.Parameters.Add(sqlParameters[i]);

            // execute
            reader = command.ExecuteReader();
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Calls a stored proc and returns the a DataSet
     */
    public static DataSet getDataSet(
        string storedProc, string srcTable, SqlParameter[] sqlParameters)
    {
        SqlDataReader reader = null;
        SqlConnection conn = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand(storedProc, conn);
            command.CommandType = CommandType.StoredProcedure;
			SqlDataAdapter sqlda = new SqlDataAdapter(command);

            // add params
            if (sqlParameters != null)
                for (int i = 0; i < sqlParameters.Length; i++)
                    command.Parameters.Add(sqlParameters[i]);

            // execute
			DataSet dataSet = new DataSet();
            sqlda.Fill(dataSet, srcTable);

            return dataSet;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Execute storedProc and return the id of the new object.
     * sqlParameters = ArrayList[SqlParameter]
     */
    public static int insert(
        string storedProc, ArrayList sqlParameters)
    {
        update(storedProc, sqlParameters);

        // the last param should be an ID out param.
        SqlParameter idParam = (SqlParameter) sqlParameters[sqlParameters.Count - 1];
        return (int) idParam.Value;
    }


    /**
     * Execute storedProc without returning anything.
     * sqlParameters = ArrayList[SqlParameter]
     */
    public static void update(
        string storedProc, ArrayList sqlParameters)
    {
        SqlConnection conn = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand(storedProc, conn);
            command.CommandType = CommandType.StoredProcedure;

            // add params
            if (sqlParameters != null)
                foreach (SqlParameter sqlParameter in sqlParameters)
                    command.Parameters.Add(sqlParameter);

            // execute
            command.ExecuteNonQuery();
        }

        finally
        {
            if (conn != null)
                conn.Close();
        }
    }



    /**
     * Creates a StoredProcedure SqlCommand
     */
    public static SqlCommand getStoredProcCommand(SqlConnection conn, string storedProc)
    {
        SqlCommand command = new SqlCommand(storedProc, conn);
        command.CommandType = CommandType.StoredProcedure;
        return command;
    }

    
    /**
     * Cleans up sql objects
     */
    public static void cleanUp(
        ref SqlConnection conn,
        ref SqlCommand command,
        ref SqlDataReader reader)
    {
        if (reader != null)
            reader.Close();

        if (command != null)
        {
            command.Parameters.Clear();
            command.Dispose();
        }

        if (conn != null)
            conn.Close();

        reader = null;
        command = null;
        conn = null;
    }

}

}