using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;

namespace Matchnet.JoesClub.BusinessLogic
{
abstract public class HasMiscAttributes : DbObject
{
    private static Log _log = new Log("HasMiscAttributes");

    // Hashtable[sting name --> MiscAttribute]
    private Hashtable _miscAttributes;

    public Hashtable miscAttributes { get { return _miscAttributes; } set { _miscAttributes = value; } }


    /**
     * Constructor
     */
    public HasMiscAttributes(int id) : this(id, false) {}

    /**
     * Constructor
     */
    public HasMiscAttributes(int id, bool empty) : base(id, empty)
    {
        _miscAttributes = new Hashtable();
    }


    /**
     * Convenience method to get the db table name
     */
    public string getDbTable()
    {
        return getDbObjectMgr().getTableName();
    }


    /**
     * Get the value
     */
    protected string getStringValue(string name)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
            return null;
        else
            return miscAttribute.getStringValue();
    }


    /**
     * Get the value
     */
    protected int getIntValue(string name)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
            return CommonConstants.NoValueInt;
        else
            return miscAttribute.getIntValue();
    }


    /**
     * Get the value
     */
    protected DateTime getDateTimeValue(string name)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
            return CommonConstants.NoValueDate;
        else
            return miscAttribute.getDateTimeValue();
    }


    /**
     * Get the value
     */
    protected bool getBoolValue(string name)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
            return false;
        else
            return miscAttribute.getBoolValue();
    }


    /**
     * Get the value
     */
    protected double getDoubleValue(string name)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
            return CommonConstants.NoValueDouble;
        else
            return miscAttribute.getDoubleValue();
    }


    /**
     * Set the value
     */
    protected void setValue(string name, string val)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
        {
            miscAttribute = new MiscAttribute(name, this, val);
            _miscAttributes[name] = miscAttribute;
        }
        else
            miscAttribute.setValue(val);        
    }


    /**
     * Set the value
     */
    protected void setValue(string name, int val)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
        {
            miscAttribute = new MiscAttribute(name, this, val);
            _miscAttributes[name] = miscAttribute;
        }
        else
            miscAttribute.setValue(val);        
    }


    /**
     * Set the value
     */
    protected void setValue(string name, DateTime val)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
        {
            miscAttribute = new MiscAttribute(name, this, val);
            _miscAttributes[name] = miscAttribute;
        }
        else
            miscAttribute.setValue(val);        
    }


    /**
     * Set the value
     */
    protected void setValue(string name, bool val)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
        {
            miscAttribute = new MiscAttribute(name, this, val);
            _miscAttributes[name] = miscAttribute;
        }
        else
            miscAttribute.setValue(val);        
    }


    /**
     * Set the value
     */
    protected void setValue(string name, double val)
    {
        MiscAttribute miscAttribute = (MiscAttribute) _miscAttributes[name];
        if (miscAttribute == null)
        {
            miscAttribute = new MiscAttribute(name, this, val);
            _miscAttributes[name] = miscAttribute;
        }
        else
            miscAttribute.setValue(val);        
    }


}


}