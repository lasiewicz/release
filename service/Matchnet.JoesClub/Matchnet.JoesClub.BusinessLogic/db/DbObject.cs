using System.Collections;
using Matchnet.JoesClub.ValueObjects;


/**
 * Base class for persistent objects.
 * 
 * We have the concept of an "empty" DbObject object to aid in the construction
 * of objects which contain other objects, for example an Event contains a Venue.
 * An empty DbObject contains only an id.  Because we can only have one SqlDataReader 
 * open at a time, the event and venue must be loaded separately.  It makes sense to 
 * get the venue ID during the normal EventMgr.load(SqlDataReader), but we need
 * a way to save that id until we can load the venue.  So, we create an empty
 * Venue object containing only the ID, save it in the Event and later in the 
 * loading process we replace the empty Venue with the real one. 
 */ 
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class DbObject : Logable
{
    // properties
    private int _id;
    public int id { get { return _id; } set { _id = value; } }

    private bool _empty;
    public bool empty { get { return _empty; } set { _empty = value; } }


    /**
     * Constructor
     */
    public DbObject(int id) : this (id, false)
    {
    }


    /**
     * Constructor
     */
    public DbObject(int id, bool empty)
    {
        _id = id;
        _empty = empty;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public abstract DbObjectMgr getDbObjectMgr();


    /**
     * Loads the DbObject 
     */
    public DbObject emptyToReal()
    {
        if (_id == DbConstants.NoId)
            return null;
        return getDbObjectMgr().load(_id);
    }


    /**
     * Save the DbObject 
     */
    public void save()
    {
        getDbObjectMgr().save(this);
    }


    /**
     * override Equals 
     */
    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        if (obj.GetType() != this.GetType())
            return false;
        DbObject dbObject = (DbObject) obj;
        if (dbObject.id == DbConstants.NoId && this.id == DbConstants.NoId)
            return base.Equals(obj);

        return dbObject.id == this.id;
    }

    public static bool operator ==(DbObject dbObject1, DbObject dbObject2) 
    {
        if (object.ReferenceEquals(dbObject1, null))
            return object.ReferenceEquals(dbObject2, null);
        
        return dbObject1.Equals(dbObject2);
    }

    public static bool operator !=(DbObject dbObject1, DbObject dbObject2) 
    {
        return !(dbObject1 == dbObject2);
    }


    /**
     * override GetHashCode 
     */
    public override int GetHashCode()
    {
        if (_id == DbConstants.NoId)
            return base.GetHashCode();
        string key = this.GetType().ToString() + _id;
        return CaseInsensitiveHashCodeProvider.Default.GetHashCode(key);
    }


    #region Logable Members

    /**
     * Logable interface method
     */
    public virtual void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_id", _id);
    }

    #endregion
}

}