using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



/**
 * Used to set and get values from a DB column
 */
namespace Matchnet.JoesClub.BusinessLogic
{
abstract class DbColumn
{
    private static Log _log = new Log("DbColumn");

    private string _columnName;
    private SqlDbType _type;

    public string columnName { get { return _columnName; } }
    public string paramName { get { return "@" + _columnName; } }
    public SqlDbType type { get { return _type; } }


    /**
     * Constructor
     */
    public DbColumn(string column, SqlDbType type)
    {
        _columnName = column;
        _type = type;
    }


    /**
     * set a command Parameter
     */
    public abstract void setParam(DbObject dbObject, SqlCommand command);


     /**
     * Gets a value from the reader
     */
    public virtual object getFromReader(SqlDataReader reader, object oDefault)
    {
        if (_type == SqlDbType.Bit)
            return DbUtils.getBoolFromReader(reader, columnName, (bool) oDefault);
        else
            _log.error("setParam() unsupported SqlDbType: " + _type.ToString());
        return null;
    }


   /**
     * set a command Parameter
     */
    public virtual void setParam(object val, SqlCommand command)
    {
        command.Parameters.Add(paramName, _type).Value = val;
    }


   /**
     * Convenience method to get "col = @col"
     */
    public string colEqualsParam()
    {
        return _columnName + " = " + paramName;
    }
}

}