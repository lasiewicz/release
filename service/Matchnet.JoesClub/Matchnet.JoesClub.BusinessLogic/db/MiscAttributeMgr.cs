using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the MiscAttribute table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MiscAttributeMgr : DbObjectMgr
{
    #region basic stuff
    private static Log _log = new Log("MiscAttributeMgr");

    private static MiscAttributeMgr _singleton;
    public static MiscAttributeMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new MiscAttributeMgr();
            return _singleton;
        } 
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "miscAttributeId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "MiscAttribute";
    }

    #endregion // basic stuff

    #region standard insert, update, load

    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        MiscAttribute miscAttribute = (MiscAttribute) dbObject;
        ArrayList SqlParams = setParams(miscAttribute, true);
        miscAttribute.id = DbUtils.insert("AddMiscAttribute", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        MiscAttribute miscAttribute = (MiscAttribute) dbObject;

        // don't need to do anything if it hasn't changed
        if (!miscAttribute.changed)
            return;

        ArrayList SqlParams = setParams(miscAttribute, false);
        DbUtils.update("UpdateMiscAttribute", SqlParams);
    }


    /**
     * Sets command Parameters for everything in MiscAttribute
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(MiscAttribute miscAttribute, bool insert)
    {        
        string ownerTable = null;
        if (miscAttribute.owner != null)
            ownerTable = miscAttribute.owner.getDbTable();

        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@miscAttributeId", miscAttribute));
        sqlParameters.Add(DbUtils.createStringParam("@name", miscAttribute.name));
        sqlParameters.Add(DbUtils.createIdParam("@ownerId", miscAttribute.owner));
        sqlParameters.Add(DbUtils.createStringParam("@ownerTable", ownerTable));
        sqlParameters.Add(DbUtils.createIntParam("@valueType", (int) miscAttribute.valueType));
        sqlParameters.Add(DbUtils.createStringParam("@stringValue", miscAttribute.stringValue));
        sqlParameters.Add(DbUtils.createIntParam("@intValue", miscAttribute.intValue));
        sqlParameters.Add(DbUtils.createBoolParam("@boolValue", miscAttribute.boolValue));
        sqlParameters.Add(DbUtils.createDateTimeParam("@dateTimeValue", miscAttribute.dateTimeValue));
        sqlParameters.Add(DbUtils.createDoubleParam("@doubleValue", miscAttribute.doubleValue));
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@miscAttributeId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader, object owner)
    {
        MiscAttribute miscAttribute = new MiscAttribute(
            DbUtils.getIdFromReader(reader, "miscAttributeId"),
            DbUtils.getStringFromReader(reader, "name"),
            (HasMiscAttributes) owner,
            (MiscAttribute.ValueType) DbUtils.getIntFromReader(reader, "valueType"),
            DbUtils.getStringFromReader(reader, "stringValue"),
            DbUtils.getIntFromReader(reader, "intValue"),
            DbUtils.getBoolFromReader(reader, "boolValue"),
            DbUtils.getDateTimeFromReader(reader, "dateTimeValue"),
            DbUtils.getDoubleFromReader(reader, "doubleValue"));
        return miscAttribute;
    }

    #endregion // standard insert, update, load


    /**
     * loads the MiscAttributes for the given HMA
     */
    public ArrayList load(HasMiscAttributes owner)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIdParam("@ownerId", owner.id),
            DbUtils.createStringParam("@ownerTable", owner.getDbTable())
        };
        return load("GetMiscAttributes", sqlParameters, owner);
    }


    /**
     * loads the MiscAttributes for the given name
     */
    public ArrayList loadByName(string name)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createStringParam("@name", name)
        };
        return load("GetMiscAttributesByName", sqlParameters, null);
    }


    /**
     * Deletes the MiscAttributes for the given owner
     */
    public void delete(int ownerId, string ownerTable)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIdParam("@ownerId", ownerId),
            DbUtils.createStringParam("@ownerTable", ownerTable)
        };
        DbUtils.runStoredProc("DeleteMiscAttributes", sqlParameters);
    }

}

}