using System.Collections;
using System.Text;
using Matchnet.JoesClub.ValueObjects;


/**
 * A Compatibility is just a double value.  This class handles all functionality for manipulating 
 * compatibilities such as converting to letter grades and combining multiple compatibilities
 * into one.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class Compatibility
{
    private static Log _log = new Log("Compatibility");

    private double _value;
    public double value { get { return _value; } set { _value = value; } }

    private const string GradeNever = "0";
    private const string GradeAlways = "1";
    private const string GradeA = "A";
    private const string GradeB = "B";
    private const string GradeC = "C";
    private const string GradeD = "D";
    private const string GradeF = "F";

    public const double ValueNever = 10000.0;
    private const double ValueAlways = 1.0;
    private const double ValueA = 2.0;
    private const double ValueB = 3.0;
    private const double ValueC = 4.0;
    private const double ValueD = 5.0;
    private const double ValueF = 6.0;

    public static Compatibility A = new Compatibility(ValueA);
    public static Compatibility C = new Compatibility(ValueC);
    public static Compatibility F = new Compatibility(ValueF);
    public static Compatibility Never = new Compatibility(ValueNever);


    /**
     * Constructor
     */
    public Compatibility(double value)
    {
        _value = value;
    }



    /**
     * Constructor from a letter grade
     */
    public Compatibility(string grade)
    {
        _value = gradeToValue(grade);
    }


    /**
     * Constructor.  Averages the compatibilities.
     * compatibilities: ArrayList[Compatibility]
     */
    public Compatibility(ArrayList compatibilities, ArrayList weights)
    {
        setValueFromArray(compatibilities, weights);
    }


    /**
     * Computes the compatibility between all the members.
     * members: ArrayList[Member]
     */
    public static Compatibility getMembersCompatibility(ArrayList members, Event ev)
    {
        // find the compatibility between all pairs of members
        ArrayList compatibilities = new ArrayList(); // ArrayList[Compatibility]
        ArrayList weights = new ArrayList(); // ArrayList[double]

        for (int m1 = 0; m1 < members.Count; m1++)
            for (int m2 = m1 + 1; m2 < members.Count; m2++)
            {
                Member member1 = (Member) members[m1];
                Member member2 = (Member) members[m2];

                _log.debug("member1.firstName: " + member1.firstName);
                _log.debug("member2.firstName: " + member2.firstName);

                Compatibility comp = ev.memberGrid.getCompatibility(member1, member2);

                // if any two people are Never's, return Never
                if (comp.isNever)
                    return Compatibility.Never;

                // convert Always to A
                if (comp.isAlways)
                    comp = Compatibility.A;

                // determine weight
                double weight = BoConstants.MaleFemaleCompatibilityWeight;
                if (member1.gender == member2.gender)
                    weight = BoConstants.SameSexCompatibilityWeight;

                compatibilities.Add(comp);
                weights.Add(weight);
            }

        return new Compatibility(compatibilities, weights);
    }




    /**
     * Averages the compatibilities.
     * compatibilities: ArrayList[Compatibility]
     * weights: ArrayList[double]
     */
    public void setValueFromArray(ArrayList compatibilities, ArrayList weights)
    {
        if (compatibilities == null || compatibilities.Count == 0)
        {
            _value = ValueC;
            return;
        }

        double sum = 0;
        double totalWeight = 0;
        for (int i = 0; i < compatibilities.Count; i++)
        {
            Compatibility compatibility = (Compatibility) compatibilities[i];
            double weight = 1.0;
            if (weights != null)
                weight = (double) weights[i];

            if (compatibility._value == ValueNever ||
                compatibility._value == ValueAlways)
            {
                _value = compatibility._value;
                return;
            }

            sum += (compatibility._value * weight);
            totalWeight += weight;
        }

        _value = sum / totalWeight;
    }


    /**
     * convert grade to value
     */
    private static double gradeToValue(string grade)
    {
        switch (grade)
        {
            case GradeNever: return ValueNever;
            case GradeAlways: return ValueAlways;
            case GradeA: return ValueA;
            case GradeB: return ValueB;
            case GradeC: return ValueC;
            case GradeD: return ValueD;
            case GradeF: return ValueF;
        }
        _log.error("gradeToValue() Bad grade: " + grade);
        return -1;
    }


    /**
     * convert value to grade 
     */
    public string grade 
    { 
        get 
        { 
            if (_value == ValueNever) return GradeNever;
            if (_value == ValueAlways) return GradeAlways;
            if (_value <= ValueA + 0.5) return GradeA;
            if (_value <= ValueB + 0.5) return GradeB;
            if (_value <= ValueC + 0.5) return GradeC;
            if (_value <= ValueD + 0.5) return GradeD;
            return GradeF;
        }
    }

    // returns whether the value is never or alway
    public bool isAlways { get { return _value == ValueAlways; } }
    public bool isNever { get { return _value == ValueNever; } }


    /**
     * Returns true if grade is valid.
     */
    public static bool validGrade(string grade)
    {
        switch (grade)
        {
            case GradeAlways:
            case GradeNever:
            case GradeA:
            case GradeB:
            case GradeC:
            case GradeD:
            case GradeF:
                return true;
        }
        return false;
    }


    /**
     * Compute the cost from the value
     */
    public double cost()
    { 
        if (_value == ValueAlways || _value == ValueNever)
            return _value;

        // to compute the cost we use a polynomial equation using the values in 
        // BoConstants.CompatibilityCostFormula for the coefficients.
        // cost = c0 + c1*v + c2*v^2 + c3*v^4...
        double cost = 0;
        double v = 1;
        foreach (double coef in BoConstants.CompatibilityCostFormula)
        {
            cost += coef * v;
            v *= _value;
        }

        return cost; 
    }


}

}