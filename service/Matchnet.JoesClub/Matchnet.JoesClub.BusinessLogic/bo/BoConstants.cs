using System;
using Matchnet.JoesClub.ValueObjects;

/**
 * Constants used by BO files
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class BoConstants
{
    // jlf obsolete.  should get rid of this and use CommonConstants.NoValueInt everywhere.
    public const int NoValueInt = CommonConstants.NoValueInt;


    // coupling values
    public const string CouplingServerSetTrue = "ServerSetTrue";
    public const string CouplingForcedFalse = "ForcedFalse";
    public const string CouplingUnspecified = "Unspecified";

    public const string EventTable = "Event";
    public const string MemberTable = "Member";
    public const string MemberGridCellTable = "MemberGridCell";
    public const string TableTable = "EventTable";
     
    // columns
    public const string Id = "id";
    public const string Description = "Description";
    public const string When = "When";
    public const string Member = "member";

    // when computing the overall compatibility of a group of people
    // this value is used to weight the compatibility male-female
    // couple differently from same sex couples.  For example,
    // a values of 3 and 1 mean male-female couple are weight 3 times as high 
    // as same sex couples.
    public const double MaleFemaleCompatibilityWeight = 3.0;
    public const double SameSexCompatibilityWeight = 1.0;
    

    // this array contains the coefficiants for the polynomial formula for a
    // compatibility cost given the value, v:
    // cost = c0 + c1*v + c2*v^2 + ... + cn*v^n
    // currently it's just: cost  = value ^ 2
    public static double[] CompatibilityCostFormula = new double[] { 0, 0, 1 };
}
}
