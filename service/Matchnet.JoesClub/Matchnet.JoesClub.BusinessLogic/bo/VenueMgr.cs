using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the Venue table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class VenueMgr : HasMiscAttributesMgr
{
    #region basic stuff
    private static Log _log = new Log("VenueMgr");

    private static VenueMgr _singleton;
    public static VenueMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new VenueMgr();
            return _singleton;
        } 
    }


    /**
     * Create an empty Venue object.
     */
    public override DbObject createEmptyDbObject(int id) 
    { 
        return new Venue(id);
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "VenueID"; }


    /**
     * Return the db table for the object
     */
    public override string getTableName() { return "Venue"; }
    #endregion


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        Venue venue = (Venue) dbObject;
        ArrayList SqlParams = setParams(venue, true);
        venue.id = DbUtils.insert("AddVenue", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        Venue venue = (Venue) dbObject;
        ArrayList SqlParams = setParams(venue, false);
        DbUtils.update("UpdateVenue", SqlParams);
    }


    /**
     * Sets command Parameters for everything in venue
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(Venue venue, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@venueId", venue));

        sqlParameters.Add(DbUtils.createStringParam("@name", venue.name));
        sqlParameters.Add(DbUtils.createStringParam("@address", venue.address));
        sqlParameters.Add(DbUtils.createStringParam("@address2", venue.address2));
        sqlParameters.Add(DbUtils.createStringParam("@city", venue.city));
        sqlParameters.Add(DbUtils.createStringParam("@state", venue.state));
        sqlParameters.Add(DbUtils.createStringParam("@zipCode", venue.zipCode));
        sqlParameters.Add(DbUtils.createStringParam("@webSiteUrl", venue.webSiteUrl));
        sqlParameters.Add(DbUtils.createStringParam("@image", venue.image));
        sqlParameters.Add(DbUtils.createStringParam("@phone", venue.phone));
        sqlParameters.Add(DbUtils.createStringParam("@contactName", venue.contactName));
        sqlParameters.Add(DbUtils.createStringParam("@contactPhone", venue.contactPhone));
        sqlParameters.Add(DbUtils.createStringParam("@contactTitle", venue.contactTitle));
        sqlParameters.Add(DbUtils.createStringParam("@mapUrl", venue.mapUrl));
        sqlParameters.Add(DbUtils.createIntParam("@parking", venue.parking));
        sqlParameters.Add(DbUtils.createDoubleParam("@parkingFee", venue.parkingFee));
        sqlParameters.Add(DbUtils.createStringParam("@priceRange", venue.priceRange));
        sqlParameters.Add(DbUtils.createStringParam("@notes", venue.notes));
        sqlParameters.Add(DbUtils.createBoolParam("@deleted", venue.deleted));
        
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@venueId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        Venue Venue = new Venue(
            (int)(decimal) reader["VenueId"],
            DbUtils.getStringFromReader(reader, "name"),
            DbUtils.getStringFromReader(reader, "address"),
            DbUtils.getStringFromReader(reader, "address2"),
            DbUtils.getStringFromReader(reader, "city"),
            DbUtils.getStringFromReader(reader, "state"),
            DbUtils.getStringFromReader(reader, "zipCode"),
            DbUtils.getStringFromReader(reader, "webSiteUrl"),
            DbUtils.getStringFromReader(reader, "image"),
            DbUtils.getStringFromReader(reader, "phone"),
            DbUtils.getStringFromReader(reader, "contactName"),
            DbUtils.getStringFromReader(reader, "contactPhone"),
            DbUtils.getStringFromReader(reader, "contactTitle"),
            DbUtils.getStringFromReader(reader, "mapUrl"),
            DbUtils.getIntFromReader(reader, "parking"),
            DbUtils.getDoubleFromReader(reader, "parkingFee", 0.0),
            DbUtils.getStringFromReader(reader, "priceRange"),
            DbUtils.getStringFromReader(reader, "notes"),
            null,  // salesCopy.  loaded later from MiscAttribute
            null,  // salesCopy2.  loaded later from MiscAttribute
            null,  // image2.  loaded later from MiscAttribute
            DbUtils.getBoolFromReader(reader, "deleted"),
            null); // meetingLocation.  loaded later from MiscAttribute
        return Venue;
    }


    /**
     * loads the hosts, ordering by name.
     */
    public ArrayList loadAllByName()
    {
        // GetAllVenues takes an optional @orderByName param
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@orderByName", true) };

        // load the venues
        return load("GetAllVenues", sqlParameters);
    }
}

}