using System;
using System.Collections;
using System.Xml;
using Matchnet.JoesClub.ValueObjects;

/**
 * This class represents the <RulesJScript> element from config.xml
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class RuleSet : Logable
{
    private static Log _log = new Log("RuleSet");

    private RuleFile _maleFemaleRuleFile;
    public RuleFile maleFemaleRuleFile { get { return _maleFemaleRuleFile; }}

    private RuleFile _maleMaleRuleFile;
    public RuleFile maleMaleRuleFile { get { return _maleMaleRuleFile; }}

    private RuleFile _femaleFemaleRuleFile;
    public RuleFile femaleFemaleRuleFile { get { return _femaleFemaleRuleFile; }}

    /**
     * Constructor
     */
    public RuleSet()
    {
        string mfFileName = Config.getProperty("MaleFemaleRules");
        _maleFemaleRuleFile = new RuleFile(mfFileName);

        string mmFileName = Config.getProperty("MaleMaleRules");
        _maleMaleRuleFile = new RuleFile(mmFileName);

        string ffFileName = Config.getProperty("FemaleFemaleRules");
        _femaleFemaleRuleFile = new RuleFile(ffFileName);

    }


    /**
     * Logable interface method
     */
    public void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_maleFemaleRuleFile", _maleFemaleRuleFile);
        objectLogger.log("_maleMaleRuleFile", _maleMaleRuleFile);
        objectLogger.log("_femaleFemaleRuleFile", _femaleFemaleRuleFile);
    }
}
}