using System;
using System.Collections;
using System.Configuration;
using System.IO; 
using System.Web; 
using System.Xml; 
using Matchnet.JoesClub.ValueObjects;


/**
 * Read config.xml
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class RuleSetConfig
{
    private static RuleSet _c_ruleSet;


    /**
     * returns _c_ruleSet
     */
    public static RuleSet ruleSet 
    { 
        get 
        { 
            init("_c_ruleSet");
            return _c_ruleSet; 
        }
    }


    /**
     * clear cached values
     */
    public static void clear() 
    { 
        _c_ruleSet = null; 
    }


    /**
     * Initialize class members that are created on demand.
     */
    private static void init(string name)
    {
        if (name == "_c_ruleSet" && _c_ruleSet == null)
            _c_ruleSet = new RuleSet();
    }
}


}