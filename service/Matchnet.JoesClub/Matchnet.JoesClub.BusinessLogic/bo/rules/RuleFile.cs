using System;
using System.Collections;
using System.CodeDom.Compiler;
using Microsoft.JScript;
using System.Reflection;
using System.Xml;
using Matchnet.JoesClub.ValueObjects;

/**
 * This class implements the JScript rules engine
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class RuleFile : Logable
{
    private static Log _log = new Log("RuleFile");
    
    // xml strings
    private const string FileAttr = "file";
    
    private Type _jScriptType;
    private object _jScriptObject;

    private string _path;
    public string path { get { return _path; }}

    /**
     * Constructor
     */
    public RuleFile(string ruleFileName)
    { 
        string matchingRulesDir = Config.getProperty("MatchingRulesDir");
        _path = matchingRulesDir + "/" + ruleFileName;
        initJScript();
    }    


    /**
     * read the jscript file
     */
    public void initJScript()
    {
        // compile the rules JS file
        ICodeCompiler compiler = new JScriptCodeProvider().CreateCompiler();
        CompilerParameters parameters = new CompilerParameters();
        parameters.GenerateInMemory = true;
        parameters.IncludeDebugInformation = true;
        
        CompilerResults results = compiler.CompileAssemblyFromFile(parameters, _path);

        Assembly assembly = results.CompiledAssembly;
        _jScriptType = assembly.GetType("JScriptRuleFile");        
        _jScriptObject = Activator.CreateInstance(_jScriptType);
    }


    /**
     * Returns the compatibility score for two members
     */
    public RuleContext getScore(Member m1, Member m2, Event ev)
    {
        _log.debug("_getScore()");

        RuleContext rc = new RuleContext(ev);
        callRuleSetMethod("getScore", new object[] {m1, m2, rc} );
        return rc;
    }


    /**
     * Call a method in the rule JS file
     */
    private object callRuleSetMethod(string method, object[] args)
    {
        object result = _jScriptType.InvokeMember(
            method, 
            BindingFlags.InvokeMethod, 
            null, 
            _jScriptObject, 
            args);
        return result;
    }


    /**
     * Logable interface method
     */
    public void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_path", _path);
        objectLogger.log("_jScriptType", _jScriptType);
        objectLogger.log("_jScriptObject", _jScriptObject);
    }
}
}