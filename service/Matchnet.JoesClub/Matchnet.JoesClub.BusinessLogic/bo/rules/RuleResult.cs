using System;
using System.Collections;
using System.Xml;
using Matchnet.JoesClub.ValueObjects;


/**
 * A RuleResult object is passed to the getScore() method in the rules
 * javascript files (mfrule.js, etc).  This object is used by the javascript
 * to add results as well as for miscellaneous functionality such as
 * logging messages, in other words, it provide sort of an all purpose 
 * hook back into the server
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class RuleResult
{
    private static Log _log = new Log("RuleResult");

    private Compatibility _compatibility;
    public Compatibility compatibility { get { return _compatibility; }}

    private string _ruleName;
    public string ruleName { get { return _ruleName; }}

    private string _explanation;
    public string explanation { get { return _explanation; }}

    private double _weight;
    public double weight { get { return _weight; }}


    /**
     * Constructor
     */
    public RuleResult(string ruleName, string grade, 
                      double weight, string explanation)
    {
        _compatibility = new Compatibility(grade);
        _ruleName = ruleName;
        _explanation = explanation;
        _weight = weight;        
    }
}
}