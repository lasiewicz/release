using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the phoneInterview table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class PhoneInterviewMgr : HasMiscAttributesMgr
{
    #region basic stuff
    private static Log _log = new Log("PhoneInterviewMgr");

    private static PhoneInterviewMgr _singleton;
    public static PhoneInterviewMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new PhoneInterviewMgr();
            return _singleton;
        } 
    }

    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "phoneInterviewId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "PhoneInterview";
    }

    #endregion // basic stuff

    #region standard insert, update, load

    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        PhoneInterview phoneInterview = (PhoneInterview) dbObject;
        ArrayList SqlParams = setParams(phoneInterview, true);
        phoneInterview.id = DbUtils.insert("AddPhoneInterview", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        PhoneInterview phoneInterview = (PhoneInterview) dbObject;
        ArrayList SqlParams = setParams(phoneInterview, false);
        DbUtils.update("UpdatePhoneInterview", SqlParams);
    }


    /**
     * Sets command Parameters for everything in phoneInterview
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(PhoneInterview phoneInterview, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@phoneInterviewId", phoneInterview));
        sqlParameters.Add(DbUtils.createIdParam("@memberId", phoneInterview.memberId));
        sqlParameters.Add(DbUtils.createIntParam("@interviewStatus", (int) phoneInterview.interviewStatus));
        sqlParameters.Add(DbUtils.createStringParam("@caller", phoneInterview.caller));
        sqlParameters.Add(DbUtils.createDateTimeParam("@whenCalled", phoneInterview.whenCalled));
        sqlParameters.Add(DbUtils.createBoolParam("@gaveContract", phoneInterview.gaveContract));
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@phoneInterviewId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader, object owner)
    {
        Member member = (Member) owner;

        PhoneInterview phoneInterview = new PhoneInterview(
            DbUtils.getIdFromReader(reader, "phoneInterviewId"),
            member.id,
            (InterviewStatus) DbUtils.getIntFromReader(reader, "interviewStatus"),
            DbUtils.getStringFromReader(reader, "caller"),
            DbUtils.getDateTimeFromReader(reader, "whenCalled"),
            DbUtils.getBoolFromReader(reader, "gaveContract"));
        return phoneInterview;
    }

    #endregion // standard insert, update, load


    /**
     * loads the phoneInterviews for the member, ordered by whenCalled
     * Returns ArrayList[PhoneInterview]
     */
    public ArrayList loadForMember(Member member)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIdParam("@memberId", member.id) };

        // load the phoneInterviews
        return load("GetPhoneInterviewsForMember", sqlParameters, member);
    }


    /**
     * Deletes the phoneInterviews for the member
     */
    public void deleteForMember(int memberId)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIdParam("@memberId", memberId) };

        // Delete the phoneInterviews
        DbUtils.runStoredProc("DeletePhoneInterviewsForMember", sqlParameters);
    }
}

}