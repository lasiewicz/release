using System;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Member.ServiceAdapters;

using Matchnet.JoesClub.ValueObjects;

/**
 * Manages logging on, off, cookies
 */
namespace Matchnet.JoesClub.BusinessLogic
{


public class AdminLogonMgr
{
    public static AdminLogonMgr Instance = new AdminLogonMgr();

    public static int JOESCLUB_ADMIN_PRIVILEGE_ID = 163;


    /**
     * Gets the logged in member's id. 
     * Returns DbConstants.NoId if there isn't one.
     */
    public bool authenticated(PageRequest pageRequest) 
    {
        return pageRequest.adminAuthenticated;
    }


    /**
     * Logs a user on and returns the AuthenticationStatus.  
     */
    public AuthenticationStatus logOn(
        PageResponse pageResponse, 
        string emailAddress, string password) 
    {
        AuthenticationStatus authStatus = authenticate(emailAddress, password);

        // add or remove guid depending on if the authentication succeeded
        //string guid = getGuid(pageRequest, pageResponse);
        if (authStatus != AuthenticationStatus.Authenticated)
            return authStatus;

        // we're good
        pageResponse.action = PageResponse.ActionAdminLogon;

        return AuthenticationStatus.Authenticated;
    }

 
    /**
     * Logs the admin off.
     */
    public void logOff(PageResponse pageResponse) 
    {
        pageResponse.action = PageResponse.ActionAdminLogoff;
    }

 
    /**
     * Perform the authentication.
     */
    private AuthenticationStatus authenticate(string emailAddress, string password) 
    {
        // check configuration to see if we need to authenticate for real.
        if (!Config.getBoolProperty("AuthenticateAdmin"))
            return AuthenticationStatus.Authenticated;

        // check email and password
        Matchnet.Member.ValueObjects.AuthenticationResult ar = 
            MemberSA.Instance.Authenticate(JDateMemberUtil.jdateBrand, 
                emailAddress, password);
        AuthenticationStatus authStatus = (AuthenticationStatus) ar.Status;
        if (authStatus != AuthenticationStatus.Authenticated)
            return authStatus;

        // check admin privilege
        bool isAdmin = MemberPrivilegeSA.Instance.GetMemberPrivileges(
            ar.MemberID).HasPrivilege(JOESCLUB_ADMIN_PRIVILEGE_ID);
        if (!isAdmin)
            return AuthenticationStatus.NoAdminPriviledge;
        
        return AuthenticationStatus.Authenticated;
    }

}
}