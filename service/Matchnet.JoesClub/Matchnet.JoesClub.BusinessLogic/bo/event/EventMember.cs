using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Matchnet.JoesClub.ValueObjects;


/**
 * Represents a member in an event.  Basically, a row in the EventMember table.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventMember : DbObject
{
    private static Log _log = new Log("EventMember");

    private Event _event;
    private Member _member;
    private MemberStatus _memberStatus;
    private bool _inEvent;
    private DateTime _cancelDate;
    private bool _lateCancellation;
    private InviteStatus _inviteStatus;
    private bool _alternate;
    private bool _billedForFine;
    private DateTime _billedTime;
    private double _amountCharged;
    private bool _gotNudge1;
    private DateTime _nudge1Time;
    private DateTime _inviteSentTime;
    private string _inviteGuid;
    private string _fineTrxId;

    public Event ev { get { return _event; } set { _event = value; } }
    public Member member { get { return _member; } set { _member = value; } }
    public MemberStatus memberStatus { get { return _memberStatus; } set { _memberStatus = value; }}
    public bool inEvent { get { return _inEvent; } set { _inEvent = value; } }
    public DateTime cancelDate { get { return _cancelDate; } set { _cancelDate = value; } }
    public bool lateCancellation { get { return _lateCancellation; } set { _lateCancellation = value; } }
    public InviteStatus inviteStatus { get { return _inviteStatus; } set { _inviteStatus = value; } }
    public bool alternate { get { return _alternate; } set { _alternate = value; } }
    public bool billedForFine { get { return _billedForFine; } set { _billedForFine = value; } }
    public DateTime billedTime { get { return _billedTime; } set { _billedTime = value; } }
    public double amountCharged { get { return _amountCharged; } set { _amountCharged = value; } }
    public bool gotNudge1 { get { return _gotNudge1; } set { _gotNudge1 = value; } }
    public DateTime nudge1Time { get { return _nudge1Time; } set { _nudge1Time = value; } }
    public DateTime inviteSentTime { get { return _inviteSentTime; } set { _inviteSentTime = value; } }
    public string inviteGuid { get { return _inviteGuid; } set { _inviteGuid = value; } }
    public string fineTrxId { get { return _fineTrxId; } set { _fineTrxId = value; } }


    /**
     * constructor
     */
    public EventMember(
        int id, 
        Event ev, 
        Member member, 
        MemberStatus memberStatus, 
        bool inEvent,
        DateTime cancelDate,
        bool lateCancellation,
        InviteStatus inviteStatus,
        bool alternate,
        bool billedForFine,
        DateTime billedTime,
        double amountCharged,
        bool gotNudge1,
        DateTime nudge1Time,
        DateTime inviteSentTime,
        string inviteGuid,
        string fineTrxId)
        : base(id)
    {
        _event = ev;
        _member = member;
        _memberStatus = memberStatus;
        _inEvent = inEvent;
        _cancelDate = cancelDate;
        _lateCancellation = lateCancellation;
        _inviteStatus = inviteStatus;
        _alternate = alternate;
        _billedForFine = billedForFine;
        _billedTime = billedTime;
        _amountCharged = amountCharged;
        _gotNudge1 = gotNudge1;
        _nudge1Time = nudge1Time;
        _inviteSentTime = inviteSentTime;
        _inviteGuid = inviteGuid;
        _fineTrxId = fineTrxId;
    }


    /**
     * constructor with default values
     */
    public EventMember() : this(
        DbConstants.NoId, // id
        null,  // Event
        null,  // member
        MemberStatus.None,  // memberStatus
        false,  // inEvent
        CommonConstants.NoValueDate,  // cancelDate
        false,  // lateCancellation
        InviteStatus.UnSet,  // InviteStatus
        false,  // alternate
        false,  // billedForFine
        CommonConstants.NoValueDate, // billedTime
        CommonConstants.NoValueDouble,  // amountCharged
        false,  // gotNudge1
        CommonConstants.NoValueDate, // nudge1Time
        CommonConstants.NoValueDate, // inviteSentTime
        null,  // inviteGuid
        null)  // fineTrxId
    {
    }    


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return EventMemberMgr.singleton;
    }

    #region XML
    /**
     * Write the EventMember as xml.  Handles a null EventMember.
     */
    public static void toXml(XmlHelper xh, EventMember em)
    {
        // if em is null, create a fake one
        if (em == null)
            em = new EventMember();

        em.toXml(xh);
    }


    /**
     * Write the EventMember as xml
     */
    public void toXml(XmlHelper xh)
    {
		memberStatusToXml(xh);
        xh.writeSimpleElem("inEvent", _inEvent);
        xh.writeSimpleElem("seated", getSeated()); 
        xh.writeSimpleElem("inviteStatus", _inviteStatus.ToString());

        string warningExplanation = getWarningExplanation();
        if (warningExplanation != null)
            xh.writeSimpleElem("warning", warningExplanation);

        // attended
        // jlf get rid of this
        bool bAttended = _memberStatus == MemberStatus.Attended;
        string sAttended = bAttended.ToString().ToLower();
        xh.writeSimpleElem("attended", sAttended);
    }


    /**
     * JLF temp
     */
    private string getSeated()
    {
        if (_inviteStatus == InviteStatus.UnSet)
            return "UnSet";
        if (_inviteStatus == InviteStatus.NotInvited)
            return "NotSeated";
        else
            return "Seated";
    }


    /**
     * Write the _memberStatus as xml
     */
    public void memberStatusToXml(XmlHelper xh)
    {
        xh.writeSimpleElem("memberStatus", _memberStatus.ToString());
    }


    /**
     * Generate the XML for the fines
     */
	public void fineXml(XmlHelper xh)
	{
        if (_amountCharged <= 0)
            return;

        xh.startElem("Fine", false, null);

        xh.writeSimpleElem("memberId", _member.id);
        xh.writeSimpleElem("name", _member.firstName + " " + _member.lastName);
        xh.writeSimpleElem("amountCharged", _amountCharged.ToString("C"));
        xh.writeSimpleElem("fineTrxId", _fineTrxId);

        string reason = null;
        if (_memberStatus == MemberStatus.NoShow)
            reason = "No Show";
        else if (_memberStatus == MemberStatus.Cancelled)
            reason = "Cancelled " + _cancelDate;
        else
            _log.error("fineXml() Bad _memberStatus: " + _memberStatus);
        xh.writeSimpleElem("reason", reason);

        xh.endElem(); // Fine
	}
    #endregion XML

    /**
     * Get the fulfillment value
     */
    public EventMemberFulfillment getFulfillment()
    {
        bool signedUp = _memberStatus != MemberStatus.None;
        bool invited = (_inviteStatus == InviteStatus.Invited ||
                        _inviteStatus == InviteStatus.InviteExpired);

        if (!signedUp)
            return EventMemberFulfillment.None;

        return invited ? EventMemberFulfillment.Fulfilled :
                         EventMemberFulfillment.Unfulfilled;
    }


    /**
    * EventStatus enum
    */
    public enum Warning : int
    {
	    RsvpedNo      = 1,
        Cancelled     = 2,
        NeedsNudge1   = 4,
        InviteExpired = 8
    }


    /**
     * Get the warning bitmask
     */
    public int getWarning()
    {
        // if they're not in the event or in a locked table, there's no warning
        if (!_inEvent)
            return 0;

        // No warning if the event is finished or cancelled
        if (_event.status == EventStatus.Finished ||
            _event.status == EventStatus.Cancelled)
            return 0;

        Hashtable membersInLockedTables = new Hashtable();
        _event.getCouplesAndMembersInLockedTables(membersInLockedTables, null);
        bool inLockedTable = membersInLockedTables[_member] != null;
        if (!inLockedTable)
            return 0;

        int warning = 0;
        if (_memberStatus == MemberStatus.RsvpedNo)
            warning |= (int) Warning.RsvpedNo;
        else if (_memberStatus == MemberStatus.Cancelled)
            warning |= (int) Warning.Cancelled;

        if (_inviteStatus == InviteStatus.InviteExpired)
            warning |= (int) Warning.InviteExpired;
        else if (needsNudge1())
            warning |= (int) Warning.NeedsNudge1;

        return warning;
    }

    /**
     * Get the warning explanation
     */
    public string getWarningExplanation()
    {
        int warning = getWarning();
        if (warning == 0)
            return null;
        StringBuilder sb = new StringBuilder();
        if ((warning & (int) Warning.RsvpedNo) > 0)
            sb.Append("They've declined their invitation.  ");
        if ((warning & (int) Warning.Cancelled) > 0)
            sb.Append("They've cancelled their invitation.  ");
        if ((warning & (int) Warning.NeedsNudge1) > 0)
            sb.Append("They need their phone nudge.  ");
        if ((warning & (int) Warning.InviteExpired) > 0)
            sb.Append("Their invitation has expired.  ");
        
        return sb.ToString();
    }


    /**
     * Figure out if the member needs nudge1
     */
    private bool needsNudge1()
    {
        if (_memberStatus != MemberStatus.None &&
            _memberStatus != MemberStatus.SignedUp)
        {
            return false;
        }
        if (_event.status == EventStatus.Finished ||
            _event.status == EventStatus.Cancelled ||
            _inviteStatus != InviteStatus.Invited ||
            _gotNudge1)
        {
            return false;
        }

        DateTime sendNudge1Time = DateTime.Now.Subtract(EmailConfig.singleton.getNudge1TimeSpan());
        return _inviteSentTime < sendNudge1Time;
    }


    /**
     * Figure out the fine based on the member's status
     */
    public void computeFine(bool bSave)
    {
        _amountCharged = computeFine();
        if (bSave)
            save();
    }

    /**
     * Figure out the fine based on the member's status
     */
    private double computeFine()
    {
        if (_memberStatus == MemberStatus.NoShow)
            return 50;

        if (_memberStatus != MemberStatus.Cancelled)
            return 0;

        return _event.computeFine(_cancelDate);
    }

}
}
