using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



/**
 * Manager everything related to an event's members.
 * Reads and updates these table: EventMember.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventMembers
{
    private static Log _log = new Log("EventMembers");

    private Event _event;

    // ArrayList[EventMember]
    private ArrayList _eventMembers;

    // Hashtable[ int memberId --> EventMember ]
    private Hashtable _c_eventMembersHash;

    // ArrayList[Member]
    private ArrayList _c_sortedMembers;

    /**
     * constructor
     */
    public EventMembers(Event ev)
    {
        _event = ev;
    }


    /**
     * Get the members in the event.  inEventOnly means only get the members
     * where inEvent == true.
     * return ArrayList[Member]
     */
    public ArrayList getMembers(bool inEventOnly)
    {
        ArrayList members = new ArrayList();
        foreach (EventMember em in _eventMembers)
            if (!inEventOnly || em.inEvent)
                members.Add(em.member);

        return members;
    }


    /**
     * Returns the member in the event with the given member id.
     */
    public Member getMember(int memberId)
    {
        EventMember em = getEventMember(memberId);
        if (em == null)
            return null;
        return em.member;
    }


    /**
     * Returns the EventMember in the event with the given member id.
     */
    public EventMember getEventMember(int memberId)
    {
        init("_c_eventMembersHash");
        if (_c_eventMembersHash.Count == 0)
            return null;
        return (EventMember) _c_eventMembersHash[memberId];
    }


    /**
     * Sets _eventMembers.
     * eventMembers = ArrayList[EventMember]
     */
    public void addEventMember(EventMember em)
    {
        if (getEventMember(em.member.id) != null)
            _log.error("addEventMember() EventMember is already in the " + 
                       "event.  member.id=" + em.member.id);

        _eventMembers.Add(em);
        clear();
    }


    /**
     * returns _eventMembers.
     * ArrayList[EventMember]
     */
    public ArrayList getEventMembers()
    {
        return _eventMembers;
    }


    /**
     * Sets _eventMembers.
     * eventMembers = ArrayList[EventMember]
     */
    public void setEventMembers(ArrayList eventMembers)
    {
        _eventMembers = eventMembers;
        clear();
    }


    /**
     * Sort the members to move couples to one side.
     * ArrayList[Member]
     */
    public ArrayList sortMembers()
    {
        init("_c_sortedMembers");
        return _c_sortedMembers;
    }



    /**
     * Initialize class members that are created on demand.
     */
    private void init(string name)
    {
        if (name == "_c_eventMembersHash" && _c_eventMembersHash == null)
            initEventMembersHash();
        else if (name == "_c_sortedMembers" && _c_sortedMembers == null)
            initSortMembers();
    }



    /**
     * Clears class members that are created on demand.
     */
    private void clear()
    {
        _c_eventMembersHash = null;
        _c_sortedMembers = null;
    }


    /**
     * Create _c_eventMembersHash
     */
    private void initEventMembersHash()
    {
        _c_eventMembersHash = new Hashtable();
        if (_eventMembers == null)
            return;

        foreach (EventMember em in _eventMembers)
            _c_eventMembersHash[em.member.id] = em;
    }


    /**
     * Creates _c_sortedMembers
     */
    public void initSortMembers()
    {
        ArrayList members = getMembers(true);

        // sort members by gender
        ArrayList men = new ArrayList();
        ArrayList women = new ArrayList();
        foreach (Member member in members)
        {
            if (member.gender == CommonConstants.Male)
                men.Add(member);
            else
                women.Add(member);
        }
        members = men;
        foreach (Member member in women)
            members.Add(member);

        // how many men and women
        int numWomen = women.Count;
        int numMen = members.Count - numWomen;

        // keep track of "always" and "coupled" cells 
        // Hashtable[member1] = MemberGridCell
        Hashtable alwaysCells = new Hashtable();
        Hashtable coupledCells = new Hashtable();
        MemberGridCell[][] cells = _event.memberGrid.cells;
        for (int r = numMen - 1; r < _event.memberGrid.size; r++) // women
        {
            Member female = null;
            Member alwaysMale = null;
            Member coupledMale = null;
            for (int c = 0; c < numMen; c++) // men
            {
                MemberGridCell cell = (MemberGridCell) cells[r][c];
                Member male = cell.member2;
                female = cell.member1;
                if (cell.compatibility.isAlways)
                {
                    alwaysMale = male;
                    break;
                }
                else if (cell.coupling == BoConstants.CouplingServerSetTrue)
                    coupledMale = male;
            }
            if (alwaysMale != null)
                alwaysCells[alwaysMale] = female;
            else if (coupledMale != null)
                coupledCells[coupledMale] = female;
        }

        ArrayList alwaysMales = new ArrayList();
        ArrayList coupledMales = new ArrayList();
        ArrayList alwaysFemales = new ArrayList();
        ArrayList coupledFemales = new ArrayList();
        ArrayList otherMales = new ArrayList();
        Hashtable pairedFemales = new Hashtable();
        for (int i = 0; i < numMen; i++)
        {
            Member male = (Member) members[i];
            Member female = (Member) alwaysCells[male];
            if (female != null)
            {
                alwaysMales.Add(male);
                alwaysFemales.Add(female);
                pairedFemales[female] = "";
                continue;
            }

            female = (Member) coupledCells[male];
            if (female != null)
            {
                coupledMales.Add(male);
                coupledFemales.Add(female);
                pairedFemales[female] = "";
                continue;
            }

            otherMales.Add(male);
        }

        ArrayList otherFemales = new ArrayList();
        foreach (Member female in members)
        {
            if (female.gender == CommonConstants.Male)
                continue;
            if (pairedFemales[female] == null)
                otherFemales.Add(female);
        }

        _c_sortedMembers = new ArrayList();
        foreach (Member member in alwaysMales)
            _c_sortedMembers.Add(member);
        foreach (Member member in coupledMales)
            _c_sortedMembers.Add(member);
        foreach (Member member in otherMales)
            _c_sortedMembers.Add(member);
        foreach (Member member in alwaysFemales)
            _c_sortedMembers.Add(member);
        foreach (Member member in coupledFemales)
            _c_sortedMembers.Add(member);
        foreach (Member member in otherFemales)
            _c_sortedMembers.Add(member);
    }
}
}
