using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the event table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventMgr : HasMiscAttributesMgr
{
    private static Log _log = new Log("EventMgr");

    private static EventMgr _singleton;
    public static EventMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new EventMgr();
            return _singleton;
        } 
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "EventID"; }


    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return BoConstants.EventTable;
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        Event ev = (Event) dbObject;
        ArrayList SqlParams = setParams(ev, true);
        ev.id = DbUtils.insert("AddEvent", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        Event ev = (Event) dbObject;
        ArrayList SqlParams = setParams(ev, false);
        DbUtils.update("UpdateEvent", SqlParams);

        // need to update the tables because setting the event's
        // venue and host could set the tables'
        if (ev.tables != null)
            foreach (Table table in ev.tables)
                table.save();
    }


    /**
     * Sets command Parameters for everything in host
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(Event ev, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@eventId", ev));

        sqlParameters.Add(DbUtils.createStringParam("@name", ev.name));
        sqlParameters.Add(DbUtils.createDateTimeParam("@date", ev.getStartTime(true)));
        sqlParameters.Add(DbUtils.createStringParam("@city", null)); // not used
        sqlParameters.Add(DbUtils.createStringParam("@notes", ev.notes));
        sqlParameters.Add(DbUtils.createDateTimeParam("@startTime", ev.getStartTime(true)));
        sqlParameters.Add(DbUtils.createDateTimeParam("@endTime", ev.getEndTime(true)));
        sqlParameters.Add(DbUtils.createIntParam("@status", (int) ev.status));
        sqlParameters.Add(DbUtils.createIdParam("@venueId", ev.venue));
        sqlParameters.Add(DbUtils.createIdParam("@hostId", ev.host));
        sqlParameters.Add(DbUtils.createBoolParam("@multiVenue", ev.multiVenue));
        sqlParameters.Add(DbUtils.createStringParam("@attire", ev.attire));
        sqlParameters.Add(DbUtils.createStringParam("@specialInstructions", ev.specialInstructions));
        sqlParameters.Add(DbUtils.createDoubleParam("@price", ev.price));
        sqlParameters.Add(DbUtils.createDateTimeParam("@finishedTime", ev.finishedTime));
        sqlParameters.Add(DbUtils.createBoolParam("@excursion", ev.excursion));

        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@eventId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        // save venue and host ids in empty objects
        int venueId = DbUtils.getIdFromReader(reader, "VenueId");
        Venue venue = (Venue) 
            VenueMgr.singleton.createEmptyDbObject(venueId);

        int hostId = DbUtils.getIdFromReader(reader, "HostId");
        Host host = (Host) 
            HostMgr.singleton.createEmptyDbObject(hostId);

        Event ev = new Event(
            (int)(decimal) reader[getIdColumn()],
            DbUtils.getStringFromReader(reader, "name"),
            DbUtils.getDateTimeFromReader(reader, "date"),
            DbUtils.getStringFromReader(reader, "notes"),
            DbUtils.getDateTimeFromReader(reader, "startTime"),
            DbUtils.getDateTimeFromReader(reader, "endTime"),
            (EventStatus) DbUtils.getIntFromReader(reader, "status"),
            venue,
            host,
            DbUtils.getBoolFromReader(reader, "multiVenue"),
            DbUtils.getStringFromReader(reader, "attire"),
            DbUtils.getStringFromReader(reader, "specialInstructions"),
            DbUtils.getDoubleFromReader(reader, "price"),
            DbUtils.getDateTimeFromReader(reader, "finishedTime"),
            DbUtils.getBoolFromReader(reader, "excursion"));

        return ev;
    }


    /**
     * Called after DbObjectMgr loads the object.  Subclasses can override
     * this to do more work such as loading associated objects. 
     */
    public override void afterLoad(DbObject dbObject) 
    {
        Event ev = (Event) dbObject;

        // load the objects associated with the event.  The order is 
        // important since the object later in the order depend on
        // the previous objects, e.g. the MemberGrid depends on the Members.
        // So, if an object or objects are null for the event, we return
        // without loading the subsequent objects.

		//load Misc Attributes
		loadMiscAttributes((HasMiscAttributes) dbObject);

        // venue
        ev.venue = (Venue) ev.venue.emptyToReal();
        ev.host = (Host) ev.host.emptyToReal();

        // load the EventMembers.  ArrayList[EventMember]
        ArrayList eventMembers = EventMemberMgr.singleton.loadEventMembers(ev);
        ev.setEventMembers(eventMembers);
        if (eventMembers.Count == 0)
            return;

        // load the MemberGrid
        MemberGrid memberGrid = MemberGridMgr.singleton.loadForEvent(ev);
        if (memberGrid == null)
            return;
        ev.memberGrid = memberGrid;

        // load the tables
        ev.tables = TableMgr.singleton.loadForEvent(ev);
    }


    /**
     * load events
     */
    public override ArrayList loadAll() { return loadAll(false, false); }


    /**
     * load events
     */
    public ArrayList loadAll(bool postedOnly, bool orderByAscendingDate)
    {
        bool showHiddenEvents = Config.getBoolProperty("ShowHiddenEvents");
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@showHiddenEvents", showHiddenEvents),
            new SqlParameter("@postedOnly", postedOnly),
            new SqlParameter("@postedStatus", (int) EventStatus.Posted),
            new SqlParameter("@closedStatus", (int) EventStatus.Closed),
            new SqlParameter("@finishedStatus", (int) EventStatus.Finished),
            new SqlParameter("@orderByAscendingDate", orderByAscendingDate) };

        // create the Events
        return load("GetAllEvents", sqlParameters);
    }


    /**
     * load all events in the past or future
     * returns ArrayList[Event]
     */
    public ArrayList loadPastOrFutureEvents(bool getPast)
    {
        // jlf write a stored proc for this
        ArrayList allEvents = loadAll();
        ArrayList events = new ArrayList();
        foreach (Event ev in allEvents)
        {
            bool isPast = ev.status == EventStatus.Finished;
            if ((getPast && isPast) ||
                (!getPast && !isPast))
                events.Add(ev);
        }

        return events;
    }


    /**
     * load events that the host hosted
     * returns ArrayList[Event]
     */
    public ArrayList loadForHost(int hostId)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@hostId", hostId) };

        // create the Events
        return load("GetEventsForHost", sqlParameters);
    }


    /**
     * Replaces the current EventMembers with members and returns the new
     * ArrayList[EventMember]
     */
    public ArrayList replaceEventMembers(Event ev, ArrayList members)
    {
        // exclude current members
        EventMemberMgr.singleton.excludeAllForEvent(ev.id);

        // include new members
        foreach (Member member in members)
        {
            // if EventMember doesn't exist, create it.
            EventMember eventMember = ev.getEventMember(member.id);
            if (eventMember == null)
            {
                eventMember = new EventMember();
                eventMember.ev = ev;
                eventMember.member = member;
                eventMember.inEvent = true;
            }
            else
                eventMember.inEvent = true;

            EventMemberMgr.singleton.save(eventMember);
        }

        // members are loaded from the db in a specific order
        // which is likely different from the array passed into 
        // this method.  So, for simplicity, we just get them from the db again.
        return EventMemberMgr.singleton.loadEventMembers(ev);
    }



    /**
     * Gets some values from the most recent event for the given venue.
     * Used by EventInfoByVenueAjax
     */
    public void getEventInfoByVenue(
        int venueId, 
        out string attire, 
        out string specialInstructions, 
        out double price)
    {
        attire = null;
        specialInstructions = null;
        price = CommonConstants.NoValueDouble;

        SqlConnection conn = null;
        SqlCommand command = null;
        SqlDataReader reader = null;

        try
        {
            // call the stored proc
            conn = DbUtils.getConnection();
            command = DbUtils.getStoredProcCommand(conn, "GetEventInfoByVenue");
            command.Parameters.Add("@venueId", SqlDbType.Int).Value = venueId;
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                attire = DbUtils.getStringFromReader(reader, "attire");
                specialInstructions = DbUtils.getStringFromReader(reader, "specialInstructions");
                price = DbUtils.getDoubleFromReader(reader, "price");
            }
        }

        finally
        {
            DbUtils.cleanUp(ref conn, ref command, ref reader);
        }
    }

    
}

}
