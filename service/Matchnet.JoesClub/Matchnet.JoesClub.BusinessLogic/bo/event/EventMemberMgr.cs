using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the EventMember table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventMemberMgr : DbObjectMgr
{

    #region basic stuff
    private static Log _log = new Log("EventMemberMgr");

    private static EventMemberMgr _singleton;
    public static EventMemberMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new EventMemberMgr();
            return _singleton;
        } 
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "EventMemberID"; }
  

    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "EventMember";
    }
    #endregion


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        EventMember eventMember = (EventMember) dbObject;
        ArrayList SqlParams = setParams(eventMember, true);
        eventMember.id = DbUtils.insert("AddEventMember", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        EventMember eventMember = (EventMember) dbObject;
        ArrayList SqlParams = setParams(eventMember, false);
        DbUtils.update("UpdateEventMember", SqlParams);
    }


    /**
     * Sets command Parameters for everything in host
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(EventMember eventMember, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@eventMemberId", eventMember));

        sqlParameters.Add(DbUtils.createIdParam("@eventId", eventMember.ev));
        sqlParameters.Add(DbUtils.createIdParam("@memberId", eventMember.member));
        sqlParameters.Add(DbUtils.createIntParam("@memberStatus", (int) eventMember.memberStatus));
        sqlParameters.Add(DbUtils.createBoolParam("@inEvent", eventMember.inEvent));
        sqlParameters.Add(DbUtils.createDateTimeParam("@cancelDate", eventMember.cancelDate));
        sqlParameters.Add(DbUtils.createBoolParam("@lateCancellation", eventMember.lateCancellation));
        sqlParameters.Add(DbUtils.createIntParam("@inviteStatus", (int) eventMember.inviteStatus));
        sqlParameters.Add(DbUtils.createBoolParam("@alternate", eventMember.alternate));
        sqlParameters.Add(DbUtils.createBoolParam("@billedForFine", eventMember.billedForFine));
        sqlParameters.Add(DbUtils.createDateTimeParam("@billedTime", eventMember.billedTime));        
        sqlParameters.Add(DbUtils.createDoubleParam("@amountCharged", eventMember.amountCharged));
        sqlParameters.Add(DbUtils.createBoolParam("@gotNudge1", eventMember.gotNudge1));
        sqlParameters.Add(DbUtils.createDateTimeParam("@nudge1Time", eventMember.nudge1Time));
        sqlParameters.Add(DbUtils.createDateTimeParam("@inviteSentTime", eventMember.inviteSentTime));
        sqlParameters.Add(DbUtils.createStringParam("@inviteGuid", eventMember.inviteGuid));
        sqlParameters.Add(DbUtils.createStringParam("@fineTrxId", eventMember.fineTrxId));

        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@eventMemberId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        throw new NotSupportedException();
    }


    /**
     * load the object at the reader's current row
     */
    public EventMember load(Event ev, Member member, SqlDataReader reader)
    {
        EventMember EventMember = new EventMember(
            (int)(decimal) reader["EventMemberId"],
            ev,
            member,
            (MemberStatus) DbUtils.getIntFromReader(reader, "memberStatus"),
            DbUtils.getBoolFromReader(reader, "inEvent"),
            DbUtils.getDateTimeFromReader(reader, "cancelDate"),
            DbUtils.getBoolFromReader(reader, "lateCancellation"),
            (InviteStatus) DbUtils.getIntFromReader(reader, "inviteStatus"),
            DbUtils.getBoolFromReader(reader, "alternate"),
            DbUtils.getBoolFromReader(reader, "billedForFine"),
            DbUtils.getDateTimeFromReader(reader, "billedTime"),
            DbUtils.getDoubleFromReader(reader, "amountCharged"),
            DbUtils.getBoolFromReader(reader, "gotNudge1"),
            DbUtils.getDateTimeFromReader(reader, "nudge1Time"),
            DbUtils.getDateTimeFromReader(reader, "inviteSentTime"),
            DbUtils.getStringFromReader(reader, "inviteGuid"),
            DbUtils.getStringFromReader(reader, "fineTrxId"));
        return EventMember;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(int id)
    {
        throw new NotSupportedException();
    }


    /**
     * load the Members for an event
     * ArrayList[EventMember]
     */
    public ArrayList loadEventMembers(Event ev)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("GetEventMembers", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@eventId", SqlDbType.Int).Value = ev.id;
            reader = command.ExecuteReader();

            ArrayList eventMembers = new ArrayList();
            while (reader.Read())
            {
                Member member = (Member) MemberMgr.singleton.load(reader);
                MemberMgr.singleton.afterLoad(member);

                EventMember eventMember = load(ev, member, reader);
                afterLoad(eventMember);

                eventMembers.Add(eventMember);
            }

            return eventMembers;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * load the Members for the given Member.  Warning: the EventMembers created here
     * will have _event == null.
     * ArrayList[EventMember]
     */
    public ArrayList loadEventMembers(Member member)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("GetEventMembersForMember", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(DbUtils.createIdParam("@memberId", member));
            reader = command.ExecuteReader();

            ArrayList eventMembers = new ArrayList();
            while (reader.Read())
            {
                EventMember eventMember = load(null, member, reader);
                eventMembers.Add(eventMember);
            }

            return eventMembers;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }



    /**
     * Deletes the EventMembers for the given event
     */
    public void excludeAllForEvent(int eventId)
    {
        SqlConnection conn = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("ExcludeAllEventMembers", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
            command.ExecuteNonQuery();
        }

        finally
        {
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Updates the memberStatus for the EventMember for the given event and member.
     * Creates the EventMember if it doesn't exist.
     * This method any side-effect behavior associated with changing the memberStatus,
     * such as sending emails.
     */
    public void updateMemberStatus(Event ev, Member member, MemberStatus memberStatus)
    {
        // if EventMember doesn't exist, create it.
        EventMember eventMember = ev.getEventMember(member.id);
        if (eventMember == null)
        {
            eventMember = new EventMember();
            eventMember.ev = ev;
            eventMember.member = member;
            ev.addEventMember(eventMember);
        }
       
        // set the status.  If it's a cancellation, keep track of the time.
        MemberStatus oldMemberStatus = eventMember.memberStatus;
        eventMember.memberStatus = memberStatus;
        if (memberStatus == MemberStatus.Cancelled)
            eventMember.cancelDate = DateTime.Now;

        // if the event is finished and the memberStatus is changing,
        // compute the fine
        if (ev.status == EventStatus.Finished && 
            memberStatus != oldMemberStatus)
        {
            eventMember.computeFine(false);
        }

        EventMemberMgr.singleton.save(eventMember);

        // send the appropriate email
        EmailInstance ei = null;
        if (memberStatus == MemberStatus.SignedUp)
            ei = ReceivedInviteReq.createEmailInstance(ev, member);

        else if (memberStatus == MemberStatus.RsvpedYes)
            ei = RsvpConfirmation.createEmailInstance(ev, member);

        else if (memberStatus == MemberStatus.RsvpedNo ||
                 memberStatus == MemberStatus.Cancelled)
            ei = MemberCancelled.createEmailInstance(ev, member, memberStatus);
        
        if (ei != null)
            ei.send();
    }


    /**
     * Returns the number of EventMember rows for a member for the given memberStatus value.
     */
    public int getMemberStatusCount(int memberId, MemberStatus memberStatus)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@memberId", memberId),
            new SqlParameter("@memberStatus", (int) memberStatus) };

        return getCountWhere("CountEventMembers", sqlParameters);
    }


    /**
     * Returns the number of late cancellations for the member.
     */
    public int getNumLateCancellations(int memberId)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@memberId", memberId) };

        return getCountWhere("CountLateCancellations", sqlParameters);
    }

    
    /**
     * Returns the id of the most recent event attended by the member.
     */
    public int getLastEventAttended(int memberId)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetLastEventAttended");
            command.Parameters.Add("@memberId", SqlDbType.Int).Value = memberId;
            command.Parameters.Add("@memberStatus", SqlDbType.Int).Value = (int) MemberStatus.Attended;
            reader = command.ExecuteReader();
            if (!reader.Read())
                return DbConstants.NoId;
            return DbUtils.getIdFromReader(reader, "eventId");
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Returns the venues attended by the member.
     * returns ArrayList[Venue]
     */
    public ArrayList getVenuesAttended(int memberId)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@memberId", memberId),
            new SqlParameter("@memberStatus", (int) MemberStatus.Attended) };

        return VenueMgr.singleton.load("GetVenuesAttended", sqlParameters);
    }


    /**
     * Sets EventMember.inviteStatus to NeedsNudge1 or InviteExpired
     * as needed
     */
    public void updateInviteStatus()
    {
        DateTime expirationTime = DateTime.Now.Subtract(EmailConfig.singleton.getInviteExpirationTimeSpan());

        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createDateTimeParam("@expirationTime", expirationTime)};

        DbUtils.runStoredProc("UpdateInviteStatus", sqlParameters);
    }



    /**
     * Returns eventId and memberId for the given EventMember InviteGuid.
     */
    public void getEventMemberByInviteGuid(
        string inviteGuid, out int eventId, out int memberId)
    {
        eventId = CommonConstants.NoValueInt;
        memberId = CommonConstants.NoValueInt;

        SqlConnection conn = null;
        SqlCommand command = null;
        SqlDataReader reader = null;

        try
        {
            // call the stored proc
            conn = DbUtils.getConnection();
            command = DbUtils.getStoredProcCommand(conn, "GetEventMemberByInviteGuid");
            command.Parameters.Add("@inviteGuid", SqlDbType.VarChar).Value = inviteGuid;
            reader = command.ExecuteReader();

            if (reader.Read())
            {
                eventId = DbUtils.getIdFromReader(reader, "eventId");
                memberId = DbUtils.getIdFromReader(reader, "memberId");
            }
        }

        finally
        {
            DbUtils.cleanUp(ref conn, ref command, ref reader);
        }
    }


}
}
