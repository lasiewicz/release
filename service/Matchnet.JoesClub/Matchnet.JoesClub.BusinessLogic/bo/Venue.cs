using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;

namespace Matchnet.JoesClub.BusinessLogic
{
public class Venue : HasMiscAttributes
{
#region members
    private static Log _log = new Log("Venue");

    private string _name;
    private string _address;
    private string _address2;
    private string _city;
    private string _state;
    private string _zipCode;
    private string _webSiteUrl;
    private string _image;
    private string _phone;
    private string _contactName;
    private string _contactPhone;
    private string _contactTitle;
    private string _mapUrl;
    private int _parking;
    private double _parkingFee;
    private string _priceRange;
    private string _notes;
    private bool _deleted;

    // properties
    public string name { get { return _name; } set { _name = value; } }
    public string address { get { return _address; } set { _address = value; } }
    public string address2 { get { return _address2; } set { _address2 = value; } }
    public string city { get { return _city; } set { _city = value; } }
    public string state { get { return _state; } set { _state = value; } }
    public string zipCode { get { return _zipCode; } set { _zipCode = value; } }
    public string webSiteUrl { get { return _webSiteUrl; } set { _webSiteUrl = value; } }
    public string image { get { return _image; } set { _image = value; } }
    public string phone { get { return _phone; } set { _phone = value; } }
    public string contactName { get { return _contactName; } set { _contactName = value; } }
    public string contactPhone { get { return _contactPhone; } set { _contactPhone = value; } }
    public string contactTitle { get { return _contactTitle; } set { _contactTitle = value; } }
    public string mapUrl { get { return _mapUrl; } set { _mapUrl = value; } }
    public int parking { get { return _parking; } set { _parking = value; } }
    public double parkingFee { get { return _parkingFee; } set { _parkingFee = value; } }
    public string priceRange { get { return _priceRange; } set { _priceRange = value; } }
    public string notes { get { return _notes; } set { _notes = value; } }
    public bool deleted { get { return _deleted; } set { _deleted = value; } }

    // MiscAttributes

    public string salesCopy 
    { 
        get { return getStringValue("salesCopy"); }
        set { setValue("salesCopy", value); } 
    }

    // MiscAttributes

    public string salesCopy2
    { 
        get { return getStringValue("salesCopy2"); }
        set { setValue("salesCopy2", value); } 
    }

    public string image2 
    { 
        get { return getStringValue("image2"); }
        set { setValue("image2", value); } 
    }

    public string meetingLocation 
    { 
        get { return getStringValue("meetingLocation"); }
        set { setValue("meetingLocation", value); } 
    }
    
    #endregion


    /**
     * Constructor
     */
    public Venue(
        int id, 
        string name,
        string address,
        string address2,
        string city,
        string state,
        string zipCode,
        string webSiteUrl,
        string image,
        string phone,
        string contactName,
        string contactPhone,
        string contactTitle,
        string mapUrl,
        int parking,
        double parkingFee,
        string priceRange,
        string notes,
        string salesCopy,
        string salesCopy2,
        string image2,
        bool deleted,
        string meetingLocation) : base(id)
    {
        _name = name;
        _address = address;
        _address2 = address2;
        _city = city;
        _state = state;
        _zipCode = zipCode;
        _webSiteUrl = webSiteUrl;
        _image = image;
        _phone = phone;
        _contactName = contactName;
        _contactPhone = contactPhone;
        _contactTitle = contactTitle;
        _mapUrl = mapUrl;
        _parking = parking;
        _parkingFee = parkingFee;
        _priceRange = priceRange;
        _notes = notes;
        _deleted = deleted;
        this.salesCopy = salesCopy;
        this.salesCopy2 = salesCopy2;
        this.image2 = image2;
        this.meetingLocation = meetingLocation;
    }


    /**
     * Constructor.  Creates an default DbObject.
     */
    public Venue() : this(
        DbConstants.NoId, // id
        null, // name
        null, // address
        null, // address2
        null, // city
        null, // state
        null, // zipCode
        null, // webSiteUrl
        null, // image
        null, // phone
        null, // contactName
        null, // contactPhone
        null, // contactTitle
        null, // mapUrl
        CommonConstants.EmptyMask, // parking
        0.0,  // parkingFee
        null, // priceRange
        null, // priceRange
        null, // salesCopy
        null, // salesCopy2
        null, // image2
        false, // deleted
        null) // meetingLocation
    {
    }


    /**
     * Constructor.  Creates an empty DbObject.
     */
    public Venue(int id) : base(id, true)
    {
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return VenueMgr.singleton;
    }


    /**
     * generate xml containg the name and is for a select list
     */
    public static void selectListXml(XmlHelper xh)
    {
        xh.startElem("venues", false, null);

        ArrayList venues = VenueMgr.singleton.loadAll();
        foreach (Venue venue in venues)
        {
            xh.startElem("venue", false, null);
            xh.writeSimpleElem("id", venue.id.ToString());
            xh.writeSimpleElem("name", venue._name);
            xh.endElem(); // venue
        }
        xh.endElem(); // venues
    }

	/**
	 * generate xml
	 */
	public void toXml(XmlHelper xh)
	{
		toXml(xh, false);
	}

    /**
     * generate xml
     */
    public void toXml(XmlHelper xh, bool full)
    {
        xh.startElem("venue", false, null);

        xh.writeSimpleElem("id", id.ToString());
        xh.writeSimpleElem("name", _name);
        xh.writeSimpleElem("city", _city);
		xh.writeSimpleElem("address", _address);
		xh.writeSimpleElem("address2", _address2);
		xh.writeSimpleElem("image", _image);
		xh.writeSimpleElem("salesCopy", salesCopy);

		if (full)
		{
			xh.writeSimpleElem("state", _state);
			xh.writeSimpleElem("zip", _zipCode);
			xh.writeSimpleElem("webSiteUrl", _webSiteUrl);
			xh.writeSimpleElem("phone", _phone);
			xh.writeSimpleElem("mapUrl", _mapUrl);

            string sParking = Enums.venueParking.getCommaSepString(_parking);
			xh.writeSimpleElem("parking", sParking);
			
            xh.writeSimpleElem("parkingFee", _parkingFee.ToString());
			xh.writeSimpleElem("priceRange", _priceRange);
			xh.writeSimpleElem("notes", _notes);
			xh.writeSimpleElem("salesCopy2", salesCopy2);
			xh.writeSimpleElem("image2", image2);
		}

        xh.endElem(); // venue
    }


    /**
     * generate <venues> containing all venues
     */
    public static void allToXml(XmlHelper xh)
    {
        xh.startElem("venues", false, null);

        ArrayList venues = VenueMgr.singleton.loadAllByName();
        foreach (Venue venue in venues)
            venue.toXml(xh);

        xh.endElem(); // venues
    }

}

}