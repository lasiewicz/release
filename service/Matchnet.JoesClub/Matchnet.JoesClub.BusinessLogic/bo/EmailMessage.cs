using System;
using Matchnet.JoesClub.ValueObjects;


/**
 * Represents an email message
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailMessage
{ 
    private static Log _log = new Log("EmailMessage");

    // properties
    private string _id;
    public string id { get { return _id; } set { _id = value; } }

    private string _title;
    public string title { get { return _title; } set { _title = value; } }

    private string _whenInstructions;
    public string whenInstructions { get { return _whenInstructions; } set { _whenInstructions = value; } }

    private DateTime _sent;
    public DateTime sent { get { return _sent; } set { _sent = value; } }
    

    /**
     * Constructor
     */
    public EmailMessage(string id, string title, string whenInstructions, DateTime sent)
    {
        _id = id;
        _title = title;
        _whenInstructions = whenInstructions;
        _sent = sent;
    }


    /**
     * generate xml for the object
     * <email>
     *     <title>Event Details Set</title>
     *     <whenInstructions>send 5 days prior to event</whenInstructions>
     *     <sent>Sent 3/30/05</sent>
     * </email>
     */
    public void toXml(XmlHelper xh)
    {
        xh.startElem("email", false, new string[] { 
            "id", _id });

        xh.startElem("title", false, null);
        xh.writeText(_title);
        xh.endElem(); // title

        xh.startElem("whenInstructions", false, null);
        xh.writeText(_whenInstructions);
        xh.endElem(); // whenInstructions

        string sSent = "";
        if (_sent != CommonConstants.DateTimeNone)
            sSent = "Sent " + _sent.ToString("M/d/yy");
        xh.startElem("sent", false, null);
        xh.writeText(sSent);
        xh.endElem(); // sent

        xh.endElem(); // email
    }
}

}