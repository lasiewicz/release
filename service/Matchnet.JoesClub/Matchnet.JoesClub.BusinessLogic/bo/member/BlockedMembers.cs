using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;

using Matchnet.JoesClub.ValueObjects;

/**
 * Keeps track of the members that are blocked by a particular member.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class BlockedMembers : Logable
{
    private static Log _log = new Log("BlockedMembers");

    private int _memberId;
    public int memberId { get { return _memberId; } }

    // Hashtable[ member.id --> "" ]
    private Hashtable _blocked;


    /**
     * Constructor.  Create the BlockedMembers object for the given member
     */
    public BlockedMembers(int memberId) : this(memberId, new Hashtable())
    {
        load();
    }


    /**
     * constructor
     */
    private BlockedMembers(int memberId, Hashtable blocked)
    {
        _memberId = memberId;
        _blocked = blocked;
    }


    /**
     * Returns true if the member is blocked
     */
    public bool blocked(int memberId)
    {
        return _blocked.ContainsKey(memberId);
    }


    /**
     * Returns the blocked members
     * Returns ICollection[int blockedMemberId]
     */
    public ArrayList getBlockedMembers()
    {
        return new ArrayList(_blocked.Keys);
    }


    /**
     * Sets the blocked members
     * blockedMemberIds = Arraylist[int blockedMemberId]
     */
    public void setBlockedMembers(ArrayList blockedMemberIds)
    {
        _blocked = new Hashtable();
        foreach (int blockedMemberId in blockedMemberIds)
            _blocked[blockedMemberId] = "";
    }


    /**
     * Saves the blocked members in the database.
     */
    public void save()
    {
        // get the BlockedMembers from the db
        BlockedMembers savedBlockedMembers = new BlockedMembers(_memberId);

        // add new ones
        foreach (int blockedId in _blocked.Keys)
            if (!savedBlockedMembers._blocked.ContainsKey(blockedId))
                insert(blockedId);

        // delete missing ones
        foreach (int blockedId in savedBlockedMembers._blocked.Keys)
            if (!_blocked.ContainsKey(blockedId))
                delete(blockedId);
    }


    /**
     * insert into db
     */
    private void insert(int blockedId)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("AddBlockedMember", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@memberId", SqlDbType.Int).Value = _memberId;
            command.Parameters.Add("@blockedId", SqlDbType.Int).Value = blockedId;
            command.Parameters.Add(DbUtils.createIdOutParam("@blockedMemberId"));
            reader = command.ExecuteReader();
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * delete from db
     */
    private void delete(int blockedId)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@memberId", _memberId),
            new SqlParameter("@blockedId", blockedId) };
        DbUtils.runStoredProc("DeleteBlockedMember", sqlParameters);
    }


    /**
     * load from db
     */
    private void load()
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetBlockedMembers");
            command.Parameters.Add("@memberId", SqlDbType.Int).Value = _memberId;
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                int blockedId = (int)(decimal) reader["blockedId"];
                _blocked[blockedId] = "";
            }
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * returns ArrayList[BlockedMemberVO]
     */
    public static ArrayList loadBlockedMemberVOs(int memberId)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetBlockedMembers");
            command.Parameters.Add("@memberId", SqlDbType.Int).Value = memberId;
            reader = command.ExecuteReader();

            ArrayList blockedMemberVOs = new ArrayList();
            while (reader.Read())
            {
                BlockedMemberVO bm = new BlockedMemberVO(
                    DbUtils.getIdFromReader(reader, "blockedId"),
                    DbUtils.getStringFromReader(reader, "firstName"),
                    DbUtils.getStringFromReader(reader, "lastName"));

                blockedMemberVOs.Add(bm);
            }

            return blockedMemberVOs;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    #region Logable Members

    public void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_memberId", _memberId);
        objectLogger.log("_blocked", _blocked);
    }
    #endregion
}

}