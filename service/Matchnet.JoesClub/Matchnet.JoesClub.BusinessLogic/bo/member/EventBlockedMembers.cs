using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



/**
 * Keeps track of the members that are blocked for a particular event
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventBlockedMembers
{
    private static Log _log = new Log("EventBlockedMembers");

    private int _eventId;

    // Hashtable[ member1.id --> Hashtable[ member2.id --> "" ] ]
    private Hashtable _blocked;


    /**
     * constructor
     */
    public EventBlockedMembers(Event ev)
    {
        _eventId = ev.id;
        _blocked = new Hashtable();
        load();
    }


    /**
     * Returns true if the either member blocks the other
     */
    public bool blocked(Member member1, Member member2)
    {
        return blocked(member1.id, member2.id) ||
               blocked(member2.id, member1.id);
    }


    /**
     * Returns true if the member1 blocks member2
     */
    private bool blocked(int memberId1, int memberId2)
    {
        Hashtable blocked = (Hashtable) _blocked[memberId1];
        if (blocked == null)
            return false;
        return blocked.ContainsKey(memberId2);
    }


    /**
     * load from db
     */
    private void load()
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetEventBlockedMembers");
            command.Parameters.Add("@eventId", SqlDbType.Int).Value = _eventId;
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                int memberId = (int)(decimal) reader[0];
                int blockedId = (int)(decimal) reader[1];

                Hashtable blocked = (Hashtable) _blocked[memberId];
                if (blocked == null)
                {
                    blocked = new Hashtable();
                    _blocked[memberId] = blocked;
                }

                blocked[blockedId] = "";
            }
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }
}

}