using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the member table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberMgr : HasMiscAttributesMgr
{
    private static Log _log = new Log("MemberMgr");

    private static MemberMgr _singleton;
    public static MemberMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new MemberMgr();
            return _singleton;
        } 
    }


    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return BoConstants.MemberTable;
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "MemberID"; }


    /**
     * load all members.  Orders by name
     */
    public override ArrayList loadAll()
    {
        return loadAll(false);
    }


    /**
     * load all members.  Orders by name
     */
    public ArrayList loadAll(bool activeOnly)
    {
        bool showDeleted = Config.getBoolProperty("ShowDeletedMembers");
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@showDeleted", showDeleted),
            new SqlParameter("@orderByName", true),
            new SqlParameter("@activeOnly", activeOnly) };

        return load("GetAllMembers", sqlParameters);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        Member member = (Member) dbObject;
        ArrayList SqlParams = setParams(member, true);
        member.id = DbUtils.insert("AddMember", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        Member member = (Member) dbObject;
        ArrayList SqlParams = setParams(member, false);
        DbUtils.update("UpdateMember", SqlParams);
    }


    /**
     * Sets command Parameters for everything in member
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(Member member, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@memberId", member));

        sqlParameters.Add(DbUtils.createIntParam("@sparkId", member.sparkId));
        sqlParameters.Add(DbUtils.createBoolParam("@gender", member.gender));
        sqlParameters.Add(DbUtils.createStringParam("@image", member.image));
        sqlParameters.Add(DbUtils.createStringParam("@firstName", member.firstName));
        sqlParameters.Add(DbUtils.createStringParam("@lastName", member.lastName));
        sqlParameters.Add(DbUtils.createStringParam("@userName", member.userName));
        sqlParameters.Add(DbUtils.createStringParam("@city", member.city));
        sqlParameters.Add(DbUtils.createStringParam("@state", member.state));
        sqlParameters.Add(DbUtils.createStringParam("@zip", member.zipCode));
        sqlParameters.Add(DbUtils.createStringParam("@emailAdd", member.emailAdd));
        sqlParameters.Add(DbUtils.createStringParam("@emailAdd2", member.emailAdd2));
        sqlParameters.Add(DbUtils.createStringParam("@phoneNum", member.phone));
        sqlParameters.Add(DbUtils.createStringParam("@altPhone", member.phone2));
        sqlParameters.Add(DbUtils.createIntParam("@shortNot", member.shortNotice ? 1 : 0));
        sqlParameters.Add(DbUtils.createIntParam("@age", member.age));
        sqlParameters.Add(DbUtils.createIntParam("@desiredAgeLow", member.desiredAgeLow));
        sqlParameters.Add(DbUtils.createIntParam("@desiredAgeHigh", member.desiredAgeHigh));
        sqlParameters.Add(DbUtils.createIntParam("@height",   member.height));
        sqlParameters.Add(DbUtils.createIntParam("@bodyType",   member.bodyType));
        sqlParameters.Add(DbUtils.createIntParam("@education",   member.education));
        sqlParameters.Add(DbUtils.createIntParam("@jdateReligion",   member.jdateReligion));
        sqlParameters.Add(DbUtils.createIntParam("@desiredJDateReligion",   member.desiredJDateReligion));
        sqlParameters.Add(DbUtils.createBoolParam("@smoking", member.smoking));
        sqlParameters.Add(DbUtils.createIntParam("@numKids",   member.numKids));
        sqlParameters.Add(DbUtils.createIntParam("@willDateAParent",   member.willDateAParent));
        sqlParameters.Add(DbUtils.createIntParam("@kidsPref",   member.kidsPref));
        sqlParameters.Add(DbUtils.createIntParam("@desiredKidsPref",   member.desiredKidsPref));
        sqlParameters.Add(DbUtils.createDateTimeParam("@SubStart", member.startDate));
        sqlParameters.Add(DbUtils.createDoubleParam("@SubRate", member.startRate));
        sqlParameters.Add(DbUtils.createDateTimeParam("@renewalDate", member.renewalDate));
        sqlParameters.Add(DbUtils.createDoubleParam("@renewalRate", member.renewalRate));
        sqlParameters.Add(DbUtils.createDateTimeParam("@SubExp", member.expirationDate));
        sqlParameters.Add(DbUtils.createStringParam("@avatar", member.avatar));
        sqlParameters.Add(DbUtils.createIntParam("@activityStatus", (int) member.activityStatus));
        sqlParameters.Add(DbUtils.createIntParam("@meetingSomeone",(int) member.meetingSomeone));
        sqlParameters.Add(DbUtils.createIntParam("@describeMe",   member.describeMe));
        sqlParameters.Add(DbUtils.createIntParam("@phoneType",   (int) member.phoneType));
        sqlParameters.Add(DbUtils.createIntParam("@phoneType2",   (int) member.phoneType2));
        sqlParameters.Add(DbUtils.createBoolParam("@deleted", member.deleted));
        sqlParameters.Add(DbUtils.createIntParam("@hearAboutJoesClub", (int) member.hearAboutJoesClub));
        sqlParameters.Add(DbUtils.createStringParam("@hearAboutJoesClubExplain", member.hearAboutJoesClubExplain));

        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@memberId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        int iShortNotice = DbUtils.getIntFromReader(reader, "shortNot");
        bool bShortNotice = iShortNotice != 0;

        int sparkId = DbUtils.getIntFromReader(reader, "sparkId");
        Member member = Member.create(sparkId);
        member.id = (int)(decimal) reader["memberId"];

        member.image = DbUtils.getStringFromReader(reader, "image");
        member.firstName = DbUtils.getStringFromReader(reader, "firstName");
        member.lastName = DbUtils.getStringFromReader(reader, "lastName");
        member.userName = DbUtils.getStringFromReader(reader, "userName");
        member.emailAdd2 = DbUtils.getStringFromReader(reader, "emailAdd2");
        member.phone = DbUtils.getStringFromReader(reader, "phoneNum");
        member.phone2 = DbUtils.getStringFromReader(reader, "altPhone");
        member.shortNotice = bShortNotice;
        member.desiredAgeLow = DbUtils.getIntFromReader(reader, "desiredAgeLow");
        member.desiredAgeHigh = DbUtils.getIntFromReader(reader, "desiredAgeHigh");
        member.bodyType = DbUtils.getIntFromReader(reader, "bodyType");
        member.education = DbUtils.getIntFromReader(reader, "education");
        member.jdateReligion = DbUtils.getIntFromReader(reader, "jdateReligion");
        member.desiredJDateReligion = DbUtils.getIntFromReader(reader, "desiredJDateReligion");
        member.smoking = DbUtils.getBoolFromReader(reader, "smoking");
        member.willDateAParent = DbUtils.getIntFromReader(reader, "willDateAParent");
        member.kidsPref = DbUtils.getIntFromReader(reader, "kidsPref");
        member.desiredKidsPref = DbUtils.getIntFromReader(reader, "desiredKidsPref");
        member.startDate = DbUtils.getDateTimeFromReader(reader, "SubStart");
        member.startRate = DbUtils.getDoubleFromReader(reader, "SubRate", 0.0);
        member.renewalDate = DbUtils.getDateTimeFromReader(reader, "renewalDate");
        member.renewalRate = DbUtils.getDoubleFromReader(reader, "renewalRate", 0.0);
        member.expirationDate = DbUtils.getDateTimeFromReader(reader, "SubExp");
        member.avatar = DbUtils.getStringFromReader(reader, "avatar");
        member.activityStatus = (MemberActivityStatus) DbUtils.getIntFromReader(reader, "activityStatus");
        member.meetingSomeone = (MeetingSomeone) DbUtils.getIntFromReader(reader, "meetingSomeone");
        member.describeMe = DbUtils.getIntFromReader(reader, "describeMe");
        member.phoneType = (PhoneType) DbUtils.getIntFromReader(reader, "phoneType");
        member.phoneType2 = (PhoneType) DbUtils.getIntFromReader(reader, "phoneType2");
        member.deleted = DbUtils.getBoolFromReader(reader, "deleted");
        member.hearAboutJoesClub = (MemberHearAboutJoesClub) DbUtils.getIntFromReader(reader, "hearAboutJoesClub");
        member.hearAboutJoesClubExplain = DbUtils.getStringFromReader(reader, "hearAboutJoesClubExplain");

        //if (member is NonJDateMember)
        //{
            member.gender = DbUtils.getBoolFromReader(reader, "gender");
            member.city = DbUtils.getStringFromReader(reader, "city");
            member.state = DbUtils.getStringFromReader(reader, "state");
            member.zipCode = DbUtils.getStringFromReader(reader, "zip");
            member.emailAdd = DbUtils.getStringFromReader(reader, "emailAdd");
            member.age = DbUtils.getIntFromReader(reader, "age");
            member.height = DbUtils.getIntFromReader(reader, "height");
            member.weight = BoConstants.NoValueInt; // not in db
            member.numKids = DbUtils.getIntFromReader(reader, "numKids");
        //}

        return member;
    }


    /**
     * Called after DbObjectMgr loads the object.  
     */
    public override void afterLoad(DbObject dbObject)
    {
        Member member = (Member) dbObject;

        // load the PhoneInterviews
        member.phoneInterviews = PhoneInterviewMgr.singleton.loadForMember(member);
    
        base.afterLoad(dbObject);
    }


    /**
     * Return the memberid for the given email address
     */
    public int getMemberIdFromSparkId(int sparkId)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("GetMemberIdFromSparkId", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(DbUtils.createIntParam("@sparkId", sparkId));

            // execute
            reader = command.ExecuteReader();
            if (!reader.Read())
                return DbConstants.NoId;
            int memberId = (int)(decimal) reader[getIdColumn()];
            if (reader.Read())
                _log.error("GetMemberIdFromSparkId() more than one member id for sparkId: " + sparkId);
            return memberId;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Return the memberid for the given email address
     */
    public int getMemberIdFromEmailAddress(string emailAddress)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("GetMemberIdFromEmailAddress", conn);
            command.CommandType = CommandType.StoredProcedure;
            DbUtils.setStringParam("@emailAdd", emailAddress, command);

            // execute
            reader = command.ExecuteReader();
            if (!reader.Read())
                return DbConstants.NoId;
            int memberId = (int)(decimal) reader[getIdColumn()];
            if (reader.Read())
                _log.error("getMemberIdFromEmailAddress() more than one member id for emailAddress: " + emailAddress);
            return memberId;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Returns the id of the most recently event attended by the 
     * member within the last 48 hours
     */
    public int getDefaultViewEvent(int memberId)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            // set up the stored proc call
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("GetEventsAttendedByMember", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(DbUtils.createIdParam("@memberId", memberId));
            command.Parameters.Add(DbUtils.createIntParam("@memberStatusAttended", (int) MemberStatus.Attended));
            command.Parameters.Add(DbUtils.createIntParam("@eventStatusFinished", (int) EventStatus.Finished));

            // execute
            reader = command.ExecuteReader();
            if (!reader.Read())
                return DbConstants.NoId;

            // get the values from the first row of results
            int eventId = DbUtils.getIdFromReader(reader, "eventId");
            DateTime finishedTime = DbUtils.getDateTimeFromReader(reader, "finishedTime");
            
            // if 2 or more days have elapsed since the event finished, return NoId
            TimeSpan ts = DateTime.Now - finishedTime;
            if (ts.Days >= 2)
                return DbConstants.NoId;
            else
                return eventId;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * save the object.  Override so we can save the PhoneInterviews
     */
    public override void save(DbObject dbObject)
    {
        Member member = (Member) dbObject;

        // set the "latestInterviewStatus" misc attr before saving
        member.setLatestInterviewStatus();

		// Check if we should send a welcome email before saving
		if (member.sentWelcomeEmail == false && 
            member.activityStatus == MemberActivityStatus.Member) 
            sendWelcomeEmail(member);
        base.save(member);
        savePhoneInterviews(member);
    }

	private void sendWelcomeEmail(Member m)
	{
		m.sentWelcomeEmail = true;
		EmailInstance welcomeEmail = WelcomeEmail.createEmailInstance(m);
		welcomeEmail.send();
	}


    /**
     * Saves the Member's PhoneInterviews
     */
    public void savePhoneInterviews(Member member)
    {
        ArrayList oldPhoneInterviews = PhoneInterviewMgr.singleton.loadForMember(member);
        Hashtable phoneInterviewIds = new Hashtable();

        // save PhoneInterviews
        foreach (PhoneInterview phoneInterview in member.phoneInterviews)
        {
            phoneInterview.save();
            phoneInterviewIds[phoneInterview.id] = "";
        }

        // delete the PhoneInterview that are gone
        foreach (PhoneInterview oldPhoneInterview in oldPhoneInterviews)
            if (!phoneInterviewIds.ContainsKey(oldPhoneInterview.id))
                PhoneInterviewMgr.singleton.delete(oldPhoneInterview.id);
    }



    /**
     * Returns the memberId for the given Guid.
     */
    public int getMemberByGuid(string memberGuid)
    {
        int memberId = CommonConstants.NoValueInt;

        SqlConnection conn = null;
        SqlCommand command = null;
        SqlDataReader reader = null;

        try
        {
            // call the stored proc
            conn = DbUtils.getConnection();
            command = DbUtils.getStoredProcCommand(conn, "GetMemberByGuid");
            command.Parameters.Add("@guid", SqlDbType.VarChar).Value = memberGuid;
            reader = command.ExecuteReader();

            if (reader.Read())
                memberId = DbUtils.getIdFromReader(reader, "memberId");

            return memberId;
        }

        finally
        {
            DbUtils.cleanUp(ref conn, ref command, ref reader);
        }
    }

}

}