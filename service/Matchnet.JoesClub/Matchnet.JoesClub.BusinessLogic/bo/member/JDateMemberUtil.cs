using System;
using System.Collections;
using System.Configuration;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class JDateMemberUtil
{
    // singleton object
    public static JDateMemberUtil Instance = new JDateMemberUtil();

    private const int JDateBrandId = 1003;
    private const int JDateCommunityId = 3;
    private const int GlobalCommunityId = 8383;
    private const int JDateSiteId = 103;
    private const int EnglishLanguageId = 2;
    private const int NoValueInt = -1;

    private static Brand _jdateBrand;
    public static Brand jdateBrand { get { return _jdateBrand; }}


    /**
     * private constructor
     */
    private JDateMemberUtil() 
    {
        // get the jdate brand
        BrandConfigSA brandConfigSA = BrandConfigSA.Instance;
        _jdateBrand = brandConfigSA.GetBrandByID(JDateBrandId);
    }


    #region methods that use MemberSA

    /**
     * get a member
     */
    public Matchnet.Member.ServiceAdapters.Member getMember(int sparkId)
    {
        if (sparkId <= 0)
            return null;

        Matchnet.Member.ServiceAdapters.Member member = 
            MemberSA.Instance.GetMember(sparkId, MemberLoadFlags.None);

        // see if it's a real member.
        // JLF not sure if we need this
        if (member.EmailAddress == null)
            return null;

        return member;
    }

    /**
     * Get a members spark ID from their email address
     */
    public int getSparkId(string emailAddress)
    {
        AuthenticationResult ar = _authenticate(emailAddress, "");
        if (ar.MemberID <= 0)
            return CommonConstants.NoValueInt;
        return ar.MemberID;
    }


    /**
     * Authenticate a member, return status
     */
    public Matchnet.JoesClub.ValueObjects.AuthenticationStatus authenticate(string emailAddress, string password)
    {
        AuthenticationResult ar = _authenticate(emailAddress, password);
        return (Matchnet.JoesClub.ValueObjects.AuthenticationStatus) ar.Status;
    }


    /**
     * Authenticate a member
     */
    private AuthenticationResult _authenticate(string emailAddress, string password)
    {
        return MemberSA.Instance.Authenticate(_jdateBrand, emailAddress, password);
    }


    #endregion methods that use MemberSA


    /**
     * Copies values from jdate member
     */
    public virtual void getJDateValues(Member jcMember)
    {
        // nothing to do if there's no jdateMember
        Matchnet.Member.ServiceAdapters.Member jdateMember = jcMember.jdateMember;
        if (jdateMember == null)
            return;

        PhotoCommunity pc = jdateMember.GetPhotos(getCommunityId(false));
        if (pc != null && pc.Count > 0)
            jcMember.image = pc[0].ThumbFileWebPath;
        else
            jcMember.image = null;

        // these 4 don't work
        jcMember.firstName = getAttributeText(jdateMember, CachedMember.ATTRIBUTEID_FIRSTNAME, true);
        if (jcMember.firstName == null)
            jcMember.firstName = "";
        jcMember.lastName = getAttributeText(jdateMember, CachedMember.ATTRIBUTEID_LASTNAME, true);
        jcMember.userName = getAttributeText(jdateMember, CachedMember.ATTRIBUTEID_USERNAME, true);
        jcMember.phone = getAttributeText(jdateMember, CachedMember.ATTRIBUTEID_PHONE, true);
        
        jcMember.desiredAgeLow = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDMINAGE, false);
        jcMember.desiredAgeHigh = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDMAXAGE, false);

        jcMember.desiredMaritalStatus = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDMARITALSTATUS, true);
        jcMember.desiredJDateReligion = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDJDATERELIGION, true);
        jcMember.desiredEducationLevel = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDEDUCATIONLEVEL, true);
        jcMember.desiredDrinkingHabits = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDDRINKINGHABITS, true);
        jcMember.desiredSmokingHabits = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_DESIREDSMOKINGHABITS, true);

        int iGenderMask = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_GENDERMASK, false);
        jcMember.gender = (iGenderMask & 2) > 0; // female = 2 = true

        jcMember.city = getRegionLanguage(jdateMember).CityName;
        jcMember.state = getRegionLanguage(jdateMember).StateAbbreviation;
        jcMember.zipCode = getRegionLanguage(jdateMember).PostalCode;
        
        double cm = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_HEIGHT, true);
        jcMember.height = (int) Math.Round((cm / 2.54)); // convert to inches

        jcMember.weight = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_WEIGHT, true); // milligrams
        jcMember.numKids = getAttributeInt(jdateMember, CachedMember.ATTRIBUTEID_CHILDRENCOUNT, true);

        jcMember.birthdate = jdateMember.GetAttributeDate(
            getCommunityId(true), JDateSiteId, JDateBrandId, 
            CachedMember.ATTRIBUTEID_BIRTHDATE, CommonConstants.NoValueDate);
        TimeSpan tsAge = DateTime.Now - jcMember.birthdate;
        jcMember.age = (int) (tsAge.TotalDays / 365.25);
    }

    private RegionLanguage getRegionLanguage(Matchnet.Member.ServiceAdapters.Member jdateMember) 
    {
        int regionID = jdateMember.GetAttributeInt(JDateMemberUtil.jdateBrand, "regionid");
        int languageID = JDateMemberUtil.jdateBrand.Site.LanguageID;
        return RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);
    }
 
    /**
     * get a member text attribute
     */
    private string getAttributeText(Matchnet.Member.ServiceAdapters.Member member, int attrId, bool global)
    {
        return member.GetAttributeText(getCommunityId(global), JDateSiteId, JDateBrandId, 
            EnglishLanguageId, attrId, null);
    }

 
    /**
     * get a member int attribute
     */
    private int getAttributeInt(Matchnet.Member.ServiceAdapters.Member member, int attrId, bool global)
    {
        return member.GetAttributeInt(getCommunityId(global), JDateSiteId, JDateBrandId, 
            attrId, NoValueInt);
    }

 
    /**
     * get the community id
     */
    private int getCommunityId(bool global)
    {
        return global ? GlobalCommunityId : JDateCommunityId;
    }

 
    /**
     * gets the blocked lists for the members with the given sparkIds
     * returns Hashtable[int sparkId --> ArrayList[int sparkId]
     */
    public Hashtable getBlockedLists(int[] sparkIds)
    {
        Hashtable blockedLists = new Hashtable();

        foreach (int sparkId in sparkIds)
        {
            if (sparkId > 0)
            {
                try
                {
                    Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(sparkId);
                    int rowCount;
                    ArrayList blockedSparkIds = list.GetListMembers(
                        HotListCategory.IgnoreList,
                        JDateCommunityId,
                        JDateSiteId,
                        1,
                        1000,
                        out rowCount);

                    if (blockedSparkIds.Count > 0)
                        blockedLists[sparkId] = blockedSparkIds;
                }

                // just eat exceptions.  hopefully there won't be any.
                catch {}
            }
        }

        return blockedLists;
    }
}
}
