using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



/**
 * Keeps track of who sat together.  For now, just hard code 6 month window
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class SeatingHistory
{
    private static Log _log = new Log("SeatingHistory");
    private Event _event;

    // Hashtable[ key(member1,member2) --> DateTime (most recent) ]
    private Hashtable _satTogether;


    /**
     * constructor
     */
    public SeatingHistory(Event ev)
    {
        _event = ev;
        _satTogether = new Hashtable();
        load();
    }


    /**
     * If they've sat together in the last 6 months, returns the most
     * recent date as a string.
     */
    public string satTogether(Member member1, Member member2)
    {
        DateTime when = CommonConstants.DateTimeNone;
        string key = satTogetherKey(member1.id, member2.id);
        if (_satTogether.ContainsKey(key))
            when = (DateTime) _satTogether[key];

        return when == CommonConstants.DateTimeNone ?
            null : when.ToString("M/d/yy");
    }


    /**
     * load from db
     */
    private void load()
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetSeatingHistory");

            DateTime eventDate = (DateTime) _event.date;
            command.Parameters.Add("@memberStatus", SqlDbType.Int).Value = 
                MemberStatus.Attended;
            command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = 
                eventDate.AddMonths(-6);
            command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = 
                eventDate;
    
            reader = command.ExecuteReader();

            // tableId, tableMembers and when are for the current table
            int tableId = DbConstants.NoId;
            ArrayList tableMembers = new ArrayList();
            DateTime when = CommonConstants.DateTimeNone;
            while (reader.Read())
            {
                int memberId = (int)(decimal) reader[0];
                int newTableId = (int)(decimal) reader[1];
                DateTime newWhen = DbUtils.getDateTimeFromReader(reader, 2);

                // add members to tableMembers until we get a different table
                if (newTableId != tableId)
                {
                    if (tableId != DbConstants.NoId)
                        addTableMembers(tableMembers, when);
                    tableMembers.Clear();
                    tableId = newTableId;
                    when = newWhen;
                }
                tableMembers.Add(memberId);
            }

            // do the last table
            if (tableMembers.Count != 0)
                addTableMembers(tableMembers, when);
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Adds all the combinations of people at a table
     */
    private void addTableMembers(ArrayList tableMembers, DateTime when)
    {
        for (int i = 0; i < tableMembers.Count; i++)
            for (int j = i + 1; j < tableMembers.Count; j++)
            {
                int memberId1 = (int) tableMembers[i];
                int memberId2 = (int) tableMembers[j];
                add(memberId1, memberId2, when);
            }
    }


    /**
     * Adds a pair to _satTogether
     */
    private void add(int memberId1, int memberId2, DateTime when)
    {
        add(satTogetherKey(memberId1, memberId2), when);
        add(satTogetherKey(memberId2, memberId1), when);
    }


    /**
     * Adds a key
     */
    private void add(string key, DateTime when)
    {
        // if key is already in _satTogether, see if "when" is more 
        // recent.
        if (_satTogether.ContainsKey(key))
        {
            DateTime oldWhen = (DateTime) _satTogether[key];
            if (when > oldWhen)
                _satTogether[key] = when;
            return;
        }

        if (!_satTogether.ContainsKey(key))
            _satTogether[key] = when;
    }


    /**
     * returns a key for the _satTogether hashtable
     */
    private string satTogetherKey(int memberId1, int memberId2)
    {
        return memberId1.ToString() + "_" + memberId2;
    }
}

}