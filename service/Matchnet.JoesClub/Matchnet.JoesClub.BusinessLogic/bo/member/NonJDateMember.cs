using System;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * A Member with no corresponding JDate member
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class NonJDateMember : Member
{


    /**
     * Constructor
     */
    public NonJDateMember() : base()
    {
    }
}
}

