using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;




/**
 * Used to sync up the members' blocked list from JDate.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class JDateBlockedMembers 
{
    private static Log _log = new Log("JDateBlockedMembers");


    /**
     * Gets the blocked list from JDate and imports into our DB.
     */
    public static void update()
    {
        // get all the members in the system
        ArrayList members = MemberMgr.singleton.loadAll();

        // Hashtable[int sparkId --> Member]
        Hashtable sparkIdToMember = new Hashtable();
        foreach (Member member in members)
            if (member.sparkId > 0)
                sparkIdToMember[member.sparkId] = member;

        // make a int[] of the sparkIds
        int[] sparkIds = new int[sparkIdToMember.Count];
        int i = 0;
        foreach (int sparkId in sparkIdToMember)
            sparkIds[i++] = sparkId;

        // get blocked list from JDate
        // Hashtable[int sparkId --> List[int sparkId]
        Hashtable jdateHash = JDateMemberUtil.Instance.getBlockedLists(sparkIds);

        // update each member in jdateHash
        foreach (int sparkId in jdateHash.Keys)
        {
            // get the blocked members in our DB for the given member
            // hBlockedMemberIds = Hashtable[int blockedMemberId]
            Member member = (Member) sparkIdToMember[sparkId];
            BlockedMembers blockedMembers = new BlockedMembers(member.id);
            ArrayList blockedMemberIds = blockedMembers.getBlockedMembers();
            Hashtable hBlockedMemberIds = Misc.arrayListToHashtable(blockedMemberIds);

            // ArrayList[int sparkId]
            ArrayList jdateBlocked = (ArrayList) jdateHash[sparkId];
            bool changed = false;
            foreach (int blockedSparkId in jdateBlocked)
            {
                // blockedMember will be null if they're not a JoesClub member.
                // If they are a JoesClub member and they're not in our blocked list,
                // add them
                Member blockedMember = (Member) sparkIdToMember[blockedSparkId];
                if (blockedMember != null &&
                    !hBlockedMemberIds.ContainsKey(blockedMember.id))
                {
                    blockedMemberIds.Add(blockedMember.id);
                    changed = true;
                }

                // save changes
                if (changed)
                    blockedMembers.setBlockedMembers(blockedMemberIds);
            }
        }
    }

}

}