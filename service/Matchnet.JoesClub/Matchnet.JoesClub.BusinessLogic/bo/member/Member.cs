using System;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Web;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class Member : HasMiscAttributes, Logable
{
    #region members
    private static Log _log = new Log("Member");

    private int _sparkId;
    private Matchnet.Member.ServiceAdapters.Member _jdateMember;

    private bool _gender;
    private string _city;
    private string _state;
    private string _zipCode;
    private string _emailAdd;
    private DateTime _birthdate;
    private int _age;
    private int _height;
    private int _weight;
    private int _numKids; 

    private string _image;
    private string _firstName;
    private string _lastName;
    private string _userName;
    private string _emailAdd2;
    private string _phone;
    private string _phone2;
    private bool _shortNotice;
    private int _desiredAgeLow;
    private int _desiredAgeHigh;
    private int _bodyType;
    private int _education;
    private int _jdateReligion;
    private int _desiredJDateReligion;
    private bool _smoking;
    private int _willDateAParent; // one of BitMask.willDateAParent
    private int _kidsPref;        // one of BitMask.kidsPref
    private int _desiredKidsPref; // mask of BitMask.kidsPref
    private DateTime _startDate;
    private double _startRate;
    private DateTime _renewalDate;
    private double _renewalRate;
    private DateTime _expirationDate;
    private string _avatar;
    private MemberActivityStatus _activityStatus;
    private MeetingSomeone _meetingSomeone;
    private int _describeMe;
    private PhoneType _phoneType;
    private PhoneType _phoneType2;
    private bool _deleted;
    private MemberHearAboutJoesClub _hearAboutJoesClub;
    private string _hearAboutJoesClubExplain;

    // Hashtable[ int memberId --> "" ] Indicates this 
    // member blocked the other member
    private Hashtable _blocked;

    private ArrayList _phoneInterviews; // ArrayList[PhoneInterview]

    public int sparkId { get { return _sparkId; } set { _sparkId = value; } }
    public Matchnet.Member.ServiceAdapters.Member jdateMember { get { return _jdateMember; } set { _jdateMember = value; } }
    
    public bool gender { get { return _gender; } set { _gender = value; } }
    public string city { get { return _city; } set { _city = value; } }
    public string state { get { return _state; } set { _state = value; } }
    public string zipCode { get { return _zipCode; } set { _zipCode = value; } }
    public string emailAdd { get { return _emailAdd; } set { _emailAdd = value; } }
    public int height { get { return _height; } set { _height = value; } }
    public int weight { get { return _weight; } set { _weight = value; } }
    public int numKids { get { return _numKids; } set { _numKids = value; } }
    public DateTime birthdate { get { return _birthdate; } set { _birthdate = value; } }
    public int age { get { return _age; } set { _age = value; } }
 
    /*
    // we get these from jdate.  They are non-settable so we should remove set()
    public virtual bool gender 
    { 
        get { return JDateMemberUtil.Instance.getGender(_jdateMember); }
        set {}
    }
    public virtual string city 
    { 
        get { return JDateMemberUtil.Instance.getCity(_jdateMember); }
        set {}
    }
    public virtual string state 
    { 
        get { return JDateMemberUtil.Instance.getState(_jdateMember); }
        set {}
    }
    public virtual string zipCode 
    { 
        get { return JDateMemberUtil.Instance.getZipcode(_jdateMember); }
        set {}
    }
    public virtual string emailAdd 
    { 
        get { return JDateMemberUtil.Instance.getEmailAddress(_jdateMember); }
        set {}
    }
    public virtual int height 
    { 
        get { return JDateMemberUtil.Instance.getHeight(_jdateMember); }
        set {}
    }
    public virtual int weight 
    { 
        get { return JDateMemberUtil.Instance.getWeight(_jdateMember); }
        set {}
    }
    public virtual int numKids 
    { 
        get { return JDateMemberUtil.Instance.getNumKids(_jdateMember); }
        set {}
    }
    public virtual DateTime birthdate 
    { 
        get { return JDateMemberUtil.Instance.getBirthdate(_jdateMember); }
        set {}
    }
    public virtual int age 
    { 
        get 
        { 
            TimeSpan age = DateTime.Now - birthdate;
            return (int) (age.TotalDays / 365.25);
        }
        set {}
    }
*/

    public string image { get { return _image; } set { _image = value; } }
    public string firstName { get { return _firstName; } set { _firstName = value; } }
    public string lastName { get { return _lastName; } set { _lastName = value; } }
    public string userName { get { return _userName; } set { _userName = value; } }
    public string emailAdd2 { get { return _emailAdd2; } set { _emailAdd2 = value; } }
    public string phone { get { return _phone; } set { _phone = Misc.normalizePhoneNumber(value); } }
    public string phone2 { get { return _phone2; } set { _phone2 = Misc.normalizePhoneNumber(value); } }
    public bool shortNotice { get { return _shortNotice; } set { _shortNotice = value; } }
    public int desiredAgeLow { get { return _desiredAgeLow; } set { _desiredAgeLow = value; } }
    public int desiredAgeHigh { get { return _desiredAgeHigh; } set { _desiredAgeHigh = value; } }
    public int bodyType { get { return _bodyType; } set { _bodyType = value; } }
    public int education { get { return _education; } set { _education = value; } }
    public int jdateReligion { get { return _jdateReligion; } set { _jdateReligion = value; } }
    public int desiredJDateReligion { get { return _desiredJDateReligion; } set { _desiredJDateReligion = value; } }
    public bool smoking { get { return _smoking; } set { _smoking = value; } }
    public int willDateAParent { get { return _willDateAParent; } set { _willDateAParent = value; } }
    public int kidsPref { get { return _kidsPref; } set { _kidsPref = value; } }
    public int desiredKidsPref { get { return _desiredKidsPref; } set { _desiredKidsPref = value; } }
    public DateTime startDate { get { return _startDate; } set { _startDate = value; } }
    public double startRate { get { return _startRate; } set { _startRate = value; } }
    public DateTime renewalDate { get { return _renewalDate; } set { _renewalDate = value; } }
    public double renewalRate { get { return _renewalRate; } set { _renewalRate = value; } }
    public DateTime expirationDate { get { return _expirationDate; } set { _expirationDate = value; } }
    public string avatar { get { return _avatar; } set { _avatar = value; } }
    public MemberActivityStatus activityStatus { get { return _activityStatus; } set { _activityStatus = value; } }
    public MeetingSomeone meetingSomeone { get { return _meetingSomeone; } set { _meetingSomeone = value; } }
    // "describeMe" is the personality characteristics
    public int describeMe { get { return _describeMe; } set { _describeMe = value; } }
    public PhoneType phoneType { get { return _phoneType; } set { _phoneType = value; } }
    public PhoneType phoneType2 { get { return _phoneType2; } set { _phoneType2 = value; } }
    public bool deleted { get { return _deleted; } set { _deleted = value; } }
    public MemberHearAboutJoesClub hearAboutJoesClub { get { return _hearAboutJoesClub; } set { _hearAboutJoesClub = value; } }
    public string hearAboutJoesClubExplain { get { return _hearAboutJoesClubExplain; } set { _hearAboutJoesClubExplain = value; } }

    // ArrayList[PhoneInterview]
    public ArrayList phoneInterviews { get { return _phoneInterviews; } set { _phoneInterviews = value; } }


    // MiscAttributes

    public MemberWhatAppealedToYou whatAppealedToYou 
    { 
        get { return (MemberWhatAppealedToYou) getIntValue("whatAppealedToYou"); }
        set { setValue("whatAppealedToYou", (int) value); } 
    }

    public string jdateExperience 
    { 
        get { return getStringValue("jdateExperience"); }
        set { setValue("jdateExperience", value); } 
    }

    public MemberBiggestConcern biggestConcern 
    { 
        get { return (MemberBiggestConcern) getIntValue("biggestConcern"); }
        set { setValue("biggestConcern", (int) value); } 
    }

    public MemberPeopleNotice peopleNotice 
    { 
        get { return (MemberPeopleNotice) getIntValue("peopleNotice"); }
        set { setValue("peopleNotice", (int) value); } 
    }

    public MemberOccupation occupation 
    { 
        get { return (MemberOccupation) getIntValue("occupation"); }
        set { setValue("occupation", (int) value); } 
    }

    public MemberSpendTime spendTime 
    { 
        get { return (MemberSpendTime) getIntValue("spendTime"); }
        set { setValue("spendTime", (int) value); } 
    }

    public string spendTimeText
    { 
        get { return getStringValue("spendTimeText"); }
        set { setValue("spendTimeText", value); } 
    }

    public int locationPref 
    { 
        get { return getIntValue("locationPref"); }
        set { setValue("locationPref", value); } 
    }

    public int timePref 
    { 
        get { return getIntValue("timePref"); }
        set { setValue("timePref", value); } 
    }

    public int dietaryRestrictions 
    { 
        get { return getIntValue("dietaryRestrictions"); }
        set { setValue("dietaryRestrictions", value); } 
    }

    public int cuisinesToAvoid
    { 
        get { return getIntValue("cuisinesToAvoid"); }
        set { setValue("cuisinesToAvoid", value); } 
    }

    public int additionalNights
    { 
        get { return getIntValue("additionalNights"); }
        set { setValue("additionalNights", value); } 
    }

    public int desiredMaritalStatus
    { 
        get { return getIntValue("desiredMaritalStatus"); }
        set { setValue("desiredMaritalStatus", value); } 
    }

    public int desiredEducationLevel
    { 
        get { return getIntValue("desiredEducationLevel"); }
        set { setValue("desiredEducationLevel", value); } 
    }

    public int desiredDrinkingHabits
    { 
        get { return getIntValue("desiredDrinkingHabits"); }
        set { setValue("desiredDrinkingHabits", value); } 
    }

    public int desiredSmokingHabits
    { 
        get { return getIntValue("desiredSmokingHabits"); }
        set { setValue("desiredSmokingHabits", value); } 
    }

    public bool contractReceived
    { 
        get { return getBoolValue("contractReceived"); }
        set { setValue("contractReceived", value); } 
    }

    public MemberLoveMostAboutJDate loveMostAboutJDate 
    { 
        get { return (MemberLoveMostAboutJDate) getIntValue("loveMostAboutJDate"); }
        set { setValue("loveMostAboutJDate", (int) value); } 
    }

    public MemberPersonalityQuestion1 personalityQuestion1
    { 
        get { return (MemberPersonalityQuestion1) getIntValue("personalityQuestion1"); }
        set { setValue("personalityQuestion1", (int) value); } 
    }

    public MemberPersonalityQuestion2 personalityQuestion2 
    { 
        get { return (MemberPersonalityQuestion2) getIntValue("personalityQuestion2"); }
        set { setValue("personalityQuestion2", (int) value); } 
    }

    public MemberPersonalityQuestion3 personalityQuestion3 
    { 
        get { return (MemberPersonalityQuestion3) getIntValue("personalityQuestion3"); }
        set { setValue("personalityQuestion3", (int) value); } 
    }

    public MemberPersonalityQuestion4 personalityQuestion4 
    { 
        get { return (MemberPersonalityQuestion4) getIntValue("personalityQuestion4"); }
        set { setValue("personalityQuestion4", (int) value); } 
    }

    public MemberPersonalityQuestion5 personalityQuestion5 
    { 
        get { return (MemberPersonalityQuestion5) getIntValue("personalityQuestion5"); }
        set { setValue("personalityQuestion5", (int) value); } 
    }

    public MemberPersonalityQuestion6 personalityQuestion6 
    { 
        get { return (MemberPersonalityQuestion6) getIntValue("personalityQuestion6"); }
        set { setValue("personalityQuestion6", (int) value); } 
    }

    public MemberPersonalityQuestion7 personalityQuestion7 
    { 
        get { return (MemberPersonalityQuestion7) getIntValue("personalityQuestion7"); }
        set { setValue("personalityQuestion7", (int) value); } 
    }

    public MemberPersonalityQuestion8 personalityQuestion8 
    { 
        get { return (MemberPersonalityQuestion8) getIntValue("personalityQuestion8"); }
        set { setValue("personalityQuestion8", (int) value); } 
    }

    public MemberPersonalityQuestion9 personalityQuestion9 
    { 
        get { return (MemberPersonalityQuestion9) getIntValue("personalityQuestion9"); }
        set { setValue("personalityQuestion9", (int) value); } 
    }

    public MemberPersonalityQuestion10 personalityQuestion10 
    { 
        get { return (MemberPersonalityQuestion10) getIntValue("personalityQuestion10"); }
        set { setValue("personalityQuestion10", (int) value); } 
    }

    public MemberPersonalityQuestion11 personalityQuestion11 
    { 
        get { return (MemberPersonalityQuestion11) getIntValue("personalityQuestion11"); }
        set { setValue("personalityQuestion11", (int) value); } 
    }

    public MemberPersonalityQuestion12 personalityQuestion12 
    { 
        get { return (MemberPersonalityQuestion12) getIntValue("personalityQuestion12"); }
        set { setValue("personalityQuestion12", (int) value); } 
    }

    public string applicationRemarks
    { 
        get { return getStringValue("applicationRemarks"); }
        set { setValue("applicationRemarks", value); } 
    }

    public string inPersonInterviewTime
    { 
        get { return getStringValue("inPersonInterviewTime"); }
        set { setValue("inPersonInterviewTime", value); } 
    }

    public string phoneInterviewNotes
    { 
        get { return getStringValue("phoneInterviewNotes"); }
        set { setValue("phoneInterviewNotes", value); } 
    }

    public string interviewQuestionaireNotes
    { 
        get { return getStringValue("interviewQuestionaireNotes"); }
        set { setValue("interviewQuestionaireNotes", value); } 
    }

    public MemberRelationship relationship 
    { 
        get { return (MemberRelationship) getIntValue("relationship"); }
        set { setValue("relationship", (int) value); } 
    }

    public string whatElseToKnow
    { 
        get { return getStringValue("whatElseToKnow"); }
        set { setValue("whatElseToKnow", value); } 
    }

    public string ensureSuccess
    { 
        get { return getStringValue("ensureSuccess"); }
        set { setValue("ensureSuccess", value); } 
    }

    public string whenToCall
    { 
        get { return getStringValue("whenToCall"); }
        set { setValue("whenToCall", value); } 
    }

    public DateTime applicationReceived
    { 
        get { return getDateTimeValue("applicationReceived"); }
        set { setValue("applicationReceived", value); } 
    }

    public string feesChargedText
    { 
        get { return getStringValue("feesChargedText"); }
        set { setValue("feesChargedText", value); } 
    }

    public bool emailOptedOut
    { 
        get { return getBoolValue("emailOptedOut"); }
        set { setValue("emailOptedOut", value); } 
    }

    public string trxId
    { 
        get { return getStringValue("trxId"); }
        set { setValue("trxId", value); } 
    }

    // a guid to identify this member
    public string guid
    { 
        get { return getStringValue("guid"); }
        set { setValue("guid", value); } 
    }   

	public bool sentWelcomeEmail
	{
		get { return getBoolValue("sentWelcomeEmail"); }
		set { setValue("sentWelcomeEmail", value); }
	}

	public int interviewDiscussed
	{
		get { return getIntValue("interviewDiscussed"); }
		set { setValue("interviewDiscussed", value); }
	}

	public int interviewReservations
	{
		get { return getIntValue("interviewReservations"); }
		set { setValue("interviewReservations", value); }
	}
    
    #endregion

    #region constructors


    /**
     * Constructor.  All default values.
     */
    public static Member create(int sparkId)
    {
        // try to get the jdate member
        Matchnet.Member.ServiceAdapters.Member jdateMember = null;
        if (sparkId > 0)
            jdateMember = JDateMemberUtil.Instance.getMember(sparkId);

        // create the joesclub member
        Member member = null;
        if (jdateMember != null)
            member = new Member();
        else
            member = new NonJDateMember();

        member._sparkId = sparkId;
        member._jdateMember = jdateMember;

        return member;
    }


    /**
     * Constructor.  All default values.
     */
    protected Member() : base(DbConstants.NoId)
    {
        // get default values from MemberVO
        MemberVO memberVO = new MemberVO();
        VOConvert.convert(memberVO, this);

        _blocked = new Hashtable();
    }
    #endregion



    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return MemberMgr.singleton;
    }


    #region XML

    // mask bits
    private const int _Simple = 1;
    private const int _EditPreferencesPage = 2;
    private const int _EditEventPage = 4;
    private const int _MyAccountPage = 8;
    private const int _CalendarPage = 16;
    private const int _AlertsPage = 32;

    /**
     * MemberXmlStyle enum
     */
    public enum MemberXmlStyle : int
    {
	    Simple              = _Simple,
	    EditPreferencesPage = _Simple | _EditPreferencesPage,
	    MyAccountPage       = _MyAccountPage,
	    CalendarPage        = _Simple | _CalendarPage,
	    AlertsPage          = _AlertsPage
    }


    /**
     * generate member xml 
     */
    public void toXml(XmlHelper xh, MemberXmlStyle style)
    {
        toXml(xh, style, null, false);
    }


    /**
     * generate member xml 
     */
    public void toXml(
        XmlHelper xh, MemberXmlStyle style, Event ev, bool inLockedTable)
    {
		string sGender = this.gender == CommonConstants.Male ? "m" : "f";
		xh.startElem("member", false, new string[] { "gender", sGender });

        if (inStyle(_Simple, style))
		{
            xh.writeSimpleElem("sparkId", _sparkId.ToString());
            xh.writeSimpleElem("id", id);
            xh.writeSimpleElem("image", _image);
            xh.writeSimpleElem("firstName", _firstName);
            xh.writeSimpleElem("lastName", _lastName);
            xh.writeSimpleElem("city", this.city);
            xh.writeSimpleElem("state", this.state);
            xh.writeSimpleElem("emailAdd", this.emailAdd);
            xh.writeSimpleElem("age", this.age.ToString());
		    xh.startElem("desiredAgeRange", true, desiredAgeRangeToArray());
            xh.writeSimpleElem("height", MemberVO.heightToString(this.height));
            xh.writeSimpleElem("bodyType", Enums.bodyType.getName(_bodyType));
            xh.writeSimpleElem("education", Enums.memberEducation.getName(_education));
            xh.writeSimpleElem("jdateReligion", Enums.jdateReligion.getName(_jdateReligion));
            xh.writeSimpleElem("kids", (this.numKids == 0 ? "No" : this.numKids.ToString()) + " Kids");
            xh.writeSimpleElem("smoking", _smoking ? "Smokes" : "No Smoking");
            xh.writeSimpleElem("jdateUrl", this.jdateUrl);
            xh.writeSimpleElem("substitute", _activityStatus == MemberActivityStatus.Substitute);
            xh.writeSimpleElem("fulfillment", getPercentFulfillmentString());
		    xh.writeSimpleElem("inLockedTable", inLockedTable);
		
            // EventMember xml
            if (ev != null)
            {
                EventMember em = ev.getEventMember(id);
                EventMember.toXml(xh, em);
            }
        }

        if (inStyle(_EditPreferencesPage, style))
		{
			xh.writeSimpleElem("emailAdd2", _emailAdd2);
			xh.writeSimpleElem("phone", _phone);
			xh.writeSimpleElem("phone2", _phone2);
			xh.startElem("shortNotice", true, new string[2] { "value", _shortNotice? "yes" : "no" });
			xh.startElem("willDateParent", true, willDateParentToArray());
			xh.writeSimpleElem("avatar", _avatar);
			xh.writeSimpleElem("activityStatus", (int) _activityStatus);
			xh.writeSimpleElem("meetingSomeone", (int) _meetingSomeone);
			xh.writeSimpleElem("describeMe", _describeMe);
			xh.writeSimpleElem("phoneType", ValueSet.toIntString(_phoneType));
            xh.writeSimpleElem("phoneType2", ValueSet.toIntString(_phoneType2));
            xh.writeSimpleElem("hearAboutJoesClub", Enums.memberHearAboutJoesClub.getName(_hearAboutJoesClub));
        }

        if (inStyle(_MyAccountPage, style))
		{
            xh.writeSimpleElem("firstName", _firstName);
            xh.writeSimpleElem("lastName", _lastName);
            xh.writeSimpleElem("desiredAgeLow", _desiredAgeLow);
            xh.writeSimpleElem("desiredAgeHigh", _desiredAgeHigh);
            xh.writeSimpleElem("phone", _phone);
			xh.writeSimpleElem("phone2", _phone2);
            BitMaskXml.toXml(Enums.describeMe, xh, "describeMe", _describeMe);
            ValueSetXml.toXml(Enums.phoneType, xh, "phoneType", _phoneType);
            ValueSetXml.toXml(Enums.phoneType, xh, "phoneType2", _phoneType2);
            ValueSetXml.toXml(Enums.memberChildrenCount, xh, "numKids", this.numKids);
            ValueSetXml.toXml(Enums.willDateAParent, xh, "willDateAParent", _willDateAParent);        
            ValueSetXml.toXml(Enums.memberOccupation, xh, "occupation", occupation);
            BitMaskXml.toXml(Enums.memberLocationPref, xh, "locationPref", locationPref);
            BitMaskXml.toXml(Enums.memberTimePref, xh, "timePref", timePref);
            BitMaskXml.toXml(Enums.memberDietaryRestrictions, xh, "dietaryRestrictions", dietaryRestrictions);
            BitMaskXml.toXml(Enums.memberCuisinesToAvoid, xh, "cuisinesToAvoid", cuisinesToAvoid);
            BitMaskXml.toXml(Enums.daysOfTheWeek, xh, "additionalNights", additionalNights);
            BitMaskXml.toXml(Enums.jdateReligion, xh, "desiredJDateReligion", _desiredJDateReligion);
            BitMaskXml.toXml(Enums.memberEducation, xh, "desiredEducationLevel", desiredEducationLevel);
            BitMaskXml.toXml(Enums.drinking, xh, "desiredDrinkingHabits", desiredDrinkingHabits);
            BitMaskXml.toXml(Enums.memberSmokingHabits, xh, "desiredSmokingHabits", desiredSmokingHabits);
            BitMaskXml.toXml(Enums.memberMaritalStatus, xh, "desiredMaritalStatus", desiredMaritalStatus);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion1, xh, "personalityQuestion1", personalityQuestion1);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion2, xh, "personalityQuestion2", personalityQuestion2);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion3, xh, "personalityQuestion3", personalityQuestion3);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion4, xh, "personalityQuestion4", personalityQuestion4);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion5, xh, "personalityQuestion5", personalityQuestion5);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion6, xh, "personalityQuestion6", personalityQuestion6);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion7, xh, "personalityQuestion7", personalityQuestion7);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion8, xh, "personalityQuestion8", personalityQuestion8);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion9, xh, "personalityQuestion9", personalityQuestion9);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion10, xh, "personalityQuestion10", personalityQuestion10);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion11, xh, "personalityQuestion11", personalityQuestion11);
            ValueSetXml.toXml(Enums.memberPersonalityQuestion12, xh, "personalityQuestion12", personalityQuestion12);

            // birthdate
            int day   = CommonConstants.NoValueInt;
            int month = CommonConstants.NoValueInt;
            int year  = CommonConstants.NoValueInt;
            if (birthdate != CommonConstants.NoValueDate)
            {
                day   = birthdate.Day;
                month = birthdate.Month;
                year  = birthdate.Year;
            }
            xh.writeSimpleElem("birthdateDay", day);
            xh.writeSimpleElem("birthdateMonth", month);
            xh.writeSimpleElem("birthdateYear", year);
        }

        if (inStyle(_CalendarPage, style))
		{
            // get the default event for the member
            int defaultEventId = MemberMgr.singleton.getDefaultViewEvent(id);
            if (defaultEventId != DbConstants.NoId)
                xh.writeSimpleElem("defaultEventId", defaultEventId);
        }

        if (inStyle(_AlertsPage, style))
		{
            xh.writeSimpleElem("id", id);
            xh.writeSimpleElem("firstName", _firstName);
            xh.writeSimpleElem("lastName", _lastName);
            xh.writeSimpleElem("phone", _phone);
            xh.writeSimpleElem("phoneType", _phoneType.ToString());
        }

        xh.endElem(); // member
    }


    /**
     * generate xml for the object
     * members = ArrayList[Member]
     */
    public static void toXml(XmlHelper xh, ArrayList members, Event ev)
    {
        StringBuilder sbMaleIds = new StringBuilder();
        StringBuilder sbFemaleIds = new StringBuilder();
        foreach (Member member in members)
        {
            StringBuilder sb = member.gender == CommonConstants.Male ? 
                sbMaleIds : sbFemaleIds;
            sb.Append(member.id);
            sb.Append(",");
        }

        string maleIds = sbMaleIds.ToString();
        string femaleIds = sbFemaleIds.ToString();
        maleIds = maleIds.Substring(0, maleIds.Length - 1); // strip off final comma
        femaleIds = femaleIds.Substring(0, femaleIds.Length - 1);

        xh.startElem("members", false, new string[] { 
            "maleIds", maleIds, 
            "femaleIds", femaleIds } );

        foreach (Member member in members)
            member.toXml(xh, MemberXmlStyle.Simple, ev, false);

        xh.endElem(); // </members>
    }


    /**
     * return true if the maskBit is in the style mask.
     */
    private bool inStyle(int maskBit, MemberXmlStyle style)
    {
        return (maskBit & ((int) style)) > 0;
    }

    #endregion XML


	/**
	 * Returns min/max desired age range as an attribute array.
	 */
	public string[] desiredAgeRangeToArray()
	{
		string minVal = (_desiredAgeLow == CommonConstants.NoValueInt)? "-1" : _desiredAgeLow.ToString();
		string maxVal = (_desiredAgeHigh == CommonConstants.NoValueInt)? "-1" : _desiredAgeHigh.ToString();
		return new string[4] { "min", minVal, "max", maxVal };
	}

	/**
	 * Returns code for willDateParent as attribute array.
	 */
	public string[] willDateParentToArray()
	{
		return new string[2] { "code", _willDateAParent.ToString() };
	}

    /**
     * Returns true if member's JDateReligion is acceptable for
     * this member.
     */
    public bool jdateReligionMatches(Member member)
    {
        return Enums.jdateReligion.inMask(member.jdateReligion, _desiredJDateReligion);
    }


    /**
     * Indicates this member blocked the other member
     */
    public bool blocked(Member member)
    {
        return _blocked.ContainsKey(member.id); 
    }


    /**
     * If the kid preferences are incompatible, returns a string explaining
     * why, otherwise returns null.
     */
    public string kidPrefsIncompatible(Member member)
    {
        string explanation = _kidPrefsIncompatible(member);
        if (explanation == null)
            explanation = member._kidPrefsIncompatible(this);
        return explanation;
    }

    private string _kidPrefsIncompatible(Member member)
    {
        if (this.willDateAParent == (int) WillDateAParent.No &&
            member.numKids > 0)
        {
            return this.firstName + " doesn't want to date someone with kids.";
        }
        else
            return null;
    }


    /**
     * Returns the member's JDate url.
     */
    public string jdateUrl 
    { 
        get 
        { 
            if (_sparkId == CommonConstants.NoValueInt)
                return null;

            string jdateUrl = Config.getProperty("JDateUrl") + 
                "/Applications/MemberProfile/ViewProfile.aspx?Ordinal=1&PersistLayoutTemplate=1&EntryPoint=4&LayoutTemplateID=11&MemberID=" + _sparkId;
            return jdateUrl;
        }
    }


    /**
     * Returns the number of late cancellations for this member.
     */
    public int getNumLateCancellations()
    {
        return EventMemberMgr.singleton.getNumLateCancellations(id);
    }


    /**
     * Returns the number of no shows for this member.
     */
    public int getNumNoShows()
    {
        return EventMemberMgr.singleton.getMemberStatusCount(id, MemberStatus.NoShow);
    }


    /**
     * The number of dinner attended
     */
    public int numDinAttend 
    { 
        get 
        { 
            return EventMemberMgr.singleton.getMemberStatusCount(id, MemberStatus.Attended);
        } 
        
        set 
        { 
            _log.error("numDinAttend not settable.");
        } 
    }


    /**
     * Returns the most recent event attended by the member.
     */
    public Event getLastEventAttended()
    {
        int eventId = EventMemberMgr.singleton.getLastEventAttended(id);
        if (eventId == DbConstants.NoId)
            return null;
        return (Event) EventMgr.singleton.load(eventId);
    }



    /**
     * Returns the venues attended by the member.
     * returns ArrayList[Venue]
     */
    public ArrayList getVenuesAttended()
    {
        return EventMemberMgr.singleton.getVenuesAttended(id);
    }


    /**
     * Activate the Member.  sets the activity status and sends
     * the Welcome email.
     */
    public bool isActive()
    {
        return _activityStatus == MemberActivityStatus.Member ||
               _activityStatus == MemberActivityStatus.Substitute;
    }


    /**
     * Returns true if someone else is using the sparkId
     */
    public bool sparkIdAlreadyUsed(int sparkId)
    {
        if (sparkId == CommonConstants.NoValueInt)
            return false;

        int otherMemberId = MemberMgr.singleton.getMemberIdFromSparkId(sparkId);
        return otherMemberId != this.id && otherMemberId != DbConstants.NoId;
    }


    /**
     * Returns true if someone else is using the email address
     */
    public bool emailAddressAlreadyUsed(string emailAddress)
    {
        if (emailAddress == null || emailAddress == "")
            return false;

        int otherMemberId = MemberMgr.singleton.getMemberIdFromEmailAddress(emailAddress);
        return otherMemberId != this.id && otherMemberId != DbConstants.NoId;
    }


    /**
     * Saves the most recent InterviewStatus in the "latestInterviewStatus"
     * misc attr.
     */
    public void setLatestInterviewStatus()
    { 
        InterviewStatus latestInterviewStatus = InterviewStatus.NoValue;
        DateTime latestWhenCalled = DateTime.MinValue;

        if (_phoneInterviews != null)
            foreach (PhoneInterview pi in _phoneInterviews)
                if (pi.whenCalled > latestWhenCalled)
                    latestInterviewStatus = pi.interviewStatus;

        setValue("latestInterviewStatus", (int) latestInterviewStatus);
    }


    #region Fulfillment
    /**
     * Get the fulfillment as a percentage string, i.e. 0.75 --> "75%"
     */
    public string getPercentFulfillmentString()
    {
        double fulfillment = getFulfillment();
        int iFulfillment = (int) (getPercentFulfillment() + 0.5);
        return iFulfillment.ToString() + "%";
    }


    /**
     * Returns the fulfillment as a percentage.
     */
    public double getPercentFulfillment()
    {
        return getFulfillment() * 100.0;
    }


    /**
     * Compute the fulfillment for this member
     */
    public double getFulfillment()
    {
        ArrayList eventMembers = EventMemberMgr.singleton.loadEventMembers(this);
        double numFulfilled = 0;
        double numUnfulfilled = 0;
        foreach (EventMember eventMember in eventMembers)
        {
            EventMemberFulfillment emf = eventMember.getFulfillment();
            if (emf == EventMemberFulfillment.Fulfilled)
                ++numFulfilled;
            else if (emf == EventMemberFulfillment.Unfulfilled)
                ++numUnfulfilled;
        }

        double fulfillment = 1.0;
        if (numFulfilled + numUnfulfilled > 0)
            fulfillment = numFulfilled / (numFulfilled + numUnfulfilled);

        return fulfillment;
    }
    #endregion Fulfillment


#region Logable interface
    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_sparkId", this.sparkId);
        objectLogger.log("_gender", this.gender );
        objectLogger.log("_image", this.image);
        objectLogger.log("_firstName", this.firstName);
        objectLogger.log("_lastName", this.lastName);
        objectLogger.log("_city", this.city);
        objectLogger.log("_state", this.state);
        objectLogger.log("_emailAdd", this.emailAdd);
        objectLogger.log("_age", this.age);
        objectLogger.log("_desiredAgeLow", this.desiredAgeLow);
        objectLogger.log("_desiredAgeHigh", this.desiredAgeHigh);
        objectLogger.log("_height", this.height);
        objectLogger.log("_bodyType", this.bodyType);
        objectLogger.log("_weight", this.weight);
        objectLogger.log("_education", this.education);
        objectLogger.log("_jdateReligion", this.jdateReligion);
        objectLogger.log("_desiredJDateReligion", this.desiredJDateReligion);
        objectLogger.log("_numKids", this.numKids);
        objectLogger.log("_smoking", this.smoking);
        objectLogger.log("_blocked", _blocked);
        objectLogger.log("_deleted", this.deleted);
    }
#endregion
}

}