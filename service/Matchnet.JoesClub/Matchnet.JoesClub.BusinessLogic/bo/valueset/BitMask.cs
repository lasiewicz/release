using System;
using System.Collections;
using System.Text;

/**
 * Used to represent a bit mask
 */
namespace Matchnet.JoesClub.BusinessLogic
{

public class BitMask : ValueSet
{
    private static Log _log = new Log("BitMask");


    /**
     * Constructor.
     * 
     * values must single bits, i.e. 1, 2, 4, 8, etc.  No zero, no combinations
     * of bits.
     */ 
    public BitMask(object[] values, string[] names) :
        base(values, names, CommonConstants.EmptyMask)
    {
        int mask = 0;
        foreach (int value in values)
        {
            if ((value & mask) != 0)
            {
                _log.logObject("values", values, Log.iError);
                _log.error("BitMask() bad values.");
            }
            mask = mask | value;
        }
    }


    /**
     * true if value is in the mask
     */
    public bool inMask(int value, int mask)
    {
        if (mask == CommonConstants.NoValueInt)
            mask = CommonConstants.EmptyMask;
        if (value == CommonConstants.NoValueInt)
            value = CommonConstants.EmptyMask;

        return (value & mask) != 0;
    }


    /**
     * Or's all the values together
     */
    public int getAllMask()
    {
        int allMask = 0;
        foreach (int value in _values)
            allMask |= value;
        return allMask;
    }




    /**
     * This is to let BitMask use 0 instead of -1 for no value.
     */
    protected override int getNoValueInt()
    {
        return CommonConstants.EmptyMask;
    }


    /**
     * Return the mask values as a comma separated string
     */
    public string getCommaSepString(int mask)
    {
        ArrayList strs = new ArrayList();
        foreach (int bit in _values)
        {
            if ((bit & mask) != 0)
                strs.Add(getName(bit));
        }

        if (strs.Count == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        sb.Append(strs[0]);
        for (int i = 1; i < strs.Count; i++)
        {
            sb.Append(", ");
            sb.Append(strs[i]);
        }

        return sb.ToString();
    }


    /**
     * Generate XML
     */
    public override void toXml(XmlHelper xh, string tag, object selection)
    {
        xh.startElem(tag, false, null);

        for (int i = 0; i < _values.Length; i++)
        {
            int mask = (int) selection;
            int val = (int) _values[i];
            bool selected = inMask(val, mask);

            string[] attrs = new string[selected ? 6 : 4];
            attrs[0] = "label";
            attrs[1] = _names[i];
            attrs[2] = "value";
            attrs[3] = val.ToString();
            if (selected)
            {
                attrs[4] = "selected";
                attrs[5] = "true";
            }

            xh.startElem("option", true, attrs);
        }

        xh.endElem(); // tag
    }

}

}