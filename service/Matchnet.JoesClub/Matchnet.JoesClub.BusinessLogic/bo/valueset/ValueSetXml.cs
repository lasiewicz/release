using System;
using System.Collections;

using Matchnet.JoesClub.ValueObjects;


/**
 * Used to represent a set of values
 */
namespace Matchnet.JoesClub.BusinessLogic
{

public class ValueSetXml 
{
    private static Log _log = new Log("ValueSetXml");

//    public const int Any = 0;
    public const string AnyStr = "Any";



    /**
     * Generate XML
     */
    public static void toXml(ValueSet valueSet, XmlHelper xh, string tag, object selection)
    {
        toXml(valueSet, xh, tag, selection, false);
    }


    /**
     * Generate XML
     */
    public static void toXml(ValueSet valueSet, XmlHelper xh, string tag, object selection, bool includeNoValue)
    {
        xh.startElem(tag, false, null);

        if (includeNoValue)
        {
            bool selected = CommonConstants.NoValueInt == (int) selection;
            optionXml(xh, "", CommonConstants.NoValueInt, selected);
        }

        for (int i = 0; i < valueSet.values.Length; i++)
        {
            int val = (int) valueSet.values[i];
            bool selected = val == (int) selection;
            optionXml(xh, valueSet.names[i], val, selected);
        }

        xh.endElem(); // tag
    }


    /**
     * Generate XML for an option
     */
    private static void optionXml(XmlHelper xh, string name, object val, bool selected)
    {
        string[] attrs;
        if (selected)
            attrs = new string[] { "label", name, "value", val.ToString(), "selected", "true" };
        else
            attrs = new string[] { "label", name, "value", val.ToString() };

        xh.startElem("option", true, attrs);
    }


}

}