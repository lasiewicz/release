using System;
using System.Collections;
using System.Text;

using Matchnet.JoesClub.ValueObjects;

/**
 * Used to represent a bit mask
 */
namespace Matchnet.JoesClub.BusinessLogic
{

public class BitMaskXml
{
    private static Log _log = new Log("BitMaskXml");

    /**
     * Generate XML
     */
    public static void toXml(BitMask bitMask, XmlHelper xh, string tag, object selection)
    {
        xh.startElem(tag, false, null);

        for (int i = 0; i < bitMask.values.Length; i++)
        {
            int mask = (int) selection;
            int val = (int) bitMask.values[i];
            bool selected = bitMask.inMask(val, mask);

            string[] attrs = new string[selected ? 6 : 4];
            attrs[0] = "label";
            attrs[1] = bitMask.names[i];
            attrs[2] = "value";
            attrs[3] = val.ToString();
            if (selected)
            {
                attrs[4] = "selected";
                attrs[5] = "true";
            }

            xh.startElem("option", true, attrs);
        }

        xh.endElem(); // tag
    }

}

}