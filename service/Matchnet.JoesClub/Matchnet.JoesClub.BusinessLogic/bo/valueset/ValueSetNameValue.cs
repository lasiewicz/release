

/**
 * Simple name/value pair
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class ValueSetNameValue
{
    public string _name;
    public string name { get { return _name; } set { _name = value; } }

    public object _value;
    public object value { get { return _value; } set { _value = value; } }

    public ValueSetNameValue(string name, object value)
    {
        _name = name;
        _value = value;
    }
}
}
