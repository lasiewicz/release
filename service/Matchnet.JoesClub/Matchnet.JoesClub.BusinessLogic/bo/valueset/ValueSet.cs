using System;
using System.Collections;


/**
 * Used to represent a set of values
 */
namespace Matchnet.JoesClub.BusinessLogic
{

public class ValueSet : Logable
{
    private static Log _log = new Log("ValueSet");

//    public const int Any = 0;
    public const string AnyStr = "Any";


    protected string[] _names;
    public string[] names { get { return _names; } }

    protected object[] _values;
    public object[] values { get { return _values; } }

    private object _defaultValue;
    public object defaultValue { get { return _defaultValue; } }
    public int defaultValueInt { get { return (int) _defaultValue; } }



    /**
     * Constructor
     */ 
    public ValueSet(object[] values, string[] names, object defaultValue)
    {
        _names = names;
        _values = values;
        _defaultValue = defaultValue;

        if (_names.Length != _values.Length)
        {
            _log.logObject("_names", _names, Log.iError);
            _log.logObject("_values", _values, Log.iError);
            _log.error("ValueSet() _names.Length != _values.Length");
        }
    }


    /**
     * This is to let BitMask use 0 instead of -1 for no value.
     */
    protected virtual int getNoValueInt()
    {
        return CommonConstants.NoValueInt;
    }


    /**
     * Returns all names and values as a ArrayList[ValueSetNameValue]
     */
    public ArrayList getNamesAndValues(bool includeNoValue)
    {
        ArrayList nv = new ArrayList();

        // Do they want a "no value" element?
        if (includeNoValue)
            nv.Add(new ValueSetNameValue("", getNoValueInt()));

        for (int i = 0; i < _values.Length; i++)
            nv.Add(new ValueSetNameValue(_names[i], _values[i]));
        return nv;
    }


    /**
     * Returns the name for the given value
     */
    public string getName(object value)
    {
        int iValue = (int) value;
        for (int i = 0; i < _values.Length; i++)
            if (iValue == (int) _values[i])
                return _names[i];

        // jlf hack
        return "";
        /*
        if (CommonConstants.NoValueInt == (int) value)
            return "";

        _log.error("getName() bad value: " + iValue);
        return null;
        */
    }


    /**
     * returns false if the value is bad
     */
    public bool isValidValue(object value)
    {
        for (int i = 0; i < _values.Length; i++)
            if (value == _values[i])
                return true;

        return false;
    }

    /**
     * throws an exception if the value is bad
     */
    public void validate(object value)
    {
        if (!isValidValue(value))
            _log.error("validate() bad value: " + value);
    }


    /**
     * Just a convenience method to convert a value's int value 
     * to a string, e.g.
     * toIntString(Rsvp.LateCancel) --> "4"
     */
    public static string toIntString(object value)
    {
        return ((int) value).ToString();
    }


    /**
     * Finds the element of _values for the given string value. 
     */
    public object enumFromString(string sValue)
    {
        foreach (object oValue in _values)
            if (sValue == oValue.ToString())
                return oValue;
        _log.error("enumFromString() unknown value: " + sValue);
        return null;
    }


    /**
     * Generate XML
     */
    public virtual void toXml(XmlHelper xh, string tag, object selection)
    {
        toXml(xh, tag, selection, false);
    }


    /**
     * Generate XML
     */
    public virtual void toXml(XmlHelper xh, string tag, object selection, bool includeNoValue)
    {
        xh.startElem(tag, false, null);

        if (includeNoValue)
        {
            bool selected = CommonConstants.NoValueInt == (int) selection;
            optionXml(xh, "", CommonConstants.NoValueInt, selected);
        }

        for (int i = 0; i < _values.Length; i++)
        {
            int val = (int) _values[i];
            bool selected = val == (int) selection;
            optionXml(xh, _names[i], val, selected);
        }

        xh.endElem(); // tag
    }


    /**
     * Generate XML for an option
     */
    private void optionXml(XmlHelper xh, string name, object val, bool selected)
    {
        string[] attrs;
        if (selected)
            attrs = new string[] { "label", name, "value", val.ToString(), "selected", "true" };
        else
            attrs = new string[] { "label", name, "value", val.ToString() };

        xh.startElem("option", true, attrs);
    }


    /**
     * Logable interface method
     */
    public virtual void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_names", _names);
        objectLogger.log("_values", _values);
        objectLogger.log("_defaultValue", _defaultValue);
    }

}

}