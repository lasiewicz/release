using System;
using Matchnet.JoesClub.ValueObjects;


/**
 * Implementation of the "Hungarian" a.k.a Munkres algorithm to find a 
 * minimal cost assignment of rows to columns in a matrix.
 * Source: http://www.public.iastate.edu/~ddoty/HungarianAlgorithm.html
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class Hungarian
{
    private static Log _log = new Log("Hungarian");

    // sort of a hack.  old "no value" constant which might still be in the db here and there.
    private const int StarZero = 1; 
    private const int PrimeZero = 2; 
    private const int Done = 7; 
    private static bool debug = false;
    public const int Match = StarZero; 
    public const int NoMatch = -1; 
    private const int NoValueInt = Int32.MinValue;

    private double[][] _user;
    private double[][] _cost;
    private int[][] _mask;
    private int _size;
    private bool[] _coveredRows;
    private bool[] _coveredCols;
    private int _paddedRows;
    private int _paddedCols;


    /**
     * Entry point into the algorithm
     */
    public static HungarianResult run(double[][] cost)
    {
        return new Hungarian()._run(cost);
    }


    /**
     * run the algorithm
     */
    private HungarianResult _run(double[][] user)
    {
        _log.debug("_run() start user:");
        log(user);
        _user = user;

        // step 0: Convert to a square matrix by padding with 0's
        _log.debug("Step 0");

        // figure out how many rows or columns to pad
        int rows = user.Length;
        int cols = user[0].Length;
        if (rows > cols)
            _paddedCols = rows - cols;
        else if (cols > rows)
            _paddedRows = cols - rows;

        // alloc new _cost matrix
        _size = rows + _paddedRows;
        _cost = new double[_size][];
        for (int r = 0; r < _size; r++)
            _cost[r] = new double[_size];

        // copy user matrix to _cost, padding as needed
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
            {
                if (r < rows && c < cols)
                    _cost[r][c] = user[r][c];
                else
                    _cost[r][c] = 0;
            }


        // alloc _mask, _coveredRows, _coveredCols
        _mask = new int[_size][];
        for (int r = 0; r < _size; r++)
            _mask[r] = new int[_size];
        _coveredRows = new bool[_size];
        _coveredCols = new bool[_size];

        log();

        // Step 1:  For each row of the matrix, find the smallest 
        // element and subtract it from every element in its row
        _log.debug("Step 1");
        for (int r = 0; r < _size; r++)
        {
            double min = minInRow(r);
            for (int c = 0; c < _size; c++)
                _cost[r][c] -= min;
        }
        log();


        // Step 2:  Find a zero (Z) in the resulting matrix.  If there 
        // is no starred zero in its row or column, star Z. Repeat for 
        // each element in the matrix. Go to Step 3. 
        _log.debug("Step 2");
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
                if (_cost[r][c] == 0 &&
                    !_coveredRows[r] &&
                    !_coveredCols[c])
                {
                    _mask[r][c] = StarZero;
                    _coveredRows[r] = true;
                    _coveredCols[c] = true;
                }

        // clear covered vectors
        clearCovers();
        log();


        int step = 3;
        RowCol z0 = null;
        while (step != Done)
        {
            switch (step)
            {
                case 3:
                    int count = step3();
                    step = count >= _size ? Done : 4;
                    break;
                case 4:
                    z0 = step4();
                    step = z0 == null ? 6 : 5;
                    break;
                case 5:
                    step5(z0);
                    step = 3;
                    break;
                case 6:
                    step6();
                    step = 4;
                    break;
            }

            log();
        }

        HungarianResult result = new HungarianResult(rows, cols, _user, _mask);
        _log.logObject("result", result, Log.iInfo);
        return result;
    }


    /**
     * Used by step 1. find the minimum value in a row
     */
    private double minInRow(int r)
    {
        double min = double.MaxValue;
        for (int c = 0; c < _size; c++)
            if (_cost[r][c] < min)
                min = _cost[r][c];
        return min;
    }


    /**
     * Step 3:  Cover each column containing a starred zero.  If K 
     * columns are covered, the starred zeros describe a complete set 
     * of unique assignments.  In this case, Go to DONE, otherwise, 
     * Go to Step 4. 
     */
    private int step3()
    {
        _log.debug("Step 3");
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
                if (_mask[r][c] == StarZero)
                    _coveredCols[c] = true;

        int count = 0;
        for (int c = 0; c < _size; c++)
            if (_coveredCols[c])
                ++count;

        return count;
    }


    /**
     * Step 4:  Find a noncovered zero and prime it.  If there is no 
     * starred zero in the row containing this primed zero, Go to Step 5.  
     * Otherwise, cover this row and uncover the column containing the 
     * starred zero. Continue in this manner until there are no uncovered 
     * zeros left. Save the smallest uncovered value and Go to Step 6. 
     */
    private RowCol step4()
    {
        _log.debug("Step 4");
        while (true)
        {
            RowCol rc = findAZero();
            if (rc == null)
                return null;
            int r = rc.row;
            int c = rc.col;
            _mask[r][c] = PrimeZero;
            log();
            if (starInRow(r))
            {
                c = findStarInRow(r);
                _coveredRows[r] = true;
                _coveredCols[c] = false;
            log();
            }
            else
                return rc;
        }
    }

    
    /**
     * used by step 4
     */
    private RowCol findAZero()
    {
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
                if (_cost[r][c] == 0 &&
                    !_coveredRows[r] &&
                    !_coveredCols[c])
                {
                    return new RowCol(r, c);
                }

        return null;
    }

    /**
     * used by step 4
     */
    private bool starInRow(int r)
    {
        for (int c = 0; c < _size; c++)
            if (_mask[r][c] == StarZero)
                return true;
        return false;
    }

    
    /**
     * used by step 4
     */
    private int findStarInRow(int r)
    {
        for (int c = 0; c < _size; c++)
            if (_mask[r][c] == StarZero)
                return c;
        return NoValueInt;
    }
    

    /**
     * Step 5:  Construct a series of alternating primed and starred 
     * zeros as follows.  Let Z0 represent the uncovered primed zero 
     * found in Step 4.  Let Z1 denote the starred zero in the column 
     * of Z0 (if any). Let Z2 denote the primed zero in the row of Z1 
     * (there will always be one).  Continue until the series terminates 
     * at a primed zero that has no starred zero in its column.  
     * Unstar each starred zero of the series, star each primed zero 
     * of the series, erase all primes and uncover every line in the 
     * matrix.  Return to Step 3. 
     */
    private void step5(RowCol z0)
    {
        _log.debug("Step 5");

        // alloc path
        int maxPathLength = 2 * _size; // I think ...
        int[][] path = new int[maxPathLength][]; 
        for (int j = 0; j < maxPathLength; j++)
            path[j] = new int[2];

        int count = 1;
        int i = 0;
        path[0][0] = z0.row;
        path[0][1] = z0.col;
        for (bool done = false; !done; )
        {
            int r = findStarInCol(path[i][1]);
            if (r != NoValueInt)
            {
                ++count;
                ++i;
                path[i][0] = r;
                path[i][1] = path[i - 1][1];
            }
            else
                done = true;

            if (!done)
            {
                int c = findPrimeInRow(path[i][0]);
                ++count;
                ++i;
                path[i][0] = path[i - 1][0];
                path[i][1] = c;
            }
        }

        convertPath(path, count);
        log();
        clearCovers();
        log();
        erasePrimes();
        log();
    }       

    /**
     * used by step 5
     */
    private int findStarInCol(int c)
    {
        for (int r = 0; r < _size; r++)
            if (_mask[r][c] == StarZero)
                return r;
        return NoValueInt;
    }    

    /**
     * used by step 5
     */
    private int findPrimeInRow(int r)
    {
        for (int c = 0; c < _size; c++)
            if (_mask[r][c] == PrimeZero)
                return c;
        return NoValueInt;
    }

    /**
     * used by step 5
     */
    private void convertPath(int[][] path, int count)
    {
        for (int i = 0; i < count; i++)
        {
            int r = path[i][0];
            int c = path[i][1];
            if (_mask[r][c] == StarZero)
                _mask[r][c] = 0;
            else
                _mask[r][c] = StarZero;
        }
    }
    
    /**
     * used by step 5
     */
    private void clearCovers()
    {
        for (int r = 0; r < _size; r++)
            _coveredRows[r] = false;
        for (int c = 0; c < _size; c++)
            _coveredCols[c] = false;
    }
        
    /**
     * used by step 5
     */
    private void erasePrimes()
    {
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
                if (_mask[r][c] == PrimeZero)
                    _mask[r][c] = 0;
    }

            
    /**
     * Step 6:  Add the value found in Step 4 to every element of 
     * each covered row, and subtract it from every element of each 
     * uncovered column.  Return to Step 4 without altering any stars, 
     * primes, or covered lines. 
     */
    private void step6()
    {
        _log.debug("Step 6");

        double min = findSmallest();
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
            {
                if (_coveredRows[r])
                    _cost[r][c] += min;
                if (!_coveredCols[c])
                    _cost[r][c] -= min;
            }

    }            

    /**
     * used by step 6
     */
    private double findSmallest()
    {
        double min = double.MaxValue;
        for (int r = 0; r < _size; r++)
            for (int c = 0; c < _size; c++)
                if (!_coveredRows[r] &&
                    !_coveredCols[c] &&
                    min > _cost[r][c])
                {
                    min = _cost[r][c];
                }
        return min;
    }


    /**
     * print a matrix
     */
    private static void log(double[][] matrix)
    {
        int rows = matrix.Length;
        if (rows == 0)
            return;
        int cols = matrix[0].Length;
        for (int r = 0; r < rows; r++)
        {
            string s = "[ ";
            for (int c = 0; c < cols; c++)
            {
                double v = matrix[r][c];
                if (v == double.MaxValue)
                    s += "i ";
                else
                    s += v + " ";
            }
            s += "]";
            _log.debug(s);
        }
    }


    /**
     * print an array
    private static void log(int[] array)
    {
        for (int r = 0; r < array.Length; r++)
            _log.info(r.ToString() + " --> " + array[r]);
    }
     */


    /**
     * print _cost & _mask
     */
    private void log()
    {
        if (!debug)
            return;

        _log.debug("");

        int rows = _cost.Length;
        int cols = _cost[0].Length;

        // _coveredCols
        string s = "   ";
        for (int c = 0; c < cols; c++)
        {
            if (_coveredCols[c])
                s += "c ";
            else
                s += "  ";
        }
        _log.debug(s);

        for (int r = 0; r < rows; r++)
        {
            if (_coveredRows[r])
                s = "c[ ";
            else
                s = " [ ";
            for (int c = 0; c < cols; c++)
            {
                string v = _cost[r][c].ToString();
                if (_cost[r][c] == 0)
                {
                    if (_mask[r][c] == StarZero)
                        v = "*";
                    else if (_mask[r][c] == PrimeZero)
                        v = "p";
                }
                s += v + " ";
            }
            s += "]";
            _log.debug(s);
        }
        
    }


    /**
     * this show that it doesn't work with non-square matrixes.
     */
    public static void test()
    {
        double[][] cost = new double[][] {
                                       new double[] { 1, 2, 3 }, 
                                       new double[] { 2, 4, 6 }, 
                                       new double[] { 3, 6, 9 }};

        int m = 0;//Int32.MaxValue;
        double[][] cost3 = new double[][] {
                                       new double[] { 1, 3, 3 }, 
                                       new double[] { 2, 3, 5 }};
        double[][] cost4 = new double[][] {
                                       new double[] { 1, 2 }, 
                                       new double[] { 3, 4 }, 
                                       new double[] { 3, 3 }};

        double[][] cost2 = new double[][] {
                                       new double[] { 1, 2, m }, 
                                       new double[] { 3, 4, m }, 
                                       new double[] { 3, 3, m }};

        int[] result = Hungarian.run(cost).matches;
        
    }


    /**
     * test this class
     */
    public static void test3()
    {
        double[][] cost = new double[][] {
                                       new double[] { 2, 4, 7, 9 }, 
                                       new double[] { 3, 9, 5, 1 }, 
                                       new double[] { 8, 2, 9, 7 }};

        int[] result = Hungarian.run(cost).matches;
        
    }

    /**
     * this show that it doesn't work with non-square matrixes.
     */
    public static void test2()
    {
        double[][] cost = new double[][] {
                                       new double[] { 1 }, 
                                       new double[] { 0 }};

        int[] result = Hungarian.run(cost).matches;
        
    }



    /**
     * test this class
     */
    public static void test1()
    {
        double[][] cost = new double[][] {
                                       new double[] { 1, 2, 3 }, 
                                       new double[] { 2, 4, 6 }, 
                                       new double[] { 3, 6, 9 }};

        int[] result = Hungarian.run(cost).matches;
        
    }


    /**
     * simple class holding row and col values
     */
    private class RowCol
    {
        private int _row;
        public int row { get{ return _row; }}
        private int _col;
        public int col { get{ return _col; }}

        public RowCol(int row, int col)
        {
            _row = row;
            _col = col;
        }
    }
}
}