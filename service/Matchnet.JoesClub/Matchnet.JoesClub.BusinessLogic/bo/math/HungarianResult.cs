using System;
using Matchnet.JoesClub.ValueObjects;


/**
 * The results of the "Hungarian" algorithm 
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class HungarianResult : Logable
{
    private static Log _log = new Log("HungarianResult");

    // matches[row] = col
    private int[] _matches;
    public int[] matches { get { return _matches; } }

    private double _totalCost;
    public double totalCost { get { return _totalCost; } }

    private double _averageCost;
    public double averageCost { get { return _averageCost; } }

    private double[][] _cost;


    /**
     * Entry point into the algorithm
     */
    public HungarianResult(int rows, int cols, double[][] cost, int[][] mask)
    {
        _cost = cost;

        // covert mask to an array, ignoring padded rows and cols
        _matches = new int[rows];
        _totalCost = 0;
        int numMatches = 0;
        for (int r = 0; r < rows; r++)
        {
            _matches[r] = Hungarian.NoMatch;
            for (int c = 0; c < cols; c++)
            {
                if (mask[r][c] == Hungarian.Match)
                {
                    _matches[r] = c;
                    _totalCost += cost[r][c];
                    ++numMatches;
                    break;
                }
            }
        }

        if (numMatches > 0)
            _averageCost = (double) _totalCost / numMatches;
        else
            _averageCost = 0;
    }


    /**
     * Returns the warning string if there is one.
     */
    public string warning
    {
        get
        {
            if (_totalCost >= Compatibility.Never.cost())
                return "Warning! The system could not create any compatible tables.";
            return null;
        }
    }


    /**
     * Returns true if the HungarianResult has a Never match.
     */
    public bool hasNeverMatch()
    {
        for (int r = 0; r < _matches.Length; r++)
        {
            int c =  _matches[r];
            double cost = _cost[r][c];
            if (cost == Compatibility.Never.cost())
                return true;
        }

        return false;
    }


    /**
     * Logable interface method
     */
    public void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_matches", _matches);
        objectLogger.log("_totalCost", _totalCost);
        objectLogger.log("_averageCost", _averageCost);
    }
}
}