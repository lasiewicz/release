using System;
using System.Collections;
using System.Text;
using Matchnet.JoesClub.ValueObjects;


/**
 * Assigns couples to tables
 */  
namespace Matchnet.JoesClub.BusinessLogic
{
public class RandTableGenerator : TableGenerator
{
    private static Log _log = new Log("RandTableGenerator");

    // Random number generator
    private static Random _random = new Random();

    // we'll run the algorithm a number of times in proportion to the number
    // of couples.
    private const int numRunsPerCouple = 10;

    private Event _event;
    private ArrayList _bestTables;
    private ArrayList _currentTables;
    private double _bestScore;
    private double _currentScore;


    /**
     * Constructor
     */
    public RandTableGenerator(Event theEvent)
    {
        _event = theEvent;
    }


    /**
     * generates the tables for the event
     * ArrayList[table]
     */
    public ArrayList generateTables()
    {
        // handle cases with less than one full table
        ArrayList allCouples = _event.getCouplesNotInLockedTables();
        if (allCouples.Count < 3)
        {
            // this doesn't seem to be working
            //_event.addAlert(
            //    "Warning! The system could not create any compatible tables.");

            return new ArrayList();
        }

        // generate the remaining tables
        // tables = ArrayList[TempTable]        
        ArrayList tables = generateTables(allCouples);

        // remove partial and "Never" tables
        // realTables = ArrayList[Table]
        ArrayList realTables = new ArrayList();
        foreach (TempTable table in tables)
            if (tableIsOK(table))
                realTables.Add(table.toRealTable(_event));

        return realTables;
    }


    /**
     * generates the tables for the event.  alwaysCells is the set of couples
     * which must be together.
     * couples = ArrayList[MemberGridCell]
     * returns ArrayList[table]
     * 
     */
    private ArrayList generateTables(ArrayList couples)
    {
        _bestScore = Double.MaxValue; // lower score os better
        _bestTables = new ArrayList();

        // since the algorithm uses randomness and is not guaranteed 
        // optimal, run it multiple time and keep the best.
        int numCouples = couples.Count;
        int numRuns = numRunsPerCouple * numCouples;

        for (int i = 0; i < numRuns; i++)
        {
            repeatedMatching(couples);
            //_log.debug("generateTables() _currentScore: " + _currentScore);
            
            if (_currentScore < _bestScore)
            {
                _bestScore = _currentScore;
                _bestTables = _currentTables;
            }
        }

        if (_bestScore >= Compatibility.ValueNever)
            _event.addAlert(
                "Warning! The system could not create any compatible tables.");

        _log.debug("generateTables() _bestScore: " + _bestScore);
        return _bestTables;
    }


    /**
     * generates the tables for the event using the "repeated matching"
     * method.  Sets _currentScore and _currentTables;
     * couples = ArrayList[MemberGridCell]
     * returns ArrayList[table]
     */
    private void repeatedMatching(ArrayList couples)
    {
        // Shuffle the members.  Algorithm found at:
        // http://www.everything2.com/index.pl?node_id=1010893
        MemberGridCell[] randomizedCouples = new MemberGridCell[couples.Count];

        for (int i = 0; i < couples.Count; i++)
            randomizedCouples[i] = (MemberGridCell) couples[i];

        for (int i = 0; i < couples.Count; i++)
        {
            int j = _random.Next(couples.Count);
            MemberGridCell temp = randomizedCouples[i];
            randomizedCouples[i] = randomizedCouples[j];
            randomizedCouples[j] = temp;
        }


        // divide into 3 groups.
        MemberGridCell[] group2;
        int groupSize = couples.Count / 3;

        MemberGridCell[][] groups = new MemberGridCell[3][];
        groups[0] = new MemberGridCell[groupSize];
        groups[1] = new MemberGridCell[groupSize];
        groups[2] = new MemberGridCell[couples.Count - 2*groupSize];

        int group2Size = 0;
        for (int i = 0; i < randomizedCouples.Length; i++)
        {
            // fill group 0
            if (i < groupSize)
                groups[0][i] = randomizedCouples[i];

            // fill group 1. 
            else if (i < 2*groupSize)
                groups[1][i - groupSize] = randomizedCouples[i];

            // fill group 2 with whatever's left
            else 
            {
                groups[2][i - 2*groupSize] = randomizedCouples[i];
                ++group2Size;
            }
        }


        // run the hungarian algorithm on the first 2 groups
        // create the cost grid
        double[][] cost = new double[groupSize][];
        for (int r = 0; r < groupSize; r++)
            cost[r] = new double[groupSize];
        for (int r = 0; r < groupSize; r++)
            for (int c = 0; c < groupSize; c++)
            {
                MemberGridCell couple1 = groups[0][r];
                MemberGridCell couple2 = groups[1][c];
                Compatibility comp = getCompatibility(_event, couple1, couple2, null);
                cost[r][c] = comp.cost();
            }

        HungarianResult hungarianResult = Hungarian.run(cost);
        _currentScore = hungarianResult.totalCost;

        _currentTables = new ArrayList();
        int[] couple2Indexes = hungarianResult.matches;
        for (int r = 0; r < groupSize; r++)
        {
            MemberGridCell couple1 = groups[0][r];
            MemberGridCell couple2 = groups[1][couple2Indexes[r]];
            TempTable table = new TempTable();
            table.addCouple(couple1);
            table.addCouple(couple2);
            _currentTables.Add(table);
        }
        group2 = groups[2];

        // run the hungarian algorithm with the 1st 2 couples currently in
        // the tables and the 3rd couple in group[2].
        // create the cost grid
        if (group2Size == 0)
            return;
        int numTables = _currentTables.Count;
        if (numTables == 0)
            return;
        cost = new double[numTables][];
        for (int r = 0; r < numTables; r++)
            cost[r] = new double[group2Size];
        for (int r = 0; r < numTables; r++)
            for (int c = 0; c < group2Size; c++)
            {
                TempTable table = (TempTable) _currentTables[r];
                MemberGridCell couple1 = (MemberGridCell) table.couples[0];
                MemberGridCell couple2 = (MemberGridCell) table.couples[1];
                MemberGridCell couple3 = group2[c];

                Compatibility comp = getCompatibility(_event, couple1, couple2, couple3);
                cost[r][c] = comp.cost();
            }

        hungarianResult = Hungarian.run(cost);
        int[] couple3Indexes = hungarianResult.matches;
        _currentScore += hungarianResult.totalCost;

        Hashtable group2Used = new Hashtable();
        for (int r = 0; r < numTables; r++)
        {
            TempTable table = (TempTable) _currentTables[r];
            int couple3Index = couple3Indexes[r];
            MemberGridCell couple3 = null;
            if (couple3Index != -1)
            {
                couple3 = group2[couple3Index];
                group2Used[couple3] = "";
            }

            table.addCouple(couple3);
        }

        // if there were couples in group2 which weren't matched up,
        // create an extra table for them
        MemberGridCell[] extraTableCouples = new MemberGridCell[] { null, null, null };
        int extraTableCouplesCount = 0;
        foreach (MemberGridCell couple3 in group2)
        {
            if (!group2Used.ContainsKey(couple3))
            {
                extraTableCouples[extraTableCouplesCount] = couple3;
                ++extraTableCouplesCount;
            }
        }
        if (extraTableCouplesCount > 0)
        {
            TempTable table = new TempTable();
            table.addCouple(extraTableCouples[0]);
            table.addCouple(extraTableCouples[1]);
            table.addCouple(extraTableCouples[2]);
            _currentTables.Add(table);
        }
    }


    /**
     * Check for partial and "never" tables
     */
    private bool tableIsOK(TempTable table)
    {
        // check for partial table
        if (table.couples.Count < 3)
            return false;

        // check for Never table
        Compatibility tableComp = Compatibility.getMembersCompatibility(
            table.getMembers(), _event);
        if (tableComp == Compatibility.Never)
            return false;

        return true;
    }


    /**
     * This is just a set of couples.  These eventually get turns into 
     * real tables.
     */
    public class TempTable
    {
        // ArrayList[MemberGridCell]
        private ArrayList _couples = new ArrayList();
        public ArrayList couples { get { return _couples; }}

        /**
         * Add a couple
         */
        public void addCouple(MemberGridCell couple)
        {
            if (couple != null)
                _couples.Add(couple);
        }

        /**
         * Convert to a real table object
         */
        public Table toRealTable(Event ev)
        {
            return new Table(ev, getMembers());
        }

        /**
         * Get the members
         * return ArrayList[Member]
         */
        public ArrayList getMembers()
        {
            // make member array
            ArrayList members = new ArrayList();
            foreach (MemberGridCell couple in _couples)
            {
                members.Add(couple.member1);
                members.Add(couple.member2);
            }

            return members;
        }
    }


    /**
     * Returns the compatibility among three couples, any of which
     * might be null.  Used to get a table's overall compatibility.
     */
    public static Compatibility getCompatibility(
        Event ev,
        MemberGridCell couple1, MemberGridCell couple2, MemberGridCell couple3)
    {
        ArrayList members = MemberGridCell.getMembers(couple1, couple2, couple3);
        return Compatibility.getMembersCompatibility(members, ev);
    }

}

}