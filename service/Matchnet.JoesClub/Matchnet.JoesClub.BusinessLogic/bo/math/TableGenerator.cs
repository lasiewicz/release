using System;
using System.Collections;
using System.Text;


/**
 * Assigns couples to tables
 */  
namespace Matchnet.JoesClub.BusinessLogic
{
public interface TableGenerator
{

    /**
     * generates the tables for the event
     * ArrayList[table]
     */
    ArrayList generateTables();
}
}