using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the grid table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberGridMgr
{
    private static Log _log = new Log("MemberGridMgr");

    private static MemberGridMgr _singleton;
    public static MemberGridMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new MemberGridMgr();
            return _singleton;
        } 
    }
    

    /**
     * load the object
     */
    public MemberGrid loadForEvent(Event theEvent)
    {
        ArrayList cells = MemberGridCellMgr.singleton.loadForEvent(theEvent);
        if (cells.Count == 0)
            return null;

        // create cells
        int size = getGridSize(theEvent);
        MemberGridCell[][] cellMatrix = allocGridCellMatrix(size - 1);
        int i = 0;
        for (int r = 0; r < cellMatrix.Length; r++)
        {
            cellMatrix[r] = allocGridCellArray(r + 1);
            for (int c = 0; c <= r; c++)
            {
                if (i >= cells.Count)
                    throw new ConcurrencyException("loadForEvent() i >= cells.Count");
                cellMatrix[r][c] = (MemberGridCell) cells[i];
                i++;
            }
        }
        /*
        if (i < cells.Count)
            _log.warning("loadForEvent i < cells.Count event=" + theEvent.id + 
                        " i=" + i + 
                        " cells.Count=" + cells.Count);
                        */
//            throw new ConcurrencyException("loadForEvent() i < cells.Count");

        return new MemberGrid(theEvent, cellMatrix);
    }



    /**
     * save the object
     */
    public void save(MemberGrid grid)
    {
        for (int r = 0; r < grid.size; r++)
            for (int c = 0; c <= r; c++)
            {
                // keep track of the cells the user set true
                MemberGridCell cell = grid.cells[r][c];
                MemberGridCellMgr.singleton.save(cell);
            }
    }



    /**
     * Saves the changes the user made to the MemberGrid
     */
    public MemberGrid replaceMemberGrid(Event ev)
    {
        MemberGrid oldMemberGrid = ev.memberGrid;
        Hashtable oldCells = null;
        if (oldMemberGrid != null)
        {
            oldCells = oldMemberGrid.getCellHash();
            MemberGridMgr.singleton.deleteGridForEvent(ev.id);

            // the old cells are no longer in the db.
            foreach (MemberGridCell cell in oldCells.Values)
                cell.id = DbConstants.NoId;
        }

        ArrayList includedMembers = ev.members(true);
        if (!hasEnoughMembers(includedMembers))
            return null;

        // create cells
        int size = includedMembers.Count;
        MemberGridCell[][] cells = allocGridCellMatrix(size - 1);
        for (int r = 0; r < cells.Length; r++)
        {
            cells[r] = allocGridCellArray(r + 1);
            for (int c = 0; c <= r; c++)
            {
                Member member1 = (Member) includedMembers[r + 1];
                Member member2 = (Member) includedMembers[c];

                MemberGridCell cell = MemberGrid.getCellFromHash(member1, member2, oldCells);
                if (cell == null)
                    cell = new MemberGridCell(
                        DbConstants.NoId, 
                        ev, 
                        member1, 
                        member2,
                        Compatibility.Never,
                        BoConstants.CouplingUnspecified,
                        null);
                cells[r][c] = cell;
            }
        }

        // create MemberGrid
        MemberGrid memberGrid = new MemberGrid(ev, cells);
        memberGrid.computeCompatibility(oldCells);
        MemberGridMgr.singleton.save(memberGrid);
        return memberGrid;
    }

    
    /**
     * Get a Grid's size
     */
    private int getGridSize(Event theEvent)
    {
        return theEvent.members(true).Count;
    }


    /**
     * Allocates GridCell[][]
     */
    private MemberGridCell[][] allocGridCellMatrix(int size)
    {
        return new MemberGridCell[size][];
    }


    /**
     * Allocates GridCell[]
     */
    private MemberGridCell[] allocGridCellArray(int size)
    {
        return new MemberGridCell[size];
    }


    /**
     * See if there are at least 3 men and 3 women
     */
    private bool hasEnoughMembers(ArrayList members)
    {
        int men = 0;
        int women = 0;
        for (int i = 0; i < members.Count && (men < 3 || women < 3); i++)
        {
            Member member = (Member) members[i];
            if (member.gender == CommonConstants.Male)
                ++men;
            else
                ++women;
        }

        return men >= 3 && women >= 3;
    }



    /**
     * Delete the object
     */
    private void deleteGridForEvent(int eventId)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "DeleteMemberGridForEvent");
            command.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
            reader = command.ExecuteReader();
       }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }

}

}