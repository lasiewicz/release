using System;
using System.IO;
using System.Collections; // ArrayList, Hashtable
using System.Collections.Specialized;
using System.Text; // StringBuilder
using Matchnet.JoesClub.ValueObjects;


/**
 * MemberGrid contains the member to member compatibility and coupling
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberGrid : Logable
{
    private static Log _log = new Log("MemberGrid");

    // if true, rules.js changes will not take effect until the
    // configuration is refreshed from Admin.aspx.
    private static bool _cacheJavaScript = false;

    private Event _event;
    public Event theEvent { get { return _event; } set { _event = value; } }

    private MemberGridCell[][] _cells;
    public MemberGridCell[][] cells { get { return _cells; } set { _cells = value; } }

    public int size { get { return _cells.Length; } }

    // cached stuff
    private Hashtable _c_cellsHash;
    private ArrayList _c_couples;
    private Hashtable _c_couplesHash;


    /**
     * Returns the GridMgr for the class
     */
    public MemberGridMgr getGridMgr()
    {
        return MemberGridMgr.singleton;
    }


    /**
     * Constructor. 
     */
    public MemberGrid(Event theEvent, MemberGridCell[][] cells)
    {
        _event = theEvent;
        _cells = cells;
    }


    #region grid modifiers
    // There are four methods that modify the grid:
    //     computeCompatibility: Runs the rules to set compatibilities
    //     setUserSettings: Save user compatibility settings
    //     computeCoupling: Runs Hungarian to compute the couples.
    //     clearCoupling: Clears the couples

    
    /**
     * Computes the compatibility for all the cells except the ones in oldCells.
     * Doesn't affect coupling.
     */
    public void computeCompatibility(Hashtable oldCells)
    {
        _log.debug("computeCompatibility() start");
        RuleFile mfRuleFile = RuleSetConfig.ruleSet.maleFemaleRuleFile;
        RuleFile mmRuleFile = RuleSetConfig.ruleSet.maleMaleRuleFile;
        RuleFile ffRuleFile = RuleSetConfig.ruleSet.femaleFemaleRuleFile;

        if (!_cacheJavaScript)
        {
            mfRuleFile.initJScript();
            mmRuleFile.initJScript();
            ffRuleFile.initJScript();
        }

        string badScoreMessage = null;
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = (MemberGridCell) cells[r][c];

                // see if we need to skip this cell
                if (MemberGrid.getCellFromHash(cell.member1, cell.member2, oldCells) != null)
                    continue;

                // figure out which RuleFile to use
                RuleFile rf = mfRuleFile;
                if (cell.member1.gender == CommonConstants.Male &&
                    cell.member2.gender == CommonConstants.Male)
                    rf = mmRuleFile;
                else if (cell.member1.gender == CommonConstants.Female &&
                    cell.member2.gender == CommonConstants.Female)
                    rf = ffRuleFile;

                // call the RuleFile to get the compatibility
                RuleContext rc = rf.getScore(cell.member1, cell.member2, theEvent);
                rc.checkError();                
                cell.compatibility = rc.compatibility;
                cell.explanation = rc.explanation;
            }

        // add an alert if there were any bad scores.  We do this outside
        // the loop because we don't want to add multiple alerts if the
        // rules file is always returning bad values.
        if (badScoreMessage != null)
            theEvent.addAlert(badScoreMessage);

        // Validate the grid.  
        //validateCompatibility();
        _log.debug("computeCompatibility() finish");
    }



    /**
     * Update the compatibilities from the values submitted by the 
     * MemberGrid.aspx page.  Doesn't affect coupling.
     */
    public void setUserSettings(NameValueCollection queryParams)
    {
        Hashtable scores = getUserScores(queryParams);

        // find the score for each cell
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = (MemberGridCell) cells[r][c];
                string grade = (string) 
                    scores[cell.member1.id + "__" + cell.member2.id];

                // grade will be null if the user didn't set that cell
                if (grade != null)
                {
                    Compatibility newCompatibility = new Compatibility(grade);
                    if (cell.compatibility.value != newCompatibility.value)
                    {
                        cell.compatibility = newCompatibility;
                        cell.explanation = RuleContext.simpleExplanation("User set this value.");
                    }
                }
            }

        // clear cached info.
        reset();
    }


    /**
     * performs the coupling using the Hungarian
     */
    public void computeCoupling()
    {
        // Figure out which rows and cols not to use in the cost grid.
        // We skip rows and cols containing "always" compatibility and
        // ones where the members is in a locked table.
        bool[] skipRows = new bool[size];    // true if the row is not used in the cost grid
        bool[] skipCols = new bool[size]; // true if the col is not used in the cost grid
        int numSkipRows = 0;
        int numSkipCols = 0;

        Hashtable lockedCouples = new Hashtable();
        _event.getCouplesAndMembersInLockedTables(null, lockedCouples);

        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell mgc = cells[r][c];
                Compatibility compatibility = mgc.compatibility;
                bool locked = lockedCouples[mgc] != null;
                if (compatibility.isAlways || locked)
                {
                    skipRows[r] = true;
                    skipCols[c] = true;
                    ++numSkipRows;
                    ++numSkipCols;
                }
            }

        // skip male rows.  careful not to incr numSkipRows for the same row twice.
        for (int r = 0; r < size; r++)
        {
            MemberGridCell mgc = (MemberGridCell) cells[r][0];
            if (mgc.member1.gender == CommonConstants.Male &&
                !skipRows[r])
            {
                skipRows[r] = true;
                ++numSkipRows;
            }
        }

        // skip female cols
        for (int c = 0; c < size; c++)
        {
            MemberGridCell mgc = (MemberGridCell) cells[c][c];
            if (mgc.member2.gender == CommonConstants.Female &&
                !skipCols[c])
            {
                skipCols[c] = true;
                ++numSkipCols;
            }
        }

        // if there are free rows & columns, run the Hungarian
        int[] hungarianMatches = null;
        double[][] cost = null;
        int costRow;
        int costCol;
        if (numSkipRows < size &&
            numSkipCols < size)
        {
            // alloc cost grid
            cost = new double[size - numSkipRows][];
            for (int r = 0; r < cost.Length; r++)
                cost[r] = new double[size - numSkipCols];

            // create cost grid, skipping the "one" rows and cols
            costRow = 0;
            for (int r = 0; r < size; r++)
            {
                costCol = 0;
                if (!skipRows[r])
                {
                    for (int c = 0; c <= r; c++)
                    {
                        if (!skipCols[c])
                        {
                            Compatibility comp = cells[r][c].compatibility;
                            cost[costRow][costCol] = comp.cost();
                            ++costCol;
                        }
                    }
                    ++costRow;
                }
            }

            // run the hungarian
            HungarianResult hungarianResult = Hungarian.run(cost);
            //if (hungarianResult.warning != null)
            //    this.theEvent.addAlert(hungarianResult.warning);
            hungarianMatches = hungarianResult.matches;
        }


        // set cells[r][c].coupling
        costRow = 0;
        for (int r = 0; r < size; r++)
        {
            costCol = 0;
            for (int c = 0; c <= r; c++)
            {
                // if the cell's a "1"
                Compatibility comp = cells[r][c].compatibility;
                if (comp.isAlways)
                    cells[r][c].coupling = BoConstants.CouplingServerSetTrue;

                // handle rows and columns we skipped
                else if (skipRows[r] || skipCols[c])
                {
                    MemberGridCell mgc = cells[r][c];
                    if (lockedCouples[mgc] != null)
                        mgc.coupling = BoConstants.CouplingServerSetTrue;
                    else
                        mgc.coupling = BoConstants.CouplingForcedFalse;
                }

                // get the value from the Hungarian result grid, mask.
                // if hungarianMatches==null, we shouldn't be here
                else if (hungarianMatches[costRow] == costCol &&
                         cost[costRow][costCol] != Compatibility.Never.cost())
                    cells[r][c].coupling = BoConstants.CouplingServerSetTrue;
                else 
                    cells[r][c].coupling = BoConstants.CouplingForcedFalse;

                if (!skipCols[c])
                    ++costCol;
            }
            if (!skipRows[r])
                ++costRow;
        }

        // clear cached couple info.
        reset();
    }



    /**
     * resets the coupling set by the server.  base class because
     * we need to clear couple caches in this class.
     */
    public void clearCoupling()
    {
        // keep track of the rows and columns that contain a
        // cell which the user set true
        Hashtable userRows = new Hashtable();
        Hashtable userCols = new Hashtable();
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = cells[r][c];
                if (cell.compatibility.isAlways)
                {
                    userRows.Add(r, r);
                    userCols.Add(c, c);
                }
            }

        // don't uncouple couples in locked tables.
        Hashtable lockedCouples = new Hashtable();
        _event.getCouplesAndMembersInLockedTables(null, lockedCouples);

        // update the coupling
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = cells[r][c];
                string coupling = cell.coupling;

                // figure out cell.coupling
                if (lockedCouples[cell] != null)
                    cell.coupling = BoConstants.CouplingServerSetTrue;
                else if (    userRows[r] != null || 
                        userCols[c] != null ||
                        cell.compatibility.isNever)
                    cell.coupling = BoConstants.CouplingForcedFalse;
                else
                    cell.coupling = BoConstants.CouplingUnspecified;
            }

        reset();
    }
    #endregion


    #region "get" methods

    
    /**
     * Returns a hashtable that tells if a couple is in a table
     * return Hashtable[int coupleId --> ""]
     */
    private Hashtable getCouplesInTablesHash()
    {
        Hashtable couplesInTables = new Hashtable();
        ArrayList tables = theEvent.tables;
        if (tables == null)
            return couplesInTables;

        foreach (Table table in tables)
            foreach (MemberGridCell couple in table.couples)
                couplesInTables[couple.id] = "";

        return couplesInTables;
    }


    /**
     * Returns the MemberGridCell with the given id
     */
    public MemberGridCell getCouple(int coupleId)
    {
        MemberGridCell couple = (MemberGridCell) getCouplesHash()[coupleId];
        if (couple == null)
            _log.error("getCouple() No object for coupleId=" + coupleId);
        return couple;
    }



    /**
     * Returns the compatibility for two members
     */
    public Compatibility getCompatibility(Member member1, Member member2)
    {
        string key = cellsHashKey(member1, member2);
        MemberGridCell cell = (MemberGridCell) getCellHash()[key];
        if (cell == null)
        {
            key = cellsHashKey(member2, member1);
            cell = (MemberGridCell) getCellHash()[key];
        }
        return cell.compatibility;
    }


    /**
     * Gets the score from from the values submitted by the 
     * MemberGrid.aspx page
     */
    protected Hashtable getUserScores(NameValueCollection queryParams)
    {
        string m1namesParam = queryParams.Get("subId1");
        string[] m1names = m1namesParam.Split(new char[] { ',' });
        string m2namesParam = queryParams.Get("subId2");
        string[] m2names = m2namesParam.Split(new char[] { ',' });

        // Hashtable[string member1.name__member2.name --> score,
        //           string member2.name__member1.name --> score]                     
        Hashtable scores = new Hashtable();
        foreach (string m1name in m1names)
        {
            string scoresParam = queryParams.Get("sub-" + m1name);
            //_log.debug("sub-" + m1name + ": " + scoresParam);

            for (int i = 0; i < m2names.Length; i++)
            {
                string sScore = scoresParam[i].ToString();
                if (sScore != "-")
                {
                    string m2name = m2names[i];
                    scores[m1name + "__" + m2name] = sScore;
                    scores[m2name + "__" + m1name] = sScore;
                }
            }
        }

        return scores;
    }


    /**
     * construct a key for _c_cellsHash
     */
    public static string cellsHashKey(Member member1, Member member2)
    {
        return member1.id.ToString() + "_" + member2.id;
    }


    /**
     * Returns a GridCell from a hashtable using the ids
     * for the key.  Used with the Hashtable returned by getCellHash()
     */
    public static MemberGridCell getCellFromHash(
        Member rowObject, Member colObject, Hashtable hCells)
    {
        if (hCells == null)
            return null;
        string key = rowObject.id.ToString() + "_" + colObject.id;
        return (MemberGridCell) hCells[key];
    }
    #endregion
    
    #region cached stuff

    /**
     * Returns a hashtable of the cells
     * Hashtable[ id1_id2 --> MemberGridCell ]
     */
    public Hashtable getCellHash()
    {
        init("_c_cellsHash");
        return _c_cellsHash;
    }

    
    /**
     * Returns the coupled MemberGridCells
     * ArrayList[MemberGridCell]
     */
    public ArrayList getCouples()
    {
        init("_c_couples");
        return _c_couples;
    }


    /**
     * Returns a hashtable of the couples in this MemberGrid
     * Hashtable[id --> MemberGridCell]
     */
    private Hashtable getCouplesHash()
    {
        init("_c_couplesHash");
        return _c_couplesHash;
    }


    /**
     * Reset cached values
     */
    private void init(string name)
    {
        if (name == "_c_cellsHash" && _c_cellsHash == null)
            initCellsHash();
        else if (name == "_c_couples" && _c_couples == null)
            initCouples();
        else if (name == "_c_couplesHash" && _c_couplesHash == null)
            initCouplesHash();
    }


    /**
     * Reset cached values
     */
    private void reset()
    {
        _c_cellsHash = null;
        _c_couples = null;
        _c_couplesHash = null;
    }


    /**
     * init _c_cellsHash
     */
    private void initCellsHash()
    {
        _c_cellsHash = new Hashtable();
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = (MemberGridCell) cells[r][c];
                string key = cellsHashKey(cell.member1, cell.member2);
                _c_cellsHash[key] = cell;
            }
    }


    /**
     * init _c_couples
     */
    private void initCouples()
    {
        _c_couples = new ArrayList();
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = cells[r][c];
                if (cell.coupling == BoConstants.CouplingServerSetTrue)
                    _c_couples.Add(cell);
            }
    }


    /**
     * init _c_couplesHash
     */
    private void initCouplesHash()
    {
        _c_couplesHash = new Hashtable();
        ArrayList couples = getCouples();
        foreach (MemberGridCell couple in couples)
            _c_couplesHash[couple.id] = couple;
    }


    #endregion
 
    #region xml

    /**
     * Write the object as xml
     */
    public void toXml(XmlHelper xh)
    {
        // <couples>
        couplesToXml(xh);

        // <tables>
        theEvent.tablesToXml(xh);

        // <compatibilities memberIds="..." > 
        compatibilitiesToXml(xh);
    }



    /**
     * Write the the member-member compatibilities in the <compatibilities>
     * element
     */
    public void compatibilitiesToXml(XmlHelper xh)
    {
        // Hashtable[ id1_id2 --> compatibility ]
        Hashtable hashCompatibilities = new Hashtable();
        for (int r = 0; r < size; r++)
            for (int c = 0; c <= r; c++)
            {
                MemberGridCell cell = (MemberGridCell) cells[r][c];
                string key = "" + cell.member1.id + "_" + cell.member2.id;
                hashCompatibilities.Add(key, cell.compatibility.grade);
            }

        // memberIds = "m12,m43,m565,f454,f5456,...."
        StringBuilder sbMemberIds = new StringBuilder();
        ArrayList members = theEvent.sortMembers();
        foreach (Member member in members)
        {
            sbMemberIds.Append(member.id);
            sbMemberIds.Append(",");
        }
        string memberIds = sbMemberIds.ToString();
        memberIds = memberIds.Substring(0, memberIds.Length - 1);

        // compatibilities = "-ABAABACAA---AABAA...."
        StringBuilder sbCompatibilities = new StringBuilder();
        foreach (Member member1 in members)
            foreach (Member member2 in members)
            {
                string key = "" + member1.id + "_" + member2.id; 
                string compatibility = (string) hashCompatibilities[key];
                if (compatibility == null)
                {
                    key = "" + member2.id + "_" + member1.id; 
                    compatibility = (string) hashCompatibilities[key];
                }
                if (compatibility == null)
                    compatibility = "-";
                sbCompatibilities.Append(compatibility);
            }

        xh.startElem("compatibilities", true, new string[] { 
            "memberIds", memberIds, 
            "scores", sbCompatibilities.ToString() } );
    }


    /**
     * Write the object as xml
     */
    public void couplesToXml(XmlHelper xh)
    {
        // <couples>
        xh.startElem("couples", false, null);

        // ArrayList[MemberGridCell]
        ArrayList couples = getCouples();
        Hashtable couplesInTables = getCouplesInTablesHash();
        foreach (MemberGridCell couple in couples)
        {
            string who = "s";
            if (couple.compatibility.isAlways)
                who = "u";

            bool inTable = couplesInTables.ContainsKey(couple.id);
            string sInTable = inTable.ToString().ToLower();

            // <match>
            xh.startElem("couple", true, new string[] { 
                "coupleId", "c" + couple.id.ToString(),
                "memberId1", couple.member2.id.ToString(), // reverse
                "memberId2", couple.member1.id.ToString(),
                "who", who,
                "inTable", sInTable });
        }

        xh.endElem(); // </couples>    
    }
    #endregion
    
    #region Logable

    /**
     * Logable interface method
     */
    public void log(ObjectLogger objectLogger)
    {
        objectLogger.log("_event", theEvent);
        objectLogger.log("_cells", cells);
    }
    #endregion
}
}