using System;
using System.Collections;
using Matchnet.JoesClub.ValueObjects;


/**
 * Represents a cell in the compatibility grid
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberGridCell : DbObject, Logable
{
    private static Log _log = new Log("MemberGridCell");

    private Event _event;
    public Event theEvent { get { return _event; } set { _event = value; } }
     
    private Compatibility _compatibility;
    public Compatibility compatibility { get { return _compatibility; } set { _compatibility = value; } }
    
    private string _coupling;
    public string coupling { get { return _coupling; } set { _coupling = value; } }
    
    private string _explanation;
    public string explanation { get { return _explanation; } set { _explanation = value; } }
 
    private Member _member1;
    public Member member1 { get { return (Member) _member1; } set { _member1 = value; } }
    
    private Member _member2;
    public Member member2 { get { return (Member) _member2; } set { _member2 = value; } }
  

    /**
     * Constructor
     */
    public MemberGridCell(
        int id, Event ev, 
        Member member1, Member member2, 
        Compatibility compatibility, string coupling, string explanation) 
        : base(id)
    {
        _event = ev;
        _compatibility = compatibility;
        _coupling = coupling;
        _member1 = member1;
        _member2 = member2;
        _explanation = explanation;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return MemberGridCellMgr.singleton;
    }

    /**
     * Convenience method to get the members from 3 couples, any of which
     * may be null.
     */
    public static ArrayList getMembers(
        MemberGridCell couple1, MemberGridCell couple2, MemberGridCell couple3)
    {
        ArrayList members = new ArrayList();
        addCoupleMembers(couple1, members);
        addCoupleMembers(couple2, members);
        addCoupleMembers(couple3, members);

        return members;
    }


    /**
     * Used by method above to add a couple's members to an array.
     */
    private static void addCoupleMembers(MemberGridCell couple, ArrayList members)
    {
        if (couple != null)
        {            
            members.Add(couple.member1);
            members.Add(couple.member2);
        }
    }


    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        objectLogger.log("compatibility", compatibility);
        objectLogger.log("coupling", coupling);
        objectLogger.log("rowObject", _member1);
        objectLogger.log("colObject", _member2);
    }
}
}