using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the gridCell table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberGridCellMgr : DbObjectMgr
{
    private static Log _log = new Log("MemberGridCellMgr");

    private static MemberGridCellMgr _singleton;
    public static MemberGridCellMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new MemberGridCellMgr();
            return _singleton;
        } 
    }



    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "id"; }
    

    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return BoConstants.MemberGridCellTable;
    }



    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        throw new NotSupportedException("load()");
    }

    /**
     * loads the GridCells for a gridId
     * ArrayList[GridCell]
     */
    public virtual ArrayList loadForEvent(
        Event theEvent)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetMemberGridCells");
            command.Parameters.Add("@eventId", SqlDbType.Int).Value = theEvent.id;
            reader = command.ExecuteReader();

            ArrayList dbObjects = new ArrayList();
            while (reader.Read())
            {
                DbObject dbObject = load(reader, theEvent);
                dbObjects.Add(dbObject);
            }

            return dbObjects;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }



    /**
     * load the object at the reader's current row
     */
    public DbObject load(SqlDataReader reader, Event theEvent)
    {
        int memberId1 = (int)(decimal) reader["member1"];
        int memberId2 = (int)(decimal) reader["member2"];
        Member member1 = theEvent.getMember(memberId1);
        Member member2 = theEvent.getMember(memberId2);
        double dCompatibility = (double) reader["compatibility"];
        Compatibility Compatibility = new Compatibility(dCompatibility);

        MemberGridCell gridCell = new MemberGridCell(
            (int)(decimal) reader[BoConstants.Id],
            theEvent,
            member1,
            member2,
            Compatibility,
            (string) reader["coupling"],
            DbUtils.getStringFromReader(reader, "explanation"));

        return gridCell;
    }


    /**
     * loads the explanation for two members
     */
    public string loadExplanation(int eventId, int memberId1, int memberId2)
    {
        string expl = _loadExplanation(eventId, memberId1, memberId2);

        // if it's null, switch the members
        if (expl != null)
            return expl;
        else
            return _loadExplanation(eventId, memberId2, memberId1);
    }


    /**
     * loads the explanation for two members
     */
    private string _loadExplanation(int eventId, int memberId1, int memberId2)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetMemberGridCellExplanation");

            command.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
            command.Parameters.Add("@memberId1", SqlDbType.Int).Value = memberId1;
            command.Parameters.Add("@memberId2", SqlDbType.Int).Value = memberId2;

            reader = command.ExecuteReader();

            ArrayList dbObjects = new ArrayList();
            if (!reader.Read())
                return null;
            return DbUtils.getStringFromReader(reader, "explanation");
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        MemberGridCell mgc = (MemberGridCell) dbObject;
        ArrayList SqlParams = setParams(mgc, true);
        mgc.id = DbUtils.insert("AddMemberGridCell", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        MemberGridCell mgc = (MemberGridCell) dbObject;
        ArrayList SqlParams = setParams(mgc, false);
        DbUtils.update("UpdateMemberGridCell", SqlParams);
    }


    /**
     * Sets command Parameters for everything in host
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(MemberGridCell mgc, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@memberGridCellId", mgc));

        sqlParameters.Add(DbUtils.createIdParam("@eventId", mgc.theEvent));
        sqlParameters.Add(DbUtils.createIdParam("@memberId1", mgc.member1));
        sqlParameters.Add(DbUtils.createIdParam("@memberId2", mgc.member2));
        sqlParameters.Add(DbUtils.createDoubleParam("@compatibility", mgc.compatibility.value));
        sqlParameters.Add(DbUtils.createStringParam("@coupling", mgc.coupling));
        sqlParameters.Add(DbUtils.createStringParam("@explanation", mgc.explanation));
        
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@memberGridCellId"));

        return sqlParameters;
    }

}

}