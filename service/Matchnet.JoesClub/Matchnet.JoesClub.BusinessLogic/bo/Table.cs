using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class Table : DbObject
{
    private static Log _log = new Log("Table");

    private Event _event;      
    private ArrayList _members; // ArrayList[Member]
    private ArrayList _c_couples;
    private DateTime _time;
    private bool _locked;
    private Host _host;
    private Venue _venue;

    public Event theEvent { get { return _event; } set { _event = value; } }
    public DateTime time { get { return _time; } set { _time = value; } }
    public bool locked { get { return _locked; } set { _locked = value; } }
    public Host host { get { return _host; } set { _host = value; } }
    public Venue venue { get { return _venue; } set { _venue = value; } }

    // ArrayList[Member]
    public ArrayList members 
    { 
        get { return _members; } 
        set 
        { 
            _members = value; 
            resetCouples(); 
        } 
    }
      

    /**
     * Constructor.  Called to create new in-memory table.
     */
    public Table(
        Event theEvent,
        ArrayList members) : 
    this(DbConstants.NoId, 
         theEvent, 
         members, 
         theEvent.venue, 
         theEvent.host,
         theEvent.getStartTime(false), 
         false) // locked
    {
    }


    /**
     * Constructor. 
     */
    public Table(
        int id,
        Event theEvent,
        ArrayList members, // ArrayList[Member]
        Venue venue,
        Host host,
        DateTime time,
        bool locked) : base(id)
    {
        _event = theEvent;
        _members = members;
        _venue = venue;
        _host = host;
        _time = time;
        _locked = locked;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return TableMgr.singleton;
    }


    #region completeTable

    /**
     * Handles the completeTable action.  Finds the best people from
     * the waiting area to complete the table.
     */
    public void completeTable()
    {
        // figure out how many men and women we need
        int menNeeded = 3;
        int womenNeeded = 3;
        foreach (Member member in _members)
            if (member.gender == CommonConstants.Male)
                --menNeeded;
            else
                --womenNeeded;
        if (menNeeded < 0)
            _log.error("completeTable() menNeeded=" + menNeeded);
        if (womenNeeded < 0)
            _log.error("completeTable() womenNeeded=" + womenNeeded);

        // get the members in tables
        Hashtable tableMembers = new Hashtable();
        foreach (Table table in _event.tables)
            foreach (Member member in table.members)
                tableMembers[member] = "";

        // get the members in the waiting area
        ArrayList availableMen = new ArrayList();
        ArrayList availableWomen = new ArrayList();
        foreach (EventMember eventMember in _event.eventMembers)
        {
            Member member = eventMember.member;
            if (!tableMembers.ContainsKey(member) &&
                eventMember.memberStatus != MemberStatus.Cancelled &&
                eventMember.memberStatus != MemberStatus.RsvpedNo)
            {
                if (member.gender == CommonConstants.Male)
                    availableMen.Add(member);
                else
                    availableWomen.Add(member);
            }
        }

        // we can't get more people than are available
        if (availableMen.Count < menNeeded)
            menNeeded = availableMen.Count;
        if (availableWomen.Count < womenNeeded)
            womenNeeded = availableWomen.Count;

        // tryMembers is the set of members we're going to evaluate
        int tableSizeNeeded = _members.Count + menNeeded + womenNeeded;
        ArrayList tryMembers = new ArrayList(tableSizeNeeded);
        foreach (Member member in _members)
            tryMembers.Add(member);
        for (int i = _members.Count; i < tableSizeNeeded; i++) // init array capacity
            tryMembers.Add(null);

        // iterate of combinations of available men and women
        double bestCompatibility = double.MaxValue;
        ArrayList bestMembers = new ArrayList();
        int[] menIndexes = firstIndexes(menNeeded);

        // loop over the possible sets of men
        for (bool menOk = true;
             menOk;
             menOk = nextIndexes(menIndexes, 0, availableMen.Count))
        {
            // menIndexes determines which of the available men we're going
            // to try.  Of course, this works even if no men are needed or
            // no women are needed.
            for (int i = 0; i < menNeeded; i++)
            {
                Member tryMan = (Member) availableMen[menIndexes[i]];
                tryMembers[_members.Count + i] = tryMan;
            }

            // loop over the possible sets of women
            int[] womenIndexes = firstIndexes(womenNeeded);
            for (bool womenOk = true;
                 womenOk;
                 womenOk = nextIndexes(womenIndexes, 0, availableWomen.Count))
            {
                for (int i = 0; i < womenNeeded; i++)
                {
                    Member tryWoman = (Member) availableWomen[womenIndexes[i]];
                    tryMembers[_members.Count + menNeeded + i] = tryWoman;
                }

                // evaluate the compatibility
                Compatibility comp = Compatibility.getMembersCompatibility(
                    tryMembers, _event);
                if (comp.value < bestCompatibility)
                {
                    bestCompatibility = comp.value;
                    bestMembers = new ArrayList();
                    foreach (Member member in tryMembers)
                        bestMembers.Add(member);
                }
            }
        }

        // save results
        _members = bestMembers;
        save();
    }


    /**
     * Create the initial indexes array.  Each element of indexes is
     * an index into the set of available men or women.  So, indexes
     * represents a set of men or a set of women which will be evaluated
     * for compatibility.  No two values in indexes can be the same, e.g.
     * { 0, 1, 1 } is disallowed.  Also, we don't want to evaluate equivalent
     * sets more than once, e.g. { 0, 1, 2 } == { 0, 2, 1 }.  So, we start
     * with the set { n, n-1, ... 2, 1, 0 } and maintain this rule: 
     * indexes[i] > indexes[j] for all i < j.
     */
    private int[] firstIndexes(int size)
    {
        int[] indexes = new int[size];
        for (int i = 0; i < size; i++)
            indexes[i] = size - 1 - i;
        return indexes;
    }


    /**
     * Advance to the next indexes array.  Returns false
     * when there are no more indexes.
     */
    private bool nextIndexes(int[] indexes, int digit, int mod)
    {
        if (digit >= indexes.Length)
            return false;
        ++indexes[digit];
        if (indexes[digit] < mod)
            return true;
        else
        {
            if (!nextIndexes(indexes, digit + 1, mod))
                return false;
            indexes[digit] = indexes[digit + 1];
            return nextIndexes(indexes, digit, mod);
        }
    }

    #endregion completeTable
    
    #region xml

    /**
     * generate xml for the object
     * <table ...>
     */
    public void toXml(XmlHelper xh)
    {
        ArrayList alAttrs = new ArrayList();
        alAttrs.Add("tableId");
        alAttrs.Add("t" + id);

        if (_time != CommonConstants.DateTimeNone)
        {
            alAttrs.Add("time");
            alAttrs.Add(_time.ToString("h:mmtt"));
        }

        // compatibility attribute
        Compatibility comp = Compatibility.getMembersCompatibility(_members, _event);
        //Compatibility comp = Compatibility.getCompatibility(_event, getCouple(0), getCouple(1), getCouple(2));
        string sComp = comp.grade;
        alAttrs.Add("compatibility");
        alAttrs.Add(sComp);

        alAttrs.Add("locked");
        alAttrs.Add(_locked.ToString().ToLower());

        string[] attrs = new string[alAttrs.Count];
        for (int i = 0; i < alAttrs.Count; i++)
            attrs[i] = (string) alAttrs[i];

        xh.startElem("table", false, attrs);

        // members
        foreach (Member member in _members)
            xh.writeSimpleElem("member", member.id);

        // time
        xh.startElem("time", false, null);
        xh.writeText(timeToString());
        xh.endElem(); 

        // venue
        xh.writeSimpleElem("venue", DbUtils.getId(getVenue(true)));

        // host
        xh.writeSimpleElem("host", DbUtils.getId(getHost(true)));

        xh.endElem(); // table
    }


    /**
     * get the venue, possibly using _event's
     */
    private Venue getVenue(bool checkMultiVenue)
    {
        if (!checkMultiVenue)
            return _venue;
        if (!_event.multiVenue || _venue == null)
            return _event.venue;
        return _venue;
    }


    /**
     * get the host, possibly using _event's
     */
    private Host getHost(bool checkMultiVenue)
    {
        if (!checkMultiVenue)
            return _host;
        if (!_event.multiVenue || _host == null)
            return _event.host;
        return _host;
    }


    /**
     * get the time, possibly using _event's
     */
    private DateTime getTime(bool maybeUseEvents)
    {
        if (!maybeUseEvents)
            return _time;
        if (_time == CommonConstants.DateTimeNone)
            return _event.getStartTime(false);
        return _time;
    }


    /**
     * Returns the time as a string
     */
    public string timeToString()
    { 
        DateTime time = getTime(true);
        if (time == CommonConstants.DateTimeNone)
            return "";
        else
            return time.ToString("t");
    }

    #endregion 


    #region saveUserSettings

    /**
     * Saves the changes the user made to the tables
     * JLF currently expects 3 couples.
     */
    public void saveUserSettings(NameValueCollection queryParams)
    {
        string[] values = queryParams["hidden_t" + id].Split(',');

        _members = new ArrayList();
        int numMembers = int.Parse(values[0]);
        for (int i = 0; i < numMembers; i++)
        {
            int memberId = int.Parse(values[i + 1]);
            Member member = _event.getMember(memberId);
            _members.Add(member);
        }
        resetCouples();

        _locked = values[numMembers + 1] == "true";

        // _time
        if (values[numMembers + 2] != "")
            _time = DateTime.Parse(values[numMembers + 2]);
        else
            _time = CommonConstants.DateTimeNone;

        // _venue
        int venueId = int.Parse(values[numMembers + 3]);
        if (venueId == DbConstants.NoId)
            _venue = null;
        else
            _venue = (Venue) VenueMgr.singleton.load(venueId);

        // _host
        int hostId = int.Parse(values[numMembers + 4]);
        if (hostId == DbConstants.NoId)
            _host = null;
        else
            _host = (Host) HostMgr.singleton.load(hostId);


        // save it
        TableMgr.singleton.save(this);
    }

    #endregion 


    #region couples
    

    /**
     * Get the couples for the table.
     */
    public ArrayList couples 
    { 
        get 
        { 
            initCouples();
            return _c_couples; 
        }
    }


     /**
     * Invalidates _c_couples.  Must be called whenever the members change.
     */
    private void resetCouples()
    {
        _c_couples = null;
    }


    /**
     * Figires out the couples from the members and the memberGrid.
     * Sets _c_couples
     */
    private void initCouples()
    {
        if (_c_couples != null) // alreadey initialized
            return;
        _c_couples = new ArrayList();
        if (_members == null)
            return;
        MemberGrid mg = _event.memberGrid;
        Hashtable tableMembers = Misc.arrayListToHashtable(_members);

        // check each couple to see if it's in the table.
        foreach (MemberGridCell couple in mg.getCouples())
            if (tableMembers[couple.member1] != null &&
                tableMembers[couple.member2] != null)
                _c_couples.Add(couple);
    }

    #endregion //couples

}

}