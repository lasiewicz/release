using System;
using Matchnet.JoesClub.ValueObjects;


/**
 * This object represents a call made to a new applicant.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class PhoneInterview : HasMiscAttributes
{
    private static Log _log = new Log("PhoneInterview");

    //private Member _member;
    private int _memberId;
    private InterviewStatus _interviewStatus;
	//private InterviewScheduleStatus _interviewScheduleStatus;
    private string _caller;
    private DateTime _whenCalled;
    private bool _gaveContract;

    //public Member member { get { return _member; } set { _member = value; } }

    public int memberId { get { return _memberId; } set { _memberId = value; } }
    public InterviewStatus interviewStatus { get { return _interviewStatus; } set { _interviewStatus = value; } }
	//public InterviewScheduleStatus interviewScheduleStatus { get { return _interviewScheduleStatus; } set { _interviewScheduleStatus = value; } }
    public string caller { get { return _caller; } set { _caller = value; } }
    public DateTime whenCalled { get { return _whenCalled; } set { _whenCalled = value; } }
    public bool gaveContract { get { return _gaveContract; } set { _gaveContract = value; } }

	//Misc Attributes
	public InterviewScheduleStatus interviewScheduleStatus 
	{
		get { return (InterviewScheduleStatus) getIntValue("interviewScheduleStatus"); }
		set { setValue("interviewScheduleStatus", (int) value); } 
	}

	public DateTime scheduleDate
	{
		get { return getDateTimeValue("scheduleDate"); }
		set { setValue("scheduleDate", value); }
	}


    /**
     * Constructor
     */
    public PhoneInterview(
        int id, 
        int memberId,
        InterviewStatus interviewStatus,
        string caller,
        DateTime whenCalled,
        bool gaveContract) : base(id)
    {
//        _member = member;
        _memberId = memberId;
        _interviewStatus = interviewStatus;
        _caller = caller;
        _whenCalled = whenCalled;
        _gaveContract = gaveContract;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return PhoneInterviewMgr.singleton;
    }

    #region Logable Members

    public override void log(ObjectLogger objectLogger)
    {
        base.log(objectLogger);
        objectLogger.log("_memberId", _memberId);
        objectLogger.log("_interviewStatus", _interviewStatus);
        objectLogger.log("_caller", _caller);
        objectLogger.log("_whenCalled", _whenCalled);
        objectLogger.log("_gaveContract", _gaveContract);
    }

    #endregion
}


}