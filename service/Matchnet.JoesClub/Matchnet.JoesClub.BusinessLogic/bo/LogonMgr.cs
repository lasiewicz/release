using System;
using System.Collections;
using System.Web;
using System.Data;
using System.Data.SqlClient;

using Matchnet.JoesClub.ValueObjects;

/**
 * Manages logging on, off, cookies
 */
namespace Matchnet.JoesClub.BusinessLogic
{


public class LogonMgr
{
    public static LogonMgr Instance = new LogonMgr();


    /**
     * Gets the logged in member's id. 
     * Returns DbConstants.NoId if there isn't one.
     */
    public int getMemberId(PageRequest pageRequest) 
    {
        if (pageRequest.memberId != DbConstants.NoId)
            return pageRequest.memberId;

        if (pageRequest.sparkId != CommonConstants.NoValueInt)        
            return MemberMgr.singleton.getMemberIdFromSparkId(pageRequest.sparkId);

        return DbConstants.NoId;
    }


    /**
     * Logs a user on and returns the AuthenticationStatus.  
     */
    public AuthenticationStatus logOn(
        PageResponse pageResponse, 
        string emailAddress, string password) 
    {
        AuthenticationStatus authStatus;

        // see if the email matches a JoesClub member
        int memberId = 
            MemberMgr.singleton.getMemberIdFromEmailAddress(emailAddress);
        Member member = null;

        if (memberId == DbConstants.NoId)
            return AuthenticationStatus.NotAJoesClubMember;

        member = (Member) MemberMgr.singleton.load(memberId);

        // see if they're active
        if (member.deleted)
            return AuthenticationStatus.MemberDeleted;
        else if (member.activityStatus == MemberActivityStatus.Applicant ||
                    member.activityStatus == MemberActivityStatus.Pending)
            return AuthenticationStatus.ProcessingMembership;
        else if (!member.isActive())
            return AuthenticationStatus.NotActive;

        // make sure there's no discrepencies with the jdateMember
        else if (member.jdateMember == null ||
            member.sparkId != member.jdateMember.MemberID ||
            member.emailAdd.ToLower() != member.jdateMember.EmailAddress.ToLower())
        {
            return AuthenticationStatus.NotAJDateMember;
        }

        // authenticate through JDate
        authStatus = JDateMemberUtil.Instance.authenticate(emailAddress, password);


        // add or remove guid depending on if the authentication succeeded
        //string guid = getGuid(pageRequest, pageResponse);
        if (authStatus != AuthenticationStatus.Authenticated)
            return authStatus;

        // we're good
        pageResponse.action = PageResponse.ActionLogon;
        pageResponse.memberId = member.id;
        pageResponse.sparkId = member.sparkId;

        return AuthenticationStatus.Authenticated;
    }


    /**
     * Logon from Admin calendar icon 
     */
    public AuthenticationStatus logOn(
        PageResponse pageResponse, 
        string guid) 
    {
        int memberId = getMemberGuidFromDb(guid);
        if (memberId == DbConstants.NoId)
            return AuthenticationStatus.NotAJoesClubMember;

        pageResponse.action = PageResponse.ActionLogonJoesClubOnly;
        pageResponse.memberId = memberId;
        pageResponse.sparkId = CommonConstants.NoValueInt;
        return AuthenticationStatus.Authenticated;
    }

 
    /**
     * Logs the current member off.
     */
    public void logOff(PageResponse pageResponse) 
    {
        pageResponse.action = PageResponse.ActionLogoff;
        pageResponse.memberId = CommonConstants.NoValueInt;
        pageResponse.sparkId = CommonConstants.NoValueInt;
    }




    /**
     * Gets the guid from the request.  If there isn't one, 
     * sets the cookie in the response.
    private string getGuid(PageRequest pageRequest, PageResponse pageResponse) 
    {
        // get the cookie guid from the request
        string guid = pageRequest.cookieGuid;

        // create a new cookie if necessary
        if (guid == null)
        {
            guid = Guid.NewGuid().ToString();
            pageResponse.cookieGuid = guid;
        }

        return guid;
    }
     */



    /**
     * Adds a memberId and guid to the database.
     * returns the guid
     */
    public string addMemberGuidToDb(int memberId) 
    {
        // Create a MiscAttribute using a guid for the name and 
        // the memberId for the value
        string guid = Guid.NewGuid().ToString();
        MiscAttribute miscAttribute = new MiscAttribute(guid, null, memberId);
        miscAttribute.save();

        return guid;
    }


    /**
     * Finds memberId for the guid in the database.  If there is one,
     * the MiscAttribute is deleted and the memberId is returned.
     */
    private int getMemberGuidFromDb(string guid) 
    {
        // find the MiscAttribute containing the memberId
        ArrayList miscAttributes = MiscAttributeMgr.singleton.loadByName(guid);
        if (miscAttributes.Count != 1)
            return DbConstants.NoId;
        MiscAttribute miscAttribute = (MiscAttribute) miscAttributes[0];
        int memberId = miscAttribute.getIntValue();

        // delete the miscAttribute
        MiscAttributeMgr.singleton.delete(miscAttribute.id);

        return memberId;
    }
}
}