using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the host table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class HostMgr : DbObjectMgr
{
    #region basic stuff
    private static Log _log = new Log("HostMgr");

    private static HostMgr _singleton;
    public static HostMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new HostMgr();
            return _singleton;
        } 
    }


    /**
     * Create an empty Host object.
     */
    public override DbObject createEmptyDbObject(int id) 
    { 
        return new Host(id);
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "hostId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "Host";
    }

    #endregion // basic stuff

    #region standard insert, update, load

    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        Host host = (Host) dbObject;
        ArrayList SqlParams = setParams(host, true);
        host.id = DbUtils.insert("AddHost", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        Host host = (Host) dbObject;
        ArrayList SqlParams = setParams(host, false);
        DbUtils.update("UpdateHost", SqlParams);
    }


    /**
     * Sets command Parameters for everything in host
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(Host host, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@hostId", host));
        sqlParameters.Add(DbUtils.createStringParam("@firstName", host.firstName));
        sqlParameters.Add(DbUtils.createStringParam("@lastName", host.lastName));
        sqlParameters.Add(DbUtils.createStringParam("@imageUrl", host.image));
        sqlParameters.Add(DbUtils.createStringParam("@phoneNum", host.phone));
        sqlParameters.Add(DbUtils.createStringParam("@phoneNum2", host.phone2));
        sqlParameters.Add(DbUtils.createStringParam("@emailAddress", host.emailAddress));
        sqlParameters.Add(DbUtils.createIntParam("@availableNights", host.availableNights));
        sqlParameters.Add(DbUtils.createStringParam("@manager", host.manager));
        sqlParameters.Add(DbUtils.createStringParam("@notes", host.notes));
        sqlParameters.Add(DbUtils.createBoolParam("@deleted", host.deleted));
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@hostId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        Host host = new Host(
            (int)(decimal) reader["hostId"],
            DbUtils.getStringFromReader(reader, "firstName"),
            DbUtils.getStringFromReader(reader, "lastName"),
            DbUtils.getStringFromReader(reader, "imageUrl"),
            DbUtils.getStringFromReader(reader, "phoneNum"),
            DbUtils.getStringFromReader(reader, "phoneNum2"),
            DbUtils.getStringFromReader(reader, "emailAddress"),
            DbUtils.getIntFromReader(reader, "availableNights", 0),
            DbUtils.getStringFromReader(reader, "manager"),
            DbUtils.getStringFromReader(reader, "notes"),
            DbUtils.getBoolFromReader(reader, "deleted"));
        return host;
    }

    #endregion // standard insert, update, load


    /**
     * loads the hosts, ordering by name.
     */
    public ArrayList loadAllByName()
    {
        // GetAllHosts takes an optional @orderByName param
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@orderByName", true) };

        // load the hosts
        return load("GetAllHosts", sqlParameters);
    }

}

}