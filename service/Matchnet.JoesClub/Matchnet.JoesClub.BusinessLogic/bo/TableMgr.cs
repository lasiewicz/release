using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the table table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class TableMgr : DbObjectMgr
{
    private static Log _log = new Log("TableMgr");

    // used to generate random tables
    private Random _random = new Random();

    private const int NoMeasurementValue = -1;

    private static TableMgr _singleton;
    public static TableMgr singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new TableMgr();
            return _singleton;
        } 
    }


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "id"; }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        Table table = (Table) dbObject;
        ArrayList SqlParams = setParams(table, true);
        table.id = DbUtils.insert("AddTable", SqlParams);

        // insert TableMembers
        insertTableMembers(table);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        Table table = (Table) dbObject;
        ArrayList SqlParams = setParams(table, false);
        DbUtils.update("UpdateTable", SqlParams);

        // update TableMembers
        deleteTableMembers(table);
        insertTableMembers(table);
    }


    /**
     * Sets command Parameters for everything in host
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(Table table, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@tableId", table));

        sqlParameters.Add(DbUtils.createIdParam("@eventId", table.theEvent));
        sqlParameters.Add(DbUtils.createDateTimeParam("@time", table.time));
        sqlParameters.Add(DbUtils.createBoolParam("@locked", table.locked));
        sqlParameters.Add(DbUtils.createIdParam("@venueId", table.venue));
        sqlParameters.Add(DbUtils.createIdParam("@hostId", table.host));

        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@tableId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        throw new NotSupportedException("load()");
    }


    /**
     * load the object at the reader's current row
     */
    public DbObject load(SqlDataReader reader, Event theEvent)
    {
        // save venue and host ids in empty objects
        int venueId = DbUtils.getIdFromReader(reader, "VenueId");
        Venue venue = (Venue) 
            VenueMgr.singleton.createEmptyDbObject(venueId);

        int hostId = DbUtils.getIdFromReader(reader, "HostId");
        Host host = (Host) 
            HostMgr.singleton.createEmptyDbObject(hostId);

        DateTime time = DbUtils.getDateTimeFromReader(reader, "time");
        bool locked = DbUtils.getBoolFromReader(reader, "locked", false);

        Table table = new Table(
            (int)(decimal) reader[BoConstants.Id],
            theEvent,
            null, // members set later
            venue,
            host,
            time,
            locked);

        return table;
    }


    /**
     * Called after DbObjectMgr loads the object.  Sets the table's members
     * and couples. 
     */
    public override void afterLoad(DbObject dbObject)
    {
        Table table = (Table) dbObject;

        // load venue and host
        table.venue = (Venue) table.venue.emptyToReal();
        table.host = (Host) table.host.emptyToReal();

        // load members and couples
        SqlConnection conn = null;
        SqlDataReader reader = null;
        try
        {
            conn = DbUtils.getConnection();

            SqlCommand command = new SqlCommand("GetTableMembers", conn);
            command.CommandType = CommandType.StoredProcedure;
            DbUtils.setIdParam("@tableId", table, command);
            reader = command.ExecuteReader();

            ArrayList members = new ArrayList();
            Event ev = table.theEvent;
            int currentCoupleId = DbConstants.NoId;
            while (reader.Read())
            {
                // get the member
                int memberId = DbUtils.getIdFromReader(reader, "memberId");
                Member member = ev.getMember(memberId);
                members.Add(member);
            }

            table.members = members;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }



    /**
     * loads the Tables for an Event
     * ArrayList[Table]
     */
    public ArrayList loadForEvent(Event theEvent)
    {
        _log.debug("loadForEvent()");
        SqlConnection conn = null;
        SqlDataReader reader = null;
        ArrayList dbObjects = new ArrayList();

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = new SqlCommand("GetTablesForEvent", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@eventId", SqlDbType.Int).Value = theEvent.id;
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                DbObject dbObject = load(reader, theEvent);
                dbObjects.Add(dbObject);
            }

            // call afterLoad for each DbObject in case the subclass
            // needs to do more work such as loading associated objects.
            // Close reader first because you can't have >1 reader open.
            reader.Close();
            reader = null;
            foreach (DbObject dbObject in dbObjects)
                afterLoad(dbObject);
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }


        if (dbObjects.Count == 0)
            return null;
        else
            return dbObjects;
    }
    

    /**
     * Deletes the tables for the given event
     */
    public void saveForEvent(Event theEvent)
    {
        // insert new tables
        if (theEvent.tables != null)
            foreach (Table table in theEvent.tables)
                save(table);
    }
     

    /**
     * Deletes the tables for the given event
     */
    public void deleteForEvent(Event ev, bool unlockedOnly)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@eventId", ev.id),
            new SqlParameter("@unlockedOnly", unlockedOnly) };
        DbUtils.runStoredProc("DeleteTablesForEvent", sqlParameters);
    }


    /**
     * Deletes the TableMembers for the given table
     */
    private void deleteTableMembers(Table table)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            new SqlParameter("@tableId", table.id) };
        DbUtils.runStoredProc("DeleteTableMembers", sqlParameters);
    }


    /**
     * Inserts the TableMembers for the given table
     */
    private void insertTableMembers(Table table)
    {
        foreach (Member member in table.members)
            insertTableMember(table, member);
    }


    /**
     * Inserts a row into the TableMembers table
     */
    private void insertTableMember(Table table, Member member)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();

            // insert TableMembers
            SqlCommand command = new SqlCommand("AddTableMember", conn);
            command.CommandType = CommandType.StoredProcedure;
            DbUtils.setIdParam("@tableId", table, command);
            DbUtils.setIdParam("@memberId", member, command);
            command.Parameters.Add(DbUtils.createIdOutParam("@tableMemberId"));
            reader = command.ExecuteReader();
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }

    
    }


    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return BoConstants.TableTable;
    }

}

}