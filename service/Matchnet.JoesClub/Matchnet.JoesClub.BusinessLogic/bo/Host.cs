using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;

namespace Matchnet.JoesClub.BusinessLogic
{
public class Host : DbObject
{
    private static Log _log = new Log("Host");

    private string _firstName;
    private string _lastName;
    private string _image;
    private string _phone;
    private string _phone2;
    private string _emailAddress;
    private int _availableNights; // BitMask.daysOfWeek
    private string _manager;
    private string _notes;
    private bool _deleted;

    public string firstName { get { return _firstName; } set { _firstName = value; } }
    public string lastName { get { return _lastName; } set { _lastName = value; } }
    public string image { get { return _image; } set { _image = value; } }
    public string phone { get { return _phone; } set { _phone = value; } }
    public string phone2 { get { return _phone2; } set { _phone2 = value; } }
    public string emailAddress { get { return _emailAddress; } set { _emailAddress = value; } }
    public int availableNights { get { return _availableNights; } set { _availableNights = value; } }
    public string manager { get { return _manager; } set { _manager = value; } }
    public string notes { get { return _notes; } set { _notes = value; } }
    public bool deleted { get { return _deleted; } set { _deleted = value; } }


    /**
     * Constructor
     */
    public Host(
        int id, 
        string firstName,
        string lastName,
        string image,
        string phone,
        string phone2,
        string emailAddress,
        int availableNights, 
        string manager,
        string notes,
        bool deleted) : base(id)
    {
        _firstName = firstName;
        _lastName = lastName;
        _image = image;
        _phone = phone;
        _phone2 = phone2;
        _emailAddress = emailAddress;
        _availableNights = availableNights;
        _manager = manager;
        _notes = notes;
        _deleted = deleted;
    }


    /**
     * Constructor.  Creates an empty DbObject.
     */
    public Host(int id) : base(id, true)
    {
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return HostMgr.singleton;
    }


    /**
     * generate xml containg the firstName and is for a select list
     */
    public static void selectListXml(XmlHelper xh)
    {
        xh.startElem("hosts", false, null);

        ArrayList hosts = HostMgr.singleton.loadAll();
        foreach (Host host in hosts)
        {
            xh.startElem("host", false, null);
            xh.writeSimpleElem("id", host.id.ToString());
            xh.writeSimpleElem("name", host.fullName());
            xh.endElem(); // host
        }
        xh.endElem(); // hosts
    }


    /**
     * generate xml
     */
    public void toXml(XmlHelper xh)
    {
        xh.startElem("host", false, null);

        xh.writeSimpleElem("id", id.ToString());
        xh.writeSimpleElem("name", fullName()); // can remove this when the UI stops using it
        xh.writeSimpleElem("firstName", _firstName);
        xh.writeSimpleElem("lastName", _lastName);
        xh.writeSimpleElem("image", _image);

        xh.endElem(); // host
    }


    /**
     * generate the full name
     */
    private string fullName()
    {
        string name = _firstName;
        if (_lastName != null)
            name += " " + _lastName;
        return name;
    }


    /**
     * generate <hosts> containing all hosts
     */
    public static void allToXml(XmlHelper xh)
    {
        xh.startElem("hosts", false, null);

        ArrayList hosts = HostMgr.singleton.loadAllByName();
        foreach (Host host in hosts)
            host.toXml(xh);

        xh.endElem(); // hosts
    }


    /**
     * Load events that the host hosted
     * returns ArrayList[Event]
     */
    public ArrayList getEventsHosted()
    {
        return EventMgr.singleton.loadForHost(id);
    }
}


}