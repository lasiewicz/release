using System;
using Matchnet.JoesClub.BusinessLogic.WebReference;


/**
 * Class used to call stormpost
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class StormPost
{
    // singleton object
    private static StormPost _singleton;
    public static StormPost singleton 
    { 
        get 
        { 
            if (_singleton == null)
                _singleton = new StormPost();
            return _singleton; 
        } 
    }

    private SoapRequestProcessorService _c_service;


    /**
     * constructor
     */
    private StormPost()
    {
    }


    /**
        * Send a test email through stormpost
        */
    public void sendEmail(
        int templateID, string recipient, 
        string[] tokenNames, string[] tokenVals)
    {
        initService();

        // merge tokens into name=val format
        string[] tokens = new string[tokenNames.Length];
        for (int i = 0; i < tokenNames.Length; i++)
            tokens[i] = tokenNames[i] + "=" + tokenVals[i];

        _c_service.sendMessageFromTemplate(templateID, recipient, tokens);
    }


    /**
     * initialize _c_service
     */
    public void initService()
    {
        if (_c_service != null)
            return;

        _c_service = new SoapRequestProcessorService();
        _c_service.authenticationValue = new authentication();
        _c_service.authenticationValue.username="StormpostSOAP@spark.net";
        _c_service.authenticationValue.password = "h4x0r";
    }
}
}


