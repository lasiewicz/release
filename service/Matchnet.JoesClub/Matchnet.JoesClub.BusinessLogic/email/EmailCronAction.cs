using System;
using Matchnet.JoesClub.ValueObjects;


/**
 * Interface for classes called by CronThread
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailCronAction : CronAction
{
    private static Log _log = new Log("EmailCronAction");


    /**
     * Do the action
     */
    public void doAction()
    {
        _log.debug("doAction()");

        Nudge.createEmailInstances(true);
        TMinus.createEmailInstances(true);
        EventMemberMgr.singleton.updateInviteStatus();
        //DailyReport.createEmailInstance(true);
    }

}
}
