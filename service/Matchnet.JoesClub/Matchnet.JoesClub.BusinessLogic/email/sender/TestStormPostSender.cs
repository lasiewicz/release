using System.Collections;
using System.Text;
using Matchnet.JoesClub.ValueObjects;




/**
 * Writes emails to the log file.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class TestStormPostSender : Sender
{
    private static Log _log = new Log("TestStormPostSender");

    public static TestStormPostSender singleton = new TestStormPostSender();


    /**
     * Constructor
     */
    private TestStormPostSender()
    {

    }


    /**
     * Sender subclasses implement this to send the email to one recipient with
     * the given tokens.
     * tokens = ArrayList[EmailToken]
     */
    protected override void send(
        EmailInstance emailInstance,
        EmailRecipient recipient,
        ArrayList tokens)
    {
        // send an email to each of the debug recipients
        string[] recipientEmailAddrs = EmailConfig.singleton.getDebugRecipients();
        foreach (string recipientEmailAddr in recipientEmailAddrs)
        {
            string[] tokenNames = new string[tokens.Count];
            string[] tokenVals = new string[tokens.Count];
            for (int i = 0; i < tokens.Count; i++)
            {
                EmailToken token = (EmailToken) tokens[i];
                tokenNames[i] = token.tokenDef.name;
                tokenVals[i] = token.val;
            }

            // send it
            if (emailInstance.emailDef.templateId != CommonConstants.NoValueInt)
            {
                StormPost.singleton.sendEmail(
                    emailInstance.emailDef.templateId,
                    recipientEmailAddr,
                    tokenNames,
                    tokenVals);
            }
        }

        // log it too
        LogSender.singleton.log(emailInstance, recipient, tokens);
    }
}
}
