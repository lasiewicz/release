using System.Collections;
using System.Text;
using Matchnet.JoesClub.ValueObjects;




/**
 * Sends email to the recipient through stormpost.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class StormPostSender : Sender
{
    private static Log _log = new Log("StormPostSender");

    public static StormPostSender singleton = new StormPostSender();


    /**
     * Sender subclasses implement this to send the email to one recipient with
     * the given tokens.
     * tokens = ArrayList[EmailToken]
     */
    protected override void send(
        EmailInstance emailInstance,
        EmailRecipient recipient,
        ArrayList tokens)
    {
        string recipientEmailAddr = recipient.getEmailAddress();
        if (recipientEmailAddr == null)
            return;

        // set up tokens
        string[] tokenNames = new string[tokens.Count];
        string[] tokenVals = new string[tokens.Count];
        for (int i = 0; i < tokens.Count; i++)
        {
            EmailToken token = (EmailToken) tokens[i];
            tokenNames[i] = token.tokenDef.name;
            tokenVals[i] = token.val;
        }

        // send it
        if (emailInstance.emailDef.templateId != CommonConstants.NoValueInt)
        {
            StormPost.singleton.sendEmail(
                emailInstance.emailDef.templateId,
                recipientEmailAddr,
                tokenNames,
                tokenVals);
            
            // send an email to each of the debug recipients
            string[] debugEmailAddrs = EmailConfig.singleton.getDebugRecipients();
            foreach (string debugEmailAddr in debugEmailAddrs)
            {
                StormPost.singleton.sendEmail(
                    emailInstance.emailDef.templateId,
                    debugEmailAddr,
                    tokenNames,
                    tokenVals);
            }
        }

        // log it too
        LogSender.singleton.log(emailInstance, recipient, tokens);
    }
}
}
