using System;
using System.Collections;
using System.Text;
using System.Threading;
using Matchnet.JoesClub.ValueObjects;



/**
 * Writes emails to the log file.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class AsyncSender : Sender
{
    private static Log _log = new Log("AsyncSender");

    private static int _sleepTime = 5000;

    private ArrayList _queue; // ArrayList[EmailInstance]
    private bool _awake;

    private Thread _thread;


    // singleton object
    private static AsyncSender _singleton;
    public static AsyncSender singleton 
    { 
        get 
        { 
            lock (typeof(AsyncSender))
            {
                if (_singleton == null)
                    _singleton = new AsyncSender();
            }
            return _singleton;
        }
    }


    /**
     * Constructor
     */
    private AsyncSender()
    {
        _queue = new ArrayList();
        _awake = false;
    }


    /**
     * stop the thread
     */
    public void stop() 
    { 
        if (_thread == null)
            return;
        _log.warning("stop() starting...");
        lock (this)
        {
            _thread.Abort();
            bool terminated = _thread.Join(60000);
            if (!terminated)
                _log.warning("restart() Couldn't terminate thread.");
            else
                _thread = null;
        }
        _log.warning("stop() finished.");
    }


    /**
     * start the thread
     */
    public void start() 
    { 
        lock (this)
        {
            ThreadStart threadStart = new ThreadStart(run);
            _thread = new Thread(threadStart);
            _thread.Start();
        }
    }


    /**
     * Run the thread
     */
    public void run()
    {
        while (true)
        {
            try
            {
                if (_queue.Count > 0)
                {
                    wakeUp();

                    // use the Sender in web.config
                    Sender sender = EmailConfig.singleton.getSender();
                    while (_queue.Count > 0)
                    {
                        ArrayList queue;
                        lock(this)
                        {
                            queue = _queue;
                            _queue = new ArrayList();
                        }

                        // send the emails
                        foreach (EmailInstance ei in queue)
                        {
                            try
                            {
                                sender.send(ei);
                            }
                            catch (Exception ex)
                            {
                                _log.error(ex);
                                if (ex is ThreadAbortException)
                                    return;
                            }
                        }
                    }
                    goToSleep(true);
                }

                else
                    goToSleep(false);
            }
            catch (Exception ex)
            {
                _log.error(ex);
                if (ex is ThreadAbortException)
                    return;
            }
        }

    }


    /**
     * Kind of messy, but the code expect each thread to set up some stuff
     */
    private void wakeUp()
    {
        LogFile.open();
        DbObjectCache.create();

        if (_awake)
            _log.warning("wakeUp() _awake == true");
        _awake = true;
    }


    /**
     * Put the thread to sleep
     */
    private void goToSleep(bool cleanUp)
    {
        if (cleanUp) 
        {
            if (!_awake)
                _log.warning("goToSleep() _awake == false");
            LogFile.close();
            DbObjectCache.destroy();
        }

        _awake = false;
        Thread.Sleep(_sleepTime);
    }


    /**
     * Send the email to all the recipients
     */
    public override void send(EmailInstance emailInstance)
    {
        // queue it
        lock(this)
        {
            if (_thread == null)
                start();
            _queue.Add(emailInstance);
        }
    }


    /**
     * Sender subclasses implement this to send the email to one recipient with
     * the given tokens.
     * tokens = ArrayList[EmailToken]
     */
    protected override void send(
        EmailInstance emailInstance,
        EmailRecipient recipient,
        ArrayList tokens)
    {
        // this should never get called
        throw new MethodNotSupportedException("send()");        
    }

}
}
