using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Sender is used to send the emails.  Subclasses implement the actualy
 * sending.  
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class Sender 
{
    private static Log _log = new Log("Sender");


    /**
     * Returns the email Sender specified by "EmailSender"
     * in web.config.
    public static Sender defaultSender
    {        
        get { return AsyncSender.singleton; }
    }
     */


    /**
     * Send the email to all the recipients
     */
    public virtual void send(EmailInstance emailInstance)
    {
        if (emailInstance.recipients == null || 
            emailInstance.recipients.Count == 0)
            return;

        // save in DB
        emailInstance.sentTime = DateTime.Now;
        emailInstance.save();

        // allRecipientTokens will have the non-recipient specific tokens
        ArrayList allRecipientTokens = new ArrayList();

        // check if it's OK to send to each recipient
        ArrayList recipients = new ArrayList();
        foreach (EmailRecipient recipient in emailInstance.recipients)
            if (recipient.okToSend())
                recipients.Add(recipient);

        // sort all the tokens according to their recipient
        // recipientTokenArrays = Hashtable[EmailRecipient --> ArrayList[EmailToken]]
        Hashtable recipientTokenArrays = new Hashtable();
        foreach (EmailRecipient recipient in recipients)
            recipientTokenArrays[recipient] = new ArrayList();
        foreach (EmailToken token in emailInstance.tokens)
        {
            ArrayList recipientTokens = allRecipientTokens;
            EmailRecipient recipient = token.recipient;
            if (recipient != null)
                recipientTokens = (ArrayList) recipientTokenArrays[recipient];
            recipientTokens.Add(token);
        }

        // add the "all" tokens to each recipient token array
        foreach (EmailRecipient recipient in recipients)
        {
            ArrayList recipientTokens = (ArrayList) recipientTokenArrays[recipient];
            foreach (EmailToken token in allRecipientTokens)
                recipientTokens.Add(token);
        }

        // send the email to each recipient
        foreach (EmailRecipient recipient in recipients)
        {
            ArrayList recipientTokens = (ArrayList) recipientTokenArrays[recipient];
            send(emailInstance, recipient, recipientTokens);
        }
    }


    /**
     * Subclasses implement this to send the email to one recipient with
     * the given tokens.
     * tokens = ArrayList[EmailToken]
     */
    protected abstract void send(
        EmailInstance emailInstance,
        EmailRecipient recipient,
        ArrayList tokens);

}

}
