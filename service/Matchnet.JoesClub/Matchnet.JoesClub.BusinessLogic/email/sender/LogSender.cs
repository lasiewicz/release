using System.Collections;
using System.Text;
using Matchnet.JoesClub.ValueObjects;



/**
 * Writes emails to the log file.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class LogSender : Sender
{
    private static Log _log = new Log("LogSender");

    public static LogSender singleton = new LogSender();


    /**
     * Sender subclasses implement this to send the email to one recipient with
     * the given tokens.
     * tokens = ArrayList[EmailToken]
     */
    protected override void send(
        EmailInstance emailInstance,
        EmailRecipient recipient,
        ArrayList tokens)
    {
        log(emailInstance, recipient, tokens);
    }


    /**
     * Log it
     */
    public void log(
        EmailInstance emailInstance,
        EmailRecipient recipient,
        ArrayList tokens)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("\nemailDef: ");
        sb.Append(emailInstance.emailDef.name);

        sb.Append("\ntemplate id: ");
        sb.Append(emailInstance.emailDef.templateId);

        sb.Append("\nevent id: ");
        sb.Append(DbUtils.getId(emailInstance.ev));

        sb.Append("\nto: ");
        sb.Append(recipient.emailAddress);

        foreach (EmailToken token in tokens)
        {
            sb.Append("\n");
            if (token.recipient != null)
                sb.Append("recipient ");
            sb.Append("token: ");
            sb.Append(token.tokenDef.name);
            sb.Append("=");
            sb.Append(token.val);
        }
        sb.Append("\n");

        _log.write(sb.ToString());
    }

}
}
