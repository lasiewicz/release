using System;

/**
 * Constants used by the whole system
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailConstants
{
    // EmailDef Names
    public const string Welcome = "Welcome";
    public const string ReceivedInviteReq = "ReceivedInviteReq";
    public const string VipEventAd = "VipEventAd";
    public const string Alternate = "Alternate";
    public const string EventFull = "EventFull";
    public const string Invitation = "Invitation";
    public const string Nudge = "Nudge";
    public const string RsvpConfirmation = "RsvpConfirmation";
    public const string TMinus5 = "TMinus5";
    public const string TMinus1 = "TMinus1";
    public const string EventCancelled = "EventCancelled";
    public const string AfterDinner = "AfterDinner";
    public const string NoShow = "NoShow";
    public const string DailyReport = "DailyReport";
    public const string MemberCancelled = "MemberCancelled";
    public const string ApplicationReply = "ApplicationReply";

    // EmailTokenDef names
    public const string EventDate = "EventDate";
    public const string EventAttire = "EventAttire";
    public const string EventSpecialInstructions = "EventSpecialInstructions";
    public const string EventTypeName = "EventTypeName";
    public const string EventPrice = "EventPrice";
    public const string MemberEmailAddress = "MemberEmailAddress";
    public const string MemberFirstName = "MemberFirstName";
    public const string MemberLastName = "MemberLastName";
    public const string MemberImage1 = "MemberImage1";
    public const string MemberImage2 = "MemberImage2";
    public const string MemberImage3 = "MemberImage3";
    public const string MemberImage4 = "MemberImage4";
    public const string MemberImage5 = "MemberImage5";
    public const string MemberCity1 = "MemberCity1";
    public const string MemberCity2 = "MemberCity2";
    public const string MemberCity3 = "MemberCity3";
    public const string MemberCity4 = "MemberCity4";
    public const string MemberCity5 = "MemberCity5";
    public const string MemberJdateUrl1 = "MemberJdateUrl1";
    public const string MemberJdateUrl2 = "MemberJdateUrl2";
    public const string MemberJdateUrl3 = "MemberJdateUrl3";
    public const string MemberJdateUrl4 = "MemberJdateUrl4";
    public const string MemberJdateUrl5 = "MemberJdateUrl5";
    public const string TableTime = "TableTime";
    public const string VenueName = "VenueName";
    public const string VenueAddress = "VenueAddress";
    public const string VenueAddress2 = "VenueAddress2";
    public const string VenueCity = "VenueCity";
    public const string VenueState = "VenueState";
    public const string VenueZipCode = "VenueZipCode";
    public const string VenueWebSiteUrl = "VenueWebSiteUrl";
    public const string VenueMapUrl = "VenueMapUrl";
    public const string VenuePhone = "VenuePhone";
    public const string VenueParking = "VenueParking";
    public const string VenueMeetingLocation = "VenueMeetingLocation";
    public const string HostFirstName = "HostFirstName";
    public const string HostImage = "HostImage";
    public const string HostPhone = "HostPhone";
    public const string HostEmailAddress = "HostEmailAddress";
    public const string VipEventAdCommonality = "VipEventAdCommonality";
    public const string MemberCancelledAction = "MemberCancelledAction";
    public const string InvitationLink = "InvitationLink";
    public const string LoginLink = "LoginLink";
    public const string OptOutLink = "OptOutLink";    
    public const string WelcomeLink = "WelcomeLink";
    public const string UxUrl = "UxUrl";
    public const string AcceptLink = "AcceptLink";
    public const string DeclineLink = "DeclineLink";

    // TokenDef Sources
    public const string Source_None   = "None";
    public const string Source_Event  = "Event";
    public const string Source_Member = "Member";
    public const string Source_Venue  = "Venue";
    public const string Source_Host   = "Host";
    
}
}

