using System.Collections;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailTokenDef : DbObject
{
    private static Log _log = new Log("EmailTokenDef");

    private EmailDef _emailDef;
    private string _name;
    private bool _isPerRecipient;
    private bool _required;
    private string _defaultVal;
    private string _source;

    public EmailDef emailDef { get { return _emailDef; } set { _emailDef = value; } }
    public string name { get { return _name; } set { _name = value; } }
    public bool isPerRecipient { get { return _isPerRecipient; } set { _isPerRecipient = value; } }
    public bool required { get { return _required; } set { _required = value; } }
    public string defaultVal { get { return _defaultVal; } set { _defaultVal = value; } }
    public string source { get { return _source; } set { _source = value; } }


    /**
     * Constructor
     */
    public EmailTokenDef(
        int id, 
        EmailDef emailDef,
        string name,
        bool isPerRecipient,
        bool required,
        string defaultVal,
        string source) : base(id)
    {
        _emailDef = emailDef;
        _name = name;
        _isPerRecipient = isPerRecipient;
        _required = required;
        _defaultVal = defaultVal;
        _source = source;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return EmailTokenDefMgr.singleton;
    }



    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        base.log(objectLogger);
        objectLogger.log("_name", _name);
        objectLogger.log("_isPerRecipient", _isPerRecipient);
        objectLogger.log("_required", _required);
        objectLogger.log("_defaultVal", _defaultVal);
        objectLogger.log("_source", _source);
    }

}


}