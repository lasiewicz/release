using System.Collections;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailRecipient : DbObject
{
    private static Log _log = new Log("EmailRecipient");

    private EmailInstance _emailInstance;

    // the recipient is either a member or a host, so one of these
    // will be null.
    private Member _member;
    private Host _host;


    // _emailAddress can be used to override the _member's or _host's
    // email address, or when there is no _member or _host.
    private string _emailAddress;

    public EmailInstance emailInstance { get { return _emailInstance; } set { _emailInstance = value; } }
    public Member member { get { return _member; } set { _member = value; } }
    public Host host { get { return _host; } set { _host = value; } }
    public string firstName { get { return getName(true); } } 
    public string lastName { get { return getName(false); } } 


    /**
     * Constructor for creating a new Member recipient
     */
    public EmailRecipient(
        EmailInstance emailInstance,
        Member member) : this(DbConstants.NoId, emailInstance, member, null, null)
    {
    }


    /**
     * Constructor for creating a new Host recipient
     */
    public EmailRecipient(
        EmailInstance emailInstance,
        Host host) : this(DbConstants.NoId, emailInstance, null, host, null)
    {
    }


    /**
     * Constructor for loading from db
     */
    public EmailRecipient(
        int id, 
        EmailInstance emailInstance,
        Member member,
        Host host,
        string emailAddress) : base(id)
    {
        _emailInstance = emailInstance;
        _member = member;
        _host = host;
        _emailAddress = emailAddress;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return EmailRecipientMgr.singleton;
    }


    /**
     * Get the email address.  The address used depends on what members
     * are non-null.
     */
    public string emailAddress 
    { 
        get 
        { 
            if (_emailAddress != null)
                return _emailAddress;
            else if (_member != null)
                return _member.emailAdd;
            else if (_host != null)
                return _host.emailAddress;
            else
                return null;
        } 
    }


    /**
     * Writes the xml for this EmailRecipient
     */
    public void toXml(XmlHelper xh, ArrayList recipientTokens)
    {
        xh.startElem("Recipient", false, null);

        xh.writeSimpleElem("emailAddress", _emailAddress);
        xh.writeSimpleElem("firstName", firstName);
        xh.writeSimpleElem("lastName", lastName);
        if (_member != null)
            xh.writeSimpleElem("memberId", _member.id);
        if (_host != null)
            xh.writeSimpleElem("hostId", _host.id);

        // tokens
        if (recipientTokens != null)
        {            
            xh.startElem("Tokens", false, null);
            foreach (EmailToken token in recipientTokens)
                token.toXml(xh);
            xh.endElem(); // Tokens
        }

        xh.endElem(); // Recipient
    }


    /**
     * get the first name
     */
    private string getName(bool first)
    {
        if (_member != null)
            return first ? _member.firstName : _member.lastName;
        else if (_host != null)
            return first ? _host.firstName : _host.lastName;
        else
            return null;
    }


    /**
     * Returns the email address for this recipient.
     */
    public string getEmailAddress()
    {
        if (_emailAddress != null)
            return _emailAddress;
        else if (_member != null)
            return _member.emailAdd;
        else if (_host != null)
            return _host.emailAddress;
        return null;
    }


    /**
     * Returns true if it's ok to send the email to this recipient
     */
    public bool okToSend()
    {
        return _member == null || !_member.emailOptedOut;
    }


    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        base.log(objectLogger);
        objectLogger.log("_emailInstance", _emailInstance.id);
        objectLogger.log("_member", _member);
        objectLogger.log("_host", _host);
    }

}


}