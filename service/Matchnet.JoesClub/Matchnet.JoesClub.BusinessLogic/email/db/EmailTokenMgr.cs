using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the emailToken table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailTokenMgr : DbObjectMgr
{
    #region basic stuff
    private static Log _log = new Log("EmailTokenMgr");

    public static EmailTokenMgr singleton = new EmailTokenMgr();


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "emailTokenId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "EmailToken";
    }

    #endregion // basic stuff

    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        EmailToken emailToken = (EmailToken) dbObject;
        ArrayList SqlParams = setParams(emailToken, true);
        emailToken.id = DbUtils.insert("AddEmailToken", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        EmailToken emailToken = (EmailToken) dbObject;
        ArrayList SqlParams = setParams(emailToken, false);
        DbUtils.update("UpdateEmailToken", SqlParams);
    }


    /**
     * Sets command Parameters for everything in emailToken
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(EmailToken emailToken, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@emailTokenId", emailToken));
        sqlParameters.Add(DbUtils.createIdParam("@emailInstanceId", emailToken.emailInstance));
        sqlParameters.Add(DbUtils.createIdParam("@emailTokenDefId", emailToken.tokenDef));
        sqlParameters.Add(DbUtils.createIdParam("@emailRecipientId", emailToken.recipient));
        sqlParameters.Add(DbUtils.createStringParam("@val", emailToken.val));
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@emailTokenId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row.  Doesn't set the 
     * emailInstance.
     */
    public override DbObject load(SqlDataReader reader, object owner)
    {
        int emailTokenDefId = DbUtils.getIdFromReader(reader, "emailTokenDefId");
        EmailTokenDef tokenDef = EmailTokenDefMgr.singleton.getEmailTokenDef(emailTokenDefId);

        // recipient may be null
        int emailRecipientId = DbUtils.getIdFromReader(reader, "emailRecipientId");
        EmailRecipient recipient = null;
        if (emailRecipientId != DbConstants.NoId)
            recipient = (EmailRecipient) EmailRecipientMgr.singleton.load(emailRecipientId);

        EmailToken emailToken = new EmailToken(
            DbUtils.getIdFromReader(reader, "emailTokenId"),
            (EmailInstance) owner, 
            tokenDef,
            recipient,
            DbUtils.getStringFromReader(reader, "val"));

        return emailToken;
    }


    /**
     * load the EmailTokens for the given EmailInstance.
     */
    public ArrayList load(EmailInstance emailInstance)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIntParam("@emailInstanceId", emailInstance.id) };
        return load("GetEmailTokens", sqlParameters, emailInstance);
    }


    /**
     * Delete the EmailTokens for the given EmailInstance.
     */
    public void delete(EmailInstance emailInstance)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIntParam("@emailInstanceId", emailInstance.id) };
        DbUtils.runStoredProc("DeleteEmailTokens", sqlParameters);
    }

}

}