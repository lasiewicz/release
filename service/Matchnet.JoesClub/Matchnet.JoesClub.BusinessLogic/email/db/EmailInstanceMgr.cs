using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the emailInstance table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailInstanceMgr : DbObjectMgr
{
    private static Log _log = new Log("EmailInstanceMgr");

    public static EmailInstanceMgr singleton = new EmailInstanceMgr();


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "emailInstanceId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "EmailInstance";
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        EmailInstance emailInstance = (EmailInstance) dbObject;
        ArrayList SqlParams = setParams(emailInstance, true);
        emailInstance.id = DbUtils.insert("AddEmailInstance", SqlParams);

        saveRecipientsAndTokens(emailInstance);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        EmailInstance emailInstance = (EmailInstance) dbObject;
        ArrayList SqlParams = setParams(emailInstance, false);
        DbUtils.update("UpdateEmailInstance", SqlParams);

        saveRecipientsAndTokens(emailInstance);
    }


    /**
     * Sets command Parameters for everything in emailInstance
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(EmailInstance emailInstance, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@emailInstanceId", emailInstance));
        
        sqlParameters.Add(DbUtils.createIdParam("@emailDefId", emailInstance.emailDef));
        sqlParameters.Add(DbUtils.createIdParam("@eventId", emailInstance.ev));
        sqlParameters.Add(DbUtils.createDateTimeParam("@sentTime", emailInstance.sentTime));
        
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@emailInstanceId"));

        return sqlParameters;
    }


    /**
     * Save the EmailInstance's recipients and tokens
     */
    private void saveRecipientsAndTokens(EmailInstance emailInstance)
    {
        // delete the old ones
        EmailTokenMgr.singleton.delete(emailInstance);
        EmailRecipientMgr.singleton.delete(emailInstance);

        if (emailInstance.recipients != null)
            foreach (EmailRecipient recipient in emailInstance.recipients)
            {
                recipient.id = DbConstants.NoId; // if it was in the db, it's not anymore
                recipient.save();
            }

        if (emailInstance.tokens != null)
            foreach (EmailToken token in emailInstance.tokens)
            {
                token.id = DbConstants.NoId; // if it was in the db, it's not anymore
                token.save();
            }
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        int emailDefId = DbUtils.getIdFromReader(reader, "emailDefId");
        EmailDef emailDef = EmailDefMgr.singleton.getEmailDef(emailDefId);

        int eventId = DbUtils.getIdFromReader(reader, "eventId");
        Event ev = (Event) EventMgr.singleton.load(eventId);

        EmailInstance emailInstance = new EmailInstance(
            DbUtils.getIdFromReader(reader, "emailInstanceId"),
            emailDef,
            ev, 
            DbUtils.getDateTimeFromReader(reader, "sentTime"));
        return emailInstance;
    }


    /**
     * Called after DbObjectMgr loads the object.  Load the recipients
     * and tokens. 
     */
    public override void afterLoad(DbObject dbObject)
    {
        EmailInstance emailInstance = (EmailInstance) dbObject;

        // load the recipients and tokens
        emailInstance.recipients = EmailRecipientMgr.singleton.load(emailInstance);
        emailInstance.tokens = EmailTokenMgr.singleton.load(emailInstance);
    }


    /**
     * Load all the EmailInstances for the given event
     */
    public ArrayList load(Event ev)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIntParam("@eventId", ev.id) };
        return load("GetEmailInstances", sqlParameters);
    }


}

}