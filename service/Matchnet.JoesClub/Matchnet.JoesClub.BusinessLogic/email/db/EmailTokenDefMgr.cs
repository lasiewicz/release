using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the emailTokenDef table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailTokenDefMgr : DbObjectMgr
{
    #region basic stuff
    private static Log _log = new Log("EmailTokenDefMgr");

    public static EmailTokenDefMgr singleton = new EmailTokenDefMgr();

    // Hashtable[int emailTokenDefId --> EmailTokenDef]
    private Hashtable _c_emailTokenDefsById;


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "emailTokenDefId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "EmailTokenDef";
    }

    #endregion // basic stuff


    /**
     * load the object at the reader's current row.  Doesn't set the 
     * emailDef.
     */
    public override DbObject load(SqlDataReader reader, object owner)
    {
        EmailTokenDef emailTokenDef = new EmailTokenDef(
            DbUtils.getIdFromReader(reader, "emailTokenDefId"),
            (EmailDef) owner, 
            DbUtils.getStringFromReader(reader, "name"),
            DbUtils.getBoolFromReader(reader, "isPerRecipient"),
            DbUtils.getBoolFromReader(reader, "required"),
            DbUtils.getStringFromReader(reader, "defaultVal"),
            DbUtils.getStringFromReader(reader, "source"));
        return emailTokenDef;
    }


    /**
     * load the EmailTokenDefs for the given EmailDef id.
     */
    public ArrayList load(EmailDef emailDef)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIntParam("@emailDefId", emailDef.id) };
        return load("GetEmailTokenDefs", sqlParameters, emailDef);
    }


    #region cached stuff

    /**
     * Returns the EmailTokenDef with the given id
     */
    public EmailTokenDef getEmailTokenDef(int emailTokenDefId)
    {
        init("_c_emailTokenDefsById");
        return (EmailTokenDef) _c_emailTokenDefsById[emailTokenDefId];
    }


    /**
     * initialize cached values
     */
    private void init(string name)
    {
        if (name == "_c_emailTokenDefsById" && _c_emailTokenDefsById == null)
        {
            _c_emailTokenDefsById = new Hashtable();
            foreach (EmailDef emailDef in EmailDefMgr.singleton.emailDefs)
                foreach (EmailTokenDef emailTokenDef in emailDef.tokenDefs)
                    _c_emailTokenDefsById[emailTokenDef.id] = emailTokenDef;
        }
    }


    /**
     * reset cached values
     */
    public void reset()
    {
        _c_emailTokenDefsById = null;
    }

    #endregion // cached stuff

}

}