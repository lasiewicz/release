using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;

namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailDef : DbObject
{
    private static Log _log = new Log("EmailDef");

    private string _name;
    private int _templateId;
    private ArrayList _tokenDefs;  // ArrayList[EmailTokenDef]

    // Hashtable[string name --> EmailTokenDef]
    private Hashtable _c_tokenDefsByName;

    public string name { get { return _name; } set { _name = value; } }
    public int templateId { get { return _templateId; } set { _templateId = value; } }
    public ArrayList tokenDefs { get { return _tokenDefs; } set { _tokenDefs = value; } }


    /**
     * Constructor
     */
    public EmailDef(
        int id, 
        string name,
        int templateId,
        ArrayList tokenDefs) : base(id)
    {
        _name = name;
        _templateId = templateId;
        _tokenDefs = tokenDefs;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return EmailDefMgr.singleton;
    }


    #region cached stuff

    /**
     * Returns the EmailTokenDef with the given name
     */
    public EmailTokenDef getEmailTokenDef(string name)
    {
        init("_c_tokenDefsByName");
        return (EmailTokenDef) _c_tokenDefsByName[name];
    }

    /**
     * initialize cached values
     */
    private void init(string name)
    {
        if (name == "_c_tokenDefsByName" && _c_tokenDefsByName == null)
        {
            _c_tokenDefsByName = new Hashtable();
            foreach (EmailTokenDef tokenDef in _tokenDefs)
                _c_tokenDefsByName[tokenDef.name] = tokenDef;
        }
            
        else
            _log.error("init() bad name: " + name);
    }

    #endregion // cached stuff

    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        base.log(objectLogger);
        objectLogger.log("_name", _name);
        objectLogger.log("_templateId", _templateId);
        objectLogger.log("_tokenDefs", _tokenDefs);
    }

}


}