using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the emailRecipient table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MiscQueries 
{
    private static Log _log = new Log("MiscQueries");


    /**
     * Returns the member ids of everyone who has recieved the email 
     * with name emailDefName for the event
     * Returns ArrayList[int memberId]
     */
    public static ArrayList GetSentEmailMemberIds(int eventId, string emailDefName)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetSentEmailMembers");
            command.Parameters.Add(DbUtils.createIdParam("@eventId", eventId));
            command.Parameters.Add(DbUtils.createStringParam("@emailDefName", emailDefName));
            reader = command.ExecuteReader();

            // read the results
            ArrayList memberIds = new ArrayList();
            while (reader.Read())
                memberIds.Add(DbUtils.getIdFromReader(reader, "memberId"));

            return memberIds;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }


    /**
     * Calls a stored proc which returns hashtable.  Used by Nudge and TMinus code.
     * Returns Hashtable[eventId --> Hashtable[memberId]]
     */
    public static Hashtable getEventMemberHash(
        string storedProcName,
        DateTime eventStartTime,
        string emailDefName)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {

            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, storedProcName);
            command.Parameters.Add(DbUtils.createStringParam("@emailDefName", emailDefName));
            command.Parameters.Add(DbUtils.createDateTimeParam("@eventStartTime", eventStartTime));

            // read the results
            reader = command.ExecuteReader();
            ArrayList memberIds = new ArrayList();
            Hashtable results = new Hashtable();
            while (reader.Read())
            {
                int eventId = DbUtils.getIdFromReader(reader, "eventId");
                int memberId = DbUtils.getIdFromReader(reader, "memberId");

                Hashtable members = (Hashtable) results[eventId];
                if (members == null)
                {
                    members = new Hashtable();
                    results[eventId] = members;
                }

                members[memberId] = "";
            }
            
            return results;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }



    /**
     * Filters the events and members in removeEvents out of events.
     * Used by Nudge and TMinus code.
     * events, removeEvents and the return value are all:
     *     Hashtable[eventId --> Hashtable[memberId]]
     */
    public static Hashtable filterEventMemberHash(
        Hashtable events, Hashtable removeEvents)
    {
        Hashtable newEvents = new Hashtable();
        foreach (int eventId in events.Keys)
        {
            Hashtable members = (Hashtable) events[eventId];
            Hashtable removeMembers = (Hashtable) removeEvents[eventId];
            Hashtable newMembers = new Hashtable();

            // if the member is not in removeMembers, add it to newMembers.
            foreach (int memberId in members.Keys)
                if (removeMembers == null || !removeMembers.ContainsKey(memberId))
                    newMembers[memberId] = "";

            if (newMembers.Count > 0)
                newEvents[eventId] = newMembers;

        }

        return newEvents;

    }
}

}