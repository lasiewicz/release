using System.Collections;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailToken : DbObject
{
    private static Log _log = new Log("EmailToken");

    private EmailInstance _emailInstance;
    private EmailTokenDef _tokenDef;
    private EmailRecipient _recipient;
    private string _val;

    public EmailInstance emailInstance { get { return _emailInstance; } set { _emailInstance = value; } }
    public EmailTokenDef tokenDef { get { return _tokenDef; } set { _tokenDef = value; } }
    public EmailRecipient recipient { get { return _recipient; } set { _recipient = value; } }
    public string val { get { return _val; } set { _val = value; } }
	enum Month {Null, January, February, March, April, May, June, July, August, September, October, November, December}


    /**
     * Constructor
     */
    public EmailToken(
        int id, 
        EmailInstance emailInstance,
        EmailTokenDef tokenDef,
        EmailRecipient recipient,
        string val) : base(id)
    {
        _emailInstance = emailInstance;
        _tokenDef = tokenDef;
        _recipient = recipient;
        _val = val;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return EmailTokenMgr.singleton;
    }


    /**
     * Writes the xml for this EmailToken
     */
    public void toXml(XmlHelper xh)
    {
        xh.startElem("Token", false, null);
        xh.writeSimpleElem("name", _tokenDef.name);
		//Check to see if the token is the 'EventDate' token, and if so convert the value from
		//mm/dd/yy format to Month dd, yyyy
		
		if (_tokenDef.name == "TableTime")
		{
			_val = _val.ToLower();
		}

		if (_tokenDef.name == "EventDate")  
		{
			//Ensure valid date format
			string[] numbers = _val.Split('/');
			if (isValidDateFormat(numbers))
			{
				_val = ((Month)System.Convert.ToInt32(numbers[0])).ToString() + " " + numbers[1]+ ", " + numbers[2];
			}
		}
        xh.writeSimpleElem("value", _val);
        xh.endElem(); // Token
    }

	private bool isValidDateFormat(string[] values)
	{
		if (!(values.Length == 3)) return false;
		foreach (string v in values)
		{
			try 
			{
				System.Convert.ToInt32(v);
			}
			catch
			{
				return false;
			}
		}
		return true;
	}

    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        base.log(objectLogger);
        objectLogger.log("_emailInstance", _emailInstance.id);
        objectLogger.log("_tokenDef", _tokenDef.name);
        objectLogger.log("recipient", recipient.id);
        objectLogger.log("_val", _val);
    }

}


}