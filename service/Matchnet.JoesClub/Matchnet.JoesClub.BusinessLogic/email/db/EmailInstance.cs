using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;

namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailInstance : DbObject
{
    private static Log _log = new Log("EmailInstance");

    private EmailDef _emailDef;
    private Event _event;
    private DateTime _sentTime;

    private ArrayList _recipients;  // ArrayList[EmailRecipients]
    private ArrayList _tokens;  // ArrayList[EmailToken]



    public EmailDef emailDef { get { return _emailDef; } set { _emailDef = value; } }
    public Event ev { get { return _event; } set { _event = value; } }
    public DateTime sentTime { get { return _sentTime; } set { _sentTime = value; } }
    public ArrayList recipients { get { return _recipients; } set { _recipients = value; } }
    public ArrayList tokens { get { return _tokens; } set { _tokens = value; } }
    public bool sent { get { return sentTime != CommonConstants.NoValueDate; } }


    /**
     * Constructor for creating a new in-memory object
     */
    public EmailInstance(
        EmailDef emailDef,
        Event ev) : 
        this(DbConstants.NoId, emailDef, ev, CommonConstants.DateTimeNone)
    {
    }


    /**
     * Constructor for loading from db
     */
    public EmailInstance(
        int id, 
        EmailDef emailDef,
        Event ev,
        DateTime sentTime) : base(id)
    {
        _emailDef = emailDef;
        _event = ev;
        _sentTime = sentTime;
    }


    /**
     * return the DbObjectMgr for this class
     */
    public override DbObjectMgr getDbObjectMgr()
    {
        return EmailInstanceMgr.singleton;
    }


    /**
     * Removes any recipients whose member is not in members
     */
    public void removeMissingRecipients(ArrayList members)
    {
        // if there's the same number, then there must not be any missing
        if (members.Count == _recipients.Count)
            return;

        // remove recipients
        Hashtable hMembers = Misc.arrayListToHashtable(members);
        ArrayList newRecipients = new ArrayList();
        Hashtable keptRecipients = new Hashtable();
        foreach (EmailRecipient er in _recipients)
            if (hMembers.ContainsKey(er.member))
            {
                newRecipients.Add(er);
                keptRecipients[er] = "";
            }
        _recipients = newRecipients;

        // remove tokens for removed recipients
        ArrayList newTokens = new ArrayList();
        foreach (EmailToken et in _tokens)
            if (et.recipient == null || keptRecipients.ContainsKey(et.recipient))
                newTokens.Add(et);
        _tokens = newTokens;
    }

    /**
     * Sets the recipients from the given members
     * members = ArrayList[Member]
    public void setMemberRecipients(ArrayList members)
    {
        if (!recipientsChanged(members))
            return;
        EmailFactory.setMemberRecipients(this, members);
    }
     */



    /**
     * Figures out if the EmailInstance's recipients are the same as "members"
    private bool recipientsChanged(ArrayList members)
    {
        if (members.Count != _recipients.Count)
            return true;
        Hashtable hMembers = Misc.arrayListToHashtable(members);
        foreach (EmailRecipient er in _recipients)
            if (!hMembers.ContainsKey(er.member))
                return true;
        return false;
    }

     */

    /**
     * Gets the recipient specific tokens for the given recipient
     * returns ArrayList[EmailToken]
     */
    public ArrayList getRecipientTokens(EmailRecipient recipient)
    {
        ArrayList tokens = new ArrayList();
        foreach (EmailToken token in _tokens)
            if (token.tokenDef.isPerRecipient && token.recipient == recipient)
                tokens.Add(token);

        return tokens;
    }


    /**
     * Send the email
     */
    public void send()
    {
        Sender sender = EmailFactory.getSender(this);
        sender.send(this);
    }

    #region xml
    /**
     * Writes the xml for this EmailInstance
     */
    public void toXml(XmlHelper xh, bool details)
    {
        xh.startElem("EmailInstance", false, null);

        if (details)
            xmlDetails(xh);
        else
            xmlOverview(xh);

        xh.endElem(); // EmailInstance
    }


    /**
     * Writes the overview xml for this EmailInstance
     */
    private void xmlOverview(XmlHelper xh)
    {
        xh.writeSimpleElem("id", id);
        xh.writeSimpleElem("emailDefName", _emailDef.name);
		xh.writeSimpleElem("sentTime", getFormattedDateTime(_sentTime));

        bool multipleRecipients = false;
        if (_recipients != null && _recipients.Count > 0)
        {
            EmailRecipient recipient = (EmailRecipient) _recipients[0];
            recipient.toXml(xh, null);
            multipleRecipients = _recipients.Count > 1;
        }
        xh.writeSimpleElem("multipleRecipients", multipleRecipients);
    }


    /**
     * Writes the details xml for this EmailInstance
     */
    private void xmlDetails(XmlHelper xh)
    {
        xh.writeSimpleElem("id", id);
        xh.writeSimpleElem("emailDefName", _emailDef.name);
		xh.writeSimpleElem("sentTime", getFormattedDateTime(_sentTime));

        // sort tokens
        // commonTokens = ArrayList[EmailToken]
        // hRecipientTokens = Hashtable[EmailRecipient-->ArrayList[EmailToken]]
        ArrayList commonTokens = new ArrayList();
        Hashtable hRecipientTokens = new Hashtable();
        foreach (EmailToken token in _tokens)
        {
            if (token.tokenDef.isPerRecipient)
            {                
                ArrayList aRecipientTokens = (ArrayList) hRecipientTokens[token.recipient];
                if (aRecipientTokens == null)
                {
                    aRecipientTokens = new ArrayList();
                    hRecipientTokens[token.recipient] = aRecipientTokens;
                }
                aRecipientTokens.Add(token);
            }
            else
                commonTokens.Add(token);
        }

        // common tokens
        xh.startElem("CommonTokens", false, null);
        foreach (EmailToken token in commonTokens)
            token.toXml(xh);
        xh.endElem(); // CommonTokens

        // recipients
        xh.startElem("Recipients", false, null);
        foreach (EmailRecipient recipient in _recipients)
        {
            ArrayList aRecipientTokens = (ArrayList) hRecipientTokens[recipient];
            recipient.toXml(xh, aRecipientTokens);
        }
        xh.endElem(); // Recipients
    }

	private static string getFormattedDateTime(DateTime dt)
	{
		string formatted = dt.ToShortDateString() + " " + dt.ToShortTimeString();
		DateTime now = DateTime.Now;
		if ((dt.Year == now.Year) &&
			(dt.Month == now.Month)) 
		{
			if (dt.Day == now.Day) 
			{
				formatted = "Today " + dt.ToShortTimeString();
			}
			else if (dt.Day == now.Day-1)
			{
				formatted = "Yesterday " + dt.ToShortTimeString();
			}
		}
		return formatted;
	}


    /**
     * Writes the emails sent for a given event as xml
     */
    public static void toXml(XmlHelper xh, int eventId)
    {
        Event ev = (Event) EventMgr.singleton.load(eventId);
        ArrayList emailInstances = EmailInstanceMgr.singleton.load(ev);
        xh.startElem("EmailInstances", false, null);

        foreach (EmailInstance emailInstance in emailInstances)
            if (emailInstance.sent)
                emailInstance.toXml(xh, false);

        xh.endElem(); // EmailInstances
    }


    #endregion xml



    /**
     * Logable interface method
     */
    public override void log(ObjectLogger objectLogger)
    {
        base.log(objectLogger);
        objectLogger.log("_emailDef", _emailDef.name);
        objectLogger.log("_event", _event.id);
        objectLogger.log("_sentTime", _sentTime);
    }

}


}