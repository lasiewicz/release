using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the emailRecipient table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailRecipientMgr : DbObjectMgr
{
    #region basic stuff
    private static Log _log = new Log("EmailRecipientMgr");

    public static EmailRecipientMgr singleton = new EmailRecipientMgr();


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "emailRecipientId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "EmailRecipient";
    }

    #endregion // basic stuff


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void insert(SqlConnection conn, DbObject dbObject)
    {
        EmailRecipient emailRecipient = (EmailRecipient) dbObject;
        ArrayList SqlParams = setParams(emailRecipient, true);
        emailRecipient.id = DbUtils.insert("AddEmailRecipient", SqlParams);
    }


    /**
     * Create an object.  Caller closes the SqlConnection.
     */
    public override void update(SqlConnection conn, DbObject dbObject)
    {
        EmailRecipient emailRecipient = (EmailRecipient) dbObject;
        ArrayList SqlParams = setParams(emailRecipient, false);
        DbUtils.update("UpdateEmailRecipient", SqlParams);
    }


    /**
     * Sets command Parameters for everything in emailRecipient
     * returns ArrayList[SqlParameter]
     */
    private ArrayList setParams(EmailRecipient emailRecipient, bool insert)
    {        
        ArrayList sqlParameters = new ArrayList();        

        if (!insert)
            sqlParameters.Add(DbUtils.createIdParam("@emailRecipientId", emailRecipient));
        sqlParameters.Add(DbUtils.createIdParam("@emailInstanceId", emailRecipient.emailInstance));
        sqlParameters.Add(DbUtils.createIdParam("@memberId", emailRecipient.member));
        sqlParameters.Add(DbUtils.createIdParam("@hostId", emailRecipient.host));
        sqlParameters.Add(DbUtils.createStringParam("@emailAddress", emailRecipient.emailAddress));
        if (insert)
            sqlParameters.Add(DbUtils.createIdOutParam("@emailRecipientId"));

        return sqlParameters;
    }


    /**
     * load the object at the reader's current row.  Doesn't set the 
     * emailInstance.
     */
    public override DbObject load(SqlDataReader reader, object owner)
    {
        int memberId = DbUtils.getIdFromReader(reader, "memberId");
        Member member = null;
        if (memberId != DbConstants.NoId)
            member = (Member) MemberMgr.singleton.load(memberId);

        int hostId = DbUtils.getIdFromReader(reader, "hostId");
        Host host = null;
        if (hostId != DbConstants.NoId)
            host = (Host) HostMgr.singleton.load(hostId);

        EmailRecipient emailRecipient = new EmailRecipient(
            DbUtils.getIdFromReader(reader, "emailRecipientId"),
            (EmailInstance) owner, 
            member,
            host,
            DbUtils.getStringFromReader(reader, "emailAddress"));

        return emailRecipient;
    }


    /**
     * load the EmailRecipients for the given EmailInstance id.
     */
    public ArrayList load(EmailInstance emailInstance)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIntParam("@emailInstanceId", emailInstance.id) };
        return load("GetEmailRecipients", sqlParameters, emailInstance);
    }


    /**
     * Delete the EmailRecipients for the given EmailInstance.
     */
    public void delete(EmailInstance emailInstance)
    {
        SqlParameter[] sqlParameters = new SqlParameter[] {
            DbUtils.createIntParam("@emailInstanceId", emailInstance.id) };
        DbUtils.runStoredProc("DeleteEmailRecipients", sqlParameters);
    }


}

}