using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


/**
 * Manages the emailDef table
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailDefMgr : DbObjectMgr
{
    private static Log _log = new Log("EmailDefMgr");

    public static EmailDefMgr singleton = new EmailDefMgr();

    // since the EmailDefs aren't going to change very often, just keep in memory
    // ArrayList[EmailDef]
    private ArrayList _c_emailDefs;

    // Hashtable[string name --> EmailDef]
    private Hashtable _c_emailDefsByName;

    // Hashtable[int emailDefId --> EmailDef]
    private Hashtable _c_emailDefsById;


    /**
     * return the id column name for the table
     */
    public override string getIdColumn() { return "emailDefId"; }

 
    /**
     * Return the db table for the object
     */
    public override string getTableName()
    {
        return "EmailDef";
    }


    /**
     * load the object at the reader's current row
     */
    public override DbObject load(SqlDataReader reader)
    {
        EmailDef emailDef = new EmailDef(
            DbUtils.getIdFromReader(reader, "emailDefId"),
            DbUtils.getStringFromReader(reader, "name"),
            DbUtils.getIntFromReader(reader, "templateId"),
            null);
        return emailDef;
    }


    /**
     * Called after DbObjectMgr loads the object.  Load the EmailTokenDefs. 
     */
    public override void afterLoad(DbObject dbObject)
    {
        EmailDef emailDef = (EmailDef) dbObject;
        emailDef.tokenDefs = EmailTokenDefMgr.singleton.load(emailDef);
    }


    #region cached stuff

    /**
     * Returns the EmailDef with the given name
     */
    public EmailDef getEmailDef(string name)
    {
        init("_c_emailDefsByName");
        return (EmailDef) _c_emailDefsByName[name];
    }


    /**
     * Returns the EmailDef with the given id
     */
    public EmailDef getEmailDef(int emailDefId)
    {
        init("_c_emailDefsById");
        return (EmailDef) _c_emailDefsById[emailDefId];
    }


    /**
     * Returns all of the EmailDefs
     */
    public ArrayList emailDefs 
    { 
        get 
        { 
            init("_c_emailDefs");
            return _c_emailDefs; 
        }
    }


    /**
     * initialize cached values
     */
    private void init(string name)
    {
        if (name == "_c_emailDefs" && _c_emailDefs == null)
            _c_emailDefs = loadAll();

        else if (name == "_c_emailDefsByName" && _c_emailDefsByName == null)
        {
            init("_c_emailDefs");
            _c_emailDefsByName = new Hashtable();
            foreach (EmailDef emailDef in _c_emailDefs)
                _c_emailDefsByName[emailDef.name] = emailDef;
        }

        else if (name == "_c_emailDefsById" && _c_emailDefsById == null)
        {
            init("_c_emailDefs");
            _c_emailDefsById = new Hashtable();
            foreach (EmailDef emailDef in _c_emailDefs)
                _c_emailDefsById[emailDef.id] = emailDef;
        }
    }


    /**
     * reset cached values
     */
    public void reset()
    {
        _c_emailDefs = null;
        _c_emailDefsByName = null;
        _c_emailDefsById = null;
    }

    #endregion // cached stuff

}

}