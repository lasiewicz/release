using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;

/**
 * Used to create TMinus1 and TMinus5 EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class TMinus : MemberEmail
{
    private static Log _log = new Log("TMinus");

    private bool _tMinus1;
    private ICollection _memberIds;


    /**
     * Creates the Nudge EmailInstances for all the upcoming events.
     * Returns ArrayList[EmailInstance]
     */
    public static ArrayList createEmailInstances(bool send)
    {
        return createEmailInstances(send, DateTime.Now);
    }

    public static ArrayList createEmailInstances(bool send, DateTime now)
    {
        // figure out who should get T-1 emails
        DateTime timeTMinus1 = now.Add(EmailConfig.singleton.getTMinus1TimeSpan());
        Hashtable getTMinus1 = MiscQueries.getEventMemberHash(
            "GetTMinusEmailMembers", timeTMinus1, EmailConstants.Invitation);

        // figure out who should get T-5 emails
        DateTime timeTMinus5 = now.Add(EmailConfig.singleton.getTMinus5TimeSpan());
        Hashtable getTMinus5 = MiscQueries.getEventMemberHash(
            "GetTMinusEmailMembers", timeTMinus5, EmailConstants.Invitation);
        getTMinus5 = MiscQueries.filterEventMemberHash(getTMinus5, getTMinus1);

        // create an EmailInstance for each event that needs nudges
        ArrayList emailInstances = new ArrayList();
        createEmailInstances(true, getTMinus1, emailInstances);
        createEmailInstances(false, getTMinus5, emailInstances);
    
        // send?
        if (send)
            foreach (EmailInstance emailInstance in emailInstances)
                emailInstance.send();

        return emailInstances;
    }


    /**
     * Creates the Nudge EmailInstances for all the upcoming events.
     * Returns ArrayList[EmailInstance]
     */
    private static void createEmailInstances(
        bool tMinus1,
        Hashtable tMinusHash,
        ArrayList emailInstances)
    {
        // create an EmailInstance for each event that needs tminuses
        foreach (int eventId in tMinusHash.Keys)
        {
            Event ev = (Event) EventMgr.singleton.load(eventId); 
            ICollection memberIds = ((Hashtable) tMinusHash[eventId]).Keys;
            TMinus tMinus = new TMinus(ev, tMinus1, memberIds);
            EmailInstance emailInstance = tMinus.createEmailInstance();
            if (emailInstance != null)
                emailInstances.Add(emailInstance);
        }
    }


    /**
     * private Constructor
     */
    public TMinus(Event ev, bool tMinus1, ICollection memberIds) : 
        base(tMinus1 ? EmailConstants.TMinus1 : EmailConstants.TMinus5, 
             ev)
    {
        _tMinus1 = tMinus1;
        _memberIds = memberIds;
        nullWhenNoRecipients = true;
    }


    /**
     * Subclasses override this to set the recipients
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        // get the member for each id
        ArrayList members = new ArrayList();
        foreach (int memberId in _memberIds)
            members.Add(_event.getMember(memberId));

        return members;
    }
}
}
