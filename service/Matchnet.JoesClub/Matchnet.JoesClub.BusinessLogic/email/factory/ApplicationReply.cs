using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "ApplicationReply" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class ApplicationReply : MemberEmail
{
    private static Log _log = new Log("ApplicationReply");

    private Member _member;


    /**
     * Create a new EmailInstance
     */
    public static EmailInstance createEmailInstance(Member member)
    {
        return new ApplicationReply(member).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public ApplicationReply(Member member) : 
        base(EmailConstants.ApplicationReply, null)
    {
        _member = member;
    }


    /**
     * Subclasses determine which Members get the email
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        ArrayList members = new ArrayList();
        members.Add(_member);
        return members;
    }
}

}
