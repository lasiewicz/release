using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "EventCancelled" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventCancelled : MemberEmail
{
    private static Log _log = new Log("EventCancelled");


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(Event ev)
    {
        return new EventCancelled(ev).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public EventCancelled(Event ev) : 
        base(EmailConstants.EventCancelled, ev)
    {
    }


    /**
     * Subclasses override this to set the recipients.  Send to everyone who got
     * an invitation.
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        // find out which members already got an Invitation
        ArrayList invitedMemberIds = MiscQueries.GetSentEmailMemberIds(
            _event.id, EmailConstants.Invitation);
        ArrayList members = new ArrayList();
        foreach (int memberId in invitedMemberIds)
            members.Add(_event.getMember(memberId));

        return members;
    }


}

}
