using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "EventFull" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventFull : MemberEmail
{
    private static Log _log = new Log("EventFull");


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(
        Event ev)
    {
        return new EventFull(ev).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public EventFull(        
        Event ev) : 
        base(EmailConstants.EventFull, ev)
    {
    }



    /**
     * Subclasses implement this to get the members
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        // return the members who signed up and were not invited.
        ArrayList members = new ArrayList();
        foreach (EventMember eventMember in _event.eventMembers)
            if (eventMember.inviteStatus == InviteStatus.NotInvited &&
                eventMember.memberStatus == MemberStatus.SignedUp)
                members.Add(eventMember.member);

        return members;
    }
}

}
