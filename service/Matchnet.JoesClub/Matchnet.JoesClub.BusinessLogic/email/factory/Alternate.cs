using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "Alternate" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class Alternate : MemberEmail
{
    private static Log _log = new Log("Alternate");

    private ArrayList _members;


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(
        Event ev,
        ArrayList members)
    {
        return new Alternate(ev, members).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public Alternate(        
        Event ev,
        ArrayList members) : 
        base(EmailConstants.Alternate, ev)
    {
        _members = members;
    }



    /**
     * Subclasses implement this to get the members
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        return _members;
    }

}

}
