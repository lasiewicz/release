using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "ReceivedInviteReq" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class ReceivedInviteReq : MemberEmail
{
    private static Log _log = new Log("ReceivedInviteReq");

    private Member _member;


    /**
     * Create a new EmailInstance
     */
    public static EmailInstance createEmailInstance(
        Event ev,
        Member member)
    {
        return new ReceivedInviteReq(ev, member).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public ReceivedInviteReq(        
        Event ev,
        Member member) : 
        base(EmailConstants.ReceivedInviteReq, ev)
    {
        _member = member;
    }


    /**
     * Subclasses override this to set the recipients
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        ArrayList members = new ArrayList();
        members.Add(_member);
        return members;
    }

}

}
