using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "Invitation" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class Invitation : MemberEmail
{
    private static Log _log = new Log("Invitation");


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(Event ev)
    {
        return new Invitation(ev).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public Invitation(Event ev) : 
        base(EmailConstants.Invitation, ev)
    {
    }


    /**
     * Subclasses override this to set the recipients
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        // return the invited members
        ArrayList members = new ArrayList();
        foreach (EventMember eventMember in _event.eventMembers)
            if (eventMember.inviteStatus == InviteStatus.Invited)
                members.Add(eventMember.member);

        return members;
    }
}

}
