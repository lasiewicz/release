using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;

/**
 * Used to create Nudge2 EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class Nudge : MemberEmail
{
    private static Log _log = new Log("Nudge");

    private ICollection _memberIds;


    /**
     * Creates the Nudge EmailInstances for all the upcoming events.
     * Returns ArrayList[EmailInstance]
     */
    public static ArrayList createEmailInstances(bool send)
    {
        return createEmailInstances(send, DateTime.Now);
    }

    public static ArrayList createEmailInstances(bool send, DateTime now)
    {
        // figure out who should get nudge emails
        // the invitation sent time has to be at least as old as timeNudge
        DateTime timeNudge = now.Subtract(EmailConfig.singleton.getNudge2TimeSpan());
        Hashtable getNudge = OLD__getEventMemberHash(
            "GetNudgeEmailMembers", timeNudge);

        // create an EmailInstance for each event that needs nudges
        ArrayList emailInstances = new ArrayList();
        createEmailInstances(getNudge, emailInstances);
    
        // send?
        if (send)
            foreach (EmailInstance emailInstance in emailInstances)
                emailInstance.send();

        return emailInstances;
    }


    /**
     * Creates the Nudge EmailInstances for all the upcoming events.
     * eventsHash = Map[eventId --> Map[memberId]]
     * Returns ArrayList[EmailInstance]
     */
    private static void createEmailInstances(
        Hashtable eventsHash,
        ArrayList emailInstances)
    {
        // create an EmailInstance for each event that needs tminuses
        foreach (int eventId in eventsHash.Keys)
        {
            Event ev = (Event) EventMgr.singleton.load(eventId); 
            ICollection memberIds = ((Hashtable) eventsHash[eventId]).Keys;
            Nudge nudge = new Nudge(ev, memberIds);
            EmailInstance emailInstance = nudge.createEmailInstance();
            if (emailInstance != null)
                emailInstances.Add(emailInstance);
        }
    }


    /**
     * private Constructor
     */
    public Nudge(Event ev, ICollection memberIds) : 
        base(EmailConstants.Nudge, ev)
    {
        _memberIds = memberIds;
        nullWhenNoRecipients = true;
    }


    /**
     * Subclasses override this to set the recipients
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        // get the member for each id
        ArrayList members = new ArrayList();
        foreach (int memberId in _memberIds)
            members.Add(_event.getMember(memberId));

        return members;
    }


    /**
     * Calls a stored proc which returns hashtable.  Used by Nudge and TMinus code.
     * Returns Hashtable[eventId --> Hashtable[memberId]]
     */
    private static Hashtable OLD__getEventMemberHash(
        string storedProcName,
        DateTime invitationSentTime)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {

            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, storedProcName);
            command.Parameters.Add(DbUtils.createDateTimeParam("@invitationSentTime", invitationSentTime));

            // read the results
            reader = command.ExecuteReader();
            ArrayList memberIds = new ArrayList();
            Hashtable results = new Hashtable();
            while (reader.Read())
            {
                int eventId = DbUtils.getIdFromReader(reader, "eventId");
                int memberId = DbUtils.getIdFromReader(reader, "memberId");

                Hashtable members = (Hashtable) results[eventId];
                if (members == null)
                {
                    members = new Hashtable();
                    results[eventId] = members;
                }

                members[memberId] = "";
            }
            
            return results;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }



    /**
     * Calls a stored proc which returns hashtable.  Used by Nudge and TMinus code.
     * Returns Hashtable[eventId --> Hashtable[memberId]]
     * 
     * JLF this will replace OLD__getEventMemberHash() above
     */
    private static Hashtable NEW__getEventMemberHash(DateTime now)
    {
        DateTime nudge1Time = now.Subtract(EmailConfig.singleton.getNudge1TimeSpan());
        DateTime nudge2Time = now.Subtract(EmailConfig.singleton.getNudge2TimeSpan());

        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetNudgeMembers");
            command.Parameters.Add(DbUtils.createDateTimeParam("@nudge1Time", nudge1Time));
            command.Parameters.Add(DbUtils.createDateTimeParam("@nudge2Time", nudge2Time));
            command.Parameters.Add(DbUtils.createBoolParam("@nudge1", false));

            // read the results
            reader = command.ExecuteReader();
            ArrayList memberIds = new ArrayList();
            Hashtable results = new Hashtable();
            while (reader.Read())
            {
                int eventId = DbUtils.getIdFromReader(reader, "eventId");
                int memberId = DbUtils.getIdFromReader(reader, "memberId");

                Hashtable members = (Hashtable) results[eventId];
                if (members == null)
                {
                    members = new Hashtable();
                    results[eventId] = members;
                }

                members[memberId] = "";
            }
            
            return results;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }
}

}
