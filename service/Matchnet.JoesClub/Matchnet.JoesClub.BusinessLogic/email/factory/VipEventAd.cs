using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "VipEventAd" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class VipEventAd : MemberEmail
{
    private static Log _log = new Log("VipEventAd");

    private ArrayList _members;
    private string _commonality;


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(
        Event ev,
        ArrayList members,
        string commonality)
    {
        return new VipEventAd(ev, members, commonality).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public VipEventAd(        
        Event ev,
        ArrayList members,
        string commonality) : 
        base(EmailConstants.VipEventAd, ev)
    {
        _members = members;
        _commonality = commonality;
    }



    /**
     * Subclasses override this to set the recipients
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        return _members;
    }


    /**
     * Subclasses implement this to get the members
     */
    public override string getTokenVal(EmailRecipient recipient, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;
        if (tokenName == EmailConstants.VipEventAdCommonality)
            return _commonality;
        
        else
            return base.getTokenVal(recipient, tokenDef);
    }

}

}
