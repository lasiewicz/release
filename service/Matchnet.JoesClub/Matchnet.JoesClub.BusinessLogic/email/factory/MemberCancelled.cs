using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "MemberCancelled" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberCancelled : EmailFactory
{
    private static Log _log = new Log("MemberCancelled");

    private Event _event;
    private Member _member;
    private MemberStatus _memberStatus;


    /**
     * Create a new EmailInstance
     */
    public static EmailInstance createEmailInstance(
        Event ev,
        Member member,
        MemberStatus memberStatus)
    {
        return new MemberCancelled(ev, member, memberStatus).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public MemberCancelled(        
        Event ev,
        Member member,
        MemberStatus memberStatus) : 
        base(EmailConstants.MemberCancelled)
    {
        _event = ev;
        _member = member;
        _memberStatus = memberStatus;

        // check memberStatus
        if (_memberStatus != MemberStatus.RsvpedNo &&
            _memberStatus != MemberStatus.Cancelled)
            _log.error("MemberCancelled() unexpected memberStatus: " + memberStatus);
    }


    /**
     * Override this to set the recipients.  Just send to JoesClubReports@spark.net
     */
    protected override ArrayList getRecipients(EmailInstance emailInstance)
    {
        ArrayList recipients = new ArrayList();
        EmailRecipient er = new EmailRecipient(
            DbConstants.NoId, emailInstance, null, null, "joesclubalerts@spark.net");
        recipients.Add(er);
        return recipients;
    }


    /**
     * Subclasses implement this to get the tokens
     */
    public override string getTokenVal(EmailRecipient recipient, EmailTokenDef tokenDef)
    {
        // event token
        if (tokenDef.source == EmailConstants.Source_Event)
            return base.getTokenVal(recipient, _event, tokenDef);

        // member token
        else if (tokenDef.source == EmailConstants.Source_Member)
            return base.getTokenVal(recipient, _member, tokenDef);

        else if (tokenDef.name == EmailConstants.MemberCancelledAction)
        {
            string action = "This member has ";
            if (_memberStatus == MemberStatus.RsvpedNo)
                return action + "declined their invitation";
            else
                return action + "cancelled";
        }

        else
            return null;
    }
}

}
