using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * EmailFactory is used to create new EmailInstance objects.  There is a
 * subclass of EmailFactory for each of the different email types in the 
 * system.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class MemberEmail : EmailFactory
{
    private static Log _log = new Log("MemberEmail");

    protected Event _event;
    private ICollection _members;

    // if true, members who have already received the given email type 
    // for the event will automatically be removed from the set of
    // recipients so the subclass doesn't have to do it.
    private bool _dontSendTwice;
    public bool dontSendTwice { get { return _dontSendTwice; } 
                                set { _dontSendTwice = value; } }

    // Hashtable[Member --> Table]
    private Hashtable _c_membersInLockedTables;


    /**
     * Constructor
     */
    protected MemberEmail(string emailDefName, Event ev)
        : base(emailDefName)
    {
        _event = ev;
        _dontSendTwice = true;
    }


    /**
     * Subclasses implement this to get the members
     */
    protected abstract ICollection getMembers(EmailInstance emailInstance);


    /**
     * Override this to set the recipients
     */
    protected override ArrayList getRecipients(EmailInstance emailInstance)
    {
        ArrayList recipients = new ArrayList();
        _members = getMembers(emailInstance);

        if (_dontSendTwice)
            removeAlreadySent();

        foreach (Member member in _members)
        {
            EmailRecipient er = new EmailRecipient(emailInstance, member);
            if (er.okToSend())
                recipients.Add(er);
        }
        return recipients;
    }


    /**
     * Updates _members, removing the members who have already received the 
     * email for this event.
     */
    protected void removeAlreadySent()
    {
        // handle null _event
        int eventId = DbUtils.getId(_event);

        // ArrayList[int memberId]
        ArrayList sentMemberIdsArray = 
            MiscQueries.GetSentEmailMemberIds(eventId, _emailDef.name);
        Hashtable sentMemberIds = Misc.arrayListToHashtable(sentMemberIdsArray);

        ArrayList newMembers = new ArrayList();
        foreach (Member member in _members)
            if (sentMemberIds[member.id] == null)
                newMembers.Add(member);
        _members = newMembers;
    }


    /**
     * Return the event for the EmailInstance
     */
    public override Event getEvent() { return _event; } 


    /**
     * Get non-member specific tokens
     */
    public override string getTokenVal(EmailRecipient recipient, EmailTokenDef tokenDef)
    {
        string tokenVal = base.getTokenVal(recipient, tokenDef);
        if (tokenVal != null)
            return tokenVal;

        return getTokenVal(recipient, _event, tokenDef);
    }


    /**
     * Get member specific tokens
     */
    public override string getTokenVal(EmailRecipient recipient, Member member, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;
        if (tokenName == EmailConstants.InvitationLink)
        {
            EventMember em = _event.getEventMember(member.id);
            return Config.getProperty("UxUrl") + "/Invitation.aspx?id=" + em.inviteGuid;
        }

        else if (tokenName == EmailConstants.AcceptLink ||
                 tokenName == EmailConstants.DeclineLink)
        {
            EventMember em = _event.getEventMember(member.id);
            string action = tokenName == EmailConstants.AcceptLink ? "RsvpedYes" : "RsvpedNo";
            return Config.getProperty("UxUrl") + "/" + UiConstants.RsvpAjaxPage + 
                "?action=" + action + "&guid=" + em.inviteGuid;
        }

        else if (tokenName == EmailConstants.OptOutLink)
        {
            // make sure the member has a guid
            if (member.guid == null)
            {
                member.guid = Guid.NewGuid().ToString();
                member.save();
            }

            return Config.getProperty("UxUrl") + "/OptOut.aspx?id=" + member.guid;
        }

        string tokenVal = base.getTokenVal(recipient, member, tokenDef);
        if (tokenVal != null)
            return tokenVal;

        // get token from the member's table
        Table table = getMemberTable(member);
        return getTokenVal(recipient, table, tokenDef);
    }


    #region cached stuff
    
    /**
     * Get the (locked) table that the member is in.
     */
    protected Table getMemberTable(Member member)
    {
        init("_c_membersInLockedTables");
        return (Table) _c_membersInLockedTables[member];
    }

    
    /**
     * initialize cached values
     */
    private void init(string name)
    {
        if (name == "_c_membersInLockedTables" && _c_membersInLockedTables == null)
        {
            if (_event != null)
                _c_membersInLockedTables = _event.getMembersInLockedTables();  
            else
                _c_membersInLockedTables = new Hashtable();
        }
    }

    #endregion
}


}