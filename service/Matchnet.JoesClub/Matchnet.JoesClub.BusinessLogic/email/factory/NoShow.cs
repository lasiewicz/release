using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "NoShow" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class NoShow : MemberEmail
{
    private static Log _log = new Log("NoShow");


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(Event ev)
    {
        return new NoShow(ev).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public NoShow(Event ev) : 
        base(EmailConstants.NoShow, ev)
    {
    }


    /**
     * Subclasses override this to set the recipients.
     * Returns all the no-shows for the event
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        ArrayList members = new ArrayList();
        foreach (EventMember eventMember in _event.eventMembers)
            if (eventMember.memberStatus == MemberStatus.NoShow)
                members.Add(eventMember.member);

        return members;
    }


}

}
