using System;
using System.Collections;
using System.Text;
using Matchnet.JoesClub.ValueObjects;


/**
 * EmailFactory is used to create new EmailInstance objects.  There is a
 * subclass of EmailFactory for each of the different email types in the 
 * system.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class EmailFactory
{
    private static Log _log = new Log("EmailFactory");

    protected EmailDef _emailDef;

    // if true, createEmailInstance() will return null if there are
    // no recipients
    protected bool _nullWhenNoRecipients;
    public bool nullWhenNoRecipients { get { return _nullWhenNoRecipients; } 
                                set { _nullWhenNoRecipients = value; } }


    /**
     * Constructor
     */
    protected EmailFactory(string emailDefName)
    {
        _emailDef = EmailDefMgr.singleton.getEmailDef(emailDefName);
        _nullWhenNoRecipients = false;
    }


    /**
     * Create a new EmailInstance
     */
    protected EmailInstance createEmailInstance()
    {
        EmailInstance emailInstance = new EmailInstance(_emailDef, getEvent());

        // get the recipients
        emailInstance.recipients = getRecipients(emailInstance);
        if (_nullWhenNoRecipients && 
            (emailInstance.recipients == null ||
             emailInstance.recipients.Count == 0))
            return null;

        // set the token values
        ArrayList tokens = new ArrayList();
        ArrayList missingTokenDefs = new ArrayList();
        foreach (EmailTokenDef tokenDef in _emailDef.tokenDefs)
        {
            // if it's a per-recipient token, create a token for each recipient
            if (tokenDef.isPerRecipient)
            {
                bool inMissingTokenDefs = false;
                foreach (EmailRecipient recipient in emailInstance.recipients)
                {
                    inMissingTokenDefs = addToken(
                        tokenDef,
                        recipient,
                        emailInstance,
                        tokens, 
                        missingTokenDefs,
                        inMissingTokenDefs);
                }
            }

            else
            {
                addToken(
                    tokenDef,
                    null,
                    emailInstance,
                    tokens, 
                    missingTokenDefs,
                    false);
            }
        }

        // report missing tokens
        if (missingTokenDefs.Count > 0)
        {
            StringBuilder sbError = new StringBuilder();
            sbError.Append("createEmailInstance() missing tokens: ");
            sbError.Append(((EmailTokenDef) missingTokenDefs[0]).name);
            for (int i = 1; i < missingTokenDefs.Count; i++)
            {
                sbError.Append(", ");
                sbError.Append(((EmailTokenDef) missingTokenDefs[i]).name);
            }

            //_log.error(sbError.ToString());
        }

        emailInstance.tokens = tokens;

        return emailInstance;
    }


    /**
     * Shared code used above
     */
    private bool addToken(
        EmailTokenDef tokenDef,
        EmailRecipient recipient,
        EmailInstance emailInstance,
        ArrayList tokens, 
        ArrayList missingTokenDefs,
        bool inMissingTokenDefs)
    {
        // get the value
        string val = null;
        if (recipient != null && recipient.member != null)
            val = getTokenVal(recipient, recipient.member, tokenDef);
        else if (recipient != null && recipient.host != null)
            val = getTokenVal(recipient, recipient.host, tokenDef);
        else 
            val = getTokenVal(recipient, tokenDef);

        // handle missing value
        if (val == null)
            val = tokenDef.defaultVal;
        if (val == null && tokenDef.required && !inMissingTokenDefs)
        {
            missingTokenDefs.Add(tokenDef);
            inMissingTokenDefs = true;
        }

        // handle missing value
        if (val == null)
            val = "";

        // create the token (even if the value is missing)
        EmailToken token = new EmailToken(
            DbConstants.NoId,
            emailInstance,
            tokenDef,
            recipient,
            val);

        tokens.Add(token);

        return inMissingTokenDefs;
    }


    /**
     * Subclasses override this to set the recipients
     */
    protected abstract ArrayList getRecipients(EmailInstance emailInstance);


    /**
     * Return the event for the EmailInstance
     */
    public virtual Event getEvent() { return null; } 


    /**
     * Get the appropriate Sender for the given EmailInstance
     */
    public static Sender getSender(EmailInstance emailInstance)
    {
        // Nudge and TMinus don't need to use the AsyncSender because
        // they're already asynchronous
        string emailDefName = emailInstance.emailDef.name;
        if (emailDefName == EmailConstants.Nudge ||
            emailDefName == EmailConstants.TMinus1 ||
            emailDefName == EmailConstants.TMinus5)
            return EmailConfig.singleton.getSender();
        else
            return AsyncSender.singleton;
    }


    /**
     * Sets the recipients from the given members
     * members = ArrayList[Member]
    public static void setMemberRecipients(EmailInstance emailInstance, ArrayList members)
    {
        getEmailFactory(emailInstance)._setMemberRecipients(emailInstance, members);
    }
     */


    /**
     * Sets the recipients from the given members
     * members = ArrayList[Member]
    private void _setMemberRecipients(EmailInstance emailInstance, ArrayList members)
    {
        // create the new recipients
        emailInstance.recipients = new ArrayList();
        foreach (Member member in members)
        {
            EmailRecipient er = new EmailRecipient(emailInstance, member);
            if (er.okToSend())
                emailInstance.recipients.Add(er);
        }

        // create the new tokens. 
        ArrayList tokens = new ArrayList();

        // keep the common tokens
        foreach (EmailToken token in emailInstance.tokens)
            if (!token.tokenDef.isPerRecipient)
                tokens.Add(token);

        // make PerRecipient tokens
        ArrayList missingTokenDefs = new ArrayList();
        foreach (EmailTokenDef tokenDef in _emailDef.tokenDefs)
        {
            // if it's a per-recipient token, create a token for each recipient
            if (tokenDef.isPerRecipient)
            {
                bool inMissingTokenDefs = false;
                foreach (EmailRecipient recipient in emailInstance.recipients)
                {
                    inMissingTokenDefs = addToken(
                        tokenDef,
                        recipient,
                        emailInstance,
                        tokens, 
                        missingTokenDefs,
                        inMissingTokenDefs);
                }
            }
        }

        emailInstance.tokens = tokens;
    }
     */



    /**
     * Creates the appropriate factory for the given EmailInstance
    private static EmailFactory getEmailFactory(EmailInstance emailInstance)
    {
        Event ev = emailInstance.ev;

        switch (emailInstance.emailDef.name)
        {
            case EmailConstants.ReceivedInviteReq:
                return new ReceivedInviteReq(ev, null);
            case EmailConstants.VipEventAd:
                return new VipEventAd(ev, null, null);
            case EmailConstants.Alternate:
                return new Alternate(ev, null);
            case EmailConstants.EventFull:
                return new EventFull(ev);
            case EmailConstants.Invitation:
                return new Invitation(ev);
            case EmailConstants.Nudge:
                return new Nudge(ev, null);
            case EmailConstants.RsvpConfirmation:
                return new RsvpConfirmation(ev, null);
            case EmailConstants.TMinus5:
                return new TMinus(ev, false, null);
            case EmailConstants.TMinus1:
                return new TMinus(ev, true, null);
            case EmailConstants.EventCancelled:
                return new EventCancelled(ev);
            case EmailConstants.AfterDinner:
                return new AfterDinner(ev);
            case EmailConstants.NoShow:
                return new NoShow(ev);
            case EmailConstants.DailyReport:
                return new DailyReport();
        }

        _log.error("getEmailFactory() failed: " + emailInstance.emailDef.name);
        return null;
    }
     */

    #region get Token methods

    /**
     * Subclasses override these to set token values.
     * return null for missing values
     */
    public virtual string getTokenVal(EmailRecipient recipient, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;

        if (tokenName == EmailConstants.LoginLink)
            return Config.getProperty("UxUrl") + "/Index.htm";
        else if (tokenName == EmailConstants.UxUrl)
            return Config.getProperty("UxUrl");

        return null;
    }

    /**
     * Get Event specific tokens
     */
    public virtual string getTokenVal(EmailRecipient recipient, Event ev, EmailTokenDef tokenDef)
    {
        if (ev == null)
            return null;

        string tokenName = tokenDef.name;
        if (tokenName == EmailConstants.VenueName && ev.venue != null)
            return ev.venue.name;
        
        else if (tokenName == EmailConstants.EventDate)
           return ev.date.ToString("MMMM dd, yyyy");
        
        else if (tokenName == EmailConstants.EventAttire)
            return ev.attire;
        
        else if (tokenName == EmailConstants.EventSpecialInstructions)
            return ev.specialInstructions;
        
        else if (tokenName == EmailConstants.EventPrice)
            return ev.price.ToString("C");
        
        else if (tokenName == EmailConstants.EventTypeName)
            return ev.excursion ? "Excursion" : "Dinner";

        else
            return null;
    }


    /**
     * Get member specific tokens
     */
    public virtual string getTokenVal(EmailRecipient recipient, Member member, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;
        if (tokenName == EmailConstants.MemberEmailAddress)
            return member.emailAdd;
        else if (tokenName == EmailConstants.MemberFirstName)
            return member.firstName;
        else if (tokenName == EmailConstants.MemberLastName)
            return member.lastName;
        else
            return null;
    }


    /**
     * Get table specific tokens
     */
    public virtual string getTokenVal(EmailRecipient recipient, Table table, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;
        if (table == null)
            return null;
        
        if (tokenName == EmailConstants.TableTime)
            return (table.time.ToString("t")).ToLower();

        // get from table's venue
        if (tokenDef.source == EmailConstants.Source_Venue)
            return getTokenVal(recipient, table.venue, tokenDef);

        // get from table's host
        if (tokenDef.source == EmailConstants.Source_Host)
            return getTokenVal(recipient, table.host, tokenDef);

        return null;
    }


    /**
     * Get venue tokens
     */
    public virtual string getTokenVal(EmailRecipient recipient, Venue venue, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;
        if (venue == null)
            return null;

        if (tokenName == EmailConstants.VenueName)
            return venue.name;
        
        // We'll need the full address...
        else if (tokenName == EmailConstants.VenueAddress)
            return venue.address;
        else if (tokenName == EmailConstants.VenueAddress2)
            return venue.address2;
        else if (tokenName == EmailConstants.VenueCity)
            return venue.city;
        else if (tokenName == EmailConstants.VenueState)
            return venue.state;
        else if (tokenName == EmailConstants.VenueZipCode)
            return venue.zipCode;
        else if (tokenName == EmailConstants.VenueWebSiteUrl)
            return venue.webSiteUrl;
        else if (tokenName == EmailConstants.VenueMapUrl)
            return venue.mapUrl;
        else if (tokenName == EmailConstants.VenuePhone)
            return venue.phone;

        else if (tokenName == EmailConstants.VenueParking)
        {
            if (venue.parking == CommonConstants.EmptyMask)
                return null;

            return Enums.venueParking.getCommaSepString(venue.parking);
        }

        else if (tokenName == EmailConstants.VenueMeetingLocation)
            return venue.meetingLocation;

        return null;
    }


    /**
     * Get host tokens
     */
    public virtual string getTokenVal(EmailRecipient recipient, Host host, EmailTokenDef tokenDef)
    {
        string tokenName = tokenDef.name;
        if (host == null)
            return null;

        if (tokenName == EmailConstants.HostFirstName)
            return host.firstName;

        else if (tokenName == EmailConstants.HostImage)
            return host.image;

        else if (tokenName == EmailConstants.HostPhone)
            return host.phone;

        else if (tokenName == EmailConstants.HostEmailAddress)
            return host.emailAddress;

        return null;
    }
    #endregion get Token methods


}


}