using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "Welcome" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class WelcomeEmail : MemberEmail
{
    private static Log _log = new Log("WelcomeEmail");

    private Member _member;


    /**
     * Create a new EmailInstance
     */
    public static EmailInstance createEmailInstance(Member member)
    {
        return new WelcomeEmail(member).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public WelcomeEmail(Member member) : 
        base(EmailConstants.Welcome, null)
    {
        _member = member;
    }


    /**
     * Subclasses determine which Members get the email
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        ArrayList members = new ArrayList();
        members.Add(_member);
        return members;
    }


    /**
     * Get non-member specific tokens
     */
    public override string getTokenVal(EmailRecipient recipient, Member member, EmailTokenDef tokenDef)
    {
        if (tokenDef.name == EmailConstants.WelcomeLink)
        {
            // make sure the member has a guid
            if (member.guid == null)
            {
                member.guid = Guid.NewGuid().ToString();
                member.save();
            }

            return Config.getProperty("UxUrl") + "/Welcome.aspx?id=" + member.guid;
        }

        return base.getTokenVal(recipient, member, tokenDef);
    }
}

}
