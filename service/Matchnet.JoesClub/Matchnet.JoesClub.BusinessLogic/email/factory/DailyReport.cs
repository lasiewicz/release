using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;

/**
 * Used to create DailyReport1 and DailyReport2 EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class DailyReport : EmailFactory
{
    private static Log _log = new Log("DailyReport");


    /**
     * Creates the DailyReport EmailInstances for all the upcoming events.
     * Returns ArrayList[EmailInstance]
     */
    public static EmailInstance createEmailInstance(bool send)
    {
        return createEmailInstance(send, DateTime.Now);
    }

    public static EmailInstance createEmailInstance(bool send, DateTime now)
    {
        // did we already send one today?
        if (alreadySent(now))
            return null;

        // see if any event are in a warning state.
        ArrayList events = EventMgr.singleton.loadAll();
        foreach (Event ev in events)
            if (ev.hasWarning())
            {
                EmailInstance ei = new DailyReport().createEmailInstance();
                if (send)
                    ei.send();
                return ei;
            }

        return null;
    }


    /**
     * private Constructor
     */
    public DailyReport() : 
        base(EmailConstants.DailyReport)
    {
    }


    /**
     * Override this to set the recipients.  Just send to JoesClubReports@spark.net
     */
    protected override ArrayList getRecipients(EmailInstance emailInstance)
    {
        ArrayList recipients = new ArrayList();
        EmailRecipient er = new EmailRecipient(
            DbConstants.NoId, emailInstance, null, null, "joesclubreports@spark.net");
        recipients.Add(er);
        return recipients;
    }


    /**
     * Subclasses override these to set token values.  Subclasses should
     * return null for missing values.  This email doesn't have
     * any tokens.
     */
    public override string getTokenVal(EmailRecipient recipient, EmailTokenDef tokenDef) { 
        return null; }

    /**
     * Returns the member ids of everyone who has recieved the email 
     * with name emailDefName for the event
     * Returns ArrayList[int memberId]
     */
    private static bool alreadySent(DateTime now)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            // the time span is the start of the current day to the end of it.
            TimeSpan ts = new TimeSpan(now.Hour, now.Minute, now.Second);
            DateTime startTime = now.Subtract(ts);
            DateTime endTime = startTime.AddDays(1);

            // set up the query
            conn = DbUtils.getConnection();
            SqlCommand command = DbUtils.getStoredProcCommand(conn, "GetEmailInstancesInTimespan");
            command.Parameters.Add(DbUtils.createStringParam("@emailDefName", EmailConstants.DailyReport));
            command.Parameters.Add(DbUtils.createDateTimeParam("@startTime", startTime));
            command.Parameters.Add(DbUtils.createDateTimeParam("@endTime", endTime));

            // If there are any results, then we already sent the daily
            // report today
            reader = command.ExecuteReader();
            while (reader.Read())
                return true;

            return false;
        }

        finally
        {
            if (reader != null)
                reader.Close();
            if (conn != null)
                conn.Close();
        }
    }

}

}
