using System;
using System.Collections;
using System.Text;
using System.Web;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create "AfterDinner" EmailInstances.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class AfterDinner : MemberEmail
{
    private static Log _log = new Log("AfterDinner");


    /**
     * Create a new EmailInstance
     * 
     * members is the set of members who will receive the email.
     * members = ArrayList[Member]
     */
    public static EmailInstance createEmailInstance(Event ev)
    {
        return new AfterDinner(ev).createEmailInstance();
    }


    /**
     * private Constructor
     */
    public AfterDinner(Event ev) : 
        base(EmailConstants.AfterDinner, ev)
    {
    }


    /**
     * Subclasses override this to set the recipients.
     * Returns all the attendees for the event
     */
    protected override ICollection getMembers(EmailInstance emailInstance)
    {
        ArrayList members = new ArrayList();
        foreach (EventMember eventMember in _event.eventMembers)
            if (eventMember.memberStatus == MemberStatus.Attended)
                members.Add(eventMember.member);

        return members;
    }



    /**
     * Get member specific tokens
     */
    public override string getTokenVal(EmailRecipient recipient, Member member, EmailTokenDef tokenDef)
    {
        string tokenVal = base.getTokenVal(recipient, member, tokenDef);
        if (tokenVal != null)
            return tokenVal;

        // get token from the member's table
        Table table = getMemberTable(member);
        return getTokenVal(recipient, member, table, tokenDef);
    }


    /**
     * Get tokens for the other members at the table, e.g. MemberCity3
     */
    private string getTokenVal(EmailRecipient recipient, Member member, Table table, EmailTokenDef tokenDef)
    {
        string tokenDefName = null;
        int memberIndex = -1;
        if (tokenDef.name.StartsWith("MemberImage"))
            tokenDefName = "MemberImage";
        else if (tokenDef.name.StartsWith("MemberCity"))
            tokenDefName = "MemberCity";
        else if (tokenDef.name.StartsWith("MemberJdateUrl"))
            tokenDefName = "MemberJdateUrl";

        if (tokenDefName != null)
        {
            string sMemberIndex = tokenDef.name.Substring(tokenDefName.Length);
            try {
                memberIndex = int.Parse(sMemberIndex);
                --memberIndex; // make it zero based
            }
            catch {}
        }

        if (memberIndex == -1)
            return null;

        ArrayList tableMembers = new ArrayList();
        foreach (Member tm in table.members)
        {
            EventMember eventMember = _event.getEventMember(tm.id);
            if (tm != member && eventMember.memberStatus == MemberStatus.Attended)
                tableMembers.Add(tm);
        }

        if (tableMembers.Count <= memberIndex)
            return null;
        Member tableMember = (Member) tableMembers[memberIndex];

        if (tokenDefName == "MemberImage")
            return tableMember.image;
        else if (tokenDefName == "MemberCity")
            return tableMember.city;
        else if (tokenDefName == "MemberJdateUrl")
            return tableMember.jdateUrl;

        return null;
    }
}

}
