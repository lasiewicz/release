using System;
using Matchnet.JoesClub.ValueObjects;


/**
 * Gets email related values from web.config.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailConfig
{
    private static Log _log = new Log("EmailConfig");

    public static EmailConfig singleton = new EmailConfig();


    /**
     * Get the Nudge1 TimeSpan
     */
    public TimeSpan getNudge1TimeSpan()
    {
        return Config.getTimeSpan("EmailNudge1Time", "EmailNudge1Units");
    }


    /**
     * Get the Nudge2 TimeSpan
     */
    public TimeSpan getNudge2TimeSpan()
    {
        return Config.getTimeSpan("EmailNudge2Time", "EmailNudge2Units");
    }


    /**
     * Get the InviteExpiration TimeSpan
     */
    public TimeSpan getInviteExpirationTimeSpan()
    {
        return Config.getTimeSpan("EmailInviteExpirationTime", "EmailInviteExpirationUnits");
    }


    /**
     * Get the TMinus1 TimeSpan
     */
    public TimeSpan getTMinus1TimeSpan()
    {
        return Config.getTimeSpan("EmailTMinus1Time", "EmailTMinus1Units");
    }


    /**
     * Get the TMinus5 TimeSpan
     */
    public TimeSpan getTMinus5TimeSpan()
    {
        return Config.getTimeSpan("EmailTMinus5Time", "EmailTMinus5Units");
    }


    /**
     * Returns the email Sender specified by "EmailSender"
     * in web.config.
     */
    public Sender getSender()
    {
        string sSender = Config.getProperty("EmailSender");
        if (sSender == "LogSender")
            return LogSender.singleton;
        else if (sSender == "TestStormPostSender")
            return TestStormPostSender.singleton;
        else if (sSender == "StormPostSender")
            return StormPostSender.singleton;
        else if (sSender == "AsyncSender")
            return AsyncSender.singleton;

        return null;
    }


    /**
     * Returns the email recipients specified by "EmailDebugRecipients"
     * in web.config.
     */
    public string[] getDebugRecipients()
    {
        string sDebugRecipient = Config.getProperty("EmailDebugRecipients");
        if (sDebugRecipient == null || sDebugRecipient.Trim() == "")
            return new string[0];
        string[] debugRecipients = sDebugRecipient.Split(new char[] {',', ';'});
        for (int i = 0; i < debugRecipients.Length; i++)
            debugRecipients[i] = debugRecipients[i].Trim();
        return debugRecipients;
    }
}
}
