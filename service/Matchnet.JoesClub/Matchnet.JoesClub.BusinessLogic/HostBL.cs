using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class HostBL
{
    private static Log _log = new Log("HostBL");

    public static HostBL Instance = new HostBL();

    
    private HostBL()
    {
    }


    /**
     * Load the Host
     */
    public HostVO load(int id)
    {
        Host host = (Host) HostMgr.singleton.load(id);
        if (host == null)
            return null;

        HostVO hostVO = new HostVO(
            host.id,
            host.firstName,
            host.lastName,
            host.image,
            host.phone,
            host.phone2,
            host.emailAddress,
            host.availableNights,
            host.manager,
            host.notes);

        return hostVO;
    }


    /**
     * delete the Host
     */
    public void delete(int id)
    {
        HostMgr.singleton.delete(id);
    }


    /**
     * save the Host
     */
    public int save(HostVO hostVO)
    {
        if (hostVO.id == DbConstants.NoId)
            add(hostVO);
        else
            update(hostVO);

        return hostVO.id;
    }


    /**
     * add the Host
     */
    public void add(HostVO hostVO)
    {
        Host host = new Host(
            DbConstants.NoId,
            hostVO.firstName,
            hostVO.lastName,
            hostVO.image,
            hostVO.phone,
            hostVO.phone2,
            hostVO.emailAddress,
            hostVO.availableNights,
            hostVO.manager,
            hostVO.notes,
            false); // deleted

        host.save();
        hostVO.id = host.id;
    }


    /**
     * update the Host
     */
    public void update(HostVO hostVO)
    {
        Host host = (Host) HostMgr.singleton.load(hostVO.id);

        host.firstName   = hostVO.firstName;
        host.lastName    = hostVO.lastName;
        host.image   = hostVO.image;
        host.phone   = hostVO.phone;
        host.phone2  = hostVO.phone2;
        host.emailAddress    = hostVO.emailAddress;
        host.availableNights     = hostVO.availableNights;
        host.manager     = hostVO.manager;
        host.notes   = hostVO.notes;

        host.save();

    }


    /**
     * Gets a dataset of all the hosts
     */
    public DataSet getDataSet()
    {
        return DbUtils.getDataSet("GetAllHosts", "hosts", null);
    }


    /**
     * Get the Events the Host hosted
     * Returns ArrayList[EventVO]
     */
    public ArrayList getEventsHosted(HostVO hostVO)
    {
        Host host = (Host) HostMgr.singleton.load(hostVO.id);
        ArrayList events = host.getEventsHosted();
        ArrayList eventVOs = new ArrayList();
        foreach (Event ev in events)
        {
            EventVO eventVO = new EventVO(
                ev.id,
                ev.name,
                ev.getStartTime(true));
            eventVOs.Add(eventVO);
        }

        return eventVOs;
    }
}
}
