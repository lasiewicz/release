/**
 *  Object which can log themselves implement this
 */ 
namespace Matchnet.JoesClub.BusinessLogic
{
public interface Logable
{
    void log(ObjectLogger objectLogger);
}

}