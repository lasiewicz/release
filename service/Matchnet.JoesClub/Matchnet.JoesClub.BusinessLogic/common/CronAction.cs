using System;


/**
 * Interface for classes called by CronThread
 */
namespace Matchnet.JoesClub.BusinessLogic
{
interface CronAction
{
    /**
     * Do the action
     */
    void doAction();

}
}
