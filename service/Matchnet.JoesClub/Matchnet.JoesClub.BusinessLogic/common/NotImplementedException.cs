using System;


namespace Matchnet.JoesClub.BusinessLogic
{
public class NotImplementedException : Exception
{
    public NotImplementedException() : 
        base("Method not implemented")
    {
    }
}
}