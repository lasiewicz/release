using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Xml;
using Matchnet.JoesClub.ValueObjects;


/**
 * Convienience methods for generating xml and html.  It's just a wrapper
 * around XmlTextWriter
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class XmlHelper
{
    private TextWriter _textWriter;
    public TextWriter textWriter { get { return _textWriter; } }

    private XmlTextWriter _xw;
    public XmlWriter xmlWriter { get { return _xw; } }

    private int _elementDepth;


    /**
     * Constructor
     */
    public XmlHelper(HttpResponse response) : this(response.Output)
    {
    }


    /**
     * Constructor
     */
    public XmlHelper(TextWriter textWriter)
    {
        _textWriter = textWriter;
        _xw = new XmlTextWriter(textWriter);
        _xw.Formatting = Formatting.Indented;
        _xw.Indentation = 4;
        _elementDepth = 0;
    }


    /**
     * write an start tag: <tag .... >
     */
    public void startElem(string tag, bool end, string[] attrs)
    {
        _xw.WriteStartElement(tag);
        if (attrs != null)
            for (int i = 0; i < attrs.Length; i += 2)
            {
                string attrValue = attrs[i + 1]; //Misc.escapeQuotes(attrs[i + 1]);
                _xw.WriteAttributeString(attrs[i], attrValue);
            }
        if (end)
            _xw.WriteEndElement();
        else
            ++_elementDepth;
    }


    /**
     * write an end tag: </tag>
     */
    public void endElem()
    {
        _xw.WriteEndElement();
        --_elementDepth;
    }


    /**
     * Closes all the open tags.  If dontCloseRoot is true, the outermost
     * element won't be close so the error handling code can add more
     * xml.
     */
    public void cleanUp(bool closeRoot)
    {
        int finalDepth = closeRoot ? 0 : 1;
        for ( ; _elementDepth > finalDepth; )
            endElem(); // remember, endElem() decrements _elementDepth
    }


    /**
     * writes text
     */
    public void writeText(string value)
    {
        _xw.WriteString(value);
    }


    /**
     * Writes <tag>value</tag>
     */
    public void writeSimpleElem(string tag, string value)
    {
        startElem(tag, false, null);
        writeText(Misc.nullToEmptyString(value));
        endElem();
    }


    /**
     * Writes <tag>value</tag> for a double
     */
    public void writeSimpleElem(string tag, double value)
    {
        startElem(tag, false, null);
        writeText(value.ToString());
        endElem();
    }


    /**
     * Writes <tag>value</tag> for an int
     */
    public void writeSimpleElem(string tag, int value)
    {
        startElem(tag, false, null);
        writeText(value.ToString());
        endElem();
    }


    /**
     * Writes <tag>date</tag> for a date
     */
    public void writeSimpleElem(string tag, DateTime dt, string format)
    {
        string sDate = "";
        if (dt != CommonConstants.DateTimeNone)
            sDate = dt.ToString(format);
        writeSimpleElem(tag, sDate);
    }

    /**
     * Writes <tag>bool</tag> for a bool
     */
    public void writeSimpleElem(string tag, bool b)
    {
        writeSimpleElem(tag, b.ToString().ToLower());
    }


    /**
     * write start of page html
     */
    public void startHtmlPage(string title)
    {
        startElem("html", false, null);

        startElem("head", false, null);
        startElem("title", false, null);
        
        writeText(title);

        endElem(); // </title");
        endElem(); // </head");

		startElem("body", false, null);
	}


    /**
     * write end of page html
     */
    public void endHtmlPage()
    {
        endElem(); // </body");
        endElem(); // </html");
    }


    /**
     * write a text input elem
     */
    public void textInput(string label, string name, string value)
    {
        startElem("tr", false, null);
        startElem("td", false, new string[] { "align", "right" });
        writeText(label + ": ");
        endElem(); // </td");
        startElem("td", false, null);
        startElem("input", true, new string[] { 
                                                  "type", "text",
                                                  "name", name,
                                                  "value", value });
        endElem(); // </td>
        endElem(); // </tr>
    }



    /**
     * write a submit button
     */
    public void submitButton(string action)
    {
        startElem("input", true, new string[] { 
                                                  "type", "submit",
                                                  "name", "action",
                                                  "value", action });
    }


    /**
     * write a submit button
     */
    public void hiddenInput(string name, string value)
    {
        startElem("input", true, new string[] { 
                                                  "type", "hidden",
                                                  "name", name,
                                                  "value", value });
    }


    /**
     * JavaScript to show an alert
     */
    public void alertScript(string message)
    {
        startElem("script", false, new string[] { "language", "javascript" });
        writeText("window.alert(\"" + message + "\")");
        endElem(); // </script>
    }


    /**
     * writes a </br>
     */
    public void br()
    {
        startElem("br", true, null);
    }

    
    /**
     * writes an exception 
     */
    public void writeException(Exception e) 
    {
        startElem("error", false, null);

        // write the exception and any inner exceptions
        for ( ; e != null; e = e.InnerException)
        {
            startElem("message", false, null);
            writeText(e.Message);
            endElem(); // message

            startElem("stack", false, null);
            writeText(e.StackTrace);
            endElem(); // stack
        }

        endElem(); // error
    }

    
    /**
     * Start standard JoesClub xml
     */
    public void startJClubs()
    {
		startElem("JClubs", false, null);
		writeSimpleElem("UxUrl", Config.getProperty("UxUrl"));
    }

    
    /**
     * End standard JoesClub xml
     */
    public void endJClubs()
    {
		endElem(); // JClubs
    }
}
}