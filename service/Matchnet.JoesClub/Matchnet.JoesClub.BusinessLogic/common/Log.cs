using System;
using System.IO; // StreamWriter

namespace Matchnet.JoesClub.BusinessLogic
{
public class Log
{
    public const string sDebug = "DEBUG";
    public const string sInfo = "INFO";
    public const string sWarning = "WARNING";
    public const string sError = "ERROR";
    public const string sAlways = "LOG";
    public const string sOff = "OFF";

    public const int iDebug     = 0;
    public const int iInfo      = 1;
    public const int iWarning   = 2;
    public const int iError     = 3;
    public const int iAlways    = 4;
    public const int iOff       = 5;

    private static string[] severityNames = new string[] {  sDebug,
                                                            sInfo,
                                                            sWarning,
                                                            sError,
                                                            sAlways,
                                                            sOff };

    private string _className;

    public Log(string className)
    {
        _className = className;
    }

    public void debug(string s) { write(s, iDebug); }
    public void info(string s) { write(s, iInfo); }
    public void warning(string s) { write(s, iWarning); }
    public void write(string s) { write(s, iAlways); }

    public void error(string s) 
    { 
        write(s, iError);
        throw new Exception(s);
    }

    public void error(Exception e) 
    { 
        error(e, false);
    }

    private void error(Exception e, bool inner) 
    { 
        string sInner = inner ? "Inner exception " : "";
        write(sInner + e.Message, iError);
        write("\n" + e.StackTrace, iError);

        // write InnerException
        if (e.InnerException != null)
        {
            write("\n", iError);
            error(e.InnerException, true);
        }
    }

    public void logObject(string label, Object obj)
    {
        logObject(label, obj, iDebug);
    }

    public void logObject(string label, Object obj, int severity)
    {
        if (severityTooLow(severity))
            return;
        ObjectLogger objectLogger = new ObjectLogger(); 
        objectLogger.log(label, obj);
    }


    /**
     * This is the method that actually constructs the line to write to the file
     */
    private void write(string s, int severity)
    {
        if (severityTooLow(severity))
            return;

        LogFile.writeln(
            severityNames[severity] + " " + 
            DateTime.Now.ToString() + " " + 
            _className + " " + 
            s);
    }


    /**
     * returns true if the severity is lower than the log level
     */ 
    private bool severityTooLow(int severity)
    {
        return severity < Config.getLogSeverity(_className);
    }


    /**
     * return the severity int for the string
     */
    public static int severityStringToInt(string s)
    {
        string us = s.ToUpper();
        switch (us)
        {
            case sDebug:    return iDebug;
            case sInfo:     return iInfo;
            case sWarning:  return iWarning;
            case sError:    return iError;
            case sAlways:   return iAlways;
            case sOff:      return iOff;
        }
        throw new Exception("Bad severity string: " + s);
    }
}

}