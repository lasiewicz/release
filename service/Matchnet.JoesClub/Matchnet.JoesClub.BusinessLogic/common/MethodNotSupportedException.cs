using System;


namespace Matchnet.JoesClub.BusinessLogic
{
public class MethodNotSupportedException : Exception
{
    public MethodNotSupportedException(string method) : 
        base("Method not supported: " + method)
    {
    }
}
}