using System;
using System.IO; // StreamWriter
using System.Collections; // Hashtable
using Microsoft.JScript;


/**
 * This is used in conjunction with Logable for debugging.  These classes make it easy to 
 * print java objects.  The static printObject() methods are for clients and the 
 * non-static ones are for Logable classes to use in their log() method.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class ObjectLogger
{
	private int _indent;
		
    
    /**
     * private Constructor
     */
    public ObjectLogger()
    {
    }

    
	/**
	 * log an object.  Used by Logable classes to log their sub-components
	 */
	public void log(string label, Object obj)
	{
		log(label, obj, false);
	}


    /**
     * Log an int.  
     */
    public void log(string label, int i)
    {
        log(label, i, true);
    }


    /**
     * Log a byte.  
     */
    public void log(string label, byte b)
    {
        log(label, b, true);
    }


	/**
	 * Log a bool.  
	 */
    public void log(string label, bool b)
	{
		log(label, b, true);
	}


	/**
	 * Log a long.  
	 */
    public void log(string label, long l)
	{
		log(label, l, true);
	}


	/**
	 * Log a double.  
	 */
    public void log(string label, double d)
	{
		log(label, d, true);
	}


	/**
	 * Log a float.  
	 */
    public void log(string label, float f)
	{
		log(label, f, true);
	}


	/**
	 * Log an object.  Used by Logable classes to log their sub-components
	 */
    private void log(string label, Object obj, bool primitive)
	{
    	// indent
    	for (int i = 0; i < _indent; i++)
    		write("    ");

    	// print label
    	if (label != null)
    	{
    		write(label);
    		write(": ");
    	}
    	if (obj != null) 
    	{
    		write("(");
    		write(getClassName(obj, primitive));
    		write(") "); 
    	}

    	// print the object properly, depending on the type
    	if (obj == null)
    		writeln("null");

    	else if (obj is string)
    	{
    		if (((string) obj).Equals("")) 
    			write("<empty>");
    		else
    			write((string) obj);
    		writeln(); 
    	}

    	// Logable
    	else if (obj is Logable)
    	{
    		writeln();
    		++_indent;
    		((Logable) obj).log(this);
    		--_indent;
    	}

    	
    	// Map
    	else if (obj is Hashtable)
    	{
    		writeln();

            Hashtable h = (Hashtable) obj;
            
            foreach (Object key in h.Keys)
            {
                Object val = h[key];
    			++_indent;
    			log("[" + key.ToString() + "]", val); 
    			--_indent;
            }
    	}

        // List
        else if (obj is IList)
        {
            writeln();

            IList v = (IList) obj;
            int i = 0;
            foreach (Object val in v)
            {
                ++_indent;
                log("[" + i + "]", val);
                --_indent;
                ++i;
            }
        }

        // Microsoft.JScript.ArrayObject
        else if (obj is ArrayObject)
        {
            writeln();
            ArrayObject ao = (ArrayObject) obj;
            foreach (Object val in ao)
            {
                ++_indent;
                log("[" + val + "]", ao[val]);
                --_indent;
            }
        }

        // Element
        /*
        else if (obj is Node)
        {
            writeln();
            ++_indent;
            String xml = Misc.serializeNode((Node) obj, false);
            write(xml);
            --_indent;
        }
        */

    	// Object Array
    	else if (obj is object[])
    	{
    		writeln();

    		Object[] array = (Object[]) obj;
    		for (int i = 0; i < array.Length; i++)
    		{
    			++_indent;
    			log("[" + i + "]", array[i]);
    			--_indent;
    		}
    	}

    	// int Array
    	else if (obj is int[])
    	{
    		writeln();

    		int[] array = (int[]) obj;
    		for (int i = 0; i < array.Length; i++)
    		{
    			++_indent;
    			log("[" + i + "]", array[i]);
    			--_indent;
    		}
    	}

    	// long Array
    	else if (obj is long[])
    	{
    		writeln();

    		long[] array = (long[]) obj;
    		for (int i = 0; i < array.Length; i++)
    		{
    			++_indent;
    			log("[" + i + "]", array[i]);
    			--_indent;
    		}
    	}

    	// bool Array
    	else if (obj is bool[])
    	{
    		writeln();

    		bool[] array = (bool[]) obj;
    		for (int i = 0; i < array.Length; i++)
    		{
    			++_indent;
    			log("[" + i + "]", array[i]);
    			--_indent;
    		}
    	}

    	// char Array
    	else if (obj is char[])
    	{
    		writeln();

    		char[] array = (char[]) obj;
    		for (int i = 0; i < array.Length; i++)
    		{
    			++_indent;
    			log("[" + i + "]", array[i]);
    			--_indent;
    		}
    	}

    	// double Array
    	else if (obj is double[])
    	{
    		writeln();

    		double[] array = (double[]) obj;
    		for (int i = 0; i < array.Length; i++)
    		{
    			++_indent;
    			log("[" + i + "]", array[i]);
    			--_indent;
    		}
    	}

        // byte Array
        else if (obj is byte[])
        {
            writeln();

            byte[] array = (byte[]) obj;
            for (int i = 0; i < array.Length; i++)
            {
                ++_indent;
                log("[" + i + "]", array[i]);
                --_indent;
            }
        }

    	else
    	{
    		writeln(obj.ToString());
    	}

    }


	/**
	 * Gets an object's class name, stripping off common prefixes.
	 */
	private string getClassName(Object obj, bool primitive)
	{
		if (primitive)
		{
			if (obj is int)
				return "int";
			if (obj is bool)
				return "bool";
			if (obj is long)
				return "long";
			if (obj is double)
				return "double";
			if (obj is float)
				return "float";
		}
		
		string className = obj.GetType().Name;
        /*
		if (className.startsWith("java.lang."))
			return className.substring("java.lang.".Length());
		if (className.startsWith("java.util."))
			return className.substring("java.util.".Length());
            */
		return className;
	}
    
    
    /**
     * Get the textual representation of an object. 
    public static string getText(Object obj)
    {
        // create a StreamWriter 
        StreamWriter sw = new StreamWriter();
        
        // generate text
        ObjectLogger objectLogger = new ObjectLogger(sw);
        objectLogger.log(null, obj);
        
        // return the text
        return sw.getBuffer().ToString();
    }
      */
   
    
    private void write(string s)
    {
        LogFile.write(s);
    }
    
    
    private void writeln()
    {
        writeln("");
    }
    
    
    private void writeln(string s)
    {
        LogFile.write(s + "\n");
    }

}

}