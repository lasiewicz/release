using System;
using System.Collections;
using System.Threading;
using Matchnet.JoesClub.ValueObjects;


/**
 * Used to create time triggered emails
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class CronThread
{
    private static Log _log = new Log("CronThread");

    public static CronThread singleton = new CronThread();
    private Thread _thread;

    private bool _running;
    public bool running { get { return _running; } set { _running = value; } }

    private ArrayList _cronActions;


    /**
     * start it
     */
    public void start()
    {
        if (_running)
            return;
        _running = true;
        _thread.Start();
        
    }


    /**
     * stop it
     */
    public void stop()
    {
        init();
    }


    /**
     * Constructor
     */
    private CronThread()
    {
        init();
    }


    /**
     * Initailize everything
     */
    private void init()
    {
        ThreadStart threadStart = new ThreadStart(run);
        _thread = new Thread(threadStart);
        _running = false;


        // create the CronActions
        _cronActions = new ArrayList();
        _cronActions.Add(new EmailCronAction());
    }


    /**
     * Run the thread
     */
    public void run()
    {
        try
        {
            while (_running)
            {
                wakeUp();

                foreach (CronAction cronAction in _cronActions)
                {
                    try
                    {
                        // do the action
                        cronAction.doAction();
                    }
                    catch (Exception e)
                    {
                        _log.error(e);
                    }
                }

                // wait until it's time to check again.
                goToSleep();
            }
        }
        catch (Exception e)
        {
            _log.error(e);
        }
    }


    /**
     * Kind of messy, but the code expect each thread to set up some stuff
     */
    private void wakeUp()
    {
        LogFile.open();
        DbObjectCache.create();
    }

    
    /**
     * Put the thread to sleep
     */
    private void goToSleep()
    {
        // clean up stuff from last execution
        LogFile.close();
        DbObjectCache.destroy();

        // figure out how long to sleep
        TimeSpan sleepTimeSpan = Config.getTimeSpan("CronSleepTime", "CronSleepUnits");
        Thread.Sleep(sleepTimeSpan);
    }
}
}
