using System;



/**
 * Used to indicate that some inconsistency has arisen, for example, while
 * loading database objects
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class ConcurrencyException : Exception
{
    public ConcurrencyException() : base() {}
    public ConcurrencyException(string message) : base(message) {}
}
}
