using System;
using System.Collections;
using System.Configuration;
using System.IO; 
using System.Web; 
using System.Xml; 
using Matchnet.JoesClub.ValueObjects;


/**
 * Read config.xml
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class Config
{
    private static RuleSet _c_ruleSet;


    /**
     * returns a the property for the given name
     */
    public static string getProperty(string name)
    {
        return ConfigurationSettings.AppSettings[name];
    }

    /**
     * returns a bool property for the given name
     */
    public static bool getBoolProperty(string name)
    {
        string sValue = getProperty(name);
        if (sValue == null)
            return false;
        return bool.Parse(sValue);
    }

    /**
     * returns a int property for the given name
     */
    public static int getIntProperty(string name)
    {
        string sValue = getProperty(name);
        if (sValue == null)
            return CommonConstants.NoValueInt;
        return int.Parse(sValue);
    }


    /**
     * returns the log level for the given className
     */
    public static int getLogSeverity(string className)
    {
        string sSeverity = Config.getProperty("logSeverity." + className);
        if (sSeverity == null || sSeverity == "")
            sSeverity = Config.getProperty("logSeverity.default");

        return Log.severityStringToInt(sSeverity);
    }


    /**
     * returns _c_ruleSet
     */
    public static RuleSet ruleSet 
    { 
        get 
        { 
            init("_c_ruleSet");
            return _c_ruleSet; 
        }
    }


    /**
     * Returns the location in the file system where images are uploaded.
     */
    public static string getImageUploadRoot()
    { 
        string sImageUploadRoot = ConfigurationSettings.AppSettings["ImageUploadRoot"];
        if (sImageUploadRoot == null)
            return getJoesClubRoot() + "images/uploaded/";
        else
            return sImageUploadRoot + "/";
    }


    /**
     * Returns the url where images are uploaded.
     */
    public static string getImageUploadRootUrl()
    { 
        string sImageUploadRootUrl = ConfigurationSettings.AppSettings["ImageUploadRootUrl"];
        if (sImageUploadRootUrl == null)
            return getJoesClubRootUrl() + "images/uploaded/";
        else
            return sImageUploadRootUrl + "/";
    }


    /**
     * Returns the location of the JoesClub installation in the file system.
     * For convenience, adds \ to the end.
     */
    public static string getJoesClubRoot()
    { 
        return HttpContext.Current.Request.MapPath("") + "\\";
    }


    /**
     * Returns the root JoesClub url, e.g. http://dv-joeclub/JoesClub/
     * For convenience, adds / to the end.
     */
    public static string getJoesClubRootUrl()
    { 
        HttpRequest request = HttpContext.Current.Request;
        string appPath = request.ApplicationPath;
        string url = request.Url.AbsoluteUri;
        int endIndex = request.Url.AbsoluteUri.IndexOf(appPath);
        url = url.Substring(0, endIndex) + appPath + "/";
        return url;
    }


    /**
     * Returns true if we're on the spark network
     */
    public static bool onSparkNetwork() 
    { 
        return getBoolProperty("OnSparkNetwork");
    }


    /**
     * Returns true if this is the Admin website
     */
    public static bool isAdmin() 
    { 
        return getBoolProperty("IsAdmin");
    }


    /**
     * Returns the set of venue cities
     */
    public static string[] getVenueCities() 
    { 
        string sVenueCities = getProperty("VenueCities");
        string[] venueCities = sVenueCities.Split(new char[] {','});
        for (int i = 0; i < venueCities.Length; i++)
            venueCities[i] = venueCities[i].Trim();
        return venueCities;
    }


    /**
     * clear cached values
     */
    public static void clear() 
    { 
        _c_ruleSet = null; 
    }


    /**
     * Initialize class members that are created on demand.
     */
    private static void init(string name)
    {
        if (name == "_c_ruleSet" && _c_ruleSet == null)
            _c_ruleSet = new RuleSet();
    }
}


}