using System;

/**
 * Constants used by the whole system
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class CommonConstants
{
    // gender
    public const bool Male = false;
    public const bool Female = true;

    // used to indicate a missing int value
    public const int NoValueInt = -1;

    // used to indicate a missing int value
    public const double NoValueDouble = -1.0;

    // indicates no DateTime since you can't seem to use null.
    public static DateTime NoValueDate = new DateTime(2000, 1, 1);

    // used to indicate an empty bit mask
    public const int EmptyMask = 0;



    // jlf obsolete.  should get rid of this and use NoValueDate everywhere.
    public static DateTime DateTimeNone = NoValueDate;
}
}

