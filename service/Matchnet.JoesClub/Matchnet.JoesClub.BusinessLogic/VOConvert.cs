using System;
using System.Collections;


using Matchnet.JoesClub.ValueObjects;


/**
 * Converts between VO objects and Biz objects
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class VOConvert
{

    /**
     * Copy the values from member to memberVO.
     */
    public static MemberVO convert(Member member)
    {
        MemberVO memberVO = new MemberVO();

        memberVO.id = member.id;
        memberVO.sparkId = member.sparkId;
        memberVO.gender = member.gender;
        memberVO.image = member.image;
        memberVO.firstName = member.firstName;
        memberVO.lastName = member.lastName;
        memberVO.userName = member.userName;
        memberVO.city = member.city;
        memberVO.state = member.state;
        memberVO.zipCode = member.zipCode;
        memberVO.emailAdd = member.emailAdd;
        memberVO.emailAdd2 = member.emailAdd2;
        memberVO.phone = member.phone;
        memberVO.phone2 = member.phone2;
        memberVO.shortNotice = member.shortNotice;
        memberVO.age = member.age;
        memberVO.desiredAgeLow = member.desiredAgeLow;
        memberVO.desiredAgeHigh = member.desiredAgeHigh;
        memberVO.height = member.height;
        memberVO.bodyType = member.bodyType;
        memberVO.weight = member.weight;
        memberVO.education = member.education;
        memberVO.jdateReligion = member.jdateReligion;
        memberVO.desiredJDateReligion = member.desiredJDateReligion;
        memberVO.smoking = member.smoking;
        memberVO.numKids = member.numKids;
        memberVO.willDateAParent = member.willDateAParent;
        memberVO.kidsPref = member.kidsPref;
        memberVO.desiredKidsPref = member.desiredKidsPref;
        memberVO.startDate = member.startDate;
        memberVO.startRate = member.startRate;
        memberVO.renewalDate = member.renewalDate;
        memberVO.renewalRate = member.renewalRate;
        memberVO.expirationDate = member.expirationDate;
        memberVO.avatar = member.avatar;
        memberVO.activityStatus = member.activityStatus;
        memberVO.meetingSomeone = member.meetingSomeone;
        memberVO.describeMe = member.describeMe;
        memberVO.phoneType = member.phoneType;
        memberVO.phoneType2 = member.phoneType2;
        memberVO.deleted = member.deleted;
        memberVO.hearAboutJoesClub = member.hearAboutJoesClub;
        memberVO.hearAboutJoesClubExplain = member.hearAboutJoesClubExplain;
        memberVO.whatAppealedToYou = member.whatAppealedToYou;
        memberVO.jdateExperience = member.jdateExperience;
        memberVO.biggestConcern = member.biggestConcern;
        memberVO.peopleNotice = member.peopleNotice;
        memberVO.occupation = member.occupation;
        memberVO.spendTime = member.spendTime;
        memberVO.spendTimeText = member.spendTimeText;
        memberVO.locationPref = member.locationPref;
        memberVO.timePref = member.timePref;
        memberVO.dietaryRestrictions = member.dietaryRestrictions;
        memberVO.cuisinesToAvoid = member.cuisinesToAvoid;
        memberVO.additionalNights = member.additionalNights;
        memberVO.desiredMaritalStatus = member.desiredMaritalStatus;
        memberVO.desiredEducationLevel = member.desiredEducationLevel;
        memberVO.desiredDrinkingHabits = member.desiredDrinkingHabits;
        memberVO.desiredSmokingHabits = member.desiredSmokingHabits;
        memberVO.birthdate = member.birthdate;
        memberVO.contractReceived = member.contractReceived;
        memberVO.loveMostAboutJDate = member.loveMostAboutJDate;
        memberVO.personalityQuestion1 = member.personalityQuestion1;
        memberVO.personalityQuestion2 = member.personalityQuestion2;
        memberVO.personalityQuestion3 = member.personalityQuestion3;
        memberVO.personalityQuestion4 = member.personalityQuestion4;
        memberVO.personalityQuestion5 = member.personalityQuestion5;
        memberVO.personalityQuestion6 = member.personalityQuestion6;
        memberVO.personalityQuestion7 = member.personalityQuestion7;
        memberVO.personalityQuestion8 = member.personalityQuestion8;
        memberVO.personalityQuestion9 = member.personalityQuestion9;
        memberVO.personalityQuestion10 = member.personalityQuestion10;
        memberVO.personalityQuestion11 = member.personalityQuestion11;
        memberVO.personalityQuestion12 = member.personalityQuestion12;
        memberVO.applicationRemarks = member.applicationRemarks;
        memberVO.inPersonInterviewTime = member.inPersonInterviewTime;
        memberVO.phoneInterviewNotes = member.phoneInterviewNotes;
        memberVO.interviewQuestionaireNotes = member.interviewQuestionaireNotes;
        memberVO.relationship = member.relationship;
        memberVO.whatElseToKnow = member.whatElseToKnow;
        memberVO.whenToCall = member.whenToCall;
        memberVO.applicationReceived = member.applicationReceived;
        memberVO.feesChargedText = member.feesChargedText;
        memberVO.emailOptedOut = member.emailOptedOut;
        memberVO.trxId = member.trxId;
        memberVO.guid = member.guid;
        memberVO.phoneInterviews = convert(member.phoneInterviews);
        memberVO.jdateUrl = member.jdateUrl;
		memberVO.interviewDiscussed = member.interviewDiscussed;
		memberVO.interviewReservations = member.interviewReservations;

        return memberVO;
    }



    /**
     * Copy the values from memberVO to member.
     */
    public static Member convert(MemberVO memberVO)
    {
        if (memberVO == null)
            return null;

        // create Member
        Member member;
        if (memberVO.id != DbConstants.NoId)
            member = (Member) MemberMgr.singleton.load(memberVO.id);
        else
            member =  Member.create(memberVO.sparkId);

        // copy values
        convert(memberVO, member);

        return member;
    }


    /**
     * Copy the values from memberVO to member.
     */
    public static void convert(MemberVO memberVO, Member member)
    {
        if (memberVO == null)
            return;

        member.id = memberVO.id;
        member.sparkId = memberVO.sparkId;
        member.gender = memberVO.gender;
        member.image = memberVO.image;
        member.firstName = memberVO.firstName;
        member.lastName = memberVO.lastName;
        member.userName = memberVO.userName;
        member.city = memberVO.city;
        member.state = memberVO.state;
        member.zipCode = memberVO.zipCode;
        member.emailAdd = memberVO.emailAdd;
        member.emailAdd2 = memberVO.emailAdd2;
        member.phone = memberVO.phone;
        member.phone2 = memberVO.phone2;
        member.shortNotice = memberVO.shortNotice;
        member.age = memberVO.age;
        member.desiredAgeLow = memberVO.desiredAgeLow;
        member.desiredAgeHigh = memberVO.desiredAgeHigh;
        member.height = memberVO.height;
        member.bodyType = memberVO.bodyType;
        member.weight = memberVO.weight;
        member.education = memberVO.education;
        member.jdateReligion = memberVO.jdateReligion;
        member.desiredJDateReligion = memberVO.desiredJDateReligion;
        member.smoking = memberVO.smoking;
        member.numKids = memberVO.numKids;
        member.willDateAParent = memberVO.willDateAParent;
        member.kidsPref = memberVO.kidsPref;
        member.desiredKidsPref = memberVO.desiredKidsPref;
        member.startDate = memberVO.startDate;
        member.startRate = memberVO.startRate;
        member.renewalDate = memberVO.renewalDate;
        member.renewalRate = memberVO.renewalRate;
        member.expirationDate = memberVO.expirationDate;
        member.avatar = memberVO.avatar;
        member.activityStatus = memberVO.activityStatus;
        member.meetingSomeone = memberVO.meetingSomeone;
        member.describeMe = memberVO.describeMe;
        member.phoneType = memberVO.phoneType;
        member.phoneType2 = memberVO.phoneType2;
        member.deleted = memberVO.deleted;
        member.hearAboutJoesClub = memberVO.hearAboutJoesClub;
        member.hearAboutJoesClubExplain = memberVO.hearAboutJoesClubExplain;
        member.whatAppealedToYou = memberVO.whatAppealedToYou;
        member.jdateExperience = memberVO.jdateExperience;
        member.biggestConcern = memberVO.biggestConcern;
        member.peopleNotice = memberVO.peopleNotice;
        member.occupation = memberVO.occupation;
        member.spendTime = memberVO.spendTime;
        member.spendTimeText = memberVO.spendTimeText;
        member.locationPref = memberVO.locationPref;
        member.timePref = memberVO.timePref;
        member.dietaryRestrictions = memberVO.dietaryRestrictions;
        member.cuisinesToAvoid = memberVO.cuisinesToAvoid;
        member.additionalNights = memberVO.additionalNights;
        member.desiredMaritalStatus = memberVO.desiredMaritalStatus;
        member.desiredEducationLevel = memberVO.desiredEducationLevel;
        member.desiredDrinkingHabits = memberVO.desiredDrinkingHabits;
        member.desiredSmokingHabits = memberVO.desiredSmokingHabits;
        member.birthdate = memberVO.birthdate;
        member.contractReceived = memberVO.contractReceived;
        member.loveMostAboutJDate = memberVO.loveMostAboutJDate;
        member.personalityQuestion1 = memberVO.personalityQuestion1;
        member.personalityQuestion2 = memberVO.personalityQuestion2;
        member.personalityQuestion3 = memberVO.personalityQuestion3;
        member.personalityQuestion4 = memberVO.personalityQuestion4;
        member.personalityQuestion5 = memberVO.personalityQuestion5;
        member.personalityQuestion6 = memberVO.personalityQuestion6;
        member.personalityQuestion7 = memberVO.personalityQuestion7;
        member.personalityQuestion8 = memberVO.personalityQuestion8;
        member.personalityQuestion9 = memberVO.personalityQuestion9;
        member.personalityQuestion10 = memberVO.personalityQuestion10;
        member.personalityQuestion11 = memberVO.personalityQuestion11;
        member.personalityQuestion12 = memberVO.personalityQuestion12;
        member.applicationRemarks = memberVO.applicationRemarks;
        member.inPersonInterviewTime = memberVO.inPersonInterviewTime;
        member.phoneInterviewNotes = memberVO.phoneInterviewNotes;
        member.interviewQuestionaireNotes = memberVO.interviewQuestionaireNotes;
        member.relationship = memberVO.relationship;
        member.whatElseToKnow = memberVO.whatElseToKnow;
        member.whenToCall = memberVO.whenToCall;
        member.applicationReceived = memberVO.applicationReceived;
        member.feesChargedText = memberVO.feesChargedText;
        member.emailOptedOut = memberVO.emailOptedOut;
        member.trxId = memberVO.trxId;
        member.guid = memberVO.guid;
        member.phoneInterviews = memberVO.phoneInterviews;
		member.interviewDiscussed = memberVO.interviewDiscussed;
		member.interviewReservations = memberVO.interviewReservations;

        if (memberVO.phoneInterviews == null)
            member.phoneInterviews = null;
        else
        {
            ArrayList pis = new ArrayList();
            foreach (PhoneInterviewVO piVO in memberVO.phoneInterviews)
                pis.Add(convert(piVO));
            member.phoneInterviews = pis;
        }
    }

    /**
     * returns a PhoneInterviewVO for the PhoneInterview
     */
    public static PhoneInterviewVO convert(PhoneInterview pi)
    {
        if (pi == null)
            return null;

        /*return new PhoneInterviewVO(
            pi.id,
            pi.memberId,
            pi.interviewStatus,
            pi.caller,
            pi.whenCalled,
            pi.gaveContract);*/
		PhoneInterviewVO newpi = new PhoneInterviewVO(
			pi.id,
			pi.memberId,
			pi.interviewStatus,
			pi.caller,
			pi.whenCalled,
			pi.gaveContract);
		newpi.interviewScheduleStatus = pi.interviewScheduleStatus;
		newpi.scheduleDate = pi.scheduleDate;
		return newpi;
    }


    /**
     * PhoneInterviewVO --> PhoneInterview
     */
    public static PhoneInterview convert(PhoneInterviewVO pi)
    {
        if (pi == null)
            return null;

        /*return new PhoneInterview(
            pi.id,
            pi.memberId,
            pi.interviewStatus,
            pi.caller,
            pi.whenCalled,
            pi.gaveContract);*/

		PhoneInterview newpi = new PhoneInterview(
			pi.id,
			pi.memberId,
			pi.interviewStatus,
			pi.caller,
			pi.whenCalled,
			pi.gaveContract);
		newpi.interviewScheduleStatus = pi.interviewScheduleStatus;
		newpi.scheduleDate = pi.scheduleDate;
		return newpi;
    }


    /**
     * Event --> EventVO
     */
    public static EventVO convert(Event ev)
    {
        if (ev == null)
            return null;

        return new EventVO(
                ev.id,
                ev.name,
                ev.getStartTime(true));
    }

    /**
     * Venue --> VenueVO
     */
    public static VenueVO convert(Venue venue)
    {
        if (venue == null)
            return null;

        VenueVO venueVO = new VenueVO(
            venue.id,
            venue.name,
            venue.address,
            venue.address2,
            venue.city,
            venue.state,
            venue.zipCode,
            venue.webSiteUrl,
            venue.image,
            venue.phone,
            venue.contactName,
            venue.contactPhone,
            venue.contactTitle,
            venue.mapUrl,
            venue.parking,
            venue.parkingFee,
            venue.priceRange,
            venue.notes,
            venue.salesCopy,
            venue.salesCopy2,
            venue.image2,
            venue.meetingLocation);

        return venueVO;
    }


    /**
     * Converts the members of an array, returning a new array
     */
    public static ArrayList convert(ArrayList array)
    {
        if (array == null)
            return null;
        ArrayList newArray = new ArrayList(array.Count);
        foreach (Object obj in array)
        {
            if (obj == null)
                newArray.Add(null);
            else
            {
                // assume the object is OK unless it's one of the types we know we need
                // to convert
                Object newObj = obj;

                if (obj is Event)
                    newObj = convert((Event) obj);
                else if (obj is PhoneInterview)
                    newObj = convert((PhoneInterview) obj);
                else if (obj is Venue)
                    newObj = convert((Venue) obj);
                else if (obj is ArrayList)
                    newObj = convert((ArrayList) obj);

                newArray.Add(newObj);


            }
        }

        return newArray;
    }

}
}
