using Matchnet.JoesClub.ValueObjects;

/**
 * Constants used by UI files
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class UiConstants : CommonUiConstants
{

    // param names
    public const string EventParam = "event";
    public const string MemberParam = "member";
    public const string VenueParam = "venue";
	public const string EmailIdParam = "emailId";

    // action names
    public const string SaveAction = "Save";
    public const string CancelAction = "Cancel";
    public const string NewAction = "New";
    public const string DeleteAction = "delete";
    public const string ShowParticipantsAction = "participants";
    public const string ComputeAction = "compute";
    public const string RefreshAction = "refresh";
    public const string CouplingAction = "coupling";
    public const string CoupleGridAction = "CoupleGrid";
    public const string TablesAction = "Tables";
	public const string SendInvitesAction = "sendInvites";
	public const string SendRejectsAction = "sendRejects";
	public const string CompleteTableAction = "completeTable";

    // MemberGrid actions
    public const string ClearAction = "Clear Couples";
    public const string CompatibilityAction = "Re-Compute Compatibility";
    public const string ClearTablesAction = "Clear Tables";
    public const string MakeTablesAction = "Make Tables";

    //  The number of columns to use when displaying a large number of member names
    public const int NumMemberColumns = 3;


}
}