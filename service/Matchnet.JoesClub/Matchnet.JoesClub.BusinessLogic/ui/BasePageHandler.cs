using System;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Xml; 
using System.Xml.Xsl;
using System.Xml.XPath;


using Matchnet.JoesClub.ValueObjects;


/**
 * Base class for Page Handlers
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class BasePageHandler 
{
    private static Log _log = new Log("BasePageHandler");

    protected PageRequest _pageRequest;
    protected NameValueCollection _queryParams;

    protected PageResponse _pageResponse;
    protected XmlHelper _xh;


    /**
     * Generates the xml for the Ajax response
     */
    protected abstract void processRequest();


    /**
     * Called before processRequest();
     */
    protected virtual void preProcessRequest() {}


    /**
     * Public method which does everything.
     */
    public PageResponse processRequest(PageRequest pageRequest) 
    {
        // request stuff
        _pageRequest = pageRequest;
        _queryParams = _pageRequest.queryParams;

        // response stuff
        _pageResponse = new PageResponse();
        _xh = new XmlHelper(new StringWriter());

        // Do necessary preparation before handling a request
        // open the log file
        try
        {
            LogFile.open();
        }
        catch (Exception e)
        {
            if (_xh == null)
                throw e;
            else
            {
                _xh.writeException(e);
                return null;
            }
        }

        // create a DbObjectCache
        DbObjectCache.create();

        try
        {
            _log.write("processRequest() pageName=" + _pageRequest.pageName);

            // process the request
            preProcessRequest();
            processRequest();

            // put the output in a PageResponse
            _pageResponse.output = _xh.textWriter.ToString();
            return _pageResponse;
        }

        catch (Exception e)
        {
            // leave last element open so we can write error xml
            _xh.cleanUp(false);
            _xh.writeException(e);
            _log.error(e);
            return null;
        }

        finally
        {
            // close xml
            if (_xh != null)
                _xh.cleanUp(true);

            // close logfile
            LogFile.close();

            // destroy the DbObjectCache
            DbObjectCache.destroy();
        }
    }


    /**
     * IHttpHandler interface method.
     */
    public bool IsReusable { get { return true; } } 


    /**
     * Just a convenient method to get an id from _queryParams.
     */
    protected int getIdParam(string param)
    {
        string sId = _queryParams.Get(param);
        if (sId == null || sId == "")
            _log.error("getIdParam() missing param: " + param);
        return int.Parse(sId);
    }


    /**
     * Just a convenient method to redirect
     */
    protected void redirect(string redirectUrl)
    {
        _pageResponse.redirectUrl = redirectUrl;
    }

}
}