
using Matchnet.JoesClub.ValueObjects;


/**
 * returns xml regarding the table a member is in for an event.
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventInfoByVenueAjax : SecurePage
{
    private static Log _log = new Log("EventInfoByVenueAjax");


    /**
     * Generates the xml for the response
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        // get the event values for the venue
        int venueId = getIdParam(UiConstants.VenueParam);
        string attire;
        string specialInstructions;
        double price;
        EventMgr.singleton.getEventInfoByVenue(
            venueId, out attire, out specialInstructions, out price);

        _xh.startElem("event", false, null);

        _xh.writeSimpleElem("attire", attire);
        _xh.writeSimpleElem("specialInstructions", specialInstructions);
        _xh.writeSimpleElem("price", Event.getPriceXmlString(price));

        _xh.endElem(); // event 
    
    
    
    }
}

}