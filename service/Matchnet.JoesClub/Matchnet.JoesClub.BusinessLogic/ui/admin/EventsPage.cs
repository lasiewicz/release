using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class EventsPage : SecurePage 
{
    private static Log _log = new Log("EventsPage");


    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        _xh.startElem("JClubs", false, null);

        // write xml for all the events
        Event.getEventsPageXml(_xh);

        _xh.endElem(); // JClubs
    }

}

}