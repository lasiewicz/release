using System;
using System.IO;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberGridPage : SecurePage 
{
    private static Log _log = new Log("MemberGridPage");


    /**
     * Generates the xml for the ShowEvent page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        _log.debug("processRequest() start");

        // get query params
        string action = _queryParams.Get(UiConstants.ActionParam);
        string sId = _queryParams.Get(UiConstants.EventParam);
        int eventId = sId == null ? DbConstants.NoId : int.Parse(sId);

        if (eventId == DbConstants.NoId)
        {
            // go back to edit event page
            redirect(UiConstants.EventsPage);
            return;
        }

        Event ev = (Event) EventMgr.singleton.load(eventId);

        // save user settings
		if (action == UiConstants.SaveAction)
			ev.saveMemberGridUserSettings(_queryParams);

			// compute coupling
		else if (action == UiConstants.MakeTablesAction) 
		{
			ev.computeMemberGridCoupling(_queryParams);
			redirect(UiConstants.TablesPage + "?event=" + sId);
			return;
		}

			// clear server coupling
		else if (action == UiConstants.ClearAction)
			ev.clearMemberGridCoupling(_queryParams);

			// re-compute compatibility
		else if (action == UiConstants.CompatibilityAction) 
			ev.computeMemberGridCompatibility();

        // write xml
        _xh.startElem("JClubs", false, new string[] { 
            "title", "Member Compatibility for Event " + eventId,
            "xmlns:html", "http://www.w3.org/1999/_xhtml",
            "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance" });

        // event
        ev.everythingToXml(_xh, UiConstants.MemberGridPage);

        // send information on the state of the controls
        bool clearCouplesButtonEnabled = ev.memberGrid.getCouples().Count > 0;
        bool makeCouplesButtonEnabled = true; //!clearCouplesButtonEnabled;
        bool showAdvancedActionsChecked = _queryParams.Get("showAdvancedActions") == "on";
        _xh.startElem("Controls", true, new string[] { 
            "clearCouplesButtonEnabled", clearCouplesButtonEnabled.ToString(),
            "makeCouplesButtonEnabled", makeCouplesButtonEnabled.ToString(),
            "recomputeCompatibilityButtonVisible", showAdvancedActionsChecked.ToString(),
            "showAdvancedActionsChecked", showAdvancedActionsChecked.ToString() });
        
        // which grid to show
        _xh.startElem("grid", false, null);
        string grid = _queryParams.Get("grid");
        if (grid == null || grid == "")
            grid = "mfGrid";
        _xh.writeText(grid);
        _xh.endElem();

        // members
        Member.toXml(_xh, ev.sortMembers(), ev);

        // output the MemberGrid
        ev.memberGrid.toXml(_xh);

        // tail
        _xh.endElem(); // </JClubs>

        _log.debug("processRequest() finish");
    }

}

}