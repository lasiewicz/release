using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailsPage : SecurePage 
{
    private static Log _log = new Log("EmailsPage");

    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

		_log.debug("processRequest() start");

		// Get the event.  if there's a problem, go back to edit event page
		Event ev = getEvent(_queryParams);
		if (ev == null)
		{
			redirect(UiConstants.EventsPage);
			return;
		}
		
		_xh.startElem("JClubs", false, null);

        // email xml
        EmailInstance.toXml(_xh, ev.id);

		// event
		ev.everythingToXml(_xh, UiConstants.TablesPage);
		
		_xh.endElem(); // JClubs

		_log.debug("processRequest() finish");
	}

	/**
	 * Load the event, does some checking
	 */
	private Event getEvent(NameValueCollection _queryParams)
	{
		string sId = _queryParams.Get(UiConstants.EventParam);
		int eventId = sId == null ? DbConstants.NoId : int.Parse(sId);
		if (eventId == DbConstants.NoId)
			return null;
		Event ev = (Event) EventMgr.singleton.load(eventId);

		// if there are no tables, we want to bail out
        // jlf I assume we don't want this:
		//if (ev.tables == null || ev.tables.Count == 0)
		//	return null;

		return ev;
	}

}

}