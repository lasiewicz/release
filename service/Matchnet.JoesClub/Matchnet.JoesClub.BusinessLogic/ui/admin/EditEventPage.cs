using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class EditEventPage : SecurePage 
{
    private static Log _log = new Log("EditEventPage");

    private Event _event;
    private string _alertText;


    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        _alertText = null;

        // load event
        string sId = _queryParams.Get(UiConstants.EventParam);
        int eventId = sId == null ? DbConstants.NoId : int.Parse(sId);
        if (eventId == DbConstants.NoId)
            _event = new Event();
        else
            _event = (Event) EventMgr.singleton.load(eventId);

        // do action
        string redirectUrl = handleAction();
        if (redirectUrl != null)
        {
            redirect(redirectUrl);
            return;
        }

        // write xml
        _xh.startElem("JClubs", false, null);

        // possibly display an alert
        if (_alertText != null)
            _xh.writeSimpleElem("alert", _alertText);

        _event.everythingToXml(_xh, UiConstants.EditEventPage);
        membersXml(_event, _xh);

        // hosts & venues
        Host.allToXml(_xh);
        Venue.allToXml(_xh);
        
        _xh.endElem(); // JClubs
    }


    /**
     * Performs the requested action.  Returns true if processRequest should
     * return.
     */
    private string handleAction()
    {
        // save
        string action = _queryParams.Get(UiConstants.ActionParam);
        _log.debug("action: " + action);

		if (action == UiConstants.DeleteAction)
		{
			EventMgr.singleton.delete(_event.id);
			return UiConstants.EventsPage;
		}

		else if (action == UiConstants.SaveAction)
		{
			// venue
			Venue venue = null;
			string sVenueId = _queryParams.Get("select_Venue");
			_log.debug("sVenueId: " + sVenueId);
			if (sVenueId == "-1")
				_event.venue = null;
			else
			{
				int venueId = int.Parse(sVenueId);
				venue = (Venue) VenueMgr.singleton.load(venueId);
				_event.venue = venue;
			}

			// host
			Host host = null;
			string sHostId = _queryParams.Get("select_Host");
			_log.debug("sHostId: " + sHostId);
			if (sHostId == "-1")
				_event.host = null;
			else
			{
				int hostId = int.Parse(sHostId);
				host = (Host) HostMgr.singleton.load(hostId);
				_event.host = host;
			}

			// startTime
			string sStartTime = _queryParams.Get("select_StartTime");
			_event.startTime = CommonConstants.DateTimeNone;
			if (sStartTime != null && sStartTime != "")
				_event.startTime = DateTime.Parse(sStartTime);            

			// endTime
			string sEndTime = _queryParams.Get("select_EndTime");
			_event.endTime = CommonConstants.DateTimeNone;
			if (sEndTime != null && sEndTime != "")
				_event.endTime = DateTime.Parse(sEndTime);            

			string name = Misc.emptyToNullString(_queryParams.Get("name"));
			string notes = Misc.emptyToNullString(_queryParams.Get("notes"));
			//string eventCopy = _queryParams.Get("eventCopy");
			string eventCopy = Misc.emptyToNullString(_queryParams.Get("eventCopy"));
			//string eventCopy = "Matt";

			// convert date string to an ArrayList[DateTime]            
			string sDate = _queryParams.Get("date");
            DateTime date = DateTime.Parse(sDate);

			// multi-venue
			string sMulti = _queryParams.Get("multiVenue");
			_event.multiVenue = sMulti != null;

			// excursion
			string sExcursion = _queryParams.Get("excursion");
			_event.excursion = sExcursion != null;
            if (!_event.excursion)
                name = null;

            _event.attire = _queryParams.Get("attire");
            _event.specialInstructions = _queryParams.Get("specialInstructions");
            _event.price = Misc.getCurrencyFromQueryParam(_queryParams, "price");

			_event.name = name;
			_event.date = date;
			_event.notes = notes;
			_event.eventCopy = eventCopy;
			EventMgr.singleton.save(_event);

			string sMembersChanged = Misc.emptyToNullString(_queryParams.Get("membersChanged"));
			bool membersChanged = sMembersChanged == "true";
			if (membersChanged)
				saveMembers();
    
			return null; //UiConstants.EditEventPage + "?event=" + _event.id;
		}

		else if (action == EventStatus.Posted.ToString())
		{
			_event.status = EventStatus.Posted;
			_event.save();
		}

		else if (action == EventStatus.Closed.ToString())
		{
			_event.status = EventStatus.Closed;
			_event.save();
		}

		else if (action == UiConstants.CancelAction)
			return UiConstants.EditEventPage + "?event=" + _event.id;

        // no redirect
        return null;
    }


    /**
     * generate xml for members
     */
    private void membersXml(Event ev, XmlHelper _xh)
    {
        _xh.startElem("members", false, null);

        ArrayList allMembers = MemberMgr.singleton.loadAll(true);
        // allMembers = Misc.orderByColumns(allMembers, UiConstants.NumMemberColumns);

        Hashtable memberInLockedTables = new Hashtable();
        ev.getCouplesAndMembersInLockedTables(memberInLockedTables, null);

        foreach (Member member in allMembers)
            if (member.isActive())
                member.toXml(_xh, Member.MemberXmlStyle.Simple, ev, 
                    memberInLockedTables[member] != null);

        _xh.endElem(); // members
    }


    /**
     * updates the event members
     */
    private void saveMembers()
    {
        _log.debug("save()");

        // find the members
        ArrayList members = new ArrayList();

        // check the checkbox for all members
        int maleCount = 0;
        int femaleCount = 0;
        foreach (Member member in MemberMgr.singleton.loadAll())
        {
            string sInEvent = _queryParams.Get("member_" + member.id);
            bool inEvent = sInEvent != null;
            if (inEvent)
            {
                members.Add(member);
                if (member.gender == CommonConstants.Male)
                    ++maleCount;
                else
                    ++femaleCount;
            }
        }

        _alertText = _event.setMembers(members);
    }
}

}