using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class CompExplPage : SecurePage 
{
    private static Log _log = new Log("CompExplPage");


    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        // load event
        string sEventId = _queryParams.Get(UiConstants.EventParam);
        string sMemberId1 = _queryParams.Get("member1");
        string sMemberId2 = _queryParams.Get("member2");

        int eventId = int.Parse(sEventId);
        int memberId1 = int.Parse(sMemberId1);
        int memberId2 = int.Parse(sMemberId2);

        // get the explanation and write to the response
        string explanation = MemberGridCellMgr.singleton.loadExplanation(
            eventId, memberId1, memberId2);

        if (explanation == null)
            explanation = RuleContext.simpleExplanation("No info available.");
        _xh.textWriter.Write(explanation);
        //_httpContext.Response.Write(explanation);
    }
}

}