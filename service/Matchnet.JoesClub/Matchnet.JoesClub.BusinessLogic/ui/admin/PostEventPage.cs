using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;

using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class PostEventPage : SecurePage 
{
    private static Log _log = new Log("PostEventPage");

    private Event _event;

    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        // temp
		//_log.logObject("_queryParams", _queryParams, Log.iAlways);

        _log.debug("processRequest() start");

		// Get the event.  if there's a problem, go back to edit event page
		_event = getEvent(_queryParams);
		if (_event == null)
		{
			redirect(UiConstants.EventsPage);
			return;
		}

		string action = _queryParams.Get(UiConstants.ActionParam);

		// save user settings
		if (action != null && action.Equals("Save"))
			saveMemberAttendance();
		else if (action != null && action.Equals("SendEmails"))
			sendEmails();
		
		_xh.startElem("JClubs", false, null);

		// event
		_event.everythingToXml(_xh, UiConstants.TablesPage);

		// <members>
		Member.toXml(_xh, _event.members(true), _event);

		// <couples>
		_event.memberGrid.couplesToXml(_xh);

		// <tables>
		ArrayList tables = _event.tables;
		if (tables != null) 
		{
			// <tables>
			_xh.startElem("tables", false, null);
			foreach (Table table in tables) 
			{
				if (table.locked)
					table.toXml(_xh);
			}
			_xh.endElem(); // </tables>
		}
		
		// venue and host xml
		Venue.allToXml(_xh);
		Host.allToXml(_xh);

        fineXml();

		_xh.endElem(); // JClubs

		_log.debug("processRequest() finish");
	}

	/**
	 * Load the event, does some checking
	 */
	private static Event getEvent(NameValueCollection _queryParams)
	{
		string sId = _queryParams.Get(UiConstants.EventParam);
		int eventId = sId == null ? DbConstants.NoId : int.Parse(sId);
		if (eventId == DbConstants.NoId)
			return null;
		Event ev = (Event) EventMgr.singleton.load(eventId);

		// if there are no tables, we want to bail out
		if (ev.tables == null || ev.tables.Count == 0)
			return null;

		return ev;
	}

	private void saveMemberAttendance()
	{
        // if the event status hasn't been set to finished yet, compute
        // the fines for evenyone
        if (_event.status != EventStatus.Finished)
        {
		    _event.status = EventStatus.Finished;
            _event.finishedTime = DateTime.Now;
		    _event.save();
            _event.computeFines();
        }

        // save fines
        foreach (EventMember em in _event.eventMembers)
        {
            int memberId = em.member.id;
            string sAmountCharged = Misc.emptyToNullString(_queryParams.Get("amountCharged_" +  memberId));
            double amountCharged = CommonConstants.NoValueDouble;
            if (sAmountCharged != null)
                amountCharged = double.Parse(
                    sAmountCharged, NumberStyles.Currency, null);

            string fineTrxId = Misc.emptyToNullString(_queryParams.Get("fineTrxId_" +  memberId));
            
            if (em.amountCharged != amountCharged || 
                em.fineTrxId != fineTrxId)
            {
                em.amountCharged = amountCharged;
                em.fineTrxId = fineTrxId;
                em.save();
            }
        }

        // save statuses
		string prefix = "radio_attended_mem_";
		ArrayList tables = _event.tables;
		for (int i = 0; i < tables.Count; i++) 
		{
			Table t = (Table)tables[i];
			if (!t.locked)
				continue;

			ArrayList members = t.members;
			for (int m = 0; m < members.Count; m++) 
			{
				Member member = (Member)members[m];
				string param = prefix + member.id;
				string val = _queryParams.Get(param);

				MemberStatus status = "yes".Equals(val)? MemberStatus.Attended : MemberStatus.NoShow;
				EventMemberMgr.singleton.updateMemberStatus(_event, member, status);
			}
		}
	}


    /**
     * Generate the XML for the fines
     */
	private void fineXml()
	{
        _xh.startElem("Fines", false, null);
        foreach (EventMember em in _event.eventMembers)
            em.fineXml(_xh);
        _xh.endElem(); // Fines
	}


    /**
     * Sends the AfterDinner and NoShow emails
     */
	private void sendEmails()
	{
        EmailInstance afterDinnerEmail = AfterDinner.createEmailInstance(_event);
        EmailInstance noShowEmail = NoShow.createEmailInstance(_event);

        afterDinnerEmail.send();
        noShowEmail.send();
	}
}

}