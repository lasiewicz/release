using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class EmailDetailsPage : SecurePage 
{
    private static Log _log = new Log("EmailDetailsPage");


    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

		_log.debug("processRequest() start");
        //_log.logObject("_queryParams", _queryParams, Log.iAlways);

		// Get the event.  if there's a problem, go back to edit event page
		Event ev = getEvent(_queryParams);
		if (ev == null)
		{
            redirect(UiConstants.EventsPage);
			return;
		}

		string sEmailId = _queryParams.Get(UiConstants.EmailIdParam);
		if (sEmailId == null)
		{
            redirect(UiConstants.EmailsPage);
			return;
		}

		int emailId = int.Parse(sEmailId);
		EmailInstance ei = (EmailInstance) EmailInstanceMgr.singleton.load(emailId);

		bool editable = false;
		string sActionParam = _queryParams.Get(UiConstants.ActionParam);
		if (sActionParam != null && sActionParam.Equals("edit"))
		{
			editable = true;
		}
		else if (sActionParam != null && sActionParam.Equals("send"))
		{
            sendEmail(ei, ev);
		}
		
		else if (sActionParam != null && sActionParam.Equals("cancel"))
		{
            // delete the EmailInstance
            EmailInstanceMgr.singleton.delete(ei.id);

            // redirect to tables page
            redirect(UiConstants.TablesPage + "?" + UiConstants.EventParam + "=" + ev.id);
			return;
		}
		
		_xh.startElem("JClubs", false, null);
		_xh.writeSimpleElem("editable", editable);

        // email instance xml
		ei.toXml(_xh, true);

		// event
		ev.everythingToXml(_xh, UiConstants.TablesPage);

        // event members
        eventMembersXml(ev);
		
		_xh.endElem(); // JClubs

		_log.debug("processRequest() finish");
	}

	/**
	 * Load the event, does some checking
	 */
	private Event getEvent(NameValueCollection _queryParams)
	{
		string sId = _queryParams.Get(UiConstants.EventParam);
		int eventId = sId == null ? DbConstants.NoId : int.Parse(sId);
		if (eventId == DbConstants.NoId)
			return null;
		Event ev = (Event) EventMgr.singleton.load(eventId);

		// if there are no tables, we want to bail out
        // jlf I assume we don't want this:
		//if (ev.tables == null || ev.tables.Count == 0)
		//	return null;

		return ev;
	}


	/**
	 * Generate some xml for each event member
	 */
	private void eventMembersXml(Event ev)
	{
		_xh.startElem("Members", false, null);

        foreach (EventMember eventMember in ev.eventMembers)
            if (eventMember.inEvent)
            {
                Member member = eventMember.member;
		        _xh.startElem("Member", false, null);
                _xh.writeSimpleElem("memberId", member.id);
                _xh.writeSimpleElem("firstName", member.firstName);
                _xh.writeSimpleElem("lastName", member.lastName);
		        _xh.endElem(); // Member
            }

		_xh.endElem(); // Members
	}


	/**
	 * Sends the email
	 */
	private void sendEmail(EmailInstance ei, Event ev)
	{
        // get the recipient ids
        string sRecipIds = _queryParams.Get("recipIds");
        string[] sRecipIdsArray = sRecipIds.Split(new char[] {','});

        ArrayList recipientMembers = new ArrayList();
        foreach (string sRecipId in sRecipIdsArray)
        {
            if (sRecipId == "")
                continue;
            int memberId = int.Parse(sRecipId);
            Member member = ev.getMember(memberId);
            recipientMembers.Add(member);
        }

        ei.removeMissingRecipients(recipientMembers);
        ei.send();
	}

}

}