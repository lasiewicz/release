using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.Web;

using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class AlertsPage : SecurePage 
{
    private static Log _log = new Log("AlertsPage");

    private static SqlParameter _eventStatusNewParam = DbUtils.createIntParam("@eventStatusNew", (int) EventStatus.New);
    private static SqlParameter _eventStatusFinishedParam = DbUtils.createIntParam("@eventStatusFinished", (int) EventStatus.Finished);
    private static SqlParameter _eventStatusCancelledParam = DbUtils.createIntParam("@eventStatusCancelled", (int) EventStatus.Cancelled);
    private static SqlParameter _invitationEmailDefParam = DbUtils.createStringParam("@invitationEmailDef", EmailConstants.Invitation);
    private static SqlParameter _eventFullEmailDefParam = DbUtils.createStringParam("@eventFullEmailDef", EmailConstants.EventFull);
    private static SqlParameter _memberStatusCancelledParam = DbUtils.createIntParam("@memberStatusCancelled", (int) MemberStatus.Cancelled);
    private static SqlParameter _memberStatusRsvpedNoParam = DbUtils.createIntParam("@memberStatusRsvpedNo", (int) MemberStatus.RsvpedNo);

    // how many hours to display things that have completed
    private static int _completedHours = 24;

    private static string _nudge1Tag = "Nudge1";
    private static string _fineTag = "Fine";
    
    private Hashtable _openEvents; // Hashtable[Event.id --> Event]

    // All EventMembers in open events
    // Hashtable[EventMember.id --> EventMember]
    private Hashtable _eventMembers; 

    // keep track of all the events and members used on the page
    private Hashtable _usedEvents; // Hashtable[Event]
    private Hashtable _usedMembers; // Hashtable[Member]


    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.AlertsPage);
		if (!_adminAuthenticated)
			return;

        // load open events and EventMembers
        getEvents();

		// save user settings
		if (_queryParams.Get(UiConstants.ActionParam) == UiConstants.SaveAction)
            save();

        _usedEvents = new Hashtable();
        _usedMembers = new Hashtable();


        ArrayList cancelledEventMembers = getCancelledEventMembers(); // ArrayList[EventMember]
        ArrayList unpublishedEvents = getUnpublishedEvents(); // ArrayList[Event]
        ArrayList needInvites = getNeedInvites(); // ArrayList[Event]
        ArrayList needEventFulls = getNeedEventFulls(); // ArrayList[Event]
        ArrayList needsNudge1 = getNeedsNudge1(); // ArrayList[EventMember]
        ArrayList gotNudge1 = getGotNudge1(); // ArrayList[EventMember]
        ArrayList needsFine = getAlertFinesEventMembers(false); // ArrayList[EventMember]
        ArrayList gotFine = getAlertFinesEventMembers(true); // ArrayList[EventMember]

        // generate XML
        _xh.startJClubs();

        // output info for each event
        foreach (Event ev in _usedEvents.Keys)
            writeEventXml(ev);

        // output info for each member
        foreach (Member member in _usedMembers.Keys)
            member.toXml(_xh, Member.MemberXmlStyle.AlertsPage);

        writeEventMemberElems(cancelledEventMembers, "CancelledEventMember");
        writeEventElems(needInvites, "NeedsInvite");
        writeEventElems(needEventFulls, "NeedsEventFull");
        writeEventElems(unpublishedEvents, "UnpublishedEvent");
        writeNudge1Elems(needsNudge1, gotNudge1);
        writeFineElems(needsFine, gotFine);
        
        _xh.endJClubs();
	}

    
    /**
     * Returns the set of events that aren't finished or cancelled.
     * Populates _openEvents and _eventMembers
     */
    private void getEvents() 
    {
        // load the events
        /*
        SqlParameter[] sqlParameters = new SqlParameter[] {
            _eventStatusFinishedParam,
            _eventStatusCancelledParam };

        ArrayList events = EventMgr.singleton.load("getEvents", sqlParameters);
        */
        ArrayList events = EventMgr.singleton.loadAll(false, true);

        // put in hashtables
        _openEvents = new Hashtable();
        _eventMembers = new Hashtable();
        foreach (Event ev in events)
        {
            _openEvents[ev.id] = ev;

            foreach (EventMember eventMember in ev.eventMembers)
                _eventMembers[eventMember.id] = eventMember;
        }
	}


    /**
     * Returns the EventMembers that are in locked tables and have cancelled.
     * returns ArrayList[EventMember]
     */
    private ArrayList getCancelledEventMembers() 
    {
        ArrayList eventMembers = new ArrayList();

        // find the cancelled EventMembers
        Hashtable cancelledEventMembers = getAlertEventMembers("CancelledEventMembers");
        foreach (int eventMemberId in cancelledEventMembers.Keys)
        {
            EventMember eventMember = (EventMember) _eventMembers[eventMemberId];
            addEventMember(eventMember, eventMembers);
        }

        return eventMembers;
	}


    /**
     * Returns the set of unpublished events
     * returns ArrayList[Event]
     */
    private ArrayList getUnpublishedEvents() 
    {
        ArrayList unpublishedEvents = new ArrayList();
        foreach (Event ev in _openEvents.Values)
            if (ev.status == EventStatus.New)
                addEvent(ev, unpublishedEvents);
        return unpublishedEvents;
	}


    /**
     * Returns the Events that have people who need invitations
     * returns ArrayList[Event]
     */
    private ArrayList getNeedInvites() 
    {
        Hashtable hEvents = new Hashtable();

        // find the people in locked tables who haven't received Invitations
        Hashtable inLockedTables = getAlertEventMembers("InLockedTables");
        Hashtable receivedInvitation = getAlertEventMembers("ReceivedInvitation");
        foreach (int eventMemberId in inLockedTables.Keys)
            if (!receivedInvitation.ContainsKey(eventMemberId))
            {
                EventMember eventMember = (EventMember) _eventMembers[eventMemberId];
                hEvents[eventMember.ev] = "";
                _usedEvents[eventMember.ev] = "";
            }
        
        return Misc.hashtableToArrayList(hEvents);
	}


    /**
     * Returns the EventMembers that are in locked tables but have not
     * been sent invitations.
     * returns ArrayList[Event]
     */
    private ArrayList getNeedEventFulls() 
    {
        Hashtable hEvents = new Hashtable();

        // find the people 
        //   - not in locked tables 
        //   - haven't received EventFull emails
        //   - signed up for the event
        Hashtable inLockedTables = getAlertEventMembers("InLockedTables");
        Hashtable receivedEventFull = getAlertEventMembers("ReceivedEventFull");
        foreach (int eventMemberId in _eventMembers.Keys)
        {
            EventMember eventMember = (EventMember) _eventMembers[eventMemberId];
            if (!inLockedTables.ContainsKey(eventMemberId) &&
                !receivedEventFull.ContainsKey(eventMemberId) &&
                eventMember.memberStatus == MemberStatus.SignedUp)
            {
                hEvents[eventMember.ev] = "";
                _usedEvents[eventMember.ev] = "";
            }
        }
        
        return Misc.hashtableToArrayList(hEvents);
	}


    /**
     * Returns the EventMembers who got nudge1 phone calls within the last day
     * returns ArrayList[EventMember]
     */
    private ArrayList getGotNudge1() 
    {
        Hashtable hGotNudge1 = getAlertEventMembers("GotNudge1");

        // create ArrayList
        ArrayList eventMembers = new ArrayList();
        foreach (int eventMemberId in hGotNudge1.Keys)
        {
            EventMember eventMember = (EventMember) _eventMembers[eventMemberId];
            addEventMember(eventMember, eventMembers);
        }

        return eventMembers;
	}


    /**
     * Returns the EventMembers who need nudge phone calls
     * returns ArrayList[EventMember]
     */
    private ArrayList getNeedsNudge1() 
    {
        DateTime nudge1Time = DateTime.Now.Subtract(EmailConfig.singleton.getNudge1TimeSpan());
        DateTime nudge2Time = DateTime.Now.Subtract(EmailConfig.singleton.getNudge2TimeSpan());

        SqlConnection conn = null;
        SqlCommand command = null;
        SqlDataReader reader = null;

        try
        {
            conn = DbUtils.getConnection();
            command = DbUtils.getStoredProcCommand(conn, "GetNudgeMembers");
            command.Parameters.Add(DbUtils.createDateTimeParam("@nudge1Time", nudge1Time));
            command.Parameters.Add(DbUtils.createDateTimeParam("@nudge2Time", nudge2Time));
            command.Parameters.Add(DbUtils.createBoolParam("@nudge1", true));

            // read the results
            reader = command.ExecuteReader();

            // create ArrayList
            ArrayList eventMembers = new ArrayList();
            while (reader.Read())
            {
                int eventMemberId = DbUtils.getIdFromReader(reader, "eventMemberId");
                EventMember eventMember = (EventMember) _eventMembers[eventMemberId];
                addEventMember(eventMember, eventMembers);
            }
            
            return eventMembers;
        }

        finally
        {
            DbUtils.cleanUp(ref conn, ref command, ref reader);
        }
	}

    
    /**
     * Returns the EventMembers from GetAlertEventMembers
     * Returns Hashtable[eventMember.id]
     */
    private Hashtable getAlertEventMembers(string query) 
    {
        SqlConnection conn = null;
        SqlCommand command = null;
        SqlDataReader reader = null;

        try
        {
            // call the stored proc
            conn = DbUtils.getConnection();
            command = DbUtils.getStoredProcCommand(conn, "GetAlertEventMembers");

            command.Parameters.Add(DbUtils.createStringParam("@query", query));
            command.Parameters.Add(_eventStatusNewParam);
            command.Parameters.Add(_eventStatusFinishedParam);
            command.Parameters.Add(_eventStatusCancelledParam);
            command.Parameters.Add(_memberStatusCancelledParam);
            command.Parameters.Add(_memberStatusRsvpedNoParam);
            command.Parameters.Add(_invitationEmailDefParam);
            command.Parameters.Add(_eventFullEmailDefParam);

            // show people who got nudge1 within the last day
            DateTime nudge1Time = DateTime.Now.AddHours(-1 * _completedHours);
            command.Parameters.Add(DbUtils.createDateTimeParam("@nudge1Time", nudge1Time));

            reader = command.ExecuteReader();

            // create Hashtable
            Hashtable eventMembers = new Hashtable();
            while (reader.Read())
            {
                int eventMemberId = DbUtils.getIdFromReader(reader, "eventMemberId");
                eventMembers[eventMemberId] = "";
            }

            return eventMembers;
        }

        finally
        {
            DbUtils.cleanUp(ref conn, ref command, ref reader);
        }
    }

    
    /**
     * Returns the EventMembers from getAlertFinesEventMembers
     * returns ArrayList[EventMember]
     */
    private ArrayList getAlertFinesEventMembers(bool billedForFine) 
    {
        SqlConnection conn = null;
        SqlCommand command = null;
        SqlDataReader reader = null;

        try
        {
            // call the stored proc
            conn = DbUtils.getConnection();
            command = DbUtils.getStoredProcCommand(conn, "getAlertFinesEventMembers");
            command.Parameters.Add(DbUtils.createBoolParam("@billedForFine", billedForFine));
            DateTime billedTime = DateTime.Now.AddHours(-1 * _completedHours);
            command.Parameters.Add(DbUtils.createDateTimeParam("@billedTime", billedTime));

            reader = command.ExecuteReader();

            // create ArrayList
            ArrayList eventMembers = new ArrayList();
            while (reader.Read())
            {
                int eventMemberId = DbUtils.getIdFromReader(reader, "eventMemberId");
                EventMember eventMember = (EventMember) _eventMembers[eventMemberId];
                addEventMember(eventMember, eventMembers);
            }
            
            return eventMembers;
        }

        finally
        {
            DbUtils.cleanUp(ref conn, ref command, ref reader);
        }
    }


    /**
     * Adds the event to the array and adds it to _usedEvents
     */
    private void addEvent(Event ev, ArrayList eventSet) 
    {
        eventSet.Add(ev);
        _usedEvents[ev] = "";
	}


    /**
     * Adds the EventMember to the array and adds to _usedEvents
     * and _usedMembers
     */
    private void addEventMember(EventMember eventMember, ArrayList eventMemberSet) 
    {
        eventMemberSet.Add(eventMember);
        _usedEvents[eventMember.ev] = "";
        _usedMembers[eventMember.member] = "";
	}



    /**
     * Write xml elems specifying EventMembers
     * eventMembers = ArrayList[EventMember]
     */
    private void writeEventMemberElems(ArrayList eventMembers, string tag) 
    {
        foreach (EventMember eventMember in eventMembers)
        {
            string[] attrs = new string[] {
                "eventId", eventMember.ev.id.ToString(),
                "memberId", eventMember.member.id.ToString()};
            _xh.startElem(tag, true, attrs);
        }
	}


    /**
     * Write xml elems specifying Events
     * events = ArrayList[Event]
     */
    private void writeEventElems(ArrayList events, string tag) 
    {
        foreach (Event ev in events)
        {
            string[] attrs = new string[] {"eventId", ev.id.ToString()};
            _xh.startElem(tag, true, attrs);
        }
	}
        


    /**
     * generate the xml for an event
     */
    private void writeEventXml(Event ev)
    {
        _xh.startElem("event", false, null);

        _xh.writeSimpleElem("id", ev.id.ToString());
        _xh.writeSimpleElem("date", ev.date, "d");
        _xh.writeSimpleElem("startTime", ev.getStartTime(true), "t");

        string venueName = "";
        if (ev.venue != null)
            venueName = ev.venue.name;
        _xh.writeSimpleElem("venue", venueName);

        _xh.endElem(); // event
    }


    /**
     * Write xml elems for the Nudge1 phone calls
     * in/completeEventMembers = ArrayList[EventMember]
     */
    private void writeNudge1Elems(
        ArrayList incompleteEventMembers, 
        ArrayList completeEventMembers)
    {
        foreach (EventMember eventMember in incompleteEventMembers)
        {
            string[] attrs = new string[] {
                "eventId", eventMember.ev.id.ToString(),
                "memberId", eventMember.member.id.ToString(),
                "completed", "false"};
            _xh.startElem(_nudge1Tag, true, attrs);
        }

        foreach (EventMember eventMember in completeEventMembers)
        {
            string[] attrs = new string[] {
                "eventId", eventMember.ev.id.ToString(),
                "memberId", eventMember.member.id.ToString(),
                "completed", "true"};
            _xh.startElem(_nudge1Tag, true, attrs);
        }
	}


    /**
     * Write xml elems for the Nudge1 phone calls
     * in/completeEventMembers = ArrayList[EventMember]
     */
    private void writeFineElems(
        ArrayList incompleteEventMembers, 
        ArrayList completeEventMembers)
    {
        foreach (EventMember eventMember in incompleteEventMembers)
        {
            string[] attrs = new string[] {
                "eventId", eventMember.ev.id.ToString(),
                "memberId", eventMember.member.id.ToString(),
                "amountCharged", eventMember.amountCharged.ToString("C"),
                "completed", "false"};
            _xh.startElem(_fineTag, true, attrs);
        }

        foreach (EventMember eventMember in completeEventMembers)
        {
            string[] attrs = new string[] {
                "eventId", eventMember.ev.id.ToString(),
                "memberId", eventMember.member.id.ToString(),
                "amountCharged", eventMember.amountCharged.ToString("C"),
                "completed", "true"};
            _xh.startElem(_fineTag, true, attrs);
        }
	}

    /**
     * Save changes from http post.  Save nudge1 and fine completion
     */
    private void save()
    {
//        _log.logObject("_queryParams", _queryParams, Log.iAlways);

        foreach (EventMember eventMember in _eventMembers.Values)
        {
            bool dirty = false;
            string sIds = "_" + eventMember.ev.id + "_" + eventMember.member.id;
            string sNudge1 = Misc.emptyToNullString(_queryParams.Get(_nudge1Tag + sIds));
            string sBilledForFine = Misc.emptyToNullString(_queryParams.Get(_fineTag + sIds));
            
            if (sNudge1 != null)
            {
                bool gotNudge1 = (sNudge1.ToLower().Trim() == "true");
                if (gotNudge1 != eventMember.gotNudge1)
                {
                    eventMember.gotNudge1 = gotNudge1;
                    eventMember.nudge1Time = gotNudge1 ? DateTime.Now : CommonConstants.DateTimeNone;
                    dirty = true;
                }
            }
                    
            if (sBilledForFine != null)
            {
                bool billedForFine = (sBilledForFine.ToLower().Trim() == "true");
                if (billedForFine != eventMember.billedForFine)
                {
                    eventMember.billedForFine = billedForFine;
                    eventMember.billedTime = billedForFine ? DateTime.Now : CommonConstants.DateTimeNone;
                    dirty = true;
                }
            }
                    
            if (dirty)
                eventMember.save();
        }

    }
}

}