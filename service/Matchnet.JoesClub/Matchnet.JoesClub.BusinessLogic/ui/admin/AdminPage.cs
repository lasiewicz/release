using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class AdminPage : SecurePage 
{
    private static Log _log = new Log("AdminPage");


    /**
     * Generates the xml for the ShowEvent page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.AdminPage);
		if (!_adminAuthenticated)
			return;

        _log.debug("processRequest()");

        // get query params
        string action = _queryParams.Get(UiConstants.ActionParam);

        if (action == UiConstants.RefreshAction)
        {
            RuleSetConfig.clear();
            LogFile.reset();
        }

        // write content
        _xh.startHtmlPage("Admin");


        _xh.startElem("h1", false, null);
        _xh.writeText("config.xml properties");
        _xh.endElem(); // </h1>

        _xh.startElem("table", false, new string[]{"border", "1"});

        _xh.startElem("tr", false, null);
        _xh.startElem("td", false, null);
        _xh.startElem("b", false, null);
        _xh.writeText("Name");
        _xh.endElem(); // </b>
        _xh.endElem(); // </td>
        _xh.startElem("td", false, null);
        _xh.startElem("b", false, null);
        _xh.writeText("Value");
        _xh.endElem(); // </b>
        _xh.endElem(); // </td>
        _xh.endElem(); // </tr>

        foreach (string key in ConfigurationSettings.AppSettings.Keys)
        {
            _xh.startElem("tr", false, null);
            _xh.startElem("td", false, null);
            _xh.writeText(key.ToString());
            _xh.endElem(); // </td>
            _xh.startElem("td", false, null);
            _xh.writeText(ConfigurationSettings.AppSettings[key].ToString());
            _xh.endElem(); // </td>
            _xh.endElem(); // </tr>
        }

        _xh.endElem(); // </table>

        _xh.startElem("form", false, new string[] { 
            "method", "get",
            "action", UiConstants.AdminPage });

        _xh.submitButton(UiConstants.RefreshAction);
        _xh.endElem(); // </form>


        _xh.endHtmlPage();
    }


}

}