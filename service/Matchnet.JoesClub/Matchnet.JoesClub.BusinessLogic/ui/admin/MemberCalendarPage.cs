using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class MemberCalendarPage : SecurePage
{
    private static Log _log = new Log("MemberCalendarPage");

    
    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        // add the memberId and a guid to the db
        int memberId = int.Parse(_queryParams.Get("memberId"));
        string guid =  LogonMgr.Instance.addMemberGuidToDb(memberId);

        // send the guid to the UX Logon page
		redirect(Config.getProperty("UxUrl") + "/Logon.aspx?action=authenticateGuid&guid=" + guid);
    }

}

}