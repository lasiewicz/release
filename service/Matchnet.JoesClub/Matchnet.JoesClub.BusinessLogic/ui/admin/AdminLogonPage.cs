using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;

using Matchnet.JoesClub.ValueObjects;

/**
 * UserCalendar Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class AdminLogonPage : BasePageHandler
{
    private static Log _log = new Log("AdminLogonPage");

    private static string _htmlPage = "AdminLogon.htm";

    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
        string action = _queryParams.Get(UiConstants.ActionParam);
        bool authenticated = false;

        // authenticate 
        if (action == "authenticate")
        {
            AuthenticationStatus authStatus;
			string emailAddress = _queryParams.Get("emailAddress");
			string password = _queryParams.Get("password");

            authStatus = AdminLogonMgr.Instance.logOn(_pageResponse, emailAddress, password);

			if (AuthenticationStatus.Authenticated == authStatus) 
				authenticated = true;
			else
			{
				int errorCode = (int)authStatus;
				redirect(_htmlPage + "?errorCode=" + errorCode);
				return;
			}
        }

        // logout
        else if (action == "logout")
        {
            AdminLogonMgr.Instance.logOff(_pageResponse);
			redirect(_htmlPage);
			return;
        }

        // check for redirect.  Only redirect if logon was successful.
        string redirectUrl = Misc.nullToEmptyString(_queryParams.Get("redirect"));
        if (authenticated && redirectUrl != "")
        {
            redirect(redirectUrl);
            return;
        }

        if (!authenticated)
        {
			redirect(_htmlPage + "?redirect=" + redirectUrl);
			return;
        }
        else // default redirect when authenticated
        {
			redirect(UiConstants.IndexPage);
			return;
		}
    }
}

}