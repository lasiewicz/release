using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class JsRequest : SecurePage 
{
    private static Log _log = new Log("JsRequest");

    static int _count = 0;


    /**
     * Generates the xml for the ShowEvent page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        _log.debug("processRequest() start");

        string responseJs = null;

        // get query params
        string action = _queryParams.Get(UiConstants.ActionParam);
        if (action == "setTableGrade")
        {

            // load the event
            int eventId = getIdParam("event");
            Event ev = (Event) EventMgr.singleton.load(eventId);

            // get the members
            string sNumMembers = _queryParams.Get("numMembers");
            int numMembers = int.Parse(sNumMembers);
            ArrayList members = new ArrayList();
            for (int i = 0; i < numMembers; i++)
            {
                int memberId = getIdParam("memberId" + (i + 1));
                members.Add(ev.getMember(memberId));
            }
 
            Compatibility comp = Compatibility.getMembersCompatibility(members, ev);
            string sComp = comp.grade;

            string sTableId = _queryParams.Get("table");
            responseJs = "setTableGrade(\"" + sTableId + "\", \"" + sComp + "\")";
        }

        else
        {
            responseJs = "setDebugDiv(\"" + _count + "\")";
        }

        _xh.textWriter.WriteLine(responseJs);
        ++_count;
    }
}
}