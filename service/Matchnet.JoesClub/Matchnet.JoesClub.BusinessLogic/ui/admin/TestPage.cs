using System.Web;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;


/**
 * This is just used to test misc stuff
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class TestPage : SecurePage 
{
    private static Log _log = new Log("TestPage");

    /**
     * Entry point
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.TestPage);
		if (!_adminAuthenticated)
			return;

        int test = getIntParam("test");
        if (test == 1)
            test1();
        else if (test == 2)
            test2();
        else if (test == 3)
            test3();

        _xh.textWriter.Write("done.");
        _log.debug("finish: " + DateTime.Now);
        _log.info("processRequest finished");
    }
    
    private void test3() 
    {
        // load and save events to fix start times
        ArrayList events = EventMgr.singleton.loadAll();
        foreach (Event ev in events)
            ev.save();
        _log.write("test3() finished");
    }
    
    private void test2() 
    {
        // email --> memberId
        string email = getStringParam("email");
        int memberId = JDateMemberUtil.Instance.getSparkId(email);
        Matchnet.Member.ServiceAdapters.Member member = JDateMemberUtil.Instance.getMember(memberId);
        int regionID = member.GetAttributeInt(JDateMemberUtil.jdateBrand, "regionid");

        int languageID = JDateMemberUtil.jdateBrand.Site.LanguageID;
        RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);
    
        log("regionLanguage.CityName: " + regionLanguage.CityName);
        log("regionLanguage.CityName: " + regionLanguage.PostalCode);
        log("regionLanguage.CityName: " + regionLanguage.StateAbbreviation);
    }
    
    private void test1() 
    {
        ArrayList members = MemberMgr.singleton.loadAll();
        foreach (Member member in members)
        {
            if (member.jdateMember != null)
            {
                Member tempMember = Member.create(member.sparkId);
                JDateMemberUtil.Instance.getJDateValues(tempMember);
                member.city = tempMember.city;
                member.state = tempMember.state;
                member.zipCode = tempMember.zipCode;
                member.save();
            }
        }
    }


    #region helper functions
    /**
     * get an int param
     */
    private int getIntParam(string param) 
    {
        string sValue = Misc.emptyToNullString(_queryParams.Get(param));
        if (sValue == null)
            return CommonConstants.NoValueInt;

        return int.Parse(sValue);
    }


    /**
     * get a string param
     */
    private string getStringParam(string param) 
    {
        return Misc.emptyToNullString(_queryParams.Get(param));
    }


    /**
     * write an object to the log
     */
    private void log(string label, object obj) 
    {
        _log.logObject(label, obj, Log.iAlways);
    }


    /**
     * write a string to the log
     */
    private void log(string msg) 
    {
        _log.write(msg);
    }


    /**
     * write a string to the log
     */
    private void log(string label, AuthenticationResult ar) 
    {
        log("ar.MemberID: " + ar.MemberID);
        log("ar.Status: " + ar.Status);
        log("ar.UserName: " + ar.UserName);
    }

    #endregion helper functions

    
}

}