using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using Matchnet.JoesClub.ValueObjects;



namespace Matchnet.JoesClub.BusinessLogic
{
public class TablesPage : SecurePage 
{
    private static Log _log = new Log("TablesPage");


    /**
     * Generates the xml for the ShowEvent page
     */
    protected override void processRequest() 
    {
		// Make sure an admin is logged in 
		checkAdmin(UiConstants.EventsPage);
		if (!_adminAuthenticated)
			return;

        _log.debug("processRequest() start");

        // get query params
        string action = _queryParams.Get(UiConstants.ActionParam);
        string warning = null;

        // Get the event.  if there's a problem, go back to edit event page
        Event ev = getEvent(_queryParams);
        if (ev == null)
        {
            redirect(UiConstants.EventsPage);
            return;
        }

        // save user settings
		if ((_queryParams.Get("saveBtn.x") != null) ||
			(action != null && action.Equals("Save")))
		{
			ev.saveTableUserSettings(_queryParams);
		}
		else if (action != null && action.Equals(UiConstants.SendInvitesAction))
		{
			int emailId = ev.sendInvitations();

			//_httpContext.Response.BufferOutput = true;
			string url = UiConstants.EmailDetailsPage + "?" + UiConstants.EventParam + "=" + ev.id + "&"
				+ UiConstants.EmailIdParam + "=" + emailId + "&"
				+ UiConstants.ActionParam + "=edit";
            redirect(url);
			return;
		}
		else if (action != null && action.Equals(UiConstants.SendRejectsAction))
		{
			int emailId = ev.sendRejections();

			//_httpContext.Response.BufferOutput = true;
			string url = UiConstants.EmailDetailsPage + "?" + UiConstants.EventParam + "=" + ev.id + "&"
				+ UiConstants.EmailIdParam + "=" + emailId + "&"
				+ UiConstants.ActionParam + "=edit";
            redirect(url);
			return;
		}
		else if (action == UiConstants.CompleteTableAction)
		{
			completeTable(ev);
		}
		else if (action == "changeMemberStatus")
		{
			// get the Member
			string sMemberId = _queryParams.Get(UiConstants.MemberParam);
			Member mem = null;
			int memberId = int.Parse(sMemberId);
			mem = (Member) MemberMgr.singleton.load(memberId);

			// MemberStatus is in the action param
			string status = _queryParams.Get("status");
			MemberStatus memberStatus = (MemberStatus) Enums.memberStatus.enumFromString(status);

			// update the EventMember status
			EventMemberMgr.singleton.updateMemberStatus(ev, mem, memberStatus);
		}

        // generate xml
        _xh.startElem("JClubs", false, new string[] { 
            "title", "Member Compatibility for Event " + ev.id,
            "xmlns:html", "http://www.w3.org/1999/_xhtml",
            "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance" });

        // warning
        if (warning != null)
            _xh.startElem("warning", true, new string[] { "message", warning });
		
		// event
        ev.everythingToXml(_xh, UiConstants.TablesPage);

        // <members>
        Member.toXml(_xh, ev.members(true), ev);

        // <tables>
        ev.tablesToXml(_xh);

        // <compatibilities>
        //ev.coupleGrid.compatibilitiesToXml(_xh);
        ev.memberGrid.compatibilitiesToXml(_xh);

        // venue and host xml
        Venue.allToXml(_xh);
        Host.allToXml(_xh);

        // tail
        _xh.endElem(); // </JClubs>

        _log.debug("processRequest() finish");
    }


    /**
     * Load the event, does some checking
     */
    private Event getEvent(NameValueCollection _queryParams)
    {
        string sId = _queryParams.Get(UiConstants.EventParam);
        int eventId = sId == null ? DbConstants.NoId : int.Parse(sId);
        if (eventId == DbConstants.NoId)
            return null;
        Event ev = (Event) EventMgr.singleton.load(eventId);

        // if there are no tables, we wqant to bail out
        if (ev.tables == null || ev.tables.Count == 0)
            return null;

        return ev;
    }


    /**
     * Handles the completeTable action
     */
    private void completeTable(Event ev)
    {
        // get the table id
		string sTableId = _queryParams.Get("selectedTable");
        if (sTableId.StartsWith("t"))
            sTableId = sTableId.Substring(1);
        int tableId = int.Parse(sTableId);

        foreach (Table table in ev.tables)
            if (table.id == tableId)
            {
                table.completeTable();
                return;
            }
    }
}

}
