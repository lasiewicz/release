using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;



/**
 * FAQ Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class FaqPage : SecurePage 
{
	private static Log _log = new Log("FaqPage");


	/**
	 * Generates the xml for the page
	 */
	protected override void processRequest() 
	{
		// Make sure there is a logged in member
		checkMember(UiConstants.FaqPage);
		if (_member == null)
			return;

		// XML
		_xh.startElem("JClubs", false, null);
		if (_member != null)
			_member.toXml(_xh, Member.MemberXmlStyle.Simple);
		_xh.endElem(); // JClubs
	}
}

}