using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using Matchnet.JoesClub.ValueObjects;



/**
 * UserCalendar Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class EventMemberTableAjax : SecureAjax
{
    private static Log _log = new Log("EventMemberTableAjax");

    private Event _event;


    /**
     * Generates the xml for the response
     */
    protected override void processRequest() 
    {
        // Make sure there is a logged in member
        checkMember();
        if (_member == null)
            return;

        // get the event 
        int eventId = getIdParam(UiConstants.EventParam);
        _event = (Event) EventMgr.singleton.load(eventId);

        // get the Member
        int memberId = getIdParam(UiConstants.MemberParam);
        _member = _event.getMember(memberId);

        // find the member's table
        Table table = null;
        if (_member != null)
            table = _event.getMemberTable(_member);

        // handle no table
        if (table == null)
        {
            _xh.writeSimpleElem("NoTable", "");
            return;
        } 

        // host
        Host host = _event.getHost(_member);
        if (host != null)
            host.toXml(_xh);

		foreach (Member member in table.members) 
		{
			EventMember em = _event.getEventMember(member.id);
			if (em.memberStatus == MemberStatus.Attended)
				memberToXml(member);
		}
    }


    /**
     * Write xml for a couple
     */
    private void coupleToXml(MemberGridCell couple)
    {
        if (couple == null)
            return;
        memberToXml(couple.member1);
        memberToXml(couple.member2);
    }


    /**
     * Write xml for a table member
     */
    private void memberToXml(Member member)
    {
        _xh.startElem("member", false, null);

        _xh.writeSimpleElem("id", member.id.ToString());
        _xh.writeSimpleElem("firstName", member.firstName);
        _xh.writeSimpleElem("lastName", member.lastName);
        _xh.writeSimpleElem("image", member.image);
		_xh.writeSimpleElem("jdateUrl", member.jdateUrl);
		_xh.writeSimpleElem("avatar", member.avatar);

        _xh.endElem(); // member
    }

}

}