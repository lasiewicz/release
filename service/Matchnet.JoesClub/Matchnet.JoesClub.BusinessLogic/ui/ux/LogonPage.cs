using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;

using Matchnet.JoesClub.ValueObjects;

/**
 * UserCalendar Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class LogonPage : BasePageHandler
{
    private static Log _log = new Log("LogonPage");

    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
        // get currently logged in member
        int memberId = DbConstants.NoId;

        string action = _queryParams.Get(UiConstants.ActionParam);
        if (action == null || action == "")
            memberId = LogonMgr.Instance.getMemberId(_pageRequest);

        // authenticate 
        else if (action == "authenticate" || action == "authenticateGuid")
        {
            AuthenticationStatus authStatus;
			string emailAddress = _queryParams.Get("emailAddress");
			string password = _queryParams.Get("password");
			string guid = _queryParams.Get("guid");

            // authenticate either with email and password or guid
            if (action == "authenticate")
                authStatus = LogonMgr.Instance.logOn(_pageResponse, emailAddress, password);
            else 
                authStatus = LogonMgr.Instance.logOn(_pageResponse, guid);

			if (AuthenticationStatus.Authenticated == authStatus) 
				memberId = _pageResponse.memberId;
			else
			{
				int errorCode = (int)authStatus;
				redirect("Index.htm?errorCode=" + errorCode);
				return;
			}
        }

        // logout
        else if (action == "logout")
        {
            LogonMgr.Instance.logOff(_pageResponse);
			redirect("Index.htm");
			return;
        }

        else
            _log.error("processRequest() Unknown action: " + action);

        // check for redirect.  Only redirect if logon was successful.
        string redirectUrl = Misc.nullToEmptyString(_queryParams.Get("redirect"));
        if (memberId != DbConstants.NoId && redirectUrl != "")
        {
            redirect(redirectUrl);
            return;
        }

        if (memberId == DbConstants.NoId)
        {
			redirect("Index.htm?redirect=" + redirectUrl);
			return;
        }
        else
        {
			redirect("UserCalendar2.aspx");
			return;
		}
    }
}

}