using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;



/**
 * Invitation Page (Flash)
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class InvitationPage : BasePageHandler
{
	private static Log _log = new Log("InvitationPage");

	/**
	 * Generates the xml for the page
	 */
	protected override void processRequest() 
	{
        // get event and member from guid
		string inviteGuid = Misc.emptyToNullString(_queryParams.Get("id"));
        if (inviteGuid == null)
            return;

        int eventId;
        int memberId;
        EventMemberMgr.singleton.getEventMemberByInviteGuid(
            inviteGuid, out eventId, out memberId);
        if (eventId == CommonConstants.NoValueInt || // check for bad inviteGuid
            memberId == CommonConstants.NoValueInt)
        {
            return;
        }

        Event ev = (Event) EventMgr.singleton.load(eventId);
        Member member = ev.getMember(memberId);
        EventMember eventMember = ev.getEventMember(memberId);
        bool inviteExpired = eventMember.inviteStatus == InviteStatus.InviteExpired;

		MemberStatus memStatus = eventMember.memberStatus;
		if (memStatus == MemberStatus.RsvpedNo || memStatus == MemberStatus.RsvpedYes)
		{
			redirect(memStatus == MemberStatus.RsvpedYes? UiConstants.InviteAcceptedPage : UiConstants.InviteDeclinedPage);
			return;
		}

		// XML
		_xh.startElem("JClubs", false, null);

		_xh.writeSimpleElem("expired", inviteExpired);
		_xh.writeSimpleElem("guid", inviteGuid);
		_xh.writeSimpleElem("memberId", memberId);
		_xh.writeSimpleElem("firstName", member.firstName);
		_xh.writeSimpleElem("eventId", eventId);
		Host host = ev.getHost(member);
		host.toXml(_xh);
		Venue venue = ev.getVenue(member);
		venue.toXml(_xh);

		// date and time
		string time = ev.getMemberTable(member).timeToString();
		_xh.writeSimpleElem("time", time);
		_xh.writeSimpleElem("date", ev.date.ToShortDateString());

		_xh.endElem(); // JClubs
	}


}

}