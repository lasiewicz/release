using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;



/**
 * Opt-out Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class OptOutPage : SecurePage 
{
	private static Log _log = new Log("OptOutPage");

	/**
	 * Generates the xml for the page
	 */
	protected override void processRequest() 
	{
		string action = _queryParams.Get(UiConstants.ActionParam);

		string divId = "div_error";
		string guid = _queryParams.Get("id");
		Member member = null;
		if (guid != null && guid.Length > 0) 
		{
			int memberId = MemberMgr.singleton.getMemberByGuid(guid);
			if (memberId != -1)
			{
				member = (Member) MemberMgr.singleton.load(memberId);
				divId = member.emailOptedOut? "div_confirmation" : "div_notice";
			}
		}
		
		if (action == "submit") 
		{
			string status = "error";
			if (member != null)
			{
				member.emailOptedOut = true;
				member.save();
				status = "ok";
			}

            string xml = "<" + status + "/>";
			_xh.textWriter.Write(xml);
			return;
		}

		// XML
		_xh.startJClubs();
		_xh.writeSimpleElem("showDiv", divId);
		_xh.endJClubs();
	}
}

}