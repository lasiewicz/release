using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;


/**
 * Application Page (aka self-registration page)
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class ApplicationPage : SecurePage
{
	private static Log _log = new Log("ApplicationPage");

    private string _firstName;
    private string _lastName;
    private string _emailAddress;
    private string _phonePart0;
    private string _phonePart1;
    private string _phonePart2;
    private MemberWhenToCall _whenToCall;
    private PhoneType _phoneType;

    private const int NO_ERROR = 0;
    private const int DUPE_EMAIL_ERROR = 1;


	/**
	 * Generates the xml for the page
	 */
	protected override void processRequest() 
	{
        _firstName = "";
        _lastName = "";
        _emailAddress = "";
        _phonePart0 = "";
        _phonePart1 = "";
        _phonePart2 = "";
        _whenToCall = MemberWhenToCall.NoValue;
        _phoneType = PhoneType.NoValue;

        // save
		string action = _queryParams.Get(UiConstants.ActionParam);
		_log.debug("action: " + action);
        int error = NO_ERROR;
		if (action == UiConstants.SaveAction) 
		{
            error = performSave();
			if (error == NO_ERROR)
            {
			    redirect(UiConstants.ApplicationConfPage);
			    return;
            }
		}

		// XML
		_xh.startElem("JClubs", false, null);

        _xh.writeSimpleElem("firstName", _firstName);
        _xh.writeSimpleElem("lastName", _lastName);
        _xh.writeSimpleElem("emailAddress", _emailAddress);
        _xh.writeSimpleElem("phonePart0", _phonePart0);
        _xh.writeSimpleElem("phonePart1", _phonePart1);
        _xh.writeSimpleElem("phonePart2", _phonePart2);

		// Generate xml for "when to call" and phone type choices
        ValueSetXml.toXml(Enums.memberWhenToCall, _xh, "whenToCall", _whenToCall);
        ValueSetXml.toXml(Enums.phoneType, _xh, "phoneType", _phoneType);

        if (error == DUPE_EMAIL_ERROR)
        {
            int errorCode = (int) AuthenticationStatus.DuplicateEmail;
			redirect("Index.htm?errorCode=" + errorCode);
			return;
        }

        _xh.endElem(); // JClubs
	}


    /*
     * Create a new with data from form.
     */
	private int performSave()
	{
		// get form data
		string fName = Misc.emptyToNullString(_queryParams.Get("firstName"));
		string lName = Misc.emptyToNullString(_queryParams.Get("lastName"));
		string email = Misc.emptyToNullString(_queryParams.Get("email"));
        if (email != null)
            email = email.Trim();
		string phone = Misc.emptyToNullString(_queryParams.Get("phone"));

		string sWhenToCall = _queryParams.Get("whenToCall");
		int iWhenToCall = int.Parse(sWhenToCall);
		MemberWhenToCall whenToCall = (MemberWhenToCall) iWhenToCall;
        sWhenToCall = Enums.memberWhenToCall.getName(whenToCall);

		string sPhoneType = _queryParams.Get("phoneType");
		int iPhoneType = int.Parse(sPhoneType);
		PhoneType phoneType = (PhoneType) iPhoneType;

        // check for duplicate email
        if (MemberMgr.singleton.getMemberIdFromEmailAddress(email) != 
            DbConstants.NoId)
        {
            _firstName = fName;
            _lastName = lName;
            _emailAddress = email;
            _whenToCall = whenToCall;
            _phoneType = phoneType;

            string[] phoneParts = phone.Split(new char[] {'-'});
            _phonePart0 = phoneParts[0];
            _phonePart1 = phoneParts[1];
            _phonePart2 = phoneParts[2];

            return DUPE_EMAIL_ERROR;
        }

		// try to create the member using their JDate info.  If they're 
		// not a jdate member, just create a new member.
        int sparkId = JDateMemberUtil.Instance.getSparkId(email);
		Member member = Member.create(sparkId);
        JDateMemberUtil.Instance.getJDateValues(member);

		// use form data, even if it override values from JDate
		member.firstName = fName;
		member.lastName = lName;
		member.emailAdd = email;
		member.phone = phone;
        member.phoneType = phoneType;
		member.whenToCall = sWhenToCall;
        member.applicationReceived = DateTime.Now;

		// this is the initial activityStatus
		member.activityStatus = MemberActivityStatus.Applicant;

		// save to db
		member.save();

        // Send ApplicationReply email
        EmailInstance applicationReply = ApplicationReply.createEmailInstance(member);
		applicationReply.send();

        return NO_ERROR;
	}
}

}