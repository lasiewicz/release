using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using Matchnet.JoesClub.ValueObjects;



/**
 * Ajax page to send venue details
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class VenueDetailsAjax : SecureAjax
{
    private static Log _log = new Log("VenueDetailsAjax");

    private Venue _venue;



    /**
     * Generates the xml for the response
     */
    protected override void processRequest() 
    {
        // Make sure there is a logged in member
        if (_member == null)
            return;

        // get the Venue
        string sVenueId = _queryParams.Get(UiConstants.VenueParam);
        if (sVenueId == null || sVenueId == "")
            _log.error("processRequest() missing parameter: " + UiConstants.VenueParam);
        int venueId = int.Parse(sVenueId);
        _venue = (Venue) VenueMgr.singleton.load(venueId);

        // generate detailed xml
        _venue.toXml(_xh, true);
    }
}

}