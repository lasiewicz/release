using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using Matchnet.JoesClub.ValueObjects;



/**
 * UserCalendar Page -- new version
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class UserCalendarPage2 : SecurePage 
{
    private static Log _log = new Log("UserCalendarPage2");


    /**
     * Generates the xml for the page
     */
    protected override void processRequest() 
    {
        // Make sure there is a logged in member
        checkMember("UserCalendar2.aspx");
        if (_member == null)
            return;

        // write xml
        _xh.startElem("JClubs", false, null);

        if (_member != null)
            _member.toXml(_xh, Member.MemberXmlStyle.CalendarPage);
        
        Event.getCalendarXml(_xh, _member);

        // VenueCities
        _xh.startElem("VenueCities", false, null);
        foreach (string city in VenueBL.Instance.getVenueCities())
            _xh.writeSimpleElem("city", city);
        _xh.endElem(); 

        _xh.endElem(); // JClubs
    }
}


}