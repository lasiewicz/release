using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using Matchnet.JoesClub.ValueObjects;



/**
 * UserCalendar Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class RsvpAjax : BasePageHandler
{
    private static Log _log = new Log("RsvpAjax");

    private Event _event;
    private Member _member;


    /**
     * Generates the xml for the response
     */
    protected override void processRequest() 
    {
        _event = null;
        _member = null;
		bool flash = false;

        // figure out the member and event.  If there's a "id" param,
        // use it
        string inviteGuid = Misc.emptyToNullString(_queryParams.Get("guid"));
        if (inviteGuid != null)
        {
            int eventId;
            int memberId;
            EventMemberMgr.singleton.getEventMemberByInviteGuid(
                inviteGuid, out eventId, out memberId);
            if (eventId == DbConstants.NoId || // check for bad inviteGuid
                memberId == DbConstants.NoId)
                _log.error("bad inviteGuid"); 

            _event = (Event) EventMgr.singleton.load(eventId);
            _member = _event.getMember(memberId);

			// if there's a guid, source was flash
			flash = true;
        }
        else
        {
            // get logged in member
            _member = SecureAjax.getMember(_pageRequest);

            // get the event 
            string sEventId = _queryParams.Get(UiConstants.EventParam);
            if (sEventId == null || sEventId == "")
                _log.error("handleAction() no event param");
            int eventId = int.Parse(sEventId);
            _event = (Event) EventMgr.singleton.load(eventId);
        }

        // error checking
        if (_member == null)
            _log.error("_member == null"); 
        if (_event == null)
            _log.error("_event == null"); 

        // MemberStatus is in the action param
        string sAction = _queryParams.Get(UiConstants.ActionParam);
        MemberStatus memberStatus = (MemberStatus) Enums.memberStatus.enumFromString(sAction);

        // update the EventMember status
        EventMemberMgr.singleton.updateMemberStatus(_event, _member, memberStatus);

		if (flash) 
		{
			redirect(sAction.Equals("RsvpedYes")? UiConstants.InviteAcceptedPage : UiConstants.InviteDeclinedPage);
		}
		else
		{
			// generate response xml
			_xh.writeSimpleElem("eventId", _event.id.ToString());
			_xh.writeSimpleElem("memberId", _member.id.ToString());

			EventMember em = _event.getEventMember(_member.id);
			em.memberStatusToXml(_xh);
		}
    }


    /**
     * See is s1 contains s2; case-insensitive
     */
    bool stringContains(string s1, string s2)
    {
        return s1 != null && s1.ToLower().IndexOf(s2.ToLower()) != -1;
    }

}

}