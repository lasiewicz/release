using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;



/**
 * Tutorial Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class TutorialPage : SecurePage 
{
	private static Log _log = new Log("TutorialPage");


	/**
	 * Generates the xml for the page
	 */
	protected override void processRequest() 
	{
		// See if there is a logged in member
		checkMember(UiConstants.TutorialPage);

        // not sure if this is suppose to be secure.  If not,
        // just delete these two lines:
		if (_member == null)
            return;

		if (_member == null)
        {
            // try to get the member from the id
            string memberGuid = Misc.emptyToNullString(_queryParams.Get("id"));
            if (memberGuid != null)
            {
                int memberId = MemberMgr.singleton.getMemberByGuid(memberGuid);
                if (memberId != DbConstants.NoId)
                    _member = (Member) MemberMgr.singleton.load(memberId);
            }
        }

		// XML
        _xh.startJClubs();
		if (_member != null)
			_member.toXml(_xh, Member.MemberXmlStyle.Simple);
		_xh.endJClubs();
	}
}

}