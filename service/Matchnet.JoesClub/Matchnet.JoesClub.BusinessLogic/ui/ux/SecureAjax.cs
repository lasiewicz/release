using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;

using Matchnet.JoesClub.ValueObjects;


/**
 * 
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class SecureAjax : BasePageHandler
{
    private static Log _log = new Log("SecureAjax");
    protected Member _member;

    /**
     * Called before processRequest();
     */
    protected override void preProcessRequest() 
    {
        // Make sure there is a logged in member
        checkMember();

        base.preProcessRequest();
    }


    /**
     * Gets the current member.  If there isn't one, redirects to the 
     * logon page
     */
    protected void checkMember() 
    {
        _member = getMember(_pageRequest);
        if (_member == null)
            _log.error("checkMember() no member logged in.  callingPage=" + _pageRequest.pageName);
    }


    /**
     * Gets the current (logged in) member. 
     */
    public static Member getMember(PageRequest pageRequest)
    {
        int memberId = LogonMgr.Instance.getMemberId(pageRequest);
        if (memberId != DbConstants.NoId)
            return (Member) MemberMgr.singleton.load(memberId);
        else
            return null;
    }
}
}
