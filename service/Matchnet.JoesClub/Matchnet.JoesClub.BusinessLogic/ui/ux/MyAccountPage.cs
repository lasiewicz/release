using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;


/**
 * EditPreferences Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
	public class MyAccountPage : SecurePage 
	{
		private static Log _log = new Log("MyAccountPage");

		/**
		 * Generates the xml for the page
		 */
		protected override void processRequest() 
		{
			// Make sure there is a logged in member
			checkMember(UiConstants.MyAccountPage);
			if (_member == null)
				return;

			// save
			string action = _queryParams.Get(UiConstants.ActionParam);
			_log.debug("action: " + action);
			if (action == UiConstants.SaveAction) 
			{
				performSave(_member);
			}

			// write xml
			_xh.startElem("JClubs", false, null);

			if (_member != null)
				_member.toXml(_xh, Member.MemberXmlStyle.MyAccountPage);

			_xh.endElem(); // JClubs
		}

		/**
		 * Update member's preferences with data from form.
		 */
		private void performSave(Member member)
		{
			if (null == member) 
			{
				_log.error("PerformSave() was called without a member id.");
				return;
			}
			_log.debug("peform save for member: " + member);

			string section = _queryParams.Get("section");
			if (section.Equals("info"))
			{
				saveBasicInfo(member);
			}
			else if (section.Equals("prefs"))
			{
				savePreferences(member);
			}
			else if (section.Equals("personality"))
			{
				savePersonality(member);
			}
		}

		private void saveBasicInfo(Member member)
		{
			string fName = Misc.emptyToNullString(_queryParams.Get("firstName"));
			member.firstName = fName;

			string lName = Misc.emptyToNullString(_queryParams.Get("lastName"));
			member.lastName = lName;

			string phone1 = Misc.emptyToNullString(_queryParams.Get("phone1"));
			member.phone = phone1;

			string phone2 = Misc.emptyToNullString(_queryParams.Get("phone2"));
			member.phone2 = phone2;

			string sPhoneType1 = Misc.emptyToNullString(_queryParams.Get("select_phoneType1"));
			member.phoneType = (PhoneType) int.Parse(sPhoneType1);

			string sPhoneType2 = Misc.emptyToNullString(_queryParams.Get("select_phoneType2"));
			member.phoneType2 = (PhoneType) int.Parse(sPhoneType2);

			string sAgeLow = Misc.emptyToNullString(_queryParams.Get("desiredAgeLow"));
			member.desiredAgeLow = int.Parse(sAgeLow);

			string sAgeHigh = Misc.emptyToNullString(_queryParams.Get("desiredAgeHigh"));
			member.desiredAgeHigh = int.Parse(sAgeHigh);

			string sOccupationType = Misc.emptyToNullString(_queryParams.Get("select_occupation"));
			member.occupation = (MemberOccupation)int.Parse(sOccupationType);

			string sWillDateParent = Misc.emptyToNullString(_queryParams.Get("willDateParent"));
			member.willDateAParent = int.Parse(sWillDateParent);

			member.save();
		}

		private void savePreferences(Member member)
		{
			member.locationPref = getValuesForParam("locationPref");
			member.timePref = getValuesForParam("timePref");
			member.dietaryRestrictions = getValuesForParam("dietaryRestrictions");
			member.cuisinesToAvoid = getValuesForParam("cuisinesToAvoid");
			member.additionalNights = getValuesForParam("additionalNights");
			member.desiredMaritalStatus = getValuesForParam("desiredMaritalStatus");
			member.desiredJDateReligion =  getValuesForParam("desiredJDateReligion");
			member.desiredEducationLevel = getValuesForParam("desiredEducationLevel");
			member.desiredDrinkingHabits = getValuesForParam("desiredDrinkingHabits");
			member.desiredSmokingHabits = getValuesForParam("desiredSmokingHabits");

			member.save();
		}


        /**
         * Saves the responses to the personality questions
         */
		private void savePersonality(Member member)
		{
			member.personalityQuestion1 = (MemberPersonalityQuestion1) getIntParam("personalityQuestion1");
			member.personalityQuestion2 = (MemberPersonalityQuestion2) getIntParam("personalityQuestion2");
			member.personalityQuestion3 = (MemberPersonalityQuestion3) getIntParam("personalityQuestion3");
			member.personalityQuestion4 = (MemberPersonalityQuestion4) getIntParam("personalityQuestion4");
			member.personalityQuestion5 = (MemberPersonalityQuestion5) getIntParam("personalityQuestion5");
			member.personalityQuestion6 = (MemberPersonalityQuestion6) getIntParam("personalityQuestion6");
			member.personalityQuestion7 = (MemberPersonalityQuestion7) getIntParam("personalityQuestion7");
			member.personalityQuestion8 = (MemberPersonalityQuestion8) getIntParam("personalityQuestion8");
			member.personalityQuestion9 = (MemberPersonalityQuestion9) getIntParam("personalityQuestion9");
			member.personalityQuestion10 = (MemberPersonalityQuestion10) getIntParam("personalityQuestion10");
			member.personalityQuestion11 = (MemberPersonalityQuestion11) getIntParam("personalityQuestion11");
			member.personalityQuestion12 = (MemberPersonalityQuestion12) getIntParam("personalityQuestion12");
			member.describeMe = getValuesForParam("describeMe");

			member.save();
		}

		/**
		 * Get the value or values from a set of checkboxes or
		 * radio buttons.
		 * 
		 * @param paramName	name of the querystring parameter
		 * @return	The values OR'ed together.
		 */
		private int getValuesForParam(string paramName)
		{
			string sCodes = Misc.emptyToNullString(_queryParams.Get(paramName));
			if (null == sCodes)
				return 0;

			string[] sCodesArray = sCodes.Split(',');
			int code = 0;
			foreach (string sCode in sCodesArray)
			{
				code |= int.Parse(sCode);
			}
			return code;
		}


		/**
         * Returns the value of a parameter expected to be an int.
		 * @param paramName	name of the querystring parameter
		 */
		private int getIntParam(string paramName)
		{
			string sValue = Misc.emptyToNullString(_queryParams.Get(paramName));
			if (null == sValue)
				return CommonConstants.NoValueInt;

            return int.Parse(sValue);
		}
	}

}