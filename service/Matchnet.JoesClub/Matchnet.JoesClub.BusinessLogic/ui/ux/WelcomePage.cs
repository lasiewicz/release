using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Matchnet.JoesClub.ValueObjects;



/**
 * Welcome Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class WelcomePage : BasePageHandler 
{
	private static Log _log = new Log("WelcomePage");


	/**
	 * Generates the xml for the page
	 */
	protected override void processRequest() 
	{
        // get the member
        string memberGuid = Misc.emptyToNullString(_queryParams.Get("id"));
        if (memberGuid == null)
            _log.error("processRequest() memberGuid == null");
        int memberId = MemberMgr.singleton.getMemberByGuid(memberGuid);
        if (memberId == DbConstants.NoId)
            _log.error("processRequest() memberId == DbConstants.NoId");
        Member member = (Member) MemberMgr.singleton.load(memberId);

		// XML
		_xh.startJClubs();
		_xh.writeSimpleElem("memberGuid", memberGuid);
		_xh.writeSimpleElem("memberName", member.firstName);
		_xh.writeSimpleElem("calendarUrl", Config.getProperty("UxUrl") + "/UserCalendar2.aspx");
		_xh.endJClubs();
	}
}

}