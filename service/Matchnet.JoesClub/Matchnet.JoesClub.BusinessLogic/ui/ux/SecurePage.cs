using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using Matchnet.JoesClub.ValueObjects;


/**
 * UserCalendar Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public abstract class SecurePage : BasePageHandler
{
    protected Member _member;
    protected bool _adminAuthenticated;

    /**
     * Gets the current member.  If there isn't one, redirects to the 
     * logon page
     */
    protected void checkMember(string callingPage) 
    {
        int memberId = LogonMgr.Instance.getMemberId(_pageRequest);
        if (memberId != DbConstants.NoId)
            _member = (Member) MemberMgr.singleton.load(memberId);
        else
        {
             _member = null;
            redirect("Logon.aspx?redirect=" + callingPage);
        }
    }


    /**
     * Checks whether the admin is authenticated.  If not, redirects to the 
     * logon page
     */
    protected void checkAdmin(string callingPage) 
    {
        _adminAuthenticated = AdminLogonMgr.Instance.authenticated(_pageRequest);
        if (!_adminAuthenticated)
            redirect("AdminLogon.aspx?redirect=" + callingPage);
    }

}

}