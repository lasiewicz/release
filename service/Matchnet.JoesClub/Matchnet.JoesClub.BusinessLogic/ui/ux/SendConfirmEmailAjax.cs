using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using Matchnet.JoesClub.ValueObjects;



/**
 * UserCalendar Page
 */
namespace Matchnet.JoesClub.BusinessLogic
{
public class SendConfirmEmailAjax : SecureAjax
{
    private static Log _log = new Log("SendConfirmEmailAjax");

    private Event _event;


    /**
     * Generates the xml for the response
     */
    protected override void processRequest() 
    {
        // Make sure there is a logged in member
        checkMember();
        if (_member == null)
            return;

        // get the event 
        string sEventId = _queryParams.Get(UiConstants.EventParam);
        if (sEventId == null || sEventId == "")
            _log.error("handleAction() no event param");
        int eventId = int.Parse(sEventId);
        _event = (Event) EventMgr.singleton.load(eventId);

        // Send the emails

        // generate response xml
        _xh.writeSimpleElem("eventId", _event.id.ToString());

    }

}

}