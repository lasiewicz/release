using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class VenueBL
{
    private static Log _log = new Log("VenueBL");

    public static VenueBL Instance = new VenueBL();

    
    private VenueBL()
    {
    }


    /**
     * Load the Venue
     */
    public VenueVO load(int id)
    {
        Venue venue = (Venue) VenueMgr.singleton.load(id);
        return VOConvert.convert(venue);
    }


    /**
     * delete the Venue
     */
    public void delete(int id)
    {
        VenueMgr.singleton.delete(id);
    }


    /**
     * save the Venue
     */
    public int save(VenueVO venueVO)
    {
        if (venueVO.id == DbConstants.NoId)
            add(venueVO);
        else
            update(venueVO);

        return venueVO.id;
    }


    /**
     * add the Venue
     */
    public void add(VenueVO venueVO)
    {
        Venue venue = new Venue(
            DbConstants.NoId,
            venueVO.name,
            venueVO.address,
            venueVO.address2,
            venueVO.city,
            venueVO.state,
            venueVO.zipCode,
            venueVO.webSiteUrl,
            venueVO.image,
            venueVO.phone,
            venueVO.contactName,
            venueVO.contactPhone,
            venueVO.contactTitle,
            venueVO.mapUrl,
            venueVO.parking,
            venueVO.parkingFee,
            venueVO.priceRange,
            venueVO.notes,
            venueVO.salesCopy,
            venueVO.salesCopy2,
            venueVO.image2,
            false, // deleted
            venueVO.meetingLocation);

        venue.save();
        venueVO.id = venue.id;
    }


    /**
     * update the Venue
     */
    public void update(VenueVO venueVO)
    {
        Venue venue = (Venue) VenueMgr.singleton.load(venueVO.id);

        venue.name = venueVO.name;
        venue.address = venueVO.address;
        venue.address2 = venueVO.address2;
        venue.city = venueVO.city;
        venue.state = venueVO.state;
        venue.zipCode = venueVO.zipCode;
        venue.webSiteUrl = venueVO.webSiteUrl;
        venue.image = venueVO.image;
        venue.phone = venueVO.phone;
        venue.contactName = venueVO.contactName;
        venue.contactPhone = venueVO.contactPhone;
        venue.contactTitle = venueVO.contactTitle;
        venue.mapUrl = venueVO.mapUrl;
        venue.parking = venueVO.parking;
        venue.parkingFee = venueVO.parkingFee;
        venue.priceRange = venueVO.priceRange;
        venue.notes = venueVO.notes;
        venue.salesCopy = venueVO.salesCopy;
        venue.salesCopy2 = venueVO.salesCopy2;
        venue.image2 = venueVO.image2;
        venue.meetingLocation = venueVO.meetingLocation;

        venue.save();

    }


    /**
     * Gets a dataset of all the venues
     */
    public DataSet getDataSet()
    {
        return DbUtils.getDataSet("GetAllVenues", "venues", null);
    }


    /**
     * Returns the set of venue cities
     */
    public string[] getVenueCities() 
    { 
        string sVenueCities = Config.getProperty("VenueCities");
        string[] venueCities = sVenueCities.Split(new char[] {','});
        for (int i = 0; i < venueCities.Length; i++)
            venueCities[i] = venueCities[i].Trim();
        return venueCities;
    }

}
}
