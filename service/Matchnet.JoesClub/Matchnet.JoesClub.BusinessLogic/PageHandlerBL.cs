using System;
using System.Collections.Specialized;

using Matchnet.JoesClub.ValueObjects;


namespace Matchnet.JoesClub.BusinessLogic
{
public class PageHandlerBL
{
    private static Log _log = new Log("PageHandlerBL");
    
    private PageHandlerBL()
	{
	}

    public static PageResponse processRequest(PageRequest pageRequest)
    {
        string pageName = pageRequest.pageName;
        BasePageHandler page = null;

        // admin pages
        if (pageName == UiConstants.AdminPage)
            page = new AdminPage();
        else if (pageName == UiConstants.CompExplPage)
            page = new CompExplPage();
        else if (pageName == UiConstants.EditEventPage)
            page = new EditEventPage();
        else if (pageName == UiConstants.EmailDetailsPage)
            page = new EmailDetailsPage();
        else if (pageName == UiConstants.EmailsPage)
            page = new EmailsPage();
        else if (pageName == UiConstants.EventInfoByVenueAjax)
            page = new EventInfoByVenueAjax();
        else if (pageName == UiConstants.EventsPage)
            page = new EventsPage();
        else if (pageName == UiConstants.JsRequest)
            page = new JsRequest();
        else if (pageName == UiConstants.MemberGridPage)
            page = new MemberGridPage();
        else if (pageName == UiConstants.PostEventPage)
            page = new PostEventPage();
        else if (pageName == UiConstants.TablesPage)
            page = new TablesPage();
        else if (pageName == UiConstants.MemberCalendarPage)
            page = new MemberCalendarPage();
        else if (pageName == UiConstants.TestPage)
            page = new TestPage();
        else if (pageName == UiConstants.AlertsPage)
            page = new AlertsPage();

        // ux pages
        else if (pageName == UiConstants.ApplicationPage)
            page = new ApplicationPage();
        else if (pageName == UiConstants.FaqPage)
            page = new FaqPage();
		else if (pageName == UiConstants.TutorialPage)
			page = new TutorialPage();
        else if (pageName == UiConstants.InvitationPage)
            page = new InvitationPage();
        else if (pageName == UiConstants.LogonPage)
            page = new LogonPage();
        else if (pageName == UiConstants.AdminLogonPage)
            page = new AdminLogonPage();
        else if (pageName == UiConstants.LogonPage)
            page = new LogonPage();
        else if (pageName == UiConstants.MyAccountPage)
            page = new MyAccountPage();
        else if (pageName == UiConstants.OptOutPage)
            page = new OptOutPage();
        else if (pageName == UiConstants.RsvpAjaxPage)
            page = new RsvpAjax();
        else if (pageName == UiConstants.EventMemberTableAjax)
            page = new EventMemberTableAjax();
        else if (pageName == UiConstants.SendConfirmEmailAjaxPage)
            page = new SendConfirmEmailAjax();
        else if (pageName == UiConstants.UserCalendarPage2)
            page = new UserCalendarPage2();
        else if (pageName == UiConstants.VenueDetailsAjaxPage)
            page = new VenueDetailsAjax();
		else if (pageName == UiConstants.WelcomePage)
			page = new WelcomePage(); /// TODO: put in UiConstants.cs

        if (page == null)
            _log.error("processRequest() page == null.  pageName=" + pageName);

        PageResponse pr = page.processRequest(pageRequest);
        return pr;
    }


	}
}
