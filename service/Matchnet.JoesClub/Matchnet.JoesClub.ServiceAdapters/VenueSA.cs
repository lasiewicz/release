using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceAdapters
{
public class VenueSA : IVenueService
{
	public static readonly VenueSA Instance = new VenueSA();

	private const string SERVICE_MANAGER_NAME = "VenueSM";

	private VenueSA()
	{
	}

    public VenueVO load(int id)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).load(id);
        }
        catch (Exception ex)
        {
            throw new SAException("load() error (uri: " + uri, ex);
        }
    }


    public int save(VenueVO venue)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).save(venue);
        }
        catch (Exception ex)
        {
            throw new SAException("save() error (uri: " + uri, ex);
        }
    }


    public void delete(int id)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            getService(uri).delete(id);
        }
        catch (Exception ex)
        {
            throw new SAException("delete() error (uri: " + uri, ex);
        }
    }


    /**
     * Gets a dataset of all the venues
     */
    public DataSet getDataSet()
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getDataSet();
        }
        catch (Exception ex)
        {
            throw new SAException("getDataSet() error (uri: " + uri, ex);
        }
    }



    /**
     * Returns the set of venue cities
     */
    public string[] getVenueCities() 
    { 
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getVenueCities();
        }
        catch (Exception ex)
        {
            throw new SAException("getVenueCities() error (uri: " + uri, ex);
        }
    }

	private IVenueService getService(string uri)
	{
		try
		{
			return (IVenueService)Activator.GetObject(typeof(IVenueService), uri);
		}
		catch(Exception ex)
		{
			throw new SAException("Cannot activate remote service manager at " + uri, ex);
		}
	}
}
}
