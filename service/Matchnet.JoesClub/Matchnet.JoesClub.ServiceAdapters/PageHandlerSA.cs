using System;
using System.Collections.Specialized;

using Matchnet.Exceptions;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceAdapters
{
	public class PageHandlerSA
	{
		public static readonly PageHandlerSA Instance = new PageHandlerSA();

		private const string SERVICE_MANAGER_NAME = "PageHandlerSM";

		private PageHandlerSA()
		{
		}

        public PageResponse processRequest(PageRequest pageRequest)
        {
            string uri = "";

            try
            {
                uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
                return getService(uri).processRequest(pageRequest);
            }
            catch (Exception ex)
            {
                throw new SAException("processRequest() error (uri: " + uri + ", pageName: " + pageRequest.pageName + ").", ex);
            }
        }

		private IPageHandlerService getService(string uri)
		{
			try
			{
				return (IPageHandlerService)Activator.GetObject(typeof(IPageHandlerService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}
	}
}
