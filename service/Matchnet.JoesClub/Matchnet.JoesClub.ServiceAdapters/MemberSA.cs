using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceAdapters
{
public class MemberSA : IMemberService
{
	public static readonly MemberSA Instance = new MemberSA();

	private const string SERVICE_MANAGER_NAME = "MemberSM";

	private MemberSA()
	{
	}

    public MemberVO load(int id)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).load(id);
        }
        catch (Exception ex)
        {
            throw new SAException("load() error (uri: " + uri, ex);
        }
    }


    public int save(MemberVO member)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).save(member);
        }
        catch (Exception ex)
        {
            throw new SAException("save() error (uri: " + uri, ex);
        }
    }


    public void delete(int id)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            getService(uri).delete(id);
        }
        catch (Exception ex)
        {
            throw new SAException("delete() error (uri: " + uri, ex);
        }
    }


    /**
     * Gets a dataset of all the members
     */
    public DataSet getDataSet()
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getDataSet();
        }
        catch (Exception ex)
        {
            throw new SAException("getDataSet() error (uri: " + uri, ex);
        }
    }


    /**
     * Gets a dataset of all the members with the given name
     */
	public DataSet getDataSetByName(string name)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getDataSetByName(name);
        }
        catch (Exception ex)
        {
            throw new SAException("getDataSetByName() error (uri: " + uri, ex);
        }
    }


    /**
     * Gets a dataset of all the members with the given email
     */
	public DataSet getDataSetByEmail(string email)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getDataSetByEmail(email);
        }
        catch (Exception ex)
        {
            throw new SAException("getDataSetByEmail() error (uri: " + uri, ex);
        }
    }


    public ArrayList loadPhoneInterviewsForMember(int memberId)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).loadPhoneInterviewsForMember(memberId);
        }
        catch (Exception ex)
        {
            throw new SAException("loadPhoneInterviewsForMember() error (uri: " + uri, ex);
        }
    }


    public ArrayList loadAll()
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).loadAll();
        }
        catch (Exception ex)
        {
            throw new SAException("loadAll() error (uri: " + uri, ex);
        }
    }



    public ArrayList getBlockedMembers(int memberId)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getBlockedMembers(memberId);
        }
        catch (Exception ex)
        {
            throw new SAException("getBlockedMembers() error (uri: " + uri, ex);
        }
    }

    public void saveBlockedMembers(int memberId, ArrayList blockedMemberIds)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            getService(uri).saveBlockedMembers(memberId, blockedMemberIds);
        }
        catch (Exception ex)
        {
            throw new SAException("saveBlockedMembers() error (uri: " + uri, ex);
        }
    }

    public int numDinAttend(MemberVO member)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).numDinAttend(member);
        }
        catch (Exception ex)
        {
            throw new SAException("numDinAttend() error (uri: " + uri, ex);
        }
    }


    public double getPercentFulfillment(MemberVO member)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getPercentFulfillment(member);
        }
        catch (Exception ex)
        {
            throw new SAException("getPercentFulfillment() error (uri: " + uri, ex);
        }
    }


    public EventVO getLastEventAttended(MemberVO member)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getLastEventAttended(member);
        }
        catch (Exception ex)
        {
            throw new SAException("getLastEventAttended() error (uri: " + uri, ex);
        }
    }


    public int getNumNoShows(MemberVO member)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getNumNoShows(member);
        }
        catch (Exception ex)
        {
            throw new SAException("getNumNoShows() error (uri: " + uri, ex);
        }
    }


    public ArrayList getVenuesAttended(MemberVO member)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getVenuesAttended(member);
        }
        catch (Exception ex)
        {
            throw new SAException("getVenuesAttended() error (uri: " + uri, ex);
        }
    }


    public bool sparkIdAlreadyUsed(MemberVO member, int sparkId)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).sparkIdAlreadyUsed(member, sparkId);
        }
        catch (Exception ex)
        {
            throw new SAException("sparkIdAlreadyUsed() error (uri: " + uri, ex);
        }
    }


    public bool emailAddressAlreadyUsed(MemberVO member, string emailaddr)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).emailAddressAlreadyUsed(member, emailaddr);
        }
        catch (Exception ex)
        {
            throw new SAException("emailAddressAlreadyUsed() error (uri: " + uri, ex);
        }
    }




	private IMemberService getService(string uri)
	{
		try
		{
			IMemberService service = (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
		    return service;
        }
		catch(Exception ex)
		{
			throw new SAException("Cannot activate remote service manager at " + uri, ex);
		}
	}
}
}
