using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceAdapters
{
public class HostSA : IHostService
{
	public static readonly HostSA Instance = new HostSA();

	private const string SERVICE_MANAGER_NAME = "HostSM";

	private HostSA()
	{
	}

    public HostVO load(int id)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).load(id);
        }
        catch (Exception ex)
        {
            throw new SAException("load() error (uri: " + uri, ex);
        }
    }


    public int save(HostVO host)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).save(host);
        }
        catch (Exception ex)
        {
            throw new SAException("save() error (uri: " + uri, ex);
        }
    }


    public void delete(int id)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            getService(uri).delete(id);
        }
        catch (Exception ex)
        {
            throw new SAException("delete() error (uri: " + uri, ex);
        }
    }


    /**
     * Gets a dataset of all the hosts
     */
    public DataSet getDataSet()
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getDataSet();
        }
        catch (Exception ex)
        {
            throw new SAException("getDataSet() error (uri: " + uri, ex);
        }
    }

    /**
     * Get the Events the Host hosted
     * Returns ArrayList[EventVO]
     */
    public ArrayList getEventsHosted(HostVO hostVO)
    {
        string uri = "";
        try
        {
            uri = SMUtil.getServiceManagerUri(SERVICE_MANAGER_NAME);
            return getService(uri).getEventsHosted(hostVO);
        }
        catch (Exception ex)
        {
            throw new SAException("getEventsHosted() error (uri: " + uri, ex);
        }
    }

	private IHostService getService(string uri)
	{
		try
		{
			return (IHostService)Activator.GetObject(typeof(IHostService), uri);
		}
		catch(Exception ex)
		{
			throw new SAException("Cannot activate remote service manager at " + uri, ex);
		}
	}
}
}
