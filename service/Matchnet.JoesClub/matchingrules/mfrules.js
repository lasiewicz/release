class JScriptRuleFile
{
    // getScore must return one of these values.
    var GradeNever = "0";
    var GradeAlways = "1";
    var GradeA = "A";
    var GradeB = "B";
    var GradeC = "C";
    var GradeD = "D";
    var GradeF = "F";

    
    // indicates a value missing from the database
    var NoValue = -1;

    // globals
    var _female;
    var _male;
    var _ruleContext;
    var _event;
    
    
    // Entry point.  Calls each rule
    public function getScore(female, male, ruleContext)
    { 
        _female = female;
        _male = male;
        _ruleContext = ruleContext;
        _event = _ruleContext.theEvent;
        
        // run each rule 
        block();
        satTogether();
        desiredAge(2);
        height(1);
        kids(1, 3);
        //jdateReligion(0);
    }
    
    
    // see if members block each other
    public function block()
    { 
        if (_event.blocked(_male, _female))
            _ruleContext.addResult("block", GradeNever, 1, "These members are blocked from sitting together.");
    }
    

    // see if members sat together
    public function satTogether()
    { 
        var when = _event.satTogether(_male, _female);
        if (when)
            _ruleContext.addResult("satTogether", GradeNever, 1, "These members sat together on " + when + ".");
    }


    // desiredAge
    public function desiredAge(weight)
    { 
        if (_male.age == NoValue || 
            _female.age == NoValue)
            return;
        
        if (_male.desiredAgeLow == NoValue)
            _male.desiredAgeLow = _male.age - 10;
        if (_male.desiredAgeHigh == NoValue)
            _male.desiredAgeHigh = _male.age;
        if (_female.desiredAgeLow == NoValue)
            _female.desiredAgeLow = _female.age - 5;
        if (_female.desiredAgeHigh == NoValue)
            _female.desiredAgeHigh = _female.age + 5;
        
        var d1 =  _female.age - _male.desiredAgeHigh;
        var d2 =  _male.desiredAgeLow - _female.age;
        var d3 =  _male.age - _female.desiredAgeHigh;
        var d4 =  _female.desiredAgeLow - _male.age;
        
        var dMax = d1;
        if (d2 > dMax)
            dMax = d2;
        if (d3 > dMax)
            dMax = d3;
        if (d4 > dMax)
            dMax = d4;
        
        var grade;
        var explanation;
        if (dMax <= 0)
        {
            grade = GradeA;
            explanation = "They are within each other's desired age range.";
        }
        else if (dMax <= 1)
        {
            grade = GradeB;
            explanation = "They are outside each other's desired age range by 1 year.";
        }
        else if (dMax <= 2)
        {
            grade = GradeC;
            explanation = "They are outside each other's desired age range by 2 years.";
        }
        else if (dMax <= 3)
        {
            grade = GradeF;
            explanation = "They are outside each other's desired age range by 3 years.";
        }
        else
        {
            grade = GradeNever;
            explanation = "They are outside each other's desired age range by more than 3 years.";
        }
            
        _ruleContext.addResult("desiredAge", grade, weight, explanation);
    }


    // height
    public function height(weight)
    { 
        if (_male.height == NoValue || _female.height == NoValue)
            return;

        var d =  _male.height - _female.height;
        var grade;
        var explanation;
        if (d >= 4)
        {
            grade = GradeA; 
            explanation = "The man is at least 4 inches taller.";
        }
        else if (d >= 3)
        {
            grade = GradeB; 
            explanation = "The man is 3 inches taller.";
        }
        else if (d >= 2)
        {
            grade = GradeC; 
            explanation = "The man is 2 inches taller.";
        }
        else if (d >= 0)
        {
            grade = GradeD; 
            explanation = "The man is 0 or 1 inches taller.";
        }
        else
        {
            grade = GradeF; 
            explanation = "The man is shorter.";
        }
            
        _ruleContext.addResult("height", grade, weight, explanation);
    }


    // kids.  We weight this rule differently depending on whether or
    // not the member's kid preferences are compatible because it's good
    // if they are compatible, but it's very bad if they're not.
    public function kids(compatibleWeight, incompatibleWeight)
    { 
        var explanation = _male.kidPrefsIncompatible(_female);
        if (explanation != null)
            _ruleContext.addResult("kids", GradeF, incompatibleWeight, explanation);
        else
            _ruleContext.addResult("kids", GradeA, compatibleWeight, 
                "Their kid preferences are compatible.");
    }
    

    // jdateReligion
    var Blank = 0;
    public function jdateReligion(weight)
    { 
        if (_male.jdateReligion == Blank ||
            _female.jdateReligion == Blank)
            return; 

        var grade;
        if (_male.jdateReligion == _female.jdateReligion)
            grade = GradeA; 

        else if (_male.jdateReligionMatches(_female) &&
            _female.jdateReligionMatches(_male))
            grade = GradeB; 
            
        else 
            grade = GradeD;
            
        _ruleContext.addResult("jdateReligion", grade, weight, "jdateReligion");
    }


    // test
    public function test(weight)
    { 
        _ruleContext.addResult("test", GradeF, weight, "test");
    }
}
    