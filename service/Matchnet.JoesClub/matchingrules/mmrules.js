class JScriptRuleFile
{
    // getScore must return one of these values.
    var GradeNever = "0";
    var GradeAlways = "1";
    var GradeA = "A";
    var GradeB = "B";
    var GradeC = "C";
    var GradeD = "D";
    var GradeF = "F";

    
    // indicates a value missing from the database
    var NoValue = -1;

    // globals
    var _member1;
    var _member2;
    var _ruleContext;
    var _event;
    
    
    // Entry point.  Calls each rule
    public function getScore(member1, member2, ruleContext)
    { 
        _member1 = member1;
        _member2 = member2;
        _ruleContext = ruleContext;
        _event = _ruleContext.theEvent;
        
        block();
        age(1);
    }
    

    // see if members block each other
    public function block()
    { 
        if (_event.blocked(_member1, _member2))
            _ruleContext.addResult("block", GradeNever, 1, "");
    }
    
   
    // age rule
    public function age(weight)
    { 
        var grade;
        var explanation;
        
        if (_member1.age == NoValue || 
            _member2.age == NoValue)
            return;
        
        var d = Math.abs(_member1.age - _member2.age);
        if (d <= 3)
        {
            grade = GradeA;
            explanation = "Their age difference is 3 years or less.";
        }
        else if (d <= 5)
        {
            grade = GradeB;
            explanation = "Their age difference is 4 or 5 years.";
        }
        else if (d <= 7)
        {
            grade = GradeC;
            explanation = "Their age difference is 6 or 7 years.";
        }
        else if (d <= 9)
        {
            grade = GradeD;
            explanation = "Their age difference is 8 or 9 years.";
        }
        else
        {
            grade = GradeF;
            explanation = "Their age difference is 10 or more years.";
        }
               
        _ruleContext.addResult("age", grade, weight, explanation);
    }

}
