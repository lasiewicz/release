using System;
using System.Collections.Specialized;

using Matchnet.Exceptions;
using Matchnet.JoesClub.BusinessLogic;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceManagers
{
public class PageHandlerSM : SMBase, IPageHandlerService
{
	public PageHandlerSM()
	{
	}


    /**
     * Handle a page request
     */
    public PageResponse processRequest(PageRequest pageRequest)
    {
        try
        {
            // this method handle Log setup, etc, so we don't need to call
            // the SMBase methods likethe other SMs do.
            return PageHandlerBL.processRequest(pageRequest);
        }
        catch (Exception ex)
        {
            throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                "processRequest() error (pageName: " + pageRequest.pageName + ").",
                ex);
        }
    }
}
}
