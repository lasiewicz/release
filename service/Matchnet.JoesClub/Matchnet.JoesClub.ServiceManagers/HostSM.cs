using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.JoesClub.BusinessLogic;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceManagers
{
public class HostSM : SMBase, IHostService
{
    private static Log _log = new Log("HostSM");


    /**
     * Load the Host
     */
    public HostVO load(int id)
    {
        try
        {
            initCall();
            return HostBL.Instance.load(id);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "load");
        }
        finally
        {
            finishCall();
        }
    }

    /**
     * save the Host
     */
    public int save(HostVO host)
    {
        try
        {
            initCall();
            return HostBL.Instance.save(host);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "save");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * delete the Host
     */
    public void delete(int id)
    {
        try
        {
            initCall();
            HostBL.Instance.delete(id);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "delete");
        }
        finally
        {
            finishCall();
        }
    }



    /**
     * Gets a dataset of all the hosts
     */
    public DataSet getDataSet()
    {
        try
        {
            initCall();
            return HostBL.Instance.getDataSet();
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getDataSet");
        }
        finally
        {
            finishCall();
        }
    }

    /**
     * Get the Events the Host hosted
     * Returns ArrayList[EventVO]
     */
    public ArrayList getEventsHosted(HostVO hostVO)
    {
        try
        {
            initCall();
            return HostBL.Instance.getEventsHosted(hostVO);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getEventsHosted");
        }
        finally
        {
            finishCall();
        }
    }

}
}
