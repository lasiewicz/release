using System;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.BusinessLogic;
using Matchnet.Exceptions;


namespace Matchnet.JoesClub.ServiceManagers
{
public class SMBase : MarshalByRefObject
{

    /**
     * Need this so the service doesn't stop running
     */
    public override object InitializeLifetimeService() {return null;}




    /**
     * Do set-up before using service code
     */
    public void initCall()
    {
        LogFile.open();
        DbObjectCache.create();
    }


    /**
     * Do set-up before using service code
     */
    public ServiceBoundaryException handleError(Log log, Exception ex, string method)
    {
        log.error(ex);
        return new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
            method + "() error", ex);
    }


    /**
     * Do set-up before using service code
     */
    public void finishCall()
    {
        // close logfile
        LogFile.close();

        // destroy the DbObjectCache
        DbObjectCache.destroy();
    }


}
}
