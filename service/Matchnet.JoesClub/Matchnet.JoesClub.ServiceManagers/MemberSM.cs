using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.JoesClub.BusinessLogic;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceManagers
{
public class MemberSM : SMBase, IMemberService
{
    private static Log _log = new Log("MemberSM");


    /**
     * Load the Member
     */
    public MemberVO load(int id)
    {
        try
        {
            initCall();
            return MemberBL.Instance.load(id);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "load");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * save the Member
     */
    public int save(MemberVO member)
    {
        try
        {
            initCall();
            return MemberBL.Instance.save(member);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "save");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * delete the Member
     */
    public void delete(int id)
    {
        try
        {
            initCall();
            MemberBL.Instance.delete(id);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "delete");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * Gets a dataset of all the members
     */
    public DataSet getDataSet()
    {
        try
        {
            initCall();
            return MemberBL.Instance.getDataSet();
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getDataSet");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * Gets a dataset of all the members with the given name
     */
	public DataSet getDataSetByName(string name)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getDataSetByName(name);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getDataSetByName");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * Gets a dataset of all the members with the given email
     */
	public DataSet getDataSetByEmail(string email)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getDataSetByEmail(email);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getDataSetByEmail");
        }
        finally
        {
            finishCall();
        }
    }


    public ArrayList loadPhoneInterviewsForMember(int memberId)
    {
        try
        {
            initCall();
            return MemberBL.Instance.loadPhoneInterviewsForMember(memberId);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "loadPhoneInterviewsForMember");
        }
        finally
        {
            finishCall();
        }
    }


    public ArrayList loadAll()
    {
        try
        {
            initCall();
            return MemberBL.Instance.loadAll();
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "loadAll");
        }
        finally
        {
            finishCall();
        }
    }

    

    public ArrayList getBlockedMembers(int memberId)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getBlockedMembers(memberId);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getBlockedMembers");
        }
        finally
        {
            finishCall();
        }
    }

    public void saveBlockedMembers(int memberId, ArrayList blockedMemberIds)
    {
        try
        {
            initCall();
            MemberBL.Instance.saveBlockedMembers(memberId, blockedMemberIds);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "saveBlockedMembers");
        }
        finally
        {
            finishCall();
        }
    }


    public int numDinAttend(MemberVO member)
    {
        try
        {
            initCall();
            return MemberBL.Instance.numDinAttend(member);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "numDinAttend");
        }
        finally
        {
            finishCall();
        }
    }


    public double getPercentFulfillment(MemberVO member)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getPercentFulfillment(member);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getPercentFulfillment");
        }
        finally
        {
            finishCall();
        }
    }


    public EventVO getLastEventAttended(MemberVO member)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getLastEventAttended(member);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getLastEventAttended");
        }
        finally
        {
            finishCall();
        }
    }


    public int getNumNoShows(MemberVO member)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getNumNoShows(member);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getNumNoShows");
        }
        finally
        {
            finishCall();
        }
    }


    public ArrayList getVenuesAttended(MemberVO member)
    {
        try
        {
            initCall();
            return MemberBL.Instance.getVenuesAttended(member);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getVenuesAttended");
        }
        finally
        {
            finishCall();
        }
    }


    public bool sparkIdAlreadyUsed(MemberVO member, int sparkId)
    {
        try
        {
            initCall();
            return MemberBL.Instance.sparkIdAlreadyUsed(member, sparkId);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "sparkIdAlreadyUsed");
        }
        finally
        {
            finishCall();
        }
    }


    public bool emailAddressAlreadyUsed(MemberVO member, string emailaddr)
    {
        try
        {
            initCall();
            return MemberBL.Instance.emailAddressAlreadyUsed(member, emailaddr);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "emailAddressAlreadyUsed");
        }
        finally
        {
            finishCall();
        }
    }
}
}
