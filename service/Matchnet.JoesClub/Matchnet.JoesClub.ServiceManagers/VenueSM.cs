using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.JoesClub.BusinessLogic;
using Matchnet.JoesClub.ValueObjects;
using Matchnet.JoesClub.ValueObjects.ServiceDefinitions;


namespace Matchnet.JoesClub.ServiceManagers
{
public class VenueSM : SMBase, IVenueService
{
    private static Log _log = new Log("VenueSM");

    /**
     * Load the Venue
     */
    public VenueVO load(int id)
    {
        try
        {
            initCall();
            return VenueBL.Instance.load(id);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "load");
        }
        finally
        {
            finishCall();
        }
    }

    /**
     * save the Venue
     */
    public int save(VenueVO venue)
    {
        try
        {
            initCall();
            return VenueBL.Instance.save(venue);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "save");
        }
        finally
        {
            finishCall();
        }
    }


    /**
     * delete the Venue
     */
    public void delete(int id)
    {
        try
        {
            initCall();
            VenueBL.Instance.delete(id);
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "delete");
        }
        finally
        {
            finishCall();
        }
    }



    /**
     * Gets a dataset of all the venues
     */
    public DataSet getDataSet()
    {
        try
        {
            initCall();
            return VenueBL.Instance.getDataSet();
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getDataSet");
        }
        finally
        {
            finishCall();
        }
    }



    /**
     * Returns the set of venue cities
     */
    public string[] getVenueCities() 
    { 
        try
        {
            initCall();
            return VenueBL.Instance.getVenueCities();
        }
        catch (Exception ex)
        {
            throw handleError(_log, ex, "getVenueCities");
        }
        finally
        {
            finishCall();
        }
    }
}
}
