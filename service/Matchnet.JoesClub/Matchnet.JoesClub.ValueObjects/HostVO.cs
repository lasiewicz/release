using System;
using System.Collections;


/**
 * ValueObject corresponding to Matchnet.JoesClub.BusinessLogic.Host
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class HostVO : DbObjectVO
{
    private string _firstName;
    private string _lastName;
    private string _image;
    private string _phone;
    private string _phone2;
    private string _emailAddress;
    private int _availableNights; // BitMask.daysOfWeek
    private string _manager;
    private string _notes;

    public string firstName { get { return _firstName; } set { _firstName = value; } }
    public string lastName { get { return _lastName; } set { _lastName = value; } }
    public string image { get { return _image; } set { _image = value; } }
    public string phone { get { return _phone; } set { _phone = value; } }
    public string phone2 { get { return _phone2; } set { _phone2 = value; } }
    public string emailAddress { get { return _emailAddress; } set { _emailAddress = value; } }
    public int availableNights { get { return _availableNights; } set { _availableNights = value; } }
    public string manager { get { return _manager; } set { _manager = value; } }
    public string notes { get { return _notes; } set { _notes = value; } }


    /**
     * Constructor
     */
    public HostVO(
        int id, 
        string firstName,
        string lastName,
        string image,
        string phone,
        string phone2,
        string emailAddress,
        int availableNights, 
        string manager,
        string notes) : base(id)
    {
        _firstName = firstName;
        _lastName = lastName;
        _image = image;
        _phone = phone;
        _phone2 = phone2;
        _emailAddress = emailAddress;
        _availableNights = availableNights;
        _manager = manager;
        _notes = notes;
    }

}
}
