using System;
using System.Collections.Specialized;


namespace Matchnet.JoesClub.ValueObjects.ServiceDefinitions
{
	public interface IPageHandlerService
	{
        PageResponse processRequest(PageRequest pageRequest);
	}
}
