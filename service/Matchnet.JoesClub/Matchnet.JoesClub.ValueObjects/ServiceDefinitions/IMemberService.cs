using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;


namespace Matchnet.JoesClub.ValueObjects.ServiceDefinitions
{
public interface IMemberService
{
    MemberVO load(int id);
    int save(MemberVO member);
    void delete(int id);
    DataSet getDataSet();
	DataSet getDataSetByName(string name);
	DataSet getDataSetByEmail(string email);
    ArrayList loadPhoneInterviewsForMember(int memberId);
    ArrayList loadAll();
    ArrayList getBlockedMembers(int memberId);
    void saveBlockedMembers(int memberId, ArrayList blockedMemberIds);
    int numDinAttend(MemberVO member);
    double getPercentFulfillment(MemberVO member);
    EventVO getLastEventAttended(MemberVO member);
    int getNumNoShows(MemberVO member);
    ArrayList getVenuesAttended(MemberVO member);
    bool sparkIdAlreadyUsed(MemberVO member, int sparkId);
    bool emailAddressAlreadyUsed(MemberVO member, string emailaddr);
}
}
