using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;


namespace Matchnet.JoesClub.ValueObjects.ServiceDefinitions
{
public interface IHostService
{
    HostVO load(int id);
    int save(HostVO host);
    void delete(int id);
    ArrayList getEventsHosted(HostVO hostVO);
    DataSet getDataSet();
}
}
