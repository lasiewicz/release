using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;


namespace Matchnet.JoesClub.ValueObjects.ServiceDefinitions
{
public interface IVenueService
{
    VenueVO load(int id);
    int save(VenueVO venue);
    void delete(int id);
    DataSet getDataSet();
    string[] getVenueCities();
}
}
