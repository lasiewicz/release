using System;

namespace Matchnet.JoesClub.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "JOESCLUB_SVC";
		public const string SERVICE_NAME = "Matchnet.JoesClub.Service";

		private ServiceConstants()
		{
		}
	}
}
