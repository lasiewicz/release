/**
 *  Object which can log themselves implement this
 */ 
namespace Matchnet.JoesClub.ValueObjects
{
public interface Logable
{
    void log(ObjectLogger objectLogger);
}

}