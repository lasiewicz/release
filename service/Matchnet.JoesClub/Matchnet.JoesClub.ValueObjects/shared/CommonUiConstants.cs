
/**
 * Constants used by UI files
 */
namespace Matchnet.JoesClub.ValueObjects
{
public class CommonUiConstants
{
    // admin page names
    public const string AdminPage = "Admin.aspx";
    public const string EventsPage = "Events.aspx";
    public const string EditEventPage = "EditEvent.aspx";
    public const string MemberGridPage = "MemberGrid.aspx";
    public const string TablesPage = "Tables.aspx";
    public const string TestPage = "Test.aspx";
    public const string EmailsPage = "Emails.aspx";
    public const string EmailDetailsPage = "EmailDetails.aspx";
    public const string CompExplPage = "CompExpl.aspx";
    public const string EventInfoByVenueAjax = "EventInfoByVenueAjax.aspx";
    public const string JsRequest = "JsRequest.aspx";
    public const string MemberCalendarPage = "MemberCalendar.aspx";
    public const string PostEventPage = "PostEvent.aspx";
    public const string AlertsPage = "Alerts.aspx";
    public const string AdminLogonPage = "AdminLogon.aspx";
    public const string IndexPage = "Index.aspx";

    // UX page names
    public const string UserCalendarPage = "UserCalendar.aspx";
    public const string UserCalendarPage2 = "UserCalendar2.aspx";
    public const string EditPreferencesPage = "EditPreferences.aspx";
    public const string MyAccountPage = "MyAccount.aspx";
    public const string ApplicationPage = "Application.aspx";
    public const string ApplicationConfPage = "ApplicationConf.aspx";
	public const string FaqPage = "Faq.aspx";
	public const string TutorialPage = "Tutorial.aspx";
	public const string RsvpAjaxPage = "RsvpAjax.aspx";
    public const string SendConfirmEmailAjaxPage = "SendConfirmEmailAjax.aspx";
    public const string VenueDetailsAjaxPage = "VenueDetailsAjaxPage.aspx";
    public const string OptOutPage = "OptOut.aspx";
    public const string EventMemberTableAjax = "EventMemberTableAjax.aspx";
    public const string LogonPage = "Logon.aspx";
	public const string InvitationPage = "Invitation.aspx";
	public const string InviteAcceptedPage = "InviteAccepted.htm";
	public const string InviteDeclinedPage = "InviteDeclined.htm";
	public const string WelcomePage = "Welcome.aspx";

    // params
    public const string ActionParam = "action";

}
}