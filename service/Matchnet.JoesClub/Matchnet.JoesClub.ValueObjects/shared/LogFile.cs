using System;
using System.IO; // StreamWriter


/**
 * Manages the log file
 */
namespace Matchnet.JoesClub.ValueObjects
{
public class LogFile
{
    //private const string path = "c:/jflight/temp/jlf.txt";

    private static int _refCount = 0;
    private static StreamWriter _streamWriter;


    /**
     * returns a StreamWriter
     */
    public static void open()
    {
        if (_streamWriter == null)
            openStreamWriter();
        if (_streamWriter != null)
            ++_refCount;
    }


    /**
     * close the file
     */
    public static void close()
    {
        if (_streamWriter != null)
            --_refCount;
        if (_refCount == 0 && _streamWriter != null)
        {
            _streamWriter.Close();
            _streamWriter = null;
        }
    }


    /**
     * returns a StreamWriter
     */
    public static void reset()
    {
        if (_streamWriter == null)
            return;
        _streamWriter.Close();
        openStreamWriter();
    }


    private static void openStreamWriter()
    {
        try
        {
            string path = Config.getProperty("LogFile");
            _streamWriter = System.IO.File.AppendText(path);
        }

        // if we can't open the file, just fail silently
        catch 
        {
            _streamWriter = null;
        }
    }


    /**
     * returns a StreamWriter
     */
    public static void write(string s)
    {
        if (_streamWriter == null)
            return;
        _streamWriter.Write(s);
        _streamWriter.Flush();
    }


    /**
     * returns a StreamWriter
     */
    public static void writeln(string s)
    {
        write(s + "\n");
    }
}
}