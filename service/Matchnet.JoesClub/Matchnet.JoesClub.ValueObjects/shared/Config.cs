using System;
using System.Collections;
using System.Configuration;
using System.IO; 
using System.Web; 
using System.Xml; 
using Matchnet.JoesClub.ValueObjects;


/**
 * Read config.xml
 */
namespace Matchnet.JoesClub.ValueObjects
{
public class Config
{

    /**
     * returns a the property for the given name
     */
    public static string getProperty(string name)
    {
        return ConfigurationSettings.AppSettings[name];
    }

    /**
     * returns a bool property for the given name
     */
    public static bool getBoolProperty(string name)
    {
        string sValue = getProperty(name);
        if (sValue == null)
            return false;
        return bool.Parse(sValue);
    }

    /**
     * returns a int property for the given name
     */
    public static int getIntProperty(string name)
    {
        string sValue = getProperty(name);
        if (sValue == null)
            return CommonConstants.NoValueInt;
        return int.Parse(sValue);
    }


    /**
     * returns the log level for the given className
     */
    public static int getLogSeverity(string className)
    {
        string sSeverity = Config.getProperty("logSeverity." + className);
        if (sSeverity == null || sSeverity == "")
            sSeverity = Config.getProperty("logSeverity.default");

        return Log.severityStringToInt(sSeverity);
    }


    /**
     * Get a TimeSpan
     */
    public static TimeSpan getTimeSpan(string amountKey, string unitsKey)
    {
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        
        int amount = Config.getIntProperty(amountKey);
        string units = Config.getProperty(unitsKey);
        units = units.Trim().ToLower();
        if (units.StartsWith("day"))
            days = amount;
        else if (units.StartsWith("hour"))
            hours = amount;
        else if (units.StartsWith("minute"))
            minutes = amount;
        else if (units.StartsWith("second"))
            seconds = amount;
        else
            throw new Exception("getTimeSpan() bad units: " + units);

        return new TimeSpan(days, hours, minutes, seconds);
    }
}


}