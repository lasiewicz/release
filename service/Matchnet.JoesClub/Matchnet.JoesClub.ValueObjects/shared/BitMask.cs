using System;
using System.Collections;
using System.Text;

/**
 * Used to represent a bit mask
 */
namespace Matchnet.JoesClub.ValueObjects
{

public class BitMask : ValueSet
{

    /**
     * Constructor.
     * 
     * values must single bits, i.e. 1, 2, 4, 8, etc.  No zero, no combinations
     * of bits.
     */ 
    public BitMask(object[] values, string[] names) :
        base(values, names, CommonConstants.EmptyMask)
    {
        int mask = 0;
        foreach (int value in values)
        {
            if ((value & mask) != 0)
                throw new Exception("BitMask() bad values.");
            mask = mask | value;
        }
    }


    /**
     * true if value is in the mask
     */
    public bool inMask(int value, int mask)
    {
        if (mask == CommonConstants.NoValueInt)
            mask = CommonConstants.EmptyMask;
        if (value == CommonConstants.NoValueInt)
            value = CommonConstants.EmptyMask;

        return (value & mask) != 0;
    }


    /**
     * Or's all the values together
     */
    public int getAllMask()
    {
        int allMask = 0;
        foreach (int value in _values)
            allMask |= value;
        return allMask;
    }




    /**
     * This is to let BitMask use 0 instead of -1 for no value.
     */
    protected override int getNoValueInt()
    {
        return CommonConstants.EmptyMask;
    }


    /**
     * Return the mask values as a comma separated string
     */
    public string getCommaSepString(int mask)
    {
        ArrayList strs = new ArrayList();
        foreach (int bit in _values)
        {
            if ((bit & mask) != 0)
                strs.Add(getName(bit));
        }

        if (strs.Count == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        sb.Append(strs[0]);
        for (int i = 1; i < strs.Count; i++)
        {
            sb.Append(", ");
            sb.Append(strs[i]);
        }

        return sb.ToString();
    }


}

}