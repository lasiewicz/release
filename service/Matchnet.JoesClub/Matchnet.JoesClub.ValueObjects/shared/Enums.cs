using System;
using System.Collections;


/**
 * Used to represent a set of values
 */
namespace Matchnet.JoesClub.ValueObjects
{

    #region Member Enums


/**
 * MemberActivityStatus enum
 */
public enum MemberActivityStatus : int
{
	Applicant           = 10,
	Inactive            = 20,
	Member              = 30,
	Pending             = 40,
	Substitute          = 50,
	Unreachable			= 60
}


/**
 * Smoking enum
 */
public enum MemberSmokingHabits : int
{
	NonSmoker           = 2,
	OccasionalSmoker    = 4,
	SmokesRegularly     = 8,
	TryingToQuit        = 16
}

/**
 * Drinking enum
 */
public enum Drinking : int
{
	Never         = 2,
	Socially      = 4,
	Occasionally  = 8,
	Frequently    = 16
}

/**
 * WillDateAParent enum
 */
public enum WillDateAParent : int
{
	NoValue         = CommonConstants.NoValueInt,
	Yes             = 1,
	No              = 2,
    NoPreference    = 3
}

/**
 * KidsPref enum
 */
public enum KidsPref : int
{
	WantsKids       = 1,
	DoesntWantKids  = 1 << 1,
	Maybe           = 1 << 2
}


/**
 * JDateReligion enum
 */
public enum JDateReligion : int
{
    OrthodoxBaalTeshuva    = 1,
    Conservative           = 2,
    Conservadox            = 4,
    Hassidic               = 8,
    ModernOrthodox         = 16,
    OrthodoxFrum           = 32,
    AnotherStream          = 64,
    Reconstructionist      = 128,
    Reform                 = 256,
    Secular                = 512,
    Traditional            = 1024,
    Unaffiliated           = 2048,
    Religious              = 4096,
}

/**
 * MeetingSomeone enum
 */
public enum MeetingSomeone : int
{
	NoValue                = CommonConstants.NoValueInt,
	CompletelyComfortable  = 1,
	SomewhatComfortable    = 2,
	TakesALittleWhile      = 3
}

/**
 * DescribeMe enum
 */
public enum DescribeMe : int
{
    Witty = 1 << 0,
    Bold = 1 << 1,
    Charming = 1 << 2,
    Charismatic = 1 << 3,
    Sincere = 1 << 4,
    Practical = 1 << 5,
    Independent = 1 << 6,
    Confident = 1 << 7,
    Sensitive = 1 << 8,
    Optimistic = 1 << 9,
    Adventurous = 1 << 10,
    Grounded = 1 << 11,
    Creative = 1 << 12,
    Insightful = 1 << 13,
    Stylish = 1 << 14,
    Brave = 1 << 15,
    Social = 1 << 16,
    Considerate = 1 << 17,
    Intelligent = 1 << 18,
    Warm = 1 << 19,
    Carefree = 1 << 20,
    Reserved = 1 << 21,
    Quirky = 1 << 22
}


/**
 * BodyType enum
 */
public enum BodyType : int
{
    Athletic    = 1,
    Average     = 1 << 1,
    Curvy       = 1 << 2,
    Heavy       = 1 << 3
}


/**
 * MemberEducation enum
 */
public enum MemberEducation : int
{
    Elementary      = 2,
    HighSchool      = 4,
    SomeCollege     = 8,
    BachelorsDegree = 16,
    MastersDegree   = 32,
    Doctorate       = 64,
    AssociateDegree = 128
}



/**
 * MemberHearAboutJoesClub enum
 */
public enum MemberHearAboutJoesClub : int
{
	NoValue        = CommonConstants.NoValueInt,
	Email = 1,
	Friend = 2,
	Jdate = 3,
	PrintAd = 4,
	SearchEngine = 5,
	LifestyleMagazine = 6,
	JewishJournal = 7,
	Temple = 8,
	JewishOrganization = 9
}

public enum MemberDiscussed : int
{
		WhatIsJoesClub = 1,
		Scheduling = 2,
		Frequency = 4,
		Events = 8,
		Matchmaking = 16,
		Price = 32,
		Selection = 64,
		Interview = 128
}

public enum MemberReservations : int
{
		TimeCommitment = 1,
		MonthToMonth = 2,
		Price = 4,
		Expensive = 8,
		Matching = 16,
		Busy = 32,
		DoNotNeed = 64
}

/**
 * MemberWhenToCall enum
 */
public enum MemberWhenToCall : int
{
	NoValue        = CommonConstants.NoValueInt,
	Morning = 0,
	Midday = 1,
	Evening = 2
}


/**
 * MemberWhatAppealedToYou enum
 */
public enum MemberWhatAppealedToYou : int
{
	NoValue        = CommonConstants.NoValueInt,
	ConvenienceAndEfficiency = 1,
	EasyAndFun = 2,
	HavingPlans = 3,
	Meeting5NewPeopleAtOnce = 4,
	TryingNewRestaurants = 5
}


/**
 * MemberLoveMostAboutJDate enum
 */
public enum MemberLoveMostAboutJDate : int
{
	NoValue        = CommonConstants.NoValueInt,
	Selection = 1,
	Convenience = 2,
	EaseOfUse = 3,
	Cost = 4,
	Anonymity = 5
}

/**
 * MemberBiggestConcern enum
 */
public enum MemberBiggestConcern : int
{
	NoValue        = CommonConstants.NoValueInt,
	Control = 1,
	Cost = 2,
	Pressure = 3,
	TimeSpent = 4,
	PeopleNotWhoTheySayTheyAre = 5
}


/**
 * MemberPeopleNotice enum
 */
public enum MemberPeopleNotice : int
{
	NoValue        = CommonConstants.NoValueInt,
	MyEyes = 1,
	MySmile = 2,
	MySenseOfHumor = 3,
	MyBrains = 4,
	MyPersonality = 5
}


/**
 * MemberOccupation enum
 */
public enum MemberOccupation : int
{
	NoValue   = CommonConstants.NoValueInt,
    AdministrativeSupportServices = 1,
    AdvertisingMarketingPR = 2,
    ArchitectureInteriorDesignLandscaping = 3,
    ArtistsMusicianWriter = 4,
    BankingFinancialRealEstate = 5,
    Communication = 6,
    Computers = 7,
    DesignVisualandgraphicarts = 8,
    EducationTeachingAcademicresearch = 9,
    Entertainment = 10,
    EntrepreneurialStartup = 11,
    EnvironmentalServices = 12,
    ExecutiveGeneralManagementConsulting = 13,
    FashionStyleApparelBeauty = 14,
    FinancialAccounting = 15,
    GovernmentCivilServicePublicPolicy = 16,
    HealthFitness = 17,
    HospitalityTravel = 18,
    InternetECommerceTechnology = 19,
    LawEnforcementMilitarySecurity = 20,
    LawLegalJudiciary = 21,
    ManufacturingDistribution = 22,
    MedicalDentalHealthcare = 23,
    NewMedia = 24,
    NonProfitVolunteerActivist = 25,
    PoliticalGovernmentMilitary = 26,
    ProductionFilmTV = 27,
    PsychologistTherapistSocialServices = 28,
    PublicsafetyFireParamedic = 29,
    PublishingPrintMedia = 30,
    RealEstate = 31,
    RestaurantFoodServicesCatering = 32,
    Retired = 33,
    SalesRepresentativeWholesaleRetail = 34,
    Student = 35,
    TechnicalScienceEngineering = 36,
    VeterinaryServices = 37
}


/**
 * MemberLocationPref bit mask enum
 */
public enum MemberLocationPref : int
{
    TheWestside = 1 << 0,
    BeverlyHills = 1 << 1,
    Hollywood = 1 << 2,
    TheValley = 1 << 3,
    OC = 1 << 4
}


/**
 * MemberTimePref bit mask enum
 */
public enum MemberTimePref : int
{
    TimePreference6PM = 1 << 0,
    TimePreference7PM = 1 << 1,
    TimePreference8PM = 1 << 2,
    TimePreference9PM = 1 << 3
}


/**
 * MemberDietaryRestrictions bit mask enum
 */
public enum MemberDietaryRestrictions : int
{
    Vegetarian = 1 << 0,
    Noredmeat = 1 << 1,
    Nopork = 1 << 2,
    Noshellfish = 1 << 3,
    Kosher = 1 << 4
}


/**
 * MemberCuisinesToAvoid bit mask enum
 */
public enum MemberCuisinesToAvoid : int
{
    Thai = 1 << 0,
    Japanese = 1 << 1,
    Italian = 1 << 2,
    Indian = 1 << 3,
    Meat = 1 << 4
}


/**
 * MemberMaritalStatus bit mask enum
 */
public enum MemberMaritalStatus : int
{
    Single     = 2,
    Divorced   = 8,
    Separated  = 16,
    Widowed    = 32
}


/**
 * I like to go out to:
 */
public enum MemberSpendTime : int
{
	NoValue                = CommonConstants.NoValueInt,
    Restaurants = 1,
    ClubsDancing = 2,
    CharityVolunteer = 3,
    Sportsevents = 4,
    ConcertsTheaterArtopenings = 5
}


/**
 * MemberChildrenCount
 */
public enum MemberChildrenCount : int
{
	NoValue     = CommonConstants.NoValueInt,
    None        = 0,
    One         = 1,
    Two         = 2,
    ThreeOrMore = 3
}


/**
 * MemberRelationship
 */
public enum MemberRelationship : int
{
	NoValue                 = CommonConstants.NoValueInt,
    Date                    = 2,
    Friend                  = 4,
    Marriage                = 8,
    MarriageAndChildren     = 16,
    LongTermRelationship    = 32,
    ActivityPartner         = 64
}


    #region MemberPersonalityQuestion Enums

/**
 * 1. Who would you most like to sit next to on a plane?
 */
public enum MemberPersonalityQuestion1 : int
{
	NoValue                = CommonConstants.NoValueInt,
    JonStewart = 1,
    HowardStern = 2,
    MadeleineAlbright = 3,
    SigmundFreud = 4,
    PhilipRoth = 5,
    HenryKissinger = 6
}

/**
 * 2. What superhero force would you most want?
 * 
 */
public enum MemberPersonalityQuestion2 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Flight = 1,
    Invisibility = 2,
    XRayvision = 3,
    Mindreading = 4,
    Cripplingsenseofhumor = 5
}

/**
 * 3. What�s your can�t-live-without-it gadget?
 * 
 */
public enum MemberPersonalityQuestion3 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Cell = 1,
    iPod = 2,
    PDA = 3,
    DigitalCamera = 4,
    SuperGizmo = 5,
    Notepad = 6
}

/**
 * 4.  What�s on your bookshelf?
 * 
 */
public enum MemberPersonalityQuestion4 : int
{
	NoValue                = CommonConstants.NoValueInt,
    OscarWilde = 1,
    DavidSedaris = 2,
    JohnGrisham = 3,
    Aristotle = 4,
    NewYorker = 5,
    Pictures = 7
}

/**
 * 5. How do you work up a sweat?
 * 
 */
public enum MemberPersonalityQuestion5 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Weights = 1,
    Running = 2,
    Yoga = 3,
    Kickboxing = 4,
    Dancing = 5,
    Publicspeaking = 6
}

/**
 * 6. What�s your favorite ticket?
 * 
 */
public enum MemberPersonalityQuestion6 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Planeticket = 1,
    Operaticket = 2,
    Theaterticket = 3,
    Amusementparkticket = 4,
    Backstagepasses = 5,
    Lottoticket = 6
}

/**
 * 7. What�s your perfect apartment view?
 * 
 */
public enum MemberPersonalityQuestion7 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Oceanview = 1,
    Mountainview = 2,
    Forestview = 3,
    Penthouseview = 4,
    Poolview = 5,
    Cityview = 6
}

/**
 * 8. If you could be painted by any artist in history, who would you pick?
 * 
 */
public enum MemberPersonalityQuestion8 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Michelangelo = 1,
    Matisse = 2,
    DaVinci = 3,
    Warhol = 4,
    Picasso = 5,
    FrancisBacon = 6
}
 
/**
 * 9. What�s your getaway of choice?
 * 
 */
public enum MemberPersonalityQuestion9 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Tropicalparadise = 1,
    Exoticcity = 2,
    Safari = 3,
    Cruiseship = 4,
    CampingNature = 5,
    Workingupasweat = 6
}

/**
 * 10. If you could live forever at a certain age-range, what would it be?
 */
public enum MemberPersonalityQuestion10 : int
{
	NoValue     = CommonConstants.NoValueInt,
    GradeSchool = 1,
    JuniorHigh  = 2,
    HighSchool  = 6,
    College     = 3,
    Twenties    = 4,
    Now         = 5
}

/**
 * 11. If you could have personally witnessed any one event from history, 
 * what would it be?
 */
public enum MemberPersonalityQuestion11 : int
{
    NoValue                 = CommonConstants.NoValueInt,
    FirstOlympics           = 1,
    BuildingOfThePyramids   = 2,
    CivilWar                = 3,
    SeabiscuitVsWarAdmiral  = 4,
    BerlinWallComingDown    = 5,
    WomensRightToVote       = 6
}

/**
 * 12. If you could only keep one of your five senses, which would you save?
 * 
 */
public enum MemberPersonalityQuestion12 : int
{
	NoValue                = CommonConstants.NoValueInt,
    Sight = 1,
    Smell = 2,
    Touch = 3,
    Taste = 4,
    Hearing = 5
}

    #endregion MemberPersonalityQuestion Enums

    #endregion Member Enums

    #region Other Enums


/**
 * EventStatus enum
 */
public enum EventStatus : int
{
	New       = 0,
	Posted    = 1,
    Closed    = 2,
    Finished  = 3,
    Cancelled = 4
}


/**
 * EventMemberSeated enum
 */
public enum InviteStatus : int
{
	UnSet         = CommonConstants.NoValueInt,
	NotInvited    = 0,
    Invited       = 1,
    InviteExpired = 4
}


/**
 * MemberStatus enum
 */
public enum MemberStatus : int
{
	None        = 0,
    SignedUp    = 1,
    RsvpedYes   = 2,
    Cancelled   = 3,
    NoShow      = 4,
    Attended    = 5,
    RsvpedNo    = 6
}


/**
 * EventMemberFulfillment enum
 */
public enum EventMemberFulfillment : int
{
	None         = CommonConstants.NoValueInt,
    Fulfilled    = 1,
    Unfulfilled  = 2
}
    
    
/**
 * VenueParking enum
 */
public enum VenueParking : int
{
	Valet           = 1,
	StreetParking   = 1 << 1,
	FreeParkingLot  = 1 << 2,
	PayParkingLot   = 1 << 3
}


/**
 * VenuePriceRange enum
 */
public enum VenuePriceRange : int
{
	NoValue = CommonConstants.NoValueInt,
	High    = 1,
	Medium  = 2,
	Low     = 3
}

/**
 * Result of login attemp.
 */
public enum AuthenticationStatus : int
{
	Authenticated = 0,
	InvalidEmailAddress = -1,
	InvalidPassword = -2,
	AdminSuspended = -3,
	NotAJoesClubMember = -4,
	NotActive = -5,
	ProcessingMembership = -6,
	MemberDeleted = -7,
	NotAJDateMember = -8,
    NoAdminPriviledge = -9,
    DuplicateEmail = -10
}
    
   
/**
 * PhoneType enum
 */
public enum PhoneType : int
{
	NoValue = CommonConstants.NoValueInt,
	Work    = 0,
    Home    = 1,
    Cell    = 2
}


/**
 * DaysOfTheWeek bit mask enum
 */
public enum DaysOfTheWeek : int
{
    Monday = 1 << 0,
    Tuesday = 1 << 1,
    Wednesday = 1 << 2,
    Thursday = 1 << 3,
    Friday = 1 << 4,
    Saturday = 1 << 5,
    Sunday = 1 << 6
}
   

/**
 * InterviewStatus enum
 */
public enum InterviewStatus : int
{
	NoValue     = CommonConstants.NoValueInt,
	New         = 1,
    //CallBack    = 2,
    NoAnswer    = 3,
    LM          = 4,
    Busy        = 5,
    WrongNumber = 6,
    SpokeTo = 7
}

public enum InterviewScheduleStatus : int
{
	NoValue				= CommonConstants.NoValueInt,
	ScheduledCallback	= 1,
	Scheduled			= 2,
	Cancelled			= 3,
	Rescheduled			= 4,
	NeedsReschedule		= 5,
	NotInterested		= 6
}


    #endregion Other Enums


/*
 * This class contains a bunch of static ValueSet object
 */
public class Enums
{
    #region Member ValueSets, BitMasks


    // MemberActivityStatus
    public static ValueSet memberActivityStatus = new ValueSet(
        new object[] {
            MemberActivityStatus.Applicant,
            MemberActivityStatus.Pending,
            MemberActivityStatus.Member,
            MemberActivityStatus.Substitute,
            MemberActivityStatus.Inactive,
			MemberActivityStatus.Unreachable}, 
        new string[] {
            "Applicant",
            "Pending",
            "Member",
            "Substitute",
            "Inactive",
			"Unreachable"}, 
        MemberActivityStatus.Inactive);

    // MemberSmokingHabits
    private static string[] _smokingNames = new string[] {
        "Non-Smoker", 
        "Occasional Smoker", 
        "Smokes Regularly", 
        "Trying to quit" };
    
    private static object[] _smokingValues = new object[] {
        MemberSmokingHabits.NonSmoker,
        MemberSmokingHabits.OccasionalSmoker,
        MemberSmokingHabits.SmokesRegularly,
        MemberSmokingHabits.TryingToQuit };

    public static BitMask memberSmokingHabits = new BitMask(
        _smokingValues, _smokingNames);
    
    
    // drinking
    private static string[] _drinkingNames = new string[] {
        "Never", 
        "Socially", 
        "Occasionally", 
        "Frequently" };
    
    private static object[] _drinkingValues = new object[] {
        Drinking.Never,
        Drinking.Socially,
        Drinking.Occasionally,
        Drinking.Frequently };

    public static BitMask drinking = new BitMask(
        _drinkingValues, _drinkingNames);


    // willDateAParent
    public static ValueSet willDateAParent = new ValueSet(
        new object[] {
            WillDateAParent.Yes,
            WillDateAParent.No,
            WillDateAParent.NoPreference }, 
        new string[] {
            "Yes", 
            "No",
            "No preference"}, 
        WillDateAParent.NoValue);


    // kidsPref
    private static string[] _kidsPrefNames = new string[] {
        "Wants kids", 
        "Doesn't want kids", 
        "Maybe" };
    
    private static object[] _kidsPrefValues = new object[] {
        KidsPref.WantsKids,
        KidsPref.DoesntWantKids,
        KidsPref.Maybe };

    public static BitMask kidsPref = new BitMask(
        _kidsPrefValues, _kidsPrefNames);


    // jdateReligion
    private static string[] _jdateReligionNames = new string[] 
    {
        "Orthodox(Baal Teshuva)",
        "Conservative",
        "Conservadox",
        "Hassidic",
        "Modern Orthodox",
        "Orthodox (Frum)",
        "Another Stream",
        "Reconstructionist",
        "Reform",
        "Secular",
        "Traditional",
        "Unaffiliated",
        "Religious"
    };

    /**
    * JDateReligion enum
    */
    private static object[] _jdateReligionValues = new object[] {
        JDateReligion.OrthodoxBaalTeshuva,
        JDateReligion.Conservative,
        JDateReligion.Conservadox,
        JDateReligion.Hassidic,
        JDateReligion.ModernOrthodox,
        JDateReligion.OrthodoxFrum,
        JDateReligion.AnotherStream,
        JDateReligion.Reconstructionist,
        JDateReligion.Reform,
        JDateReligion.Secular,
        JDateReligion.Traditional,
        JDateReligion.Unaffiliated,
        JDateReligion.Religious
    };

    public static BitMask jdateReligion = new BitMask(
        _jdateReligionValues, _jdateReligionNames);


    // MeetingSomeone
    public static ValueSet meetingSomeone = new ValueSet(
        new object[] {
            MeetingSomeone.CompletelyComfortable,
            MeetingSomeone.SomewhatComfortable,
            MeetingSomeone.TakesALittleWhile }, 
        new string[] {
            "Am Completely comfortable", 
            "Am Somewhat comfortable", 
            "Takes a little while to warm up" }, 
        MeetingSomeone.NoValue);

    // DescribeMe
    public static BitMask describeMe = new BitMask(
        new object[] {
            DescribeMe.Witty,
            DescribeMe.Bold,
            DescribeMe.Charming,
            DescribeMe.Charismatic,
            DescribeMe.Sincere,
            DescribeMe.Practical,
            DescribeMe.Independent,
            DescribeMe.Confident,
            DescribeMe.Sensitive,
            DescribeMe.Optimistic,
            DescribeMe.Adventurous,
            DescribeMe.Grounded,
            DescribeMe.Creative,
            DescribeMe.Insightful,
            DescribeMe.Stylish,
            DescribeMe.Brave,
            DescribeMe.Social,
            DescribeMe.Considerate,
            DescribeMe.Intelligent,
            DescribeMe.Warm,
            DescribeMe.Carefree,
            DescribeMe.Reserved,
            DescribeMe.Quirky },
        new string[] {
            "Witty",
            "Bold",
            "Charming",
            "Charismatic",
            "Sincere",
            "Practical",
            "Independent",
            "Confident",
            "Sensitive",
            "Optimistic",
            "Adventurous",
            "Grounded",
            "Creative",
            "Insightful",
            "Stylish",
            "Brave",
            "Social",
            "Considerate",
            "Intelligent",
            "Warm",
            "Carefree",
            "Reserved",
            "Quirky"
                     });


    // BodyType
    public static BitMask bodyType = new BitMask(
        new object[] {
            BodyType.Athletic,
            BodyType.Average,
            BodyType.Curvy,
            BodyType.Heavy }, 
        new string[] {
            "Athletic", 
            "Average", 
            "Curvy", 
            "Heavy" });


    // MemberEducation
    public static BitMask memberEducation = new BitMask(
        new object[] {
            MemberEducation.Elementary,
            MemberEducation.HighSchool,
            MemberEducation.SomeCollege,
            MemberEducation.BachelorsDegree,
            MemberEducation.MastersDegree,
            MemberEducation.Doctorate,
            MemberEducation.AssociateDegree }, 
        new string[] {
            "Elementary", 
            "High School", 
            "Some College", 
            "Bachelor's Degree", 
            "Master's Degree", 
            "Doctorate", 
            "Associate Degree" });


    // MemberHearAboutJoesClub
    public static ValueSet memberHearAboutJoesClub = new ValueSet(
        new object[] {
            MemberHearAboutJoesClub.Email,
            MemberHearAboutJoesClub.Friend,
            MemberHearAboutJoesClub.Jdate,
            MemberHearAboutJoesClub.PrintAd,
            MemberHearAboutJoesClub.SearchEngine,
            MemberHearAboutJoesClub.LifestyleMagazine,
            MemberHearAboutJoesClub.JewishJournal,
            MemberHearAboutJoesClub.Temple,
            MemberHearAboutJoesClub.JewishOrganization }, 
        new string[] {
            "E-mail",
            "Friend",
            "Jdate",
            "Print Ad",
            "Search Engine",
            "Lifestyle Magazine",
            "Jewish Journal",
            "Temple",
            "Jewish Organization" }, 
        MemberHearAboutJoesClub.NoValue);

    // MemberWhenToCall
    public static ValueSet memberWhenToCall = new ValueSet(
        new object[] {
            MemberWhenToCall.Morning,
            MemberWhenToCall.Midday,
            MemberWhenToCall.Evening }, 
        new string[] {
            "Morning",
            "Midday",
            "Evening" }, 
        MemberWhenToCall.NoValue);


    // MemberWhatAppealedToYou
    public static ValueSet memberWhatAppealedToYou = new ValueSet(
        new object[] {
            MemberWhatAppealedToYou.ConvenienceAndEfficiency,
            MemberWhatAppealedToYou.EasyAndFun,
            MemberWhatAppealedToYou.HavingPlans,
            MemberWhatAppealedToYou.Meeting5NewPeopleAtOnce,
            MemberWhatAppealedToYou.TryingNewRestaurants }, 
        new string[] {
            "Convenience and efficiency",
            "Easy and fun",
            "Having plans",
            "Meeting 5 new people at once",
            "Trying new restaurants" }, 
        MemberWhatAppealedToYou.NoValue);

    // MemberLoveMostAboutJDate
    public static ValueSet memberLoveMostAboutJDate = new ValueSet(
        new object[] {
            MemberLoveMostAboutJDate.NoValue,
            MemberLoveMostAboutJDate.Selection,
            MemberLoveMostAboutJDate.Convenience,
            MemberLoveMostAboutJDate.EaseOfUse,
            MemberLoveMostAboutJDate.Cost,
            MemberLoveMostAboutJDate.Anonymity 
        }, 
        new string[] {
            "",
            "Selection",
            "Convenience",
            "Ease of use",
            "Cost",
            "Anonymity" 
        }, 
        MemberLoveMostAboutJDate.NoValue);

	public static ValueSet memberDiscussed = new ValueSet(
		new object[] {
			MemberDiscussed.WhatIsJoesClub,
			MemberDiscussed.Scheduling,
			MemberDiscussed.Frequency,
			MemberDiscussed.Events,
			MemberDiscussed.Matchmaking,
			MemberDiscussed.Price,
			MemberDiscussed.Selection,
			MemberDiscussed.Interview
		},
		new string[] {
			"What is Joe's Club",
			"Scheduling of events",
			"Frequency of going out",
			"The events themselves",
			"Three levels of Matchmaking",
			"Price",
			"Selection Process",
			"Interview Process"
		},
		CommonConstants.NoValueInt);

	public static ValueSet memberReservations = new ValueSet(
		new object[] {
			MemberReservations.TimeCommitment,
			MemberReservations.MonthToMonth,
			MemberReservations.Price,
			MemberReservations.Expensive,
			MemberReservations.Matching,
			MemberReservations.Busy,
			MemberReservations.DoNotNeed
		},
		new string[] {
			"Time Commitment",
			"Want to go month to month (no full year)",
			"Price",
			"Too Expensive, Want Installments",
			"Concerned with matching",
			"Too busy",
			"Don't Need it"
		},
		CommonConstants.NoValueInt);

    // MemberBiggestConcern
    public static ValueSet memberBiggestConcern = new ValueSet(
        new object[] {
            MemberBiggestConcern.Control,
            MemberBiggestConcern.Cost,
            MemberBiggestConcern.Pressure,
            MemberBiggestConcern.TimeSpent,
            MemberBiggestConcern.PeopleNotWhoTheySayTheyAre 
        }, 
        new string[] {
            "Control",
            "Cost",
            "Pressure",
            "Time spent",
            "People not who they say they are" 
        }, 
        MemberBiggestConcern.NoValue);


    // MemberPeopleNotice
    public static ValueSet memberPeopleNotice = new ValueSet(
        new object[] {
            MemberPeopleNotice.MyEyes,
            MemberPeopleNotice.MySmile,
            MemberPeopleNotice.MySenseOfHumor,
            MemberPeopleNotice.MyBrains,
            MemberPeopleNotice.MyPersonality }, 
        new string[] {
            "My eyes",
            "My smile",
            "My sense of humor",
            "My brains",
            "My personality" }, 
        MemberPeopleNotice.NoValue);

    // MemberOccupation
    public static ValueSet memberOccupation = new ValueSet(
        new object[] {
            MemberOccupation.AdministrativeSupportServices,
            MemberOccupation.AdvertisingMarketingPR,
            MemberOccupation.ArchitectureInteriorDesignLandscaping,
            MemberOccupation.ArtistsMusicianWriter,
            MemberOccupation.BankingFinancialRealEstate,
            MemberOccupation.Communication,
            MemberOccupation.Computers,
            MemberOccupation.DesignVisualandgraphicarts,
            MemberOccupation.EducationTeachingAcademicresearch,
            MemberOccupation.Entertainment,
            MemberOccupation.EntrepreneurialStartup,
            MemberOccupation.EnvironmentalServices,
            MemberOccupation.ExecutiveGeneralManagementConsulting,
            MemberOccupation.FashionStyleApparelBeauty,
            MemberOccupation.FinancialAccounting,
            MemberOccupation.GovernmentCivilServicePublicPolicy,
            MemberOccupation.HealthFitness,
            MemberOccupation.HospitalityTravel,
            MemberOccupation.InternetECommerceTechnology,
            MemberOccupation.LawEnforcementMilitarySecurity,
            MemberOccupation.LawLegalJudiciary,
            MemberOccupation.ManufacturingDistribution,
            MemberOccupation.MedicalDentalHealthcare,
            MemberOccupation.NewMedia,
            MemberOccupation.NonProfitVolunteerActivist,
            MemberOccupation.PoliticalGovernmentMilitary,
            MemberOccupation.ProductionFilmTV,
            MemberOccupation.PsychologistTherapistSocialServices,
            MemberOccupation.PublicsafetyFireParamedic,
            MemberOccupation.PublishingPrintMedia,
            MemberOccupation.RealEstate,
            MemberOccupation.RestaurantFoodServicesCatering,
            MemberOccupation.Retired,
            MemberOccupation.SalesRepresentativeWholesaleRetail,
            MemberOccupation.Student,
            MemberOccupation.TechnicalScienceEngineering,
            MemberOccupation.VeterinaryServices }, 
        new string[] {
            "Administrative/Support Services",
            "Advertising/Marketing/PR",
            "Architecture/Interior Design/ Landscaping",
            "Artists/Musician/Writer",
            "Banking/Financial/Real Estate",
            "Communication",
            "Computers",
            "Design/Visual and graphic arts",
            "Education/Teaching/Academic research",
            "Entertainment",
            "Entrepreneurial/Start up",
            "Environmental Services",
            "Executive/ General Management/ Consulting",
            "Fashion/Style/Apparel/Beauty",
            "Financial/Accounting",
            "Government/Civil Service/ Public Policy",
            "Health/Fitness",
            "Hospitality/ Travel",
            "Internet/ E-commerce/Technology",
            "Law Enforcement/ Military/ Security",
            "Law/Legal/ Judiciary",
            "Manufacturing/ Distribution",
            "Medical/Dental/ Healthcare",
            "New Media",
            "Non-profit/Volunteer/Activist",
            "Political/Government/Military",
            "Production Film/TV",
            "Psychologist/Therapist/ Social Services",
            "Public safety/Fire/Paramedic",
            "Publishing/Print Media",
            "Real Estate",
            "Restaurant/Food Services/ Catering",
            "Retired",
            "Sales Representative/Wholesale/Retail",
            "Student",
            "Technical/ Science/Engineering",
            "Veterinary Services" }, 
        MemberOccupation.NoValue);

    // MemberLocationPref
    public static BitMask memberLocationPref = new BitMask(
        new object[] {
            MemberLocationPref.TheWestside,
            MemberLocationPref.BeverlyHills,
            MemberLocationPref.Hollywood,
            MemberLocationPref.TheValley,
            MemberLocationPref.OC
                     }, 
        new string[] {
            "The Westside (Santa Monica, Venice, Marina del Rey, Playa del Rey, Brentwood)",
            "Beverly Hills",
            "Hollywood",
            "The Valley",
            "OC"
                     });

    // MemberTimePref
    public static BitMask memberTimePref = new BitMask(
        new object[] {
            MemberTimePref.TimePreference6PM,
            MemberTimePref.TimePreference7PM,
            MemberTimePref.TimePreference8PM,
            MemberTimePref.TimePreference9PM
                     }, 
        new string[] {
            "6PM",
            "7PM",
            "8PM",
            "9PM"
                     });

    // MemberDietaryRestrictions
    public static BitMask memberDietaryRestrictions = new BitMask(
        new object[] {
            MemberDietaryRestrictions.Vegetarian,
            MemberDietaryRestrictions.Noredmeat,
            MemberDietaryRestrictions.Nopork,
            MemberDietaryRestrictions.Noshellfish,
            MemberDietaryRestrictions.Kosher
                     }, 
        new string[] {
            "Vegetarian",
            "No red meat",
            "No pork",
            "No shellfish",
            "Kosher"
                     });

    // MemberCuisinesToAvoid
    public static BitMask memberCuisinesToAvoid = new BitMask(
        new object[] {
            MemberCuisinesToAvoid.Thai,
            MemberCuisinesToAvoid.Japanese,
            MemberCuisinesToAvoid.Italian,
            MemberCuisinesToAvoid.Indian,
            MemberCuisinesToAvoid.Meat
                     }, 
        new string[] {
            "Thai",
            "Japanese",
            "Italian",
            "Indian",
            "Meat"
                     });

    // MemberMaritalStatus
    public static BitMask memberMaritalStatus = new BitMask(
        new object[] {
            MemberMaritalStatus.Single,
            MemberMaritalStatus.Divorced,
            MemberMaritalStatus.Separated,
            MemberMaritalStatus.Widowed }, 
        new string[] {
            "Single", 
            "Divorced", 
            "Separated", 
            "Widowed" });

    // MemberSpendTime
    public static ValueSet memberSpendTime = new ValueSet(
        new object[] {
            MemberSpendTime.Restaurants,
            MemberSpendTime.ClubsDancing,
            MemberSpendTime.CharityVolunteer,
            MemberSpendTime.Sportsevents,
            MemberSpendTime.ConcertsTheaterArtopenings
                     }, 
        new string[] {
            "Restaurants",
            "Clubs/Dancing",
            "Charity/Volunteer",
            "Sports events",
            "Concerts/Theater/Art openings"
                     },
        MemberSpendTime.NoValue);


    // MemberChildrenCount
    public static ValueSet memberChildrenCount = new ValueSet(
        new object[] {
            MemberChildrenCount.None,
            MemberChildrenCount.One,
            MemberChildrenCount.Two,
            MemberChildrenCount.ThreeOrMore
                     }, 
        new string[] {
            "None",
            "1",
            "2",
            "3 or more"
                     },
        MemberChildrenCount.NoValue);


    // MemberRelationship
    public static ValueSet memberRelationship = new ValueSet(
        new object[] {
            MemberRelationship.Date,
            MemberRelationship.Friend,
            MemberRelationship.Marriage,
            MemberRelationship.MarriageAndChildren,
            MemberRelationship.LongTermRelationship,
            MemberRelationship.ActivityPartner
                     }, 
        new string[] {
            "A date",
            "Friend",
            "Marriage",
            "Marriage & Children",
            "A long-term relationship",
            "Activity Partner"
                     },
        MemberRelationship.NoValue);

    #region MemberPersonalityQuestion ValueSets, BitMasks

    // MemberPersonalityQuestion1
    public static ValueSet memberPersonalityQuestion1 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion1.JonStewart,
            MemberPersonalityQuestion1.HowardStern,
            MemberPersonalityQuestion1.MadeleineAlbright,
            MemberPersonalityQuestion1.SigmundFreud,
            MemberPersonalityQuestion1.PhilipRoth,
            MemberPersonalityQuestion1.HenryKissinger
                     }, 
        new string[] {
            "Jon Stewart",
            "Howard Stern",
            "Madeleine Albright",
            "Sigmund Freud",
            "Philip Roth",
            "Henry Kissinger"
                     },
        MemberPersonalityQuestion1.NoValue);


    // MemberPersonalityQuestion2
    public static ValueSet memberPersonalityQuestion2 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion2.Flight,
            MemberPersonalityQuestion2.Invisibility,
            MemberPersonalityQuestion2.XRayvision,
            MemberPersonalityQuestion2.Mindreading,
            MemberPersonalityQuestion2.Cripplingsenseofhumor
                     }, 
        new string[] {
            "Flight",
            "Invisibility",
            "X-Ray vision",
            "Mind-reading",
            "Crippling sense of humor"
                     },
        MemberPersonalityQuestion2.NoValue);


    // MemberPersonalityQuestion3
    public static ValueSet memberPersonalityQuestion3 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion3.Cell,
            MemberPersonalityQuestion3.iPod,
            MemberPersonalityQuestion3.PDA,
            MemberPersonalityQuestion3.DigitalCamera,
            MemberPersonalityQuestion3.SuperGizmo,
            MemberPersonalityQuestion3.Notepad
                     }, 
        new string[] {
            "Cell",
            "iPod",
            "PDA",
            "Digital Camera",
            "Super Gizmo (does it all)",
            "Uh...notepad?"
                     },
        MemberPersonalityQuestion3.NoValue);


    // MemberPersonalityQuestion4
    public static ValueSet memberPersonalityQuestion4 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion4.OscarWilde,
            MemberPersonalityQuestion4.DavidSedaris,
            MemberPersonalityQuestion4.JohnGrisham,
            MemberPersonalityQuestion4.Aristotle,
            MemberPersonalityQuestion4.NewYorker,
            MemberPersonalityQuestion4.Pictures
                     }, 
        new string[] {
            "Oscar Wilde",
            "David Sedaris",
            "John Grisham",
            "Aristotle",
            "New Yorker",
            "Pictures"
                     },
        MemberPersonalityQuestion4.NoValue);


    // MemberPersonalityQuestion5
    public static ValueSet memberPersonalityQuestion5 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion5.Weights,
            MemberPersonalityQuestion5.Running,
            MemberPersonalityQuestion5.Yoga,
            MemberPersonalityQuestion5.Kickboxing,
            MemberPersonalityQuestion5.Dancing,
            MemberPersonalityQuestion5.Publicspeaking
                     }, 
        new string[] {
            "Weights",
            "Running",
            "Yoga",
            "Kickboxing",
            "Dancing",
            "Public speaking"
                     },
        MemberPersonalityQuestion5.NoValue);


    // MemberPersonalityQuestion6
    public static ValueSet memberPersonalityQuestion6 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion6.Planeticket,
            MemberPersonalityQuestion6.Operaticket,
            MemberPersonalityQuestion6.Theaterticket,
            MemberPersonalityQuestion6.Amusementparkticket,
            MemberPersonalityQuestion6.Backstagepasses,
            MemberPersonalityQuestion6.Lottoticket
                     }, 
        new string[] {
            "Plane ticket",
            "Opera ticket",
            "Theater ticket",
            "Amusement park ticket",
            "Backstage passes",
            "Lotto ticket"
                     },
        MemberPersonalityQuestion6.NoValue);


    // MemberPersonalityQuestion7
    public static ValueSet memberPersonalityQuestion7 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion7.Oceanview,
            MemberPersonalityQuestion7.Mountainview,
            MemberPersonalityQuestion7.Forestview,
            MemberPersonalityQuestion7.Penthouseview,
            MemberPersonalityQuestion7.Poolview,
            MemberPersonalityQuestion7.Cityview
                     }, 
        new string[] {
            "Ocean view",
            "Mountain view",
            "Forest view",
            "Penthouse view",
            "Pool view",
            "City view"
                     },
        MemberPersonalityQuestion7.NoValue);


    // MemberPersonalityQuestion8
    public static ValueSet memberPersonalityQuestion8 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion8.Michelangelo,
            MemberPersonalityQuestion8.Matisse,
            MemberPersonalityQuestion8.DaVinci,
            MemberPersonalityQuestion8.Warhol,
            MemberPersonalityQuestion8.Picasso,
            MemberPersonalityQuestion8.FrancisBacon
                     }, 
        new string[] {
            "Michelangelo",
            "Matisse",
            "Da Vinci",
            "Warhol",
            "Picasso",
            "Francis Bacon"
                     },
        MemberPersonalityQuestion8.NoValue);


    // MemberPersonalityQuestion9
    public static ValueSet memberPersonalityQuestion9 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion9.Tropicalparadise,
            MemberPersonalityQuestion9.Exoticcity,
            MemberPersonalityQuestion9.Safari,
            MemberPersonalityQuestion9.Cruiseship,
            MemberPersonalityQuestion9.CampingNature,
            MemberPersonalityQuestion9.Workingupasweat
                     }, 
        new string[] {
            "Tropical paradise",
            "Exotic city",
            "Safari",
            "Cruise ship",
            "Camping/Nature",
            "Working up a sweat"
                     },
        MemberPersonalityQuestion9.NoValue);


    // MemberPersonalityQuestion10
    public static ValueSet memberPersonalityQuestion10 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion10.GradeSchool,
            MemberPersonalityQuestion10.JuniorHigh,
            MemberPersonalityQuestion10.HighSchool,
            MemberPersonalityQuestion10.College,
            MemberPersonalityQuestion10.Twenties,
            MemberPersonalityQuestion10.Now
                     }, 
        new string[] {
            "Grade school",
            "Junior High",
            "High School",
            "College",
            "20's ",
            "Now"
                     },
        MemberPersonalityQuestion10.NoValue);


    // MemberPersonalityQuestion11
    public static ValueSet memberPersonalityQuestion11 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion11.FirstOlympics,
            MemberPersonalityQuestion11.BuildingOfThePyramids,
            MemberPersonalityQuestion11.CivilWar,
            MemberPersonalityQuestion11.SeabiscuitVsWarAdmiral,
            MemberPersonalityQuestion11.BerlinWallComingDown,
            MemberPersonalityQuestion11.WomensRightToVote
                     }, 
        new string[] {
            "The first Olympics",
            "Building of the pyramids",
            "Civil War",
            "Seabiscuit vs. War Admiral",
            "Berlin wall coming down",
            "Women�s right to vote"
                     },
        MemberPersonalityQuestion11.NoValue);


    // MemberPersonalityQuestion12
    public static ValueSet memberPersonalityQuestion12 = new ValueSet(
        new object[] {
            MemberPersonalityQuestion12.Sight,
            MemberPersonalityQuestion12.Smell,
            MemberPersonalityQuestion12.Touch,
            MemberPersonalityQuestion12.Taste,
            MemberPersonalityQuestion12.Hearing
                     }, 
        new string[] {
            "Sight",
            "Smell",
            "Touch",
            "Taste",
            "Hearing"
                     },
        MemberPersonalityQuestion12.NoValue);

    #endregion MemberPersonalityQuestion ValueSets, BitMasks

    #endregion Member ValueSets, BitMasks

    #region Other ValueSets, BitMasks

    // eventStatus
    public static ValueSet eventStatus = new ValueSet(
        new object[] {
            EventStatus.New,
            EventStatus.Posted,
            EventStatus.Closed,
            EventStatus.Finished,
            EventStatus.Cancelled }, 
        new string[] {
            EventStatus.New.ToString(),
            EventStatus.Posted.ToString(),
            EventStatus.Closed.ToString(),
            EventStatus.Finished.ToString(),
            EventStatus.Cancelled.ToString() }, 
        EventStatus.New);

    
    // MemberStatus
    public static ValueSet memberStatus = new ValueSet(
        new object[] {
            MemberStatus.None,
            MemberStatus.SignedUp,
            MemberStatus.RsvpedYes,
            MemberStatus.Cancelled,
            MemberStatus.NoShow,
            MemberStatus.Attended,
            MemberStatus.RsvpedNo}, 
        new string[] {
            "None", 
            "Signed Up",
            "Rsvped Yes", 
            "Cancelled", 
            "No Show", 
            "Attended", 
            "Rsvped No" }, 
        MemberStatus.None);

    // venueParking
    private static string[] _venueParkingNames = new string[] {
        "Valet", 
        "Street Parking", 
        "Free Parking Lot/Garage", 
        "Pay Parking Lot/Garage" };
    
    private static object[] _venueParkingValues = new object[] {
        VenueParking.Valet,
        VenueParking.StreetParking,
        VenueParking.FreeParkingLot,
        VenueParking.PayParkingLot };

    public static BitMask venueParking = new BitMask(
        _venueParkingValues, _venueParkingNames);


    // venuePriceRange
    private static string[] _venuePriceRangeNames = new string[] {
        "$51 or more", 
        "$31 to $50", 
        "$30 and under" };
    
    private static object[] _venuePriceRangeValues = new object[] {
        VenuePriceRange.High,
        VenuePriceRange.Medium,
        VenuePriceRange.Low };

    public static ValueSet venuePriceRange = new ValueSet(
        _venuePriceRangeValues, _venuePriceRangeNames, VenuePriceRange.NoValue);


    // phoneType
    public static ValueSet phoneType = new ValueSet(
        new object[] {
            PhoneType.Work,
            PhoneType.Home,
            PhoneType.Cell }, 
        new string[] {
            "Work", 
            "Home", 
            "Cell" }, 
        PhoneType.NoValue);



    // DaysOfTheWeek
    public static BitMask daysOfTheWeek = new BitMask(
        new object[] {
            DaysOfTheWeek.Monday,
            DaysOfTheWeek.Tuesday,
            DaysOfTheWeek.Wednesday,
            DaysOfTheWeek.Thursday,
            DaysOfTheWeek.Friday,
            DaysOfTheWeek.Saturday,
            DaysOfTheWeek.Sunday
                     }, 
        new string[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday "
                     });

    // InterviewStatus
    public static ValueSet interviewStatus = new ValueSet(
        new object[] {
            InterviewStatus.New,
            //InterviewStatus.CallBack,
            InterviewStatus.NoAnswer,
            InterviewStatus.LM,
            InterviewStatus.Busy,
            InterviewStatus.WrongNumber,
            InterviewStatus.SpokeTo }, 
        new string[] {
            "New", 
            //"Call Back", 
            "No Answer", 
            "LM", 
            "Busy", 
            "Wrong Number", 
            "Spoke to" }, 
        InterviewStatus.NoValue);

	// Interview Schedule Status
	public static ValueSet interviewScheduleStatus = new ValueSet(
		new object[] {
						 InterviewScheduleStatus.ScheduledCallback,
						 InterviewScheduleStatus.Scheduled,
						 InterviewScheduleStatus.Cancelled,
						 InterviewScheduleStatus.Rescheduled,
						 InterviewScheduleStatus.NeedsReschedule,
						 InterviewScheduleStatus.NotInterested },
		new string[] {
				"Scheduled Callback",
				"Scheduled",
				"Cancelled",
				"Rescheduled",
				"Needs Reschedule",
				"Not Interested" },
			InterviewScheduleStatus.NoValue);

    #endregion Other ValueSets, BitMasks

}
}
