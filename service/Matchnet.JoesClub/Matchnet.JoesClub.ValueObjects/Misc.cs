using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Text;

using Matchnet.JoesClub.ValueObjects;



/**
 * Misc utility methods.
 */
namespace Matchnet.JoesClub.ValueObjects
{
public class Misc
{

    /**
     * Replaces ' and " with \' and \"
     */
    public static string escapeQuotes(string s)
    {
        s = s.Replace("'", "\\'");
        s = s.Replace("\"", "\\\"");
        return s;
    }


    /**
     * Gets an int from a Hashtable, returning defaultVal if the key is missing.
     */
    public static int intFromHash(Hashtable h, object key, int defaultVal)
    {
        if (!h.ContainsKey(key))
            return defaultVal;
        return (int) h[key];
    }


    /**
     * Converts an empty string to null.
     */
    public static string emptyToNullString(string s)
    {
        return s == "" ? null : s;
    }

    /**
     * Converts a null string to empty.
     */
    public static string nullToEmptyString(string s)
    {
        return s == null ? "" : s;
    }


    /**
     * Re-order the elements of inArray to display like this
     *    a1 a4 a7
     *    a2 a5 a8
     *    a3 
     */
    public static ArrayList orderByColumns(ArrayList inArray, int nCols)
    {
        int sz = inArray.Count;
        int nRows = sz / nCols;
        int extra = sz - nRows * nCols;

        ArrayList outArray = new ArrayList();
        int col = 0;
        int row = 0;
        int colOffset = 0;
        for (int outIndex = 0; outIndex < sz; outIndex++)
        {
            int inIndex = row + colOffset;
            outArray.Add(inArray[inIndex]);

            ++col;
            if (col == nCols)
            {
                col = 0;
                ++row;
                colOffset = 0;
            }
            else
            {
                colOffset += nRows;
                if (col <= extra)
                    ++colOffset;
            }
        }
        
        return outArray;
    }


    /**
     * Converts an ArrayList[object] to Hashtable[object --> ""]
     */
    public static Hashtable arrayListToHashtable(ICollection a)
    {
        Hashtable h = new Hashtable();
        foreach (object o in a)
            h[o] = "";
        return h;
    }


    /**
     * Converts an Hashtable[object --> ""] to ArrayList[object]
     */
    public static ArrayList hashtableToArrayList(Hashtable h)
    {
        ArrayList a = new ArrayList();
        foreach (object o in h.Keys)
            a.Add(o);
        return a;
    }


    /**
     * Parses a param value string to get a double
     */
    public static double getCurrencyFromQueryParam(
        NameValueCollection queryParams,
        string param)
    {
        string sPrice = Misc.nullToEmptyString(queryParams.Get("price")).Trim();
        if (sPrice == "")
            return CommonConstants.NoValueInt;
        return double.Parse(sPrice, NumberStyles.Currency, null);
    }


    /**
     * Converts the input phone number to the form xxx-xxx-xxxx
     */
    public static string normalizePhoneNumber(string phoneIn)
    {
        if (phoneIn == null)
            return null;
        phoneIn = phoneIn.Trim();
        if (phoneIn == "")
            return "";

        StringBuilder sb = new StringBuilder();
        int count = 0;
        foreach (char c in phoneIn.ToCharArray())
        {
            if (char.IsDigit(c))
            {
                sb.Append(c);
                ++count;
                if (count == 3 || count == 6)
                    sb.Append('-');
            }
        }

        if (count != 10)
            return null;
            //_log.error("normalizePhoneNumber() bad phoneIn: " + phoneIn);

        return sb.ToString();
    }

}
}