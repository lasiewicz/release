using System;
using System.Collections;


/**
 * ValueObject corresponding to Matchnet.JoesClub.BusinessLogic.Venue
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class VenueVO : DbObjectVO
{

#region members
    private string _name;
    private string _address;
    private string _address2;
    private string _city;
    private string _state;
    private string _zipCode;
    private string _webSiteUrl;
    private string _image;
    private string _phone;
    private string _contactName;
    private string _contactPhone;
    private string _contactTitle;
    private string _mapUrl;
    private int _parking;
    private double _parkingFee;
    private string _priceRange;
    private string _notes;
    private string _salesCopy;
    private string _salesCopy2;
    private string _image2;
    private string _meetingLocation;

    // properties
    public string name { get { return _name; } set { _name = value; } }
    public string address { get { return _address; } set { _address = value; } }
    public string address2 { get { return _address2; } set { _address2 = value; } }
    public string city { get { return _city; } set { _city = value; } }
    public string state { get { return _state; } set { _state = value; } }
    public string zipCode { get { return _zipCode; } set { _zipCode = value; } }
    public string webSiteUrl { get { return _webSiteUrl; } set { _webSiteUrl = value; } }
    public string image { get { return _image; } set { _image = value; } }
    public string phone { get { return _phone; } set { _phone = value; } }
    public string contactName { get { return _contactName; } set { _contactName = value; } }
    public string contactPhone { get { return _contactPhone; } set { _contactPhone = value; } }
    public string contactTitle { get { return _contactTitle; } set { _contactTitle = value; } }
    public string mapUrl { get { return _mapUrl; } set { _mapUrl = value; } }
    public int parking { get { return _parking; } set { _parking = value; } }
    public double parkingFee { get { return _parkingFee; } set { _parkingFee = value; } }
    public string priceRange { get { return _priceRange; } set { _priceRange = value; } }
    public string notes { get { return _notes; } set { _notes = value; } }
    public string salesCopy { get { return _salesCopy; } set { _salesCopy = value; } }
    public string salesCopy2 { get { return _salesCopy2; } set { _salesCopy2 = value; } }
    public string image2 { get { return _image2; } set { _image2 = value; } }
    public string meetingLocation { get { return _meetingLocation; } set { _meetingLocation = value; } }

    
    #endregion



    /**
     * Constructor
     */
    public VenueVO(
        int id, 
        string name,
        string address,
        string address2,
        string city,
        string state,
        string zipCode,
        string webSiteUrl,
        string image,
        string phone,
        string contactName,
        string contactPhone,
        string contactTitle,
        string mapUrl,
        int parking,
        double parkingFee,
        string priceRange,
        string notes,
        string salesCopy,
        string salesCopy2,
        string image2,
        string meetingLocation) : base(id)
    {
        _name = name;
        _address = address;
        _address2 = address2;
        _city = city;
        _state = state;
        _zipCode = zipCode;
        _webSiteUrl = webSiteUrl;
        _image = image;
        _phone = phone;
        _contactName = contactName;
        _contactPhone = contactPhone;
        _contactTitle = contactTitle;
        _mapUrl = mapUrl;
        _parking = parking;
        _parkingFee = parkingFee;
        _priceRange = priceRange;
        _notes = notes;
        _salesCopy = salesCopy;
        _salesCopy2 = salesCopy2;
        _image2 = image2;
        _meetingLocation = meetingLocation;
    }

}
}
