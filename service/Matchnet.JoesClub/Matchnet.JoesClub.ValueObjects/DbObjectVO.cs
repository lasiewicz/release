using System;

/**
 * This is what the PageHandler service send back
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class DbObjectVO
{
    private int _id;
    public int id { get { return _id; } set { _id = value; } }


    /**
     * Constructor
     */
    public DbObjectVO(int id)
    {
        _id = id;
    }

}
}
