using System;
using System.Collections;


/**
 * Used to represent a set of values
 */
namespace Matchnet.JoesClub.ValueObjects
{

public class ValueSet
{

//    public const int Any = 0;
    public const string AnyStr = "Any";


    protected string[] _names;
    public string[] names { get { return _names; } }

    protected object[] _values;
    public object[] values { get { return _values; } }

    private object _defaultValue;
    public object defaultValue { get { return _defaultValue; } }
    public int defaultValueInt { get { return (int) _defaultValue; } }



    /**
     * Constructor
     */ 
    public ValueSet(object[] values, string[] names, object defaultValue)
    {
        _names = names;
        _values = values;
        _defaultValue = defaultValue;

        if (_names.Length != _values.Length)
            throw new Exception("ValueSet() _names.Length != _values.Length");
    }


    /**
     * This is to let BitMask use 0 instead of -1 for no value.
     */
    protected virtual int getNoValueInt()
    {
        return CommonConstants.NoValueInt;
    }


    /**
     * Returns all names and values as a ArrayList[ValueSetNameValue]
     */
    public ArrayList getNamesAndValues(bool includeNoValue)
    {
        ArrayList nv = new ArrayList();

        // Do they want a "no value" element?
        if (includeNoValue)
            nv.Add(new ValueSetNameValue("", getNoValueInt()));

        for (int i = 0; i < _values.Length; i++)
            nv.Add(new ValueSetNameValue(_names[i], _values[i]));
        return nv;
    }


    /**
     * Returns the name for the given value
     */
    public string getName(object value)
    {
        int iValue = (int) value;
        for (int i = 0; i < _values.Length; i++)
            if (iValue == (int) _values[i])
                return _names[i];

        // jlf hack
        return "";
        /*
        if (CommonConstants.NoValueInt == (int) value)
            return "";

        throw new Exception("getName() bad value: " + iValue);
        return null;
        */
    }


    /**
     * returns false if the value is bad
     */
    public bool isValidValue(object value)
    {
        for (int i = 0; i < _values.Length; i++)
            if (value == _values[i])
                return true;

        return false;
    }

    /**
     * throws an exception if the value is bad
     */
    public void validate(object value)
    {
        if (!isValidValue(value))
            throw new Exception("validate() bad value: " + value);
    }


    /**
     * Just a convenience method to convert a value's int value 
     * to a string, e.g.
     * toIntString(Rsvp.LateCancel) --> "4"
     */
    public static string toIntString(object value)
    {
        return ((int) value).ToString();
    }


    /**
     * Finds the element of _values for the given string value. 
     */
    public object enumFromString(string sValue)
    {
        foreach (object oValue in _values)
            if (sValue == oValue.ToString())
                return oValue;
        throw new Exception("enumFromString() unknown value: " + sValue);
    }


}

}