using System;
using System.Collections;


/**
 * ValueObject corresponding to Matchnet.JoesClub.BusinessLogic.Event
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class EventVO : DbObjectVO
{
    private string _name;
    private DateTime _startTime;

    public string name { get { return _name; } set { _name = value; } }
    public DateTime startTime { get { return _startTime; } set { _startTime = value; } }


    /**
     * Constructor
     */
    public EventVO(
        int id, 
        string name,
        DateTime startTime) : base(id)
    {
        _name = name;
        _startTime = startTime;
    }
}
}
