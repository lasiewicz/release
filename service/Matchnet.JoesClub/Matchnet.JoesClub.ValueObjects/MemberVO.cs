using System;
using System.Collections;


/**
 * ValueObject corresponding to Matchnet.JoesClub.BusinessLogic.Member
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class MemberVO : DbObjectVO
{
    #region members

    private int _sparkId;
    private bool _gender;
    private string _image;
    private string _firstName;
    private string _lastName;
    private string _userName;
    private string _city;
    private string _state;
    private string _zipCode;
    private string _emailAdd;
    private string _emailAdd2;
    private string _phone;
    private string _phone2;
    private bool _shortNotice;
    private int _age;
    private int _desiredAgeLow;
    private int _desiredAgeHigh;
    private int _height;
    private int _bodyType;
    private int _weight;
    private int _education;
    private int _jdateReligion;
    private int _desiredJDateReligion;
    private bool _smoking;
    private int _numKids;         // number of kids
    private int _willDateAParent; // one of BitMask.willDateAParent
    private int _kidsPref;        // one of BitMask.kidsPref
    private int _desiredKidsPref; // mask of BitMask.kidsPref
    private DateTime _startDate;
    private double _startRate;
    private DateTime _renewalDate;
    private double _renewalRate;
    private DateTime _expirationDate;
    private string _avatar;
    private MemberActivityStatus _activityStatus;
    private MeetingSomeone _meetingSomeone;
    private int _describeMe;
    private PhoneType _phoneType;
    private PhoneType _phoneType2;
    private bool _deleted;
    private MemberHearAboutJoesClub _hearAboutJoesClub;
    private string _hearAboutJoesClubExplain;
    private MemberWhatAppealedToYou _whatAppealedToYou;
    private string _jdateExperience;
    private MemberBiggestConcern _biggestConcern;
    private MemberPeopleNotice _peopleNotice;
    private MemberOccupation _occupation;
    private MemberSpendTime _spendTime;
    private string _spendTimeText;
    private int _locationPref;
    private int _timePref;
    private int _dietaryRestrictions;
    private int _cuisinesToAvoid;
    private int _additionalNights;
    private int _desiredMaritalStatus;
    private int _desiredEducationLevel;
    private int _desiredDrinkingHabits;
    private int _desiredSmokingHabits;
    private DateTime _birthdate;
    private bool _contractReceived;
    private MemberLoveMostAboutJDate _loveMostAboutJDate;
    private MemberPersonalityQuestion1 _personalityQuestion1;
    private MemberPersonalityQuestion2 _personalityQuestion2;
    private MemberPersonalityQuestion3 _personalityQuestion3;
    private MemberPersonalityQuestion4 _personalityQuestion4;
    private MemberPersonalityQuestion5 _personalityQuestion5;
    private MemberPersonalityQuestion6 _personalityQuestion6;
    private MemberPersonalityQuestion7 _personalityQuestion7;
    private MemberPersonalityQuestion8 _personalityQuestion8;
    private MemberPersonalityQuestion9 _personalityQuestion9;
    private MemberPersonalityQuestion10 _personalityQuestion10;
    private MemberPersonalityQuestion11 _personalityQuestion11;
    private MemberPersonalityQuestion12 _personalityQuestion12;
    private string _applicationRemarks;
    private string _inPersonInterviewTime;
    private string _phoneInterviewNotes;
    private string _interviewQuestionaireNotes;
    private MemberRelationship _relationship;
    private string _whatElseToKnow;
    private string _whenToCall;
    private DateTime _applicationReceived;
    private string _feesChargedText;
    private bool _emailOptedOut;
    private string _trxId;
    private string _guid;
    private ArrayList _phoneInterviews;
    private string _jdateUrl;
	private int _interviewDiscussed;
	private int _interviewReservations;

    public int sparkId { get { return _sparkId; } set { _sparkId = value; } }
    public bool gender { get { return _gender; } set { _gender = value; } }
    public string image { get { return _image; } set { _image = value; } }
    public string firstName { get { return _firstName; } set { _firstName = value; } }
    public string lastName { get { return _lastName; } set { _lastName = value; } }
    public string userName { get { return _userName; } set { _userName = value; } }
    public string city { get { return _city; } set { _city = value; } }
    public string state { get { return _state; } set { _state = value; } }
    public string zipCode { get { return _zipCode; } set { _zipCode = value; } }
    public string emailAdd { get { return _emailAdd; } set { _emailAdd = value; } }
    public string emailAdd2 { get { return _emailAdd2; } set { _emailAdd2 = value; } }
    public string phone { get { return _phone; } set { _phone = Misc.normalizePhoneNumber(value); } }
    public string phone2 { get { return _phone2; } set { _phone2 = Misc.normalizePhoneNumber(value); } }
    public bool shortNotice { get { return _shortNotice; } set { _shortNotice = value; } }
    public int age { get { return _age; } set { _age = value; } }
    public int desiredAgeLow { get { return _desiredAgeLow; } set { _desiredAgeLow = value; } }
    public int desiredAgeHigh { get { return _desiredAgeHigh; } set { _desiredAgeHigh = value; } }
    public int height { get { return _height; } set { _height = value; } }
    public int bodyType { get { return _bodyType; } set { _bodyType = value; } }
    public int weight { get { return _weight; } set { _weight = value; } }
    public int education { get { return _education; } set { _education = value; } }
    public int jdateReligion { get { return _jdateReligion; } set { _jdateReligion = value; } }
    public int desiredJDateReligion { get { return _desiredJDateReligion; } set { _desiredJDateReligion = value; } }
    public bool smoking { get { return _smoking; } set { _smoking = value; } }
    public int numKids { get { return _numKids; } set { _numKids = value; } }
    public int willDateAParent { get { return _willDateAParent; } set { _willDateAParent = value; } }
    public int kidsPref { get { return _kidsPref; } set { _kidsPref = value; } }
    public int desiredKidsPref { get { return _desiredKidsPref; } set { _desiredKidsPref = value; } }
    public DateTime startDate { get { return _startDate; } set { _startDate = value; } }
    public double startRate { get { return _startRate; } set { _startRate = value; } }
    public DateTime renewalDate { get { return _renewalDate; } set { _renewalDate = value; } }
    public double renewalRate { get { return _renewalRate; } set { _renewalRate = value; } }
    public DateTime expirationDate { get { return _expirationDate; } set { _expirationDate = value; } }
    public string avatar { get { return _avatar; } set { _avatar = value; } }
    public MemberActivityStatus activityStatus { get { return _activityStatus; } set { _activityStatus = value; } }
    public MeetingSomeone meetingSomeone { get { return _meetingSomeone; } set { _meetingSomeone = value; } }
    // "describeMe" is the personality characteristics
    public int describeMe { get { return _describeMe; } set { _describeMe = value; } }
    public PhoneType phoneType { get { return _phoneType; } set { _phoneType = value; } }
    public PhoneType phoneType2 { get { return _phoneType2; } set { _phoneType2 = value; } }
    public bool deleted { get { return _deleted; } set { _deleted = value; } }
    public MemberHearAboutJoesClub hearAboutJoesClub { get { return _hearAboutJoesClub; } set { _hearAboutJoesClub = value; } }
    public string hearAboutJoesClubExplain { get { return _hearAboutJoesClubExplain; } set { _hearAboutJoesClubExplain = value; } }


    public MemberWhatAppealedToYou whatAppealedToYou { get { return _whatAppealedToYou; } set { _whatAppealedToYou = value; } }
    public string jdateExperience { get { return _jdateExperience; } set { _jdateExperience = value; } }
    public MemberBiggestConcern biggestConcern { get { return _biggestConcern; } set { _biggestConcern = value; } }
    public MemberPeopleNotice peopleNotice { get { return _peopleNotice; } set { _peopleNotice = value; } }
    public MemberOccupation occupation { get { return _occupation; } set { _occupation = value; } }
    public MemberSpendTime spendTime { get { return _spendTime; } set { _spendTime = value; } }
    public string spendTimeText { get { return _spendTimeText; } set { _spendTimeText = value; } }
    public int locationPref { get { return _locationPref; } set { _locationPref = value; } }
    public int timePref { get { return _timePref; } set { _timePref = value; } }
    public int dietaryRestrictions { get { return _dietaryRestrictions; } set { _dietaryRestrictions = value; } }
    public int cuisinesToAvoid { get { return _cuisinesToAvoid; } set { _cuisinesToAvoid = value; } }
    public int additionalNights { get { return _additionalNights; } set { _additionalNights = value; } }
    public int desiredMaritalStatus { get { return _desiredMaritalStatus; } set { _desiredMaritalStatus = value; } }
    public int desiredEducationLevel { get { return _desiredEducationLevel; } set { _desiredEducationLevel = value; } }
    public int desiredDrinkingHabits { get { return _desiredDrinkingHabits; } set { _desiredDrinkingHabits = value; } }
    public int desiredSmokingHabits { get { return _desiredSmokingHabits; } set { _desiredSmokingHabits = value; } }
    public DateTime birthdate { get { return _birthdate; } set { _birthdate = value; } }
    public bool contractReceived { get { return _contractReceived; } set { _contractReceived = value; } }
    public MemberLoveMostAboutJDate loveMostAboutJDate { get { return _loveMostAboutJDate; } set { _loveMostAboutJDate = value; } }
    public MemberPersonalityQuestion1 personalityQuestion1 { get { return _personalityQuestion1; } set { _personalityQuestion1 = value; } }
    public MemberPersonalityQuestion2 personalityQuestion2 { get { return _personalityQuestion2; } set { _personalityQuestion2 = value; } }
    public MemberPersonalityQuestion3 personalityQuestion3 { get { return _personalityQuestion3; } set { _personalityQuestion3 = value; } }
    public MemberPersonalityQuestion4 personalityQuestion4 { get { return _personalityQuestion4; } set { _personalityQuestion4 = value; } }
    public MemberPersonalityQuestion5 personalityQuestion5 { get { return _personalityQuestion5; } set { _personalityQuestion5 = value; } }
    public MemberPersonalityQuestion6 personalityQuestion6 { get { return _personalityQuestion6; } set { _personalityQuestion6 = value; } }
    public MemberPersonalityQuestion7 personalityQuestion7 { get { return _personalityQuestion7; } set { _personalityQuestion7 = value; } }
    public MemberPersonalityQuestion8 personalityQuestion8 { get { return _personalityQuestion8; } set { _personalityQuestion8 = value; } }
    public MemberPersonalityQuestion9 personalityQuestion9 { get { return _personalityQuestion9; } set { _personalityQuestion9 = value; } }
    public MemberPersonalityQuestion10 personalityQuestion10 { get { return _personalityQuestion10; } set { _personalityQuestion10 = value; } }
    public MemberPersonalityQuestion11 personalityQuestion11 { get { return _personalityQuestion11; } set { _personalityQuestion11 = value; } }
    public MemberPersonalityQuestion12 personalityQuestion12 { get { return _personalityQuestion12; } set { _personalityQuestion12 = value; } }
    public string applicationRemarks { get { return _applicationRemarks; } set { _applicationRemarks = value; } }
    public string inPersonInterviewTime { get { return _inPersonInterviewTime; } set { _inPersonInterviewTime = value; } }
    public string phoneInterviewNotes { get { return _phoneInterviewNotes; } set { _phoneInterviewNotes = value; } }
    public string interviewQuestionaireNotes { get { return _interviewQuestionaireNotes; } set { _interviewQuestionaireNotes = value; } }
    public MemberRelationship relationship { get { return _relationship; } set { _relationship = value; } }
    public string whatElseToKnow { get { return _whatElseToKnow; } set { _whatElseToKnow = value; } }
    public string whenToCall { get { return _whenToCall; } set { _whenToCall = value; } }
    public DateTime applicationReceived { get { return _applicationReceived; } set { _applicationReceived = value; } }
    public string feesChargedText { get { return _feesChargedText; } set { _feesChargedText = value; } }
    public bool emailOptedOut { get { return _emailOptedOut; } set { _emailOptedOut = value; } }
    public string trxId { get { return _trxId; } set { _trxId = value; } }
    public string guid { get { return _guid; } set { _guid = value; } }
    public ArrayList phoneInterviews { get { return _phoneInterviews; } set { _phoneInterviews = value; } }
    public string jdateUrl { get { return _jdateUrl; } set { _jdateUrl = value; } }

	public int interviewDiscussed { get { return _interviewDiscussed; } set { _interviewDiscussed = value; } }
	public int interviewReservations { get { return _interviewReservations; } set { _interviewReservations = value; } }

    #endregion


    /**
     * Constructor
     */
    public MemberVO() : base(DbConstants.NoId)
    {
        _sparkId = CommonConstants.NoValueInt; 
        _gender = CommonConstants.Male; 
        _image = "images/photoUnavailable.gif";
        _firstName = null; 
        _lastName = null;
        _userName = null;
        _city = null;
        _state = null;
        _zipCode = null;
        _emailAdd = null;
        _emailAdd2 = null;
        _phone = null;
        _phone2 = null;
        _shortNotice = false;
        _age = CommonConstants.NoValueInt; 
        _desiredAgeLow = CommonConstants.NoValueInt; 
        _desiredAgeHigh = CommonConstants.NoValueInt; 
        _height = CommonConstants.NoValueInt; 
        _bodyType = (int) Enums.bodyType.defaultValue;
        _weight = CommonConstants.NoValueInt; 
        _education = CommonConstants.NoValueInt;
        _jdateReligion = (int) Enums.jdateReligion.defaultValue;
        _desiredJDateReligion = (int) Enums.jdateReligion.defaultValue;
        _smoking = false; 
        _numKids = CommonConstants.NoValueInt;
        _willDateAParent = (int) Enums.willDateAParent.defaultValue;
        _kidsPref = (int) Enums.kidsPref.defaultValue;
        _desiredKidsPref = (int) Enums.kidsPref.defaultValue;
        _startDate = CommonConstants.NoValueDate; 
        _startRate = CommonConstants.NoValueDouble;
        _renewalDate = CommonConstants.NoValueDate;
        _renewalRate = CommonConstants.NoValueDouble;
        _expirationDate = CommonConstants.NoValueDate; 
        _avatar = null; 
        _activityStatus = (MemberActivityStatus) Enums.memberActivityStatus.defaultValue;
        _meetingSomeone = (MeetingSomeone) Enums.meetingSomeone.defaultValue;
        _describeMe = (int) Enums.describeMe.defaultValue;
        _phoneType = (PhoneType) Enums.phoneType.defaultValue;
        _phoneType2 = (PhoneType) Enums.phoneType.defaultValue;
        _deleted = false; 
        _hearAboutJoesClub = MemberHearAboutJoesClub.NoValue;
        _hearAboutJoesClubExplain = null;
        _whatAppealedToYou = MemberWhatAppealedToYou.NoValue;
        _jdateExperience = null;
        _biggestConcern = MemberBiggestConcern.NoValue;
        _peopleNotice = MemberPeopleNotice.NoValue;
        _occupation = MemberOccupation.NoValue;
        _spendTime = MemberSpendTime.NoValue;
        _spendTimeText = null;
        _locationPref = CommonConstants.NoValueInt;
        _timePref = CommonConstants.NoValueInt;
        _dietaryRestrictions = CommonConstants.NoValueInt;
        _cuisinesToAvoid = CommonConstants.NoValueInt;
        _additionalNights = CommonConstants.NoValueInt;
        _desiredMaritalStatus = CommonConstants.NoValueInt;
        _desiredEducationLevel = CommonConstants.NoValueInt;
        _desiredDrinkingHabits = CommonConstants.NoValueInt;
        _desiredSmokingHabits = CommonConstants.NoValueInt;
        _birthdate = CommonConstants.NoValueDate;
        _contractReceived = false;
        _loveMostAboutJDate = MemberLoveMostAboutJDate.NoValue;
        _personalityQuestion1 = MemberPersonalityQuestion1.NoValue;
        _personalityQuestion2 = MemberPersonalityQuestion2.NoValue;
        _personalityQuestion3 = MemberPersonalityQuestion3.NoValue;
        _personalityQuestion4 = MemberPersonalityQuestion4.NoValue;
        _personalityQuestion5 = MemberPersonalityQuestion5.NoValue;
        _personalityQuestion6 = MemberPersonalityQuestion6.NoValue;
        _personalityQuestion7 = MemberPersonalityQuestion7.NoValue;
        _personalityQuestion8 = MemberPersonalityQuestion8.NoValue;
        _personalityQuestion9 = MemberPersonalityQuestion9.NoValue;
        _personalityQuestion10 = MemberPersonalityQuestion10.NoValue;
        _personalityQuestion11 = MemberPersonalityQuestion11.NoValue;
        _personalityQuestion12 = MemberPersonalityQuestion12.NoValue;
        _applicationRemarks = null;
        _inPersonInterviewTime = null;
        _phoneInterviewNotes = null;
        _interviewQuestionaireNotes = null;
        _relationship = MemberRelationship.NoValue;
        _whatElseToKnow = null;
        _whenToCall = null;
        _applicationReceived = CommonConstants.NoValueDate; 
        _feesChargedText = null;
        _emailOptedOut = false;
        _trxId = null;
        _guid = null;
        _phoneInterviews = new ArrayList();
        _jdateUrl = null;
    }


    /**
     * Returns the member id passed to the UI
     */
    public string heightToString()
    {
        return heightToString(_height);
    }


    /**
     * Returns the member id passed to the UI
     */
    public static string heightToString(int h)
    {
        if (h == CommonConstants.NoValueInt)
            return "";
        int feet = h / 12;
        int inches = h - (12 * feet);

        // double excape the quotes or it will mess up the xsl.
        string sInches = inches == 0 ? "" : inches.ToString() + "\\\"";
        return feet.ToString() + "\\'" + sInches;
    }
}
}
