using System;
using System.Collections.Specialized;

/**
 * This is what the PageHandler service send back
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class PageRequest
{
    private string _pageName;
    public string pageName { get { return _pageName; } set { _pageName = value; } }

    private NameValueCollection _queryParams;
    public NameValueCollection queryParams { get { return _queryParams; } set { _queryParams = value; } }

    private int _memberId;
    public int memberId { get { return _memberId; } set { _memberId = value; } }

    private int _sparkId;
    public int sparkId { get { return _sparkId; } set { _sparkId = value; } }

    private bool _adminAuthenticated;
    public bool adminAuthenticated { get { return _adminAuthenticated; } set { _adminAuthenticated = value; } }


    /**
     * Constructor
     */
	public PageRequest(string pageName, NameValueCollection queryParams, 
        int memberId, int sparkId, bool adminAuthenticated)
	{ 
        _pageName = pageName;
        _queryParams = queryParams;
        _memberId = memberId;
        _sparkId = sparkId;
        _adminAuthenticated = adminAuthenticated;
	}
}
}
