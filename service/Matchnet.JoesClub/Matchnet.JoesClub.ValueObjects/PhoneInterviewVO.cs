using System;
using System.Collections;


/**
 * ValueObject corresponding to Matchnet.JoesClub.BusinessLogic.Event
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class PhoneInterviewVO : DbObjectVO
{
    private int _memberId;
    private InterviewStatus _interviewStatus;
	private InterviewScheduleStatus _interviewScheduleStatus;
	private DateTime _scheduleDate;
    private string _caller;
    private DateTime _whenCalled;
    private bool _gaveContract;

    public int memberId { get { return _memberId; } set { _memberId = value; } }
    public InterviewStatus interviewStatus { get { return _interviewStatus; } set { _interviewStatus = value; } }
	public InterviewScheduleStatus interviewScheduleStatus { get { return _interviewScheduleStatus; } set { _interviewScheduleStatus = value; } }
	public DateTime scheduleDate { get { return _scheduleDate; } set { _scheduleDate = value; } }
    public string caller { get { return _caller; } set { _caller = value; } }
    public DateTime whenCalled { get { return _whenCalled; } set { _whenCalled = value; } }
    public bool gaveContract { get { return _gaveContract; } set { _gaveContract = value; } }


    /**
     * Constructor
     */
    public PhoneInterviewVO(
        int id, 
        int memberId,
        InterviewStatus interviewStatus,
        string caller,
        DateTime whenCalled,
        bool gaveContract) : base(id)
    {
        _memberId = memberId;
        _interviewStatus = interviewStatus;
        _caller = caller;
        _whenCalled = whenCalled;
        _gaveContract = gaveContract;
    }

}
}
