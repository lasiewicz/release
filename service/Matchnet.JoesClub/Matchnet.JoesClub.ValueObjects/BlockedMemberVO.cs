using System;
using System.Collections;


/**
 * ValueObject corresponding to Matchnet.JoesClub.BusinessLogic.BlockedMember
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class BlockedMemberVO
{
    private int _memberId;
    private string _firstName;
    private string _lastName;

    public int memberId { get { return _memberId; } set { _memberId = value; } }
    public string firstName { get { return _firstName; } set { _firstName = value; } }
    public string lastName { get { return _lastName; } set { _lastName = value; } }


    /**
     * Constructor
     */
    public BlockedMemberVO(
        int memberId,
        string firstName,
        string lastName)
    {
        _memberId = memberId;
        _firstName = firstName;
        _lastName = lastName;
    }

}
}
