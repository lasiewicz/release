using System;

/**
 * This is what the PageHandler service send back
 */
namespace Matchnet.JoesClub.ValueObjects
{
[Serializable]
public class PageResponse
{

    // PageResponse Actions
    public const int ActionNone = CommonConstants.NoValueInt;
    public const int ActionLogon = 1;
    public const int ActionLogonJoesClubOnly = 2;
    public const int ActionLogoff = 3;
    public const int ActionAdminLogon = 4;
    public const int ActionAdminLogoff = 5;

    private string _output;
    public string output { get { return _output; } set { _output = value; } }

    private string _redirectUrl;
    public string redirectUrl { get { return _redirectUrl; } set { _redirectUrl = value; } }

    private int _action;
    public int action { get { return _action; } set { _action = value; } }

    private int _memberId;
    public int memberId { get { return _memberId; } set { _memberId = value; } }

    private int _sparkId;
    public int sparkId { get { return _sparkId; } set { _sparkId = value; } }

    private bool _adminAuthenticated;
    public bool adminAuthenticated { get { return _adminAuthenticated; } set { _adminAuthenticated = value; } }


    /**
     * Constructor
     */
    public PageResponse()
    { 
        _action = ActionNone;
        _memberId = CommonConstants.NoValueInt;
        _sparkId = CommonConstants.NoValueInt;
        _adminAuthenticated = false;
    }


}
}
