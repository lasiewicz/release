using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Charge.ServiceDefinitions;
using Matchnet.Charge.ValueObjects;


namespace Matchnet.Charge.ServiceManagers
{
	public class ChargeSM : IChargeService
	{
		public ChargeSM()
		{
		}


		public PaymentResponse ProcessPaymentRequest(PaymentRequest paymentRequest)
		{
			try
			{
				return null;
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"ProcessPaymentRequest() error.",
					ex);
			}
		}


		public static void PerfCounterInstall()
		{
		}


		public static void PerfCounterUninstall()
		{
		}
	}
}
