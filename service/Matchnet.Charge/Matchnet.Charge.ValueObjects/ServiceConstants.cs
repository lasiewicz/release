using System;

namespace Matchnet.Charge.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "CHARGE_SVC";
		public const string SERVICE_NAME = "Matchnet.Charge.Service";
		public const string SERVICE_MANAGER_NAME = "ChargeSM";


		private ServiceConstants()
		{
		}
	}
}
