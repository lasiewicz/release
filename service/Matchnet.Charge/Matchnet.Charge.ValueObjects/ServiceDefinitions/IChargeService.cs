using System;

using Matchnet.Charge.ValueObjects;


namespace Matchnet.Charge.ServiceDefinitions
{
	public interface IChargeService
	{
		PaymentResponse ProcessPaymentRequest(PaymentRequest paymentRequest);
	}
}
