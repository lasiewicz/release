using System;

namespace Matchnet.Charge.ValueObjects
{
	[Serializable]
	public class PaymentResult
	{
		private Int32 _responseCode;

		public PaymentResult(Int32 responseCode)
		{
			_responseCode = responseCode;
		}


		public Int32 ResponseCode
		{
			get
			{
				return _responseCode;
			}
		}
	}
}
