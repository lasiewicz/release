using System;

namespace Matchnet.Charge.ValueObjects
{
	[Serializable]
	public class PaymentRequest
	{
		private Int32 _merchantID;

		public PaymentRequest(Int32 merchantID)
		{
			_merchantID = merchantID;
		}


		public Int32 MerchantID
		{
			get
			{
				return _merchantID;
			}
		}
	}
}
