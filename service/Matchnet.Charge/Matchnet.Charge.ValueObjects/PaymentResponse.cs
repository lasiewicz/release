using System;

namespace Matchnet.Charge.ValueObjects
{
	[Serializable]
	public class PaymentResponse
	{
		private Int32 _responseCode;

		public PaymentResponse(Int32 responseCode)
		{
			_responseCode = responseCode;
		}


		public Int32 ResponseCode
		{
			get
			{
				return _responseCode;
			}
		}
	}
}
