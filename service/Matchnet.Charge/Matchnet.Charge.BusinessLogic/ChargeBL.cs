using System;
using System.Data;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Charge.ValueObjects;


namespace Matchnet.Charge.BusinessLogic
{
	public class ChargeBL
	{
		public static readonly ChargeBL Instance = new ChargeBL();

		private ChargeBL()
		{
		}


		public PaymentResponse ProcessPaymentRequest(PaymentRequest paymentRequest)
		{
			// PaymentInfo Parameters
			Int32 paymentInfoID;
			Int32 paymentType;
			string firstName;
			string lastName;
			string driversLicenseNumber;
			string driversLicenseState;
			string phoneNumber;
			string addressLine1;
			string addressLine2;
			string city;
			string state;
			string postalCode;
			Int32 countryRegionID;
			Int32 creditCardExpirationMonth;
			Int32 creditCardExpirationYear;
			Int32 creditCardTypeID;
			string accountNumber;
			string routingNumber;

			// Charge Parmameters
			Int32 chargeID;
			Int32 merchantID;
			Int32 clientRefID;
			Int32 memberPaymentID;
			Int32 chargeTypeID;
			decimal chargeAmount;
			Int32 chargeStatus;

			try
			{
				/*
				Begin Tran
					*Insert MemberPayment records
					Insert Charge record
				End Tran
				*/
				Command[] preProviderCommands;
				Command commandPaymentInfoInsert = null;
				Command commandChargeInsert = null;

				if (true) //todo: only need to save if this is a new payment
				{
					commandPaymentInfoInsert = new Command("mnCharge", "dbo.up_PaymentInfo_Save", 0);
					//todo: add parameters to commandMemberPayment
					commandPaymentInfoInsert.AddParameter("@PaymentInfoID", SqlDbType.Int, ParameterDirection.Input, paymentInfoID);
					commandPaymentInfoInsert.AddParameter("@PaymentType", SqlDbType.Int, ParameterDirection.Input, paymentType);
					commandPaymentInfoInsert.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, firstName);
					commandPaymentInfoInsert.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, lastName);
					commandPaymentInfoInsert.AddParameter("@DriversLicenseNumber", SqlDbType.NVarChar, ParameterDirection.Input, driversLicenseNumber);
					commandPaymentInfoInsert.AddParameter("@DriversLicenseState", SqlDbType.NVarChar, ParameterDirection.Input, driversLicenseState);
					commandPaymentInfoInsert.AddParameter("@PhoneNumber", SqlDbType.NVarChar, ParameterDirection.Input, phoneNumber);
					commandPaymentInfoInsert.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, addressLine1);
					commandPaymentInfoInsert.AddParameter("@AddressLine2", SqlDbType.NVarChar, ParameterDirection.Input, addressLine2);
					commandPaymentInfoInsert.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, city);
					commandPaymentInfoInsert.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, state);
					commandPaymentInfoInsert.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, postalCode);
					commandPaymentInfoInsert.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, memberPaymentId);
					commandPaymentInfoInsert.AddParameter("@CreditCardExpirationMonth", SqlDbType.Int, ParameterDirection.Input, creditCardExpirationMonth);
					commandPaymentInfoInsert.AddParameter("@CreditCardExpirationYear", SqlDbType.Int, ParameterDirection.Input, creditCardExpirationYear);
					commandPaymentInfoInsert.AddParameter("@CreditCardTypeID", SqlDbType.Int, ParameterDirection.Input, creditCardTypeID);
					commandPaymentInfoInsert.AddParameter("@AccountNumber", SqlDbType.VarChar, ParameterDirection.Input, accountNumber);
					commandPaymentInfoInsert.AddParameter("@RoutingNumber", SqlDbType.VarChar, ParameterDirection.Input, routingNumber);
					commandPaymentInfoInsert.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

					preProviderCommands = (Command[])Array.CreateInstance(typeof(Command), 2);
					preProviderCommands[0] = commandMemberPayment;
				}
				else
				{
					preProviderCommands = (Command[])Array.CreateInstance(typeof(Command), 1);
				}

				commandChargeInsert = new Command("mnCharge", "dbo.up_Charge_Save", 0);
				//todo: add parameters to commandChargeInsert
				commandPaymentInfoInsert.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, chargeID);
				commandPaymentInfoInsert.AddParameter("@MerchantID", SqlDbType.Int, ParameterDirection.Input, merchantID);
				commandPaymentInfoInsert.AddParameter("@ClientRefID", SqlDbType.Int, ParameterDirection.Input, cleintRefID);
				commandPaymentInfoInsert.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPayemntID);
				commandPaymentInfoInsert.AddParameter("@ChargeTypeID", SqlDbType.Int, ParameterDirection.Input, chargeTypeID);
				commandPaymentInfoInsert.AddParameter("@ChargeAmount", SqlDbType.Decimal, ParameterDirection.Input, chargeAmount);
				commandPaymentInfoInsert.AddParameter("@ChargeStatus", SqlDbType.Int, ParameterDirection.Input, chargeStatus);
				commandPaymentInfoInsert.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

				preProviderCommands[preProviderCommands.Length - 1] = commandChargeInsert;


				Matchnet.Data.Hydra.SyncWriter sw = new SyncWriter();
				Exception swEx = null;
				sw.Execute(preProviderCommands, out swEx);
				if (swEx != null)
				{
					throw swEx;
				}



				//exercise payment provider

				/*
				Begin Tran
					Update Charge record
					Insert ChargeLog records
				End Tran
				*/


				return null;
			}
			catch (Exception ex)
			{
				throw new Exception("ProcessPaymentRequest() error.", ex);
			}
		}
	}
}
