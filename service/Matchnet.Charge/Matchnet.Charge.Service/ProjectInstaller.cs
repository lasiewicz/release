using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

using Matchnet.Charge.ServiceManagers;
using Matchnet.Charge.ValueObjects;

namespace Matchnet.Charge.Service
{
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
		private System.ServiceProcess.ServiceInstaller serviceInstaller1;
		private System.ComponentModel.Container components = null;

		public ProjectInstaller()
		{
			InitializeComponent();
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		private void InitializeComponent()
		{
			this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
			this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
			this.serviceProcessInstaller1.Password = null;
			this.serviceProcessInstaller1.Username = null;
			this.serviceInstaller1.DisplayName = ServiceConstants.SERVICE_NAME;
			this.serviceInstaller1.ServiceName = ServiceConstants.SERVICE_NAME;
			this.serviceInstaller1.ServicesDependedOn = new string[] {
																		 "MSMQ"};
			this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller1,
																					  this.serviceInstaller1});

			this.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_BeforeUninstall);
			this.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterInstall);
		}


		private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
		{
			ChargeSM.PerfCounterInstall();
		}


		private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
		{
			ChargeSM.PerfCounterUninstall();
		}
	}
}
