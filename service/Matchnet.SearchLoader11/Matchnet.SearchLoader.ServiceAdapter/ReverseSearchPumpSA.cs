using System;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.SearchLoader.ValueObjects;
using Matchnet.SearchLoader.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingClient;

namespace Matchnet.SearchLoader.ServiceAdapter
{
	public class ReverseSearchPumpSA : SABase
	{
		public static readonly ReverseSearchPumpSA Instance = new ReverseSearchPumpSA();

		private ReverseSearchPumpSA()
		{

		}
		
		public void DeleteMember(int communityID, int memberID)
		{
			string uri = string.Empty;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					getService(uri).DeleteMember(communityID, memberID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw (new SAException("Error occurred while attempting to delete member. (uri: " + uri + ")", ex));
			} 
		}

		internal static ISearchLoaderService getService(string uri)
		{
			try
			{
				return (ISearchLoaderService)Activator.GetObject(typeof(ISearchLoaderService), uri);
			}
			catch (Exception ex)
			{
				throw (new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		internal static string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHLOADERSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
			}
		}

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHLOADERSVC_SA_CONNECTION_LIMIT"));
		}
	}
}
