using System;
using System.Text;

namespace Matchnet.SearchLoader.ValueObjects.ServiceDefinitions
{
	public interface ISearchLoaderService
	{
		void DeleteMember(int communityID, int memberID);
	}
}
