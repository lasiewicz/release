﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.ValueObjects.ServiceDefinition;

namespace Spark.SearchEngine.Service
{
    // NOTE: If you change the class name "LoaderSvc" here, you must also update the reference to "LoaderSvc" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class LoaderSvc : ILoader
    {
        public void IndexMember(SearchMember member)
        {
        }

        public void IndexCommunity(int communityid)
        {
        }
        public int GetCommunityQueueCounts(int communityid)
        {
            throw new NotImplementedException();
        }

        public string GetCurrentIndexDirectory(int communityid, string server) { return ""; }
    }

}
