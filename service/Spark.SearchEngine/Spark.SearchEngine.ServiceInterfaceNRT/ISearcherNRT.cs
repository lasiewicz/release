﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Spark.SearchEngine.ValueObjects;

namespace Spark.SearchEngine.ServiceInterfaceNRT
{
    [ServiceContract(Namespace = "spark.net")]
    public interface ISearcherNRT
    {
        [OperationContract]
        void NRTUpdateMember(SearchMember searchMember);

        [OperationContract]
        void NRTUpdateMemberByDB(SearchMemberUpdate searchMemberUpdate);

        [OperationContract]
        void NRTRemoveMember(SearchMemberUpdate searchMemberUpdate);
    }
}
