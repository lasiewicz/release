﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Spark.BBWPersonalsPlus.Searcher.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new BBWPersonalsPlusSearcherService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
