﻿using System;
using System.Diagnostics;
using Matchnet;

using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.BusinessLogic.Indexer;
using Spark.Logging;
using Matchnet.Data.Hydra;
using Spark.SearchEngine.ServiceInterface;
namespace Spark.SearchEngine.ServiceManager
{
    public class LoaderSM : MarshalByRefObject, ILoader, IServiceManager, IDisposable
    {
        private HydraWriter _hydraWriter = null;


        public LoaderSM()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_INDEXER_CONST, "LoaderSM", "constructor");
                LoaderBL.Instance.Start();
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnsystem", "mnSearchStore" , "SearchLoad" });
                _hydraWriter.Start();
                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_INDEXER_CONST, "LoaderSM", "constructor");
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_INDEXER_NAME, "LoaderSM", "constructor");
            }
        }


        #region IServiceManager
        public void PrePopulateCache()
        {
        }
        public override object InitializeLifetimeService()
        { return null; }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
            LoaderBL.Instance.Stop();
        }
        #endregion


        #region performance counters
        public static void PerfCounterInstall()
        {
            try
            {
                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());
                PerformanceCounterCategory.Create(ServiceConstants.SERVICE_INDEXER_NAME, ServiceConstants.SERVICE_INDEXER_NAME, ccdc);

            }
            catch (Exception ex)
            { ; }
        }


        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_INDEXER_NAME);
        }

        public static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { };
            //PerformanceCounterCategory.Create(Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.PhotoSearch.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
        }



        private void initPerfCounters()
        {
           

        }


        private void resetPerfCounters()
        {
          


        }
     
        #endregion

    }
}
