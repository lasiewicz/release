﻿using System.ComponentModel;
using System.Configuration.Install;


namespace Spark.Spark.Searcher.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
