﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Search.ServiceAdapters;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.BusinessLogic.Searcher;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;

namespace TestHarness
{
    public partial class MatchRating : Form
    {
        public MatchRating()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int matchPercent = 0;
            //set up data, using dictionaries instead of what they would be (i.e. lucene docs and such)

            //set up searcher "has" data
            Dictionary<string, object> searcherMemberData = new Dictionary<string, object>();
            searcherMemberData.Add("hasbirthdate", (new DateTime(1975, 1, 1)).ToString());
            searcherMemberData.Add("hasmaritalstatus", 2);

            //set up searcher "prefs/wants" data
            searcherMemberData.Add("agemin", 25);
            searcherMemberData.Add("agemax", 50);
            searcherMemberData.Add("maritalstatus", 2);


            //set up resulting member "has" data
            Dictionary<string, object> resultMemberData = new Dictionary<string,object>();
            resultMemberData.Add("birthdate", (new DateTime(1975, 1, 1)).Ticks);
            resultMemberData.Add("maritalstatus", 2);

            //set up resulting member "prefs/wants" data
            resultMemberData.Add("preferredminage", 40);
            resultMemberData.Add("preferredmaxage", 50);
            resultMemberData.Add("preferredmaritalstatus", 10);

            //perform match calculation
            string[] attrNames = new string[]
                                             {
                                                 "birthdate", "maritalstatus", "jdatereligion", "religion",
                                                 "keepkosher", "synagogueattendance", "educationlevel",
                                                 "childrencount", "morechildrenflag", "bodytype", "languagemask",
                                                 "height", "smokinghabits", "activitylevel", "custody",
                                                 "drinkinghabits", "relocateflag", "jdateethnicity", "ethnicity"
                                             };

            MatchRatingUtil.MatchRatingCalculationVersion matchVersion = MatchRatingUtil.MatchRatingCalculationVersion.Version2;
            double defaultImportance = 3;

            double searcherMemberSubTotal = 1;
            double resultMemberSubTotal = 1;

            foreach (string attrName in attrNames)
                    {
                        if (attrName == "birthdate")
                        {
                            //searcher member wants
                            DateTime resultMemberHasBirthdate = (resultMemberData.ContainsKey(attrName)) ? new DateTime(long.Parse(resultMemberData[attrName].ToString())) : DateTime.MinValue;
                            int resultMemberAge = 18;
                            if (resultMemberHasBirthdate > DateTime.MinValue)
                            {
                                resultMemberAge = DateTime.Now.Year - resultMemberHasBirthdate.Year;
                            }

                            double searcherMemberImportance = defaultImportance;
                            int searcherMemberWantsMin = (searcherMemberData.ContainsKey("agemin")) ? Convert.ToInt32(searcherMemberData["agemin"]) : 18;
                            int searcherMemberWantsMax = (searcherMemberData.ContainsKey("agemax")) ? Convert.ToInt32(searcherMemberData["agemax"]) : 99;
                            if (searcherMemberWantsMin < 18)
                                searcherMemberWantsMin = 18;
                            if (searcherMemberWantsMax > 99 || searcherMemberWantsMax < searcherMemberWantsMin)
                                searcherMemberWantsMax = 99;

                            if ((resultMemberAge < searcherMemberWantsMin) || (resultMemberAge > searcherMemberWantsMax))
                            {
                                if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                {
                                    searcherMemberSubTotal -= ((searcherMemberImportance * searcherMemberImportance) / 100);
                                }
                                else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                {
                                    searcherMemberSubTotal *= 1 - ((searcherMemberImportance * searcherMemberImportance) / 100);
                                }
                            }

                            //result member wants
                            int resultMemberWantsMin = (resultMemberData.ContainsKey("preferredminage")) ? Int32.Parse(resultMemberData["preferredminage"].ToString()) : 18;
                            int resultMemberWantsMax = (resultMemberData.ContainsKey("preferredmaxage")) ? Int32.Parse(resultMemberData["preferredmaxage"].ToString()) : 99;
                            if (resultMemberWantsMin < 18)
                                resultMemberWantsMin = 18;
                            if (resultMemberWantsMax > 99 || resultMemberWantsMax < resultMemberWantsMin)
                                resultMemberWantsMax = 99;

                            double resultMemberImportance = defaultImportance;

                            DateTime searchMemberHasBirthdate = (searcherMemberData.ContainsKey("has" + attrName)) ? Convert.ToDateTime(searcherMemberData["has" + attrName]) : DateTime.MinValue;
                            int searcherMemberAge = 18;
                            if (searchMemberHasBirthdate > DateTime.MinValue)
                            {
                                searcherMemberAge = DateTime.Now.Year - searchMemberHasBirthdate.Year;
                            }

                            if ((searcherMemberAge < resultMemberWantsMin) || (searcherMemberAge > resultMemberWantsMax))
                            {
                                if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                {
                                    resultMemberSubTotal -= ((resultMemberImportance * resultMemberImportance) / 100);
                                }
                                else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                {
                                    resultMemberSubTotal *= 1 - ((resultMemberImportance * resultMemberImportance) / 100);
                                }
                            }
                        }
                        else if (attrName == "height")
                        {
                            //searcher member wants
                            int resultMemberHas = (resultMemberData.ContainsKey(attrName)) ? Int32.Parse(resultMemberData[attrName].ToString()) : int.MinValue;
                            double searcherMemberImportance = defaultImportance;
                            int searcherMemberWantsMin = (searcherMemberData.ContainsKey("heightmin")) ? Convert.ToInt32(searcherMemberData["heightmin"]) : int.MinValue;
                            int searcherMemberWantsMax = (searcherMemberData.ContainsKey("heightmax")) ? Convert.ToInt32(searcherMemberData["heightmax"]) : int.MinValue;

                            if ((searcherMemberWantsMin >= 0 && resultMemberHas < searcherMemberWantsMin)
                                || (searcherMemberWantsMax >= 0 && resultMemberHas > searcherMemberWantsMax))
                            {
                                if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                {
                                    searcherMemberSubTotal -= ((searcherMemberImportance * searcherMemberImportance) / 100);
                                }
                                else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                {
                                    searcherMemberSubTotal *= 1 - ((searcherMemberImportance * searcherMemberImportance) / 100);
                                }
                            }

                            //result member wants
                            int resultMemberWantsMin = (resultMemberData.ContainsKey("preferredminheight")) ? Int32.Parse(resultMemberData["preferredminheight"].ToString()) : int.MinValue;
                            int resultMemberWantsMax = (resultMemberData.ContainsKey("preferredmaxheight")) ? Int32.Parse(resultMemberData["preferredmaxheight"].ToString()) : int.MinValue;
                            double resultMemberImportance = defaultImportance;
                            int searcherMemberHas = (searcherMemberData.ContainsKey("has" + attrName)) ? Convert.ToInt32(searcherMemberData["has" + attrName]) : int.MinValue;
                            
                            if ((resultMemberWantsMin >= 0 && searcherMemberHas < resultMemberWantsMin)
                                || (resultMemberWantsMax >= 0 && searcherMemberHas > resultMemberWantsMax))
                            {
                                if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                {
                                    resultMemberSubTotal -= ((resultMemberImportance * resultMemberImportance) / 100);
                                }
                                else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                {
                                    resultMemberSubTotal *= 1 - ((resultMemberImportance * resultMemberImportance) / 100);
                                }
                            }
                        }
                        else
                        {
                            //searcher member wants
                            int resultMemberHas = (resultMemberData.ContainsKey(attrName)) ? Int32.Parse(resultMemberData[attrName].ToString()) : int.MinValue;
                            double searcherMemberImportance = defaultImportance;
                            int searcherMemberWants = (searcherMemberData.ContainsKey(attrName)) ? Convert.ToInt32(searcherMemberData[attrName]) : int.MinValue;

                            if (searcherMemberWants >= 0 && (resultMemberHas & searcherMemberWants) != resultMemberHas)
                            {
                                if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                {
                                    searcherMemberSubTotal -= ((searcherMemberImportance * searcherMemberImportance) / 100);
                                }
                                else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                {
                                    searcherMemberSubTotal *= 1 - ((searcherMemberImportance * searcherMemberImportance) / 100);
                                }
                            }

                            //result member wants
                            int resultMemberWants = (resultMemberData.ContainsKey("preferred" + attrName)) ? Int32.Parse(resultMemberData["preferred" + attrName].ToString()) : int.MinValue;
                            double resultMemberImportance = defaultImportance;
                            int searcherMemberHas = (searcherMemberData.ContainsKey("has" + attrName)) ? Convert.ToInt32(searcherMemberData["has" + attrName]) : int.MinValue;
                            if (resultMemberWants >= 0 && (searcherMemberHas & resultMemberWants) != searcherMemberHas)
                            {
                                if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                {
                                    resultMemberSubTotal -= ((resultMemberImportance * resultMemberImportance) / 100);
                                }
                                else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                {
                                    resultMemberSubTotal *= 1 - ((resultMemberImportance * resultMemberImportance) / 100);
                                }
                            }
                        }
                    }

            if (searcherMemberSubTotal < 0)
                searcherMemberSubTotal = 0;

            if (resultMemberSubTotal < 0)
                resultMemberSubTotal = 0;

            int searcherMemberPercent = 50;
            int resultMemberPercent = 100 - searcherMemberPercent;

            double searcherMemberPercentTotal = (searcherMemberPercent * searcherMemberSubTotal);
            double resultMemberPercentTotal = (resultMemberPercent * resultMemberSubTotal);
            matchPercent = (int)Math.Floor(searcherMemberPercentTotal + resultMemberPercentTotal);
            txtMatchRating.Text = string.Format("Searcher Member:{0}, Result Member:{1}, MatchPercentage:{2}\n", searcherMemberPercentTotal, resultMemberPercentTotal, matchPercent);
                    
        }
    }
}
