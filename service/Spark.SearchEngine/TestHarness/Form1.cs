﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows.Forms;
using Lucene.Net.Spatial.Queries;
using Spark.SearchEngine.BusinessLogic;
using Spark.SearchEngine.BusinessLogic.Indexer;
using Spark.SearchEngine.ServiceAdapters;
using Spark.SearchEngine.ValueObjects;
using Lucene.Net;
using Lucene.Net.Store;
using Lucene.Net.Index;
using Lucene.Net.Spatial;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;
using Lucene.Net.Documents;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;
using Point = System.Drawing.Point;
using Version = Lucene.Net.Util.Version;

namespace TestHarness
{
    public partial class btnSearchIndex : Form
    {
        private LoaderBL _loader = null;
      
        public btnSearchIndex()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int commid=Int32.Parse(txtCommunityID.Text);
            LoaderSA.Instance.TestURI="tcp://bhd-arodriguez:64700/LoaderSM.rem";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int agemin=Int32.Parse(txtAgeMin.Text);
            int agemax=Int32.Parse(txtAgeMax.Text);
            int gendermask=Int32.Parse(txtGenderMask.Text);

           //double longitude=Double.Parse(txtLongitude.Text);
          //  double latitude = Double.Parse(txtLatitude.Text);
           

            DateTime bmin = DateTime.Now.AddYears(-agemin);
            DateTime bmax = DateTime.Now.AddYears( -agemax);

           Query q1 = NumericRangeQuery.NewLongRange("birthdate", bmax.Ticks, bmin.Ticks, true, true);
           Term t1 = new Term("gendermask", txtGenderMask.Text);
           Query q2 = NumericRangeQuery.NewIntRange("gendermask", gendermask, gendermask, true, true);

            BooleanQuery boolQuery = new BooleanQuery();

            boolQuery.Add(q1, Occur.MUST);
            boolQuery.Add(q2, Occur.MUST);

            //geosearch

//            DistanceQueryBuilder dq;
        //    dq = new DistanceQueryBuilder(latitude,longitude, 200,"latitude","longitude","_localTier", true);

          //  DistanceFieldComparatorSource dsort;
         //   dsort = new DistanceFieldComparatorSource(dq.DistanceFilter);
          //  Sort sort = new Sort(new SortField("distance", dsort));

            System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(txtIndexPath.Text);
            Directory dir = FSDirectory.Open(dirinfo);
            IndexSearcher searcher = new IndexSearcher(dir);
          //  TopDocs matches = searcher.Search(boolQuery,dq.DistanceFilter, 1000,sort);
            TopDocs matches = searcher.Search(boolQuery, 1000);

            List<int> memberids = new List<int>();
            for (int i = 0; i < matches.ScoreDocs.Length; i++)
            {
             Document doc = searcher.Doc(matches.ScoreDocs[i].Doc);
             int id=Int32.Parse(doc.GetField("id").StringValue );

             memberids.Add(id);
                
            }
          
            searcher.Dispose();
            dir.Dispose();
            dgvMembers.DataSource = memberids;
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtIndexPath.Text = "d:\\\\matchnet\\index\\jdate";
            txtGenderMask.Text = "6";
            txtLongitude.Text = "-2.065607";
            txtLatitude.Text = "0.594622";
            txtCommunityID.Text = "3";
            txtMemberURI.Text = "tcp://devapp01:42000/MemberSM.rem";

        }

        private void btnGeoSearch_Click(object sender, EventArgs e)
        {
            IndexSearcher searcher = null;
            int agemin = Int32.Parse(txtAgeMin.Text);
            int agemax = Int32.Parse(txtAgeMax.Text);
        
            //double longitude=Double.Parse(txtLongitude.Text);
            //  double latitude = Double.Parse(txtLatitude.Text);


            DateTime bmin = DateTime.Now.AddYears(-agemin);
            DateTime bmax = DateTime.Now.AddYears(-agemax);

            Query age = NumericRangeQuery.NewLongRange("birthdate", bmax.Ticks, bmin.Ticks, true, true);


            double longitude = Double.Parse(txtLongitude.Text);
            double latitude = Double.Parse(txtLatitude.Text);
         
            
            double radius = double.Parse(txtRadius.Text);
            int gendermask = Int32.Parse(txtGenderMask.Text);


            // QueryParser q = new QueryParser(String.Format(query,gendermask new Lucene.Net.Analysis.SimpleAnalyzer());
            Query g1 = NumericRangeQuery.NewIntRange("gendermask", gendermask, gendermask, true, true);
            Query g2 = NumericRangeQuery.NewIntRange("gendermask", 9, 9, true, true);

            Query q0 = NumericRangeQuery.NewIntRange("maritalstatus", 0, 0, true, true);
            Query q2 = NumericRangeQuery.NewIntRange("maritalstatus", 2, 2, true, true);
            Query q4 = NumericRangeQuery.NewIntRange("maritalstatus", 4, 4, true, true);
            Query q8 = NumericRangeQuery.NewIntRange("maritalstatus", 8, 8, true, true);
            Query q16 = NumericRangeQuery.NewIntRange("maritalstatus", 16, 16, true, true);

            Query b0 = NumericRangeQuery.NewIntRange("bodytype", 0, 0, true, true);
            Query b2 = NumericRangeQuery.NewIntRange("bodytype", 2, 2, true, true);
            Query b4 = NumericRangeQuery.NewIntRange("bodytype", 4, 4, true, true);
            Query b8 = NumericRangeQuery.NewIntRange("bodytype", 8, 8, true, true);
            Query b16 = NumericRangeQuery.NewIntRange("bodytype", 16, 16, true, true);
             //q.Add(new Term[]{new Term("bodytype","0"),new Term("bodytype","16")});

            Query e0 = NumericRangeQuery.NewLongRange("languagemask", 0, 0, true, true);
            Query e2 = NumericRangeQuery.NewLongRange("languagemask", 2, 2, true, true);
            Query e4 = NumericRangeQuery.NewLongRange("languagemask", 4, 4, true, true);
           

            BooleanQuery gender = new BooleanQuery();
            gender.Add(g1, Occur.SHOULD);
        //    gender.Add(g2, Occur.SHOULD);

            
            BooleanQuery maritalstatus = new BooleanQuery();

            maritalstatus.Add(q0, Occur.SHOULD);
            maritalstatus.Add(q2, Occur.SHOULD);
            maritalstatus.Add(q4, Occur.SHOULD);
           // body.Add(q8, Occur.SHOULD);
            maritalstatus.Add(q16, Occur.SHOULD);


            BooleanQuery body = new BooleanQuery();

            body.Add(b0, Occur.SHOULD);
            body.Add(b2, Occur.SHOULD);
            body.Add(b4, Occur.SHOULD);
            body.Add(b8, Occur.SHOULD);
            body.Add(b16, Occur.SHOULD);


            BooleanQuery education = new BooleanQuery();

            education.Add(e0, Occur.SHOULD);
            education.Add(e2, Occur.SHOULD);
            education.Add(e4, Occur.SHOULD);
            BooleanQuery boolQuery = new BooleanQuery();


          boolQuery.Add(gender, Occur.MUST);
          boolQuery.Add(age, Occur.MUST);

      //    Term t1 = new Term("relationshipmask", 2);
          boolQuery.Add(maritalstatus, Occur.MUST);
        //  boolQuery.Add(body, Occur.MUST);
          boolQuery.Add(education, Occur.MUST);
       //   boolQuery.Add(body, Occur.MUST);



          radius = Utils.GetRadiusOffsetBuffer(radius);
          Spatial4n.Core.Shapes.Point shapeCenter = Utils.SpatialContext.MakePoint(longitude, latitude);
          Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
          SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
          Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);
           // Sort sort = new Sort(new SortField("distance", dsort));

            System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(txtIndexPath.Text);
            Directory dir = FSDirectory.Open(dirinfo);
            
           
            if (searcher == null)
            {
                searcher = new IndexSearcher(dir);
            }
            SortField f1 = new SortField("lastactivedate",SortField.LONG,true);
            Sort sort=new Sort(f1);
            DateTime start = DateTime.Now;
            TopDocs matches = searcher.Search(boolQuery, spatialFilter, 1000, sort);
           

            List<SearchMember> memberids = new List<SearchMember>();
            DateTime finish = DateTime.Now;

            MessageBox.Show("Search Time:" + (finish - start).TotalSeconds.ToString() + ", Results: " + matches.ScoreDocs.Length.ToString());
            for (int i = 0; i < matches.ScoreDocs.Length; i++)
            {

                Document doc = searcher.Doc(matches.ScoreDocs[i].Doc);
                int id = Int32.Parse(doc.GetField("id").StringValue);
                
                SearchMember m = GetMember(id, txtMemberURI.Text, 2);
                if (m == null)
                    m = new SearchMember();
                m.MemberID = id;
                m.GenderMask = Int32.Parse(doc.GetField("gendermask").StringValue);
                if(doc.GetField("bodytype") != null)
                    m.BodyType = Int32.Parse(doc.GetField("bodytype").StringValue);
                if (doc.GetField("relationshipmask") != null)
                    m.RelationshipMask = Int32.Parse(doc.GetField("relationshipmask").StringValue);
                if (doc.GetField("maritalstatus") != null)
                    m.MaritalStatus = Int32.Parse(doc.GetField("maritalstatus").StringValue);
                m.LastActiveDate = new DateTime(Int64.Parse(doc.GetField("lastactivedate").StringValue));
                m.Birthdate = new DateTime(Int64.Parse(doc.GetField("birthdate").StringValue));
                memberids.Add(m);

            }

          //  searcher.Dispose();
            dir.Dispose();
            
            dgvMembers.DataSource = memberids;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LoaderBL.Instance.Start();
        }


        public SearchMember GetMember(int memberid, string uri, int version)
        {

            SearchMember searchmember = new SearchMember();
            try
            {
                searchmember.MemberID = memberid;
                int brandid=Int32.Parse(txtBrandID.Text);
               Matchnet.Content.ValueObjects.BrandConfig.Brand b= BrandConfigSA.Instance.GetBrandByID(brandid);
                IMemberService svc = ((IMemberService)Activator.GetObject(typeof(IMemberService), uri));

                byte[] res1 = svc.GetCachedMemberBytes(System.Environment.MachineName,
                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(300)),
                    memberid,
                    false, version);

                CachedMember m = new CachedMember(res1, 1);
                Member member = MemberSA.Instance.PopulateMemberObj(m);

               
                searchmember.MemberID = memberid;
                searchmember.CommunityID = 1003;

                Type t = searchmember.GetType();
                List<PropertyInfo> searchMemberProps = t.GetProperties().ToList<PropertyInfo>();
                searchmember.GenderMask = GetAttributeInt(member, "gendermask", b);
                searchmember.BodyType = GetAttributeInt(member, "BodyType", b);
                searchmember.MaritalStatus = GetAttributeInt(member, "MaritalStatus", b);
                searchmember.Birthdate = GetAttributeDate(member, "birthdate", b);
                searchmember.CommunityInsertDate = GetAttributeDate(member, "brandinsertdate", b);
                searchmember.LastActiveDate = GetAttributeDate(member, "brandlastlogondate", b);
                try
                {
                    int regionid = GetAttributeInt(member, "regionid", b);
                    Matchnet.Content.ValueObjects.Region.RegionLanguage region = RegionSA.Instance.RetrievePopulatedHierarchy(regionid, 2);
                    searchmember.Depth1RegionID = region.Depth1RegionID;
                    searchmember.Depth2RegionID = region.StateRegionID;
                    searchmember.Depth3RegionID = region.CityRegionID;
                    searchmember.Depth4RegionID = region.PostalCodeRegionID;
                }catch(Exception ex){}

                return searchmember;
                
                
            }
            catch (Exception ex)
            {
                return searchmember;

            }


        }


        private DateTime GetAttributeDate( Member member,string name, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                return member.GetAttributeDate(brand, name);
            }
            catch (Exception ex)
            {
                return DateTime.MinValue;
            }
        }

        private Int32 GetAttributeInt(Member member, string name, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
        {
            try
            {
                return member.GetAttributeInt(brand, name);
            }
            catch (Exception ex)
            {
                return Int32.MinValue;
            }
        }

        private void btnParser_Click(object sender, EventArgs e)
        {
            
         
            
            double longitude = Double.Parse(txtLongitude.Text);
            double latitude = Double.Parse(txtLatitude.Text);                     
            double radius = double.Parse(txtRadius.Text);



            int gendermask = Int32.Parse(txtGenderMask.Text);

           

            BooleanQuery boolQuery = new BooleanQuery();

            //use radius offset buffer to increase radius by a safe amount
            radius = Utils.GetRadiusOffsetBuffer(radius);
            Spatial4n.Core.Shapes.Point shapeCenter = Utils.SpatialContext.MakePoint(longitude, latitude);
            Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
            SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
            Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);

            
           // Sort sort = new Sort(new SortField("distance", dsort));
            if (!String.IsNullOrEmpty(txtQuery.Text))
            {
                QueryParser qp = new QueryParser(Version.LUCENE_CURRENT, "", new Lucene.Net.Analysis.SimpleAnalyzer());

                Query q = qp.Parse(txtQuery.Text);

                boolQuery.Add(q, Occur.SHOULD);
            }
            else
            {
                Query q = NumericRangeQuery.NewIntRange("gendermask", gendermask, gendermask, true, true);
                boolQuery.Add(q, Occur.MUST);
            }
            System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(txtIndexPath.Text);
            Directory dir = FSDirectory.Open(dirinfo);
            IndexSearcher searcher = new IndexSearcher(dir);
            SortField f1 = new SortField("lastactivedate",SortField.LONG,true);
            Sort sort=new Sort(f1);
            TopDocs matches = searcher.Search(boolQuery,spatialFilter,1000,sort);


            List<SearchMember> memberids = new List<SearchMember>();
            for (int i = 0; i < matches.ScoreDocs.Length; i++)
            {
                Document doc = searcher.Doc(matches.ScoreDocs[i].Doc);
                int id = Int32.Parse(doc.GetField("id").StringValue);
                SearchMember m = GetMember(id, txtMemberURI.Text, 2);
                m.LastActiveDate = new DateTime(Int64.Parse(doc.GetField("lastactivedate").StringValue));
                m.Birthdate = new DateTime(Int64.Parse(doc.GetField("birthdate").StringValue));
                memberids.Add(m);

            }
            

            searcher.Dispose();
            dir.Dispose();
            dgvMembers.DataSource = memberids;
        }
    }
}
