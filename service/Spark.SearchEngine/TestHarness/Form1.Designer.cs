﻿namespace TestHarness
{
    partial class btnSearchIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIndexCommunity = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtCommunityID = new System.Windows.Forms.TextBox();
            this.txtGenderMask = new System.Windows.Forms.TextBox();
            this.lblGender = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAgeMin = new System.Windows.Forms.TextBox();
            this.txtAgeMax = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtZipCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLongitude = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLatitude = new System.Windows.Forms.TextBox();
            this.txtIndexPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvMembers = new System.Windows.Forms.DataGridView();
            this.btnGeoSearch = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRadius = new System.Windows.Forms.TextBox();
            this.txtMemberURI = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBrandID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtQuery = new System.Windows.Forms.RichTextBox();
            this.btnParser = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMembers)).BeginInit();
            this.SuspendLayout();
            // 
            // btnIndexCommunity
            // 
            this.btnIndexCommunity.Location = new System.Drawing.Point(261, 5);
            this.btnIndexCommunity.Name = "btnIndexCommunity";
            this.btnIndexCommunity.Size = new System.Drawing.Size(109, 32);
            this.btnIndexCommunity.TabIndex = 0;
            this.btnIndexCommunity.Text = "Index Community";
            this.btnIndexCommunity.UseVisualStyleBackColor = true;
            this.btnIndexCommunity.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(634, 163);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(66, 31);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtCommunityID
            // 
            this.txtCommunityID.Location = new System.Drawing.Point(93, 12);
            this.txtCommunityID.Name = "txtCommunityID";
            this.txtCommunityID.Size = new System.Drawing.Size(102, 20);
            this.txtCommunityID.TabIndex = 2;
            // 
            // txtGenderMask
            // 
            this.txtGenderMask.Location = new System.Drawing.Point(177, 189);
            this.txtGenderMask.Name = "txtGenderMask";
            this.txtGenderMask.Size = new System.Drawing.Size(69, 20);
            this.txtGenderMask.TabIndex = 3;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(85, 189);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(71, 13);
            this.lblGender.TabIndex = 4;
            this.lblGender.Text = "Gender Mask";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Age:";
            // 
            // txtAgeMin
            // 
            this.txtAgeMin.Location = new System.Drawing.Point(177, 215);
            this.txtAgeMin.Name = "txtAgeMin";
            this.txtAgeMin.Size = new System.Drawing.Size(69, 20);
            this.txtAgeMin.TabIndex = 5;
            // 
            // txtAgeMax
            // 
            this.txtAgeMax.Location = new System.Drawing.Point(347, 215);
            this.txtAgeMax.Name = "txtAgeMax";
            this.txtAgeMax.Size = new System.Drawing.Size(69, 20);
            this.txtAgeMax.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(280, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "to";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "ZipCOde";
            // 
            // txtZipCode
            // 
            this.txtZipCode.Location = new System.Drawing.Point(177, 241);
            this.txtZipCode.Name = "txtZipCode";
            this.txtZipCode.Size = new System.Drawing.Size(69, 20);
            this.txtZipCode.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Longitude";
            // 
            // txtLongitude
            // 
            this.txtLongitude.Location = new System.Drawing.Point(177, 267);
            this.txtLongitude.Name = "txtLongitude";
            this.txtLongitude.Size = new System.Drawing.Size(101, 20);
            this.txtLongitude.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(344, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Latitude";
            // 
            // txtLatitude
            // 
            this.txtLatitude.Location = new System.Drawing.Point(401, 264);
            this.txtLatitude.Name = "txtLatitude";
            this.txtLatitude.Size = new System.Drawing.Size(69, 20);
            this.txtLatitude.TabIndex = 13;
            // 
            // txtIndexPath
            // 
            this.txtIndexPath.Location = new System.Drawing.Point(177, 163);
            this.txtIndexPath.Name = "txtIndexPath";
            this.txtIndexPath.Size = new System.Drawing.Size(412, 20);
            this.txtIndexPath.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Index Dir";
            // 
            // dgvMembers
            // 
            this.dgvMembers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMembers.Location = new System.Drawing.Point(69, 363);
            this.dgvMembers.Name = "dgvMembers";
            this.dgvMembers.Size = new System.Drawing.Size(605, 259);
            this.dgvMembers.TabIndex = 17;
            // 
            // btnGeoSearch
            // 
            this.btnGeoSearch.Location = new System.Drawing.Point(634, 235);
            this.btnGeoSearch.Name = "btnGeoSearch";
            this.btnGeoSearch.Size = new System.Drawing.Size(116, 31);
            this.btnGeoSearch.TabIndex = 18;
            this.btnGeoSearch.Text = "Geo Search";
            this.btnGeoSearch.UseVisualStyleBackColor = true;
            this.btnGeoSearch.Click += new System.EventHandler(this.btnGeoSearch_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(441, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 32);
            this.button1.TabIndex = 19;
            this.button1.Text = "Init Loader";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(85, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Radius";
            // 
            // txtRadius
            // 
            this.txtRadius.Location = new System.Drawing.Point(177, 293);
            this.txtRadius.Name = "txtRadius";
            this.txtRadius.Size = new System.Drawing.Size(101, 20);
            this.txtRadius.TabIndex = 20;
            this.txtRadius.Text = "100";
            // 
            // txtMemberURI
            // 
            this.txtMemberURI.Location = new System.Drawing.Point(177, 78);
            this.txtMemberURI.Name = "txtMemberURI";
            this.txtMemberURI.Size = new System.Drawing.Size(405, 20);
            this.txtMemberURI.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(90, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "MemberSVC URI";
            // 
            // txtBrandID
            // 
            this.txtBrandID.Location = new System.Drawing.Point(184, 52);
            this.txtBrandID.Name = "txtBrandID";
            this.txtBrandID.Size = new System.Drawing.Size(102, 20);
            this.txtBrandID.TabIndex = 24;
            this.txtBrandID.Text = "1003";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(90, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Brand ID";
            // 
            // txtQuery
            // 
            this.txtQuery.Location = new System.Drawing.Point(177, 113);
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(435, 44);
            this.txtQuery.TabIndex = 26;
            this.txtQuery.Text = "";
            // 
            // btnParser
            // 
            this.btnParser.Location = new System.Drawing.Point(634, 293);
            this.btnParser.Name = "btnParser";
            this.btnParser.Size = new System.Drawing.Size(116, 31);
            this.btnParser.TabIndex = 27;
            this.btnParser.Text = "Parser Geo Search";
            this.btnParser.UseVisualStyleBackColor = true;
            this.btnParser.Click += new System.EventHandler(this.btnParser_Click);
            // 
            // btnSearchIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 691);
            this.Controls.Add(this.btnParser);
            this.Controls.Add(this.txtQuery);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBrandID);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtMemberURI);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtRadius);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnGeoSearch);
            this.Controls.Add(this.dgvMembers);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtIndexPath);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtLatitude);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLongitude);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtZipCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAgeMax);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAgeMin);
            this.Controls.Add(this.lblGender);
            this.Controls.Add(this.txtGenderMask);
            this.Controls.Add(this.txtCommunityID);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnIndexCommunity);
            this.Name = "btnSearchIndex";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMembers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIndexCommunity;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtCommunityID;
        private System.Windows.Forms.TextBox txtGenderMask;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAgeMin;
        private System.Windows.Forms.TextBox txtAgeMax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtZipCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLongitude;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLatitude;
        private System.Windows.Forms.TextBox txtIndexPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvMembers;
        private System.Windows.Forms.Button btnGeoSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRadius;
        private System.Windows.Forms.TextBox txtMemberURI;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBrandID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox txtQuery;
        private System.Windows.Forms.Button btnParser;
    }
}

