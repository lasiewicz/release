﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Spark.SearchEngine.ServiceAdapters;
using Spark.SearchEngine.ValueObjects;
using Matchnet.Search.ValueObjects;
using Spark.SearchEngine.BusinessLogic;
using Matchnet.Search.Interfaces;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet;
using Spark.Common.AccessService;
using Matchnet.Content.ValueObjects.Region;
using System.Collections;
using Spark.SearchEngine.ServiceAdaptersNRT;

namespace TestHarness
{
    public partial class NRTTest : Form
    {
        public NRTTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                txtNRTOutput.Text = "";

                //add member to E2 via NRT
                int memberID = Convert.ToInt32(txtNRTMemberID.Text);
                int communityID = Convert.ToInt32(txtNRTCommunityID.Text);
                int brandID = Convert.ToInt32(txtNRTBrandID.Text);

                SearchMemberUpdate searchMemberUpdate = new SearchMemberUpdate();
                searchMemberUpdate.MemberID = memberID;
                searchMemberUpdate.CommunityID = communityID;
                searchMemberUpdate.UpdateMode = UpdateModeEnum.add;
                searchMemberUpdate.UpdateReason = UpdateReasonEnum.adminUnsuspend;

                SearcherNRTSA.Instance.NRTUpdateMember(searchMemberUpdate, brandID);
                txtNRTOutput.Text = "Done: " + DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                txtNRTOutput.Text = ex.Message;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                txtNRTOutput.Text = "";

                //remove member from E2 via NRT
                int memberID = Convert.ToInt32(txtNRTMemberID.Text);
                int communityID = Convert.ToInt32(txtNRTCommunityID.Text);

                SearchMemberUpdate searchMemberUpdate = new SearchMemberUpdate();
                searchMemberUpdate.MemberID = memberID;
                searchMemberUpdate.CommunityID = communityID;
                searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
                searchMemberUpdate.UpdateReason = UpdateReasonEnum.adminSuspend;

                SearcherNRTSA.Instance.NRTRemoveMember(searchMemberUpdate);
                txtNRTOutput.Text = "Done: " + DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                txtNRTOutput.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                txtNRTOutput.Text = "";

                //update member to E2 via NRT
                int memberID = Convert.ToInt32(txtNRTMemberID.Text);
                int communityID = Convert.ToInt32(txtNRTCommunityID.Text);
                int brandID = Convert.ToInt32(txtNRTBrandID.Text);

                SearchMemberUpdate searchMemberUpdate = new SearchMemberUpdate();
                searchMemberUpdate.MemberID = memberID;
                searchMemberUpdate.CommunityID = communityID;
                searchMemberUpdate.UpdateMode = UpdateModeEnum.update;
                searchMemberUpdate.UpdateReason = UpdateReasonEnum.logon;

                SearcherNRTSA.Instance.NRTUpdateMember(searchMemberUpdate, brandID);
                txtNRTOutput.Text = "Done: " + DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                txtNRTOutput.Text = ex.Message;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int communityID = Convert.ToInt32(txtNRTCommunityID.Text);
            
            SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", communityID.ToString());
            searchPrefs.Add("searchtype", "1"); //web search
            searchPrefs.Add("searchtypeid", "1"); //zip code
            searchPrefs.Add("searchorderby", "2"); //last online (lastactivedate)
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6"); //6-women seeking male, 9-male seeking female
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817"); //beverly hills 3443817
            searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("regionIDCity", "3403764");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchredesign30", "1");
            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityID, 103);

            MatchnetQueryResults results = SearcherSA.Instance.RunQuery(q, communityID) as MatchnetQueryResults;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Done: " + DateTime.Now.ToString());
            sb.AppendLine("Results found: " + results.MatchesFound.ToString());
            sb.AppendLine("Results returned: " + results.MatchesReturned.ToString());
            sb.AppendLine("");
            for(int i=0; i < results.MatchesReturned; i++)
            {
                sb.Append(results.Items[i].MemberID.ToString() + ",");
            }
            txtNRTOutput.Text = sb.ToString();
        }

        public int GetSearchColorCode(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            int color = 0;
            if (member != null && brand != null)
            {
                string attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, "ColorCodePrimaryColor", "");
                if (!String.IsNullOrEmpty(attributeText))
                {
                    switch (attributeText.ToLower())
                    {
                        case "white":
                            color = 1;
                            break;
                        case "blue":
                            color = 2;
                            break;
                        case "yellow":
                            color = 4;
                            break;
                        case "red":
                            color = 8;
                            break;
                    }
                }
            }
            return color;
        }

        private static string getAreaCode(Int32 regionID)
        {
            string areaCode = string.Empty;

            RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, (Int32)Matchnet.Language.English);
            Int32 cityRegionID = regionLanguage.CityRegionID;

            RegionAreaCodeDictionary dictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();

            if (dictionary.Contains(cityRegionID))
            {
                ArrayList areaCodes = dictionary[cityRegionID];

                if (areaCodes.Count == 1)
                {
                    areaCode = areaCodes[0].ToString();
                }
                else
                {
                    //If more than one, use lowest area code in region since user can only have one
                    Int32 maxAreaCode = 0;

                    foreach (Int32 areaCodeNum in areaCodes)
                    {
                        if (areaCodeNum > maxAreaCode)
                        {
                            maxAreaCode = areaCodeNum;
                        }
                    }

                    areaCode = maxAreaCode.ToString();
                }
            }

            return areaCode;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int communityID = Convert.ToInt32(txtNRTCommunityID.Text);

            SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", communityID.ToString());
            searchPrefs.Add("searchtype", "1"); //web search
            searchPrefs.Add("searchtypeid", "1"); //zip code
            searchPrefs.Add("searchorderby", "2"); //last online (lastactivedate)
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6"); //6-women seeking male, 9-male seeking female
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817"); //beverly hills 3443817
            searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("regionIDCity", "3403764");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchredesign30", "1");
            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityID, 103);

            ArrayList results = SearcherSA.Instance.RunDetailedQuery(q, communityID);
            foreach (IMatchnetResultItem item in results)
            {

            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Done: " + DateTime.Now.ToString());
            sb.AppendLine("Results returned: " + results.Count.ToString());
            sb.AppendLine("");
            foreach (DetailedQueryResult item in results)
            {
                sb.Append(item.MemberID.ToString() + ",");
            }
            txtNRTOutput.Text = sb.ToString();
        }

    }
}
