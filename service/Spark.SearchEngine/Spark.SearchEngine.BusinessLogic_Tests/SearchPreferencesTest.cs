﻿using System;
using System.Linq;
using System.Diagnostics;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using NUnit.Framework;
using Spark.SearchEngine.BusinessLogic;
using Spark.SearchEngine.BusinessLogic.Searcher;

namespace Spark.SearchEngine.BusinessLogic_Tests
{
    [TestFixture]
    public class SearchPreferencesTest : AbstractSparkUnitTest
    {

        [TestFixtureSetUp]
        public void StartUp()
        {
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            TestQueryBuilder.SettingsService = _mockSettingsService;
            SearchersSetup();
            //search members for each community for testing accuracy
            CreateTestSearchMember();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {            
            SearchersTeardown();
            searchMembers.Clear();
            searchMembers = null;
            Utils.SettingsService = null;
            TestQueryBuilder.SettingsService = null;
        }

        [SetUp]
        public void setUp()
        {
        }

        [TearDown]
        public void tearDown()
        {
        }

        [Test]
        public void TestSparkIndexingSearchPrefs()
        {
            int communityId = 1;
            int siteId = 101;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL);

            //assert accuracy that result members will have preferred age range for 20 year old
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "55");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("preferredgendermask", "9");
            searchPrefs.Add("preferredage", "20");
            int[] expectedMemberIds = new int[] { 23247846, 988270506 };
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            //assert accuracy that result members will have preferred age range for 32 year old
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "55");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("preferredgendermask", "9");
            searchPrefs.Add("preferredage", "32");
            expectedMemberIds = new int[] { 988270506 };
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);
            
            //assert accuracy that result members will have preferred age range for 59 year old
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "55");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("preferredgendermask", "9");
            searchPrefs.Add("preferredage", "59");
            expectedMemberIds = new int[] { 988271920 };
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);
            
            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;
            //Regenerate SearchBL to use FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]        
        public void TestSparkIndexingSingleAgeRangeSearchPrefs()
        {
            int communityId = 1;
            int siteId = 101;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL);

            //assert accuracy that result members will have preferred age range for 20 year old
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "0"); 
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "37");
            searchPrefs.Add("maxage", "37");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("preferredgendermask", "9");
            searchPrefs.Add("preferredage", "20");
            int[] expectedMemberIds = new int[] { 23247846 };
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;
            //Regenerate SearchBL to use FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

//        SearchType: ReverseSearchWebSearch|
//        Param: PreferredAge, Val:36|
//        Param: SearchRedesign30, Val:1|
//        Param: HasPhotoFlag, Val:1|
//        Param: PreferredGenderMask, Val:6|
//        Param: PreferredAreaCodes, Val:212,212,212,212,646,646,646,646,917,917,917,917|
//        Param: AreaCode, Val:212,|
//        Param: Sorting, Val:1|
//        Param: DomainID, Val:1|
//        Param: MemberID, Val:100004579
        [Test]
        public void TestSparkReverseSearch()
        {
            int communityId = 1;
            int siteId = 101;
            int memberAge = 36;

            int[] prefAreaCodes = new int[] { 212, 917 };

            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("searchtype", "6");
            searchPrefs.Add("searchtypeid", "6");
//            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("preferredage", memberAge + "");
            searchPrefs.Add("preferredhasphotoflag", "1");
//            searchPrefs.Add("preferredareacodes","718");
            searchPrefs.Add("preferredgendermask", "6");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityId, siteId);

            stopWatch.Start();
            IMatchnetQueryResults results = sparkSearcherBL.RunDetailedQuery(q, communityId);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    int minage = Convert.ToInt32(detailedQueryResult.Tags["preferredminage"]);
                    int maxage = Convert.ToInt32(detailedQueryResult.Tags["preferredmaxage"]);
                    int gendermask = Convert.ToInt32(detailedQueryResult.Tags["preferredgendermask"]);
                    int hasphotoflag = Convert.ToInt32(detailedQueryResult.Tags["preferredhasphotoflag"]);
                    string synagogueAttendance = detailedQueryResult.Tags["preferredsynagogueattendance"] as string;
                    string jdateReligion = detailedQueryResult.Tags["preferredjdatereligion"] as string;
                    string smokingHabits = detailedQueryResult.Tags["preferredsmokinghabit"] as string;
                    string drinkingHabits = detailedQueryResult.Tags["preferreddrinkinghabit"] as string;
                    string keepkosher = detailedQueryResult.Tags["preferredkosherstatus"] as string;
                    string educationLevel = detailedQueryResult.Tags["preferrededucationlevel"] as string;
                    string activityLevel = detailedQueryResult.Tags["preferredactivitylevel"] as string;
                    string moreChildrenFlag = detailedQueryResult.Tags["preferredmorechildrenflag"] as string;
                    string ethnicity = detailedQueryResult.Tags["preferredethnicity"] as string;
                    string languageMask = detailedQueryResult.Tags["preferredlanguagemask"] as string;
                    int minheight = Convert.ToInt32(detailedQueryResult.Tags["preferredminheight"]);
                    int maxheight = Convert.ToInt32(detailedQueryResult.Tags["preferredmaxheight"]);
                    string relocationFlag = detailedQueryResult.Tags["preferredrelocation"] as string;
                    string preferredAreaCodes = detailedQueryResult.Tags["preferredareacodes"] as string;

                    print(string.Format("MemberId:{0}, Preferred Min/Max Ages: {1}/{2}, GenderMask:{3}, HasPhoto:{4}, Distance:{5}mi, SyagogueAttendance:{6}, JdateReligion:{7}, SmokingHabits:{8}, DrinkingHabits:{9}, KeepKosher:{10}, EducationLevel:{11}, ActivityLevel:{12}, MoreChildrenFlag:{13}, Ethnicity:{14}, LanguageMask:{15}, Preferred Min/Max Height: {16}/{17}, RelocationFlag:{18}, Pref Area Codes:{19}\n", detailedQueryResult.MemberID, minage, maxage, gendermask, hasphotoflag, detailedQueryResult.Distance, synagogueAttendance, jdateReligion, smokingHabits, drinkingHabits, keepkosher, educationLevel, activityLevel, moreChildrenFlag, ethnicity, languageMask, minheight, maxheight, relocationFlag, preferredAreaCodes));
                    Assert.LessOrEqual(minage, memberAge);
                    Assert.GreaterOrEqual(maxage, memberAge);
                    Assert.AreEqual(6, gendermask);
                    Assert.AreEqual(1, hasphotoflag);
                }
            }
            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;
        }


        [Test]        
        public void TestNumericBetweenSearch()
        {
            IndexWriter indexA = new IndexWriter(new RAMDirectory(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);

            AddDocumentA(indexA, 1, 50, 1, 20, 30);
            AddDocumentA(indexA, 2, 55, 1, 20, 45);
            AddDocumentA(indexA, 3, 25, 1, 43, 60);
            AddDocumentA(indexA, 4, 44, 1, 55, 65);

            IndexSearcher indexSearcherA = new IndexSearcher(indexA.GetReader());
            NumericRangeFilter<int> preferredMinAgeFilter = NumericRangeFilter.NewIntRange("preferredminage", 18, 45, true, false);
            NumericRangeFilter<int> preferredMaxAgeFilter = NumericRangeFilter.NewIntRange("preferredmaxage", 44, 90, true, false);
            BooleanFilter boolFilter = new BooleanFilter();
            boolFilter.Add(new FilterClause(preferredMaxAgeFilter, Occur.MUST));
            boolFilter.Add(new FilterClause(preferredMinAgeFilter, Occur.MUST));
            Filter searchFilter = new CachingWrapperFilter(boolFilter);

            BooleanQuery booleanQuery = new BooleanQuery();
            booleanQuery.Add(new TermQuery(new Term("hasphotoflag", Lucene.Net.Util.NumericUtils.IntToPrefixCoded(1))), Occur.SHOULD);
            
            TopDocs topDocs = indexSearcherA.Search(booleanQuery, searchFilter, 10);

            Assert.Greater(topDocs.TotalHits, 0);

            for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
            {
                Document doc = indexSearcherA.Doc(topDocs.ScoreDocs[i].Doc);
                print(string.Format("Doc {0}:", i));
                foreach (Field f in doc.fields_ForNUnit)
                {
                    print(string.Format("\tField \"{0}\", Value \"{1}\"", f.Name, f.StringValue));
                }
            }
        }

        [Test]
        public void TestJDateAgeFilter()
        {
            int communityId = 3;
            int siteId = 103;
            int memberAge = 21;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("searchtype", "3");
            searchPrefs.Add("searchtypeid", "3");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("preferredage", memberAge+"");
            searchPrefs.Add("preferredage", memberAge+"");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("regioncityid", "3404282");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("regionid", "3404282");
            searchPrefs.Add("radius", "1000");
            searchPrefs.Add("domainid", communityId+"");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("memberid", "100068787");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityId, siteId);

            stopWatch.Start();            
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, communityId);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    int minage = Convert.ToInt32(detailedQueryResult.Tags["preferredminage"]);
                    int maxage = Convert.ToInt32(detailedQueryResult.Tags["preferredmaxage"]);
                    print(string.Format("MemberId:{0}, Preferred Min/Max Ages: {1}/{2}\n", detailedQueryResult.MemberID, minage,maxage));
                    Assert.LessOrEqual(minage, memberAge);
                    Assert.GreaterOrEqual(maxage, memberAge);
                }
            }
            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

        [Test]
        public void TestJDateReverseSearch()
        {
//Param: PreferredAge, Val:37|Param: RegionIDCountry, Val:223|Param: SearchRedesign30, Val:1|Param: HasPhotoFlag, Val:1|Param: PreferredDistance, Val:160
//|Param: PreferredGenderMask, Val:9|Param: RegionIDCity, Val:3424292|Param: Sorting, Val:2|Param: Longitude, Val:-1.291251
            //|Param: DomainID, Val:3|Param: Latitude, Val:0.711132|Param: PreferredRegionId, Val:3466365|Param: MemberID, Val:114402580


            int communityId = 3;
            int siteId = 103;
            int memberAge = 37;
            int memberHeight = 152;
            int prefDistance = 1600;
            int[] prefSynagogueAttendances = new int[] { 1,2,4,8,16,32 };
            int[] prefJDateReligions = new int[] { 1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536 };
            int[] prefSmokingHabits = new int[] { 2, 4, 8, 16 };
            int[] prefDrinkingHabits = new int[] { 2, 4, 8, 16 };
            int[] prefKeepKosher = new int[] { 1, 2, 4, 8, 16 };
//            int[] prefEducationLevel = new int[] {2,4,8,16,32,64,128,256,512,1024,2048,4096 };
//            int[] prefActivityLevel = new int[] { 2, 4, 8, 16, 32 };
//            int[] prefMoreChildrenFlag = new int[] { 1, 2, 4 };
//            int[] prefMoreChildrenFlag = new int[] { 1 };
            int[] prefEthnicity = new int[] { 2,4,8,16,32,64,128,512,1024,2048,4096,8192};
            int[] prefLanguageMask = new int[]{1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216,33554432,67108864,134217728,268435456,536870912,1073741824};
//            int[] prefRelocateFlag = new int[] { 1,2,4 };

            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("searchtype", "6");
            searchPrefs.Add("searchtypeid", "6");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("searchorderby", "2");
//            searchPrefs.Add("preferredage", memberAge + "");
//            searchPrefs.Add("preferredhasphotoflag", "1");
            searchPrefs.Add("preferredregionid", "3466365");
            searchPrefs.Add("preferreddistance", prefDistance+"");
            searchPrefs.Add("preferredgendermask", "9");
//            searchPrefs.Add("preferredsynagogueattendance", prefSynagogueAttendances.Sum()+"");
//            searchPrefs.Add("preferredjdatereligion", prefJDateReligions.Sum()+""); 
//            searchPrefs.Add("preferredsmokinghabit", prefSmokingHabits.Sum()+"");
//            searchPrefs.Add("preferreddrinkinghabit", prefDrinkingHabits.Sum()+"");
//            searchPrefs.Add("preferredkosherstatus", prefKeepKosher.Sum()+ "");
//            searchPrefs.Add("preferrededucationlevel", prefEducationLevel.Sum()+"");
//            searchPrefs.Add("preferredactivitylevel", prefActivityLevel.Sum()+"");
//            searchPrefs.Add("preferredmorechildrenflag", prefMoreChildrenFlag.Sum()+"");
//            searchPrefs.Add("preferredlanguagemask", prefLanguageMask.Sum()+"");
//            searchPrefs.Add("preferredheight", memberHeight+"");
//            searchPrefs.Add("preferredethnicity", prefEthnicity.Sum()+"");            
//            searchPrefs.Add("preferredrelocation", prefRelocateFlag.Sum() + "");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("regioncityid", "3424292");
            searchPrefs.Add("memberid", "114402580");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityId, siteId);

            stopWatch.Start();
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, communityId);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    int minage = Convert.ToInt32(detailedQueryResult.Tags["preferredminage"]);
                    int maxage = Convert.ToInt32(detailedQueryResult.Tags["preferredmaxage"]);
                    int gendermask = Convert.ToInt32(detailedQueryResult.Tags["preferredgendermask"]);
                    int hasphotoflag = Convert.ToInt32(detailedQueryResult.Tags["preferredhasphotoflag"]);
                    string synagogueAttendance = detailedQueryResult.Tags["preferredsynagogueattendance"] as string;
                    string jdateReligion = detailedQueryResult.Tags["preferredjdatereligion"] as string;
                    string smokingHabits = detailedQueryResult.Tags["preferredsmokinghabit"] as string;
                    string drinkingHabits = detailedQueryResult.Tags["preferreddrinkinghabit"] as string;
                    string keepkosher = detailedQueryResult.Tags["preferredkosherstatus"] as string;
                    string educationLevel = detailedQueryResult.Tags["preferrededucationlevel"] as string;
                    string activityLevel = detailedQueryResult.Tags["preferredactivitylevel"] as string;
                    string moreChildrenFlag = detailedQueryResult.Tags["preferredmorechildrenflag"] as string;
                    string ethnicity = detailedQueryResult.Tags["preferredethnicity"] as string;
                    string languageMask = detailedQueryResult.Tags["preferredlanguagemask"] as string;
                    int minheight = Convert.ToInt32(detailedQueryResult.Tags["preferredminheight"]);
                    int maxheight = Convert.ToInt32(detailedQueryResult.Tags["preferredmaxheight"]);
                    string relocationFlag = detailedQueryResult.Tags["preferredrelocation"] as string;

                    print(string.Format("MemberId:{0}, Preferred Min/Max Ages: {1}/{2}, GenderMask:{3}, HasPhoto:{4}, Distance:{5}mi, SyagogueAttendance:{6}, JdateReligion:{7}, SmokingHabits:{8}, DrinkingHabits:{9}, KeepKosher:{10}, EducationLevel:{11}, ActivityLevel:{12}, MoreChildrenFlag:{13}, Ethnicity:{14}, LanguageMask:{15}, Preferred Min/Max Height: {16}/{17}, RelocationFlag:{18}\n", detailedQueryResult.MemberID, minage, maxage, gendermask, hasphotoflag, detailedQueryResult.Distance, synagogueAttendance, jdateReligion, smokingHabits, drinkingHabits, keepkosher, educationLevel, activityLevel, moreChildrenFlag, ethnicity, languageMask, minheight, maxheight, relocationFlag));
//                    Assert.LessOrEqual(minage, memberAge);
//                    Assert.GreaterOrEqual(maxage, memberAge);
//                    Assert.LessOrEqual(minheight, memberHeight);
//                    Assert.GreaterOrEqual(maxheight, memberHeight);
                      Assert.AreEqual(9, gendermask);
//                    Assert.AreEqual(1, hasphotoflag);
//                    Assert.Less(detailedQueryResult.Distance, Utils.GetRadiusOffsetBuffer(prefDistance));
//                    TestMaskValues(synagogueAttendance, prefSynagogueAttendances);
//                    TestMaskValues(jdateReligion, prefJDateReligions);
//                    TestMaskValues(smokingHabits, prefSmokingHabits);
//                    TestMaskValues(drinkingHabits, prefDrinkingHabits);
//                    TestMaskValues(keepkosher, prefKeepKosher);
//                    TestMaskValues(educationLevel, prefEducationLevel);
//                    TestMaskValues(activityLevel, prefActivityLevel);
//                    TestMaskValues(moreChildrenFlag, prefMoreChildrenFlag);
//                    TestMaskValues(ethnicity, prefEthnicity);
//                    TestMaskValues(languageMask, prefLanguageMask);
//                    TestMaskValues(relocationFlag, prefRelocateFlag);
                }
            }
            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

        protected void TestMaskValues(string actualMaskValueString, int[] expectedMaskValues)
        {
            bool containsMaskValues = false;
            foreach (int intVal in expectedMaskValues)
            {
                if (!containsMaskValues)
                {
                    containsMaskValues = actualMaskValueString.Contains(intVal + "");
                }
                else
                {
                    break;
                }
            }
            Assert.IsTrue(containsMaskValues,string.Format("No matches between {{{0}}} and {{{1}}}", actualMaskValueString,string.Join(",", Array.ConvertAll(expectedMaskValues, x => x.ToString()))));
        }

        //[Test]
        //[Ignore("Skipping ParallelMultiSearcher test.")]
        //public void TestParallelSearcher()
        //{
        //    IndexWriter indexA = new IndexWriter(new RAMDirectory(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);
        //    IndexWriter indexB = new IndexWriter(new RAMDirectory(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);

        //    AddDocumentA(indexA, 1, 50, 0, 20, 30);
        //    AddDocumentA(indexA, 2, 55, 1, 20, 45);
        //    AddDocumentA(indexA, 3, 25, 0, 43, 60);
        //    AddDocumentA(indexA, 4, 40, 1, 55, 65);

        //    AddDocumentB(indexB, 1, "Steve", "Forbes", "this is steve's essay.");
        //    AddDocumentB(indexB, 2, "Steve", "Jobs", "not a big deal.");
        //    AddDocumentB(indexB, 3, "Harriet", "Smith", "the lazy dog jumps over the fox.");
        //    AddDocumentB(indexB, 4, "Larry", "Sanders", "hey now");

        //    IndexSearcher indexSearcherA = new IndexSearcher(indexA.GetReader());
        //    IndexSearcher indexSearcherB = new IndexSearcher(indexB.GetReader());
        //    Searchable[] searchables = new Searchable[]
        //                                   {
        //                                       indexSearcherA, indexSearcherB
        //                                   };
        //    ParallelMultiSearcher parallelMultiSearcher = new ParallelMultiSearcher(searchables);

        //    BooleanQuery booleanQuery = new BooleanQuery();
        //    booleanQuery.Add(new TermQuery(new Term("firstname", "Steve")),BooleanClause.Occur.SHOULD);
        //    booleanQuery.Add(new TermQuery(new Term("hasphotoflag", Lucene.Net.Util.NumericUtils.IntToPrefixCoded(1))), BooleanClause.Occur.SHOULD);
            

        //    TopDocs topDocs = parallelMultiSearcher.Search(booleanQuery, 10);

        //    Assert.Greater(topDocs.totalHits,0);

        //    for (int i = 0; i < topDocs.scoreDocs.Length; i++)
        //    {
        //        Document doc = parallelMultiSearcher.Doc(topDocs.scoreDocs[i].doc);
        //        print(string.Format("Doc {0}:", i));
        //        foreach (Field f in doc.fields_ForNUnit)
        //        {
        //            print(string.Format("\tField \"{0}\", Value \"{1}\"", f.Name(), f.StringValue()));
        //        }
        //    }
        //}

        private void AddDocumentA(IndexWriter indexA, int memberId, int age, int hasPhoto, int preferredMinAge, int preferredMaxAge)
        {
            Document documentA = new Document();
            documentA.Add(new Field("id", memberId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            documentA.Add(new NumericField("memberid", Field.Store.YES, true).SetIntValue(memberId));
            documentA.Add(new NumericField("hasphotoflag", Field.Store.YES, true).SetIntValue(hasPhoto));
            DateTime bday = DateTime.Now.AddYears(-age);
            documentA.Add(new NumericField("birthdate", Field.Store.YES, true).SetLongValue(bday.Ticks));            
            documentA.Add(new NumericField("preferredminage", Field.Store.YES, true).SetIntValue(preferredMinAge));
            documentA.Add(new NumericField("preferredmaxage", Field.Store.YES, true).SetIntValue(preferredMaxAge));
            indexA.UpdateDocument(new Term("id", memberId.ToString()), documentA);
        }

        private void AddDocumentB(IndexWriter indexB, int memberId, string firstName, string lastName, string essay)
        {
            Document documentB = new Document();
            documentB.Add(new Field("id", memberId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            documentB.Add(new NumericField("memberid", Field.Store.YES, true).SetIntValue(memberId));
            documentB.Add(new Field("firstname", firstName, Field.Store.YES, Field.Index.NOT_ANALYZED));
            documentB.Add(new Field("lastname", lastName, Field.Store.YES, Field.Index.NOT_ANALYZED));
            documentB.Add(new Field("essay", essay, Field.Store.YES, Field.Index.NOT_ANALYZED));
            indexB.UpdateDocument(new Term("id", memberId.ToString()), documentB);
        }

    }
}
