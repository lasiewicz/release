﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchEngine.BusinessLogic_Tests
{
    public interface IStressTestable
    {
        bool RunSuceeded { get; }
        long ElapsedMillis { get; }

        

        bool IsDone { get; }

        //this method makes sure everything is ready to go 
        //before the stress testing startss
        void Prepare();
        //this method actually does the stress testing
        void Run();
        
    }
}
