﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Matchnet.Exceptions;
using NUnit.Framework;
using Matchnet.Search.Interfaces;
using Spark.SearchEngine.BusinessLogic;
using Spark.SearchEngine.BusinessLogic.Searcher;
using Spark.SearchEngine.ValueObjects;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;

namespace Spark.SearchEngine.BusinessLogic_Tests
{
    [TestFixture]
    public class SearchEngineBLTests : AbstractSparkUnitTest
    {
        protected const string JDATE_COMMUNITY = "jdate";
        protected const string SPARK_COMMUNITY = "spark";

        //Note: These are paths to another copy of the index and should differ from the ones specified in SearchersSetup()
        //These are used to test switching indexes
        protected const string newJdateIndexPath = "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\Jdate2";
        protected const string newSparkIndexPath = "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\Spark2";

        [TestFixtureSetUp]
        public void StartUp()
        {
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            TestQueryBuilder.SettingsService = _mockSettingsService;
            MatchRatingUtil.SettingsService = _mockSettingsService;
            SearchersSetup();
            CreateTestSearchMember();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            SearchersTeardown();
            searchMembers.Clear(); 
            searchMembers = null;
            Utils.SettingsService = null;
            TestQueryBuilder.SettingsService = null;
            MatchRatingUtil.SettingsService = null;
        }

        [SetUp]
        public void setUp()
        {
        }

        [TearDown]
        public void tearDown()
        {
        }

        #region Test Methods
        [Test]
        public void LatLongRadiansConversionTest()
        {
            //beverly hills (zip=90211)
            double[] radians = new double[] { 0.594557, -2.066127 };
            double[] expectedDegrees = new double[] { 34.0656067799597, -118.380357038026 };
            TestRadianConversion(radians, expectedDegrees);

            //beverly hills 2 miles away (zip=90212)
            radians = new double[] { 0.594481, -2.066492 };
            expectedDegrees = new double[] { 34.0612523007167, -118.401269997549 };
            TestRadianConversion(radians, expectedDegrees);

            //pasadena 20 miles away (zip=91101)
            radians = new double[] { 0.595984, -2.061917 };
            expectedDegrees = new double[] { 34.1473678573249, -118.139141806276 };
            TestRadianConversion(radians, expectedDegrees);

            //new york 3000 miles away (zip=10017)
            radians = new double[] { 0.711260, -1.291094 };
            expectedDegrees = new double[] { 40.7521961364749, -73.9742371546635 };
            TestRadianConversion(radians, expectedDegrees);

            radians = new double[] { 0.569571, -2.042141 };
            expectedDegrees = new double[] { 32.6340144330458, -117.006060470625 };
            TestRadianConversion(radians, expectedDegrees);
            	
        }

        [Test]
        public void SparkSpatialSearchTest()
        {
            int communityId = 1;
            int siteId = 101;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;
            List<SearchMember> sparkSearchMembers = new List<SearchMember>();
            //beverly hills (zip=90211)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939806, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //hollywood 5 miles away (zip=90028)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103820222, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 8, 8, 2, 256, 2, 2, 168, 2, -21474, 0, 0, 0, 0, 4, 0, 64, -1, -1, 223, 1538, 3404282, 9805694, -2.065197, 0.595144, -1, string.Empty, 0, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //pasadena 20 miles away (zip=91101)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(23247846, 1, 6, DateTime.Parse("1974-12-10"), DateTime.Parse("2003-06-12"), DateTime.Now, 0, 128, 131072, 2, 2, 4, 4, -21474, 8, 0, 0, 0, 0, 0, 128, 0, 8, 65536, 0, 223, 1538, 3404721, 3445194, -2.061917, 0.595984, -1, string.Empty, 0, DateTime.Parse("2006-02-07"), -1, -1, -1, DateTime.Parse("2008-06-26"), -1, -1, -1, -1, 1, 0));
            //new york 3000 miles away (zip=10017)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103820223, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 8, 8, 2, 256, 2, 2, 168, 2, -21474, 0, 0, 0, 0, 4, 0, 64, -1, -1, 223, 1566, 3424292, 3466331, -1.291094, 0.711260, -1, string.Empty, 0, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL, sparkSearchMembers);
            //search
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("radius", "1");
            //searchPrefs.Add("regionid", "3443818");
            searchPrefs.Add("regionid", "3443821");

            int[] expectedMemberIds = new int[] { 103939806 };
            //assert accuracy
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            //search
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("radius", "5");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");

            expectedMemberIds = new int[] { 103939806, 103820222 };

            //search
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            //error boundary:  97 - 192
            searchPrefs.Add("radius", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");

            expectedMemberIds = new int[] { 103939806, 103820222, 23247846 };
            //assert accuracy
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            //search
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("radius", "3500");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");

            expectedMemberIds = new int[] { 103939806, 103820222, 23247846, 103820223 };
            //assert accuracy
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);
            
            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;

            //Regenerate SearchBL to use FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void SparkBlockedMembersSearchTest()
        {
            int communityId = 1;
            int siteId = 101;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL);
            //search
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("radius", "3000");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("blockedmemberids", "104624112, 23247846, 988270506");
            
            int[] expectedMemberIds = new int[] { 103939806, 103820222, 988271920 };
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            searchPrefs.Add("blockedmemberids", "104624112, 103939806, 103820222, 988271920");
            expectedMemberIds = new int[] { 23247846, 988270506};
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            searchPrefs.Add("blockedmemberids", " 23247846, 988270506, 103939806, 103820222, 988271920 ");
            expectedMemberIds = new int[] { 104624112 };
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            sparkSearcherBL.NUNIT_Print = false;

            //Regenerate SearchBL to use FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void SparkNRTRemoveMember_AdminSuspend()
        {
            int communityId = 1;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            if (!sparkSearcherBL.NUNIT_UseNRT)
            {
                sparkSearcherBL.NUNIT_UseNRT = true;
                sparkSearcherBL.GetSearcher(communityId).NUNIT_UseNRT = true;
                sparkSearcherBL.GetSearcher(communityId).UpdateNRT();
            }
            sparkSearcherBL.NUNIT_Print = true;

            //set test members
            List<SearchMember> sparkSearchMembers = new List<SearchMember>();
            //beverly hills (zip=90211)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939806, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939807, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //hollywood 5 miles away (zip=90028)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103820222, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 8, 8, 2, 256, 2, 2, 168, 2, -21474, 0, 0, 0, 0, 4, 0, 64, -1, -1, 223, 1538, 3404282, 9805694, -2.065197, 0.595144, -1, string.Empty, 0, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //pasadena 20 miles away (zip=91101)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(23247846, 1, 6, DateTime.Parse("1974-12-10"), DateTime.Parse("2003-06-12"), DateTime.Now, 0, 128, 131072, 2, 2, 4, 4, -21474, 8, 0, 0, 0, 0, 0, 128, 0, 8, 65536, 0, 223, 1538, 3404721, 3445194, -2.061917, 0.595984, -1, string.Empty, 0, DateTime.Parse("2006-02-07"), -1, -1, -1, DateTime.Parse("2008-06-26"), -1, -1, -1, -1, 1, 0));

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL, sparkSearchMembers);
            
            //basic "any" prefs
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", communityId.ToString());
            searchPrefs.Add("searchtype", "1"); //web search
            searchPrefs.Add("searchtypeid", "1"); //zip code
            searchPrefs.Add("searchorderby", "2"); //last online (lastactivedate)
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6"); //6-women seeking male, 9-male seeking female
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817"); //beverly hills 3443817
            searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("regionIDCity", "3403764");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchredesign30", "1");

            //get initial results count
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(sparkSearchMembers.Count, initialResultsCount);

            //remove member NRT
            int memberID = 103820222;
            SearchMemberUpdate searchMemberUpdate = new SearchMemberUpdate();
            searchMemberUpdate.CommunityID = communityId;
            searchMemberUpdate.MemberID = memberID;
            searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
            searchMemberUpdate.UpdateReason = UpdateReasonEnum.adminSuspend;

            sparkSearcherBL.NRTRemoveMember(searchMemberUpdate);

            //refresh NRT Reader
            sparkSearcherBL.GetSearcher(communityId).UpdateNRT();

            //check results count
            IMatchnetQueryResults finalResults = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            int finalResultsCount = finalResults.MatchesFound;
            Trace.WriteLine("SparkNRTRemoveMember Test - MemberID:" + memberID.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(initialResultsCount - 1, finalResultsCount);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void SparkNRTUpdateMember_LoginDate()
        {
            int communityId = 1;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            if (!sparkSearcherBL.NUNIT_UseNRT)
            {
                sparkSearcherBL.NUNIT_UseNRT = true;
                sparkSearcherBL.GetSearcher(communityId).NUNIT_UseNRT = true;
                sparkSearcherBL.GetSearcher(communityId).UpdateNRT();
            }
            sparkSearcherBL.NUNIT_Print = true;

            //set test members
            List<SearchMember> sparkSearchMembers = new List<SearchMember>();
            //beverly hills (zip=90211)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939806, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939807, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-04-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //hollywood 5 miles away (zip=90028)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103820222, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-05-28"), 0, 8, 8, 2, 256, 2, 2, 168, 2, -21474, 0, 0, 0, 0, 4, 0, 64, -1, -1, 223, 1538, 3404282, 9805694, -2.065197, 0.595144, -1, string.Empty, 0, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //pasadena 20 miles away (zip=91101)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(23247846, 1, 6, DateTime.Parse("1974-12-10"), DateTime.Parse("2003-06-12"), DateTime.Parse("2011-06-28"), 0, 128, 131072, 2, 2, 4, 4, -21474, 8, 0, 0, 0, 0, 0, 128, 0, 8, 65536, 0, 223, 1538, 3404721, 3445194, -2.061917, 0.595984, -1, string.Empty, 0, DateTime.Parse("2006-02-07"), -1, -1, -1, DateTime.Parse("2008-06-26"), -1, -1, -1, -1, 1, 0));

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL, sparkSearchMembers);

            //basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", communityId.ToString());
            searchPrefs.Add("searchtype", "1"); //web search
            searchPrefs.Add("searchtypeid", "1"); //zip code
            searchPrefs.Add("searchorderby", "2"); //last online (lastactivedate)
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6"); //6-women seeking male, 9-male seeking female
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817"); //beverly hills 3443817
            searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("regionIDCity", "3403764");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchredesign30", "1");

            //get initial results count
            IMatchnetQueryResults results = sparkSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            int initialResultsCount = results.MatchesFound;
            int initialMostRecentActiveMember = results.Items[0].MemberID;
            Assert.AreEqual(sparkSearchMembers.Count, initialResultsCount);

            //update member NRT
            int memberID = 103939807;
            SearchMember searchMember = SearchMember.GenerateSearchMember(memberID, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Now, 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            searchMember.UpdateReason = UpdateReasonEnum.logon;
            searchMember.UpdateMode = UpdateModeEnum.update;

            sparkSearcherBL.NRTUpdateMember(searchMember);

            //refresh NRT Reader
            sparkSearcherBL.GetSearcher(communityId).UpdateNRT();

            //check results count and most recent active member
            IMatchnetQueryResults finalResults = sparkSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            int finalResultsCount = finalResults.MatchesFound;
            int finalMostRecentActiveMember = finalResults.Items[0].MemberID;

            Trace.WriteLine("SparkNRTUpdateMember Test - InitialMostRecentMemberID:" + initialMostRecentActiveMember.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", MemberIDToUpdate: " + memberID.ToString() + ", FinalMostRecentMemberID: " + finalMostRecentActiveMember.ToString() + ",  FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(initialResultsCount, finalResultsCount);
            Assert.AreEqual(memberID, finalMostRecentActiveMember);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void SearchSparkAnyPrefsTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("minheight", "-2147483647");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("maxheight", "-2147483647");
            searchPrefs.Add("languagemask", "2");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "6");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "55");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            IMatchnetQueryResults results = GetSearcherBL(1).RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            for (int i = 0; i < results.Items.Count; i++)
            {
                IMatchnetResultItem item = results.Items[i];
                Console.WriteLine(string.Format("MemberId: {0}", item.MemberID));
            }
        }

        [Test]
        public void SearchSparkAnyPrefsSortTest()
        {
            //sort by join date
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("languagemask", "2");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("radius", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("regionid", "3443821");
            //searchPrefs.Add("longitude", "-2.066127");
            //searchPrefs.Add("latitude", "0.594557");
            //searchPrefs.Add("regionid", "3403764");
            //searchPrefs.Add("longitude", "-2.066687");
            //searchPrefs.Add("latitude", "0.595098");

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            sparkSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 1200);
            printResults(searchPrefs, results, millis, sparkSearcherBL);

            //sort by popularity
            searchPrefs.Add("searchorderby", "4");
            stopWatch.Reset();
            stopWatch.Start();
            results = null;
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long newMillis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            Assert.Less(newMillis, 1200);
            printResults(searchPrefs, results, newMillis, sparkSearcherBL);

            //sort by last login date
            searchPrefs.Add("searchorderby", "2");
            stopWatch.Reset();
            stopWatch.Start();
            results = null;
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            newMillis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            Assert.Less(newMillis, 1200);
            printResults(searchPrefs, results, newMillis, sparkSearcherBL);

            //sort by proximity
            searchPrefs.Add("searchorderby", "3");
            stopWatch.Reset();
            stopWatch.Start();
            results = null;
            results = sparkSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            newMillis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            Assert.Less(newMillis, 1200);
            printResults(searchPrefs, results, newMillis, sparkSearcherBL);
            sparkSearcherBL.NUNIT_Print = false;
        }


        //ClassName:SearcherBL, Info, RunQuery Data Time: 58 ms, Results: 13, Criteria: SearchType: YourMatchesWebSearch
        //|Param: SearchRedesign30, Val:1|
        //Param: JDateReligion, Val:2047|
        //Param: JDateReligion, Val:2047|
        //Param: RegionIDCountry, Val:223|
        //Param: HasPhotoFlag, Val:1|
        //Param: RegionID, Val:3437447|
        //Param: GeoDistance, Val:5|
        //Param: RegionIDCity, Val:3437447|
        //Param: Sorting, Val:2|
        //Param: Longitude, Val:-1.952901|
        //Param: DomainID, Val:3|
        //Param: Latitude, Val:0.710928|
        //Param: AgeMin, Val:31|
        //Param: AgeMax, Val:45|
        //Param: MemberID, Val:115584714|
        //Param: GenderMask, Val:9
        [Test]
        public void SearchJDateAnyPrefsLastLogonSortTest()
        {
            //sort by join date
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");            
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "31");
            searchPrefs.Add("maxage", "45");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("regionid", "3437447");
            searchPrefs.Add("searchredesign30","1");
            searchPrefs.Add("JDateReligion","2047");
            searchPrefs.Add("geodistance","5");
            searchPrefs.Add("searchorderby", "2");

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            //sort by last login date
            stopWatch.Stop();
            long newMillis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(newMillis, 2200);
            printResults(searchPrefs, results, newMillis, jdateSearcherBL);

            jdateSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void SearchASparkAllPrefsTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();

            //searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("minheight", "140");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("maxheight", "244");
            searchPrefs.Add("languagemask", "2147483647");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "5");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("ethnicity", "1790");
            searchPrefs.Add("maritalstatus", "56");
            searchPrefs.Add("educationlevel", "510");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("smokinghabits", "30");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("bodytype", "2097149");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("religion", "8388606");
            searchPrefs.Add("maxage", "55");
            searchPrefs.Add("drinkinghabits", "30");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();

            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void SearchSparkTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("distance", "1");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "2");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", "22");
            searchPrefs.Add("maxage", "90");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("memberid", "114402580");

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));            
        }

        [Test]
        public void SearchBBWMatchMailTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "2");
            searchPrefs.Add("geodistance", "5");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "23");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL bbwSearchBL = GetSearcherBL(23);
            IMatchnetQueryResults results = bbwSearchBL.RunQuery(TestQueryBuilder.build(searchPrefs, 23, 0), 23);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 4000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void SearchJDateTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("countryregionid", "101");
            //searchPrefs.Add("hasphotoflag", "0");
            //searchPrefs.Add("schoolid", "");
            //searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            //searchPrefs.Add("regionid", "8630531");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("languagemask", "2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();

            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2500);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void SearchJDateWithRegistrationDateRangeTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("languagemask", "2");
            searchPrefs.Add("radius", "160");
            searchPrefs.Add("registrationdatemin", "01/01/2000");
            searchPrefs.Add("registrationdatemax", "01/01/2012");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();

            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2500);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void SearchJDateFranceWithCommasTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "76");
            searchPrefs.Add("maritalstatus", "2");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "9795409");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("regionidcity", "9795409");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("longitude", "0,039852");
            searchPrefs.Add("latitude", "0,852302");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 105), 3);
            stopWatch.Stop();

            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5500);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void SearchJDateFranceTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "76");
            searchPrefs.Add("maritalstatus", "2");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "9795409");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("regionidcity", "9795409");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("longitude", "0.039852");
            searchPrefs.Add("latitude", "0.852302");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 105), 3);
            stopWatch.Stop();

            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5500);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void SearchJDateILAreaCodeTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "105");
            searchPrefs.Add("regionid", "9800654");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("areacodes", ",4,,5,");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "95");
            searchPrefs.Add("gendermask", "9");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 105), 3);
            stopWatch.Stop();

            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 3500);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            jdateSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void SearchJDateChangePathTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("languagemask", "2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 3000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}\n\n\n", millis, results.Items.Count, searchPrefs.ToString()));

            Searcher searcher = jdateSearcherBL.GetSearcher(3);
            string originalIndexPath = searcher.IndexPath;
            searcher.NUNIT_IndexPath = newJdateIndexPath;
            searcher.Update();

            stopWatch.Start();
            IMatchnetQueryResults results2 = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();


            millis = stopWatch.ElapsedMilliseconds;
            Assert.AreEqual(searcher.NUNIT_IndexPath, searcher.IndexPath);
            Assert.NotNull(results2);
            Assert.GreaterOrEqual(results2.Items.Count, 1);
            //Assert.Less(millis, 13000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results2.Items.Count, searchPrefs.ToString()));
            testSearchPrefs.Add("domainid", 3 + "");
            jdateSearcherBL.RunQuery(TestQueryBuilder.build(testSearchPrefs, 3, 0), 3);
            testSearchPrefs.Add("domainid", "");

            //wait, ensure close old searcher finishes
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }

            //reset back to original index path
            searcher.NUNIT_IndexPath = originalIndexPath;
            searcher.Update();
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }

        }

        [Test]
        public void SearchSparkChangePathTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("colorcode", "");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("languagemask", "2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 3000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}\n\n\n", millis, results.Items.Count, searchPrefs.ToString()));

            Searcher searcher = sparkSearcherBL.GetSearcher(1);
            string originalIndexPath = searcher.IndexPath;
            searcher.NUNIT_IndexPath = newSparkIndexPath;
            searcher.Update();

            stopWatch.Start();
            IMatchnetQueryResults results2 = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            stopWatch.Stop();


            millis = stopWatch.ElapsedMilliseconds;
            Assert.AreEqual(searcher.NUNIT_IndexPath, searcher.IndexPath);
            Assert.NotNull(results2);
            Assert.GreaterOrEqual(results2.Items.Count, 1);
            //Assert.Less(millis, 13000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results2.Items.Count, searchPrefs.ToString()));

            testSearchPrefs.Add("domainid", 1 + "");
            sparkSearcherBL.RunQuery(TestQueryBuilder.build(testSearchPrefs, 1, 0), 1);
            testSearchPrefs.Add("domainid", "");

            //wait, ensure close old searcher finishes
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }

            //reset back to original index path
            searcher.NUNIT_IndexPath = originalIndexPath;
            searcher.Update();
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }
        }


        [Test]
        public void TestCopyIndex()
        {
            string server = "devapp01";
            string indexPath1 = "c:/Matchnet/Index/Spark";
            Assert.AreEqual("\\\\devapp01\\c$\\Matchnet\\Index\\Spark1", createUncPathForCopyIndex(indexPath1, 1, server));

            indexPath1 = "d:\\Matchnet\\Index\\Spark";
            Assert.AreEqual("\\\\devapp01\\d$\\Matchnet\\Index\\Spark2", createUncPathForCopyIndex(indexPath1, 2, server));

            
            string indexPath2 = "c://Matchnet/Index/Spark";
            Assert.AreEqual("\\\\devapp01\\c$\\Matchnet\\Index\\Spark3", createUncPathForCopyIndex(indexPath2, 3, server));

            indexPath2 = "d:\\\\Matchnet\\Index\\Spark";
            Assert.AreEqual("\\\\devapp01\\d$\\Matchnet\\Index\\Spark4", createUncPathForCopyIndex(indexPath2, 4, server));
        }

        [Test]
        public void SearchIncompleteHeightSparkQueriesAnyPrefsTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection noHeightMaxSearchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            noHeightMaxSearchPrefs.Add("minheight", "140");
            noHeightMaxSearchPrefs.Add("colorcode", "");
            noHeightMaxSearchPrefs.Add("languagemask", "2147483647");
            noHeightMaxSearchPrefs.Add("regionidcity", "3403764");
            noHeightMaxSearchPrefs.Add("schoolid", "");
            noHeightMaxSearchPrefs.Add("distance", "160");
            noHeightMaxSearchPrefs.Add("regionid", "3443821");
            noHeightMaxSearchPrefs.Add("searchtype", "1");
            noHeightMaxSearchPrefs.Add("areacodes", "");
            noHeightMaxSearchPrefs.Add("geodistance", "5");
            noHeightMaxSearchPrefs.Add("searchtypeid", "1");
            noHeightMaxSearchPrefs.Add("ethnicity", "1790");
            noHeightMaxSearchPrefs.Add("maritalstatus", "56");
            noHeightMaxSearchPrefs.Add("educationlevel", "510");
            noHeightMaxSearchPrefs.Add("countryregionid", "223");
            noHeightMaxSearchPrefs.Add("smokinghabits", "30");
            noHeightMaxSearchPrefs.Add("longitude", "-2.066426");
            noHeightMaxSearchPrefs.Add("domainid", "1");
            noHeightMaxSearchPrefs.Add("bodytype", "2097149");
            noHeightMaxSearchPrefs.Add("latitude", "0.594637");
            noHeightMaxSearchPrefs.Add("searchorderby", "1");
            noHeightMaxSearchPrefs.Add("minage", "20");
            noHeightMaxSearchPrefs.Add("religion", "8388606");
            noHeightMaxSearchPrefs.Add("maxage", "55");
            noHeightMaxSearchPrefs.Add("drinkinghabits", "30");
            noHeightMaxSearchPrefs.Add("gendermask", "6");
            noHeightMaxSearchPrefs.Add("longitude", "-2.066127");
            noHeightMaxSearchPrefs.Add("latitude", "0.594557");
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(noHeightMaxSearchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, noHeightMaxSearchPrefs.ToString()));
        }

        [Test]
        public void SearchIncompleteAgeSparkQueriesAnyPrefsTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection noAgeMaxSearchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            noAgeMaxSearchPrefs.Add("minheight", "140");
            noAgeMaxSearchPrefs.Add("colorcode", "");
            noAgeMaxSearchPrefs.Add("maxheight", "244");
            noAgeMaxSearchPrefs.Add("languagemask", "2147483647");
            noAgeMaxSearchPrefs.Add("regionidcity", "3403764");
            noAgeMaxSearchPrefs.Add("schoolid", "");
            noAgeMaxSearchPrefs.Add("distance", "160");
            noAgeMaxSearchPrefs.Add("regionid", "3443821");
            noAgeMaxSearchPrefs.Add("searchtype", "1");
            noAgeMaxSearchPrefs.Add("areacodes", "");
            noAgeMaxSearchPrefs.Add("geodistance", "5");
            noAgeMaxSearchPrefs.Add("searchtypeid", "1");
            noAgeMaxSearchPrefs.Add("ethnicity", "1790");
            noAgeMaxSearchPrefs.Add("maritalstatus", "56");
            noAgeMaxSearchPrefs.Add("educationlevel", "510");
            noAgeMaxSearchPrefs.Add("countryregionid", "223");
            noAgeMaxSearchPrefs.Add("smokinghabits", "30");
            noAgeMaxSearchPrefs.Add("longitude", "-2.066426");
            noAgeMaxSearchPrefs.Add("domainid", "1");
            noAgeMaxSearchPrefs.Add("bodytype", "2097149");
            noAgeMaxSearchPrefs.Add("latitude", "0.594637");
            noAgeMaxSearchPrefs.Add("searchorderby", "1");
            noAgeMaxSearchPrefs.Add("minage", "20");
            noAgeMaxSearchPrefs.Add("religion", "8388606");
//            noHeightMaxSearchPrefs.Add("maxage", "55");
            noAgeMaxSearchPrefs.Add("drinkinghabits", "30");
            noAgeMaxSearchPrefs.Add("gendermask", "6");
            noAgeMaxSearchPrefs.Add("longitude", "-2.066127");
            noAgeMaxSearchPrefs.Add("latitude", "0.594557");
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(noAgeMaxSearchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, noAgeMaxSearchPrefs.ToString()));
        }

        [Test]
        public void SearchIncompleteLocationSparkQueriesAnyPrefsTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection locationSearchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            locationSearchPrefs.Add("minheight", "140");
            locationSearchPrefs.Add("colorcode", "");
            locationSearchPrefs.Add("maxheight", "244");
            locationSearchPrefs.Add("languagemask", "2147483647");
            locationSearchPrefs.Add("regionidcity", "3403764");
            locationSearchPrefs.Add("schoolid", "");
            locationSearchPrefs.Add("distance", "160");
            locationSearchPrefs.Add("regionid", "3443821");
            locationSearchPrefs.Add("searchtype", "1");
            locationSearchPrefs.Add("areacodes", "");
            //locationSearchPrefs.Add("geodistance", "5");
            locationSearchPrefs.Add("searchtypeid", "1");
            locationSearchPrefs.Add("ethnicity", "1790");
            locationSearchPrefs.Add("maritalstatus", "56");
            locationSearchPrefs.Add("educationlevel", "510");
            locationSearchPrefs.Add("countryregionid", "223");
            locationSearchPrefs.Add("smokinghabits", "30");
            locationSearchPrefs.Add("longitude", "-2.066426");
            locationSearchPrefs.Add("domainid", "1");
            locationSearchPrefs.Add("bodytype", "2097149");
            locationSearchPrefs.Add("latitude", "0.594637");
            locationSearchPrefs.Add("searchorderby", "1");
            locationSearchPrefs.Add("minage", "20");
            locationSearchPrefs.Add("religion", "8388606");
            locationSearchPrefs.Add("maxage", "55");
            locationSearchPrefs.Add("drinkinghabits", "30");
            locationSearchPrefs.Add("gendermask", "6");
            locationSearchPrefs.Add("longitude", "-2.066127");
            locationSearchPrefs.Add("latitude", "0.594557");
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(locationSearchPrefs, 1, 101), 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, locationSearchPrefs.ToString()));
        }

        [Test]
        public void TestSparkQueryWithAgeDupes()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("RegionIDCountry","223");
            searchPrefs.Add("RegionIDCountry","223");
            searchPrefs.Add("RegionIDCountry","223");
            searchPrefs.Add("RegionIDCountry","223");
            searchPrefs.Add("RegionID","3463725");
            searchPrefs.Add("AgeMax","35");
            searchPrefs.Add("HasPhotoFlag","1");
            searchPrefs.Add("GeoDistance","5");
            searchPrefs.Add("RegionIDCity","3421574");
            searchPrefs.Add("Sorting","1");
            searchPrefs.Add("Longitude","-1.296946");
            searchPrefs.Add("DomainID","1");
            searchPrefs.Add("DomainID","1");
            searchPrefs.Add("Latitude","0.708971");
            searchPrefs.Add("AgeMin","18");
            searchPrefs.Add("AgeMin","18");
            searchPrefs.Add("AgeMin","18");
            searchPrefs.Add("GenderMask","6");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 1, 101);
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMin, "18"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMin, "18"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMin, "18"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.DomainID, "1"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.DomainID, "1"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.RegionIDCountry, "223"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.RegionIDCountry, "223"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.RegionIDCountry, "223"));

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(q, 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));

            searchPrefs = null;
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("HeightMin", "152");
            searchPrefs.Add("RegionIDCountry", "223");
            searchPrefs.Add("RegionID", "3403837");
            searchPrefs.Add("Latitude", "0.595971");
            searchPrefs.Add("DomainID", "1");
            searchPrefs.Add("AgeMin", "20");
            searchPrefs.Add("AgeMax", "55");
            searchPrefs.Add("AgeMax", "55");
            searchPrefs.Add("AgeMax", "55");
            searchPrefs.Add("Sorting", "1");
            searchPrefs.Add("HasPhotoFlag", "1");
            searchPrefs.Add("Longitude", "-2.071135");
            searchPrefs.Add("GeoDistance", "6");
            searchPrefs.Add("GenderMask", "6");
            searchPrefs.Add("RegionIDCity", "3403837");
            searchPrefs.Add("HeightMax", "188");

            stopWatch.Reset();

            q = TestQueryBuilder.build(searchPrefs, 1, 101);
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMax, "55"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMax, "55"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMax, "55"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMax, "55"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.AgeMax, "55"));

            stopWatch.Start();
            IMatchnetQueryResults results2 = sparkSearcherBL.RunQuery(q, 1);
            stopWatch.Stop();
            long millis2 = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results2);
            Assert.GreaterOrEqual(results2.Items.Count, 1);
            //Assert.Less(millis2, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis2, results2.Items.Count, searchPrefs.ToString()));
        }
        
        [Test]
        public void TestSparkQueryWithHeightDupes()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("HeightMin","152");
            searchPrefs.Add("HeightMin","152");
            searchPrefs.Add("RegionIDCountry","223");
            searchPrefs.Add("RegionID","3403837");
            searchPrefs.Add("Latitude","0.595971");
            searchPrefs.Add("DomainID","1");
            searchPrefs.Add("AgeMin","20");
            searchPrefs.Add("AgeMax","55");
            searchPrefs.Add("Sorting","1");
            searchPrefs.Add("HasPhotoFlag","1");
            searchPrefs.Add("Longitude","-2.071135");
            searchPrefs.Add("GeoDistance","6");
            searchPrefs.Add("GenderMask","6");
            searchPrefs.Add("RegionIDCity","3403837");
            searchPrefs.Add("HeightMax", "188");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 1, 101);
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMin, "152"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMin, "152"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMin, "152"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMin, "152"));

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(q, 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));

            searchPrefs = null;
            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("HeightMin", "152");
            searchPrefs.Add("RegionIDCountry", "223");
            searchPrefs.Add("RegionID", "3403837");
            searchPrefs.Add("Latitude", "0.595971");
            searchPrefs.Add("DomainID", "1");
            searchPrefs.Add("AgeMin", "20");
            searchPrefs.Add("AgeMax", "55");
            searchPrefs.Add("Sorting", "1");
            searchPrefs.Add("HasPhotoFlag", "1");
            searchPrefs.Add("Longitude", "-2.071135");
            searchPrefs.Add("GeoDistance", "6");
            searchPrefs.Add("GenderMask", "6");
            searchPrefs.Add("RegionIDCity", "3403837");
            searchPrefs.Add("HeightMax", "188");
            searchPrefs.Add("HeightMax", "188");

            stopWatch.Reset();

            q = TestQueryBuilder.build(searchPrefs, 1, 101);
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMax, "188"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMax, "188"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMax, "188"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMax, "188"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMax, "188"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.HeightMax, "188"));

            stopWatch.Start();
            IMatchnetQueryResults results2 = sparkSearcherBL.RunQuery(q, 1);
            stopWatch.Stop();
            long millis2 = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results2);
            Assert.GreaterOrEqual(results2.Items.Count, 1);
            //Assert.Less(millis2, 2000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis2, results2.Items.Count, searchPrefs.ToString()));
        }

        [Test]
        public void TestSparkQueryNullException()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("RegionIDCountry", "223");
            //searchPrefs.Add("HasPhotoFlag", "1");
//            searchPrefs.Add("RegionID", "3479144");
            searchPrefs.Add("AreaCodes", "813,727,941,,407,954,718,");
            //searchPrefs.Add("AreaCodes", "619,760,");
            //searchPrefs.Add("AreaCode", "254,");
            //searchPrefs.Add("AreaCode", "360,206,425,253,");
//            searchPrefs.Add("GeoDistance", "5");
            searchPrefs.Add("Sorting", "1");
            searchPrefs.Add("DomainID", "1");
            searchPrefs.Add("AgeMin", "20");
            searchPrefs.Add("AgeMax", "89");
            searchPrefs.Add("GenderMask", "6");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 1, 101);
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.RegionIDCountry, "223"));
            q.AddParameter(new Matchnet.Search.ValueObjects.MatchnetQueryParameter(QuerySearchParam.RegionIDCountry, "223"));

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunQuery(q, 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.MatchesFound, 1);
            //Assert.Less(millis,5000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.MatchesFound, searchPrefs.ToString()));
        }

        [Test]
        public void TestSparkQueryMinMaxAgeResult()
        {
            int minAge = 27;
            int maxAge = 27;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minage", minAge.ToString());
            searchPrefs.Add("maxage", maxAge.ToString());
            searchPrefs.Add("gendermask", "6");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 1, 101);

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunDetailedQuery(q, 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            bool minIncluded = false;
            bool maxIncluded = false;
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Age: " + detailedQueryResult.Tags["age"]);
                    int age = Convert.ToInt32(detailedQueryResult.Tags["age"]);
                    if (minAge == age) minIncluded = true;
                    if (maxAge == age) maxIncluded = true;
                }
            }
            Assert.IsTrue(minIncluded, "No results had min age = " + minAge);
            Assert.IsTrue(maxIncluded, "No results had max age = " + maxAge);
        }


        [Test]
        public void TestSparkQueryMinMaxHeightResult()
        {
            int minHeight = 190;
            int maxHeight = 195;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("minheight", minHeight.ToString());
            searchPrefs.Add("maxheight", maxHeight.ToString());
            searchPrefs.Add("gendermask", "9");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 1, 101);

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunDetailedQuery(q, 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            bool minIncluded = false;
            bool maxIncluded = false;
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Height: " + detailedQueryResult.Tags["height"]);
                    int height = Convert.ToInt32(detailedQueryResult.Tags["height"]);
                    if (minHeight == height) minIncluded = true;
                    if (maxHeight == height) maxIncluded = true;
                }
            }
            Assert.IsTrue(minIncluded, "No results had min height = " + minHeight);
            Assert.IsTrue(maxIncluded, "No results had max height = " + maxHeight);
        }

        [Test]
        public void TestJDateQueryMaritalStatusSingleResult()
        {
            int maritalstatus = 2;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("distance", "40");
            searchPrefs.Add("regionid", "3403764");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "5");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("maritalstatus", maritalstatus.ToString());
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("agemin","18");
            searchPrefs.Add("agemax","89");
            searchPrefs.Add("memberid", "104700290");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: "+detailedQueryResult.MemberID+", MaritalStatus: " + detailedQueryResult.Tags["maritalstatus"]);
                    int val = Convert.ToInt32(detailedQueryResult.Tags["maritalstatus"]);
                    Assert.AreEqual(maritalstatus, val);
                }
            }
        }

        [Test]
        public void TestJDateQueryMaritalStatusSingleAndMarriedResult()
        {
            int maritalstatus = 2+8;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3444766");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("maritalstatus", maritalstatus.ToString());
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("agemin", "18");
            searchPrefs.Add("agemax", "70");
            searchPrefs.Add("memberid", "126475891");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", MaritalStatus: " + detailedQueryResult.Tags["maritalstatus"]);
                    int val = Convert.ToInt32(detailedQueryResult.Tags["maritalstatus"]);
                    Assert.True((val & maritalstatus) == val);
                }
            }
        }

        [Test]
        public void TestJDateQueryRegistrationDateRangeResult()
        {
            DateTime registrationMinDate = DateTime.Parse("01/01/2009");
            DateTime registrationMaxDate = DateTime.Parse("01/01/2012");
            
            int maritalstatus = 2 + 8;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3444766");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("maritalstatus", maritalstatus.ToString());
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("agemin", "18");
            searchPrefs.Add("agemax", "70");
            searchPrefs.Add("memberid", "126475891");
            searchPrefs.Add("registrationdatemin", registrationMinDate.ToString());
            searchPrefs.Add("registrationdatemax", registrationMaxDate.ToString());

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    print("Id: " + detailedQueryResult.MemberID + ", CommunityInsertDate: " + detailedQueryResult.RegisterDate);
                    Assert.True(detailedQueryResult.RegisterDate >= registrationMinDate && detailedQueryResult.RegisterDate <= registrationMaxDate);
                }
            }
        }

        [Test]
        public void TestJDateQuerySubExpirationDateRangeResult()
        {
            DateTime subExpirationMinDate = DateTime.Parse("01/01/2009");
            DateTime subExpirationMaxDate = DateTime.Parse("01/01/2012");

            int maritalstatus = 2 + 8;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3444766");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("maritalstatus", maritalstatus.ToString());
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("agemin", "18");
            searchPrefs.Add("agemax", "70");
            searchPrefs.Add("memberid", "126475891");
            searchPrefs.Add("subexpirationdatemin", subExpirationMinDate.ToString());
            searchPrefs.Add("subexpirationdatemax", subExpirationMaxDate.ToString());

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    DateTime subExpirationDate =DateTime.Parse(detailedQueryResult.Tags["subscriptionexpirationdate"].ToString());
                    print("Id: " + detailedQueryResult.MemberID + ", CommunityInsertDate: " + subExpirationDate.ToString());

                    Assert.True(subExpirationDate >= subExpirationMinDate && subExpirationDate <= subExpirationMaxDate);
                }
            }
        }

        [Test]
        public void TestJDateQueryMoreChildrenFlagNotSureResult()
        {
            int moreChildrenFlag = 4;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("radius", "3000");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            //searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("morechildrenflag", moreChildrenFlag.ToString());
            searchPrefs.Add("gendermask", "9");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2500);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            Dictionary<int, int> moreChildrenConversionTable = Utils.GetMoreChildrenFlagConversionTable();
            int mcValue = moreChildrenConversionTable[moreChildrenFlag];
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", MoreChildrenFlag: " + detailedQueryResult.Tags["morechildrenflag"]);
                    int val = Convert.ToInt32(detailedQueryResult.Tags["morechildrenflag"]);
                    Assert.AreEqual(mcValue, val);
                }
            }
        }

        [Test]
        public void TestJDateQueryMoreChildrenFlagYesAndNoResult()
        {
            int moreChildrenFlag = 1+2;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("morechildrenflag", moreChildrenFlag.ToString());
            searchPrefs.Add("gendermask", "6");

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            Dictionary<int, int> moreChildrenConversionTable = Utils.GetMoreChildrenFlagConversionTable();
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", MoreChildrenFlag: " + detailedQueryResult.Tags["morechildrenflag"]);
                    int val = Convert.ToInt32(detailedQueryResult.Tags["morechildrenflag"]);
                    int maskValue = moreChildrenConversionTable.Where(kvp => kvp.Value == val).Select(kvp => kvp.Key).FirstOrDefault();
                    Assert.True((maskValue & moreChildrenFlag) == maskValue);
                }
            }
        }

        [Test]
        //Man seeking Woman - 6
        //Woman seeking Man - 9
        public void TestRafiSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("distance", "40");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "5");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "40");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("longitude", "-2.066127");
            searchPrefs.Add("latitude", "0.594557");
            searchPrefs.Add("childrencount", "1");

            Stopwatch stopWatch = new Stopwatch();
            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    print("Id: " + detailedQueryResult.MemberID + ", Age: " + detailedQueryResult.Tags["age"]);
                }
            }
        }

        [Test]
        public void TestLocalSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            int maritalstatus = 2;
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("distance", "40");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "5");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("minage", "29");
            searchPrefs.Add("maxage", "89");
            searchPrefs.Add("memberid", "411");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("maritalstatus", maritalstatus.ToString());

            Stopwatch stopWatch = new Stopwatch();
            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 1, 101);

            stopWatch.Start();
            SearcherBL sparkSearcherBL = GetSearcherBL(1);
            IMatchnetQueryResults results = sparkSearcherBL.RunDetailedQuery(q, 1);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", MaritalStatus: " + detailedQueryResult.Tags["maritalstatus"]);
                    int val = Convert.ToInt32(detailedQueryResult.Tags["maritalstatus"]);
                    Assert.AreEqual(maritalstatus, val);
                }
            }
        }

        [Test]
        //Man seeking Woman - 6
        //Woman seeking Man - 9
        public void TestJdateChildrenCountOneSearch()
        {
            int childrencount = 1;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "9801464");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("countryregionid", "105");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "90");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("childrencount", "2");//2 is the mask value for 1 child

            Stopwatch stopWatch = new Stopwatch();
            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 3000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", ChildrenCount: " + detailedQueryResult.Tags["childrencount"]);
                    int val = Convert.ToInt32(detailedQueryResult.Tags["childrencount"]);
                    Assert.AreEqual(childrencount, val);
                }
            }
        }

        [Test]
        public void TestJdateChildrenCountTwoAndThreeOrMoreSearch()
        {
            int childrencount = 4+8;
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "9801464");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("countryregionid", "105");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "90");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("childrencount", childrencount.ToString());

            Stopwatch stopWatch = new Stopwatch();

            IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, 3, 103);

            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(q, 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 3000);
            print(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            Dictionary<int,int> ccTable = Utils.GetChildrenCountConversionTable();
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", ChildrenCount: " + detailedQueryResult.Tags["childrencount"]);
                    int val = ccTable[Convert.ToInt32(detailedQueryResult.Tags["childrencount"])];
                    Assert.True((val & childrencount) == val);
                }
            }
        }

        [Test]
        public void TestSparkCustody()
        {
            int communityId = 1;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);

            //set test members
            List<SearchMember> sparkSearchMembers = new List<SearchMember>();
            //custody: not answered
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939806, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0));
            //custody: i have no children (2)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103939807, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-04-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, 2, -1, -1, 1, 0));
            //custody: lives with me sometimes (8)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(103820222, 1, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-05-28"), 0, 8, 8, 2, 256, 2, 2, 168, 2, -21474, 0, 0, 0, 0, 4, 0, 64, -1, -1, 223, 1538, 3404282, 9805694, -2.065197, 0.595144, -1, string.Empty, 0, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, 8, -1, -1, 1, 0));
            //custody: i have no children + lives with me sometimes (10)
            sparkSearchMembers.Add(SearchMember.GenerateSearchMember(23247846, 1, 6, DateTime.Parse("1974-12-10"), DateTime.Parse("2003-06-12"), DateTime.Parse("2011-06-28"), 0, 128, 131072, 2, 2, 4, 4, -21474, 8, 0, 0, 0, 0, 0, 128, 0, 8, 65536, 0, 223, 1538, 3404721, 3445194, -2.061917, 0.595984, -1, string.Empty, 0, DateTime.Parse("2006-02-07"), -1, -1, -1, DateTime.Parse("2008-06-26"), -1, 10, -1, -1, 1, 0));

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL, sparkSearchMembers);

            //basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", communityId.ToString());
            searchPrefs.Add("searchtype", "1"); //web search
            searchPrefs.Add("searchtypeid", "1"); //zip code
            searchPrefs.Add("searchorderby", "2"); //last online (lastactivedate)
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6"); //6-women seeking male, 9-male seeking female
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817"); //beverly hills 3443817
            searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("regionIDCity", "3403764");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchredesign30", "1");

            IMatchnetQueryResults results;
            //get initial results count
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            int initialResultsCount = results.MatchesFound;
            int initialMostRecentActiveMember = results.Items[0].MemberID;
            Assert.AreEqual(sparkSearchMembers.Count, initialResultsCount);

            //custody value: 2
            searchPrefs.Add("custody", "2");
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            Assert.AreEqual(1, results.MatchesFound);

            //custody value: 8
            searchPrefs.Add("custody", "8");
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            Assert.AreEqual(1, results.MatchesFound);

            //custody value: 10
            searchPrefs.Add("custody", "10");
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            Assert.AreEqual(2, results.MatchesFound);

            //custody value: 16
            searchPrefs.Add("custody", "16");
            results = sparkSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 1, 101), 1);
            Assert.AreEqual(0, results.MatchesFound);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void SearchCupidAnyPrefsTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "105");
            searchPrefs.Add("regionid", "9800654");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("radius", "1000");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
//            searchPrefs.Add("areacodes", ",4,,5,");
            searchPrefs.Add("domainid", "10");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "88");
            searchPrefs.Add("gendermask", "9");

            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            SearcherBL cupidSearcherBL = GetSearcherBL(10);
            IMatchnetQueryResults results = cupidSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 10, 15), 10);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.MatchesFound, 1);
            //Assert.Less(millis, 3000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.MatchesFound, searchPrefs.ToString()));
            for (int i = 0; i < results.Items.Count; i++)
            {
                IMatchnetResultItem item = results.Items[i];
                Console.WriteLine(string.Format("MemberId: {0}", item.MemberID));
            }
        }

        [Test]
        public void TestJdate5MileRadiusSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("regionid", "3465051");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("geodistance","2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 3200);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            jdateSearcherBL.NUNIT_Print = false;       
        }

        [Test]
        public void TestJdateAnyRadiusSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("regionid", "3465051");
            searchPrefs.Add("searchorderby", "3");
            //searchPrefs.Add("geodistance", "2");
            searchPrefs.Add("radius", "3070");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("memberid", "119620021");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2500);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            jdateSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void TestJdateZeroResultsSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("geodistance", "2");
            searchPrefs.Add("regionid", "3466373");
            searchPrefs.Add("minage", "25");
            searchPrefs.Add("maxage", "34");
            //searchPrefs.Add("jdatereligion", "2047");
            //searchPrefs.Add("maritalstatus", "2");
            //searchPrefs.Add("maxheight", "168");
            //searchPrefs.Add("educationlevel", "240");

            //searchPrefs.Add("synagogueattendance", "1");
            //searchPrefs.Add("keepkosher", "2");
            //searchPrefs.Add("minheight", "170");
            //searchPrefs.Add("morechildrenflag", "1");
            //searchPrefs.Add("jdateethnicity", "31");
            //searchPrefs.Add("smokinghabits", "2");
            //searchPrefs.Add("activitylevel", "50");
            //searchPrefs.Add("drinkinghabits", "14");
            //searchPrefs.Add("languagemask", "2");
            //searchPrefs.Add("relocateflag", "2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5200);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", RegionId: " + detailedQueryResult.Tags["regionid"]);
//                    int val = Convert.ToInt32(detailedQueryResult.Tags["languagemask"]);
                }
            }
            jdateSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void TestBlackSinglesZeroResultsSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();

            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("regionid", "3450224");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "1");
            searchPrefs.Add("domainid", "24");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("geodistance", "7");
            //searchPrefs.Add("minage", "50");
            //searchPrefs.Add("maxage", "64");
            //searchPrefs.Add("smokinghabits", "2");
            //searchPrefs.Add("minheight", "178");
            //searchPrefs.Add("minheight", "185");
            //searchPrefs.Add("maritalstatus", "42");
            //searchPrefs.Add("drinkinghabits", "8");
            //searchPrefs.Add("educationlevel", "2048");            
            searchPrefs.Add("ethnicity", "16");
            searchPrefs.Add("bodytype", "4");
            searchPrefs.Add("religion", "8192");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL blackSearcherBL = GetSearcherBL(24);
            blackSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = blackSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 24, 0), 24);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5200);
            printResults(searchPrefs, results, millis, blackSearcherBL);
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", Education: " + detailedQueryResult.Tags["educationlevel"]);
//                    int val = Convert.ToInt32(detailedQueryResult.Tags["languagemask"]);
                }
            }
            blackSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void TestBlackSinglesAnyRadiusSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "1");
            //searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("regionid", "3455300");
            //searchPrefs.Add("regionid", "3472697");
            //searchPrefs.Add("searchtype", "1");
            //searchPrefs.Add("searchtypeid", "1");
            //searchPrefs.Add("searchorderby", "1");
            //searchPrefs.Add("domainid", "24");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("searchorderby", "2");
            searchPrefs.Add("radius", "2000");
            //searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("minage", "36");
            searchPrefs.Add("maxage", "46");
            //searchPrefs.Add("smokinghabits", "2");
            searchPrefs.Add("minheight", "135");
            searchPrefs.Add("maxheight", "188");
            //searchPrefs.Add("maritalstatus", "42");
            //searchPrefs.Add("drinkinghabits", "8");
            //searchPrefs.Add("educationlevel", "2048");            
            //searchPrefs.Add("ethnicity", "16");
            //searchPrefs.Add("bodytype", "4");
            //searchPrefs.Add("religion", "8192");
            //searchPrefs.Add("searchredesign30","1");
            searchPrefs.Add("memberid","118674227");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL blackSearcherBL = GetSearcherBL(24);
            blackSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = blackSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 24, 0), 24);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5200);
            printResults(searchPrefs, results, millis, blackSearcherBL);
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", Education: " + detailedQueryResult.Tags["educationlevel"]);
                    //                    int val = Convert.ToInt32(detailedQueryResult.Tags["languagemask"]);
                }
            }
            blackSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void TestJdateNoDistanceResultsSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("gendermask", "6");
            //searchPrefs.Add("geodistance", "2");
            searchPrefs.Add("regionid", "3466373");
            searchPrefs.Add("minage", "25");
            searchPrefs.Add("maxage", "80");
            //searchPrefs.Add("jdatereligion", "2047");
            //searchPrefs.Add("maritalstatus", "2");
            //searchPrefs.Add("maxheight", "168");
            //searchPrefs.Add("educationlevel", "240");

            //searchPrefs.Add("synagogueattendance", "1");
            //searchPrefs.Add("keepkosher", "2");
            //searchPrefs.Add("minheight", "170");
            //searchPrefs.Add("morechildrenflag", "1");
            //searchPrefs.Add("jdateethnicity", "31");
            //searchPrefs.Add("smokinghabits", "2");
            //searchPrefs.Add("activitylevel", "50");
            //searchPrefs.Add("drinkinghabits", "14");
            //searchPrefs.Add("languagemask", "2");
            //searchPrefs.Add("relocateflag", "2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5200);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", RegionId: " + detailedQueryResult.Tags["regionid"]);
                    //                    int val = Convert.ToInt32(detailedQueryResult.Tags["languagemask"]);
                }
            }
            jdateSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void TestJdateSecretAdmirerSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("searchtype", "3");
            searchPrefs.Add("searchtypeid", "3");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minage", "41");
            searchPrefs.Add("maxage", "51");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5200);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", BodyType: " + detailedQueryResult.Tags["bodytype"]);
                    //                    int val = Convert.ToInt32(detailedQueryResult.Tags["languagemask"]);
                }
            }
            jdateSearcherBL.NUNIT_Print = false;
        }

//        earchType: WebSearch|Param: Sorting, Val:6|Param: KeywordSearch, Val:science|Param: KeywordSearch, Val:science|Param: SearchRedesign30, Val:1|Param: RegionIDCountry, Val:223|Param: JDateReligion, Val:18431|Param: DomainID, Val:3|Param: DomainID, Val:3|Param: HasPhotoFlag, Val:1|Param: RegionID, Val:9808897|Param: GeoDistance, Val:10|Param: MaritalStatus, Val:2|Param: ChildrenCount, Val:1|Param: RegionIDCity, Val:3414161|Param: SmokingHabits, Val:2|Param: Longitude, Val:-1.720155|Param: BlockedMemberIDs, Val:120124191,120310214,119307477,15411466,127271653,115559132,114726824,119579465,115712912,113953359,114567453,31693430,118867908,115515775,119882645,119944214,112472044,40965815,126407542,114268256,114653949,126819691,47392088,115147240,115413402,126424591,115365993,118845153|Param: Latitude, Val:0.694926|Param: AgeMin, Val:18|Param: AgeMax, Val:54|Param: AgeMax, Val:54|Param: GenderMask, Val:6 ---> Matchnet.Exceptions.BLException: Unable to run query.  Query: SearchType: WebSearch|Param: Sorting, Val:6|Param: KeywordSearch, Val:science|Param: KeywordSearch, Val:science|Param: SearchRedesign30, Val:1|Param: RegionIDCountry, Val:223|Param: JDateReligion, Val:18431|Param: DomainID, Val:3|Param: DomainID, Val:3|Param: HasPhotoFlag, Val:1|Param: RegionID, Val:9808897|Param: GeoDistance, Val:10|Param: MaritalStatus, Val:2|Param: ChildrenCount, Val:1|Param: RegionIDCity, Val:3414161|Param: SmokingHabits, Val:2|Param: Longitude, Val:-1.720155|Param: BlockedMemberIDs, Val:120124191,120310214,119307477,15411466,127271653,115559132,114726824,119579465,115712912,113953359,114567453,31693430,118867908,115515775,119882645,119944214,112472044,40965815,126407542,114268256,114653949,126819691,47392088,115147240,115413402,126424591,115365993,118845153|Param: Latitude, Val:0.694926|Param: AgeMin, Val:18|Param: AgeMax, Val:54|Param: AgeMax, Val:54|Param: GenderMask, Val:6 ---> Matchnet.Exceptions.BLException: Unable to run query.  Query: SearchType: WebSearch|Param: Sorting, Val:6|Param: KeywordSearch, Val:science|Param: KeywordSearch, Val:science|Param: SearchRedesign30, Val:1|Param: RegionIDCountry, Val:223|Param: JDateReligion, Val:18431|Param: DomainID, Val:3|Param: DomainID, Val:3|Param: HasPhotoFlag, Val:1|Param: RegionID, Val:9808897|Param: GeoDistance, Val:10|Param: MaritalStatus, Val:2|Param: ChildrenCount, Val:1|Param: RegionIDCity, Val:3414161|Param: SmokingHabits, Val:2|Param: Longitude, Val:-1.720155|Param: BlockedMemberIDs, Val:120124191,120310214,119307477,15411466,127271653,115559132,114726824,119579465,115712912,113953359,114567453,31693430,118867908,115515775,119882645,119944214,112472044,40965815,126407542,114268256,114653949,126819691,47392088,115147240,115413402,126424591,115365993,118845153|Param: Latitude, Val:0.694926|Param: AgeMin, Val:18|Param: AgeMax, Val:54|Param: AgeMax, Val:54|Param: GenderMask, Val:6 ---> 
        [Test]
        public void TestJdateFailingSearch()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("searchtype", "3");
            searchPrefs.Add("searchtypeid", "3");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minage", "41");
            searchPrefs.Add("maxage", "51");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            //sort by proximity
            stopWatch.Stop();
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 5200);
            printResults(searchPrefs, results, millis, jdateSearcherBL);
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                if (item is DetailedQueryResult)
                {
                    DetailedQueryResult detailedQueryResult = (DetailedQueryResult)item;
                    //print("Memberid: " + detailedQueryResult.MemberID);
                    print("Id: " + detailedQueryResult.MemberID + ", BodyType: " + detailedQueryResult.Tags["bodytype"]);
                    //                    int val = Convert.ToInt32(detailedQueryResult.Tags["languagemask"]);
                }
            }
            jdateSearcherBL.NUNIT_Print = false;
        }


        [Test]
        public void SearchJDateUpdateCloseOldSearcherTest()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            //searchPrefs.Add("hasphotoflag", "0");
            searchPrefs.Add("schoolid", "");
            searchPrefs.Add("distance", "160");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("areacodes", "");
            searchPrefs.Add("geodistance", "7");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("regionidcity", "3403764");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("minage", "20");
            searchPrefs.Add("maxage", "85");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("languagemask", "2");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            Searcher jdateSearcher = jdateSearcherBL.GetSearcher(3);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();
            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            string originalIndexPath = jdateSearcher.IndexPath;
            string lockFile = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT,System.Environment.MachineName);
            Assert.IsTrue(System.IO.File.Exists(originalIndexPath + "/" + lockFile));
            //Assert.Less(millis, 3000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}\n\n\n", millis, results.Items.Count, searchPrefs.ToString()));
            
            Searcher searcher = jdateSearcherBL.GetSearcher(3);
            searcher.NUNIT_IndexPath = newJdateIndexPath;
            searcher.Update();

            stopWatch.Start();
            IMatchnetQueryResults results2 = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();


            millis = stopWatch.ElapsedMilliseconds;
            Assert.AreEqual(searcher.NUNIT_IndexPath, searcher.IndexPath);
            Assert.NotNull(results2);
            Assert.GreaterOrEqual(results2.Items.Count, 1);
            Assert.IsTrue(System.IO.File.Exists(jdateSearcher.IndexPath + "/" + lockFile));

            //Assert.Less(millis, 13000);
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results2.Items.Count, searchPrefs.ToString()));
            testSearchPrefs.Add("domainid", 3 + "");
            jdateSearcherBL.RunQuery(TestQueryBuilder.build(testSearchPrefs, 3, 0), 3);
            testSearchPrefs.Add("domainid", "");
            if (searcher.NUNIT_UseNRT)
            {
                System.Threading.Thread.Sleep(10000);
            }
            else
            {
                System.Threading.Thread.Sleep(5000);
            }
            Assert.IsFalse(System.IO.File.Exists(originalIndexPath + "/" + lockFile));
        }

        [Test]
        public void TestDefaultPreferences()
        {
            //This test will ensure members with no preferences are given default preferences when indexed
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //has preferences
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(1039398061, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs1.Add("minage", "19");
            searchPrefs1.Add("maxage", "95");
            searchPrefs1.Add("gendermask", "6");
            searchPrefs1.Add("smokinghabits", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //has no preferences
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(1039398071, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            searchMembers.Add(searchMember2);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                DetailedQueryResult detailedQueryResult = (DetailedQueryResult)results.Items[i];
                int memberID = detailedQueryResult.MemberID;
                switch (memberID)
                {
                    case 1039398061:
                        //this member has preferences; verifying default preferences did not overwrite
                        //preferred min age is 19, preferred max age is 95;
                        Assert.AreEqual(19, Convert.ToInt32(detailedQueryResult.Tags["preferredminage"]));
                        Assert.AreEqual(95, Convert.ToInt32(detailedQueryResult.Tags["preferredmaxage"]));
                        //preferred gendermask is 6
                        Assert.AreEqual(6, Convert.ToInt32(detailedQueryResult.Tags["preferredgendermask"]));
                        //preferred smoking habit is 1
                        Assert.AreEqual(1, Convert.ToInt32(detailedQueryResult.Tags["preferredsmokinghabit"]));
                        break;
                    case 1039398071:
                        //this member has no preferences, verifying default preferences were automatically added
                        //preferred min/max age should be greater than 0
                        Assert.Greater(Convert.ToInt32(detailedQueryResult.Tags["preferredminage"]), 0);
                        Assert.Greater(Convert.ToInt32(detailedQueryResult.Tags["preferredmaxage"]), 0);
                        //preferred gendermask should be 9 (male seeking female) since this person is a female seeking male
                        Assert.AreEqual(9, Convert.ToInt32(detailedQueryResult.Tags["preferredgendermask"]));
                        //preferred smoking habit is not provided as part of default preferences so it should be 0 or less; any
                        Assert.LessOrEqual(Convert.ToInt32(detailedQueryResult.Tags["preferredsmokinghabit"]), 0);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }
        public void SearchJDateTestAgeRange()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("radius", "50");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "70");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6");

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            jdateSearcherBL.NUNIT_Print = true;
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            stopWatch.Stop();

            long millis = stopWatch.ElapsedMilliseconds;
            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            //Assert.Less(millis, 2500);
            printResults(searchPrefs,results, millis, jdateSearcherBL);
//            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            jdateSearcherBL.NUNIT_Print = false;
        }

        [Test]
        public void TestBLExceptionThrown()
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("regionid", "90211");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("radius", "50");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("minage", "88");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", "6");

            SearcherBL jdateSearcherBL = GetSearcherBL(3);
            try
            {
                IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(TestQueryBuilder.build(searchPrefs, 3, 103), 3);
            }
            catch (BLException e)
            {
                Assert.IsInstanceOf(typeof(ExceptionBase), e.InnerException);                
                print(e.ToString());
            }            
        }
        #endregion

        #region Test Methods - Mutual Match
        [Test]
        public void TestJdateMutualMatchCalculation()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "false");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //perfect match
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(103939806, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //1 mismatch (children count)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(103939807, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("childrencount", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //2 mismatch (children count, drinking habits)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(103939808, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("childrencount", "1");
            searchPrefs3.Add("drinkinghabits", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);
            //3 mismatch (children count, drinking habits, custody)
            SearchMember searchMember4 = SearchMember.GenerateSearchMember(103939809, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs4 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs4.Add("childrencount", "1");
            searchPrefs4.Add("drinkinghabits", "1");
            searchPrefs4.Add("custody", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember4, searchPrefs4);
            searchMembers.Add(searchMember4);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for(int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 103939806:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    default:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 0);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void TestJdateMutualMatchCalculation_Religion()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "false");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);
            searchPrefs.Add("hasjdatereligion", "256"); // (Reform)

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //match - (Jewish)
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(1039398061, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs1.Add("jdatereligion", "2047");
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //match - (Reform)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(1039398071, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("jdatereligion", "256");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //mismatch (Willing To convert)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(1039398081, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("jdatereligion", "16384");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 1039398061:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398071:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398081:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 90);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void TestJdateMutualMatchCalculation_LanguageMask()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "false");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);
            searchPrefs.Add("haslanguagemask", "1048582"); // (English, Japanese, Vietnamese)

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //match - (English)
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(1039398061, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs1.Add("languagemask", "2");
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //match - (English, Japanese)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(1039398071, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("languagemask", "1048578");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //mismatch (Korean)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(1039398081, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("languagemask", "2097152");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 1039398061:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398071:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398081:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 90);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        #endregion

        #region Test Methods - Mutual Match with FieldCache
        [Test]
        public void TestJdateMutualMatchCalculation_FieldCache()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "true");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //perfect match
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(103939806, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //1 mismatch (children count)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(103939807, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("childrencount", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //2 mismatch (children count, drinking habits)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(103939808, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("childrencount", "1");
            searchPrefs3.Add("drinkinghabits", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);
            //3 mismatch (children count, drinking habits, custody)
            SearchMember searchMember4 = SearchMember.GenerateSearchMember(103939809, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs4 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs4.Add("childrencount", "1");
            searchPrefs4.Add("drinkinghabits", "1");
            searchPrefs4.Add("custody", "1");
            TestQueryBuilder.PopulateSearchPreferences(searchMember4, searchPrefs4);
            searchMembers.Add(searchMember4);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 103939806:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    default:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 0);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void TestJdateMutualMatchCalculation_FieldCache_Religion()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "true");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);
            searchPrefs.Add("hasjdatereligion", "256"); // (Reform)

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //match - (Jewish)
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(1039398061, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs1.Add("jdatereligion", "2047");
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //match - (Reform)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(1039398071, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("jdatereligion", "256");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //mismatch (Willing To convert)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(1039398081, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("jdatereligion", "16384");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 1039398061:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398071:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398081:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 90);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void TestJdateMutualMatchCalculation_FieldCache_LanguageMask()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "true");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);
            searchPrefs.Add("haslanguagemask", "1048582"); // (English, Japanese, Vietnamese)

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //match - (English)
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(1039398061, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs1.Add("languagemask", "2");
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //match - (English, Japanese)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(1039398071, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("languagemask", "1048578");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //mismatch (Korean)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(1039398081, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("languagemask", "2097152");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);
            //match (Korean, Japanese)
            SearchMember searchMember4 = SearchMember.GenerateSearchMember(1039398091, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs4 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs4.Add("languagemask", "3145728");
            TestQueryBuilder.PopulateSearchPreferences(searchMember4, searchPrefs4);
            searchMembers.Add(searchMember4);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 1039398061:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398071:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398081:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 90);
                        break;
                    case 1039398091:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        [Test]
        public void TestJdateMutualMatchCalculation_FieldCache_Age()
        {
            int communityId = 3;
            int siteID = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);

            //set NUnit settings
            jdateSearcherBL.USE_NUNIT = true;
            jdateSearcherBL.NUNIT_Print = true;
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "true");
            _mockSettingsService.AddSetting("MATCH_RATING_DEFAULT_IMPORTANCE_LEVEL", "3");
            _mockSettingsService.AddSetting("MATCH_RATING_SEARCHER_MEMBER_PERCENTAGE", "50");
            _mockSettingsService.AddSetting("MATCH_RATING_FORMULA_VERSION", "2");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "true");

            //SEARCHER MEMBER
            //set search prefs and profile info: basic "any" prefs, sort by lastactivedate
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = GetBasicSearchPrefs(communityId, 6, true);
            searchPrefs.Add("hasbirthdate", (new DateTime(1973, 1, 1)).ToString()); // (40 years old)

            //RESULTING MEMBERS
            //set test members to index
            List<SearchMember> searchMembers = new List<SearchMember>();
            //match - (18 to 99)
            SearchMember searchMember1 = SearchMember.GenerateSearchMember(1039398061, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs1 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs1.Add("minage", "18");
            searchPrefs1.Add("maxage", "99");
            TestQueryBuilder.PopulateSearchPreferences(searchMember1, searchPrefs1);
            searchMembers.Add(searchMember1);
            //match - (40 to 40)
            SearchMember searchMember2 = SearchMember.GenerateSearchMember(1039398071, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs2 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs2.Add("minage", "40");
            searchPrefs2.Add("maxage", "40");
            TestQueryBuilder.PopulateSearchPreferences(searchMember2, searchPrefs2);
            searchMembers.Add(searchMember2);
            //mismatch (18 to 39)
            SearchMember searchMember3 = SearchMember.GenerateSearchMember(1039398081, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs3 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs3.Add("minage", "18");
            searchPrefs3.Add("maxage", "39");
            TestQueryBuilder.PopulateSearchPreferences(searchMember3, searchPrefs3);
            searchMembers.Add(searchMember3);
            //mismatch (41 to 50)
            SearchMember searchMember4 = SearchMember.GenerateSearchMember(1039398091, communityId, 6, DateTime.Parse("1987-01-22"), DateTime.Parse("2006-03-28"), DateTime.Parse("2011-03-28"), 0, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, 3443821, -2.066127, 0.594557, -1, string.Empty, 82, DateTime.Parse("2006-05-12"), -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1, 1, 0);
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs4 = GetBasicSearchPrefs(communityId, 6, false);
            searchPrefs4.Add("minage", "41");
            searchPrefs4.Add("maxage", "50");
            TestQueryBuilder.PopulateSearchPreferences(searchMember4, searchPrefs4);
            searchMembers.Add(searchMember4);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, searchMembers);

            //perform search
            //get initial results count
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(TestQueryBuilder.build(searchPrefs, communityId, siteID), communityId);
            int initialResultsCount = results.MatchesFound;
            Assert.AreEqual(searchMembers.Count, initialResultsCount);

            //verify match scores
            for (int i = 0; i < results.Items.Count; i++)
            {
                int memberID = results.Items[i].MemberID;
                switch (memberID)
                {
                    case 1039398061:
                    case 1039398071:
                        Assert.AreEqual(100, results.Items[i].MatchScore);
                        break;
                    case 1039398081:
                    case 1039398091:
                        Assert.Less(results.Items[i].MatchScore, 100);
                        Assert.Greater(results.Items[i].MatchScore, 90);
                        break;
                }
            }

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, indexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, testSearchPrefs);
        }

        #endregion

        #region Test Helper Methods
        protected string createUncPathForCopyIndex(string indexPath, int rollingDirId, string server)
        {
            string uncPath = indexPath.Replace("/", "\\").Replace(":\\\\", ":\\");
            string targetpath = "\\\\{0}\\" + uncPath.Replace(":\\", "$\\") + rollingDirId.ToString();
            return string.Format(targetpath, server);
        }

        protected void TestRadianConversion(double[] radians, double[] expectedDegrees)
        {
            double[] degrees = Utils.ConvertRadiansToDegrees(radians[0], radians[1]);
            print(string.Format("Lat/Long(Radians): {0}/{1}  -> Lat/Long(Degrees): {2}/{3}", radians[0], radians[1], degrees[0], degrees[1]), "SpatialLatLngObject");
            Assert.AreEqual(Math.Round(expectedDegrees[0], 9), Math.Round(degrees[0], 9));
            Assert.AreEqual(Math.Round(expectedDegrees[1], 9), Math.Round(degrees[1], 9));
        }

        protected Matchnet.Search.ValueObjects.SearchPreferenceCollection GetBasicSearchPrefs(int communityID, int gendermask, bool includeProfileData)
        {
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            //prefs
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", communityID.ToString());
            searchPrefs.Add("searchtype", "4"); //your matches web search
            searchPrefs.Add("searchtypeid", "1"); //zip code
            searchPrefs.Add("searchorderby", "2"); //last online (lastactivedate)
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "99");
            searchPrefs.Add("gendermask", gendermask.ToString()); //6-women seeking male, 9-male seeking female
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817"); //beverly hills 3443817
            searchPrefs.Add("geodistance", "9");
            searchPrefs.Add("regionIDCity", "3403764");
            searchPrefs.Add("longitude", "-2.066426");
            searchPrefs.Add("latitude", "0.594637");
            searchPrefs.Add("searchredesign30", "1");

            if (includeProfileData)
            {
                //profile info passed in as part of prefs, used for match calculation (if enabled)
                searchPrefs.Add(QuerySearchParam.HasMemberID.ToString().ToLower(), "411");
                searchPrefs.Add(QuerySearchParam.HasActivityLevel.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasBirthDate.ToString().ToLower(), (new DateTime(1980, 12, 12)).Ticks.ToString());
                searchPrefs.Add(QuerySearchParam.HasBodyType.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasChildrenCount.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasCustody.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasDrinkingHabits.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasEducationLevel.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasHeight.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasLanguageMask.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasMaritalStatus.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasMoreChildrenFlag.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasSynagogueAttendance.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasRelocateFlag.ToString().ToLower(), int.MinValue.ToString());
                searchPrefs.Add(QuerySearchParam.HasSmokingHabits.ToString().ToLower(), int.MinValue.ToString());

                if (communityID == 3)
                {
                    searchPrefs.Add(QuerySearchParam.HasJDateEthnicity.ToString().ToLower(), int.MinValue.ToString());
                    searchPrefs.Add(QuerySearchParam.HasJDateReligion.ToString().ToLower(), int.MinValue.ToString());
                    searchPrefs.Add(QuerySearchParam.HasKeepKosher.ToString().ToLower(), int.MinValue.ToString());
                }
                else
                {
                    searchPrefs.Add(QuerySearchParam.HasEthnicity.ToString().ToLower(), int.MinValue.ToString());
                    searchPrefs.Add(QuerySearchParam.HasReligion.ToString().ToLower(), int.MinValue.ToString());
                }
            }

            return searchPrefs;
        }
        #endregion
    }
}
