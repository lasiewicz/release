﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using NUnit.Framework;
using Spark.SearchEngine.BusinessLogic;
using Spark.SearchEngine.BusinessLogic.Documents;
using Spark.SearchEngine.BusinessLogic.Indexer;
using Spark.SearchEngine.BusinessLogic.Searcher;
using Spark.SearchEngine.ValueObjects;

namespace Spark.SearchEngine.BusinessLogic_Tests
{
    public abstract class AbstractSparkUnitTest
    {
        protected bool ENABLE_PRINT = false;
        protected Dictionary<int, SearcherBL> searcherBLs = null;
        protected Dictionary<int, List<SearchMember>> searchMembers = null;
        protected Dictionary<int, List<SearchKeyword>> searchKeywords = null;
        protected string[] nunitCommunityList = new string[] { "1", "3","10", "24", "23" };
        protected Dictionary<int, string> indexPaths = new Dictionary<int, string>();
        protected Dictionary<int, string> keywordIndexPaths = new Dictionary<int, string>();
        protected SearchPreferenceCollection testSearchPrefs = null;
        protected decimal avgSearchTime = 0;
        protected decimal searchTimes = 0;
        protected int totalSearches = 0;
        protected MockSettingsService _mockSettingsService = null;
        protected Random _random = new Random();
        protected void SearcherBL_IncrementCounters() { }
        protected void SearcherBL_DecrementCounter() { }

        protected void SearcherBL_AverageSearchTime(long searchTime)
        {
            totalSearches++;
            searchTimes += searchTime;
            avgSearchTime = searchTimes / totalSearches;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if(!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        protected void printResults(Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs, IMatchnetQueryResults results, long millis, SearcherBL searcherBL)
        {
            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
            Console.WriteLine(searcherBL.NUNIT_StringBuilder.ToString());
            searcherBL.NUNIT_StringBuilder = null;
        }

        protected void testMaskValues(List<int> values, int expectedSize, AttributeOptionCollection optionCollection)
        {
            printMaskValues(values);
            Assert.NotNull(values);
            Assert.AreEqual(expectedSize, values.Count);
            foreach (AttributeOption option in optionCollection)
            {
                if (option.Value > 0)
                {
                    Assert.IsTrue(values.Contains(option.Value), string.Format("{0} was not in option collection!", option.Value));
                }
            }
        }

        protected void printMaskValues(List<int> values)
        {
            if (ENABLE_PRINT)
            {
                Console.WriteLine(string.Format("---------VALUES SIZE {0}---------", values.Count));
                foreach (int value in values)
                {
                    Console.WriteLine(string.Format("Value: {0}", value));
                }
            }
        }

        protected SearcherBL GetSearcherBL(int communityId)
        {
            if (searcherBLs.ContainsKey(communityId))
                return searcherBLs[communityId];
            else
                return null;
        }

        protected void RemoveSearcherBL(int communityID)
        {
            SearcherBL searcherBL = GetSearcherBL(communityID);
            if (searcherBL != null)
            {
                searcherBL.Stop();
                searcherBLs.Remove(communityID);
            }
        }

        protected SearcherBL CreateSearcherBL(int communityID, Dictionary<int, string> indexPaths, string[] nunitCommunityList, SearcherBL.SearcherAddSearchEventHandler addSearchEventHandler, SearcherBL.SearcherRemoveSearchEventHandler removeSearchEventHandler, SearcherBL.SearcherAverageSearchEventHandler averageSearchEventHandler)
        {
            SearcherBL searcherBL = SearcherBL.CreateSearcherBL();
            searcherBL.USE_NUNIT = true;
            searcherBL.NUNIT_UseNRT = true;
            if (null != indexPaths)
            {
                searcherBL.NUNIT_IndexPaths = indexPaths;
            }
            if (null != keywordIndexPaths)
            {
                searcherBL.NUNIT_KeywordIndexPaths = keywordIndexPaths;
            }
            searcherBL.NUNIT_Community_List = nunitCommunityList;
            searcherBL.SearcherAddSearch += new SearcherBL.SearcherAddSearchEventHandler(addSearchEventHandler);
            searcherBL.SearcherRemoveSearch += new SearcherBL.SearcherRemoveSearchEventHandler(removeSearchEventHandler);
            searcherBL.SearcherAverageSearch += new SearcherBL.SearcherAverageSearchEventHandler(averageSearchEventHandler);
            switch (communityID)
            {
                case 1:
                    searcherBL.Start("Spark");
                    break;
                case 3:
                    searcherBL.Start("Jdate");
                    break;
                case 10:
                    searcherBL.Start("Cupid");
                    break;
                case 21:
                    searcherBL.Start("ItalianSingles");
                    break;
                case 23:
                    searcherBL.Start("BBWPersonalsPlus");
                    break;
                case 24:
                    searcherBL.Start("BlackSingles");
                    break;
                default:
                    break;
            }
            return searcherBL;
        }

        protected void TestAccuracy(Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs, int communityId, int siteId, int[] expectedMemberIds, SearcherBL searcherBL)
        {
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);

            IMatchnetQueryResults results = searcherBL.RunDetailedQuery(matchnetQuery, communityId);

            for (int i = 0; i < results.Items.Count; i++)
            {
                DetailedQueryResult result = results.Items[i] as DetailedQueryResult;
                print(string.Format("Memberid {0}:", result.MemberID));
                foreach (string key in result.Tags.Keys)
                {
                    print(string.Format("\tField \"{0}\", Value \"{1}\"", key, result.Tags[key]));
                }
            }

            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);

            foreach (int memberId in expectedMemberIds)
            {
                bool success = false;
                foreach (IMatchnetResultItem resultItem in results.Items.List)
                {
                    if (resultItem.MemberID == memberId)
                    {
                        success = true;
                    }
                }
                Assert.IsTrue(success, string.Format("{0} is not in Results!!", memberId));
            }
            Assert.AreEqual(expectedMemberIds.Length, results.Items.Count);
        }

        protected DataTable CreateKeywordDataTable(string tableName)
        {
            DataTable dataTable = new DataTable(tableName);
            //columns for search
            dataTable.Columns.Add("memberid", typeof(int));
            dataTable.Columns.Add("communityid", typeof(int));
            dataTable.Columns.Add("gendermask", typeof(int));
            dataTable.Columns.Add("lastactivedate", typeof(DateTime));
            dataTable.Columns.Add("updatedate", typeof(DateTime));
            dataTable.Columns.Add("depth1regionid", typeof(int));
            dataTable.Columns.Add("depth2regionid", typeof(int));
            dataTable.Columns.Add("depth3regionid", typeof(int));
            dataTable.Columns.Add("depth4regionid", typeof(int));
            dataTable.Columns.Add("longitude", typeof(double));
            dataTable.Columns.Add("latitude", typeof(double));
            dataTable.Columns.Add("areacode", typeof(string));
            //columns for keyword search
            dataTable.Columns.Add("aboutme", typeof(string));
            dataTable.Columns.Add("perfectmatchessay", typeof(string));
            dataTable.Columns.Add("perfectfirstdateessay", typeof(string));
            dataTable.Columns.Add("idealrelationshipessay", typeof(string));
            dataTable.Columns.Add("learnfromthepastessay", typeof(string));
            dataTable.Columns.Add("lifeandabmitionessay", typeof(string));
            dataTable.Columns.Add("historyofmylifeessay", typeof(string));
            dataTable.Columns.Add("onourfirstdateremindmetoessay", typeof(string));
            dataTable.Columns.Add("thingscantlivewithoutessay", typeof(string));
            dataTable.Columns.Add("favoritebooksmoviesetcessay", typeof(string));
            dataTable.Columns.Add("coolestplacesvisitedessay", typeof(string));
            dataTable.Columns.Add("forfuniliketoessay", typeof(string));
            dataTable.Columns.Add("onfrisatitypicallyessay", typeof(string));
            dataTable.Columns.Add("messagemeifyouessay", typeof(string));
            dataTable.Columns.Add("cuisine", typeof(string));
            dataTable.Columns.Add("entertainmentlocation", typeof(string));
            dataTable.Columns.Add("leisureactivity", typeof(string));
            dataTable.Columns.Add("music", typeof(string));
            dataTable.Columns.Add("personalitytrait", typeof(string));
            dataTable.Columns.Add("pets", typeof(string));
            dataTable.Columns.Add("physicalactivity", typeof(string));
            dataTable.Columns.Add("reading", typeof(string));
            dataTable.Columns.Add("essayfirstdate", typeof(string));
            dataTable.Columns.Add("essaygettoknowme", typeof(string));
            dataTable.Columns.Add("essaypastrelationship", typeof(string));
            dataTable.Columns.Add("essayimportantthings", typeof(string));
            dataTable.Columns.Add("essayperfectday", typeof(string));
            dataTable.Columns.Add("essaymorethingstoadd", typeof(string));
            dataTable.Columns.Add("favoritebands", typeof(string));
            dataTable.Columns.Add("favoritemovies", typeof(string));
            dataTable.Columns.Add("favoritetv", typeof(string));
            dataTable.Columns.Add("favoriterestaurant", typeof(string));
            dataTable.Columns.Add("schoolsattended", typeof(string));
            dataTable.Columns.Add("mygreattripidea", typeof(string));
            dataTable.Columns.Add("moviegenre", typeof(string));
            return dataTable;
        }

        protected DataTable CreateDataTable(string tableName)
        {
            DataTable dataTable = new DataTable(tableName);
            //columns for search
            dataTable.Columns.Add("memberid", typeof (int));
            dataTable.Columns.Add("communityid", typeof (int));
            dataTable.Columns.Add("gendermask", typeof (int));
            dataTable.Columns.Add("birthdate", typeof (DateTime));
            dataTable.Columns.Add("communityinsertdate", typeof (DateTime));
            dataTable.Columns.Add("lastactivedate", typeof(DateTime));
            dataTable.Columns.Add("hasphotoflag", typeof (int));
            dataTable.Columns.Add("educationlevel", typeof (int));
            dataTable.Columns.Add("religion", typeof (int));
            dataTable.Columns.Add("languagemask", typeof (int));
            dataTable.Columns.Add("ethnicity", typeof (int));
            dataTable.Columns.Add("smokinghabits", typeof (int));
            dataTable.Columns.Add("drinkinghabits", typeof (int));
            dataTable.Columns.Add("height", typeof (int));
            dataTable.Columns.Add("maritalstatus", typeof (int));
            dataTable.Columns.Add("jdatereligion", typeof (int));
            dataTable.Columns.Add("jdateethnicity", typeof (int));
            dataTable.Columns.Add("synagogueattendance", typeof (int));
            dataTable.Columns.Add("keepkosher", typeof (int));
            dataTable.Columns.Add("sexualidentitytype", typeof (int));
            dataTable.Columns.Add("relationshipmask", typeof (int));
            dataTable.Columns.Add("relationshipstatus", typeof (int));
            dataTable.Columns.Add("bodytype", typeof (int));
            dataTable.Columns.Add("zodiac", typeof (int));
            dataTable.Columns.Add("majortype", typeof (int));
            dataTable.Columns.Add("depth1regionid", typeof (int));
            dataTable.Columns.Add("depth2regionid", typeof (int));
            dataTable.Columns.Add("depth3regionid", typeof (int));
            dataTable.Columns.Add("depth4regionid", typeof (int));
            dataTable.Columns.Add("longitude", typeof (double));
            dataTable.Columns.Add("latitude", typeof (double));
            dataTable.Columns.Add("schoolid", typeof (int));
            dataTable.Columns.Add("areacode", typeof (string));
            dataTable.Columns.Add("emailcount", typeof (int));
            dataTable.Columns.Add("updatedate", typeof (DateTime));
            dataTable.Columns.Add("weight", typeof (int));
            dataTable.Columns.Add("relocateflag", typeof (int));
            dataTable.Columns.Add("childrencount", typeof (int));
            dataTable.Columns.Add("subscriptionexpirationdate", typeof (DateTime));
            dataTable.Columns.Add("activitylevel", typeof (int));
            dataTable.Columns.Add("custody", typeof (int));
            dataTable.Columns.Add("morechildrenflag", typeof (int));
            dataTable.Columns.Add("colorcode", typeof (int));

            //columns for reverse search
            dataTable.Columns.Add("preferredgendermask", typeof(int));
            dataTable.Columns.Add("preferredrelocation", typeof(int));
            dataTable.Columns.Add("preferredminheight", typeof (int));
            dataTable.Columns.Add("preferredmaxheight", typeof (int));
            dataTable.Columns.Add("preferredminage", typeof (int));
            dataTable.Columns.Add("preferredmaxage", typeof (int));
            dataTable.Columns.Add("preferredsynagogueattendance", typeof (int));
            dataTable.Columns.Add("preferredjdatereligion", typeof (int));
            dataTable.Columns.Add("preferredsmokinghabit", typeof (int));
            dataTable.Columns.Add("preferreddrinkinghabit", typeof (int));
            dataTable.Columns.Add("preferredjdateethnicity", typeof (int));
            dataTable.Columns.Add("preferredactivitylevel", typeof (int));
            dataTable.Columns.Add("preferredkosherstatus", typeof (int));
            dataTable.Columns.Add("preferredmaritalstatus", typeof (int));
            dataTable.Columns.Add("preferredlanguagemask", typeof (int));
            dataTable.Columns.Add("preferreddistance", typeof (int));
            dataTable.Columns.Add("preferredregionid", typeof (int));
            dataTable.Columns.Add("preferredareacodes", typeof (string));
            dataTable.Columns.Add("preferredhasphotoflag", typeof (int));
            dataTable.Columns.Add("preferrededucationlevel", typeof (int));
            dataTable.Columns.Add("preferredreligion", typeof (int));
            dataTable.Columns.Add("preferredethnicity", typeof (int));
            dataTable.Columns.Add("preferredsexualidentitytype", typeof (int));
            dataTable.Columns.Add("preferredrelationshipmask", typeof (int));
            dataTable.Columns.Add("preferredrelationshipstatus", typeof (int));
            dataTable.Columns.Add("preferredbodytype", typeof (int));
            dataTable.Columns.Add("preferredzodiac", typeof (int));
            dataTable.Columns.Add("preferredmajortype", typeof (int));
            dataTable.Columns.Add("preferredmorechildrenflag", typeof (int));
            dataTable.Columns.Add("preferredchildrencount", typeof (int));

            return dataTable;
        }

        protected DataRow CreateDataRow(DataTable dt, object[] itemArray)
        {
            DataRow dataRow = dt.NewRow();
            dataRow.ItemArray = itemArray;
            return dataRow;
        }

        protected void CreateTestSearchMember()
        {
            searchKeywords = new Dictionary<int, List<SearchKeyword>>();
            DataTable dataTable1 = CreateKeywordDataTable("sparkKeywords");
            List<SearchKeyword> sparkSearchKeywords = new List<SearchKeyword>();
            sparkSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  103939806, 1,6, DateTime.Now, DateTime.Parse("2008-07-26"), 38, 351,
                                                                  2643909, 3349830, -1.321973, 0.793262, "819",
                                                                  "Kind, considerate, sincere, truthful, polite, shy, playful, flirty, confident, strong minded, bashful, talented, flaky, athletic, adventurous, musical, a thinker, independent.",
                                                                  "Someone kind, straight forward, enthusiastic, daring, considerate, and loyal who can knock me back a peg when I think I know more than I do, but can listen and relate when I have strong feelings about something - someone who can help me sort things out when I'm deciding what is needed."
                                                              })));
            sparkSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  103820222, 1,6, DateTime.Now, DateTime.Parse("2008-07-26"),223, 1577,
                                                                  3432824, 3475342, -1.722455, 0.592492, "940",
                                                                  "Enjoying a great stage of life...  I really do love the sun & sand.... music with a beat gets my hands clapping and my feet moving! I enjoy a good novel and a great D&I movie with a who-done-it flavor! Hiking in the woods? Well, OK... but only with the right person. I'm a good conversationalist and I'd like to think I can see both sides of an issue.  Having learned a bit from past experiences, I look forward to whatever the future holds! Love my friends and family, but looking for that special someone to share my life with.",
                                                                  "Would love to spend time with a person who has a passion for life, is caring, considerate and comfortable to be with. An individual with solid convictions and the ability to respect the point of view of others. Someone who has experienced life's betrayals and has emerged stronger, wiser and better for them. Looks have never been the primary criteria for me, the warmth of spirit and soul, honor and integrity are much more important. Family is an important part of my life, which I would love to share."
                                                              })));
            sparkSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  23247846, 1, 6,DateTime.Now, DateTime.Parse("2008-06-26"),223, 1538,
                                                                  3403764, 3443821, -2.066426, 0.594637, string.Empty,
                                                                  "Hi There! I'm a busy professional Man and while I am blessed with wonderful friends and a full and happy life, I am now ready and looking for someone special to share it with. I am finding that in today's hectic, busy world, it's not as easy as it used to be to meet a great single Woman! But I know you're out there!!! So here I am willing to give this a try. So here goes...",
                                                                  "I'm searching for a one man woman who is ready to share the rest of her life with me, no games cause life is to short not to enjoy it to the fullest.. I'm looking for a woman who will love and understand me for who i am not what i have...."
                                                              })));
            sparkSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  988270506, 1, 6,DateTime.Now, DateTime.Parse("2008-06-26"),223, 1538,
                                                                  3403764, 3443821, -2.066426, 0.594637, string.Empty,
                                                                  "I love people and get along with most, never afraid to make new friends. I have a good sense of humor, a little sarcastic sometimes maybe but in a humorous way really only, the ironies in life kill me and I laugh best when I see them in my own life. I would describe myself as a little rough around the edges it’s hard to shock me.\n\nPositive, happy person who's honest, caring and interested in dating and maybe more...I find no matter what happens in life, it's always better with a smile and laughter. I like all types of music, dancing, the beach, traveling, dining out/in and spending time with friends. My morals and values are very important to me, if you are just a player, don't even think about it - you're wasting your time. I believe that romance is not dead. I feel that laughter and friendship are vital to any relationship and that holding hands or glances across a table can be as sensual as any other form of intimacy. Dating after 11 years of marriage is a whole new adventure. One I am embracing with enthusiasm.I am enjoying life now more, perhaps than I have since I was in my 20's.",
                                                                  "I am looking for soul mate . Someone who has the locks to fit my keys, and the keys to fit my locks. When we feel safe enough to open the locks, ourselves trust will step out and we can be completely and honestly loved for who we are. Each of us will unveil the best part of each other. A soul mate is someone who shares our deepest longings, our sense of direction. That's what I am looking for...I know somehow, somewhere that person exists. After all, there is someone for everyone. So, that's me in a nutshell."
                                                              })));
            sparkSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  988271920, 1, 9,DateTime.Now, DateTime.Parse("2008-06-06"), 223, 1538, 3404470,
                                                                  3444750, -2.064637, 0.594102, string.Empty,
                                                                  "I love travel,quality conversation,fun loving seeking same. Widow 7 years. My family is very important to me. I am very active and in great health. I would like to talk to individuals that have the same music interest as I",
                                                                  "clean cut individual, HONEST, fun loving with a great sense of humor. An individual who is open minded and comes from a nice family."
                                                              })));
            searchKeywords.Add(1, sparkSearchKeywords);

            List<SearchKeyword> jdateSearchKeywords = new List<SearchKeyword>();
            jdateSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  100000130, 3, 6,DateTime.Now, DateTime.Parse("2008-05-17"),223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, "   ",
                                                                  "Travel Junky I am a quiet person who enjoys good company good food and having a good time. I dislike Jewish princeses country",
                                                                  "כדורעף רולר בליידס / סקייטבורד יוגה/מדיטציה  בעלי החיים האהובים עלי ציפור אוגר חתול ארנבת כלב חיות אחרות דג חיות זה לא אני זוחלים  יחסים וזוגיות"
                                                              })));
            jdateSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  100000534, 3, 6,DateTime.Now,DateTime.Parse("2008-05-17"), 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, "   ",
                                                                  "I am very energetic and active. I travel a lot and have been and lived in other places. I enjoy getting out and doing things, music whether it be a street fest, golf, boating, or just getting out and being a part of the world. I am always looking for new adventures. Honesty is very important to me and I expect it from others. country I am direct and usually to the point but in a witty and charming way. I love to laugh and make others laugh despite my sarcastic sense of humor which only 1 in 10 get. I love good food. I am always in search of a nice and cozy chair. I don't like doing things half way, I either do it or I don't. When I go to the movies I like to sit in the middle so I can be music in the center of it all.",
                                                                  "I think the first date better be something fun! I don't want to sit across the table and go through the list of usual questions in a crowded place. An interesting place surrounded with tons of things to stimulate the mind. I don't want to hear music myself talk the whole time and I certainly don't want to spend it listening either. junky"
                                                              })));
            jdateSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  100001140, 3, 6,DateTime.Now, DateTime.Parse("2008-05-17"),83, 3483311, 8294167, 0,
                                                                  0.163605, 0.897293, "   " ,
                                                                  "נשית ,  חייכנית, רזה, חמה תוססת  + 3 ילדים מקסימים. לא מעשנת, צמחונית. עצמאית ולא תלותית, חברותית, סקרנית ורחבת אופקים. מעריכה נדיבות אדיבות ופירגון, ומנסה לנהוג כך בעצמי... מחפשת קשר חם מהנה ואוהב, וזוגיות לא חונקת...",
                                                                  "לדבר ולהכיר, לא לזייף, למצוא שפה משותפת, בלי לחץ ובלי ציפיות מוגזמות"
                                                              })));
            jdateSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  100001342, 3, 9,DateTime.Now, DateTime.Parse("2008-05-17"),105, 3484024, 9801115, 0
                                                                  , 0.610846, 0.572681, "   ",
                                                                  "I was born in the good old and long defunct USSR. I'm from the part that is now called Ukraine and I get ticked off when people call me a Russian even though I sometimes do it myself. I'm Jewish first, American second, and Ukrainian a more and more distant third. \n\nI'm a pretty well rounded guy. A jack of all trades so to speak. I can fix your computer, play on the piano and tell you who won the last World Series of Baseball. I keep up with the latest news stories on Fox or CNN just as much as I do with the lates episodes of Law & Order (I loved the original). I've recently discovered ballet, and I think I'd like to go see more of it, but if you have tickets to a Bon Jovi concert I'll be just as happy to go see that instead. \n\nI'm looking for a like minded, well rounded woman who can enjoy all the same things I can enjoy. Is willing to explore new things with me as well as show me all kinds of things I've been missing. Doesn't matter to me if you're Russian, Ukrainian, American or anywhere in between. If you're a good hearted person I'd like to get to know you.",
                                                                  "The last first date I go on will be my perfect first date..... Seriously though, as long as there is chemistry, laughing, and no awkward silences, it's good. country music"
                                                              })));
            jdateSearchKeywords.Add(
                new SearchKeyword().Populate(CreateDataRow(dataTable1,
                                                          new object[]
                                                              {
                                                                  100001746, 3, 9,DateTime.Now, DateTime.Parse("2008-05-17"),223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, "   ",
                                                                  "I'm a Croatian-Brazilian Jewish woman who was blessed to grow up in a very loving home with my birth family surrounded by love and affection. Femininity surrounds the core of my pores and I like to be treated like a lady with all pampering that only a gentleman can do for me. I love to have fun, laugh, and do simple things in life that can be more exciting than outrageously indulgence in expensive things that may bring nothing interesting to remember. Being in a lavishly date is very exciting, but the quality of remembering who the person is more important to me.",
                                                                  "travel This is difficult to express...but, I love to be pampered with gentleness and flowers."
                                                              })));            
            searchKeywords.Add(3, jdateSearchKeywords);

            //search members for each community for testing accuracy
            searchMembers = new Dictionary<int, List<SearchMember>>();
            DataTable dataTable = CreateDataTable("sparkUsers");
            List<SearchMember> sparkSearchMembers = new List<SearchMember>();
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  103939806, 1, 6, DateTime.Parse("1949-08-26"),
                                                                  DateTime.Parse("2006-03-28"), DateTime.Now, 1, 128, 8, 10, 2, 2,
                                                                  4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 38, 351,
                                                                  2643909, 3349830, -1.321973, 0.793262, -1, "819", 82,
                                                                  DateTime.Parse("2006-05-12"), -1, -1, -1,
                                                                  DateTime.Parse("2008-07-26"), -1, -1, -1, -1,
                                                                  9,int.MinValue,int.MinValue,int.MinValue,20,25
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  103820222, 1, 6, DateTime.Parse("1987-01-22"),
                                                                  DateTime.Parse("2006-03-28"), DateTime.Now, 0, 8, 8, 2, 256, 2, 2
                                                                  , 168, 2, -21474, 0, 0, 0, 0, 4, 0, 64, -1, -1, 223, 1577,
                                                                  3432824, 3475342, -1.722455, 0.592492, -1, "940", 0,
                                                                  DateTime.Parse("2006-05-12"), -1, -1, -1,
                                                                  DateTime.Parse("2008-07-26"), -1, -1, -1, -1,
                                                                  9,int.MinValue,int.MinValue,int.MinValue,25,26
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  23247846, 1, 6, DateTime.Now.AddYears(-37),
                                                                  DateTime.Parse("2003-06-12"), DateTime.Now, 0, 128, 131072, 2, 2,
                                                                  4, 4, -21474, 8, 0, 0, 0, 0, 0, 128, 0, 8, 65536, 0, 223, 1538,
                                                                  3403764, 3443821, -2.066426, 0.594637, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-07"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-26"), -1, -1, -1, -1,
                                                                  9,int.MinValue,int.MinValue,int.MinValue,20,20
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  988270506, 1, 6, DateTime.Parse("1985-01-01"),
                                                                  DateTime.Parse("2003-06-12"), DateTime.Now, 0, 128, 131072, 2, 2,
                                                                  4, 4, -21474, 8, 0, 0, 0, 0, 0, 128, 0, 8, 65536, 0, 223, 1538,
                                                                  3403764, 3443821, -2.066426, 0.594637, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-07"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-26"), -1, -1, -1, -1,
                                                                  9,int.MinValue,int.MinValue,int.MinValue,18,35
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  988271920, 1, 6, DateTime.Parse("1968-08-05"),
                                                                  DateTime.Parse("2004-09-24"), DateTime.Now, 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3404470,
                                                                  3444750, -2.064637, 0.594102, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-06"), -1, -1, -1, -1,
                                                                  9,int.MinValue,int.MinValue,int.MinValue,40,65
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104624112, 1, 6, DateTime.Parse("1982-03-20"),
                                                                  DateTime.Parse("2006-04-29"), DateTime.Now, 0, 8, 8, 2, 2, 2, 2,
                                                                  175, 2, -21474, 0, 0, 0, 0, 260, 0, 4, -1, -1, 223, 1538, 3404902
                                                                  , 3445596, -2.044403, 0.571994, -1, "858", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104728344, 1, 6, DateTime.Parse("1967-06-06"),
                                                                  DateTime.Parse("2006-05-05"), DateTime.Now, 1, 32, 8192, 32866, 2
                                                                  , 4, 8, 160, 8, -21474, 0, 0, 0, 0, 44, 0, 4, -1, -1, 59, 0,
                                                                  9623662, 0, 0.252491, 0.878482, -1, "   ", 7,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104711376, 1, 9, DateTime.Parse("1986-02-24"),
                                                                  DateTime.Parse("2006-05-04"), DateTime.Now, 0, 8, 4, 2, 2, 2, 4,
                                                                  203, 2, -21474, 0, 0, 0, 0, 32, 0, 8, -1, -1, 223, 1569, 3428895,
                                                                  3470883, -1.477072, 0.703445, -1, "419", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1,
                                                                  6,-1,-1,-1,18,45
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104851584, 1, 9, DateTime.Parse("1978-11-28"),
                                                                  DateTime.Parse("2006-05-11"), DateTime.Now, 0, 8, 524288, 2, 2, 8
                                                                  , 4, 183, 16, -21474, 0, 0, 0, 0, 128, 0, 4, -1, -1, 223, 1548,
                                                                  3412399, 3454241, -1.500482, 0.727319, -1, "574", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-06"), -1, -1, -1, -1,
                                                                  6,-1,-1,-1,50,65
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104653200, 1, 9, DateTime.Parse("1974-02-11"),
                                                                  DateTime.Parse("2006-05-01"), DateTime.Now, 0, 16, 524288,
                                                                  1048594, 4, 2, 4, 175, 16, -21474, 0, 0, 0, 0, 6, 0, 4, -1, -1,
                                                                  138, 3484682, 7808737, 0, -1.732239, 0.330158, -1, "   ", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100266787, 1, 9, DateTime.Parse("1980-02-26"),
                                                                  DateTime.Parse("2008-11-17"), DateTime.Now, 0, 2, 1048576, 262146
                                                                  , 32, 2, 2, 145, 2, -21474, 0, 0, 0, 0, 4, 0, 2048, -1, -1, 38,
                                                                  345, 2637727, 2756444, -0.920449, 0.830218, -1, "709", 0,
                                                                  DateTime.Parse("2009-05-18"), 44453, 2, 0,
                                                                  DateTime.Parse("2008-12-17"), 8, 8, 1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104605800, 1, 9, DateTime.Parse("1988-04-13"),
                                                                  DateTime.Parse("2006-04-28"), DateTime.Now, 1, 4, 16, 18, 2, 2, 2
                                                                  , 190, 2, -21474, 0, 0, 0, 0, 486, 0, 4, -1, -1, 223, 1557,
                                                                  3417641, 3459763, -1.671776, 0.775721, -1, "507", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-07-26"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  103946169, 1, 9, DateTime.Parse("1961-09-05"),
                                                                  DateTime.Parse("2006-03-28"), DateTime.Now, 0, 4, 4, 2, 2, 4, 4,
                                                                  175, 8, -21474, 0, 0, 0, 0, 488, 0, 16, -1, -1, 223, 1544,
                                                                  3405752, 3447276, -1.488526, 0.605807, -1, "706", 2,
                                                                  DateTime.Parse("2006-05-12"), -1, -1, -1,
                                                                  DateTime.Parse("2008-07-26"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100005220, 1, 9, DateTime.Parse("1967-01-27"),
                                                                  DateTime.Parse("2005-05-06"), DateTime.Now, 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3404917,
                                                                  3445798, -2.106563, 0.615565, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-26"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100042792, 1, 9, DateTime.Parse("1980-01-01"),
                                                                  DateTime.Parse("2005-11-02"), DateTime.Now, 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-26"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100037338, 1, 9, DateTime.Parse("1977-01-02"),
                                                                  DateTime.Parse("2005-09-08"), DateTime.Now, 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3404470,
                                                                  3444721, -2.064637, 0.594102, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-26"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  23247826, 1, 9, DateTime.Parse("1959-12-03"),
                                                                  DateTime.Parse("2003-06-12"), DateTime.Now, 0, 0, 0, 2, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3405393,
                                                                  3446718, -2.067338, 0.594170, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-07"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-06"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  23248028, 1, 9, DateTime.Parse("1984-01-01"),
                                                                  DateTime.Parse("2003-06-18"), DateTime.Now, 0, 0, 0, 2, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, string.Empty, 0,
                                                                  DateTime.Parse("2006-02-07"), -1, -1, -1,
                                                                  DateTime.Parse("2008-06-06"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104587752, 1, 9, DateTime.Parse("1972-01-19"),
                                                                  DateTime.Parse("2006-04-27"), DateTime.Now, 0, 32, 524288, 2, 2,
                                                                  4, 4, 178, 2, -21474, 0, 0, 0, 0, 450, 0, 2, -1, -1, 223, 1569,
                                                                  3428028, 3470090, -1.425424, 0.723780, -1, "330", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104689560, 1, 9, DateTime.Parse("1950-05-30"),
                                                                  DateTime.Parse("2006-05-03"), DateTime.Now, 1, 16, 8192, 34, 2, 8
                                                                  , 4, 178, 8, -21474, 0, 0, 0, 0, 168, 0, 4, -1, -1, 223, 1538,
                                                                  3404947, 3445906, -2.141592, 0.671266, -1, "707", 4,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104604720, 1, 10, DateTime.Parse("1969-09-12"),
                                                                  DateTime.Parse("2006-04-28"), DateTime.Now, 0, 32, 8, 18, 4, 2, 8
                                                                  , 170, 8, -21474, 0, 0, 0, 0, 422, 0, 64, -1, -1, 223, 1538,
                                                                  3404174, 3444324, -2.090760, 0.641525, -1, "559", 1,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            sparkSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  104769552, 1, 10, DateTime.Parse("1985-05-12"),
                                                                  DateTime.Parse("2006-05-07"), DateTime.Now, 0, 8, 8192, 2, 16, 2,
                                                                  4, 170, 2, -21474, 0, 0, 0, 0, 486, 0, 4, -1, -1, 19, 3482815,
                                                                  7954277, 0, -1.038762, 0.232711, -1, "   ", 0,
                                                                  DateTime.Parse("2006-05-15"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            searchMembers.Add(1, sparkSearchMembers);

            List<SearchMember> jdateSearchMembers = new List<SearchMember>();
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100000130, 3, 6, DateTime.Parse("1985-01-01"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, 2, 2, 2, 16, 2, 2,
                                                                  -21474, 8, 0, 0, 0, 0, 0, 0, 0, 2, 16, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1,
                                                                  9,-1,-1,-1,20,25
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100000534, 3, 6, DateTime.Parse("2005-03-08"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1,
                                                                  9,-1,-1,-1,25,26
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100001140, 3, 6, DateTime.Parse("1951-07-10"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 3483311, 8294167, 0,
                                                                  0.163605, 0.897293, 0, "   ", 0, DateTime.Parse("2006-02-09"), -1
                                                                  , -1, -1, DateTime.Parse("2008-05-17"), -1, -1, -1, -1,
                                                                  9,-1,-1,-1,20,20
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100001342, 3, 9, DateTime.Parse("1975-05-02"),
                                                                  DateTime.Parse("2005-04-19"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 3484024, 9801115, 0
                                                                  , 0.610846, 0.572681, 0, "   ", 0, DateTime.Parse("2006-02-09"),
                                                                  -1, -1, -1, DateTime.Parse("2008-05-17"), -1, -1, -1, -1,
                                                                  6,-1,-1,-1,18,35
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100001746, 3, 9, DateTime.Parse("1980-11-15"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1,
                                                                  6,-1,-1,-1,40,65
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100001948, 3, 9, DateTime.Parse("1986-02-11"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-05-17"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  988271570, 3, 6, DateTime.Parse("1977-03-28"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, 16, 4194304, 2, 2,
                                                                  2, 4, -21474, 2, 0, 0, 0, 0, 0, 0, 0, 4, 16, 0, 223, 1538,
                                                                  3403764, 3443818, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-04-20"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  988273388, 3, 9, DateTime.Parse("1970-01-01"),
                                                                  DateTime.Parse("2005-08-22"), DateTime.Now.AddDays(-25), 0, 8, 131072, 82, 256
                                                                  , 4, 4, -21474, 8, 0, 0, 0, 0, 4, 0, 0, 16, 32, 0, 223, 1538,
                                                                  3403764, 3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-04-20"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100000305, 3, 6, DateTime.Parse("1982-06-06"),
                                                                  DateTime.Parse("2005-02-09"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 2, 2,
                                                                  -21474, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-04-20"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100000507, 3, 9, DateTime.Parse("1980-01-01"),
                                                                  DateTime.Parse("2005-03-02"), DateTime.Now.AddDays(-25), 0, 2, 0, 0, 0, 2, 2,
                                                                  -21474, 2, 1, 1, 1, 1, 0, 128, 0, 0, 0, 0, 223, 1577, 3433808,
                                                                  3476314, -1.719287, 0.514347, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-04-20"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100001719, 3, 6, DateTime.Parse("1972-11-16"),
                                                                  DateTime.Parse("2005-04-19"), DateTime.Now.AddDays(-25), 0, 0, 0, 0, 0, 0, 0,
                                                                  -21474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 223, 1538, 3403764,
                                                                  3443817, -2.066426, 0.594637, 0, "   ", 0,
                                                                  DateTime.Parse("2006-02-09"), -1, -1, -1,
                                                                  DateTime.Parse("2008-04-20"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100019899, 3, 9, DateTime.Parse("1980-02-01"),
                                                                  DateTime.Parse("2005-05-10"), DateTime.Now.AddDays(-25), 0, 2, 0, 0, 0, 2, 2,
                                                                  -21474, 2, 1, 16, 1, 1, 0, 4, 0, 0, 0, 0, 105, 3484014, 9801273,
                                                                  0, 0.608183, 0.563091, 0, "   ", 0, DateTime.Parse("2006-02-09"),
                                                                  -1, -1, -1, DateTime.Parse("2008-04-20"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100003656, 3, 6, DateTime.Parse("1991-01-01"),
                                                                  DateTime.Parse("2009-03-23"), DateTime.Now.AddDays(-25), 0, 8, -1, -1, -1, 4,
                                                                  16, 0, 2, 8, 16, 32, 4, 0, 8, 0, -1, -1, -1, 105, 3484014,
                                                                  9801147, 0, 0.609682, 0.569610, -1, "9  ", 5,
                                                                  DateTime.Parse("2009-05-07"), 0, -1, 0,
                                                                  DateTime.Parse("2009-04-24"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100066456, 3, 6, DateTime.Parse("1973-02-03"),
                                                                  DateTime.Parse("2007-08-23"), DateTime.Now.AddDays(-25), 0, 2, -1, -1, -1, 4,
                                                                  8, 0, 2, 1, 1, 1, 1, 0, 16, 0, -1, -1, -1, 223, 1538, 3404462,
                                                                  3444696, -2.062687, 0.589861, -1, "562", 0,
                                                                  DateTime.Parse("2007-08-23"), 0, -1, 0,
                                                                  DateTime.Parse("2008-09-15"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100067398, 3, 9, DateTime.Parse("1977-05-04"),
                                                                  DateTime.Parse("2008-03-28"), DateTime.Now.AddDays(-25), 0, 4, -1, -1, -1, 2,
                                                                  2, 0, 2, 2, 16, 1, 1, 0, 34, 0, -1, -1, -1, 105, 3484027, 9801694
                                                                  , 0, 0.610012, 0.515837, -1, "8  ", 0,
                                                                  DateTime.Parse("2008-03-28"), 0, -1, 0,
                                                                  DateTime.Parse("2008-09-15"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100066927, 3, 9, DateTime.Parse("1977-01-01"),
                                                                  DateTime.Parse("2007-11-14"), DateTime.Now.AddDays(-25), 1, 16, -1, -1, -1, 2,
                                                                  2, 0, 2, 256, 2, 1, 8, 0, 2, 0, -1, -1, -1, 223, 1578, 3437511,
                                                                  3480020, -1.954226, 0.708730, -1, "801", 0,
                                                                  DateTime.Parse("2008-04-02"), 0, -1, 0,
                                                                  DateTime.Parse("2008-09-15"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100001772, 3, 6, DateTime.Parse("1981-08-03"),
                                                                  DateTime.Parse("2005-04-19"), DateTime.Now.AddDays(-25), 0, -1, -1, -1, -1, 0,
                                                                  0, 0, 0, -21474, 0, 0, 0, 0, -1, 0, -1, -1, -1, 223, 1538,
                                                                  3403764, 3443817, -2.066426, 0.594637, -1, "424", 0,
                                                                  DateTime.Parse("2008-08-14"), 0, -1, 0,
                                                                  DateTime.Parse("2011-04-01"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  108181629, 3, 9, DateTime.Parse("1958-03-06"),
                                                                  DateTime.Parse("2009-09-22"), DateTime.Now.AddDays(-25), 0, 128, -1, -1, -1, 8
                                                                  , 8, 0, 2, 4, 4, 16, 2, 0, 2, 0, -1, -1, -1, 223, 1538, 3403764,
                                                                  3443821, -2.066426, 0.594637, -1, "424", 0,
                                                                  DateTime.Parse("2009-09-22"), 0, -1, 0,
                                                                  DateTime.Parse("2011-04-01"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100000516, 3, 6, DateTime.Parse("1981-12-08"),
                                                                  DateTime.Parse("2005-03-04"), DateTime.Now.AddDays(-25), 0, 64, -1, -1, -1, 4,
                                                                  8, 0, 8, 64, 16, 8, 8, 0, 2, 0, -1, -1, -1, 223, 1538, 3403764,
                                                                  3443821, -2.066426, 0.594637, -1, "424", 0,
                                                                  DateTime.Parse("2008-12-08"), 0, -1, 0,
                                                                  DateTime.Parse("2011-04-01"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  988264754, 3, 9, DateTime.Parse("1977-04-06"),
                                                                  DateTime.Parse("2005-10-03"), DateTime.Now.AddDays(-25), 0, -1, -1, -1, -1, 0,
                                                                  0, 0, 0, -21474, 0, 0, 0, 0, -1, 0, -1, -1, -1, 223, 1538,
                                                                  3404470, 3444735, -2.064637, 0.594102, -1, "818", 0,
                                                                  DateTime.Parse("2008-12-25"), 0, -1, 0,
                                                                  DateTime.Parse("2011-04-01"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100003499, 3, 9, DateTime.Parse("1991-01-01"),
                                                                  DateTime.Parse("2009-02-24"), DateTime.Now, 0, 4, -1, -1, -1, 4,
                                                                  4, 0, 2, 4, 16, 32, 1, 0, 4, 0, -1, -1, -1, 105, 3484024, 9801033
                                                                  , 0, 0.614984, 0.576949, -1, "4  ", 0,
                                                                  DateTime.Parse("2009-05-07"), 0, -1, 0,
                                                                  DateTime.Parse("2009-05-24"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100267102, 3, 6, DateTime.Parse("1978-04-15"),
                                                                  DateTime.Parse("2009-04-01"), DateTime.Now, 0, 8, -1, -1, -1, 4,
                                                                  16, 0, 8, 1, 4, 2, 8, 0, 4, 0, -1, -1, -1, 223, 1538, 3403764,
                                                                  3443821, -2.066426, 0.594637, -1, "424", 0,
                                                                  DateTime.Parse("2009-04-01"), 0, -1, 0,
                                                                  DateTime.Parse("2011-04-01"), -1, -1, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100003342, 3, 9, DateTime.Parse("1978-01-17"),
                                                                  DateTime.Parse("2005-05-02"), DateTime.Now, 0, 16, -1, 2, -1, 2,
                                                                  4, 160, 2, 256, 16, 4, 4, 0, 126, 0, 4, 32768, -1, 223, 1538,
                                                                  3404470, 3444742, -2.064637, 0.594102, -1, "818", 41,
                                                                  DateTime.Parse("2009-10-06"), 52618, 1, 2,
                                                                  DateTime.Parse("2010-02-13"), 16, 128, -1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  101615104, 3, 6, DateTime.Parse("1980-07-04"),
                                                                  DateTime.Parse("2010-04-26"), DateTime.Now, 0, 128, -1, 264194,
                                                                  -1, 4, 8, 138, 2, 4096, 4, 8, 4, 0, 34, 0, 128, 256, -1, 105,
                                                                  3484017, 9800630, 0, 0.614792, 0.554632, -1, "2  ", 3,
                                                                  DateTime.Parse("2010-04-26"), 42788, 1, 0,
                                                                  DateTime.Parse("2011-04-01"), 2, 4, 1, -1
                                                              })));
            jdateSearchMembers.Add(
                new SearchMember().Populate(CreateDataRow(dataTable,
                                                          new object[]
                                                              {
                                                                  100267573, 3, 9, DateTime.Parse("1990-01-02"),
                                                                  DateTime.Parse("2009-05-15"), DateTime.Now, 0, 2, -1, -1, -1, 2,
                                                                  2, 0, 8, 2048, 1, 1, 1, 0, 4, 0, -1, -1, -1, 223, 1538, 3403764,
                                                                  3443821, -2.066426, 0.594637, -1, "424", 0,
                                                                  DateTime.Parse("2009-05-15"), 0, -1, 0,
                                                                  DateTime.Parse("2011-04-01"), -1, -1, -1, -1
                                                              })));
            searchMembers.Add(3, jdateSearchMembers);
        }

        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL, IProcessorType iProcessorType)
        {
            IList members = null;
            switch(iProcessorType)
            {
                case IProcessorType.KeywordIndexProcessor:
                case IProcessorType.KeywordIndexVersion3Processor:
                    members = searchKeywords[communityId];
                    break;
                case IProcessorType.MemberIndexProcessor:
                case IProcessorType.MemberIndexVersion3Processor:
                default:
                    members = searchMembers[communityId];
                    break;
            }
            SetupTestDataInWriter(communityId, xmlFilePath, searcherBL, members, iProcessorType);
        }

        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL)
        {
            List<SearchMember> commSearchMembers = searchMembers[communityId];
            SetupTestDataInWriter(communityId, xmlFilePath, searcherBL, commSearchMembers);
        }

        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL, IList commSearchMembers)
        {
            SetupTestDataInWriter(communityId,xmlFilePath,searcherBL,commSearchMembers,IProcessorType.MemberIndexProcessor);
        }

        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL, IList commSearchMembers, IProcessorType iProcessorType)
        {
            Directory dir = new RAMDirectory();
            IndexWriter writer = new IndexWriter(dir, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);
            writer.UseCompoundFile = false;
            //add docs
            DocumentMetadata documentMetadata = Serialization.FromXmlFile<DocumentMetadata>(xmlFilePath);

            Searcher searcher = null;
            switch (iProcessorType)
            {
                case IProcessorType.KeywordIndexProcessor:
                case IProcessorType.KeywordIndexVersion3Processor:
                    KeywordIndexProcessor keywordIndexProcessor = new KeywordIndexProcessor(documentMetadata, writer, dir, commSearchMembers.Count*2);
                    keywordIndexProcessor.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                    keywordIndexProcessor.RegionLanguages = Utils.RegionLanguages;
                    keywordIndexProcessor.SpatialContext = Utils.SpatialContext;
                    keywordIndexProcessor.SpatialStrategy = Utils.SpatialStrategy;
                    keywordIndexProcessor.SpatialShapeFields=Utils.SpatialShapeFields;
                    foreach (SearchKeyword searchMember in commSearchMembers)
                    {
                        searchMember.UpdateType = UpdateTypeEnum.all;
                        keywordIndexProcessor.IndexKeyword(searchMember, documentMetadata, writer);
                    }
                    writer.Dispose();
                    searcher = searcherBL.GetKeywordSearcher(communityId);
                    break;
                case IProcessorType.MemberIndexProcessor:
                case IProcessorType.MemberIndexVersion3Processor:
                default:
                    MemberIndexProcessor memberIndexProcessor = new MemberIndexProcessor(documentMetadata, writer, dir, commSearchMembers.Count * 2);
                    memberIndexProcessor.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                    memberIndexProcessor.RegionLanguages = Utils.RegionLanguages;
                    memberIndexProcessor.SpatialContext = Utils.SpatialContext;
                    memberIndexProcessor.SpatialStrategy = Utils.SpatialStrategy;
                    memberIndexProcessor.SpatialShapeFields=Utils.SpatialShapeFields;
                    foreach (SearchMember searchMember in commSearchMembers)
                    {
                        searchMember.UpdateType = UpdateTypeEnum.all;
                        memberIndexProcessor.IndexMember(searchMember, documentMetadata, writer);
                    }
                    writer.Dispose();
                    searcher = searcherBL.GetSearcher(communityId);
                    break;
            }
            searcher.Update(dir, string.Empty, false);
        }

        protected void CreateAndInitializeSearcherBL(int communityID, SearcherBL searcherBL, SearchPreferenceCollection testSearchPrefs)
        {
            RemoveSearcherBL(communityID);
            searcherBLs.Add(communityID, searcherBL);
            testSearchPrefs.Add("domainid", communityID + "");
            searcherBL.RunQuery(TestQueryBuilder.build(testSearchPrefs, communityID, 0), communityID);
            testSearchPrefs.Add("domainid", "");
        }

        protected void SearchersSetup()
        {
            testSearchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            testSearchPrefs.Add("countryregionid", "223");
            testSearchPrefs.Add("radius", "500");
            testSearchPrefs.Add("regionid", "3443821");
            testSearchPrefs.Add("searchtype", "1");
            testSearchPrefs.Add("geodistance", "6");
            testSearchPrefs.Add("searchtypeid", "1");
            testSearchPrefs.Add("regionidcity", "3403764");
            testSearchPrefs.Add("searchorderby", "1");
            testSearchPrefs.Add("minage", "20");
            testSearchPrefs.Add("maxage", "99");
            testSearchPrefs.Add("gendermask", "6");

            #region Local Indexes - Use these when you need to test against a local index for some reason
            //LOCAL
//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "c:\\Matchnet\\Index\\Jdate1");
//            if (!indexPaths.ContainsKey(1)) indexPaths.Add(1, "d:\\Matchnet\\Index\\prod\\Spark2");
//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "d:\\Matchnet\\Index\\prod\\JDate2.member");
//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "c:\\Matchnet\\Index\\prod\\Jdate1");
//            if (!indexPaths.ContainsKey(10)) indexPaths.Add(10, "c:\\Matchnet\\Index\\Cupid1");
//            if (!indexPaths.ContainsKey(24)) indexPaths.Add(24, "c:\\Matchnet\\Index\\prod\\BlackSingles1");
//            if (!indexPaths.ContainsKey(23)) indexPaths.Add(23, "c:\\Matchnet\\Index\\BBWPersonalsPlus1");


//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "d:\\Matchnet\\IndexStage\\Jdate1");
//            if (!indexPaths.ContainsKey(1)) indexPaths.Add(1, "d:\\Matchnet\\IndexStage\\Spark1");
//            if (!indexPaths.ContainsKey(10)) indexPaths.Add(10, "d:\\Matchnet\\IndexStage\\Cupid1");
//            if (!indexPaths.ContainsKey(24)) indexPaths.Add(24, "d:\\Matchnet\\IndexStage\\BlackSingles2");
//            if (!indexPaths.ContainsKey(23)) indexPaths.Add(23, "d:\\Matchnet\\IndexStage\\BBWPersonals1");

//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "d:\\Matchnet\\Index\\prod\\Jdate2");
//            if (!indexPaths.ContainsKey(1)) indexPaths.Add(1, "d:\\Matchnet\\Index\\stage\\Spark1");
//            if (!indexPaths.ContainsKey(10)) indexPaths.Add(10, "d:\\Matchnet\\Index\\stage\\Cupid2");
//            if (!indexPaths.ContainsKey(24)) indexPaths.Add(24, "d:\\Matchnet\\Index\\stage\\BlackSingles3");
//            if (!indexPaths.ContainsKey(23)) indexPaths.Add(23, "d:\\Matchnet\\Index\\stage\\BBWPersonals3");

//            if (!keywordIndexPaths.ContainsKey(3)) keywordIndexPaths.Add(3, "d:\\Matchnet\\KeywordIndex\\Prod\\Jdate1");
//            if (!keywordIndexPaths.ContainsKey(3)) keywordIndexPaths.Add(3, "d:\\Matchnet\\KeywordIndexStage\\Jdate1");
//            if (!keywordIndexPaths.ContainsKey(1)) keywordIndexPaths.Add(1, "d:\\Matchnet\\KeywordIndexStage\\Spark1");
//            if (!keywordIndexPaths.ContainsKey(10)) keywordIndexPaths.Add(10, "d:\\Matchnet\\KeywordIndexStage\\Cupid1");
//            if (!keywordIndexPaths.ContainsKey(24)) keywordIndexPaths.Add(24, "d:\\Matchnet\\KeywordIndexStage\\BlackSingles2");
//            if (!keywordIndexPaths.ContainsKey(23)) keywordIndexPaths.Add(23, "d:\\Matchnet\\KeywordIndexStage\\BBWPersonals1");

//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "d:\\Matchnet\\IndexVersion3Stage\\Jdate2");
            //            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "D:/matchnet/IndexVersion3/prod/JDate1.member");
//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "D:/matchnet/Index/prod/JDate2.member");
            //            if (!indexPaths.ContainsKey(1)) indexPaths.Add(1, "d:\\Matchnet\\IndexVersion3Stage\\Spark3");
            //            if (!indexPaths.ContainsKey(10)) indexPaths.Add(10, "d:\\Matchnet\\IndexVersion3Stage\\Cupid2");
            //            if (!indexPaths.ContainsKey(24)) indexPaths.Add(24, "d:\\Matchnet\\IndexVersion3Stage\\BlackSingles1");
            //            if (!indexPaths.ContainsKey(23)) indexPaths.Add(23, "d:\\Matchnet\\IndexVersion3Stage\\BBWPersonals2");


//            if (!keywordIndexPaths.ContainsKey(3)) keywordIndexPaths.Add(3, "d:\\Matchnet\\KeywordIndexVersion3Stage\\Jdate3");
            //           if (!keywordIndexPaths.ContainsKey(3)) keywordIndexPaths.Add(3, "D:/matchnet/KeywordIndexVersion3/prod/JDate3.keyword");            
//            if (!keywordIndexPaths.ContainsKey(3)) keywordIndexPaths.Add(3, "D:/matchnet/KeywordIndex/prod/JDate3.keyword");            
            //            if (!keywordIndexPaths.ContainsKey(1)) keywordIndexPaths.Add(1, "D:/matchnet/KeywordIndexVersion3Stage/Spark3");
            //            if (!keywordIndexPaths.ContainsKey(10)) keywordIndexPaths.Add(10, "d:\\Matchnet\\KeywordIndexVersion3Stage\\Cupid3");
            //            if (!keywordIndexPaths.ContainsKey(24)) keywordIndexPaths.Add(24, "d:\\Matchnet\\KeywordIndexVersion3Stage\\BlackSingles1");
            //            if (!keywordIndexPaths.ContainsKey(23)) keywordIndexPaths.Add(23, "d:\\Matchnet\\KeywordIndexVersion3Stage\\BBWPersonals3");

            
            #endregion

            #region Devapp01 Indexes - Shared indexes so unit test works for everyone
            //devapp01 PROD Copy - use these for unit tests so it's available to all devs, notice we use "UnitTestIndexes" folder so it doesn't conflict with actual services
            if (!indexPaths.ContainsKey(1)) indexPaths.Add(1, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\Spark1");
            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\Jdate1");
            if (!indexPaths.ContainsKey(21)) indexPaths.Add(21, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\ItalianSingles1");
            if (!indexPaths.ContainsKey(10)) indexPaths.Add(10, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\Cupid1");
            if (!indexPaths.ContainsKey(24)) indexPaths.Add(24, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\BlackSingles1");
            if (!indexPaths.ContainsKey(23)) indexPaths.Add(23, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\IndexVersion3\\BBWPersonalsPlus1");

            if (!keywordIndexPaths.ContainsKey(1)) keywordIndexPaths.Add(1, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\KeywordIndexVersion3\\Spark1");
            if (!keywordIndexPaths.ContainsKey(3)) keywordIndexPaths.Add(3, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\KeywordIndexVersion3\\Jdate1");
            if (!keywordIndexPaths.ContainsKey(21)) keywordIndexPaths.Add(21, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\KeywordIndexVersion3\\ItalianSingles1");
            if (!keywordIndexPaths.ContainsKey(10)) keywordIndexPaths.Add(10, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\KeywordIndexVersion3\\Cupid1");
            if (!keywordIndexPaths.ContainsKey(24)) keywordIndexPaths.Add(24, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\KeywordIndexVersion3\\BlackSingles1");
            if (!keywordIndexPaths.ContainsKey(23)) keywordIndexPaths.Add(23, "\\\\devapp01\\c$\\Matchnet\\UnitTestIndexes\\KeywordIndexVersion3\\BBWPersonalsPlus1");

            #endregion


            searcherBLs = new Dictionary<int, SearcherBL>();
            string[] communityList = Utils.GetSetting("SEARCHER_COMMUNITY_LIST", "").Split(new char[] { ';' });
            foreach (string s in communityList)
            {
                int communityID = Convert.ToInt32(s);
                SearcherBL searcherBL = CreateSearcherBL(communityID, indexPaths, communityList,
                                                         SearcherBL_IncrementCounters, SearcherBL_DecrementCounter,
                                                         SearcherBL_AverageSearchTime);
                CreateAndInitializeSearcherBL(communityID, searcherBL, testSearchPrefs);
            }
        }

        protected void SearchersTeardown()
        {
            if (null != searcherBLs)
            {
                foreach (SearcherBL searcherBL in searcherBLs.Values)
                {
                    searcherBL.Stop();
                    searcherBL.USE_NUNIT = false;
                    searcherBL.NUNIT_StringBuilder = null;
                }
                indexPaths.Clear();
                searcherBLs = null;
            }
        }

        protected void SetupMockSettingsService()
        {
            _mockSettingsService = new MockSettingsService();
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_LIST", "23;24;1;3;10");
//            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_LIST", "3");
            _mockSettingsService.AddSetting("SEARCHER_SEARCH_THRESHOLD", "3000");
            _mockSettingsService.AddSetting("SEARCH30_SEARCHER_MAX_RESULTS", "1000");
            _mockSettingsService.AddSetting("SEARCHER_CLOSE_WAIT_TIME", "10000");
            _mockSettingsService.AddSetting("ENABLE_MATCHMAIL_RADIUS_LIMIT", "true");
            _mockSettingsService.AddSetting("SEARCHER_MAX_RESULTS", "360");
            _mockSettingsService.AddSetting("USE_E2_DISTANCE", "true");
            _mockSettingsService.AddSetting("USE_MINGLE_DISTANCE", "false");
            _mockSettingsService.AddSetting("SEARCHER_MAX_ACTIVE_DAYS", "1800");
            _mockSettingsService.AddSetting("SEARCHER_USE_ALL_INDEXES", "false");
            _mockSettingsService.AddSetting("INDEXER_ACTIVE_DAYS", "500");
            _mockSettingsService.AddSetting("INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("INDEXER_COPY_INDEX", "false");
            _mockSettingsService.AddSetting("INDEXER_DB_TIMEOUT", "120");
            _mockSettingsService.AddSetting("INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("INDEXER_NUM_OF_PARTITIONS", "157");
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/");
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "15");
            _mockSettingsService.AddSetting("INDEXER_USE_COMPOUND_FILE", "true");
            _mockSettingsService.AddSetting("INDEXER_USE_QUEUE", "false");
            _mockSettingsService.AddSetting("INDEXER_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("SEARCHINDEXER_INDEX_ONSTART", "true");
            _mockSettingsService.AddSetting("SEARCHINDEXER_SVC_LOGGER_MASK", "63");
            _mockSettingsService.AddSetting("ENABLE_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/");
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_METADATA_FILE", "SearchDocument.xml");
            _mockSettingsService.AddSetting("SEARCHER_PER_COMMUNITY", "true");
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "75", 3);
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "10", 10);
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "10", 23);
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "10", 24);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/Spark", 1);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/JDate", 3);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/Cupid", 10);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/BlackSingles", 24);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/Spark", 1);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/JDate", 3);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/Cupid", 10);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/BBWPersonalsPlus", 23);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/BlackSingles", 24);

            _mockSettingsService.AddSetting("INDEXER_VERSION_3_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3/");
            _mockSettingsService.AddSetting("SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3/Spark", 1);
            _mockSettingsService.AddSetting("SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3/JDate", 3);
            _mockSettingsService.AddSetting("SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3/Cupid", 10);
            _mockSettingsService.AddSetting("SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3/BBWPersonalsPlus", 23);
            _mockSettingsService.AddSetting("SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3/BlackSingles", 24);
            _mockSettingsService.AddSetting("INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3Stage/");
            _mockSettingsService.AddSetting("INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3Stage/Spark", 1);
            _mockSettingsService.AddSetting("INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3Stage/JDate", 3);
            _mockSettingsService.AddSetting("INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3Stage/Cupid", 10);
            _mockSettingsService.AddSetting("INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3Stage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexVersion3Stage/BlackSingles", 24);

            
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_ACTIVE_DAYS", "500");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_COPY_INDEX", "false");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_DB_TIMEOUT", "120");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_NUM_OF_PARTITIONS", "157");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_SWAPPING_INTERVAL", "60");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_USE_COMPOUND_FILE", "true");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_USE_QUEUE", "false");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_METADATA_FILE", "KeywordSearchDocument.xml");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/Spark", 1);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/JDate", 3);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/Cupid", 10);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/BlackSingles", 24);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/Spark1", 1);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/JDate1", 3);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/Cupid1", 10);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/BBWPersonalsPlus1", 23);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/BlackSingles1", 24);

            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3/");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3/Spark", 1);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3/JDate", 3);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3/Cupid", 10);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3/BBWPersonalsPlus", 23);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3/BlackSingles", 24);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3Stage/");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3Stage/Spark", 1);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3Stage/JDate", 3);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3Stage/Cupid", 10);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3Stage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_VERSION_3_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexVersion3Stage/BlackSingles", 24);
            

            _mockSettingsService.AddSetting("ADMIN_INDEXER_ACTIVE_DAYS", "180");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_COPY_INDEX", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_DB_TIMEOUT", "300");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_NUM_OF_PARTITIONS", "157");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/");
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "D:/Matchnet/AdminIndex/");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_SWAPPING_INTERVAL", "60");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_COMPOUND_FILE", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_QUEUE", "false");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("ENABLE_ADMIN_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_METADATA_FILE", "AdminSearchDocument.xml");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/Spark", 1);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/JDate", 3);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/Cupid", 10);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/BlackSingles", 24);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/Spark", 1);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/JDate", 3);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/Cupid", 10);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/BBWPersonalsPlus", 23);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/BlackSingles", 24);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_ONDEMAND_ENABLED", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_ONDEMAND_DATETIME", "06/20/2012 12:02 PM", 1);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_ROWBLOCKS", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_ROWBLOCKS_DATE_RANGE_IN_DAYS", "1");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_PROCESSOR_TIME_LIMIT", "180");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_BRANDLASTLOGONDATE_LOOKBACK", "5");
            
            _mockSettingsService.AddSetting("NRT_USE_SEARCH_PREF_SA", "false");
            _mockSettingsService.AddSetting("SEARCHER_SLOP_FACTOR", "2");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_FILTER", "true");
            _mockSettingsService.AddSetting("SEARCHER_USE_FUZZY_QUERY_FOR_SINGLE_TERMS", "false");
            _mockSettingsService.AddSetting("INDEXER_USE_SYSTEM_DB_FOR_INDEX_UPDATES", "true");
            _mockSettingsService.AddSetting("SEARCHER_USE_SYSTEM_DB_FOR_INDEX_UPDATES", "true");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_MAX_RESULTS", "5000");
            _mockSettingsService.AddSetting("SEARCHER_MIN_KEYWORD_LENGTH", "2");
            _mockSettingsService.AddSetting("SEARCHER_PREPROCESS_KEYWORD_TERMS", "true");

            _mockSettingsService.AddSetting("USE_LUCENE_CACHED_DISTANCE_VALUES_SORT", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "false");
            _mockSettingsService.AddSetting("SEARCHER_INDEX_UPDATE_INTERVAL", "90000");
            _mockSettingsService.AddSetting("SEARCHER_FILTER_ORDER","5,1,2,3,4,6");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");
            _mockSettingsService.AddSetting("ENABLE_MATCH_RATING_CALCULATION", "false");
        }
    }

    public class MockSettingsService : ISettingsSA
    {
        private Dictionary<int, Dictionary<string, string>> communitySettings = new Dictionary<int, Dictionary<string, string>>();
        #region ISettingsSA Members

        public void AddSetting(string key, string value)
        {
            AddSetting(key, value, 0);
        }

        public void AddSetting(string key, string value, int communityId)
        {
            if (!communitySettings.ContainsKey(communityId)) communitySettings.Add(communityId, new Dictionary<string, string>());

            if (communitySettings[communityId].ContainsKey(key))
                communitySettings[communityId][key] = value;
            else
            communitySettings[communityId].Add(key, value);
        }

        public void RemoveSetting(string key)
        {
            RemoveSetting(key, 0);
        }

        public void RemoveSetting(string key, int communityId)
        {
            if (SettingExistsFromSingleton(key, communityId, 0))
            {
                communitySettings[communityId].Remove(key);
            }
            else if (SettingExistsFromSingleton(key, 0, 0))
            {
                communitySettings[0].Remove(key);
            }
        }

        public System.Collections.Generic.List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new System.NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant)
        {
            try
            {
                return communitySettings[0][constant];
            }
            catch (Exception e)
            {
                print(string.Format("Could not find setting key:{0}, community:{1}", constant, 0));
                throw e;
            }
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityId, int siteId)
        {
            bool settingExistsFromSingleton = communitySettings.ContainsKey(communityId) && communitySettings[communityId].ContainsKey(constant);
            //if (!settingExistsFromSingleton)
            //{
            //    print(string.Format("Could not find setting key:{0}, community:{1}", constant, communityId));
            //}
            return settingExistsFromSingleton;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if (!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        public List<Matchnet.Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
