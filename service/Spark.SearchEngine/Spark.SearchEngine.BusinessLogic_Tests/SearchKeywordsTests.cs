﻿using System;
using System.Linq;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using NUnit.Framework;
using Spark.SearchEngine.BusinessLogic;
using Spark.SearchEngine.BusinessLogic.Searcher;
using Spark.SearchEngine.ValueObjects;
using Searcher = Spark.SearchEngine.BusinessLogic.Searcher.Searcher;

namespace Spark.SearchEngine.BusinessLogic_Tests
{
    [TestFixture]
    public class SearchKeywordsTests : AbstractSparkUnitTest
    {
        [TestFixtureSetUp]
        public void StartUp()
        {
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            TestQueryBuilder.SettingsService = _mockSettingsService;
            SearchersSetup();
            //search members for each community for testing accuracy
            CreateTestSearchMember();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            SearchersTeardown();
            searchMembers.Clear();
            searchMembers = null;
            Utils.SettingsService = null;
            TestQueryBuilder.SettingsService = null;
        }

        [SetUp]
        public void setUp()
        {
        }

        [TearDown]
        public void tearDown()
        {
        }
        
        [Test]
        public void TestProcessKeywords()
        {
            string phrase1 = "\"this is a phrase\"";
            string phrase2 = "this \"is a\" PhraseQuery";
            string phrase3 = "this ISearchPreference a \"phrase\"";
            string phrase4 = "this ISearchPreference, a \"phrase\"";
            string phrase5 = "joe verse the volcano, meet joe black, titanic, \"avatar, the last airbender\"; goodfellas";
            string phrase6 = "Red Sox";
            string phrase7 = "i am the";
            string phrase8 = "a nice person!!";
            string phrase9 = "caring, funny";
            string phrase10 = "caring funny";

            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase1, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase2, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase3, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase4, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase5, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase6, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase7, 2));
            string preProcessKeywords = QueryBuilderFactory.Instance.PreProcessKeywords(phrase8, 2);
            print(preProcessKeywords);
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase9, 2));
            print(QueryBuilderFactory.Instance.PreProcessKeywords(phrase10, 2));

        }

        [Test]
        public void AJdateOneMemberKeywordsSearchTest()
        {
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            TermQuery termQuery = new TermQuery(new Term("id", 134160011 + ""));
            TopDocs topDocs = keywordSearcher.IndexSearcher.Search(termQuery,10);

            Assert.NotNull(topDocs.ScoreDocs);
            Assert.GreaterOrEqual(topDocs.ScoreDocs.Length, 1);

            for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
            {
                int docID = topDocs.ScoreDocs[i].Doc;
                Document doc = keywordSearcher.IndexSearcher.Doc(docID);
                int id = Int32.Parse(doc.GetField("id").StringValue);
                Explanation explanation = keywordSearcher.IndexSearcher.Explain(termQuery, docID);

                print("-----------------------------\n");
                print(doc.ToString());
                print(explanation.ToString());

            }

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

        [Test]
        public void AJdateKeywordFilterTest()
        {
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            //SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, IProcessorType.MemberIndexProcessor);
            //SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", jdateSearcherBL, IProcessorType.KeywordIndexProcessor);


            
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("keywordsearch", "Registered Nurse");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("keywordsearch", "Registered Nurse");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "2");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("geodistance", "4");
            searchPrefs.Add("regionid", "3466198");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("blockedmemberids", "134169672,127384096,8170062,140820475,11214278");
            searchPrefs.Add("minage", "35");
            searchPrefs.Add("maxage", "50");
            searchPrefs.Add("memberid", "134093433");
            searchPrefs.Add("keywordsearch", "Registered Nurse");
//            searchPrefs.Add("keywordsearch", "country");
//            searchPrefs.Add("keywordsearch", "country music");
//            searchPrefs.Add("keywordsearch", "\"country music\"");


//            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            IMatchnetQueryResults results = jdateSearcherBL.RunQuery(matchnetQuery, communityId);
//            QueryBuilder queryBuilder = jdateSearcherBL.QueryBuilder_forNUNIT;

            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            QueryBuilder keywordQueryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            int [] memberids = new int [results.Items.Count];
            int i = 0;
            foreach(IMatchnetResultItem item in results.Items.List)
            {
                memberids[i] = item.MemberID;
                i++;
            }
            Array.Sort(memberids);
            foreach (int memberid in memberids)
            {
                print(memberid.ToString());                
            }

            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("keywordsearch", "the beach");
            searchPrefs.Add("languagemask", "2");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("keywordsearch", "the beach");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("geodistance", "1");
            searchPrefs.Add("regionid", "3445678");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("blockedmemberids", "120097040");
            searchPrefs.Add("minage", "27");
            searchPrefs.Add("maxage", "52");
            searchPrefs.Add("memberid", "113804686");
//            searchPrefs.Add("MaritalStatus", "8");
//            searchPrefs.Add("SmokingHabits", "16");
//            searchPrefs.Add("DrinkingHabits", "16");
            
            matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            results = jdateSearcherBL.RunQuery(matchnetQuery, communityId);
            //            QueryBuilder queryBuilder = jdateSearcherBL.QueryBuilder_forNUNIT;

            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            memberids = new int[results.Items.Count];
            i = 0;
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                memberids[i] = item.MemberID;
                i++;
            }
            Array.Sort(memberids);
            foreach (int memberid in memberids)
            {
                print(memberid.ToString());
            }

//            foreach (IMatchnetResultItem result in results.Items.List)
//            {
//                TermQuery termQuery = new TermQuery(new Term("id", result.MemberID.ToString()));
//                TopDocs docs = keywordSearcher.IndexSearcher.Search(termQuery, 1);
//                print("\n-----------------------------\n");
//                if (docs.ScoreDocs.Length > 0)
//                {
//                    int docID = docs.ScoreDocs[0].doc;
//                    Document doc = keywordSearcher.IndexSearcher.Doc(docID);
//                    Explanation explanation = keywordSearcher.IndexSearcher.Explain(keywordQueryBuilder.queries.Last<Query>(), docID);
//                    print(doc.ToString());
//                    print(explanation.ToString());
//                }
//                else
//                {
//                    print(string.Format("Could not find Member id:{0} in keyword index.", result.MemberID));
//                }
//            }

//            results = jdateSearcherBL.RunDetailedQuery(matchnetQuery, communityId);
//            Assert.NotNull(results);
//            Assert.GreaterOrEqual(results.Items.Count, 1);

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

        [Test]
        public void AAJdateKeywordSearchTest()
        {
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            //SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, IProcessorType.MemberIndexProcessor);
            //SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", jdateSearcherBL, IProcessorType.KeywordIndexProcessor);

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("memberid", "115675140");
            searchPrefs.Add("radius", "500");
            searchPrefs.Add("regionid", "3443817");
//            searchPrefs.Add("keywordsearch", "music");
            //            searchPrefs.Add("keywordsearch", "country");
//            searchPrefs.Add("keywordsearch", "country music");
            //            searchPrefs.Add("keywordsearch", "\"country music\"");
//            searchPrefs.Add("keywordsearch", "a nice person++");
//            searchPrefs.Add("keywordsearch", "a nice person@@");
//            searchPrefs.Add("keywordsearch", "a nice person##");
//            searchPrefs.Add("keywordsearch", "a nice person$$");
//            searchPrefs.Add("keywordsearch", "a nice person%%");
//            searchPrefs.Add("keywordsearch", "a nice person--");
//            searchPrefs.Add("keywordsearch", "a nice person&&");
//            searchPrefs.Add("keywordsearch", "a nice person==");
//            searchPrefs.Add("keywordsearch", "a nice person||");

//            searchPrefs.Add("keywordsearch", "a nice person!!"); 
//            searchPrefs.Add("keywordsearch", "a nice person^^"); 
//            searchPrefs.Add("keywordsearch", "a nice person**"); 
//            searchPrefs.Add("keywordsearch", "a nice person(("); 
//            searchPrefs.Add("keywordsearch", "a nice person()");
//            searchPrefs.Add("keywordsearch", "a nice person))");
//            searchPrefs.Add("keywordsearch", "a nice person{{");
//            searchPrefs.Add("keywordsearch", "a nice person{}");
//            searchPrefs.Add("keywordsearch", "a nice person}}");
//            searchPrefs.Add("keywordsearch", "a nice person[[");
//            searchPrefs.Add("keywordsearch", "a nice person[]");
//            searchPrefs.Add("keywordsearch", "a nice person]]");
//            searchPrefs.Add("keywordsearch", "a nice person::");
//            searchPrefs.Add("keywordsearch", "a nice person~~");
//            searchPrefs.Add("keywordsearch", "a nice person??");
//            searchPrefs.Add("keywordsearch", "a nice person\"\"");
//            searchPrefs.Add("keywordsearch", "a nice person\"");
//            searchPrefs.Add("keywordsearch", "a nice person,,");
//            searchPrefs.Add("keywordsearch", "a nice person,");
//                        searchPrefs.Add("keywordsearch", "a nice person;;");
                        searchPrefs.Add("keywordsearch", "a nice person;");


            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            QueryBuilder queryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            queryBuilder.MaxNumberOfMatches = 2000;
            TopDocs topDocs = keywordSearcher.Search(queryBuilder);
                      
            Assert.NotNull(topDocs.ScoreDocs);
            int length = topDocs.ScoreDocs.Length;
            Assert.GreaterOrEqual(length, 1);
            searchPrefs.Add("Num Of Results", length.ToString());
            print(searchPrefs.ToString());

            int[] memberids = new int[length];
            for (int i = 0; i < length; i++)
            {
                int docID = topDocs.ScoreDocs[i].Doc;
                Document doc = keywordSearcher.IndexSearcher.Doc(docID);
                int id = Int32.Parse(doc.GetField("id").StringValue);            
                memberids[i] = id;
            }
//            Array.Sort(memberids);
            foreach (int memberid in memberids)
            {
                print(memberid.ToString());
            }

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

       [Test]
        public void AJdateKeywordProdFilterTest()
        {
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("geodistance", "3");
            searchPrefs.Add("regionid", "3404470");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "39");
            searchPrefs.Add("memberid", "127220437");
            searchPrefs.Add("keywordsearch", "red sox");
            searchPrefs.Add("keywordsearch", "red sox");

            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(matchnetQuery, communityId);
            QueryBuilder queryBuilder = jdateSearcherBL.QueryBuilder_forNUNIT;

            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            QueryBuilder keywordQueryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            int[] memberids = new int[results.Items.Count];
            int i = 0;
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                memberids[i] = item.MemberID;
                i++;
            }
            Array.Sort(memberids);
            foreach (int memberid in memberids)
            {
                print(memberid.ToString());
            }            

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

       [Test]
       public void AJdateKeywordProdSearchTest()
       {
           int communityId = 3;
           int siteId = 103;
           SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
           jdateSearcherBL.NUNIT_Print = true;
           jdateSearcherBL.NUNIT_Show_Distances = true;

           Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs =
               new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
           searchPrefs.Add("domainid", communityId + "");
           searchPrefs.Add("domainid", communityId + "");
           searchPrefs.Add("domainid", communityId + "");
           searchPrefs.Add("hasphotoflag", "1");
           searchPrefs.Add("countryregionid", "223");
           searchPrefs.Add("countryregionid", "223");
           searchPrefs.Add("searchtype", "1");
           searchPrefs.Add("searchtypeid", "1");
           searchPrefs.Add("searchorderby", "6");
           searchPrefs.Add("gendermask", "6");
           searchPrefs.Add("geodistance", "3");
           searchPrefs.Add("regionid", "3404470");
           searchPrefs.Add("searchredesign30", "1");
           searchPrefs.Add("minage", "18");
           searchPrefs.Add("maxage", "39");
           searchPrefs.Add("memberid", "127220437");
           searchPrefs.Add("keywordsearch", "red sox");
           ProcessKeywordSearch(jdateSearcherBL, searchPrefs, siteId, communityId);


           jdateSearcherBL.NUNIT_Print = false;
           jdateSearcherBL.NUNIT_Show_Distances = false;
       }

        [Test]
        public void NoSingleTermJdateKeywordSearchTest()
                {
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("geodistance", "3");
            searchPrefs.Add("regionid", "3404470");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "39");
            searchPrefs.Add("memberid", "127220437");
            searchPrefs.Add("keywordsearch", "i am the");
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            QueryBuilder queryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            TopDocs topDocs = keywordSearcher.Search(queryBuilder);

            searchPrefs.Add("Num Of Results", topDocs.ScoreDocs.Length.ToString());
            print(searchPrefs.ToString());
            Assert.NotNull(topDocs.ScoreDocs);
            Assert.IsTrue(topDocs.ScoreDocs.Length==0);

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }
        
        [Test]
        public void AJdateKeywordsSearchTest()
        {
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

//            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", jdateSearcherBL, IProcessorType.KeywordIndexProcessor);

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("keywordsearch", "country music");
            searchPrefs.Add("searchredesign30", "1");
            ProcessKeywordSearch(jdateSearcherBL, searchPrefs, siteId, communityId);

            searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("keywordsearch", "\"country music\"");
            searchPrefs.Add("searchredesign30", "1");
            ProcessKeywordSearch(jdateSearcherBL, searchPrefs, siteId, communityId);

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }

        private void ProcessKeywordSearch(SearcherBL jdateSearcherBL, SearchPreferenceCollection searchPrefs, int siteId, int communityId)
        {
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            QueryBuilder queryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            TopDocs topDocs = keywordSearcher.Search(queryBuilder);

            Assert.NotNull(topDocs.ScoreDocs);
            Assert.GreaterOrEqual(topDocs.ScoreDocs.Length, 1);
            searchPrefs.Add("Num Of Results", topDocs.ScoreDocs.Length.ToString());
            searchPrefs.Add("Indexpath", keywordSearcher.IndexPath); 
            print(searchPrefs.ToString());
            
            for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
            {
                int docID = topDocs.ScoreDocs[i].Doc;
                Document doc = keywordSearcher.IndexSearcher.Doc(docID);
                int id = Int32.Parse(doc.GetField("id").StringValue);
//                Explanation explanation = keywordSearcher.IndexSearcher.Explain(queryBuilder.queries.Last<Query>(), docID);

                print("-----------------------------\n");
                print(id + "");
                print(doc.ToString());
//                print(explanation.ToString());
            }
        }

        [Test]
        public void SparkKeywordsSearchTest()
        {
            int communityId = 1;
            int siteId = 101;
            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", sparkSearcherBL, IProcessorType.KeywordIndexProcessor);
                        
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();            
            searchPrefs.Add("domainid", communityId+"");
            searchPrefs.Add("keywordsearch", "Enjoying, buSy");
//Memberid: 23247846
//Memberid: 103820222
//Memberid: 988270506

//Memberid: 988270506
//Memberid: 23247846
//Memberid: 103820222
            int[] expectedMemberIds = new int[] { 103820222, 988270506, 23247846 };
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            Searcher keywordSearcher = sparkSearcherBL.GetKeywordSearcher(communityId);
            QueryBuilder queryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            TopDocs topDocs = keywordSearcher.Search(queryBuilder);

            Assert.NotNull(topDocs.ScoreDocs);
            Assert.GreaterOrEqual(topDocs.ScoreDocs.Length, 1);

            for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
            {
                Document doc = keywordSearcher.IndexSearcher.Doc(topDocs.ScoreDocs[i].Doc);
                int id = Int32.Parse(doc.GetField("id").StringValue);

                print(string.Format("Memberid: {0}", id));

                bool success = false;
                foreach (int memberId in expectedMemberIds)
                {
                    if (id == memberId)
                    {
                        success = true;
                        break;
                    }
                }
                Assert.IsTrue(success, string.Format("{0} is not in Results!!", id));                
            }
            Assert.AreEqual(expectedMemberIds.Length, topDocs.ScoreDocs.Length);

            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;
        }

        [Test]
        public void SparkNoResultsKeywordsFilterTest()
        {
            int communityId = 1;
            int siteId = 101;
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");

            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL, IProcessorType.MemberIndexProcessor);
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", sparkSearcherBL, IProcessorType.KeywordIndexProcessor);

            //assert accuracy that result members will have preferred age range for 20 year old
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("radius", "1500");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("keywordsearch", "supercalifrajilisticespicallodocious");

            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);

            IMatchnetQueryResults results = sparkSearcherBL.RunDetailedQuery(matchnetQuery, communityId);
            Assert.NotNull(results);
            Assert.AreEqual(results.Items.Count, 0);

            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;
        }

        [Test]
        public void SparkKeywordsFilterTest()
        {
            int communityId = 1;
            int siteId = 101;
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");

            SearcherBL sparkSearcherBL = GetSearcherBL(communityId);
            sparkSearcherBL.NUNIT_Print = true;
            sparkSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", sparkSearcherBL, IProcessorType.MemberIndexProcessor);
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", sparkSearcherBL, IProcessorType.KeywordIndexProcessor);
            
            //assert accuracy that result members will have preferred age range for 20 year old
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId+"");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("radius", "1500");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("keywordsearch", "Enjoying, buSy");

            int[] expectedMemberIds = new int[] { 103820222, 988270506, 23247846 };
            //assert accuracy
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, sparkSearcherBL);

            sparkSearcherBL.NUNIT_Print = false;
            sparkSearcherBL.NUNIT_Show_Distances = false;
        }

        [Test]
        public void JDateILKeywordsFilterTest()
        {
            int communityId = 3;
            int siteId = 4;
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");

            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/SearchDocument.xml", jdateSearcherBL, IProcessorType.MemberIndexProcessor);
            SetupTestDataInWriter(communityId, "../../../Spark.SearchEngine.ValueObjects/KeywordSearchDocument.xml", jdateSearcherBL, IProcessorType.KeywordIndexProcessor);

            //assert accuracy that result members will have preferred age range for 20 year old
            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("radius", "1500");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("keywordsearch", "רולר ,חברותית");

            int[] expectedMemberIds = new int[] { 100000130 };
            //assert accuracy
            TestAccuracy(searchPrefs, communityId, siteId, expectedMemberIds, jdateSearcherBL);

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }


        [Test]
        public void FailedJdateSearch()
        {
            _mockSettingsService.RemoveSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true");
            int communityId = 3;
            int siteId = 103;
            SearcherBL jdateSearcherBL = GetSearcherBL(communityId);
            jdateSearcherBL.NUNIT_Print = true;
            jdateSearcherBL.NUNIT_Show_Distances = true;

            Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("domainid", communityId + "");
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("searchtype", "1");
            searchPrefs.Add("searchtypeid", "1");
            searchPrefs.Add("searchorderby", "6");
            searchPrefs.Add("gendermask", "6");
            searchPrefs.Add("geodistance", "10");
            searchPrefs.Add("regionid", "9808897");
            searchPrefs.Add("searchredesign30", "1");
            searchPrefs.Add("blockedmemberids", "120124191,120310214,119307477,15411466,127271653,115559132,114726824,119579465,115712912,113953359,114567453,31693430,118867908,115515775,119882645,119944214,112472044,40965815,126407542,114268256,114653949,126819691,47392088,115147240,115413402,126424591,115365993,118845153");
            searchPrefs.Add("minage", "18");
            searchPrefs.Add("maxage", "54");
            searchPrefs.Add("memberid", "115675140");
            searchPrefs.Add("jdatereligion", "18431");
            searchPrefs.Add("hasphotoflag", "1");
            searchPrefs.Add("maritalstatus", "2");
            searchPrefs.Add("childrencount", "1");
            searchPrefs.Add("regioncityid", "3414161");
            searchPrefs.Add("smokinghabits", "2");
            searchPrefs.Add("longitude", "-1.720155");
            searchPrefs.Add("latitiude", "0.694926");
            searchPrefs.Add("keywordsearch", "scuba, flip-flops");
            //            searchPrefs.Add("keywordsearch", "country");
            //            searchPrefs.Add("keywordsearch", "country music");
            //            searchPrefs.Add("keywordsearch", "\"country music\"");


            Searcher keywordSearcher = jdateSearcherBL.GetKeywordSearcher(communityId);
            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
            IMatchnetQueryResults results = jdateSearcherBL.RunDetailedQuery(matchnetQuery, communityId);
            QueryBuilder queryBuilder = jdateSearcherBL.QueryBuilder_forNUNIT;

            Assert.NotNull(results);
            Assert.GreaterOrEqual(results.Items.Count, 1);
            QueryBuilder keywordQueryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(matchnetQuery, communityId);
            int[] memberids = new int[results.Items.Count];
            int i = 0;
            foreach (IMatchnetResultItem item in results.Items.List)
            {
                memberids[i] = item.MemberID;
                i++;
            }
            Array.Sort(memberids);
            foreach (int memberid in memberids)
            {
                print(memberid.ToString());
            }
            //            foreach (IMatchnetResultItem result in results.Items.List)
            //            {
            //                TermQuery termQuery = new TermQuery(new Term("id", result.MemberID.ToString()));
            //                TopDocs docs = keywordSearcher.IndexSearcher.Search(termQuery, 1);
            //                print("\n-----------------------------\n");
            //                if (docs.ScoreDocs.Length > 0)
            //                {
            //                    int docID = docs.ScoreDocs[0].doc;
            //                    Document doc = keywordSearcher.IndexSearcher.Doc(docID);
            //                    Explanation explanation = keywordSearcher.IndexSearcher.Explain(keywordQueryBuilder.queries.Last<Query>(), docID);
            //                    print(doc.ToString());
            //                    print(explanation.ToString());
            //                }
            //                else
            //                {
            //                    print(string.Format("Could not find Member id:{0} in keyword index.", result.MemberID));
            //                }
            //            }

            //            results = jdateSearcherBL.RunDetailedQuery(matchnetQuery, communityId);
            //            Assert.NotNull(results);
            //            Assert.GreaterOrEqual(results.Items.Count, 1);

            jdateSearcherBL.NUNIT_Print = false;
            jdateSearcherBL.NUNIT_Show_Distances = false;
        }


    }
}
