﻿using System;
using System.Collections;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using NUnit.Framework;
using Spark.SearchEngine.BusinessLogic;
using Spark.SearchEngine.BusinessLogic.Indexer;
using Spark.SearchEngine.BusinessLogic.Searcher;
using Spark.SearchEngine.ValueObjects;

namespace Spark.SearchEngine.BusinessLogic_Tests
{
    [TestFixture]
    public class SearchIndexerTests : AbstractSparkUnitTest
    {
        protected decimal avgSearchTime = 0;
        protected decimal searchTimes = 0;
        protected int totalSearches = 0;
        protected void SearcherBL_IncrementCounters() { }
        protected void SearcherBL_DecrementCounter() { }
        protected void SearcherBL_AverageSearchTime(long searchTime) {
            totalSearches++;
            searchTimes += searchTime;
            avgSearchTime = searchTimes / totalSearches;
        }

        [TestFixtureSetUp]
        public void StartUp()
        {
            LoaderBL.Instance.USE_NUNIT = true;
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            TestQueryBuilder.SettingsService = _mockSettingsService;

        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            LoaderBL.Instance.USE_NUNIT = false;
            _mockSettingsService = null;
            Utils.SettingsService = null;
            TestQueryBuilder.SettingsService = null;
        }

        [SetUp]
        public void setUp()
        {}

        [TearDown]
        public void tearDown()
        {}

        private Hashtable GetHashConfig(int communityId, IProcessorType iProcessorType)
        {
            Hashtable h = new Hashtable();
            CommunityIndexConfig cic = LoaderBL.Instance.GetCommunityConfig(communityId, iProcessorType);
            h["config"] = cic;
            h["path"] = (!cic.EnableRollingUpdates) ? cic.IndexStagePath : cic.IndexStagePath + cic.RollingIndexDirectoryID;
            return h;
        }

//        [Test]
//        public void TestSparkIndexCommunityWithThreadsUsingRowBlocks()
//        {
//            int communityId = 3;
//
//            IndexProcessorFactory.Instance.ResetCommunity(communityId, IProcessorType.AdminIndexProcessor);
//            LoaderBL.Instance.IndexCommunityWithThreadsUsingRowBlocks(GetHashConfig(communityId, IProcessorType.AdminIndexProcessor));
//            System.Threading.Thread.Sleep(30000); 

            // do a simple search to make sure the index is there
            //System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(Utils.SettingsService.GetSettingFromSingleton("ADMIN_SEARCHER_COMMUNITY_DIR", 1) + "1");
            //Directory searchDir = FSDirectory.Open(dirinfo);
            //IndexReader reader = IndexReader.Open(searchDir);
            //IndexSearcher searcher = new IndexSearcher(reader);

            //Term t = new Term("firstname", "michael");
            //Query query = new TermQuery(t);
            //TopDocs docs = searcher.Search(query, 10);

            //int count = docs.TotalHits;

            //if (count > 0)
            //{
            //    var docId = docs.ScoreDocs[0].doc;
            //    var document = searcher.Doc(docId);
            //    var field = document.GetField("id");
            //    var memberId = Int32.Parse(field.StringValue());
            //}

            //searcher.Dispose();
            //reader.Dispose();
//        }

        [Test]
        public void TestSparkIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(1, IProcessorType.MemberIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(1, IProcessorType.MemberIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestJDateIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(3, IProcessorType.MemberIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(3, IProcessorType.MemberIndexProcessor));
            System.Threading.Thread.Sleep(10000);
//            while (!IndexProcessorFactory.Instance.AreAllDone(3, IProcessorType.MemberIndexProcessor))
//            {
//                System.Threading.Thread.Sleep(1000);
//            }
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestCupidIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(10, IProcessorType.MemberIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(10, IProcessorType.MemberIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestBBWIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(23, IProcessorType.MemberIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(23, IProcessorType.MemberIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestBSIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(24, IProcessorType.MemberIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(24, IProcessorType.MemberIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestSparkKeywordIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(1, IProcessorType.KeywordIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(1, IProcessorType.KeywordIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestCupidKeywordIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(10, IProcessorType.KeywordIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(10, IProcessorType.KeywordIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestBBWKeywordIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(23, IProcessorType.KeywordIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(23, IProcessorType.KeywordIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestBSKeywordIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(24, IProcessorType.KeywordIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(24, IProcessorType.KeywordIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }

        [Test]
        public void TestJDateKeywordIndexCommunityWithThreads()
        {
            IndexProcessorFactory.Instance.ResetCommunity(3, IProcessorType.KeywordIndexProcessor);
            LoaderBL.Instance.IndexCommunityWithThreads(GetHashConfig(3, IProcessorType.KeywordIndexProcessor));
            System.Threading.Thread.Sleep(30000);
        }
    }
}
