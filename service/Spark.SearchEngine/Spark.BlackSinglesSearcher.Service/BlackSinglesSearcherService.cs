﻿using System;
using System.Diagnostics;
using Spark.SearchEngine.ServiceManager;
using Matchnet.Exceptions;

namespace Spark.BlackSingles.Searcher.Service
{
    public partial class BlackSinglesSearcherService : Matchnet.RemotingServices.RemotingServiceBase
    {
        public const string SERVICE_NAME = "Spark.BlackSinglesSearcher.Service";
        SearcherSM _searcherSM;

        public BlackSinglesSearcherService()
        {            
            InitializeComponent();
        }
        
        protected override void RegisterServiceManagers()
        {
            try
            {
                _searcherSM = new SearcherSM(GetCommunity());
                base.RegisterServiceManager(_searcherSM);
            }
            catch (Exception ex)
            {
                Trace.Write(SERVICE_NAME + " - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                //System.Threading.Thread.Sleep(300000);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
        }

        public static string GetCommunity()
        {
            string community = "BlackSingles";
            return community;
        }
    }
}
