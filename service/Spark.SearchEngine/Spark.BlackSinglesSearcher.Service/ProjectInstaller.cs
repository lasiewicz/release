﻿using System.ComponentModel;
using System.Configuration.Install;


namespace Spark.BlackSingles.Searcher.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
