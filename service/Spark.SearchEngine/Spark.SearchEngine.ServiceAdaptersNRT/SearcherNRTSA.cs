﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Spark.SearchEngine.ValueObjects;
using Matchnet.Exceptions;
using Spark.SearchEngine.ServiceInterfaceNRT;
using Matchnet.Configuration.ServiceAdapters;
using System.Collections;
using Matchnet;
using Matchnet.Content.ValueObjects.Region;
using Spark.Common.AccessService;
using Matchnet.MembersOnline.ServiceAdapters;

namespace Spark.SearchEngine.ServiceAdaptersNRT
{
    public class SearcherNRTSA : SABase
    {
        public static readonly SearcherNRTSA Instance = new SearcherNRTSA();

        public string TestURI { get; set; }

        private SearcherNRTSA() { }

        /// <summary>
        /// Updates member in our search index via NRT, this will get latest Member info (ignores cache), so if the caller already has updated member info, use the overloaded method instead
        /// </summary>
        /// <param name="searchMemberUpdate"></param>
        /// <param name="brandID"></param>
        public void NRTUpdateMember(SearchMemberUpdate searchMemberUpdate, int brandID)
        {
            NRTUpdateMember(searchMemberUpdate, brandID, null);
        }

        /// <summary>
        /// Updates member in our search index via NRT, uses member info passed in as CachedMember
        /// </summary>
        /// <param name="searchMemberUpdate"></param>
        /// <param name="brandID"></param>
        /// <param name="cachedMember"></param>
        public void NRTUpdateMember(SearchMemberUpdate searchMemberUpdate, int brandID, Matchnet.Member.ValueObjects.CachedMember cachedMember)
        {
            try
            {
                if (searchMemberUpdate != null && searchMemberUpdate.CommunityID > 0 && searchMemberUpdate.MemberID > 0 && HasSearcher(searchMemberUpdate.CommunityID))
                {
                    bool useNRT = Boolean.TrueString.ToLower().Equals(GetSetting("USE_E2_NRT", searchMemberUpdate.CommunityID, "false").ToLower());
                    if (useNRT && searchMemberUpdate.UpdateReason == UpdateReasonEnum.logon)
                    {
                        useNRT = Boolean.TrueString.ToLower().Equals(GetSetting("ENABLE_E2_NRT_LOGON", searchMemberUpdate.CommunityID, "true").ToLower());
                    }

                    if (useNRT)
                    {
                        if (searchMemberUpdate.UpdateMode == UpdateModeEnum.remove)
                        {
                            NRTRemoveMember(searchMemberUpdate);
                        }
                        else
                        {
                            //Validate - suspend/hide/fraud
                            bool isValidMember = true;
                            Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
                            Matchnet.Member.ServiceAdapters.Member member = null;
                            if (brand == null)
                            {
                                throw new Exception("Brand does not exist. BrandID: " + brandID.ToString());
                            }

                            if (cachedMember != null)
                            {
                                member = new Matchnet.Member.ServiceAdapters.Member(cachedMember);
                            }
                            else if (searchMemberUpdate.MemberID > 0)
                            {
                                member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(searchMemberUpdate.MemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.IngoreSACache);
                            }

                            if (member != null)
                            {
                                //self-suspended flag
                                if (member.GetAttributeInt(brand, "SelfSuspendedFlag", 0) == 1)
                                {
                                    isValidMember = false;
                                }
                                //hidemask - hidden from search
                                else if ((member.GetAttributeInt(brand, "HideMask", 0) & 1) == 1)
                                {
                                    isValidMember = false;
                                }
                                //admin suspend
                                else if ((member.GetAttributeInt(brand, "GlobalStatusMask", 0) & 1) == 1)
                                {
                                    isValidMember = false;
                                }
                                //ftaapproved (fraud)
                                else if (member.GetAttributeInt(brand, "FTAApproved", 1) == 0)
                                {
                                    isValidMember = false;
                                }
                            }

                            if (isValidMember)
                            {
                                //Determine whether search data will be retrieved from SearchStore/SearchLoad DB
                                bool useDB = (searchMemberUpdate.UpdateMode == UpdateModeEnum.add) ? true : false;
                                if (searchMemberUpdate.UpdateMode == UpdateModeEnum.add)
                                {
                                    useDB = Boolean.TrueString.ToLower().Equals(GetSetting("USE_DB_FOR_E2_NRT_ADDS", "false").ToLower());
                                }
                                else
                                {
                                    useDB = Boolean.TrueString.ToLower().Equals(GetSetting("USE_DB_FOR_E2_NRT_UPDATES", "false").ToLower());
                                }

                                SearchMember searchMember = null;
                                if (!useDB && member != null)
                                {
                                    searchMember = CreateSearchMember(searchMemberUpdate, brand, member);
                                }

                                //Update all instances in all partitioons used by the community
                                List<string> uriList = getServiceManagerUriAllInstances(searchMemberUpdate.CommunityID);
                                foreach (string uri in uriList)
                                {
                                    base.Checkout(uri);
                                    try
                                    {
                                        if (useDB)
                                        {
                                            getService(uri).NRTUpdateMemberByDB(searchMemberUpdate);
                                        }
                                        else if (searchMember != null)
                                        {
                                            getService(uri).NRTUpdateMember(searchMember);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        //ignore, these API calls are OneWay, if service instance is down, we'll skip it
                                    }
                                    finally
                                    {
                                        base.Checkin(uri);
                                    }
                                }
                            }
                            else
                            {
                                //TBD - Not sure if we want to automatically remove based on validation
                                //searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
                                //searchMemberUpdate.UpdateReason = UpdateReasonEnum.updateChangedToRemove;
                                //NRTRemoveMember(searchMemberUpdate);
                            }
                        }
                    }
                }
                else
                {
                    //throw new Exception("NRTUpdateMember(): Missing community ID or MemberID");
                }
            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
        }

        public void NRTRemoveMember(SearchMemberUpdate searchMemberUpdate)
        {
            try
            {
                if (searchMemberUpdate != null && searchMemberUpdate.CommunityID > 0 && HasSearcher(searchMemberUpdate.CommunityID))
                {
                    bool useNRT = Boolean.TrueString.ToLower().Equals(GetSetting("USE_E2_NRT", searchMemberUpdate.CommunityID, "false").ToLower());
                    if (useNRT)
                    {
                        List<string> uriList = getServiceManagerUriAllInstances(searchMemberUpdate.CommunityID);
                        foreach (string uri in uriList)
                        {
                            base.Checkout(uri);
                            try
                            {
                                searchMemberUpdate.UpdateMode = UpdateModeEnum.remove;
                                getService(uri).NRTRemoveMember(searchMemberUpdate);
                            }
                            catch (Exception e)
                            {
                                //ignore, these API calls are OneWay, if service instance is down, we'll skip it
                            }
                            finally
                            {
                                base.Checkin(uri);
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("NRTRemoveMember(): Missing community ID");
                }
            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
        }

        private ISearcherNRT getService(string uri)
        {
            try
            {
                return (ISearcherNRT)Activator.GetObject(typeof(ISearcherNRT), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        private string getServiceManagerUri(int communityId)
        {
            try
            {
                if (!string.IsNullOrEmpty(TestURI))
                {
                    return TestURI;
                }
                bool useSearcherByCommunity = Boolean.TrueString.ToLower().Equals(GetSetting("SEARCHER_PER_COMMUNITY", "false"));
                //if flag set use community id passed in, otherwise default to sparksearcher for all communities
                int cid = (useSearcherByCommunity) ? communityId : (int)ServiceConstants.COMMUNITY_ID.Spark;
                string communityNameUpper = ServiceConstants.GetCommunityName(cid).ToUpper();
                string serviceSearcherConst = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, communityNameUpper);
                string uri = AdapterConfigurationSA.GetServicePartition(serviceSearcherConst, PartitionMode.Random).ToUri(ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST);
                string overrideHostName = GetSetting(string.Format(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, communityNameUpper), "");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private List<string> getServiceManagerUriAllInstances(int communityId)
        {
            try
            {
                List<string> uriList = new List<string>();
                if (!string.IsNullOrEmpty(TestURI))
                {
                    uriList.Add(TestURI);
                }
                else
                {
                    bool useSearcherByCommunity = Boolean.TrueString.ToLower().Equals(GetSetting("SEARCHER_PER_COMMUNITY", "false"));
                    //if flag set use community id passed in, otherwise default to sparksearcher for all communities
                    int cid = (useSearcherByCommunity) ? communityId : (int)ServiceConstants.COMMUNITY_ID.Spark;
                    string communityNameUpper = ServiceConstants.GetCommunityName(cid).ToUpper();
                    string serviceSearcherConst = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, communityNameUpper);

                    ArrayList servicePartitionList = AdapterConfigurationSA.GetServicePartitions(serviceSearcherConst);
                    foreach (Matchnet.Configuration.ValueObjects.ServicePartition servicePartition in servicePartitionList)
                    {
                        for (int i = 0; i < servicePartition.Count; i++)
                        {
                            Matchnet.Configuration.ValueObjects.ServiceInstance serviceInstance = servicePartition[i];
                            uriList.Add("tcp://" + serviceInstance.HostName + ":" + servicePartition.Port + "/" + ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST + ".rem");
                        }

                    }

                    string uri = "";
                    if (uriList.Count > 0)
                    {
                        uri = uriList[0];
                    }
                    string overrideHostName = GetSetting(string.Format(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, communityNameUpper), "");
                    if (overrideHostName.Length > 0 && !string.IsNullOrEmpty(uri))
                    {
                        uriList.Clear();
                        UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                        uriList.Add("tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path);
                    }
                }
                return uriList;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            try
            {
                base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPARKSEARCHER_SA_CONNECTION_LIMIT"));
            }
            catch (Exception ex)
            {
                base.MaxConnections = 50;
            }
        }


        public string GetSetting(string name, string defaultvalue)
        {
            try
            {
                return RuntimeSettings.GetSetting(name);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

        public string GetSetting(string name, int communityID, string defaultvalue)
        {
            try
            {
                return RuntimeSettings.GetSetting(name, communityID);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

        private SearchMember CreateSearchMember(SearchMemberUpdate searchMemberUpdate, Brand brand, Matchnet.Member.ServiceAdapters.Member member)
        {
            SearchMember searchMember = null;

            if (member == null)
            {
                throw new Exception("CreateSearchMember() error: Member is null");
            }

            if (brand == null || brand.Site.Community.CommunityID != searchMemberUpdate.CommunityID)
            {
                throw new Exception("CreateSearchMember() error: brand does exist for communityID " + searchMemberUpdate.CommunityID.ToString());
            }

            if (member != null)
            {
                DateTime lastActiveDate = member.GetLastLogonDate(searchMemberUpdate.CommunityID);
                //test only
                lastActiveDate = DateTime.Now;

                //get location info
                double longitude = 0;
                double latitude = 0;
                int depth1RegionID = Constants.NULL_INT;
                int depth2RegionID = Constants.NULL_INT;
                int depth3RegionID = Constants.NULL_INT;
                int depth4RegionID = Constants.NULL_INT;
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(brand, "RegionID", Constants.NULL_INT), (Int32)Matchnet.Language.English);
                if (regionLanguage != null)
                {
                    longitude = (double)regionLanguage.Longitude;
                    latitude = (double)regionLanguage.Latitude;
                    depth1RegionID = regionLanguage.CountryRegionID;
                    depth2RegionID = regionLanguage.StateRegionID;
                    depth3RegionID = regionLanguage.CityRegionID;
                    depth4RegionID = regionLanguage.PostalCodeRegionID;
                }

                //get MOL info
                int isOnline = 0;
                if ((member.GetAttributeInt(brand, "HideMask", 0) & 4) != 4)
                {
                    if (searchMemberUpdate.UpdateReason == UpdateReasonEnum.logon || searchMemberUpdate.UpdateReason == UpdateReasonEnum.online)
                    {
                        isOnline = 1;
                    }
                    else if (searchMemberUpdate.UpdateReason == UpdateReasonEnum.offline)
                    {
                        isOnline = 0;
                    }
                    else
                    {
                        //get from MOL
                        isOnline = (MembersOnlineSA.Instance.IsMemberOnline(brand.Site.Community.CommunityID, member.MemberID)) ? 1 : 0;
                    }
                }

                searchMember = SearchMember.GenerateSearchMember(searchMemberUpdate.MemberID,
                    searchMemberUpdate.CommunityID,
                    member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT),
                    member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue),
                    member.GetInsertDate(searchMemberUpdate.CommunityID),
                    lastActiveDate,
                    member.GetAttributeInt(brand, "HasPhotoFlag", 0),
                    member.GetAttributeInt(brand, "EducationLevel", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "Religion", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "LanguageMask", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "Ethnicity", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "SmokingHabits", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "DrinkingHabits", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "Height", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "MaritalStatus", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "JDateReligion", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "JDateEthnicity", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "SynagogueAttendance", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "KeepKosher", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "SexualIdentityType", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "RelationshipMask", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "RelationshipStatus", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "BodyType", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "Zodiac", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "MajorType", Constants.NULL_INT),
                    depth1RegionID,
                    depth2RegionID,
                    depth3RegionID,
                    depth4RegionID,
                    longitude,
                    latitude,
                    member.GetAttributeInt(brand, "SchoolID", Constants.NULL_INT),
                    getAreaCode(regionLanguage),
                    member.GetAttributeInt(brand, "CommunityEmailCount", Constants.NULL_INT),
                    DateTime.Now,
                    member.GetAttributeInt(brand, "Weight", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "RelocateFlag", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "ChildrenCount", Constants.NULL_INT),
                    GetCommunitySubscriptionExpiration(searchMemberUpdate.CommunityID, member),
                    member.GetAttributeInt(brand, "ActivityLevel", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "Custody", Constants.NULL_INT),
                    member.GetAttributeInt(brand, "MoreChildrenFlag", Constants.NULL_INT),
                    GetSearchColorCode(member, brand),
                    isOnline,
                    member.GetAttributeInt(brand, "RamahAlum", Constants.NULL_INT));

                searchMember.UpdateMode = searchMemberUpdate.UpdateMode;
                searchMember.UpdateReason = searchMemberUpdate.UpdateReason;
            }

            return searchMember;
        }

        private int GetSearchColorCode(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            int color = 0;
            if (member != null && brand != null)
            {
                string attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, "ColorCodePrimaryColor", "");
                if (!String.IsNullOrEmpty(attributeText))
                {
                    switch (attributeText.ToLower())
                    {
                        case "white":
                            color = 1;
                            break;
                        case "blue":
                            color = 2;
                            break;
                        case "yellow":
                            color = 4;
                            break;
                        case "red":
                            color = 8;
                            break;
                    }
                }
            }
            return color;
        }

        private string getAreaCode(RegionLanguage regionLanguage)
        {
            string areaCode = string.Empty;

            if (regionLanguage != null)
            {
                Int32 cityRegionID = regionLanguage.CityRegionID;

                RegionAreaCodeDictionary dictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();

                if (dictionary.Contains(cityRegionID))
                {
                    ArrayList areaCodes = dictionary[cityRegionID];

                    if (areaCodes.Count == 1)
                    {
                        areaCode = areaCodes[0].ToString();
                    }
                    else
                    {
                        //If more than one, use lowest area code in region since user can only have one
                        Int32 maxAreaCode = 0;

                        foreach (Int32 areaCodeNum in areaCodes)
                        {
                            if (areaCodeNum > maxAreaCode)
                            {
                                maxAreaCode = areaCodeNum;
                            }
                        }

                        areaCode = maxAreaCode.ToString();
                    }
                }
            }
            return areaCode;
        }

        public DateTime GetCommunitySubscriptionExpiration(int communityID, Matchnet.Member.ServiceAdapters.Member member)
        {
            //getSiteIDList
            DateTime subscriptionExpiration = DateTime.MinValue;
            Brands brands = BrandConfigSA.Instance.GetBrands();
            Site[] sites = brands.GetSites();

            for (Int32 siteNum = 0; siteNum < sites.Length; siteNum++)
            {
                Site site = sites[siteNum];
                if (site.Community.CommunityID == communityID)
                {
                    AccessPrivilege ap = member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, Constants.NULL_INT, site.SiteID, Constants.NULL_INT);
                    if (ap != null)
                    {
                        if (ap.EndDatePST > subscriptionExpiration)
                        {
                            subscriptionExpiration = ap.EndDatePST;
                        }
                    }
                }
            }

            return subscriptionExpiration;
        }

        public bool HasSearcher(int communityID)
        {
            bool hasSearcher = false;
            switch (communityID)
            {
                case (int)ServiceConstants.COMMUNITY_ID.Spark:
                case (int)ServiceConstants.COMMUNITY_ID.BBWPersonalsPlus:
                case (int)ServiceConstants.COMMUNITY_ID.BlackSingles:
                case (int)ServiceConstants.COMMUNITY_ID.ItalianSingles:
                case (int)ServiceConstants.COMMUNITY_ID.Jdate:
                case (int)ServiceConstants.COMMUNITY_ID.Cupid:
                    hasSearcher = true;
                    break;
            }

            return hasSearcher;
        }
    }
}
