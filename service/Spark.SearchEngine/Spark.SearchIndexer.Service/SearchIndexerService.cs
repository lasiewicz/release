﻿using System;
using System.Diagnostics;
using Spark.SearchEngine.ServiceManager;
using Matchnet.Exceptions;
namespace Spark.SearchIndexer.Service
{
    public partial class SearchIndexerService : Matchnet.RemotingServices.RemotingServiceBase
    {
        public const string SERVICE_NAME = "Spark.SearchIndexer.Service";
        LoaderSM _loaderSM;
        public SearchIndexerService()
        {
            InitializeComponent();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {

                _loaderSM = new LoaderSM();
                base.RegisterServiceManager(_loaderSM);
               
            }
            catch (Exception ex)
            {
                Trace.Write(SERVICE_NAME + " - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }


        protected override void OnStart(string[] args)
        {
            try
            {
               // System.Threading.Thread.Sleep(20000);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
