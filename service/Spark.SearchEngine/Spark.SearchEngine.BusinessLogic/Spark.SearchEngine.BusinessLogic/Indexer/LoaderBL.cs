﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Threading;
using Lucene.Net.Store;
using Spark.SearchEngine.BusinessLogic.Documents;
using Spark.SearchEngine.ValueObjects;
using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Caching;
using Spark.Logging;
using System.Data;
using System.Diagnostics;
using Lucene.Net.Index;
using Matchnet.MembersOnline.ServiceAdapters;

namespace Spark.SearchEngine.BusinessLogic.Indexer
{
    public class LoaderBL
    {
        #region debug code
#if DEBUG
        private string searchLoadConnStr = "Data Source=lasqlstage202.sparkstage.com;Initial Catalog=mnSearchStore;Persist Security Info=True;User ID=test;Password=test123;Asynchronous Processing=true";
        private bool _useNUnit = false;
        Dictionary<int, StringBuilder> _communityStringBuilders = new Dictionary<int, StringBuilder>();

        public bool USE_NUNIT
        {
            get { return _useNUnit; }
            set { _useNUnit = value; }
        }

        private StringBuilder GetCommunityStringBuilder(int communityId)
        {
            lock (_communityStringBuilders)
            {
                if (!_communityStringBuilders.ContainsKey(communityId))
                {
                    _communityStringBuilders[communityId] = new StringBuilder();
                }
            }
            return _communityStringBuilders[communityId];
        }
#endif
        #endregion

        public const int  MAX_ROLLING_DIRS=3;
        Dictionary<int, Stopwatch> _communityStopwatch = new Dictionary<int, Stopwatch>();
        Dictionary<int, Counter> _communityMemberCount = new Dictionary<int, Counter>();
        String APP_NAME = "Spark.SearchEngine";
        public static readonly string CLASS_NAME = "LoaderBL";
        private IProcessorFactory iProcessorFactory = null;
        
        IndexWriter _indexWriter;
       public readonly static LoaderBL Instance = new LoaderBL();

        private LoaderBL()
        {
            try
            {}
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, ex);
                throw (new BLException(ex));
               
            }
        }

        public void setIProcessorFactory(IProcessorFactory factory)
        {
            this.iProcessorFactory = factory;
        }

        public IProcessorFactory GetIProcessorFactory()
        {            
            if (null == iProcessorFactory)
            {
                return IndexProcessorFactory.Instance;    
            }
            return iProcessorFactory;
        }

        public Stopwatch GetStopwatchForCommunity(int communityId)
        {
            lock (_communityStopwatch)
            {
                if (!_communityStopwatch.ContainsKey(communityId))
                {
                    _communityStopwatch[communityId] = new Stopwatch();
                }
            }
            return _communityStopwatch[communityId];
        }

        public Counter GetCommunityMemberCount(int communityId)
        {
            lock (_communityMemberCount)
            {
                if (!_communityMemberCount.ContainsKey(communityId))
                {
                    _communityMemberCount[communityId] = new Counter();
                }
            }
            return _communityMemberCount[communityId];
        }

        private int GetNumberOfPartitions()
        {
            string numOfPartitionsStr = Utils.GetSetting("INDEXER_NUM_OF_PARTITIONS", "157");
            return Convert.ToInt32(numOfPartitionsStr);
        }

        public CommunityIndexConfig GetCommunityConfig(int communityId, IProcessorType iProcessorType)
        {
            CommunityIndexConfig config = null;
            List<CommunityIndexConfig> _communityConfigs = GetCommunityIndexConfigs();
            if(null != _communityConfigs){
                config = _communityConfigs.Find(delegate(CommunityIndexConfig c) { return c.CommunityID == communityId && c.IProcessorType == iProcessorType; });
            }
            return config;
        }

        private List<CommunityIndexConfig> GetCommunityIndexConfigs()
        {
            CommunityIndexConfigCollection _cachedCommunityConfigs = Cache.Instance.Get(CommunityIndexConfigCollection.GetCacheKeyString()) as CommunityIndexConfigCollection;
            if (null == _cachedCommunityConfigs)
            {
                initConfigs();
                _cachedCommunityConfigs = Cache.Instance.Get(CommunityIndexConfigCollection.GetCacheKeyString()) as CommunityIndexConfigCollection;
            }
            List<CommunityIndexConfig> _communityConfigs = _cachedCommunityConfigs.CommunityIndexConfigs;
            return _communityConfigs;
        }

        //this is called once on Start
        public void Start()
        {
            try
            {
                GetCommunityIndexConfigs();
                foreach (CommunityIndexConfig c in GetCommunityIndexConfigs())
                {

                    string path = c.IndexPath;
                    if (c.EnableRollingUpdates)
                    {
                        path = CalculateIndexPath(c);                        
                        IndexRoller.CleanDir(path);
                    } 
                    Index(c, path);

                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Start", ex, null);
                throw (new BLException(ex));
            }
        }

        private void Index(CommunityIndexConfig c, string path)
        {
            Hashtable h = new Hashtable();
            h["config"] = c;
            h["path"] = path;
            Thread indexCommunity = null;

            indexCommunity = new Thread(IndexCommunityWithThreads);

            indexCommunity.IsBackground = true;
            indexCommunity.Start(h);
        }

        public void IndexCommunityWithThreads(object obj)
        {
            IDataReader dataReader = null;
            try
            {
                Hashtable h = (Hashtable) obj;
                CommunityIndexConfig cic = (CommunityIndexConfig)h["config"];
                string path = (string) h["path"];

                FSDirectory directory = FSDirectory.Open(path);
                IndexWriter indexWriter = new IndexWriter(directory, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);
                indexWriter.UseCompoundFile = cic.UseCompoundFile;
                indexWriter.MergeFactor = cic.MergeFactor;
                indexWriter.SetRAMBufferSizeMB(cic.MaxMBBufferSize);

                DocumentMetadata documentMetadata = Serialization.FromXmlFile<DocumentMetadata>(cic.MetadataFile);

                DateTime indexStartTime = DateTime.Now;
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                int totalRowCount = 0;

                bool useSearchStoreDB = false;
                dataReader = GetCommunitySearchMembersByReader(cic);
                
                stopWatch.Stop();

                Matchnet.MembersOnline.ValueObjects.MOLCommunity molCommunityAtBegin = null;
                if (cic.EnableMOLUpdate)
                {
                    //get mol data at start of new index cycle
                    molCommunityAtBegin = MembersOnlineSA.Instance.GetMOLCommunity(cic.CommunityID, true);
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, String.Format("Get MOL Community data at start of index process. CommunityID: {0}, Count: {1}", cic.CommunityID, (molCommunityAtBegin != null) ? molCommunityAtBegin.Count().ToString() : "null"), null);
                }

                if (null != dataReader && ((SqlDataReader)dataReader).HasRows)
                {
                    //process new index cycle
                    dataReader.Read();
                    totalRowCount = (dataReader.IsDBNull(dataReader.GetOrdinal("TotalActiveMembers"))) ? 50000 : dataReader.GetInt32(dataReader.GetOrdinal("TotalActiveMembers"));
                    dataReader.NextResult();

                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME,
                                                              string.Format("Retrieved {0} members in {1} minutes for {2} index communityId={3} from {4}",
                                                              totalRowCount, (((double)stopWatch.ElapsedMilliseconds / (double)1000) / (double)60), cic.IProcessorType, cic.CommunityID, useSearchStoreDB ? "SearchStore DB" : "SearchLoad DB"), null);
                    stopWatch.Reset();
                    stopWatch.Start();
                    int maxProcessorSize = (totalRowCount/cic.IndexerThreads) + 1;

                    while (dataReader.Read())
                    {
                        IProcessor mip = GetIProcessorFactory().GetIProcessor(cic.CommunityID, documentMetadata, indexWriter, maxProcessorSize, cic.IProcessorType);
                        if (cic.EnableMOLUpdate)
                        {
                            if (mip.MOLCommunity == null)
                            {
                                mip.MOLCommunity = molCommunityAtBegin;
                            }
                        }
                        mip.AddDataRow(dataReader);
                    }

                    //start last processor if it isn't started
                    GetIProcessorFactory().StartProcessors(cic.CommunityID, cic.IProcessorType);
                                       
                    //stop indexing if processors are all done or if time limit is reached (indexing interval +80 minutes)
                    int processingTimeLimit = cic.RollingIndexInterval+80;
                    DateTime nextindexdate = DateTime.Now.AddMinutes(Conversion.CDouble(processingTimeLimit.ToString()));
                    while (!GetIProcessorFactory().AreAllDone(cic.CommunityID, cic.IProcessorType) && DateTime.Now < nextindexdate)
                    {
                        Thread.Sleep(500);
                    }
                    stopWatch.Stop();
                    if (GetIProcessorFactory().AreAllDone(cic.CommunityID, cic.IProcessorType))
                    {
                        //get mol data again for delta updates at end of indexing cycle
                        int molDeltaOnline = 0;
                        int molDeltaOffline = 0;
                        Matchnet.MembersOnline.ValueObjects.MOLCommunity molCommunityAtEnd = null;

                        if (cic.EnableMOLUpdate)
                        {
                            molCommunityAtEnd = MembersOnlineSA.Instance.GetMOLCommunity(cic.CommunityID, true);
                            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, String.Format("Get MOL Community data at end of index process. CommunityID: {0}, Count: {1}", cic.CommunityID, (molCommunityAtEnd != null) ? molCommunityAtEnd.Count().ToString() : "null"), null);

                            if (molCommunityAtEnd != null && molCommunityAtEnd.Count() > 0 && molCommunityAtBegin != null && molCommunityAtBegin.Count() > 0)
                            {
                                //update online status for deltas, members that came online after we started index process
                                foreach (Matchnet.MembersOnline.ValueObjects.MOLMember mm in molCommunityAtEnd)
                                {
                                    if (!molCommunityAtBegin.ContainsMember(mm.MemberId))
                                    {
                                        //create new document
                                        SearchDocument memberDoc = GetSearchDocumentPostMOLUpdate(mm.MemberId, cic.CommunityID, documentMetadata, true);

                                        if (memberDoc != null)
                                        {
                                            //update index document
                                            indexWriter.UpdateDocument(new Term("id", mm.MemberId.ToString()), memberDoc.LuceneDocument);
                                            molDeltaOnline++;
                                        }
                                    }
                                }

                                //update offline status for deltas, members that were online when we started, but are no longer online
                                foreach (Matchnet.MembersOnline.ValueObjects.MOLMember mm in molCommunityAtBegin)
                                {
                                    if (!molCommunityAtEnd.ContainsMember(mm.MemberId))
                                    {
                                        //create new document
                                        SearchDocument memberDoc = GetSearchDocumentPostMOLUpdate(mm.MemberId, cic.CommunityID, documentMetadata, false);

                                        if (memberDoc != null)
                                        {
                                            //update index document
                                            indexWriter.UpdateDocument(new Term("id", mm.MemberId.ToString()), memberDoc.LuceneDocument);
                                            molDeltaOffline++;
                                        }
                                    }
                                }
                            }
                        }

                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                                  string.Format("Processed and indexed {0} members in {1} minutes for {2} index communityId={3}. MOL Post Delta Updates Online={4}, Offline={5}.",
                                                                      totalRowCount, (((double)stopWatch.ElapsedMilliseconds / (double)1000) / (double)60), cic.IProcessorType, cic.CommunityID, molDeltaOnline, molDeltaOffline), null);
                        stopWatch.Reset();

                        //optimize index
                        stopWatch.Start();
                        try
                        {
                            indexWriter.Optimize();
                        }
                        catch (Exception iwe)
                        {
                            RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, iwe);
                        }
                        finally
                        {
                            indexWriter.Dispose();
                        }

                        stopWatch.Stop();
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                                  string.Format("{0} index optimized in {1} minutes for communityId={2}",
                                                                      cic.IProcessorType,(((double)stopWatch.ElapsedMilliseconds / (double)1000) / (double)60), cic.CommunityID),
                                                                  null);
                        //update the last index date
                        //cic.NextIndexDate = cic.LastIndexDate;
                        cic.LastIndexDate = indexStartTime;
                        updateLastIndexDate(cic);
#if DEBUG
                        if (!USE_NUNIT)
                        {
#endif
                            //copy index to search servers
                            Thread copyThread = new Thread(copyIndex);
                            copyThread.IsBackground = true;
                            copyThread.Start(h);
#if DEBUG
                        }
#endif

                        //clear out processors
                        GetIProcessorFactory().ResetCommunity(cic.CommunityID, cic.IProcessorType);
                    }
                    else
                    {

                        RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                                                      string.Format("{0} indexing failed to complete in {1} minutes for communityId={2}", cic.IProcessorType, processingTimeLimit, cic.CommunityID),
                                                                                      null);
                        //clear out the unfinished processors for this community
                        GetIProcessorFactory().ResetCommunity(cic.CommunityID, cic.IProcessorType);
                    }
                }
                else
                {
                    
                    RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                                                  string.Format("No members found for {0} index communityId={1}", cic.IProcessorType, cic.CommunityID),
                                                                                  new BLException(""),
                                                                                  null);
                }
             
#if DEBUG
                if (!USE_NUNIT)
                {
#endif
                    //start again                       
                    Thread roller = new Thread(RollingCommunityIndex);
                    roller.IsBackground = true;
                    Hashtable hash = new Hashtable();
                    hash["communityId"] = cic.CommunityID;
                    hash["iProcessorType"] = cic.IProcessorType;
                    roller.Start(hash);
#if DEBUG
                }
#endif
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexCommunityWithThreads", ex, null);
                throw (new BLException(ex));
            }
            finally
            {
                if (null != dataReader && !dataReader.IsClosed)
                {
                    dataReader.Close();
                }
            }          
        }

        #region public ILoader implementations
        //indexes community if swapping directory index is enabled
        public void RollingCommunityIndex(object obj)
        {
            Hashtable hash = (Hashtable)obj;
            int communityid = (int)hash["communityId"];
            IProcessorType iProcessorType = (IProcessorType)hash["iProcessorType"];

            CommunityIndexConfig c = GetCommunityConfig(communityid, iProcessorType);            
            try
            {
                DateTime now = DateTime.Now;
                DateTime nextindexdate = now.AddMinutes(Conversion.CDouble(c.RollingIndexInterval.ToString()));
                var culture = CultureInfo.CreateSpecificCulture("en-US");
                var styles = DateTimeStyles.None;
                while (DateTime.Now < nextindexdate)
                {
                    Thread.Sleep(30000);

                    // if this indexer has OnDemand index enabled, check against the DB to see if we should run the indexer right now
                    if(c.OnDemandIndexEnabled)
                    {
                        // since we capture no state information regarding if the indexer ran due to this onDemand value, we will only honor
                        // values that are within last 2 minutes. if it falls outside this 2 min window, we will assume we ran this already due
                        // to this onDemand datetime value.
                        var onDemandIndexDateTime = Utils.SettingsService.GetSettingFromSingleton(c.OnDemandIndexDateSettingConstant, c.CommunityID);
                        var dateTimeResult = DateTime.MinValue;
                        if(!string.IsNullOrEmpty(onDemandIndexDateTime) && DateTime.TryParse(onDemandIndexDateTime, culture, styles, out dateTimeResult) &&
                            DateTime.Now > dateTimeResult)
                        {
                            var difference = DateTime.Now.Subtract(dateTimeResult);
                            if(difference.Minutes < 2)
                            {
                                break;
                            }
                        }
                    }
                }
                // LastIndexDate should be modified in the actual indexing portion of the code. it definitely shouldn't take on
                // the faulty value of NextIndexDate
                //c.LastIndexDate = c.NextIndexDate;

                string path = CalculateIndexPath(c);
                IndexRoller.CleanDir(path);

                Index(c, path);

                // startPartitionLoader does nothing; don't even bother calling. only after adding something useful there, enable this back
                // i have no idea why c.StopWatch is being started / reset when it's never used
                //if (Conversion.CBool(Utils.GetSetting("SEARCHINDEXER_INDEX_ONSTART", "true")))
                //{
                //    Thread loaderThread = new Thread(startPartitionLoader);
                //    loaderThread.Start(c);
                //    c.Stopwatch.Reset();
                //    c.Stopwatch.Start();
                //}

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexCommunity", ex, communityid.ToString());
                throw (new BLException(ex));
            }
        }

        private string CalculateIndexPath(CommunityIndexConfig c)
        {
            for (int i = 0; i < MAX_ROLLING_DIRS; i++)
            {
                string indexDirectoryInUse = Utils.IndexDirectoryInUse(c);
                if (!string.IsNullOrEmpty(indexDirectoryInUse))
                {
                    c.RollingIndexDirectoryID = (c.RollingIndexDirectoryID < MAX_ROLLING_DIRS) ? c.RollingIndexDirectoryID + 1 : 1;
                }
                else
                {
                    break;
                }
            }
            string path = c.IndexStagePath + c.RollingIndexDirectoryID.ToString();
            return path;
        }
        
        #endregion

        private void initConfigs()
        {
            string communitylist = string.Empty;
            try
            {
                communitylist = Utils.GetSetting("SEARCHER_COMMUNITY_LIST", "");
                List<CommunityIndexConfig> _communityConfigs = new List<CommunityIndexConfig>();
                if (String.IsNullOrEmpty(communitylist))
                {
                    RollingFileLogger.Instance.LogWarningMessage(APP_NAME, CLASS_NAME, "Found no communities for searcher", null);
                    return;
                }

                string[] communityList = communitylist.Split(new char[] { ';' });
                if (communityList==null || communityList.Length==0)
                {
                    RollingFileLogger.Instance.LogWarningMessage(APP_NAME, CLASS_NAME, "Cannot get communities for searcher:" + communitylist, null);
                    return;
                }

                for (int i = 0; i < communityList.Length; i++)
                {
                    int communityid = Conversion.CInt(communityList[i]);
                    CommunityIndexConfig config = createCommunityConfigForMemberIndex(communityid);
                    _communityConfigs.Add(config);
                    CommunityIndexConfig keywordConfig = createCommunityConfigForKeywordIndex(communityid);
                    _communityConfigs.Add(keywordConfig);
                    // todo: mike this is not ready so commenting it out
                    //CommunityIndexConfig adminConfig = createCommunityConfigForAdminIndex(communityid);
                    //_communityConfigs.Add(adminConfig);
                }

                CommunityIndexConfigCollection cachedCommunityIndexConfigs = new CommunityIndexConfigCollection(_communityConfigs);
                Cache.Instance.Add(cachedCommunityIndexConfigs);
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST,CLASS_NAME,"Refeshed configs in cache.",null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "initConfigs", ex, communitylist);
            }
        }

        private CommunityIndexConfig createCommunityConfigForMemberIndex(int communityid)
        {
            CommunityIndexConfig config = new CommunityIndexConfig();
            config.IProcessorType = IProcessorType.MemberIndexProcessor;
            string indexPath = Utils.GetSetting("SEARCHER_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\Index\\" + communityid.ToString() + "\\");
            string indexStagePath = Utils.GetSetting("INDEXER_STAGE_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\IndexStage\\" + communityid.ToString() + "\\");

            config.CommunityID = communityid;
            config.IndexPath = indexPath;
            config.IndexStagePath = indexStagePath;
            config.MetadataFile = AppDomain.CurrentDomain.BaseDirectory + "/" + Utils.GetSetting("SEARCHER_COMMUNITY_METADATA_FILE", communityid, "SearchDocument.xml");
            config.IndexerThreads = Conversion.CInt(Utils.GetSetting("INDEXER_COMMUNITY_THREADS", communityid, "1"));
            config.MergeFactor = Conversion.CInt(Utils.GetSetting("INDEXER_MERGE_FACTOR", communityid, "30"));
            config.MaxMergeDocs = Conversion.CInt(Utils.GetSetting("INDEXER_MAX_MERGE_DOCS", communityid, "100"));
            config.LastActiveDays = Conversion.CInt(Utils.GetSetting("INDEXER_ACTIVE_DAYS", communityid, "180"));
            config.EnableRollingUpdates = Conversion.CBool(Utils.GetSetting("ENABLE_INDEX_DIR_SWAP_UPDATES", communityid, "false"));
            config.RollingIndexInterval = Conversion.CInt(Utils.GetSetting("INDEXER_SWAPPING_INTERVAL", communityid, "1"));
            config.UseCompoundFile = Conversion.CBool(Utils.GetSetting("INDEXER_USE_COMPOUND_FILE", communityid, "false"));
            config.MaxMBBufferSize = Conversion.CInt(Utils.GetSetting("INDEXER_MAX_MB_BUFFER_SIZE", communityid, "48"));
            int id = 0;
            config.LastIndexDate = getLastIndexDate(communityid, out id, config.IProcessorType);
            config.RollingIndexDirectoryID = (id < MAX_ROLLING_DIRS) ? id + 1 : 1;
            for (int i = 0; i < MAX_ROLLING_DIRS; i++)
            {
                if (!string.IsNullOrEmpty(Utils.IndexDirectoryInUse(config)))
                {
                    config.RollingIndexDirectoryID = (id < MAX_ROLLING_DIRS) ? id + 1 : 1;
                }
                else
                {
                    break;
                }
            }
            config.EnableMOLUpdate = true;

            config.Metadata = Serialization.FromXmlFile<DocumentMetadata>(config.MetadataFile);
            return config;
        }

        private CommunityIndexConfig createCommunityConfigForKeywordIndex(int communityid)
        {
            CommunityIndexConfig config = new CommunityIndexConfig();
            config.IProcessorType=IProcessorType.KeywordIndexProcessor;
            string indexPath = Utils.GetSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\KeywordIndex\\" + communityid.ToString() + "\\");
            string indexStagePath = Utils.GetSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\KeywordIndexStage\\" + communityid.ToString() + "\\");

            config.CommunityID = communityid;
            config.IndexPath = indexPath;
            config.IndexStagePath = indexStagePath;
            config.MetadataFile = AppDomain.CurrentDomain.BaseDirectory + "/" + Utils.GetSetting("KEYWORD_SEARCHER_COMMUNITY_METADATA_FILE", communityid, "KeywordSearchDocument.xml");
            config.IndexerThreads = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_COMMUNITY_THREADS", communityid, "1"));
            config.MergeFactor = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_MERGE_FACTOR", communityid, "30"));
            config.MaxMergeDocs = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_MAX_MERGE_DOCS", communityid, "100"));
            config.LastActiveDays = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_ACTIVE_DAYS", communityid, "180"));
            config.EnableRollingUpdates = Conversion.CBool(Utils.GetSetting("ENABLE_KEYWORD_INDEX_DIR_SWAP_UPDATES", communityid, "false"));
            config.RollingIndexInterval = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_SWAPPING_INTERVAL", communityid, "1"));
            config.UseCompoundFile = Conversion.CBool(Utils.GetSetting("KEYWORD_INDEXER_USE_COMPOUND_FILE", communityid, "false"));
            config.MaxMBBufferSize = Conversion.CInt(Utils.GetSetting("KEYWORD_INDEXER_MAX_MB_BUFFER_SIZE", communityid, "48"));
            int id = 0;
            config.LastIndexDate = getLastIndexDate(communityid, out id, config.IProcessorType);
            config.RollingIndexDirectoryID = (id < MAX_ROLLING_DIRS) ? id + 1 : 1;
            for (int i = 0; i < MAX_ROLLING_DIRS; i++)
            {
                if (!string.IsNullOrEmpty(Utils.IndexDirectoryInUse(config)))
                {
                    config.RollingIndexDirectoryID = (id < MAX_ROLLING_DIRS) ? id + 1 : 1;
                }
                else
                {
                    break;
                }
            }
            //config.NextIndexDate = DateTime.Now;

            config.Metadata = Serialization.FromXmlFile<DocumentMetadata>(config.MetadataFile);

            return config;
        }

        private CommunityIndexConfig createCommunityConfigForAdminIndex(int communityid)
        {
            CommunityIndexConfig config = new CommunityIndexConfig();
            config.IProcessorType = IProcessorType.AdminIndexProcessor;
            string indexPath = Utils.GetSetting("ADMIN_SEARCHER_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\AdminIndex\\" + communityid.ToString() + "\\");
            string indexStagePath = Utils.GetSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", communityid, "c:\\\\Matchnet\\AdminIndexStage\\" + communityid.ToString() + "\\");

            config.CommunityID = communityid;
            config.IndexPath = indexPath;
            config.IndexStagePath = indexStagePath;
            config.MetadataFile = AppDomain.CurrentDomain.BaseDirectory + "/" + Utils.GetSetting("ADMIN_SEARCHER_COMMUNITY_METADATA_FILE", communityid, "AdminSearchDocument.xml");
            config.IndexerThreads = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_COMMUNITY_THREADS", communityid, "1"));
            config.MergeFactor = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_MERGE_FACTOR", communityid, "30"));
            config.MaxMergeDocs = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_MAX_MERGE_DOCS", communityid, "100"));
            //config.LastActiveDays = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_ACTIVE_DAYS", communityid, "180"));
            config.EnableRollingUpdates = Conversion.CBool(Utils.GetSetting("ENABLE_ADMIN_INDEX_DIR_SWAP_UPDATES", communityid, "false"));
            config.RollingIndexInterval = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_SWAPPING_INTERVAL", communityid, "1"));
            config.UseCompoundFile = Conversion.CBool(Utils.GetSetting("ADMIN_INDEXER_USE_COMPOUND_FILE", communityid, "false"));
            config.MaxMBBufferSize = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_MAX_MB_BUFFER_SIZE", communityid, "48"));
            int id = 0;
            config.LastIndexDate = getLastIndexDate(communityid, out id, config.IProcessorType);
            config.RollingIndexDirectoryID = (id < MAX_ROLLING_DIRS) ? id + 1 : 1;
            for (int i = 0; i < MAX_ROLLING_DIRS; i++)
            {
                if (!string.IsNullOrEmpty(Utils.IndexDirectoryInUse(config)))
                {
                    config.RollingIndexDirectoryID = (id < MAX_ROLLING_DIRS) ? id + 1 : 1;
                }
                else
                {
                    break;
                }
            }
            //config.NextIndexDate = DateTime.Now;

            config.Metadata = Serialization.FromXmlFile<DocumentMetadata>(config.MetadataFile);

            // new settings: if other indexers were to use this row blocks idea, they must create these settings too
            config.OnDemandIndexEnabled = Conversion.CBool(Utils.GetSetting("ADMIN_INDEXER_ONDEMAND_ENABLED", communityid, "true"));
            config.OnDemandIndexDateSettingConstant = "ADMIN_INDEXER_ONDEMAND_DATETIME";
            config.UseRowBlocks = Conversion.CBool(Utils.GetSetting("ADMIN_INDEXER_USE_ROWBLOCKS", communityid, "true"));
            config.RowBlockDateRangeInDays = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_ROWBLOCKS_DATE_RANGE_IN_DAYS", communityid, "30"));
            config.ProcTimeLimitPerRowBlockInMinutes = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_PROCESSOR_TIME_LIMIT", communityid, "180"));
            config.BrandLastLogonDateLookBack = Conversion.CInt(Utils.GetSetting("ADMIN_INDEXER_BRANDLASTLOGONDATE_LOOKBACK", communityid, "5"));
            return config;
        }

        private DataTable GetCommunitySearchMembersFromSearchStore(CommunityIndexConfig config)
        {
            DataTable dt = null;
            try
            {
                int count = 0;
                int partitions = GetNumberOfPartitions();
                for (int i = 0; i < partitions; i++)
                {
                    Command command = new Command(ServiceConstants.SEARCHSTORE_DB_NAME, "[up_mnSearchStore_OUT_byCommunity]", 0);

                    command.AddParameter("@Partition", SqlDbType.Int, ParameterDirection.Input, i);

                    command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, config.CommunityID);
                    if (!config.EnableRollingUpdates)
                    {
                        if (config.LastIndexDate > DateTime.MinValue)
                        {
                            command.AddParameter("@LastIndexDate", SqlDbType.DateTime, ParameterDirection.Input, config.LastIndexDate);
                        }
                    }
                    if (config.LastActiveDays > 0)
                    {
                        command.AddParameter("@LastActiveDays", SqlDbType.Int, ParameterDirection.Input, config.LastActiveDays);
                    }
                    DataTable tmpDt = Client.Instance.ExecuteDataTable(command);
                    if (null == dt)
                    {
                        dt = tmpDt;
                    }
                    else
                    {
                        //merge tables together
                        dt.Merge(tmpDt);
                    }
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Retrieved members from SearchStore DB for community " + config.CommunityID.ToString(), null);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMembersFromSearchStore()", ex, config);
            }
            return dt;
        }

        private DataTable GetCommunitySearchMembers(CommunityIndexConfig config)
        {
            DataTable dt = null;
            try
            {
                int count = 0;
                string getCommunityDataCommand = "[up_SearchLoad_OUT_byCommunity_full]";
                Command command = new Command(ServiceConstants.SEARCHLOAD_DB_NAME, getCommunityDataCommand, 0);

                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, config.CommunityID);
                command.DBTimeoutInSecs = Utils.GetSetting("INDEXER_DB_TIMEOUT", 90);
                if (!config.EnableRollingUpdates)
                {
                    if (config.LastIndexDate > DateTime.MinValue)
                    {
                        command.AddParameter("@LastIndexDate", SqlDbType.DateTime, ParameterDirection.Input, config.LastIndexDate);
                    }

                    //if (config.NextIndexDate > DateTime.MinValue)
                    //{
                    //    command.AddParameter("@NextIndexDate", SqlDbType.DateTime, ParameterDirection.Input, config.NextIndexDate);
                    //}
                }
                if (config.LastActiveDays > 0)
                {
                    command.AddParameter("@LastActiveDays", SqlDbType.Int, ParameterDirection.Input, config.LastActiveDays);
                }

                dt = Client.Instance.ExecuteDataTable(command);

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Retrieved members from SearchLoad DB for community " + config.CommunityID.ToString(), null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMembers()", ex, config);
            }
            return dt;
        }

        private IDataReader GetCommunitySearchMembersByReader(CommunityIndexConfig config)
        {
            IDataReader reader = null;
            try
            {
                int count = 0;
                string getCommunityDataCommand = "[up_SearchLoad_OUT_byCommunity_Reader]";
                Command command = new Command(ServiceConstants.SEARCHLOAD_DB_NAME, getCommunityDataCommand, 0);

                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, config.CommunityID);
                command.DBTimeoutInSecs = Utils.GetSetting("INDEXER_DB_TIMEOUT", 900);
                if (config.LastActiveDays > 0)
                {
                    command.AddParameter("@LastActiveDays", SqlDbType.Int, ParameterDirection.Input, config.LastActiveDays);
                }

                reader = Client.Instance.ExecuteReader(command);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMembersByReader()", ex, config);
            }
            return reader;
        }

        private IDataReader GetCommunitySearchMembersFromSearchStoreByReader(CommunityIndexConfig config)
        {
            IDataReader reader = null;
            try
            {
                Command command = new Command(ServiceConstants.SEARCHSTORE_DB_NAME, "[up_mnSearchStore_OUT_byCommunity_Reader]", 0);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, config.CommunityID);
                if (config.LastActiveDays > 0)
                {
                    command.AddParameter("@LastActiveDays", SqlDbType.Int, ParameterDirection.Input, config.LastActiveDays);
                }
                reader= Client.Instance.ExecuteReader(command);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMembersFromSearchStoreByReader()", ex, config);
            }
            return reader;
        }

        /// <summary>
        /// Using the From and To date range, search members are retrieved.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="from">Exclusive datetime</param>
        /// <param name="to">Inclusive datetime</param>
        /// <returns></returns>
        private DataTable GetCommunitySearchMembersBlocks(CommunityIndexConfig config, DateTime from, DateTime to)
        {
           
            DataTable dt = null;
            try
            {
                var command = new Command(ServiceConstants.ADMINSEARCH_DB_NAME, "dbo.up_AdminBlockSearchByCommunity", 0);
                //command.DBTimeoutInSecs = Utils.GetSetting("ADMIN_INDEXER_DB_TIMEOUT", 300);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, config.CommunityID);
                command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, from);
                command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, to);
                dt = Client.Instance.ExecuteDataTable(command);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMembersBlocks()", ex, config);
            }
            return dt;
            
            #region Fake DataTable
            //var fakeData = new DataTable();
            //var typeInt32 = Type.GetType("System.Int32");
            //var typeString = Type.GetType("System.String");
            ////var typeDecimal = Type.GetType("System.Decimal");
            //var typeDateTime = Type.GetType("System.DateTime");
            //var typeDouble = Type.GetType("System.Double");

            //fakeData.Columns.Add(new DataColumn("MemberID", typeInt32));
            //fakeData.Columns.Add(new DataColumn("CommunityID", typeInt32));
            //fakeData.Columns.Add(new DataColumn("LastActiveDate", typeDateTime));
            //fakeData.Columns.Add(new DataColumn("UpdateDate", typeDateTime));
            //fakeData.Columns.Add(new DataColumn("CommunityInsertDate", typeDateTime));

            //fakeData.Columns.Add(new DataColumn("Longitude", typeDouble));
            //fakeData.Columns.Add(new DataColumn("Latitude", typeDouble));
            //fakeData.Columns.Add(new DataColumn("Depth1RegionID", typeInt32));
            //fakeData.Columns.Add(new DataColumn("Depth2RegionID", typeInt32));
            //fakeData.Columns.Add(new DataColumn("Depth3RegionID", typeInt32));
            //fakeData.Columns.Add(new DataColumn("Depth4RegionID", typeInt32));
            
            //fakeData.Columns.Add(new DataColumn("IPAddress", typeString));
            //fakeData.Columns.Add(new DataColumn("Username", typeString));
            //fakeData.Columns.Add(new DataColumn("FirstName", typeString));
            //fakeData.Columns.Add(new DataColumn("LastName", typeString));
            

            //var timeDiff = DateTime.Now.Subtract(from);
            //int seed = (int)timeDiff.TotalDays/60;
            //seed--;
            //int dataChunkSize = 100000;

            //for (int i = 0; i < dataChunkSize; i++)
            //{
            //    var newRow = fakeData.NewRow();
            //    newRow["MemberID"] = (seed * dataChunkSize) + i;
            //    newRow["CommunityID"] = 3;
            //    newRow["LastActiveDate"] = DateTime.Now;
            //    newRow["UpdateDate"] = DateTime.Now;
            //    newRow["CommunityInsertDate"] = DateTime.Now.AddMonths(-1);

            //    // i got this data from someone in Los Angeles
            //    newRow["Longitude"] = -2.067981;
            //    newRow["Latitude"] = 0.594780;
            //    newRow["Depth1RegionID"] = 223;
            //    newRow["Depth2RegionID"] = 1538;
            //    newRow["Depth3RegionID"] = 3404470;
            //    newRow["Depth4RegionID"] = 9805713;

            //    newRow["IPAddress"] = "192.168.1.1";
            //    newRow["Username"] = "mcho";
            //    newRow["FirstName"] = "michael";
            //    newRow["LastName"] = "cho";

            //    fakeData.Rows.Add(newRow);
            //}

            //return fakeData;
            #endregion
        }

        private SearchDocument GetSearchDocumentPostMOLUpdate(int memberId, int communityId, DocumentMetadata documentMetadata, bool isOnline)
        {
            SearchDocument memberDoc = null;

            try
            {
                //query searchstore
                DataRow searchMemberDataRow = GetCommunitySearchMember(memberId, communityId);

                if (searchMemberDataRow != null)
                {
                    //create SearchMember
                    SearchMember searchMember = new SearchMember();
                    searchMember.Populate(searchMemberDataRow);
                    searchMember.Online = (isOnline) ? 1 : 0;
                    searchMember.UpdateDate = DateTime.Now;
                    searchMember.UpdateType = UpdateTypeEnum.all;

                    //create new document
                    memberDoc = new SearchDocument(documentMetadata);
                    memberDoc.CommunityName = ServiceConstants.GetCommunityName(searchMember.CommunityID);
                    memberDoc.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                    memberDoc.RegionLanguages = Utils.RegionLanguages;
                    memberDoc.Strategy = Utils.SpatialStrategy;
                    memberDoc.Context = Utils.SpatialContext;
                    memberDoc.Populate(searchMember, documentMetadata);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetSearchDocumentPostMOLUpdate()", ex, "memberID: " + memberId.ToString() + ",communityID: " + communityId.ToString() + ", isOnline: " + isOnline.ToString());
            }

            return memberDoc;
        }

        private DataRow GetCommunitySearchMember(int memberID, int communityID)
        {
            DataTable dt = null;
            DataRow searchMemberDataRow = null;
            try
            {
                if (memberID > 0 && communityID > 0)
                {
                    Command command = new Command(ServiceConstants.SEARCHLOAD_DB_NAME, "[up_SearchLoad_Get_Member]", 0);

                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);

                    dt = Client.Instance.ExecuteDataTable(command);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        searchMemberDataRow = dt.Rows[0];
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMember", "Member not found in " + ServiceConstants.SEARCHLOAD_DB_NAME + ". CommunityID:" + communityID.ToString() + ", MemberID:" + memberID.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "GetCommunitySearchMember()", ex, "memberID: " + memberID.ToString() + ",communityID: " + communityID.ToString());
            }
            return searchMemberDataRow;
        }
              
        private void copyIndex(object c)
        {
            try
            {
                Hashtable h = (Hashtable) c;
                CommunityIndexConfig config = (CommunityIndexConfig) h["config"];
                string path = (string) h["path"];
                if ((null != config) && Utils.GetSetting("INDEXER_COPY_INDEX", config.CommunityID, true))
                {
                    List<string> servers = Utils.getSearcherServers(config.CommunityID);
                    foreach (string server in servers)
                    {
                        string indexDirectoryInUseForServer = string.Empty;
                        //loop through the index directories and find an open one
                        for (int i = 0; i < MAX_ROLLING_DIRS; i++)
                        {
                            indexDirectoryInUseForServer = Utils.IndexDirectoryInUseForServer(config, server);
                            if (string.IsNullOrEmpty(indexDirectoryInUseForServer))
                            {
                                break;
                            }
                            else
                            {
                                config.IncrementRollingIndexDirectoryID();
                            }
                        }
                        //copy index to open dir
                        if (string.IsNullOrEmpty(indexDirectoryInUseForServer))
                        {
                            //copy each server index using different thread.
                            if (!h.ContainsKey("config"))
                            {
                                h["config"] = config;
                            }
                            h["server"] = server;
                            h["sourcepath"] = path;

                            Thread copy = new Thread(CopyIndexToServer);
                            copy.Start(h);
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Lock file exists for community:{0}. {1} Index not copied to {2}", config.CommunityID, config.IProcessorType, indexDirectoryInUseForServer), null);
                        }
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Copying {0} index turned off for community:{1}",config.IProcessorType,config.CommunityID), null);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "copyIndex", ex, null);
            }
        }

        public void CopyIndexToServer(object m)            
        {
            string uri = string.Empty;
            string sourcepath = string.Empty;
            string serverPath = string.Empty;
            try
            {
                Hashtable map = m as Hashtable;
                CommunityIndexConfig config = map["config"] as CommunityIndexConfig;
                string server = map["server"] as string;
                serverPath = Utils.GetUNCTargetPath(config.IndexPath, config.RollingIndexDirectoryID, server); 
                sourcepath = map["sourcepath"] as string;                
                IndexRoller.CopyIndex(sourcepath, serverPath, server, config);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("CopyIndexToServer: [srcpath:{0}, destpath:{1}]",sourcepath,serverPath), ex, null);
            }
        }

        public string GetCurrentIndexDirectory(int communityid, string server, IProcessorType iProcessorType)
        {
            return Utils.GetCurrentIndexDirectory(communityid, iProcessorType, server, CLASS_NAME);
        }

        private DateTime getLastIndexDate(int communityid, out int rollingdirid, IProcessorType iProcessorType)
        {
            DateTime lastindexdate = DateTime.MinValue;
            System.Data.SqlClient.SqlDataReader rs = null;
            rollingdirid = 0;
            try
            {
                string dbName = Utils.GetIndexUpdateDB(communityid, CLASS_NAME, iProcessorType);
                Command command = new Command(dbName, "[up_GetLastIndexDate]", 0);

                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityid);
                command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, System.Environment.MachineName);
                SqlParameterCollection sqlParams;
                rs = Client.Instance.ExecuteReader(command);

                if (rs.Read())
                {
                    lastindexdate = Conversion.CDateTime(rs["indexdate"]);
                    rollingdirid = Conversion.CInt(rs["rollingdirid"]);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "getLastIndexDate", ex, null);
            }
            finally
            {
                if (rs != null)
                { rs.Dispose(); }
            }
            return lastindexdate;
        }

        private void updateLastIndexDate(CommunityIndexConfig c)
        {
            DateTime lastindexdate = DateTime.MinValue;
            try
            {
                string dbName = Utils.GetIndexUpdateDB(c.CommunityID, CLASS_NAME, c.IProcessorType);
                Command command = new Command(dbName, "[up_InsertLastIndexDate]", 0);

                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, c.CommunityID);
                command.AddParameter("@IndexDate", SqlDbType.DateTime, ParameterDirection.Input, c.LastIndexDate);
                command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, System.Environment.MachineName);
                command.AddParameter("@Rollingdirid", SqlDbType.Int, ParameterDirection.Input, c.RollingIndexDirectoryID);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "getLastIndexDate", ex, null);
            }
        }

        public void Stop()
        {
            IndexProcessorFactory.Instance.Dispose();
        }
    }


  public class CommunityIndexConfig
  {
        public int CommunityID { get; set; }
        public string IndexPath { get; set; }
        public string IndexStagePath { get; set; }
        public string MetadataFile { get; set; }
        public int IndexerThreads { get; set; }
        public DateTime LastIndexDate { get; set; }
        public int MergeFactor{ get; set; }
        public int MaxMergeDocs { get; set; }
        public int LastActiveDays { get; set; }
        public DocumentMetadata Metadata { get; set; }
        public int RollingIndexDirectoryID { get; set; }
        public bool EnableRollingUpdates { get; set; }
        public bool OnDemandIndexEnabled { get; set; }
        public string OnDemandIndexDateSettingConstant { get; set; }
        public int RowBlockDateRangeInDays { get; set; }
        public int ProcTimeLimitPerRowBlockInMinutes { get; set; }
        public bool UseRowBlocks { get; set; }
        public int RollingIndexInterval { get; set; }
        public bool UseCompoundFile { get; set; }
        public int MaxMBBufferSize { get; set; }
        public IProcessorType IProcessorType { get; set; }
        public int BrandLastLogonDateLookBack { get; set; } // how many years to go back for brandlastlogondate
        public bool EnableMOLUpdate { get; set; }

        public Stopwatch Stopwatch
        {
            get { return LoaderBL.Instance.GetStopwatchForCommunity(CommunityID); }
        }

        public Counter MemberCount
        {
            get { return LoaderBL.Instance.GetCommunityMemberCount(CommunityID); }
        }  

        public void IncrementRollingIndexDirectoryID()
        {
            RollingIndexDirectoryID = RollingIndexDirectoryID + 1;
            if (RollingIndexDirectoryID > LoaderBL.MAX_ROLLING_DIRS)
            {
                RollingIndexDirectoryID = 0;
            }
        }

        public override string ToString()
        {
            try
            {
                return
                    string.Format(
                        "Community Index Config, communityid:{0}, index path:{1}, metadata:{2},  active days: {3}, last index date :{4}, dir id:{5}, max buffer size:{6} MB, use compound file:{7}, processor type:{8}",
                        CommunityID, IndexPath, MetadataFile, LastActiveDays, LastIndexDate,
                        RollingIndexDirectoryID, MaxMBBufferSize, UseCompoundFile, IProcessorType);
            }
            catch
            {
                return base.ToString();
            }
        }
    
    }

    
    public class IndexCommitter
         {
        private IndexWriter _indexWriter;
        private bool run = true;
             public IndexCommitter(IndexWriter w)
             {
                 _indexWriter = w;
             }

        public void Stop()
        {
            run = false;
            _indexWriter = null;
        }

             public void Start()
             {
            while (run)
                 {
                     try
                     {
                         
                    if (null != _indexWriter)
                    {
                         _indexWriter.Commit();
                    }
                         Thread.Sleep(60000);
                     }
                     catch (Exception ex)
                     {
                         
                    RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "IndexCommitter",
                                                            "Start", ex, null);
                         Thread.CurrentThread.Join(100);
                         Thread.CurrentThread.Abort();
                     }
                 }
             }
         }

    public class CommunityIndexConfigCollection : ICacheable
    {

        private List<CommunityIndexConfig> _communityIndexConfigs = null;
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        private const string CACHEKEYFORMAT = "CommunityIndexConfigs";

        public List<CommunityIndexConfig> CommunityIndexConfigs
        {
            get { return _communityIndexConfigs; }
            set { _communityIndexConfigs = value; }
        }


        public CommunityIndexConfigCollection(List<CommunityIndexConfig> configs)
        {
            this._communityIndexConfigs = configs;
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEYFORMAT;
        }
        #endregion
    }

    public class Counter
    {
        private int _count = 0;

        public int Count { get { return _count; } }

        public void Increment()
        {
            _count++;
        }

        public void Reset()
        {
            _count = 0;
        }

        public override string ToString()
        {
            return _count.ToString();
        }
    }
}
