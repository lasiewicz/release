﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Reflection;
using Spark.Logging;
using Spark.SearchEngine.BusinessLogic.Indexer;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.BusinessLogic.Documents;
using Matchnet.Exceptions;
using Lucene.Net.Index;
using Lucene.Net.Spatial.Tier.Projectors;

namespace Spark.SearchEngine.BusinessLogic
{
    public class IndexLoader:IDisposable
    {


        public delegate void IndexingCompleteEventHandler(Hashtable hash);
        public event IndexingCompleteEventHandler IndexingComplete;

        private const string SEARCHSTORE_DB_NAME = "mnSearchStore";
         string CLASS_NAME = "IndexLoader";
        List<PropertyInfo> _searchMemberProps;
        private IndexWriter _indexWriter=null;
        DocumentMetadata _documentMetadata = null;
        IProjector projector = null;
        Dictionary< int,CartesianTierPlotter> _tiers;

        public int CommunityID { get; set; }
        public int PartitionCounts { get; set; }
        public string IndexPath { get; set; }
        public string MetadataPath { get; set; }
       
        public DocumentMetadata DocumentMetadata { get { return _documentMetadata; } set { _documentMetadata = value; } }
        Queue<SearchMember> _memberQueue = new Queue<SearchMember>();
       
        private ReaderWriterLock _loaderQueueLock = new ReaderWriterLock();

        public IndexWriter Writer { get { return _indexWriter; } set { _indexWriter = value; } }
        public Queue<SearchMember> MemberQueue { get { return _memberQueue; } }
        public bool EnableRollingUpdates { get; set; }
        public IndexCommitter IndexCommitter { get; set; }
        public IndexLoader(int communityid, int partitions,string path, string metadatapath)
        {
            CommunityID = communityid;
            PartitionCounts = partitions;
            IndexPath = path;
            MetadataPath = metadatapath;
            LoadMetadata();
            getSearchMemberProps();
            _tiers = new Dictionary<int, CartesianTierPlotter>();
            IProjector projector = new SinusoidalProjector();

            //MemberDequeued += new MemberDeQueuedEventHandler(IndexMember);
           // PartitionQueued += new PartitionQueuedEventHandler(loadPartition);
            for (int tier = _documentMetadata.StartTier; tier <= _documentMetadata.EndTier; tier++)
            {
                CartesianTierPlotter ctp;
                ctp = new CartesianTierPlotter(tier, projector, "_localTier");
                _tiers.Add(tier, ctp);
               
            }
        }

        public void Start()
        {
            try
            {
             
                Thread t1 = new Thread(new ThreadStart(IndexWorkerCycle));
                t1.Start();

            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error loading partitions for community: " + CommunityID.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Start", blex, null);
            }
        }

        public void IndexWorkerCycle()
        {
            bool doWork=true;
            long maxqueue = 0;
            int sleepcount = 0;
            int membercount = 0;
            int loggedcount = 0;
            int loggedqueuecount = 0;
            int memberId = -1;
            try
            {
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Start IndexWorkerCycle for community: " + CommunityID.ToString(), null);
                Timer timer = new Timer();
                Timer queueTimer = new Timer();
                while (doWork)
                {
                    SearchMember member = null;
                    while (_memberQueue.Count <= 0)
                    {
                        Thread.Sleep(10);
                        if (EnableRollingUpdates)
                        {
                            //we are going restart indexing to the another directory if we have rolling updates
                            //if we have incremental and realtime updates it will continue listening to the queue
                            sleepcount += 1;
                            if (sleepcount >= 10000)
                            {
                                Hashtable h = new Hashtable();
                                h["communityId"] = CommunityID;
                                h["committer"] = IndexCommitter;
                                IndexingComplete(h);
                                string queueCount = (null != _memberQueue) ? _memberQueue.Count.ToString() : "[_memberQueue is null]";
                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Breaking from DoWork loop. Processing index queue, Queued: " + queueCount + " for community: " + CommunityID.ToString(), null);
                                doWork = false;
                                break;
                            }
                        }
                    }
                    if (!doWork)
                        break;
                    int queuecount = (null != _memberQueue) ? _memberQueue.Count : 0;
                    try
                    {
                        _loaderQueueLock.AcquireWriterLock(GetLockWaitTime());
                        if (_memberQueue != null)
                        {
                            member = _memberQueue.Dequeue();
                        }
                    }
                    catch (Exception ex)
                    {
                        RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexWorkerCycle", ex, null);
                    }
                    finally
                    {
                        _loaderQueueLock.ReleaseLock();
                    }


                    if (member != null)
                    {
                        IndexMember(member);

                        membercount += 1;
                    }
                    else
                    {
                        Thread.Sleep(500);
                    }
                  
                      maxqueue = Math.Max(queuecount, maxqueue);
                      sleepcount = 0;
                      if (queueTimer.Minutes() > 0)
                    {
                        queueTimer.Stop();
                      //  RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Indexing for community: " + CommunityID.ToString() + "Max Queue size:" + maxqueue.ToString() + ", Members Indexed: " + membercount.ToString() + ", Current queue:" + queuecount.ToString(), null);
                         int diffq = queuecount - loggedqueuecount;
                         int diffm = membercount - loggedcount;
                        loggedcount = membercount;
                        loggedqueuecount = queuecount;
                        
                        System.Diagnostics.Trace.Write("Indexing for community: " + CommunityID.ToString() + ", Max Queue size:" + maxqueue.ToString() + ", Members Indexed: " + membercount.ToString() + ",indexed since last logging: " + diffm.ToString() + ", Current queue:" + queuecount.ToString() + ", difference: " + diffq.ToString());
                        queueTimer.Start();
                    }

                   
                    
                
                }

                string queueCount2 = (null != _memberQueue) ? _memberQueue.Count.ToString() : "[_memberQueue is null]";
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Outside of DoWork loop. Processing index queue, Queued: " + queueCount2 + " for community: " + CommunityID.ToString(), null);
            }
            catch (ThreadAbortException ex)
            {
                BLException blex = new BLException("Thread Abort Error indexing for community: " + CommunityID.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexWorkerCycle", blex, null);

            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error indexing for community: " + CommunityID.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexWorkerCycle", blex, null);

             
            }
        }

        private int GetLockWaitTime()
        {
            return 100;
        }

        public void Enqueue(SearchMember member)
        {
            try
            {
                _loaderQueueLock.AcquireWriterLock(GetLockWaitTime());
                if (null != _memberQueue)
                {
                    _memberQueue.Enqueue(member);
                }
            }
            finally
            {
                _loaderQueueLock.ReleaseLock();
            }
        }
      

        public int GetQueueCount()
        {
            return _memberQueue.Count;

        }
        public void IndexMember(SearchMember member)
        {
            IndexMember(member, _documentMetadata, _indexWriter);
        }

        public void IndexMember(SearchMember member, DocumentMetadata docMetadata, IndexWriter writer)
        {
            try
            {

                #region reflection
                //gave it up for performance :((
               // SearchDocument memberdoc = GetSearchDocument(member);
                
                
                #endregion
                #region no reflection

                SearchDocument memberdoc = new SearchDocument(docMetadata);
                memberdoc.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                memberdoc.RegionLanguages = Utils.RegionLanguages;
                if (member.MemberID <= 0 || member.CommunityID <= 0 || member.GenderMask <= 0)
                    return;
                if (member.UpdateType == UpdateTypeEnum.all)
                {
                    memberdoc.Populate(member, docMetadata);
                }
                else if(member.UpdateType==UpdateTypeEnum.emailcount)
                {
                        memberdoc.Add(docMetadata.GetField("EmailCount"), member.EmailCount.ToString());
                }
                else if (member.UpdateType == UpdateTypeEnum.hasphoto)
                {
                    memberdoc.Add(docMetadata.GetField("HasPhotoFlag"), member.HasPhotoFlag.ToString());
                }
                else if (member.UpdateType == UpdateTypeEnum.lastactivedate)
                {
                    memberdoc.Add(docMetadata.GetField("LastActiveDate"), member.LastActiveDate.ToString());
                }
                #endregion
                if(member.UpdateType !=UpdateTypeEnum.delete)
                    writer.UpdateDocument(new Term("id",member.MemberID.ToString()),memberdoc.LuceneDocument);
                else
                    writer.DeleteDocuments(new Term("id",member.MemberID.ToString()));
                //RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Member {0} written to index.", member.MemberID),null);             
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error indexing member for community: " + CommunityID.ToString() +  ", memberid: " + member.MemberID.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexMember", blex, null);
            }
            
        }


        public SearchDocument GetSearchDocument(SearchMember member)
        {
            try
            {

                //   Timer t = new Timer();
                //   RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexMember", new string[]{member.MemberID.ToString()});
                SearchDocument memberdoc = new SearchDocument(DocumentMetadata);
                memberdoc.CommunityName = ServiceConstants.GetCommunityName(CommunityID);
                memberdoc.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                memberdoc.RegionLanguages = Utils.RegionLanguages;

                foreach (PropertyInfo p in _searchMemberProps)
                {
                    SearchField fld = _documentMetadata.GetField(p.Name);
                    if (fld == null)
                        continue;
                    if (fld.FieldType != DataType.LOCATION && fld.FieldType != DataType.AREACODE && fld.FieldType != DataType.TXT)
                        memberdoc.Add(fld, p.GetValue(member, null).ToString());
                    else if (fld.FieldType == DataType.LOCATION)
                    {
                        memberdoc.AddLocation( member.Latitude, member.Longitude, _documentMetadata.StartTier, _documentMetadata.EndTier, _tiers);
                    }
                    else if (fld.FieldType == DataType.AREACODE)
                    {
                        int regionID= (member.Depth4RegionID >0 ) ? member.Depth4RegionID : member.Depth3RegionID;
                        memberdoc.AddAreaCode(fld, regionID);
                    }
                    else if (fld.FieldType == DataType.TXT)
                    {
                        memberdoc.Add(fld, p.GetValue(member, null).ToString());
                    }



                }
                return memberdoc;
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error indexing member for community: " + CommunityID.ToString() + ", memberid: " + member.MemberID.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexMember", blex, null);
                return null;
            }

        }

        private void getSearchMemberProps()
        {

            SearchMember member = new SearchMember();
            Type t = member.GetType();
            _searchMemberProps = t.GetProperties().ToList<PropertyInfo>();
            member = null;
            
        }


        public void LoadMetadata()
        {
            _documentMetadata = Serialization.FromXmlFile<DocumentMetadata>(MetadataPath);
            _documentMetadata.GetBestGeoTier();


        }

        public void Commit()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Commit", null);
                _indexWriter.Commit();
                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Commit", null);
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error committing, community: " + CommunityID.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Commit", blex, null);
            }

        }


        #region IDisposable Members
        public void Dispose()
        {
            if (_memberQueue != null)
            {
                _memberQueue = null;
            }
        }
        #endregion
    }


}
