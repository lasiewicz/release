﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchEngine.BusinessLogic.Indexer
{
    public class IndexLoaderCommunity
    {
        public int CommunityID { get; set; }

        public List<IndexLoader> IndexLoaders { get; set; }

    }
}
