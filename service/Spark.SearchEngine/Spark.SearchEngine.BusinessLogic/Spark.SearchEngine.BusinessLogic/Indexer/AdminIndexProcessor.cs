﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Spatial;
using Lucene.Net.Store;
using Matchnet.Content.ValueObjects.Region;
using Spark.Logging;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.BusinessLogic.Documents;
using Spatial4n.Core.Context;

namespace Spark.SearchEngine.BusinessLogic.Indexer
{
    public class AdminIndexProcessor : IProcessor
    {
        public static string CLASS_NAME = "AdminIndexProcessor";
        private int _maxSize = 100000;
        private List<SearchAdmin> _searchAdmins = null;
        private long count = 0;
        private long elapsedProcessingTime = 0;
        private long elapsedIndexWriterTime = 0;
        private IndexWriter _writer;
        private DocumentMetadata _documentMetadata;
        private SearchDocument _admindoc;
        private IProcessorType _processorType = IProcessorType.AdminIndexProcessor;
        private RegionAreaCodeDictionary _areaCodeDictionary = null;
        private Dictionary<int, RegionLanguage> _regionLanguages = null;
        private SpatialContext _spatialContext = null;
        private SpatialStrategy _spatialStrategy = null;
        private Dictionary<string, AbstractField[]> _spatialShapeFields = null;
        private Matchnet.MembersOnline.ValueObjects.MOLCommunity _MOLCommunity = null;

        Stopwatch processingTime = new Stopwatch();
        Stopwatch indexWriterTime = new Stopwatch();

        public bool IsProcessing { get; set; }
        public bool IsFull { get; set; }

        public DocumentMetadata DocumentMetadata
        {
            get { return _documentMetadata; }
            set { _documentMetadata = value; }
        }

        public IndexWriter Writer
        {
            get { return _writer; }
            set { _writer = value; }
        }

        public int MaxSize
        {
            get { return _maxSize; }
            set { _maxSize = value; }
        }

        public int CommunityId { get; set; }

        public Directory IndexDir { get; set; }

        public RegionAreaCodeDictionary AreaCodeDictionary
        {
            get { return _areaCodeDictionary; }
            set { _areaCodeDictionary = value; }
        }

        public Dictionary<int, RegionLanguage> RegionLanguages
        {
            get { return _regionLanguages; }
            set { _regionLanguages = value; }
        }

        public SpatialContext SpatialContext
        {
            get { return _spatialContext; }
            set { _spatialContext = value; }
        }

        public SpatialStrategy SpatialStrategy
        {
            get { return _spatialStrategy; }
            set { _spatialStrategy = value; }
        }

        public Dictionary<string, AbstractField[]> SpatialShapeFields
        {
            get { return _spatialShapeFields; }
            set { _spatialShapeFields = value; }
        }

        public Matchnet.MembersOnline.ValueObjects.MOLCommunity MOLCommunity
        {
            get { return _MOLCommunity; }
            set { _MOLCommunity = value; }
        }

        public decimal AverageProcessingTime
        {
            get { return (count == 0) ? 0 : (decimal)elapsedProcessingTime / (decimal)count; }
        }

        public decimal AverageIndexWriterTime
        {
            get { return (count == 0) ? 0 : (decimal)elapsedIndexWriterTime / (decimal)count; }
        }

        public bool IsInitialized
        {
            get
            {
                return (null != Writer)
                       && (null != DocumentMetadata)
                       && (null != AreaCodeDictionary)
                       && (null != RegionLanguages)
                       && (null != SpatialContext)
                       && (null != SpatialStrategy)
                       && (null != SpatialShapeFields);
            }

        }

        public void Initialize(Hashtable context)
        {
            IndexWriter indexWriter = context["indexWriter"] as IndexWriter;
            DocumentMetadata documentMetadata = context["documentMetadata"] as DocumentMetadata;
            RegionAreaCodeDictionary areaCodeDictionary = context["areaCodeDictionary"] as RegionAreaCodeDictionary;
            Dictionary<int, RegionLanguage> regionLanguages = context["regionLanguages"] as Dictionary<int, RegionLanguage>;
            SpatialContext spatialContext = context["spatialContext"] as SpatialContext;
            SpatialStrategy spatialStrategy = context["spatialStrategy"] as SpatialStrategy;
            Dictionary<string, AbstractField[]> spatialShapeFields = context["spatialShapeFields"] as Dictionary<string, AbstractField[]>;
            int maxSize = (int)context["maxSize"];

            this.AreaCodeDictionary = areaCodeDictionary;
            this.RegionLanguages = regionLanguages;
            this.Writer = indexWriter;
            this.DocumentMetadata = documentMetadata;
            this.SpatialContext = spatialContext;
            this.SpatialStrategy = spatialStrategy;
            this.SpatialShapeFields = spatialShapeFields;
            this.MaxSize = maxSize;
        }

        public AdminIndexProcessor(DocumentMetadata documentMetadata, IndexWriter writer, Directory indexDir, int maxSize)
        {
            this.DocumentMetadata = documentMetadata;
            this.Writer = writer;
            this.IndexDir = indexDir;
            this.MaxSize = maxSize;
            _admindoc = new SearchDocument(DocumentMetadata);
        }

//        public void AddDataRow(DataRow dataRow)
//        {
//            if (IsFull)
//            {
//                throw new ConstraintException("Processor is Full!!");
//            }
//            if (null == _searchAdmins)
//            {
//                _searchAdmins = new List<DataRow>(MaxSize);
//            }
//            _searchAdmins.Add(dataRow);
//            if (_searchAdmins.Count >= MaxSize)
//            {
//                IsFull = true;
//            }
//        }

        public void AddDataRow(IDataReader iDataReader)
        {
            if (IsFull)
            {
                throw new ConstraintException("Processor is Full!!");
            }
            if (null == _searchAdmins)
            {
                _searchAdmins = new List<SearchAdmin>(MaxSize);
            }
            processingTime.Start();
            SearchAdmin searchAdmin = new SearchAdmin().Populate(iDataReader);
            searchAdmin.UpdateType = UpdateTypeEnum.all;
            _searchAdmins.Add(searchAdmin);
            processingTime.Stop();
            elapsedProcessingTime += processingTime.ElapsedMilliseconds;
            processingTime.Reset();
            if (_searchAdmins.Count >= MaxSize)
            {
                IsFull = true;
                //start processing
                this.IsProcessing = true;
                Thread t = new Thread(new ThreadStart(this.Run));
                t.IsBackground = true;
                t.Start();
            }
        }

        public void Run()
        {
            IsProcessing = true;
            count = _searchAdmins.Count;
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                      string.Format(
                                                          "Total Processing Time: {0}s, Avg Processing Time: {1}ms, Total AdminMembers Processed: {2} for communityId={3} and processor type={4}\n",
                                                          (elapsedProcessingTime / 1000),
                                                          AverageProcessingTime,
                                                          count,
                                                          CommunityId, _processorType), null);

            try
            {
                IndexProcessorFactory.Instance.LogActiveProcessorCount(CommunityId, _processorType);

                int length = _searchAdmins.Count;
                for (int i = 0; i < length; i++)
                {
                    if (IsProcessing)
                    {
                        SearchAdmin admin = _searchAdmins[i];
                        indexWriterTime.Start();
                        IndexAdmin(admin, DocumentMetadata, Writer);
                        indexWriterTime.Stop();
                        elapsedIndexWriterTime += indexWriterTime.ElapsedMilliseconds;
                        indexWriterTime.Reset();
                        _searchAdmins[i] = null;
                        admin = null;
                    }
                    if (!IsProcessing)
                    {
                        break;
                    }
                }

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                          string.Format(
                                              "Total Index Write Time: {0}min, Avg Index Write Time: {1}ms, Total Members Indexed: {2} for communityId={3} and processor type={4}\n",
                                              ((elapsedIndexWriterTime / 1000) / 60),
                                              AverageIndexWriterTime, count, CommunityId, _processorType),
                                          null);

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, ex);
            }
            finally
            {
                IsProcessing = false;
            }
            if (null == IndexDir)
            {
                CleanUp(false);
            }
        }

        private void CleanUp(bool isDisposing)
        {
            if (isDisposing)
            {
                Writer = null;
                DocumentMetadata = null;
                AreaCodeDictionary = null;
                RegionLanguages = null;
                SpatialContext = null;
                SpatialShapeFields = null;
                SpatialStrategy = null;
            }
            
            _searchAdmins.Clear();
            count = 0;
            elapsedProcessingTime = 0;
            elapsedIndexWriterTime = 0;
            IsProcessing = false;
            IsFull = false;
        }

        public void IndexAdmin(SearchAdmin searchAdmin, DocumentMetadata docMetadata, IndexWriter writer)
        {
            try
            {
                _admindoc.CommunityName = ServiceConstants.GetCommunityName(this.CommunityId);
                _admindoc.AreaCodeDictionary = AreaCodeDictionary;
                _admindoc.RegionLanguages = RegionLanguages;
                _admindoc.Context = SpatialContext;
                _admindoc.Strategy = SpatialStrategy;
                _admindoc.SpatialShapeFields = SpatialShapeFields;

                if (searchAdmin.MemberId <= 0 || searchAdmin.CommunityId <= 0)
                    return;
                if (searchAdmin.UpdateType == UpdateTypeEnum.all)
                {
                    _admindoc.Populate(searchAdmin, docMetadata);
                }
                if (searchAdmin.UpdateType != UpdateTypeEnum.delete)
                    writer.UpdateDocument(new Term("id", searchAdmin.MemberId.ToString()), _admindoc.LuceneDocument);
                else
                    writer.DeleteDocuments(new Term("id", searchAdmin.MemberId.ToString()));
                //clear field values
                _admindoc.ClearAllFieldValues();

                //RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Member {0} written to index.", member.MemberID),null);             
            }
            catch (Lucene.Net.Store.AlreadyClosedException ace)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Already closded writer: {0}, {1}, {2},{3}", this.Writer.ToString(), this.IndexDir, this.CommunityId, this.count), ace, null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexMember", ex, null);
            }

        }

        public void Dispose()
        {
            this.IsProcessing = false;
            this.CleanUp(true);
        }
    }
}
