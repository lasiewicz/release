﻿using System;
using System.IO;
using Spark.Logging;
using Matchnet.Exceptions;
using Spark.SearchEngine.ValueObjects;

namespace Spark.SearchEngine.BusinessLogic.Indexer
{
    public class IndexRoller
    {
        //public delegate void IndexCopiedEventHandler(int communityid,string server, int dirid);
        //public static event IndexCopiedEventHandler IndexCopied;
        public const string CLASS_NAME = "IndexRoller";
        int _communityid;
        string _path;
        int _dirid;
        public IndexRoller(int communityid,string path, int dirid)
        {
            _communityid = communityid;
            _path = path;
            _dirid = dirid;
        }

        public static void CleanDir(string path, int dirid)
        {
            try
            {
                string dir = path + dirid.ToString();
                DirectoryInfo dirinfo = new DirectoryInfo(dir);
                foreach (FileInfo f in dirinfo.GetFiles())
                {
                    f.Delete();
                }
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error cleaning index dir: " + path + ", dirid " + dirid.ToString() + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "CleanDir", blex, null);
            }
        }

        public static void CleanDir(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Cleaning Index Dir: " + path, null);

                    DirectoryInfo dirinfo = new DirectoryInfo(path);
                    foreach (FileInfo f in dirinfo.GetFiles())
                    {
                        f.Delete();
                    }
                    foreach (DirectoryInfo dir in dirinfo.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Finish Cleaning Index Dir: " + path, null);
                }

            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error cleaning index dir: " + path + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "CleanDir", blex, null);
            }
        }

        public static void CleanSubDirs(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Cleaning Index Sub Dirs: " + path, null);

                    DirectoryInfo dirinfo = new DirectoryInfo(path);
                    foreach (DirectoryInfo dir in dirinfo.GetDirectories())
                    {
                        dir.Delete(true);                        
                    }
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Finish Cleaning Index Sub Dirs: " + path, null);
                }
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error cleaning index sub dirs: " + path + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "CleanSubDirs", blex, null);
            }
        }

        public static void CopyIndex(object sourcepath, object targetpath, object targetserver,object config)
        {
            try
            {
                System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Copying Index Dir: " + sourcepath + " to " + targetpath, null);
                string server = targetserver as string;
                CleanDir(targetpath as string);
                CopyIndex(sourcepath as string, targetpath as string);
                stopWatch.Stop();
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "Total Copy Time: " + ((stopWatch.ElapsedMilliseconds / 1000) / 60) + " minutes. Finished Copying Index Dir: " + sourcepath + " to " + targetpath, null);
                updateIndexDirectory(config as CommunityIndexConfig, targetserver as string, targetpath as string);
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error copying index dir: " + sourcepath + " to " + targetpath + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "CopyIndex", blex, null);
            }
        }
        
        public static void CopyIndex(string sourcepath, string targetpath)
        {
            try
            {
                if (!Directory.Exists(targetpath))
                {
                    Directory.CreateDirectory(targetpath);
                }

                DirectoryInfo dirinfo = new DirectoryInfo(sourcepath);
                foreach (FileInfo f in dirinfo.GetFiles())
                {
                    string dest = targetpath + "\\" + f.Name;
                    f.CopyTo(dest, true);
                }                
            }
            catch (Exception ex)
            {
                BLException blex = new BLException("Error copying index dir: " + sourcepath + " to " + targetpath + ", Inner Exception: " + ex.ToString(), ex);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "CopyIndex", blex, null);
            }
        }

        private static void updateIndexDirectory(CommunityIndexConfig c, string server, string directory)
        {
            Utils.UpdateIndexDirectory(c.CommunityID, c.IProcessorType, server, directory, CLASS_NAME);
        }

    }
}
