﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Matchnet.Content.ValueObjects.Region;
using Spark.Logging;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.BusinessLogic.Documents;

namespace Spark.SearchEngine.BusinessLogic.Indexer
{
    public class IndexProcessorFactory : IProcessorFactory
    {
        public static string CLASS_NAME = "IndexProcessorFactory";
        private Dictionary<int, Dictionary<IProcessorType,List<IProcessor>>> communityProcessors = new Dictionary<int, Dictionary<IProcessorType, List<IProcessor>>>();
        public static readonly IProcessorFactory Instance = new IndexProcessorFactory();
        private static object _lockObject = new Object();

        private IndexProcessorFactory(){}

        public bool AreAllDone(int communityId, IProcessorType iProcessorType)
        {
            bool b = true;
            List<IProcessor> processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            foreach (IProcessor processor in processors)
            {
                if (processor.IsProcessing)
                {
                    b = false;
                    break;
                }
            }
            return b;
        }

        public List<IndexWriter> GetIndexWriters(int communityId, IProcessorType iProcessorType)
        {
            List<IndexWriter> indexWriters = new List<IndexWriter>();
            List<IProcessor> processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            foreach (IProcessor processor in processors)
            {
                indexWriters.Add(processor.Writer);
            }
            return indexWriters;
        }

        public void StartProcessors(int communityId, IProcessorType iProcessorType)
        {
            List<IProcessor> processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            foreach (IProcessor mip in processors)
            {
                if (!mip.IsProcessing)
                {
                    mip.IsProcessing = true;
                    Thread t = new Thread(new ThreadStart(mip.Run));
                    t.IsBackground = true;
                    t.Start();
                }                    
            }
        }

        public void StartProcessorsPostRun(int communityId, IProcessorType iProcessorType)
        {
            List<IProcessor> processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            foreach (IProcessor mip in processors)
            {
                if (!mip.IsProcessing)
                {
                    mip.IsProcessing = true;
                    Thread t = new Thread(new ThreadStart(mip.Run));
                    t.IsBackground = true;
                    t.Start();
                }
            }
        }

        public IProcessor GetIProcessor(int communityId, DocumentMetadata documentMetadata, IndexWriter writer, int maxSize, IProcessorType iProcessorType)
        {
            IProcessor processor = GetIProcessor(communityId, iProcessorType);

            if (null == processor)
            {
                processor = CreateProcessor(communityId, documentMetadata, writer, null, maxSize, iProcessorType);
            }
            else
            {
                if (!processor.IsInitialized)
                {
                    Hashtable context = new Hashtable();
                    context["indexWriter"] = writer;
                    context["documentMetadata"] = documentMetadata;
                    context["maxSize"] = maxSize;
                    context["areaCodeDictionary"] = Utils.RegionAreaCodeDictionary;
                    context["regionLanguages"] = Utils.RegionLanguages;
                    context["spatialContext"] = Utils.SpatialContext;
                    context["spatialStrategy"] = Utils.SpatialStrategy;
                    context["spatialShapeFields"] = Utils.SpatialShapeFields;
                    processor.Initialize(context);
                }
            }
            return processor;
        }

        public IProcessor GetIProcessor(int communityId, DocumentMetadata documentMetadata, string indexPath, int maxSize, IProcessorType iProcessorType)
        {
            IProcessor processor = GetIProcessor(communityId, iProcessorType);

            if (null == processor)
            {
                IndexWriter indexWriter = CreateIndexWriter(indexPath, GetIProcessorsForCommunity(communityId, iProcessorType).Count + 1);
                processor = CreateProcessor(communityId, documentMetadata, indexWriter, indexWriter.Directory, maxSize, iProcessorType);
            }
            else
            {
                if (!processor.IsInitialized)
                {
                    IndexWriter indexWriter = CreateIndexWriter(indexPath, GetIProcessorsForCommunity(communityId, iProcessorType).Count + 1);
                    Hashtable context = new Hashtable();
                    context["indexWriter"] = indexWriter;
                    context["documentMetadata"] = documentMetadata;
                    context["maxSize"] = maxSize;
                    context["areaCodeDictionary"] = Utils.RegionAreaCodeDictionary;
                    context["regionLanguages"] = Utils.RegionLanguages;
                    context["spatialContext"] = Utils.SpatialContext;
                    context["spatialStrategy"] = Utils.SpatialStrategy;
                    context["spatialShapeFields"] = Utils.SpatialShapeFields;

                    processor.Initialize(context);
                }
            }
            return processor;
        }

        public IProcessor GetIProcessor(int communityId, DocumentMetadata documentMetadata, Directory dir, int maxSize, IProcessorType iProcessorType)
        {
            IProcessor processor = GetIProcessor(communityId, iProcessorType);

            if (null == processor)
            {
                Directory d = dir;
                if (null == d)
                {
                    d = new RAMDirectory();
                }
                IndexWriter indexWriter = CreateIndexWriter(d);
                processor = CreateProcessor(communityId, documentMetadata, indexWriter, d, maxSize, iProcessorType);
            }
            else
            {
                if (!processor.IsInitialized)
                {
                    Directory d = dir;
                    if (null == d)
                    {
                        d = new RAMDirectory();
                    }
                    IndexWriter indexWriter = CreateIndexWriter(d);
                    Hashtable context = new Hashtable();
                    context["indexWriter"]=indexWriter;
                    context["documentMetadata"] = documentMetadata;
                    context["maxSize"] = maxSize;
                    context["areaCodeDictionary"] = Utils.RegionAreaCodeDictionary;
                    context["regionLanguages"] = Utils.RegionLanguages;
                    context["spatialContext"] = Utils.SpatialContext;
                    context["spatialStrategy"] = Utils.SpatialStrategy;
                    context["spatialShapeFields"] = Utils.SpatialShapeFields;

                    processor.Initialize(context);
                }
            }

            return processor;
        }

        private IndexWriter CreateIndexWriter(string indexPath, int numOfIProcessors)
        {
            string path = indexPath + "/" + numOfIProcessors;
            FSDirectory directory = FSDirectory.Open(path);
            return CreateIndexWriter(directory);
        }

        private IndexWriter CreateIndexWriter(Directory dir)
        {
            IndexWriter indexWriter = new IndexWriter(dir, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);
            indexWriter.UseCompoundFile = false;
            indexWriter.MergeFactor = 30;
            indexWriter.SetMaxBufferedDocs(1000);
            return indexWriter;
        }

        private IProcessor CreateProcessor(int communityId, DocumentMetadata documentMetadata, IndexWriter writer, Directory dir, int maxSize, IProcessorType iProcessorType)
        {
            IProcessor processor = null;
            switch (iProcessorType)
            {
                case IProcessorType.KeywordIndexProcessor:
                case IProcessorType.KeywordIndexVersion3Processor:
                    KeywordIndexProcessor kip = new KeywordIndexProcessor(documentMetadata, writer, dir, maxSize);
                    processor = kip;
                    break;
                case IProcessorType.AdminIndexProcessor:
                    AdminIndexProcessor aip = new AdminIndexProcessor(documentMetadata, writer, dir, maxSize);
                    processor = aip;
                    break;
                case IProcessorType.MemberIndexProcessor:
                case IProcessorType.MemberIndexVersion3Processor:
                default:                    
                    MemberIndexProcessor mip = new MemberIndexProcessor(documentMetadata, writer, dir, maxSize);
                    processor = mip;
                    break;
            }
            processor.CommunityId = communityId;
            processor.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
            processor.RegionLanguages = Utils.RegionLanguages;
            processor.SpatialContext = Utils.SpatialContext;
            processor.SpatialStrategy = Utils.SpatialStrategy;
            processor.SpatialShapeFields = Utils.SpatialShapeFields;

            List<IProcessor> processors = null;
            processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            processors.Add(processor);
            return processor;
        }

        private List<IProcessor> GetIProcessorsForCommunity(int communityId, IProcessorType iProcessorType)
        {
            List<IProcessor> processors = null;
            lock (_lockObject)
            {
                Dictionary<IProcessorType, List<IProcessor>> processorsForCommunity = null;
                if (communityProcessors.ContainsKey(communityId))
                {
                    processorsForCommunity = communityProcessors[communityId];
                }
                else
                {
                    processorsForCommunity = new Dictionary<IProcessorType, List<IProcessor>>();
                    communityProcessors.Add(communityId, processorsForCommunity);
                }

                if (processorsForCommunity.ContainsKey(iProcessorType))
                {
                    processors = processorsForCommunity[iProcessorType];
                }
                else
                {
                    processors = new List<IProcessor>();
                    processorsForCommunity.Add(iProcessorType, processors);
                }
            }
            return processors;
        }

        private IProcessor GetIProcessor(int communityId, IProcessorType iProcessorType)
        {
            IProcessor processor = null;
            List<IProcessor> processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            if (processors.Count > 0)
            {
                foreach (IProcessor p in processors)
                {
                    if (!p.IsFull && !p.IsProcessing)
                    {
                        processor = p;
                        break;
                    }
                }
            }
            return processor;
        }

        public void LogActiveProcessorCount(int communityId, IProcessorType iProcessorType)
        {
            int count = ActiveProcessorCount(communityId, iProcessorType);
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Currently using {0} {1}{2} for communityId={3}", count, iProcessorType.ToString(), (count > 1)?"s":string.Empty, communityId), null);
        }

        public int ActiveProcessorCount(int communityId, IProcessorType iProcessorType)
        {
            int count = 0;
            List<IProcessor> processors = GetIProcessorsForCommunity(communityId, iProcessorType);
            if (null != processors)
            {
                List<IProcessor> isProcessingList = processors.FindAll(delegate(IProcessor p) { return p.IsProcessing; });
                if (isProcessingList != null)
                {
                    count = isProcessingList.Count;
                }
            }
            return count;
        }

        public void ResetCommunity(int communityId, IProcessorType iProcessorType)
        {
            if (this.communityProcessors.ContainsKey(communityId))
            {
                Dictionary<IProcessorType, List<IProcessor>> processorsForCommunity = this.communityProcessors[communityId];
                if (processorsForCommunity.ContainsKey(iProcessorType))
                {
                    List<IProcessor> processors = processorsForCommunity[iProcessorType];
                    foreach (IProcessor indexProcessor in processors)
                    {
                        indexProcessor.Dispose();
                    }
                }
            }
        }

        public void ChangeMaxSize(int communityId, IProcessorType iProcessorType, int maxSize)
        {
            if (this.communityProcessors.ContainsKey(communityId))
            {
                Dictionary<IProcessorType, List<IProcessor>> processorsForCommunity = this.communityProcessors[communityId];
                if (processorsForCommunity.ContainsKey(iProcessorType))
                {
                    List<IProcessor> processors = processorsForCommunity[iProcessorType];
                    foreach (IProcessor indexProcessor in processors)
                    {
                        indexProcessor.MaxSize = maxSize;
                    }
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            foreach (KeyValuePair<int, Dictionary<IProcessorType, List<IProcessor>>> communityProcessor in communityProcessors)
            {
                foreach (KeyValuePair<IProcessorType, List<IProcessor>> processors in communityProcessor.Value)
                {
                    foreach (IProcessor processor in processors.Value)
                    {
                        processor.Dispose();
                    }
                }
            }
        }
        #endregion
    }
}

