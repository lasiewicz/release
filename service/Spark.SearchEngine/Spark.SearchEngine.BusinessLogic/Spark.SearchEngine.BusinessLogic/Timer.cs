﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SearchEngine.BusinessLogic
{
    public class Timer
    {

        DateTime _start;
        DateTime _finish;
        TimeSpan _span;
        public Timer()
        {
            _start = DateTime.Now;
        }

        public void Start()
        {
            _start = DateTime.Now;
            _finish = DateTime.MinValue;
            _span = (_start - _start);
            
        }

        public int Minutes()
        {
            
            return(DateTime.Now - _start).Minutes;

        }

        public void Stop()
        {
            _finish = DateTime.Now;
            _span = _finish - _start;
        }

        public override string ToString()
        {
           return _span.Milliseconds.ToString();
        }

        public  string ToString(string format)
        {
            return String.Format(format, _span.Milliseconds.ToString());
        }
        public string ToMinutes(string format)
        {
            return String.Format(format, _span.Minutes.ToString());
        }
    }
}
