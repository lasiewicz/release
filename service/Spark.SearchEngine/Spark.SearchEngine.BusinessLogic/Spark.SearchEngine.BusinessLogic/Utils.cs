﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Documents;
using Lucene.Net.Spatial;
using Lucene.Net.Spatial.Vector;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Exceptions;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Search.ValueObjects;
using Spark.Logging;
using System.Data;
using Spark.SearchEngine.BusinessLogic.Documents;
using Spark.SearchEngine.BusinessLogic.Indexer;
using Matchnet.Search.Interfaces;
using Spark.SearchEngine.BusinessLogic.Searcher;
using ServiceConstants = Spark.SearchEngine.ValueObjects.ServiceConstants;
using Spark.SearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;
using Matchnet.Search.ServiceAdapters;

namespace Spark.SearchEngine.BusinessLogic
{
   public class Utils
    {
    #region Settings helpful

       private static Dictionary<int, int> childrenCountTable = new Dictionary<int, int>();
       private static Dictionary<int, int> moreChildrenFlagTable = new Dictionary<int, int>();
       private static Dictionary<int, int> moreChildrenFlagTableReverse = new Dictionary<int, int>();
       private static List<MatchRatingAttribute> matchAttributes = new List<MatchRatingAttribute>();
       private static Dictionary<int, int> brands = new Dictionary<int, int>();
       private static Dictionary<int, int> sites = new Dictionary<int, int>();      
       private static object _lockObject = new object();
       private static object _lockObjectMatchAttributes = new object();
       private static RegionAreaCodeDictionary _areaCodeDictionary = null;
       private static Dictionary<int, RegionLanguage> _regionLanguages = new Dictionary<int, RegionLanguage>();
       private static Dictionary<string, AbstractField[]> _spatialShapeFields = new Dictionary<string, AbstractField[]>();
       private static object _metadataLockObject = new object();
       private static Dictionary<int, DocumentMetadata> _documentMetadata = new Dictionary<int, DocumentMetadata>();
       public const int MAX_ROLLING_DIRS = 3;
       private static SpatialContext _spatialContext;
       private static SpatialStrategy _spatialStrategy;

       private static ISettingsSA _settingsService = null;

       public static ISettingsSA SettingsService
       {
           get
           {
               if (null == _settingsService) return RuntimeSettings.Instance;
               return _settingsService;
           }
           set { _settingsService = value; }
       }

       public static RegionAreaCodeDictionary RegionAreaCodeDictionary
       {
           get
           {
               if (null == _areaCodeDictionary)
               {
                   lock (_lockObject)
                   {
                       if (null == _areaCodeDictionary)
                       {
                           _areaCodeDictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();
                       }
                   }
               }
               return _areaCodeDictionary;
           }
       }

       public static Dictionary<int, RegionLanguage> RegionLanguages { get { return _regionLanguages; } }

       public static Dictionary<string, AbstractField[]> SpatialShapeFields
       {
           get { return _spatialShapeFields; }
       }

       public static SpatialContext SpatialContext
       {
           get
           {
               if (null == _spatialContext)
               {
                   lock (_lockObject)
                   {
                       if(null==_spatialContext) _spatialContext = SpatialContext.GEO;
                   }
               }
               return _spatialContext;
           }
       }

       public static SpatialStrategy SpatialStrategy
       {
           get
           {
               if (null == _spatialStrategy)
               {
                   lock (_lockObject)
                   {
                       if (null == _spatialStrategy)
                       {
//                           GeohashPrefixTree geohashPrefixTree = new GeohashPrefixTree(SpatialContext, PrecisionLevel);
//                           _spatialStrategy = new RecursivePrefixTreeStrategy(geohashPrefixTree, "geoField");

                           // using PVS for optimizer
                           PointVectorStrategy pointVectorStrategy = new SparkPointVectorStrategy(SpatialContext, "geoField");
                           pointVectorStrategy.SetPrecisionStep(PrecisionLevel);
                           _spatialStrategy = pointVectorStrategy;
                       }
                   }
               }
               return _spatialStrategy;
           }
       }

       public static int PrecisionLevel
       {
           get
           {
               int precisionLevel = 11;
               return precisionLevel;
           }
       }

       public static DocumentMetadata GetDocMetadata(int communityID)
       {
           if (!_documentMetadata.ContainsKey(communityID))
           {
               lock (_metadataLockObject)
               {
                   if (!_documentMetadata.ContainsKey(communityID))
                   {
                       string metaFilePath = AppDomain.CurrentDomain.BaseDirectory + "/" + GetSetting("SEARCHER_COMMUNITY_METADATA_FILE", communityID, "SearchDocument.xml");
                       DocumentMetadata documentMetaData = Serialization.FromXmlFile<DocumentMetadata>(metaFilePath);
                       _documentMetadata.Add(communityID, documentMetaData);
                   }
               }
           }

           return _documentMetadata[communityID];
       }

       public static string GetSetting(string name, string defaultvalue)
       {
           try
           {
               return SettingsService.GetSettingFromSingleton(name);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static string GetSetting(string name, int communityid, string defaultvalue)
       {
           try
           {
               return SettingsService.GetSettingFromSingleton(name,communityid);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static int GetSetting(string name, int defaultvalue)
       {
           try
           {
             return  Conversion.CInt(SettingsService.GetSettingFromSingleton(name),defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static double GetSetting(string name, double defaultvalue)
       {
           try
           {
               return Conversion.CDouble(SettingsService.GetSettingFromSingleton(name), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }
       
       public static DateTime GetSetting(string name, DateTime defaultvalue)
       {
           try
           {
               return Conversion.CDateTime(SettingsService.GetSettingFromSingleton(name), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static bool GetSetting(string name, bool defaultvalue)
       {
           try
           {
               return Conversion.CBool(SettingsService.GetSettingFromSingleton(name), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static bool GetSetting(string name, int communityid, bool defaultvalue)
       {
           try
           {
               return Conversion.CBool(SettingsService.GetSettingFromSingleton(name, communityid), defaultvalue);

           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static int GetSetting(string name, int communityId, int defaultvalue)
       {
           try
           {
               return Conversion.CInt(SettingsService.GetSettingFromSingleton(name, communityId), defaultvalue);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Utils", "GetSetting", ex, null);
               return defaultvalue;
           }
       }

       public static int GetMaxResults(int communityId, bool useSearchRedesign30)
       {
           string key = (useSearchRedesign30) ? "SEARCH30_SEARCHER_MAX_RESULTS" : "SEARCHER_MAX_RESULTS";
           int maxResults = GetSetting(key, communityId, 1000);
           return maxResults;
       }

       public static string GetIndexUpdateDB(int communityId, string caller, IProcessorType iProcessorType)
       {
           string dbName = ServiceConstants.SYSTEM_DB_NAME;
           string settingConstant = (Searcher.Searcher.CLASS_NAME == caller)
                                        ? "SEARCHER_USE_SYSTEM_DB_FOR_INDEX_UPDATES"
                                        : "INDEXER_USE_SYSTEM_DB_FOR_INDEX_UPDATES";
           if (GetSetting(settingConstant, communityId, false))
           {
               dbName = ServiceConstants.SYSTEM_DB_NAME;
           }
           else
           {
               dbName = (UseSearchStoreDB(communityId, iProcessorType))
                            ? ServiceConstants.SEARCHSTORE_DB_NAME
                            : ServiceConstants.SEARCHLOAD_DB_NAME;
           }
           return dbName;
       }

       public static bool UseSearchStoreDB(int communityId, IProcessorType iProcessorType)
       {
           bool useSearchStoreDB=false;
           switch(iProcessorType)
           {
               case IProcessorType.KeywordIndexProcessor:
                   useSearchStoreDB = Conversion.CBool(GetSetting("KEYWORD_INDEXER_USE_SEARCHSTOREDB", communityId, "false"));
                   break;
               case IProcessorType.KeywordIndexVersion3Processor:
                   useSearchStoreDB = Conversion.CBool(GetSetting("KEYWORD_INDEXER_VERSION_3_USE_SEARCHSTOREDB", communityId, "false"));
                   break;
               case IProcessorType.MemberIndexVersion3Processor:
                   useSearchStoreDB = Conversion.CBool(GetSetting("INDEXER_VERSION_3_USE_SEARCHSTOREDB", communityId, "false"));
                   break;
               case IProcessorType.MemberIndexProcessor:
               default:
                   useSearchStoreDB = Conversion.CBool(GetSetting("INDEXER_USE_SEARCHSTOREDB", communityId, "false"));
                   break;
           }
           return useSearchStoreDB;
       }       

       public static List<int> GetIntValuesFromMask(int maskValue)
       {
           List<int> results = new List<int>();
           BitArray arr = new BitArray(BitConverter.GetBytes(maskValue));
           int idx = 0;
           foreach (bool b in arr.Cast<bool>())
           {
               if (b)
               {
                   results.Add((int)Math.Pow(2,idx));
               }
               idx++;
           }
           return results;
       }

       public static AttributeOptionCollection GetIntValuesFromMask(string attributeName, int communityId)
       {
           AttributeOptionCollection optionCollection = null;
           CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(attributeName, communityId)) as CacheAttributeOptionCollection;
           if (null == cachedOptionCollection)
           {
               if ("colorcode".Equals(attributeName.ToLower()))
               {
                   //0 = no, 1 = white, 2 = blue, 4 = yellow, 8 = red
                   AttributeOptionCollection colorCodes = new AttributeOptionCollection();
                   colorCodes.Add(new AttributeOption(1, 999999, 1, 1, "white", string.Empty));
                   colorCodes.Add(new AttributeOption(2, 999999, 2, 2, "blue", string.Empty));
                   colorCodes.Add(new AttributeOption(3, 999999, 4, 3, "yellow", string.Empty));
                   colorCodes.Add(new AttributeOption(4, 999999, 8, 4, "red", string.Empty));
                   optionCollection = colorCodes;
               }
               else
               {
                   optionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, GetBrandIdsByCommunity()[communityId]);
               }

               if (null != optionCollection)
               {
                   cachedOptionCollection = new CacheAttributeOptionCollection(optionCollection, attributeName, communityId);
                   Cache.Instance.Add(cachedOptionCollection);
               }
           }
           else
           {
               optionCollection = cachedOptionCollection.OptionCollection;
           }
           return optionCollection;
       }

       public static AttributeOptionCollection GetAttributeOptionCollection(string attributeName, int communityId)
       {
           CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(attributeName, communityId)) as CacheAttributeOptionCollection;
           if (null == cachedOptionCollection)
           {
               AttributeOptionCollection optionCollection=AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, Constants.NULL_INT);
               cachedOptionCollection = new CacheAttributeOptionCollection(optionCollection, attributeName, communityId);
               Cache.Instance.Add(cachedOptionCollection);
           }
           return cachedOptionCollection.OptionCollection;           
       }

       public static AttributeOptionCollection GetAttributeOptionCollection(string attributeName, int communityId, int siteId, int brandId)
       {
           CacheAttributeOptionCollection cachedOptionCollection = Cache.Instance.Get(CacheAttributeOptionCollection.GetCacheKeyString(attributeName, communityId)) as CacheAttributeOptionCollection;
           if (null == cachedOptionCollection)
           {
               AttributeOptionCollection optionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, communityId, siteId, brandId);
               if (null != optionCollection)
               {
                   cachedOptionCollection = new CacheAttributeOptionCollection(optionCollection, attributeName, communityId);
                   Cache.Instance.Add(cachedOptionCollection);
               }
           }
           return cachedOptionCollection.OptionCollection;
       }

       public static double[] ConvertRadiansToDegrees(double latRadians, double lngRadians)
       {
           double [] degrees = new double[2];
           degrees[0] = DistanceUtils.ToDegrees(latRadians);
           degrees[1] = DistanceUtils.ToDegrees(lngRadians);
           return degrees;
       }

       //gets current index directory for searcher server if swapping directory index is enabled - ENABLE_INDEX_DIR_SWAP_UPDATES
       public static string GetCurrentIndexDirectory(int communityid, IProcessorType iProcessorType, string server, string caller)
       {
           string path = "";

           SqlDataReader rs = null;
           try
           {
               //yeah, no caching for now
               //but it shouldn't be big load
               string dbName = GetIndexUpdateDB(communityid, caller, iProcessorType);
               Command command = new Command(dbName, "[up_GetCurrentIndexDir]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityid);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, server);
               command.AddParameter("@IProcessorTypeID", SqlDbType.Int, ParameterDirection.Input, (int)iProcessorType);
               rs = Client.Instance.ExecuteReader(command);
               if (rs.Read())
               {
                   path = rs["path"].ToString();
               }               
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, caller, "GetCurrentIndexDirectory", ex, communityid.ToString());
               throw (new BLException(ex));
           }
           finally
           {
               rs.Dispose();
           }
           return path;
       }

       public static void UpdateIndexDirectory(int communityId, IProcessorType iProcessorType, string server, string indexPath, string caller)
       {
           try
           {
               string dbName = GetIndexUpdateDB(communityId, caller, iProcessorType);
               Command command = new Command(dbName, "[up_InsertCurrentIndexDir]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, server);
               command.AddParameter("@Path", SqlDbType.VarChar, ParameterDirection.Input, indexPath);
               command.AddParameter("@IProcessorTypeID", SqlDbType.Int, ParameterDirection.Input, (int)iProcessorType);
               Client.Instance.ExecuteAsyncWrite(command);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, caller, "updateIndexDirectory", ex, null);
           }
       }

       public static int CalculateAge(DateTime birthDate)
       {
           // cache the current time
           DateTime now = DateTime.Today; // today is fine, don't need the timestamp from now
           // get the difference in years
           int years = now.Year - birthDate.Year;
           // subtract another year if we're before the
           // birth day in the current year
           if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
               --years;
           return years;
       }

       public static Dictionary<int, int> GetChildrenCountConversionTable()
       {
           if (childrenCountTable.Count == 0)
           {
               //NOTE: DB is used by profile info, Web is used by Preferences
               // 0 children, db = 0, web = 1
               childrenCountTable[0] = 1;
               // 1 child, db = 1, web = 2
               childrenCountTable[1] = 2;
               // 2 children, db = 2, web = 4			
               childrenCountTable[2] = 4;
               // 3 or more children, db = 3, web = 8			
               childrenCountTable[3] = 8;
           }
           return childrenCountTable;
       }

       public static Dictionary<int, int> GetMoreChildrenFlagConversionTable()
       {
           if (moreChildrenFlagTable.Count == 0)
           {
               //NOTE: DB is used by preferences, Web is used by profile
               // No more children, db = 1, web = 0
               moreChildrenFlagTable[1] = 0;
               // Yes more children, db = 2, web = 1
               moreChildrenFlagTable[2] = 1;
               // Not sure, db = 4, web = 2
               moreChildrenFlagTable[4] = 2;
           }
           return moreChildrenFlagTable;
       }

       public static Dictionary<int, int> GetMoreChildrenFlagConversionTableReverse()
       {
           if (moreChildrenFlagTableReverse.Count == 0)
           {
               //NOTE: DB is used by preferences, Web is used by profile
               // No more children, db = 1, web = 0
               moreChildrenFlagTableReverse[0] = 1;
               // Yes more children, db = 2, web = 1
               moreChildrenFlagTableReverse[1] = 2;
               // Not sure, db = 4, web = 2
               moreChildrenFlagTableReverse[2] = 4;
           }
           return moreChildrenFlagTableReverse;
       }

       public static Dictionary<int, int> GetBrandIdsByCommunity()
       {
           if (brands.Count == 0)
           {
               brands[1] = 101;
               brands[3] = 1003;
               brands[10] = 1015;
               brands[21] = 9161;
               brands[23] = 9041;
               brands[24] = 9051;
           }
           return brands;
       }

       public static Dictionary<int, int> GetSiteIdsByCommunity()
       {
           if (sites.Count == 0)
           {
               sites[1] = 101;
               sites[3] = 103;
               sites[10] = 15;
               sites[21] = 9161;
               sites[23] = 9041;
               sites[24] = 9051;
               sites[25] = 9081;
           }
           return brands;
       }


       public static QueryBuilder GetTestQuery(int communityId, int siteId)
       {
           SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
           searchPrefs.Add("countryregionid", "223");
           searchPrefs.Add("domainid", communityId.ToString());
           searchPrefs.Add("searchtype", "2");
           searchPrefs.Add("searchtypeid", "1");
           //searchPrefs.Add("searchorderby", "1");
           searchPrefs.Add("minage", "18");
           searchPrefs.Add("maxage", "88");
           searchPrefs.Add("gendermask", "6");
           //searchPrefs.Add("radius", "500");
           //searchPrefs.Add("regionid", "3403764");
           //TODO: Change to use same query builder as search service. first that query builder needs to be moved to a shared dll.
           IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityId, siteId);
           QueryBuilder qb = QueryBuilderFactory.Instance.ConvertQuery(q, communityId);
           return qb;
       }

       public static QueryBuilder GetTestKeywordQuery(int communityId, int siteId)
       {
           SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
           searchPrefs.Add("domainid", communityId.ToString());
           IMatchnetQuery q = TestQueryBuilder.build(searchPrefs, communityId, siteId);
           QueryBuilder qb = QueryBuilderFactory.Instance.ConvertKeywordQuery(q, communityId);
           return qb;
       }

       public static double GetRadiusOffsetBuffer(double radius)
       {
           double offset = (radius + 1.5) / 10;
           return radius + offset;
       }
        #endregion

       #region Match Ratings Related

       public static bool IsMatchMismatchDataEnabled(int communityID)
       {
           bool isEnabled = false;
           if (GetSetting("ENABLE_MATCH_RATING_CAPTURE_MISMATCH_DATA", communityID, false))
           {
               isEnabled = true;
           }

           return isEnabled;
       }

       public static SearchPreferenceCollection GetDefaultSearchPreferences(int memberId, int communityId, int genderMask, DateTime birthDate, int regionID, bool isForIndexing)
       {
           //these are default match search preferences
           //note: these follows the logic currently stored in a stored procedure: mnMember..up_SearchPreference_ListDefault
           SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();

           try
           {                          
               if (genderMask <= 0)
               {
                   genderMask = 9; //default: male seeking female
               }
                      
               //gendermask is saved as opposite for preferences
               //min and max age defaults will depend on the gender
               int preferredGenderMask = 6; //default: female seeking male
               int age = 0;
               if (birthDate != DateTime.MinValue)
               {
                   age = (int) ((double) DateTime.Now.Subtract(birthDate).Days/365.25);
               }
               if (age < 18) age = 25; //default age

               int minAge = 0;
               int maxAge = 0;
               if ((genderMask & 1) == 1) //male
               {
                   if ((genderMask & 8) == 8) //seeking female
                   {
                       minAge = age - 10;
                       maxAge = age + 5;
                       preferredGenderMask = 6; //female seeking male
                   }
                   else
                   {
                       minAge = age - 5;
                       maxAge = age + 5;
                       preferredGenderMask = 5; //male seeking male
                   }
               }
               else //female
               {
                   if ((genderMask & 4) == 4) //seeking male
                   {
                       minAge = age - 5;
                       maxAge = age + 10;
                       preferredGenderMask = 9; //male seeking female
                   }
                   else
                   {
                       minAge = age - 5;
                       maxAge = age + 5;
                       preferredGenderMask = 10; //female seeking female
                   }
               }

               if (minAge < 18) minAge = 18; //default min age
               if (maxAge > 99 || maxAge < minAge) maxAge = 99; //default max age
               searchPrefs.Add("minage", minAge.ToString());
               searchPrefs.Add("maxage", maxAge.ToString());
               searchPrefs.Add("gendermask", preferredGenderMask.ToString());

               //location
               if (regionID > 0)
               {
                   searchPrefs.Add("regionid", regionID.ToString());
               }
               else
               {
                   searchPrefs.Add("regionid", "3443817"); //beverly hills
               }
           
               //distance
               searchPrefs.Add("distance", "50");

               //has photos
               searchPrefs.Add("hasphotoflag", "1"); //photo only

               //additional default prefs
               if (!isForIndexing)
               {
                   searchPrefs.Add("searchorderby", "1"); //newest
                   searchPrefs.Add("searchtypeid", "4"); //matches search
                   searchPrefs.Add("countryregionid", "");
               }

               //jdate community only
               if (communityId == 3)
               {
                   searchPrefs.Add("jdatereligion", "919551"); //jewish religion only
               }
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "GetDefaultSearchPreferences()", ex, string.Format("memberID: {0}, communityid: {1}", memberId, communityId));
           }

           return searchPrefs;
       }

       #endregion

       public static List<string> getSearcherServers(int communityId)
       {
           List<string> servers = new List<string>();

           try
           {
               bool useSearcherByCommunity = Boolean.TrueString.ToLower().Equals(GetSetting("SEARCHER_PER_COMMUNITY", "false"));
               //if flag set use community id passed in, otherwise default to sparksearcher for all communities
               int cid = (useSearcherByCommunity) ? communityId : (int)ServiceConstants.COMMUNITY_ID.Spark;

               string searcherConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(cid).ToUpper());
               ArrayList partitions = AdapterConfigurationSA.GetServicePartitions(searcherConstant);
               for (int i = 0; null != partitions && i < partitions.Count; i++)
               {
                   ServicePartition p = partitions[i] as ServicePartition;
                   if (null != p)
                   {
                       for (int k = 0; k < p.Count; k++)
                       {
                           ServiceInstance si = p[k];
                           if (servers.Find(delegate(string s) { return s == si.HostName.ToLower(); }) == null)
                           {
                               servers.Add(si.HostName.ToLower());
                           }
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "getSearcherServers()", ex, null);
           }

           return servers;
       }

       public static string GetUNCTargetPath(string indexPath, int rollingIndexDirId, string server)
       {
           string uncPath = indexPath.Replace("/", "\\").Replace(":\\\\", ":\\");
           string targetpath = "\\\\{0}\\" + uncPath.Replace(":\\", "$\\") + rollingIndexDirId.ToString();
           string serverpath = string.Format(targetpath, server);
           return serverpath;
       }

       public static string IndexDirectoryInUse(CommunityIndexConfig c)
       {
           List<string> servers = getSearcherServers(c.CommunityID);
           string directoryInUse = string.Empty;
           foreach (string server in servers)
           {
               string indexDirectoryInUseForServer = IndexDirectoryInUseForServer(c, server);
               if(!string.IsNullOrEmpty(indexDirectoryInUseForServer))
               {
                   directoryInUse = indexDirectoryInUseForServer;
               }
           }
           return directoryInUse;
       }

       public static string IndexDirectoryInUseForServer(CommunityIndexConfig c, string server)
       {
           string directoryInUse=string.Empty;
               string uncPath = GetUNCTargetPath(c.IndexPath, c.RollingIndexDirectoryID, server);
               if (Directory.Exists(uncPath))
               {
                   string[] files = Directory.GetFiles(uncPath);
                   //don't overwrite an index if it is in use
                   foreach(string fileName in files)
                   {
                       if (fileName.EndsWith(string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, server)))
                       {
                           directoryInUse = uncPath;
                   }
               }
           }
           return directoryInUse;
       }

       public static void CopyIndex(string sourcepath, string targetpath)
       {
           try
           {
               if (!Directory.Exists(targetpath))
               {
                   Directory.CreateDirectory(targetpath);
               }

               DirectoryInfo dirinfo = new DirectoryInfo(sourcepath);
               foreach (FileInfo f in dirinfo.GetFiles())
               {
                   string dest = targetpath + "\\" + f.Name;
                   f.CopyTo(dest, true);
               }
           }
           catch (Exception ex)
           {
               BLException blex = new BLException("Error copying index dir: " + sourcepath + " to " + targetpath + ", Inner Exception: " + ex.ToString(), ex);
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "CopyIndex", blex, null);
           }
       }

       public static void CleanDir(string path)
       {
           try
           {
               if (Directory.Exists(path))
               {
                   RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "Cleaning Index Dir: " + path, null);

                   DirectoryInfo dirinfo = new DirectoryInfo(path);
                   CleanDir(dirinfo);
                   RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "Finish Cleaning Index Dir: " + path, null);
               }

           }
           catch (Exception ex)
           {
               BLException blex = new BLException("Error cleaning index dir: " + path + ", Inner Exception: " + ex.ToString(), ex);
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "CleanDir", blex, null);
           }
       }

       private static void CleanDir(DirectoryInfo dirinfo)
       {
           foreach (FileInfo f in dirinfo.GetFiles())
           {
               f.Delete();
           }
           foreach (DirectoryInfo dir in dirinfo.GetDirectories())
           {
               dir.Delete(true);
           }
       }


       public static void updateLastIndexDate(CommunityIndexConfig c)
       {
           DateTime lastindexdate = DateTime.MinValue;
           try
           {
               int communityId = c.CommunityID;
               string caller = LoaderBL.CLASS_NAME;
               IProcessorType iProcessorType = c.IProcessorType;
               string dbName = ServiceConstants.SYSTEM_DB_NAME;
               Command command = new Command(dbName, "[up_InsertLastIndexDate]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, c.CommunityID);
               command.AddParameter("@IndexDate", SqlDbType.DateTime, ParameterDirection.Input, c.LastIndexDate);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, Environment.MachineName);
               command.AddParameter("@Rollingdirid", SqlDbType.Int, ParameterDirection.Input, c.RollingIndexDirectoryID);
               Client.Instance.ExecuteAsyncWrite(command);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "getLastIndexDate", ex, null);
           }
       }

       public static void CopyIndexToServer(object m)
       {
           string sourcepath = String.Empty;
           string serverPath = String.Empty;
           try
           {
               Hashtable map = m as Hashtable;
               CommunityIndexConfig config = map["config"] as CommunityIndexConfig;
               string server = map["server"] as string;
               serverPath = GetUNCTargetPath(config.IndexPath, config.RollingIndexDirectoryID, server);
               sourcepath = map["sourcepath"] as string;
               Stopwatch stopWatch = new Stopwatch();
               RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "Copying Index Dir: " + sourcepath + " to " + serverPath, null);
               CleanDir(serverPath as string);
               CopyIndex(sourcepath as string, serverPath as string);
               stopWatch.Stop();
               RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "Total Copy Time: " + ((stopWatch.ElapsedMilliseconds / 1000) / 60) + " minutes. Finished Copying Index Dir: " + sourcepath + " to " + serverPath, null);
               UpdateIndexDirectory(config.CommunityID, config.IProcessorType, server, serverPath, LoaderBL.CLASS_NAME);
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, String.Format("CopyIndexToServer: [srcpath:{0}, destpath:{1}]", sourcepath, serverPath), ex, null);
           }
       }

       public static DateTime getLastIndexDate(int communityid, out int rollingdirid, IProcessorType iProcessorType)
       {
           DateTime lastindexdate = DateTime.MinValue;
           SqlDataReader rs = null;
           rollingdirid = 0;
           try
           {
               string caller = LoaderBL.CLASS_NAME;
               string dbName = ServiceConstants.SYSTEM_DB_NAME;
               Command command = new Command(dbName, "[up_GetLastIndexDate]", 0);

               command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityid);
               command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, Environment.MachineName);
               SqlParameterCollection sqlParams;
               rs = Client.Instance.ExecuteReader(command);

               if (rs.Read())
               {
                   lastindexdate = Conversion.CDateTime(rs["indexdate"]);
                   rollingdirid = Conversion.CInt(rs["rollingdirid"]);
               }
           }
           catch (Exception ex)
           {
               RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, LoaderBL.CLASS_NAME, "getLastIndexDate", ex, null);
           }
           finally
           {
               if (rs != null)
               { rs.Dispose(); }
           }
           return lastindexdate;
       }

       public static string CalculateIndexPath(CommunityIndexConfig c)
       {
           for (int i = 0; i < MAX_ROLLING_DIRS; i++)
           {
               string indexDirectoryInUse = IndexDirectoryInUse(c);
               if (!String.IsNullOrEmpty(indexDirectoryInUse))
               {
                   c.RollingIndexDirectoryID = (c.RollingIndexDirectoryID < MAX_ROLLING_DIRS) ? c.RollingIndexDirectoryID + 1 : 1;
               }
               else
               {
                   break;
               }
           }
           string path = c.IndexStagePath + c.RollingIndexDirectoryID.ToString();
           return path;
       }

       public static ExceptionBase ConvertException(string appName, Exception e, IValueObject obj)
       {
           return new BLException(ServiceTrace.Instance.getTrace(appName,e,obj));
       }

       public static bool IsNoisyLoggingEnabled()
       {
           try
           {
               if (System.Configuration.ConfigurationManager.AppSettings["EnableNoisyLogging"].ToLower() == "true")
                   return true;
               else
                   return false;
           }
           catch (Exception ex)
           {
               //probably missing setting
               return false;
           }
       }

       public static bool IsSearchPrefCacheDisabled()
       {
           try
           {
               if (System.Configuration.ConfigurationManager.AppSettings["DisableSearchPrefsCache"].ToLower() == "true")
                   return true;
               else
                   return false;
           }
           catch (Exception ex)
           {
               //probably missing setting
               return false;
           }
       }
  
    }

   public class ServiceTrace : IValueObject
   {
       private const string FORMAT_STR = "App Source:{0}\r\n============\r\n Ex.Source:{1} - Ex.Msg:{2}\r\n============";
       private const string FORMAT_STR_TRACE = "App Source:{0}\r\n============\r\nTrace:{1}\r\n============";
       private const string DETAILED_FORMAT_STR = "App Source:{0}.{1}\r\n============\r\n Ex.Source:{2} - Ex.Msg:{3}\r\n============";
       //private const string  FORMAT_STR="App Source:{0}\r\nEx.Source:{1}\r\nEx.Msg:{2}";
       public static readonly ServiceTrace Instance = new ServiceTrace();

       private ServiceTrace()
       {
           //
           // TODO: Add constructor logic here
           //
       }

       public string getTrace(string appSource, Exception ex, IValueObject obj)
       {
           string exStr = "";
           string innerexStr = "";

           try
           {
               exStr = String.Format(FORMAT_STR, appSource, ex.Source, ex.Message);
               if (ex.InnerException != null)
                   innerexStr = String.Format(FORMAT_STR, appSource, ex.InnerException.Source, ex.InnerException.Message);
               exStr += innerexStr;
               if (obj != null)
                   exStr += "\r\nValue Obj:" + obj.ToString();

               return exStr;
           }
           catch (Exception e)
           {
               e = null;
               return exStr;
           }
       }

       public String GetCommandObject(Matchnet.Data.Command comm)
       {
           string commString = "";
           string commFormat = "Comm. Name {0}\r\nDatabase:{1}\r\nParams:{2}";
           try
           {
               if (comm == null)
                   return commString;
               //parameters

               StringBuilder parameters = new StringBuilder();
               for (int i = 0; i < comm.Parameters.Count; i++)
               {
                   Parameter p = comm.Parameters[i];
                   parameters.Append(p.Name + ":" + p.ParameterValue.ToString() + "\r\n");
               }

               commString = String.Format(commFormat, comm.StoredProcedureName, comm.LogicalDatabaseName, parameters.ToString());

               parameters = null;
               return commString;
           }
           catch (Exception e)
           {
               return commString;
           }
       }
   }

   public class CacheAttributeOptionCollection : ICacheable
   {
       private AttributeOptionCollection _optionCollection = null;
       private int _communityId;
       private string _attributeName;
       private int _cacheTTLSeconds = 540;
       private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
       private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
       const string CACHEKEYFORMAT = "AttributeOption-{0}-{1}";

       public AttributeOptionCollection OptionCollection
       {
           get { return _optionCollection; }
           set { _optionCollection = value; }
       }

       public int CommunityId
       {
           get { return _communityId; }
           set { _communityId = value; }
       }

       public string AttributeName
       {
           get { return _attributeName; }
           set { _attributeName = value; }
       }

       public CacheAttributeOptionCollection(AttributeOptionCollection collection, string attributeName, int commmunityId)
       {
           this._communityId = commmunityId;
           this._attributeName = attributeName;
           this._optionCollection = collection;
       }

       #region ICacheable Members

       public CacheItemMode CacheMode
       {
           get { return _cacheItemMode; }
           set { _cacheItemMode = value; }
       }

       public CacheItemPriorityLevel CachePriority
       {
           get { return _cachePriority; }
           set { _cachePriority = value; }
       }

       public int CacheTTLSeconds
       {
           get { return _cacheTTLSeconds; }
           set { _cacheTTLSeconds = value; }
       }

       public string GetCacheKey()
       {
           return GetCacheKeyString(_attributeName, _communityId);
       }

       public static string GetCacheKeyString(string attributeName, int communityId)
       {
           return string.Format(CACHEKEYFORMAT, attributeName, communityId);
       }


       #endregion
   }

   public static class Extensions
   {
       public static IList<TResult> GetBitsAs<TResult>(this BitArray bits) where TResult : struct
       {
           return GetBitsAs<TResult>(bits, 0);
       }

       /// <summary>
       /// Gets the bits from an BitArray as an IList combined to the given type.
       /// </summary>
       /// <typeparam name="TResult">The type of the result.</typeparam>
       /// <param name="bits">An array of bit values, which are represented as Booleans.</param>
       /// <param name="index">The zero-based index in array at which copying begins.</param>
       /// <returns>An read-only IList containing all bits combined to the given type.</returns>
       public static IList<TResult> GetBitsAs<TResult>(this BitArray bits, int index) where TResult : struct
       {
           var instance = default(TResult);
           var type = instance.GetType();
           int sizeOfType = Marshal.SizeOf(type);

           int arraySize = (int)Math.Ceiling(((bits.Count - index) / 8.0) / sizeOfType);
           var array = new TResult[arraySize];

           bits.CopyTo(array, index);

           return array;
       }
   }
}
