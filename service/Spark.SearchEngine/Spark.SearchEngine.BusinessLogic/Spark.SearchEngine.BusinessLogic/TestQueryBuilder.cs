﻿using System;
using System.Collections;
using System.Text.RegularExpressions;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Spark.SearchEngine.ValueObjects;

//HACK: This was pulled from Search service since I didn't want a circular dependency.
namespace Spark.SearchEngine.BusinessLogic
{
    /// <summary>
    /// Utility class for transforming the SearchPreferenceCollection (VO) into an instance
    /// of IMatchnetQuery (VO).
    /// </summary>
    public class TestQueryBuilder
    {
        // These constant definitions correspond to the string representation of search preferences.
        public const string AGEMAX = "maxage";
        public const string AGEMIN = "minage";
        public const string ALLTEXTFIELDS = "alltextfields";
        public const string AREACODE = "areacodes";
        public const string BODYTYPE = "bodytype";
        public const string DISPLAYPHOTOS = "displayphotostoguests";
        public const string DOMAINID = "domainid";
        public const string DRINKINGHABITS = "drinkinghabits";
        public const string EDUCATIONLEVEL = "educationlevel";
        public const string ESSAYWORDS = "essaywords";
        public const string ETHNICITY = "ethnicity";
        public const string GENDERMASK = "gendermask";
        public const string GEODISTANCE = "geodistance";
        public const string HASPHOTOFLAG = "hasphotoflag";
        public const string HEIGHTMAX = "maxheight";
        public const string HEIGHTMIN = "minheight";
        public const string INSERTDATE = "insertdate";
        public const string JDATEETHNICITY = "jdateethnicity";
        public const string JDATERELIGION = "jdatereligion";
        public const string KEEPKOSHER = "keepkosher";
        public const string LANGUAGEMASK = "languagemask";
        public const string LASTLOGINDATE = "lastlogindate";
        public const string LASTUPDATED = "lastupdated";
        public const string LATITUDE = "latitude";
        public const string LONGITUDE = "longitude";
        public const string MAJORTYPE = "majortype";
        public const string MARITALSTATUS = "maritalstatus";
        public const string REGIONID = "regionid";
        public const string REGIONIDCITY = "regionidcity";
        public const string REGIONIDCOUNTRY = "countryregionid";
        public const string RELATIONSHIPMASK = "relationshipmask";
        public const string RELATIONSHIPSTATUS = "relationshipstatus";
        public const string RELIGION = "religion";
        public const string SCHOOLID = "schoolid";
        public const string SEARCHORDERBY = "searchorderby";
        public const string SEXUALIDENTITYTYPE = "sexualidentitytype";
        public const string SMOKINGHABITS = "smokinghabits";
        public const string SYNAGOGUEATTENDANCE = "synagogueattendance";
        public const string USERNAME = "username";
        public const string ZIPCODE = "zipcode";
        public const string ZODIAC = "zodiac";
        public const string WEIGHTMIN = "weightmin";
        public const string WEIGHTMAX = "weightmax";
        public const string CHILDRENCOUNT = "childrencount";
        public const string RELOCATEFLAG = "relocateflag";
        public const string ACTIVITYLEVEL = "activitylevel";
        public const string CUSTODY = "custody";
        public const string MORECHILDRENFLAG = "morechildrenflag";
        public const string COLORCODE = "colorcode";
        public const string RADIUS = "radius";
        public const string MEMBERID = "memberid";
        public const string BLOCKEDMEMBERIDS = "blockedmemberids";
        public const string SEARCHREDESIGN30 = "searchredesign30";
		public const string DISTANCE = "distance";
		public const string PREFERREDAGE = "preferredage";
        public const string PREFERREDGENDERMASK = "preferredgendermask";
 		public const string REGISTRATIONDATEMIN = "registrationdatemin";
        public const string REGISTRATIONDATEMAX = "registrationdatemax";
        public const string SUBEXPIRATIONDATEMIN = "subexpirationdatemin";
        public const string SUBEXPIRATIONDATEMAX = "subexpirationdatemax";
        public const string PREFERREDRELOCATION = "preferredrelocation";
        public const string PREFERREDHEIGHT = "preferredheight";
        public const string PREFERREDSYNAGOGUEATTENDANCE = "preferredsynagogueattendance";
        public const string PREFERREDJDATERELIGION = "preferredjdatereligion";
        public const string PREFERREDSMOKINGHABIT = "preferredsmokinghabit";
        public const string PREFERREDDRINKINGHABIT = "preferreddrinkinghabit";
        public const string PREFERREDJDATEETHNICITY = "preferredjdateethnicity";
        public const string PREFERREDACTIVITYLEVEL = "preferredactivitylevel";
        public const string PREFERREDKOSHERSTATUS = "preferredkosherstatus";
        public const string PREFERREDMARITALSTATUS = "preferredmaritalstatus";
        public const string PREFERREDLANGUAGEMASK = "preferredlanguagemask";
        public const string PREFERREDDISTANCE = "preferreddistance";
        public const string PREFERREDREGIONID = "preferredregionid";
        public const string PREFERREDAREACODES = "preferredareacodes";
        public const string PREFERREDHASPHOTOFLAG = "preferredhasphotoflag";
        public const string PREFERREDEDUCATIONLEVEL = "preferrededucationlevel";
        public const string PREFERREDRELIGION = "preferredreligion";
        public const string PREFERREDETHNICITY = "preferredethnicity";
        public const string PREFERREDSEXUALIDENTITYTYPE = "preferredsexualidentitytype";
        public const string PREFERREDRELATIONSHIPMASK = "preferredrelationshipmask";
        public const string PREFERREDRELATIONSHIPSTATUS = "preferredrelationshipstatus";
        public const string PREFERREDBODYTYPE = "preferredbodytype";
        public const string PREFERREDZODIAC = "preferredzodiac";
        public const string PREFERREDMAJORTYPE = "preferredmajortype";
        public const string PREFERREDMORECHILDRENFLAG = "preferredmorechildrenflag";
        public const string PREFERREDCHILDRENCOUNT = "preferredchildrencount";
        public const string PREFERREDCUSTODY = "preferredcustody";
        public const string PREFERREDCOLORCODE = "preferredcolorcode";    
        public const string KEYWORDSEARCH = "keywordsearch";
        public const string MAXNUMBEROFMATCHES = "maxnumberofmatches";

        //member attribute for match calculation in query, append "has" to the front
        public const string HASBIRTHDATE = "hasbirthdate";
        public const string HASMARITALSTATUS = "hasmaritalstatus";
        public const string HASJDATERELIGION = "hasjdatereligion";
        public const string HASRELIGION = "hasreligion";
        public const string HASKEEPKOSHER = "haskeepkosher";
        public const string HASSYNAGOGUEATTENDANCE = "hassynagogueattendance";
        public const string HASEDUCATIONLEVEL = "haseducationlevel";
        public const string HASCHILDRENCOUNT = "haschildrencount";
        public const string HASMORECHILDRENFLAG = "hasmorechildrenflag";
        public const string HASBODYTYPE = "hasbodytype";
        public const string HASLANGUAGEMASK = "haslanguagemask";
        public const string HASHEIGHT = "hasheight";
        public const string HASSMOKINGHABITS = "hassmokinghabits";
        public const string HASACTIVITYLEVEL = "hasactivitylevel";
        public const string HASCUSTODY = "hascustody";
        public const string HASDRINKINGHABITS = "hasdrinkinghabits";
        public const string HASRELOCATEFLAG = "hasrelocateflag";
        public const string HASJDATEETHNICITY = "hasjdateethnicity";
        public const string HASETHNICITY = "hasethnicity";
        public const string HASMEMBERID = "hasmemberid";

        private static ISettingsSA _settingsService;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }

            set { _settingsService = value; }
        }

        private TestQueryBuilder(){}

        /// <summary>
        /// Use this method to transform a SearchPreferenceCollection value object into an IMatchnetQuery
        /// (utilized by the Matchnet.FAST query engine).
        /// </summary>
        /// <param name="pPreferences"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pSiteID"></param>
        /// <returns></returns>
        public static IMatchnetQuery build(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            IMatchnetQuery query = new MatchnetQuery(pPreferences.Count) as IMatchnetQuery;

            IMatchnetQueryParameter queryParameter = null;
            ICollection keys = pPreferences.Keys();

            foreach (string key in keys)
            {
                // The FAST engine does not care about params that are <= 0 OR null for mask preferences.
                if (pPreferences[key] != null && pPreferences[key].Trim() != string.Empty)
                {
                    if (key == KEYWORDSEARCH)
                    {
                        if (IsKeywordMatchesSearchPrefEnabled(Constants.NULL_INT, pSiteID, pCommunityID))
                        {
                            queryParameter = buildQueryParameter(key, Sanitize(pPreferences[key]));
                        }
                    }
                    else if (key == HASBIRTHDATE)
                    {
                        queryParameter = buildQueryParameter(key, Sanitize(pPreferences[key]));
                    }
                    else
                    {
                        double preferenceVal;
                        // check for numeric preference values that are zero or negative, ignore them when encountered
                        preferenceVal = Conversion.CDouble(pPreferences[key]);

                        if (preferenceVal != (double)Constants.NULL_INT)
                        {
                            queryParameter = buildQueryParameter(key, pPreferences[key]);
                        }
                    }

                    if (queryParameter != null)
                    {
                        query.AddParameter(queryParameter);
                    }
                }
            }

            if (!string.IsNullOrEmpty(pPreferences["SearchType"]))
            {
                try
                {
                    query.SearchType = (SearchType) Enum.Parse(typeof (SearchType), pPreferences["SearchType"]);
                }
                catch (Exception ex)
                {
                    query.SearchType = SearchType.WebSearch;
                }
            }
            else
            {
                query.SearchType = SearchType.WebSearch;
            }
            return query;
        }

        public static string Sanitize(string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            //remove special characters that lucene uses for advanced searching
            string sanitizedString = Regex.Replace(s, @"[^\'\""\,\;\w\.@-]", " ");
            return sanitizedString.Trim();
        }

        public static void PopulateSearchPreferences(SearchMember searchMember, SearchPreferenceCollection searchPreferences)
        {
            if (null != searchPreferences)
            {
                if ((searchPreferences.ContainsKey(TestQueryBuilder.ACTIVITYLEVEL) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.ACTIVITYLEVEL])))
                {
                    searchMember.PreferredActivityLevel = Convert.ToInt32(searchPreferences[TestQueryBuilder.ACTIVITYLEVEL]);
                }
                else
                    searchMember.PreferredActivityLevel = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.AREACODE) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.AREACODE])))
                {
                    searchMember.PreferredAreaCodes = searchPreferences[TestQueryBuilder.AREACODE];
                }
                else
                    searchMember.PreferredAreaCodes = "";

                if ((searchPreferences.ContainsKey(TestQueryBuilder.BODYTYPE) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.BODYTYPE])))
                {
                    searchMember.PreferredBodyType = Convert.ToInt32(searchPreferences[TestQueryBuilder.BODYTYPE]);
                }
                else
                    searchMember.PreferredBodyType = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.CHILDRENCOUNT) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.CHILDRENCOUNT])))
                {
                    searchMember.PreferredChildrenCount =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.CHILDRENCOUNT]);
                }
                else
                    searchMember.PreferredChildrenCount = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.CUSTODY) &&
                        !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.CUSTODY])))
                {
                    searchMember.PreferredCustody = Convert.ToInt32(searchPreferences[TestQueryBuilder.CUSTODY]);
                }
                else
                    searchMember.PreferredCustody = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.DISTANCE) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.DISTANCE])))
                {
                    searchMember.PreferredDistance = Convert.ToInt32(searchPreferences[TestQueryBuilder.DISTANCE]);
                }
                else
                    searchMember.PreferredDistance = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.DRINKINGHABITS) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.DRINKINGHABITS])))
                {
                    searchMember.PreferredDrinkingHabit =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.DRINKINGHABITS]);
                }
                else
                    searchMember.PreferredDrinkingHabit = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.EDUCATIONLEVEL) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.EDUCATIONLEVEL])))
                {
                    searchMember.PreferredEducationLevel =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.EDUCATIONLEVEL]);
                }
                else
                    searchMember.PreferredEducationLevel = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.ETHNICITY) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.ETHNICITY])))
                {
                    searchMember.PreferredEthnicity = Convert.ToInt32(searchPreferences[TestQueryBuilder.ETHNICITY]);
                }
                else
                    searchMember.PreferredEthnicity = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.GENDERMASK) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.GENDERMASK])))
                {
                    searchMember.PreferredGenderMask =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.GENDERMASK]);
                }
                else
                    searchMember.PreferredGenderMask = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.HASPHOTOFLAG) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.HASPHOTOFLAG])))
                {
                    searchMember.PreferredHasPhotoFlag =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.HASPHOTOFLAG]);
                }
                else
                    searchMember.PreferredHasPhotoFlag = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.JDATEETHNICITY) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.JDATEETHNICITY])))
                {
                    searchMember.PreferredJdateEthnicity =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.JDATEETHNICITY]);
                }
                else
                    searchMember.PreferredJdateEthnicity = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.JDATERELIGION) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.JDATERELIGION])))
                {
                    searchMember.PreferredJdateReligion =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.JDATERELIGION]);
                }
                else
                    searchMember.PreferredJdateReligion = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.KEEPKOSHER) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.KEEPKOSHER])))
                {
                    searchMember.PreferredKosherStatus =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.KEEPKOSHER]);
                }
                else
                    searchMember.PreferredKosherStatus = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.LANGUAGEMASK) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.LANGUAGEMASK])))
                {
                    searchMember.PreferredLanguageMask =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.LANGUAGEMASK]);
                }
                else
                    searchMember.PreferredLanguageMask = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.MAJORTYPE) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.MAJORTYPE])))
                {
                    searchMember.PreferredMajorType = Convert.ToInt32(searchPreferences[TestQueryBuilder.MAJORTYPE]);
                }
                else
                    searchMember.PreferredMajorType = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.MARITALSTATUS) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.MARITALSTATUS])))
                {
                    searchMember.PreferredMaritalStatus =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.MARITALSTATUS]);
                }
                else
                    searchMember.PreferredMaritalStatus = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.AGEMAX) &&
                        !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.AGEMAX])))
                {
                    searchMember.PreferredMaxAge = Convert.ToInt32(searchPreferences[TestQueryBuilder.AGEMAX]);
                }
                else
                    searchMember.PreferredMaxAge = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.AGEMIN) &&
                        !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.AGEMIN])))
                {
                    searchMember.PreferredMinAge = Convert.ToInt32(searchPreferences[TestQueryBuilder.AGEMIN]);
                }
                else
                    searchMember.PreferredMinAge = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.HEIGHTMAX) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.HEIGHTMAX])))
                {
                    searchMember.PreferredMaxHeight = Convert.ToInt32(searchPreferences[TestQueryBuilder.HEIGHTMAX]);
                }
                else
                    searchMember.PreferredMaxHeight = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.HEIGHTMIN) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.HEIGHTMIN])))
                {
                    searchMember.PreferredMinHeight = Convert.ToInt32(searchPreferences[TestQueryBuilder.HEIGHTMIN]);
                }
                else
                    searchMember.PreferredMinHeight = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.MORECHILDRENFLAG) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.MORECHILDRENFLAG])))
                {
                    searchMember.PreferredMoreChildrenFlag =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.MORECHILDRENFLAG]);
                }
                else
                    searchMember.PreferredMoreChildrenFlag = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.REGIONID) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.REGIONID])))
                {
                    searchMember.PreferredRegionId = Convert.ToInt32(searchPreferences[TestQueryBuilder.REGIONID]);
                }
                else
                    searchMember.PreferredRegionId = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.RELATIONSHIPMASK) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.RELATIONSHIPMASK])))
                {
                    searchMember.PreferredRelationshipMask =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.RELATIONSHIPMASK]);
                }
                else
                    searchMember.PreferredRelationshipMask = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.RELATIONSHIPSTATUS) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.RELATIONSHIPSTATUS])))
                {
                    searchMember.PreferredRelationshipstatus =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.RELATIONSHIPSTATUS]);
                }
                else
                    searchMember.PreferredRelationshipstatus = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.RELIGION) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.RELIGION])))
                {
                    searchMember.PreferredReligion = Convert.ToInt32(searchPreferences[TestQueryBuilder.RELIGION]);
                }
                else
                    searchMember.PreferredReligion = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.RELOCATEFLAG) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.RELOCATEFLAG])))
                {
                    searchMember.PreferredRelocation =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.RELOCATEFLAG]);
                }
                else
                    searchMember.PreferredRelocation = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.SEXUALIDENTITYTYPE) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.SEXUALIDENTITYTYPE])))
                {
                    searchMember.PreferredSexualIdentityType =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.SEXUALIDENTITYTYPE]);
                }
                else
                    searchMember.PreferredSexualIdentityType = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.SMOKINGHABITS) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.SMOKINGHABITS])))
                {
                    searchMember.PreferredSmokingHabit =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.SMOKINGHABITS]);
                }
                else
                    searchMember.PreferredSmokingHabit = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.SYNAGOGUEATTENDANCE) &&
                            !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.SYNAGOGUEATTENDANCE])))
                {
                    searchMember.PreferredSynagogueAttendance =
                        Convert.ToInt32(searchPreferences[TestQueryBuilder.SYNAGOGUEATTENDANCE]);
                }
                else
                    searchMember.PreferredSynagogueAttendance = int.MinValue;

                if ((searchPreferences.ContainsKey(TestQueryBuilder.ZODIAC) &&
                        !string.IsNullOrEmpty(searchPreferences[TestQueryBuilder.ZODIAC])))
                {
                    searchMember.PreferredZodiac = Convert.ToInt32(searchPreferences[TestQueryBuilder.ZODIAC]);
                }
                else
                    searchMember.PreferredZodiac = int.MinValue;
            }
        }


        private static IMatchnetQueryParameter buildQueryParameter(string pParameterName, string pParameterValue)
        {
            QuerySearchParam param = GetQuerySearchParameter(pParameterName);
            if (param != 0)
            {
                return new MatchnetQueryParameter(param, pParameterValue);
            }
            else
            {
                return null;
            }
        }

        private static QuerySearchParam GetQuerySearchParameter(string pParameterName)
        {
            switch (pParameterName)
            {
                case AGEMAX:
                    return QuerySearchParam.AgeMax;
                case AGEMIN:
                    return QuerySearchParam.AgeMin;
                case ALLTEXTFIELDS:
                    return QuerySearchParam.AllTextFields;
                case AREACODE:
                    return QuerySearchParam.AreaCode;
                case BODYTYPE:
                    return QuerySearchParam.BodyType;
                case DISPLAYPHOTOS:
                    return QuerySearchParam.DisplayPhotosToGuests;
                case DOMAINID:
                    return QuerySearchParam.DomainID;
                case DRINKINGHABITS:
                    return QuerySearchParam.DrinkingHabits;
                case EDUCATIONLEVEL:
                    return QuerySearchParam.EducationLevel;
                case ESSAYWORDS:
                    return QuerySearchParam.EssayWords;
                case ETHNICITY:
                    return QuerySearchParam.Ethnicity;
                case GENDERMASK:
                    return QuerySearchParam.GenderMask;
                case GEODISTANCE:
                    return QuerySearchParam.GeoDistance;
                case HASPHOTOFLAG:
                    return QuerySearchParam.HasPhotoFlag;
                case HEIGHTMAX:
                    return QuerySearchParam.HeightMax;
                case HEIGHTMIN:
                    return QuerySearchParam.HeightMin;
                case INSERTDATE:
                    return QuerySearchParam.InsertDate;
                case JDATEETHNICITY:
                    return QuerySearchParam.JDateEthnicity;
                case JDATERELIGION:
                    return QuerySearchParam.JDateReligion;
                case KEEPKOSHER:
                    return QuerySearchParam.KeepKosher;
                case LANGUAGEMASK:
                    return QuerySearchParam.LanguageMask;
                case LASTLOGINDATE:
                    return QuerySearchParam.LastLoginDate;
                case LASTUPDATED:
                    return QuerySearchParam.LastUpdated;
                case LATITUDE:
                    return QuerySearchParam.Latitude;
                case LONGITUDE:
                    return QuerySearchParam.Longitude;
                case MAJORTYPE:
                    return QuerySearchParam.MajorType;
                case MARITALSTATUS:
                    return QuerySearchParam.MaritalStatus;
                case REGIONID:
                    return QuerySearchParam.RegionID;
                case REGIONIDCITY:
                    return QuerySearchParam.RegionIDCity;
                case REGIONIDCOUNTRY:
                    return QuerySearchParam.RegionIDCountry;
                case RELATIONSHIPMASK:
                    return QuerySearchParam.RelationshipMask;
                case RELATIONSHIPSTATUS:
                    return QuerySearchParam.RelationshipStatus;
                case RELIGION:
                    return QuerySearchParam.Religion;
                case SCHOOLID:
                    return QuerySearchParam.SchoolID;
                case SEARCHORDERBY:
                    return QuerySearchParam.Sorting;
                case SEXUALIDENTITYTYPE:
                    return QuerySearchParam.SexualIdentityType;
                case SMOKINGHABITS:
                    return QuerySearchParam.SmokingHabits;
                case SYNAGOGUEATTENDANCE:
                    return QuerySearchParam.SynagogueAttendance;
                case USERNAME:
                    return QuerySearchParam.Username;
                case ZIPCODE:
                    return QuerySearchParam.ZipCode;
                case ZODIAC:
                    return QuerySearchParam.Zodiac;
                case WEIGHTMIN:
                    return QuerySearchParam.WeightMin;
                case WEIGHTMAX:
                    return QuerySearchParam.WeightMax;
                case CHILDRENCOUNT:
                    return QuerySearchParam.ChildrenCount;
                case RELOCATEFLAG:
                    return QuerySearchParam.RelocateFlag;
                case ACTIVITYLEVEL:
                    return QuerySearchParam.ActivityLevel;
                case CUSTODY:
                    return QuerySearchParam.Custody;
                case MORECHILDRENFLAG:
                    return QuerySearchParam.MoreChildrenFlag;
                case COLORCODE:
                    return QuerySearchParam.ColorCode;
                case RADIUS:
                    return QuerySearchParam.Radius;
                case MEMBERID:
                    return QuerySearchParam.MemberID;
                case BLOCKEDMEMBERIDS:
                    return QuerySearchParam.BlockedMemberIDs;
                case SEARCHREDESIGN30:
                    return QuerySearchParam.SearchRedesign30;
        		case REGISTRATIONDATEMIN:
                    return QuerySearchParam.RegistrationDateMin;
                case REGISTRATIONDATEMAX:
                    return QuerySearchParam.RegistrationDateMax;
                case SUBEXPIRATIONDATEMIN:
                    return QuerySearchParam.SubscriptionExpirationDateMin;
                case SUBEXPIRATIONDATEMAX:
                    return QuerySearchParam.SubscriptionExpirationDateMax;
                case PREFERREDAGE:
                    return QuerySearchParam.PreferredAge;
                case PREFERREDACTIVITYLEVEL:
                    return QuerySearchParam.PreferredActivityLevel;
                case PREFERREDAREACODES:
                    return QuerySearchParam.PreferredAreaCodes;
                case PREFERREDBODYTYPE:
                    return QuerySearchParam.PreferredBodyType;
                case PREFERREDCHILDRENCOUNT:
                    return QuerySearchParam.PreferredChildrenCount;
                case PREFERREDCOLORCODE:
                    return QuerySearchParam.PreferredColorCode;
                case PREFERREDCUSTODY:
                    return QuerySearchParam.PreferredCustody;
                case PREFERREDDISTANCE:
                    return QuerySearchParam.PreferredDistance;
                case PREFERREDDRINKINGHABIT:
                    return QuerySearchParam.PreferredDrinkingHabit;
                case PREFERREDEDUCATIONLEVEL:
                    return QuerySearchParam.PreferredEducationLevel;
                case PREFERREDETHNICITY:
                    return QuerySearchParam.PreferredEthnicity;
                case PREFERREDGENDERMASK:
                    return QuerySearchParam.PreferredGenderMask;
                case PREFERREDHASPHOTOFLAG:
                    return QuerySearchParam.PreferredHasPhotoFlag;
                case PREFERREDHEIGHT:
                    return QuerySearchParam.PreferredHeight;
                case PREFERREDJDATEETHNICITY:
                    return QuerySearchParam.PreferredJdateEthnicity;
                case PREFERREDJDATERELIGION:
                    return QuerySearchParam.PreferredJdateReligion;
                case PREFERREDKOSHERSTATUS:
                    return QuerySearchParam.PreferredKosherStatus;
                case PREFERREDLANGUAGEMASK:
                    return QuerySearchParam.PreferredLanguageMask;
                case PREFERREDMAJORTYPE:
                    return QuerySearchParam.PreferredMajorType;
                case PREFERREDMARITALSTATUS:
                    return QuerySearchParam.PreferredMaritalStatus;
                case PREFERREDMORECHILDRENFLAG:
                    return QuerySearchParam.PreferredMoreChildrenFlag;
                case PREFERREDREGIONID:
                    return QuerySearchParam.PreferredRegionId;
                case PREFERREDRELATIONSHIPMASK:
                    return QuerySearchParam.PreferredRelationshipMask;
                case PREFERREDRELATIONSHIPSTATUS:
                    return QuerySearchParam.PreferredRelationshipStatus;
                case PREFERREDRELIGION:
                    return QuerySearchParam.PreferredReligion;
                case PREFERREDRELOCATION:
                    return QuerySearchParam.PreferredRelocation;
                case PREFERREDSEXUALIDENTITYTYPE:
                    return QuerySearchParam.PreferredSexualIdentityType;
                case PREFERREDSMOKINGHABIT:
                    return QuerySearchParam.PreferredSmokingHabit;
                case PREFERREDSYNAGOGUEATTENDANCE:
                    return QuerySearchParam.PreferredSynagogueAttendance;
                case PREFERREDZODIAC:
                    return QuerySearchParam.PreferredZodiac;
                case KEYWORDSEARCH:
                    return QuerySearchParam.KeywordSearch;
                case MAXNUMBEROFMATCHES:
                    return QuerySearchParam.MaxNumberOfMatches;
                case HASACTIVITYLEVEL:
                    return QuerySearchParam.HasActivityLevel;
                case HASBIRTHDATE:
                    return QuerySearchParam.HasBirthDate;
                case HASBODYTYPE:
                    return QuerySearchParam.HasBodyType;
                case HASCHILDRENCOUNT:
                    return QuerySearchParam.HasChildrenCount;
                case HASCUSTODY:
                    return QuerySearchParam.HasCustody;
                case HASDRINKINGHABITS:
                    return QuerySearchParam.HasDrinkingHabits;
                case HASEDUCATIONLEVEL:
                    return QuerySearchParam.HasEducationLevel;
                case HASETHNICITY:
                    return QuerySearchParam.HasEthnicity;
                case HASJDATEETHNICITY:
                    return QuerySearchParam.HasJDateEthnicity;
                case HASHEIGHT:
                    return QuerySearchParam.HasHeight;
                case HASJDATERELIGION:
                    return QuerySearchParam.HasJDateReligion;
                case HASKEEPKOSHER:
                    return QuerySearchParam.HasKeepKosher;
                case HASLANGUAGEMASK:
                    return QuerySearchParam.HasLanguageMask;
                case HASMARITALSTATUS:
                    return QuerySearchParam.HasMaritalStatus;
                case HASMORECHILDRENFLAG:
                    return QuerySearchParam.HasMoreChildrenFlag;
                case HASRELIGION:
                    return QuerySearchParam.HasReligion;
                case HASRELOCATEFLAG:
                    return QuerySearchParam.HasRelocateFlag;
                case HASSMOKINGHABITS:
                    return QuerySearchParam.HasSmokingHabits;
                case HASSYNAGOGUEATTENDANCE:
                    return QuerySearchParam.HasSynagogueAttendance;
                case HASMEMBERID:
                    return QuerySearchParam.HasMemberID;
                default:
                    return 0;
            }
        }

        private static bool IsKeywordMatchesSearchPrefEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = Boolean.FalseString.ToLower();
            try
            {
                isEnabled = SettingsService.GetSettingFromSingleton("ENABLE_KEYWORD_MATCHES_SEARCHPREF", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = Boolean.FalseString.ToLower();
            }

            if (isEnabled.ToLower().Trim() == Boolean.TrueString.ToLower())
                return true;
            else
                return false;
        }
    }
}
