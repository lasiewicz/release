﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.Spatial;
using Lucene.Net.Spatial.BBox;
using Lucene.Net.Util;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Spark.Logging;
using Spark.SearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.SearchEngine.BusinessLogic.Documents
{
    [Serializable]
    public class SearchDocument
    {
        private Document _document;
        private DocumentMetadata _documentMetadata;
        private Dictionary<string, Field> _documentFields = new Dictionary<string, Field>();
        private Dictionary<string, NumericField> _documentNumericFields = new Dictionary<string, NumericField>();
        private Dictionary<string, Dictionary<object, NumericField>> _documentMaskNumericFields = new Dictionary<string, Dictionary<object, NumericField>>();

        public int ID { get; set; }
        private static object _lockObject = new object();

        private RegionAreaCodeDictionary _regionAreaCodeDictionary;
        private Dictionary<int, RegionLanguage> _regionLanguages;
        private SpatialContext _context;
        private SpatialStrategy _strategy;
        private Dictionary<string, AbstractField[]> _spatialShapeFields = null;

        public string CommunityName { get; set; }

        public Dictionary<int, RegionLanguage> RegionLanguages
        {
            get { return _regionLanguages; }
            set { _regionLanguages = value; }
        }

        public RegionAreaCodeDictionary AreaCodeDictionary
        {
            get { return _regionAreaCodeDictionary; }
            set { _regionAreaCodeDictionary = value; }
        }

        private void AddRegionLanguage(int regionId, RegionLanguage regionLanguage)
        {
            lock (_lockObject)
            {
                if (null != RegionLanguages && !RegionLanguages.ContainsKey(regionId))
                {
                    RegionLanguages.Add(regionId, regionLanguage);
                }
            }
        }

        private bool HasRegionLanguage(int regionId)
        {
            bool b = false;
            lock (_lockObject)
            {
                if (null != RegionLanguages)
                {
                    b = RegionLanguages.ContainsKey(regionId);
                }
            }
            return b;
        }

        public SpatialContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public SpatialStrategy Strategy
        {
            get { return _strategy; }
            set { _strategy = value; }
        }

        public Dictionary<string, AbstractField[]> SpatialShapeFields
        {
            get { return _spatialShapeFields; }
            set { _spatialShapeFields = value; }
        }

        private void AddSpatialShapeFields(string latLongKey, AbstractField [] fields)
        {
            lock (_lockObject)
            {
                if (null != SpatialShapeFields && !SpatialShapeFields.ContainsKey(latLongKey))
                {
                    SpatialShapeFields.Add(latLongKey, fields);
                }
            }
        }

        private bool HasSpatialShapeFields(string latLongKey)
        {
            bool b = false;
            lock (_lockObject)
            {
                if (null != SpatialShapeFields)
                {
                    b = SpatialShapeFields.ContainsKey(latLongKey);
                }
            }
            return b;
        }

        public SearchDocument(DocumentMetadata metadata)
        {
            _document = new Document();
            _documentMetadata = metadata;
        }

        public Document LuceneDocument
        {
            get { return _document; }
            set { _document = value; }
        }

        #region Lucence Indexing Optimization

        private Field GetField(string fieldName, string value, Field.Index fieldIndex, Field.Store fieldStore, float boost, bool useTermVectors)
        {
            Field f = null;
            if (_documentFields.ContainsKey(fieldName))
            {
                f = _documentFields[fieldName];
                if (null != f && string.IsNullOrEmpty(f.StringValue))
                {
                    f.SetValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                if (useTermVectors)
                {
                    f = new Field(fieldName, value, fieldStore, fieldIndex, Field.TermVector.YES);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                else
                {
                    f = new Field(fieldName, value, fieldStore, fieldIndex);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                if (!_documentFields.ContainsKey(fieldName)) _documentFields.Add(fieldName, f);
            }
            return f;
        }

        private Field GetField(string fieldName, TokenStream tokenStream, float boost, bool useTermVectors)
        {
            Field f = null;
            if (_documentFields.ContainsKey(fieldName))
            {
                f = _documentFields[fieldName];
                if (null != f && null == f.TokenStreamValue)
                {                    
                    f.SetTokenStream(tokenStream);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                if (useTermVectors)
                {
                    f = new Field(fieldName, tokenStream, Field.TermVector.YES);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                else
                {
                    f = new Field(fieldName, tokenStream);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                if (!_documentFields.ContainsKey(fieldName)) _documentFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, int value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && int.MinValue.Equals((int) f.NumericValue))
                {
                    f.SetIntValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetIntValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && double.MinValue.Equals((double) f.NumericValue))
                {
                    f.SetDoubleValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, int precisionLevel, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && double.MinValue.Equals((double)f.NumericValue))
                {
                    f.SetDoubleValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, precisionLevel, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, long value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && long.MinValue.Equals((long) f.NumericValue))
                {
                    f.SetLongValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetLongValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, int value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && int.MinValue.Equals((int) f.NumericValue))
                    {
                        f.SetIntValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetIntValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName))
                    _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && double.MinValue.Equals((double) f.NumericValue))
                    {
                        f.SetDoubleValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName))
                    _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, long value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && long.MinValue.Equals((long) f.NumericValue))
                    {
                        f.SetLongValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetLongValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName))
                    _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        public void ClearAllFieldValues()
        {
            foreach (Field f in _documentFields.Values)
            {
                f.SetValue(string.Empty);
                if (f.Boost > 0)
                {
                    f.Boost = 0f;
                }
                LuceneDocument.RemoveFields(f.Name);
            }

            foreach (NumericField nf in _documentNumericFields.Values)
            {
                if (nf.NumericValue is int) nf.SetIntValue(int.MinValue);
                if (nf.NumericValue is double) nf.SetDoubleValue(double.MinValue);
                if (nf.NumericValue is long) nf.SetLongValue(long.MinValue);
                if (nf.Boost > 0)
                {
                    nf.Boost = 0f;
                }
                LuceneDocument.RemoveFields(nf.Name);
            }

            foreach (Dictionary<object, NumericField> dnf in _documentMaskNumericFields.Values)
            {
                foreach (NumericField nf in dnf.Values)
                {
                    if (nf.NumericValue is int) nf.SetIntValue(int.MinValue);
                    if (nf.NumericValue is double) nf.SetDoubleValue(double.MinValue);
                    if (nf.NumericValue is long) nf.SetLongValue(long.MinValue);
                    if (nf.Boost > 0)
                    {
                        nf.Boost = 0f;
                    }
                    LuceneDocument.RemoveFields(nf.Name);
                }
            }
        }
        #endregion

        public void Populate(SearchKeyword keyword, DocumentMetadata docMetadata)
        {
            try
            {
                Add(docMetadata.GetField("MemberID"), keyword.MemberID.ToString());
                Add(docMetadata.GetField("CommunityID"), keyword.CommunityID.ToString());
                Add(docMetadata.GetField("GenderMask"), keyword.GenderMask.ToString());
                Add(docMetadata.GetField("LastActiveDate"), keyword.LastActiveDate.ToString());
                Add(docMetadata.GetField("UpdateDate"), keyword.UpdateDate.ToString());
                Add(docMetadata.GetField("AboutMe"), keyword.AboutMe);
                Add(docMetadata.GetField("Depth1RegionID"), keyword.Depth1RegionId.ToString());
                Add(docMetadata.GetField("Depth2RegionID"), keyword.Depth2RegionId.ToString());
                Add(docMetadata.GetField("Depth3RegionID"), keyword.Depth3RegionId.ToString());
                Add(docMetadata.GetField("Depth4RegionID"), keyword.Depth4RegionId.ToString());
                // Converting radian lat long to degrees because it is what is expected by Spatial.net
                AddLocation(DistanceUtils.ToDegrees(keyword.Latitude), DistanceUtils.ToDegrees(keyword.Longitude));
                int regionID = (keyword.Depth4RegionId > 0) ? keyword.Depth4RegionId : keyword.Depth3RegionId;
                AddAreaCode(docMetadata.GetField("AreaCode"), regionID);
                Add(docMetadata.GetField("perfectmatchessay"), keyword.PerfectMatchEssay);
                Add(docMetadata.GetField("perfectfirstdateessay"), keyword.PerfectFirstDateEssay);
                Add(docMetadata.GetField("idealrelationshipessay"), keyword.IdealRelationshipEssay);
                Add(docMetadata.GetField("learnfromthepastessay"), keyword.LearnFromThePastEssay);
                Add(docMetadata.GetField("lifeandabmitionessay"), keyword.LifeAndAbmitionEssay);
                Add(docMetadata.GetField("historyofmylifeessay"), keyword.HistoryOfMyLifeEssay);
                Add(docMetadata.GetField("onourfirstdateremindmetoessay"), keyword.OnOurFirstDateRemindMeToEssay);
                Add(docMetadata.GetField("thingscantlivewithoutessay"), keyword.ThingsCantLiveWithoutEssay);
                Add(docMetadata.GetField("favoritebooksmoviesetcessay"), keyword.FavoriteBooksMoviesEtcEssay);
                Add(docMetadata.GetField("coolestplacesvisitedessay"), keyword.CoolestPlacesVisitedEssay);
                Add(docMetadata.GetField("forfuniliketoessay"), keyword.ForFunILikeToEssay);
                Add(docMetadata.GetField("onfrisatitypicallyessay"), keyword.OnFriSatITypicallyEssay);
                Add(docMetadata.GetField("messagemeifyouessay"), keyword.MessageMeIfYouEssay);
                Add(docMetadata.GetField("cuisine"), keyword.Cuisine);
                Add(docMetadata.GetField("entertainmentlocation"), keyword.EntertainmentLocation);
                Add(docMetadata.GetField("leisureactivity"), keyword.LeisureActivity);
                Add(docMetadata.GetField("music"), keyword.Music);
                Add(docMetadata.GetField("personalitytrait"), keyword.PersonalityTrait);
                Add(docMetadata.GetField("pets"), keyword.Pets);
                Add(docMetadata.GetField("physicalactivity"), keyword.PhysicalActivity);
                Add(docMetadata.GetField("reading"), keyword.Reading);
                Add(docMetadata.GetField("essayfirstdate"), keyword.EssayFirstDate);
                Add(docMetadata.GetField("essaygettoknowme"), keyword.EssayGetToKnowMe);
                Add(docMetadata.GetField("essaypastrelationship"), keyword.EssayPastRelationship);
                Add(docMetadata.GetField("essayimportantthings"), keyword.EssayImportantThings);
                Add(docMetadata.GetField("essayperfectday"), keyword.EssayPerfectDay);
                Add(docMetadata.GetField("essaymorethingstoadd"), keyword.EssayMoreThingsToAdd);
                Add(docMetadata.GetField("favoritebands"), keyword.FavoriteBands);
                Add(docMetadata.GetField("favoritemovies"), keyword.FavoriteMovies);
                Add(docMetadata.GetField("favoritetv"), keyword.FavoriteTv);
                Add(docMetadata.GetField("favoriterestaurant"), keyword.FavoriteRestaurant);
                Add(docMetadata.GetField("schoolsattended"), keyword.SchoolsAttended);
                Add(docMetadata.GetField("mygreattripidea"), keyword.MyGreatTripIdea);
                Add(docMetadata.GetField("moviegenre"), keyword.MovieGenres);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", "Populate(SearchKeyword)", ex, CommunityName);
                throw ex;
            }
        }

        public void Populate(SearchMember member)
        {
            Populate(member, _documentMetadata);
        }

        public void Populate(SearchMember member, DocumentMetadata docMetadata)
        {
            try
            {
                Add(docMetadata.GetField("MemberID"), member.MemberID.ToString());
                Add(docMetadata.GetField("CommunityID"), member.CommunityID.ToString());
                Add(docMetadata.GetField("GenderMask"), member.GenderMask.ToString());
                Add(docMetadata.GetField("BodyType"), member.BodyType.ToString());
                Add(docMetadata.GetField("Birthdate"), member.Birthdate.ToString());
                Add(docMetadata.GetField("CommunityInsertDate"), member.CommunityInsertDate.ToString());
                Add(docMetadata.GetField("LastActiveDate"), member.LastActiveDate.ToString());
                Add(docMetadata.GetField("HasPhotoFlag"), member.HasPhotoFlag.ToString());
                Add(docMetadata.GetField("EducationLevel"), member.EducationLevel.ToString());
                Add(docMetadata.GetField("Religion"), member.Religion.ToString());
                Add(docMetadata.GetField("LanguageMask"), member.LanguageMask.ToString());
                Add(docMetadata.GetField("LanguageMaskMask"), member.LanguageMask.ToString()); //this stores language mask as a single int value
                Add(docMetadata.GetField("Ethnicity"), member.Ethnicity.ToString());
                Add(docMetadata.GetField("SmokingHabits"), member.SmokingHabits.ToString());
                Add(docMetadata.GetField("DrinkingHabits"), member.DrinkingHabits.ToString());
                Add(docMetadata.GetField("Height"), member.Height.ToString());
                Add(docMetadata.GetField("MaritalStatus"), member.MaritalStatus.ToString());
                Add(docMetadata.GetField("JDateReligion"), member.JDateReligion.ToString());
                Add(docMetadata.GetField("JDateEthnicity"), member.JDateEthnicity.ToString());
                Add(docMetadata.GetField("SynagogueAttendance"), member.SynagogueAttendance.ToString());
                Add(docMetadata.GetField("KeepKosher"), member.KeepKosher.ToString());
                Add(docMetadata.GetField("SexualIdentityType"), member.SexualIdentityType.ToString());
                Add(docMetadata.GetField("RelationshipMask"), member.RelationshipMask.ToString());
                Add(docMetadata.GetField("RelationshipStatus"), member.RelationshipStatus.ToString());
                Add(docMetadata.GetField("Zodiac"), member.Zodiac.ToString());
                Add(docMetadata.GetField("MajorType"), member.MajorType.ToString());
                Add(docMetadata.GetField("Depth1RegionID"), member.Depth1RegionID.ToString());
                Add(docMetadata.GetField("Depth2RegionID"), member.Depth2RegionID.ToString());
                Add(docMetadata.GetField("Depth3RegionID"), member.Depth3RegionID.ToString());
                Add(docMetadata.GetField("Depth4RegionID"), member.Depth4RegionID.ToString());
                Add(docMetadata.GetField("SchoolID"), member.SchoolID.ToString());

                Add(docMetadata.GetField("EmailCount"), member.EmailCount.ToString());
                Add(docMetadata.GetField("UpdateDate"), member.UpdateDate.ToString());
                Add(docMetadata.GetField("Weight"), member.Weight.ToString());
                Add(docMetadata.GetField("RelocateFlag"), member.RelocateFlag.ToString());
                Add(docMetadata.GetField("CommunityID"), member.CommunityID.ToString());
                Add(docMetadata.GetField("ChildrenCount"), member.ChildrenCount.ToString());
                Add(docMetadata.GetField("SubscriptionExpirationDate"), member.SubscriptionExpirationDate.ToString());
                Add(docMetadata.GetField("ActivityLevel"), member.ActivityLevel.ToString());
                Add(docMetadata.GetField("Custody"), member.Custody.ToString());
                Add(docMetadata.GetField("MoreChildrenFlag"), member.MoreChildrenFlag.ToString());
                Add(docMetadata.GetField("colorcode"), member.colorcode.ToString());
                Add(docMetadata.GetField("Online"), member.Online.ToString());
                Add(docMetadata.GetField("RamahAlum"), member.RahmahAlum.ToString());
                // Converting radian lat long to degrees because it is what is expected by Spatial.net
                AddLocation(DistanceUtils.ToDegrees(member.Latitude), DistanceUtils.ToDegrees(member.Longitude));
                int regionID = (member.Depth4RegionID > 0) ? member.Depth4RegionID : member.Depth3RegionID;
                AddAreaCode(docMetadata.GetField("AreaCode"), regionID);

                //search preferences fields
                //Note: Preferred fields are masks and are added as multiple fields for each value it contains, so also add an additional field with the same name with "mask"
                //appended to store the mask as a single value
                Add(docMetadata.GetField("preferredrelocation"), member.PreferredRelocation.ToString());
                Add(docMetadata.GetField("preferredrelocationmask"), member.PreferredRelocation.ToString());
                Add(docMetadata.GetField("preferredminheight"), member.PreferredMinHeight.ToString());
                Add(docMetadata.GetField("preferredmaxheight"), member.PreferredMaxHeight.ToString());
                Add(docMetadata.GetField("preferredminage"), member.PreferredMinAge.ToString());
                Add(docMetadata.GetField("preferredmaxage"), member.PreferredMaxAge.ToString());
                Add(docMetadata.GetField("preferredsynagogueattendance"), member.PreferredSynagogueAttendance.ToString());
                Add(docMetadata.GetField("preferredsynagogueattendancemask"), member.PreferredSynagogueAttendance.ToString());
                Add(docMetadata.GetField("preferredjdatereligion"), member.PreferredJdateReligion.ToString());
                Add(docMetadata.GetField("preferredjdatereligionmask"), member.PreferredJdateReligion.ToString());
                Add(docMetadata.GetField("preferredsmokinghabit"), member.PreferredSmokingHabit.ToString());
                Add(docMetadata.GetField("preferredsmokinghabitmask"), member.PreferredSmokingHabit.ToString());
                Add(docMetadata.GetField("preferreddrinkinghabit"), member.PreferredDrinkingHabit.ToString());
                Add(docMetadata.GetField("preferreddrinkinghabitmask"), member.PreferredDrinkingHabit.ToString());
                Add(docMetadata.GetField("preferredjdateethnicity"), member.PreferredJdateEthnicity.ToString());
                Add(docMetadata.GetField("preferredjdateethnicitymask"), member.PreferredJdateEthnicity.ToString());
                Add(docMetadata.GetField("preferredactivitylevel"), member.PreferredActivityLevel.ToString());
                Add(docMetadata.GetField("preferredactivitylevelmask"), member.PreferredActivityLevel.ToString());
                Add(docMetadata.GetField("preferredkosherstatus"), member.PreferredKosherStatus.ToString());
                Add(docMetadata.GetField("preferredkosherstatusmask"), member.PreferredKosherStatus.ToString());
                Add(docMetadata.GetField("preferredmaritalstatus"), member.PreferredMaritalStatus.ToString());
                Add(docMetadata.GetField("preferredmaritalstatusmask"), member.PreferredMaritalStatus.ToString());
                Add(docMetadata.GetField("preferredlanguagemask"), member.PreferredLanguageMask.ToString());
                Add(docMetadata.GetField("preferredlanguagemaskmask"), member.PreferredLanguageMask.ToString());
                Add(docMetadata.GetField("preferreddistance"), member.PreferredDistance.ToString());
                Add(docMetadata.GetField("preferredregionid"), member.PreferredRegionId.ToString());
                Add(docMetadata.GetField("preferredgendermask"), member.PreferredGenderMask.ToString());
                if (!string.IsNullOrEmpty(member.PreferredAreaCodes))
                {
                    string[] pAreaCodes = member.PreferredAreaCodes.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string pAreaCode in pAreaCodes)
                    {
                        int areaCodeInt = Convert.ToInt32(pAreaCode);
                        SearchField fld = docMetadata.GetField("preferredareacodes");
                        _document.Add(GetMaskNumericField(fld.Name, areaCodeInt, true, fld.getLuceneStore(), fld.Boost));
                    }
                }
                Add(docMetadata.GetField("preferredhasphotoflag"), member.PreferredHasPhotoFlag.ToString());
                Add(docMetadata.GetField("preferrededucationlevel"), member.PreferredEducationLevel.ToString());
                Add(docMetadata.GetField("preferrededucationlevelmask"), member.PreferredEducationLevel.ToString());
                Add(docMetadata.GetField("preferredreligion"), member.PreferredReligion.ToString());
                Add(docMetadata.GetField("preferredreligionmask"), member.PreferredReligion.ToString());
                Add(docMetadata.GetField("preferredethnicity"), member.PreferredEthnicity.ToString());
                Add(docMetadata.GetField("preferredethnicitymask"), member.PreferredEthnicity.ToString());
                Add(docMetadata.GetField("preferredsexualidentitytype"), member.PreferredSexualIdentityType.ToString());
                Add(docMetadata.GetField("preferredsexualidentitytypemask"), member.PreferredSexualIdentityType.ToString());
                Add(docMetadata.GetField("preferredrelationshipmask"), member.PreferredRelationshipMask.ToString());
                Add(docMetadata.GetField("preferredrelationshipmaskmask"), member.PreferredRelationshipMask.ToString());
                Add(docMetadata.GetField("preferredrelationshipstatus"), member.PreferredRelationshipstatus.ToString());
                Add(docMetadata.GetField("preferredrelationshipstatusmask"), member.PreferredRelationshipstatus.ToString());
                Add(docMetadata.GetField("preferredbodytype"), member.PreferredBodyType.ToString());
                Add(docMetadata.GetField("preferredbodytypemask"), member.PreferredBodyType.ToString());
                Add(docMetadata.GetField("preferredzodiac"), member.PreferredZodiac.ToString());
                Add(docMetadata.GetField("preferredzodiacmask"), member.PreferredZodiac.ToString());
                Add(docMetadata.GetField("preferredmajortype"), member.PreferredMajorType.ToString());
                Add(docMetadata.GetField("preferredmajortypemask"), member.PreferredMajorType.ToString());
                Add(docMetadata.GetField("preferredmorechildrenflag"), member.PreferredMoreChildrenFlag.ToString());
                Add(docMetadata.GetField("preferredmorechildrenflagmask"), member.PreferredMoreChildrenFlag.ToString());
                Add(docMetadata.GetField("preferredchildrencount"), member.PreferredChildrenCount.ToString());
                Add(docMetadata.GetField("preferredchildrencountmask"), member.PreferredChildrenCount.ToString());
                Add(docMetadata.GetField("preferredcustody"), member.PreferredCustody.ToString());
                Add(docMetadata.GetField("preferredcustodymask"), member.PreferredCustody.ToString());
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, CommunityName.ToUpper()), "Document", "Populate(SearchMember)", ex, null);
                throw;
            }
        }

        public void Populate(SearchAdmin searchAdmin, DocumentMetadata docMetaData)
        {
            try
            {
                Add(docMetaData.GetField("memberid"), searchAdmin.MemberId.ToString());
                Add(docMetaData.GetField("communityid"), searchAdmin.CommunityId.ToString());

                Add(docMetaData.GetField("lastactivedate"), searchAdmin.LastActiveDate.ToString());
                Add(docMetaData.GetField("updatedate"), searchAdmin.UpdateDate.ToString());
                Add(docMetaData.GetField("communityinsertdate"), searchAdmin.CommunityInsertDate.ToString());

                Add(docMetaData.GetField("depth1regionid"), searchAdmin.Depth1RegionId.ToString());
                Add(docMetaData.GetField("depth2regionid"), searchAdmin.Depth2RegionId.ToString());
                Add(docMetaData.GetField("depth3regionid"), searchAdmin.Depth3RegionId.ToString());
                Add(docMetaData.GetField("depth4regionid"), searchAdmin.Depth4RegionId.ToString());
                // Converting radian lat long to degrees because it is what is expected by Spatial.net
                AddLocation(DistanceUtils.ToDegrees(searchAdmin.Latitude), DistanceUtils.ToDegrees(searchAdmin.Longitude));

                Add(docMetaData.GetField("ipaddress"), (searchAdmin.IPAddress != Matchnet.Constants.NULL_INT) ? new IPAddress(BitConverter.GetBytes(searchAdmin.IPAddress)).ToString() : "");
                Add(docMetaData.GetField("username"), searchAdmin.Username);
                Add(docMetaData.GetField("firstname"), searchAdmin.FirstName);
                Add(docMetaData.GetField("lastname"), searchAdmin.LastName);
                Add(docMetaData.GetField("emailaddress"), searchAdmin.EmailAddress);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", "Populate(SearchAdmin)", ex, CommunityName);
                throw;
            }
        }

        public void Add(SearchField fld, String value)
        {
            if (fld != null)
            {
                if (fld.IsDocumentID)
                {

                    int id = Conversion.CInt(value);
                    if (id > 0)
                    {
                        _document.Add(GetField("id", value, Field.Index.NOT_ANALYZED, Field.Store.YES, fld.Boost, false));
                        ID = id;
                        return;
                    }
                }
                switch (fld.FieldType)
                {
                    case DataType.LONG:
                    case DataType.INT:
                    case DataType.DOUBLE:
                        addNumeric(fld, value);
                        break;
                    case DataType.MASK:
                        addMask(fld, value);
                        break;
                    case DataType.DATE:
                        addDate(fld, value);
                        break;
                    case DataType.TXT:
                        addText(fld, value);
                        break;
                }
            }
            else
            {
                RollingFileLogger.Instance.LogWarningMessage(ServiceConstants.SERVICE_INDEXER_CONST, "Document", "SearchField is null, document xml is likely missing a new field", null);
            }
        }

        public void AddLocation(double lat, double lng)
        {
            try
            {
                string latLongKey = lat.ToString() + "," + lng.ToString();
                AbstractField[] shapeFields = null;
                if (HasSpatialShapeFields(latLongKey))
                {
                    shapeFields = SpatialShapeFields[latLongKey];
                }
                else
                {
                    Shape shape = null;
                    if (this.Strategy is BBoxStrategy)
                    {
                        shape = Context.MakeRectangle(DistanceUtils.NormLonDEG(lng), DistanceUtils.NormLonDEG(lng), DistanceUtils.NormLatDEG(lat), DistanceUtils.NormLatDEG(lat));
                    }
                    else
                    {
                        shape = Context.MakePoint(DistanceUtils.NormLonDEG(lng), DistanceUtils.NormLatDEG(lat));
                    }
                    shapeFields = Strategy.CreateIndexableFields(shape);
                    AddSpatialShapeFields(latLongKey, shapeFields);
                }

                //Potentially more than one shape in this field is supported by some
                // strategies; see the javadocs of the SpatialStrategy impl to see.
                foreach (AbstractField f in shapeFields)
                {
                    //For PointVectorStrategy, AbstractField is always a NumericField
                    if (f is NumericField)
                    {
                        NumericField nf = (NumericField) f;
                        double val = Double.Parse(nf.StringValue);
                        NumericField field = GetNumericField(f.Name, Utils.PrecisionLevel, val, f.IsIndexed,
                                                             Field.Store.YES, f.Boost);
                        field.OmitNorms = true;
                        field.OmitTermFreqAndPositions = true;
                        _document.Add(field);
                    }
                }
                //add lat long values to index too, VERY IMPORTANT - DO NOT ALTER THIS CODE
                _document.Add(GetField("latitude", NumericUtils.DoubleToPrefixCoded(lat), Field.Index.NOT_ANALYZED, Field.Store.YES, 0f, false));
                _document.Add(GetField("longitude", NumericUtils.DoubleToPrefixCoded(lng), Field.Index.NOT_ANALYZED, Field.Store.YES, 0f, false));
            }
            catch (Exception e)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", string.Format("AddLocation({0},{1})", lat.ToString(), lng.ToString()), e, null);
                throw;
            }
        }


        public void AddAreaCode (SearchField fld, Int32 regionID)
        {
            string areaCode = string.Empty;

            RegionLanguage regionLanguage = null;
            if (!HasRegionLanguage(regionID))
            {
                regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, (Int32) Matchnet.Language.English);
                AddRegionLanguage(regionID, regionLanguage);
            }
            else
            {
                regionLanguage = RegionLanguages[regionID];
            }

            Int32 cityRegionID = regionLanguage.CityRegionID;

            if (null == AreaCodeDictionary)
            {
                AreaCodeDictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();
            }

            if (AreaCodeDictionary.Contains(cityRegionID))
            {
                ArrayList areaCodes = AreaCodeDictionary[cityRegionID];

                if (areaCodes.Count == 1)
                {
                    areaCode = areaCodes[0].ToString();
                }
                else
                {
                    //If more than one, use lowest area code in region since user can only have one
                    Int32 maxAreaCode = 0;

                    foreach (Int32 areaCodeNum in areaCodes)
                    {
                        if (areaCodeNum > maxAreaCode)
                        {
                            maxAreaCode = areaCodeNum;
                        }
                    }
                    areaCode = maxAreaCode.ToString();
                }
            }

            if (!string.IsNullOrEmpty(areaCode))
            {
                int areaCodeInt = Convert.ToInt32(areaCode);
                _document.Add(GetNumericField(fld.Name, areaCodeInt, true, fld.getLuceneStore(), fld.Boost));
            }
            else
            {
                _document.Add(GetNumericField(fld.Name, Matchnet.Constants.NULL_INT, true, fld.getLuceneStore(), fld.Boost));
            }
        }

        private void addNumeric (SearchField fld, String value)
        {
            try
            {
                switch (fld.FieldType)
                {
                    case DataType.INT:
                        {
                            int val = Conversion.CInt(value);
                            _document.Add(GetNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.DOUBLE:
                        {
                            double val = Conversion.CDouble(value);
                            if (val > 0)
                                _document.Add(GetNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.LONG:
                        {
                            long val = Conversion.CInt(value);
                            _document.Add(GetNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.MASK:
                        {
                            long val = Conversion.CInt(value);
                            _document.Add(GetMaskNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void addText (SearchField fld, string value)
        {
            if (null != value)
            {
                Field field = GetField(fld.Name, value, fld.getLuceneIndex(), fld.getLuceneStore(), fld.Boost, true);
                _document.Add(field);
            }
        }

        private void addDate (SearchField fld, String value)
        {
            addDate(fld, Conversion.CDateTime(value));
        }

        private void addDate(SearchField fld, DateTime value)
        {
            _document.Add(GetNumericField(fld.Name, value.Ticks, true, fld.getLuceneStore(), fld.Boost));
        }

        private void addMask(SearchField fld, String value)
        {
            Int32 mask = Conversion.CInt(value);
            long step = 0;
            for (long i = 0; (long) Math.Pow(2, i) <= mask; i++)
            {
                step = (long) Math.Pow(2, i);
                if ((step & mask) == step)
                {
                    addNumeric(fld, step.ToString());
                }
            }
        }
    }
}
