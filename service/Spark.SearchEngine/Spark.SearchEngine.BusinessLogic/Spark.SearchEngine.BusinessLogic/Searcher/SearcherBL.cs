﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Lucene.Net.Documents;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Util;
using Lucene.Net.Store;
using Matchnet.Exceptions;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Spark.Logging;
using Spark.SearchEngine.BusinessLogic.Documents;
using Spark.SearchEngine.ValueObjects;
using Lucene.Net.Index;
using System.Data;
using Matchnet.Data;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;
using Newtonsoft.Json;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class SearcherBL
    {
        #region debug code
#if DEBUG
        private bool _Show_Distances = false;
        public bool NUNIT_Show_Distances
        {
            get { return _Show_Distances; }
            set { _Show_Distances = value; }
        }

        private bool _Use_NUnit = false;
        public bool USE_NUNIT
        {
            get { return _Use_NUnit; }
            set { _Use_NUnit = value; }
        }

        private bool _NUnit_UseNRT = false;
        public bool NUNIT_UseNRT
        {
            get { return _NUnit_UseNRT; }
            set { _NUnit_UseNRT = value; }
        }

        private bool _Nunit_Print = false;
        public bool NUNIT_Print
        {
            get { return _Nunit_Print; }
            set { _Nunit_Print = value; }
        }

        private StringBuilder _Nunit_StringBuilder = null;
        public StringBuilder NUNIT_StringBuilder
        {
            get {
                if (null == _Nunit_StringBuilder) _Nunit_StringBuilder = new StringBuilder();
                return _Nunit_StringBuilder; 
            }
            set { _Nunit_StringBuilder=value; }
        }

        public Dictionary<int, string> NUNIT_IndexPaths { get; set; }
        public Dictionary<int, string> NUNIT_KeywordIndexPaths { get; set; }
        public string[] NUNIT_Community_List { get; set; }
        public TopDocs TopDocs_forNUNIT { get; set; }
        public QueryBuilder QueryBuilder_forNUNIT { get; set; }
        public QueryBuilder KeywordQueryBuilder_forNUNIT { get; set; }

        public static SearcherBL CreateSearcherBL()
        {
            return new SearcherBL();
        }

        private void WriteOutputToBuffer(QueryBuilder queryBuilder, int sortType, Document doc, IMatchnetResultItem item)
        {
            if (NUNIT_Print)
            {
                string sortAttr = string.Empty;
                string sortVal = string.Empty;
                string lastLoginDate = string.Empty;
                try
                {
                    lastLoginDate = new DateTime(Convert.ToInt64(doc.GetField("lastactivedate").StringValue)).ToString();
                    switch (sortType)
                    {
                        case 1:
                            sortAttr = "communityinsertdate";
                            sortVal = new DateTime(Convert.ToInt64(doc.GetField(sortAttr).StringValue)).ToString();
                            break;
                        case 4:
                            sortAttr = "emailcount";
                            sortVal = doc.GetField(sortAttr).StringValue;
                            break;
                        case 3:
                        case 5:
                            sortAttr = "proximity";
                            double rsLat = Lucene.Net.Util.NumericUtils.PrefixCodedToDouble(doc.Get("latitude"));
                            double rsLng = Lucene.Net.Util.NumericUtils.PrefixCodedToDouble(doc.Get("longitude"));
                            Point docPoint = Utils.SpatialContext.MakePoint(rsLng, rsLat);
                            double docDistDEG = Utils.SpatialContext.GetDistCalc().Distance(queryBuilder.SpatialShape.GetCenter(), docPoint);
                            double distanceMi = DistanceUtils.Degrees2Dist(docDistDEG, DistanceUtils.EARTH_MEAN_RADIUS_MI);
                            sortVal = distanceMi.ToString();
                            break;
                        default:
                            sortAttr = "lastactivedate";
                            sortVal = lastLoginDate;
                            break;
                    }
                }
                catch (Exception e) { }
                string areaCode = (item is DetailedQueryResult) ? ((DetailedQueryResult)item).Tags["areacode"].ToString() : string.Empty;
                DateTime birthDate = (item is DetailedQueryResult) ? (DateTime)((DetailedQueryResult)item).Tags["birthdate"]: DateTime.Now;
                int age = Utils.CalculateAge(birthDate);
                NUNIT_StringBuilder.Append(string.Format("\nMemberId: {0}, Age: {1}, AreaCode: {2}, LastLoginDate: {3}, SortField: {4}, SortValue: {5}\n", item.MemberID, age, areaCode ,lastLoginDate, sortAttr, sortVal));
            }
        }

        private int GetSortType(IMatchnetQuery query)
        {
            int sortType = 1;
            if (NUNIT_Print)
            {
                for (int i = 0; i < query.Count; i++)
                {
                    if (query[i].Parameter == QuerySearchParam.Sorting)
                    {
                        sortType = Convert.ToInt32(query[i].Value);
                        break;
                    }
                }
            }
            return sortType;
        }

#endif
        #endregion
        
        public static readonly SearcherBL Instance = new SearcherBL();
        private string _communityName;
        private Dictionary<int, Searcher> searchers = new Dictionary<int, Searcher>();
        private Dictionary<int, Searcher> keywordSearchers = new Dictionary<int, Searcher>();
        private Thread _updateThread = null;
        private Thread _gcThread = null;
        private string _serviceConstant = null;
        private System.Diagnostics.PerformanceCounter availableRamCounter;
        private bool _isRunning = false;

        public delegate void SearcherAddSearchEventHandler();
        public event SearcherAddSearchEventHandler SearcherAddSearch;

        public delegate void SearcherRemoveSearchEventHandler();
        public event SearcherRemoveSearchEventHandler SearcherRemoveSearch;

        public delegate void SearcherAverageSearchEventHandler(long searchTime);
        public event SearcherAverageSearchEventHandler SearcherAverageSearch;

        private SearcherBL() {}

        public void Start(string communityName)
        {
            this._isRunning = true;
            this._communityName = communityName;
            _serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, _communityName.ToUpper());

            //NOTE: Keep one instance of indexReader and searcher open until service is stopped.  more efficient.
            string[] communityList = Utils.GetSetting("SEARCHER_COMMUNITY_LIST", "").Split(new char[] { ';' });
#if DEBUG
            if (USE_NUNIT)
            {
                if (null != NUNIT_Community_List)
                {
                    communityList = NUNIT_Community_List;
                }
            }
#endif
            int thisSearcherCommunityId = ServiceConstants.GetCommunityId(_communityName);
            bool useAllIndexes = Utils.GetSetting("SEARCHER_USE_ALL_INDEXES", thisSearcherCommunityId, false);
            bool useSharding = Utils.GetSetting("SEARCHER_PER_COMMUNITY", false);
            bool useNRT = IsUsingNRT(thisSearcherCommunityId);

            foreach (string cid in communityList)
            {
                int communityid = Convert.ToInt32(cid);

                bool isSameCommunityAsSearcher = (communityid == thisSearcherCommunityId);

                if (!searchers.ContainsKey(communityid) && ((useAllIndexes && (!useNRT || !useSharding)) || isSameCommunityAsSearcher))
                {
                    Searcher searcher = new Searcher(communityid, IProcessorType.MemberIndexProcessor);
#if DEBUG
                    searcher.USE_NUNIT = USE_NUNIT;
                    searcher.NUNIT_UseNRT = NUNIT_UseNRT;
                    if (null != NUNIT_IndexPaths && NUNIT_IndexPaths.ContainsKey(communityid))
                    {
                        searcher.NUNIT_IndexPath = NUNIT_IndexPaths[communityid];
                    }
#endif
                    searcher.Init();
                    searchers.Add(communityid, searcher);
                }

                if (!keywordSearchers.ContainsKey(communityid) && (useAllIndexes || (useSharding && isSameCommunityAsSearcher)))
                {
                    Searcher searcher = new Searcher(communityid, IProcessorType.KeywordIndexProcessor);
#if DEBUG
                    searcher.USE_NUNIT = USE_NUNIT;
                    searcher.NUNIT_UseNRT = NUNIT_UseNRT;
                    if (null != NUNIT_KeywordIndexPaths && NUNIT_KeywordIndexPaths.ContainsKey(communityid))
                    {
                        searcher.NUNIT_IndexPath = NUNIT_KeywordIndexPaths[communityid];
                    }
#endif
                    searcher.Init();
                    keywordSearchers.Add(communityid, searcher);
                }
            }

            _updateThread = new Thread(new ThreadStart(Update));
            _updateThread.Start();
#if DEBUG
            if (!USE_NUNIT)
            {
#endif
                availableRamCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
                _gcThread = new Thread(new ThreadStart(ManageMemory));
                _gcThread.Start();
#if DEBUG
            }
#endif
        }

        public void Stop()
        {
            this._isRunning = false;
            _updateThread.Abort();

            if (null != searchers && searchers.Count > 0)
            {
                foreach (Searcher searcher in searchers.Values)
                {
                    searcher.Dispose();
                }
                searchers.Clear();
            }

            if (null != keywordSearchers && keywordSearchers.Count > 0)
            {
                foreach (Searcher searcher in keywordSearchers.Values)
                {
                    searcher.Dispose();
                }
                keywordSearchers.Clear();
            }

            _updateThread.Abort();
#if DEBUG
            if (!USE_NUNIT)
            {
#endif
            _gcThread.Abort();
#if DEBUG
            }
#endif
        }

        private int GetUpdateInterval()
        {
            return Utils.GetSetting("SEARCHER_INDEX_UPDATE_INTERVAL", 60000);
        }

        private int GetGCInterval()
        {
            return Utils.GetSetting("SEARCHER_GC_INTERVAL", 3600*1000);
        }

        private void Update()
        {
            while (this._isRunning)
            {
                Thread.Sleep(GetUpdateInterval());
                string[] communityList = Utils.GetSetting("SEARCHER_COMMUNITY_LIST", "").Split(new char[] {';'});
                int thisSearcherCommunityId = ServiceConstants.GetCommunityId(_communityName);
                bool useAllIndexes = Utils.GetSetting("SEARCHER_USE_ALL_INDEXES", thisSearcherCommunityId, false);
                bool useSharding = Utils.GetSetting("SEARCHER_PER_COMMUNITY", false);
                bool useNRT = IsUsingNRT(thisSearcherCommunityId);

                foreach (string communityStr in communityList)
                {
                    int communityId = Convert.ToInt32(communityStr);
                    bool isSameCommunityAsSearcher = (communityId == thisSearcherCommunityId);
                    if ((useAllIndexes && (!useNRT || !useSharding)) || isSameCommunityAsSearcher)
                    {
                        Searcher searcher = GetSearcher(communityId);
                        if (null != searcher)
                        {
                            searcher.Update();
                        }
                        Searcher keywordSearcher = GetKeywordSearcher(communityId);
                        if (null != keywordSearcher)
                        {
                            keywordSearcher.Update();
                        }
                    }
                    else
                    {
                        RemoveSearcher(communityId);
                        RemoveKeywordSearcher(communityId);
                    }
                }
            }
        }

        public float getAvailableRAM()
        {
            return availableRamCounter.NextValue();
        }

        private void ManageMemory()
        {
            while (true)
            {
                Thread.Sleep(GetGCInterval());
                //manage memory
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", string.Format("SearcherBL_MemoryManage(): Starting GC with {0} MB available memory.", getAvailableRAM().ToString()), null);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", string.Format("SearcherBL_MemoryManage(): Finished GC with {0} MB available memory.", getAvailableRAM().ToString()), null);
            }
        }

        public Searcher GetSearcher(int communityid)
        {
            if (!this._isRunning) return null;
            Searcher searcher = null;
            try
            {
                if (searchers.ContainsKey(communityid))
                {
                    searcher = searchers[communityid];
                }

                if (null == searcher)
                {
                    searcher = new Searcher(communityid, IProcessorType.MemberIndexProcessor);
                    searcher.Init();
                    searchers.Add(communityid, searcher);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "GetSearcher()", ex, null);
            }
            return searcher;
        }

        public void RemoveSearcher(int communityid)
        {
            Searcher searcher = null;
            try
            {
                if (searchers.ContainsKey(communityid))
                {
                    searcher = searchers[communityid];
                }

                if (null != searcher)
                {
                    searcher.Dispose();
                    searchers.Remove(communityid);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "RemoveSearcher()", ex, null);
            }
        }

        public Searcher GetKeywordSearcher(int communityid)
        {
            if (!this._isRunning) return null;
            Searcher searcher = null;
            try
            {
                if (keywordSearchers.ContainsKey(communityid))
                {
                    searcher = keywordSearchers[communityid];
                }

                if (null == searcher)
                {
                    searcher = new Searcher(communityid, IProcessorType.KeywordIndexProcessor);
                    searcher.Init();
                    keywordSearchers.Add(communityid, searcher);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "GetKeywordSearcher()", ex, null);
            }
            return searcher;
        }

        public void RemoveKeywordSearcher(int communityid)
        {
            Searcher searcher = null;
            try
            {
                if (keywordSearchers.ContainsKey(communityid))
                {
                    searcher = keywordSearchers[communityid];
                }

                if (null != searcher)
                {
                    searcher.Dispose();
                    keywordSearchers.Remove(communityid);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "RemoveKeywordSearcher()", ex, null);
            }
        }

        public IMatchnetQueryResults RunDetailedQuery(IMatchnetQuery query, int communityid)
        {
            return RunSparkQuery(query, communityid, ServiceConstants.SearchResultType.DetailedResult);
        }

        public IMatchnetQueryResults RunQuery(IMatchnetQuery query, int communityid)
        {
            return RunSparkQuery(query, communityid, ServiceConstants.SearchResultType.TerseResult);
        }

        public IMatchnetQueryResults RunSparkQuery(IMatchnetQuery query, int communityid, ServiceConstants.SearchResultType resultType)
        {
            string methodName = "RunQuery";
            MatchnetQueryResults queryResults = null;
            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
            Searcher searcher = null;
            QueryBuilder queryBuilder = null;
            TopDocs topDocs = null;
            try
            {
                if (!this._isRunning)
                {
                    throw Utils.ConvertException(_serviceConstant,new AlreadyClosedException("Searchers already closed!"),null);
                }
                
                switch (resultType)
                {
                    case ServiceConstants.SearchResultType.DetailedResult:
                        methodName = "RunDetailedQuery";
                        break;
                    default:
                        methodName = "RunQuery";
                        break;
                }

                bool enableKeywordFilter = Utils.GetSetting("ENABLE_KEYWORD_FILTER", communityid, false);
                searcher = GetSearcher(communityid);
                //if keywords filtering is on and user has keyword terms in their search prefs, filter out users without keyword matches
                bool hasKeywordTerms = HasKeywordTerms(query);
                IMemberIdsAccessor keywordMemberIdsAccessor = null;
                if (enableKeywordFilter && hasKeywordTerms)
                {
                    QueryBuilder keywordQueryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(query, communityid);
                    //if max results for keyword is not set by MatchnetQuery, use the setting
                    if(keywordQueryBuilder.MaxNumberOfMatches <= 0)
                    {
                        int maxResults = Utils.GetSetting("KEYWORD_SEARCHER_MAX_RESULTS", communityid, 2000);                        
                        keywordQueryBuilder.MaxNumberOfMatches = maxResults;
                    }
                    Searcher keywordSearcher = GetKeywordSearcher(communityid);
                    if (!stopWatch.IsRunning) stopWatch.Start();
                    TopDocs keywordResults = keywordSearcher.Search(keywordQueryBuilder);
                    int length = keywordResults.ScoreDocs.Length;
                    string[] memberIds = new string[length];
                    for (int i = 0; i < length; i++)
                    {
                        Document doc = keywordSearcher.IndexSearcher.Doc(keywordResults.ScoreDocs[i].Doc);
                        memberIds[i] = doc.GetField("id").StringValue;
                    }
                    keywordMemberIdsAccessor = new MemberIdsAccessor(memberIds);
#if DEBUG
                    KeywordQueryBuilder_forNUNIT = keywordQueryBuilder;
#endif
                }

                queryBuilder = QueryBuilderFactory.Instance.ConvertQuery(query, communityid, keywordMemberIdsAccessor);

                // add blocked member ids or any members to exclude
                MemberIdsAccessor memberIdsBlockedAccessor = null;
                for (var i = 0; i < query.Count; i++)
                {
                    // only add Ids for blocked or YnmNo
                    if (query[i].Parameter != QuerySearchParam.BlockedMemberIDs &&
                        query[i].Parameter != QuerySearchParam.ExcludeYnmMemberids) continue;

                    var value = query[i].Value;
                    
                    if (string.IsNullOrEmpty(value.ToString())) continue;
                    
                    var memberIdsBlocked = Regex.Split(value.ToString(), @"\W+");
                    
                    if (memberIdsBlockedAccessor == null)
                    {
                        memberIdsBlockedAccessor = new MemberIdsAccessor(memberIdsBlocked);
                    }
                    else
                    {
                        memberIdsBlockedAccessor.AddMembers(memberIdsBlocked);
                    }
                }

                if (memberIdsBlockedAccessor != null)
                {
                    var blockedMemberFilter = new BlockedMemberFilter(memberIdsBlockedAccessor);
                    queryBuilder.AddFilter(blockedMemberFilter);
                }

                //increment search counter
                SearcherAddSearch();

                if(!stopWatch.IsRunning) stopWatch.Start();
                topDocs = searcher.Search(queryBuilder);
            }
            catch (Exception ex)
            {
                string qs = query.ToString().Replace("\r\n", "|");
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", string.Format("RunQuery failed for query:{0}", qs), ex, null);
                throw new BLException("Unable to run query.  Query: " + qs, Utils.ConvertException(_serviceConstant,ex,null));
            }
            finally
            {
                //decrement search counter
                SearcherRemoveSearch();
                if (null != stopWatch)
                {
                    stopWatch.Stop();
                    //compute search average
                    SearcherAverageSearch(stopWatch.ElapsedMilliseconds);
                }
            }

            try
            {

                if (null == topDocs || topDocs.ScoreDocs.Length == 0)
                {
                    queryResults = new MatchnetQueryResults(0);
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", methodName, string.Format("Time: {0} ms, Results: {1}, Criteria: {2}", stopWatch.ElapsedMilliseconds, 0, query.ToString().Replace("\r\n", "|")));
                }
                else
                {

                    queryResults = new MatchnetQueryResults(topDocs.ScoreDocs.Length);
#if DEBUG
                    int sortType = 0;
                    if (USE_NUNIT)
                    {
                        sortType = GetSortType(query);
                    }
#endif
                    StringBuilder sbIDList = new StringBuilder();
                    for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
                    {
                        Document doc = searcher.IndexSearcher.Doc(topDocs.ScoreDocs[i].Doc);
                        IMatchnetResultItem item = null;
                        int id = Int32.Parse(doc.GetField("id").StringValue);
                        switch (resultType)
                        {
                            case ServiceConstants.SearchResultType.DetailedResult:
                                item = new DetailedQueryResult();
                                DetailedQueryResult detailedQueryResult = (DetailedQueryResult) item;
                                //populate int attributes
                                PopulateIntAttribute("id", detailedQueryResult, doc);
                                PopulateIntAttribute("communityid", detailedQueryResult, doc);
                                PopulateIntAttribute("gendermask", detailedQueryResult, doc);
                                PopulateIntAttribute("height", detailedQueryResult, doc);
                                PopulateIntAttribute("weight", detailedQueryResult, doc);
                                PopulateIntAttribute("colorcode", detailedQueryResult, doc);
                                PopulateIntAttribute("areacode", detailedQueryResult, doc);
                                PopulateIntAttribute("religion", detailedQueryResult, doc);
                                PopulateIntAttribute("ethnicity", detailedQueryResult, doc);
                                PopulateIntAttribute("smokinghabits", detailedQueryResult, doc);
                                PopulateIntAttribute("drinkinghabits", detailedQueryResult, doc);
                                PopulateIntAttribute("maritalstatus", detailedQueryResult, doc);
                                PopulateIntAttribute("jdatereligion", detailedQueryResult, doc);
                                PopulateIntAttribute("jdateethnicity", detailedQueryResult, doc);
                                PopulateIntAttribute("synagogueattendance", detailedQueryResult, doc);
                                PopulateIntAttribute("keepkosher", detailedQueryResult, doc);
                                PopulateIntAttribute("sexualidentitytype", detailedQueryResult, doc);
                                PopulateIntAttribute("relationshipstatus", detailedQueryResult, doc);
                                PopulateIntAttribute("bodytype", detailedQueryResult, doc);
                                PopulateIntAttribute("zodiac", detailedQueryResult, doc);
                                PopulateIntAttribute("majortype", detailedQueryResult, doc);
                                PopulateIntAttribute("schoolid", detailedQueryResult, doc);
                                PopulateIntAttribute("custody", detailedQueryResult, doc);
                                PopulateIntAttribute("relocateflag", detailedQueryResult, doc);
                                PopulateIntAttribute("activitylevel", detailedQueryResult, doc);
                                PopulateIntAttribute("morechildrenflag", detailedQueryResult, doc);
                                PopulateIntAttribute("childrencount", detailedQueryResult, doc);
                                PopulateIntAttribute("educationlevel", detailedQueryResult, doc);
                                PopulateIntAttribute("emailcount", detailedQueryResult, doc);
                                PopulateIntAttribute("depth4regionid", detailedQueryResult, doc);
                                PopulateIntAttribute("preferredminage", detailedQueryResult, doc);
                                PopulateIntAttribute("preferredmaxage", detailedQueryResult, doc);
                                PopulateIntAttribute("preferredgendermask", detailedQueryResult, doc);
                                PopulateIntAttribute("preferredhasphotoflag", detailedQueryResult, doc);
                                PopulateIntAttribute("preferredminheight", detailedQueryResult, doc);
                                PopulateIntAttribute("preferredmaxheight", detailedQueryResult, doc);

                                //populate datetime attributes
                                PopulateDateTimeAttribute("lastactivedate", detailedQueryResult, doc);
                                PopulateDateTimeAttribute("communityinsertdate", detailedQueryResult, doc);
                                PopulateDateTimeAttribute("subscriptionexpirationdate", detailedQueryResult, doc);
                                PopulateDateTimeAttribute("birthdate", detailedQueryResult, doc);

                                //populate mask attributes
                                PopulateMaskAttribute(doc, detailedQueryResult, "relationshipmask");
                                PopulateMaskAttribute(doc, detailedQueryResult, "languagemask");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredsynagogueattendance");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredjdatereligion");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredsmokinghabit");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferreddrinkinghabit");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredkosherstatus");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferrededucationlevel");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredactivitylevel");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredmorechildrenflag");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredethnicity");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredlanguagemask");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredrelocation");
                                PopulateMaskAttribute(doc, detailedQueryResult, "preferredareacodes");

                                //populate has photo 
                                if (!string.IsNullOrEmpty(doc.GetField("hasphotoflag").StringValue))
                                    detailedQueryResult.HasPhoto = (doc.GetField("hasphotoflag").StringValue == "1");

                                //populate age
                                int age = 0;
                                if (detailedQueryResult.Tags.ContainsKey("birthdate") &&
                                    null != detailedQueryResult.Tags["birthdate"])
                                {
                                    DateTime birthDate = (DateTime) detailedQueryResult.Tags["birthdate"];
                                    age = Utils.CalculateAge(birthDate);
                                    detailedQueryResult.Tags["age"] = age;
                                }

                                //TODO: Get isonline
                                //detailedQueryResult.IsOnline = (memberSpec.IsOnline == UpdateFlag.SetYes);

                                //populate distance
                                double distanceMi = double.MinValue;
                                if (!string.IsNullOrEmpty(doc.GetField("latitude").StringValue) &&
                                    !string.IsNullOrEmpty(doc.GetField("longitude").StringValue)
                                    && null != queryBuilder && null != queryBuilder.SpatialShape)
                                {
                                    try
                                    {
                                        double lng =
                                            Lucene.Net.Util.NumericUtils.PrefixCodedToDouble(
                                                doc.GetField("longitude").StringValue);
                                        double lat =
                                            Lucene.Net.Util.NumericUtils.PrefixCodedToDouble(
                                                doc.GetField("latitude").StringValue);
                                        Shape shape = null;
                                        try
                                        {
                                            shape = Utils.SpatialContext.MakePoint(lng, lat);
                                            var pt = shape as Point;
                                            DistanceCalculator distanceCalculator = Utils.SpatialContext.GetDistCalc();
                                            double docDistDEG = distanceCalculator.Distance(pt,
                                                                                            queryBuilder.SpatialShape.
                                                                                                GetCenter());
                                            detailedQueryResult.Distance = DistanceUtils.Degrees2Dist(docDistDEG,
                                                                                                      DistanceUtils.
                                                                                                          EARTH_MEAN_RADIUS_MI);
                                        }
                                        catch (InvalidOperationException)
                                        {
                                            detailedQueryResult.Distance = double.NaN;
                                        }
                                    }
                                    catch (Exception de)
                                    {
                                        RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL",
                                                                                resultType.ToString(), de, null);
                                    }
                                }

                                detailedQueryResult.AgeScore = age;
                                detailedQueryResult.CsczScore = (int) distanceMi;
                                //calculate if user is new
                                if (null != detailedQueryResult.RegisterDate &&
                                    detailedQueryResult.RegisterDate > DateTime.MinValue)
                                {
                                    int isNew = (DateTime.Now.Subtract(detailedQueryResult.RegisterDate).TotalDays <= 14)
                                                    ? 1
                                                    : 0;
                                    detailedQueryResult.NewScore = isNew;
                                }
                                detailedQueryResult.TotalScore = (int) doc.Boost;

                                if (queryBuilder.matchPercentages.ContainsKey(id))
                                {
                                    detailedQueryResult.MatchScore = queryBuilder.matchPercentages[id];
                                }
                                queryResults.AddResult(item);
                                break;
                            default:
                                //id = Int32.Parse(doc.GetField("id").StringValue);
                                item = new MatchnetResultItem(id);
                                if (queryBuilder.matchPercentages.ContainsKey(id))
                                {
                                    item.MatchScore = queryBuilder.matchPercentages[id];
                                }

                                if (queryBuilder.saveMismatchData)
                                {
                                    if (item.MatchScore >= 0)
                                    {
                                        if (queryBuilder.matchMismatches.ContainsKey(id))
                                        {
                                            sbIDList.Append(id.ToString() + " (" +
                                                            queryBuilder.matchPercentages[id].ToString() + "-" +
                                                            queryBuilder.matchMismatches[id] + ")");
                                        }
                                        else
                                        {
                                            sbIDList.Append(id.ToString() + " (" +
                                                            queryBuilder.matchPercentages[id].ToString() + ")");
                                        }
                                    }
                                    else
                                    {
                                        sbIDList.Append(id.ToString());
                                    }

                                    if (queryBuilder.distances.ContainsKey(id))
                                    {
                                        sbIDList.Append(" (" + queryBuilder.distances[id].ToString() + ")");
                                    }

                                    sbIDList.Append(", ");
                                }

                                queryResults.AddResult(item);
                                break;
                        }
#if DEBUG
                        if (USE_NUNIT)
                        {
                            WriteOutputToBuffer(queryBuilder, sortType, doc, item);
                        }
                        QueryBuilder_forNUNIT = queryBuilder;
#endif
                    }

                    //log final search info
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", methodName, string.Format("Time: {0} ms, Results: {1}, Criteria: {2}, MatchRating: {3}, UsedMatchRatingFieldCache: {4}", stopWatch.ElapsedMilliseconds, topDocs.ScoreDocs.Length, query.ToString().Replace("\r\n", "|"), queryBuilder.calculatedMatchRating, queryBuilder.usedFieldCacheForCalculation));
                    queryResults.MatchesFound = topDocs.ScoreDocs.Length;

                    if (queryBuilder.saveMismatchData)
                    {
                        //outputs results along with match rating info (score and mismatches)
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", methodName, sbIDList.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "RunQuery", ex, null);
                throw new BLException("Unable to run query.  Query: " + query.ToString().Replace("\r\n", "|"),Utils.ConvertException(_serviceConstant,ex,null));
            }
            return queryResults;
        }

        private bool HasKeywordTerms(IMatchnetQuery query)
        {
            bool hasKeywordTerms = false;
            for (int i = 0; i < query.Count; i++)
            {
                if (query[i].Parameter == QuerySearchParam.KeywordSearch)
                {
                    object value = query[i].Value;
                    if (!string.IsNullOrEmpty(value.ToString()))
                    {
                        hasKeywordTerms = true;
                    }
                }
            }
            return hasKeywordTerms;
        }

        public void NRTUpdateMember(SearchMember searchMember)
        {
            if (!this._isRunning) return;
            SearchPreferenceCollection searchPreferences = null;
            if (Utils.GetSetting("NRT_USE_SEARCH_PREF_SA", true))
            {
                if (!Utils.IsSearchPrefCacheDisabled())
                {
                    //use cache
                    searchPreferences = Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSearchPreferences(searchMember.MemberID, searchMember.CommunityID);
                }
                else
                {
                    //ignore cache - Temp for QA to quickly change prefs for the same member over and over
                    searchPreferences = Matchnet.Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSearchPreferences(searchMember.MemberID, searchMember.CommunityID, true);
                }
            }
            NRTUpdateMember(searchMember, searchPreferences);
        }

        public void NRTUpdateMember(SearchMember searchMember, SearchPreferenceCollection searchPreferences)
        {
            try
            {
                if (!this._isRunning) return;

                if (searchMember != null)
                {
                    bool useNRT = IsUsingNRT(searchMember.CommunityID);
                    string paramInfo = "CommunityID:" + searchMember.CommunityID.ToString() + ", MemberID:" + searchMember.MemberID.ToString() + ", UpdateMode:" + searchMember.UpdateMode.ToString() + ", UpdateReason:" + searchMember.UpdateReason.ToString();
                    if (Utils.IsNoisyLoggingEnabled())
                    {
                        try
                        {
                            paramInfo += ", Attributes: " + JsonConvert.SerializeObject(searchMember);
                            if (searchPreferences != null)
                            {
                                paramInfo += ", Prefs: " + JsonConvert.SerializeObject(searchPreferences);
                            }
                        }
                        catch (Exception serializeException)
                        {
                            //ignore
                        }
                    }

                    if (useNRT)
                    {
                        if (searchMember.CommunityID > 0 && searchMember.MemberID > 0
                            && (searchMember.UpdateMode == UpdateModeEnum.add || searchMember.UpdateMode == UpdateModeEnum.update))
                        {
                            PopulateSearchPreferences(searchMember, searchPreferences);
                            //update index
                            Searcher searcher = GetSearcher(searchMember.CommunityID);
                            if (searcher != null && searcher.IndexWriter != null)
                            {
                                bool isValid = true;

                                //term based on memberID
                                Term memberIDTerm = new Term("id", searchMember.MemberID.ToString());

                                #region Uncomment if we decide to only update if member exists in index
                                //if (searchMember.UpdateMode == UpdateModeEnum.update)
                                //{
                                //    TermQuery cq = new TermQuery(memberIDTerm);
                                //    //only update is member exists in index
                                //    QueryBuilder queryBuilder = new QueryBuilder();
                                //    queryBuilder.AddQuery(cq);
                                //    TopDocs topDocs = searcher.Search(queryBuilder);
                                //    if (null == topDocs || topDocs.scoreDocs.Length == 0)
                                //    {
                                //        isValid = false;
                                //        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberField", "Member document not found. CommunityID:" + searchMember.CommunityID.ToString() + ", MemberID:" + searchMember.MemberID.ToString());
                                //    }
                                //}
                                #endregion

                                if (isValid)
                                {
                                    //create new document
                                    DocumentMetadata documentMetadata = Utils.GetDocMetadata(searchMember.CommunityID);
                                    SearchDocument memberDoc = new SearchDocument(documentMetadata);
                                    memberDoc.CommunityName = ServiceConstants.GetCommunityName(searchMember.CommunityID);
                                    memberDoc.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                                    memberDoc.RegionLanguages = Utils.RegionLanguages;
                                    memberDoc.Strategy = Utils.SpatialStrategy;
                                    memberDoc.Context = Utils.SpatialContext;
                                    memberDoc.Populate(searchMember, documentMetadata);

                                    //update index document
                                    searcher.IndexWriter.UpdateDocument(memberIDTerm, memberDoc.LuceneDocument);                                    
                                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMember", "Updated Member in index. " + paramInfo);
                                }
                            }
                            else
                            {
                                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMember", "IndexWriter is unavailable. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                            }
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMember", "Invalid data in SearchMember obj. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                        }
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMember", "NRT is disabled. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMember", "SearchMember obj is null");
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "NRTUpdateMember", ex, searchMember);
                throw new BLException("Unable to update Member for NRT", Utils.ConvertException(_serviceConstant,ex,null));
            }
        }

        private void PopulateSearchPreferences(SearchMember searchMember, SearchPreferenceCollection searchPreferences)
        {
            if (null != searchPreferences)
            {
                try
                {
                    TestQueryBuilder.PopulateSearchPreferences(searchMember, searchPreferences);
                }
                catch (Exception e)
                {
                    RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "PopulateSearchPreferences", e, searchPreferences);
                }
            }
        }

        public void NRTUpdateMemberByDB(SearchMemberUpdate searchMemberUpdate)
        {
            try
            {
                if (!this._isRunning) return;
                if (searchMemberUpdate != null)
                {
                    bool useNRT = IsUsingNRT(searchMemberUpdate.CommunityID);
                    string paramInfo = "CommunityID:" + searchMemberUpdate.CommunityID.ToString() + ", MemberID:" + searchMemberUpdate.MemberID.ToString() + ", UpdateMode:" + searchMemberUpdate.UpdateMode.ToString() + ", UpdateReason:" + searchMemberUpdate.UpdateReason.ToString();
                    if (useNRT)
                    {
                        if (searchMemberUpdate.CommunityID > 0 && searchMemberUpdate.MemberID > 0
                            && (searchMemberUpdate.UpdateMode == UpdateModeEnum.add || searchMemberUpdate.UpdateMode == UpdateModeEnum.update))
                        {
                            //update index
                            Searcher searcher = GetSearcher(searchMemberUpdate.CommunityID);
                            if (searcher != null && searcher.IndexWriter != null)
                            {
                                bool isValid = true;

                                //term based on memberID
                                Term memberIDTerm = new Term("id", searchMemberUpdate.MemberID.ToString());
                                TermQuery cq = new TermQuery(memberIDTerm);

                                #region Uncomment if we decide to only update if member exists in index
                                //if (searchMemberUpdate.UpdateMode == UpdateModeEnum.update)
                                //{
                                //    //only update is member exists in index
                                //    QueryBuilder queryBuilder = new QueryBuilder();
                                //    queryBuilder.AddQuery(cq);
                                //    TopDocs topDocs = searcher.Search(queryBuilder);
                                //    if (null == topDocs || topDocs.scoreDocs.Length == 0)
                                //    {
                                //        isValid = false;
                                //        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", "Member document not found. " + paramInfo);
                                //    }
                                //}
                                #endregion

                                if (isValid)
                                {
                                    //query searchstore
                                    DataRow searchMemberDataRow = GetCommunitySearchMemberRecord(searchMemberUpdate.MemberID, searchMemberUpdate.CommunityID);

                                    if (searchMemberDataRow != null)
                                    {
                                        //create SearchMember
                                        SearchMember searchMember = new SearchMember();
                                        searchMember.Populate(searchMemberDataRow);
                                        searchMember.UpdateType = UpdateTypeEnum.all;
                                        searchMember.UpdateReason = searchMemberUpdate.UpdateReason;
                                        searchMember.UpdateMode = searchMemberUpdate.UpdateMode;

                                        //create new document
                                        DocumentMetadata documentMetadata = Utils.GetDocMetadata(searchMember.CommunityID);
                                        SearchDocument memberDoc = new SearchDocument(documentMetadata);
                                        memberDoc.CommunityName = ServiceConstants.GetCommunityName(searchMember.CommunityID);
                                        memberDoc.AreaCodeDictionary = Utils.RegionAreaCodeDictionary;
                                        memberDoc.RegionLanguages = Utils.RegionLanguages;
                                        memberDoc.Strategy = Utils.SpatialStrategy;
                                        memberDoc.Context = Utils.SpatialContext;
                                        memberDoc.Populate(searchMember, documentMetadata);

                                        //update index document
                                        searcher.IndexWriter.UpdateDocument(memberIDTerm, memberDoc.LuceneDocument);
                                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", "Updated Member in index. " + paramInfo);
                                    }
                                }
                            }
                            else
                            {
                                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", "IndexWriter is unavailable. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                            }
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", "Invalid data in SearchMember obj. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                        }
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", "NRT is disabled. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", "SearchMember obj is null");
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "NRTUpdateMemberByDB", ex, searchMemberUpdate);
                throw new BLException("Unable to update Member for NRT", Utils.ConvertException(_serviceConstant,ex,null));
            }
        }

        public void NRTRemoveMember(SearchMemberUpdate searchMemberUpdate)
        {
            if (!this._isRunning) return;
            string paramInfo = "";
            try
            {
                if (searchMemberUpdate != null)
                {
                    paramInfo = "memberID: " + searchMemberUpdate.MemberID.ToString() + ",communityID: " + searchMemberUpdate.CommunityID + ",Reason: " + searchMemberUpdate.UpdateReason.ToString();

                    if (searchMemberUpdate.MemberID > 0 && searchMemberUpdate.CommunityID > 0)
                    {
                        bool useNRT = IsUsingNRT(searchMemberUpdate.CommunityID);
                        if (useNRT)
                        {
                            //remove member from index, if exists
                            Searcher searcher = GetSearcher(searchMemberUpdate.CommunityID);
                            if (searcher != null && searcher.IndexWriter != null)
                            {
                                Term memberIDTerm = new Term("id", searchMemberUpdate.MemberID.ToString());
                                searcher.IndexWriter.DeleteDocuments(memberIDTerm);
                                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemoveMember", "Removed member from index. " + paramInfo);
                            }
                            else
                            {
                                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemoveMember", "IndexWriter is unavailable. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                            }
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemoveMember", "NRT is disabled. USE_E2_NRT: " + useNRT.ToString() + ", " + paramInfo);
                        }
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemoveMember", "Invalid data. " + paramInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "NRTRemoveMember", ex, null);
                throw new BLException("Unable to remove Member for NRT", Utils.ConvertException(_serviceConstant, ex, null));
            }
        }

        #region Private methods
        private static void PopulateIntAttribute(string fieldName, DetailedQueryResult detailedQueryResult, Document doc)
        {
            try
            {
                int intValue = int.MinValue;
                Field field = doc.GetField(fieldName);
                if (null != field && !string.IsNullOrEmpty(field.StringValue))
                {
                    intValue = Int32.Parse(field.StringValue);
                }
                if (intValue > int.MinValue)
                {
                    switch(fieldName)
                    {
                        case "id":
                            detailedQueryResult.MemberID = intValue;
                            break;
                        case "communityid":
                            detailedQueryResult.CommunityID = intValue;
                            break;
                        case "gendermask":
                            detailedQueryResult.GenderMask = intValue;
                            break;
                        case "colorcode":
                            detailedQueryResult.ColorCode = intValue;
                            break;
                        case "depth4regionid":
                            detailedQueryResult.Tags["regionid"] = intValue;
                            break;
                        default:                           
                            detailedQueryResult.Tags[fieldName] = intValue;
                            break;
                    }
                }
            }
            catch (Exception ex){}
        }

        private static void PopulateDateTimeAttribute(string fieldName, DetailedQueryResult detailedQueryResult, Document doc)
        {
            try
            {
                DateTime dateTimeValue = DateTime.MinValue;
                Field field = doc.GetField(fieldName);
                if (null != field && !string.IsNullOrEmpty(field.StringValue))
                {
                    dateTimeValue = new DateTime(Int64.Parse(field.StringValue));
                }
                if (dateTimeValue > DateTime.MinValue)
                {
                    switch (fieldName)
                    {
                        case "lastactivedate":
                            detailedQueryResult.ActiveDate = dateTimeValue;
                            break;
                        case "communityinsertdate":
                            detailedQueryResult.RegisterDate = dateTimeValue;
                            break;
                        default:
                            detailedQueryResult.Tags[fieldName] = dateTimeValue;
                            break;
                    }
                    
                }
            }
            catch (Exception ex){}
        }
                                        

        private void PopulateMaskAttribute(Document doc, DetailedQueryResult detailedQueryResult, string attributeName)
        {
            try
            {
                if (null != doc.GetField(attributeName) && !string.IsNullOrEmpty(doc.GetField(attributeName).StringValue))
                {
                    Field[] fields = doc.GetFields(attributeName);
                    for (int j = 0; null != fields && j < fields.Length; j++)
                    {
                        if (string.IsNullOrEmpty(detailedQueryResult.Tags[attributeName] as string)) detailedQueryResult.Tags[attributeName] = string.Empty;
                        detailedQueryResult.Tags[attributeName] += fields[j].StringValue;
                        if (j + 1 != fields.Length) detailedQueryResult.Tags[attributeName] += ",";
                    }
                }

            }
            catch (Exception ex){}
        }

        private DataRow GetCommunitySearchMemberRecord(int memberID, int communityID)
        {
            DataTable dt = null;
            DataRow searchMemberDataRow = null;

            string dbStore = "SearchLoad";
            dt = GetCommunitySearchMember(memberID, communityID);

            if (dt != null && dt.Rows.Count > 0)
            {
                searchMemberDataRow = dt.Rows[0];
            }
            else
            {
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "GetCommunitySearchMemberRecord", "Member not found in " + dbStore + ". CommunityID:" + communityID.ToString() + ", MemberID:" + memberID.ToString());
            }

            return searchMemberDataRow;
        }

        private DataTable GetCommunitySearchMember(int memberID, int communityID)
        {
            DataTable dt = null;
            try
            {
                if (memberID > 0 && communityID > 0)
                {
                    Command command = new Command(ServiceConstants.SEARCHLOAD_DB_NAME, "[up_SearchLoad_Get_Member]", 0);

                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);

                    dt = Client.Instance.ExecuteDataTable(command);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "GetCommunitySearchMember()", ex, "memberID: " + memberID.ToString() + ",communityID: " + communityID.ToString());
            }
            return dt;
        }

        private bool IsUsingNRT(int communityID)
        {
#if DEBUG
            if (USE_NUNIT)
            {
                return NUNIT_UseNRT;
            }
#endif
            return Utils.GetSetting("USE_E2_NRT", communityID, false);
        }
        #endregion
    }
}
