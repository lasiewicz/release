﻿using System;
using System.Linq;
using System.Threading;
using Lucene.Net.Analysis;
using Lucene.Net.Search;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Spark.SearchEngine.ValueObjects;
using Spark.Logging;
using Directory = Lucene.Net.Store.Directory;
using Matchnet;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class Searcher
    {
#if DEBUG
        private bool _Use_NUnit = false;
        public bool USE_NUNIT
        {
            get { return _Use_NUnit; }
            set { _Use_NUnit = value; }
        }

        private bool _NUnit_UseNRT = false;
        public bool NUNIT_UseNRT
        {
            get { return _NUnit_UseNRT; }
            set { _NUnit_UseNRT = value; }
        }

        public string NUNIT_IndexPath { get; set; }
        public bool NUNIT_DeleteOldIndex { get; set; }
#endif
        private IndexSearcher _indexSearcher = null;
        private IndexReader _indexReader = null;
        private Directory _indexDirectory = null;
        private IndexWriter _indexWriter = null;
        private string _indexPath;
        private int _searchCount;
        private int _communityId;
        private IndexSearcher oldIndexSearcher = null;
        private IndexReader oldIndexReader = null;
        private Directory oldIndexDirectory = null;
        private IndexWriter oldIndexWriter = null;
        private string _oldIndexPath = "";
        private string _serviceConstant = string.Empty; 
        private string _machineName = string.Empty;
        private IProcessorType _iProcessorType;
        public static readonly string CLASS_NAME = "Searcher";

        private IProcessorType IProcessorType
        {
            get { return _iProcessorType; }
        }

        private string MachineName
        {
            get { 
                if(string.IsNullOrEmpty(_machineName)) return System.Environment.MachineName;
                return _machineName;
            }
            set { _machineName = value; }
        }

        public IndexSearcher IndexSearcher
        {
            get { return _indexSearcher; }
        }

        public string IndexPath
        {
            get { return _indexPath; }
        }

        public IndexWriter IndexWriter
        {
            get { return _indexWriter; }
        }

        public Searcher(int communityId, IProcessorType iProcessorType)
        {
            _communityId = communityId;
            _iProcessorType = iProcessorType;
        }

        public void Init()
        {
            _serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(_communityId).ToUpper());
            _indexPath = GetIndexPath();
            _indexDirectory = CreateIndexDirectory(_indexPath);

            bool useNRT = IsUsingNRT(_communityId);
            _indexWriter = CreateIndexWriter(_indexDirectory, _indexPath);
            _indexReader = CreateIndexReader(_indexDirectory, _indexWriter);
            _indexSearcher = CreateIndexSearcher(_indexReader);
            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Initialized Searcher. USE_E2_NRT: " + useNRT.ToString() +  ", indexPath: " + _indexPath, null);
        }

        public void Dispose()
        {
            try
            {
                CloseOldSearcher(false);

                string oldLockFile = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, MachineName.ToLower());
                if (_indexDirectory.FileExists(oldLockFile))
                {
                    _indexDirectory.DeleteFile(oldLockFile);
                }

                if (_indexSearcher != null)
                    _indexSearcher.Dispose();

                if (_indexReader != null)
                    _indexReader.Dispose();

                if (_indexWriter != null)
                    _indexWriter.Dispose();

                if (_indexDirectory != null)
                    _indexDirectory.Dispose();
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "Dispose()", ex, null);
            }
            finally
            {                
                _indexPath = null;
                _indexDirectory = null;
                _indexReader = null;
                _indexSearcher = null;
                _searchCount = 0;
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "Searcher.Dispose()", string.Format("Disposed {0} on {1}", _communityId, MachineName), null);
            }
        }

        private bool GetDeleteOldIndex()
        {
#if DEBUG
            if (USE_NUNIT)
            {
                return NUNIT_DeleteOldIndex;
            }
#endif
            return Boolean.TrueString.ToLower().Equals(Utils.GetSetting("SEARCHER_DELETE_OLD_INDEX", "true"));
        }

        private int GetCloseWaitTime()
        {
            string maxResultsStr = Utils.GetSetting("SEARCHER_CLOSE_WAIT_TIME", "3000");
            return Convert.ToInt32(maxResultsStr);
        }

        private IndexSearcher CreateIndexSearcher(IndexReader indexReader)
        {
            IndexSearcher indexSearcher = new IndexSearcher(indexReader);
            return indexSearcher;
        }

        private IndexReader CreateIndexReader(Directory directory, IndexWriter indexWriter)
        {
            IndexReader indexReader = null;
            if (indexWriter != null)
            {
                indexReader = indexWriter.GetReader();
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Created NRT IndexReader", null);
            }
            else
            {
                indexReader = IndexReader.Open(directory, true);
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Created Regular IndexReader", null);
            }

            return indexReader;
        }

        private Directory CreateIndexDirectory(string indexPath)
        {
            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Opening indexPath=" + indexPath, null);
            Directory dir = null;
            if (!string.IsNullOrEmpty(indexPath))
            {
                System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(indexPath);
                System.IO.FileStream file = null;
                try
                {
                    string lockFilePath = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, _indexPath + "/" + MachineName.ToLower());
                    if (!System.IO.File.Exists(lockFilePath))
                    {
                        file = System.IO.File.Create(lockFilePath);
                    }
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "GetIndexDirectory", ex, null);
                }
                finally
                {
                    if (null != file)
                    {
                        file.Close();
                    }
                }
                dir = FSDirectory.Open(dirinfo);
            }
            return dir;
        }

        private IndexWriter CreateIndexWriter(Directory directory, string dirIndexPath)
        {
            IndexWriter indexWriter = null;
            try
            {
                bool useNRT = IsUsingNRT(_communityId);
                if (useNRT && directory != null)
                {
                    indexWriter = new IndexWriter(directory, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), false, IndexWriter.MaxFieldLength.UNLIMITED);
                    indexWriter.UseCompoundFile = true;
                    indexWriter.MergeFactor = Conversion.CInt(Utils.GetSetting("INDEXER_MERGE_FACTOR", _communityId, "30"));
                    //indexWriter.SetMaxBufferedDocs(Conversion.CInt(Utils.GetSetting("INDEXER_MAX_MERGE_DOCS", _communityId, "1000")));
                    indexWriter.SetRAMBufferSizeMB(Conversion.CInt(Utils.GetSetting("INDEXER_MAX_MB_BUFFER_SIZE", _communityId, "48")));

                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "Created IndexWriter for " + dirIndexPath, null);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "CreateIndexWriter", ex, null);
            }
            return indexWriter;
        }

        private string GetIndexPath()
        {
            string indexPath = null;
            try
            {
                indexPath = Utils.GetCurrentIndexDirectory(_communityId, IProcessorType, MachineName, CLASS_NAME);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "GetIndexDirectory", ex, null);
            }

            if (string.IsNullOrEmpty(indexPath))
            {
                //if no indexpath is found and current index path exists, return current one
                if (!string.IsNullOrEmpty(_indexPath))
                {                    
                    indexPath = _indexPath;
                }
                else
                {
                    //if no current index path then create one using setting
                    string searcherCommunityDirKey = "SEARCHER_COMMUNITY_DIR";
                    switch (_iProcessorType)
                    {
                        case IProcessorType.KeywordIndexProcessor:
                            searcherCommunityDirKey = "KEYWORD_SEARCHER_COMMUNITY_DIR";
                            break;
                        case IProcessorType.KeywordIndexVersion3Processor:
                            searcherCommunityDirKey = "KEYWORD_SEARCHER_VERSION_3_COMMUNITY_DIR";
                            break;
                        case IProcessorType.AdminIndexProcessor:
                            searcherCommunityDirKey = "ADMIN_SEARCHER_COMMUNITY_DIR";
                            break;
                        case IProcessorType.MemberIndexVersion3Processor:
                            searcherCommunityDirKey = "SEARCHER_VERSION_3_COMMUNITY_DIR";
                            break;
                        case IProcessorType.MemberIndexProcessor:
                        default:
                            searcherCommunityDirKey = "SEARCHER_COMMUNITY_DIR";
                            break;
                    }
                    indexPath = Utils.GetSetting(searcherCommunityDirKey, _communityId, "c:\\Matchnet\\Index\\" + _communityId.ToString() + "\\");                    
                }
            }

        #if DEBUG
            if (USE_NUNIT)
            {
                if (!string.IsNullOrEmpty(NUNIT_IndexPath))
                {
                    indexPath = NUNIT_IndexPath;
                }
            }
            else if (System.Environment.MachineName.ToLower() == "bhd-tlam")
            {
                indexPath = indexPath.Replace("d:", "c:");
            }
        #endif
            return indexPath;
        }


        public void Update()
        {
            //RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "Searcher", "Update() - Checking to update Searcher. CommunityID:" + _communityId.ToString(), null);
            Directory newDir = null;
            try
            {
                string newIndexPath = GetIndexPath();
                if (!string.IsNullOrEmpty(newIndexPath))
                {
                    if (newIndexPath != _indexPath)
                    {
                        System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(newIndexPath);
                        newDir = FSDirectory.Open(dirinfo);
                    }

                    bool useNRT = IsUsingNRT(_communityId);
                    int maxSearchThreshold = Utils.GetSetting("SEARCHER_SEARCH_THRESHOLD", _communityId, 3000);
                    bool refreshDir = _searchCount > maxSearchThreshold;

                    if (null != newDir || (refreshDir && !useNRT))
                    {
                        Update(newDir, newIndexPath, refreshDir);
                        //reset _searchCount if past max threshold
                        if (_searchCount > maxSearchThreshold) _searchCount = 0;
                    }
                    else
                    {
                        UpdateNRT();
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "Update() exception with indexPath=" + _indexPath, ex, null);
            }
        }

        public void Update(Directory newDir, string newIndexPath, bool refreshDir)
        {
            try
            {
                bool deleteOldIndex = GetDeleteOldIndex();
                
                if (null != newDir || refreshDir)
                {
                    bool useNRT = IsUsingNRT(_communityId);

                    Directory newDirectory = newDir;
                    //if null == newDirectory then we are closing/reopening current index dir
                    //to free up objects for the garbage collector
                    if (null == newDirectory && !useNRT)
                    {
                        System.IO.DirectoryInfo dirinfo = new System.IO.DirectoryInfo(_indexPath);
                        newDirectory = FSDirectory.Open(dirinfo);
                        deleteOldIndex = false;
                    }

                    if (newDirectory != null)
                    {
                        IndexWriter newWriter = CreateIndexWriter(newDirectory, newIndexPath);
                        IndexReader newReader = CreateIndexReader(newDirectory, newWriter);
                        IndexSearcher newSearcher = new IndexSearcher(newReader);
                        if (VerifySearchIndex(newSearcher))
                        {
                            //if index is verified set new index path
                            _oldIndexPath = _indexPath;
                            _indexPath = newIndexPath;

                            //retain pointers to old searcher
                            oldIndexDirectory = _indexDirectory;
                            oldIndexWriter = _indexWriter;
                            oldIndexReader = _indexReader;
                            oldIndexSearcher = _indexSearcher;

                            //set new searcher
                            _indexDirectory = newDirectory;
                            _indexWriter = newWriter;
                            _indexReader = newReader;
                            _indexSearcher = newSearcher;

                            //close old searcher after wait time
                            Thread closeThread = new Thread(CleanupOldSearcher);
                            closeThread.Start(deleteOldIndex);

                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "UpdateSearcher() Updated Searcher for indexPath=" + _indexPath, null);
                        }
                        else
                        {
                            try
                            {
                                if (null != newSearcher)
                                {
                                    newSearcher.Dispose();
                                }

                                if (null != newReader)
                                {
                                    newReader.Dispose();
                                }

                                if (null != newWriter)
                                {
                                    newWriter.Dispose();
                                }

                                if (null != newDirectory)
                                {
                                    newDirectory.Dispose();
                                }
                            }
                            catch (Exception ex)
                            {
                                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "UpdateSearcher() exception when closing for invalid newIndexPath=" + newIndexPath, ex, null);
                            }
                            finally
                            {
                                newSearcher = null;
                                newReader = null;
                                newWriter = null;
                                newDirectory = null;                                
                            }

                            //set db index path to current working index
                            if (!string.IsNullOrEmpty(_indexPath))
                            {
                                Utils.UpdateIndexDirectory(_communityId, IProcessorType, MachineName, _indexPath, CLASS_NAME);
                            }

                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "UpdateSearcher() Invalid index at indexPath=" + newIndexPath, null);
                        }
                    }
                }
                                                                   
                //write lock file in new index dir
                System.IO.FileStream file = null;
                try
                {
                    string lockFilePath = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, _indexPath + "/" + System.Environment.MachineName.ToLower());
                    if (!System.IO.File.Exists(lockFilePath))
                    {
                        file = System.IO.File.Create(lockFilePath);
                    }
                }
                finally
                {
                                    
                    if (null != file)
                    {
                        file.Close();
                    }
                }
            
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "UpdateSearcher() exception with indexPath=" + _indexPath, ex, null);
            }        
        }

        public void UpdateNRT()
        {
            try
            {
                bool useNRT = IsUsingNRT(_communityId);
                if (useNRT)
                {
                    if (!_indexReader.IsCurrent() || _indexWriter == null)
                    {
                        if (_indexWriter == null)
                        {
                            _indexWriter = CreateIndexWriter(_indexDirectory, _indexPath);
                        }

                        if (_indexWriter != null)
                        {
                            //getting updated NRT Reader
                            IndexReader newReader = CreateIndexReader(_indexDirectory, _indexWriter);
                            IndexSearcher newSearcher = new IndexSearcher(newReader);

                            //retain pointers to old searcher
                            oldIndexReader = _indexReader;
                            oldIndexSearcher = _indexSearcher;

                            //set new searcher
                            _indexReader = newReader;
                            _indexSearcher = newSearcher;

                            //close old searcher after wait time
                            Thread closeThread = new Thread(CleanupOldSearcher);
                            closeThread.Start(false);

                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "UpdateNRT() - Updated Searcher and Reader for NRT", null);
                        }
                    }
                }
                else
                {
                    //this is likely real time disable of NRT
                    if (_indexWriter != null)
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, "UpdateNRT() - NRT is false with opened IndexWriter, closing IndexWriter for indexpath: " + _indexPath, null);
                        _indexWriter.Dispose();
                        _indexWriter = null;

                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "UpdateNRT()", ex, null);
            }    
        }

        private void CleanupOldSearcher(object o)
        {
            Thread.Sleep(GetCloseWaitTime());
            CloseOldSearcher(o);
        }

        private void CloseOldSearcher(object o)
        {
            try
            {
                bool closedOldSearcher = false;
                bool closedOldReader = false;
                bool closedOldWriter = false;
                bool closedOldDirectory = false;

                if (null != oldIndexSearcher)
                {
                    //close old searcher
                    oldIndexSearcher.Dispose();
                    closedOldSearcher = true;
                }

                if (null != oldIndexReader)
                {
                    oldIndexReader.Dispose();
                    closedOldReader = true;
                }

                if (null != oldIndexWriter)
                {
                    oldIndexWriter.Dispose();
                    closedOldWriter = true;
                }

                if (null != oldIndexDirectory)
                {
                    bool deleteOldIndex = (bool)o;
                    if (deleteOldIndex)
                    {
                        //remove all files from old directory
                        foreach (string fileName in oldIndexDirectory.ListAll())
                        {
                            if (oldIndexDirectory.FileExists(fileName))
                            {
                                oldIndexDirectory.DeleteFile(fileName);
                            }
                        }
                    }
                    else
                    {
                        //remove old lock file 
                        string oldLockFile = string.Format(ServiceConstants.INDEX_DIR_LOCK_FILE_FORMAT, MachineName.ToLower());
                        if (oldIndexDirectory.FileExists(oldLockFile))
                        {
                            oldIndexDirectory.DeleteFile(oldLockFile);
                        }
                    }

                    oldIndexDirectory.Dispose();
                    closedOldDirectory = true;
                }

                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, CLASS_NAME, string.Format("CloseOldSearcher() - searcher: {0}, reader: {1}, writer: {2}, directory: {3}, old path: ", closedOldSearcher, closedOldReader, closedOldWriter, closedOldDirectory, _oldIndexPath), null);
            }
            catch (Exception e)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "CloseOldSearcher()", e, null);
            }
            finally
            {
                oldIndexSearcher = null;
                oldIndexReader = null;
                oldIndexWriter = null;
                oldIndexDirectory = null;
            }           
        }

        public bool VerifySearchIndex(IndexSearcher newIndexSearcher)
        {
            bool isValid = false;
            TopDocs topDocs = null;
            try
            {
                switch (this.IProcessorType)
                {
                    case IProcessorType.KeywordIndexProcessor:
                    case IProcessorType.KeywordIndexVersion3Processor:
                        QueryBuilder kqb = Utils.GetTestKeywordQuery(_communityId, 0);
                        topDocs = Search(kqb, newIndexSearcher);
                        break;
                    case IProcessorType.MemberIndexProcessor:
                    case IProcessorType.MemberIndexVersion3Processor:
                    default:
                        QueryBuilder qb = Utils.GetTestQuery(_communityId, 0);
                        topDocs = Search(qb, newIndexSearcher);
                        break;
                }
                isValid = (null != topDocs && topDocs.ScoreDocs.Length > 0);
#if DEBUG
                if (USE_NUNIT)
                {
                    isValid = true;
                }
#endif
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, CLASS_NAME, "VerifySearchIndex", ex, null);
                isValid = false;
            }
            return isValid;
        }

        public TopDocs Search(QueryBuilder qb)
        {
            return Search(qb, _indexSearcher);
        }

        public TopDocs Search(QueryBuilder qb, IndexSearcher searcher)
        {
            TopDocs topDocs = null;
            int maxResults = (qb.MaxNumberOfMatches > 0) ? qb.MaxNumberOfMatches: Utils.GetMaxResults(_communityId, qb.UseSearchRedesign30);
            if (qb.filters.Count > 0)
            {
                ChainedFilter chainedFilter = new ChainedFilter(qb.filters.ToArray(), ChainedFilter.Logic.AND);
                Filter searchFilter = new CachingWrapperFilter(chainedFilter);
                
                if (qb.sortFields.Count > 0)
                {
                    topDocs = searcher.Search(qb.queries.Last<Query>(), searchFilter, maxResults, new Sort(qb.sortFields.ToArray<SortField>()));
                }
                else
                {
                    topDocs = searcher.Search(qb.queries.Last<Query>(), searchFilter, maxResults);
                }
            }
            else
            {
                topDocs = searcher.Search(qb.queries.Last<Query>(), maxResults);
            }

            //increment search count
            _searchCount++;
            return topDocs;
        }

        private bool IsUsingNRT(int communityID)
        {
#if DEBUG
            if (USE_NUNIT)
            {
                return NUNIT_UseNRT;
            }
#endif
            return Utils.GetSetting("USE_E2_NRT", communityID, false);
        }
    }
}
