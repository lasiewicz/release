using System;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public interface IMemberIdsAccessor
    {
        String[] MemberIds();
    }
}