using System.Collections.Generic;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Prefix;
using Matchnet.Search.Interfaces;
using Spatial4n.Core.Shapes;
using Spark.SearchEngine.ValueObjects;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class QueryBuilder
    {
        public Filter spatialFilter = null;
        public List<Query> queries = new List<Query>();
        public List<Filter> filters = new List<Filter>();
        public List<SortField> sortFields = new List<SortField>();
        private SearchType _searchType = SearchType.WebSearch;
        private bool _useSearchRedesign30 = false;
        private int maxNumberOfMatches = int.MinValue;
        public string serviceConstant = "";

        public Shape SpatialShape { get; set; }
        //distance
        public Dictionary<int, double> distances = new Dictionary<int, double>();

        //match rating calculation related
        public int hasMemberID = int.MinValue;
        public bool calculatedMatchRating = false;
        public Dictionary<int, int> matchPercentages = new Dictionary<int, int>();
        public Dictionary<int, string> matchMismatches = new Dictionary<int, string>();
        public Dictionary<string, IMatchnetQueryParameter> queryParameters = new Dictionary<string, IMatchnetQueryParameter>();
        public Dictionary<string, double> importances = new Dictionary<string, double>();
        public int communityID = int.MinValue;
        public MatchRatingUtil.MatchRatingCalculationVersion matchVersion = MatchRatingUtil.MatchRatingCalculationVersion.Version2;
        public int matchDefaultImportance = 3;
        public int matchSearcherMemberPercentage = 50;
        public int matchResultingMemberPercentage = 50;
        public List<MatchRatingAttribute> matchRatingAttributeList = null;
        public bool saveMismatchData = false;
        public bool usedFieldCacheForCalculation = false;
        public int matchMinimumProfileAttributeRequired = 0;
        public int matchMaxMissingAttributeForNoRating = 100;

        public SearchType SearchType
        {
            get { return _searchType; }
            set { _searchType = value; }
        }

        public bool UseSearchRedesign30
        {
            get { return _useSearchRedesign30; }
            set { _useSearchRedesign30 = value; }                
        }

        public int MaxNumberOfMatches
        {
            get { return maxNumberOfMatches; }
            set { maxNumberOfMatches = value; }
        }

        public void AddQuery(Query q)
        {
            queries.Add(q);
        }

        public void ClearQueries()
        {
            queries.Clear();
        }

        public void AddFilter(Filter f)
        {
            filters.Add(f);
        }

        public void AddFilterFirst(Filter f)
        {
            List<Filter> newfilters = new List<Filter>();
            newfilters.Add(f);
            foreach (Filter _f in filters)
            {
                newfilters.Add(_f);
            }
            filters = null;
            filters=newfilters;
        }

        public void AddSortField(SortField s)
        {
            sortFields.Add(s);
        }

        public void AddQueryParameter(string name, IMatchnetQueryParameter value)
        {
            queryParameters[name] = value;
        }

        public void AddImportance(string name, double value)
        {
            importances[name] = value;
        }

        public void AddMatchPercentage(int memberId, int matchPercentage)
        {
            matchPercentages[memberId] = matchPercentage;
        }

        public void AddMatchMismatch(int memberId, string attribute)
        {
            if (!matchMismatches.ContainsKey(memberId))
            {
                matchMismatches.Add(memberId, attribute);
            }
            else
            {
                matchMismatches[memberId] = matchMismatches[memberId] + ", " + attribute;
            }
        }

        public void AddDistance(int memberId, double distance)
        {
            distances[memberId] = distance;
        }
    }
}
