﻿using System.Linq;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class MemberIdsAccessor : IMemberIdsAccessor
    {
        private string[] _memberIds;

        public MemberIdsAccessor(string[] pMemberIds)
        {
            _memberIds = pMemberIds;
        }

        public string[] MemberIds()
        {
            return _memberIds;
        }

        public void AddMembers(string[] pMemberIds)
        {
            if (pMemberIds != null && pMemberIds.Length > 0)
            {
                if (_memberIds == null)
                {
                    _memberIds = pMemberIds;
                }
                else
                {
                    _memberIds = _memberIds.Union(pMemberIds).ToArray();
                }
            }
        }
    }
}
