﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Util;
using Lucene.Net.Spatial.Vector;
using Lucene.Net.Util;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class SparkSpatialDistanceSortField : SortField
    {
        private readonly Point center;
        private readonly QueryBuilder _queryBuilder;
        public SparkSpatialDistanceSortField(string field, bool reverse, QueryBuilder queryBuilder)
            : base(field, CUSTOM, reverse)
        {
            try
            {
                if (null != queryBuilder && null != queryBuilder.SpatialShape)
                {
                    _queryBuilder = queryBuilder;
                    center = queryBuilder.SpatialShape.GetCenter();
                }
            }
            catch (Exception e)
            {
                throw new ArgumentNullException("queryBuilder", e);
            }
        }

        public override FieldComparator GetComparator(int numHits, int sortPos)
        {
			return new SpatialDistanceFieldComparatorSource.SpatialDistanceFieldComparator(center, _queryBuilder, numHits);
        }

        public override FieldComparatorSource ComparatorSource
        {
            get
            {
				return new SpatialDistanceFieldComparatorSource(center, _queryBuilder);
            }
        }
    }

    public class SpatialDistanceFieldComparatorSource : FieldComparatorSource
    {
        private readonly Point center;
        private readonly QueryBuilder _queryBuilder;

		public SpatialDistanceFieldComparatorSource(Point center, QueryBuilder qb)
        {
            this.center = center;
		    this._queryBuilder = qb;
        }

        public override FieldComparator NewComparator(string fieldname, int numHits, int sortPos, bool reversed)
        {
			return new SpatialDistanceFieldComparator(center, _queryBuilder, numHits);
        }

        public class SpatialDistanceFieldComparator : FieldComparator
        {
            protected const string LATITUDE_FIELD_NAME = "latitude";
            protected const string LONGITUDE_FIELD_NAME = "longitude";
            private readonly double[] values;
            private double bottom;
            private readonly Point originPt;
            private readonly QueryBuilder _queryBuilder;            
            private IndexReader currentIndexReader;
		    private bool UseCachedDistances;
            private double[] ptX, ptY;

            private static string LatFieldName
            {
                get
                {
                    string latFieldName = (Utils.SpatialStrategy is PointVectorStrategy)
                                              ? ((PointVectorStrategy)Utils.SpatialStrategy).GetFieldNameY()
                                              : LATITUDE_FIELD_NAME;
                    return latFieldName;
                }
            }

            private static string LngFieldName
            {
                get
                {
                    string lngFieldName = (Utils.SpatialStrategy is PointVectorStrategy)
                                              ? ((PointVectorStrategy)Utils.SpatialStrategy).GetFieldNameX()
                                              : LONGITUDE_FIELD_NAME;
                    return lngFieldName;
                }
            }

		    public SpatialDistanceFieldComparator(Point origin, QueryBuilder qb, int numHits)
            {
                values = new double[numHits];
                originPt = origin;
			    this._queryBuilder = qb;
			    UseCachedDistances = Utils.GetSetting("USE_LUCENE_CACHED_DISTANCE_VALUES_SORT", true);
            }

            public override int Compare(int slot1, int slot2)
            {
                double a = values[slot1];
                double b = values[slot2];
                if (a > b)
                    return 1;
                if (a < b)
                    return -1;

                return 0;
            }

            public override void SetBottom(int slot)
            {
                bottom = values[slot];
            }

            public override int CompareBottom(int doc)
            {
                var v2 = (UseCachedDistances) ? CalculateDistanceFromCache(doc) : CalculateDistance(doc);
                if (bottom > v2)
                {
                    return 1;
                }

                if (bottom < v2)
                {
                    return -1;
                }

                return 0;
            }

            public override void Copy(int slot, int doc)
            {
                values[slot] = (UseCachedDistances) ? CalculateDistanceFromCache(doc) : CalculateDistance(doc);
            }

            private double CalculateDistance(int doc)
            {
                if (null == currentIndexReader || originPt == null) return double.NaN;
                var document = currentIndexReader.Document(doc);
                if (document == null) return double.NaN;
                double lng = NumericUtils.PrefixCodedToDouble(document.GetField(LONGITUDE_FIELD_NAME).StringValue);
                double lat = NumericUtils.PrefixCodedToDouble(document.GetField(LATITUDE_FIELD_NAME).StringValue);
                Shape shape;
                try
                {
                    shape = Utils.SpatialContext.MakePoint(lng, lat);
                }
                catch (InvalidOperationException)
                {
                    return double.NaN;
                }
                var pt = shape as Point;
                DistanceCalculator distanceCalculator = Utils.SpatialContext.GetDistCalc();
                double docDistDEG = distanceCalculator.Distance(pt, originPt);
                return DistanceUtils.Degrees2Dist(docDistDEG, DistanceUtils.EARTH_MEAN_RADIUS_MI);

            }

            private double CalculateDistanceFromCache(int doc)
            {
                double distance = double.NaN;
                if (null == _queryBuilder || null == _queryBuilder.spatialFilter || originPt == null) return distance;
                if (!(_queryBuilder.spatialFilter is SparkValueSourceFilter)) return distance;
                var filter = (SparkValueSourceFilter)_queryBuilder.spatialFilter;
                if (ptX != null && ptX.Length > doc && ptX[doc] != double.NaN)
                {
                    if (ptY != null && ptY.Length > doc && ptY[doc] != double.NaN)
                    {
                        string lng = ptX[doc].ToString();
                        string lat = ptY[doc].ToString();
                        string key = SparkValueSourceFilter.GetDistancesKey(lat, lng);

                        distance = (filter.DocIdDistances.ContainsKey(key)) ? (double)filter.DocIdDistances[key] : double.NaN;
                    }
                }
                return distance;
            }

            public override void SetNextReader(IndexReader reader, int docBase)
            {
                currentIndexReader = reader;
                if (UseCachedDistances)
                {
                    ptX = FieldCache_Fields.DEFAULT.GetDoubles(reader, LngFieldName);
                    ptY = FieldCache_Fields.DEFAULT.GetDoubles(reader, LatFieldName);                        
                }
            }

            public override IComparable this[int slot]
            {
                get { return values[slot]; }
            }
        }
    }
}
    
