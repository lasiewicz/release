﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Lucene.Net.Documents;
using Lucene.Net.Search;
using Matchnet.Exceptions;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Spark.Logging;
using Spark.SearchEngine.ValueObjects;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class MultiSearcherBL
    {
#if DEBUG
        private bool _Use_NUnit = false;
        public bool USE_NUNIT
        {
            get { return _Use_NUnit; }
            set { _Use_NUnit = value; }
        }

        private bool _NUnit_UseNRT = false;
        public bool NUNIT_UseNRT
        {
            get { return _NUnit_UseNRT; }
            set { _NUnit_UseNRT = value; }
        }

        public Dictionary<int, string> NUNIT_IndexPaths { get; set; }
#endif
        // singleton instance
        public static readonly MultiSearcherBL Instance = new MultiSearcherBL();

        // fields
        private Dictionary<int, Searcher> _searchers = new Dictionary<int, Searcher>();
        private Thread _updateThread = null;
        private Thread _gcThread = null;
        private System.Diagnostics.PerformanceCounter _availableRamCounter;
        private string _serviceConstant = null;
        private int[] _communityIds = null;
        private IProcessorType _processorType;
        private MultiSearcher _multiSearcher = null;
        private int _maxResults = 100;

        // properties
        public MultiSearcher MultiSearcher
        {
            get
            {
                if (_multiSearcher == null)
                    _multiSearcher = GetMultiSearcher();

                return _multiSearcher;
            }
        }
        public int MaxResults
        {
            get { return _maxResults; }
            set { _maxResults = value; }
        }

        // private c'tor
        private MultiSearcherBL() {}

        // main logic
        public void Start(int[] communityIds, string serviceConstant, IProcessorType processorType)
        {
            _communityIds = communityIds;
            _serviceConstant = serviceConstant;
            _processorType = processorType;

            foreach (int cId in _communityIds)
            {
                if (!_searchers.ContainsKey(cId))
                {
                    Searcher searcher = new Searcher(cId, _processorType);
#if DEBUG
                    searcher.USE_NUNIT = USE_NUNIT;
                    searcher.NUNIT_UseNRT = NUNIT_UseNRT;
                    if (null != NUNIT_IndexPaths && NUNIT_IndexPaths.ContainsKey(cId))
                    {
                        searcher.NUNIT_IndexPath = NUNIT_IndexPaths[cId];
                    }
#endif
                    searcher.Init();
                    _searchers.Add(cId, searcher);
                }
            }

#if DEBUG
            if (!USE_NUNIT)
            {
#endif
                _availableRamCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
                _updateThread = new Thread(new ThreadStart(Update));
                _updateThread.Start();
                _gcThread = new Thread(new ThreadStart(ManageMemory));
                _gcThread.Start();
#if DEBUG
            }
#endif
        }

        public void Stop()
        {
            if (null != _searchers && _searchers.Count > 0)
            {
                foreach (Searcher searcher in _searchers.Values)
                {
                    searcher.Dispose();
                }
                _searchers.Clear();
            }

            if(_multiSearcher != null)
            {
                _multiSearcher.Dispose();
            }

#if DEBUG
            if (!USE_NUNIT)
            {
#endif
                _updateThread.Abort();
                _gcThread.Abort();
#if DEBUG
            }
#endif
        }

        private void Update()
        {
            while (true)
            {
                Thread.Sleep(GetUpdateInterval());
                
                foreach (int cId in _communityIds)
                {
                    Searcher searcher = GetSearcher(cId);
                    if (null != searcher)
                    {
                        searcher.Update();
                    }
                }
            }
        }
        
        public IMatchnetQueryResults RunQuery(IMatchnetQuery query, ServiceConstants.SearchResultType resultType)
        {
            MatchnetQueryResults queryResults = null;
            TopDocs topDocs = null;

            // build query and run
            try
            {
                var queryBuilder = QueryBuilderFactory.Instance.ConvertAdminQuery(query);
                topDocs = MultiSearcher.Search(queryBuilder.queries.Last<Query>(), _maxResults);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "MultiSearcherBL", "RunQuery", ex, null);
                throw new BLException("Unable to run query.  Query: " + query.ToString().Replace("\r\n", "|"), ex);
            }

            // prepare the results
            try
            {
                if (null == topDocs || topDocs.ScoreDocs.Length == 0)
                {
                    queryResults = new MatchnetQueryResults(0);
                }
                else
                {
                    queryResults = new MatchnetQueryResults(topDocs.ScoreDocs.Length);

                    foreach (ScoreDoc t in topDocs.ScoreDocs)
                    {
                        Document doc = MultiSearcher.Doc(t.Doc);
                        int id = Int32.Parse(doc.GetField("id").StringValue);
                        var item = new MatchnetResultItem(id);
                        queryResults.AddResult(item);
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "MultiSearcherBL", "RunQuery", ex, null);
                throw new BLException("Unable to interpret the query result.  Query: " + query.ToString().Replace("\r\n", "|"), ex);
            }

            return queryResults;
        }

        // helper methods
        private MultiSearcher GetMultiSearcher()
        {
            if (_searchers == null)
                return null;

            var indexSearchers = new Searchable[_searchers.Count];
            var i = 0;
            var enumerator = _searchers.GetEnumerator();
            while (enumerator.MoveNext())
            {
                indexSearchers[i] = enumerator.Current.Value.IndexSearcher;
                i++;
            }

            var multiSearcher = new MultiSearcher(indexSearchers);
            return multiSearcher;
        }
        
        private Searcher GetSearcher(int communityid)
        {
            Searcher searcher = null;
            try
            {
                if (_searchers.ContainsKey(communityid))
                {
                    searcher = _searchers[communityid];
                }

                if (null == searcher)
                {
                    searcher = new Searcher(communityid, _processorType);
                    searcher.Init();
                    _searchers.Add(communityid, searcher);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "GetSearcher()", ex, null);
            }
            return searcher;
        }

        private int GetUpdateInterval()
        {
            return Utils.GetSetting("SEARCHER_INDEX_UPDATE_INTERVAL", 60000);
        }

        private void ManageMemory()
        {
            while (true)
            {
                Thread.Sleep(GetGCInterval());
                //manage memory
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "MultiSearcherBL", string.Format("MultiSearcherBL_MemoryManage(): Starting GC with {0} MB available memory.", getAvailableRAM().ToString()), null);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "MultiSearcherBL", string.Format("MultiSearcherBL_MemoryManage(): Finished GC with {0} MB available memory.", getAvailableRAM().ToString()), null);
            }
        }

        public float getAvailableRAM()
        {
            return _availableRamCounter.NextValue();
        }

        private int GetGCInterval()
        {
            return Utils.GetSetting("SEARCHER_GC_INTERVAL", 3600 * 1000);
        }
        
    }
}
