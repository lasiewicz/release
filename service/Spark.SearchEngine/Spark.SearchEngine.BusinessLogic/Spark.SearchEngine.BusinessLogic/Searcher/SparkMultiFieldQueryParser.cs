﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lucene.Net.Analysis;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Version = Lucene.Net.Util.Version;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class SparkMultiFieldQueryParser : MultiFieldQueryParser
    {
        private Boolean _useFuzzyQueryForSingleTerms = false;
        private int _minimumWordLength = 2;

        public bool UseFuzzyQueryForSingleTerms
        {
            get { return _useFuzzyQueryForSingleTerms; }
            set { _useFuzzyQueryForSingleTerms = value; }
        }

        public int MinimumWordLength
        {
            get { return _minimumWordLength; }
            set { _minimumWordLength = value; }
        }

        public SparkMultiFieldQueryParser(Version matchVersion, string[] fields, Analyzer analyzer) : base(matchVersion, fields, analyzer)
        {}

        protected override Lucene.Net.Search.Query GetWildcardQuery(string field, string termStr)
        {
            throw new ParseException("No wildcards allowed!!");
        }

        protected override Lucene.Net.Search.Query GetFuzzyQuery(string field, string termStr, float minSimilarity)
        {
            throw new ParseException("No fuzzy queries allowed!!");
        }

        //overriding GetFieldQuery to improve single term keyword searches - arod
        protected override Query GetFieldQuery(string field, string queryText)
        {
            Query fieldQuery = base.GetFieldQuery(field, queryText);
            //if query is null (no single terms) return default
            if (null == fieldQuery)
            {
                return fieldQuery;
            }

            //if fuzzy query setting is on, convert TermQuery to FuzzyQuery for each term
            //FuzzyQuery is slower than a TermQuery so use with caution
            HashSet<Term> hashSet = new HashSet<Term>();
            fieldQuery.ExtractTerms(hashSet);
            BooleanQuery newFieldQuery = new BooleanQuery();
            HashSet<Term>.Enumerator enumerator = hashSet.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Term t = enumerator.Current;
                string text = t.Text;                
                if (!string.IsNullOrEmpty(text) && text.Length > MinimumWordLength)
                {
                    Query query=null;
                    if (UseFuzzyQueryForSingleTerms)
                    {
                        query = new FuzzyQuery(new Term(t.Field, text));
                    }
                    else
                    {
                        query = new TermQuery(new Term(t.Field, text));
                    }
                    
                    newFieldQuery.Add(query, Occur.SHOULD);
                }
            }
            return newFieldQuery;
        }
    }
}
