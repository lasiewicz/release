﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Index;
using Lucene.Net.Spatial.Queries;
using Matchnet.Search.Interfaces;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Spark.Logging;
using Spark.SearchEngine.ValueObjects;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;
using Spark.SearchEngine.BusinessLogic.Searcher.Sorting;
using Matchnet.Search.ServiceAdapters;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class QueryBuilderFactory
    {
        private const int REGIONID_ISRAEL = 105;
        public static readonly QueryBuilderFactory Instance = new QueryBuilderFactory();

        private QueryBuilderFactory() { }

        private int GetMaxActiveDays(int communityId)
        {
            string maxActiveDaysStr = Utils.GetSetting("SEARCHER_MAX_ACTIVE_DAYS", communityId, "180");
            return Convert.ToInt32(maxActiveDaysStr);
        }

        private int GetSlopFactor(int communityId)
        {
            string slopFactorStr = Utils.GetSetting("SEARCHER_SLOP_FACTOR", communityId, "2");
            return Convert.ToInt32(slopFactorStr);
        }

        private int [] GetFilterOrder(int communityId, string serviceConstant)
        {
            int[] ints=null;
            try
            {
                string searcherFilterOrder = Utils.GetSetting("SEARCHER_FILTER_ORDER", communityId, "1,2,3,4,5,6,7");
                string [] strings = searcherFilterOrder.Split(new string[]{","}, StringSplitOptions.RemoveEmptyEntries);
                ints = strings.Select(x => int.Parse(x)).ToArray();
            }
            catch (Exception e)
            {
                RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", e, null);
                ints = new int[]{1,2,3,4,5,6,7};
            }
            return ints;
        }
        public string PreProcessKeywords(string value, int minWordLength)
        {
            StringBuilder result = new StringBuilder();
            //remove phrases
            string[] phrases = value.Split(new string[] { "\"", "\'" }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder multiTerms = new StringBuilder();
            if (phrases.Length > 1)
            {
                for (int i = 0; i < phrases.Length; i++)
                {
                    if (i == 0 || i % 2 == 0)
                    {
                        string s = phrases[i].Trim();
                        if (!string.IsNullOrEmpty(s))
                        {
                            multiTerms.Append(s + " ");
                        }
                    }
                }
            }
            else
            {
                if (!value.StartsWith("\""))
                {
                    multiTerms.Append(value);
                }
            }
      

            if (multiTerms.Length > 0)
            {
                //split out terms by punctuation
                string[] multiTerm = multiTerms.ToString().Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                //create phrase terms for multi terms, this will boost relevance if text has entire phrase
                foreach (string terms in multiTerm)
                {
                    //first split multi term by word
                    string[] term = terms.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    StringBuilder termBuilder = new StringBuilder();
                    foreach (string s in term)
                    {
                        //only use words of a certain length (greater than 2:default)
                        if (s.Trim().Length > minWordLength)
                        {
                            termBuilder.Append(s + " ");
                        }
                    }
                    //if final phrase is more than 1 word, add it as a phrase query
                    if (termBuilder.ToString().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Length > 1)
                    {
                        result.Append("\"" + termBuilder.ToString().ToLower().Trim() + "\",");
                    }
                }
            }
            //append original terms after sanitizing them to prevent Lucene parse exceptions on advanced search chars: !, ?, ~, etc
            result.Append(TestQueryBuilder.Sanitize(value));
            return result.ToString();
        }

        public QueryBuilder ConvertKeywordQuery(IMatchnetQuery query, int communityId)
        {
            QueryBuilder queryBuilder = new QueryBuilder();
            List<Sort> sorts = new List<Sort>();
            BooleanQuery boolQuery = new BooleanQuery();
            queryBuilder.AddQuery(boolQuery);
            //always get the latest active members
            int maxActiveDays = GetMaxActiveDays(communityId);
            NumericRangeFilter<long> activeDateFilter = NumericRangeFilter.NewLongRange("lastactivedate", DateTime.Now.AddDays(-maxActiveDays).Ticks, DateTime.Now.AddDays(1).Ticks, true, true);
            queryBuilder.AddFilter(activeDateFilter);
            Dictionary<string, double> locationMap = new Dictionary<string, double>();
            string areaCodeField = string.Empty;
            string[] areaCodes = null;
            int regionIDCountry = int.MinValue;

            string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
            bool onlyCreateKeywordOnce = true;
            for (int i = 0; i < query.Count; i++)
            {
                object value = query[i].Value;
                if (!string.IsNullOrEmpty(value.ToString()))
                {
                    switch (query[i].Parameter)
                    {
                        case QuerySearchParam.DomainID:
                            int domainid = Convert.ToInt32(value);
                            TermQuery cq = new TermQuery(new Term("communityid", Lucene.Net.Util.NumericUtils.IntToPrefixCoded(domainid)));
                            boolQuery.Add(cq, Occur.MUST);
                            break;
                        case QuerySearchParam.GenderMask:
                            int val = Convert.ToInt32(value);
                            TermQuery sq = new TermQuery(new Term(query[i].Parameter.ToString().ToLower(), Lucene.Net.Util.NumericUtils.IntToPrefixCoded(val)));
                            boolQuery.Add(sq, Occur.MUST);
                            break;
                        case QuerySearchParam.SearchRedesign30:
                            bool useSearch30 = (value.ToString().Equals("1"));
                            queryBuilder.UseSearchRedesign30 = useSearch30;
                            break;
                        case QuerySearchParam.MaxNumberOfMatches:
                            int maxNumOfMatches = Convert.ToInt32(value);
                            queryBuilder.MaxNumberOfMatches = maxNumOfMatches;
                            break;
                        case QuerySearchParam.KeywordSearch:
                            if (onlyCreateKeywordOnce)
                            {
                                onlyCreateKeywordOnce = false;
                                try
                                {
                                    //these text attributes are used across all communities
                                    List<string> fields = new List<string>(new string[]
                                                                               {
                                                                                   "aboutme",
                                                                                   "cuisine",
                                                                                   "music",
                                                                                   "leisureactivity",
                                                                                   "physicalactivity"
                                                                               });
                                    //different attributes are displayed on different communities so adjust the searchable text attributes accordingly
                                    if (Convert.ToInt32(ServiceConstants.COMMUNITY_ID.BBWPersonalsPlus) == communityId ||
                                        Convert.ToInt32(ServiceConstants.COMMUNITY_ID.BlackSingles) == communityId)
                                    {
                                        fields.Add("moviegenre");
                                        fields.Add("essayfirstdate");
                                        fields.Add("essaygettoknowme");
                                        fields.Add("essaypastrelationship");
                                        fields.Add("essayimportantthings");
                                        fields.Add("essayperfectday");
                                        fields.Add("essaymorethingstoadd");
                                        fields.Add("favoritebands");
                                        fields.Add("favoritemovies");
                                        fields.Add("favoritetv");
                                        fields.Add("favoriterestaurant");
                                        fields.Add("schoolsattended");
                                        fields.Add("mygreattripidea");
                                    }
                                    else
                                    {
                                        fields.Add("lifeandabmitionessay");
                                        fields.Add("historyofmylifeessay");
                                        fields.Add("onourfirstdateremindmetoessay");
                                        fields.Add("thingscantlivewithoutessay");
                                        fields.Add("favoritebooksmoviesetcessay");
                                        fields.Add("coolestplacesvisitedessay");
                                        fields.Add("forfuniliketoessay");
                                        fields.Add("onfrisatitypicallyessay");
                                        fields.Add("perfectmatchessay");
                                        fields.Add("messagemeifyouessay");
                                        fields.Add("perfectfirstdateessay");
                                        fields.Add("idealrelationshipessay");
                                        fields.Add("learnfromthepastessay");
                                        fields.Add("personalitytrait");
                                        fields.Add("entertainmentlocation");
                                        fields.Add("reading");
                                        fields.Add("pets");
                                    }

                                    //Using MultiFieldQueryParser instead of DisjuncationMaxQuery.  MultiFieldQueryParser produces more relevant matches
                                    int minWordLength = Utils.GetSetting("SEARCHER_MIN_KEYWORD_LENGTH", communityId, 2);
                                    SparkMultiFieldQueryParser multiFieldQueryParser = new SparkMultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_CURRENT, fields.ToArray(), new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT));
                                    multiFieldQueryParser.UseFuzzyQueryForSingleTerms = Utils.GetSetting("SEARCHER_USE_FUZZY_QUERY_FOR_SINGLE_TERMS", communityId, false);
                                    multiFieldQueryParser.MinimumWordLength = minWordLength;
                                    multiFieldQueryParser.PhraseSlop = GetSlopFactor(communityId);
                                    multiFieldQueryParser.DefaultOperator = QueryParser.Operator.OR;
                                    string s = value.ToString();
                                    if (Utils.GetSetting("SEARCHER_PREPROCESS_KEYWORD_TERMS", false))
                                    {
                                        s = PreProcessKeywords(value.ToString(), minWordLength);
                                    }

                                    Query query1 = multiFieldQueryParser.Parse(s);
                                    query1.Boost = 5.0f;
                                    boolQuery.Add(query1, Occur.MUST);
                                }
                                catch (Exception e)
                                {
                                    RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", e, null);
                                }
                            }
                            break;
                        case QuerySearchParam.Radius:
                            if (null != locationMap)
                            {
                                locationMap.Remove("radius");
                                locationMap.Add("radius", Convert.ToDouble(value));
                            }
                            break;
                        case QuerySearchParam.GeoDistance:
                            if (null != locationMap)
                            {
                                int listOrder = Convert.ToInt32(value);
                                string distanceAttribute = (Utils.GetSetting("USE_E2_DISTANCE", communityId, false)) ? "E2Distance" : "Distance";
                                if (Utils.GetSetting("USE_MINGLE_DISTANCE", communityId, false))
                                {
                                    distanceAttribute = "MingleDistance";
                                }

                                Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection optionCollection = Utils.GetAttributeOptionCollection(distanceAttribute, communityId);
                                int defaultRadiusValue = 160;
                                try
                                {
                                    double radius = Convert.ToDouble(optionCollection.FindByListOrder(listOrder).Value);
                                    //limiting matchmail radius for temporary memory relief
                                    if (Utils.GetSetting("ENABLE_MATCHMAIL_RADIUS_LIMIT", false) && query.SearchType == SearchType.MatchMail)
                                    {
                                        if (radius > defaultRadiusValue)
                                        {
                                            radius = defaultRadiusValue;
                                        }
                                    }
                                    locationMap.Remove("radius");
                                    locationMap.Add("radius", radius);
                                }
                                catch (Exception dex)
                                {
                                    RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", string.Format("Could not get radius from attribute/listorder={0}/{1}", distanceAttribute, listOrder), dex, null);
                                    locationMap.Remove("radius");
                                    locationMap.Add("radius", defaultRadiusValue);
                                }
                            }
                            break;
                        case QuerySearchParam.AreaCode:
                            areaCodes = value.ToString().Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (null != areaCodes && areaCodes.Length > 0)
                            {
                                locationMap = null;
                                areaCodeField = (QuerySearchParam.AreaCode == query[i].Parameter) ? "areacode" : TestQueryBuilder.PREFERREDAREACODES;
                            }
                            break;
                        case QuerySearchParam.RegionID:
                            Region region = RegionSA.Instance.RetrieveRegionByID(Convert.ToInt32(value), (int)Matchnet.Language.English);
                            if (null != locationMap && null != region)
                            {
                                string lat = region.Latitude.ToString();
                                string lon = region.Longitude.ToString();
                                double latitude = DistanceUtils.ToDegrees(Convert.ToDouble(lat));
                                double longitude = DistanceUtils.ToDegrees(Convert.ToDouble(lon));
                                if (!locationMap.ContainsKey("latitude"))
                                {
                                    locationMap.Add("latitude", latitude);
                                }
                                if (!locationMap.ContainsKey("longitude"))
                                {
                                    locationMap.Add("longitude", longitude);
                                }
                            }
                            break;
                        case QuerySearchParam.RegionIDCountry:
                            //only used for area codes for israel
                            regionIDCountry = Convert.ToInt32(value);
                            break;
                        case QuerySearchParam.Latitude:
                        case QuerySearchParam.Longitude:
                            break;
                        default:
                            //TODO: open this up for any term/value
                            break;
                    }
                }
            }
            //set the search type
            queryBuilder.SearchType = query.SearchType;
            if (null != areaCodes && areaCodes.Length > 0)
            {
                BooleanQuery areaCodeQuery = new BooleanQuery();
                foreach (string areaCode in areaCodes)
                {
                    try
                    {
                        string areaCodeString = areaCode;
                        int areaCodeValue = Convert.ToInt32(areaCodeString);

                        if (areaCodeValue != int.MinValue)
                        {
                            if (regionIDCountry == REGIONID_ISRAEL)
                            {
                                Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection optionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection("IsraelAreaCode", 10, 15, 1015);
                                if (null != optionCollection[areaCodeValue] && !string.IsNullOrEmpty(optionCollection[areaCodeValue].Description))
                                {
                                    areaCodeValue = Convert.ToInt32(optionCollection[areaCodeValue].Description);
                                }
                            }
                            areaCodeQuery.Add(new TermQuery(new Term(areaCodeField, Lucene.Net.Util.NumericUtils.IntToPrefixCoded(areaCodeValue))), Occur.SHOULD);
                        }
                    }
                    catch (Exception ex)
                    {
                        RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", "ConvertKeywordQuery()", ex, areaCode);
                    }
                }
                boolQuery.Add(areaCodeQuery, Occur.MUST);
            }

            if (null != locationMap)
            {
                //set default radius of 160 if radius is not set 
                if (!locationMap.ContainsKey("radius"))
                {
                    locationMap.Add("radius", Convert.ToDouble("160"));
                    RollingFileLogger.Instance.LogInfoMessage(serviceConstant, "QueryBuilderFactory", "ConvertKeywordQuery", string.Format("Radius value not set. Using default value (160 mi). Criteria: {0}", query.ToString().Replace("\r\n", "|")));
                }

                if (locationMap.ContainsKey(QuerySearchParam.Latitude.ToString().ToLower()) &&
                    locationMap.ContainsKey(QuerySearchParam.Longitude.ToString().ToLower()) &&
                    locationMap.ContainsKey("radius"))
                {
                    double radius = locationMap["radius"];
                    //use radius offset buffer to increase radius by a safe amount
                    radius = Utils.GetRadiusOffsetBuffer(radius);
                    Point shapeCenter = Utils.SpatialContext.MakePoint(locationMap[QuerySearchParam.Longitude.ToString().ToLower()], locationMap[QuerySearchParam.Latitude.ToString().ToLower()]);
                    Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
                    SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
                    Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);
                    queryBuilder.SpatialShape = shapeCenter;
                    queryBuilder.AddFilter(spatialFilter);
                }
                else
                {
                    RollingFileLogger.Instance.LogWarningMessage(serviceConstant, "QueryBuilderFactory", "ConvertKeywordQuery()", string.Format("Location values incomplete in query. Criteria: {0}", query.ToString().Replace("\r\n", "|")));
                }
            }

            return queryBuilder;
        }

        public QueryBuilder ConvertAdminQuery(IMatchnetQuery query)
        {
            // main bool query to add all the queries to
            var queryBuilder = new QueryBuilder();
            var mainBoolQuery = new BooleanQuery();
            queryBuilder.AddQuery(mainBoolQuery);

            IMatchnetQueryParameter singleFieldQueryParm = null;
            var singleFieldRanking = new Dictionary<string, int>()
                                         {
                                             {"memberid", 1},
                                             {"emailaddress", 2},
                                             {"username", 3},
                                             {"ipaddress", 4}
                                         };
            var locationMap = new Dictionary<string, double>();
            
            // memberid, emailaddress, username, ipaddress are single field search only
            // if any one of these is specified, we are searching by that value only
            for (int i = 0; i < query.Count; i++)
            {
                object value = query[i].Value;
                if(string.IsNullOrEmpty(value.ToString()))
                    continue;

                switch (query[i].Parameter)
                {
                    case QuerySearchParam.MemberID:
                    case QuerySearchParam.EmailAddress:
                    case QuerySearchParam.Username:
                    case QuerySearchParam.IPAddress:
                        if (singleFieldQueryParm == null)
                        {
                            singleFieldQueryParm = query[i];
                        }
                        else
                        {
                            var existingFieldName =
                                Enum.GetName(typeof (QuerySearchParam), singleFieldQueryParm.Parameter).ToLower();
                            var newFieldName = 
                                Enum.GetName(typeof (QuerySearchParam), query[i].Parameter).ToLower();

                            if(singleFieldRanking[newFieldName] < singleFieldRanking[existingFieldName])
                            {
                                singleFieldQueryParm = query[i];
                            }

                        }
                        break;
                    case QuerySearchParam.RegionID:
                        Region region = RegionSA.Instance.RetrieveRegionByID(Convert.ToInt32(value), (int)Matchnet.Language.English);
                        if (null != region)
                        {
                            string lat = region.Latitude.ToString();
                            string lon = region.Longitude.ToString();
                            double latitude = DistanceUtils.ToDegrees(Convert.ToDouble(lat));
                            double longitude = DistanceUtils.ToDegrees(Convert.ToDouble(lon));
                            if (!locationMap.ContainsKey("latitude"))
                            {
                                locationMap.Add("latitude", latitude);
                            }
                            if (!locationMap.ContainsKey("longitude"))
                            {
                                locationMap.Add("longitude", longitude);
                            }
                        }                           
                        break;
                    default:
                        mainBoolQuery.Add(BuildLuceneQueryForAdminSearch(query[i]), Occur.MUST);
                        break;
                }

            }

            // if this is a single field query, we need to remove all other queriess
            if (singleFieldQueryParm != null)
            {
                queryBuilder.ClearQueries();
                mainBoolQuery = new BooleanQuery();
                queryBuilder.AddQuery(mainBoolQuery);
                mainBoolQuery.Add(BuildLuceneQueryForAdminSearch(singleFieldQueryParm), Occur.MUST);
            }

            // add the location filter here
            if (locationMap.ContainsKey(QuerySearchParam.Latitude.ToString().ToLower()) &&
                    locationMap.ContainsKey(QuerySearchParam.Longitude.ToString().ToLower()))
            {
                // hardcode this for now: not sure if we should make this setting based or user input base
                double radius = 10;
                //use radius offset buffer to increase radius by a safe amount
                radius = Utils.GetRadiusOffsetBuffer(radius);
                Point shapeCenter = Utils.SpatialContext.MakePoint(locationMap[QuerySearchParam.Longitude.ToString().ToLower()], locationMap[QuerySearchParam.Latitude.ToString().ToLower()]);
                Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
                SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
                Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);
                queryBuilder.SpatialShape = shapeCenter;
                queryBuilder.AddFilter(spatialFilter);
            }

            return queryBuilder;
        }

        private Query BuildLuceneQueryForAdminSearch(IMatchnetQueryParameter queryParameter)
        {
            Query luceneQuery = null;
            var fld = Enum.GetName(typeof (QuerySearchParam), queryParameter.Parameter).ToLower();

            switch(queryParameter.Parameter)
            {
                case QuerySearchParam.MemberID:
                    // term query
                    var val =Convert.ToInt32(queryParameter.Value);
                    luceneQuery = new TermQuery(new Term(fld, Lucene.Net.Util.NumericUtils.IntToPrefixCoded(val)));
                    break;
                case QuerySearchParam.IPAddress:
                    // term query
                    luceneQuery = new TermQuery(new Term(fld, queryParameter.Value.ToString()));
                    break;
               default:
                    // wild card
                    var wildCardStr = string.Format("*{0}*", queryParameter.Value.ToString().ToLower());
                    luceneQuery = new WildcardQuery(new Term(fld, wildCardStr));
                    break;

            }

            return luceneQuery;
        }

        public QueryBuilder ConvertQuery(IMatchnetQuery query, int communityId)
        {
            return ConvertQuery(query, communityId, null);
        }

        public QueryBuilder ConvertQuery(IMatchnetQuery query, int communityId, IMemberIdsAccessor memberIdsAccessor)
        {
            QueryBuilder queryBuilder = new QueryBuilder();

            List<Sort> sorts = new List<Sort>();
            BooleanQuery boolQuery = new BooleanQuery();
            queryBuilder.AddQuery(boolQuery);
            //always get the latest active members
            int maxActiveDays = GetMaxActiveDays(communityId);
            Dictionary<int, Filter> filters = new Dictionary<int, Filter>();
            NumericRangeFilter<long> activeDateFilter = NumericRangeFilter.NewLongRange("lastactivedate", DateTime.Now.AddDays(-maxActiveDays).Ticks, DateTime.Now.AddDays(1).Ticks, true, true);
            filters[(int)SearchFilter.LastActiveDateFilter]=activeDateFilter;

            Dictionary<string, long> ageRangeQuery = new Dictionary<string, long>();
            Dictionary<string, long> registrationDateRangeQuery = new Dictionary<string, long>();
            Dictionary<string, long> subscriptionExpirationDateRangeQuery = new Dictionary<string, long>();
            Dictionary<string, long> onlineDateRangeQuery = new Dictionary<string, long>();
            Dictionary<string, int> heightRangeQuery = new Dictionary<string, int>();
            Dictionary<string, int> weightRangeQuery = new Dictionary<string, int>();
            Dictionary<string, double> locationMap = new Dictionary<string, double>();
            string areaCodeField = string.Empty;
            string[] areaCodes = null;
            int regionIDCountry = int.MinValue;

            QuerySorting resultSort = QuerySorting.LastLogonDate;
            string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
            bool isMatchCalculationEnabled = MatchRatingUtil.IsMatchRatingCalculationEnabled(communityId, query.SearchType);

            for (int i = 0; i < query.Count; i++)
            {
                object value = query[i].Value;
                if (!string.IsNullOrEmpty(value.ToString()))
                {
                    switch (query[i].Parameter)
                    {
                        case QuerySearchParam.Sorting:
                            resultSort = (QuerySorting)Enum.Parse(typeof(QuerySorting), value.ToString());
                            if (resultSort == QuerySorting.ColorCode)
                            {
                                resultSort = QuerySorting.Proximity;
                            }

                            if (resultSort == QuerySorting.KeywordRelevance && null == memberIdsAccessor)
                            {
                                resultSort = QuerySorting.JoinDate;
                            }
                            break;
                        case QuerySearchParam.DomainID:
                            int domainid = Convert.ToInt32(value);
                            TermQuery cq = new TermQuery(new Term("communityid", Lucene.Net.Util.NumericUtils.IntToPrefixCoded(domainid)));
                            boolQuery.Add(cq, Occur.MUST);
                            break;
                        case QuerySearchParam.SchoolID:
                        case QuerySearchParam.GenderMask:
                        case QuerySearchParam.PreferredGenderMask:
                        case QuerySearchParam.HasPhotoFlag:
                        case QuerySearchParam.PreferredHasPhotoFlag:
                        case QuerySearchParam.RamahAlum:
                            int val = Convert.ToInt32(value);
                            if (val > 0)
                            {
                                TermQuery sq = new TermQuery(new Term(query[i].Parameter.ToString().ToLower(), Lucene.Net.Util.NumericUtils.IntToPrefixCoded(val)));
                                boolQuery.Add(sq, Occur.MUST);
                            }
                            break;
                        case QuerySearchParam.AgeMin:
                        case QuerySearchParam.AgeMax:
                            if (!ageRangeQuery.ContainsKey(query[i].Parameter.ToString().ToLower()))
                            {
                                int ageValue = Convert.ToInt32(value);
                                if (ageValue > 0)
                                {
                                    DateTime ageDateTime = DateTime.Now.AddYears(-ageValue);
                                    ageRangeQuery.Add(query[i].Parameter.ToString().ToLower(), ageDateTime.Ticks);
                                }
                            }
                            break;
                        case QuerySearchParam.RegistrationDateMin:
                        case QuerySearchParam.RegistrationDateMax:
                            if (!registrationDateRangeQuery.ContainsKey(query[i].Parameter.ToString().ToLower()))
                            {
                                DateTime regDate = Convert.ToDateTime(value);
                                registrationDateRangeQuery.Add(query[i].Parameter.ToString().ToLower(), regDate.Ticks);
                            }
                            break;
                        case QuerySearchParam.SubscriptionExpirationDateMin:
                        case QuerySearchParam.SubscriptionExpirationDateMax:
                            if (!subscriptionExpirationDateRangeQuery.ContainsKey(query[i].Parameter.ToString().ToLower()))
                            {
                                DateTime subExpirationDate = Convert.ToDateTime(value);
                                subscriptionExpirationDateRangeQuery.Add(query[i].Parameter.ToString().ToLower(), subExpirationDate.Ticks);
                            }
                            break;
                        case QuerySearchParam.HeightMin:
                        case QuerySearchParam.HeightMax:
                            if (!heightRangeQuery.ContainsKey(query[i].Parameter.ToString().ToLower()))
                            {
                                int height = Convert.ToInt32(value);
                                if (height > 0)
                                {
                                    heightRangeQuery.Add(query[i].Parameter.ToString().ToLower(), height);
                                }
                            }
                            break;
                        case QuerySearchParam.WeightMin:
                        case QuerySearchParam.WeightMax:
                            if (!weightRangeQuery.ContainsKey(query[i].Parameter.ToString().ToLower()))
                            {
                                int weight = Convert.ToInt32(value);
                                if (weight > 0)
                                {
                                    weightRangeQuery.Add(query[i].Parameter.ToString().ToLower(), weight);
                                }
                            }
                            break;
                        case QuerySearchParam.PreferredAge:
                            int ageForPrefs = Convert.ToInt32(value);
                            if (ageForPrefs > 0)
                            {
                                NumericRangeFilter<int> preferredMinAgeFilter = NumericRangeFilter.NewIntRange("preferredminage", 18, ageForPrefs + 1, true, false);
                                NumericRangeFilter<int> preferredMaxAgeFilter = NumericRangeFilter.NewIntRange("preferredmaxage", ageForPrefs, 90, true, false);
                                queryBuilder.AddFilter(preferredMinAgeFilter);
                                queryBuilder.AddFilter(preferredMaxAgeFilter);
                            }
                            break;
                        case QuerySearchParam.PreferredHeight:
                            int heightForPrefs = Convert.ToInt32(value);
                            if (heightForPrefs > 0)
                            {
                                NumericRangeFilter<int> preferredMinHeightFilter = NumericRangeFilter.NewIntRange("preferredminheight", 136, heightForPrefs + 1, true, false);
                                NumericRangeFilter<int> preferredMaxHeightFilter = NumericRangeFilter.NewIntRange("preferredmaxheight", heightForPrefs, 245, true, false);
                                queryBuilder.AddFilter(preferredMinHeightFilter);
                                queryBuilder.AddFilter(preferredMaxHeightFilter);
                            }
                            break;
                        case QuerySearchParam.PreferredDistance:
                        case QuerySearchParam.Radius:
                            if (null != locationMap)
                            {
                                locationMap.Remove("radius");
                                locationMap.Add("radius", Convert.ToDouble(value));
                            }
                            break;
                        case QuerySearchParam.GeoDistance:
                            if (null != locationMap)
                            {
                                int listOrder = Convert.ToInt32(value);
                                string distanceAttribute = (Utils.GetSetting("USE_E2_DISTANCE", communityId, false)) ? "E2Distance" : "Distance";
                                if (Utils.GetSetting("USE_MINGLE_DISTANCE", communityId, false))
                                {
                                    distanceAttribute = "MingleDistance";
                                }

                                Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection optionCollection = Utils.GetAttributeOptionCollection(distanceAttribute, communityId);
                                int defaultRadiusValue = 160;
                                try
                                {                                                                        
                                    double radius = Convert.ToDouble(optionCollection.FindByListOrder(listOrder).Value);
                                    //limiting matchmail radius for temporary memory relief
                                    if (Utils.GetSetting("ENABLE_MATCHMAIL_RADIUS_LIMIT", false) && query.SearchType == SearchType.MatchMail)
                                    {
                                        if (radius > defaultRadiusValue)
                                        {
                                            radius = defaultRadiusValue;
                                        }
                                    }
                                    locationMap.Remove("radius");
                                    locationMap.Add("radius", radius);
                                }
                                catch (Exception dex)
                                {
                                    RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", string.Format("Could not get radius from attribute/listorder={0}/{1}", distanceAttribute, listOrder), dex, null);
                                    locationMap.Remove("radius");
                                    locationMap.Add("radius", defaultRadiusValue);                                    
                                }
                            }
                            break;
                        case QuerySearchParam.PreferredAreaCodes:
                        case QuerySearchParam.AreaCode:
                            areaCodes = value.ToString().Trim().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (null != areaCodes && areaCodes.Length > 0)
                            {
                                locationMap = null;
                                areaCodeField = (QuerySearchParam.AreaCode == query[i].Parameter)
                                                    ? "areacode"
                                                    : TestQueryBuilder.PREFERREDAREACODES;
                            }
                            break;
                        case QuerySearchParam.PreferredRegionId:
                        case QuerySearchParam.RegionID:
                            Region region = RegionSA.Instance.RetrieveRegionByID(Convert.ToInt32(value), (int)Matchnet.Language.English);
                            if (null != locationMap && null != region)
                            {
                                string lat = region.Latitude.ToString();
                                string lon = region.Longitude.ToString();
                                double latitude = DistanceUtils.ToDegrees(Convert.ToDouble(lat));
                                double longitude = DistanceUtils.ToDegrees(Convert.ToDouble(lon));
                                if (!locationMap.ContainsKey("latitude"))
                                {
                                    locationMap.Add("latitude", latitude);
                                }
                                if (!locationMap.ContainsKey("longitude"))
                                {
                                    locationMap.Add("longitude", longitude);
                                }
                            }                           
                            break;
                        case QuerySearchParam.RegionIDCountry:
                            //only used for area codes for israel
                            regionIDCountry = Convert.ToInt32(value);
                            break;
                        case QuerySearchParam.Latitude:
                        case QuerySearchParam.Longitude:
                            break;
                        case QuerySearchParam.PreferredLanguageMask:
                        case QuerySearchParam.PreferredRelationshipMask:
                        case QuerySearchParam.PreferredReligion:
                        case QuerySearchParam.PreferredRelationshipStatus:
                        case QuerySearchParam.PreferredMaritalStatus:
                        case QuerySearchParam.PreferredBodyType:
                        case QuerySearchParam.PreferredSmokingHabit:
                        case QuerySearchParam.PreferredDrinkingHabit:
                        case QuerySearchParam.PreferredEducationLevel:
                        case QuerySearchParam.PreferredEthnicity:
                        case QuerySearchParam.PreferredJdateEthnicity:
                        case QuerySearchParam.PreferredJdateReligion:
                        case QuerySearchParam.PreferredSynagogueAttendance:
                        case QuerySearchParam.PreferredKosherStatus:
                        case QuerySearchParam.PreferredActivityLevel:
                        case QuerySearchParam.PreferredRelocation:
                        case QuerySearchParam.PreferredCustody:
                        case QuerySearchParam.LanguageMask:
                        case QuerySearchParam.RelationshipMask:
                        case QuerySearchParam.Religion:
                        case QuerySearchParam.RelationshipStatus:
                        case QuerySearchParam.MaritalStatus:
                        case QuerySearchParam.BodyType:
                        case QuerySearchParam.SmokingHabits:
                        case QuerySearchParam.DrinkingHabits:
                        case QuerySearchParam.EducationLevel:
                        case QuerySearchParam.ColorCode:
                        case QuerySearchParam.Ethnicity:
                        case QuerySearchParam.JDateEthnicity:
                        case QuerySearchParam.JDateReligion:
                        case QuerySearchParam.SynagogueAttendance:
                        case QuerySearchParam.KeepKosher:
                        case QuerySearchParam.ActivityLevel:
                        case QuerySearchParam.RelocateFlag:
                        case QuerySearchParam.Custody:
                            int intValue = Convert.ToInt32(value);
                            BooleanQuery maskQuery = GetMaskQuery(query[i].Parameter.ToString().ToLower(), intValue, communityId);
                            if (null != maskQuery)
                            {
                                boolQuery.Add(maskQuery, Occur.MUST);
                            }
                            break;
                        case QuerySearchParam.PreferredChildrenCount:
                        case QuerySearchParam.ChildrenCount:
                            int ccValue = Convert.ToInt32(value);
                            BooleanQuery ccMaskQuery = GetChildrenCountQuery(query[i].Parameter.ToString().ToLower(), ccValue, communityId);
                            if (null != ccMaskQuery)
                            {
                                boolQuery.Add(ccMaskQuery, Occur.MUST);
                            }
                            break;
                        case QuerySearchParam.PreferredMoreChildrenFlag:
                            int pmcValue = Convert.ToInt32(value);
                            BooleanQuery pmcMaskQuery = GetPreferredMoreChildrenFlagQuery(query[i].Parameter.ToString().ToLower(), pmcValue, communityId);
                            if (null != pmcMaskQuery)
                            {
                                boolQuery.Add(pmcMaskQuery, Occur.MUST);
                            }
                            break;
                        case QuerySearchParam.MoreChildrenFlag:
                            int mcValue = Convert.ToInt32(value);
                            BooleanQuery mcMaskQuery = GetMoreChildrenFlagQuery(query[i].Parameter.ToString().ToLower(), mcValue, communityId);
                            if (null != mcMaskQuery)
                            {
                                boolQuery.Add(mcMaskQuery, Occur.MUST);
                            }
                            break;
                        case QuerySearchParam.SearchRedesign30:
                            bool useSearch30 = (value.ToString().Equals("1"));
                            queryBuilder.UseSearchRedesign30 = useSearch30;
                            break;
                        case QuerySearchParam.MaxNumberOfMatches:
                            int maxNumOfMatches = Convert.ToInt32(value);
                            queryBuilder.MaxNumberOfMatches = maxNumOfMatches;
                            break;
                        case QuerySearchParam.Online:
                            int valOnline = Convert.ToInt32(value);
                            if (valOnline >= 0)
                            {
                                TermQuery sqOnline = new TermQuery(new Term(query[i].Parameter.ToString().ToLower(), Lucene.Net.Util.NumericUtils.IntToPrefixCoded(valOnline)));
                                boolQuery.Add(sqOnline, Occur.MUST);

                                if (!onlineDateRangeQuery.ContainsKey(query[i].Parameter.ToString().ToLower()))
                                {
                                    DateTime onlineLastUpdateMinDate = DateTime.Now.AddMinutes(-30);
                                    DateTime onlineLastUpdateMaxDate = DateTime.Now.AddDays(1);
                                    onlineDateRangeQuery["onlineLastUpdateMin"] = onlineLastUpdateMinDate.Ticks;
                                    onlineDateRangeQuery["onlineLastUpdateMax"] = onlineLastUpdateMaxDate.Ticks;
                                }
                            }
                            break;
                        default:
                            //TODO: open this up for any term/value
                            break;
                    }

                    if (isMatchCalculationEnabled)
                    {
                        if (queryBuilder.queryParameters.ContainsKey(query[i].Parameter.ToString().ToLower()))
                        {
                            RollingFileLogger.Instance.LogInfoMessage(serviceConstant, "QueryBuilderFactory", "ConvertQuery", string.Format("Query Parameter duplicate: {0} - {1}", query[i].Parameter.ToString().ToLower(), query[i]));
                        }

                        //add searcher query data for match rating support
                        queryBuilder.AddQueryParameter(query[i].Parameter.ToString().ToLower(), query[i]);
                    }
                }
            }

            //set the search type
            queryBuilder.SearchType = query.SearchType;
            if (heightRangeQuery.Count > 0)
            {
                if (!heightRangeQuery.ContainsKey(QuerySearchParam.HeightMin.ToString().ToLower())) { heightRangeQuery.Add(QuerySearchParam.HeightMin.ToString().ToLower(), 0); }
                if (!heightRangeQuery.ContainsKey(QuerySearchParam.HeightMax.ToString().ToLower())) { heightRangeQuery.Add(QuerySearchParam.HeightMax.ToString().ToLower(), 250); }
                int maxHeight = heightRangeQuery[QuerySearchParam.HeightMax.ToString().ToLower()] + 1;
                Query heightQuery = NumericRangeQuery.NewIntRange("height", heightRangeQuery[QuerySearchParam.HeightMin.ToString().ToLower()], maxHeight, true, false);
                boolQuery.Add(heightQuery, Occur.MUST);
            }

            if (weightRangeQuery.Count > 0)
            {
                if (!weightRangeQuery.ContainsKey(QuerySearchParam.WeightMin.ToString().ToLower())) { weightRangeQuery.Add(QuerySearchParam.WeightMin.ToString().ToLower(), 0); }
                if (!weightRangeQuery.ContainsKey(QuerySearchParam.WeightMax.ToString().ToLower())) { weightRangeQuery.Add(QuerySearchParam.WeightMin.ToString().ToLower(), 2500); }
                int maxWeight = weightRangeQuery[QuerySearchParam.WeightMax.ToString().ToLower()] + 1;
                Query weightQuery = NumericRangeQuery.NewIntRange("weight", weightRangeQuery[QuerySearchParam.WeightMin.ToString().ToLower()], maxWeight, true, false);
                boolQuery.Add(weightQuery, Occur.MUST);
            }

            if (ageRangeQuery.Count > 0)
            {
                if (ageRangeQuery.ContainsKey(QuerySearchParam.AgeMax.ToString().ToLower()) && ageRangeQuery.ContainsKey(QuerySearchParam.AgeMin.ToString().ToLower()))
                {
                    DateTime maxAge = new DateTime(ageRangeQuery[QuerySearchParam.AgeMax.ToString().ToLower()]).AddYears(-1);
                    NumericRangeFilter<long> ageFilter = NumericRangeFilter.NewLongRange("birthdate", maxAge.Ticks, ageRangeQuery[QuerySearchParam.AgeMin.ToString().ToLower()], false, true);
                    filters[(int)SearchFilter.AgeRangeFilter]=ageFilter;
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(serviceConstant, "QueryBuilderFactory", "ConvertQuery", string.Format("AgeRange incomplete in query. Criteria: {0}", query.ToString().Replace("\r\n", "|")));
                }
            }

            if (registrationDateRangeQuery.Count > 0)
            {
                if (registrationDateRangeQuery.ContainsKey(QuerySearchParam.RegistrationDateMin.ToString().ToLower()) && registrationDateRangeQuery.ContainsKey(QuerySearchParam.RegistrationDateMax.ToString().ToLower()))
                {
                    NumericRangeFilter<long> regDateFilter = NumericRangeFilter.NewLongRange("communityinsertdate", registrationDateRangeQuery[QuerySearchParam.RegistrationDateMin.ToString().ToLower()], registrationDateRangeQuery[QuerySearchParam.RegistrationDateMax.ToString().ToLower()], false, true);
                    filters[(int)SearchFilter.JoinDateFilter]=regDateFilter;
                }
            }

            if (subscriptionExpirationDateRangeQuery.Count > 0)
            {
                if (subscriptionExpirationDateRangeQuery.ContainsKey(QuerySearchParam.SubscriptionExpirationDateMin.ToString().ToLower()) && subscriptionExpirationDateRangeQuery.ContainsKey(QuerySearchParam.SubscriptionExpirationDateMax.ToString().ToLower()))
                {
                    NumericRangeFilter<long> subExpirationDateFilter = NumericRangeFilter.NewLongRange("subscriptionexpirationdate", subscriptionExpirationDateRangeQuery[QuerySearchParam.SubscriptionExpirationDateMin.ToString().ToLower()], subscriptionExpirationDateRangeQuery[QuerySearchParam.SubscriptionExpirationDateMax.ToString().ToLower()], false, true);
                    filters[(int)SearchFilter.SubscriberFilter]=subExpirationDateFilter;
                }
            }

            if (onlineDateRangeQuery.Count > 0)
            {
                NumericRangeFilter<long> onlineLastUpdateFilter = NumericRangeFilter.NewLongRange("updatedate", onlineDateRangeQuery["onlineLastUpdateMin"], onlineDateRangeQuery["onlineLastUpdateMax"], true, true);
                filters[(int)SearchFilter.OnlineDateFilter] = onlineLastUpdateFilter;
            }

            if (null != areaCodes && areaCodes.Length > 0)
            {
                BooleanQuery areaCodeQuery = new BooleanQuery();
                foreach (string areaCode in areaCodes)
                {
                    try
                    {
                        string areaCodeString = areaCode;
                        int areaCodeValue = Convert.ToInt32(areaCodeString);

                        if (areaCodeValue != int.MinValue)
                        {
                            if (regionIDCountry == REGIONID_ISRAEL)
                            {
                                Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection optionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection("IsraelAreaCode", 10, 15, 1015);
                                if (null != optionCollection[areaCodeValue] && !string.IsNullOrEmpty(optionCollection[areaCodeValue].Description))
                                {
                                    areaCodeValue = Convert.ToInt32(optionCollection[areaCodeValue].Description);
                                }
                            }
                            areaCodeQuery.Add(new TermQuery(new Term(areaCodeField, Lucene.Net.Util.NumericUtils.IntToPrefixCoded(areaCodeValue))), Occur.SHOULD);
                        }
                    }
                    catch (Exception ex)
                    {
                        RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", "ConvertQuery()", ex, areaCode);
                    }
                }
                boolQuery.Add(areaCodeQuery, Occur.MUST);
            }

            if (null != locationMap)
            {
                //set default radius of 160 if radius is not set 
                if (!locationMap.ContainsKey("radius"))
                {
                    locationMap.Add("radius", Convert.ToDouble("160"));
                    RollingFileLogger.Instance.LogInfoMessage(serviceConstant, "QueryBuilderFactory", "ConvertQuery", string.Format("Radius value not set. Using default value (160 mi). Criteria: {0}", query.ToString().Replace("\r\n", "|")));
                }
           
                if (locationMap.ContainsKey(QuerySearchParam.Latitude.ToString().ToLower()) &&
                    locationMap.ContainsKey(QuerySearchParam.Longitude.ToString().ToLower()) &&
                    locationMap.ContainsKey("radius"))
                {
                    double radius = locationMap["radius"];
                    //use radius offset buffer to increase radius by a safe amount
                    radius = Utils.GetRadiusOffsetBuffer(radius);
                    Point shapeCenter = Utils.SpatialContext.MakePoint(locationMap[QuerySearchParam.Longitude.ToString().ToLower()], locationMap[QuerySearchParam.Latitude.ToString().ToLower()]);
                    Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
                    SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
                    Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);
                    queryBuilder.SpatialShape = shapeCenter;
                    filters[(int)SearchFilter.ProximityFilter]=spatialFilter;
                    queryBuilder.spatialFilter = spatialFilter;
                }
                else
                {
                    RollingFileLogger.Instance.LogWarningMessage(serviceConstant, "QueryBuilderFactory", "ConvertQuery()", string.Format("Location values incomplete in query. Criteria: {0}", query.ToString().Replace("\r\n", "|")));
                }
            }

            //add keyword filter
            if (null != memberIdsAccessor)
            {
                filters[(int)SearchFilter.KeywordFilter]=new KeywordFilter(memberIdsAccessor);
            }

            //verify searcher has valid memberID (i.e. not visitor) for match rating to be enabled as sorting
            bool addedMatchSort = false;
            if (isMatchCalculationEnabled)
            {
                if (!queryBuilder.queryParameters.ContainsKey(QuerySearchParam.HasMemberID.ToString().ToLower()))
                {
                    //disable match calculation for visitors
                    isMatchCalculationEnabled = false;
                }
                else
                {
                    queryBuilder.hasMemberID =
                        int.Parse(
                            queryBuilder.queryParameters[QuerySearchParam.HasMemberID.ToString().ToLower()].Value.ToString());
                    if (queryBuilder.hasMemberID <= 0)
                    {
                        //disable match calculation for visitors
                        isMatchCalculationEnabled = false;
                    }
                }
            }

            //add sort
            switch (resultSort)
            {
                case QuerySorting.Proximity:
                    SortField sparkSpatialDistanceSortField = new SparkSpatialDistanceSortField("geoField", false, queryBuilder);
                    queryBuilder.AddSortField(sparkSpatialDistanceSortField);
                    break;
                case QuerySorting.JoinDate:
                    queryBuilder.AddSortField(new SortField("communityinsertdate", SortField.LONG, true));
                    break;
                case QuerySorting.Popularity:  //email count
                    queryBuilder.AddSortField(new SortField("emailcount", SortField.INT, true));
                    break;
                case QuerySorting.KeywordRelevance:
                    if(null != memberIdsAccessor)
                    {
                        SortField keywordRelevanceSort = new KeywordRelavanceSortField("keyword", false, memberIdsAccessor);
                        queryBuilder.AddSortField(keywordRelevanceSort); //field is unused for sort
                    }
                    break;                
                case QuerySorting.LastLogonDate:
                    break;
                case QuerySorting.MutualMatch:
                    if (isMatchCalculationEnabled)
                    {
                        SparkMatchPercentageSortField matchPercentageSortField = new SparkMatchPercentageSortField("matchPercentField", true, queryBuilder);
                        queryBuilder.AddSortField(matchPercentageSortField);
                        addedMatchSort = true;
                    }
                    break;
                default:
                    break;
            }

            if (queryBuilder.SearchType != SearchType.MatchMail)
            {
                //always sort by last active date
                queryBuilder.AddSortField(new SortField("lastactivedate", SortField.LONG, true));                
            }

            queryBuilder.communityID = communityId;
            queryBuilder.serviceConstant = serviceConstant;
            if (isMatchCalculationEnabled)
            {
                if (!addedMatchSort)
                {
                    //always calculate match if enabled
                    SparkMatchPercentageSortField matchPercentageSortField = new SparkMatchPercentageSortField("matchPercentField", true, queryBuilder);
                    queryBuilder.AddSortField(matchPercentageSortField);
                }

                //add various settings to support match rating calculation
                queryBuilder.matchSearcherMemberPercentage = MatchRatingUtil.GetMatchRatingSearchMemberPercentage(communityId);
                queryBuilder.matchResultingMemberPercentage = 100 - queryBuilder.matchSearcherMemberPercentage;
                queryBuilder.matchVersion = MatchRatingUtil.GetMatchRatingFormulaVersion(communityId);
                queryBuilder.matchDefaultImportance = MatchRatingUtil.GetMatchRatingDefaultImportanceLevel(communityId);
                queryBuilder.matchRatingAttributeList = MatchRatingUtil.GetMatchRatingAttributeListE2(communityId);
                queryBuilder.matchMinimumProfileAttributeRequired = MatchRatingUtil.GetMatchRatingMinimumAttributesRequired(communityId);
                queryBuilder.matchMaxMissingAttributeForNoRating = queryBuilder.matchRatingAttributeList.Count - queryBuilder.matchMinimumProfileAttributeRequired;
                queryBuilder.saveMismatchData = Utils.IsMatchMismatchDataEnabled(communityId);
                queryBuilder.calculatedMatchRating = true;
            }

            //add filters in order represented by setting
            int[] filterOrder = GetFilterOrder(communityId, serviceConstant);
            foreach (int idx in filterOrder)
            {
                if (filters.ContainsKey(idx))
                {
                    queryBuilder.AddFilter(filters[idx]);
                }
            }

            return queryBuilder;
        }

        public BooleanQuery GetMaskQuery(string fieldName, int maskValue, int communityId)
        {
            bool isIndexedAsMasked = IsIndexedAsMasked(fieldName);
            string attributeName = fieldName.Replace("preferred", ""); //remove the preferred word from the attribute name
            if (TestQueryBuilder.PREFERREDSMOKINGHABIT.Equals(fieldName) || TestQueryBuilder.PREFERREDDRINKINGHABIT.Equals(fieldName))
            {
                if (!attributeName.EndsWith("s"))
                {
                    //add an s at the end
                    attributeName += "s";
                }
            } else if (TestQueryBuilder.PREFERREDKOSHERSTATUS.Equals(fieldName))
            {
                attributeName = "keepkosher";
            } else if (TestQueryBuilder.PREFERREDRELOCATION.Equals(fieldName))
            {
                attributeName = "relocateflag";
            }

            Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection options = Utils.GetIntValuesFromMask(attributeName, communityId);
            BooleanQuery boolQuery = null;
            if (null != options)
            {
                foreach (Matchnet.Content.ValueObjects.AttributeOption.AttributeOption option in options)
                {
                    if (option.Value == int.MinValue)
                    {
                        continue;
                    }

                    //date and mask are longs
                    //location is double
                    if ((option.Value & maskValue) == option.Value)
                    {
                        Query q = null;
                        if (isIndexedAsMasked)
                        {
                            q = new TermQuery(new Term(fieldName.ToLower(), Lucene.Net.Util.NumericUtils.LongToPrefixCoded(option.Value)));
                        }
                        else
                        {
                            q = new TermQuery(new Term(fieldName.ToLower(), Lucene.Net.Util.NumericUtils.IntToPrefixCoded(option.Value)));
                        }

                        if (null == boolQuery) boolQuery = new BooleanQuery();
                        boolQuery.Add(q, Occur.SHOULD);
                    }
                }
            }
            else
            {
                string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
                RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", "GetMaskQuery", new Exception(string.Format("Cannot find attribute options for {0} in community:{1}", attributeName, communityId)), null);
            }

            return boolQuery;
        }

        private static bool IsIndexedAsMasked(string fieldName)
        {
            return fieldName.Contains("preferred") || 
                   TestQueryBuilder.LANGUAGEMASK.Equals(fieldName) || 
                   TestQueryBuilder.RELATIONSHIPMASK.Equals(fieldName);
        }

        public BooleanQuery GetChildrenCountQuery(string fieldName, int maskValue, int communityId)
        {
            bool isIndexedAsMasked = IsIndexedAsMasked(fieldName);
            string attributeName = fieldName.Replace("preferred", ""); //remove the preferred word from the attribute name
            Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection options = Utils.GetIntValuesFromMask(attributeName, communityId);
            BooleanQuery boolQuery = null;

            Dictionary<int, int> childrenCountTable = Utils.GetChildrenCountConversionTable();
            if (null != options)
            {
                foreach (Matchnet.Content.ValueObjects.AttributeOption.AttributeOption option in options)
                {
                    if (option.Value >= 0)
                    {
                        //convert db value to hardcoded web tier value
                        if (childrenCountTable.ContainsKey(option.Value))
                        {
                            int optionMaskValue = childrenCountTable[option.Value];

                            if ((optionMaskValue & maskValue) == optionMaskValue)
                            {

                                Query q=null;
                                if (isIndexedAsMasked)
                                {
                                    q = new TermQuery(new Term(fieldName.ToLower(), Lucene.Net.Util.NumericUtils.LongToPrefixCoded(option.Value)));
                                }
                                else
                                {
                                    q = new TermQuery(new Term(fieldName.ToLower(), Lucene.Net.Util.NumericUtils.IntToPrefixCoded(option.Value)));
                                }
                                if (null == boolQuery) boolQuery = new BooleanQuery();
                                boolQuery.Add(q, Occur.SHOULD);
                            }

                        }
                    }
                }
            }
            else
            {
                string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
                RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", "GetChildrenCountQuery", new Exception(string.Format("Cannot find attribute options for {0} in community:{1}", attributeName, communityId)), null);
            }
            return boolQuery;
        }

        public BooleanQuery GetMoreChildrenFlagQuery(string fieldName, int maskValue, int communityId)
        {
            Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection options = Utils.GetIntValuesFromMask(fieldName, communityId);
            BooleanQuery boolQuery = null;

            Dictionary<int, int> moreChildrenFlagTable = Utils.GetMoreChildrenFlagConversionTable();
            if (null != options)
            {
                foreach (Matchnet.Content.ValueObjects.AttributeOption.AttributeOption option in options)
                {
                    if (option.Value > 0)
                    {
                        //convert db value to hardcoded web tier value
                        if (moreChildrenFlagTable.ContainsKey(option.Value))
                        {
                            int optionMaskValue = moreChildrenFlagTable[option.Value];
                            if ((option.Value & maskValue) == option.Value)
                            {
                                Query q = new TermQuery(new Term(fieldName.ToLower(), Lucene.Net.Util.NumericUtils.IntToPrefixCoded(optionMaskValue)));
                                if (null == boolQuery) boolQuery = new BooleanQuery();
                                boolQuery.Add(q, Occur.SHOULD);
                            }
                        }
                    }
                }
            }
            else
            {
                string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
                RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", "GetChildrenCountQuery", new Exception(string.Format("Cannot find attribute options for {0} in community:{1}", fieldName, communityId)), null);
            }
            return boolQuery;
        }

        public BooleanQuery GetPreferredMoreChildrenFlagQuery(string fieldName, int maskValue, int communityId)
        {
            string attributeName = fieldName.Replace("preferred", ""); //remove the preferred word from the attribute name
            Matchnet.Content.ValueObjects.AttributeOption.AttributeOptionCollection options = Utils.GetIntValuesFromMask(attributeName, communityId);
            BooleanQuery boolQuery = null;

            if (null != options)
            {
                foreach (Matchnet.Content.ValueObjects.AttributeOption.AttributeOption option in options)
                {
                    if (option.Value > 0)
                    {
                        if ((option.Value & maskValue) == option.Value)
                        {
                            Query q =new TermQuery(new Term(fieldName.ToLower(), Lucene.Net.Util.NumericUtils.LongToPrefixCoded(option.Value)));
                            if (null == boolQuery) boolQuery = new BooleanQuery();
                            boolQuery.Add(q, Occur.SHOULD);
                        }
                    }
                }
            }
            else
            {
                string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(communityId).ToUpper());
                RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", "GetChildrenCountQuery", new Exception(string.Format("Cannot find attribute options for {0} in community:{1}", attributeName, communityId)), null);
            }
            return boolQuery;
        }
        
    }
}
