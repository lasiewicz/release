﻿using System;
using Lucene.Net.Index;
using Lucene.Net.Search;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class KeywordRelavanceSortField : SortField
    {
        private IMemberIdsAccessor _memberIdsAccessor = null;

        public KeywordRelavanceSortField(string field, bool reverse, IMemberIdsAccessor memberIds)
            : base(field, CUSTOM, reverse)
        {
            _memberIdsAccessor = memberIds;
        }

        public override FieldComparator GetComparator(int numHits, int sortPos)
        {
            return new KeywordRelavanceFieldComparatorSource.KeywordRelavanceFieldComparator(_memberIdsAccessor, numHits);
        }

        public override FieldComparatorSource ComparatorSource
        {
            get
            {
                return new KeywordRelavanceFieldComparatorSource(_memberIdsAccessor);
            }
        }
    }

    public class KeywordRelavanceFieldComparatorSource : FieldComparatorSource
    {
        private IMemberIdsAccessor _memberIdsAccessor = null;

        public KeywordRelavanceFieldComparatorSource(IMemberIdsAccessor memberIdsAccessor)
        {
            this._memberIdsAccessor = memberIdsAccessor;
        }

        public override FieldComparator NewComparator(string fieldname, int numHits, int sortPos, bool reversed)
        {
            return new KeywordRelavanceFieldComparator(_memberIdsAccessor, numHits);
        }

        public class KeywordRelavanceFieldComparator : FieldComparator
        {
            private readonly double[] values;
            private double bottom;
            private IMemberIdsAccessor memberIdsAccessor;

            private IndexReader currentIndexReader;
            private int[] ids;

            public KeywordRelavanceFieldComparator(IMemberIdsAccessor memberIdsAccessor, int numHits)
            {
                values = new double[numHits];
                this.memberIdsAccessor = memberIdsAccessor;
            }

            public override int Compare(int slot1, int slot2)
            {
                double a = values[slot1];
                double b = values[slot2];
                if (a > b)
                    return 1;
                if (a < b)
                    return -1;

                return 0;
            }

            public override void SetBottom(int slot)
            {
                bottom = values[slot];
            }

            public override int CompareBottom(int doc)
            {
                var v2 = CalculateKeywordRelevance(doc);
                if (bottom > v2)
                {
                    return 1;
                }

                if (bottom < v2)
                {
                    return -1;
                }

                return 0;
            }

            public override void Copy(int slot, int doc)
            {
                values[slot] = CalculateKeywordRelevance(doc);
            }

            private double CalculateKeywordRelevance(int doc)
            {
                //position in member ids corresponds to keyword relevance
                string userId = (ids != null && ids.Length > doc && ids[doc] > 0) ? ids[doc].ToString() : "0";

                int idx = int.MaxValue;
                for (int i = 0; i < memberIdsAccessor.MemberIds().Length; i++)
                {
                    string id = memberIdsAccessor.MemberIds()[i];
                    if (userId == id)
                    {
                        idx = i;
                        break;
                    }
                }
                return idx;
            }

            public override void SetNextReader(IndexReader reader, int docBase)
            {
                currentIndexReader = reader;

                //get all ids
                ids = FieldCache_Fields.DEFAULT.GetInts(reader, "id");
            }

            public override IComparable this[int slot]
            {
                get { return values[slot]; }
            }
        }
    }
}
