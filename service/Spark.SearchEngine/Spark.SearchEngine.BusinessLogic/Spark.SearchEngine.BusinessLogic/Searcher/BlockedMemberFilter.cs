﻿using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Util;

namespace Spark.SearchEngine.BusinessLogic.Searcher
{
    public class BlockedMemberFilter : Filter
    {
        private IMemberIdsAccessor _accesor;

        public BlockedMemberFilter(IMemberIdsAccessor accessor)
        {
            this._accesor = accessor;
        }

        public override DocIdSet GetDocIdSet(IndexReader reader)
        {
            var bits = new OpenBitSet(reader.MaxDoc);
            bits.Set(0, reader.MaxDoc + 1);
            var memberIDs = _accesor.MemberIds();

            var docs = new int[1];
            var freqs = new int[1];

            foreach (var memberid in memberIDs)
            {
                if (string.IsNullOrEmpty(memberid)) continue;
                var termDocs = reader.TermDocs(new Term("id", memberid));
                var count = termDocs.Read(docs, freqs);
                if (count == 1)
                {
                    bits.Flip(docs[0]);
                }
            }
            return bits;
        }
    }
}
