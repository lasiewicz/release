﻿using System;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Matchnet.Search.ServiceAdapters;
using Spark.SearchEngine.BusinessLogic.Searcher;
using Matchnet.Search.Interfaces;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.BusinessLogic;
using System.Collections.Generic;
using System.Text;
using Lucene.Net.Spatial.Util;
using Matchnet.Search.ValueObjects;

namespace Spark.SearchEngine.BusinessLogic.Searcher.Sorting
{
    public class SparkMatchPercentageSortField : SortField
    {
        private QueryBuilder _queryBuilder;
        public static string HasBirthDateString = QuerySearchParam.HasBirthDate.ToString().ToLower();
        public static string HasHeightString = QuerySearchParam.HasHeight.ToString().ToLower();
        public static string HasChildrenCountString = QuerySearchParam.HasChildrenCount.ToString().ToLower();
        public static string HasMoreChildrenFlagString = QuerySearchParam.HasMoreChildrenFlag.ToString().ToLower();
        public static string HasLanguageMaskString = QuerySearchParam.HasLanguageMask.ToString().ToLower();

        public SparkMatchPercentageSortField(string field, bool reverse, QueryBuilder queryBuilder)
            : base(field, CUSTOM, reverse)
        {
            _queryBuilder = queryBuilder;
        }

        public override FieldComparator GetComparator(int numHits, int sortPos)
        {
            return new MatchPercentageFieldComparatorSource.MatchPercentageFieldComparator(_queryBuilder, numHits);
        }

        //public override FieldComparatorSource GetComparatorSource()
        //{
        //    return new MatchPercentageFieldComparatorSource(_queryBuilder);
        //}

        public override FieldComparatorSource ComparatorSource
        {
            get
            {
                return new MatchPercentageFieldComparatorSource(_queryBuilder);
            }
        }
    }

    public class MatchPercentageFieldComparatorSource : FieldComparatorSource
    {
        private QueryBuilder _queryBuilder;

        public MatchPercentageFieldComparatorSource(QueryBuilder queryBuilder)
        {
            this._queryBuilder = queryBuilder;
        }

        public override FieldComparator NewComparator(string fieldname, int numHits, int sortPos, bool reversed)
        {
            return new MatchPercentageFieldComparator(_queryBuilder, numHits);
        }

        public class MatchPercentageFieldComparator : FieldComparator
        {
            private readonly double[] values;
            private double bottom;
            private QueryBuilder _queryBuilder;
            private bool calculateSearcherWants = false;
            private DateTime _searcherHasBirthdate = DateTime.MinValue;
            private bool _calculatedSearcherHasBirthdate = false;

            private IndexReader currentIndexReader;
            private bool UseFieldCache = false;
            private Dictionary<string, CacheReferenceHolder> resultMemberCache = new Dictionary<string, CacheReferenceHolder>();
            private int[] ids;

            public MatchPercentageFieldComparator(QueryBuilder queryBuilder, int numHits)
            {
                values = new double[numHits];
                this._queryBuilder = queryBuilder;
                this.UseFieldCache = Utils.GetSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", _queryBuilder.communityID, false);
                this._queryBuilder.usedFieldCacheForCalculation = UseFieldCache;
                if (_queryBuilder.SearchType != SearchType.YourMatchesWebSearch)
                {
                    this.calculateSearcherWants = true;
                }
            }

            #region Overrides
            public override int Compare(int slot1, int slot2)
            {
                double a = (slot1 <= values.Length) ? values[slot1] : 0;
                double b = (slot2 <= values.Length) ? values[slot2] : 0;
                if (a > b)
                    return 1;
                if (a < b)
                    return -1;

                return 0;
            }

            public override void SetBottom(int slot)
            {
                bottom = (slot <= values.Length) ? values[slot] : 0;
            }

            public override int CompareBottom(int doc)
            {
                var v2 = UseFieldCache ? CalculateMatchPercentageWithFieldCache(doc) : CalculateMatchPercentage(doc);
                if (bottom > v2)
                {
                    return 1;
                }

                if (bottom < v2)
                {
                    return -1;
                }

                return 0;
            }

            public override void Copy(int slot, int doc)
            {
                values[slot] = UseFieldCache ? CalculateMatchPercentageWithFieldCache(doc) : CalculateMatchPercentage(doc);
            }

            public override void SetNextReader(IndexReader reader, int docBase)
            {
                currentIndexReader = reader;

                //get field cache data
                if (UseFieldCache)
                {
                    //get all ids
                    ids = FieldCache_Fields.DEFAULT.GetInts(reader, "id");

                    resultMemberCache.Clear();
                    if (_queryBuilder.matchRatingAttributeList != null)
                    {
                        string attrName = "";
                        try
                        {
                            //get has (profile attribute data) and wants (prefs) for resulting members
                            foreach (MatchRatingAttribute matchAttribute in _queryBuilder.matchRatingAttributeList)
                            {
                                attrName = matchAttribute.searcherMemberHasAttribute;
                                if (attrName == SparkMatchPercentageSortField.HasBirthDateString)
                                {
                                    long[] hasLong = calculateSearcherWants ? FieldCache_Fields.DEFAULT.GetLongs(reader, matchAttribute.resultingMemberHasAttribute) : null;
                                    int[] wantsMin = FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberWantAttributeMin);
                                    int[] wantsMax = FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberWantAttributeMax);

                                    resultMemberCache[attrName] = new CacheReferenceHolder(hasLong, wantsMin, wantsMax);
                                }
                                else if (attrName == SparkMatchPercentageSortField.HasHeightString)
                                {
                                    int[] has = calculateSearcherWants ? FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberHasAttribute) : null;
                                    int[] wantsMin = FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberWantAttributeMin);
                                    int[] wantsMax = FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberWantAttributeMax);

                                    resultMemberCache[attrName] = new CacheReferenceHolder(has, wantsMin, wantsMax);
                                }
                                else if (attrName == SparkMatchPercentageSortField.HasLanguageMaskString)
                                {
                                    int[] has = calculateSearcherWants ? FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberHasAttributeMask) : null;
                                    int[] wants = FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberWantAttributeMask);

                                    resultMemberCache[attrName] = new CacheReferenceHolder(has, wants);
                                }
                                else
                                {
                                    int[] has = calculateSearcherWants ? FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberHasAttribute) : null;
                                    int[] wants = FieldCache_Fields.DEFAULT.GetInts(reader, matchAttribute.resultingMemberWantAttributeMask);

                                    resultMemberCache[attrName] = new CacheReferenceHolder(has, wants);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //bypass use field cache
                            UseFieldCache = false;
                            _queryBuilder.usedFieldCacheForCalculation = false;

                            Spark.Logging.RollingFileLogger.Instance.LogException(_queryBuilder.serviceConstant,
                                    "SparkMatchPercentageSortField", "SetNextReader", ex,
                                    string.Format("Error getting field cache info, AttrName:{0}\n", attrName));
                        }
                    }
                }
            }

            public override IComparable this[int slot]
            {
                get { return (slot <= values.Length) ? values[slot] : 0; }
            }

            //public override IComparable Value(int slot)
            //{
            //    return (slot <= values.Length) ? values[slot] : 0;
            //}

            #endregion

            #region Match Percentage Related

            private double CalculateMatchPercentage(int doc)
            {
                int matchPercent = 0;
                string attrName = "";

                if (currentIndexReader == null) return 0;
                var document = currentIndexReader.Document(doc);
                if (document == null) return 0;
                int userId = (null != document.GetField("id")) ? int.Parse(document.GetField("id").StringValue) : 0;
                if (userId <= 0) return 0;

                
                if (_queryBuilder.matchPercentages.ContainsKey(userId))
                {
                    matchPercent = _queryBuilder.matchPercentages[userId];
                }
                else if (_queryBuilder.matchRatingAttributeList != null)
                {
                    try
                    {
                        MatchRatingUtil.MatchRatingCalculationVersion matchVersion = _queryBuilder.matchVersion;
                        double defaultImportance = _queryBuilder.matchDefaultImportance;

                        int searcherMemberMissingAttributeCount = 0;
                        int resultMemberMissingAttributeCount = 0;

                        double searcherMemberSubTotal = 1;
                        double resultMemberSubTotal = 1;

                        foreach (MatchRatingAttribute matchAttribute in _queryBuilder.matchRatingAttributeList)
                        {
                            attrName = matchAttribute.searcherMemberHasAttribute;

                            if (_queryBuilder.matchMinimumProfileAttributeRequired > 0 && (searcherMemberMissingAttributeCount > _queryBuilder.matchMaxMissingAttributeForNoRating || resultMemberMissingAttributeCount > _queryBuilder.matchMaxMissingAttributeForNoRating))
                            {
                                //member 1 or 2 missing more profile attributes than allowed for calculation
                                //return 0 to indicate no match rating
                                searcherMemberSubTotal = 0;
                                resultMemberSubTotal = 0;
                                break;
                            }

                            if (attrName == SparkMatchPercentageSortField.HasBirthDateString)
                            {
                                //searcher member wants
                                if (calculateSearcherWants)
                                {
                                    int searcherMemberWantsMin = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMin)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMin].Value.ToString()) : 18;
                                    int searcherMemberWantsMax = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMax)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMax].Value.ToString()) : 99;

                                    long resultMemberHasBirthdateTicks = (null != document.GetField(matchAttribute.resultingMemberHasAttribute)) ? long.Parse(document.GetField(matchAttribute.resultingMemberHasAttribute).StringValue) : 0;
                                    DateTime resultMemberHasBirthdate = resultMemberHasBirthdateTicks > 0 ? new DateTime(resultMemberHasBirthdateTicks) : DateTime.MinValue;
                                    int resultMemberAge = 18;
                                    if (resultMemberHasBirthdate > DateTime.MinValue)
                                    {
                                        resultMemberAge = (int)((double)DateTime.Now.Subtract(resultMemberHasBirthdate).Days / 365.25);
                                    }
                                    else
                                    {
                                        resultMemberMissingAttributeCount++;
                                    }

                                    if ((resultMemberAge < searcherMemberWantsMin) || (resultMemberAge > searcherMemberWantsMax))
                                    {
                                        if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                        {
                                            searcherMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                        {
                                            searcherMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }
                                    }
                                }

                                //result member wants
                                int resultMemberWantsMin = (null != document.GetField(matchAttribute.resultingMemberWantAttributeMin)) ? Int32.Parse(document.GetField(matchAttribute.resultingMemberWantAttributeMin).StringValue) : 18;
                                int resultMemberWantsMax = (null != document.GetField(matchAttribute.resultingMemberWantAttributeMax)) ? Int32.Parse(document.GetField(matchAttribute.resultingMemberWantAttributeMax).StringValue) : 99;

                                DateTime searchMemberHasBirthdate = _searcherHasBirthdate;
                                if (!_calculatedSearcherHasBirthdate && _queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberHasAttribute))
                                {
                                    long searcherMemberHasBirthdateTicks = 0;
                                    long.TryParse(
                                        _queryBuilder.queryParameters[matchAttribute.searcherMemberHasAttribute].Value
                                                                                                                .ToString(),
                                        out searcherMemberHasBirthdateTicks);

                                    if (searcherMemberHasBirthdateTicks > 0)
                                    {
                                        searchMemberHasBirthdate = new DateTime(searcherMemberHasBirthdateTicks);
                                    }
                                    _calculatedSearcherHasBirthdate = true;
                                    _searcherHasBirthdate = searchMemberHasBirthdate;
                                }

                                int searcherMemberAge = 18;
                                if (searchMemberHasBirthdate > DateTime.MinValue)
                                {
                                    searcherMemberAge = (int)((double)DateTime.Now.Subtract(searchMemberHasBirthdate).Days / 365.25);
                                }
                                else
                                {
                                    searcherMemberMissingAttributeCount++;
                                }

                                if ((searcherMemberAge < resultMemberWantsMin) || (searcherMemberAge > resultMemberWantsMax))
                                {
                                    if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                    {
                                        resultMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                    }
                                    else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                    {
                                        resultMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                    }

                                    if (_queryBuilder.saveMismatchData)
                                    {
                                        _queryBuilder.AddMatchMismatch(userId, matchAttribute.resultingMemberHasAttribute);
                                    }
                                }
                            }
                            else if (attrName == SparkMatchPercentageSortField.HasHeightString)
                            {
                                //searcher member wants
                                if (calculateSearcherWants)
                                {
                                    int resultMemberHas = (null != document.GetField(matchAttribute.resultingMemberHasAttribute)) ? Int32.Parse(document.GetField(matchAttribute.resultingMemberHasAttribute).StringValue) : int.MinValue;
                                    int searcherMemberWantsMin = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMin)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMin].Value.ToString()) : int.MinValue;
                                    int searcherMemberWantsMax = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMax)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMax].Value.ToString()) : int.MinValue;

                                    if (resultMemberHas <= 0)
                                    {
                                        resultMemberMissingAttributeCount++;
                                    }

                                    if ((searcherMemberWantsMin > 0 && resultMemberHas < searcherMemberWantsMin)
                                        || (searcherMemberWantsMax > 0 && resultMemberHas > searcherMemberWantsMax))
                                    {
                                        if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                        {
                                            searcherMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                        {
                                            searcherMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }
                                    }
                                }

                                //result member wants
                                int resultMemberWantsMin = (null != document.GetField(matchAttribute.resultingMemberWantAttributeMin)) ? Int32.Parse(document.GetField(matchAttribute.resultingMemberWantAttributeMin).StringValue) : int.MinValue;
                                int resultMemberWantsMax = (null != document.GetField(matchAttribute.resultingMemberWantAttributeMax)) ? Int32.Parse(document.GetField(matchAttribute.resultingMemberWantAttributeMax).StringValue) : int.MinValue;
                                int searcherMemberHas = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberHasAttribute)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberHasAttribute].Value.ToString()) : int.MinValue;

                                if (searcherMemberHas <= 0)
                                {
                                    searcherMemberMissingAttributeCount++;
                                }

                                if ((resultMemberWantsMin > 0 && searcherMemberHas < resultMemberWantsMin)
                                    || (resultMemberWantsMax > 0 && searcherMemberHas > resultMemberWantsMax))
                                {
                                    if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                    {
                                        resultMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                    }
                                    else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                    {
                                        resultMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                    }

                                    if (_queryBuilder.saveMismatchData)
                                    {
                                        _queryBuilder.AddMatchMismatch(userId, matchAttribute.resultingMemberHasAttribute);
                                    }
                                }
                            }
                            else
                            {
                                //searcher member wants
                                if (calculateSearcherWants)
                                {
                                    int searcherMemberWants = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttribute)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttribute].Value.ToString()) : int.MinValue;
                                    int resultMemberHas = (null != document.GetField(matchAttribute.resultingMemberHasAttribute)) ? Int32.Parse(document.GetField(matchAttribute.resultingMemberHasAttribute).StringValue) : int.MinValue;
                                    
                                    if (resultMemberHas < 0)
                                    {
                                        resultMemberMissingAttributeCount++;
                                    }

                                    if (searcherMemberWants >= 0)
                                    {
                                        bool searcherMemberWantsMatch = false;
                                        
                                        if (resultMemberHas >= 0)
                                        {
                                            if (attrName == SparkMatchPercentageSortField.HasLanguageMaskString)
                                            {
                                                //mask vs mask comparison
                                                if (resultMemberHas > 0)
                                                {
                                                    int higherMask = resultMemberHas > searcherMemberWants
                                                                           ? resultMemberHas
                                                                           : searcherMemberWants;
                                                    for (long i = 1; (long)Math.Pow(2, i) <= higherMask; i++)
                                                    {
                                                        long step = (long)Math.Pow(2, i);
                                                        if (((step & resultMemberHas) == step) && ((step & searcherMemberWants) == step))
                                                        {
                                                            searcherMemberWantsMatch = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //single value comparison
                                                if (attrName == SparkMatchPercentageSortField.HasMoreChildrenFlagString && Utils.GetMoreChildrenFlagConversionTableReverse().ContainsKey(resultMemberHas))
                                                {
                                                    resultMemberHas = Utils.GetMoreChildrenFlagConversionTableReverse()[resultMemberHas];
                                                }
                                                else if (attrName == SparkMatchPercentageSortField.HasChildrenCountString && Utils.GetChildrenCountConversionTable().ContainsKey(resultMemberHas))
                                                {
                                                    resultMemberHas = Utils.GetChildrenCountConversionTable()[resultMemberHas];
                                                }

                                                if (resultMemberHas > 0)
                                                {
                                                    if ((resultMemberHas & searcherMemberWants) == resultMemberHas)
                                                    {
                                                        searcherMemberWantsMatch = true;
                                                    }
                                                }
                                            }
                                        }

                                        if (!searcherMemberWantsMatch)
                                        {
                                            if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                            {
                                                searcherMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                            }
                                            else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                            {
                                                searcherMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                            }
                                        }
                                    }
                                }

                                //result member wants                                
                                Lucene.Net.Documents.Field[] resultMemberWantsFields = document.GetFields(matchAttribute.resultingMemberWantAttribute);
                                int searcherMemberHas = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberHasAttribute)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberHasAttribute].Value.ToString()) : int.MinValue;

                                if (searcherMemberHas < 0)
                                {
                                    searcherMemberMissingAttributeCount++;
                                }

                                if (null != resultMemberWantsFields && resultMemberWantsFields.Length > 0)
                                {
                                    StringBuilder resultMemberWantsSB = new StringBuilder();
                                    bool resultMemberWantsMatch = false;

                                    if (searcherMemberHas >= 0)
                                    {
                                        if (attrName == SparkMatchPercentageSortField.HasMoreChildrenFlagString &&
                                            Utils.GetMoreChildrenFlagConversionTableReverse()
                                                 .ContainsKey(searcherMemberHas))
                                        {
                                            searcherMemberHas =
                                                Utils.GetMoreChildrenFlagConversionTableReverse()[searcherMemberHas];
                                        }
                                        else if (attrName == SparkMatchPercentageSortField.HasChildrenCountString &&
                                                 Utils.GetChildrenCountConversionTable().ContainsKey(searcherMemberHas))
                                        {
                                            searcherMemberHas =
                                                Utils.GetChildrenCountConversionTable()[searcherMemberHas];
                                        }
                                        string searcherMemberHasString = searcherMemberHas.ToString();

                                        //prefs in index for these are stored multiple fields for masks
                                        for (int j = 0; j < resultMemberWantsFields.Length; j++)
                                        {
                                            string resultMemberWantsString = resultMemberWantsFields[j].StringValue;
                                            resultMemberWantsSB.Append(resultMemberWantsString + ",");
                                            if (!string.IsNullOrEmpty(resultMemberWantsString))
                                            {
                                                if (attrName == SparkMatchPercentageSortField.HasLanguageMaskString)
                                                {
                                                    //mask value comparison (profile values are mask)
                                                    int resultMemberWants = int.Parse(resultMemberWantsString);
                                                    if ((resultMemberWants & searcherMemberHas) == resultMemberWants)
                                                    {
                                                        resultMemberWantsMatch = true;
                                                        break;
                                                    }
                                                }
                                                else if (resultMemberWantsString == searcherMemberHasString)
                                                {
                                                    //single value comparison
                                                    resultMemberWantsMatch = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (!resultMemberWantsMatch)
                                    {
                                        if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                        {
                                            resultMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                        {
                                            resultMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }

                                        if (_queryBuilder.saveMismatchData)
                                        {
                                            _queryBuilder.AddMatchMismatch(userId, matchAttribute.resultingMemberHasAttribute + "[" + searcherMemberHas + ":" + resultMemberWantsSB.ToString() + "]");
                                        }
                                    }
                                }                                
                            }
                        }

                        if (searcherMemberSubTotal < 0)
                            searcherMemberSubTotal = 0;

                        if (resultMemberSubTotal < 0)
                            resultMemberSubTotal = 0;

                        //determine total match percentage
                        matchPercent = (int)Math.Floor((_queryBuilder.matchSearcherMemberPercentage * searcherMemberSubTotal) + (_queryBuilder.matchResultingMemberPercentage * resultMemberSubTotal));

                        //Spark.Logging.RollingFileLogger.Instance.LogInfoMessage(_queryBuilder.serviceConstant,
                        //        "MatchPercentageSortField",
                        //        string.Format("UserId:{0}, MatchPercentage:{1}\n", userId, matchPercent), null);

                        _queryBuilder.AddMatchPercentage(userId, matchPercent);
                    }
                    catch (Exception ex)
                    {
                        Spark.Logging.RollingFileLogger.Instance.LogException(_queryBuilder.serviceConstant,
                                    "SparkMatchPercentageSortField", "CalculateMatchPercentage", ex,
                                    string.Format("UserId:{0}, AttrName:{1}\n", userId, attrName));
                    }
                }
                

                return matchPercent;
            }

            private double CalculateMatchPercentageWithFieldCache(int doc)
            {
                int matchPercent = 0;
                string attrName = "";

                int userId = (ids != null && ids.Length > doc) ? ids[doc] : 0;
                if (userId <= 0) return 0;

                if (_queryBuilder.matchPercentages.ContainsKey(userId))
                {
                    matchPercent = _queryBuilder.matchPercentages[userId];
                }
                else if (_queryBuilder.matchRatingAttributeList != null)
                {
                    try
                    {
                        MatchRatingUtil.MatchRatingCalculationVersion matchVersion = _queryBuilder.matchVersion;
                        
                        double defaultImportance = _queryBuilder.matchDefaultImportance;

                        double searcherMemberSubTotal = 1;
                        double resultMemberSubTotal = 1;

                        foreach (MatchRatingAttribute matchAttribute in _queryBuilder.matchRatingAttributeList)
                        {
                            attrName = matchAttribute.searcherMemberHasAttribute;

                            if (attrName == SparkMatchPercentageSortField.HasBirthDateString)
                            {
                                //searcher member wants
                                if (calculateSearcherWants)
                                {
                                    int searcherMemberWantsMin = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMin)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMin].Value.ToString()) : 18;
                                    int searcherMemberWantsMax = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMax)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMax].Value.ToString()) : 99;

                                    long resultMemberHasBirthdateTicks = (resultMemberCache[attrName].hasLong.Length > doc) ? resultMemberCache[attrName].hasLong[doc] : 0;
                                    DateTime resultMemberHasBirthdate = resultMemberHasBirthdateTicks > 0 ? new DateTime(resultMemberHasBirthdateTicks) : DateTime.MinValue;

                                    int resultMemberAge = 18;
                                    if (resultMemberHasBirthdate > DateTime.MinValue)
                                    {
                                        resultMemberAge = (int)((double)DateTime.Now.Subtract(resultMemberHasBirthdate).Days / 365.25);
                                    }

                                    if ((resultMemberAge < searcherMemberWantsMin) || (resultMemberAge > searcherMemberWantsMax))
                                    {
                                        if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                        {
                                            searcherMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                        {
                                            searcherMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }
                                    }
                                }

                                //result member wants
                                int resultMemberWantsMin = (resultMemberCache[attrName].wantsMin.Length > doc && resultMemberCache[attrName].wantsMin[doc] > 0) ? resultMemberCache[attrName].wantsMin[doc] : 18;
                                int resultMemberWantsMax = (resultMemberCache[attrName].wantsMax.Length > doc && resultMemberCache[attrName].wantsMax[doc] > 0) ? resultMemberCache[attrName].wantsMax[doc] : 99;

                                DateTime searchMemberHasBirthdate = _searcherHasBirthdate;
                                if (!_calculatedSearcherHasBirthdate && _queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberHasAttribute))
                                {
                                    long searcherMemberHasBirthdateTicks = 0;
                                    long.TryParse(
                                        _queryBuilder.queryParameters[matchAttribute.searcherMemberHasAttribute].Value
                                                                                                                .ToString(),
                                        out searcherMemberHasBirthdateTicks);

                                    if (searcherMemberHasBirthdateTicks > 0)
                                    {
                                        searchMemberHasBirthdate = new DateTime(searcherMemberHasBirthdateTicks);
                                    }
                                    _calculatedSearcherHasBirthdate = true;
                                    _searcherHasBirthdate = searchMemberHasBirthdate;
                                }

                                int searcherMemberAge = 18;
                                if (searchMemberHasBirthdate > DateTime.MinValue)
                                {
                                    searcherMemberAge = (int)((double)DateTime.Now.Subtract(searchMemberHasBirthdate).Days / 365.25);
                                }

                                if ((searcherMemberAge < resultMemberWantsMin) || (searcherMemberAge > resultMemberWantsMax))
                                {
                                    if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                    {
                                        resultMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                    }
                                    else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                    {
                                        resultMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                    }

                                    if (_queryBuilder.saveMismatchData)
                                    {
                                        _queryBuilder.AddMatchMismatch(userId, matchAttribute.resultingMemberHasAttribute + "[" + searcherMemberAge.ToString() + ":" + resultMemberWantsMin.ToString() + "-" + resultMemberWantsMax.ToString() + "]");
                                    }
                                }
                            }
                            else if (attrName == SparkMatchPercentageSortField.HasHeightString)
                            {
                                //searcher member wants
                                if (calculateSearcherWants)
                                {
                                    int resultMemberHas = (resultMemberCache[attrName].has.Length > doc) ? resultMemberCache[attrName].has[doc] : int.MinValue;
                                    int searcherMemberWantsMin = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMin)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMin].Value.ToString()) : int.MinValue;
                                    int searcherMemberWantsMax = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttributeMax)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttributeMax].Value.ToString()) : int.MinValue;

                                    if ((searcherMemberWantsMin > 0 && resultMemberHas < searcherMemberWantsMin)
                                        || (searcherMemberWantsMax > 0 && resultMemberHas > searcherMemberWantsMax))
                                    {
                                        if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                        {
                                            searcherMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                        {
                                            searcherMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }
                                    }
                                }

                                //result member wants
                                int resultMemberWantsMin = (resultMemberCache[attrName].wantsMin.Length > doc) ? resultMemberCache[attrName].wantsMin[doc] : int.MinValue;
                                int resultMemberWantsMax = (resultMemberCache[attrName].wantsMax.Length > doc) ? resultMemberCache[attrName].wantsMax[doc] : int.MinValue;
                                int searcherMemberHas = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberHasAttribute)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberHasAttribute].Value.ToString()) : int.MinValue;

                                if ((resultMemberWantsMin > 0 && searcherMemberHas < resultMemberWantsMin)
                                    || (resultMemberWantsMax > 0 && searcherMemberHas > resultMemberWantsMax))
                                {
                                    if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                    {
                                        resultMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                    }
                                    else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                    {
                                        resultMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                    }

                                    if (_queryBuilder.saveMismatchData)
                                    {
                                        _queryBuilder.AddMatchMismatch(userId, matchAttribute.resultingMemberHasAttribute + "[" + searcherMemberHas.ToString() + ":" + resultMemberWantsMin.ToString() + "-" + resultMemberWantsMax.ToString() + "]");
                                    }
                                }
                            }
                            else
                            {
                                //searcher member wants
                                if (calculateSearcherWants)
                                {
                                    int searcherMemberWants = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberWantAttribute)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberWantAttribute].Value.ToString()) : int.MinValue;
                                    if (searcherMemberWants >= 0)
                                    {
                                        bool searcherMemberWantsMatch = false;
                                        int resultMemberHas = (resultMemberCache[attrName].has.Length > doc) ? resultMemberCache[attrName].has[doc] : int.MinValue;
                                        if (resultMemberHas >= 0)
                                        {
                                            if (attrName == SparkMatchPercentageSortField.HasLanguageMaskString)
                                            {
                                                //mask vs mask comparison
                                                if (resultMemberHas > 0)
                                                {
                                                    int higherMask = resultMemberHas > searcherMemberWants
                                                                           ? resultMemberHas
                                                                           : searcherMemberWants;
                                                    for (long i = 1; (long)Math.Pow(2, i) <= higherMask; i++)
                                                    {
                                                        long step = (long)Math.Pow(2, i);
                                                        if (((step & resultMemberHas) == step) && ((step & searcherMemberWants) == step))
                                                        {
                                                            searcherMemberWantsMatch = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //single value comparison (searcher profile data is single value, result member prefs is mask value)
                                                if (attrName == SparkMatchPercentageSortField.HasMoreChildrenFlagString && Utils.GetMoreChildrenFlagConversionTableReverse().ContainsKey(resultMemberHas))
                                                {
                                                    resultMemberHas = Utils.GetMoreChildrenFlagConversionTableReverse()[resultMemberHas];
                                                }
                                                else if (attrName == SparkMatchPercentageSortField.HasChildrenCountString && Utils.GetChildrenCountConversionTable().ContainsKey(resultMemberHas))
                                                {
                                                    resultMemberHas = Utils.GetChildrenCountConversionTable()[resultMemberHas];
                                                }

                                                if (resultMemberHas > 0)
                                                {
                                                    if ((resultMemberHas & searcherMemberWants) == resultMemberHas)
                                                    {
                                                        searcherMemberWantsMatch = true;
                                                    }
                                                }
                                            }                                            
                                        }

                                        if (!searcherMemberWantsMatch)
                                        {
                                            if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                            {
                                                searcherMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                            }
                                            else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                            {
                                                searcherMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                            }
                                        }
                                    }
                                }

                                //result member wants
                                int resultMemberWants = (resultMemberCache[attrName].wants.Length > doc) ? resultMemberCache[attrName].wants[doc] : int.MinValue;
                                if (resultMemberWants > 0)
                                {
                                    bool resultMemberWantsMatch = false;

                                    int searcherMemberHas = (_queryBuilder.queryParameters.ContainsKey(matchAttribute.searcherMemberHasAttribute)) ? int.Parse(_queryBuilder.queryParameters[matchAttribute.searcherMemberHasAttribute].Value.ToString()) : int.MinValue;
                                    if (searcherMemberHas >= 0)
                                    {
                                        if (attrName == SparkMatchPercentageSortField.HasLanguageMaskString)
                                        {
                                            //mask vs mask comparison
                                            if (searcherMemberHas > 0)
                                            {
                                                int higherMask = searcherMemberHas > resultMemberWants
                                                                       ? searcherMemberHas
                                                                       : resultMemberWants;
                                                for (long i = 1; (long)Math.Pow(2, i) <= higherMask; i++)
                                                {
                                                    long step = (long) Math.Pow(2, i);
                                                    if (((step & searcherMemberHas) == step) && ((step & resultMemberWants) == step))
                                                    {
                                                        resultMemberWantsMatch = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //single value comparison (result profile data is single value, searcher member prefs is mask value)
                                            if (attrName == SparkMatchPercentageSortField.HasMoreChildrenFlagString && Utils.GetMoreChildrenFlagConversionTableReverse().ContainsKey(searcherMemberHas))
                                            {
                                                searcherMemberHas = Utils.GetMoreChildrenFlagConversionTableReverse()[searcherMemberHas];
                                            }
                                            else if (attrName == SparkMatchPercentageSortField.HasChildrenCountString && Utils.GetChildrenCountConversionTable().ContainsKey(searcherMemberHas))
                                            {
                                                searcherMemberHas = Utils.GetChildrenCountConversionTable()[searcherMemberHas];
                                            }

                                            if (searcherMemberHas > 0)
                                            {
                                                if ((searcherMemberHas & resultMemberWants) == searcherMemberHas)
                                                {
                                                    resultMemberWantsMatch = true;
                                                }
                                            }
                                        }
                                    }

                                    if (!resultMemberWantsMatch)
                                    {
                                        if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version2)
                                        {
                                            resultMemberSubTotal -= ((defaultImportance * defaultImportance) / 100);
                                        }
                                        else if (matchVersion == MatchRatingUtil.MatchRatingCalculationVersion.Version3)
                                        {
                                            resultMemberSubTotal *= 1 - ((defaultImportance * defaultImportance) / 100);
                                        }

                                        if (_queryBuilder.saveMismatchData)
                                        {
                                            _queryBuilder.AddMatchMismatch(userId, matchAttribute.resultingMemberHasAttribute + "[" + searcherMemberHas.ToString() + ":" + resultMemberWants.ToString() + "]");
                                        }
                                    }
                                }
                            }
                        }

                        if (searcherMemberSubTotal < 0)
                            searcherMemberSubTotal = 0;

                        if (resultMemberSubTotal < 0)
                            resultMemberSubTotal = 0;

                        //determine total match percentage
                        matchPercent = (int)Math.Floor((_queryBuilder.matchSearcherMemberPercentage * searcherMemberSubTotal) + (_queryBuilder.matchResultingMemberPercentage * resultMemberSubTotal));

                        //Spark.Logging.RollingFileLogger.Instance.LogInfoMessage(_queryBuilder.serviceConstant,
                        //        "SparkMatchPercentageSortField",
                        //        string.Format("UserId:{0}, MatchPercentage:{1}\n", userId, matchPercent), null);

                        _queryBuilder.AddMatchPercentage(userId, matchPercent);
                    }
                    catch (Exception ex)
                    {
                        Spark.Logging.RollingFileLogger.Instance.LogException(_queryBuilder.serviceConstant,
                                    "SparkMatchPercentageSortField", "CalculateMatchPercentageWithFieldCache", ex,
                                    string.Format("Searcher MemberID: {0}, UserId:{1}, AttrName:{2}\n", _queryBuilder.hasMemberID, userId, attrName));
                    }
                }

                return matchPercent;
            }
            #endregion

        }
    }

    public class CacheReferenceHolder
    {
        public int[] has, wants, wantsMin, wantsMax;
        public long[] hasLong;
        public CacheReferenceHolder(int[] _has, int[] _wants)
        {
            has = _has != null ? _has : new int[] { 0 };
            wants = _wants != null ? _wants : new int[] { 0 };
        }

        public CacheReferenceHolder(int[] _has, int[] _wantsMin, int[] _wantsMax)
        {
            has = _has != null ? _has : new int[] {0};
            wantsMin = _wantsMin != null ? _wantsMin : new int[] {0};
            wantsMax = _wantsMax != null ? _wantsMax : new int[] {0};
        }

        public CacheReferenceHolder(long[] _hasLong, int[] _wantsMin, int[] _wantsMax)
        {
            hasLong = _hasLong != null ? _hasLong : new long[] { 0 };
            wantsMin = _wantsMin != null ? _wantsMin : new int[] { 0 };
            wantsMax = _wantsMax != null ? _wantsMax : new int[] { 0 };
        }
    }
}
