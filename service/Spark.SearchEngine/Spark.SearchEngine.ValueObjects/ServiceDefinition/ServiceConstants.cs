﻿
using System;

namespace Spark.SearchEngine.ValueObjects
{
    public enum IProcessorType
    {
        MemberIndexProcessor = 1,
        KeywordIndexProcessor = 2,
        AdminIndexProcessor = 3,
        PhotoIndexProcessor = 4,
        MemberIndexVersion3Processor = 5,
        KeywordIndexVersion3Processor = 6
    }

    public enum SearchFilter
    {
        LastActiveDateFilter=1,
        AgeRangeFilter=2,
        JoinDateFilter=3,
        SubscriberFilter=4,
        ProximityFilter=5,
        KeywordFilter=6,
        OnlineDateFilter=7
    }

    public class ServiceConstants
    {
        public enum SearchResultType
        {
            TerseResult=1,
            DetailedResult=2
        }

        public const string SERVICE_INDEXER_CONST = "SEARCHINDEXER_SVC";
        public const string SERVICE_INDEXER_IISHOST_CONST = "SEARCHINDEXER_IIS_SVC";
        public const string SERVICE_INDEXER_NAME = "Spark.SearchIndexer.Service";


        public const string SERVICE_INDEXER_MANAGER_CONST = "LoaderSM";

        public const string SERVICE_SEARCHER_CONST = "{0}SEARCHER_SVC";
        public const string SERVICE_SEARCHER_NAME = "Spark.{0}Searcher.Service";
        public const string SEARCHER_SA_HOST_OVERRIDE = "{0}SEARCHER_SA_HOST_OVERRIDE";

        public const string SERVICE_SEARCHER_MANAGER_CONST = "SearcherSM";
        public const string INDEX_DIR_LOCK_FILE_FORMAT = "{0}.lock";
        public const string SEARCHSTORE_DB_NAME = "mnSearchStore";
        public const string SEARCHLOAD_DB_NAME = "SearchLoad";
        public const string ADMINSEARCH_DB_NAME = "mnMember_ProdFlat";
        public const string SYSTEM_DB_NAME = "mnSystem";

        public enum COMMUNITY_ID : int
        {
            Spark = 1,
            Jdate = 3,
            Corp = 8,
            Cupid = 10,
            College = 12,
            Mingle = 20,
            ItalianSingles = 21,
            InterRacialSingles = 22,
            BBWPersonalsPlus = 23,
            BlackSingles = 24,
            ChristianMingle = 25
        }

        public static string GetCommunityName(int communityId)
        {
            string communityName = Enum.Parse(typeof (COMMUNITY_ID), communityId.ToString()).ToString().ToUpper();
            return communityName;
        }

        public static int GetCommunityId(string communityName)
        {
            int communityId = Convert.ToInt32(Enum.Parse(typeof(COMMUNITY_ID), communityName));
            return communityId;
        }
    }
}
