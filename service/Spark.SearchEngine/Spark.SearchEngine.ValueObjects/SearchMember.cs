﻿using System;
using System.Data;
using System.Linq;
using Matchnet;
namespace Spark.SearchEngine.ValueObjects
{
    [Serializable]
    public enum UpdateTypeEnum:int
    {
        none=0,
        all=1,
        emailcount=2,
        lastactivedate=3,
        hasphoto=4,
        delete=5,
        add=6,
        partial=7
    }

    [Serializable]
    public enum UpdateReasonEnum : int
    {
        none=0,
        adminSuspend=1,
        adminUnsuspend = 2,
        selfSuspend=3,
        selfUnsuspend = 4,
        hide = 5,
        unHide = 6,
        searchDataChanged=7,
        logon=8,
        updateChangedToRemove =9,
        online=10,
        offline=11
    }

    [Serializable]
    public enum UpdateModeEnum : int
    {
        none=0,
        add=1,
        update=2,
        remove=3
    }

    #region class SearchMember
    [Serializable]
    public class SearchMember
    {

        #region private
        int _memberid;
        int _communityid;
        int _gendermask;
        DateTime _birthdate;
        DateTime _communityinsertdate;
        DateTime _lastactivedate;
        int _hasphotoflag;
        int _educationlevel;
        int _religion;
        int _languagemask;
        int _ethnicity;
        int _smokinghabits;
        int _drinkinghabits;
        int _height;
        int _maritalstatus;
        int _jdatereligion;
        int _jdateethnicity;
        int _synagogueattendance;
        int _keepkosher;
        int _sexualidentitytype;
        int _relationshipmask;
        int _relationshipstatus;
        int _bodytype;
        int _zodiac;
        int _majortype;
        double _longitude;
        double _latitude;
        int _depth1regionid;
        int _depth2regionid;
        int _depth3regionid;
        int _depth4regionid;
        int _schoolid;
        string _areacode;
        int _emailcount;
        DateTime _updatedate;
        int _weight;
        int _relocateflag;
        int _childrencount;
        DateTime _subscriptionexpirationdate;
        int _activitylevel;
        int _custody;
        int _morechildrenflag;
        int _colorcode;
        int _online;
        int _ramahAlum;
        int _preferredRelocation;
        int _preferredMinHeight;
        int _preferredMaxHeight;
        int _preferredMinAge;
        int _preferredMaxAge;
        int _preferredSynagogueAttendance;
        int _preferredJdateReligion;
        int _preferredSmokingHabit;
        int _preferredDrinkingHabit;
        int _preferredJdateEthnicity;
        int _preferredActivityLevel;
        int _preferredKosherStatus;
        int _preferredMaritalStatus;
        int _preferredDistance;
        int _preferredRegionID;
        int _preferredLanguageMask;
        int _preferredGenderMask;
        string _preferredAreaCodes;
        int _preferredHasPhotoFlag;
        int _preferredEducationLevel;
        int _preferredReligion;
        int _preferredEthnicity;
        int _preferredSexualIdentityType;
        int _preferredRelationshipMask;
        int _preferredRelationshipstatus;
        int _preferredBodyType;
        int _preferredZodiac;
        int _preferredMajorType;
        int _preferredMoreChildrenFlag;
        int _preferredChildrenCount;
        int _preferredCustody;
        int _preferredColorcode;
 

        private UpdateReasonEnum _UpdateReason = UpdateReasonEnum.none;
        private UpdateModeEnum _UpdateMode = UpdateModeEnum.none;
        #endregion
        #region properties

        //for compatibility with e1loader updates
        public UpdateTypeEnum UpdateType { get; set; }
        public UpdateReasonEnum UpdateReason
        {
            get { return _UpdateReason; }
            set { _UpdateReason = value; }
        }
        public UpdateModeEnum UpdateMode
        {
            get { return _UpdateMode; }
            set { _UpdateMode = value; }
        }

        public int MemberID
        { 
            get { return _memberid;}
            set {_memberid = value;} 
        }

        public int CommunityID
        {
            get { return _communityid; }
            set { _communityid = value; }
        }

        public int GenderMask
        {
            get { return _gendermask; }
            set { _gendermask = value; }
        }

        public DateTime Birthdate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }

        public DateTime CommunityInsertDate
        {
            get { return _communityinsertdate; }
            set { _communityinsertdate = value; }
        }

        public DateTime LastActiveDate
        {
            get { return _lastactivedate; }
            set { _lastactivedate = value; }
        }

        public int HasPhotoFlag
        {
            get { return _hasphotoflag; }
            set { _hasphotoflag = value; }
        }

        public int EducationLevel
        {
            get { return _educationlevel; }
            set { _educationlevel = value; }
        }

        public int Religion
        {
            get { return _religion; }
            set { _religion = value; }
        }

        public int LanguageMask
        {
            get { return _languagemask; }
            set { _languagemask = value; }
        }

        public int Ethnicity
        {
            get { return _ethnicity; }
            set { _ethnicity = value; }
        }

        public int SmokingHabits
        {
            get { return _smokinghabits; }
            set { _smokinghabits = value; }
        }

        public int DrinkingHabits
        {
            get { return _drinkinghabits; }
            set { _drinkinghabits = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public int MaritalStatus
        {
            get { return _maritalstatus; }
            set { _maritalstatus = value; }
        }

        public int JDateReligion
        {
            get { return _jdatereligion; }
            set { _jdatereligion = value; }
        }

        public int JDateEthnicity
        {
            get { return _jdateethnicity; }
            set { _jdateethnicity = value; }
        }

        public int SynagogueAttendance
        {
            get { return _synagogueattendance; }
            set { _synagogueattendance = value; }
        }

        public int KeepKosher
        {
            get { return _keepkosher; }
            set { _keepkosher = value; }
        }

        public int SexualIdentityType
        {
            get { return _sexualidentitytype; }
            set { _sexualidentitytype = value; }
        }

        public int RelationshipMask
        {
            get { return _relationshipmask; }
            set { _relationshipmask = value; }
        }

        public int RelationshipStatus
        {
            get { return _relationshipstatus; }
            set { _relationshipstatus = value; }
        }

        public int BodyType
        {
            get { return _bodytype; }
            set { _bodytype = value; }
        }

        public int Zodiac
        {
            get { return _zodiac; }
            set { _zodiac = value; }
        }

        public int MajorType
        {
            get { return _majortype; }
            set { _majortype = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public int Depth1RegionID
        {
            get { return _depth1regionid; }
            set { _depth1regionid = value; }
        }

        public int Depth2RegionID
        {
            get { return _depth2regionid; }
            set { _depth2regionid = value; }
        }

        public int Depth3RegionID
        {
            get { return _depth3regionid; }
            set { _depth3regionid = value; }
        }

        public int Depth4RegionID
        {
            get { return _depth4regionid; }
            set { _depth4regionid = value; }
        }

        public int SchoolID
        {
            get { return _schoolid; }
            set { _schoolid = value; }
        }

        public string AreaCode
        {
            get { return _areacode; }
            set { _areacode = value; }
        }

        public int EmailCount
        {
            get { return _emailcount; }
            set { _emailcount = value; }
        }

        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public int RelocateFlag
        {
            get { return _relocateflag; }
            set { _relocateflag = value; }
        }

        public int ChildrenCount
        {
            get { return _childrencount; }
            set { _childrencount = value; }
        }

        public DateTime SubscriptionExpirationDate
        {
            get { return _subscriptionexpirationdate; }
            set { _subscriptionexpirationdate = value; }
        }

        public int ActivityLevel
        {
            get { return _activitylevel; }
            set { _activitylevel = value; }
        }

        public int Custody
        {
            get { return _custody; }
            set { _custody = value; }
        }

        public int MoreChildrenFlag
        {
            get { return _morechildrenflag; }
            set { _morechildrenflag = value; }
        }

        public int colorcode
        {
            get { return _colorcode; }
            set { _colorcode = value; }
        }

        public int Online
        {
            get { return _online; }
            set { _online = value; }
        }

        public int RahmahAlum
        {
            get { return _ramahAlum; }
            set { _ramahAlum = value; }
        }

        public int PreferredRelocation
        {
            get { return _preferredRelocation; }
            set { _preferredRelocation = value; }
        }

        public int PreferredMinHeight
        {
            get { return _preferredMinHeight; }
            set { _preferredMinHeight = value; }
        }

        public int PreferredMaxHeight
        {
            get { return _preferredMaxHeight; }
            set { _preferredMaxHeight = value; }
        }

        public int PreferredMinAge
        {
            get { return _preferredMinAge; }
            set { _preferredMinAge = value; }
        }

        public int PreferredMaxAge
        {
            get { return _preferredMaxAge; }
            set { _preferredMaxAge = value; }
        }

        public int PreferredSynagogueAttendance
        {
            get { return _preferredSynagogueAttendance; }
            set { _preferredSynagogueAttendance = value; }
        }

        public int PreferredJdateReligion
        {
            get { return _preferredJdateReligion; }
            set { _preferredJdateReligion = value; }
        }

        public int PreferredSmokingHabit
        {
            get { return _preferredSmokingHabit; }
            set { _preferredSmokingHabit = value; }
        }

        public int PreferredDrinkingHabit
        {
            get { return _preferredDrinkingHabit; }
            set { _preferredDrinkingHabit = value; }
        }

        public int PreferredJdateEthnicity
        {
            get { return _preferredJdateEthnicity; }
            set { _preferredJdateEthnicity = value; }
        }

        public int PreferredActivityLevel
        {
            get { return _preferredActivityLevel; }
            set { _preferredActivityLevel = value; }
        }

        public int PreferredKosherStatus
        {
            get { return _preferredKosherStatus; }
            set { _preferredKosherStatus = value; }
        }

        public int PreferredMaritalStatus
        {
            get { return _preferredMaritalStatus; }
            set { _preferredMaritalStatus = value; }
        }

        public int PreferredLanguageMask
        {
            get { return _preferredLanguageMask; }
            set { _preferredLanguageMask = value; }
        }

        public int PreferredDistance
        {
            get { return _preferredDistance; }
            set { _preferredDistance = value; }
        }

        public int PreferredRegionId
        {
            get { return _preferredRegionID; }
            set { _preferredRegionID = value; }
        }

        public int PreferredGenderMask
        {
            get { return _preferredGenderMask; }
            set { _preferredGenderMask = value; }
        }

        public string PreferredAreaCodes
        {
            get { return _preferredAreaCodes; }
            set { _preferredAreaCodes = value; }
        }

        public int PreferredHasPhotoFlag
        {
            get { return _preferredHasPhotoFlag; }
            set { _preferredHasPhotoFlag = value; }
        }

        public int PreferredEducationLevel
        {
            get { return _preferredEducationLevel; }
            set { _preferredEducationLevel = value; }
        }

        public int PreferredReligion
        {
            get { return _preferredReligion; }
            set { _preferredReligion = value; }
        }

        public int PreferredEthnicity
        {
            get { return _preferredEthnicity; }
            set { _preferredEthnicity = value; }
        }

        public int PreferredSexualIdentityType
        {
            get { return _preferredSexualIdentityType; }
            set { _preferredSexualIdentityType = value; }
        }

        public int PreferredRelationshipMask
        {
            get { return _preferredRelationshipMask; }
            set { _preferredRelationshipMask = value; }
        }

        public int PreferredRelationshipstatus
        {
            get { return _preferredRelationshipstatus; }
            set { _preferredRelationshipstatus = value; }
        }

        public int PreferredBodyType
        {
            get { return _preferredBodyType; }
            set { _preferredBodyType = value; }
        }

        public int PreferredZodiac
        {
            get { return _preferredZodiac; }
            set { _preferredZodiac = value; }
        }

        public int PreferredMajorType
        {
            get { return _preferredMajorType; }
            set { _preferredMajorType = value; }
        }

        public int PreferredMoreChildrenFlag
        {
            get { return _preferredMoreChildrenFlag; }
            set { _preferredMoreChildrenFlag = value; }
        }

        public int PreferredChildrenCount
        {
            get { return _preferredChildrenCount; }
            set { _preferredChildrenCount = value; }
        }

        public int PreferredCustody
        {
            get { return _preferredCustody; }
            set { _preferredCustody = value; }
        }
        #endregion


        public static SearchMember GenerateSearchMember(int memberId, int communityId, int genderMask, DateTime birthDate, 
            DateTime communityInsertDate, DateTime lastActiveDate, int hasPhotoFlag, int educationLevel, int religion, int languageMask,
            int ethnicity, int smokingHabits, int drinkingHabits, int height, int maritalStatus, int jdateReligion, int jdateEthnicity,
            int synagougeAttendance, int keepKosher, int sexualIdentityType, int relationshipMask, int relationshipStatus, int bodyType,
            int zodiac, int majorType, int depth1RegionId, int depth2RegionId, int depth3RegionId, int depth4RegionId, double longitude, double latitude,
            int schoolId, string areaCode, int emailCount, DateTime updateDate, int weight, int relocateFlag, int childrenCount,
            DateTime subscriptionExpirationDate, int activityLevel, int custody, int moreChildrenFlag, int colorCode, int online, int ramahAlum)
        {
            SearchMember searchMember = new SearchMember();
            searchMember.MemberID = memberId;
            searchMember.CommunityID = communityId;
            searchMember.GenderMask = genderMask;
            searchMember.Birthdate = birthDate;
            searchMember.CommunityInsertDate = communityInsertDate;
            searchMember.LastActiveDate = lastActiveDate;
            searchMember.HasPhotoFlag = hasPhotoFlag;
            searchMember.EducationLevel = educationLevel;
            searchMember.Religion = religion;
            searchMember.LanguageMask = languageMask;
            searchMember.Ethnicity = ethnicity;
            searchMember.SmokingHabits = smokingHabits;
            searchMember.DrinkingHabits = drinkingHabits;
            searchMember.Height = height;
            searchMember.MaritalStatus = maritalStatus;
            searchMember.JDateReligion = jdateReligion;
            searchMember.JDateEthnicity = jdateEthnicity;
            searchMember.SynagogueAttendance = synagougeAttendance;
            searchMember.KeepKosher = keepKosher;
            searchMember.SexualIdentityType = sexualIdentityType;
            searchMember.RelationshipMask = relationshipMask;
            searchMember.RelationshipStatus = relationshipStatus;
            searchMember.BodyType = bodyType;
            searchMember.Zodiac = zodiac;
            searchMember.MajorType = majorType;
            searchMember.Depth1RegionID = depth1RegionId;
            searchMember.Depth2RegionID = depth2RegionId;
            searchMember.Depth3RegionID = depth3RegionId;
            searchMember.Depth4RegionID = depth4RegionId;
            searchMember.Longitude = longitude;
            searchMember.Latitude = latitude;
            searchMember.SchoolID = schoolId;
            searchMember.AreaCode = areaCode;
            searchMember.EmailCount = emailCount;
            searchMember.UpdateDate = updateDate;
            searchMember.Weight = weight;
            searchMember.RelocateFlag = relocateFlag;
            searchMember.ChildrenCount = childrenCount;
            searchMember.SubscriptionExpirationDate = subscriptionExpirationDate;
            searchMember.ActivityLevel = activityLevel;
            searchMember.Custody = custody;
            searchMember.MoreChildrenFlag = moreChildrenFlag;
            searchMember.colorcode = colorCode;
            searchMember.Online = online;
            searchMember.RahmahAlum = ramahAlum;

            return searchMember;
        }

        public SearchMember Populate(DataRow rs)
        {
            //columns for search
            if (rs.Table.Columns.Contains("memberid")) MemberID = Conversion.CInt(rs["memberid"]);
            if (rs.Table.Columns.Contains("communityid")) CommunityID = Conversion.CInt(rs["communityid"]);
            if (rs.Table.Columns.Contains("gendermask")) GenderMask = Conversion.CInt(rs["gendermask"]);
            if (rs.Table.Columns.Contains("birthdate")) Birthdate = Conversion.CDateTime(rs["birthdate"]);
            if (rs.Table.Columns.Contains("communityinsertdate")) CommunityInsertDate = Conversion.CDateTime(rs["communityinsertdate"]);
            if (rs.Table.Columns.Contains("lastactivedate")) LastActiveDate = Conversion.CDateTime(rs["lastactivedate"]);
            if (rs.Table.Columns.Contains("hasphotoflag")) HasPhotoFlag = Conversion.CBool(rs["hasphotoflag"]) == true ? 1 : 0;
            if (rs.Table.Columns.Contains("educationlevel")) EducationLevel = Conversion.CInt(rs["educationlevel"]);
            if (rs.Table.Columns.Contains("religion")) Religion = Conversion.CInt(rs["religion"]);
            if (rs.Table.Columns.Contains("languagemask")) LanguageMask = Conversion.CInt(rs["languagemask"]);
            if (rs.Table.Columns.Contains("ethnicity")) Ethnicity = Conversion.CInt(rs["ethnicity"]);
            if (rs.Table.Columns.Contains("smokinghabits")) SmokingHabits = Conversion.CInt(rs["smokinghabits"]);
            if (rs.Table.Columns.Contains("drinkinghabits")) DrinkingHabits = Conversion.CInt(rs["drinkinghabits"]);
            if (rs.Table.Columns.Contains("height")) Height = Conversion.CInt(rs["height"]);
            if (rs.Table.Columns.Contains("maritalstatus")) MaritalStatus = Conversion.CInt(rs["maritalstatus"]);
            if (rs.Table.Columns.Contains("jdatereligion")) JDateReligion = Conversion.CInt(rs["jdatereligion"]);
            if (rs.Table.Columns.Contains("jdateethnicity")) JDateEthnicity = Conversion.CInt(rs["jdateethnicity"]);
            if (rs.Table.Columns.Contains("synagogueattendance")) SynagogueAttendance = Conversion.CInt(rs["synagogueattendance"]);
            if (rs.Table.Columns.Contains("keepkosher")) KeepKosher = Conversion.CInt(rs["keepkosher"]);
            if (rs.Table.Columns.Contains("sexualidentitytype")) SexualIdentityType = Conversion.CInt(rs["sexualidentitytype"]);
            if (rs.Table.Columns.Contains("relationshipmask")) RelationshipMask = Conversion.CInt(rs["relationshipmask"]);
            if (rs.Table.Columns.Contains("relationshipstatus")) RelationshipStatus = Conversion.CInt(rs["relationshipstatus"]);
            if (rs.Table.Columns.Contains("bodytype")) BodyType = Conversion.CInt(rs["bodytype"]);
            if (rs.Table.Columns.Contains("zodiac")) Zodiac = Conversion.CInt(rs["zodiac"]);
            if (rs.Table.Columns.Contains("majortype")) MajorType = Conversion.CInt(rs["majortype"]);
            if (rs.Table.Columns.Contains("depth1regionid")) Depth1RegionID = Conversion.CInt(rs["depth1regionid"]);
            if (rs.Table.Columns.Contains("depth2regionid")) Depth2RegionID = Conversion.CInt(rs["depth2regionid"]);
            if (rs.Table.Columns.Contains("depth3regionid")) Depth3RegionID = Conversion.CInt(rs["depth3regionid"]);
            if (rs.Table.Columns.Contains("depth4regionid")) Depth4RegionID = Conversion.CInt(rs["depth4regionid"]);
            if (rs.Table.Columns.Contains("longitude")) Longitude = Conversion.CDouble(rs["longitude"].ToString());
            if (rs.Table.Columns.Contains("latitude")) Latitude = Conversion.CDouble(rs["latitude"].ToString());
            if (rs.Table.Columns.Contains("schoolid")) SchoolID = Conversion.CInt(rs["schoolid"]);
            if (rs.Table.Columns.Contains("areacode")) AreaCode = rs["areacode"] != null ? rs["areacode"].ToString() : "";
            if (rs.Table.Columns.Contains("emailcount")) EmailCount = Conversion.CInt(rs["emailcount"]);
            if (rs.Table.Columns.Contains("updatedate")) UpdateDate = Conversion.CDateTime(rs["updatedate"]);
            if (rs.Table.Columns.Contains("weight")) Weight = Conversion.CInt(rs["weight"]);
            if (rs.Table.Columns.Contains("relocateflag")) RelocateFlag = Conversion.CInt(rs["relocateflag"]);
            if (rs.Table.Columns.Contains("childrencount")) ChildrenCount = Conversion.CInt(rs["childrencount"]);
            if (rs.Table.Columns.Contains("subscriptionexpirationdate")) SubscriptionExpirationDate = Conversion.CDateTime(rs["subscriptionexpirationdate"]);
            if (rs.Table.Columns.Contains("activitylevel")) ActivityLevel = Conversion.CInt(rs["activitylevel"]);
            if (rs.Table.Columns.Contains("custody")) Custody = Conversion.CInt(rs["custody"]);
            if (rs.Table.Columns.Contains("morechildrenflag")) MoreChildrenFlag = Conversion.CInt(rs["morechildrenflag"]);
            if (rs.Table.Columns.Contains("colorcode")) colorcode = Conversion.CInt(rs["colorcode"]);
            if (rs.Table.Columns.Contains("ramahalum")) RahmahAlum = Conversion.CInt(rs["ramahalum"]);

            //columns for reverse search
            if (rs.Table.Columns.Contains("preferredrelocation")) PreferredRelocation = Conversion.CInt(rs["preferredrelocation"]);
            if (rs.Table.Columns.Contains("preferredminheight")) PreferredMinHeight = Conversion.CInt(rs["preferredminheight"]);
            if (rs.Table.Columns.Contains("preferredmaxheight")) PreferredMaxHeight = Conversion.CInt(rs["preferredmaxheight"]);
            if (rs.Table.Columns.Contains("preferredminage")) PreferredMinAge = Conversion.CInt(rs["preferredminage"]);
            if (rs.Table.Columns.Contains("preferredmaxage")) PreferredMaxAge = Conversion.CInt(rs["preferredmaxage"]);
            if (rs.Table.Columns.Contains("preferredsynagogueattendance")) PreferredSynagogueAttendance = Conversion.CInt(rs["preferredsynagogueattendance"]);
            if (rs.Table.Columns.Contains("preferredjdatereligion")) PreferredJdateReligion = Conversion.CInt(rs["preferredjdatereligion"]);
            if (rs.Table.Columns.Contains("preferredsmokinghabit")) PreferredSmokingHabit = Conversion.CInt(rs["preferredsmokinghabit"]);
            if (rs.Table.Columns.Contains("preferreddrinkinghabit")) PreferredDrinkingHabit = Conversion.CInt(rs["preferreddrinkinghabit"]);
            if (rs.Table.Columns.Contains("preferredjdateethnicity")) PreferredJdateEthnicity = Conversion.CInt(rs["preferredjdateethnicity"]);
            if (rs.Table.Columns.Contains("preferredactivitylevel")) PreferredActivityLevel = Conversion.CInt(rs["preferredactivitylevel"]);
            if (rs.Table.Columns.Contains("preferredkosherstatus")) PreferredKosherStatus = Conversion.CInt(rs["preferredkosherstatus"]);
            if (rs.Table.Columns.Contains("preferredmaritalstatus")) PreferredMaritalStatus = Conversion.CInt(rs["preferredmaritalstatus"]);
            if (rs.Table.Columns.Contains("preferredlanguagemask")) PreferredLanguageMask = Conversion.CInt(rs["preferredlanguagemask"]);
            if (rs.Table.Columns.Contains("preferreddistance")) PreferredDistance = Conversion.CInt(rs["preferreddistance"]);
            if (rs.Table.Columns.Contains("preferredregionid")) PreferredRegionId = Conversion.CInt(rs["preferredregionid"]);
            if (rs.Table.Columns.Contains("preferredgendermask")) PreferredGenderMask = Conversion.CInt(rs["preferredgendermask"]);
            if (rs.Table.Columns.Contains("preferredareacodes")) PreferredAreaCodes = (rs["preferredareacodes"] != null) ? rs["preferredareacodes"].ToString() : "";
            if (rs.Table.Columns.Contains("preferredhasphotoflag")) PreferredHasPhotoFlag = Conversion.CInt(rs["preferredhasphotoflag"]);
            if (rs.Table.Columns.Contains("preferrededucationlevel")) PreferredEducationLevel = Conversion.CInt(rs["preferrededucationlevel"]);
            if (rs.Table.Columns.Contains("preferredreligion")) PreferredReligion = Conversion.CInt(rs["preferredreligion"]);
            if (rs.Table.Columns.Contains("preferredethnicity")) PreferredEthnicity = Conversion.CInt(rs["preferredethnicity"]);
            if (rs.Table.Columns.Contains("preferredsexualidentitytype")) PreferredSexualIdentityType = Conversion.CInt(rs["preferredsexualidentitytype"]);
            if (rs.Table.Columns.Contains("preferredrelationshipmask")) PreferredRelationshipMask = Conversion.CInt(rs["preferredrelationshipmask"]);
            if (rs.Table.Columns.Contains("preferredrelationshipstatus")) PreferredRelationshipstatus = Conversion.CInt(rs["preferredrelationshipstatus"]);
            if (rs.Table.Columns.Contains("preferredbodytype")) PreferredBodyType = Conversion.CInt(rs["preferredbodytype"]);
            if (rs.Table.Columns.Contains("preferredzodiac")) PreferredZodiac = Conversion.CInt(rs["preferredzodiac"]);
            if (rs.Table.Columns.Contains("preferredmajortype")) PreferredMajorType = Conversion.CInt(rs["preferredmajortype"]);
            if (rs.Table.Columns.Contains("preferredmorechildrenflag")) PreferredMoreChildrenFlag = Conversion.CInt(rs["preferredmorechildrenflag"]);
            if (rs.Table.Columns.Contains("preferredchildrencount")) PreferredChildrenCount = Conversion.CInt(rs["preferredchildrencount"]);
            if (rs.Table.Columns.Contains("preferredcustody")) PreferredCustody = Conversion.CInt(rs["preferredcustody"]);
            
            return this;
        }

        public SearchMember Populate(IDataReader dr)
        {
            var columns = dr.GetSchemaTable().Rows.Cast<DataRow>().Select(row => row["ColumnName"].ToString().ToLower()).ToList(); 
            //columns for search
            if (columns.Contains("memberid") && !dr.IsDBNull(dr.GetOrdinal("memberid"))) MemberID = dr.GetInt32(dr.GetOrdinal("memberid"));
            if (columns.Contains("communityid") && !dr.IsDBNull(dr.GetOrdinal("communityid"))) CommunityID = (dr.GetValue(dr.GetOrdinal("communityid")) is Int32) ? dr.GetInt32(dr.GetOrdinal("communityid")) : (int)dr.GetByte(dr.GetOrdinal("communityid"));
            if (columns.Contains("gendermask") && !dr.IsDBNull(dr.GetOrdinal("gendermask"))) GenderMask = (dr.GetValue(dr.GetOrdinal("gendermask")) is Int32) ? dr.GetInt32(dr.GetOrdinal("gendermask")) : (int)dr.GetInt16(dr.GetOrdinal("gendermask"));
            if (columns.Contains("birthdate") && !dr.IsDBNull(dr.GetOrdinal("birthdate"))) Birthdate = dr.GetDateTime(dr.GetOrdinal("birthdate"));
            if (columns.Contains("communityinsertdate") && !dr.IsDBNull(dr.GetOrdinal("communityinsertdate"))) CommunityInsertDate = dr.GetDateTime(dr.GetOrdinal("communityinsertdate"));
            if (columns.Contains("lastactivedate") && !dr.IsDBNull(dr.GetOrdinal("lastactivedate"))) LastActiveDate = dr.GetDateTime(dr.GetOrdinal("lastactivedate"));
            if (columns.Contains("hasphotoflag") && !dr.IsDBNull(dr.GetOrdinal("hasphotoflag"))) HasPhotoFlag = (dr.GetValue(dr.GetOrdinal("hasphotoflag")) is Int32) ? dr.GetInt32(dr.GetOrdinal("hasphotoflag")) : ((dr.GetBoolean(dr.GetOrdinal("hasphotoflag"))) ? 1 : 0);
            if (columns.Contains("educationlevel") && !dr.IsDBNull(dr.GetOrdinal("educationlevel"))) EducationLevel = (dr.GetValue(dr.GetOrdinal("educationlevel")) is Int32) ? dr.GetInt32(dr.GetOrdinal("educationlevel")) : (int)dr.GetInt16(dr.GetOrdinal("educationlevel"));
            if (columns.Contains("religion") && !dr.IsDBNull(dr.GetOrdinal("religion"))) Religion = dr.GetInt32(dr.GetOrdinal("religion"));
            if (columns.Contains("languagemask") && !dr.IsDBNull(dr.GetOrdinal("languagemask"))) LanguageMask = dr.GetInt32(dr.GetOrdinal("languagemask"));
            if (columns.Contains("ethnicity") && !dr.IsDBNull(dr.GetOrdinal("ethnicity"))) Ethnicity = (dr.GetValue(dr.GetOrdinal("ethnicity")) is Int32) ? dr.GetInt32(dr.GetOrdinal("ethnicity")) : (int)dr.GetInt16(dr.GetOrdinal("ethnicity"));
            if (columns.Contains("smokinghabits") && !dr.IsDBNull(dr.GetOrdinal("smokinghabits"))) SmokingHabits = (dr.GetValue(dr.GetOrdinal("smokinghabits")) is Int32) ? dr.GetInt32(dr.GetOrdinal("smokinghabits")) : (int)dr.GetByte(dr.GetOrdinal("smokinghabits"));
            if (columns.Contains("drinkinghabits") && !dr.IsDBNull(dr.GetOrdinal("drinkinghabits"))) DrinkingHabits = (dr.GetValue(dr.GetOrdinal("drinkinghabits")) is Int32) ? dr.GetInt32(dr.GetOrdinal("drinkinghabits")) : (int)dr.GetByte(dr.GetOrdinal("drinkinghabits"));
            if (columns.Contains("height") && !dr.IsDBNull(dr.GetOrdinal("height"))) Height = dr.GetInt32(dr.GetOrdinal("height"));
            if (columns.Contains("maritalstatus") && !dr.IsDBNull(dr.GetOrdinal("maritalstatus"))) MaritalStatus = (dr.GetValue(dr.GetOrdinal("maritalstatus")) is Int32) ? dr.GetInt32(dr.GetOrdinal("maritalstatus")) : (int)dr.GetByte(dr.GetOrdinal("maritalstatus"));
            if (columns.Contains("jdatereligion") && !dr.IsDBNull(dr.GetOrdinal("jdatereligion"))) JDateReligion = (dr.GetValue(dr.GetOrdinal("jdatereligion")) is Int32) ? dr.GetInt32(dr.GetOrdinal("jdatereligion")) : (int)dr.GetInt16(dr.GetOrdinal("jdatereligion"));
            if (columns.Contains("jdateethnicity") && !dr.IsDBNull(dr.GetOrdinal("jdateethnicity"))) JDateEthnicity = (dr.GetValue(dr.GetOrdinal("jdateethnicity")) is Int32) ? dr.GetInt32(dr.GetOrdinal("jdateethnicity")) : (int)dr.GetByte(dr.GetOrdinal("jdateethnicity"));
            if (columns.Contains("synagogueattendance") && !dr.IsDBNull(dr.GetOrdinal("synagogueattendance"))) SynagogueAttendance = (dr.GetValue(dr.GetOrdinal("synagogueattendance")) is Int32) ? dr.GetInt32(dr.GetOrdinal("synagogueattendance")) : (int)dr.GetByte(dr.GetOrdinal("synagogueattendance"));
            if (columns.Contains("keepkosher") && !dr.IsDBNull(dr.GetOrdinal("keepkosher"))) KeepKosher = (dr.GetValue(dr.GetOrdinal("keepkosher")) is Int32) ? dr.GetInt32(dr.GetOrdinal("keepkosher")) : (int)dr.GetByte(dr.GetOrdinal("keepkosher"));
            if (columns.Contains("sexualidentitytype") && !dr.IsDBNull(dr.GetOrdinal("sexualidentitytype"))) SexualIdentityType = (dr.GetValue(dr.GetOrdinal("sexualidentitytype")) is Int32) ? dr.GetInt32(dr.GetOrdinal("sexualidentitytype")) : (int)dr.GetByte(dr.GetOrdinal("sexualidentitytype"));
            if (columns.Contains("relationshipmask") && !dr.IsDBNull(dr.GetOrdinal("relationshipmask"))) RelationshipMask = (dr.GetValue(dr.GetOrdinal("relationshipmask")) is Int32) ? dr.GetInt32(dr.GetOrdinal("relationshipmask")) : (int)dr.GetInt16(dr.GetOrdinal("relationshipmask"));
            if (columns.Contains("relationshipstatus") && !dr.IsDBNull(dr.GetOrdinal("relationshipstatus"))) RelationshipStatus = (dr.GetValue(dr.GetOrdinal("relationshipstatus")) is Int32) ? dr.GetInt32(dr.GetOrdinal("relationshipstatus")) : (int)dr.GetByte(dr.GetOrdinal("relationshipstatus"));
            if (columns.Contains("bodytype") && !dr.IsDBNull(dr.GetOrdinal("bodytype"))) BodyType = dr.GetInt32(dr.GetOrdinal("bodytype"));
            if (columns.Contains("zodiac") && !dr.IsDBNull(dr.GetOrdinal("zodiac"))) Zodiac = dr.GetInt32(dr.GetOrdinal("zodiac"));
            if (columns.Contains("majortype") && !dr.IsDBNull(dr.GetOrdinal("majortype"))) MajorType = (dr.GetValue(dr.GetOrdinal("majortype")) is Int32) ? dr.GetInt32(dr.GetOrdinal("majortype")) : (int)dr.GetInt16(dr.GetOrdinal("majortype"));
            if (columns.Contains("depth1regionid") && !dr.IsDBNull(dr.GetOrdinal("depth1regionid"))) Depth1RegionID = dr.GetInt32(dr.GetOrdinal("depth1regionid"));
            if (columns.Contains("depth2regionid") && !dr.IsDBNull(dr.GetOrdinal("depth2regionid"))) Depth2RegionID = dr.GetInt32(dr.GetOrdinal("depth2regionid"));
            if (columns.Contains("depth3regionid") && !dr.IsDBNull(dr.GetOrdinal("depth3regionid"))) Depth3RegionID = dr.GetInt32(dr.GetOrdinal("depth3regionid"));
            if (columns.Contains("depth4regionid") && !dr.IsDBNull(dr.GetOrdinal("depth4regionid"))) Depth4RegionID = dr.GetInt32(dr.GetOrdinal("depth4regionid"));
            if (columns.Contains("longitude") && !dr.IsDBNull(dr.GetOrdinal("longitude"))) Longitude = (double)dr.GetDecimal(dr.GetOrdinal("longitude"));
            if (columns.Contains("latitude") && !dr.IsDBNull(dr.GetOrdinal("latitude"))) Latitude = (double)dr.GetDecimal(dr.GetOrdinal("latitude"));
            if (columns.Contains("schoolid") && !dr.IsDBNull(dr.GetOrdinal("schoolid"))) SchoolID = dr.GetInt32(dr.GetOrdinal("schoolid"));
            if (columns.Contains("areacode") && !dr.IsDBNull(dr.GetOrdinal("areacode"))) AreaCode = (dr.GetValue(dr.GetOrdinal("areacode")) is Int32) ? dr.GetInt32(dr.GetOrdinal("areacode")).ToString() : dr.GetString(dr.GetOrdinal("areacode"));
            if (columns.Contains("emailcount") && !dr.IsDBNull(dr.GetOrdinal("emailcount"))) EmailCount = (dr.GetValue(dr.GetOrdinal("emailcount")) is Int32) ? dr.GetInt32(dr.GetOrdinal("emailcount")) : (int)dr.GetInt64(dr.GetOrdinal("emailcount"));
            if (columns.Contains("updatedate") && !dr.IsDBNull(dr.GetOrdinal("updatedate"))) UpdateDate = dr.GetDateTime(dr.GetOrdinal("updatedate"));
            if (columns.Contains("weight") && !dr.IsDBNull(dr.GetOrdinal("weight"))) Weight = dr.GetInt32(dr.GetOrdinal("weight"));
            if (columns.Contains("relocateflag") && !dr.IsDBNull(dr.GetOrdinal("relocateflag"))) RelocateFlag = (dr.GetValue(dr.GetOrdinal("relocateflag")) is Int32) ? dr.GetInt32(dr.GetOrdinal("relocateflag")) : (int)dr.GetInt16(dr.GetOrdinal("relocateflag"));
            if (columns.Contains("childrencount") && !dr.IsDBNull(dr.GetOrdinal("childrencount"))) ChildrenCount = dr.GetInt32(dr.GetOrdinal("childrencount"));
            if (columns.Contains("subscriptionexpirationdate") && !dr.IsDBNull(dr.GetOrdinal("subscriptionexpirationdate"))) SubscriptionExpirationDate = dr.GetDateTime(dr.GetOrdinal("subscriptionexpirationdate"));
            if (columns.Contains("activitylevel") && !dr.IsDBNull(dr.GetOrdinal("activitylevel"))) ActivityLevel = dr.GetInt32(dr.GetOrdinal("activitylevel"));
            if (columns.Contains("custody") && !dr.IsDBNull(dr.GetOrdinal("custody"))) Custody = dr.GetInt32(dr.GetOrdinal("custody"));
            if (columns.Contains("morechildrenflag") && !dr.IsDBNull(dr.GetOrdinal("morechildrenflag"))) MoreChildrenFlag = dr.GetInt32(dr.GetOrdinal("morechildrenflag"));
            if (columns.Contains("colorcode") && !dr.IsDBNull(dr.GetOrdinal("colorcode"))) colorcode = dr.GetInt32(dr.GetOrdinal("colorcode"));
            if (columns.Contains("ramahalum") && !dr.IsDBNull(dr.GetOrdinal("ramahalum"))) RahmahAlum = dr.GetInt32(dr.GetOrdinal("ramahalum"));
           
            //columns for reverse search
            if (columns.Contains("preferredrelocation") && !dr.IsDBNull(dr.GetOrdinal("preferredrelocation"))) PreferredRelocation = dr.GetInt32(dr.GetOrdinal("preferredrelocation"));
            if (columns.Contains("preferredminheight") && !dr.IsDBNull(dr.GetOrdinal("preferredminheight"))) PreferredMinHeight = dr.GetInt32(dr.GetOrdinal("preferredminheight"));
            if (columns.Contains("preferredmaxheight") && !dr.IsDBNull(dr.GetOrdinal("preferredmaxheight"))) PreferredMaxHeight = dr.GetInt32(dr.GetOrdinal("preferredmaxheight"));
            if (columns.Contains("preferredminage") && !dr.IsDBNull(dr.GetOrdinal("preferredminage"))) PreferredMinAge = dr.GetInt32(dr.GetOrdinal("preferredminage"));
            if (columns.Contains("preferredmaxage") && !dr.IsDBNull(dr.GetOrdinal("preferredmaxage"))) PreferredMaxAge = dr.GetInt32(dr.GetOrdinal("preferredmaxage"));
            if (columns.Contains("preferredsynagogueattendance") && !dr.IsDBNull(dr.GetOrdinal("preferredsynagogueattendance"))) PreferredSynagogueAttendance = dr.GetInt32(dr.GetOrdinal("preferredsynagogueattendance"));
            if (columns.Contains("preferredjdatereligion") && !dr.IsDBNull(dr.GetOrdinal("preferredjdatereligion"))) PreferredJdateReligion = dr.GetInt32(dr.GetOrdinal("preferredjdatereligion"));
            if (columns.Contains("preferredsmokinghabit") && !dr.IsDBNull(dr.GetOrdinal("preferredsmokinghabit"))) PreferredSmokingHabit = dr.GetInt32(dr.GetOrdinal("preferredsmokinghabit"));
            if (columns.Contains("preferreddrinkinghabit") && !dr.IsDBNull(dr.GetOrdinal("preferreddrinkinghabit"))) PreferredDrinkingHabit = dr.GetInt32(dr.GetOrdinal("preferreddrinkinghabit"));
            if (columns.Contains("preferredjdateethnicity") && !dr.IsDBNull(dr.GetOrdinal("preferredjdateethnicity"))) PreferredJdateEthnicity = dr.GetInt32(dr.GetOrdinal("preferredjdateethnicity"));
            if (columns.Contains("preferredactivitylevel") && !dr.IsDBNull(dr.GetOrdinal("preferredactivitylevel"))) PreferredActivityLevel = dr.GetInt32(dr.GetOrdinal("preferredactivitylevel"));
            if (columns.Contains("preferredkosherstatus") && !dr.IsDBNull(dr.GetOrdinal("preferredkosherstatus"))) PreferredKosherStatus = dr.GetInt32(dr.GetOrdinal("preferredkosherstatus"));
            if (columns.Contains("preferredmaritalstatus") && !dr.IsDBNull(dr.GetOrdinal("preferredmaritalstatus"))) PreferredMaritalStatus = dr.GetInt32(dr.GetOrdinal("preferredmaritalstatus"));
            if (columns.Contains("preferredlanguagemask") && !dr.IsDBNull(dr.GetOrdinal("preferredlanguagemask"))) PreferredLanguageMask = dr.GetInt32(dr.GetOrdinal("preferredlanguagemask"));
            if (columns.Contains("preferreddistance") && !dr.IsDBNull(dr.GetOrdinal("preferreddistance"))) PreferredDistance = dr.GetInt32(dr.GetOrdinal("preferreddistance"));
            if (columns.Contains("preferredregionid") && !dr.IsDBNull(dr.GetOrdinal("preferredregionid"))) PreferredRegionId = dr.GetInt32(dr.GetOrdinal("preferredregionid"));
            if (columns.Contains("preferredgendermask") && !dr.IsDBNull(dr.GetOrdinal("preferredgendermask"))) PreferredGenderMask = dr.GetInt32(dr.GetOrdinal("preferredgendermask"));
            if (columns.Contains("preferredareacodes") && !dr.IsDBNull(dr.GetOrdinal("preferredareacodes"))) PreferredAreaCodes = (dr.GetString(dr.GetOrdinal("preferredareacodes")) != null) ? dr.GetString(dr.GetOrdinal("preferredareacodes")) : "";
            if (columns.Contains("preferredhasphotoflag") && !dr.IsDBNull(dr.GetOrdinal("preferredhasphotoflag"))) PreferredHasPhotoFlag = dr.GetInt32(dr.GetOrdinal("preferredhasphotoflag"));
            if (columns.Contains("preferrededucationlevel") && !dr.IsDBNull(dr.GetOrdinal("preferrededucationlevel"))) PreferredEducationLevel = dr.GetInt32(dr.GetOrdinal("preferrededucationlevel"));
            if (columns.Contains("preferredreligion") && !dr.IsDBNull(dr.GetOrdinal("preferredreligion"))) PreferredReligion = dr.GetInt32(dr.GetOrdinal("preferredreligion"));
            if (columns.Contains("preferredethnicity") && !dr.IsDBNull(dr.GetOrdinal("preferredethnicity"))) PreferredEthnicity = dr.GetInt32(dr.GetOrdinal("preferredethnicity"));
            if (columns.Contains("preferredsexualidentitytype") && !dr.IsDBNull(dr.GetOrdinal("preferredsexualidentitytype"))) PreferredSexualIdentityType = dr.GetInt32(dr.GetOrdinal("preferredsexualidentitytype"));
            if (columns.Contains("preferredrelationshipmask") && !dr.IsDBNull(dr.GetOrdinal("preferredrelationshipmask"))) PreferredRelationshipMask = dr.GetInt32(dr.GetOrdinal("preferredrelationshipmask"));
            if (columns.Contains("preferredrelationshipstatus") && !dr.IsDBNull(dr.GetOrdinal("preferredrelationshipstatus"))) PreferredRelationshipstatus = dr.GetInt32(dr.GetOrdinal("preferredrelationshipstatus"));
            if (columns.Contains("preferredbodytype") && !dr.IsDBNull(dr.GetOrdinal("preferredbodytype"))) PreferredBodyType = dr.GetInt32(dr.GetOrdinal("preferredbodytype"));
            if (columns.Contains("preferredzodiac") && !dr.IsDBNull(dr.GetOrdinal("preferredzodiac"))) PreferredZodiac = dr.GetInt32(dr.GetOrdinal("preferredzodiac"));
            if (columns.Contains("preferredmajortype") && !dr.IsDBNull(dr.GetOrdinal("preferredmajortype"))) PreferredMajorType = dr.GetInt32(dr.GetOrdinal("preferredmajortype"));
            if (columns.Contains("preferredmorechildrenflag") && !dr.IsDBNull(dr.GetOrdinal("preferredmorechildrenflag"))) PreferredMoreChildrenFlag = dr.GetInt32(dr.GetOrdinal("preferredmorechildrenflag"));
            if (columns.Contains("preferredchildrencount") && !dr.IsDBNull(dr.GetOrdinal("preferredchildrencount"))) PreferredChildrenCount = dr.GetInt32(dr.GetOrdinal("preferredchildrencount"));
            if (columns.Contains("preferredcustody") && !dr.IsDBNull(dr.GetOrdinal("preferredcustody"))) PreferredCustody = dr.GetInt32(dr.GetOrdinal("preferredcustody"));
            return this;
        }
    }

    #endregion


}
