﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Matchnet;

namespace Spark.SearchEngine.ValueObjects
{
    public class SearchAdmin
    {
        public int MemberId { get; set; }
        public int CommunityId { get; set; }
        public DateTime LastActiveDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime CommunityInsertDate { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int Depth1RegionId { get; set; }
        public int Depth2RegionId { get; set; }
        public int Depth3RegionId { get; set; }
        public int Depth4RegionId { get; set; }
        public int IPAddress { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public UpdateTypeEnum UpdateType { get; set; }

        public SearchAdmin Populate(DataRow rs)
        {
            if (rs.Table.Columns.Contains("memberid")) MemberId = Conversion.CInt(rs["memberid"]);
            if (rs.Table.Columns.Contains("communityid")) CommunityId = Conversion.CInt(rs["communityid"]);
            if (rs.Table.Columns.Contains("communityinsertdate")) CommunityInsertDate = Conversion.CDateTime(rs["communityinsertdate"]);
            if (rs.Table.Columns.Contains("lastactivedate")) LastActiveDate = Conversion.CDateTime(rs["lastactivedate"]);
            if (rs.Table.Columns.Contains("updatedate")) UpdateDate = Conversion.CDateTime(rs["updatedate"]);
            if (rs.Table.Columns.Contains("depth1regionid")) Depth1RegionId = Conversion.CInt(rs["depth1regionid"]);
            if (rs.Table.Columns.Contains("depth2regionid")) Depth2RegionId = Conversion.CInt(rs["depth2regionid"]);
            if (rs.Table.Columns.Contains("depth3regionid")) Depth3RegionId = Conversion.CInt(rs["depth3regionid"]);
            if (rs.Table.Columns.Contains("depth4regionid")) Depth4RegionId = Conversion.CInt(rs["depth4regionid"]);
            if (rs.Table.Columns.Contains("longitude")) Longitude = Conversion.CDouble(rs["longitude"].ToString());
            if (rs.Table.Columns.Contains("latitude")) Latitude = Conversion.CDouble(rs["latitude"].ToString());
            if (rs.Table.Columns.Contains("ipaddress")) IPAddress = (rs["ipaddress"] != null) ? Conversion.CInt(rs["ipaddress"]) : Constants.NULL_INT;
            if (rs.Table.Columns.Contains("firstname")) FirstName = (rs["firstname"] != null) ? rs["firstname"].ToString() : "";
            if (rs.Table.Columns.Contains("lastname")) LastName = (rs["lastname"] != null) ? rs["lastname"].ToString() : "";
            if (rs.Table.Columns.Contains("username")) Username = (rs["username"] != null) ? rs["username"].ToString() : "";
            if (rs.Table.Columns.Contains("emailaddress")) EmailAddress = (rs["emailaddress"] != null) ? rs["emailaddress"].ToString() : "";

            return this;
        }

        public SearchAdmin Populate(IDataReader dr)
        {
            if (!dr.IsDBNull(dr.GetOrdinal("memberid"))) MemberId = dr.GetInt32(dr.GetOrdinal("memberid"));
            if (!dr.IsDBNull(dr.GetOrdinal("communityid"))) CommunityId = dr.GetInt32(dr.GetOrdinal("communityid"));
            if (!dr.IsDBNull(dr.GetOrdinal("communityinsertdate"))) CommunityInsertDate = dr.GetDateTime(dr.GetOrdinal("communityinsertdate"));
            if (!dr.IsDBNull(dr.GetOrdinal("updatedate"))) UpdateDate = dr.GetDateTime(dr.GetOrdinal("updatedate"));
            if (!dr.IsDBNull(dr.GetOrdinal("lastactivedate"))) LastActiveDate = dr.GetDateTime(dr.GetOrdinal("lastactivedate"));
            if (!dr.IsDBNull(dr.GetOrdinal("depth1regionid"))) Depth1RegionId = dr.GetInt32(dr.GetOrdinal("depth1regionid"));
            if (!dr.IsDBNull(dr.GetOrdinal("depth2regionid"))) Depth2RegionId = dr.GetInt32(dr.GetOrdinal("depth2regionid"));
            if (!dr.IsDBNull(dr.GetOrdinal("depth3regionid"))) Depth3RegionId = dr.GetInt32(dr.GetOrdinal("depth3regionid"));
            if (!dr.IsDBNull(dr.GetOrdinal("depth4regionid"))) Depth4RegionId = dr.GetInt32(dr.GetOrdinal("depth4regionid"));
            if (!dr.IsDBNull(dr.GetOrdinal("longitude"))) Longitude = dr.GetDouble(dr.GetOrdinal("longitude"));
            if (!dr.IsDBNull(dr.GetOrdinal("latitude"))) Latitude = dr.GetDouble(dr.GetOrdinal("latitude"));
            IPAddress = (!dr.IsDBNull(dr.GetOrdinal("ipaddress"))) ? dr.GetInt32(dr.GetOrdinal("ipaddress")) : Constants.NULL_INT;
            FirstName = (!dr.IsDBNull(dr.GetOrdinal("firstname"))) ?  dr.GetString(dr.GetOrdinal("firstname")) : "";
            LastName =  (!dr.IsDBNull(dr.GetOrdinal("lastname"))) ?  dr.GetString(dr.GetOrdinal("lastname")) : "";
            Username = (!dr.IsDBNull(dr.GetOrdinal("username"))) ? dr.GetString(dr.GetOrdinal("username")) : "";
            EmailAddress = (!dr.IsDBNull(dr.GetOrdinal("emailaddress"))) ? dr.GetString(dr.GetOrdinal("emailaddress")) : "";

            return this;
        }

    }
}
