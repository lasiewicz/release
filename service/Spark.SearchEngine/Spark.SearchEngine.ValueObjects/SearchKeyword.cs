﻿using System;
using System.Data;
using System.Linq;
using Matchnet;
namespace Spark.SearchEngine.ValueObjects
{
    #region class SearchKeyword
    [Serializable]
    public class SearchKeyword
    {

        #region private
        private int _memberid;
        private int _communityid;
        private int _genderMask;
        private DateTime _lastactivedate;
        private DateTime _updatedate;
        private string _aboutMe;
        private string _areaCode;
        private double _longitude;
        private double _latitude;
        private int _depth1regionId;
        private int _depth2regionId;
        private int _depth3regionId;
        private int _depth4regionId;
        private string _perfectMatchEssay;
        private string _perfectFirstDateEssay;
        private string _idealRelationshipEssay;
        private string _learnFromThePastEssay;
        private string _lifeAndAbmitionEssay;
        private string _historyOfMyLifeEssay;
        private string _onOurFirstDateRemindMeToEssay;
        private string _thingsCantLiveWithoutEssay;
        private string _favoriteBooksMoviesEtcEssay;
        private string _coolestPlacesVisitedEssay;
        private string _forFunILikeToEssay;
        private string _onFriSatITypicallyEssay;
        private string _messageMeIfYouEssay;
        private string _cuisine;
        private string _entertainmentLocation;
        private string _leisureActivity;
        private string _music;
        private string _personalityTrait;
        private string _pets;
        private string _physicalActivity;
        private string _reading;
        private string _essayFirstDate;
        private string _essayGetToKnowMe;
        private string _essayPastRelationship;
        private string _essayImportantThings;
        private string _essayPerfectDay;
        private string _essayMoreThingsToAdd;
        private string _favoriteBands;
        private string _favoriteMovies;
        private string _favoriteTV;
        private string _favoriteRestaurant;
        private string _schoolsAttended;
        private string _myGreatTripIdea;
        private string _movieGenres;

        private UpdateReasonEnum _UpdateReason = UpdateReasonEnum.none;
        private UpdateModeEnum _UpdateMode = UpdateModeEnum.none;
        #endregion
        #region properties

        //for compatibility with e1loader updates
        public UpdateTypeEnum UpdateType { get; set; }
        public UpdateReasonEnum UpdateReason
        {
            get { return _UpdateReason; }
            set { _UpdateReason = value; }
        }
        public UpdateModeEnum UpdateMode
        {
            get { return _UpdateMode; }
            set { _UpdateMode = value; }
        }

        public int MemberID
        { 
            get { return _memberid;}
            set {_memberid = value;} 
        }

        public int CommunityID
        {
            get { return _communityid; }
            set { _communityid = value; }
        }

        public int GenderMask
        {
            get { return _genderMask; }
            set { _genderMask = value; }
        }

        public DateTime LastActiveDate
        {
            get { return _lastactivedate; }
            set { _lastactivedate = value; }
        }

        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }

        public string AboutMe
        {
            get { return _aboutMe; }
            set { _aboutMe = value; }
        }

        public string PerfectMatchEssay
        {
            get { return _perfectMatchEssay; }
            set { _perfectMatchEssay = value; }
        }

        public string PerfectFirstDateEssay
        {
            get { return _perfectFirstDateEssay; }
            set { _perfectFirstDateEssay = value; }
        }

        public string IdealRelationshipEssay
        {
            get { return _idealRelationshipEssay; }
            set { _idealRelationshipEssay = value; }
        }

        public string LearnFromThePastEssay
        {
            get { return _learnFromThePastEssay; }
            set { _learnFromThePastEssay = value; }
        }

        public string LifeAndAbmitionEssay
        {
            get { return _lifeAndAbmitionEssay; }
            set { _lifeAndAbmitionEssay = value; }
        }

        public string HistoryOfMyLifeEssay
        {
            get { return _historyOfMyLifeEssay; }
            set { _historyOfMyLifeEssay = value; }
        }

        public string OnOurFirstDateRemindMeToEssay
        {
            get { return _onOurFirstDateRemindMeToEssay; }
            set { _onOurFirstDateRemindMeToEssay = value; }
        }

        public string ThingsCantLiveWithoutEssay
        {
            get { return _thingsCantLiveWithoutEssay; }
            set { _thingsCantLiveWithoutEssay = value; }
        }

        public string FavoriteBooksMoviesEtcEssay
        {
            get { return _favoriteBooksMoviesEtcEssay; }
            set { _favoriteBooksMoviesEtcEssay = value; }
        }

        public string CoolestPlacesVisitedEssay
        {
            get { return _coolestPlacesVisitedEssay; }
            set { _coolestPlacesVisitedEssay = value; }
        }

        public string ForFunILikeToEssay
        {
            get { return _forFunILikeToEssay; }
            set { _forFunILikeToEssay = value; }
        }

        public string OnFriSatITypicallyEssay
        {
            get { return _onFriSatITypicallyEssay; }
            set { _onFriSatITypicallyEssay = value; }
        }

        public string MessageMeIfYouEssay
        {
            get { return _messageMeIfYouEssay; }
            set { _messageMeIfYouEssay = value; }
        }

        public string Cuisine
        {
            get { return _cuisine; }
            set { _cuisine = value; }
        }

        public string EntertainmentLocation
        {
            get { return _entertainmentLocation; }
            set { _entertainmentLocation = value; }
        }

        public string LeisureActivity
        {
            get { return _leisureActivity; }
            set { _leisureActivity = value; }
        }

        public string Music
        {
            get { return _music; }
            set { _music = value; }
        }

        public string PersonalityTrait
        {
            get { return _personalityTrait; }
            set { _personalityTrait = value; }
        }

        public string Pets
        {
            get { return _pets; }
            set { _pets = value; }
        }

        public string PhysicalActivity
        {
            get { return _physicalActivity; }
            set { _physicalActivity = value; }
        }

        public string Reading
        {
            get { return _reading; }
            set { _reading = value; }
        }

        public string EssayFirstDate
        {
            get { return _essayFirstDate; }
            set { _essayFirstDate = value; }
        }

        public string EssayGetToKnowMe
        {
            get { return _essayGetToKnowMe; }
            set { _essayGetToKnowMe = value; }
        }

        public string EssayPastRelationship
        {
            get { return _essayPastRelationship; }
            set { _essayPastRelationship = value; }
        }

        public string EssayImportantThings
        {
            get { return _essayImportantThings; }
            set { _essayImportantThings = value; }
        }

        public string EssayPerfectDay
        {
            get { return _essayPerfectDay; }
            set { _essayPerfectDay = value; }
        }

        public string EssayMoreThingsToAdd
        {
            get { return _essayMoreThingsToAdd; }
            set { _essayMoreThingsToAdd = value; }
        }

        public string FavoriteBands
        {
            get { return _favoriteBands; }
            set { _favoriteBands = value; }
        }

        public string FavoriteMovies
        {
            get { return _favoriteMovies; }
            set { _favoriteMovies = value; }
        }

        public string FavoriteTv
        {
            get { return _favoriteTV; }
            set { _favoriteTV = value; }
        }

        public string FavoriteRestaurant
        {
            get { return _favoriteRestaurant; }
            set { _favoriteRestaurant = value; }
        }

        public string SchoolsAttended
        {
            get { return _schoolsAttended; }
            set { _schoolsAttended = value; }
        }

        public string MyGreatTripIdea
        {
            get { return _myGreatTripIdea; }
            set { _myGreatTripIdea = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public int Depth1RegionId
        {
            get { return _depth1regionId; }
            set { _depth1regionId = value; }
        }

        public int Depth2RegionId
        {
            get { return _depth2regionId; }
            set { _depth2regionId = value; }
        }

        public int Depth3RegionId
        {
            get { return _depth3regionId; }
            set { _depth3regionId = value; }
        }

        public int Depth4RegionId
        {
            get { return _depth4regionId; }
            set { _depth4regionId = value; }
        }

        public string AreaCode
        {
            get { return _areaCode; }
            set { _areaCode = value; }
        }

        public string MovieGenres
        {
            get { return _movieGenres; }
            set { _movieGenres = value; }
        }
        #endregion


        public static SearchKeyword GenerateSearchKeyword(int memberId, int communityId, DateTime lastActiveDate, DateTime updateDate, string aboutMe)
        {
            SearchKeyword searchKeyword = new SearchKeyword();
            searchKeyword.MemberID = memberId;
            searchKeyword.CommunityID = communityId;
            searchKeyword.LastActiveDate = lastActiveDate;
            searchKeyword.UpdateDate = updateDate;
            searchKeyword.AboutMe = aboutMe;

            return searchKeyword;
        }

        public SearchKeyword Populate(DataRow rs)
        {
            //columns for keyword search
            if (rs.Table.Columns.Contains("memberid")) MemberID = Conversion.CInt(rs["memberid"]);
            if (rs.Table.Columns.Contains("communityid")) CommunityID = Conversion.CInt(rs["communityid"]);
            if (rs.Table.Columns.Contains("gendermask")) GenderMask = Conversion.CInt(rs["gendermask"]);
            if (rs.Table.Columns.Contains("lastactivedate")) LastActiveDate = Conversion.CDateTime(rs["lastactivedate"]);
            if (rs.Table.Columns.Contains("updatedate")) UpdateDate = Conversion.CDateTime(rs["updatedate"]);
            if (rs.Table.Columns.Contains("aboutme")) AboutMe = (rs["aboutme"] != null) ? rs["aboutme"].ToString() : "";
            if (rs.Table.Columns.Contains("areacode")) AreaCode = rs["areacode"] != null ? rs["areacode"].ToString() : "";
            if (rs.Table.Columns.Contains("longitude")) Longitude = Conversion.CDouble(rs["longitude"].ToString());
            if (rs.Table.Columns.Contains("latitude")) Latitude = Conversion.CDouble(rs["latitude"].ToString());
            if (rs.Table.Columns.Contains("depth1regionid")) Depth1RegionId = Conversion.CInt(rs["depth1regionid"]);
            if (rs.Table.Columns.Contains("depth2regionid")) Depth2RegionId = Conversion.CInt(rs["depth2regionid"]);
            if (rs.Table.Columns.Contains("depth3regionid")) Depth3RegionId = Conversion.CInt(rs["depth3regionid"]);
            if (rs.Table.Columns.Contains("depth4regionid")) Depth4RegionId = Conversion.CInt(rs["depth4regionid"]);
            if (rs.Table.Columns.Contains("perfectmatchessay")) PerfectMatchEssay = (rs["perfectmatchessay"] != null) ? rs["perfectmatchessay"].ToString() : "";
            if (rs.Table.Columns.Contains("perfectfirstdateessay")) PerfectFirstDateEssay = (rs["perfectfirstdateessay"] != null) ? rs["perfectfirstdateessay"].ToString() : "";
            if (rs.Table.Columns.Contains("idealrelationshipessay")) IdealRelationshipEssay = (rs["idealrelationshipessay"] != null) ? rs["idealrelationshipessay"].ToString() : "";
            if (rs.Table.Columns.Contains("learnfromthepastessay")) LearnFromThePastEssay = (rs["learnfromthepastessay"] != null) ? rs["learnfromthepastessay"].ToString() : "";
            if (rs.Table.Columns.Contains("lifeandabmitionessay")) LifeAndAbmitionEssay = (rs["lifeandabmitionessay"] != null) ? rs["lifeandabmitionessay"].ToString() : "";
            if (rs.Table.Columns.Contains("historyofmylifeessay")) HistoryOfMyLifeEssay = (rs["historyofmylifeessay"] != null) ? rs["historyofmylifeessay"].ToString() : "";
            if (rs.Table.Columns.Contains("onourfirstdateremindmetoessay")) OnOurFirstDateRemindMeToEssay = (rs["onourfirstdateremindmetoessay"] != null) ? rs["onourfirstdateremindmetoessay"].ToString() : "";
            if (rs.Table.Columns.Contains("thingscantlivewithoutessay")) ThingsCantLiveWithoutEssay = (rs["thingscantlivewithoutessay"] != null) ? rs["thingscantlivewithoutessay"].ToString() : "";
            if (rs.Table.Columns.Contains("favoritebooksmoviesetcessay")) FavoriteBooksMoviesEtcEssay = (rs["favoritebooksmoviesetcessay"] != null) ? rs["favoritebooksmoviesetcessay"].ToString() : "";
            if (rs.Table.Columns.Contains("coolestplacesvisitedessay")) CoolestPlacesVisitedEssay = (rs["coolestplacesvisitedessay"] != null) ? rs["coolestplacesvisitedessay"].ToString() : "";
            if (rs.Table.Columns.Contains("forfuniliketoessay")) ForFunILikeToEssay = (rs["forfuniliketoessay"] != null) ? rs["forfuniliketoessay"].ToString() : "";
            if (rs.Table.Columns.Contains("onfrisatitypicallyessay")) OnFriSatITypicallyEssay = (rs["onfrisatitypicallyessay"] != null) ? rs["onfrisatitypicallyessay"].ToString() : "";
            if (rs.Table.Columns.Contains("messagemeifyouessay")) MessageMeIfYouEssay = (rs["messagemeifyouessay"] != null) ? rs["messagemeifyouessay"].ToString() : "";
            if (rs.Table.Columns.Contains("cuisine")) Cuisine = (rs["cuisine"] != null) ? rs["cuisine"].ToString() : "";
            if (rs.Table.Columns.Contains("entertainmentlocation")) EntertainmentLocation = (rs["entertainmentlocation"] != null) ? rs["entertainmentlocation"].ToString() : "";
            if (rs.Table.Columns.Contains("leisureactivity")) LeisureActivity = (rs["leisureactivity"] != null) ? rs["leisureactivity"].ToString() : "";
            if (rs.Table.Columns.Contains("music")) Music = (rs["music"] != null) ? rs["music"].ToString() : "";
            if (rs.Table.Columns.Contains("personalitytrait")) PersonalityTrait = (rs["personalitytrait"] != null) ? rs["personalitytrait"].ToString() : "";
            if (rs.Table.Columns.Contains("pets")) Pets = (rs["pets"] != null) ? rs["pets"].ToString() : "";
            if (rs.Table.Columns.Contains("physicalactivity")) PhysicalActivity = (rs["physicalactivity"] != null) ? rs["physicalactivity"].ToString() : "";
            if (rs.Table.Columns.Contains("reading")) Reading = (rs["reading"] != null) ? rs["reading"].ToString() : "";
            if (rs.Table.Columns.Contains("essayfirstdate")) EssayFirstDate = (rs["essayfirstdate"] != null) ? rs["essayfirstdate"].ToString() : "";
            if (rs.Table.Columns.Contains("essaygettoknowme")) EssayGetToKnowMe = (rs["essaygettoknowme"] != null) ? rs["essaygettoknowme"].ToString() : "";
            if (rs.Table.Columns.Contains("essaypastrelationship")) EssayPastRelationship = (rs["essaypastrelationship"] != null) ? rs["essaypastrelationship"].ToString() : "";
            if (rs.Table.Columns.Contains("essayimportantthings")) EssayImportantThings = (rs["essayimportantthings"] != null) ? rs["essayimportantthings"].ToString() : "";
            if (rs.Table.Columns.Contains("essayperfectday")) EssayPerfectDay = (rs["essayperfectday"] != null) ? rs["essayperfectday"].ToString() : "";
            if (rs.Table.Columns.Contains("essaymorethingstoadd")) EssayMoreThingsToAdd = (rs["essaymorethingstoadd"] != null) ? rs["essaymorethingstoadd"].ToString() : "";
            if (rs.Table.Columns.Contains("favoritebands")) FavoriteBands = (rs["favoritebands"] != null) ? rs["favoritebands"].ToString() : "";
            if (rs.Table.Columns.Contains("favoritemovies")) FavoriteMovies = (rs["favoritemovies"] != null) ? rs["favoritemovies"].ToString() : "";
            if (rs.Table.Columns.Contains("favoritetv")) FavoriteTv = (rs["favoritetv"] != null) ? rs["favoritetv"].ToString() : "";
            if (rs.Table.Columns.Contains("favoriterestaurant")) FavoriteRestaurant = (rs["favoriterestaurant"] != null) ? rs["favoriterestaurant"].ToString() : "";
            if (rs.Table.Columns.Contains("schoolsattended")) SchoolsAttended = (rs["schoolsattended"] != null) ? rs["schoolsattended"].ToString() : "";
            if (rs.Table.Columns.Contains("mygreattripidea")) MyGreatTripIdea = (rs["mygreattripidea"] != null) ? rs["mygreattripidea"].ToString() : "";
            if (rs.Table.Columns.Contains("moviegenre")) MovieGenres = (rs["moviegenre"] != null) ? rs["moviegenre"].ToString() : "";
            return this;
        }

        public SearchKeyword Populate(IDataReader dr)
        {
            var columns = dr.GetSchemaTable().Rows.Cast<DataRow>().Select(row => row["ColumnName"].ToString().ToLower()).ToList();
            //columns for keyword search
            if (columns.Contains("memberid") && !dr.IsDBNull(dr.GetOrdinal("memberid"))) MemberID = dr.GetInt32(dr.GetOrdinal("memberid"));
            if (columns.Contains("communityid") && !dr.IsDBNull(dr.GetOrdinal("communityid"))) CommunityID = (dr.GetValue(dr.GetOrdinal("communityid")) is Int32) ? dr.GetInt32(dr.GetOrdinal("communityid")) : (int)dr.GetByte(dr.GetOrdinal("communityid"));
            if (columns.Contains("gendermask") && !dr.IsDBNull(dr.GetOrdinal("gendermask"))) GenderMask = (dr.GetValue(dr.GetOrdinal("gendermask")) is Int32) ? dr.GetInt32(dr.GetOrdinal("gendermask")) : (int)dr.GetInt16(dr.GetOrdinal("gendermask"));
            if (columns.Contains("lastactivedate") && !dr.IsDBNull(dr.GetOrdinal("lastactivedate"))) LastActiveDate = dr.GetDateTime(dr.GetOrdinal("lastactivedate"));
            if (columns.Contains("updatedate") && !dr.IsDBNull(dr.GetOrdinal("updatedate"))) UpdateDate = dr.GetDateTime(dr.GetOrdinal("updatedate"));
            if (columns.Contains("areacode") && !dr.IsDBNull(dr.GetOrdinal("areacode"))) AreaCode = (dr.GetValue(dr.GetOrdinal("areacode")) is Int32) ? dr.GetInt32(dr.GetOrdinal("areacode")).ToString() : dr.GetString(dr.GetOrdinal("areacode"));
            if (columns.Contains("depth1regionid") && !dr.IsDBNull(dr.GetOrdinal("depth1regionid"))) Depth1RegionId = dr.GetInt32(dr.GetOrdinal("depth1regionid"));
            if (columns.Contains("depth2regionid") && !dr.IsDBNull(dr.GetOrdinal("depth2regionid"))) Depth2RegionId = dr.GetInt32(dr.GetOrdinal("depth2regionid"));
            if (columns.Contains("depth3regionid") && !dr.IsDBNull(dr.GetOrdinal("depth3regionid"))) Depth3RegionId = dr.GetInt32(dr.GetOrdinal("depth3regionid"));
            if (columns.Contains("depth4regionid") && !dr.IsDBNull(dr.GetOrdinal("depth4regionid"))) Depth4RegionId = dr.GetInt32(dr.GetOrdinal("depth4regionid"));
            if (columns.Contains("longitude") && !dr.IsDBNull(dr.GetOrdinal("longitude"))) Longitude = (double)dr.GetDecimal(dr.GetOrdinal("longitude"));
            if (columns.Contains("latitude") && !dr.IsDBNull(dr.GetOrdinal("latitude"))) Latitude = (double)dr.GetDecimal(dr.GetOrdinal("latitude"));

            if (columns.Contains("aboutme") && !dr.IsDBNull(dr.GetOrdinal("aboutme"))) AboutMe = (dr.GetString(dr.GetOrdinal("aboutme")) != null) ? dr.GetString(dr.GetOrdinal("aboutme")) : "";
            if (columns.Contains("perfectmatchessay") && !dr.IsDBNull(dr.GetOrdinal("perfectmatchessay"))) PerfectMatchEssay = (dr.GetString(dr.GetOrdinal("perfectmatchessay")) != null) ? dr.GetString(dr.GetOrdinal("perfectmatchessay")) : "";
            if (columns.Contains("perfectfirstdateessay") && !dr.IsDBNull(dr.GetOrdinal("perfectfirstdateessay"))) PerfectFirstDateEssay = (dr.GetString(dr.GetOrdinal("perfectfirstdateessay")) != null) ? dr.GetString(dr.GetOrdinal("perfectfirstdateessay")) : "";
            if (columns.Contains("idealrelationshipessay") && !dr.IsDBNull(dr.GetOrdinal("idealrelationshipessay"))) IdealRelationshipEssay = (dr.GetString(dr.GetOrdinal("idealrelationshipessay")) != null) ? dr.GetString(dr.GetOrdinal("idealrelationshipessay")) : "";
            if (columns.Contains("learnfromthepastessay") && !dr.IsDBNull(dr.GetOrdinal("learnfromthepastessay"))) LearnFromThePastEssay = (dr.GetString(dr.GetOrdinal("learnfromthepastessay")) != null) ? dr.GetString(dr.GetOrdinal("learnfromthepastessay")) : "";
            if (columns.Contains("lifeandabmitionessay") && !dr.IsDBNull(dr.GetOrdinal("lifeandabmitionessay"))) LifeAndAbmitionEssay = (dr.GetString(dr.GetOrdinal("lifeandabmitionessay")) != null) ? dr.GetString(dr.GetOrdinal("lifeandabmitionessay")) : "";
            if (columns.Contains("historyofmylifeessay") && !dr.IsDBNull(dr.GetOrdinal("historyofmylifeessay"))) HistoryOfMyLifeEssay = (dr.GetString(dr.GetOrdinal("historyofmylifeessay")) != null) ? dr.GetString(dr.GetOrdinal("historyofmylifeessay")) : "";
            if (columns.Contains("onourfirstdateremindmetoessay") && !dr.IsDBNull(dr.GetOrdinal("onourfirstdateremindmetoessay"))) OnOurFirstDateRemindMeToEssay = (dr.GetString(dr.GetOrdinal("onourfirstdateremindmetoessay")) != null) ? dr.GetString(dr.GetOrdinal("onourfirstdateremindmetoessay")) : "";
            if (columns.Contains("thingscantlivewithoutessay") && !dr.IsDBNull(dr.GetOrdinal("thingscantlivewithoutessay"))) ThingsCantLiveWithoutEssay = (dr.GetString(dr.GetOrdinal("thingscantlivewithoutessay")) != null) ? dr.GetString(dr.GetOrdinal("thingscantlivewithoutessay")) : "";
            if (columns.Contains("favoritebooksmoviesetcessay") && !dr.IsDBNull(dr.GetOrdinal("favoritebooksmoviesetcessay"))) FavoriteBooksMoviesEtcEssay = (dr.GetString(dr.GetOrdinal("favoritebooksmoviesetcessay")) != null) ? dr.GetString(dr.GetOrdinal("favoritebooksmoviesetcessay")) : "";
            if (columns.Contains("coolestplacesvisitedessay") && !dr.IsDBNull(dr.GetOrdinal("coolestplacesvisitedessay"))) CoolestPlacesVisitedEssay = (dr.GetString(dr.GetOrdinal("coolestplacesvisitedessay")) != null) ? dr.GetString(dr.GetOrdinal("coolestplacesvisitedessay")) : "";
            if (columns.Contains("forfuniliketoessay") && !dr.IsDBNull(dr.GetOrdinal("forfuniliketoessay"))) ForFunILikeToEssay = (dr.GetString(dr.GetOrdinal("forfuniliketoessay")) != null) ? dr.GetString(dr.GetOrdinal("forfuniliketoessay")) : "";
            if (columns.Contains("onfrisatitypicallyessay") && !dr.IsDBNull(dr.GetOrdinal("onfrisatitypicallyessay"))) OnFriSatITypicallyEssay = (dr.GetString(dr.GetOrdinal("onfrisatitypicallyessay")) != null) ? dr.GetString(dr.GetOrdinal("onfrisatitypicallyessay")) : "";
            if (columns.Contains("messagemeifyouessay") && !dr.IsDBNull(dr.GetOrdinal("messagemeifyouessay"))) MessageMeIfYouEssay = (dr.GetString(dr.GetOrdinal("messagemeifyouessay")) != null) ? dr.GetString(dr.GetOrdinal("messagemeifyouessay")) : "";
            if (columns.Contains("cuisine") && !dr.IsDBNull(dr.GetOrdinal("cuisine"))) Cuisine = (dr.GetString(dr.GetOrdinal("cuisine")) != null) ? dr.GetString(dr.GetOrdinal("cuisine")) : "";
            if (columns.Contains("entertainmentlocation") && !dr.IsDBNull(dr.GetOrdinal("entertainmentlocation"))) EntertainmentLocation = (dr.GetString(dr.GetOrdinal("entertainmentlocation")) != null) ? dr.GetString(dr.GetOrdinal("entertainmentlocation")) : "";
            if (columns.Contains("leisureactivity") && !dr.IsDBNull(dr.GetOrdinal("leisureactivity"))) LeisureActivity = (dr.GetString(dr.GetOrdinal("leisureactivity")) != null) ? dr.GetString(dr.GetOrdinal("leisureactivity")) : "";
            if (columns.Contains("music") && !dr.IsDBNull(dr.GetOrdinal("music"))) Music = (dr.GetString(dr.GetOrdinal("music")) != null) ? dr.GetString(dr.GetOrdinal("music")) : "";
            if (columns.Contains("personalitytrait") && !dr.IsDBNull(dr.GetOrdinal("personalitytrait"))) PersonalityTrait = (dr.GetString(dr.GetOrdinal("personalitytrait")) != null) ? dr.GetString(dr.GetOrdinal("personalitytrait")) : "";
            if (columns.Contains("pets") && !dr.IsDBNull(dr.GetOrdinal("pets"))) Pets = (dr.GetString(dr.GetOrdinal("pets")) != null) ? dr.GetString(dr.GetOrdinal("pets")) : "";
            if (columns.Contains("physicalactivity") && !dr.IsDBNull(dr.GetOrdinal("physicalactivity"))) PhysicalActivity = (dr.GetString(dr.GetOrdinal("physicalactivity")) != null) ? dr.GetString(dr.GetOrdinal("physicalactivity")) : "";
            if (columns.Contains("reading") && !dr.IsDBNull(dr.GetOrdinal("reading"))) Reading = (dr.GetString(dr.GetOrdinal("reading")) != null) ? dr.GetString(dr.GetOrdinal("reading")) : "";
            if (columns.Contains("essayfirstdate") && !dr.IsDBNull(dr.GetOrdinal("essayfirstdate"))) EssayFirstDate = (dr.GetString(dr.GetOrdinal("essayfirstdate")) != null) ? dr.GetString(dr.GetOrdinal("essayfirstdate")) : "";
            if (columns.Contains("essaygettoknowme") && !dr.IsDBNull(dr.GetOrdinal("essaygettoknowme"))) EssayGetToKnowMe = (dr.GetString(dr.GetOrdinal("essaygettoknowme")) != null) ? dr.GetString(dr.GetOrdinal("essaygettoknowme")) : "";
            if (columns.Contains("essaypastrelationship") && !dr.IsDBNull(dr.GetOrdinal("essaypastrelationship"))) EssayPastRelationship = (dr.GetString(dr.GetOrdinal("essaypastrelationship")) != null) ? dr.GetString(dr.GetOrdinal("essaypastrelationship")) : "";
            if (columns.Contains("essayimportantthings") && !dr.IsDBNull(dr.GetOrdinal("essayimportantthings"))) EssayImportantThings = (dr.GetString(dr.GetOrdinal("essayimportantthings")) != null) ? dr.GetString(dr.GetOrdinal("essayimportantthings")) : "";
            if (columns.Contains("essayperfectday") && !dr.IsDBNull(dr.GetOrdinal("essayperfectday"))) EssayPerfectDay = (dr.GetString(dr.GetOrdinal("essayperfectday")) != null) ? dr.GetString(dr.GetOrdinal("essayperfectday")) : "";
            if (columns.Contains("essaymorethingstoadd") && !dr.IsDBNull(dr.GetOrdinal("essaymorethingstoadd"))) EssayMoreThingsToAdd = (dr.GetString(dr.GetOrdinal("essaymorethingstoadd")) != null) ? dr.GetString(dr.GetOrdinal("essaymorethingstoadd")) : "";
            if (columns.Contains("favoritebands") && !dr.IsDBNull(dr.GetOrdinal("favoritebands"))) FavoriteBands = (dr.GetString(dr.GetOrdinal("favoritebands")) != null) ? dr.GetString(dr.GetOrdinal("favoritebands")) : "";
            if (columns.Contains("favoritemovies") && !dr.IsDBNull(dr.GetOrdinal("favoritemovies"))) FavoriteMovies = (dr.GetString(dr.GetOrdinal("favoritemovies")) != null) ? dr.GetString(dr.GetOrdinal("favoritemovies")) : "";
            if (columns.Contains("favoritetv") && !dr.IsDBNull(dr.GetOrdinal("favoritetv"))) FavoriteTv = (dr.GetString(dr.GetOrdinal("favoritetv")) != null) ? dr.GetString(dr.GetOrdinal("favoritetv")) : "";
            if (columns.Contains("favoriterestaurant") && !dr.IsDBNull(dr.GetOrdinal("favoriterestaurant"))) FavoriteRestaurant = (dr.GetString(dr.GetOrdinal("favoriterestaurant")) != null) ? dr.GetString(dr.GetOrdinal("favoriterestaurant")) : "";
            if (columns.Contains("schoolsattended") && !dr.IsDBNull(dr.GetOrdinal("schoolsattended"))) SchoolsAttended = (dr.GetString(dr.GetOrdinal("schoolsattended")) != null) ? dr.GetString(dr.GetOrdinal("schoolsattended")) : "";
            if (columns.Contains("mygreattripidea") && !dr.IsDBNull(dr.GetOrdinal("mygreattripidea"))) MyGreatTripIdea = (dr.GetString(dr.GetOrdinal("mygreattripidea")) != null) ? dr.GetString(dr.GetOrdinal("mygreattripidea")) : "";
            if (columns.Contains("moviegenre") && !dr.IsDBNull(dr.GetOrdinal("moviegenre"))) MovieGenres = (dr.GetString(dr.GetOrdinal("moviegenre")) != null) ? dr.GetString(dr.GetOrdinal("moviegenre")) : "";
            return this;
        }
    }

    #endregion


}
