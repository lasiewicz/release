﻿using System.ComponentModel;
using System.Configuration.Install;


namespace Spark.Cupid.Searcher.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
