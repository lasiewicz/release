﻿using System;
using System.Collections;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Search.Interfaces;
using Spark.SearchEngine.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet;
using Matchnet.Content.ValueObjects.Region;
using Spark.Common.AccessService;
using System.Collections.Generic;
using Spark.SearchEngine.ServiceInterface;

namespace Spark.SearchEngine.ServiceAdapters
{
    public class SearcherSA : SABase
    {
        public static readonly SearcherSA Instance = new SearcherSA();

        public string TestURI { get; set; }
        
        private SearcherSA() {}

        public IMatchnetQueryResults RunQuery(IMatchnetQuery query, int communityid) {
            string uri = "";
            IMatchnetQueryResults results = null;
            try
            {
                uri = getServiceManagerUri(communityid);
                base.Checkout(uri);
                try
                {
                    results = getService(uri).RunQuery(query, communityid);
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
            return results;
        }

        public ArrayList RunDetailedQuery(IMatchnetQuery query, int communityid)
        {
            string uri = "";
            ArrayList results = null;
            try
            {
                uri = getServiceManagerUri(communityid);
                base.Checkout(uri);
                try
                {
                    results = getService(uri).RunDetailedQuery(query, communityid);
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
            return results;
        }
        
        private ISearcher getService(string uri)
		{
			try
			{
				return (ISearcher)Activator.GetObject(typeof(ISearcher), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri(int communityId)
		{
			try
			{
                if (!string.IsNullOrEmpty(TestURI))
                {
                    return TestURI;
                }
			    bool useSearcherByCommunity = Boolean.TrueString.ToLower().Equals(GetSetting("SEARCHER_PER_COMMUNITY", "false"));
                //if flag set use community id passed in, otherwise default to sparksearcher for all communities
			    int cid = (useSearcherByCommunity) ? communityId : (int)ServiceConstants.COMMUNITY_ID.Spark;
			    string communityNameUpper = ServiceConstants.GetCommunityName(cid).ToUpper();
			    string serviceSearcherConst = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, communityNameUpper);
			    string uri = AdapterConfigurationSA.GetServicePartition(serviceSearcherConst, PartitionMode.Random).ToUri(ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST);
                string overrideHostName = GetSetting(string.Format(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, communityNameUpper), "");
                    
				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

        protected override void GetConnectionLimit()
        {
            try
            {
                base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPARKSEARCHER_SA_CONNECTION_LIMIT"));
            }
            catch (Exception ex)
            {
                base.MaxConnections = 50;
            }
        }

        public string GetSetting(string name, string defaultvalue)
        {
            try
            {
                return RuntimeSettings.GetSetting(name);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

        public string GetSetting(string name, int communityID, string defaultvalue)
        {
            try
            {
                return RuntimeSettings.GetSetting(name, communityID);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }
	
    }
}
