﻿using System;


using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.SearchEngine.ValueObjects;
using Spark.SearchEngine.ServiceInterface;

namespace Spark.SearchEngine.ServiceAdapters
{
    public class LoaderSA : SABase
    {
        public static readonly LoaderSA Instance = new LoaderSA();

        public string TestURI { get; set; }

        private LoaderSA()
		{
			//_cache = Matchnet.Caching.Cache.Instance;
        }

        #region service instrumentation
       	private ILoader getService(string uri)
		{
			try
			{
				return (ILoader)Activator.GetObject(typeof(ILoader), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
		{
			try
			{
                if (!string.IsNullOrEmpty(TestURI))
                {
                    return TestURI;
                }
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_INDEXER_CONST, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_INDEXER_MANAGER_CONST);
				string overrideHostName = GetSetting("SEARCHLOADER_SA_HOST_OVERRIDE","");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

        protected override void GetConnectionLimit()
        {
            try
            {
                base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHLOADER_SA_CONNECTION_LIMIT"));
            }
            catch (Exception ex)
            {
                base.MaxConnections = 50;
            }
        }

	
        #endregion


       public  string GetSetting(string name,  string defaultvalue)
       {
           try
           {
               return RuntimeSettings.GetSetting(name);

           }
           catch (Exception ex)
           { return defaultvalue; }



       }
    }
}
