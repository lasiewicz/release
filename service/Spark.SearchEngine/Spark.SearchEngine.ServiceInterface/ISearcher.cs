﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using Matchnet.Search.Interfaces;

namespace Spark.SearchEngine.ServiceInterface
{
    [ServiceContract(Namespace = "spark.net")]
    public interface ISearcher
    {
        [OperationContract]
        IMatchnetQueryResults RunQuery(IMatchnetQuery query, int communityid);

        [OperationContract]
        ArrayList RunDetailedQuery(IMatchnetQuery query, int communityid);
        
    }
}
