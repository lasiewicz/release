﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.ScheduledFeed.BusinessLogic;
using Matchnet.ScheduledFeed.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.InitialConfiguration;

namespace Matchnet.ScheduledFeed.Service
{
    public class ScheduledFeed : Matchnet.RemotingServices.RemotingServiceBase
    {
        private System.ComponentModel.Container components = null;
        private ScheduledFeedBL _scheduledFeedBL;

        public ScheduledFeed()
        {
            InitializeComponent();
        }

        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ScheduledFeed() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, InitializationSettings.MachineName);
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            _scheduledFeedBL = new ScheduledFeedBL();
            _scheduledFeedBL.Start();
        }

        protected override void OnStop()
        {
            _scheduledFeedBL.Stop();
            base.OnStop();
        }
        

    }
}
