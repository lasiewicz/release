﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ScheduledFeed.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_NAME = "Matchnet.ScheduledFeed.Service";
        public const string SERVICE_CONSTANT = "SCHEDULEDFEED_SVC";
        
        private ServiceConstants()
        { }

    }
}
