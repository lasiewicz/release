﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Member.ValueObjects.Enumerations;


namespace Matchnet.ScheduledFeed.ValueObjects
{
    

    [Serializable]
    public class ScheduledFeedQueueItem : IValueObject
    {
        #region Private Members
        private Int32 _MemberID;
        private Int32 _GroupID;
        private ScheduledFeedType _ScheduledFeedTypeID;
        private byte _Frequency;
        private DateTime _LastSentDate;
        private DateTime _LastAttemptDate;
        private DateTime _NextAttemptDate;        
        private DateTime _FrequencyUpdateDate;
        private DateTime _InsertDate;
        #endregion

        #region Properties
        public Int32 MemberID { get { return _MemberID; } }
        public Int32 GroupID { get { return _GroupID; } }
        public ScheduledFeedType ScheduledFeedTypeID { get { return _ScheduledFeedTypeID; } }
        public byte Frequency { get { return _Frequency; } }
        public DateTime LastSentDate { get { return _LastSentDate; } }
        public DateTime LastAttemptDate { get { return _LastAttemptDate; } }
        public DateTime NextAttemptDate { get { return _NextAttemptDate; } }
        public DateTime FrequencyUpdateDate { get { return _FrequencyUpdateDate; } }
        public DateTime InsertDate { get { return _InsertDate; } }
        #endregion

        public ScheduledFeedQueueItem(Int32 memberID, Int32 groupID, ScheduledFeedType scheduleFeedTypeID, byte frequency,
            DateTime lastSentDate, DateTime lastAttemptDate, DateTime nextAttemptDate, DateTime frequencyUpdateDate, DateTime insertDate)
        {
            _MemberID = memberID;
            _GroupID = groupID;
            _ScheduledFeedTypeID = scheduleFeedTypeID;
            _Frequency = frequency;
            _LastSentDate = lastSentDate;
            _LastAttemptDate = lastAttemptDate;
            _NextAttemptDate = nextAttemptDate;
            _FrequencyUpdateDate = frequencyUpdateDate;
            _InsertDate = insertDate;
        }

        public override string ToString()
        {
            return "MemberID=" + _MemberID.ToString() +
                    "|GroupID=" + _GroupID.ToString() +
                    "|ScheduledFeedTypeID=" + _ScheduledFeedTypeID.ToString("d") +
                    "|Frequency=" + _Frequency.ToString() +
                    "|LastSentDate=" + _LastSentDate.ToString() +
                    "|LastAttemptDate=" + _LastAttemptDate.ToString() +
                    "|NextAttemptDate=" + _NextAttemptDate.ToString() +
                    "|FrequencyUpdateDate=" + _FrequencyUpdateDate.ToString() +
                    "|InsertDate=" + _InsertDate.ToString();                    
        }

    }
}
