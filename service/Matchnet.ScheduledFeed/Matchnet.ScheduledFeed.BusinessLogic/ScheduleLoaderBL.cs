﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.ScheduledFeed.ValueObjects;
using System.Diagnostics;
using Matchnet.Exceptions;
using Matchnet.Data;
using System.Data;
using System.Messaging;
using Matchnet.Member.ValueObjects.Enumerations;


namespace Matchnet.ScheduledFeed.BusinessLogic
{
    public class ScheduleLoaderBL
    {
        /// <summary>
        /// maximum number of items in an MSMQ queue, to avoid over loading
        /// </summary>
        public const int MAX_ITEMS_IN_QUEUE = 1024 * 16;
        public const int ITEMS_PER_QUEUE_CYCLE = 32;

        public static readonly ScheduleLoaderBL Instance = new ScheduleLoaderBL();
        private static DataTable _ScheduleTable;
        private static bool _Runnable = false;

        string _cfgQueuePaths = string.Empty;
        private static string[] _QueuePaths;
        private static int _QueuePathSpinIndex = 0;
        private static DateTime _LastSetQueuePathTime = DateTime.Now;
        private static PerformanceCounter[] _perfItemsInQueue;

        private ScheduleLoaderBL()
        { }

        public bool Runnable
        {
            get { return _Runnable; }
            set { _Runnable = value; }
        }

        /// <summary>
        /// Start loading queue with items
        /// </summary>
        public void Run()
        {
            _Runnable = true;

            while (_Runnable)
            {
                

                SetQueuePaths(); // in case config changed, this call is cheap.

                bool QueueIsTooFull = false;
                for (int i = 0; i < _perfItemsInQueue.Length && !QueueIsTooFull; i++)
                {
                    QueueIsTooFull = _perfItemsInQueue[i].NextValue() >= MAX_ITEMS_IN_QUEUE;
                    System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": " + _perfItemsInQueue[i].InstanceName + " msg count " + _perfItemsInQueue[i].NextValue().ToString());

                }

                if (QueueIsTooFull)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": Snore. Queue too full.");
                    Thread.Sleep(20000); // sleep because queue has enough items for now.
                }
                else
                {
                    LoadBatchFromDB();
                    if ((_ScheduleTable != null && _ScheduleTable.Rows.Count == 0) || _ScheduleTable == null)
                    {
                        System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": Snore. DB call returned no results");
                        Thread.Sleep(10000); // sleep because last scoop brought back nothing.
                    }
                    else
                    {
                        List<ScheduledFeedQueueItem> items = ExtractQueueItems();
                        DistributeQueueItems(items);
                        _ScheduleTable = null;                        
                    }
                }
            }
        }

        /// <summary>
        /// Stop this instance
        /// </summary>
        public void Stop()
        {
            _Runnable = false;
        }

        public bool EnqueueScheduledFeedItem(List<ScheduledFeedQueueItem> items, int startAt, int count)
        {
            MessageQueue mq;

            try
            {
                mq = new MessageQueue(GetNextQueuePath());
                mq.Formatter = new BinaryMessageFormatter();
                using (MessageQueueTransaction tran = new MessageQueueTransaction())
                {
                    tran.Begin();
                    for (int i = startAt; i < Math.Min(items.Count, (startAt + count)); i++)
                    {
                        mq.Send(items[i], items[i].MemberID.ToString() + ":" + items[i].GroupID.ToString(), tran);
                    }
                    tran.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder(items.Count * 10);
                for (int i = 0; i < items.Count; i++)
                {
                    sb.Append(items[0].MemberID.ToString());
                    sb.Append(',');
                }

                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                    "EnqueueScheduledFeedItem failed on member ID's:\n" + sb.ToString(),
                    ex, false);
                return false;
            }
        }
        
        public void DistributeQueueItems(List<ScheduledFeedQueueItem> items)
        {            
            bool sentSuccess = false;
            for (int i = 0; i < items.Count; i += ITEMS_PER_QUEUE_CYCLE)
            {
                for (int attempt = 0; attempt < 3; attempt++)
                {
                    sentSuccess = EnqueueScheduledFeedItem(items, i, ITEMS_PER_QUEUE_CYCLE);
                    if (sentSuccess) { break; }
                    Thread.Sleep(10000);
                }
                if (!sentSuccess)
                {
                    new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Can't enqueue item after repeated attempts. Giving up!", null, false);
                    new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Last Batch: " + items.Count.ToString() + " total, " + i.ToString() + " sent," + (items.Count - i).ToString() + " abandoned.", null, false);
                    _Runnable = false;
                }
            }

        }

        public ScheduledFeedQueueItem GetQueueItem(DataRow row)
        {
            /******************
                    Column_name			Type			Length	Nullable
                    MemberID			int				4		no
                    GroupID				int				4		no                
                    Frequency			tinyint			1		no
                *	LastSentDate		smalldatetime	4		yes
                *	LastAttemptDate		smalldatetime	4		yes
                    NextAttemptDate		smalldatetime	4		no                    
                *	FrequencyUpdateDate	smalldatetime	4		yes
                    InsertDate			smalldatetime	4		no               

            ******************/
            Int32 memberID;
            Int32 groupID;
            ScheduledFeedType scheduledFeedTypeID;
            byte frequency;
            DateTime lastSentDate = new DateTime(1900, 1, 1); // nullable!
            DateTime lastAttemptDate = new DateTime(1900, 1, 1); // nullable!
            DateTime nextAttemptDate;
            DateTime frequencyUpdateDate = new DateTime(1900, 1, 1); // nullable!
            DateTime insertDate;

            memberID = Convert.ToInt32(row["MemberID"]);
            groupID = Convert.ToInt32(row["GroupID"]);
            scheduledFeedTypeID = (ScheduledFeedType)Convert.ToInt32(row["ScheduledFeedTypeID"]);
            frequency = Convert.ToByte(row["Frequency"]);
            if (row["LastSentDate"] != DBNull.Value) lastSentDate = Convert.ToDateTime(row["LastSentDate"]);
            if (row["LastAttemptDate"] != DBNull.Value) lastAttemptDate = Convert.ToDateTime(row["LastAttemptDate"]);
            nextAttemptDate = Convert.ToDateTime(row["NextAttemptDate"]);
            if (row["FrequencyUpdateDate"] != DBNull.Value) frequencyUpdateDate = Convert.ToDateTime(row["FrequencyUpdateDate"]);
            insertDate = Convert.ToDateTime(row["InsertDate"]);

            ScheduledFeedQueueItem queueItem = new ScheduledFeedQueueItem(
                                                        memberID,
                                                        groupID,
                                                        scheduledFeedTypeID,
                                                        frequency,
                                                        lastSentDate,
                                                        lastAttemptDate,
                                                        nextAttemptDate,
                                                        frequencyUpdateDate,
                                                        insertDate);

            return queueItem;
        }
        
        private string GetNextQueuePath()
        {
            if (DateTime.Now.Subtract(_LastSetQueuePathTime).TotalSeconds > 20)
            {
                SetQueuePaths();
            }
            _QueuePathSpinIndex = ++_QueuePathSpinIndex % _QueuePaths.Length;
            return _QueuePaths[_QueuePathSpinIndex];
        }

        private List<ScheduledFeedQueueItem> ExtractQueueItems()
        {
            List<ScheduledFeedQueueItem> items = new List<ScheduledFeedQueueItem>();
            ScheduledFeedQueueItem queueItem;
            if (_ScheduleTable != null)
            {
                for (int i = 0; i < _ScheduleTable.Rows.Count; i++)
                {
                    queueItem = GetQueueItem(_ScheduleTable.Rows[i]);
                    items.Add(queueItem);
                }
            }
            return items;
        }

        private void LoadBatchFromDB()
        {
            try
            {
                Command command = new Command("mnAlertWrite", "up_ScheduledFeedSchedule_List_Send", 0);
                command.AddParameter("@ScheduleFeedTypeID", SqlDbType.Int, ParameterDirection.Input, (int)ScheduledFeedType.Messmo);
                DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(command);

                _ScheduleTable = ds.Tables[0];                
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "LoadBatchFromDB() failed.", ex, false);
            }
        }

        /// <summary>
        /// Loads runtime config to determine which queues configured to get bulkmail work.
        /// </summary>
        private void SetQueuePaths()
        {
            try
            {
                string strQueuePaths = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_QUEUE_PATH");

                _LastSetQueuePathTime = DateTime.Now;
                if (strQueuePaths != _cfgQueuePaths)
                {
                    _cfgQueuePaths = strQueuePaths;
                    _QueuePaths = strQueuePaths.Split(',');

                    _perfItemsInQueue = new PerformanceCounter[_QueuePaths.Length];
                    for (int i = 0; i < _QueuePaths.Length; i++)
                    {
                        string instance = _QueuePaths[i];
                        instance = instance.Replace("FormatName:DIRECT=OS:", string.Empty);
                        string host = instance.Substring(0, instance.IndexOf("\\"));
                        if (host == ".")
                        {
                            host = System.Environment.MachineName.ToLower();
                            instance = instance.Replace(".", host);
                        }
                        _perfItemsInQueue[i] = new PerformanceCounter("MSMQ Queue", "Messages in Queue", instance, host);
                        System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + " starting perf counter " + instance + " has : " + _perfItemsInQueue[i].NextValue().ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetQueuePaths() failed. Can't figure out where to send queue items, or can't connect counter!", ex, false);
            }
            if (_QueuePaths.Length == 0)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetQueuePaths() failed. Got 0 queue paths.", null, false);
            }
        }


    }
}
