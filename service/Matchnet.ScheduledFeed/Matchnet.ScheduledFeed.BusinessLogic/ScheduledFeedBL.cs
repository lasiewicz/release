﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ScheduledFeed.ValueObjects;
using System.Diagnostics;
using System.Messaging;
using Matchnet.Data.Hydra;
using System.Threading;
using System.Collections;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Data;
using System.Data;

namespace Matchnet.ScheduledFeed.BusinessLogic
{
    public class ScheduledFeedBL
    {
        #region Private Variables
        private const string HYDRA_SAVE_NAME = "mnAlertSaveMM";
        private const int SLEEP_TIME = 500;
        private const int SLEEP_TIME_ERROR = 15000;

        // performance counters
        private PerformanceCounter _perfTotalMessageSent;
        private PerformanceCounter _perfTotalUncaughtExceptions;
        private PerformanceCounter _perfTotalErrorProcessingQueue;
        private PerformanceCounter _perfTotalQueueItemReceived;

        private bool _runnable = false;
        private Thread _threadGetNext;
        private Thread[] _threadQueue;
        private string _cfgScheduledFeedQueuePath;
        private int _cfgThreadCount;
        private int _cfgPollingInterval;
        private string _cfgPollingProcessorHost;
        private bool _cfgTracingFlag = false;
        private float _fThreadCount;
        private MessageQueue _queueScheduledFeed;
        private HydraWriter _hydraWriter;
        #endregion

        #region Service Control
        public void Start()
        {
            _cfgScheduledFeedQueuePath = RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_QUEUE_PATH");
            _cfgThreadCount = Convert.ToInt32(RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_THREAD_COUNT"));
            _cfgPollingInterval = Convert.ToInt32(RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_POLL_INTERVAL"));
            _cfgPollingProcessorHost = RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_POLL_PROCESSOR").ToLower();
            _cfgTracingFlag = bool.Parse(RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_TRACING_FLAG"));            

            // cache floats rather than convert on the fly for slowdown calculations            
            _fThreadCount = Convert.ToSingle(_cfgThreadCount);

            _cfgScheduledFeedQueuePath = @".\private$" + _cfgScheduledFeedQueuePath.Substring(_cfgScheduledFeedQueuePath.LastIndexOf('\\')).ToLower();

            Inform(string.Format("Config:\nMM Q:{0}\nMM Threads:{1}\nPolling Interval:{2} (deprecated!)\nPolling Host:{3}\nTracing:{4}\nQ PATH raw: {5}",
                _cfgScheduledFeedQueuePath,
                _cfgThreadCount,                
                _cfgPollingInterval,
                _cfgPollingProcessorHost,
                _cfgTracingFlag,
                Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SCHEDULEDFEEDSVC_QUEUE_PATH")
                )
                );

            initPerfCounters();
            _queueScheduledFeed = new MessageQueue(_cfgScheduledFeedQueuePath);
            _queueScheduledFeed.Formatter = new BinaryMessageFormatter();

            _runnable = true;

            _hydraWriter = new HydraWriter(HYDRA_SAVE_NAME);
            _hydraWriter.Start();

            _threadQueue = new Thread[_cfgThreadCount];
            for (int threadNum = 0; threadNum < _cfgThreadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processCycle));
                t.Name = "Process Cycle Thread " + threadNum;
                t.Start();
                _threadQueue[threadNum] = t;
            }
            Inform("Started " + _cfgThreadCount + " processing cycle threads.");

            //poll to get next matches from Alert database
            if (_cfgPollingProcessorHost == System.Environment.MachineName.ToLower())
            {
                _threadGetNext = new Thread(new ThreadStart(ScheduleLoaderBL.Instance.Run));
                _threadGetNext.Name = "Next Matches Cycle Thread";
                _threadGetNext.Start();
                Inform("Started next matches cycle thread: " + _threadGetNext.Name);
            }
        }

        public void Stop()
        {
            _runnable = false;
            _hydraWriter.Stop();
            ScheduleLoaderBL.Instance.Stop();

            if (_threadGetNext != null)
            {
                _threadGetNext.Join(20000);
            }

            for (int threadNum = 0; threadNum < _threadQueue.Length; threadNum++)
            {
                _threadQueue[threadNum].Join(10000);
            }
        }
        #endregion

        #region Process Cycles
        private void processCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);
                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();
                    try
                    {
                        ScheduledFeedQueueItem queueItem = null;
                        tran.Begin();

                        #region Receive the queue item
                        try
                        {
                            queueItem = _queueScheduledFeed.Receive(ts, tran).Body as ScheduledFeedQueueItem;
                            _perfTotalQueueItemReceived.Increment();
                        }
                        catch (MessageQueueException mex)
                        {
                            if (mex.Message != "Timeout for the requested operation has expired.")
                            {
                                // if we can't get a queue item, and it's not a timeout, don't bother trying to continue this queue processing
                                attemptRollback(tran);
                                throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                            }
                        }
                        #endregion

                        // if queue item is null for some reason (most likely timeout), let's skip over this item
                        if (queueItem == null)
                        {
                            tran.Commit();
                            Thread.Sleep(SLEEP_TIME);
                            continue;
                        }

                        #region Process the queue item
                        int brandID = Matchnet.Constants.NULL_INT;
                        int memberID = Matchnet.Constants.NULL_INT;
                        try
                        {
                            bool enabled = Convert.ToBoolean(RuntimeSettings.GetSetting("MESSMO_ENABLED", queueItem.GroupID));

                            if (enabled)
                            {
                                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(queueItem.MemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                                // this is strictly for exception handling use only                            
                                memberID = member.MemberID;

                                // SINCE THERE IS ONLY 1 FEED TYPE RIGHT NOW (MESSMO), I'M NOT EVEN DOING SWITCHING LOGIC HERE.  If new feed types are added, this is the
                                // spot where you would do the branching logic.
                                processMessmoScheduledFeed(queueItem, member, out brandID);
                            }

                            // Commit first and then update the DB
                            tran.Commit();
                            // Update the database with the results
                            saveBulkMailStatusToDB(queueItem, enabled);
                        }
                        catch (Exception ex)
                        {
                            // let the queue go on even with exceptions
                            tran.Commit();
                            // Update the database with the results
                            saveBulkMailStatusToDB(queueItem, false);
                            throw new Exception(string.Format("Error occurred during item processing. [BrandID: {0}, MemberID: {1}", brandID, memberID), ex);                             
                        }
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        _perfTotalErrorProcessingQueue.Increment();
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing ScheduledFeed transaction. [QueuePath: " + _cfgScheduledFeedQueuePath + "]" + ex.ToString(), EventLogEntryType.Error);
                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _perfTotalUncaughtExceptions.Increment();
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processCycle(): " + ex.ToString(), EventLogEntryType.Error);
                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        private void processMessmoScheduledFeed(ScheduledFeedQueueItem queueItem, Matchnet.Member.ServiceAdapters.Member member, out int brandID)
        {
            Brand brand = BrandConfigSA.Instance.GetBrandByID(GetMessmoDefaultBrandID(queueItem.GroupID));
            brandID = brand.BrandID;

            // Let's make sure this member is capable of receiving anything
            int messmoCapable = member.GetAttributeInt(brand, "MessmoCapable", 0);
            if (messmoCapable != 1)
                return;

            // They are capable of receiving; now make sure they want to receive anything at all
            int messmoAlertSetting = member.GetAttributeInt(brand, "MessmoAlertSetting", 0);
            if (messmoAlertSetting == (int)Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.DoNotSend)
                return;

            // Get the mask value indicating what type of alerts they want to receive
            int messmoAlertMask = member.GetAttributeInt(brand, "MessmoAlertMask", 0);
            
            List.ServiceAdapters.List list = List.ServiceAdapters.ListSA.Instance.GetList(member.MemberID, Matchnet.List.ServiceAdapters.ListLoadFlags.IsSelf);
            string messmoSubID = member.GetAttributeText(brand, "MessmoSubscriberID");

            #region Sending of Various Messages
                        
            // Your Favorites (20 of them)
            if((messmoAlertMask & (int)Matchnet.HTTPMessaging.Constants.MessmoAlertMask.Favorites) > 0)
            {
                SendProfilesFromList(brand, member, list, HotListCategory.Default, 20, messmoSubID);
            }

            // Members you viewed (20 of them)
            if ((messmoAlertMask & (int)Matchnet.HTTPMessaging.Constants.MessmoAlertMask.MembersYouViewed) > 0)
            {
                SendProfilesFromList(brand, member, list, HotListCategory.MembersYouViewed, 20, messmoSubID);
            }

            if ((messmoAlertMask & (int)Matchnet.HTTPMessaging.Constants.MessmoAlertMask.DailyMatches) > 0)
            {
                // Your Matches (5 of them)
                SendProfilesFromList(brand, member, list, HotListCategory.YourMatches, 5, messmoSubID);
            }
            
            // New members in your area (5 of them)
            if ((messmoAlertMask & (int)Matchnet.HTTPMessaging.Constants.MessmoAlertMask.NewMembersInArea) > 0)
            {
                #region Members in your area
                Matchnet.Member.ServiceAdapters.Member fromMember;
                int sendingMemberID, senderBrandID;
                DateTime messageDate;
                string messageBody, smartMessageTag, taggle;

                SearchPreferenceCollection searchPrefs = GetNewMemberInYourAreaSearchPrefs(brand, member);

                if (searchPrefs != null)
                {
                    MatchnetQueryResults searchResults = null;
                    ValidationResult validationResult = searchPrefs.Validate();

                    if (validationResult.Status == PreferencesValidationStatus.Success)
                    {
                        searchResults = MemberSearchSA.Instance.Search(searchPrefs,
                            brand.Site.Community.CommunityID,
                            brand.Site.SiteID,
                            0,
                            5); // we only want 5 most recent

                        ArrayList newMemberIDs = null;

                        if (searchResults != null)
                        {
                            newMemberIDs = searchResults.ToArrayList();
                        }

                        if (newMemberIDs != null)
                        {
#if DEBUG
                            Trace.WriteLine("New members in your area count: " + newMemberIDs.Count.ToString());
#endif
                            // Figure out if the receiving member wants to be notified of this message arrival (push notification)
                            bool notify = ExternalMail.ServiceAdapters.ExternalMailSA.Instance.NotifyOfMessmoMessageArrival(member.MemberID, brand.BrandID,
                                Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea);

                            int newMemberID = Constants.NULL_INT;
                            for(int i=0; i < newMemberIDs.Count; i++)                            
                            {
                                newMemberID = Convert.ToInt32(newMemberIDs[i]);

                                fromMember = MemberSA.Instance.GetMember(newMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                                if (fromMember != null)
                                {
                                    sendingMemberID = fromMember.MemberID;
                                    // since we are sending search results which do not exist as messages or list items, just set the messageDate as now
                                    messageDate = DateTime.Now;
                                    messageBody = Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea, string.Empty, string.Empty);

                                    smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea,
                                        fromMember.GetUserName(brand));
                                    taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(fromMember.MemberID, member.MemberID,
                                        Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea, brand.Site.Community.CommunityID,
                                        Constants.NULL_INT);

                                    // senderBrandID is used to retrieive the sender's profile information and photo. get it by grabbing the last logon
                                    // brandID.
                                    fromMember.GetLastLogonDate(brand.Site.Community.CommunityID, out senderBrandID);

                                    // couldn't find the member for some reason
                                    if (senderBrandID != Constants.NULL_INT)
                                    {
                                        // If the member wants to be notified, let's notify the member on the final message only.  Notify flag value is only relevant for the final message.
                                        if (i == newMemberIDs.Count - 1)
                                        {
                                            Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoMultiPartMessage(brand.BrandID, senderBrandID, sendingMemberID,
                                                messageDate, messmoSubID, messageBody, smartMessageTag, taggle, true, notify, Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea);
                                        }
                                        else
                                        {
                                            Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoMultiPartMessage(brand.BrandID, senderBrandID, sendingMemberID,
                                                messageDate, messmoSubID, messageBody, smartMessageTag, taggle, true, false, Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea);
                                        }

                                        _perfTotalMessageSent.Increment();
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }

            #endregion

        }
        #endregion

        #region Messmo-specific
        /// <summary>
        /// This is taken from BulkMail service. 
        /// </summary>
        /// <returns></returns>
        private SearchPreferenceCollection GetNewMemberInYourAreaSearchPrefs(Brand brand, Matchnet.Member.ServiceAdapters.Member member)
        {
            SearchPreferenceCollection newPrefs = null;

            int ageFactor = Convert.ToInt32(RuntimeSettings.GetSetting("NEW_MEMBER_EMAIL_AGE_FACTOR"));
            int distance = Convert.ToInt32(RuntimeSettings.GetSetting("NEW_MEMBER_EMAIL_DISTANCE"));

            int genderMask = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "GenderMask", Constants.NULL_INT);
            int regionID = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "RegionID", Constants.NULL_INT);
            DateTime birthDate = member.GetAttributeDate(brand.Site.Community.CommunityID, 0, 0, "Birthdate", DateTime.MinValue);

            if (genderMask != Constants.NULL_INT && regionID != Constants.NULL_INT && birthDate != DateTime.MinValue && Matchnet.GenderUtils.IsFlippable(genderMask))
            {
                newPrefs = new SearchPreferenceCollection();

                genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask); // Get seeking gender
                int[] agerange = AgeUtils.GetAgeRange(birthDate, Constants.NULL_INT, Constants.NULL_INT);
                newPrefs.Add("GenderMask", genderMask.ToString());
                newPrefs.Add("RegionID", regionID.ToString());
                newPrefs.Add("SearchTypeID", "4"); //Region
                newPrefs.Add("SearchOrderBy", "1"); // JoinDate
                newPrefs.Add("Distance", distance.ToString());
                newPrefs.Add("MinAge", (agerange[0] - ageFactor).ToString());
                newPrefs.Add("MaxAge", (agerange[1] + ageFactor).ToString());
            }

            return newPrefs;
        }	

        /// <summary>
        /// For now, we know only JDATE will have this. Business also said JDATE will be the only site with Messmo capability EVER!
        /// If this changes though, figure out some logic here.  This ID is used to retrieve the member's LIst information.
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
        private int GetMessmoDefaultBrandID(int communityID)
        {
            int ret = Matchnet.Constants.NULL_INT;

            if (communityID == 3)
                ret = 1003;

            return ret;
        }

        /// <summary>
        /// Sends profiles from the specified list
        /// </summary>
        /// <param name="recipientBrand">member's brand</param>
        /// <param name="member">member who owns the list</param>
        /// <param name="list">list object</param>
        /// <param name="hotListCat">hot list category</param>
        /// <param name="sendCount">how many to send (most recent ones get sent)</param>
        /// <param name="receivingMessmoSubID">Messmo subscription ID of the person to send these to</param>
        private void SendProfilesFromList(Brand recipientBrand,
            Matchnet.Member.ServiceAdapters.Member member,
            Matchnet.List.ServiceAdapters.List list,
            HotListCategory hotListCat,
            int sendCount,
            string receivingMessmoSubID)
        {
            int sendingMemberID, senderBrandID;
            DateTime messageDate;
            string messageBody, smartMessageTag, taggle;
            Matchnet.Member.ServiceAdapters.Member sendingMember = null;

            if (list != null)
            {
                int rowCount;

                // Get the Messmo Message Type from the HostList type
                Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.MapHotListCategoryToMessmoMessageType(hotListCat);
                // Figure out if the receiving member wants to be notified of this message arrival (push notification)
                bool notify = ExternalMail.ServiceAdapters.ExternalMailSA.Instance.NotifyOfMessmoMessageArrival(member.MemberID, recipientBrand.BrandID,
                    messmoMsgType);
                // We decided to only set this flag to TRUE for the very last message of the "New Member in Your Area" messages only.
                // For this reason, let's just override the value to false for now.
                notify = false;

                ArrayList memberIDs = list.GetListMembers(hotListCat, recipientBrand.Site.Community.CommunityID,
                    recipientBrand.Site.SiteID, 1, sendCount, out rowCount);

                int i = 0;
                foreach (int memberID in memberIDs)
                {
                    if (i >= sendCount)
                        break;

                    sendingMemberID = memberID;
                    sendingMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(sendingMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

                    if (sendingMember != null)
                    {
                        // MessmoSendMultiPartMessageCmd senderBrandID to grab the profile info and photo
                        senderBrandID = Matchnet.Constants.NULL_INT;
                        sendingMember.GetLastLogonDate(recipientBrand.Site.Community.CommunityID, out senderBrandID);

                        if (senderBrandID != Constants.NULL_INT)
                        {
                            ListItemDetail itemDetail = list.GetListItemDetail(hotListCat, recipientBrand.Site.Community.CommunityID,
                                recipientBrand.Site.SiteID, sendingMemberID);

                            messageDate = itemDetail.ActionDate;
                            
                            // Include the subject in the body of the messsage
                            messageBody = Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(messmoMsgType, string.Empty, string.Empty);

                            Brand brand = BrandConfigSA.Instance.GetBrandByID(senderBrandID);
                            smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(messmoMsgType, sendingMember.GetUserName(brand));
                            // this is not a real message in our system so don't pass the groupID or the messagelistID
                            taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(sendingMemberID, member.MemberID, messmoMsgType,
                                recipientBrand.Site.Community.CommunityID, Constants.NULL_INT);

                            Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoMultiPartMessage(recipientBrand.BrandID,
                                senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID, messageBody, smartMessageTag, taggle, true, notify, messmoMsgType);
                            
                            _perfTotalMessageSent.Increment();
                        }
                    }

                    i++;
                }
            }
        }               
        #endregion

        #region Helpers
        private void saveBulkMailStatusToDB(ScheduledFeedQueueItem queueItem, bool sentMessagesFlag)
        {

#if DEBUG
            Trace.WriteLine(queueItem.ToString());            
#endif

            try
            {
                Command command = new Command(HYDRA_SAVE_NAME, "up_ScheduledFeedSchedule_Save_Service", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItem.MemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, queueItem.GroupID);
                command.AddParameter("@ScheduledFeedTypeID", SqlDbType.Int, ParameterDirection.Input, (int)queueItem.ScheduledFeedTypeID);
                command.AddParameter("@Frequency", SqlDbType.TinyInt, ParameterDirection.Input, queueItem.Frequency);
                command.AddParameter("@LastSentDate", SqlDbType.SmallDateTime, ParameterDirection.Input, (sentMessagesFlag) ? DateTime.Now : queueItem.LastSentDate);
                command.AddParameter("@LastAttemptDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.LastAttemptDate);
                command.AddParameter("@NextAttemptDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.NextAttemptDate);
                command.AddParameter("@FrequencyUpdateDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.FrequencyUpdateDate);
                command.AddParameter("@InsertDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.InsertDate);
                

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new Exception("Error updating mnAlert. " + queueItem.ToString(), ex);
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Abort();
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
            }
        }

        private void Inform(string statement)
        {
            System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME,
                statement,
                EventLogEntryType.Information);
        }
        #endregion

        #region Instrumentation
        public static void PerfCounterInstall()
        {
            CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
            ccdc.AddRange(new CounterCreationData[] {	
														 new CounterCreationData("Total Messages Sent", "Total Number of Messages Sent", PerformanceCounterType.NumberOfItems32),
                                                         new CounterCreationData("Total Uncaught Exceptions", "Total Number of Uncaught Exceptions in processCycle", PerformanceCounterType.NumberOfItems32),
                                                         new CounterCreationData("Total Errors during Processing", "Total Number of Errors during Processing", PerformanceCounterType.NumberOfItems32),
                                                         new CounterCreationData("Total Queue Items Received", "Total Number of Queue Items Received", PerformanceCounterType.NumberOfItems32)

													 });

            PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME,
                PerformanceCounterCategoryType.SingleInstance, ccdc);
        }

        public static void PerfCounterUninstall()
        {
            if (PerformanceCounterCategory.Exists(ServiceConstants.SERVICE_NAME))
            {
                PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
            }
        }

        private void initPerfCounters()
        {
            _perfTotalMessageSent = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Total Messages Sent", false);
            _perfTotalUncaughtExceptions = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Total Uncaught Exceptions", false);
            _perfTotalErrorProcessingQueue = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Total Errors during Processing", false);
            _perfTotalQueueItemReceived = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Total Queue Items Received", false);

            resetPerfCounters();
        }

        private void resetPerfCounters()
        {
            _perfTotalMessageSent.RawValue = 0;
            _perfTotalUncaughtExceptions.RawValue = 0;
            _perfTotalErrorProcessingQueue.RawValue = 0;
            _perfTotalQueueItemReceived.RawValue = 0;
        }
        #endregion
    }

    /// <summary>
    /// Taken from BulkMail service
    /// </summary>
    public class AgeUtils
    {
        public static int[] GetAgeRange(DateTime birthDate, int ageMin, int ageMax)
        {
            if (ageMin == Constants.NULL_INT || ageMax == Constants.NULL_INT)
            {
                int age = (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
                if (age >= Constants.MIN_AGE_RANGE && age <= 24)
                {
                    ageMin = Constants.MIN_AGE_RANGE;
                    ageMax = 24;
                }
                else if (age >= 25 && age <= 34)
                {
                    ageMin = 25;
                    ageMax = 34;
                }
                else if (age >= 35 && age <= 44)
                {
                    ageMin = 35;
                    ageMax = 44;
                }
                else if (age >= 45 && age <= 54)
                {
                    ageMin = 45;
                    ageMax = 54;
                }
                else if (age >= 55 && age <= Constants.MAX_AGE_RANGE)
                {
                    ageMin = 55;
                    ageMax = Constants.MAX_AGE_RANGE;
                }
                else
                {
                    ageMin = 25;
                    ageMax = 34;
                }
            }
            return new int[] { ageMin, ageMax };
        }

        public static int GetAge(DateTime birthDate)
        {
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }
    }
}
