﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.DistributedCaching.ValueObjects;
using Matchnet.DistributedCaching.ValueObjects.ServiceDefinitions;

namespace Matchnet.DistributedCaching.ServiceAdapters
{
    public class DistributedCachingSA : SABase
    {
        #region Singleton implementation
        public static readonly DistributedCachingSA Instance = new DistributedCachingSA();

        private DistributedCachingSA()
        { }
        #endregion

        public void Put(string cacheKey, string cacheItem, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).Put(cacheKey, cacheItem, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public void Put(string cacheKey, object value, string namedCacheName, int cacheTtlInSeconds)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                Checkout(uri);
                try
                {
                    getService(uri).Put(cacheKey, value, namedCacheName, cacheTtlInSeconds);
                }
                finally
                {
                    Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public void Put(string cacheKey, ArrayList cacheItem, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).Put(cacheKey, cacheItem, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }                               
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }   
        }

        public void Put(string cacheKey, Array cacheItem, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).Put(cacheKey, cacheItem, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public void Put(string cacheKey, List<int> cacheItem, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).Put(cacheKey, cacheItem, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public void Remove(string cacheKey, string namedCacheName)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                Checkout(uri);
                try
                {
                    getService(uri).Remove(cacheKey, namedCacheName);
                }
                finally
                {
                    Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Remove() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public string GetString(string cacheKey, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetString(cacheKey, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public object Get(string cacheKey, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).Get(cacheKey, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public ArrayList GetArrayList(string cacheKey, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetArrayList(cacheKey, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public Array GetArray(string cacheKey, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetArray(cacheKey, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        public List<int> GetIntList(string cacheKey, string namedCacheName)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetIntList(cacheKey, namedCacheName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISTRIBUTEDCACHINGSVC_SA_CONNECTION_LIMIT"));
        }

        private IDistributedCaching getService(string uri)
        {
            try
            {
                return (IDistributedCaching)Activator.GetObject(typeof(IDistributedCaching), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        private string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISTRIBUTEDCACHINGSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }
    }
}
