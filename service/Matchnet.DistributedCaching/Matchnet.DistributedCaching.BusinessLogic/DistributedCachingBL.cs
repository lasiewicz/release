﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Membase;

#endregion

namespace Matchnet.DistributedCaching.BusinessLogic
{
    public class DistributedCachingBL : IDisposable
    {
        public static readonly DistributedCachingBL Instance = new DistributedCachingBL();
        private static ISettingsSA _settingsService;
        private readonly ReaderWriterLock _membaseLock;
        private readonly CircuitBreaker m_breaker;
        private readonly string MEMBASE_CONFIG_URL_TMPL = "http://{0}:{1}/pools/default";
        private bool _isRunning;
        private int _manageConnectionsThreadSleepInMillis = 3600*1000;
        private Dictionary<string, MembaseClient> _membaseClients;
        private Dictionary<string, int> _objectTTLInSeconds;
        private ReaderWriterLock m_externalLock;
        private Thread manageCacheConnections;

        private DistributedCachingBL()
        {
#if DEBUG
            USE_NUNIT = false;
#endif
            Trace.Write("DistributedCachingBL constructor.");
            m_externalLock = new ReaderWriterLock();
            m_breaker = new CircuitBreaker();
            _membaseLock = new ReaderWriterLock();
            _membaseClients = new Dictionary<string, MembaseClient>();
            _objectTTLInSeconds = new Dictionary<string, int>();
            _isRunning = true;
            InitMembaseClients();

#if DEBUG
            if (!USE_NUNIT)
            {
#endif
                manageCacheConnections = new Thread(ManageCacheConnections);
                manageCacheConnections.Start();
#if DEBUG
            }
#endif
        }

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public int ManageConnectionsThreadSleepInMillis
        {
            get { return _manageConnectionsThreadSleepInMillis; }
            set { _manageConnectionsThreadSleepInMillis = value; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            _isRunning = false;
            if (null != manageCacheConnections)
            {
                manageCacheConnections.Abort();
                manageCacheConnections = null;
            }

            if (null != _membaseClients)
            {
                foreach (var mc in _membaseClients.Values)
                {
                    mc.Dispose();
                }
            }
        }

        #endregion

        private void InitMembaseClients()
        {
            try
            {
                var configs = SettingsService.GetMembaseConfigsFromSingleton();

                foreach (var config in configs)
                {
                    CreateCacheConnection(config);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not get setup membase cache!", ex);
            }
        }

        //This thread will add new connections if any db changes are made
        public void ManageCacheConnections()
        {
            while (_isRunning)
            {
                Thread.Sleep(ManageConnectionsThreadSleepInMillis); // only check once per hour
                var membaseConfigs = SettingsService.GetMembaseConfigsFromSingleton();
                foreach (var config in membaseConfigs)
                {
                    try
                    {
                        if (!_membaseClients.ContainsKey(config.BucketName))
                        {
                            CreateCacheConnection(config);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry(ValueObjects.ServiceConstants.SERVICE_NAME,
                            "Could not create cache connection for bucket:" + config.BucketName + ", Exception: " +
                            e.Message, EventLogEntryType.Error);
                    }
                }
#if DEBUG
                if (USE_NUNIT)
                {
                    _isRunning = false;
                }
#endif
            }
        }

        private void CreateCacheConnection(MembaseConfig config)
        {
            var mc = new Membase.Configuration.MembaseClientConfiguration();
            mc.Bucket = config.BucketName;
            mc.BucketPassword = (string.IsNullOrEmpty(config.BucketPassword)) ? "" : config.BucketPassword;
            if (config.RetryCount > int.MinValue) mc.RetryCount = config.RetryCount;
            if (config.RetryTimeout > int.MinValue)
            {
                mc.RetryTimeout = new TimeSpan(0, 0, config.RetryCount);
            }
            if (config.SocketPoolMaxPoolSize > int.MinValue) mc.SocketPool.MaxPoolSize = config.SocketPoolMaxPoolSize;
            if (config.SocketPoolMinPoolSize > int.MinValue) mc.SocketPool.MinPoolSize = config.SocketPoolMinPoolSize;
            if (config.SocketPoolConnectionTimeout > int.MinValue)
            {
                mc.SocketPool.ConnectionTimeout = new TimeSpan(0, 0, config.SocketPoolConnectionTimeout);
            }
            if (config.SocketPoolDeadTimeout > int.MinValue)
            {
                mc.SocketPool.DeadTimeout = new TimeSpan(0, 0, config.SocketPoolDeadTimeout);
            }
            if (config.SocketPoolQueueTimeout > int.MinValue)
            {
                mc.SocketPool.QueueTimeout = new TimeSpan(0, 0, config.SocketPoolQueueTimeout);
            }
            if (config.SocketPoolReceiveTimeout > int.MinValue)
            {
                mc.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, config.SocketPoolReceiveTimeout);
            }
            foreach (var server in config.Servers)
            {
                mc.Urls.Add(new Uri(string.Format(MEMBASE_CONFIG_URL_TMPL, server.ServerIP, server.ServerPort)));
            }

            _membaseLock.AcquireWriterLock(-1);
            try
            {
                if (_membaseClients.ContainsKey(mc.Bucket))
                {
                    _membaseClients.Remove(mc.Bucket);
                }
                _membaseClients.Add(mc.Bucket, new MembaseClient(mc));

                if (_objectTTLInSeconds.ContainsKey(mc.Bucket))
                {
                    _objectTTLInSeconds.Remove(mc.Bucket);
                }

                if (config.ObjectTimeToLiveInSeconds > 0)
                {
                    _objectTTLInSeconds.Add(mc.Bucket, config.ObjectTimeToLiveInSeconds);
                }
            }
            finally
            {
                _membaseLock.ReleaseLock();
            }
        }

        public void ResetConnection()
        {
            var t = new Thread(internalReset);
            t.Start();
        }

        private void internalReset()
        {
            try
            {
                var oldClients = _membaseClients;
                _membaseLock.AcquireWriterLock(-1);
                try
                {
                    _membaseClients = new Dictionary<string, MembaseClient>();
                    _objectTTLInSeconds = new Dictionary<string, int>();
                }
                finally
                {
                    _membaseLock.ReleaseLock();
                }

                InitMembaseClients();
                foreach (var cacheName in oldClients.Keys)
                {
                    oldClients[cacheName].Dispose();
                }
            }
            catch (Exception e)
            {
            }
        }

        private MembaseClient GetMembaseClient(string cacheName)
        {
            MembaseClient _membaseClient = null;
            _membaseLock.AcquireReaderLock(-1);
            try
            {
                if (_membaseClients.ContainsKey(cacheName))
                {
                    _membaseClient = _membaseClients[cacheName];
                }
            }
            finally
            {
                _membaseLock.ReleaseLock();
            }
            return _membaseClient;
        }

        public void Put(string cacheKey, object cacheItem, string cacheName, int cacheTtlInSeconds = 0)
        {
            try
            {
                CircuitBreaker.Apply put = delegate
                {
                    var membaseClient = GetMembaseClient(cacheName);
                    if (cacheTtlInSeconds != 0)
                    {
                        membaseClient.Store(Enyim.Caching.Memcached.StoreMode.Set, cacheKey, cacheItem, DateTime.Now.AddSeconds(cacheTtlInSeconds));
                    }
                    else if (_objectTTLInSeconds.ContainsKey(cacheName))
                    {
                        membaseClient.Store(Enyim.Caching.Memcached.StoreMode.Set, cacheKey, cacheItem,
                            DateTime.Now.AddSeconds(_objectTTLInSeconds[cacheName]));
                    }
                    else
                    {
                        membaseClient.Store(Enyim.Caching.Memcached.StoreMode.Set, cacheKey, cacheItem);
                    }
                    return null;
                };
                m_breaker.Intercept(put);
            }
            catch (OpenCircuitException)
            {
                //don't throw exception.  circuit is open so just write an info message
                EventLog.WriteEntry(ValueObjects.ServiceConstants.SERVICE_NAME, "Circuit still in open state",
                    EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                throw new Exceptions.BLException("Put cache operation failed to named cache: " + cacheName, ex);
            }
        }

        public object Get(string cacheKey, string cacheName)
        {
            object o = null;
            try
            {
                CircuitBreaker.Apply get = delegate
                {
                    var _membaseClient = GetMembaseClient(cacheName);
                    return _membaseClient.Get(cacheKey);
                };
                o = m_breaker.Intercept(get);
            }
            catch (OpenCircuitException oce)
            {
                //don't throw exception.  circuit is open so just write an info message
                EventLog.WriteEntry(ValueObjects.ServiceConstants.SERVICE_NAME, "Circuit still in open state",
                    EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                throw new Exceptions.BLException("Put cache operation failed to named cache: " + cacheName, ex);
            }
            return o;
        }

        public object Remove(string cacheKey, string cacheName)
        {
            object o = null;
            try
            {
                CircuitBreaker.Apply get = delegate
                {
                    var membaseClient = GetMembaseClient(cacheName);
                    return membaseClient.Remove(cacheKey);
                };
                o = m_breaker.Intercept(get);
            }
            catch (OpenCircuitException oce)
            {
                //don't throw exception.  circuit is open so just write an info message
                EventLog.WriteEntry(ValueObjects.ServiceConstants.SERVICE_NAME, "Circuit still in open state",
                    EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                throw new Exceptions.BLException("Remove cache operation failed to named cache: " + cacheName, ex);
            }
            return o;
        }

#if DEBUG

        public bool USE_NUNIT { get; set; }

        public bool USE_MEMBASE { get; set; }
#endif
    }
}