﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.DistributedCaching.ValueObjects.ServiceDefinitions;
using System.Diagnostics;
using Matchnet.DistributedCaching.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.DistributedCaching.BusinessLogic;
using System.Collections;

namespace Matchnet.DistributedCaching.ServiceManagers
{
    public class DistributedCachingSM : MarshalByRefObject, IDistributedCaching, IServiceManager, IBackgroundProcessor, IDisposable
    {
        private PerformanceCounter m_perfAllHitCount;

        public DistributedCachingSM()
        {
            initPerfCounters();

            System.Diagnostics.Trace.WriteLine("DistributedCachingSM contstructor completed.");
        }

        /// <summary>
        /// keep the object alive indefinitely
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region Instrumentation
        private static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { new CounterCreationData(ServiceConstants.PERF_TOTALCOUNT_NAME, "Counter fo all incoming requests.", PerformanceCounterType.NumberOfItems64) };
        }

        public static void PerfCounterInstall()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("DistributedCachingSM,Start PerfCounterInstall");

                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());

                PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME,
                    PerformanceCounterCategoryType.SingleInstance, ccdc);

                System.Diagnostics.Trace.WriteLine("DistributedCachingSM,Finish PerfCounterInstall");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("DistributedCachingSM Exception in PerfCounterInstall.");
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "PerfCounterInstall() error. ", ex);
            }
        }

        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
        }	

        private void initPerfCounters()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("DistributedCachingSM, Start initPerfCounters");

                m_perfAllHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_TOTALCOUNT_NAME, false);
                resetPerfCounters();

                System.Diagnostics.Trace.WriteLine("DistributedCachingSM, Finish initPerfCounters");
            }
            catch
            {
                System.Diagnostics.Trace.WriteLine("DistributedCachingSM exception in initPerfCounters()");
            }
        }

        private void resetPerfCounters()
        {
            m_perfAllHitCount.RawValue = 0;
            System.Diagnostics.Trace.WriteLine("Perf counters reseted.");
        }
        #endregion

        #region IDistributedCaching Members

        public void Put(string cacheKey, string cacheItem, string namedCacheName)
        {
            try
            {
                DistributedCachingBL.Instance.Put(cacheKey, cacheItem, namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Put() error. ", ex);
            }
        }

        public void Put(string cacheKey, object value, string namedCacheName, int cacheTtlInSeconds)
        {
            try
            {
                DistributedCachingBL.Instance.Put(cacheKey, value, namedCacheName, cacheTtlInSeconds);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Put() error. ", ex);
            }
        }

        public void Put(string cacheKey, ArrayList cacheItem, string namedCacheName)
        {
            try
            {
                DistributedCachingBL.Instance.Put(cacheKey, cacheItem, namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Put() error. ", ex);
            }
        }

        public void Put(string cacheKey, Array cacheItem, string namedCacheName)
        {
            try
            {
                DistributedCachingBL.Instance.Put(cacheKey, cacheItem, namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Put() error. ", ex);
            }
        }

        public void Put(string cacheKey, List<int> cacheItem, string namedCacheName)
        {
            try
            {
                DistributedCachingBL.Instance.Put(cacheKey, cacheItem, namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Put() error. ", ex);
            }
        }

        public void Remove(string cacheKey, string namedCacheName)
        {
            try
            {
                DistributedCachingBL.Instance.Remove(cacheKey, namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Remove() error. ", ex);
            }
        }

        public string GetString(string cacheKey, string namedCacheName)
        {
            try
            {
                return DistributedCachingBL.Instance.Get(cacheKey, namedCacheName) as string;
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Get() error. ", ex);
            }
        }

        public object Get(string cacheKey, string namedCacheName)
        {
            try
            {
                return DistributedCachingBL.Instance.Get(cacheKey, namedCacheName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Get() error. ", ex);
            }
        }

        public ArrayList GetArrayList(string cacheKey, string namedCacheName)
        {
            try
            {
                return DistributedCachingBL.Instance.Get(cacheKey, namedCacheName) as ArrayList;
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Get() error. ", ex);
            }
        }

        public Array GetArray(string cacheKey, string namedCacheName)
        {
            try
            {
                return DistributedCachingBL.Instance.Get(cacheKey, namedCacheName) as Array;
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Get() error. ", ex);
            }
        }

        public List<int> GetIntList(string cacheKey, string namedCacheName)
        {
            try
            {
                return DistributedCachingBL.Instance.Get(cacheKey, namedCacheName) as List<int>;
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Get() error. ", ex);
            }
        }

        #endregion

        #region IServiceManager Members

        public void PrePopulateCache()
        {
            // no implementation
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            // no implementation
        }

        #endregion

        #region IBackgroundProcessor Members

        public void Start()
        {
            System.Diagnostics.Trace.WriteLine("DistributedCachingSM start.");
        }

        public void Stop()
        {
            System.Diagnostics.Trace.WriteLine("DistributedCachingSM stop.");
        }

        #endregion

    }
}
