﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if DEBUG
using NUnit.Framework;
using Matchnet.DistributedCaching.BusinessLogic;
#endif

namespace Matchnet.DistributedCaching.BusinessLogic_Tests
{
#if DEBUG
    [TestFixture]
    public class DistributedCachingBLTests
    {
        [TestFixtureSetUp]
        public void StartUp()
        {
            DistributedCachingBL.Instance.USE_NUNIT = true;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            DistributedCachingBL.Instance.USE_NUNIT = false;
        }


        [Test]
        public void TestPutInAndGetFromCache()
        {
            DistributedCachingBL.Instance.USE_MEMBASE = true;
            Random r = new Random();
            int rInt = r.Next();
            string cacheName = "searchresults";
            string cacheKey = "default" + rInt;
            DistributedCachingBL.Instance.Put(cacheKey, cacheKey, cacheName);
            object cacheItem = DistributedCachingBL.Instance.Get(cacheKey, cacheName);
            Assert.NotNull(cacheItem);
            Assert.IsTrue(cacheItem is string, cacheItem.ToString() + " is not an string!!");
            Assert.AreEqual(cacheKey, cacheItem);
            DistributedCachingBL.Instance.USE_MEMBASE = false;
        }

        [Test]
        public void TestPutInAndGetFromCacheManyTimes()
        {
            DistributedCachingBL.Instance.USE_MEMBASE = true;
            Random r = new Random();
            int numberOfPuts = 100;
            for (int i = 0; i < numberOfPuts; i++)
            {
                int rInt = r.Next();
                int cacheInt = rInt % 3;
                string cacheName = "searchresults";
                switch (cacheInt)
                {
                    case 0:
                        cacheName = "accessscheduler";
                        break;
                    case 1:
                        cacheName = "emailtracker";
                        break;                    
                    default:
                        cacheName = "searchresults";
                        break;
                }
                string cacheKey = "default" + rInt;
                System.Threading.Thread.Sleep(r.Next(100, 500));
                DistributedCachingBL.Instance.Put(cacheKey, cacheKey, cacheName);
                object cacheItem = DistributedCachingBL.Instance.Get(cacheKey, cacheName);
                Assert.NotNull(cacheItem);
                Assert.IsTrue(cacheItem is string);
                Assert.AreEqual(cacheKey, cacheItem);
            }
            DistributedCachingBL.Instance.USE_MEMBASE = false;
        }
    }
#endif
}
