﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.RemotingServices;
using Matchnet.DistributedCaching.ServiceManagers;

namespace Matchnet.DistributedCaching.Service
{
    public partial class DistributedCaching : RemotingServiceBase
    {
        private DistributedCachingSM m_distributedCachingSM;

        public DistributedCaching()
        {
            InitializeComponent();
        }

        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new DistributedCaching() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        protected override void RegisterServiceManagers()
        {
            System.Diagnostics.Trace.WriteLine("DistributedCaching, Start RegisterServiceManagers");

            m_distributedCachingSM = new DistributedCachingSM();
            base.RegisterServiceManager(m_distributedCachingSM);

            base.RegisterServiceManagers();
        }
    }
}
