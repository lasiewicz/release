﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.DistributedCaching.ValueObjects
{
    public class ServiceConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_CONSTANT = "DISTRIBUTEDCACHING_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.DistributedCaching.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "DistributedCachingSM";
        /// <summary>
        /// Performance counter name for total hits.
        /// </summary>
        public const string PERF_TOTALCOUNT_NAME = "Total hit count";  
    }
}
