﻿#region

using System;
using System.Collections;
using System.Collections.Generic;

#endregion

namespace Matchnet.DistributedCaching.ValueObjects.ServiceDefinitions
{
    public interface IDistributedCaching
    {
        void Put(string cacheKey, ArrayList cacheItem, string namedCacheName);
        ArrayList GetArrayList(string cacheKey, string namedCacheName);
        void Put(string cacheKey, string value, string namedCacheName);
        void Put(string cacheKey, object value, string namedCacheName, int cacheTtlInSeconds);
        string GetString(string cacheKey, string namedCacheName);
        object Get(string cacheKey, string namedCacheName);
        void Put(string cacheKey, Array cacheItem, string namedCacheName);
        Array GetArray(string cacheKey, string namedCacheName);
        void Put(string cacheKey, List<int> cacheItem, string namedCacheName);
        void Remove(string cacheKey, string namedCacheName);
        List<int> GetIntList(string cacheKey, string namedCacheName);
    }
}