using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Renewal.BusinessLogic;
using Matchnet.Renewal.DataAccessLayer;
using Matchnet.Data.Configuration;
using Matchnet.Renewal.BusinessLogic.Adapter;
using Matchnet.Renewal.BusinessLogic.PaymentProfileService;

namespace Matchnet.Renewal.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox1;
        private TextBox textBox2;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(326, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 32);
            this.button1.TabIndex = 0;
            this.button1.Text = "Hit it!";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(24, 24);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(208, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Test GetRenewalEligibilityCount";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(24, 56);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(24, 303);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(708, 154);
            this.textBox2.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(24, 154);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(208, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "UPS - RenewPackage";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(24, 125);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(208, 23);
            this.button4.TabIndex = 5;
            this.button4.Text = "UPS - GetDefaultPaymentProfile";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(326, 125);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(216, 23);
            this.button5.TabIndex = 6;
            this.button5.Text = "Run Renewal - Matchnet Payment";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(326, 154);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(216, 23);
            this.button6.TabIndex = 7;
            this.button6.Text = "Run Renewal - UPS";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(24, 183);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(208, 23);
            this.button7.TabIndex = 8;
            this.button7.Text = "UPS - Renew Sub Only";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(24, 212);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(208, 23);
            this.button8.TabIndex = 9;
            this.button8.Text = "UPS - Renew MultiPackage";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(761, 481);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			RenewalBL rn = new RenewalBL();
			rn.Run();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			SqlConnection conn = null;
			try
			{
				conn = new SqlConnection(ConnectionDispenser.Instance.GetSqlConnection("mnSubscriptionRenew", 0).ConnectionString);
				conn.Open();
				this.textBox1.Text = RenewalDB.GetEligibleRenewalCount(103, conn).ToString();
			}
			catch (Exception ex)
			{
				this.textBox1.Text = ex.Message;
			}
			finally
			{
				if ( conn != null )
				{
					if ( conn.State == ConnectionState.Open )
					{
						conn.Close();
					}
				}
			}
		}

        private void button3_Click(object sender, EventArgs e)
        {
            //test UPS Purchase.RenewPackage()
            PurchaseServiceAdapter purchaseAdapter = new PurchaseServiceAdapter();
            int[] packageIDs = new int[] {1};
            int customerID = 200000;
            string paymentProfileID = "65ba55c9-7276-4c58-b36c-6eedf478bae7";
            int systemID = 100;
            int callingSystemTypeID = 1; //system
            int regionID = 3478792;
            try
            {
                purchaseAdapter.GetProxyInstance().RenewPackage(packageIDs, customerID, paymentProfileID, 1, systemID, callingSystemTypeID, regionID);
            }
            catch (Exception ex)
            {
                textBox2.Text = "";
                textBox2.Text = ex.Message;
            }

            textBox2.Text = "Finished calling UPS RenewPackage()";


            purchaseAdapter.CloseProxyInstance();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PaymentProfileServiceAdapter paymentProfileAdapter = new PaymentProfileServiceAdapter();
            int customerID = 200000;
            int systemID = 100;

            customerID = 100067359;
            systemID = 103;
            
            try
            {
                PaymentProfileInfo profileInfo = paymentProfileAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(customerID, systemID);

                textBox2.Text = "";
                textBox2.Text += "CallingSystemID: " + profileInfo.CallingsystemID.ToString() + "\r\n";
                textBox2.Text += "CustomerID: " + profileInfo.CustomerID.ToString() + "\r\n";
                textBox2.Text += "InsertDate: " + profileInfo.InsertDate.ToString() + "\r\n";
                textBox2.Text += "IsDefault: " + profileInfo.IsDefault.ToString() + "\r\n";
                textBox2.Text += "PaymentProfileID: " + profileInfo.PaymentProfileID + "\r\n";
                textBox2.Text += "PaymentProfileType: " + profileInfo.PaymentProfileType + "\r\n";
                textBox2.Text += "UpdatedDate: " + profileInfo.UpdatedDate.ToString() + "\r\n";
            }
            catch (Exception ex)
            {
                textBox2.Text = "";
                textBox2.Text = ex.Message;
            }

            paymentProfileAdapter.CloseProxyInstance();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //test non-ups renewal process
            //Note: "Execute" appsetting in config should be set to false if you don't want it to actually terminate or renew

            RenewalBL rn = new RenewalBL();
            rn.Run();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //test ups renewal process
            //Note: "Execute" appsetting in config should be set to false if you don't want it to actually terminate or renew

            RenewalBL20 rn = new RenewalBL20();
            rn.Run();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //test UPS Purchase.RenewPackage()
            PurchaseServiceAdapter purchaseAdapter = new PurchaseServiceAdapter();
            int[] packageIDs = new int[] { 10117 };
            int customerID = 100067359;
            string paymentProfileID = "65ba55c9-7276-4c58-b36c-6eedf478bae7";
            int systemID = 103;
            int callingSystemTypeID = 1; //system
            int regionID = 3478792;
            try
            {
                purchaseAdapter.GetProxyInstance().RenewPackage(packageIDs, customerID, paymentProfileID, 1, systemID, callingSystemTypeID, regionID);
            }
            catch (Exception ex)
            {
                textBox2.Text = "";
                textBox2.Text = ex.Message;
            }

            textBox2.Text = "Finished calling UPS RenewPackage()";


            purchaseAdapter.CloseProxyInstance();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //test UPS Purchase.RenewPackage()
            PurchaseServiceAdapter purchaseAdapter = new PurchaseServiceAdapter();
            int[] packageIDs = new int[] { 10117, 10113 };
            int customerID = 100067359;
            string paymentProfileID = "65ba55c9-7276-4c58-b36c-6eedf478bae7";
            int systemID = 103;
            int callingSystemTypeID = 1; //system
            int regionID = 3478792;
            try
            {
                purchaseAdapter.GetProxyInstance().RenewPackage(packageIDs, customerID, paymentProfileID, 1, systemID, callingSystemTypeID, regionID);
            }
            catch (Exception ex)
            {
                textBox2.Text = "";
                textBox2.Text = ex.Message;
            }

            textBox2.Text = "Finished calling UPS RenewPackage()";


            purchaseAdapter.CloseProxyInstance();
        }
	}
}
