using System;

namespace Matchnet.Renewal.BusinessLogic
{
	public interface IRunnable
	{
		void Run();
	}
}
