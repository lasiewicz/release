﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Renewal.BusinessLogic.PurchaseService;

namespace Matchnet.Renewal.BusinessLogic.Adapter
{
    /// <summary>
    /// Adapter for interfacing with Purchase Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance()
    /// at the end of the SM process.
    /// </summary>
    public class PurchaseServiceAdapter
    {
        private PurchaseServiceClient _Client = null;

        public PurchaseServiceClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new PurchaseServiceClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new PurchaseServiceClient();
                    }
                }
                else
                {
                    _Client = new PurchaseServiceClient();
                }

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
