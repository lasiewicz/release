using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using System.Messaging;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Payment.ServiceAdapters;
using Matchnet.Payment.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data.Configuration;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Renewal.DataAccessLayer;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters.Admin;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.Renewal.BusinessLogic
{
	public class RenewalBL : IRunnable
	{
		#region Service Name

		public const string SERVICE_NAME = "Matchnet.Renewal.Service";

		#endregion

		#region Performance Counter Members

		private PerformanceCounter perfRenewalsPerSecond;
		private Hashtable perfRenewalsProcessed = null;
		private Hashtable perfRenewalEligibleCount = null;

		#endregion

		#region Performance Monitor Counter Constants
		
		public const string RENEWAL_REQUESTS_PER_SECOND = "RenewalRequestsPerSecond";
		public const string RENEWAL_PROCESSED_PER_SECOND = "RenewalProcessedPerSecond";
		public const string RENEWAL_REMAINING_TO_BE_PROCESSED = "RenewalRemainingToBeProcessed";

		#endregion

		#region Private Members

		private Hashtable processing = null;
		private Hashtable renewGroups = null;
		private ArrayList memberTran = null;
		private object lockObject = null;
		private System.Timers.Timer timer = null;
		private System.Timers.Timer timerPerf = null;
		private DateTime timestamp;
		private int errors;
		private bool execute = false;

		private const string RenewsuccessStatusCode = "1000001";  

		#endregion

		#region Constructors

		public RenewalBL()
		{
			processing = new Hashtable();
			renewGroups = new Hashtable();
			memberTran = new ArrayList();
			lockObject = new Object();
			timer = new System.Timers.Timer( 86400000 );
			timer.AutoReset = true;
			timer.Enabled = true;
			timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
			timestamp = DateTime.Now;
			errors = 0;

			string value = ConfigurationSettings.AppSettings["Execute"];
			if ( value != null )
			{
				execute = Convert.ToBoolean( value );
			}

			value = ConfigurationSettings.AppSettings["Groups"];
			if ( value != null )
			{
				string[] groups = value.Split( ',' );
				for ( int i = 0; i < groups.Length; i++ )
				{
					renewGroups.Add( groups[ i ], groups[ i ] );
				}
			}

			initPerfCounters();
		}

		#endregion

		#region IRunnable Members

		public void Run()
		{
			while ( true )
			{
				SqlConnection conn = null;

				try
				{
					if ( ((TimeSpan)DateTime.Now.Subtract( timestamp )).TotalSeconds  > 5 )
					{
						timestamp = DateTime.Now;

						if ( errors >= 10 )
						{
							StopService();
						}
						else
						{
							errors = 0;
						}
					}
					
					conn = new SqlConnection(ConnectionDispenser.Instance.GetSqlConnection("mnSubscriptionRenew", 0).ConnectionString);
					conn.Open();
					load( conn );

					if ( memberTran.Count > 0 )
					{
						for ( int i = 0; i < memberTran.Count; i++ )
						{
							MemberTran mt = (MemberTran)memberTran[ i ];
                            Trace.WriteLine("Processing renewal for memberID:" + mt.MemberID.ToString() + ", groupID:" + mt.SiteID.ToString() + ", memberSubID:" + mt.MemberSubID.ToString() + ", memberTranID: " + mt.MemberTranID.ToString());
							if ( execute )
							{
								Command command = new Command("mnSubscription", "dbo.up_MemberTran_Save_Init_MemberPayment", 0);
								command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, mt.MemberID);
								command.AddParameter("@TranTypeID", SqlDbType.Int, ParameterDirection.Input, (int)TranType.Renewal);
								command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, mt.PlanID);
								command.AddParameter("@DiscountID", SqlDbType.Int, ParameterDirection.Input, mt.DiscountID);
								command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, mt.SiteID);
								command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, mt.MemberTranID);
								command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, mt.MemberPaymentID);

								string IPAddress = string.Empty;
								try
								{
									IPAddress = System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()).AddressList[0].ToString();
								}
								catch
								{
									// don't want to fail transaction just because couldn't pull local IP Address
									IPAddress = "127.0.0.1";
								}
								command.AddParameter("@IPAddress",SqlDbType.VarChar,ParameterDirection.Input,IPAddress);

								command.AddParameter("@EmailAddress",SqlDbType.VarChar,ParameterDirection.Input,"RenewalService@Spark.Net");

								//TL 0801808 Adding params for ESP project to record purchase mode, credit amount, start/end dates
								MemberSub memberSub = PurchaseSA.Instance.GetSubscription(mt.MemberID, mt.SiteID);
								DateTime updateDate = DateTime.Now; //passing in update date which is used internally to determine renew date, in order for it to sync up with our end date
								DateTime startDate = MemberTran.DetermineTransactionStartDate(mt, updateDate, memberSub);
								DateTime endDate = MemberTran.DetermineTransactionEndDate(startDate, mt, memberSub);
								if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
								{
									command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, updateDate);
									command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
									command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
								}
								command.AddParameter("@PurchaseModeID", SqlDbType.Int, ParameterDirection.Input, (int) mt.PurchaseMode);
								
								// Adding PromoID of the last transaction to this transaction
								if(mt.PromoID != Matchnet.Constants.NULL_INT)
									command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, mt.PromoID);
								
								SyncWriter sw = new SyncWriter();
								Exception ex;
								Int32 returnValue = sw.Execute(command, out ex);
								if (ex != null)
								{
									throw new Exception("up_MemberTran_Save_Init_MemberPayment error (memberTranID: " + mt.MemberTranID.ToString() + ").", ex);
								}
																
								if ( returnValue != Matchnet.Constants.RETURN_OK )
								{
									Trace.WriteLine( "ReturnValue: " + returnValue.ToString() + "|_Terminate()|MemberID:" + mt.MemberID.ToString() );
									terminate(mt.MemberID, mt.SiteID, 23);
								}
								else
								{
									System.Diagnostics.Trace.WriteLine("__amount: " + mt.Amount.ToString());
									if (mt.Amount != 0) 
									{
										Trace.WriteLine("__send");
										MessageQueue queue = new MessageQueue(Matchnet.Purchase.ValueObjects.PurchaseConstants.QUEUEPATH_PURCHASE_OUTBOUND);
										queue.Formatter = new BinaryMessageFormatter();
										queue.Send(mt, MessageQueueTransactionType.Single);
										Thread.Sleep(20);
									}
									else
									{
										Trace.WriteLine("__send zero amount");
										MessageQueue queue = new MessageQueue(Matchnet.Purchase.ValueObjects.PurchaseConstants.QUEUEPATH_PURCHASE_INBOUND);
										queue.Formatter = new BinaryMessageFormatter();
										queue.Send(new MemberTranResult(mt, null, MemberTranStatus.Success,RenewsuccessStatusCode, null, null, Matchnet.Constants.NULL_INT), MessageQueueTransactionType.Single);
										Thread.Sleep(20);
									}
								}

								//update performance counter on processing renewal
								PerformanceCounter pc = perfRenewalsProcessed[mt.SiteID.ToString()] as PerformanceCounter;
								if (pc != null)
								{
									pc.Increment();
								}

							}
						}

						memberTran.Clear();
					}
					else
					{
						Thread.Sleep( 60000 );
					}
				}
				catch(SqlException sqlEx)
				{
					errors++;
					LogError( sqlEx );
                    Trace.WriteLine("Error in Run(): " + sqlEx.Message);
				}
				catch(Exception ex)
				{
					errors++;
					LogError( ex );
                    Trace.WriteLine("Error in Run(): " + ex.Message);
				}
				finally
				{
					ClearMemberTranProcessing();
					if ( conn != null )
					{
						if ( conn.State == ConnectionState.Open )
						{
							conn.Close();
						}
					}
				}
			}
		}


		private void terminate(Int32 memberID,
			Int32 siteID,
			Int32 transactionReasonID)
		{
			try
			{
                if (execute)
                {
                    Command commandTerm = new Command("mnSubscription", "dbo.up_MemberSub_Save_Terminate", 0);
                    commandTerm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    commandTerm.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
                    commandTerm.AddParameter("@TransactionReasonID", SqlDbType.Int, ParameterDirection.Input, transactionReasonID);
                    commandTerm.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey(Constants.PRIMARY_KEY_MEMBERTRAN));

                    SyncWriter swTerm = new SyncWriter();
                    Exception exTerm;
                    swTerm.Execute(commandTerm, out exTerm);
                    if (exTerm != null)
                    {
                        throw exTerm;
                    }
                }
			}
			catch (Exception ex)
			{
				throw new Exception("Error processing terminate(memberID: " + memberID.ToString() + ", siteID: " + siteID.ToString() + ", transactionReasonID: " + transactionReasonID.ToString() + ").", ex);
			}
		}


		private void load( SqlConnection conn )
		{
			foreach ( string key in renewGroups.Keys )
			{
				int groupID = Convert.ToInt32( renewGroups[ key ] );
				Trace.WriteLine( "__load() begin GroupID:" + groupID.ToString() + " " + DateTime.Now.ToString());
				DataTable dt = loadDataTable(conn, groupID);
				
				if ( dt != null && dt.Rows.Count > 0 )
				{
					foreach ( DataRow dr in dt.Rows )
					{
						Trace.WriteLine( "__insert() GroupID:" + groupID.ToString() + " " + DateTime.Now.ToString());
						insert(conn, dr);
					}
				}
				Trace.WriteLine( "__load() end GroupID:" + groupID.ToString() + " " + DateTime.Now.ToString());
			}
		}

		private DataTable loadDataTable(
			SqlConnection conn,
			int groupID)
		{
			SqlDataReader sdr = null;
			SqlCommand cmd = RenewalDB.GetRenewals( groupID );

			try
			{
				DataTable dt = null;
				DataRow dr = null;
				cmd.Connection = conn;
				cmd.CommandTimeout = 60;

				sdr = cmd.ExecuteReader();

				if ( sdr != null )
				{
					dt = new DataTable();
					dt.Columns.Add( "MemberSubID" );
					dt.Columns.Add( "MemberID" );
					dt.Columns.Add( "GroupID" );
					dt.Columns.Add( "MemberPaymentID" );
					dt.Columns.Add( "PlanID" );
					dt.Columns.Add( "DiscountID" );
					dt.Columns.Add( "DiscountRemaining" );
					dt.Columns.Add( "CommunityID" );
					dt.Columns.Add( "CurrencyID" );
					dt.Columns.Add( "Amount" );
					dt.Columns.Add( "PromoID" );
					
					while ( sdr.Read() )
					{
						dr = dt.NewRow();
						dr["MemberSubID"] = sdr["MemberSubID"];
						dr["MemberID"] = sdr["MemberID"];
						dr["GroupID"] = sdr["GroupID"];
						dr["MemberPaymentID"] = sdr["MemberPaymentID"];
						dr["PlanID"] = sdr["PlanID"];
						dr["DiscountID"] = sdr["DiscountID"];
						dr["DiscountRemaining"] = sdr["DiscountRemaining"];
						dr["CommunityID"] = sdr["CommunityID"];
						dr["CurrencyID"] = sdr["CurrencyID"];
						dr["Amount"] = sdr["Amount"];
						dr["PromoID"] = sdr["PromoID"];
						dt.Rows.Add( dr );
					}
				}

				return dt;
			}
			catch (Exception ex)
			{
				//09082008 TL - Adding in additional logging info for troubleshooting sql errors.
				Exception exWrapper = new Exception(Matchnet.Data.DataUtility.buildErr(conn.ConnectionString, cmd), ex);
				throw exWrapper;
			}
			finally
			{
				if ( sdr != null && !sdr.IsClosed )
				{
					sdr.Close();
				}
			}
		}

		private void insert( 
			SqlConnection conn,
			DataRow dr )
		{
			int memberSubID = Convert.ToInt32( dr["MemberSubID"] );
			int memberID = Convert.ToInt32( dr["MemberID"] );
			int groupID = Convert.ToInt32( dr["GroupID"] );
			int memberPaymentID = Convert.ToInt32( dr["MemberPaymentID"] );
			int planID = Convert.ToInt32( dr["PlanID"] );
			int discountID = Convert.ToInt32( dr["DiscountID"] );
			int discountRemaining = Convert.ToInt32( dr["DiscountRemaining"] );
			int communityID = Convert.ToInt32( dr["CommunityID"] );
			CurrencyType currencyType = (CurrencyType)Enum.Parse(typeof(CurrencyType), dr["CurrencyID"].ToString());
			int memberTranID = KeySA.Instance.GetKey(Constants.PRIMARY_KEY_MEMBERTRAN);
			int chargeID = KeySA.Instance.GetKey(Constants.PRIMARY_KEY_CHARGE);
			decimal amount = getAmount( Convert.ToDecimal( dr["Amount"] ), discountID, discountRemaining );
						
			int promoID = Matchnet.Constants.NULL_INT;
			if (!(dr["PromoID"] is DBNull))
				promoID = Convert.ToInt32(dr["PromoID"]);
			
			NameValueCollection nvc = null;
			MemberPaymentInfo mpi = null;

			Trace.WriteLine( "__insert()|GetMemberPayment()|MemberID:" + memberID.ToString() + "|GroupID:" + groupID.ToString() + "|MemberPaymentID:" + memberPaymentID.ToString() + "|Amount:" + amount.ToString() + " " + DateTime.Now.ToString() );
			try
			{
				nvc = PaymentSA.Instance.GetMemberPayment( memberID, communityID, memberPaymentID );
				mpi = PaymentSA.Instance.GetMemberPaymentInfo( memberID, memberPaymentID );

				if (nvc == null)
				{
					throw new Exception("Cannot get member payment " + memberPaymentID.ToString() + " for member " + memberID.ToString());
				}

				if (mpi == null)
				{
					throw new Exception("Cannot get member payment info " + memberPaymentID.ToString());
				}
			}
			catch (Exception ex)
			{
				//Note: Renewal service will not terminate automatically if payment info is not found
                //which could be due to connection timeouts, so the renewal recyling process in the stored procs will take care of it.
                Trace.WriteLine("Get Payment error: " + ex.Message);
				new ServiceBoundaryException(SERVICE_NAME, "Get Payment info error, member (memberID: " + memberID.ToString() + ", groupID: " + groupID.ToString() + ")", ex);
				return;
			}
			
			CreditCardType creditCardType = GetCreditCardType( nvc["CreditCardType"], nvc["CreditCardNumber"] );
			int countryRegionID = nvc["Country"] == null ? Constants.DEFAULT_REGION_ID : Convert.ToInt32( nvc["Country"] );
			int expirationMonth = nvc["CreditCardExpirationMonth"] == null ? Constants.DEFAULT_EXPIRATION_MONTH : Convert.ToInt32( nvc["CreditCardExpirationMonth"] );
			int expirationYear = nvc["CreditCardExpirationYear"] == null ? DateTime.Now.Year : Convert.ToInt32( nvc["CreditCardExpirationYear"] );

			Trace.WriteLine( "__insert()|GetMemberTran()|MemberID:" + memberID.ToString() + "|GroupID:" + groupID.ToString() + "|MemberPaymentID:" + memberPaymentID.ToString() + " " + DateTime.Now.ToString() );
			MemberTran mt = GetMemberTran( currencyType, PaymentType.CreditCard, chargeID, memberTranID, memberID, Matchnet.Constants.NULL_INT, discountID, groupID, memberPaymentID, memberSubID, planID, amount, nvc["FirstName"], nvc["LastName"], nvc["IsraeliID"], nvc["Phone"], nvc["AddressLine1"], nvc["City"], nvc["State"], countryRegionID, nvc["PostalCode"], creditCardType, expirationMonth, expirationYear, promoID); 
			mt.SavePayment = false;
			mt.RequestType = Matchnet.Purchase.ValueObjects.RequestType.ProviderRequest;

			if ( renew( conn, memberID, groupID, communityID, memberPaymentID, mpi.PaymentTypeID ) )
			{
				bool add = false;
				Trace.WriteLine( "Attempting to add MemberID:" + memberID.ToString() );
				lock( lockObject )
				{
					if ( !processing.Contains( memberSubID ) )
					{
						add = true;
						processing.Add( memberSubID, mt );
						memberTran.Add( mt );
					}
				}
				if (!add)
				{
					Trace.WriteLine( "Not added MemberID:" + memberID.ToString() );
				}
			}
		}

		private bool renew(
			SqlConnection conn,
			int memberID,
			int groupID,
			int communityID,
			int memberPaymentID,
			int paymentTypeID)
		{
			int terminationReasonID = Matchnet.Constants.NULL_INT;
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember( memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None );
			
			if ( member == null )
			{
				throw new Exception( "Cannot find member " + memberID.ToString() );
			}
			
			Trace.WriteLine( "__renew()|GetAttributeInt()|MemberID:" + memberID.ToString() + "|GroupID:" + groupID.ToString() + "|MemberPaymentID:" + memberPaymentID.ToString() + " " + DateTime.Now.ToString() );
			int selfSuspendedFlag = member.GetAttributeInt( communityID, Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT, Constants.SELF_SUSPENDED_ATTRIBUTE_NAME, Matchnet.Constants.NULL_INT );
			int globalStatusMask = member.GetAttributeInt( Matchnet.Constants.NULL_INT, groupID, Matchnet.Constants.NULL_INT, Constants.GLOBAL_STATUS_MASK_ATTRIBUTE_NAME, Matchnet.Constants.NULL_INT );

			if ( selfSuspendedFlag == 1 )
			{
				terminationReasonID = Constants.SELF_SUSPENDED_REASON_ID;
			}

			if ( (globalStatusMask & 1) > 0 )
			{
				terminationReasonID = Constants.ADMIN_SUSPENDED_REASON_ID;
			}

			if ( memberPaymentID == Matchnet.Constants.NULL_INT )
			{
				terminationReasonID = Constants.UNABLE_TO_RENEW_REASON_ID;
			}

			if ( paymentTypeID == Constants.CHECK_PAYMENT_TYPE )
			{
				terminationReasonID = Constants.CHECK_PERIOD_ENDED_REASON_ID;
			}

			if ( paymentTypeID == Constants.DIRECT_DEBIT_PAYMENT_TYPE )
			{
				terminationReasonID = Constants.DIRECT_DEBIT_PERIOD_ENDED_REASON_ID;
			}

			if ( terminationReasonID != Matchnet.Constants.NULL_INT )
			{
				if ( execute )
				{
					terminate(memberID, groupID, terminationReasonID);
				}
				Trace.WriteLine( "__renew()|Terminate()|MemberID:" + memberID.ToString() + "|GroupID:" + groupID.ToString() + "|MemberPaymentID:" + memberPaymentID.ToString() + " " + DateTime.Now.ToString() );
				return false;
			}

			Trace.WriteLine( "__renew()|BeginRenewal()|MemberID:" + memberID.ToString() + "|GroupID:" + groupID.ToString() + "|MemberPaymentID:" + memberPaymentID.ToString() + " " + DateTime.Now.ToString() );
			return true;
		}

		private decimal getAmount(
			decimal amount,
			int discountID,
			int discountRemaining)
		{
			decimal newAmount = amount;

			if ( discountRemaining > 0 && discountID > 0)
			{
				DiscountCollection dc = PurchaseSA.Instance.GetDiscounts();

				if ( dc != null && dc.Count > 0 )
				{
					Discount d = dc.FindByID( discountID );

					if ( d != null )
					{
						switch ( d.DiscountType )
						{
							case DiscountType.PercentageDiscount:
								newAmount = amount * ( 1 - ( d.Amount / 100) );
								break;
							case DiscountType.MoneyDiscount:
								newAmount = amount - d.Amount;
								break;
							case DiscountType.AbsolutePrice:
								newAmount = d.Amount;
								break;
							default:
								throw new Exception( "DiscountType is invalid " + d.DiscountType.ToString() );
						}
					}
					else
					{
						throw new Exception( "Cannot find discount " + discountID.ToString() );
					}
				}
				else
				{
					throw new Exception( "Cannot get discounts" );
				}
			}

			return newAmount;
		}

		private MemberTran GetMemberTran( 
			CurrencyType currencyType,
			PaymentType paymentType,
			int chargeID,
			int memberTranID, 
			int memberID, 
			int adminMemberID,
			int discountID,
			int siteID,
			int memberPaymentID,
			int memberSubID,
			int planID,
			decimal amount,
			string firstName,
			string lastName,
			string israeliID,
			string phone,
			string addressLine1,
			string city,
			string state,
			int countryRegionID,
			string postalCode,
			CreditCardType creditCardType,
			int expirationMonth,
			int expirationYear,
			int promoID)
		{
			System.Diagnostics.Trace.WriteLine("__GetMemberTran() " + firstName + ", " + lastName);

			try
			{
				Matchnet.Purchase.ValueObjects.Payment payment = null;
				SiteMerchant merchant = null;

				SiteMerchantCollection siteMerchantCollection = PurchaseSA.Instance.GetSiteMerchants(siteID, currencyType, paymentType);
				if (siteMerchantCollection.Count == 0)
				{
					System.Diagnostics.Trace.WriteLine("__Error getting siteMerchant");
					throw new Exception("Error getting siteMerchant (memberID: " + memberID.ToString() + ", memberTranID: " + memberTranID.ToString() + ", siteID: " + siteID.ToString() + ", currencyType: " + currencyType.ToString() + ", paymentType: " + paymentType.ToString() + ").");
				}
				merchant = siteMerchantCollection[0];
 
				payment = new CreditCardPayment(memberID,
					siteID,
					memberPaymentID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					paymentType,
					currencyType,
					null,
					expirationMonth,
					expirationYear,
					creditCardType,
					null,
					israeliID );
			
				MemberTran mt = new MemberTran(
					memberTranID,
					chargeID,
					Matchnet.Constants.NULL_INT,
					memberID,
					siteID,
					currencyType,
					null,
					TranType.Renewal,
					null,
					memberSubID,
					planID,
					null,
					null,
					discountID,
					memberPaymentID,
					payment,
					null,
					null,
					MemberTranStatus.None,
					null,
					null,
					adminMemberID,
					amount,
					Matchnet.Constants.NULL_INT,
					DurationType.Month,
					null,
					Matchnet.Constants.NULL_INT,
					DateTime.Now,
					DateTime.Now,
					null,
					null,
					paymentType,
					merchant,
					Matchnet.Constants.NULL_INT,
					promoID);

				if (mt.Plan == null)
				{
					//09242008 TL/JR/WL - Getting Plan object from Purchase to populate MemberTran, needed to calculate start/end dates of ESPremium
					mt.Plan = PlanSA.Instance.GetPlan(planID, this.getBrand(siteID).BrandID);
                    if (mt.Plan != null)
                    {
                        Trace.WriteLine("Called PlanSA to get plan " + mt.Plan.PlanID.ToString() + " successfully.");
                    }
                    else
                    {
                        Trace.WriteLine("Called PlanSA to get plan " + mt.Plan.PlanID.ToString() + " did not return a plan.");
                    }
				}

				return mt;


			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", " ").Replace("\n", " "));
				throw new Exception("GetMemberTran() error (memberTranID: " + memberTranID.ToString() + ", siteID: " + siteID.ToString() + ", currencyType: " + currencyType.ToString() + ", paymentType: " + paymentType.ToString() + ").", ex);
			}
		}

		private CreditCardType GetCreditCardType(
			string creditCardType,
			string creditCardNumber)
		{
			if ( creditCardType == null )
			{
				if ( creditCardNumber != null )
				{
					if ( creditCardNumber.Substring( 0, 1 ) == "4" )
					{
						return CreditCardType.Visa;
					}
					if ( creditCardNumber.Substring( 0, 1 ) == "5" )
					{
						return CreditCardType.MasterCard;
					}
					if ( creditCardNumber.Substring( 0, 1 ) == "6" )
					{
						return CreditCardType.Discover;
					}
					if ( creditCardNumber.Substring( 0, 2 ) == "34" ||
						creditCardNumber.Substring( 0, 2 ) == "37" )
					{
						return CreditCardType.AmericanExpress;
					}
					if ( creditCardNumber.Substring( 0, 2 ) == "38" ||
						creditCardNumber.Substring( 0, 2 ) == "36" )
					{
						return CreditCardType.DinersClub;
					}
				}
				return CreditCardType.Visa;
			}
			else
			{
				return (CreditCardType)Enum.Parse(typeof(CreditCardType), creditCardType );
			}
		}

		private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			Trace.WriteLine( "__timer_Elapsed() " + DateTime.Now.ToString() );
			lock( lockObject )
			{
				processing.Clear();
			}
		}

		private void perfTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			SqlConnection conn = null;
			try
			{
				conn = new SqlConnection(ConnectionDispenser.Instance.GetSqlConnection("mnSubscriptionRenew", 0).ConnectionString);
				conn.Open();

				//update performance counter
				foreach (string group in perfRenewalEligibleCount.Keys)
				{
					PerformanceCounter pc = perfRenewalEligibleCount[group] as PerformanceCounter;
					if (pc != null)
					{
						pc.RawValue = RenewalDB.GetEligibleRenewalCount(Convert.ToInt32(group), conn);
					}
				}
			}
			catch (Exception ex)
			{
				this.LogError(ex);
			}
			finally
			{
				if ( conn != null )
				{
					if ( conn.State == ConnectionState.Open )
					{
						conn.Close();
					}
				}
			}
		}

		private void StopService()
		{
			ServiceController sc = new ServiceController( SERVICE_NAME );
			if ( sc != null )
			{
				sc.Stop();
			}
		}

		private void LogError(Exception ex)
		{
			try 
			{
				string msg = ex.Message + "\r\n";
				
				Exception inner = ex.InnerException;
				while (inner != null)
				{
					msg += inner.Message;
					inner = inner.InnerException;
				}
				
				msg += "\r\n" + ex.StackTrace;

				EventLog.WriteEntry( "Matchnet.Renewal.Service", msg, System.Diagnostics.EventLogEntryType.Error );
			} 
			catch (Exception ex1)
            {
                Trace.WriteLine("LogError() error: " + ex1.Message);
            }
		}

		/// <summary>
		/// This function will clear the current set of member tran being processed by renewal, so
		/// that they can be recreated as a part of the renewal system, without having to wait 24 hours.
		/// This is made to be used when a SQL Exception occurs, and will help to avoid duplicate errors.
		/// </summary>
		private void ClearMemberTranProcessing()
		{
			try
			{
				if (memberTran != null && memberTran.Count > 0)
				{
					for ( int i = 0; i < memberTran.Count; i++ )
					{
						MemberTran mt = (MemberTran)memberTran[i];
						if (processing.Contains(mt.MemberSubID))
						{
							processing.Remove(mt.MemberSubID);
						}
					}

					memberTran.Clear();
				}
                Trace.WriteLine("ClearMemberTranProcessing() has completed.");
			}
			catch
			{
			}
		}

		private Matchnet.Content.ValueObjects.BrandConfig.Brand getBrand(int siteid)
		{
			Matchnet.Content.ValueObjects.BrandConfig.Brands  brands = BrandConfigSA.Instance.GetBrandsBySite(siteid);
			Matchnet.Content.ValueObjects.BrandConfig.Brand lowestBrand=null;
			int lowestBrandID = Matchnet.Constants.NULL_INT;
			foreach(Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
			{
				if (lowestBrandID == Matchnet.Constants.NULL_INT)
				{
					lowestBrandID = brand.BrandID;
				}
				else if (brand.BrandID < lowestBrandID)
				{
					// The assumption is that the lowest brand ID for all the brands
					// of a site will provide the default brand ID for the site 
					// In the case that the lowest brand ID is not the default brand 
					// ID for the site, then the correct plan will still be retrieved.
					// However, a collection of plans for a new brand ID will be cached.  
					lowestBrandID = brand.BrandID;
				}
			}
			lowestBrand=Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(lowestBrandID);
			return lowestBrand;
			

		}

		#endregion

		#region instrumentation

		public static void PerfCounterInstall()
		{	
			if ( PerformanceCounterCategory.Exists(SERVICE_NAME) )
			{
				PerformanceCounterCategory.Delete(SERVICE_NAME);
			}

			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData(RENEWAL_REQUESTS_PER_SECOND, RENEWAL_REQUESTS_PER_SECOND, PerformanceCounterType.RateOfCountsPerSecond32)
													 });

			//add Performance Counters per site
			string groupsValue = "4,13,15,101,103,105,107,112,9171,19";//ConfigurationSettings.AppSettings["Groups"];
			if ( groupsValue != null )
			{
				string[] groups = groupsValue.Split( ',' );
				foreach (string group in groups)
				{
					ccdc.Add(new CounterCreationData(RENEWAL_PROCESSED_PER_SECOND + group.Trim(), RENEWAL_PROCESSED_PER_SECOND + group, PerformanceCounterType.RateOfCountsPerSecond32));
					ccdc.Add(new CounterCreationData(RENEWAL_REMAINING_TO_BE_PROCESSED + group.Trim(), RENEWAL_REMAINING_TO_BE_PROCESSED + group, PerformanceCounterType.NumberOfItems32));
				}
			}

			PerformanceCounterCategory.Create(SERVICE_NAME, SERVICE_NAME, ccdc);
		}

		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(SERVICE_NAME);
		}

		private void initPerfCounters()
		{
			perfRenewalsPerSecond = new PerformanceCounter(SERVICE_NAME, RENEWAL_REQUESTS_PER_SECOND, false);

			//get reference to performance counters per site
			perfRenewalEligibleCount = new Hashtable();
			perfRenewalsProcessed = new Hashtable();
			foreach (string group in renewGroups.Keys)
			{
				perfRenewalEligibleCount.Add(group, new PerformanceCounter(SERVICE_NAME, RENEWAL_REMAINING_TO_BE_PROCESSED + group.Trim(), false));
				perfRenewalsProcessed.Add(group, new PerformanceCounter(SERVICE_NAME, RENEWAL_PROCESSED_PER_SECOND + group.Trim(), false));
			}

			resetPerfCounters();

			//initialize Timer to execute perf counter that updates every..
			timerPerf = new System.Timers.Timer(300000); //5 minutes
			timerPerf.AutoReset = true;
			timerPerf.Enabled = true;
			timerPerf.Elapsed += new System.Timers.ElapsedEventHandler(perfTimer_Elapsed);
		}

		private void resetPerfCounters()
		{
			perfRenewalsPerSecond.RawValue = 0;

			foreach (string group in perfRenewalsProcessed.Keys)
			{
				PerformanceCounter pc = perfRenewalsProcessed[group] as PerformanceCounter;
				if (pc != null)
				{
					pc.RawValue = 0;
				}
			}

			foreach (string group in perfRenewalEligibleCount.Keys)
			{
				PerformanceCounter pc = perfRenewalsProcessed[group] as PerformanceCounter;
				if (pc != null)
				{
					pc.RawValue = 0;
				}
			}
			
		}

		#endregion
	}
}
