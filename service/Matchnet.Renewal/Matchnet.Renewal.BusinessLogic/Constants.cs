using System;

using Matchnet;

namespace Matchnet.Renewal.BusinessLogic
{
	public sealed class Constants
	{
		private Constants()
		{
		}

		// Transaction Reason IDs
		public const int SELF_SUSPENDED_REASON_ID = 21;
		public const int ADMIN_SUSPENDED_REASON_ID = 20;
		public const int UNABLE_TO_RENEW_REASON_ID = 23;
		public const int CHECK_PERIOD_ENDED_REASON_ID = 22;
		public const int DIRECT_DEBIT_PERIOD_ENDED_REASON_ID = 42;

		// Primary keys
		public const string PRIMARY_KEY_MEMBERTRAN = "MemberTranID";
		public const string PRIMARY_KEY_CHARGE = "ChargeID";

		// Payment types
		public const int CREDIT_CARD_PAYMENT_TYPE = 1;
		public const int CHECK_PAYMENT_TYPE = 2;
        public const int DIRECT_DEBIT_PAYMENT_TYPE = 4;
        public const int PAYPAL_PAYMENT_TYPE = 8;
        public const int ELECTRONICFUNDSTRANSFER_PAYMENT_TYPE = 128;
        public const int UNSUPPORTED_PAYMENT_TYPE = 0;

		// Attribute Names
		public const string SELF_SUSPENDED_ATTRIBUTE_NAME = "SelfSuspendedFlag";
        public const string GLOBAL_STATUS_MASK_ATTRIBUTE_NAME = "GlobalStatusMask";
        public const string REGION_ID_ATTRIBUTE_NAME = "RegionID";

		// Defaults
		public const int DEFAULT_REGION_ID = 223;
		public const int DEFAULT_EXPIRATION_MONTH = 12;
	}
}
