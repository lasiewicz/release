using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet;
using Matchnet.Data;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Renewal.DataAccessLayer
{
	sealed public class RenewalDB
	{
		#region Constructors

		private RenewalDB()
		{
		}

		#endregion

		#region Methods

		public static SqlCommand GetRenewals(
			int groupID)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "dbo.up_MemberSub_GetRenewal";
			cmd.Parameters.Add("@GroupID", SqlDbType.Int).Value = groupID;
			return cmd;
		}

        public static SqlCommand GetALaCarteRenewals(
            int memberSubID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dbo.up_MemberSubExtended_GetRenewal";
            cmd.Parameters.Add("@MemberSubID", SqlDbType.Int).Value = memberSubID;
            return cmd;
        }

		/// <summary>
		/// Gets a count representing number of eligible members up for renewal
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public static int GetEligibleRenewalCount(int siteID, SqlConnection conn)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = conn;
			cmd.CommandTimeout = 60;
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "dbo.up_Eligible_Renewal_Counts";
			cmd.Parameters.Add("@siteid", SqlDbType.Int).Value = siteID;
			int renewalCount = 0;

			SqlDataReader reader = null;
			try
			{
				reader = cmd.ExecuteReader();
			
				if (reader != null)
				{
					if (reader.Read())
					{
						renewalCount = reader.GetInt32(reader.GetOrdinal("renewaleligibal"));
					}
				}
			}
			finally
			{
				if (reader != null)
				{
					reader.Close();
				}
			}

			return renewalCount;
		}


		#endregion

	}
}
