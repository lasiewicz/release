using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Renewal.BusinessLogic;

namespace Matchnet.Renewal.Service
{
	public class RenewalService : System.ServiceProcess.ServiceBase
	{
		private System.ComponentModel.Container components = null;
		private Thread thread = null;
		private FileStream fs = null;
		private HydraWriter _hw;

		public RenewalService()
		{
			InitializeComponent();
		}

		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new RenewalService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = RenewalBL.SERVICE_NAME;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnStart(string[] args)
		{
			
			base.OnStart (args);

			BooleanSwitch traceSwitch = new BooleanSwitch("TraceLog", "Enables/Disables all log tracking");
			
			if (traceSwitch.Enabled)
			{
				string path = ConfigurationSettings.AppSettings["RenewalTraceLogPath"];

				if ( path != null )
				{
					if ( !Directory.Exists( path ) )
					{
						Directory.CreateDirectory( path );
					}

					string file = ConfigurationSettings.AppSettings["RenewalTraceLog"];

					if ( file != null )
					{
						fs = new FileStream( path + file, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite ); 
						TextWriterTraceListener listener = new TextWriterTraceListener(fs);
						Trace.Listeners.Add(listener);
					}
				}
			}
			
			_hw = new HydraWriter(new string[]{"mnSubscription"});
			_hw.Start();

			//07152009 TL - Renewal Integration with Unified Purchase System
            bool isUPSIntegrationEnabled = false;
            try
            {
                //isUPSIntegrationEnabled = (Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED").ToLower() == "true");
                isUPSIntegrationEnabled = (ConfigurationSettings.AppSettings["IS_UPS_INTEGRATION_ENABLED"].ToLower() == "true");
            }
            catch (Exception ex)
            {
                isUPSIntegrationEnabled = false;
                Trace.WriteLine("Error reading configuration setting for IS_UPS_INTEGRATION_ENABLED; " + ex.Message);

            }

            Trace.WriteLine("Configuration setting for IS_UPS_INTEGRATION_ENABLED is set to " + isUPSIntegrationEnabled.ToString());

            //isUPSIntegrationEnabled = true;
            if (isUPSIntegrationEnabled)
			{
				RenewalBL20 rn20 = new RenewalBL20();
				thread = new Thread(new ThreadStart(rn20.Run));
			}
			else
			{
				RenewalBL rn  = new RenewalBL();
				thread = new Thread( new ThreadStart(rn.Run) );
			}
			
			thread.Start();
		}
 
		protected override void OnStop()
		{
			_hw.Stop();

			if ( fs != null )
			{
				fs.Close();
			}

			if ( thread != null ) 
			{
				if ( (thread.ThreadState & System.Threading.ThreadState.Suspended) != 0 ) 
				{
					thread.Resume();
				}
				thread.Abort();
			}
		}
	}
}
