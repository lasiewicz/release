using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

using Matchnet.Renewal.BusinessLogic;

namespace Matchnet.Renewal.Service
{
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer
	{
		#region Private Members

		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
		private System.ServiceProcess.ServiceInstaller serviceInstaller1;
		private System.ComponentModel.Container components = null;
		
		#endregion

		#region Constructors

		public ProjectInstaller()
		{
			InitializeComponent();
			this.serviceInstaller1.ServiceName = "Matchnet.Renewal.Service";
		}

		#endregion

		#region Dispose

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion


		#region Events

		private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
		{
			RenewalBL.PerfCounterInstall();
		}


		private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
		{
			RenewalBL.PerfCounterUninstall();
		}

		#endregion

		#region Component Designer generated code
		
		private void InitializeComponent()
		{
			this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
			this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
			// 
			// serviceProcessInstaller1
			// 
			this.serviceProcessInstaller1.Password = null;
			this.serviceProcessInstaller1.Username = null;
			// 
			// serviceInstaller1
			// 
			this.serviceInstaller1.ServiceName = RenewalBL.SERVICE_NAME;
			this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller1,
																					  this.serviceInstaller1});

			this.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_BeforeUninstall);
			this.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterInstall);
		}
		#endregion
	}
}
