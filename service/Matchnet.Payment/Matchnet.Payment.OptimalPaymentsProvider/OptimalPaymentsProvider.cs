using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.OptimalPaymentsProvider
{
	public class OptimalPaymentsProvider : Provider
	{
		public const string FIELD_OPERATION = "operation";
		public const string FIELD_EXPMONTH = "expMonth";
		public const string FIELD_EXPYEAR = "expYear";
		public const string FIELD_EXPDATE = "cardExp";
		public const string FIELD_FNAME = "custFname";
		public const string FIELD_LNAME = "custLname";
		public const string FIELD_NAME = "custName1";
		public const string FIELD_CARDTYPE = "cardType";
		public const string FIELD_CARDNUMBER = "cardNumber";
		public const string FIELD_AMOUNT = "amount";
		public const string FIELD_MERCHANTTXN = "merchantTxn";
		public const string FIELD_TXNNUMBER = "txnNumber";
		public const string FIELD_SETTLENUMBER = "settleNumber";
		public const string FIELD_CVDINDICATOR = "cvdIndicator";
		public const string FIELD_STREET = "streetAddr";
		public const string FIELD_PHONE = "phone";
		public const string FIELD_EMAIL = "email";
		public const string FIELD_CITY = "city";
		public const string FIELD_PROVINCE = "province";
		public const string FIELD_ZIP = "zip";
		public const string FIELD_COUNTRY = "country";
		public const string FIELD_STATUS = "status";
		public const string FIELD_PAYMENTTYPEID = "PaymentTypeID";
		public const string FIELD_TRANTYPEID = "TranTypeID";
		public const string FIELD_AUTHTIME = "authTime";
		public const string FIELD_AUTHCODE = "authCode";
		public const string FIELD_AVSINFO = "avsInfo";
		public const string FIELD_CVDINFO = "cvdInfo";
		public const string FIELD_CURAMOUNT = "curAmount";
		public const string FIELD_PREVIOUS = "previousCustomer";

		public const string CARD_AMEX = "AM";
		public const string CARD_DINERS = "DC";
		public const string CARD_DISCOVER = "DI";
		public const string CARD_FIREPAY = "FP";
		public const string CARD_MASTER = "MC";
		public const string CARD_SOLO = "SO";
		public const string CARD_SWITCH = "SW";
		public const string CARD_VISA = "VI";

		private string request;
		private string response;

		public override string RawRequest() 
		{
			return request;
		}

		public override string RawResponse()
		{
			return response;
		}

		protected override ProviderResponse DoPurchase(MemberTran mt)
		{
			try
			{
				Array Keys = Array.CreateInstance(typeof(String), providerProperties.Count);
				providerProperties.Keys.CopyTo(Keys,0);

				/*				** FOR TESTING ONLY REMOVE ME ** 
								Amount less than 20.00 = Approval
								20.00 to 29.99 = (221, 1002) Reenter 
								30.00 to 39.99 = (221, 1003) Referral 
								40.00 to 49.99 = (221, 1004) PickUp 
								50.00 to 59.99 = (34, 1005) Decline 
								60.00 to 69.99 = (2) Timeout 
								Amount greater than 69.99 = Approval
				*/				
				//								providerProperties["amount"] = "15.00";


				//Combine Keys
				if (providerProperties.ContainsKey(FIELD_EXPYEAR) && providerProperties.ContainsKey(FIELD_EXPMONTH) )
				{
					//Format Expiration
					string year = providerProperties[FIELD_EXPYEAR].ToString();
					year = year.Length  > 2 ? year.Substring(year.Length - 2) : year;
					string month = providerProperties[FIELD_EXPMONTH].ToString();
					month = month.Length == 2 ? month : "0" + month;
					//Remove old keys and add the new one
					providerProperties.Remove(FIELD_EXPYEAR);
					providerProperties.Remove(FIELD_EXPMONTH);
					providerProperties.Add(FIELD_EXPDATE, month + "/" + year);
				}
				if (providerProperties.Contains(FIELD_FNAME) && providerProperties.Contains(FIELD_LNAME))
				{
					//Format Name
					string name =System.Convert.ToString(providerProperties[FIELD_FNAME]).Trim() + " " + System.Convert.ToString(providerProperties[FIELD_LNAME]).Trim();
					providerProperties.Remove(FIELD_FNAME);
					providerProperties.Remove(FIELD_LNAME);
					providerProperties.Add(FIELD_NAME, name);
				}
					
				//Decode CreditCardType
				if (providerProperties.Contains(FIELD_CARDTYPE))
				{
					string cardType = providerProperties[FIELD_CARDTYPE].ToString();
					switch (Convert.ToInt16(cardType))
					{
						case 1:	//American Express
							providerProperties[FIELD_CARDTYPE] = CARD_AMEX;
							break;
						case 4: // Diners Club
							providerProperties[FIELD_CARDTYPE] = CARD_DINERS;
							break;
						case 5: //Switch
							providerProperties[FIELD_CARDTYPE] = CARD_SWITCH;
							break;
						case 8: //Discover Card
							providerProperties[FIELD_CARDTYPE] = CARD_DISCOVER;
							break;
						case 9: //Solo
							providerProperties[FIELD_CARDTYPE] = CARD_SOLO;
							break;
						case 32: //Master Card
							providerProperties[FIELD_CARDTYPE] = CARD_MASTER;
							break;
						case 64: //Visa
							providerProperties[FIELD_CARDTYPE] = CARD_VISA;
							break;
						default:
							break;
					}
				}

				// Check if this is a renewal and make adjustments if needed.
				if (mt.TranType == TranType.Renewal)
				{
					providerProperties[FIELD_CVDINDICATOR] = "0";
					providerProperties[FIELD_PREVIOUS] = "Y";
					if (providerProperties.ContainsKey(FIELD_CVDINFO))
					{
						providerProperties.Remove(FIELD_CVDINFO);
					}				
				}	

				string referer = string.Empty;
				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

				string data = FormatProperties(providerProperties);
				data = data + "&" + FIELD_OPERATION + "=P"; // Purchase
				request = data;
				string url = (string)providerProperties[FIELD_URL];
				string res = sendData(url, data, referer);
				response = res;

				OptimalPaymentsResponse opResponse = new OptimalPaymentsResponse();
				opResponse.FromString(res);
				return opResponse;
			}
			catch(Exception ex)
			{
				throw new Exception("Error in OptimalPaymentsProcessor: DoPurchase(). MemberId = " + mt.MemberID + 
					", MemberTranId = " + mt.MemberTranID + ", MemberTran properties = " + mt.ToString(),ex);
			}		
		}

		private string FormatProperties(Hashtable props) 
		{
			try
			{
				StringBuilder builder = new StringBuilder();
				//Format Amount
				if (props.Contains(FIELD_AMOUNT))
				{	
					// Convert Amount so that $5.00 is sent as 500
					props[FIELD_AMOUNT] = System.Convert.ToInt32((System.Convert.ToDecimal(props[FIELD_AMOUNT].ToString()) * 100)).ToString();
				}

				// Format for sending to OP
				bool first = true;
				foreach (string key in props.Keys) 
				{
					if (!IsExcludedField(key)) 
					{
						if (first) 
						{
							first = false;
						} 
						else 
						{
							builder.Append("&");
						}
						string val = System.Convert.ToString(props[key]);
						builder.Append(key + "=" + val);
					}
				}
				return builder.ToString();
			}
			catch (Exception ex) 
			{
				throw new Exception("Error in OptimalPaymentsProcessor: FormatData(). " + props.ToString(), ex);
			}
		}

		public override ProviderResponse Authorize(MemberTran mt) 
		{
			// This is a cut & paste job from DoPurchase() but we had less than 1 day to implement
			try
			{

				/*				** FOR TESTING ONLY REMOVE ME ** 
								Amount less than 20.00 = Approval
								20.00 to 29.99 = (221, 1002) Reenter 
								30.00 to 39.99 = (221, 1003) Referral 
								40.00 to 49.99 = (221, 1004) PickUp 
								50.00 to 59.99 = (34, 1005) Decline 
								60.00 to 69.99 = (2) Timeout 
								Amount greater than 69.99 = Approval
				*/				
				//				providerProperties["amount"] = "15.00";


				//Combine Keys
				if (providerProperties.ContainsKey(FIELD_EXPYEAR) && providerProperties.ContainsKey(FIELD_EXPMONTH) )
				{
					//Format Expiration
					string year = providerProperties[FIELD_EXPYEAR].ToString();
					year = year.Length  > 2 ? year.Substring(year.Length - 2) : year;
					string month = providerProperties[FIELD_EXPMONTH].ToString();
					month = month.Length == 2 ? month : "0" + month;
					//Remove old keys and add the new one
					providerProperties.Remove(FIELD_EXPYEAR);
					providerProperties.Remove(FIELD_EXPMONTH);
					providerProperties.Add(FIELD_EXPDATE, month + "/" + year);
				}
				if (providerProperties.Contains(FIELD_FNAME) && providerProperties.Contains(FIELD_LNAME))
				{
					//Format Name
					string name =System.Convert.ToString(providerProperties[FIELD_FNAME]).Trim() + " " + System.Convert.ToString(providerProperties[FIELD_LNAME]).Trim();
					providerProperties.Remove(FIELD_FNAME);
					providerProperties.Remove(FIELD_LNAME);
					providerProperties.Add(FIELD_NAME, name);
				}
					
				//Decode CreditCardType
				if (providerProperties.Contains(FIELD_CARDTYPE))
				{
					string cardType = providerProperties[FIELD_CARDTYPE].ToString();
					switch (Convert.ToInt16(cardType))
					{
						case 1:	//American Express
							providerProperties[FIELD_CARDTYPE] = CARD_AMEX;
							break;
						case 4: // Diners Club
							providerProperties[FIELD_CARDTYPE] = CARD_DINERS;
							break;
						case 5: //Switch
							providerProperties[FIELD_CARDTYPE] = CARD_SWITCH;
							break;
						case 8: //Discover Card
							providerProperties[FIELD_CARDTYPE] = CARD_DISCOVER;
							break;
						case 9: //Solo
							providerProperties[FIELD_CARDTYPE] = CARD_SOLO;
							break;
						case 32: //Master Card
							providerProperties[FIELD_CARDTYPE] = CARD_MASTER;
							break;
						case 64: //Visa
							providerProperties[FIELD_CARDTYPE] = CARD_VISA;
							break;
						default:
							break;
					}
				}

				//Decode Country
				if (providerProperties.Contains(FIELD_COUNTRY))
				{
					Region region = RegionBL.Instance.GetRegion( Convert.ToInt32( mt.Payment.CountryRegionID ) );
					providerProperties[FIELD_COUNTRY] = region.IsoCountryCode2;
				}

				string referer = string.Empty;
				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

				string data = FormatProperties(providerProperties);
				data = data + "&" + FIELD_OPERATION + "=A"; // Authorize
				request = data;
				string url = (string)providerProperties[FIELD_URL];
				string res = sendData(url, data, referer);
				response = res;

				OptimalPaymentsResponse opResponse = new OptimalPaymentsResponse();
				opResponse.FromString(res);
				return opResponse;
			}
			catch(Exception ex)
			{
				throw new Exception("Error in OptimalPaymentsProcessor: Authorize(). " + providerProperties.ToString(),ex);
			}
		}

		public override ProviderResponse Void(MemberTran mt) 
		{
			try
			{
				string referer = string.Empty;

				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

				RemovePurchaseProperites(providerProperties);

				if (providerProperties.ContainsKey(FIELD_MERCHANTTXN))
				{
					providerProperties.Remove(FIELD_MERCHANTTXN);
				}

				providerProperties[FIELD_SETTLENUMBER] = "0";
				
				string data = FormatProperties(providerProperties);

				data = data + "&" + FIELD_OPERATION + "=X"; // Cancel
				request = data;
				string url = (string)providerProperties[FIELD_URL];
				string res = sendData(url, data, referer);
				response = res;

				OptimalPaymentsResponse opResponse = new OptimalPaymentsResponse();
				opResponse.FromString(res);
				return opResponse;
			}
			catch(Exception ex)
			{
				throw new Exception("Error in OptimalPaymentsProcessor: Void(). " + providerProperties.ToString(),ex);
			}

		}

		protected override ProviderResponse DoCredit(MemberTran mt)
		{
			try
			{
				string referer = string.Empty;
				// Remove all the providerProperties related to purchase
				RemovePurchaseProperites(providerProperties);

				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}
				
				providerProperties[FIELD_TXNNUMBER] = getLogValue(mt.MemberID, mt.ReferenceMemberTranID, FIELD_TXNNUMBER);
				providerProperties[FIELD_MERCHANTTXN] = mt.MemberTranID.ToString();
				// OP doesn't want the amount as a negative value and that is the way the Credit Tool passes it.
				providerProperties[FIELD_AMOUNT] = ((mt.Amount < 0) ? (mt.Amount * -1): mt.Amount).ToString();  
				
				string data = FormatProperties(providerProperties);

				data = data + "&" + FIELD_OPERATION + "=CR"; // Credit
				request = data;
				string url = (string)providerProperties[FIELD_URL];
				string res = sendData(url, data, referer);
				response = res;

				OptimalPaymentsResponse opResponse = new OptimalPaymentsResponse();
				opResponse.FromString(res);
				return opResponse;
			}
			catch(Exception ex)
			{
				throw new Exception("Error in OptimalPaymentsProcessor: DoCredit(). MemberId = " + mt.MemberID + 
					", ReferenceMemberTranID = " + mt.ReferenceMemberTranID + " Error Message: " + ex.Message,ex);
			}
		}


		private void RemovePurchaseProperites(Hashtable props)
		{
			// These properties only for Purchase but need to be removed for Credit/CancelSettlement
			try
			{
				if (props.ContainsKey(FIELD_EXPYEAR))
				{
					props.Remove(FIELD_EXPYEAR);
				}
				if (props.ContainsKey(FIELD_EXPMONTH))
				{
					props.Remove(FIELD_EXPMONTH);
				}
				if (props.ContainsKey(FIELD_FNAME))
				{
					props.Remove(FIELD_FNAME);
				}
				if (props.ContainsKey(FIELD_LNAME))
				{
					props.Remove(FIELD_LNAME);
				}
				if (props.ContainsKey(FIELD_CARDTYPE))
				{	
					props.Remove(FIELD_CARDTYPE);
				}
				if (props.ContainsKey(FIELD_CARDNUMBER))
				{
					props.Remove(FIELD_CARDNUMBER);
				}
				if (props.ContainsKey(FIELD_CVDINDICATOR))
				{
					props.Remove(FIELD_CVDINDICATOR);
				}
				if (props.ContainsKey(FIELD_STREET))
				{
					props.Remove(FIELD_STREET);
				}
				if (props.ContainsKey(FIELD_PHONE))
				{
					props.Remove(FIELD_PHONE);
				}
				if (props.ContainsKey(FIELD_EMAIL))
				{
					props.Remove(FIELD_EMAIL);
				}
				if (props.ContainsKey(FIELD_CITY))
				{
					props.Remove(FIELD_CITY);
				}					
				if (props.ContainsKey(FIELD_PROVINCE))
				{
					props.Remove(FIELD_PROVINCE);
				}
				if (props.ContainsKey(FIELD_ZIP))
				{
					props.Remove(FIELD_ZIP);
				}
				if (props.ContainsKey(FIELD_COUNTRY))
				{
					props.Remove(FIELD_COUNTRY);
				}
				if (props.ContainsKey(FIELD_STATUS))
				{
					props.Remove(FIELD_STATUS);
				}
				if (props.ContainsKey(FIELD_PAYMENTTYPEID))
				{
					props.Remove(FIELD_PAYMENTTYPEID);
				}
				if (props.ContainsKey(FIELD_TRANTYPEID))
				{
					props.Remove(FIELD_TRANTYPEID);
				}
				if (props.ContainsKey(FIELD_CARDNUMBER))
				{
					props.Remove(FIELD_CARDNUMBER);
				}
				if (props.ContainsKey(FIELD_AMOUNT))
				{
					props.Remove(FIELD_AMOUNT);
				}
				if (props.ContainsKey(FIELD_CURAMOUNT))
				{
					props.Remove(FIELD_CURAMOUNT);
				}
				if (props.ContainsKey(FIELD_AUTHTIME))
				{
					props.Remove(FIELD_AUTHTIME);
				}
				if (props.ContainsKey(FIELD_AUTHCODE))
				{
					props.Remove(FIELD_AUTHCODE);
				}
				if (props.ContainsKey(FIELD_AVSINFO))
				{
					props.Remove(FIELD_AVSINFO);
				}
				if (props.ContainsKey(FIELD_CVDINFO))
				{
					props.Remove(FIELD_CVDINFO);
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error in OptimalPaymentsProcessor: RemovePurchaseProperties(). " + props.ToString(), ex);
			}
		}
	}
}
