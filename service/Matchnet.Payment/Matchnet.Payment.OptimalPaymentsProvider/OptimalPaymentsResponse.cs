using System;
using System.Collections;
using System.Web;

using Matchnet.Payment.BusinessLogic;

namespace Matchnet.Payment.OptimalPaymentsProvider
{
	[Serializable]
	public class OptimalPaymentsResponse : ProviderResponse
	{
		public const string FIELD_STATUS = "status";
		public const string FIELD_ERROR = "errCode";

		public void FromString(string response) 
		{
			try 
			{
				string[] properties = response.Split("&".ToCharArray());
				for (int x = 0; x < properties.Length; x++) 
				{
					string[] prop = properties[x].Split("=".ToCharArray());
					string name = HttpUtility.UrlDecode((prop[0]));
					string val = HttpUtility.UrlDecode((prop[1]));
					Property property = new Property(name, val);
					AddProperty(property);
					switch (name)
					{
						case FIELD_STATUS:
						switch (val) 
						{
							case "SP": // Purchase, Settlement, Credit 
							case "A" : // Authorization
							case "C" : // Cancel Settlement
								Success = true;
								ResponseCode = "0";
								break;
							case "E":
							default:
								Success = false;
								break;
						}
							break;
						case FIELD_ERROR:
							ResponseCode = val;
							break;
					}
				}
			}
			catch(Exception ex)
			{
				throw new Exception("Error in OptimalPyamentsResponse: FromString(). response = " + response, ex);
			}
		}
	}
}
