using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

using Matchnet.Payment.BusinessLogic;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Payment.ValueObjects;

namespace Matchnet.Payment.TestProvider
{
	public class TestProvider : Provider
	{
		private const string FIELD_BILLADDRESS1 = "addressLine1";

		protected override ProviderResponse DoPurchase(MemberTran mt)
		{
			return getTestResponse(mt);
		}


		public override ProviderResponse Authorize(MemberTran mt)
		{
			return getTestResponse(mt);
		}


		protected override ProviderResponse DoCredit(MemberTran mt)
		{
			return getTestResponse(mt);
		}


		public override ProviderResponse Void(MemberTran mt)
		{
			return getTestResponse(mt);
		}

		private ProviderResponse getTestResponse(MemberTran mt)
		{
			
			string Address = mt.Payment.AddressLine1;
			Address = Address.ToUpper().Trim();
			string responseCode = string.Empty;
			/*if(0 == Address.IndexOf("SPARKTESTER:"))
			{
				responseCode = Address.Substring(12);
			}*/
			
			responseCode = Address;

			bool success = (Address == "0");
			//07222008 MM - this should always return true and it should only be used on dev anyway. 
			//responseCode = Address;
			//bool success = true;
			return new ProviderResponse(success, responseCode, string.Empty, string.Empty, string.Empty, mt, 0);
		}


		public override string RawRequest()
		{
			return null;
		}


		public override string RawResponse()
		{
			return null;
		}
	}
}
