
using System;
using System.IO;
using System.Text;

namespace BinLoader
{
	class BinLoader
	{
//		This is a console app developed for the purpose of creating the bin files used by Litle.  The agreement with the business is that
//		they will supply two excel sheets (US and International) for each site.  This app will take those two files and create a "bin.txt" file
//		for use by the PaymentService.  Usage: "C:\BinLoader international_bins_AS.csv us_bins__AS.csv" Result: bin.txt
//		

		static void Main(string[] args)
		{
			try
			{
				StreamReader sr = new StreamReader(args[0]);
				StreamReader sr2 = new StreamReader(args[1]);
				StreamWriter sw = new StreamWriter("bin.txt");
				string line;
				string[] lineSplit;

				while ((line = sr.ReadLine()) != null)
				{
					lineSplit = line.Split(',');

					if (lineSplit[lineSplit.GetUpperBound(0)] == "Y")
					{
						sw.WriteLine(lineSplit[lineSplit.GetUpperBound(0) - 1]);
					}
				}
				sr.Close();

				while ((line = sr2.ReadLine()) != null)
				{
					lineSplit = line.Split(',');

					if (lineSplit[lineSplit.GetUpperBound(0)] == "Y")
					{
						sw.WriteLine(lineSplit[lineSplit.GetUpperBound(0) - 1]);
					}
				}
				sr2.Close();

				sw.Close();

				Console.WriteLine("Done!");

			}
			catch (Exception ex)
			{
				Console.WriteLine("Error: " + ex.Message);
			}
		}
	}
}

