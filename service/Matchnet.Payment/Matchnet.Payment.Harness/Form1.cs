using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

using Matchnet.Data;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Payment.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
//using MatchnetBedrockConversion;
using Matchnet.Payment.ValueObjects;


namespace Matchnet.Payment.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button4 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.button5 = new System.Windows.Forms.Button();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(16, 336);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(152, 56);
			this.button2.TabIndex = 1;
			this.button2.Text = "Generate Sales File";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(528, 24);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(208, 20);
			this.textBox1.TabIndex = 3;
			this.textBox1.Text = "textBox1";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(424, 352);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(152, 48);
			this.button4.TabIndex = 4;
			this.button4.Text = "Retrieve file from DB";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(424, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 23);
			this.label1.TabIndex = 5;
			this.label1.Text = "File Name:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(528, 72);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(208, 20);
			this.textBox2.TabIndex = 6;
			this.textBox2.Text = "textBox2";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(424, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "Output loc:";
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(592, 352);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(152, 48);
			this.button5.TabIndex = 8;
			this.button5.Text = "Decrypt Sales File";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(528, 120);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(208, 20);
			this.textBox3.TabIndex = 9;
			this.textBox3.Text = "textBox3";
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(528, 168);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(208, 20);
			this.textBox4.TabIndex = 10;
			this.textBox4.Text = "textBox4";
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(528, 216);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(208, 20);
			this.textBox5.TabIndex = 11;
			this.textBox5.Text = "textBox5";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(384, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(112, 23);
			this.label3.TabIndex = 12;
			this.label3.Text = "Public key loc:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(384, 176);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(120, 23);
			this.label4.TabIndex = 13;
			this.label4.Text = "Secret key loc:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(424, 216);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 23);
			this.label5.TabIndex = 14;
			this.label5.Text = "Password:";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(784, 498);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBox5);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream fs = new FileStream(@"c:\matchnet\17.body", FileMode.Open);
			ProviderResponse pr = formatter.Deserialize(fs) as ProviderResponse;
			fs.Close();


			StreamWriter sw = new StreamWriter("out.sql", false, System.Text.Encoding.Unicode);

			sw.WriteLine("exec dbo.up_MemberPayment_Save_CreditCard @MemberPaymentID = " + pr.MemberTran.Payment.MemberPaymentID.ToString() + ",");
			sw.WriteLine("@MemberID = " + pr.MemberTran.MemberID + ",");
			sw.WriteLine("@GroupID = " + pr.MemberTran.SiteID.ToString() + ",");
			sw.WriteLine("@FirstName = '" + pr.MemberTran.Payment.FirstName + "',");
			sw.WriteLine("@LastName = '" + pr.MemberTran.Payment.LastName + "',");
			sw.WriteLine("@Phone = '" + pr.MemberTran.Payment.Phone + "',");
			sw.WriteLine("@AddressLine1 = '" + pr.MemberTran.Payment.AddressLine1 + "',");
			sw.WriteLine("@City = '" + pr.MemberTran.Payment.City + "',");
			sw.WriteLine("@State = '" + pr.MemberTran.Payment.State + "',");
			sw.WriteLine("@CountryRegionID = " + pr.MemberTran.Payment.CountryRegionID.ToString() + ",");
			sw.WriteLine("@PostalCode = '" + pr.MemberTran.Payment.PostalCode + "',");
			//sw.WriteLine("@CreditCardNumber = '" + new Crypto().Encrypt(((CreditCardPayment)pr.MemberTran.Payment).CreditCardNumber) + "',");
			sw.WriteLine("@CreditCardExpirationMonth = " + ((CreditCardPayment)pr.MemberTran.Payment).ExpirationMonth.ToString() + ",");
			sw.WriteLine("@CreditCardExpirationYear = " + ((CreditCardPayment)pr.MemberTran.Payment).ExpirationYear.ToString() + ", ");
			sw.WriteLine("@CreditCardType = " + ((Int32)((CreditCardPayment)pr.MemberTran.Payment).CreditCardType).ToString());
			
			
			sw.WriteLine("insert into Charge (ChargeID, MemberID, MemberTranID, MemberPaymentID, GroupID, ChargeTypeID, ChargeAmount, ChargeStatus, InsertDate) values (" + pr.MemberTran.ChargeID.ToString() + ", " + pr.MemberTran.MemberID.ToString() + ", " + pr.MemberTran.MemberTranID + ", " + pr.MemberTran.Payment.MemberPaymentID.ToString() + ", " + pr.MemberTran.SiteID.ToString() + ", " + ((Int32)pr.MemberTran.PaymentType).ToString() + ", " + pr.MemberTran.Amount + ", " + (pr.Success ? "2" : "3") + ", '" + pr.MemberTran.InsertDate.ToString() + "')");

			Hashtable log = getUniqueLogValues(pr.Properties);
			
			if (log != null)
			{
				IDictionaryEnumerator de = log.GetEnumerator();
				while (de.MoveNext())
				{
					string val = "";

					if (de.Value != null)
					{
						val = de.Value.ToString();
					}

					sw.WriteLine("exec dbo.up_ChargeLog_Save @ChargeID = " + pr.MemberTran.ChargeID.ToString() + ",");
					sw.WriteLine("@MerchantID = " + pr.MemberTran.SiteMerchant.MerchantID.ToString() + ",");
					sw.WriteLine("@Name = '" + de.Key + "',");
					sw.WriteLine("@Value = '" + val + "',");
					sw.WriteLine("@ResponseTypeID = " + ((Int32)RequestType.ProviderRequest).ToString() + ",");
					sw.WriteLine("@UpdateDate = '" + pr.MemberTran.InsertDate.ToString() + "'");

				}				
			}

			sw.Close();
		}

		private Hashtable getUniqueLogValues(ArrayList properties)
		{
			Hashtable newProperties = null;

			if ( properties != null && properties.Count > 0 )
			{
				newProperties = new Hashtable();
				
				for ( int i = 0; i < properties.Count; i++ )
				{
					Matchnet.Payment.BusinessLogic.Property property = (Matchnet.Payment.BusinessLogic.Property)properties[ i ];
					string name = property.Name;
					string value = property.Value;
						
					if ( !newProperties.Contains( name ) )
					{
						for ( int j = 0; j < properties.Count; j++ )
						{
							if ( ((Matchnet.Payment.BusinessLogic.Property)properties[ j ]).Name == name && j != i )
							{
								value = value + "|" + ((Matchnet.Payment.BusinessLogic.Property)properties[ j ]).Value;
							}
						}
						newProperties.Add( name, value.Replace("\r", "").Replace("\n", "") );
					}
				}
			}

			return newProperties;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			int rec = PaymentSA.Instance.GenerateEncryptedSalesFile();
			MessageBox.Show("Files Generated! Records process: " + rec.ToString());
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			SqlDataReader dataReader = null;
		
			try
			{
				Command command = new Command("mnFileExchange", "dbo.MikeTestSel", 0);
				command.AddParameter("@FileName", SqlDbType.VarChar, ParameterDirection.Input, textBox1.Text);
				
				dataReader = Client.Instance.ExecuteReader(command);

				while(dataReader.Read())
				{
					FileStream fs = new FileStream(textBox2.Text + textBox1.Text, FileMode.OpenOrCreate, FileAccess.Write);
					BinaryWriter bw = new BinaryWriter(fs);

					long startIndex = 0;
                    int bufferSize = 100;
					byte[] outbyte = new byte[bufferSize];
					long retval;

                    retval = dataReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);
					while(retval == bufferSize)
					{
						bw.Write(outbyte);
						bw.Flush();

						startIndex += bufferSize;
						retval = dataReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);
					}

					bw.Write(outbyte, 0, (int)retval);
					bw.Flush();

					bw.Close();
					fs.Close();
				}

			}			
			finally
			{
				if(dataReader != null)
				{
					dataReader.Close();
				}
			}
			

		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			PGP1_1.Crypto.Instance.Decrypt(textBox2.Text + textBox1.Text , textBox3.Text, 
				textBox4.Text, textBox5.Text, textBox2.Text + "DECRYPTED_" + textBox1.Text);  
		}

	}
}
