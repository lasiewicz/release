using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.RemotingServices;
using Matchnet.Payment.ServiceManagers;
using Matchnet.Payment.ValueObjects;


namespace Matchnet.Payment.Service
{
	public class PaymentService : RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private PaymentSM _paymentSM;

		public PaymentService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] {new PaymentService()};
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void RegisterServiceManagers()
		{
			_paymentSM = new PaymentSM();
			base.RegisterServiceManager(_paymentSM);
			base.RegisterServiceManagers ();
		}

		
		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}
	}
}
