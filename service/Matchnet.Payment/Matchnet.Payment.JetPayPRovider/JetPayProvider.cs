using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Payment.JetPayProvider
{
	public class JetPayProvider : Provider
	{
		public const string FIELD_TRANSACTIONTYPE = "TransactionType";
		public const string FIELD_TRANSACTIONID = "TransactionID";
		public const string FIELD_TOTALAMOUNT = "TotalAmount";
		public const string FIELD_CARDEXPYEAR = "CardExpYear";
		public const string FIELD_CARDEXPMONTH = "CardExpMonth";
		public const string FIELD_BILLINGCOUNTRY = "BillingCountry";
		public const string FIELD_BILLINGADDRESS = "BillingAddress";
		public const string FIELD_BILLINGCITY = "BillingCity";
		public const string FIELD_BILLINGPHONE = "BillingPhone";
		public const string FIELD_BILLINGPOSTALCODE = "BillingPostalCode";
		public const string FIELD_BILLINGSTATEPROV = "BillingStateProv";
		public const string FIELD_MERCHANTID = "MerchantID";
		public const string FIELD_MERCHANTID_VISA = "MerchantID_VISA";  // Part of AS hack
		public const string FIELD_CARDNUM = "CardNum";
		public const string FIELD_APPROVAL = "Approval";
		public const string FIELD_ORIGIN = "Origin";

		public const string AUTH_AMOUNT = ".10";

		public const string TRANSACTIONTYPE_SALE = "SALE";
		public const string TRANSACTIONTYPE_CREDIT = "CREDIT";
		public const string TRANSACTIONTYPE_AUTH = "AUTHONLY";
		public const string TRANSACTIONTYPE_VOID = "VOID";

		public const string RESPONSE_SUCCESS = "000";

		public const string FIELD_VBV_RESPONSE_CAVV = "Cavv";
		public const string FIELD_VBV_RESPONSE_ECI_FLAG = "Eci";
		public const string FIELD_VBV_RESPONSE_XID = "Xid";

		private string xmlRequest;
		private string xmlResponse;

		public override string RawRequest() 
		{
			return xmlRequest;
		}

		public override string RawResponse()
		{
			return xmlResponse;
		}

		protected override ProviderResponse DoPurchase(MemberTran mt)
		{
			string referer = string.Empty;

			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

			tweakMerchantID(providerProperties);  //AS Hack

			bool renewFlag = false;
			if (mt.TranType == TranType.Renewal || mt.IgnoreCVV)
			{
				renewFlag = true;
			}

			string xmlDoc = GetXMLDoc(renewFlag, mt);
			string url = (string)providerProperties[FIELD_URL];

#if DEBUG
			XmlDocument requestDoc = new XmlDocument();
			requestDoc.LoadXml(xmlDoc);
			requestDoc.Save(@"c:\temp\requestJetPay" + _guid.ToString() + ".xml");
#endif

			JetPayResponse response = ParseResult(SendXML(url, xmlDoc, referer));
			return response;
		}

		public override ProviderResponse Authorize(MemberTran mt) 
		{
			// make sure we are doing an auth-only
			if (providerProperties.ContainsKey(FIELD_TRANSACTIONTYPE)) 
			{
				providerProperties.Remove(FIELD_TRANSACTIONTYPE);
			}
			providerProperties.Add(FIELD_TRANSACTIONTYPE, TRANSACTIONTYPE_AUTH);
			
			tweakMerchantID(providerProperties);  //AS Hack
			
			string xmlDoc = GetXMLDoc(false, mt);

			// get the referer
			string referer = string.Empty;
			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

			string url = (string)providerProperties[FIELD_URL];
			JetPayResponse response = ParseResult(SendXML(url, xmlDoc, referer));
			return response;
		}

		public override ProviderResponse Void(MemberTran mt) 
		{
			// make sure we are doing an auth-only
			if (providerProperties.ContainsKey(FIELD_TRANSACTIONTYPE)) 
			{
				providerProperties.Remove(FIELD_TRANSACTIONTYPE);
			}
			providerProperties.Add(FIELD_TRANSACTIONTYPE, TRANSACTIONTYPE_VOID);
			
			string xmlDoc = GetVoidXMLDoc();

			// get the referer
			string referer = string.Empty;
			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

#if DEBUG
			XmlDocument requestDoc = new XmlDocument();
			requestDoc.LoadXml(xmlDoc);
			requestDoc.Save(@"c:\temp\requestJetPay" + _guid.ToString() + ".xml");
#endif

			string url = (string)providerProperties[FIELD_URL];
			JetPayResponse response = ParseResult(SendXML(url, xmlDoc, referer));

			return response;
		}

		protected override ProviderResponse DoCredit(MemberTran mt)
		{
			// the credit request is the same as the charge request,
			// except for this field
			if (providerProperties.ContainsKey(FIELD_TRANSACTIONTYPE)) 
			{
				providerProperties.Remove(FIELD_TRANSACTIONTYPE);
			}
			providerProperties.Add(FIELD_TRANSACTIONTYPE, TRANSACTIONTYPE_CREDIT);

			if (providerProperties.ContainsKey(FIELD_TRANSACTIONID)) 
			{
				providerProperties.Remove(FIELD_TRANSACTIONID);
			}
			providerProperties.Add(FIELD_TRANSACTIONID, System.Convert.ToString(mt.MemberTranID));

			if (providerProperties.ContainsKey(FIELD_TOTALAMOUNT)) 
			{
				providerProperties.Remove(FIELD_TOTALAMOUNT);
			}

			providerProperties.Add(FIELD_TOTALAMOUNT, System.Convert.ToString((mt.Amount < 0) ? (mt.Amount * -1): mt.Amount));

			tweakMerchantID(providerProperties);  //AS Hack

			string referer = string.Empty;

			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

			string xmlDoc = GetXMLDoc(false, mt);
			
			xmlRequest = xmlDoc;
			
			string url = (string)providerProperties[FIELD_URL];
			JetPayResponse response = ParseResult(SendXML(url, xmlDoc, referer));
			return response;
		}

		private string FormatValue(string key, string val, bool renewFlag) 
		{
			string retVal = val;
			switch (key) 
			{
				case FIELD_TRANSACTIONID:
					retVal = val.PadLeft(18, '0');
					break;
				case FIELD_TOTALAMOUNT:
					retVal = System.Convert.ToString(System.Convert.ToDouble(val) * 100);
					break;
				case FIELD_CARDEXPYEAR:
					retVal = val.Length == 2 ? val : val.Substring(val.Length - 2);
					break;
				case FIELD_CARDEXPMONTH:
					retVal = val.Length == 2 ? val : "0" + val;
					break;
				case FIELD_ORIGIN:
					retVal = renewFlag ? "RECURRING" : val;
					break;
			}
			return XMLEncodeValue(retVal);
		}

		private string GetXMLDoc(bool renewFlag, MemberTran mt) 
		{
			PaymentType paymentType = PaymentType.CreditCard;

			if (providerProperties.ContainsKey(FIELD_PAYMENT_TYPE)) 
			{
				paymentType = (PaymentType)Enum.Parse(typeof(PaymentType), providerProperties[FIELD_PAYMENT_TYPE].ToString() );
			}

			StringBuilder builder = new StringBuilder();
			builder.Append("<JetPay>\r\n");
			
			if (paymentType == PaymentType.CreditCard) 
			{
				foreach (string key in providerProperties.Keys) 
				{
					if (!IsExcludedField(key)) 
					{
						if (renewFlag) 
						{
							if (!ExcludedRecurring(key)) 
							{
								AppendXML(ref builder, key, renewFlag);
							}
						} 
						else 
						{
							AppendXML(ref builder, key, renewFlag);
						}
					}
				}

				if ( mt.TranType == TranType.InitialBuy )
				{
					GetVerificationProperties( mt, ref builder );
				}
			} 
			else 
			{
				// build up check xml document (ACH)
				AppendXML(ref builder, "TransactionType", renewFlag);
				AppendXML(ref builder, "Origin", renewFlag);
				AppendXML(ref builder, "MerchantID", renewFlag);
				AppendXML(ref builder, "UDField1", renewFlag);
				AppendXML(ref builder, "TotalAmount", renewFlag);
				AppendXML(ref builder, "OrderNumber", renewFlag);
				AppendXML(ref builder, "TransactionID", renewFlag);
			
				// hack for card name, we probably want to store this differently in MemberPaymentAttribute 
				builder.Append("\t<CardName>" + (string)providerProperties["FirstName"] + " " + (string)providerProperties["LastName"] + "</CardName>\r\n");
				//AppendXML(ref builder, ref properties, "CardName", renewFlag); // name on check

				builder.Append("\t<ACH Type=\"" + (string)providerProperties["AccountType"] + "\">\r\n");
				builder.Append("\t");
				AppendXML(ref builder, "AccountNumber", renewFlag);
				builder.Append("\t");
				AppendXML(ref builder, "ABA", renewFlag);
				builder.Append("\t");
				AppendXML(ref builder, "CheckNumber", renewFlag);
				builder.Append("\t</ACH>\r\n");
			}

			builder.Append("</JetPay>");
			return builder.ToString();
		}

		private void GetVerificationProperties(
			MemberTran mt,
			ref StringBuilder builder)
		{
			if ( mt.PaRes != null && mt.PaRes.Approved )
			{
				builder.Append("\t<Verification Type=\"VbV\">\r\n");
				builder.Append("\t\t<" + FIELD_VBV_RESPONSE_CAVV + " Usage=\"2\" Encoding=\"BASE64\">" + mt.PaRes.Cavv + "</" + FIELD_VBV_RESPONSE_CAVV + ">\r\n");
				builder.Append("\t\t<" + FIELD_VBV_RESPONSE_ECI_FLAG + ">" + mt.PaRes.EciFlag + "</" + FIELD_VBV_RESPONSE_ECI_FLAG + ">\r\n");
				builder.Append("\t\t<" + FIELD_VBV_RESPONSE_XID + " Encoding=\"BASE64\">" + mt.PaRes.Xid + "</" + FIELD_VBV_RESPONSE_XID + ">\r\n");
				builder.Append("\t</Verification>\r\n");
			}
		}

		private string GetVoidXMLDoc() 
		{
			PaymentType paymentType = PaymentType.CreditCard;

			if (providerProperties.ContainsKey(FIELD_PAYMENT_TYPE)) 
			{
				paymentType = (PaymentType)Enum.Parse(typeof(PaymentType), providerProperties[FIELD_PAYMENT_TYPE].ToString() );
			}

			if (providerProperties.ContainsKey(FIELD_TOTALAMOUNT)) 
			{
				Decimal amount = Convert.ToDecimal(providerProperties[FIELD_TOTALAMOUNT]);
				providerProperties[FIELD_TOTALAMOUNT] = System.Convert.ToString((amount < 0) ? (amount * -1): amount);
			}


			StringBuilder builder = new StringBuilder();
			builder.Append("<JetPay>\r\n");
			
			// only support credit card for this
			if (paymentType == PaymentType.CreditCard) 
			{
				foreach (string key in providerProperties.Keys) 
				{
					if (IsVoidField(key)) 
					{
						AppendXML(ref builder, key, false);
					}
				}
			} 

			builder.Append("</JetPay>");
			return builder.ToString();
		}
		
		private void AppendXML(ref StringBuilder builder, string key, bool renewFlag) 
		{
			builder.Append("\t<" + key + ">" + FormatValue(key, (string)providerProperties[key], renewFlag) + "</" + key + ">\r\n");
		}

		private string SendXML(string url, string xmlDoc, string referer) 
		{
			try 
			{
				return PostData(url, xmlDoc, referer);
			} 
			catch (Exception ex) 
			{
				string hostName = "";
				try 
				{
					hostName = Dns.GetHostName();
				} 
				catch
				{ 
					// ignore so we can still log the correct error 
				}
				throw new Exception("Unable to post data to Provider [JetPay - " + url + "]\r\n\r\n" + xmlDoc + "\r\n\r\nHost:" + hostName, ex);
			}
		}

		// ** NOTE: We should remove PostData() and replace it with calls to the Provider class like we did in Optimal and Litle in the rewrite
		//
		public static string PostData(string url, string data, string referer) 
		{
			return PostData(url, data, referer, System.Threading.Timeout.Infinite);
		}
		// ** NOTE: We should remove PostData() and replace it with calls to the Provider class like we did in Optimal and Litle in the rewrite
		//
		public static string PostData(string url, string data, string referer, int timeout) 
		{
			try 
			{
				if(url.StartsWith("https")) 
				{
					//ignore invalid/expired SSL cert
					ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
				}
				
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				//request.UserAgent = "Matchnet.Lib.Util.HttpUtil";
				request.Method = "POST";
				request.Timeout = timeout;

				if (referer != null && referer.Trim().Length > 0) 
				{
					request.Referer = referer;
				}

				request.ContentType = "multipart/form-data";
				//request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = data.Length;

				byte[] postData = System.Text.Encoding.UTF8.GetBytes(data);
				Stream requestStream = request.GetRequestStream();

				requestStream.Write(postData, 0, data.Length);
				requestStream.Close();

				WebResponse response = request.GetResponse();
				StreamReader reader = new StreamReader(response.GetResponseStream());
				
				string retVal = reader.ReadToEnd();
				try 
				{
					reader.Close();
					response.Close();
				} 
				catch {} 
				return retVal;
			} 
			catch (Exception ex) 
			{
				throw new Exception("Unable to post data to JetPay", ex);
			}
		}

		private JetPayResponse ParseResult(string xmlResponse) 
		{
			JetPayResponse response = new JetPayResponse();
			
#if DEBUG
			XmlDocument requestDoc = new XmlDocument();
			requestDoc.LoadXml(xmlResponse);
			requestDoc.Save(@"c:\temp\responseJetPay" + _guid.ToString() + ".xml");
#endif

			response.FromXml(xmlResponse);
			return response;
		}

		private bool ExcludedRecurring(string fieldName) 
		{
			switch (fieldName) 
			{
				case FIELD_BILLINGCOUNTRY:
				case FIELD_BILLINGADDRESS:
				case FIELD_BILLINGCITY:
				case FIELD_BILLINGPHONE:
				case FIELD_BILLINGPOSTALCODE:
				case FIELD_BILLINGSTATEPROV:
					return true;
				default:
					return false;
			}
		}

		private bool IsVoidField(string fieldName) 
		{
			switch (fieldName) 
			{
				case FIELD_TRANSACTIONTYPE:
				case FIELD_MERCHANTID:
				case FIELD_TRANSACTIONID:
				case FIELD_CARDNUM:
				case FIELD_APPROVAL:
				case FIELD_TOTALAMOUNT:
					return true;
				default:
					return false;
			}
		}


		//bogus crap for alternate bank (visa only)
		// More hacking: 10/25/05 - now we need to do this for MC as well.
		private void tweakMerchantID(Hashtable properties)
		{
			bool useVisaMerchantID = false;

			try
			{
				if (properties.ContainsKey(FIELD_CARDNUM))
				{
					if ((properties[FIELD_CARDNUM].ToString().IndexOf("4") == 0)|| (properties[FIELD_CARDNUM].ToString().IndexOf("5") == 0 )) //Check for Visa & MC
					{
						useVisaMerchantID = true;
					}
				}

				if (useVisaMerchantID)
				{
					if (properties.ContainsKey(FIELD_MERCHANTID_VISA))
					{
						properties[FIELD_MERCHANTID] = properties[FIELD_MERCHANTID_VISA];
						properties.Remove(FIELD_MERCHANTID_VISA);
					}
				}
				else
				{
					if (properties.ContainsKey(FIELD_MERCHANTID_VISA))
					{
						properties.Remove(FIELD_MERCHANTID_VISA);
					}
				}
			}
			catch(Exception ex)
			{
				throw new Exception("Error in tweakMerchantID(). " + ex.Message, ex);
			}
		}

	}
}
