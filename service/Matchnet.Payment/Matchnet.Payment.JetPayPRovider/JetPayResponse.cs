using System;
using System.IO;
using System.Xml;

using Matchnet;
using Matchnet.Payment.BusinessLogic;

namespace Matchnet.Payment.JetPayProvider
{
	[Serializable]
	public class JetPayResponse : ProviderResponse
	{
		private string transactionID;
		private string approval;

		// left padded to fill 18 characters
		public string TransactionID 
		{
			get { return transactionID; }
		}

		// not always available
		public string Approval 
		{
			get { return approval; }
		}

		public void FromXml(string xmlResponse) 
		{
			MemoryStream stream = new MemoryStream(System.Text.Encoding.ASCII.GetBytes(xmlResponse), false);
			XmlTextReader reader = new XmlTextReader(stream);

			while (reader.Read()) 
			{
				if (reader.NodeType == XmlNodeType.Element) 
				{
					string nodeName = reader.Name;
					string nodeValue = string.Empty;

					if (nodeName.ToUpper() != "JETPAYRESPONSE") 
					{
						nodeValue = reader.ReadString();
						AddProperty(nodeName, nodeValue);

						switch (nodeName) 
						{
							case "TransactionID":
								transactionID = nodeValue;
								break;
							case "ActionCode":
								ResponseCode = nodeValue;
								Success = ResponseCode == JetPayProvider.RESPONSE_SUCCESS;
								break;
							case "Approval":
								approval = nodeValue;
								break;
							case "ResponseText":
								Message = nodeValue;
								break;
						}
					}
				}
			}
		}
	}
}
