using System;
using System.Diagnostics;
using System.Collections.Specialized;

using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Payment.ValueObjects;
using Matchnet.Payment.ValueObjects.ServiceDefinitions;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.ServiceManagers
{
	public class PaymentSM : MarshalByRefObject, IPaymentService, IBackgroundProcessor
	{
		private HydraWriter _hydraWriter;
		private PaymentBL _paymentBL;

		#region Transaction perfs
		
		private PerformanceCounter _perfTransactionDecline;
		private PerformanceCounter _perfTransactionSuccess;
		private PerformanceCounter _perfTransaction_Base;
	
		#endregion

		public PaymentSM()
		{
			_hydraWriter = new HydraWriter(new string[]{"mnCharge","mnFileExchange"});
			_paymentBL = new PaymentBL();
			//initPerfCounters();
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void Start()
		{
			_hydraWriter.Start();
			_paymentBL.Start();
		}


		public void Stop()
		{
			_paymentBL.Stop();
			_hydraWriter.Stop();
		}


		public void UpdateMemberPayment(Int32 memberPaymentID,
			Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType)
		{
			try
			{
				_paymentBL.UpdateMemberPayment(memberPaymentID,
					memberID,
					siteID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					creditCardType);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "UpdateMemberPayment() error.", ex);
			}
		}
			
			
		public void ProcessPayment(MemberTran memberTran)
		{
			try
			{
				_paymentBL.ProcessPayment(memberTran);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "ProcessPayment() error (memberTranID: " + memberTran.MemberTranID.ToString() + ").", ex);
			}
		}

		public int GenerateEncryptedSalesFile()
		{
			try
			{
				return _paymentBL.GenerateEncryptedSalesFile();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "GenerateEncryptedSalesFile() error.", ex);
			}
		}
	
		public NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID)
		{
			try
			{
				return _paymentBL.GetChargeLog(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME,
					"GetChargeLog() error (memberID: " + memberID.ToString() + ", memberTranID: " + memberTranID.ToString() + ")",
					ex);
			}
		}
			
		public NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID)
		{
			try
			{
				return PaymentBL.GetMemberPayment(memberID,
					communityID,
					memberPaymentID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure getting member payment.", ex);
			}
		}


		public NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID)
		{
			try
			{
				return PaymentBL.GetMemberPaymentByMemberTranID(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "GetMemberPaymentByMemberTranID() error.", ex);
			}
		}
			
			
		public MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID)
		{
			try
			{
				return PaymentBL.GetChargeStatus(memberID,
					memberTranID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "GetChargeStatus() error.", ex);
			}
		}
			
			
		public MemberPaymentInfo GetMemberPaymentInfo(Int32 memberID,
			Int32 memberPaymentID)
		{
			try
			{
				return PaymentBL.GetMemberPaymentInfo(memberID,
					memberPaymentID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "GetMemberPaymentInfo() error.", ex);
			}
		}


		public Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID)
		{
			try
			{
				return _paymentBL.GetSuccessfulPayment(memberID,
					siteID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "GetSuccessfulPayment() error.", ex);
			}
		}

		#region perf

		
		public static bool PerfCounterInstall()
		{        
			if ( !PerformanceCounterCategory.Exists("Transaction Summary") ) 
			{
				CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
				ccdc.AddRange(new CounterCreationData [] {	// Summary of all transactions
															 new CounterCreationData("Transaction Summary Success", "Successfull transactions", PerformanceCounterType.RawFraction),
															 new CounterCreationData("Transaction Summary Decline", "Declined transactions", PerformanceCounterType.RawFraction),
															 new CounterCreationData("Transaction Summary Total", "All transactions processed", PerformanceCounterType.RawBase)
														 });

				PerformanceCounterCategory.Create(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);

				return(true);
			}
			else
			{
				Console.WriteLine("Category exists - AverageCounter64SampleCategory");
				return(false);
			}
		}

		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME);
		}

		private void initPerfCounters()
		{
			_perfTransactionDecline = new PerformanceCounter(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "Transaction Summary Success", false);
			_perfTransactionSuccess = new PerformanceCounter(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "Transaction Summary Decline", false);
			_perfTransaction_Base = new PerformanceCounter(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, "Transaction Summary Total", false);

			int[] merchants = new int[]{12102, 12103, 12101, 12006, 12013, 8015, 8004};

			foreach (int merchantID in merchants)
			{
			//	PerformanceCounter counter = new PerformanceCounter(SERVICE_NAME, "Transaction", "Merchant" + merchantID.ToString(), false);
			//	_communityCounters.Add(merchantID, counter);
			}
			resetPerfCounters();
		}

		private void resetPerfCounters()
		{
			_perfTransactionDecline.RawValue = 0;
			_perfTransactionSuccess.RawValue = 0;
			_perfTransaction_Base.RawValue = 0;

		}

		public void transactionDecline()
		{
			_perfTransaction_Base.Increment();
			_perfTransactionDecline.Increment();
		}

		public void transactionSuccess()
		{
			_perfTransaction_Base.Increment();
			_perfTransactionSuccess.Increment();
		}

		#endregion

	}
}
