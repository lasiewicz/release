using System;
using System.IO;
using System.Xml;

using Matchnet;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Exceptions;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.LitleProvider
{
	[Serializable]
	public class LitleResponse : ProviderResponse
	{
		/*
		AVS/CCV codes per Jennie 09/08/05 

		AVS
		00 5-Digit zip and address match � Approve (send sale transaction)
		01 9-Digit zip and address match � Approve (send sale transaction)
		02 Postal code and address match � Approve (send sale transaction)
		10 5-Digit zip matches, address does not match � Approve (send sale transaction)
		11 9-Digit zip matches, address does not match - Approve (send sale transaction)
		12 Zip does not match, address matches - Approve (send sale transaction)
		13 Postal code does not match, address matches - Approve (send sale transaction)
		14 Postal code matches, address not verified - Approve (send sale transaction)
		20 Neither zip nor address match - Decline (provide message to customer � do not send sale transaction)
		30 AVS service not supported by issuer � Decline (provide message to customer � do not send sale transaction)
		31 AVS system not available � Decline (provide message to customer � do not send sale transaction)
		32 Address unavailable - Decline (provide message to customer � do not send sale transaction)
		33 General error � Approve (send sale transaction)
		34 AVS not performed � Approve (send sale transaction)
		40 Address failed Litle & Co. edit checks � Approve (send sale transaction) 
 
		CVC
		M Match - Approve (send sale transaction)
		N No Match � Decline (provide message to customer � do not send sale transaction)
		P Not Processed � Decline (provide message to customer � do not send sale transaction)
		S CVV2/CVC2 should be on the card but the merchant has indicated CVV2/CVC2 is not present - Decline (provide message to customer � do not send sale transaction)
		U Issuer is not certified for CVV2/CVC2 processing Approve (send sale transaction)
		(empty response) Check was not done for an unspecified reason Decline (provide message to customer � do not send sale transaction)
		*/

		private const string SUCCESS_CODE = "000";
		// NOTE: These AVS codes are the same as Litle if we want to add others we may need to add logic
		// 11/22/05 We are removing AVS Code 30 per Jennies request... hours later removing everything but 20 
		private const string FAILURE_CODE_AVS = ""; 
		private const string FAILURE_CODE_CVC = "N";

		//		Internal Spark Codes
		private const string RESPONSE_AVS_FAILURE = "1001";	// avs no match 
		private const string RESPONSE_CVC_FAILURE = "1002";	// invalid cvc (they seem to have a code already but I want to use our own since we are making the decision)
		public const string RESPONSE_BIN_FAILURE = "1003";	// bin block

		

		//06112008 TL - Added memberTranID as param for outputting to event log during litle errors
		public void FromXml(string xmlResponse, string tranType, bool ignoreCVV, int memberTranID) 
		{
			string prefix = "";
			bool avsFailure = false;
			bool cvvFailure = false;
			const string SPARK_NODE = "SPARKResponse";

			try
			{
				if (tranType == LitleProvider.TRANTYPE_AUTH)
				{
					prefix = "auth_";
				}

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(xmlResponse);
				XmlNamespaceManager nsmgr  = new XmlNamespaceManager(xmlDoc.NameTable);
				nsmgr.AddNamespace("Litle", "http://www.litle.com/schema/online"); 
				
				// Future implementation of Return code 353 (Decline due to AVS) upon renewal would be to ask the customer to update 
				// their address info before we retried their renewal.  
				if (xmlDoc.DocumentElement.GetAttribute("response") == "0")
				{
					XmlNodeList nodeTran = xmlDoc.SelectNodes("/Litle:litleOnlineResponse/Litle:" + tranType + "Response/*", nsmgr);
					foreach (XmlNode nodeName in nodeTran)
					{
						AddProperty(prefix + nodeName.Name, nodeName.InnerText);
						switch (nodeName.Name)
						{
							case "response":
								ResponseCode = nodeName.InnerText;
								if (ResponseCode == SUCCESS_CODE)
								{
									Success = true;
								}
								break;
							case "message":
								Message = nodeName.InnerText;
								break;
							case "fraudResult":
								XmlNodeList fraudList = nodeName.SelectNodes("/Litle:litleOnlineResponse/Litle:" + tranType + "Response/Litle:fraudResult/*", nsmgr);
								foreach (XmlNode fraudNode in fraudList)
								{
									AddProperty(prefix + fraudNode.Name, fraudNode.InnerText);
									switch (fraudNode.Name)
									{
										case "avsResult":
											this.ResponseCodeAVS = fraudNode.InnerText.Trim();
											string [] avsCodes = FAILURE_CODE_AVS.Split(new Char [] {'|'});
											for (Int32 avsNum = 0; avsNum < avsCodes.Length; avsNum ++)
											{
												if (fraudNode.InnerText.Trim() == avsCodes[avsNum].Trim())
												{
													avsFailure = true;
													ResponseCode = RESPONSE_AVS_FAILURE;
													Message = RESPONSE_AVS_FAILURE;
												}
											}
											break;
										case "cardValidationResult":
											this.ResponseCodeCVV = fraudNode.InnerText.Trim();
											if (!ignoreCVV)
											{
												string [] cvvCodes = FAILURE_CODE_CVC.Split(new Char [] {'|'});
												for (Int32 cvvNum = 0; cvvNum < cvvCodes.Length; cvvNum ++)
												{
													if (fraudNode.InnerText.Trim() == cvvCodes[cvvNum].Trim())
													{
														cvvFailure = true;
														ResponseCode = RESPONSE_CVC_FAILURE;
														Message = RESPONSE_CVC_FAILURE;
													}
												}
											}
											break;
									}
								}
								break;
							default:
								break;
						}
					}

					if (tranType == LitleProvider.TRANTYPE_AUTH && (avsFailure || cvvFailure))
					{
						Success = false;
					}
				}
				else
				{
					//06112008 TL - Output error message to event log.
					Success = false;
					Message = "Litle indicates there was an error.  MemberTranID=" + memberTranID.ToString() + ".  Litle Message: " + xmlDoc.DocumentElement.GetAttribute("message");
					Message += "\r\n\r\nRequestXML:\r\n" + this.RequestDataScrubbed + "\r\n\r\n";
					Message += "\r\n\r\nResponseXML:\r\n" + xmlResponse + "\r\n\r\n";
					
					new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME,
						Message + ").\r\n",
						null);

					//09252008 TL MPR-539 - Output response/request xml to DB for Litle errors or exceptions
					logRequestResponseData = true;
					LogRequestResponseXML(xmlResponse, this.RequestDataScrubbed, this.MemberTran);
				}

				AddProperty(prefix + SPARK_NODE, (Success ? "APPROVED" : "DECLINED"));
			}
			catch(Exception ex)
			{
				//09252008 TL MPR-539 - Output response/request xml to DB for Litle errors or exceptions
				logRequestResponseData = true;
				LogRequestResponseXML(xmlResponse, this.RequestDataScrubbed, this.MemberTran);

				System.Diagnostics.Trace.WriteLine("__response error: " + ex.ToString().Replace("\r", "").Replace("\n", ""));
				throw new Exception("Error processing Litle Response.  Response = " + xmlResponse + "\r\nRequestXML = " + this.RequestDataScrubbed, ex);
			}
		}		

	}
}
