using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Payment.ValueObjects;

namespace Matchnet.Payment.LitleProvider
{
	public class LitleProvider : Provider
	{
		private const string FIELD_VERSION = "version";
		private const string FIELD_XMLNS = "Xmlns";
		private const string FIELD_MERCHANTID = "merchantId";
		private const string FIELD_USER = "user";
		private const string FIELD_PASSWORD = "password";
		private const string FIELD_TRANSACTIONID = "id";
		private const string FIELD_REPORTGROUP = "reportGroup";
		private const string FIELD_CUSTOMERID = "customerId";
		private const string FIELD_ORDERID = "orderId";
		private const string FIELD_AMOUNT = "amount";
		private const string FIELD_ORDERSOURCE = "orderSource";
		private const string FIELD_FNAME = "firstname";
		private const string FIELD_LNAME = "lastname";
		private const string FIELD_BILLNAME = "name";
		private const string FIELD_BILLADDRESS1 = "addressLine1";
		private const string FIELD_BILLCITY = "city";
		private const string FIELD_BILLSTATE = "state";
		private const string FIELD_BILLZIP = "zip";
		private const string FIELD_BILLCOUNTRY = "country";
		private const string FIELD_BILLEMAIL = "email";
		private const string FIELD_BILLPHONE = "phone";
		private const string FIELD_CARDTYPE = "type";
		private const string FIELD_CARDNUMBER = "number";
		private const string FIELD_CARDEXPMONTH = "expMonth";
		private const string FIELD_CARDEXPYEAR = "expYear";
		private const string FIELD_CARDEXPDATE ="expDate";
		private const string FIELD_CARDCVV = "cardValidationNum";
		private const string FIELD_LITLETXN = "litleTxnId";
		private const string FIELD_CUSTREFERENCE = "customerReference";

		private const string CARD_VISA = "VI";
		private const string CARD_MASTER = "MC";
		private const string CARD_AMEX = "AX";
		private const string CARD_DISCOVER = "DI";
		private const string CARD_DINERS = "DC";

		internal const string TRANTYPE_CAPTURE ="capture";
		internal const string TRANTYPE_SALE = "sale";
		internal const string TRANTYPE_AUTH = "authorization";
		internal const string TRANTYPE_CREDIT = "credit";
		internal const string TRANTYPE_CREDIT_II = "credit2"; //Part of lame hack
		internal const string TRANTYPE_VOID = "void";

		private string _xmlRequest;
		private string _xmlResponse;
		private MemberTran _mt;

		protected override ProviderResponse DoPurchase(MemberTran mt)
		{
			LitleResponse response = null;
			LitleResponse responseAuth = null;
			XmlDocument xmlDoc = null;
			_mt = mt;

			if (mt.SiteID == 6)
			{
				System.Diagnostics.Trace.WriteLine("__DoPurchase");
			}
	
			try
			{
				bool renewFlag = mt.TranType == TranType.Renewal;

				string url = (string)providerProperties[FIELD_URL];
				string referer = string.Empty;
				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

				if (!renewFlag)
				{

					//bin blocked?
					System.Diagnostics.Trace.WriteLine("__begin BIN check");
					if (FilterConfig.Instance.IsBinListed(base._merchantID, mt.AccountNumber))
					{
						responseAuth = new LitleResponse();
						responseAuth.AddProperty("response", LitleResponse.RESPONSE_BIN_FAILURE);
						responseAuth.ResponseCode = LitleResponse.RESPONSE_BIN_FAILURE;
						responseAuth.Success = false;

						response = responseAuth;
						System.Diagnostics.Trace.WriteLine("__end BIN check");
					}
					else
					{
						
						// Authorization
						providerProperties[FIELD_AMOUNT] = mt.Amount;
						xmlDoc = formatXML(getXMLDoc(), TRANTYPE_AUTH, renewFlag, mt.MemberSubID);
#if DEBUG
						xmlDoc.Save("c:\\temp\\requestLitle" + TRANTYPE_AUTH + _guid.ToString() + ".xml");
#endif
						responseAuth = parseResult(sendXML(url, this.TextEncodePostProcess(xmlDoc.OuterXml), referer), TRANTYPE_AUTH, mt.IgnoreCVV, mt.MemberTranID, xmlDoc);

						// Capture
						if (responseAuth != null && responseAuth.Success)
						{
							// set provider's attribute 
							for (int i=0;i<responseAuth.Properties.Count;i++)
							{ 
								Matchnet.Payment.BusinessLogic.Property property =(Matchnet.Payment.BusinessLogic.Property ) responseAuth.Properties[i];
								if (property.Name.IndexOf(FIELD_LITLETXN)>0)
								{
									providerProperties[FIELD_LITLETXN] = property.Value;
								}
							}
							providerProperties[FIELD_AMOUNT] = mt.Amount;

							xmlDoc = formatXML(getXMLDoc(), TRANTYPE_CAPTURE, renewFlag, mt.MemberSubID);
					
#if DEBUG
							xmlDoc.Save("c:\\temp\\requestLitle" + TRANTYPE_CAPTURE + _guid.ToString() + ".xml");
#endif

							response = parseResult(sendXML(url, this.TextEncodePostProcess(xmlDoc.OuterXml), referer), TRANTYPE_CAPTURE, mt.IgnoreCVV, mt.MemberTranID, xmlDoc);
							
							//Transfer Authorization Response information to our Capture Response object
							//09042008 TL (ET-39) Transferred AVS and CVV Response codes from Auth to Capture response
							for (Int32 propertyNum = 0; propertyNum < responseAuth.Properties.Count; propertyNum++)
							{
								response.AddProperty((Matchnet.Payment.BusinessLogic.Property)responseAuth.Properties[propertyNum]);
							}

							response.ResponseCodeAVS = responseAuth.ResponseCodeAVS;
							response.ResponseCodeCVV = responseAuth.ResponseCodeCVV;

							
						}
						else
						{
							response = responseAuth;
						}

					}
				}
				else
				{
					providerProperties[FIELD_AMOUNT] = mt.Amount;
					xmlDoc = formatXML(getXMLDoc(), TRANTYPE_SALE, renewFlag, mt.MemberSubID);
#if DEBUG
					xmlDoc.Save("c:\\temp\\requestLitle" + TRANTYPE_SALE + _guid.ToString() + ".xml");
#endif
					response = parseResult(sendXML(url, this.TextEncodePostProcess(xmlDoc.OuterXml), referer), TRANTYPE_SALE, mt.IgnoreCVV, mt.MemberTranID, xmlDoc);
				}

				return response;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__DoPurchase error " + ex.ToString().Replace("\r", "").Replace("\n", ""));
				throw new Exception("Error in LitleProcessor: DoPurchase(). MemberID: " + mt.MemberID.ToString() + ", MemberTranID: " + mt.MemberTranID.ToString(), ex);
			}
		}


		public override ProviderResponse Authorize(MemberTran mt)
		{
			try
			{
				this._mt = mt;

				ProviderResponse providerResponse = base.VelocityCheck(mt);
				if (providerResponse != null)
				{
					return providerResponse;
				}

				XmlDocument xmlDoc = getXMLDoc();

				string url = (string)providerProperties[FIELD_URL];
					
				string referer = string.Empty;
				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

				bool renewFlag = false;
				xmlDoc = formatXML(xmlDoc, TRANTYPE_AUTH, renewFlag, mt.MemberSubID);

#if DEBUG
				xmlDoc.Save("c:\\temp\\requestLitle" + TRANTYPE_SALE + _guid.ToString() + ".xml");
#endif

				LitleResponse response = parseResult(sendXML(url, this.TextEncodePostProcess(xmlDoc.OuterXml), referer),TRANTYPE_AUTH, mt.IgnoreCVV, mt.MemberTranID, xmlDoc);
				
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception("Error in LitleProcessor: Authorize().", ex);
			}
		}


		protected override ProviderResponse DoCredit(MemberTran mt)
		{
			try
			{
				this._mt = mt;
				bool otherProcessor = false;

				try 
				{
					string originalTranID = getLogValue(mt.MemberID, mt.ReferenceMemberTranID, FIELD_LITLETXN, false);
					if (originalTranID != null)
					{
						providerProperties[FIELD_LITLETXN] = originalTranID;
					}
					else
					{
						//Credits from other processors require us to do a look up from MemberPayment.
						otherProcessor = true;

						NameValueCollection paymentAttributes = PaymentBL.GetMemberPaymentByMemberTranID(mt.MemberID,mt.ReferenceMemberTranID,true);
		
						foreach (string paymentAttribute in paymentAttributes)
						{
							switch(paymentAttribute)
							{	
								case "CreditCardNumber":
									providerProperties[FIELD_CARDNUMBER] = paymentAttributes[paymentAttribute];
									break;
								case "CreditCardExpirationMonth":
									providerProperties[FIELD_CARDEXPMONTH] = paymentAttributes[paymentAttribute];
									break;
								case "CreditCardExpirationYear":
									providerProperties[FIELD_CARDEXPYEAR] = paymentAttributes[paymentAttribute];
									break;
								case "CreditCardType":
									providerProperties[FIELD_CARDTYPE] = paymentAttributes[paymentAttribute];
									break;
							}
						}
						providerProperties.Add(FIELD_CUSTREFERENCE, mt.MemberTranID.ToString());
					}
				}
				catch (Exception ex)
				{
					throw new Exception("Error in LitleProcessor: DoCredit().  Can not find reference transactionid: " + mt.ReferenceMemberTranID.ToString() + ". MemberID: " + mt.MemberID, ex);
				}

				XmlDocument xmlDoc = getXMLDoc();

				bool renewFlag = false;
				xmlDoc = formatXML(xmlDoc, (otherProcessor ? TRANTYPE_CREDIT_II: TRANTYPE_CREDIT), renewFlag, mt.MemberSubID);

				string url = (string)providerProperties[FIELD_URL];
				string referer = string.Empty;
				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

#if DEBUG
				xmlDoc.Save("c:\\temp\\requestLitle" + TRANTYPE_SALE + _guid.ToString() + ".xml");
#endif

				LitleResponse response = parseResult(sendXML(url, this.TextEncodePostProcess(xmlDoc.OuterXml), referer),TRANTYPE_CREDIT, mt.IgnoreCVV, mt.MemberTranID, xmlDoc);
				
				return response;
			}
			catch (Exception ex)
			{
				throw new Exception("Error in LitleProcessor: DoCredit(). MemberID: " + mt.MemberID, ex);
			}
		}


		public override ProviderResponse Void(MemberTran mt)
		{
			try
			{
				_mt = mt;

				XmlDocument xmlDoc = getXMLDoc();
				bool renewFlag = false;
				xmlDoc = formatXML(xmlDoc, TRANTYPE_VOID, renewFlag, mt.MemberSubID);

				string url = (string)providerProperties[FIELD_URL];
				string referer = string.Empty;
				if (providerProperties.ContainsKey(FIELD_REFER)) 
				{
					referer = (string)providerProperties[FIELD_REFER];
				}

#if DEBUG
				xmlDoc.Save("c:\\temp\\requestLitle" + TRANTYPE_SALE + _guid.ToString() + ".xml");
#endif

				LitleResponse response = parseResult(sendXML(url, this.TextEncodePostProcess(xmlDoc.OuterXml), referer),TRANTYPE_VOID, mt.IgnoreCVV, mt.MemberTranID, xmlDoc);
				
				return response;
			}
			catch(Exception ex)
			{
				throw new Exception("Error in LitleProcessor: Void().", ex);
			}
		}


		public override string RawRequest()
		{
			return this._xmlRequest;
		}


		public override string RawResponse()
		{
			return this._xmlResponse;
		}


		private XmlDocument getXMLDoc() 
		{
			//Get the main Litle schema that is the same for all transaction types
			try
			{
				XmlDocument xmlDoc = new XmlDocument();
				//06122008 TL - Added XML processing instruction element for unicode encoding
				xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("xml", "version='1.0' encoding='utf-8'"));

				string xmlNs = providerProperties[FIELD_XMLNS].ToString();
				XmlElement rootElem = xmlDoc.CreateElement("litleOnlineRequest", xmlNs);
				rootElem.SetAttribute(FIELD_VERSION, providerProperties[FIELD_VERSION].ToString());
				rootElem.SetAttribute(FIELD_MERCHANTID, providerProperties[FIELD_MERCHANTID].ToString());
				xmlDoc.AppendChild(rootElem);

				XmlElement authElem = xmlDoc.CreateElement("authentication", xmlNs);
				XmlElement userElem = appendXML(xmlDoc, FIELD_USER, xmlNs);
				XmlElement passElem = appendXML(xmlDoc, FIELD_PASSWORD, xmlNs);
				authElem.AppendChild(userElem);
				authElem.AppendChild(passElem);
				xmlDoc.DocumentElement.AppendChild(authElem);
				return xmlDoc;
			}
			catch(Exception ex)
			{
				throw new Exception("Error creating Litle XML Document.", ex);
			}
		}


		private XmlDocument formatXML(XmlDocument xmlDocument, string trantype, bool renewal, Int32 memberSubID) 
		{
			//06162008 TL - Updated some of the properties to be encoded using TextEncodePreProcess() for encoding unicode characters
			//06212008 MM - Added text encoding of Unicode characters to more fields
			try
			{
				// Concat name into 1 field
				if (providerProperties.Contains(FIELD_FNAME) && providerProperties.Contains(FIELD_LNAME))
				{
					//Format Name
					string name = providerProperties[FIELD_FNAME].ToString().Trim() + " " + providerProperties[FIELD_LNAME].ToString().Trim();
					providerProperties.Remove(FIELD_FNAME);
					providerProperties.Remove(FIELD_LNAME);
					providerProperties.Add(FIELD_BILLNAME, this.TextEncodePreProcess(name));
				}

				// Check if Address Length over 35
				if (providerProperties.ContainsKey(FIELD_BILLADDRESS1))
				{
					if (providerProperties[FIELD_BILLADDRESS1].ToString().Length>35)
					{
						providerProperties[FIELD_BILLADDRESS1] = this.TextEncodePreProcess(providerProperties[FIELD_BILLADDRESS1].ToString().Substring(0,35));
					}
					else
					{
						providerProperties[FIELD_BILLADDRESS1] = this.TextEncodePreProcess(providerProperties[FIELD_BILLADDRESS1].ToString());
					}
				}

				//Check if Phone Length over 20
				if (providerProperties.ContainsKey(FIELD_BILLPHONE))
				{
					if (providerProperties[FIELD_BILLPHONE].ToString().Length>20)
					{
						providerProperties[FIELD_BILLPHONE] = providerProperties[FIELD_BILLPHONE].ToString().Substring(0,20);
					}
				}

				//Check if CVV Length over 4
				if (providerProperties.ContainsKey(FIELD_CARDCVV))
				{
					if (providerProperties[FIELD_CARDCVV].ToString().Length>4)
					{
						providerProperties[FIELD_CARDCVV] = providerProperties[FIELD_CARDCVV].ToString().Substring(0,4);
					}
				}

				if (providerProperties.ContainsKey(FIELD_CARDEXPYEAR) && providerProperties.ContainsKey(FIELD_CARDEXPMONTH) )
				{
					//Format Expiration
					string year = providerProperties[FIELD_CARDEXPYEAR].ToString();
					year = year.Length  > 2 ? year.Substring(year.Length - 2) : year;
					string month = providerProperties[FIELD_CARDEXPMONTH].ToString();
					month = month.Length == 2 ? month : "0" + month;
					//Remove old keys and add the new one
					providerProperties.Remove(FIELD_CARDEXPYEAR);
					providerProperties.Remove(FIELD_CARDEXPMONTH);
					providerProperties.Add(FIELD_CARDEXPDATE, month + year);
				}

				if (providerProperties.ContainsKey(FIELD_AMOUNT))
				{	
					// Convert Amount so that $5.00 is sent as 500
					decimal amount = System.Convert.ToDecimal(providerProperties[FIELD_AMOUNT].ToString());
					providerProperties[FIELD_AMOUNT] = System.Convert.ToInt32((((amount < 0) ? (amount * -1): amount) * 100));
				}
				if (providerProperties.ContainsKey(FIELD_BILLCOUNTRY))
				{
					double regionID;
					if (double.TryParse(providerProperties[FIELD_BILLCOUNTRY].ToString(), System.Globalization.NumberStyles.Integer, null, out regionID))
					{
						providerProperties[FIELD_BILLCOUNTRY] = this.TextEncodePreProcess(RegionBL.Instance.GetRegion((Int32)regionID).IsoCountryCode2);
					}
				}
				if(providerProperties.ContainsKey(FIELD_BILLSTATE))
				{
					providerProperties[FIELD_BILLSTATE] = this.TextEncodePreProcess(providerProperties[FIELD_BILLSTATE].ToString());
				}
				if(providerProperties.ContainsKey(FIELD_BILLCITY))
				{
					providerProperties[FIELD_BILLCITY] = this.TextEncodePreProcess(providerProperties[FIELD_BILLCITY].ToString());
				}
				if(providerProperties.ContainsKey(FIELD_BILLEMAIL))
				{
					providerProperties[FIELD_BILLEMAIL] = this.TextEncodePreProcess(providerProperties[FIELD_BILLEMAIL].ToString());
				}
				if (providerProperties.ContainsKey(FIELD_CARDTYPE))
				{	// Try and map by card number if a renewal
					if (renewal || trantype == TRANTYPE_CREDIT_II) 
					{
						if (providerProperties.ContainsKey(FIELD_CARDNUMBER))
						{
							switch(Convert.ToInt16(providerProperties[FIELD_CARDNUMBER].ToString().Trim().Substring(0,1)))
							{
								case 3:
								switch(Convert.ToInt16(providerProperties[FIELD_CARDNUMBER].ToString().Trim().Substring(0,2)))
								{
									case 34:
									case 37:
										providerProperties[FIELD_CARDTYPE] = CARD_AMEX;
										break;
									case 36:
									case 38:
										providerProperties[FIELD_CARDTYPE] = CARD_DINERS;
										break;
									default:
										throw new Exception("RENEWAL: Unable to match card number to card type that begins with \"3\" for Litle provider. MemberSubId: " + memberSubID.ToString());
								}
									break;
								case 4:
									providerProperties[FIELD_CARDTYPE] = CARD_VISA;
									break;
								case 5:
									providerProperties[FIELD_CARDTYPE] = CARD_MASTER;
									break;
								case 6:
									providerProperties[FIELD_CARDTYPE] = CARD_DISCOVER;
									break;
								default:
									throw new Exception("RENEWAL: Unable to match card number to card type for Litle provider. MemberSubId: " + memberSubID.ToString());
							}
						}
						else
						{
							throw new Exception("Renewal is missing card number for MemberSubId: " + memberSubID.ToString());
						}
					}
					else 
					{
						double cardType;
						if (double.TryParse(providerProperties[FIELD_CARDTYPE].ToString(), System.Globalization.NumberStyles.Integer, null, out cardType))
						{
							switch(Convert.ToInt16(cardType))
							{
								case 1:	//American Express
									providerProperties[FIELD_CARDTYPE] = CARD_AMEX;
									break;
								case 4: // Diners Club
									providerProperties[FIELD_CARDTYPE] = CARD_DINERS;
									break;
								case 8: //Discover Card
									providerProperties[FIELD_CARDTYPE] = CARD_DISCOVER;
									break;
								case 32: //Master Card
									providerProperties[FIELD_CARDTYPE] = CARD_MASTER;
									break;
								case 11: //Delta
								case 12: //Electron
								case 64: //Visa
									providerProperties[FIELD_CARDTYPE] = CARD_VISA;
									break;
								default:
									throw new Exception("unknown card type for Litle provider (" + cardType.ToString() + ")");
							}
						}
					}
				}

				if (providerProperties.ContainsKey(FIELD_ORDERSOURCE))
				{
					if (renewal)
					{
						providerProperties[FIELD_ORDERSOURCE] = "recurring";
						providerProperties[FIELD_CARDCVV] = "";
					}
					else
					{
						if( CARD_MASTER == providerProperties[FIELD_CARDTYPE].ToString())
						{
							providerProperties[FIELD_ORDERSOURCE] = "recurring";
						}
						else
						{
							providerProperties[FIELD_ORDERSOURCE] = "ecommerce";
						}
					}
				}

				// xmlns is used a lot so pull it out once
				string xmlNs = providerProperties[FIELD_XMLNS].ToString();
				XmlElement xmlElement = xmlDocument.CreateElement(trantype==TRANTYPE_CREDIT_II ? TRANTYPE_CREDIT: trantype, xmlNs);
				xmlElement.SetAttribute(FIELD_TRANSACTIONID, providerProperties[FIELD_TRANSACTIONID].ToString());
				xmlElement.SetAttribute(FIELD_REPORTGROUP, providerProperties[FIELD_REPORTGROUP].ToString());
				xmlElement.SetAttribute(FIELD_CUSTOMERID, providerProperties[FIELD_CUSTOMERID].ToString());

				switch (trantype)
				{
					case TRANTYPE_CAPTURE:
						//Auth number
						XmlElement CapturelitleElem = appendXML(xmlDocument, FIELD_LITLETXN, xmlNs);
						xmlElement.AppendChild(CapturelitleElem);				

						XmlElement CaptureAmountElem = appendXML(xmlDocument, FIELD_AMOUNT, xmlNs);						
						xmlElement.AppendChild(CaptureAmountElem );
						
						// Add <capture> node to document
						xmlDocument.DocumentElement.AppendChild(xmlElement);

						break;
					case TRANTYPE_AUTH: 
					case TRANTYPE_SALE:
					case TRANTYPE_CREDIT_II:
						//<sale>
						XmlElement orderElem = appendXML(xmlDocument, FIELD_ORDERID, xmlNs, FIELD_TRANSACTIONID);	//Put Membertran in <orderId>
						XmlElement amountElem = appendXML(xmlDocument, FIELD_AMOUNT, xmlNs);
						XmlElement sourceElem = appendXML(xmlDocument, FIELD_ORDERSOURCE, xmlNs);
						//</sale>

						//<billToAddress>
						XmlElement nameElem = appendXML(xmlDocument, FIELD_BILLNAME, xmlNs);
						XmlElement addElem = appendXML(xmlDocument, FIELD_BILLADDRESS1, xmlNs);
						XmlElement cityElem = appendXML(xmlDocument, FIELD_BILLCITY, xmlNs);
						XmlElement stateElem = appendXML(xmlDocument, FIELD_BILLSTATE, xmlNs);
						XmlElement zipElem = appendXML(xmlDocument, FIELD_BILLZIP, xmlNs);
						XmlElement countryElem = appendXML(xmlDocument, FIELD_BILLCOUNTRY, xmlNs);
						XmlElement emailElem = appendXML(xmlDocument, FIELD_BILLEMAIL, xmlNs);
						XmlElement phoneElem = appendXML(xmlDocument, FIELD_BILLPHONE, xmlNs);

						XmlElement billElem = xmlDocument.CreateElement("billToAddress", xmlNs);
						billElem.AppendChild(nameElem);
						billElem.AppendChild(addElem);
						billElem.AppendChild(cityElem);
						billElem.AppendChild(stateElem);
						billElem.AppendChild(zipElem);
						billElem.AppendChild(countryElem);
						//billElem.AppendChild(emailElem);
						billElem.AppendChild(phoneElem);
						//</billToAddres>

						// <card>
						XmlElement typeElem = appendXML(xmlDocument, FIELD_CARDTYPE, xmlNs);
						XmlElement numberElem = appendXML(xmlDocument, FIELD_CARDNUMBER, xmlNs);
						XmlElement expElem = appendXML(xmlDocument, FIELD_CARDEXPDATE, xmlNs);
						XmlElement cvvElem = appendXML(xmlDocument, FIELD_CARDCVV, xmlNs);
						
						XmlElement cardElem = xmlDocument.CreateElement("card", xmlNs);	
						cardElem.AppendChild(typeElem);
						cardElem.AppendChild(numberElem);
						cardElem.AppendChild(expElem);
						cardElem.AppendChild(cvvElem);
						// </card>

						xmlElement.AppendChild(orderElem);
						xmlElement.AppendChild(amountElem);
						xmlElement.AppendChild(sourceElem);
						xmlElement.AppendChild(billElem);
						xmlElement.AppendChild(cardElem);

						//<enhancedData>	(Optional)
						if (providerProperties.ContainsKey(FIELD_CUSTREFERENCE))
						{
							XmlElement custRefElem = appendXML(xmlDocument, FIELD_CUSTREFERENCE, xmlNs);
							XmlElement enhancedElem = xmlDocument.CreateElement("enhancedData", xmlNs);
							enhancedElem.AppendChild(custRefElem);
							xmlElement.AppendChild(enhancedElem);
						}
						// </enchancedData>
						
						// Add <sales> node to document
						xmlDocument.DocumentElement.AppendChild(xmlElement);
						break;
					case TRANTYPE_CREDIT:
						//<credit>
						XmlElement litletxnElem = appendXML(xmlDocument, FIELD_LITLETXN, xmlNs);
						XmlElement creditElem = appendXML(xmlDocument, FIELD_AMOUNT, xmlNs);
						//</credit>
						xmlElement.AppendChild(litletxnElem);
						xmlElement.AppendChild(creditElem);					
						// Add <credit> node to document
						xmlDocument.DocumentElement.AppendChild(xmlElement);
						break;
					case TRANTYPE_VOID: 
						//<void>	- LitleTxn #
						XmlElement litleElem = appendXML(xmlDocument, FIELD_LITLETXN, xmlNs);
						//</void>
						xmlElement.AppendChild(litleElem);				
						// Add <void> node to document
						xmlDocument.DocumentElement.AppendChild(xmlElement);
						break;
					default:
						break;
				}
				
				return xmlDocument;
			}
			catch (Exception ex)
			{
				throw new Exception("Error in LitleProcessor: FormatXML(). " + providerProperties.ToString(), ex);
			}
		}


		private XmlElement appendXML(XmlDocument xmldoc, string key, string xmlns)
		{
			XmlElement xmlElem = xmldoc.CreateElement(key, xmlns);
			XmlText xmlText = xmldoc.CreateTextNode(String.Empty);
			if (providerProperties.ContainsKey(key))
			{
				xmlText = xmldoc.CreateTextNode(providerProperties[key].ToString());
			}
			xmlElem.AppendChild(xmlText);
			return xmlElem;
		}
		// Overloaded method to override the value if the node and hash key are different names
		private XmlElement appendXML(XmlDocument xmldoc, string key, string xmlns, string overridevalue)
		{
			XmlElement xmlElem = xmldoc.CreateElement(key, xmlns);
			XmlText xmlText = xmldoc.CreateTextNode(String.Empty);
			if (providerProperties.ContainsKey(overridevalue))
			{
				xmlText = xmldoc.CreateTextNode(providerProperties[overridevalue].ToString());
			}
			xmlElem.AppendChild(xmlText);
			return xmlElem;
		}


		private LitleResponse parseResult(string xmlResponse,
			string tranType,
			bool ignoreCVV, int memberTranID, XmlDocument RequestDoc) 
		{
			LitleResponse response = new LitleResponse();

			//07082008 TL - added request xml to response object for logging purposes, upon error in LitleResponse
			response.RequestDataScrubbed = this.RemoveXMLSensitiveInfo(RequestDoc);
			response.ResponseData = xmlResponse;
			response.MemberTran = this._mt;

			_xmlResponse = response.ResponseData;
			_xmlRequest = response.RequestDataScrubbed;

#if DEBUG
			string filename = "c:\\temp\\responseLitle" + tranType + _guid.ToString() + ".xml";
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(xmlResponse);
			doc.Save(filename);
#endif
			
			response.FromXml(xmlResponse,
				tranType,
				ignoreCVV,
				memberTranID);
			return response;
		}

		/// <summary>
		/// 06162008 TL
		/// Performs an encoding for unicode characters.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private string TextEncodePreProcess(string s)
		{
			//using HTMLEncode in order to encode characters that will also be visible on a webpage;
			//we need this to view encoded data correctly on Litle's webpage
			return System.Web.HttpUtility.HtmlEncode(s);
		}

		/// <summary>
		/// 06162008 TL
		/// Performs post processing of xml string before sending to Litle for unicode characters
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		private string TextEncodePostProcess(string s)
		{
			//.NET will escape Ampersands by replacing it with &amp; in XMLDocuments, so we need to remove that for unicode characters
			return s.Replace("&amp;#", "&#");
		}

		/// <summary>
		/// 06132008 TL
		/// Removes sensitive info from xml, useful for logging purposes
		/// </summary>
		/// <param name="doc"></param>
		/// <returns>xml string without sensitive info</returns>
		private string RemoveXMLSensitiveInfo(XmlDocument doc)
		{
			string returnXml = "";
			try
			{
				if (doc != null)
				{
					XmlNodeList nodeList = null;

					//remove pw
					nodeList = doc.GetElementsByTagName(FIELD_PASSWORD);
					if (nodeList != null && nodeList.Count > 0)
					{
						foreach (XmlNode node in nodeList)
						{
							node.InnerText = "";
						}
					}

					//remove cvv
					nodeList = doc.GetElementsByTagName(FIELD_CARDCVV);
					if (nodeList != null && nodeList.Count > 0)
					{
						foreach (XmlNode node in nodeList)
						{
							node.InnerText = "";
						}
					}

					//remove all ccn, except last 4 digits
					nodeList = doc.GetElementsByTagName(FIELD_CARDNUMBER);
					if (nodeList != null && nodeList.Count > 0)
					{
						foreach (XmlNode node in nodeList)
						{
							if (node.InnerText != null && node.InnerText.Length > 4)
							{
								System.Text.StringBuilder firstCCPart = new StringBuilder();
								char[] firstCCPartArray = node.InnerText.ToCharArray(0, node.InnerText.Length - 4);
								foreach (char i in firstCCPartArray)
								{
									if (i.ToString() == "-") //due to errors processing dashes, we want to see if the CC number had dashes during exception logging
										firstCCPart.Append(i.ToString());
									else
										firstCCPart.Append("#"); //mask all other numbers, except last 4, as pound signs.
								}

								string lastCCPart = node.InnerText.Substring(node.InnerText.Length - 4); //last 4 digits will be shown

								node.InnerText = firstCCPart.ToString() + lastCCPart;
							}
						}
					}

					returnXml = doc.OuterXml;
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);

				//output error message to event log
				new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME,
					ex.Message + ").\r\n",
					null);
			}

			return returnXml;
		}
	}
}
