/*
	This is a WSDL generated class. The only edit was done to the CheckAuthorizationService class
	to allow for the WebService URL to be passed in via the Constructor. Documentation was also added to
	fields that pertain to our Attribute Mapping scheme for dynmically populating transaction data.
	
	This enables us to use a dynamic URL so that we can support testing and production environments
	using the same binary.
*/
namespace Matchnet.Payment.FirstDataProvider
{
	using System.Diagnostics;
	using System.Xml.Serialization;
	using System;
	using System.Web.Services.Protocols;
	using System.ComponentModel;
	using System.Web.Services;
    
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Web.Services.WebServiceBindingAttribute(Name="CheckAuthorizationServicePortSoapBinding", Namespace="http://achex.com")]
	[System.Xml.Serialization.SoapIncludeAttribute(typeof(ResponseDetail))]
	[System.Xml.Serialization.SoapIncludeAttribute(typeof(ExtendedDetail))]
	public class CheckAuthorizationService : System.Web.Services.Protocols.SoapHttpClientProtocol 
	{
		// "https://test.achex.com/ws/authorization/check/v1.0"; 
		// "https://www.achex.com/ws/authorization/check/v1.0";
		public CheckAuthorizationService(string webServiceURL) 
		{
			this.Url = webServiceURL;
		}
        
		[System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://achex.com", ResponseNamespace="http://achex.com")]
		[return: System.Xml.Serialization.SoapElementAttribute("result")]
		public AuthorizationResponse createCheckAuthorization(Credentials credentials, AuthorizationDetails authorizationDetails, DDAAccount dDAAccount, ExtendedDetail[] extendedDetails) 
		{
			object[] results = this.Invoke("createCheckAuthorization", new object[] {
																						credentials,
																						authorizationDetails,
																						dDAAccount,
																						extendedDetails});
			return ((AuthorizationResponse)(results[0]));
		}
        
		public System.IAsyncResult BegincreateCheckAuthorization(Credentials credentials, AuthorizationDetails authorizationDetails, DDAAccount dDAAccount, ExtendedDetail[] extendedDetails, System.AsyncCallback callback, object asyncState) 
		{
			return this.BeginInvoke("createCheckAuthorization", new object[] {
																				 credentials,
																				 authorizationDetails,
																				 dDAAccount,
																				 extendedDetails}, callback, asyncState);
		}
        
		public AuthorizationResponse EndcreateCheckAuthorization(System.IAsyncResult asyncResult) 
		{
			object[] results = this.EndInvoke(asyncResult);
			return ((AuthorizationResponse)(results[0]));
		}
        
		[System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://achex.com", ResponseNamespace="http://achex.com")]
		[return: System.Xml.Serialization.SoapElementAttribute("result")]
		public AuthorizationResponse createBMLAuthorization(Credentials credentials, AuthorizationDetails authorizationDetails, BMLAccount bMLAccount, ExtendedDetail[] extendedDetails) 
		{
			object[] results = this.Invoke("createBMLAuthorization", new object[] {
																					  credentials,
																					  authorizationDetails,
																					  bMLAccount,
																					  extendedDetails});
			return ((AuthorizationResponse)(results[0]));
		}
        
		public System.IAsyncResult BegincreateBMLAuthorization(Credentials credentials, AuthorizationDetails authorizationDetails, BMLAccount bMLAccount, ExtendedDetail[] extendedDetails, System.AsyncCallback callback, object asyncState) 
		{
			return this.BeginInvoke("createBMLAuthorization", new object[] {
																			   credentials,
																			   authorizationDetails,
																			   bMLAccount,
																			   extendedDetails}, callback, asyncState);
		}
        
		public AuthorizationResponse EndcreateBMLAuthorization(System.IAsyncResult asyncResult) 
		{
			object[] results = this.EndInvoke(asyncResult);
			return ((AuthorizationResponse)(results[0]));
		}
	}
    
	[System.Xml.Serialization.SoapTypeAttribute("Credentials", "java:com.achex.webservices")]
	public class Credentials 
	{
		/// <summary>
		/// Required: AN 20 1 PWD
		/// </summary>
		public string password;

		/// <summary>
		/// Required: AN 20 1 USER
		/// </summary>
		public string userName;
	}
    
	[System.Xml.Serialization.SoapTypeAttribute("BMLAccount", "java:com.achex.webservices")]
	public class BMLAccount 
	{
		public string merchantCustomerID;
		public Address billingAddress;
		public System.DateTime dateOfBirth;
		public string termsAndConditionsVersion;
		public System.DateTime merchantCustomerRegistrationDate;
		public string existingCustomerIndicator;
		public string ssn;
		public string ssn4;
	}
    
	[System.Xml.Serialization.SoapTypeAttribute("Address", "java:com.achex.webservices")]
	public class Address 
	{
		/// <summary>
		/// Required: N 5 9 ZIP
		/// </summary>
		public string postalCode;

		/// <summary>
		/// Required: A 2 7 STATE
		/// </summary>
		public string state;

		/// <summary>
		/// Optional
		/// </summary>
		public string middleInitial;

		/// <summary>
		/// Optional
		/// </summary>
		public string street2;

		/// <summary>
		/// Required: AN 50 30 STREET
		/// </summary>
		public string street1;

		/// <summary>
		/// Optional (used if firstName and lastName are not)
		/// </summary>
		public string fullName;

		/// <summary>
		/// Required: A 50 6 CITY
		/// </summary>
		public string city;

		/// <summary>
		/// Required: AN 75 12 EmailAddress (required for internet)
		/// </summary>
		public string email;

		/// <summary>
		/// Required: A 25 3 FirstName
		/// </summary>
		public string firstName;

		/// <summary>
		/// Required: A 50 4 LastName
		/// </summary>
		public string lastName;

		/// <summary>
		/// Optional
		/// </summary>
		public string phone2;

		/// <summary>
		/// Required: N 10 22 Phone
		/// </summary>
		public string phone1;

		/// <summary>
		/// NOT DOCUMENTED
		/// </summary>
		public string countryCode;
	}
    
	[System.Xml.Serialization.SoapTypeAttribute("ResponseDetail", "java:com.achex.webservices")]
	public class ResponseDetail 
	{
		public string key;
		public string description;
		public string value;
		public string reasonCode;
	}
	
	[System.Xml.Serialization.SoapTypeAttribute("AuthorizationResponse", "java:com.achex.webservices")]
	public class AuthorizationResponse 
	{
		public string statusDisplayDescription;
		public ResponseDetail[] details;
		public string statusCode;
		public string clearerID;
		public string merchantID;
		public string trackingID;
	}
    
	
	[System.Xml.Serialization.SoapTypeAttribute("ExtendedDetail", "java:com.achex.webservices")]
	public class ExtendedDetail 
	{
		public string key;
		public string value;
	}
    
	
	[System.Xml.Serialization.SoapTypeAttribute("DDAAccount", "java:com.achex.webservices")]
	public class DDAAccount 
	{
		/// <summary>
		/// Required: Protocol Address Object
		/// </summary>
		public Address billingAddress;

		/// <summary>
		/// Required: AN 35 20 DriversLicenseNumber
		/// </summary>
		public string driverLicense;

		/// <summary>
		/// Recommended: A 2 15 BankState
		/// </summary>
		public string bankState;

		/// <summary>
		/// Required: N 17 18 CheckingAccountNumber
		/// </summary>
		public string accountNumber;

		/// <summary>
		/// Required: N 2 NEW ATTRIBUTE! BankAccountTypeID (10 - Checking, 20 - Savings)
		/// </summary>
		public string accountTypeCode;

		/// <summary>
		/// Required: A 2 21 DriversLicenseState
		/// </summary>
		public string driverLicenseState;

		/// <summary>
		/// Required: N 9 19 BankRoutingNumber
		/// </summary>
		public string routingNumber;

		/// <summary>
		/// Recommended: N 2 1 CustomerTypeCode (Set to 10, Consumer)
		/// </summary>
		public string customerTypeCode;

		/// <summary>
		/// Recommended: AN 50 13 BankName
		/// </summary>
		public string bankName;

		/// <summary>
		/// Optional
		/// </summary>
		public string ssn;

		/// <summary>
		/// Future Use
		/// </summary>
		public string merchantCustomerID;
	}
    
	
	[System.Xml.Serialization.SoapTypeAttribute("AutoSettlementInstruction", "java:com.achex.webservices")]
	public class AutoSettlementInstruction 
	{
		public string memo;
		public string amount;
		public string merchantTransferID;
	}
    
	
	[System.Xml.Serialization.SoapTypeAttribute("AuthorizationDetails", "java:com.achex.webservices")]
	public class AuthorizationDetails 
	{
		/// <summary>
		/// Required: Protocol Address Object
		/// </summary>
		public Address shippingAddress;

		/// <summary>
		/// Optional: Not Supported In This Version
		/// </summary>
		public AutoSettlementInstruction autoSettlementInstruction;

		/// <summary>
		/// Required: AN 50 1 ClearerID (same val as MerchantID)
		/// </summary>
		public string clearerID;

		/// <summary>
		/// Required: AN 50 1 MerchantID
		/// </summary>
		public string merchantID;

		/// <summary>
		/// Required: AN 100 168 TrackingID
		/// </summary>
		public string trackingID;

		/// <summary>
		/// Optional: Do not use
		/// </summary>
		public string recurringPeriodCode;

		/// <summary>
		/// Required: N X 25 AMT
		/// </summary>
		public string amount;

		/// <summary>
		/// Required: N 2 1 ConsentCode (Set to 10, Internet Transaction)
		/// </summary>
		public string consentMediumCode;

		/// <summary>
		/// NOT DOCUMENTED
		/// </summary>
		public string currencyTypeCode;

		/// <summary>
		/// NOT DOCUMENTED
		/// </summary>
		public string productDeliveryTypeCode;

		/// <summary>
		/// NOT DOCUMENTED
		/// </summary>
		public string shippingAndHandlingCost;

		/// <summary>
		/// NOT DOCUMENTED
		/// </summary>
		public string merchantOrderNumber;
	}
}
