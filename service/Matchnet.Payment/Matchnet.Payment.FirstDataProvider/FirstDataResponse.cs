using System;

using Matchnet;
using Matchnet.Payment.BusinessLogic;

namespace Matchnet.Payment.FirstDataProvider
{
	[Serializable]
	public class FirstDataResponse : ProviderResponse
	{
		public void FromAuthorizationResponse(AuthorizationResponse response) 
		{
			ResponseCode = response.statusCode;
			Success = response.statusCode == "0";

			// add logging props
			AddProperty(new Property("StatusCode", response.statusCode));
			AddProperty(new Property("ClearerID", response.clearerID));
			AddProperty(new Property("MerchantID", response.merchantID));
			AddProperty(new Property("Description", response.statusDisplayDescription));
			AddProperty(new Property("TrackingID", response.trackingID));

			// add extended details if any
			ResponseDetail[] details = response.details;

			try 
			{
				for (int i = 0; i < details.Length; i++) 
				{
					ResponseDetail detail = details[i];

					// prefix so we don't override props set above
					AddProperty(new Property("Detail - Description", detail.description));
					AddProperty(new Property("Detail - Key", detail.key));
					AddProperty(new Property("Detail - ReasonCode", detail.reasonCode));
					AddProperty(new Property("Detail - Value", detail.value));
				}
			} 
			catch {}  // don't want to fail the whole thing for this.
		}
	}
}
