using System;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Payment.BusinessLogic;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Payment.FirstDataProvider
{
	public class FirstDataProvider : Provider
	{
		// ProviderAttribute
		public const string FIELD_TRACKINGID = "TrackingID";
		public const string FIELD_AMOUNT = "Amount";
		public const string FIELD_DRIVERSLICENSENUMBER = "DriversLicenseNumber";
		public const string FIELD_BANKSTATE = "BankState";
		public const string FIELD_CHECKINGACCOUNTNUMBER = "CheckingAccountNumber";
		public const string FIELD_DRIVERSLICENSESTATE = "DriversLicenseState";
		public const string FIELD_BANKROUTINGNUMBER = "BankRoutingNumber";
		public const string FIELD_CUSTOMERTYPECODE = "CustomerTypeCode";
		public const string FIELD_BANKNAME = "BankName";
		public const string FIELD_ZIP = "Zip";
		public const string FIELD_STATE = "State";
		public const string FIELD_STREET = "Street";
		public const string FIELD_CITY = "City";
		public const string FIELD_EMAILADDRESS = "EmailAddress";
		public const string FIELD_FIRSTNAME = "FirstName";
		public const string FIELD_LASTNAME = "LastName";
		public const string FIELD_PHONE = "Phone";
		public const string FIELD_BANKACCOUNTTYPEID = "BankAccountTypeID";

		// exclusion based matching
		private const string REGEX_ALPHA_NUMERIC_DASH_WS = @"[^a-zA-Z0-9\-\s]";
		private const string REGEX_ALPHA_NUMERIC_WS = @"[^a-zA-Z0-9\s]";
		private const string REGEX_ALPHA_NUMERIC = @"[^a-zA-Z0-9]";
		private const string REGEX_ALPHA = @"[^a-zA-Z]";
		private const string REGEX_NUMERIC = @"[^0-9]";
		private const string REGEX_NONE = "";

		// MerchantAttribute
		public const string FIELD_USER = "USER";
		public const string FIELD_PASSWORD = "PWD";
		public const string FIELD_MERCHANTID = "MerchantID";
		public const string FIELD_CLEARERID = "ClearerID";

		private string _URL;
		private string _DriversLicenseNumber;
		private string _BankState;
		private string _CheckingAccountNumber;
		private string _DriversLicenseState;
		private string _BankRoutingNumber;
		private string _CustomerTypeCode;
		private string _BankName;
		private string _Zip;
		private string _State;
		private string _Street;
		private string _City;
		private string _EmailAddress;
		private string _FirstName;
		private string _LastName;
		private string _Phone;
		private string _BankAccountTypeID;
		private string _User;
		private string _Password;
		private string _MerchantID;
		private string _ClearerID;


		// TODO: Implement setting
		private string _RawRequest;
		private string _RawResponse;

		public override string RawRequest()
		{
			return null;
		}

		public override string RawResponse()
		{
			return null;
		}

		protected override ProviderResponse DoPurchase(MemberTran mt)
		{
			// you will find the following objects in Protocol.cs, a WSDL generated file.
			Credentials credentials = new Credentials();
			credentials.userName = _User;
			credentials.password = _Password;

			Address address = new Address();
			address.firstName = _FirstName;
			address.lastName = _LastName;
			address.email = _EmailAddress;
			address.street1 = _Street;
			address.city = _City;
			address.state = _State;
			address.postalCode = _Zip;
			address.phone1 = this._Phone;

			AuthorizationDetails authorizationDetails = new AuthorizationDetails();
			authorizationDetails.trackingID = System.Convert.ToString(mt.MemberTranID);
			authorizationDetails.amount = System.Convert.ToString(mt.Amount);
			authorizationDetails.merchantID = _MerchantID;
			authorizationDetails.clearerID = _ClearerID;
			authorizationDetails.consentMediumCode = _CustomerTypeCode;
			authorizationDetails.shippingAddress = address;

			DDAAccount accountInfo = new DDAAccount();
			accountInfo.routingNumber = _BankRoutingNumber;
			accountInfo.accountNumber = _CheckingAccountNumber;
			accountInfo.accountTypeCode = _BankAccountTypeID;
			accountInfo.driverLicense = _DriversLicenseNumber;
			accountInfo.driverLicenseState = _DriversLicenseState;
			accountInfo.billingAddress = address;

			if ( DevelopmentMode() ) 
			{
				accountInfo.ssn = "111111111";
			}

			/* ==== Recommended Fields, we may enable later
			accountInfo.bankName = _BankName;
			accountInfo.bankState = _BankState;
			*/

			CheckAuthorizationService service = new CheckAuthorizationService(_URL);
			AuthorizationResponse response = 
				service.createCheckAuthorization(credentials, authorizationDetails, accountInfo, new ExtendedDetail[] {});

			FirstDataResponse firstDataResponse = new FirstDataResponse();
			firstDataResponse.FromAuthorizationResponse(response);
			return firstDataResponse;
		}

		// life cycle method, gets called before DoPurchase
		protected override ValidateStatus ValidatePurchase(MemberTran mt)
		{
			ValidateStatus status = new ValidateStatus();
			status.Valid = true;
			StringBuilder message = new StringBuilder();
			
			try 
			{
				_URL = PropertyValue(FIELD_URL);

				// need the url to connect, also make sure that we are pointing to Https.
				if (_URL == string.Empty || !_URL.ToUpper().StartsWith("HTTPS:")) 
				{
					status.Valid = false;
					message.Append("URL is not defined or does not point to a secure server._URL:" + _URL + "\r\n");
				}

				// no good validation for this, make sure within range and AlphaNumeric
				_DriversLicenseNumber = PropertyValue(FIELD_DRIVERSLICENSENUMBER);

				if (!ValidateString(_DriversLicenseNumber, 1, 35, REGEX_ALPHA_NUMERIC_DASH_WS))
				{
					status.Valid = false;
					message.Append("DriversLicenseNumber is empty or contains invalid characters._DriversLicenseNumber:" + _DriversLicenseNumber + "\r\n");
				}

				/* ========= We may enable these later
				_BankState = PropertyValue(FIELD_BANKSTATE);

				if (!ValidateString(_BankState, 2, 2, REGEX_ALPHA)) 
				{
					status.Valid = false;
					message.Append("BankState is empty or is not two alpha characters.\r\n");
				}

				_BankName = PropertyValue(FIELD_BANKNAME);

				if (!ValidateString(_BankName, 1, 50, REGEX_ALPHA_NUMERIC)) 
				{
					status.Valid = false;
					message.Append("BankName is empty or is not 1-50 alpha numeric characters.\r\n");
				}
				*/

				_CheckingAccountNumber = PropertyValue(FIELD_CHECKINGACCOUNTNUMBER);

				if (!ValidateString(_CheckingAccountNumber, 4, 17, REGEX_NUMERIC)) 
				{
					status.Valid = false;
					message.Append("CheckingAccountNumber is empty or is not 4-17 numeric characters._CheckingAccountNumber:" + _CheckingAccountNumber + "\r\n");
				}

				_DriversLicenseState = PropertyValue(FIELD_DRIVERSLICENSESTATE);

				if (!ValidateString(_DriversLicenseState, 2, 2, REGEX_ALPHA))
				{
					status.Valid = false;
					message.Append("DriversLicenseState is empty or is not two alpha characters._DriversLicenseState:" + _DriversLicenseState + ".\r\n");
				}

				_BankRoutingNumber = PropertyValue(FIELD_BANKROUTINGNUMBER);

				if (!ValidateString(_BankRoutingNumber, 9, 9, REGEX_NUMERIC)) 
				{
					status.Valid = false;
					message.Append("BankRoutingNumber is empty or not 9 numeric characters._BankRoutingNumber:" + _BankRoutingNumber + ".\r\n");
				} 
				else 
				{
					if (!ValidateRoutingNumber(_BankRoutingNumber)) 
					{
						status.Valid = false;
						message.Append("BankRoutingNumber does not pass validation [http://www.brainjar.com/js/validation/].\r\n");
					}
				}

				_CustomerTypeCode = PropertyValue(FIELD_CUSTOMERTYPECODE);

				if (!ValidateString(_CustomerTypeCode, 2, 2, REGEX_NUMERIC))
				{
					status.Valid = false;
					message.Append("CustomerTypeCode is empty or non-numeric._CustomerTypeCode:" + _CustomerTypeCode + "\r\n");
				}

				_Zip = PropertyValue(FIELD_ZIP);

				if (!ValidateString(_Zip, 5,5, REGEX_NUMERIC)) 
				{
					status.Valid = false;
					message.Append("Zip is empty or not 5 numeric characters._Zip:" + _Zip + "\r\n");
				}

				_State = PropertyValue(FIELD_STATE);

				if (!ValidateString(_State, 2, 2, REGEX_ALPHA)) 
				{
					status.Valid = false;
					message.Append("State is empty or not 2 alpha characters._State:" + _State + "\r\n");
				}

				_Street = PropertyValue(FIELD_STREET);

				if (!ValidateString(_Street, 1, 50, REGEX_NONE)) 
				{
					status.Valid = false;
					message.Append("Street is empty or is not 1-50 alpha numeric characters (whitespace allowed)._Street:" + _Street + "\r\n");
				}

				_City = PropertyValue(FIELD_CITY);

				if (!ValidateString(_City, 1, 50, REGEX_ALPHA_NUMERIC_WS)) 
				{
					status.Valid = false;
					message.Append("City is empty or is not 1-50 alpha numeric characters (whitespace allowed)._City:" + _City + "\r\n");
				}

				//we don't need to validate at this point, because member email address is already validated at registration.
				_EmailAddress = PropertyValue(FIELD_EMAILADDRESS);

				_FirstName = PropertyValue(FIELD_FIRSTNAME);

				if (!ValidateString(_FirstName, 1, 25, REGEX_ALPHA)) 
				{
					status.Valid = false;
					message.Append("FirstName is empty or is not 1-25 alpha characters._FirstName:" + _FirstName + "\r\n");
				}

				_LastName = PropertyValue(FIELD_LASTNAME);

				if (!ValidateString(_LastName, 1, 50, REGEX_ALPHA)) 
				{
					status.Valid = false;
					message.Append("LastName is empty or is not 1-50 alpha characters._LastName:" + _LastName + "\r\n");
				}

				_Phone = PropertyValue(FIELD_PHONE);

				// front end allows free form, but strips (, -, ), and whitespace
				// before saving
				if (!ValidateString(_Phone, 1, 10, REGEX_NUMERIC)) 
				{
					status.Valid = false;
					message.Append("Phone is empty or is not 10 numeric characters._Phone:" + _Phone + "\r\n");
				}

				_BankAccountTypeID = PropertyValue(FIELD_BANKACCOUNTTYPEID);

				if (!ValidateString(_BankAccountTypeID, 2, 2, REGEX_NUMERIC))
				{
					status.Valid = false;
					message.Append("BankAccountType is empty or is non numeric._BankAccountTypeID:" + _BankAccountTypeID + "\r\n");
				}

				_User = PropertyValue(FIELD_USER);

				if (!ValidateString(_User, 4, 20, REGEX_NONE)) 
				{
					status.Valid = false;
					message.Append("User is empty or is not 4-20 characters.\r\n");
				}

				_Password = PropertyValue(FIELD_PASSWORD);

				if (!ValidateString(_Password, 6, 20, REGEX_NONE)) 
				{
					status.Valid = false;
					message.Append("Password is empty or is not 6-20 characters.\r\n");
				}

				_MerchantID = PropertyValue(FIELD_MERCHANTID);

				if (!ValidateString(_MerchantID, 1, 50, REGEX_NONE)) 
				{
					status.Valid = false;
					message.Append("MerchantID is empty or is not 1-50 characters.\r\n");
				}

				_ClearerID = PropertyValue(FIELD_CLEARERID);

				if (!ValidateString(_ClearerID, 1, 15, REGEX_NONE)) 
				{
					status.Valid = false;
					message.Append("ClearerID is empty or is not 1-15 characters.\r\n");
				}
			} 
			catch (Exception ex) 
			{
				status.Message = ex.Message;
				status.Valid = false;
			}
			
			if (!status.Valid) 
			{
				status.Message = message.ToString();
			}

			return status;
		}

		private bool ValidateString(string val, int minLen, int maxLen, string pattern)
		{
			if (val == null || val == string.Empty) 
			{
				return false;
			}

			if (val.Length < minLen || val.Length > maxLen) 
			{
				return false;
			}

			if (pattern == REGEX_NONE) 
			{
				return true;
			}

			Regex regex = new Regex(pattern);
			return !regex.IsMatch(val);
		}

		private bool ValidateRoutingNumber(string val) 
		{
			try 
			{
				char[] chars = val.ToCharArray();

				int sum = 0;
				for (int i = 0; i < chars.Length; i += 3) 
				{
					
					sum += int.Parse(System.Convert.ToString(chars[i])) * 3
						+  int.Parse(System.Convert.ToString(chars[i + 1])) * 7
						+  int.Parse(System.Convert.ToString(chars[i + 2]));
				}

				if (sum != 0 && sum % 10 == 0) 
				{
					return true;
				}
			} 
			catch {}
			return false;
		}

		private string PropertyValue(string key) 
		{
			if (providerProperties.ContainsKey(key)) 
			{
				return System.Convert.ToString(providerProperties[key]);
			} 
			else if (providerProperties.ContainsKey(key.ToUpper())) 
			{
				return System.Convert.ToString(providerProperties[key.ToUpper()]);
			} 
			else if (providerProperties.ContainsKey(key.ToLower())) 
			{
				return System.Convert.ToString(providerProperties[key.ToLower()]);
			} 
			else 
			{
				return string.Empty;
			}
		}

		private bool DevelopmentMode()
		{
			const string KEY_DEVELOPMENTMODE = "DevelopmentMode";
			bool developmentMode = false;

			if ( ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE) != null )
			{
				developmentMode = Convert.ToBoolean(ConfigurationSettings.AppSettings.Get(KEY_DEVELOPMENTMODE));
			}

			return developmentMode;
		}

		#region ActionNotSupported
		public override ProviderResponse Authorize(MemberTran mt)
		{
			throw new Exception("FirstData Authorize should be done using Purchase");
		}

		protected override ProviderResponse DoCredit(MemberTran mt)
		{
			throw new Exception("FirstData Credit Not Supported");
		}

		public override ProviderResponse SetPayment(MemberTran mt)
		{
			throw new Exception("SetPayment Not Supported");
		}

		public override ProviderResponse Void(MemberTran mt)
		{
			throw new Exception("Void Not Supported");
		}
		#endregion ActionNotSupported
	
	}
}
