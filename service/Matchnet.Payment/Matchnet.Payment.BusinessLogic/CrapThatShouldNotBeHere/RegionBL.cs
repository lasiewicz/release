using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet;
using Matchnet.Data;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.BusinessLogic
{
	public sealed class RegionBL
	{
		#region Public Members

		public readonly static RegionBL Instance = new RegionBL();

		#endregion
		
		#region Private Members

		private Matchnet.Caching.Cache cache;
		private const string mnRegionPayment = "mnRegionPayment";

		#endregion

		private RegionBL()
		{
			cache = Matchnet.Caching.Cache.Instance;
		}

		public Region GetRegion( 
			int regionID)
		{
			Region region = null;
			RegionCollection regionCollection = getRegions();
			
			if ( regionCollection == null )
			{
				throw new BLException("Country ISO codes are empty");
			}

			region = regionCollection.FindByID( regionID );

			if ( region == null )
			{
				throw new BLException("Can not find region for regionID: " + regionID.ToString() );
			}

			return region;
		}

		private RegionCollection getRegions()
		{
			RegionCollection regionCollection = cache.Get(RegionCollection.GetCacheKey(Matchnet.Constants.NULL_INT)) as RegionCollection;

			if (regionCollection == null)
			{
				SqlDataReader dataReader = null;

				try
				{
					Command command = new Command(mnRegionPayment, "dbo.up_Country_List_All", 0);
					dataReader = Client.Instance.ExecuteReader(command);

					regionCollection = new RegionCollection();

					bool colLookupDone = false;
					Int32 ordinalRegionID = Constants.NULL_INT;
					Int32 ordinalISOCountryCode2 = Constants.NULL_INT;
					Int32 ordinalISOCountryCode3 = Constants.NULL_INT;

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalRegionID = dataReader.GetOrdinal("RegionID");
							ordinalISOCountryCode2 = dataReader.GetOrdinal("ISOCountryCode2");
							ordinalISOCountryCode3 = dataReader.GetOrdinal("ISOCountryCode3");
							colLookupDone = true;
						}

						regionCollection.Add(new Region(dataReader.GetInt32(ordinalRegionID),
							dataReader.GetString(ordinalISOCountryCode2),
							dataReader.GetString(ordinalISOCountryCode3)));
					}
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}

				cache.Add(regionCollection);
			}
			
			return regionCollection;
		}

	}
}
