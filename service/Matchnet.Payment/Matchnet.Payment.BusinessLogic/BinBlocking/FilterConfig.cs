using System;
using System.Collections;


namespace Matchnet.Payment.BusinessLogic
{
	public class FilterConfig
	{
		public static readonly FilterConfig Instance = new FilterConfig();
		private Hashtable _merchants;

		private FilterConfig()
		{
			_merchants = System.Configuration.ConfigurationSettings.GetConfig("filter") as Hashtable;
		}


		public string[] GetBins(Int32 merchantID)
		{
			string[] binList = _merchants[merchantID] as string[];

			if (binList == null)
			{
				binList = new string[0];
			}

			return binList;
		}


		public bool IsBinListed(Int32 merchantID, string cardNumber)
		{
			string[] binList = GetBins(merchantID);

			for (Int32 binNum = 0; binNum < binList.Length; binNum++)
			{
				if (cardNumber.IndexOf(binList[binNum]) == 0)
				{
					return true;
				}
			}

			return false;
		}
	}
}
