using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Xml;


namespace Matchnet.Payment.BusinessLogic
{
	internal class FilterConfigHandler : IConfigurationSectionHandler
	{
		public object Create(object parent,
			object configContext,
			XmlNode section)
		{
			Hashtable _merchants = new Hashtable();

			try
			{
				for (Int32 merchantNum = 0; merchantNum < section.ChildNodes.Count; merchantNum++)
				{
					XmlNode merchant = section.ChildNodes[merchantNum];
					Int32 merchantID = Convert.ToInt32(merchant.Attributes["id"].Value);
					string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + merchant.Attributes["fileName"].Value;
					ArrayList binList = new ArrayList();
					StreamReader sr = new StreamReader(fileName);

					while (sr.Peek() >= 0)
					{
						string line = sr.ReadLine().Trim();
						if (line.Length == 6)
						{
							binList.Add(line);
						}
					}

					sr.Close();

					_merchants[merchantID] = (string[])binList.ToArray(typeof(string));
					
					/*
					Int32 binCount = node.ChildNodes.Count;
					string[] binList = new string[binCount];
					_merchants[Convert.ToInt32(node.Attributes["id"].Value)] = binList;

					System.Diagnostics.Trace.WriteLine("__merchant: " + node.Attributes["id"].Value + ", bins: " + binCount.ToString());
				
					for (Int32 binNum = 0; binNum < binCount; binNum++)
					{
						try
						{
							binList[binNum] = node.ChildNodes[binNum].Attributes["id"].Value;
						}
						catch (Exception ex)
						{
							System.Diagnostics.Trace.WriteLine("__" + binNum.ToString() + " " + ex.ToString().Replace("\r", "").Replace("", "\n"));
						}
					}
					*/

					System.Diagnostics.Trace.WriteLine("__merchant: " + merchantID.ToString() + ", bins: " + binList.Count.ToString() + " done");
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
				throw new Exception("Error parsing filter section of config file.", ex);
			}
				
			return _merchants;
		}
	}
}
