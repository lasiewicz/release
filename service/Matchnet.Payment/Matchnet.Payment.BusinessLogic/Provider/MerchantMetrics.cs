using System;
using System.Collections;
using System.Threading;
using System.Diagnostics;

using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.BusinessLogic
{

	internal class MerchantMetrics
	{
		public static readonly MerchantMetrics Instance = new MerchantMetrics();
		private Hashtable _merchants;
		private ReaderWriterLock _merchantsLock;
		private const Int32 SAMPLE_INTERVAL = 60 * 5;



		private MerchantMetrics()
		{
			_merchants = new Hashtable();
			_merchantsLock = new ReaderWriterLock();
		}


		public void LogTransaction(Int32 merchantID,
			TranType tranType,
			bool success)
		{
			if (tranType == TranType.AuthorizationOnly
				|| tranType == TranType.InitialBuy
				|| tranType == TranType.Renewal)
			{
				ArrayList tranList = getTranList(merchantID);
				tranList.Add(new MetricTransaction(tranType, success));

				Int32 tranCountAuth = 0;
				Int32 tranCountInitialBuy = 0;
				Int32 tranCountRenewal = 0;
				Int32 tranCountAuthSuccess = 0;
				Int32 tranCountInitialBuySuccess = 0;
				Int32 tranCountRenewalSuccess = 0;

				lock (tranList)
				{
					for (Int32 i = 0; i < tranList.Count; i++)
					{
						MetricTransaction metricTransaction = tranList[i] as MetricTransaction;
						
						/*
						todo: remove metricTransaction from tranList if timestamp < current time - SAMPLE_INTERVAL seconds
						else increment corresponding tranCount
						*/

						// Remove any transactions that are not within the Sample Interval timeframe
						if (metricTransaction.Time < System.DateTime.Now.Subtract(System.TimeSpan.FromSeconds(SAMPLE_INTERVAL)))
						{
							tranList.Remove(i);
						}
						else	// Any transactions that are in the Sample Interval timeframe we should track
						{
							switch(metricTransaction.TranType)
							{
								case Matchnet.Purchase.ValueObjects.TranType.AuthorizationOnly:
									tranCountAuth ++;
									if (metricTransaction.Success) tranCountAuthSuccess++;
									break;
								case Matchnet.Purchase.ValueObjects.TranType.InitialBuy:
									tranCountInitialBuy ++;
									if (metricTransaction.Success) tranCountInitialBuySuccess++;
									break;
								case Matchnet.Purchase.ValueObjects.TranType.Renewal:
									tranCountRenewal++;
									if (metricTransaction.Success) tranCountRenewalSuccess++; 
									break;
								default:
									break;
							}
						}
					}
				}
				//todo: update perf counter values based on tranCount values


			}
		}


		private ArrayList getTranList(Int32 merchantID)
		{
			ArrayList tranList = null;

			_merchantsLock.AcquireReaderLock(-1);
			try
			{
				tranList = _merchants[merchantID] as ArrayList;

				if (tranList == null)
				{
					_merchantsLock.UpgradeToWriterLock(-1);
					tranList = new ArrayList();
					_merchants.Add(merchantID, tranList);
					//todo: create counter instances for this merchantID b/c this is first tran for merchant


				}
			}
			finally
			{
				_merchantsLock.ReleaseLock();
			}

			return tranList;
		}


		class MetricTransaction
		{
			private TranType _tranType;
			private bool _success;
			private DateTime _time;

			public MetricTransaction(TranType tranType,
				bool success)
			{
				_tranType = tranType;
				_success = success;
				_time = DateTime.Now;
			}


			public TranType TranType
			{
				get
				{
					return _tranType;
				}
			}


			public bool Success
			{
				get
				{
					return _success;
				}
			}


			public DateTime Time
			{
				get
				{
					return _time;
				}
			}
		}
	}
}
