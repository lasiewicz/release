using System;
using System.Messaging;

using Matchnet.Queuing;


namespace Matchnet.Payment.BusinessLogic
{
	[Serializable]
	public class InitMessageProvider : IInitMessage
	{
		public void Process()
		{
			System.Diagnostics.Trace.WriteLine("__InitMessageProvider");
			MessageQueue queue = new MessageQueue(PaymentBL.QUEUEPATH_RESPONSE);
			queue.Formatter = new BinaryMessageFormatter();
			queue.Send(new InitMessage(), MessageQueueTransactionType.Automatic);
		}
	}
}
