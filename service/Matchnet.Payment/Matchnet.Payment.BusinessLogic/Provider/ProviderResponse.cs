using System;
using System.Collections;

using Matchnet.Purchase.ValueObjects;
using System.Data.SqlClient;
using System.Data;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

namespace Matchnet.Payment.BusinessLogic
{
	[Serializable]
	public class ProviderResponse
	{
		private bool _success;
		private bool _retry;
		private string _responseCode;
		private string _responseCodeAVS;
		private string _responseCodeCVV;
		private string _message;
		private ArrayList _properties;
		private MemberTran _memberTran;
		protected Int32 _providerID;

		//09252008 TL - Added Request/ResponseData for logging purposes when provider returns an error (not to be confused with failure).
		protected string _requestDataScrubbed = "";
		protected string _responseData = "";
		protected bool _logRequestResponseData = false;

		public ProviderResponse(bool success,
			string responseCode,
			string responseCodeAVS,
			string responseCodeCVV,
			string message,
			MemberTran memberTran,
			Int32 providerID) 
		{
			_properties = new ArrayList();
			_success = success;
			_responseCode = responseCode;
			_responseCodeAVS = responseCodeAVS;
			_responseCodeCVV = responseCodeCVV;
			_message = message;
			_memberTran = memberTran;
			_providerID = providerID;
		}


		public ProviderResponse()
		{
			_properties = new ArrayList();
		}
		

		public void AddProperty(string name, string val) 
		{
			AddProperty(new Property(name, val));
		}


		public void AddProperty(Property property) 
		{
			_properties.Add(property);
		}


		public ArrayList Properties
		{
			get
			{
				return _properties;
			}
		}


		public bool Success 
		{
			get
			{
				return _success;
			}
			set
			{
				_success = value; 
			}
		}


		public bool Retry 
		{
			get
			{
				return _retry;
			}
			set
			{
				_retry = value;
			}
		}


		public string ResponseCode 
		{
			get
			{
				return _responseCode;
			}
			set
			{
				_responseCode = value;
			}
		}


		public string ResponseCodeAVS
		{
			get
			{
				return _responseCodeAVS;
			}
			set
			{
				_responseCodeAVS = value;
			}
		}


		public string ResponseCodeCVV 
		{
			get
			{
				return _responseCodeCVV;
			}
			set
			{
				_responseCodeCVV = value;
			}
		}


		public string Message 
		{
			get
			{
				return _message;
			}
			set
			{
				_message = value;
			}
		}


		public MemberTran MemberTran
		{
			get
			{
				return _memberTran;
			}
			set
			{
				_memberTran = value;
			}
		}


		public Int32 ProviderID
		{
			get
			{
				return _providerID;
			}
			set
			{
				_providerID = value;
			}
		}

		/// <summary>
		/// The request data (xml or name/value pairs) sent to the provider.
		/// IMPORTANT: This is mainly used to log request data upon exception, so the data should already
		/// be scrubbed of all data such as CC, CVV, PW.
		/// </summary>
		public string RequestDataScrubbed
		{
			get
			{
				return _requestDataScrubbed;
			}
			set
			{
				_requestDataScrubbed = value;
			}
		}

		/// <summary>
		/// The response data returned from the provider.
		/// </summary>
		public string ResponseData 
		{
			get
			{
				return _responseData;
			}
			set
			{
				_responseData = value;
			}
		}

		/// <summary>
		/// Indicates whether there is a need to log request/response data due to an exception.
		/// This is currently used to determine whether to send request/response data when generating
		/// a response to send to Purchase, in order to also log request/response data in mnChargeStage.
		/// </summary>
		public bool logRequestResponseData
		{
			get
			{
				return _logRequestResponseData;
			}
			set
			{
				_logRequestResponseData = value;
			}
		}

		/// <summary>
		/// This method will log response/request data (e.g. xml, name/value url string) to the mnCharge.ChargeLogXML database for logging
		/// </summary>
		protected void LogRequestResponseXML(string responseXML, string requestXML, MemberTran mt)
		{
			try
			{
				if (mt != null)
				{
					Command command = new Command("mnCharge", "dbo.up_ChargeLogXML_Insert", 0);
					command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, mt.ChargeID);
					command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, mt.MemberTranID);
					command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, mt.MemberPaymentID);
					command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, mt.MemberID);
					command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, _providerID);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, mt.SiteID);
					command.AddParameter("@XMLRequest", SqlDbType.NVarChar, ParameterDirection.Input, requestXML);
					command.AddParameter("@XMLResponse", SqlDbType.NVarChar, ParameterDirection.Input, responseXML);

					Client.Instance.ExecuteAsyncWrite(command);
				}
			}
			catch (Exception ex)
			{
				new ServiceBoundaryException(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME,
					"Error in Payment ProviderResponse.LogRequestResponseXML: " + ex.Message + ", memberTranID:" + mt.MemberTranID.ToString() + ").\r\n",
					null);
			}
		}
	}
}