using System;
using System.Reflection;

using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.BusinessLogic
{
	public class ProviderFactory
	{
		public static Provider GetProvider(SiteMerchant siteMerchant) 
		{
			string agentAssembly = siteMerchant.AgentAssembly;
			string agentType = siteMerchant.AgentType;

			string strEnableTestProvider = string.Empty;
			try
			{
				strEnableTestProvider = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_TEST_PROVIDER");
			}
			catch
			{
				strEnableTestProvider = "FALSE";
			}

			if ("TRUE" == strEnableTestProvider.ToUpper())
			{
				agentAssembly = "Matchnet.Payment.TestProvider";
				agentType = "Matchnet.Payment.TestProvider.TestProvider";
			}

			//System.Diagnostics.Trace.WriteLine("__agentAssembly: " + agentAssembly);
			//System.Diagnostics.Trace.WriteLine("__agentType: " + agentType);

			//temporary hack - namespace change
			agentAssembly = agentAssembly.Replace(".PaymentProvider.", ".Payment.");
			agentType = agentType.Replace(".PaymentProvider.", ".Payment.");

			//System.Diagnostics.Trace.WriteLine("__agentAssembly: " + agentAssembly);
			//System.Diagnostics.Trace.WriteLine("__agentType: " + agentType);

			Assembly assembly = Assembly.Load(agentAssembly);
			Type type = assembly.GetType(agentType, true, true);
			Provider provider = (Provider)Activator.CreateInstance(type);
			provider.MerchantID = siteMerchant.MerchantID;
			provider.ProviderID = siteMerchant.ProviderID;
			return provider;
		}
	}
}
