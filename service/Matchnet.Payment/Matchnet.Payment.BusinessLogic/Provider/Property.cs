using System;

using Matchnet;

namespace Matchnet.Payment.BusinessLogic
{
	[Serializable]
	public class Property
	{
		private string name;
		private string value;

		public Property(string name, string value) 
		{
			this.name = name;
			this.value = value;
		}

		public string Name 
		{
			get { return name; }
		}

		public string Value 
		{
			get { return value; }
		}
	}
}
