using System;

namespace Matchnet.Payment.BusinessLogic
{
	public class ProviderNetworkException : Exception
	{
		private string _networkErrorMessage;

		public ProviderNetworkException(string networkErrorMessage)
		{
			_networkErrorMessage = networkErrorMessage;
		}


		public string NetworkErrorMessage
		{
			get
			{
				return _networkErrorMessage;
			}
		}
	}
}
