using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Text;
using System.IO;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;

using Matchnet.Security;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.BusinessLogic
{
	public abstract class Provider
	{
		public const string FIELD_URL = "Url";
		public const string FIELD_PORT = "Port";
		public const string FIELD_REFER = "Referer";
		public const string FIELD_PURCHASE_TYPE = "PurchaseTypeID";
		public const string FIELD_RENEW_FLAG = "RenewFlag";
		public const string FIELD_PAYMENT_TYPE = "PaymentTypeID";
		public const string FIELD_TRAN_TYPE = "TranTypeID";

		public const int PAYMENT_TYPE_CREDIT_CARD = 1;
		public const int PAYMENT_TYPE_CHECK = 2;
		public const int PAYMENT_TYPE_DIRECT_DEBIT = 4;

		public const string RESPONSE_VELOCITY_FAILURE = "1004";
		public const string RESPONSE_VELOCITY_FAILURE_MSG = "velocity failure";

		public const string RESPONSE_CC_MAX_FAILURE = "1005";
		public const string RESPONSE_CC_MAX_FAILURE_MSG = "max credit card attempt failure";
		public const int  CC_MAX_ATTEMPT_HOURS = 24;

	
		private ProviderResponse providerResponse;
		protected Hashtable providerProperties;
		private static Hashtable ExcludedFields = GetExcludedFields();
		protected Int32 _merchantID;
		protected Int32 _providerID;
		protected Guid _guid;

		protected int timeoutOverride = Matchnet.Constants.NULL_INT; //override timeout, read from config

		private static Hashtable GetExcludedFields() 
		{
			Hashtable excluded = new Hashtable();
			excluded.Add(FIELD_URL,"");
			excluded.Add(FIELD_PORT,"");
			excluded.Add(FIELD_REFER,"");
			excluded.Add(FIELD_PURCHASE_TYPE,"");
			excluded.Add(FIELD_RENEW_FLAG, "");
			excluded.Add(FIELD_PAYMENT_TYPE, "");
			excluded.Add(FIELD_TRAN_TYPE, "");
			return excluded;
		}

		protected bool IsExcludedField(string name) 
		{
			return ExcludedFields.ContainsKey(name);			
		}

		public StatusType Process(MemberTran memberTran, 
			Hashtable properties)
		{
			_guid = Guid.NewGuid();

			ProviderResponse response = null;
			StatusType statusType = StatusType.Failure;

			providerProperties = properties;

			System.Diagnostics.Trace.WriteLine("__" + memberTran.TranType.ToString() + " " + memberTran.SiteID.ToString());

			//MPR-660 TL 11132008 - Adding explicit timeout
			try
			{
				if (System.Configuration.ConfigurationSettings.AppSettings["timeout" + memberTran.SiteMerchant.ProviderID.ToString()] != null)
				{
					//read timeout from config
					timeoutOverride = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["timeout" + memberTran.SiteMerchant.ProviderID.ToString()]);
				}
			}
			catch (Exception ex)
			{
				//keep default as null
				timeoutOverride = Matchnet.Constants.NULL_INT;
			}

			switch (memberTran.TranType) 
			{
				case TranType.InitialBuy:
				case TranType.Renewal:
					System.Diagnostics.Trace.WriteLine("__Purchase begin");
					response = Purchase(memberTran);
					System.Diagnostics.Trace.WriteLine("__Purchase end");
					break;
				case TranType.AuthorizationOnly:
				case TranType.AuthorizationNoTran:
				case TranType.AuthPmtProfile:
					response = Authorize(memberTran);
					break;
				case TranType.AdministrativeAdjustment:
					response = Credit(memberTran);
					break;
				case TranType.Void:
					response = Void(memberTran);
					break;
				default:
					throw new Exception("Unknown Transaction Type: " + ((int)memberTran.TranType).ToString() + " for MemberTranID: " + memberTran.MemberTranID.ToString() );
			}

			if (response != null) 
			{
				response.ProviderID = _providerID;
				response.MemberTran = memberTran;

				MessageQueue queue = new MessageQueue(PaymentBL.QUEUEPATH_RESPONSE);
				queue.Formatter = new BinaryMessageFormatter();
				queue.Send(response, System.EnterpriseServices.ContextUtil.IsInTransaction ? MessageQueueTransactionType.Automatic : MessageQueueTransactionType.Single);

			}

			return statusType;
		}


		private Hashtable GetUniqueLogValues(ArrayList properties)
		{
			Hashtable newProperties = null;

			if ( properties != null && properties.Count > 0 )
			{
				newProperties = new Hashtable();
				
				for ( int i = 0; i < properties.Count; i++ )
				{
					Property property = (Property)properties[ i ];
					string name = property.Name;
					string value = property.Value;
						
					if ( !newProperties.Contains( name ) )
					{
						for ( int j = 0; j < properties.Count; j++ )
						{
							if ( ((Property)properties[ j ]).Name == name && j != i )
							{
								value = value + "|" + ((Property)properties[ j ]).Value;
							}
						}
						newProperties.Add( name, value );
					}
				}
			}

			return newProperties;
		}

		private void GetProviderStatus(Int32 providerID,
			string responseCode,
			out string resourceConstant,
			out StatusType statusType)
		{
			resourceConstant = Matchnet.Constants.NULL_STRING;
			statusType = StatusType.Failure;
			/*
			int statusTypeID = Matchnet.Constants.NULL_INT;
			DataTable dt = ProviderDB.GetProviderStatus( providerID, responseCode );
			
			if ( dt != null && dt.Rows.Count > 0 )
			{
				resourceConstant = dt.Rows[0]["ResourceConstant"].ToString();
				statusTypeID = Convert.ToInt32( dt.Rows[0]["StatusTypeID"] );

			}

			if ( ! Enum.IsDefined(typeof(StatusType), statusTypeID) )
			{
				throw new Exception( "BL error.  Provider response mapping not found for ProviderID: " + providerID.ToString() + " ResponseCode: " + responseCode );
			}

			statusType = (StatusType)Enum.Parse(typeof(StatusType), statusTypeID.ToString() );
			*/
		}

		/// <summary>
		/// Performs a Purchase using the Provider specific
		/// implementation of DoPurchase
		/// </summary>
		/// <param name="mt">The mt to Purchase</param>
		/// <param name="merchantID">The Unique Identifier for the merchant</param>
		/// <returns>A Populated Response from the Provider</returns>
		public ProviderResponse Purchase(MemberTran memberTran) 
		{
			try
			{
				ValidateStatus validate = ValidatePurchase(memberTran);
				if (validate.Valid) 
				{
					ProviderResponse response = null;
					
					response = VelocityCheck(memberTran);
					if (response != null)
					{
						return response;
					}

					response = this.CCMaxAttemptCheck(memberTran);
					if (response != null)
					{
						return response;
					}

					return DoPurchase(memberTran);				
				} 
				else 
				{
					throw new Exception(validate.Message);
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__Purchase error: " + ex.ToString().Replace("\r", "").Replace("\n", ""));
				throw ex;
			}
		}


		public ProviderResponse VelocityCheck(MemberTran memberTran)
		{
			//MPR-134 06262008 TL - Skip velocity check for admins
			if (memberTran.AdminMemberID <= 0 || memberTran.AdminMemberID == Constants.NULL_INT)
			{
				if (memberTran.PaymentType == PaymentType.CreditCard)
				{
					//velocity check
					SqlDataReader dataReader = null;

					try
					{
						Command command = new Command("mnCharge", "dbo.up_VelocityLog_List", 0);
						command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
						command.AddParameter("@CardHash", SqlDbType.VarChar, ParameterDirection.Input, Crypto.Hash(memberTran.AccountNumber.Trim() + PurchaseConstants.SOOPER_HASH));
					
						bool AuthFlag = false;
						if(memberTran.TranType == TranType.AuthorizationOnly
							|| memberTran.TranType == TranType.AuthPmtProfile)
						{
							AuthFlag = true;
						}
						command.AddParameter("@AuthFlag", SqlDbType.Bit, ParameterDirection.Input, AuthFlag);
						dataReader = Client.Instance.ExecuteReader(command);

						bool colLookupDone = false;
						Int32 ordinalUpdateDate = Constants.NULL_INT;

						while (dataReader.Read())
						{
							if (!colLookupDone)
							{
								ordinalUpdateDate = dataReader.GetOrdinal("UpdateDate");
								colLookupDone = true;
							}

							//should be configurable, but not for now
							Int32 days = -10;

							if (memberTran.TranType == TranType.AuthorizationOnly
								|| memberTran.TranType == TranType.AuthPmtProfile)
							{
								days = -1;
							}
							else if (memberTran.SiteID == Matchnet.Purchase.ValueObjects.PurchaseConstants.CA_SITE_ID)
							{
								days = -6;
							}
							else if(memberTran.PlanID == 406)
							{
								days = -9;
							}
							else if(memberTran.PlanID == 408 || memberTran.PlanID == 420)
							{
								days = -6;
							}

							if (dataReader.GetDateTime(ordinalUpdateDate) > DateTime.Now.AddDays(days))
							{
								ProviderResponse response = new ProviderResponse(false,
									RESPONSE_VELOCITY_FAILURE,
									null,
									null,
									null,
									memberTran,
									this.ProviderID);

								response.AddProperty("response", RESPONSE_VELOCITY_FAILURE);

								return response;
							}
						}
					}
					finally
					{
						if (dataReader != null)
						{
							dataReader.Close();
						}
					}
				}
			}

			return null;
		}

		/// <summary>
		/// Verify number of credit card attempts
		/// </summary>
		/// <param name="memberTran"></param>
		/// <returns></returns>
		public ProviderResponse CCMaxAttemptCheck(MemberTran memberTran)
		{
			// verify only for credit card and initial buy
			if (memberTran.PaymentType == PaymentType.CreditCard && memberTran.TranType == Matchnet.Purchase.ValueObjects.TranType.InitialBuy)
			{
				
				SqlDataReader dataReader = null;

				try
				{
					// hash the credit card for validation
					string cardHash = string.Empty;
					Matchnet.Purchase.ValueObjects.CreditCardPayment v = (Matchnet.Purchase.ValueObjects.CreditCardPayment)memberTran.Payment;
					if(v.CreditCardType == Matchnet.Purchase.ValueObjects.CreditCardType.AmericanExpress)
					{
						// For American Express, only the first 10 digits
						cardHash = Crypto.Hash(memberTran.AccountNumber.Trim().Substring(0,10) + PurchaseConstants.SOOPER_HASH);
					}
					else
					{
						// the complete credit number for all others
						cardHash = Crypto.Hash(memberTran.AccountNumber.Trim() + PurchaseConstants.SOOPER_HASH);
					}

					Command command = new Command("mnCharge", "dbo.up_GetCCAttemptCount", 0);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
					command.AddParameter("@CardHash", SqlDbType.VarChar, ParameterDirection.Input, cardHash);
					command.AddParameter("@HoursBack", SqlDbType.Int, ParameterDirection.Input, CC_MAX_ATTEMPT_HOURS);
					dataReader = Client.Instance.ExecuteReader(command);

					bool colLookupDone = false;
					Int32 CCAttempt = Constants.NULL_INT;	

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							string s = dataReader.GetValue(0).ToString();
							CCAttempt = int.Parse(s);
							colLookupDone = true;
						}

						// default to 100, this should not affect anything
						int confixMaxAttempt = 100;
						try
						{
							confixMaxAttempt = int.Parse(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CC_MAX_ATTEMPT_" + memberTran.SiteID.ToString()));
						}
						catch{}

						if (CCAttempt >= confixMaxAttempt) 
						{
							// reponse with max credit card failure
							ProviderResponse response = new ProviderResponse(false,
								RESPONSE_CC_MAX_FAILURE,
								null,
								null,
								null,
								memberTran,
								this.ProviderID);

							response.AddProperty("response", RESPONSE_CC_MAX_FAILURE);

							return response;
						}
						else
						{
							// log the credit number and continue process purchase
							this.CCMaxAttemptLog(memberTran);
						}
					}
				}
				catch(Exception ex)
				{
					string errMsg = this.ToString() + "CCMaxAttemptCheck() - " + ex.Message;
					throw new Exception(errMsg,ex);
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}
			}

			return null;
		}

		/// <summary>
		/// Log the credit card attempt
		/// </summary>
		/// <param name="memberTran"></param>
		private void CCMaxAttemptLog(MemberTran memberTran)
		{
			SyncWriter sw = new SyncWriter();
			try
			{
				// hash the credit card number
				string cardHash = string.Empty;
				Matchnet.Purchase.ValueObjects.CreditCardPayment v = (Matchnet.Purchase.ValueObjects.CreditCardPayment)memberTran.Payment;
				if(v.CreditCardType == Matchnet.Purchase.ValueObjects.CreditCardType.AmericanExpress)
				{
					// first 10 digits for American express
					cardHash = Crypto.Hash(memberTran.AccountNumber.Trim().Substring(0,10) + PurchaseConstants.SOOPER_HASH);
				}
				else
				{
					// complete cc number for all others
					cardHash = Crypto.Hash(memberTran.AccountNumber.Trim() + PurchaseConstants.SOOPER_HASH);
				}

				Command[] commands;	
				
				commands = new Command[1];
				Command commandCCAttempt = new Command("mnCharge", "dbo.up_CCAttemptLog_Save", 0);
				commandCCAttempt.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
				commandCCAttempt.AddParameter("@CardHash", SqlDbType.VarChar, ParameterDirection.Input, cardHash);
				commandCCAttempt.AddParameter("@InsertDate ", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
				commands[0] = commandCCAttempt;
		
				
				Exception swEx;
				sw.Execute(commands,
					null,
					out swEx);

				if (swEx != null)
				{
					throw swEx;
				}
			}
			catch(Exception ex)
			{
				string errMsg = this.ToString() + "CCMaxAttemptLog() - " + ex.Message;
				throw new Exception(errMsg,ex);
			}
			finally
			{
				sw.Dispose();
			}
		}


		/// <summary>
		/// Performs an AUTH-ONLY transaction using a Provider specific
		/// implementation.
		/// </summary>
		/// <param name="properties">The name/value pairs to use when building a request to the provider</param>
		/// <returns>A Populated Response from the Provider</returns>
		public abstract ProviderResponse Authorize(MemberTran memberTran);
		
		/// <summary>
		/// Performs a VOID transaction using a Provider specific
		/// implementation.
		/// </summary>
		/// <param name="properties"></param>
		/// <returns></returns>
		public abstract ProviderResponse Void(MemberTran memberTran);
		
		/// <summary>
		/// Validates a mt, ensures negative numbers are used
		/// for NumDays and Amount, and performs a Credit using the Provider
		/// specific implementation of DoCredit.
		/// </summary>
		/// <param name="mt">The mt to Credit</param>
		/// <param name="merchantID">The Unique Identifier for the merchant</param>
		/// <returns>A Populated Response from the Provider</returns>
		public ProviderResponse Credit(MemberTran memberTran) 
		{
			ValidateStatus validate = ValidateCredit(memberTran);

			if (validate.Valid)
			{
				// make sure neg numbers for months and amount
				if (memberTran.Amount > 0) 
				{
					memberTran.Amount = -memberTran.Amount;
				}

				if (memberTran.Minutes > 0) 
				{
					memberTran.Minutes = -memberTran.Minutes;
				}
				
				return DoCredit(memberTran);
			} 
			else 
			{
				throw new Exception( validate.Message );
			}
		}


		/// <summary>
		/// A Utility Function taht probably should exist somewhere else. This
		/// properly escapes XML reserved and unicode characters.
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		protected string XMLEncodeValue(string val)
		{
			return XMLEncodeValue(val, true);
		}

		/// <summary>
		/// A Utility Function taht probably should exist somewhere else. This
		/// properly escapes XML reserved and unicode characters.
		/// </summary>
		/// <param name="val"></param>
		/// <returns></returns>
		protected string XMLEncodeValue(string val, bool escapeChar) 
		{
			// XML Reserved Characters.
			val = val.Replace("&", "&amp;");
			val = val.Replace("<", "&lt;");
			val = val.Replace(">", "&gt;");
			val = val.Replace("'", "&apos;");
			val = val.Replace("\"", "&quot;");

			StringBuilder builder = new StringBuilder();

			char[] chars = val.ToCharArray();

			for (int x = 0; x < chars.Length; x++) 
			{
				if (escapeChar == true)
				{
					if ((int)chars[x] < 128) 
					{
						builder.Append(val.Substring(x, 1));
					} 
					else 
					{
						builder.Append("&#" + (int)chars[x] + ";");
					}
				}
				else
				{
					builder.Append(val.Substring(x, 1));
				}
			}

			return builder.ToString();
		}

		/// <summary>
		/// Concrete Processor implementations should override this
		/// method to provide Validation Logic for Crediting a mt. 
		/// </summary>
		/// <param name="mt">The mt to Validate</param>
		/// <returns>A Populated ValidateStatus struct</returns>
		protected virtual ValidateStatus ValidateCredit(MemberTran memberTran) 
		{
			ValidateStatus status = new ValidateStatus();
			status.Valid = true;
			status.Message = string.Empty;
			return status;
		}

		/// <summary>
		/// Concrete Processor implementations should override this
		/// method to provide Validation Logic for Purchasing a mt. 
		/// </summary>
		/// <param name="mt">The mt to Validate</param>
		/// <returns>A Populated ValidateStatus struct</returns>
		protected virtual ValidateStatus ValidatePurchase(MemberTran memberTran) 
		{
			ValidateStatus status = new ValidateStatus();
			status.Valid = true;
			status.Message = string.Empty;
			return status;
		}

		public ProviderResponse ProviderResponse 
		{
			get
			{
				return providerResponse;
			}
		}

		/// <summary>
		/// A Utility function for debugging / logging purposes.
		/// </summary>
		/// <returns>The raw request to the Provider</returns>
		public abstract string RawRequest();

		/// <summary>
		/// A Utility function for debugging / logging purposes.
		/// </summary>
		/// <returns>The raw response from the Provider</returns>
		public abstract string RawResponse();

		/// <summary>
		/// Concrete Processor implementations must implement this
		/// method to provide Provider specific logic for crediting.
		/// </summary>
		/// <param name="mt">The mt to Credit</param>
		/// <param name="merchantID">The Unique Identifier for the merchant</param>
		/// <returns>A Populated Response from the Provider</returns>
		protected abstract ProviderResponse DoCredit(MemberTran memberTran);

		/// <summary>
		/// Concrete Processor implementations must implement this
		/// method to provide Provider specific logic for purchasing.
		/// </summary>
		/// <param name="mt">The mt to Purchase</param>
		/// <param name="merchantID">The Unique Identifier for the merchant</param>
		/// <returns>A Populated Response from the Provider</returns>
		protected abstract ProviderResponse DoPurchase(MemberTran memberTran);

		public virtual ProviderResponse SetPayment(MemberTran memberTran) 
		{
			throw new Exception("Set Payment Not Supported");
		}

		/// <summary>
		/// A Utility object for containing validation properties.
		/// </summary>
		protected struct ValidateStatus 
		{
			public bool Valid;
			public string Message;
		}


		private string postData(string url, string data, string referer) 
		{
			return postData(url, data, referer, System.Threading.Timeout.Infinite);
		}


		private string postData(string url, string data, string referer, int timeout) 
		{
			try 
			{
				if(url.StartsWith("https")) 
				{
					//ignore invalid/expired SSL cert
					ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
				}
				
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				//request.UserAgent = "Matchnet.Lib.Util.HttpUtil";
				request.Method = "POST";

				//MPR-660 11132008 - Timeout override per provider based on configuration
				request.Timeout = (timeoutOverride != Matchnet.Constants.NULL_INT && timeoutOverride > 0) ? timeoutOverride : timeout;

				if (referer != null && referer.Trim().Length > 0) 
				{
					request.Referer = referer;
				}

				request.ContentType = "multipart/form-data";
				//request.ContentType = "application/x-www-form-urlencoded";
				request.ContentLength = data.Length;

				byte[] postData = System.Text.Encoding.UTF8.GetBytes(data);
				Stream requestStream = request.GetRequestStream();

				requestStream.Write(postData, 0, data.Length);
				requestStream.Close();

				WebResponse response = request.GetResponse();
				StreamReader reader = new StreamReader(response.GetResponseStream());
				
				string retVal = reader.ReadToEnd();
				try 
				{
					reader.Close();
					response.Close();
				} 
				catch {} 
				return retVal;
			} 
			catch (WebException webEx)
			{
				throw new ProviderNetworkException(webEx.Message);
			}
			catch (Exception ex) 
			{
				throw new Exception("Unable to post data.", ex);
			}
		}

		protected string sendXML(string url, string xmlDoc, string referer) 
		{
			try 
			{
				return postData(url, xmlDoc, referer);
			} 
			catch (Exception ex) 
			{
				string hostName = "";
				try 
				{
					hostName = Dns.GetHostName();
				} 
				catch
				{ 
					// ignore so we can still log the correct error 
				}
				throw new Exception("Unable to post data to Provider [URL: " + url + "]\r\nHost:" + hostName, ex);
			}
		}

		protected string sendData(string url, string data, string referer)
		{
			return sendXML(url, data, referer);
		}


		protected string getLogValue(Int32 memberID,
			Int32 memberTranID,
			string fieldName)
		{
			return getLogValue(memberID,
				memberTranID,
				fieldName,
				true);
		}
			

		protected string getLogValue(Int32 memberID,
			Int32 memberTranID,
			string fieldName,
			bool errorIfNotFound) 
		{
			string value = Matchnet.Constants.NULL_STRING;
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_ChargeLog_List", 0);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				command.AddParameter("@ProviderFieldName", SqlDbType.VarChar, ParameterDirection.Input, fieldName);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				dataReader = Client.Instance.ExecuteReader(command);

				while (dataReader.Read())
				{
					value = dataReader.GetString( dataReader.GetOrdinal("Value")).Trim();
				}
	
				if ( value == Matchnet.Constants.NULL_STRING || value.Length == 0 )
				{
					if (errorIfNotFound)
					{
						throw new Exception( "Unable to get original " + fieldName + " (memberTranID: " + memberTranID.ToString() + ").");
					}
				}
														 
				return value;
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}


		public Int32 MerchantID
		{
			set
			{
				_merchantID = value;
			}
		}


		public Int32 ProviderID
		{
			get
			{
				return _providerID;
			}
			set
			{
				_providerID = value;
			}
		}


		public class OpenCertificatePolicy : System.Net.ICertificatePolicy 
		{
			/// <summary>
			/// Returns true regardless of Https messages
			/// </summary>
			/// <param name="srvPoint">The ServicePoint that handles the connection</param>
			/// <param name="certificate">The certificate to validate</param>
			/// <param name="request">The request that received the certificate</param>
			/// <param name="certificateProblem">The problem encountered when using the certificate</param>
			/// <returns>true regardless of certificate problem</returns>
			public bool CheckValidationResult(ServicePoint srvPoint,
				X509Certificate certificate, WebRequest request, int
				certificateProblem) 
			{
				return true;
			}
		}
	}
}
