using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Messaging;
using System.Threading;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Text;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Security;
using Matchnet.Queuing;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.PGP1_1;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Payment.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;

namespace Matchnet.Payment.BusinessLogic
{
	public class PaymentBL
	{
		public const int GLOBAL_ATTRIBUTE_GROUP_ID = 8383;
		private const string QUEUEPATH_PAYMENT_INBOUND = @".\private$\Payment_Inbound";
		internal const string QUEUEPATH_PAYMENT_OUTBOUND = @".\private$\Payment_Outbound";
		internal const string QUEUEPATH_RESPONSE = @".\private$\Payment_Response";
		private const byte QUEUEPROCESSOR_THREAD_COUNT = 4;
		private const string PAYMENTATTRIBUTE_CREDITCARDNUMBER = "CreditCardNumber";
		private const string PAYMENTATTRIBUTE_CREDITCARDTYPE = "CreditCardType";
		private const string PAYMENTATTRIBUTE_CHECKINGACCOUNTNUMBER = "CheckingAccountNumber";
		private const string PAYMENTATTRIBUTE_BANKROUTINGNUMBER = "BankRoutingNumber";
		private const string PAYMENTATTRIBUTE_DRIVERSLICENSENUMBER = "DriversLicenseNumber";
		private const string PAYMENTATTRIBUTE_ISRAELIID = "IsraeliID";

		private Matchnet.Queuing.Processor _queueProcessorPaymentInbound;
		private static Hashtable _paymentProcessors = new Hashtable();
		private static ReaderWriterLock _paymentProcessorsLock = new ReaderWriterLock();
		private Matchnet.Queuing.Processor _queueProcessorResponse;
		private Matchnet.Queuing.Processor _queueProcessorPaymentOutbound;
		
		public PaymentBL()
		{
			//calling getqueue here will create the queues if they do not exist
			Matchnet.Queuing.Util.GetQueue(QUEUEPATH_PAYMENT_INBOUND, true, true);
			Matchnet.Queuing.Util.GetQueue(QUEUEPATH_RESPONSE, true, true);
			Matchnet.Queuing.Util.GetQueue(QUEUEPATH_PAYMENT_OUTBOUND, true, true);

			_queueProcessorPaymentInbound = new Processor(QUEUEPATH_PAYMENT_INBOUND,
				QUEUEPROCESSOR_THREAD_COUNT,
				true,
				new ProcessorWorkerInbound(),
				Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME);

			_queueProcessorResponse = new Processor(QUEUEPATH_RESPONSE,
				QUEUEPROCESSOR_THREAD_COUNT,
				true,
				new ProcessorWorkerResponse(),
				Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME);

			_queueProcessorPaymentOutbound = new Processor(QUEUEPATH_PAYMENT_OUTBOUND,
				QUEUEPROCESSOR_THREAD_COUNT,
				true,
				new ProcessorWorkerOutbound(),
				Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME);
		}


		public void Start()
		{
			_queueProcessorPaymentInbound.Start();
			_queueProcessorResponse.Start();
			_queueProcessorPaymentOutbound.Start();

			StartUnprocessPayment();

			
		}

		/// <summary>
		/// scan for unprocessed payments & start providers
		/// </summary>
		private void StartUnprocessPayment()
		{
			string path = string.Empty;
			try
			{
				MessageQueue[] queues = MessageQueue.GetPrivateQueuesByMachine(System.Environment.MachineName);
				for (Int32 queueNum = 0; queueNum < queues.Length; queueNum++)
				{
					path = queues[queueNum].Path;
					if (path.IndexOf(@"private$\payment_") > -1 && path.IndexOf("response") == -1)
					{
						if (queues[queueNum].GetEnumerator().MoveNext())	
						{
							//System.Diagnostics.Trace.WriteLine("__" + path.Substring(path.LastIndexOf("\\") + 1));
							GetSiteMerchantProcessor(path.Substring(path.LastIndexOf("\\") + 1));
						}
					}
					// reassign to empty so don't get previous queue path for exception message
					path = string.Empty;
				}
			}
			catch(Exception ex)
			{
				string errorMessage = this.ToString() + ".StartUnprocessPayment() - " + ex.Message;
				errorMessage = errorMessage + " QueuePath: " + path;
				throw new ApplicationException(errorMessage);
			}
		}


		public void Stop()
		{
			_queueProcessorPaymentOutbound.Stop();
			_queueProcessorResponse.Stop();
			//_queueProcessorPaymentOutbound.Stop();
			_queueProcessorPaymentInbound.Stop();

			_paymentProcessorsLock.AcquireReaderLock(-1);
			try
			{
				IDictionaryEnumerator de = _paymentProcessors.GetEnumerator();
				while (de.MoveNext())
				{
					Processor p = de.Value as Processor;
					p.Stop();
				}

				de.Reset();
				while (de.MoveNext())
				{
					Processor p = de.Value as Processor;
					p.Join();
				}
			}
			finally
			{
				_paymentProcessorsLock.ReleaseLock();
			}

			_queueProcessorPaymentOutbound.Join();
			_queueProcessorResponse.Join();
			//_queueProcessorPaymentOutbound.Join();
			_queueProcessorPaymentInbound.Join();
		}


		public void UpdateMemberPayment(Int32 memberPaymentID,
			Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType)
		{
			Command command = new Command("mnCharge", "dbo.up_MemberPayment_Save_CreditCard", 0);
			command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, firstName);
			command.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, lastName);
			command.AddParameter("@Phone", SqlDbType.NVarChar, ParameterDirection.Input, phone);
			command.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, addressLine1);
			command.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, city);
			command.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, state);
			command.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, countryRegionID);
			command.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, postalCode);
			command.AddParameter("@CreditCardNumber", SqlDbType.NVarChar, ParameterDirection.Input, Matchnet.Security.Crypto.Encrypt(RuntimeSettings.GetSetting("KEY"), creditCardNumber));
			command.AddParameter("@CreditCardExpirationMonth", SqlDbType.Int, ParameterDirection.Input, expirationMonth);
			command.AddParameter("@CreditCardExpirationYear", SqlDbType.Int, ParameterDirection.Input, expirationYear);
			command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (int)creditCardType);

			SyncWriter sw = new SyncWriter();
			Exception ex;
			sw.Execute(command, null, out ex);
			sw.Dispose();

			if (ex != null)
			{
				throw ex;
			}
		}

		
		public Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_Charge_Success_ListPayment", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
				dataReader = Client.Instance.ExecuteReader(command);
				
				bool colLookupDone = false;
				Int32 ordinalMemberPaymentID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalMemberPaymentID = dataReader.GetOrdinal("MemberPaymentID");
						colLookupDone = true;
					}

					return dataReader.GetInt32(ordinalMemberPaymentID);

				}

				return Constants.NULL_INT;
			}
			catch (Exception ex)
			{
				throw (new BLException("BL error occurred while retrieving member payment ID.", ex));
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		internal static string GetSiteMerchantProcessor(Int32 siteID,
			Int32 merchantID)
		{
			return GetSiteMerchantProcessor("Payment_" + siteID.ToString() + "_" + merchantID.ToString());
		}

		/// <summary>
		/// This method not only return the Queue Path, but also starts a new processing thread to process the Queue.
		/// </summary>
		/// <param name="queueName"></param>
		/// <returns></returns>
		internal static string GetSiteMerchantProcessor(string queueName)
		{
			queueName = queueName.ToLower();
			string queuePath = @".\private$\" + queueName;

			_paymentProcessorsLock.AcquireReaderLock(-1);
			try
			{
				Processor p = _paymentProcessors[queueName] as Processor;

				if (p == null)
				{
					_paymentProcessorsLock.UpgradeToWriterLock(-1);
					p = _paymentProcessors[queueName] as Processor;

					if (p == null)
					{
						MessageQueue queue = Matchnet.Queuing.Util.GetQueue(queuePath, true, true);
						p = new Processor(queuePath,
							QUEUEPROCESSOR_THREAD_COUNT,
							true,
							new ProcessorWorkerProvider(),
							Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME);
						p.Start();
						_paymentProcessors.Add(queueName, p);
					}
				}
			}
			finally
			{
				_paymentProcessorsLock.ReleaseLock();
			}

			return queuePath;
		}


		public void ProcessPayment(MemberTran memberTran)
		{
			try
			{
				//hack to use ReferenceMemberTranID as reference MemberPaymentID for updating CC payment information
				if(memberTran.TranType == Purchase.ValueObjects.TranType.AuthPmtProfile
					&& memberTran.AccountNumber == null)
				{
					NameValueCollection nvc = GetMemberPayment(memberTran.MemberID,
						BrandConfigSA.Instance.GetBrands().GetCommunityID(memberTran.SiteID),
						memberTran.ReferenceMemberTranID,true);
					
					memberTran.AccountNumber = nvc[PAYMENTATTRIBUTE_CREDITCARDNUMBER];
					CreditCardPayment creditCardPayment = (CreditCardPayment)memberTran.Payment;
					creditCardPayment.CreditCardNumber = nvc[PAYMENTATTRIBUTE_CREDITCARDNUMBER];
					creditCardPayment.IsraeliID = nvc[PAYMENTATTRIBUTE_ISRAELIID];
					
					memberTran.Payment = creditCardPayment;

					
				}
					//hack, add in cc# and do not require cvc if this is a sub open with existing mbr payment
				else if (memberTran.AccountNumber == null 
					&& memberTran.PaymentType == PaymentType.CreditCard)
				{
					NameValueCollection nvc = GetMemberPayment(memberTran.MemberID,
						BrandConfigSA.Instance.GetBrands().GetCommunityID(memberTran.SiteID),
						memberTran.MemberPaymentID,
						true);

					memberTran.AccountNumber = nvc[PAYMENTATTRIBUTE_CREDITCARDNUMBER];
					memberTran.IgnoreCVV = true;
				}
				else if(memberTran.ReusePreviousPayment)
				{

					MemberSub ms = PurchaseSA.Instance.GetSubscription(memberTran.MemberID, memberTran.SiteID);

					NameValueCollection nvc = GetMemberPayment(memberTran.MemberID,
						BrandConfigSA.Instance.GetBrands().GetCommunityID(memberTran.SiteID),
						ms.MemberPaymentID,
						true);

					if(memberTran.PaymentType == PaymentType.CreditCard)
					{
						memberTran.AccountNumber = nvc[PAYMENTATTRIBUTE_CREDITCARDNUMBER];
						memberTran.IgnoreCVV = true;

						CreditCardPayment creditCardPayment = (CreditCardPayment)memberTran.Payment;
						creditCardPayment.CreditCardNumber = nvc[PAYMENTATTRIBUTE_CREDITCARDNUMBER];
						if(nvc[PAYMENTATTRIBUTE_ISRAELIID] != null)
						{
							creditCardPayment.IsraeliID = nvc[PAYMENTATTRIBUTE_ISRAELIID];
						}
					}
					else if(memberTran.PaymentType == PaymentType.Check)
					{
						memberTran.AccountNumber = nvc[PAYMENTATTRIBUTE_CHECKINGACCOUNTNUMBER];
						memberTran.IgnoreCVV = true;
						
						CheckPayment checkPayment = (CheckPayment) memberTran.Payment;
						checkPayment.CheckingAccountNumber = nvc[PAYMENTATTRIBUTE_CHECKINGACCOUNTNUMBER];
						checkPayment.BankRoutingNumber = nvc[PAYMENTATTRIBUTE_BANKROUTINGNUMBER];
						checkPayment.DriversLicenseNumber = nvc[PAYMENTATTRIBUTE_DRIVERSLICENSENUMBER];
					}

				}

				MessageQueue queue = Matchnet.Queuing.Util.GetQueue(QUEUEPATH_PAYMENT_INBOUND, true, true);
				queue.Send(memberTran, MessageQueueTransactionType.Single);
			}
			catch (Exception ex)
			{
				throw new BLException("ProcessPayment() error (memberTranID: " + memberTran.MemberTranID.ToString() + ").", ex);
			}
		}


		public static NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID)
		{
			return GetMemberPayment(memberID, 
				communityID, 
				memberPaymentID,
				false);
		}
		
	
		public static MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID)
		{
			SqlDataReader dataReader = null;
			MemberTranStatus status = MemberTranStatus.None;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_Charge_List_Status", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalChargeStatus = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalChargeStatus = dataReader.GetOrdinal("ChargeStatus");
						colLookupDone = true;
					}

					status = (MemberTranStatus)Enum.Parse(typeof(MemberTranStatus), dataReader.GetInt32(ordinalChargeStatus).ToString());
				}

				return status;
			}
			catch (Exception ex)
			{
				throw new BLException("GetChargeStatus() error (memberID: " + memberID.ToString()  + ", memberTranID: " + memberTranID.ToString() + ").", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		public static NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID,
			bool isTrusted)
		{
			SqlDataReader dataReader = null;
			string cardNumber = null;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_MemberPayment_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
				command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, GLOBAL_ATTRIBUTE_GROUP_ID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalAttributeName = Constants.NULL_INT;
				Int32 ordinalValue = Constants.NULL_INT;
				Int32 ordinalEncryptFlag = Constants.NULL_INT;

				NameValueCollection nvc = new NameValueCollection();

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalAttributeName = dataReader.GetOrdinal("AttributeName");
						ordinalValue = dataReader.GetOrdinal("Value");
						ordinalEncryptFlag = dataReader.GetOrdinal("EncryptFlag");
						colLookupDone = true;
					}

					string name = dataReader.GetString(ordinalAttributeName);
					string value = dataReader.GetString(ordinalValue);

					if (value.Trim().Length > 0)
					{
						if (dataReader.GetBoolean(ordinalEncryptFlag))
						{
							try
							{
								value = Matchnet.Security.Crypto.Decrypt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("KEY"), value);
							}
							catch(Exception ex)
							{
								string message = ex.Message + " (memberID: " + memberID.ToString() + ", communityID: " + communityID.ToString() + ", memberPaymentID: " + memberPaymentID.ToString() + ").";
								EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
								value = "";
							}
							
							if (name == PAYMENTATTRIBUTE_CREDITCARDNUMBER)
							{
								cardNumber = value;
							}
							if (!isTrusted && value.Length > 0)
							{
								value = maskValue(value);
							}
						}
						
						nvc.Add(name, value);
					}
				}

				if (!colLookupDone)
				{
					string message = "MemberPayment not found (memberID: " + memberID.ToString() + ", communityID: " + communityID.ToString() + ", memberPaymentID: " + memberPaymentID.ToString() + ").";
					EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
				}
				//hack: determine/add card type if we do not have it
				else if (cardNumber != null && nvc[PAYMENTATTRIBUTE_CREDITCARDTYPE] == null)
				{
					switch(Convert.ToInt16(cardNumber.ToString().Trim().Substring(0,1)))
					{
						case 3:
						switch(Convert.ToInt16(cardNumber.ToString().Trim().Substring(0,2)))
						{
							case 34:
							case 37:
								nvc.Add(PAYMENTATTRIBUTE_CREDITCARDTYPE, ((Int32)Matchnet.Purchase.ValueObjects.CreditCardType.AmericanExpress).ToString());
								break;
							case 36:
							case 38:
								nvc.Add(PAYMENTATTRIBUTE_CREDITCARDTYPE, ((Int32)Matchnet.Purchase.ValueObjects.CreditCardType.DinersClub).ToString());
								break;
						}
							break;
						case 4:
							nvc.Add(PAYMENTATTRIBUTE_CREDITCARDTYPE, ((Int32)Matchnet.Purchase.ValueObjects.CreditCardType.Visa).ToString());
							break;
						case 5:
							nvc.Add(PAYMENTATTRIBUTE_CREDITCARDTYPE, ((Int32)Matchnet.Purchase.ValueObjects.CreditCardType.MasterCard).ToString());
							break;
						case 6:
							nvc.Add(PAYMENTATTRIBUTE_CREDITCARDTYPE, ((Int32)Matchnet.Purchase.ValueObjects.CreditCardType.Discover).ToString());
							break;
					}
				}

				return nvc;
			}
			catch (Exception ex)
			{
				string errorInfo = "memberID: " + memberID.ToString() + " communityID: " + communityID.ToString() + " memberPaymentID: " + memberPaymentID.ToString();
				errorInfo = errorInfo + " BL error occurred while retrieving payment data.";
			
				throw new BLException(errorInfo, ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}


		public static NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID)
		{
			return GetMemberPaymentByMemberTranID(memberID,
				memberTranID,
				false);
		}
			
			
		public static NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID,
			bool isTrusted)
		{
			SqlDataReader dataReader = null;
			try
			{
				Command command = new Command("mnCharge", "dbo.up_MemberPayment_List_ByMemberTranID", 0);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, GLOBAL_ATTRIBUTE_GROUP_ID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalAttributeName = Constants.NULL_INT;
				Int32 ordinalValue = Constants.NULL_INT;
				Int32 ordinalEncryptFlag = Constants.NULL_INT;

				NameValueCollection nvc = new NameValueCollection();

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalAttributeName = dataReader.GetOrdinal("AttributeName");
						ordinalValue = dataReader.GetOrdinal("Value");
						ordinalEncryptFlag = dataReader.GetOrdinal("EncryptFlag");
						colLookupDone = true;
					}

					string value = dataReader.GetString(ordinalValue);

					if (value.Trim().Length > 0)
					{
						if (dataReader.GetBoolean(ordinalEncryptFlag))
						{
							try
							{
								value = Matchnet.Security.Crypto.Decrypt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("KEY"), value);
							}
							catch(Exception ex)
							{
								string message = ex.Message + " (memberID: " + memberID.ToString() + " memberTranID: " + memberTranID.ToString() + ").";
								EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
								value = "";
							}

							if (!isTrusted && value.Length > 0)
							{
								value = maskValue(value);
							}
						}
						
						nvc.Add(dataReader.GetString(ordinalAttributeName), value);
					}
				}

				if (!colLookupDone)
				{
					string message = "MemberPayment not found (memberID: " + memberID.ToString() + ", memberTranID: " + memberTranID.ToString() + ").";
					EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
				}

				return nvc;
			}
			catch (Exception ex)
			{
				throw (new BLException("BL error occurred while retrieving payment data.", ex));
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		public static MemberPaymentInfo GetMemberPaymentInfo(Int32 memberID,
			Int32 memberPaymentID)
		{
			return GetMemberPaymentInfo(memberID,
				memberPaymentID,
				false);
		}


		public static MemberPaymentInfo GetMemberPaymentInfo(Int32 memberID,
			Int32 memberPaymentID,
			bool isTrusted)
		{
			SqlDataReader dataReader = null;

			try
			{
				MemberPaymentInfo mpi = null;

				Command command = new Command("mnCharge", "dbo.up_MemberPayment_List_Info", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalAccountNumber = Constants.NULL_INT;
				Int32 ordinalPaymentTypeID = Constants.NULL_INT;
				Int32 ordinalPaymentTypeDescription = Constants.NULL_INT;
				Int32 ordinalPaymentTypeResourceConstant = Constants.NULL_INT;
				Int32 ordinalGroupID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalAccountNumber = dataReader.GetOrdinal("AccountNumber");
						ordinalPaymentTypeID = dataReader.GetOrdinal("PaymentTypeID");
						ordinalPaymentTypeDescription = dataReader.GetOrdinal("PaymentTypeDescription");
						ordinalPaymentTypeResourceConstant = dataReader.GetOrdinal("PaymentTypeResourceConstant");
						ordinalGroupID = dataReader.GetOrdinal("GroupID");
						colLookupDone = true;
					}

					string accountNumber = Constants.NULL_STRING;
					if (!dataReader.IsDBNull(ordinalAccountNumber))
					{
						accountNumber = dataReader.GetString(ordinalAccountNumber);
						try
						{
							accountNumber = Matchnet.Security.Crypto.Decrypt( Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("KEY"), accountNumber);
						}
						catch(Exception ex)
						{
							string message = ex.Message + " (memberID: " + memberID.ToString() + ", memberPaymentID: " + memberPaymentID.ToString() + ").";
							EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
							accountNumber = "";
						}
						if (!isTrusted && accountNumber.Length > 0)
						{
							accountNumber = maskValue(accountNumber);
						}
					}

					mpi = new MemberPaymentInfo(memberID,
						memberPaymentID,
						dataReader.GetInt32(ordinalPaymentTypeID),
						dataReader.GetString(ordinalPaymentTypeDescription), 
						dataReader.GetString(ordinalPaymentTypeResourceConstant),
						accountNumber,
						dataReader.GetInt32(ordinalGroupID));
				}

				return mpi;
			}
			catch(Exception ex)
			{
				throw new BLException("BL error occurred while getting member payment info.", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		
		public static NameValueCollection GetMemberTranLogByMerchantID(Int32 memberTranID,
			Int32 merchantID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_ChargeLog_List_ByMerchantID", 0);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				command.AddParameter("@MerchantID", SqlDbType.Int, ParameterDirection.Input, merchantID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalName = Constants.NULL_INT;
				Int32 ordinalValue = Constants.NULL_INT;

				NameValueCollection nvc = null;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalName = dataReader.GetOrdinal("Name");
						ordinalValue = dataReader.GetOrdinal("Value");
						colLookupDone = true;
						nvc = new NameValueCollection();
					}

					string name = dataReader.GetString(ordinalName);
					string value = dataReader.GetString(ordinalValue);
					value = value.Replace( "&", "&amp;" );

					if ( value.ToString().Trim().Length > 0 )
					{
						nvc.Add( name, value);
					}
				}

				if (nvc == null)
				{
					throw new Exception("MemberTran not found.");
				}

				return nvc;
			}
			catch(Exception ex)
			{
				throw (new BLException("BL error occurred getting charge log for MemberTranID " + memberTranID.ToString(), ex) );
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}


		public NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID)
		{
			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_ChargeLog_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalName = Constants.NULL_INT;
				Int32 ordinalValue = Constants.NULL_INT;

				NameValueCollection nvc = null;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalName = dataReader.GetOrdinal("Name");
						ordinalValue = dataReader.GetOrdinal("Value");
						colLookupDone = true;
						nvc = new NameValueCollection();
					}

					string name = dataReader.GetString(ordinalName);
					string value = dataReader.GetString(ordinalValue);
					value = value.Replace( "&", "&amp;" );

					if ( value.ToString().Trim().Length > 0 )
					{
						nvc.Add( name, value);
					}
				}

				return nvc;
			}
			catch(Exception ex)
			{
				throw new BLException("GetChargeLog() error (memberID: " + memberID.ToString() + ", memberTranID: " + memberTranID.ToString() + ")", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
		}

		#region Encore Credit Sales File Generation
		/// <summary>
		/// This method is used to process a matchback file from Encore. The file entries should be in mnFileExchange.Matchback before
		/// this method is called. This method is responsible for 3 things:
		/// -Validate the ClientOrderID versus the email address. ClientOrderID will be a new member attribute that we set on our side
		/// prior to sending the user to the Encore site.
		/// -Retrieve credit card data if they are valid
		/// -Generate an encrypted sales file
		/// </summary>
		public int GenerateEncryptedSalesFile()
		{
			try	
			{
				Fields fileMap = LoadCreditCardSalesFileMap();
				DataTable ccSalesRecords = CreateCreditCardSalesDataTable(fileMap);
				
				MatchbackCollection mbCol = PurchaseSA.Instance.GetMatbackRequest();

				#region Prepare the Sales Records into a DataTable
				foreach(Matchback mb in mbCol)
				{
					int memberID = Constants.NULL_INT;
					int brandID;

					// Skip records that do not have the valid ClientOrderID
					if(IsClientOrderIDValid(mb.ClientOrderID, mb.Email, out memberID, out brandID))
					{
						Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

						// We need to record EncoreMatchbackDate.
						Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, 
							Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
						member.SetAttributeDate(brand, "EncoreMatchbackDate", DateTime.Now);
						Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);

						// Go get the last valid CC payment
						int successPmtID = GetSuccessfulPayment(memberID, brand.Site.SiteID);

						// Technically, people who made it to the matchback file should all have valid cc info but if it fails for some reason,
						// skip this record
						if(successPmtID != Constants.NULL_INT)
						{
							// Using the paymentID for the last valid CC payment, get the payment info for this user
							NameValueCollection nvCol = GetMemberPayment(memberID, brand.Site.Community.CommunityID, successPmtID, true);
							DataRow ccSalesRec = ccSalesRecords.NewRow();

							// RE: Since Sale Rep From Encore: In the sales file, for Sales Rep, as these are Online sales, please leave this field blank
							// Sale Date is the data from the matchback file reformatted
							ccSalesRec["SaleDate"] = FormatSaleDate(mb.OrderDate);

							// Mail code here
							ccSalesRec["MailCode"] = mb.MailCode;

							// Handle the fields that have DB mapping
							foreach(Field field in fileMap)
							{
								if(field.DBName != string.Empty)
								{
									ccSalesRec[field.Name] = nvCol[field.DBName];
								}
							}
						
							// Reformat the credit card type
							if(!(ccSalesRec["CCType"] is DBNull))
							{
								ccSalesRec["CCType"] = GetEncoreCCTypeString(ccSalesRec["CCType"].ToString());
							}
							
							// Reformat the phone number string
							if(!(ccSalesRec["PhoneNumber"] is DBNull))
							{
								// Format the phone string
								ccSalesRec["PhoneNumber"] = FormatPhoneString(ccSalesRec["PhoneNumber"].ToString());
							}

							// Handle the cc expiration date (mmyy)
							ccSalesRec["CCExpirationDate"] = PrepareEncoreCCExpirationDate(nvCol["CreditCardExpirationMonth"], nvCol["CreditCardExpirationYear"]);

							// Email address
							ccSalesRec["Email"] = mb.Email;
							
							// Vendor variable field is: memberID_brandID. We need siteID for the membership file processing.                        					
							ccSalesRec["VariableField"] = memberID + "_" + brand.BrandID.ToString();
							//*********************************

							// If CCType is string.empty, this means user has a CC type that Encore does not support
							// do not include this user
							if(!(ccSalesRec["CCType"] is DBNull) && ccSalesRec["CCType"] != string.Empty)
							{
								ccSalesRecords.Rows.Add(ccSalesRec);
							}
						}
					}
					else
					{
						string message = string.Format("ClientOrderID sent from Encore does not match member's attribute value - MemberID: {0}, Email: {1}, ClientOrderID: {2}",
							memberID.ToString(), mb.Email, mb.ClientOrderID);
								
						EventLog.WriteEntry(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
					}
				}
				#endregion

				if(ccSalesRecords.Rows.Count > 0)
				{
					string outputFileName = GetOutputCreditSalesFileName();

					int lineID = PurchaseSA.Instance.StartMatchbackFileGeneration(outputFileName);

                    // Start the file generation process
					string outputFilePath = OutputCreditCardSalesFile(fileMap, ccSalesRecords, outputFileName);

					// Put the file into the DB
					PurchaseSA.Instance.InsertMatchbackFile(ConvertToByteArray(outputFilePath), lineID);

					// DELETE THE OUTPUT FILE
					File.Delete(outputFilePath);
					
				}

				// Update the matchback records as processed
				PurchaseSA.Instance.MarkMatchbackRequestAsComplete(mbCol);
				
				return ccSalesRecords.Rows.Count;
			}
			catch(Exception ex)
			{
				throw new BLException("GenerateEncryptedSalesFile() error", ex);
			}

		}
		/// <summary>
		/// This method checks the member attribute used for recording ClientOrderID against the member's email address.
		/// </summary>
		/// <param name="clientOrderID"></param>
		/// <param name="memberEmailAddress"></param>
		/// <param name="memberID"'></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		private bool IsClientOrderIDValid(string clientOrderID, string memberEmailAddress, out int memberID, out int brandID)
		{			
			brandID = Constants.NULL_INT;
			memberID = Constants.NULL_INT;
			string memberAttrValue = null;
			bool ret = false;

			// don't even bother if this is a bad row. skip to the next record
			if(!(clientOrderID == null || memberEmailAddress == null))
			{
				// bad ClientOrderID format. skip this record
				if(clientOrderID.Length == 15)
				{
					memberID = Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByEmail(memberEmailAddress);

					if(memberID != Constants.NULL_INT)
					{
						// get the brandId from the ClientOrderID
						string brandIdString = clientOrderID.Substring(10, 5);
						if(brandIdString[0] == '0')
							brandIdString = brandIdString.Substring(1,4);
												
						if(IntTryParse(brandIdString, out brandID))
						{
							Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
							memberAttrValue = member.GetAttributeText(BrandConfigSA.Instance.GetBrandByID(brandID), "EncoreClientOrderID", string.Empty);
							ret = memberAttrValue.ToUpper() == clientOrderID.ToUpper();
						}
					}
				}
			}

			return ret;
		}
		private string FormatSaleDate(string matchbackString)
		{
			string ret = string.Empty;
			string trimmedString = matchbackString.Trim();

			if(trimmedString.Length == 8)
			{
				string year = trimmedString.Substring(2, 2);
				string month = trimmedString.Substring(4, 2);
				string day = trimmedString.Substring(6, 2);			

				ret = month + day + year;
			}

			return ret;
		}

		private string GetEncoreCCTypeString(string ccTypeNumber)
		{
			string ccTypeNum = ccTypeNumber.Trim();
			string encoreCCType = string.Empty;

			switch(ccTypeNum)
			{
				case "64":
					encoreCCType = "V";
					break;
				case "32":
					encoreCCType = "M";
					break;
				case "8":
					encoreCCType = "Q";
					break;
				case "1":
					encoreCCType = "A";
					break;
			}

            return encoreCCType;
		}

		private string FormatPhoneString(string phoneString)
		{
			StringBuilder sb = new StringBuilder();

			for(int i=0; i < phoneString.Length; i++)
			{
				if(char.IsNumber(phoneString[i]))
					sb.Append(phoneString[i]);
			}

			return sb.ToString();
		}

		private byte[] ConvertToByteArray(string filePath)
		{
			byte[] ret;

			using(FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
			{
				using(BinaryReader br = new BinaryReader(fs))
				{
					ret = br.ReadBytes((int)fs.Length);
				}
			}

			return ret;
		}
		private string OutputCreditCardSalesFile(Fields fileMap, DataTable data, string outputFileName)
		{
			string publicKeyFile = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_PUBLIC_KEY_PATH");
			string workDir = System.AppDomain.CurrentDomain.BaseDirectory + "EncoreWorkDir\\";
			string sourceFile =  workDir + outputFileName;
			string outputFile = workDir + outputFileName + ".PGP";

			// Make sure the directory exists
			if(!Directory.Exists(workDir))
			{
				Directory.CreateDirectory(workDir);
			}

			using(StreamWriter writer =new StreamWriter(sourceFile, false, Encoding.ASCII))
			{				
				foreach(DataRow aRow in data.Rows)
				{
					StringBuilder sb = new StringBuilder();
					
					foreach(DataColumn aColumn in data.Columns)
					{
						sb.Append(MakeFixedLength(aRow[aColumn.ColumnName], GetFieldLength(fileMap, aColumn.ColumnName)));
					}
					
					writer.WriteLine(sb.ToString());
				}
			}

			// Encrypt the file
			PGP1_1.Crypto.Instance.Encrypt(sourceFile, publicKeyFile, outputFile);

            // Delete the source file
			File.Delete(sourceFile);

            return outputFile;            
		}

		private string GetOutputCreditSalesFileName()
		{
			StringBuilder sb = new StringBuilder();
			string companyString = "SPAR";
						
			if(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENCORE_OUTPUT_FILE_MODE").ToLower() == "prod")
			{
				sb.Append(companyString + "WEB");
				
				DateTime now = DateTime.Now;				
                
				// xxxxWEBCCYYMMDDHHMMss is the file name format
				sb.Append(now.Year.ToString() + PrependZero(now.Month.ToString(), 2) + PrependZero(now.Day.ToString(), 2)
					+ PrependZero(now.Hour.ToString(), 2) + PrependZero(now.Minute.ToString(), 2) + PrependZero(now.Second.ToString(), 2));
			}
			else
			{
				// xxxxWEBTEST## is the file name format for testing
				// We could have an overlap of ## but let's just do random function
				Random random = new Random();
				int number = random.Next(99);
				sb.Append(companyString + "WEBTEST" + PrependZero(number.ToString(), 2));
			}

			sb.Append(".DAT");

			return sb.ToString();
		}

		private string MakeFixedLength(object aValue, int length)
		{
			StringBuilder sb = new StringBuilder();
			int diff = 0;

			if(aValue is DBNull)
			{
				diff = length;
			}
			else
			{
				if(aValue.ToString().Length > length)
				{
					sb.Append(aValue.ToString().Substring(0, length));
				}
				else
				{
					sb.Append(aValue.ToString());
				}

				diff = length - aValue.ToString().Length;
			}

			for (int i = 0; i < diff; i++)
			{
				sb.Append(" ");
			}

			return sb.ToString().ToUpper();
		}

		private int GetFieldLength(Fields fileMap, string fieldName)
		{
			Field field = fileMap.FindByName(fieldName);

			return field.Length;
		}

		private string PrepareEncoreCCExpirationDate(string month, string year)
		{
			string ret;			

			ret = PrependZero(month, 2);

            ret += year.Substring(2, 2);

			return ret;
		}

		private DataTable CreateCreditCardSalesDataTable(Fields fileMap)
		{
			DataTable dt = new DataTable();			

			foreach(Field field in fileMap)
			{
				dt.Columns.Add(new DataColumn(field.Name, Type.GetType("System.String")));
			}

			return dt;
		}

		private Fields LoadCreditCardSalesFileMap()
		{
			Fields fields = new Fields();
			XmlDocument map = new XmlDocument();		
		
            string encoreTemplateDir = System.AppDomain.CurrentDomain.BaseDirectory + "EncoreTemplates\\";
			string ccSalesFileTemplate = encoreTemplateDir + "CreditCardSales.xml";

			using (StreamReader reader = new StreamReader(ccSalesFileTemplate))
			{
				map.Load(reader);
			}

			XmlNodeList fieldNodes = map.SelectNodes("/FileMap/Field");	
			foreach(XmlNode fieldNode in fieldNodes)
			{
				Field field = new Field();

				field.Name = fieldNode.Attributes["Name"].Value;
				field.DBName = fieldNode.Attributes["DBName"].Value;
				field.Length = Convert.ToInt32(fieldNode.Attributes["Length"].Value);
				field.Start = Convert.ToInt32(fieldNode.Attributes["Start"].Value);

				fields.Add(field);
			}

			return fields;
		}

		#endregion

		private string PrependZero(string valueString, int supposeLength)
		{
			int diff = supposeLength - valueString.Length;
			StringBuilder sb = new StringBuilder();

			for(int i=0; i < diff; i++)
			{
				sb.Append("0");
			}
			sb.Append(valueString);

			return sb.ToString();			
		}

		private bool IntTryParse(string valueString, out int outValue)
		{
			outValue = 0;
			bool isSuccess = true;

			try
			{
				outValue = int.Parse(valueString);
			}
			catch(FormatException)
			{
				isSuccess = false;
			}
			catch(OverflowException)
			{
				isSuccess = false;
			}

			return isSuccess;
		}
		
		private static string maskValue(string value)
		{
			Int32 len = value.Length;
			if (len >= 8)
			{
				value = value.Substring(value.Length - 4, 4);
				for (Int32 i = 0; i < len - 4; i++)
				{
					value = "*" + value;
				}
			}
			else
			{
				value = "";
				for (Int32 i = 0; i < len; i++)
				{
					value = value + "*";
				}
			}

			return value;
		}
	}
}
