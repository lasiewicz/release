using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Queuing;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.BusinessLogic
{
	public class ProcessorWorkerOutbound : IProcessorWorker
	{
		public void ProcessMessage(object messageBody)
		{
			MemberTranResult memberTranResult = messageBody as MemberTranResult;
			
			// set to default process error
			if(null == memberTranResult.ResponseCode)
			{
				//09292008 TL MPR-538 Null Response Code means Litle exception, we need to log information to mnChargeStage on Purchase service;
				//Added ResponseData, RequestData, logRequestResponse boolean
				bool logRequestResponse = memberTranResult.logRequestResponseData;
				memberTranResult = new MemberTranResult(memberTranResult.MemberTran
														,memberTranResult.ResourceConstant
														,memberTranResult.MemberTranStatus
														,Matchnet.Constants.NULL_INT.ToString()
														,Matchnet.Constants.NULL_INT.ToString()
														,Matchnet.Constants.NULL_INT.ToString()
														,memberTranResult.ProviderID
														,memberTranResult.ResponseData
														,memberTranResult.RequestDataScrubbed);

				
				memberTranResult.logRequestResponseData = logRequestResponse;
			}

			if (memberTranResult == null)
			{
				throw new Exception("Message body was not a MemberTranResult (ProcessorWorkerOutbound, type: " + messageBody.GetType().ToString() + ").");
			}

			PurchaseSA.Instance.SaveComplete(memberTranResult);
		}
	}
}
