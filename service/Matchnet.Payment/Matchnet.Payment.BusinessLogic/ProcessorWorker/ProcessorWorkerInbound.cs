using System;
using System.Data;
using System.Messaging;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Security;
using Matchnet.Queuing;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.Payment.BusinessLogic
{
	public class ProcessorWorkerInbound : IProcessorWorker
	{
		public void ProcessMessage(object messageBody)
		{
			MemberTran memberTran = messageBody as MemberTran;
			Command command = null;

			if (memberTran == null)
			{
				throw new Exception("Message body was not a MemberTran (ProcessorWorkerInbound, type: " + messageBody.GetType().ToString() + ").");
			}

			if (memberTran.SavePayment)
			{
				switch (memberTran.PaymentType)
				{
					case PaymentType.CreditCard:
						command = saveCreditCard(memberTran.ChargeID,
							memberTran.MemberID,
							memberTran.MemberTranID,
							memberTran.MemberPaymentID,
							memberTran.SiteID,
							memberTran.TranType,
							memberTran.Amount,
							(CreditCardPayment)memberTran.Payment);
						break;

					case PaymentType.Check:
						command = saveCheck(memberTran.ChargeID,
							memberTran.MemberID,
							memberTran.MemberTranID,
							memberTran.MemberPaymentID,
							memberTran.SiteID,
							memberTran.TranType,
							memberTran.Amount,
							(CheckPayment)memberTran.Payment);
						break;

					default:
						throw new Exception( "PaymentType not defined for MemberTranID " + memberTran.MemberTranID.ToString() );
				}
			}
			else
			{
				command = new Command("mnCharge", "dbo.up_Charge_Save", 0);
				command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, memberTran.ChargeID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberID);
				command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberTranID);
				command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberTran.MemberPaymentID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteID);
				command.AddParameter("@ChargeTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.TranType);
				command.AddParameter("@ChargeAmount", SqlDbType.Money, ParameterDirection.Input, memberTran.Amount);
				command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
			}

			SyncWriter sw = new SyncWriter();
			Exception ex;
			sw.Execute(command, null, out ex);
			sw.Dispose();

			if (ex != null)
			{
				throw ex;
			}

			MessageQueue queue = new MessageQueue(PaymentBL.GetSiteMerchantProcessor(memberTran.SiteID, memberTran.SiteMerchant.MerchantID));

			System.Messaging.Message message = new Message(memberTran, new BinaryMessageFormatter());
			if (memberTran.TranType != TranType.Renewal)
			{
				message.Priority = MessagePriority.Normal;
			}
			else
			{
				message.Priority = MessagePriority.Low;
			}
			queue.Send(message, MessageQueueTransactionType.Automatic);
		}


		private Command saveCreditCard(Int32 chargeID,
			Int32 memberID,
			Int32 memberTranID,
			Int32 memberPaymentID,
			Int32 siteID,
			TranType chargeType,
			decimal amount,
			CreditCardPayment creditCardPayment)
		{
			Command command = new Command("mnCharge", "dbo.up_Charge_Save_CreditCard", 0);

			if (creditCardPayment.IsraeliID != null)
			{
				command.AddParameter("@IsraeliID", SqlDbType.NVarChar, ParameterDirection.Input, Crypto.Encrypt(RuntimeSettings.GetSetting("KEY"), creditCardPayment.IsraeliID));
			}

			command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, chargeID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@ChargeTypeID", SqlDbType.Int, ParameterDirection.Input, (int)chargeType);
			command.AddParameter("@ChargeAmount", SqlDbType.Money, ParameterDirection.Input, amount);
			command.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.FirstName);
			command.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.LastName);
			command.AddParameter("@Phone", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.Phone);
			command.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.AddressLine1);
			command.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.City);
			command.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.State);
			command.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, creditCardPayment.CountryRegionID);
			command.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, creditCardPayment.PostalCode);
			command.AddParameter("@CreditCardNumber", SqlDbType.NVarChar, ParameterDirection.Input, Crypto.Encrypt(RuntimeSettings.GetSetting("KEY"), creditCardPayment.CreditCardNumber));
			command.AddParameter("@CreditCardExpirationMonth", SqlDbType.Int, ParameterDirection.Input, creditCardPayment.ExpirationMonth);
			command.AddParameter("@CreditCardExpirationYear", SqlDbType.Int, ParameterDirection.Input, creditCardPayment.ExpirationYear);
			command.AddParameter("@CreditCardType", SqlDbType.Int, ParameterDirection.Input, (Int32)creditCardPayment.CreditCardType);
			command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
			return command;
		}

	
		private Command saveCheck(Int32 chargeID,
			Int32 memberID,
			Int32 memberTranID,
			Int32 memberPaymentID,
			Int32 siteID,
			TranType chargeType,
			decimal amount,
			CheckPayment checkPayment)
		{
			Command command = new Command("mnCharge", "dbo.up_Charge_Save_CheckingAccount", 0);

			if (checkPayment.BankName != null)
			{
				command.AddParameter("@BankName", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.BankName);
			}

			if (checkPayment.AddressLine2 != null)
			{
				command.AddParameter("@AddressLine2", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.AddressLine2);
			}

			command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, chargeID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@MemberTranID", SqlDbType.Int, ParameterDirection.Input, memberTranID);
			command.AddParameter("@MemberPaymentID", SqlDbType.Int, ParameterDirection.Input, memberPaymentID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@ChargeTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)chargeType);
			command.AddParameter("@ChargeAmount", SqlDbType.Money, ParameterDirection.Input, amount);
			command.AddParameter("@FirstName", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.FirstName);
			command.AddParameter("@LastName", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.LastName);
			command.AddParameter("@Phone", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.Phone);
			command.AddParameter("@AddressLine1", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.AddressLine1);
			command.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.City);
			command.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.State);
			command.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.PostalCode);
			command.AddParameter("@DriversLicenseNumber", SqlDbType.NVarChar, ParameterDirection.Input, Crypto.Encrypt(RuntimeSettings.GetSetting("KEY"), checkPayment.DriversLicenseNumber));
			command.AddParameter("@DriversLicenseState", SqlDbType.NVarChar, ParameterDirection.Input, checkPayment.DriversLicenseState);
			command.AddParameter("@BankRoutingNumber", SqlDbType.NVarChar, ParameterDirection.Input, Crypto.Encrypt(RuntimeSettings.GetSetting("KEY"), checkPayment.BankRoutingNumber));
			command.AddParameter("@CheckingAccountNumber", SqlDbType.NVarChar, ParameterDirection.Input, Crypto.Encrypt(RuntimeSettings.GetSetting("KEY"), checkPayment.CheckingAccountNumber));
			command.AddParameter("@CheckNumber", SqlDbType.Int, ParameterDirection.Input, checkPayment.CheckNumber);
			command.AddParameter("@BankAccountType", SqlDbType.Int, ParameterDirection.Input, (Int32)checkPayment.BankAccountType);
			command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

			return command;
		}
	}
}
