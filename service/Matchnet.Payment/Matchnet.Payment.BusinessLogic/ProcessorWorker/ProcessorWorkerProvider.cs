using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Messaging;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Queuing;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Member.ServiceAdapters;


namespace Matchnet.Payment.BusinessLogic
{
	public class ProcessorWorkerProvider : IProcessorWorker
	{
		//Internal Spark Codes
		protected const string RESPONSE_CONNECTION_TIMEOUT = "1007";	//connection timeout

		public void ProcessMessage(object messageBody)
		{
			const byte MAX_ATTEMPTS = 3;
			MessageQueue queue = null;
			Provider provider = null;

			MemberTran memberTran = messageBody as MemberTran;

			if (memberTran == null)
			{
				throw new Exception("Message body was not a MemberTran (ProcessorWorkerProvider, type: " + messageBody.GetType().ToString() + ").");
			}

			try
			{
				/*
				if (memberTran.MemberID == 54188888)
				{
					memberTran.SiteMerchant = new SiteMerchant(11000, 7006, "[Matchnet.PaymentProvider.OptimalPaymentsProvider]Matchnet.PaymentProvider.OptimalPaymentsProvider.OptimalPaymentsProvider");
				}
				*/

				provider = ProviderFactory.GetProvider(memberTran.SiteMerchant);
				provider.Process(memberTran, getMerchantProperties(memberTran));
			}
			catch (Exception ex)
			{
				string failedTranResponseCode = Matchnet.Constants.NULL_INT.ToString();

				if (ex.GetBaseException().GetType() == typeof(ProviderNetworkException))
				{
					//MPR-660 TL 11132008 Updating ProviderNetworkException (e.g. timeout) with custom response code
					failedTranResponseCode = RESPONSE_CONNECTION_TIMEOUT;

					//increment attempts
					memberTran.Attempts++;

					if (memberTran.Attempts < MAX_ATTEMPTS)
					{
						//requeue
						queue = new MessageQueue(PaymentBL.GetSiteMerchantProcessor(memberTran.SiteID, memberTran.SiteMerchant.MerchantID));
						System.Messaging.Message message = new Message(memberTran, new BinaryMessageFormatter());
						if (memberTran.TranType != TranType.Renewal)
						{
							message.Priority = MessagePriority.High;
						}
						else
						{
							message.Priority = MessagePriority.Low;
						}
						queue.Send(message, MessageQueueTransactionType.Automatic);

						new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
							"Temporary error processing payment (attempt " + memberTran.Attempts.ToString() + " of " + MAX_ATTEMPTS.ToString() + ", memberID: " + memberTran.MemberID.ToString() + ", memberTranID: " + memberTran.MemberTranID.ToString() + ", message: " + ((ProviderNetworkException)ex.GetBaseException()).NetworkErrorMessage + ").\r\n",
							ex);
						return;
					}
				}

				queue = new MessageQueue(PaymentBL.QUEUEPATH_RESPONSE);
				queue.Formatter = new BinaryMessageFormatter();
				queue.Send(new ProviderResponse(false
												, failedTranResponseCode
												,Matchnet.Constants.NULL_INT.ToString()
												,Matchnet.Constants.NULL_INT.ToString()
												,null
												,memberTran
												,provider != null ? provider.ProviderID : Constants.NULL_INT)
												, MessageQueueTransactionType.Automatic);

				new ServiceBoundaryException( Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_NAME,
					"Failure processing payment.\r\n",
					ex);
			}
		}


		private Hashtable getMerchantProperties(MemberTran memberTran)
		{
			Hashtable properties = null;

			SqlDataReader dataReader = null;

			try
			{
				Command command = new Command("mnCharge", "dbo.up_Merchant_List", 0);
				command.AddParameter("@MerchantID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteMerchant.MerchantID);
				command.AddParameter("@PaymentTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTran.PaymentType);
				dataReader = Client.Instance.ExecuteReader(command);

				properties = new Hashtable();

				bool colLookupDone = false;
				Int32 ordinalAttributeID = Constants.NULL_INT;
				Int32 ordinalName = Constants.NULL_INT;
				Int32 ordinalValue = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalAttributeID = dataReader.GetOrdinal("AttributeID");
						ordinalName = dataReader.GetOrdinal("Name");
						ordinalValue = dataReader.GetOrdinal("Value");
						colLookupDone = true;
					}

					Int32 attributeID = dataReader.GetInt32(ordinalAttributeID);
					string value = null;
				
					if (!dataReader.IsDBNull(ordinalValue))
					{
						value = dataReader.GetString(ordinalValue);
					}

					switch(attributeID)
					{
						case PurchaseConstants.EMAIL_ADDRESS_ATTRIBUTE_ID:
							value = MemberSA.Instance.GetMember(memberTran.MemberID, MemberLoadFlags.None).EmailAddress;
							break;

						case PurchaseConstants.FIRST_NAME_ATTRIBUTE_ID:
							value = memberTran.Payment.FirstName;
							break;

						case PurchaseConstants.LAST_NAME_ATTRIBUTE_ID:
							value = memberTran.Payment.LastName;
							break;

						case PurchaseConstants.MEMBERTRAN_ID_ATTRIBUTE_ID:
							if ( memberTran.TranType != TranType.Void )
							{
								value = memberTran.MemberTranID.ToString();
							}
							else
							{
								value = memberTran.ReferenceMemberTranID.ToString();
							}
							break;

						case PurchaseConstants.ISRAELI_ID_ATTRIBUTE_ID:
							if ( ((CreditCardPayment)memberTran.Payment).IsraeliID != null )
							{
								value = ((CreditCardPayment)memberTran.Payment).IsraeliID.ToLower();
							}
							break;

						case PurchaseConstants.PHONE_ATTRIBUTE_ID:
							value = memberTran.Payment.Phone;
							break;

						case PurchaseConstants.ADDRESS_LINE1_ATTRIBUTE_ID:
							value = memberTran.Payment.AddressLine1;
							break;

						case PurchaseConstants.ADDRESS_LINE2_ATTRIBUTE_ID:
							value = ((CheckPayment)memberTran.Payment).AddressLine2;
							break;

						case PurchaseConstants.CITY_ATTRIBUTE_ID:
							value = memberTran.Payment.City;
							break;

						case PurchaseConstants.STATE_ATTRIBUTE_ID:
							value = memberTran.Payment.State;
							break;

						case PurchaseConstants.COUNTRY_REGION_ID_ATTRIBUTE_ID:
							Region region = RegionBL.Instance.GetRegion( Convert.ToInt32( memberTran.Payment.CountryRegionID ) );
							if ( memberTran.SiteMerchant.ProviderID == PurchaseConstants.JETPAY_PROVIDER_ID )
							{
								value = region.IsoCountryCode3;
							}
							else
							{
								value = region.IsoCountryCode2;
							}
							break;

						case PurchaseConstants.POSTAL_CODE_ATTRIBUTE_ID:
							value = memberTran.Payment.PostalCode;
							break;

						case PurchaseConstants.CREDIT_CARD_TYPE_ATTRIBUTE_ID:
							value = ((int)((CreditCardPayment)memberTran.Payment).CreditCardType).ToString();
							break;

						case PurchaseConstants.CARD_CARD_NUMBER_ATTRIBUTE_ID:
						case PurchaseConstants.CHECKING_ACCOUNT_NUMBER_ATTRIBUTE:
						case PurchaseConstants.DIRECT_DEBIT_ACCOUNT_NUMBER_ATTRIBUTE_ID:
							value = memberTran.AccountNumber;
							break;

						case PurchaseConstants.CREDIT_CARD_EXPIRATION_MONTH_ATTRIBUTE_ID:
							value = ((CreditCardPayment)memberTran.Payment).ExpirationMonth.ToString();
							break;

						case PurchaseConstants.CREDIT_CARD_EXPIRATION_YEAR_ATTRIBUTE_ID:
							value = ((CreditCardPayment)memberTran.Payment).ExpirationYear.ToString();
							break;

						case PurchaseConstants.CVC_ATTRIBUTE_ID:
							value = ((CreditCardPayment)memberTran.Payment).CVC;
							break;

						case PurchaseConstants.DRIVERS_LICENSE_NUMBER_ATTRIBUTE_ID:
							value = ((CheckPayment)memberTran.Payment).DriversLicenseNumber;
							break;

						case PurchaseConstants.DRIVERS_LICENSE_STATE_ATTRIBUTE_ID:
							value = ((CheckPayment)memberTran.Payment).DriversLicenseState;
							break;

						case PurchaseConstants.BANK_NAME_ATTRIBUTE_ID:
							if ( memberTran.PaymentType == PaymentType.Check )
							{
								value = ((CheckPayment)memberTran.Payment).BankName;
							}
							else
							{
								value = ((DirectDebitPayment)memberTran.Payment).BankName;
							}
							break;

						case PurchaseConstants.BANK_CODE_ATTRIBUTE_ID:
							value = ((DirectDebitPayment)memberTran.Payment).BankCode;
							break;

						case PurchaseConstants.BANK_ROUTING_NUMBER_ATTRIBUTE_ID:
							value = ((CheckPayment)memberTran.Payment).BankRoutingNumber;
							break;

						case PurchaseConstants.BANK_ACCOUNT_TYPE_ATTRIBUTE_ID:
							value = ((int)((CheckPayment)memberTran.Payment).BankAccountType).ToString();
							break;

						case PurchaseConstants.CHECK_NUMBER_ATTRIBUTE_ID:
							value = ((CheckPayment)memberTran.Payment).CheckNumber.ToString();
							break;

						case PurchaseConstants.AMOUNT_ATTRIBUTE_ID:
							value = memberTran.Amount.ToString();
							break;

						case PurchaseConstants.MEMBER_ID_ATTRIBUTE_ID:
							value = memberTran.MemberID.ToString();
							break;
					}

					if (value != null)
					{
						properties.Add(dataReader.GetString(ordinalName), value);
					}
				}

				if (memberTran.TranType == TranType.Void)
				{
					NameValueCollection nvc = PaymentBL.GetMemberTranLogByMerchantID(memberTran.ReferenceMemberTranID, memberTran.SiteMerchant.MerchantID);

					if (nvc != null)
					{
						foreach (string key in nvc.Keys)
						{
							string name = key;
							string value = nvc[key];

							if (name != string.Empty && value != string.Empty) 
							{
								if (!properties.ContainsKey(name)) 
								{
									properties.Add(name, value);
								}
							}
						}
					}
				}

				if (properties.Count == 0)
				{
					throw new Exception("No merchant properties returned for MemberID: " + memberTran.MemberID.ToString() + " SiteID: " + memberTran.SiteID.ToString() + " MerchantID: " + memberTran.SiteMerchant.MerchantID.ToString() + " PlanID: " + memberTran.PlanID.ToString());
				}
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}

			return properties;
		}
	}
}
