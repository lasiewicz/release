using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Messaging;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Security;
using Matchnet.Queuing;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.BusinessLogic
{
	public class ProcessorWorkerResponse : IProcessorWorker
	{
		public void ProcessMessage(object messageBody)
		{
			ProviderResponse response = messageBody as ProviderResponse;
			string resourceConstant = null;

			if (response == null)
			{
				throw new Exception("Message body was not a ProviderResponse (type: " + messageBody.GetType().ToString() + ").");
			}

			if (response.MemberTran == null)
			{
				throw new Exception("null member tran");
			}

			if (response.Properties == null)
			{
				throw new Exception("null properties");
			}
			saveTransactionLog(response.MemberTran, response.Properties);
			
			MemberTranStatus memberTranStatus = response.Success ? MemberTranStatus.Success : MemberTranStatus.Failure;

			Command[] commands;	
			if (memberTranStatus == MemberTranStatus.Success
				&& response.MemberTran.PaymentType == PaymentType.CreditCard
				&& (response.MemberTran.TranType == TranType.InitialBuy 
					|| response.MemberTran.TranType == TranType.Renewal 
					|| response.MemberTran.TranType == TranType.AuthorizationOnly
					|| response.MemberTran.TranType == TranType.AuthPmtProfile)
				)
			{

				commands = new Command[2];
				Command commandVelocity = new Command("mnCharge", "dbo.up_VelocityLog_Save", 0);
				commandVelocity.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, response.MemberTran.SiteID);
				commandVelocity.AddParameter("@CardHash", SqlDbType.VarChar, ParameterDirection.Input, Crypto.Hash(response.MemberTran.AccountNumber.Trim() + PurchaseConstants.SOOPER_HASH));
				
				bool AuthFlag = false;
				if(response.MemberTran.TranType == TranType.AuthorizationOnly
					|| response.MemberTran.TranType == TranType.AuthPmtProfile)
				{
					AuthFlag = true;
				}
				commandVelocity.AddParameter("@AuthFlag", SqlDbType.Bit, ParameterDirection.Input, AuthFlag);
				commandVelocity.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
				commands[1] = commandVelocity;
			}
			else
			{
				commands = new Command[1];
			}

			Command commandCharge = new Command("mnCharge", "dbo.up_Charge_Save_Complete", 0);
			commandCharge.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, response.MemberTran.ChargeID);
			commandCharge.AddParameter("@ChargeStatus", SqlDbType.Int, ParameterDirection.Input, (Int32)memberTranStatus);
			commands[0] = commandCharge;

			SyncWriter sw = new SyncWriter();
			Exception swEx;
			sw.Execute(commands,
				null,
				out swEx);
			sw.Dispose();

			if (swEx != null)
			{
				throw swEx;
			}

			StatusType statusType = response.Success ? StatusType.Success : StatusType.Failure;

			if ( (response.ResponseCode != null) && (Matchnet.Constants.NULL_INT.ToString() != response.ResponseCode) )
			{
				try
				{
					getProviderStatus(response.MemberTran.SiteMerchant.ProviderID,
						response.ResponseCode,
						out resourceConstant,
						out statusType);
				}
				catch (Exception ex)
				{
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
						"Unable to retrieve provider status (ProviderID: " + response.MemberTran.SiteMerchant.ProviderID.ToString() + ").",
						ex);
				}
			}

			//Put transaction into payment outbound queue
			MessageQueue queue = new MessageQueue(PaymentBL.QUEUEPATH_PAYMENT_OUTBOUND);
			queue.Formatter = new BinaryMessageFormatter();

			//09252008 TL MPR-539 Pass Request/Response XML for recording into mnChargeStage..ChargeLogXML
			MemberTranResult memberTranResult;
			if (response.logRequestResponseData)
			{
				memberTranResult = new MemberTranResult(response.MemberTran,
					resourceConstant,
					memberTranStatus,
					response.ResponseCode,
					response.ResponseCodeAVS,
					response.ResponseCodeCVV,
					response.ProviderID,
					response.ResponseData,
					response.RequestDataScrubbed);

				memberTranResult.logRequestResponseData = true;
			}
			else
			{
				memberTranResult = new MemberTranResult(response.MemberTran,
					resourceConstant,
					memberTranStatus,
					response.ResponseCode,
					response.ResponseCodeAVS,
					response.ResponseCodeCVV,
					response.ProviderID);

			}
			queue.Send(memberTranResult, MessageQueueTransactionType.Automatic);
		}

		private void saveTransactionLog(MemberTran memberTran, 
			ArrayList properties) 
		{
			Hashtable log = getUniqueLogValues(properties);
			
			if (log != null)
			{
				IDictionaryEnumerator de = log.GetEnumerator();
				while (de.MoveNext())
				{
					string val = "";

					if (de.Value != null)
					{
						val = de.Value.ToString();
					}

					Command command = new Command("mnCharge", "dbo.up_ChargeLog_Save", 0);
					command.AddParameter("@ChargeID", SqlDbType.Int, ParameterDirection.Input, memberTran.ChargeID);
					command.AddParameter("@MerchantID", SqlDbType.Int, ParameterDirection.Input, memberTran.SiteMerchant.MerchantID);
					command.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input, de.Key);
					command.AddParameter("@Value", SqlDbType.NVarChar, ParameterDirection.Input, val);
					command.AddParameter("@ResponseTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)RequestType.ProviderRequest);
					command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

					SyncWriter sw = new SyncWriter();
					Exception ex;
					sw.Execute(command,
						null,
						out ex);
					sw.Dispose();

					if (ex != null)
					{
						throw ex;
					}
				}				
			}
		}


		private Hashtable getUniqueLogValues(ArrayList properties)
		{
			Hashtable newProperties = null;

			if (properties != null && properties.Count > 0)
			{
				newProperties = new Hashtable();
				
				for (int i = 0; i < properties.Count; i++)
				{
					Property property = (Property)properties[i];
					string name = property.Name;
					string value = property.Value;
						
					if (!newProperties.Contains(name))
					{
						for (int j = 0; j < properties.Count; j++)
						{
							if (((Property)properties[j]).Name == name && j != i)
							{
								value = value + "|" + ((Property)properties[j]).Value;
							}
						}
						newProperties.Add(name, value);
					}
				}
			}

			return newProperties;
		}


		private void getProviderStatus(
			Int32 providerID,
			string responseCode,
			out string resourceConstant,
			out StatusType statusType)
		{
			resourceConstant = Matchnet.Constants.NULL_STRING;
			statusType = StatusType.Failure;
			Int32 statusTypeID = Matchnet.Constants.NULL_INT;

			SqlDataReader dataReader = null;

			try
			{
				//todo - this crap should be cached
				Command command = new Command("mnCharge", "dbo.up_ProviderStatus_List", 0);
				command.AddParameter("@ProviderID", SqlDbType.Int, ParameterDirection.Input, providerID);
				command.AddParameter("@StatusCode", SqlDbType.VarChar, ParameterDirection.Input, responseCode);
				dataReader = Client.Instance.ExecuteReader(command);

				bool colLookupDone = false;
				Int32 ordinalResourceConstant = Constants.NULL_INT;
				Int32 ordinalStatusTypeID = Constants.NULL_INT;

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						ordinalResourceConstant = dataReader.GetOrdinal("ResourceConstant");
						ordinalStatusTypeID = dataReader.GetOrdinal("StatusTypeID");
						colLookupDone = true;
					}

					if (!dataReader.IsDBNull(ordinalResourceConstant))
					{
						resourceConstant = dataReader.GetString(ordinalResourceConstant);
					}

					statusTypeID = dataReader.GetInt32(ordinalStatusTypeID);
				}
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}

			if (!Enum.IsDefined(typeof(StatusType), statusTypeID))
			{
				new ServiceBoundaryException(Matchnet.Purchase.ValueObjects.ServiceConstants.SERVICE_NAME,
					"Provider response mapping not found for ProviderID: " + providerID.ToString() + " ResponseCode: " + responseCode);
			}
			else
			{
				statusType = (StatusType)Enum.Parse(typeof(StatusType), statusTypeID.ToString() );
			}
		}
	}
}
