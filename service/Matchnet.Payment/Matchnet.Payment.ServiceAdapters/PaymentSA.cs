using System;
using System.Collections.Specialized;

using Matchnet.Exceptions;
using Matchnet.Payment.ValueObjects;
using Matchnet.Payment.ValueObjects.ServiceDefinitions;
using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.ServiceAdapters
{
	public class PaymentSA
	{
		public static readonly PaymentSA Instance = new PaymentSA();

		private PaymentSA()
		{
		}


		public void ProcessPayment(MemberTran memberTran)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				getService(uri).ProcessPayment(memberTran);
			}
			catch (Exception ex)
			{
				throw new SAException("ProcessPayment() error (uri: " + uri + ", memberTranID: " + memberTran.MemberTranID.ToString() + ").", ex);
			}
		}

		public int GenerateEncryptedSalesFile()
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GenerateEncryptedSalesFile();
			}
			catch(Exception ex)
			{
				throw new SAException("GenerateEncryptedSalesFile() error (uri: " + uri + ").", ex);
			}
		}
		
		public NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetChargeLog(memberID, 
					memberTranID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetChargeLog() error (uri: " + uri + ").", ex);
			}
		}
		
	
		public NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetMemberPayment(memberID, 
					communityID, 
					memberPaymentID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetMemberPayment() error (uri: " + uri + ").", ex);
			}
		}

		
		public NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetMemberPaymentByMemberTranID(memberID, 
					memberTranID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetMemberPaymentByMemberTranID() error (uri: " + uri + ").", ex);
			}
		}


		public MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetChargeStatus(memberID, 
					memberTranID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetChargeStatus() error (uri: " + uri + ").", ex);
			}
		}


		public MemberPaymentInfo GetMemberPaymentInfo(Int32 memberID,
			Int32 memberPaymentID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetMemberPaymentInfo(memberID, 
					memberPaymentID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetMemberPaymentInfo() error (uri: " + uri + ").", ex);
			}
		}


		public Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetSuccessfulPayment(memberID, 
					siteID);
			}
			catch (Exception ex)
			{
				throw new SAException("GetMemberPaymentInfo() error (uri: " + uri + ").", ex);
			}
		}


		public void UpdateMemberPayment(Int32 memberPaymentID,
			Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				getService(uri).UpdateMemberPayment(memberPaymentID,
					memberID,
					siteID,
					firstName,
					lastName,
					phone,
					addressLine1,
					city,
					state,
					countryRegionID,
					postalCode,
					creditCardNumber,
					expirationMonth,
					expirationYear,
					creditCardType);
			}
			catch (Exception ex)
			{
				throw new SAException("UpdateMemberPayment() error (uri: " + uri + ").", ex);
			}
		}
		

		private IPaymentService getService(string uri)
		{
			try
			{
				return (IPaymentService)Activator.GetObject(typeof(IPaymentService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(Matchnet.Payment.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PAYMENT_SERVICE_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}
				
				return uri;
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot get configuration settings for remote service manager.", ex);
			}
		}
	}
}
