using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using Matchnet;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Payment.BusinessLogic;
//using Matchnet.Payment.DataAccess;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.Payment.TranzilaProvider
{
	public class TranzilaProvider : Provider
	{
		public const string FIELD_TRANSACTIONTYPE = "TransactionType";

		public const string FIELD_SUM = "sum";
		public const string FIELD_CCNO = "ccno";
		public const string FIELD_EXPMONTH = "expmonth";
		public const string FIELD_EXPYEAR = "expyear";
		public const string FIELD_CURRENCY = "currency";
		public const string FIELD_MEMBERID = "memberid";
		public const string FIELD_TRANSACTIONID = "transactionid";
		public const string FIELD_REFERENCETRANSACTIONID = "referencetransactionid";
		public const string FIELD_SUPPLIER = "supplier";
		public const string FIELD_AUTHNUMBER = "authnr";
		public const string FIELD_TRANMODE = "tranmode";
		public const string FIELD_CONFIRMATION_CODE = "ConfirmationCode";
		public const string FIELD_INDEX = "index";
		public const string FIELD_TASK = "task";
		// User Defined values for Installments
		//Sum of money.
		//First pay + Subsequent pay * No. of pay = Sum
		public const string FIELD_CREDITTYPE = "cred_type";
		public const string FIELD_FIRSTPAY = "fpay";
		public const string FIELD_SUBSEQUENTPAY = "spay";
		public const string FIELD_NUMBERPAY = "npay";
		//Invoice Fields
		public const string FIELD_PLANDESCRIPTION = "PlanDescription";
		public const string FIELD_FIRSTNAME = "FirstName";
		public const string FIELD_FAMILYNAME = "FamilyName";
		public const string FIELD_FULLNAME = "FullName";

		private string request;
		private string response;

		public override string RawRequest() 
		{
			return request;
		}

		public override string RawResponse()
		{
			return response;
		}

		protected override ProviderResponse DoPurchase(MemberTran mt)
		{
			string referer = string.Empty;
			string planDescription = string.Empty;
			bool renewFlag = (mt.TranType == TranType.Renewal);

			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

			//set the current thread to hebrew Israel so that currency comes out in Shekels - mainly for plandescription below.
			System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("he-IL");

			// installments - these values are defined in the tranzilla "Settings"
			if (mt.Plan != null)
			{
				// Renewal purchases do not have payments by installments  
				if ((mt.Plan.PlanTypeMask & PlanType.Installment) == PlanType.Installment
					&& mt.TranType != TranType.Renewal)
				{
					providerProperties[FIELD_CREDITTYPE] = "8";	// 1 = regular (Default); 8 = Installments, so we need to override
					decimal totalCost = (decimal)mt.Plan.InitialCost;
					int duration = mt.Plan.InitialDuration;
					decimal intervalCost = totalCost/duration;

					providerProperties[FIELD_FIRSTPAY] = intervalCost.ToString();
					providerProperties[FIELD_SUBSEQUENTPAY] = intervalCost.ToString();
					providerProperties[FIELD_NUMBERPAY] = (duration - 1).ToString();
				}
				planDescription = GetPlanDescription(mt.Plan, renewFlag);
			}

			if (planDescription.Length == 0)
			{	//Adam
				planDescription = (renewFlag ? "Renewal ":"") + "Charge: " + Conversion.CDecimal(providerProperties[FIELD_SUM],0).ToString("c");
			}

			providerProperties[FIELD_PLANDESCRIPTION] = planDescription;

			// Concat name into 1 field for invoices; we are creating FIELD_FULLNAME on the fly and defaulting to Family name if we don't have it.
			if (providerProperties.Contains(FIELD_FIRSTNAME) && providerProperties.Contains(FIELD_FAMILYNAME))
			{
				//Format Name
				string name = providerProperties[FIELD_FIRSTNAME].ToString().Trim() + " " + providerProperties[FIELD_FAMILYNAME].ToString().Trim();
				providerProperties.Add(FIELD_FULLNAME, name);
			}
			else
			{
				providerProperties.Add(FIELD_FULLNAME, providerProperties[FIELD_FAMILYNAME].ToString());
			}
			
			// end installements/invoice changes

			string data = FormatData();
			data = data + "&" + FIELD_MEMBERID + "=" + mt.MemberID;
			request = data;
			string url = (string)providerProperties[FIELD_URL];
			string res = PostData(url, data, referer);
			response = res;

			TranzilaResponse tranzilaResponse = new TranzilaResponse();
			tranzilaResponse.FromString(res);

			return tranzilaResponse;
		}

		private string FormatData() 
		{
			StringBuilder builder = new StringBuilder();
			bool first = true;
			foreach (string key in providerProperties.Keys) 
			{
				if (!IsExcludedField(key)) 
				{
					if (first) 
					{
						first = false;
					} 
					else 
					{
						builder.Append("&");
					}
					string val = System.Convert.ToString(providerProperties[key]);
					switch (key) 
					{
						case FIELD_EXPYEAR:
							val = val.Length > 2 ? val.Substring(val.Length - 2) : val;
							break;
						case FIELD_EXPMONTH:
							val = val.Length == 2 ? val : "0" + val;
							break;
					}
					builder.Append(key + "=" + System.Web.HttpUtility.UrlEncode(val, System.Text.Encoding.GetEncoding(1255)));
				}
			}
			return builder.ToString();
		}

		public override ProviderResponse Authorize(MemberTran mt) 
		{
			string referer = string.Empty;

			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

			string data = FormatData();
			data = data + "&" + FIELD_TASK + "=Doverify";
			data = data + "&" + FIELD_TRANMODE + "=V";

			request = data;
			string url = (string)providerProperties[FIELD_URL];
			string res = PostData(url, data, referer);
			response = res;

			TranzilaResponse tranzilaResponse = new TranzilaResponse();
			tranzilaResponse.FromString(res);
			return tranzilaResponse;
		}

		public override ProviderResponse Void(MemberTran mt) 
		{
			throw new Exception("Tranzila Void Not Supported");
		}

		protected override ProviderResponse DoCredit(MemberTran mt)
		{
			string referer = string.Empty;

			if (providerProperties.ContainsKey(FIELD_REFER)) 
			{
				referer = (string)providerProperties[FIELD_REFER];
			}

			if (providerProperties.ContainsKey(FIELD_TRANSACTIONID)) 
			{
				// don't use the original transaction id
				providerProperties.Remove(FIELD_TRANSACTIONID);
			}

			if (providerProperties.ContainsKey(FIELD_SUM)) 
			{
				providerProperties.Remove(FIELD_SUM);
			}
			providerProperties.Add(FIELD_SUM, System.Convert.ToString(mt.Amount));

			string data = FormatData();

			data = data + "&" + FIELD_MEMBERID + "=" + mt.MemberID;
			data = data + "&" + FIELD_TRANSACTIONID + "=" + mt.MemberTranID;
			data = data + "&" + FIELD_REFERENCETRANSACTIONID + "=" + mt.ReferenceMemberTranID;
			
			data = data + "&" + FIELD_AUTHNUMBER + "=" + GetAuthNumber(mt);
			data = data + "&" + FIELD_TRANMODE + "=C" + GetIndex(mt);

			request = data;
			string url = (string)providerProperties[FIELD_URL];
			string res = PostData(url, data, referer);
			response = res;

			TranzilaResponse tranzilaResponse = new TranzilaResponse();
			tranzilaResponse.FromString(res);
			return tranzilaResponse;
		}

		private string GetPlanDescription (Plan plan, bool renewFlag)
		{
			string planDescription = string.Empty;

			// We need to creat a plan description to send over for invoices.  This text is stored in resources so we are creating
			// a simple sentance to describe the plan based off the plan attributes.
			if (plan.InitialCost > 0 )
			{	
				if (!renewFlag)
				{
					planDescription = "Charge: " + plan.InitialCost.ToString("c") + " for " + plan.InitialDuration.ToString() + " " + plan.InitialDurationType.ToString() + "(s)"; 
				}

				if (plan.RenewCost>0)
				{
					string renewTerm = string.Empty;
					if (plan.RenewDuration==1)
					{
						switch (plan.RenewDurationType)
						{	//Adam
							case DurationType.Month:
								renewTerm = "monthly";
								break;
							case DurationType.Year:
								renewTerm = "yearly";
								break;
							case DurationType.Week:
								renewTerm = "weekly";
								break;
							default:
								renewTerm = "montly";
								break;
						}
					}
					else
					{	//Adam
						renewTerm = " every " + plan.RenewDuration.ToString() + " " + plan.RenewDurationType.ToString() + "(s)";
					}
					//Adam
					planDescription +=(!renewFlag ? ", ":"") + "Renew Charge: " + plan.RenewCost.ToString("c") + " " + renewTerm;
				}
			}
			return planDescription;
		}

		private string GetAuthNumber(MemberTran mt)
		{
			return getLogValue(mt.MemberID, mt.ReferenceMemberTranID, FIELD_CONFIRMATION_CODE);
		}

		private string GetIndex(MemberTran mt) 
		{
			return getLogValue(mt.MemberID, mt.ReferenceMemberTranID, FIELD_INDEX);
		}

		// ** NOTE: We should remove PostData() and replace it with calls to the Provider class like we did in Optimal and Litle in the rewrite
		//
		private string PostData(string url, string data, string referer) 
		{
			return PostData(url, data, referer, System.Threading.Timeout.Infinite);
		}
		// ** NOTE: We should remove PostData() and replace it with calls to the Provider class like we did in Optimal and Litle in the rewrite
		//
		private string PostData(string url, string data, string referer, int timeout) 
		{
			WebResponse response = null;
			if(url.StartsWith("https")) 
			{
				//ignore invalid/expired SSL cert
				ServicePointManager.CertificatePolicy = new OpenCertificatePolicy();
			}
			
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "POST";
			//MPR-660 11132008 - Timeout override per provider based on configuration
			request.Timeout = (timeoutOverride != Matchnet.Constants.NULL_INT && timeoutOverride > 0) ? timeoutOverride : timeout;

			if (referer != null && referer.Trim().Length > 0) 
			{
				request.Referer = referer;
			}

			//request.ContentType = "multipart/form-data";
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = data.Length;

			byte[] postData = System.Text.Encoding.UTF8.GetBytes(data);
			Stream requestStream = request.GetRequestStream();

			requestStream.Write(postData, 0, data.Length);
			requestStream.Close();

			response = request.GetResponse();
			StreamReader reader = new StreamReader(response.GetResponseStream());
			string retVal = reader.ReadToEnd();
			try 
			{
				reader.Close();
				response.Close();
			} 
			catch {}
			return retVal;
		}
	}
}
