using System;
using System.Collections;

using Matchnet;
using Matchnet.Payment.BusinessLogic;

namespace Matchnet.Payment.TranzilaProvider
{
	[Serializable]
	public class TranzilaResponse : ProviderResponse
	{
		public const string FIELD_RESPONSE = "Response";

		public void FromString(string response) 
		{
			string[] properties = response.Split("&".ToCharArray());
			for (int x = 0; x < properties.Length; x++) 
			{
				string[] prop = properties[x].Split("=".ToCharArray());
				string name = prop[0];
				string val = prop[1];
				Property property = new Property(name, val);
				AddProperty(property);
				if (name == FIELD_RESPONSE) 
				{
					ResponseCode = val;
					switch (val) 
					{
						case "000":
							Success = true;
							break;
						default:
							Success = false;
							break;
					}
				}
			}
		}
	}
}
