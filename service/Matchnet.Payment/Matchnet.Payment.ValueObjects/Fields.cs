using System;
using System.Collections;

namespace Matchnet.Payment.ValueObjects
{
	/// <summary>
	/// Summary description for Fields.
	/// </summary>
	public class Fields : CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		public Fields()
		{
		}

		#region CollectionBase Implementation
		/// <summary>
		/// 
		/// </summary>
		public Field this[int index]
		{
			get{return (Field)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		public int Add(Field field)
		{
			return base.InnerList.Add(field);
		}		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public Field FindByName(string name)
		{
			foreach(Field field in this)
			{
				if(field.Name == name)
				{
					return field;
				}
			}

			return null;
		}		
		#endregion
	}
}
