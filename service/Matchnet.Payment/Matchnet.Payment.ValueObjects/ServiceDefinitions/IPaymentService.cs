using System;
using System.Collections.Specialized;

using Matchnet.Purchase.ValueObjects;


namespace Matchnet.Payment.ValueObjects.ServiceDefinitions
{
	public interface IPaymentService
	{
		int GenerateEncryptedSalesFile();

		void ProcessPayment(MemberTran memberTran);

		NameValueCollection GetChargeLog(Int32 memberID,
			Int32 memberTranID);
			
		NameValueCollection GetMemberPayment(Int32 memberID, 
			Int32 communityID, 
			Int32 memberPaymentID);

		MemberPaymentInfo GetMemberPaymentInfo(Int32 memberID,
			Int32 memberPaymentID);

		NameValueCollection GetMemberPaymentByMemberTranID(Int32 memberID,
			Int32 memberTranID);

		Int32 GetSuccessfulPayment(Int32 memberID, 
			Int32 siteID);

		MemberTranStatus GetChargeStatus(Int32 memberID,
			Int32 memberTranID);

		void UpdateMemberPayment(Int32 memberPaymentID,
			Int32 memberID,
			Int32 siteID,
			string firstName,
			string lastName,
			string phone,
			string addressLine1,
			string city,
			string state,
			Int32 countryRegionID,
			string postalCode,
			string creditCardNumber,
			Int32 expirationMonth,
			Int32 expirationYear,
			CreditCardType creditCardType);
	}
}
