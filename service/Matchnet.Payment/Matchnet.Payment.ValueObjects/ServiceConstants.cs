using System;

namespace Matchnet.Payment.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_NAME = "Matchnet.Payment.Service";
		public const string SERVICE_CONSTANT = "PAYMENT_SVC";
		public const string SERVICE_MANAGER_NAME = "PaymentSM";

		private ServiceConstants()
		{
		}
	}
}
