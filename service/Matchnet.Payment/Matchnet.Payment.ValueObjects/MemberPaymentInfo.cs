using System;

using Matchnet;

namespace Matchnet.Payment.ValueObjects
{
	[Serializable]
	public class MemberPaymentInfo : IValueObject
	{
		
		#region Private Members
		
		int memberID = Constants.NULL_INT;
		int memberPaymentID = Constants.NULL_INT;
		int paymentTypeID = Constants.NULL_INT;
		string description = Constants.NULL_STRING;
		string resourceConstant = Constants.NULL_STRING;
		string accountNumber = Constants.NULL_STRING;
		int groupID = Constants.NULL_INT;

		#endregion

		#region Constructors

		public MemberPaymentInfo()
		{
		}

		public MemberPaymentInfo(
			int memberID,
			int memberPaymentID,
			int paymentTypeID,
			string description,
			string resourceConstant,
			string accountNumber,
			int groupID)
		{
			this.memberPaymentID = memberPaymentID;
			this.paymentTypeID = paymentTypeID;
			this.description = description;
			this.resourceConstant = resourceConstant;
			this.accountNumber = accountNumber;
			if ( accountNumber != Constants.NULL_STRING && accountNumber.Length > 4 )
			{
				this.accountNumber = accountNumber.Substring( accountNumber.Length - 4, 4 );
			}
			this.groupID = groupID;
		}
		
		#endregion

		#region Properties

		public int MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		public int MemberPaymentID
		{
			get { return memberPaymentID; }
			set { memberPaymentID = value; }
		}

		public int PaymentTypeID
		{
			get { return paymentTypeID; }
			set { paymentTypeID = value; }
		}

		public string Description
		{
			get { return description; }
			set { description = value; }
		}

		public string ResourceConstant
		{
			get { return resourceConstant; }
			set { resourceConstant = value; }
		}

		public string AccountNumber
		{
			get { return accountNumber; }
			set { accountNumber = value; }
		}

		public int GroupID
		{
			get { return groupID; }
			set { groupID = value; }
		}

		#endregion

	}
}
