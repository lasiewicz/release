using System;

namespace Matchnet.Payment.ValueObjects
{
	/// <summary>
	/// Summary description for Field.
	/// </summary>
	public class Field
	{
		#region Members
		private string _name;
		private string _value;
		private string _dbName;
		private int _start;
		private int _length;
		#endregion

		#region Properties
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public string Value
		{
			get { return _value; }
			set { _value = value; }
		}

		public string DBName
		{
			get { return _dbName; }
			set { _dbName = value; }
		}

		public int Start
		{
			get { return _start; }
			set { _start = value; }
		}

		public int Length
		{
			get { return _length; }
			set { _length = value; }
		}
		#endregion

		public Field()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
