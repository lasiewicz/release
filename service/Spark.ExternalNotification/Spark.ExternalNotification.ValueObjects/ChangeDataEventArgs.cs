using System;
using Matchnet.Member.ServiceAdapters;

namespace Spark.ExternalNotification.ValueObjects
{
	/// <summary>
	/// Base definition of a changed something event arguments
	/// </summary>
	public class DataChangedEventArgs: EventArgs
	{
	}


	public class MemberChangedEventArgs: DataChangedEventArgs
	{
		public Matchnet.Member.ServiceAdapters.Member ChangedMember;
		public Matchnet.Member.ValueObjects.MemberUpdate Updates;

		public MemberChangedEventArgs(Matchnet.Member.ServiceAdapters.Member changedMember, Matchnet.Member.ValueObjects.MemberUpdate updates)
		{
			this.ChangedMember = changedMember;
			this.Updates = updates;
		}
	}


	public class ListChangedEventArgs: DataChangedEventArgs
	{
		public object ListData;
	}


	public class InstantMessageEventArgs: DataChangedEventArgs 
	{
		public object IMData;
	}


	

}
