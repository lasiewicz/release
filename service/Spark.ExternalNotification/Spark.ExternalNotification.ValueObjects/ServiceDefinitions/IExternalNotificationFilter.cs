using System;

namespace Spark.ExternalNotification.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IExternalNotificationFilter.
	/// </summary>
	public interface IExternalNotificationFilter
	{
		bool NotificationRequired(Matchnet.Member.ValueObjects.MemberUpdate updateData, Matchnet.Member.ServiceAdapters.Member member);
	}
}
