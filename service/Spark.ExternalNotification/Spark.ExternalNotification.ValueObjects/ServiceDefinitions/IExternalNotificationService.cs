using System;

namespace Spark.ExternalNotification.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IExternalNotificationService.
	/// </summary>
	public interface IExternalNotificationService
	{
		void ChangeDataEventHandler(object sender, DataChangedEventArgs eventArgs) ;
		bool RegisterRemoteClient(IExternalNotificationClient client);
		bool UnregisterRemoteClient(IExternalNotificationClient client);
	}
}
