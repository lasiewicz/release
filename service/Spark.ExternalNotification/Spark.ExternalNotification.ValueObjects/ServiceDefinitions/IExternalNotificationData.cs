using System;
using Matchnet;


namespace Spark.ExternalNotification.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Represents 
	/// </summary>
	public interface IExternalNotificationData: IByteSerializable
	{
		/// <summary>
		/// Retreive original data in full
		/// </summary>
		object Data{ get;}
		/// <summary>
		/// The retry policy for this notification
		/// </summary>
		Retry RetryData{ get; set; }
		/// <summary>
		/// Retreive a particular value from the original data
		/// </summary>
		/// <param name="key">the name of the item required</param>
		/// <returns>the value of the named item</returns>
		object GetValue(string key);
	}
}
