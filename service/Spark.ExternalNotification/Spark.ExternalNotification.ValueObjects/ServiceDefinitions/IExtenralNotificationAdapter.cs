using System;

namespace Spark.ExternalNotification.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Represents a class which transforms a notification data to client specific required data and shape.
	/// For a given notification, a change in an object data might result in all data being sent verbatim, or transformed into client namespace and data types or anywhere in the middle.
	/// The adapter is responsible for transforming the input to the client specific payload.
	/// </summary>
	public interface IExtenralNotificationAdapter
	{
		object Transform(IExternalNotificationData data);
	}
}
