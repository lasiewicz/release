using System;
using System.Net.Sockets;

namespace Spark.ExternalNotification.ValueObjects.ServiceDefinitions
{
	public interface IExternalNotificationClient
	{
		Uri NotificationAddress{ get; set; } 
		IExtenralNotificationAdapter NotificationAdapter{get; set;}
		bool Notify(object data);
	}
}
