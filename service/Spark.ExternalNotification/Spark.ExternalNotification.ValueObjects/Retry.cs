using System;
using Matchnet;

namespace Spark.ExternalNotification.ValueObjects
{
	/// <summary>
	/// Summary description for Retry.
	/// </summary>
	public class Retry: IByteSerializable
	{
		/// <summary>
		/// The mode for retrying this operation
		/// </summary>
		public NotificationRetryMode RetryMode;
		/// <summary>
		/// The first attempt to notify the client
		/// </summary>
		public readonly DateTime FirstAttempt = DateTime.Now;
		/// <summary>
		/// The number of attempts so far
		/// </summary>
		public int RetryCount = 0;
		/// <summary>
		/// The wait time interval between attempts
		/// </summary>
		public TimeSpan RetryInterval;

		public void FromByteArray(byte[] bytes)
		{
			///TODO: implement
		}
		
		public byte [] ToByteArray()
		{
			///TODO: implement
			return null;
		}
	}
}
