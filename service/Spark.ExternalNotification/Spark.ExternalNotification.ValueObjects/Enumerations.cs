using System;

namespace Spark.ExternalNotification.ValueObjects
{
	public enum NotificationRetryMode: int
	{
		SingleTry = 0,
		NTimes = 1,
		TimeSpan = 2,
	}
}
