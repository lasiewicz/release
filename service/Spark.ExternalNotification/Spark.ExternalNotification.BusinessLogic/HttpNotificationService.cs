using System;
using System.Collections; 
using System.Net;
using System.Threading;
using System.Web;

using Spark.ExternalNotification.ValueObjects;
using Spark.ExternalNotification.ValueObjects.ServiceDefinitions;


namespace Spark.ExternalNotification.BusinessLogic
{
	/// <summary>
	/// Summary description for HttpNotificationService.
	/// </summary>
	public class HttpNotificationService: IExternalNotificationService
	{
		private Hashtable _Clients;
		private Queue _Notifications;
		private bool IsActive = true;


		public void ChangeDataEventHandler(object sender, DataChangedEventArgs e)
		{
			_Notifications.Enqueue(e);
		}

		
		public HttpNotificationService()
		{
			_Clients = new Hashtable();
			_Notifications = new Queue(256);
			System.Threading.Thread t = new Thread(new ThreadStart(processCycle)).Start();
		}

		
		public bool UnregisterRemoteClient(IExternalNotificationClient client)
		{
			try 
			{
				_Clients.Remove(client.NotificationAddress);
				return true;
			}
			catch {
				return false;
			}
		}


		public bool RegisterRemoteClient(IExternalNotificationClient client)
		{
			try 
			{
				_Clients[client.NotificationAddress] = client;
				return true;
			}
			catch 
			{
				return false;
			}
		}

		private void processCycle()
		{
			HttpWebRequest wr = new HttpWebRequest();
			while (IsActive) 
			{
				object notification = _Notifications.Dequeue();
				if (notification == null) 
					Thread.Sleep(3);
				else
				{

					foreach (IExternalNotificationClient client in _Clients.Values)
					{
						client.Notify(notification );
					}
				}
			}
		}

		
	}
}
