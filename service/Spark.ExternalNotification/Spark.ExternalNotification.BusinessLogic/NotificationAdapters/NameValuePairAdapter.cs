using System;
using System.Collections;
using Spark.ExternalNotification.ValueObjects;
using Spark.ExternalNotification.ValueObjects.ServiceDefinitions;

namespace Spark.ExternalNotification.BusinessLogic.NotificationAdapters
{
	/// <summary>
	/// Summary description for NameValuePairAdapter.
	/// </summary>
	public class NameValuePairAdapter: IExtenralNotificationAdapter	
	{
		private string [] _InputNames;
		private string [] _OutputNames;

		public NameValuePairAdapter(string [] inputNames, string [] outputNames)
		{
			_InputNames = inputNames ;
			_OutputNames = outputNames;
		}

		#region IExtenralNotificationAdapter Members

		public object Transform(IExternalNotificationData data)
		{
			Hashtable result = new Hashtable(_InputNames.Length);
			
			for(int i = 0; i < _InputNames.Length; i++)
			{
				result[_OutputNames[i]] = data.GetValue(_InputNames[i]);
			}

			return result;
		}

		#endregion
	}
}
