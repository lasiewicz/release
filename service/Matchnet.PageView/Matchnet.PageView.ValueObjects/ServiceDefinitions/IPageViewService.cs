using System;

using Matchnet.PageView.ValueObjects;


namespace Matchnet.PageView.ValueObjects.ServiceDefinitions
{
	public interface IPageViewService
	{
		[System.Runtime.Remoting.Messaging.OneWay()]
		void LogImpressions(Impression[] impressions);
	}
}
