using System;

namespace Matchnet.PageView.ValueObjects
{
	/// <summary>
	/// Summary description for ServiceConstants.
	/// </summary>
	public class ServiceConstants
	{
		public const string SERVICE_CONSTANT = "PAGEVIEW_SVC";
		public const string SERVICE_NAME = "Matchnet.PageView.Service";
		public const string SERVICE_MANAGER_NAME = "PageViewSM";

		private ServiceConstants()
		{
		}
	}
}
