using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

using Matchnet;


namespace Matchnet.PageView.ValueObjects
{
	[Serializable]
	public class Impression : IValueObject, ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;

		private Int32 _memberID;
		private Int32 _brandID;
		private Guid _sessionKey;
		private Int32 _pageID;
		private Int32 _ipAddress;
		private DateTime _dateTime;
		private Int32[] _otherInts;

		public Impression(Int32 memberID,
			Int32 brandID,
			Guid sessionKey,
			Int32 pageID,
			string ipAddress,
			Int32[] otherInts)
		{
			Int32 ipAddressInt;
			
			//wtf, get 2 addresses separated by a comma sometimes
			Int32 commaPos = ipAddress.IndexOf(",");
			if (commaPos > -1)
			{
				ipAddress = ipAddress.Substring(0, commaPos);
			}

			try
			{
				ipAddressInt = BitConverter.ToInt32(System.Net.IPAddress.Parse(ipAddress).GetAddressBytes(), 0);
			}
			catch (Exception ex)
			{
				throw new Exception("Unable to parse IP address (ipAddress: " + ipAddress + ").", ex);
			}

			init(memberID,
				brandID,
				sessionKey,
				pageID,
				ipAddressInt,
				otherInts);
		}

		public Impression(Int32 memberID,
			Int32 brandID,
			Guid sessionKey,
			Int32 pageID,
			Int32 ipAddress,
			Int32[] otherInts)
		{
			init(memberID,
				brandID,
				sessionKey,
				pageID,
				ipAddress,
				otherInts);
		}


		private void init(Int32 memberID,
			Int32 brandID,
			Guid sessionKey,
			Int32 pageID,
			Int32 ipAddress,
			Int32[] otherInts)
		{
			if (otherInts.Length > 255)
			{
				throw new Exception("otherInts.Length must be <= 255");
			}

			_memberID = memberID;
			_brandID = brandID;
			_sessionKey = sessionKey;
			_pageID = pageID;
			_ipAddress = ipAddress;
			_dateTime = DateTime.Now;
			_otherInts = otherInts;
		}


		protected Impression(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray",ToByteArray());
		}

		
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public Int32 BrandID
		{
			get
			{
				return _brandID;
			}
		}


		public Guid SessionKey
		{
			get
			{
				return _sessionKey;
			}
		}


		public Int32 PageID
		{
			get
			{
				return _pageID;
			}
		}


		public Int32 IPAddress
		{
			get
			{
				return _ipAddress;
			}
		}


		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}


		public Int32[] OtherInts
		{
			get
			{
				return _otherInts;
			}
		}


		public override string ToString()
		{
			return "Impression: " + _memberID.ToString() + ", " +
				_brandID.ToString() + ", " +
				_sessionKey.ToString() + ", " +
				_pageID.ToString() + ", " +
				_ipAddress.ToString() + ", " +
				_dateTime.ToString();
		}


		public string ToCSVLine()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(_memberID);
			sb.Append(",");
			sb.Append(_brandID);
			sb.Append(",");
			sb.Append(_sessionKey);
			sb.Append(",");
			sb.Append(_pageID);
			sb.Append(",");
			sb.Append(_ipAddress);
			sb.Append(",");
			sb.Append(_dateTime);

			for (Int32 i = 0; i < _otherInts.Length; i++)
			{
				sb.Append(",");
				sb.Append(_otherInts[i]);
			}

			return sb.ToString();
		}


		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_memberID = br.ReadInt32();
					_brandID = br.ReadInt32();
					_sessionKey = new Guid(br.ReadBytes(16));
					_pageID = br.ReadInt32();
					_ipAddress = br.ReadInt32();
					_dateTime = DateTime.FromFileTime(br.ReadInt64());

					byte otherIntsLength = br.ReadByte();
					_otherInts = (Int32[])Array.CreateInstance(typeof(Int32), otherIntsLength);
					for (byte i = 0; i < otherIntsLength; i++)
					{
						_otherInts[i] = br.ReadInt32();
					}
					
					break;

				default:
						throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(41);	//may want to increase this if we end up using _otherInts
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(_memberID);
			bw.Write(_brandID);
			bw.Write(_sessionKey.ToByteArray());
			bw.Write(_pageID);
			bw.Write(_ipAddress);
			bw.Write(_dateTime.ToFileTime());

			bw.Write((byte)_otherInts.Length);
			for (Int32 i = 0; i < _otherInts.Length; i++)
			{
				bw.Write(_otherInts[i]);
			}

			return ms.ToArray();
		}

		#endregion
	}
}
