using System;
using System.IO;
using System.Threading;

using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.PageView.BusinessLogic
{
	internal class LogFile
	{
		private byte _slice;
		private StreamWriter _sw;
		private ReaderWriterLock _lock;

		public LogFile(byte slice)
		{
			_slice = slice;
			_lock = new ReaderWriterLock();
			Int32 sliceCharLen = _slice.ToString().Length;
			string fileName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PAGEVIEWSVC_LOG_PATH") + "PV" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + "_" + new string('0', 3 - sliceCharLen) + _slice.ToString() + "0.csv";
			_sw = new StreamWriter(fileName, true);
		}


		public byte Slice
		{
			get
			{
				return _slice;
			}
		}


		public void AcquireLock()
		{
			_lock.AcquireWriterLock(-1);
		}


		public void ReleaseLock()
		{
			if (_lock.IsWriterLockHeld)
			{
				_lock.ReleaseLock();
			}
		}

		
		public void WriteLine(string line)
		{
				_sw.WriteLine(line);
		}


		public void Close()
		{
			try
			{
				AcquireLock();
				_sw.Close();
			}
			finally
			{
				ReleaseLock();
			}
		}
	}
}
