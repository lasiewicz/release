using System;
using System.Collections;
using System.Threading;

using Matchnet.Exceptions;
using Matchnet.PageView.ValueObjects;


namespace Matchnet.PageView.BusinessLogic
{
	public class PageViewBL
	{
		private const Int32 INTERVAL_SLEEP = 5000;
		private const Int32 INTERVAL_CHECK = 20000;

		private Thread _managerThread;
		private bool _runnable = true;
		private Hashtable _logFiles;
		private ReaderWriterLock _logFilesLock;


		public PageViewBL()
		{
			_logFiles = new Hashtable();
			_logFilesLock = new ReaderWriterLock();
			_managerThread = new Thread(new ThreadStart(workCycle));
		}


		public void LogImpressions(Impression[] impressions)
		{
			LogFile logFile = null;
			Impression impression = null;
			byte slice;
			byte lastSlice = 255;

			try
			{
				for (Int32 impressionNum = 0; impressionNum < impressions.Length; impressionNum++)
				{
					impression = impressions[impressionNum];
					slice = calculateSlice(impression.DateTime);
				
					if (slice != lastSlice)
					{
						if (logFile != null)
						{
							logFile.ReleaseLock();
						}
						logFile = getLogFile(slice);
						logFile.AcquireLock();
					}

					logFile.WriteLine(impression.ToCSVLine());
				}
			}
			finally
			{
				if (logFile != null)
				{
					logFile.ReleaseLock();
				}
			}
		}


		private byte calculateSlice(DateTime dateTime)
		{
			Int32 minute = dateTime.Minute + (dateTime.Hour * 60);
			minute = minute - (minute % 10);

			if (minute > 0)
			{
				return (byte)(minute / 10);
			}
			else
			{
				return 0;
			}		
		}


		private LogFile getLogFile(byte slice)
		{
			_logFilesLock.AcquireReaderLock(-1);
			try
			{
				LogFile logFile = _logFiles[slice] as LogFile;

				if (logFile == null)
				{
					_logFilesLock.UpgradeToWriterLock(-1);
					logFile = new LogFile(slice);
					_logFiles.Add(slice, logFile);
				}

				return logFile;
			}
			finally
			{
				_logFilesLock.ReleaseLock();
			}
		}


		public void Start()
		{
			_managerThread.Start();
		}


		public void Stop()
		{
			_runnable = false;
			_managerThread.Join();
		}


		private void workCycle()
		{
			DateTime lastRun = DateTime.Now;
			IDictionaryEnumerator de;
			Byte sliceA;
			Byte sliceB;
			LogFile logFile;

			while (_runnable)
			{
				Thread.Sleep(INTERVAL_SLEEP);

				if (DateTime.Now.Subtract(lastRun).TotalMilliseconds > INTERVAL_CHECK)
				{
					_logFilesLock.AcquireReaderLock(-1);
					try
					{
						sliceA = calculateSlice(DateTime.Now);
						sliceB = calculateSlice(DateTime.Now.AddSeconds(-20));
						de = _logFiles.GetEnumerator();
						while (de.MoveNext())
						{
							logFile = de.Value as LogFile;
							if (logFile.Slice != sliceA && logFile.Slice != sliceB)
							{
								logFile.Close();
								_logFiles.Remove(logFile.Slice);
								break;
							}
						}
					}
					catch (Exception ex)
					{
						new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
							"closeFiles() error.",
							ex);
					}
					finally
					{
						_logFilesLock.ReleaseLock();
					}
				}
			}

			de = _logFiles.GetEnumerator();
			while (de.MoveNext())
			{
				logFile = de.Value as LogFile;
				logFile.Close();
			}
		}

	}
}
