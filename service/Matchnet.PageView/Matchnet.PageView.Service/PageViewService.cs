using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.PageView.ValueObjects;
using Matchnet.PageView.ServiceManagers;

namespace Matchnet.PageView
{
	public class PageViewService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private PageViewSM _pageViewSM;

		public PageViewService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PageViewService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void RegisterServiceManagers()
		{
			try
			{
				_pageViewSM = new PageViewSM();
				base.RegisterServiceManager(_pageViewSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
			}
		}

	
		protected override void Dispose( bool disposing )
		{
			if(disposing)
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}
	}
}
