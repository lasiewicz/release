using System;
using System.Collections;
using System.Threading;

using Matchnet.Exceptions;
using Matchnet.TypeInterfaces;
using Matchnet.PageView.ValueObjects;
using Matchnet.PageView.ValueObjects.ServiceDefinitions;


namespace Matchnet.PageView.ServiceAdapters
{
	public class PageViewSA : IBackgroundSA
	{
		private const Int32 BACKGROUND_INTERVAL = 10;

		public static readonly PageViewSA Instance = new PageViewSA();

		private ListItem _firstItem = null;
		private ListItem _currentItem = null;
		private ReaderWriterLock _lock;


		private PageViewSA()
		{
			_firstItem = new ListItem(null);
			_currentItem = _firstItem;
			_lock = new ReaderWriterLock();
		}


		public void AddImpression(Int32 memberID,
			Int32 brandID,
			Guid sessionKey,
			Int32 pageID,
			string ipAddress,
			Int32[] otherInts)
		{
			if (pageID == 19170)
			{
				return;
			}

			ListItem listItem = new ListItem(new Impression(memberID,
				brandID,
				sessionKey,
				pageID,
				ipAddress,
				otherInts));

			_lock.AcquireWriterLock(-1);
			try
			{
				_currentItem.NextItem = listItem;
				_currentItem = listItem;
			}
			finally
			{
				_lock.ReleaseLock();
			}		
		}


		private IPageViewService getService(string uri)
		{
			try
			{
				return (IPageViewService)Activator.GetObject(typeof(IPageViewService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}

		
		#region IBackgroundSA Members
		public void DoBackgroundWork()
		{
			ArrayList impressions = new ArrayList();

			while (_firstItem.NextItem != null)
			{
				_firstItem = _firstItem.NextItem;
				impressions.Add(_firstItem.Impression);
			}

			if (impressions.Count > 0)
			{
				string uri = SMUtil.getServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);

				try
				{
					getService(uri).LogImpressions((Impression[])impressions.ToArray(typeof(Impression)));
				}
				catch (Exception ex)
				{
					throw new Exception("LogImpressions() error (uri: " + uri + ", impression count: " + impressions.Count.ToString() + ").", ex);
				}
			}
		}


		public Int32 GetInterval()
		{
			return BACKGROUND_INTERVAL;
		}
		#endregion

		internal class ListItem
		{
			private Impression _impression;
			private ListItem _nextItem = null;

			public ListItem(Impression impression)
			{
				_impression = impression;
			}


			public Impression Impression
			{
				get
				{
					return _impression;
				}
			}


			public ListItem NextItem
			{
				get
				{
					return _nextItem;
				}
				set
				{
					_nextItem = value;
				}
			}
		}
	}
}
