using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.PageView.ValueObjects;
using Matchnet.PageView.ValueObjects.ServiceDefinitions;


namespace Matchnet.PageView.ServiceAdapters
{
	internal class SMUtil
	{
		private SMUtil()
		{
		}


		internal static string getServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PAGEVIEWSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot get configuration settings for remote service manager.",ex);
			}
		}
	}
}
