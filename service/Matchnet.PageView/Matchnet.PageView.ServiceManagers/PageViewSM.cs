using System;

using Matchnet.Exceptions;
using Matchnet.PageView.BusinessLogic;
using Matchnet.PageView.ValueObjects;
using Matchnet.PageView.ValueObjects.ServiceDefinitions;


namespace Matchnet.PageView.ServiceManagers
{
	public class PageViewSM : MarshalByRefObject, IPageViewService, Matchnet.IBackgroundProcessor
	{
		private PageViewBL _pageViewBL;

		public PageViewSM()
		{
			_pageViewBL = new PageViewBL();
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		[System.Runtime.Remoting.Messaging.OneWay()]
		public void LogImpressions(Impression[] impressions)
		{
			try
			{
				_pageViewBL.LogImpressions(impressions);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"LogImpressions() error.",
					ex);
			}
		}


		#region IBackgroundProcessor Members
		public void Start()
		{
			_pageViewBL.Start();
		}


		public void Stop()
		{
			_pageViewBL.Stop();
		}
		#endregion
	}
}
