using System;
using System.IO;
using Matchnet.Astrology.ValueObjects;

namespace Matchnet.Astrology.BusinessLogic
{
	/// <summary>
	/// Summary description for AstrologyBL.
	/// </summary>
	public class AstrologyBL : IDisposable
	{
		#region Member Variables

		private System.IO.FileStream _fileStream;
		private BinaryReader _binaryReader;
		private string astrologyDataFilePath = null;
		
		private DateTime startDate; //The VERY important start date of the file
		private DateTime endDate; //The VERY important end date of data
		private int numDaysInFile;


		#endregion

		#region Constructors
		
		public static readonly AstrologyBL Instance = new AstrologyBL();
	
		public AstrologyBL()
		{
			//set the path to the data file
			astrologyDataFilePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.ASTROLOGYSVC_ASTRO_DATA_FILE_PATH);

			//set the VERY important start and end dates of the file
			startDate = Conversion.CDateTime(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.ASTROLOGYSVC_ASTRO_DATA_START_DATE),new DateTime(1920,1,1));
			endDate = Conversion.CDateTime(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.ASTROLOGYSVC_ASTRO_DATA_END_DATE),new DateTime(1989,12,31));

			//set the number of days that the file covers
			numDaysInFile = (endDate.Subtract(startDate)).Days + 1;
		}


		#endregion Constructors

		#region Properties

		public DateTime StartDate
		{
			get
			{
				return startDate;
			}
		}


		public Int32 NumDaysInFile
		{
			get
			{
				return numDaysInFile;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// This method looks in the data file for correct row according to very specific convention
		/// of 12 bytes a row (see getRowOffset) and returns an AstrologyCompatibilitySummary object.
		/// </summary>
		/// <param name="birthday1"></param>
		/// <param name="birthday2"></param>
		/// <returns>an AstrologyCompatibilitySummary object</returns>
		public AstrologyCompatibilitySummary GetAstrologyCompatibilitySummary(DateTime birthday1, DateTime birthday2) 
		{
			if (_fileStream == null)
			{
				_fileStream = new System.IO.FileStream(astrologyDataFilePath, FileMode.Open, FileAccess.Read);
			}

			if (_binaryReader == null) 
			{
				_binaryReader = new BinaryReader(_fileStream);
			}
			long rowOffset;
			int days1;
			int days2;
			int intCompatibilitySummary;
			AstrologyCompatibilitySummary s = new AstrologyCompatibilitySummary();

			lock (_fileStream)
			{
				rowOffset = getRowOffset(birthday1, birthday2);
				_binaryReader.BaseStream.Seek(rowOffset,SeekOrigin.Begin);

				days1 = _binaryReader.ReadInt32();//use this for validation
				days2 = _binaryReader.ReadInt32();//use this for validation
				intCompatibilitySummary = _binaryReader.ReadInt32();
			}
		


			//for debugging
			System.Diagnostics.Debug.WriteLine(string.Format("days1: {0} = {1} and days2 {2} = {3}, rowOffset/12: {4}",days1 ,(birthday1.Subtract(startDate)).Days + 1, days2, birthday2.Subtract(startDate).Days + 1,rowOffset/12));

			//validate that these are the days we expected to get
			System.Diagnostics.Debug.Assert(days1 == (birthday1.Subtract(startDate)).Days + 1 && days2 == (birthday2.Subtract(startDate)).Days + 1, "Days didn't match expected results",
				string.Format("days1: {0} != {1} or days2 {2} != {3}, rowOffset/12: {4}",days1 ,(birthday1.Subtract(startDate)).Days + 1, days2, birthday2.Subtract(startDate).Days + 1,rowOffset/12));
	

			string strCompatibilitySummary = intCompatibilitySummary.ToString("d8");//8 digit padded string
			

			//The numbers returned are 0-9 which by 3rd party convention corresponds to 1 through 10, where 0 = 1 and 9 = 10
			s.HarmonyIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(0,1)) + 1);
			s.TensionIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(1,1)) + 1);
			s.SunIndex =  Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(2,1)) + 1);
			s.MercuryIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(3,1)) + 1);
			s.VenusIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(4,1)) + 1);
			s.MarsIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(5,1)) + 1); 
			s.JupiterIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(6,1)) + 1);
			s.SaturnIndex = Convert.ToByte(Convert.ToInt32(strCompatibilitySummary.Substring(7,1)) + 1);

			return s;
		}

		/// <summary>
		/// Returns the offset location of the desired "birthday" row. 
		/// There are 12 bytes per row. Each row is made up of 3 four byte integers 
		/// (birthday1, birthday2, compatibilitySummary) (order matters - the file is not commutative)
		/// If the file starts with 1/1/1920 for example, row "0" would be the row that birthday2 is "1/1/1920"
		/// and row 1 would be 1/1/1920 1/2/1920 and so on all the way up to 1/1/1920 12/31/1989. The following 
		/// row would be 1/2/1920 1/1/1920, and so on.
		/// </summary>
		/// <param name="bd1"></param>
		/// <param name="bd2"></param>
		/// <returns></returns>
		private long getRowOffset(DateTime birthday1, DateTime birthday2) 
		{
			int birthday1Offset = (birthday1.Subtract(startDate)).Days;
			int birthday2Offset = (birthday2.Subtract(startDate)).Days;
			//this logic seems a bit tricky, but it is quite simple: start with the first birthday, multiply
			//it by the number of birthdays rows it is matched with, then add the the second birthday offset
			long offsetLocation =  (birthday1Offset * numDaysInFile) + birthday2Offset;
			//as decided, there are 12 bytes per row, 3 four byte integers (birthday1, birthday2, compatibilitySummary)
			return offsetLocation * 12; 
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			if (_binaryReader != null)
			{
				_binaryReader.Close();
			}
			
			if (_fileStream != null)
			{
				_fileStream.Close();
			}
		}

		#endregion
	}
}
