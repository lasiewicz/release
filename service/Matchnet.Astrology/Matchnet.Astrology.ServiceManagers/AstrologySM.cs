using System;
using Matchnet;
//using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.Astrology.BusinessLogic;
using Matchnet.Astrology.ValueObjects.ServiceDefinitions;
using Matchnet.Astrology.ValueObjects;


namespace Matchnet.Astrology.ServiceManagers
{
	/// <summary>
	/// Summary description for AstrologySM.
	/// </summary>
	public class AstrologySM : MarshalByRefObject ,IServiceManager	, IAstrologyService	, IDisposable		
	{
		public AstrologySM() 
		{

		}
		
		public override object InitializeLifetimeService()
		{
			return null;
		}

		public void PrePopulateCache()
		{

		}
	
		public AstrologyCompatibilitySummary GetAstrologyCompatibilitySummary(DateTime birthday1, DateTime birthday2)
		{
			try 
			{
				return AstrologyBL.Instance.GetAstrologyCompatibilitySummary(birthday1,birthday2);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting AstrologyCompatibilitySummary", ex);
			}
		}
	
		public void Dispose()
		{

		}
	}
}
