using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.Astrology.BusinessLogic;
using Matchnet.Astrology.ServiceManagers;
using Matchnet.Astrology.ValueObjects;
using Matchnet.RemotingServices;

namespace Matchnet.Astrology.Service
{
	public class AstrologyService : Matchnet.RemotingServices.RemotingServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private AstrologySM _astrologySM = null;
		public AstrologyService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new AstrologyService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) 
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Register the service managers for this service.
		/// </summary>
		protected override void RegisterServiceManagers()
		{
			try
			{
				// Initialize service managers.
				_astrologySM = new AstrologySM();

				// Register them.
				base.RegisterServiceManager(_astrologySM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service, see details: " + ex.Message);
			}
		}

	}
}
