using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;

using Matchnet.Astrology.ServiceAdapters;
using Matchnet.Astrology.ValueObjects;
using Matchnet;

namespace Matchnet.Astrology.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmAstrology : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnRun;
		private System.Windows.Forms.TextBox txtOutput;
		private System.Windows.Forms.TextBox txtBirthDay2;
		private System.Windows.Forms.TextBox txtBirthday1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnRunMultiple;
		private System.Windows.Forms.Button btnGetLastRow;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.Label lblCounter;
		private System.Windows.Forms.Button btnRandom;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmAstrology()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRun = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.TextBox();
			this.txtBirthDay2 = new System.Windows.Forms.TextBox();
			this.txtBirthday1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.btnRunMultiple = new System.Windows.Forms.Button();
			this.btnGetLastRow = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.lblProgress = new System.Windows.Forms.Label();
			this.lblCounter = new System.Windows.Forms.Label();
			this.btnRandom = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnRun
			// 
			this.btnRun.Location = new System.Drawing.Point(208, 8);
			this.btnRun.Name = "btnRun";
			this.btnRun.TabIndex = 0;
			this.btnRun.Text = "Run Once";
			this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(8, 72);
			this.txtOutput.Multiline = true;
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(200, 184);
			this.txtOutput.TabIndex = 1;
			this.txtOutput.Text = "";
			// 
			// txtBirthDay2
			// 
			this.txtBirthDay2.Location = new System.Drawing.Point(8, 40);
			this.txtBirthDay2.Name = "txtBirthDay2";
			this.txtBirthDay2.TabIndex = 2;
			this.txtBirthDay2.Text = "1/1/1920";
			// 
			// txtBirthday1
			// 
			this.txtBirthday1.Location = new System.Drawing.Point(8, 8);
			this.txtBirthday1.Name = "txtBirthday1";
			this.txtBirthday1.TabIndex = 3;
			this.txtBirthday1.Text = "1/1/1920";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(216, 136);
			this.button1.Name = "button1";
			this.button1.TabIndex = 4;
			this.button1.Text = "CreateFile";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnClear
			// 
			this.btnClear.Location = new System.Drawing.Point(216, 104);
			this.btnClear.Name = "btnClear";
			this.btnClear.TabIndex = 5;
			this.btnClear.Text = "Clear";
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// btnRunMultiple
			// 
			this.btnRunMultiple.Location = new System.Drawing.Point(184, 40);
			this.btnRunMultiple.Name = "btnRunMultiple";
			this.btnRunMultiple.Size = new System.Drawing.Size(104, 23);
			this.btnRunMultiple.TabIndex = 6;
			this.btnRunMultiple.Text = "Run Multiple Test";
			this.btnRunMultiple.Click += new System.EventHandler(this.btnRunMultiple_Click);
			// 
			// btnGetLastRow
			// 
			this.btnGetLastRow.Location = new System.Drawing.Point(216, 176);
			this.btnGetLastRow.Name = "btnGetLastRow";
			this.btnGetLastRow.TabIndex = 7;
			this.btnGetLastRow.Text = "GetLastRow";
			this.btnGetLastRow.Click += new System.EventHandler(this.btnGetLastRow_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(216, 208);
			this.button2.Name = "button2";
			this.button2.TabIndex = 8;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Enabled = false;
			this.button3.Location = new System.Drawing.Point(200, 272);
			this.button3.Name = "button3";
			this.button3.TabIndex = 9;
			this.button3.Text = "Merge";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(96, 272);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.TabIndex = 10;
			// 
			// lblCounter
			// 
			this.lblCounter.Location = new System.Drawing.Point(8, 272);
			this.lblCounter.Name = "lblCounter";
			this.lblCounter.TabIndex = 11;
			// 
			// btnRandom
			// 
			this.btnRandom.Location = new System.Drawing.Point(216, 72);
			this.btnRandom.Name = "btnRandom";
			this.btnRandom.TabIndex = 12;
			this.btnRandom.Text = "Run 1 Random";
			this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
			// 
			// frmAstrology
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 301);
			this.Controls.Add(this.btnRandom);
			this.Controls.Add(this.lblCounter);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.btnGetLastRow);
			this.Controls.Add(this.btnRunMultiple);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.txtBirthday1);
			this.Controls.Add(this.txtBirthDay2);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.btnRun);
			this.Name = "frmAstrology";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new frmAstrology());
		}
		private int counter = 0;

		private void btnRun_Click(object sender, System.EventArgs e)
		{
			getAstroCompatibilitySummary(Convert.ToDateTime(this.txtBirthday1.Text),Convert.ToDateTime(this.txtBirthDay2.Text));
		}

		private void btnRunMultiple_Click(object sender, System.EventArgs e)
		{
			DateTime birthday1 = new DateTime(1900,1,1);
			DateTime birthday2 = new DateTime(1900,1,1);

			for (int i = 0;i<10;i++) 
			{
				System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(astroTest));
				t.Start();
				
			}
		}

		private void astroTest()
		{
			Int32 numTrials = 1000;

			for (Int32 i = 0; i < numTrials; i++)
			{
				runRandomReport();
				//System.Threading.Thread.Sleep(10);
			}
			//MessageBox.Show("One thread done");
		}

		private void runRandomReport() 
		{
			//set the VERY important start and end dates of the file
			DateTime startDate = Conversion.CDateTime(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.ASTROLOGYSVC_ASTRO_DATA_START_DATE),new DateTime(1920,1,1));
			DateTime endDate = Conversion.CDateTime(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.ASTROLOGYSVC_ASTRO_DATA_END_DATE),new DateTime(1989,12,31));

			//set the number of days that the file covers
			int numDaysInFile = (endDate.Subtract(startDate)).Days + 1;

			Random r = new Random();

			DateTime birthday1 = startDate.AddDays(r.Next(numDaysInFile - 1));
			DateTime birthday2 = startDate.AddDays(r.Next(numDaysInFile - 1));
			getAstroCompatibilitySummary(birthday1, birthday2);

		}

		private void btnGetLastRow_Click(object sender, System.EventArgs e)
		{
			throw new NotImplementedException();
		}

		private void getAstroCompatibilitySummary(DateTime birthday1, DateTime birthday2) 
		{
			AstrologyCompatibilitySummary astro = AstrologySA.Instance.GetAstrologyCompatibilitySummary(birthday1,birthday2);
			counter ++;

			lblCounter.Text = counter.ToString();
			this.txtOutput.Text = "astro.HarmonyIndex " + astro.HarmonyIndex + "\r\n";
			this.txtOutput.Text += "astro.TensionIndex " + astro.TensionIndex + "\r\n";
			this.txtOutput.Text += "astro.SunIndex " + astro.SunIndex + "\r\n";
			this.txtOutput.Text += "astro.MercuryIndex " + astro.MercuryIndex + "\r\n";
			this.txtOutput.Text += "astro.VenusIndex " + astro.VenusIndex + "\r\n";
			this.txtOutput.Text += "astro.MarsIndex " + astro.MarsIndex + "\r\n";
			this.txtOutput.Text += "astro.JupiterIndex " + astro.JupiterIndex + "\r\n";
			this.txtOutput.Text += "astro.SaturnIndex " + astro.SaturnIndex + "\r\n";

		}

		/// <summary>
		/// temporary to create dummy binary file for testing purposes.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button1_Click(object sender, System.EventArgs e)
		{
			string FILE_NAME = "test.dat";
			// Create the new, empty data file.
			if (File.Exists(FILE_NAME)) 
			{
				File.Delete(FILE_NAME);
				Console.WriteLine("{0} already exists!", FILE_NAME);
				//return;
			}
			FileStream fs = new FileStream(FILE_NAME, FileMode.CreateNew);
			// Create the writer for data.
			BinaryWriter w = new BinaryWriter(fs);
			// Write data to Test.data.
			//w.Seek(3,SeekOrigin.Begin);
			for (int i = 1; i < 730; i++) 
			{
				for (int j = 1; j < 730; j++) 
				{
					w.Write((int) i);
					w.Write((int) j);
					w.Write(Convert.ToInt32((99999999 - j).ToString()));
				}
			}
			w.Close();
			fs.Close();
			MessageBox.Show("done");

		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			this.txtOutput.Text = string.Empty;
			this.counter = 0;
			this.lblCounter.Text = string.Empty;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show(((int)34).ToString("d8"));
		}


		private void button3_Click(object sender, System.EventArgs e)
		{
			MergeFiles();
		}

		/// <summary>
		/// Reads B and appends B to a copy of A.
		/// </summary>
		private void MergeFiles() 
		{

			string FILE_NAME_A = @"..\..\ta_cs01_1920A_dat\ta_cs01_1920A.dat";
			string FILE_NAME_B = @"..\..\ta_cs01_1920A_dat\ta_cs01_1920B.dat";
			string FILE_NAME_MERGED = @"..\..\ta_cs01_1920A_dat\AstroData_1-1-1920_thru_12-31-99.dat";

			// Create the new, empty data file.
			if (File.Exists(FILE_NAME_MERGED)) 
			{
				//File.Delete(FILE_NAME_MERGED);
				MessageBox.Show("{0} already exists!", FILE_NAME_MERGED);
			}
			else 
			{

				File.Copy(FILE_NAME_A, FILE_NAME_MERGED);
				MessageBox.Show("copied " + FILE_NAME_A + " into " + FILE_NAME_MERGED);
			}

			FileStream fs = new FileStream(FILE_NAME_MERGED, FileMode.Append);
			FileStream fsB = new FileStream(FILE_NAME_B, FileMode.Open);

			// Create the writer for data.
			BinaryWriter w = new BinaryWriter(fs);
			BinaryReader r = new BinaryReader(fsB);

			int i = 0;
			while(r.PeekChar() != -1) 
			{
				w.Write(r.ReadBytes(104857600));
				if (i==100) {this.lblProgress.Text = (fsB.Position / fsB.Length).ToString("p");i=0;}
				i++;
			}

			w.Close();
			r.Close();
			fs.Close();
			fsB.Close();
			MessageBox.Show("done");

		}

		private void btnRandom_Click(object sender, System.EventArgs e)
		{
			this.runRandomReport();
		}





	}
}

