--new Astrology Service
INSERT INTO [mnSystem].[dbo].[Service]([ServiceID], [ServiceConstant], [ServiceDescription], [ChannelTypeID], [ServiceReplicationModeID])
VALUES(62000, 'ASTROLOGY_SVC', 'the Astrology Service', 1, 1)

INSERT INTO [mnSystem].[dbo].[ServicePartition]([ServicePartitionID], [ServiceID], [PartitionOffset], [PartitionExclusiveUri], [PartitionSharedUri], [PartitionUri], [PartitionPort])
VALUES(62000, 62000, 0, '', '', 'devapp01', 62000)

INSERT INTO [mnSystem].[dbo].[ServiceInstance]([ServiceInstanceID], [ServicePartitionID], [ServerID], [IsEnabled])
VALUES(62000, 62000, 3000, 1)

INSERT INTO [mnSystem].[dbo].[ServiceInstance]([ServiceInstanceID], [ServicePartitionID], [ServerID], [IsEnabled])
VALUES(62001, 62000, 8000, 1)

-- new settings for Astrology Service
INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62000, 7, 'ASTROLOGYSVC_SA_OVERRIDE', '', 'host override', 0, 0, getDate())

INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62001, 7, 'ASTROLOGYSVC_CACHE_TTL_ASTROLOGYREPORT_SA', '600', 'cache time to live in seconds for the AstrologyCompatibility value object on web server', 0, 0, getDate())

INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62002, 7, 'ASTROLOGYSVC_SA_CONNECTION_LIMIT', '10', 'max connections per URI', 0, 0, getDate())

INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62003, 7, 'ASTROLOGYSVC_ASTRO_DATA_FILE_PATH', 'D:\data\AstroData_1-1-1920_thru_12-31-1989.dat', 'the data file that contains the Astrology data. IMPORTANT! If path is wrong youll get service not found errors!', 0, 0, getDate())

INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62004, 7, 'ASTROLOGYSVC_ASTRO_DATA_START_DATE', '1/1/1920', 'the start date of the data file that contains the Astrology data', 0, 0, getDate())

INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62005, 7, 'ASTROLOGYSVC_ASTRO_DATA_END_DATE', '12/31/1989', 'the end date of the data file that contains the Astrology data', 0, 0, getDate())

-- new setting
INSERT INTO [mnSystem].[dbo].[Setting]([SettingID], [SettingCategoryID], [SettingConstant], [GlobalDefaultValue], [SettingDescription], [IsRequiredForCommunity], [IsRequiredForSite], [CreateDate])
VALUES(62006, 7, 'ASTROLOGYSVC_ENABLE', 'true', 'true/false whether to use the Middle Tier Astrology service. If false, then the web solution will make http requests to 3rd party astrology site for content', 0, 0, getDate())

