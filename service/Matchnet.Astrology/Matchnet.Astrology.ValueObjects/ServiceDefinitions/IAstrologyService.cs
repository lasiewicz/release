using System;

namespace Matchnet.Astrology.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IAstrologyService.
	/// </summary>
	public interface IAstrologyService
	{
		/// <summary>
		/// Looks in data file for correct row and returns an AstrologyCompatibilitySummary object.
		/// Note: should try to retrieve results from cache before remoting call.
		/// </summary>
		/// <param name="birthday1"></param>
		/// <param name="birthday2"></param>
		/// <returns></returns>
		AstrologyCompatibilitySummary GetAstrologyCompatibilitySummary(DateTime birthday1, DateTime birthday2);
	}
}
