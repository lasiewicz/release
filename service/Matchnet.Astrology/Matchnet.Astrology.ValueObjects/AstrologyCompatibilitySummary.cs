using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.Astrology.ValueObjects
{
	/// <summary>
	/// Structure for holding the results of AstrologyCompatibilitySummary.
	/// </summary>
	[Serializable()]
	public class AstrologyCompatibilitySummary : IValueObject, ICacheable, ISerializable, IByteSerializable
	{
		public AstrologyCompatibilitySummary()
		{

		}
		private const string CACHE_KEY_FORMAT = "~ASTROLOGYCOMPATSUMMARY^{0}^{1}";
		private const byte VERSION_001 = 1;

		#region Member Variables

		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private int _cacheTTLSeconds;

		private DateTime _birthday1;
		private DateTime _birthday2;
		private byte _harmonyIndex;//1
		private byte _tensionIndex;//2
		private byte _sunIndex; //3
		private byte _mercuryIndex; //4
		private byte _venusIndex; //5
		private byte _marsIndex; //6
		private byte _jupiterIndex; //7
		private byte _saturnIndex; //8
 
		#endregion

		#region Properties 

		public DateTime Birthday1 {get {return _birthday1;}set {_birthday1 = value;}}
		public DateTime Birthday2 {get {return _birthday2;}set {_birthday2 = value;}}

		public byte HarmonyIndex
		{
			get
			{
				return _harmonyIndex;
			}
			set
			{
				_harmonyIndex = value;
			}
		}

		public byte TensionIndex
		{
			get
			{
				return _tensionIndex;
			}
			set
			{
				_tensionIndex = value;
			}
		}

		public byte SunIndex {get{return _sunIndex;} set {_sunIndex= value;}}
		public byte MercuryIndex{get{return _mercuryIndex;} set {_mercuryIndex= value;}}
		public byte VenusIndex{get{return _venusIndex;} set {_venusIndex = value;}}
		public byte MarsIndex{get{return _marsIndex;} set {_marsIndex= value;}} 
		public byte JupiterIndex{get{return _jupiterIndex;} set {_jupiterIndex = value;}}
		public byte SaturnIndex{get{return _saturnIndex;} set {_saturnIndex= value;}} 

		#endregion Properties

		public static string GetCacheKey(DateTime birthday1, DateTime birthday2)
		{
			return String.Format(CACHE_KEY_FORMAT, birthday1, birthday2);
		}
		
		public static string GetCacheKey(AstrologyCompatibilitySummary astroCompatSummary)
		{
			return AstrologyCompatibilitySummary.GetCacheKey(astroCompatSummary.Birthday1, astroCompatSummary.Birthday2);
		}
		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return  Matchnet.CacheItemMode.Sliding;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		string Matchnet.ICacheable.GetCacheKey()
		{
			// TODO:  Add AstrologyCompatibilitySummary.Matchnet.ICacheable.GetCacheKey implementation
			return AstrologyCompatibilitySummary.GetCacheKey(this.Birthday1,this.Birthday2);
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			FromBytes(new BinaryReader(ms));
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(GetByteCount());
			BinaryWriter bw = new BinaryWriter(ms);

			ToBytes(bw);

			return ms.ToArray();
		}

		#endregion

		#region ISerializable Members

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", ToByteArray());
		}

		#endregion

		#region Serialization helper methods

		public void ToBytes(BinaryWriter bw)
		{
			bw.Write(VERSION_001);
			bw.Write((Int32) _cachePriority);
			bw.Write(_cacheTTLSeconds);
			bw.Write(_birthday1.Ticks);
			bw.Write(_birthday2.Ticks);
			bw.Write(_harmonyIndex);
			bw.Write(_tensionIndex);
			bw.Write(_sunIndex);
			bw.Write(_mercuryIndex);
			bw.Write(_venusIndex);
			bw.Write(_marsIndex);
			bw.Write(_jupiterIndex);
			bw.Write(_saturnIndex);
		}

		public void FromBytes(BinaryReader br)
		{
			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_cachePriority = (CacheItemPriorityLevel) br.ReadInt32();
					_cacheTTLSeconds = br.ReadInt32();
					_birthday1 = new DateTime(br.ReadInt64());
					_birthday2 = new DateTime(br.ReadInt64());
					_harmonyIndex = br.ReadByte();
					_tensionIndex = br.ReadByte();
					_sunIndex = br.ReadByte();
					_mercuryIndex = br.ReadByte();
					_venusIndex = br.ReadByte();
					_marsIndex = br.ReadByte();
					_jupiterIndex = br.ReadByte();
					_saturnIndex = br.ReadByte();
					break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		protected AstrologyCompatibilitySummary(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public Int32 GetByteCount()
		{
			return 33;
		}

		#endregion
	}
}
