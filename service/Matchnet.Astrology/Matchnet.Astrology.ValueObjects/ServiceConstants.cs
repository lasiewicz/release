using System;

namespace Matchnet.Astrology.ValueObjects
{
	/// <summary>
	/// Summary description for ServiceConstants.
	/// </summary>
	public class ServiceConstants
	{
		private ServiceConstants()
		{
		}

		/// <summary>
		/// Service name (Matchnet.Astrology.Service)
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.Astrology.Service";

		/// <summary>
		/// Service Constant (ASTROLOGY_SVC)
		/// </summary>
		public const string SERVICE_CONSTANT = "ASTROLOGY_SVC";
		
		public const string SERVICE_SA_OVERRIDE = "ASTROLOGYSVC_SA_OVERRIDE";

		public const string ASTROLOGYSVC_ASTRO_DATA_FILE_PATH = "ASTROLOGYSVC_ASTRO_DATA_FILE_PATH";
		public const string ASTROLOGYSVC_ASTRO_DATA_START_DATE = "ASTROLOGYSVC_ASTRO_DATA_START_DATE";
		public const string ASTROLOGYSVC_ASTRO_DATA_END_DATE = "ASTROLOGYSVC_ASTRO_DATA_END_DATE";
		public const string ASTROLOGYSVC_CACHE_TTL_ASTROLOGYREPORT_SA = "ASTROLOGYSVC_CACHE_TTL_ASTROLOGYREPORT_SA";
	}
}