using System;
using Matchnet.Astrology.ValueObjects;
using Matchnet.Astrology.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingClient;
using Matchnet.Exceptions;


namespace Matchnet.Astrology.ServiceAdapters
{
	/// <summary>
	/// Summary description for AstrologySA.
	/// </summary>
	public class AstrologySA : Matchnet.RemotingClient.SABase
	{

		private const string SERVICE_MANAGER_NAME	= "AstrologySM";

		#region Constructors
		
		public static readonly AstrologySA Instance = new AstrologySA();

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Gets an AstrologyCompatibilitySummary for given birthdays. Tries to get it from web cache
		/// first, if not from middle tier data file for correct row and returns an AstrologyCompatibilitySummary object.
		/// </summary>
		/// <param name="birthday1"></param>
		/// <param name="birthday2"></param>
		/// <returns></returns>
		public AstrologyCompatibilitySummary GetAstrologyCompatibilitySummary(DateTime birthday1, DateTime birthday2) 
		{
			string cacheKey = AstrologyCompatibilitySummary.GetCacheKey(birthday1, birthday2);
			
			//AstrologyCompatibilitySummary astroCompatSummary = System.Web.HttpRuntime.Cache[cacheKey] as AstrologyCompatibilitySummary;
			AstrologyCompatibilitySummary astroCompatSummary = Matchnet.Caching.Cache.Instance[cacheKey] as AstrologyCompatibilitySummary;
			if (astroCompatSummary == null) 
			{
				//remoting call to service here
				//astroCompatSummary = AstrologyBL.Instance.GetAstrologyCompatibilitySummary(birthday1, birthday2);
				string uri = null;
				try
				{
					uri = getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						astroCompatSummary = getService(uri).GetAstrologyCompatibilitySummary(birthday1,birthday2);
					}
					finally
					{
						base.Checkin(uri);
					}
				}
				catch(Exception ex)
				{
					throw(new SAException("Error occurred while attempting to AstrologyCompatibilitySummary. (uri: " + uri + ")", ex));
				} 
				
				//Cache for configurable time minutes
				//System.Web.HttpRuntime.Cache.Add(cacheKey,astroCompatSummary,null,System.Web.Caching.Cache.NoAbsoluteExpiration,TimeSpan.FromMinutes(20),System.Web.Caching.CacheItemPriority.Normal,null);
				astroCompatSummary.CacheTTLSeconds = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.ASTROLOGYSVC_CACHE_TTL_ASTROLOGYREPORT_SA));
				Matchnet.Caching.Cache.Instance.Add(astroCompatSummary);
			}

			return astroCompatSummary;

		}

		#endregion



		#region Remoting implementation

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ASTROLOGYSVC_SA_CONNECTION_LIMIT"));
		}

		private IAstrologyService getService(string uri)
		{
			try
			{
				return (IAstrologyService)Activator.GetObject(typeof(IAstrologyService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.SERVICE_SA_OVERRIDE);

				if (overrideHostName.Trim().Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		#endregion
	}
}
