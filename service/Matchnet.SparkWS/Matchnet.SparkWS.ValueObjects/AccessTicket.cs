using System;
using Matchnet;
using Matchnet.Caching;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;

/// <summary>
/// Represents the settings and state of the access mode of the service client
/// </summary>
[Serializable()]
public class AccessTicket : Matchnet.ICacheable
{
   public const string DEFAULT_TICKET = "*** DEFAULT NO IDENTITY! ***";
   /// <summary>
   /// Signales whether this ticket should be allowed at all or not
   /// </summary>
   public bool IsBlocked;

   /// <summary>
   /// The permission roll this client is a memebr of.
   /// </summary>
   /// <remarks>Roll membership is used in declerative security, to only allow certain rolls access to some functions</remarks>
   public ApiAccessRoll AccessRoll;

   /// <summary>
   /// A collection of profile sections that the client can see and use
   /// </summary>
   public Dictionary<int,ProfileSectionDefinition> ProfileSectionDefinitions;

   /// <summary>
   /// List of remote hosts that are allowed access. If the IP of the remote host is not in this list, the 
   /// ticket would not be allowed.
   /// </summary>
   public string [] RemoteHosts;


   /// <summary>
   /// The GUID key gicen to the client for authentication
   /// </summary>
   private string _Key;

   #region ICachable
   private CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
   private int _CachTTLSeconds = 60 * 60;
   private CacheItemMode _CacheItemMode = CacheItemMode.Sliding;
   #endregion

   /// <summary>
   /// Constructor.
   /// </summary>
   /// <remarks>Creates an access ticket with bunk ticket string, blocked state 
   /// with most general access roll.
   /// Methods that permit this ticket are considered permit anonymous user action.
   /// </remarks>
   public AccessTicket()
   {

	  _Key = DEFAULT_TICKET;
	  IsBlocked = true;
	  AccessRoll = ApiAccessRoll.Anyone;
	  ProfileSectionDefinitions = new Dictionary<int, ProfileSectionDefinition>();
   }

   /// <summary>
   /// Constructor
   /// </summary>
   /// <param name="accessTicket">The key GUID for this ticket</param>
   /// <param name="isBlocked">Flag whether this ticket is blocked or not</param>
   /// <param name="accessRoll">Permission roll for this ticket</param>
   public AccessTicket(string accessTicket, bool isBlocked, ApiAccessRoll accessRoll)
   {
	  _Key = accessTicket;
	  IsBlocked = isBlocked;
	  AccessRoll = accessRoll;
	  ProfileSectionDefinitions = new Dictionary<int, ProfileSectionDefinition>();
   }

   #region ICachable Members


   public object AddToCache()
   {
	  return Cache.Instance.Add(this);
   }

   public static AccessTicket GetFromCache(System.Web.Caching.Cache cacheReference, string ticketKey)
   {
	  return cacheReference[ticketKey] as AccessTicket;
   }

   #endregion

   #region ICacheable Members

   public Matchnet.CacheItemMode CacheMode
   {
	  get { return _CacheItemMode; }
   }

   public Matchnet.CacheItemPriorityLevel CachePriority
   {
	  get
	  {
		 return _CacheItemPriorityLevel;
	  }
	  set
	  {
		 _CacheItemPriorityLevel = value;
	  }
   }

   public int CacheTTLSeconds
   {
	  get
	  {
		 return _CachTTLSeconds;
	  }
	  set
	  {
		 _CachTTLSeconds = value;
	  }
   }

   public string GetCacheKey()
   {
	  return _Key;
   }

   #endregion

   public override string ToString()
   {
	  StringBuilder sb = new StringBuilder(256);
	  sb.Append(_Key);
	  sb.Append("\t");
	  sb.Append(AccessRoll);
	  sb.Append("\t");
	  sb.Append(IsBlocked);
	  sb.Append(":\n");
	  foreach (ProfileSectionDefinition psd in ProfileSectionDefinitions.Values)
	  {
		 sb.Append(psd.ToString());
		 sb.Append("\n");
	  }
	  return sb.ToString();
   }
}
