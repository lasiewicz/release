using System;
using System.Collections.Generic;
using System.Text;

public class MemberSiteInfo
{
   public int SiteID;
   public DateTime SubscriptionExpirationDate;
   public MemberSiteInfo() { }
   public MemberSiteInfo(int siteID, DateTime susbcriptionExpirationDate) {
	  SiteID = siteID;
	  SubscriptionExpirationDate = susbcriptionExpirationDate;
   }

}