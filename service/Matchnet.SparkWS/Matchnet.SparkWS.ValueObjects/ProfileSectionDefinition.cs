using System;
using System.Web;
using Matchnet;
using System.Text;

[Serializable()]
public class ProfileSectionDefinition : Matchnet.ICacheable
{
   private int _ProfileSectionID;
   public MemberAttributeParameters[] AttributeDefinitions;
   public int [] SiteIDList;
   public int [] CommunityIDList;
   public int [] PhotoCommunityIDList;

   private string _Key;
   private CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
   private int _CachTTLSeconds = 60 * 60;
   private CacheItemMode _CacheItemMode = CacheItemMode.Sliding;


   public ProfileSectionDefinition() { }

   public ProfileSectionDefinition(int profileSectionID, MemberAttributeParameters[] attributeDefinitions)
   {
	  _ProfileSectionID = profileSectionID;
	  AttributeDefinitions = attributeDefinitions;
	  _Key = GetCacheKey(profileSectionID);
   }

   public int ProfileSectionID {
	  get { return _ProfileSectionID; }
	  set
	  {
		 _ProfileSectionID = value;
		 _Key = GetCacheKey(_ProfileSectionID);
	  }
   }
   #region ICacheable Members
   public Matchnet.CacheItemMode CacheMode
   {
	  get { return _CacheItemMode; }
   }

   public Matchnet.CacheItemPriorityLevel CachePriority
   {
	  get
	  {
		 return _CacheItemPriorityLevel;
	  }
	  set
	  {
		 _CacheItemPriorityLevel = value;
	  }
   }

   public int CacheTTLSeconds
   {
	  get
	  {
		 return _CachTTLSeconds;
	  }
	  set
	  {
		 _CachTTLSeconds = value;
	  }
   }

   public string GetCacheKey()
   {
	  return _Key;
   }
   #endregion

   public ProfileSectionDefinition AddToCache()
   {
	  return Matchnet.Caching.Cache.Instance.Add(this) as ProfileSectionDefinition ;
   }

   public ProfileSectionDefinition GetFromCache(int profileSectionID) {
	  return Matchnet.Caching.Cache.Instance.Get(GetCacheKey(profileSectionID)) as ProfileSectionDefinition;
   }

   public static string GetCacheKey(int profileSectionID)
   {
	  return "SWS_PSD_" + profileSectionID.ToString();
   }

   public override string ToString()
   {
	  StringBuilder sb = new StringBuilder(256);
	  sb.Append(_Key); sb.Append(":\n");
	  for (int i = 0; i < AttributeDefinitions.Length; i++)
	  {
		 sb.Append(AttributeDefinitions[i].AttributeID);
		 sb.Append("|");
		 sb.Append(AttributeDefinitions[i].DataType);
		 sb.Append("|");
		 sb.Append(AttributeDefinitions[i].CommunityID);
		 sb.Append("|");
		 sb.Append(AttributeDefinitions[i].SiteID);
		 sb.Append("|");
		 sb.Append(AttributeDefinitions[i].BrandID);
		 sb.Append("|");
		 sb.Append(AttributeDefinitions[i].LanguageID);
		 sb.Append("\n");
	  }
	  return sb.ToString();
   }

}
