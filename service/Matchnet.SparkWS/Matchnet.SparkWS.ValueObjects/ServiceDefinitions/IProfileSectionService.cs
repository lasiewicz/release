using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.SparkWS.ValueObjects.ServiceDefinitions
{
   public interface IProfileSectionService
   {
	  ProfileSectionDefinition GetProfileSectionDefinition(int profileSectionID);
	  Dictionary<int, ProfileSectionDefinition> GetProfileSectionDefinitions(int[] profileSectionIDs);
   }


}
