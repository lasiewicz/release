using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.SparkWS.ValueObjects.ServiceDefinitions
{
   public interface IAccessTicketService
   {
	  AccessTicket GetAccessTicket(string ticketKey);
   }
}
