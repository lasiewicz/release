using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;


[Serializable()]
[  XmlInclude(typeof(List<MemberCommunityInfo>)),
   XmlInclude(typeof(List<MemberSiteInfo>))]
public class MemberInfo : VersionedVO
{
   public List<MemberSiteInfo> Sites;
   public List<MemberCommunityInfo> Communities;

   public MemberInfo()
	  : base("1.0.0")
   {
   }
}

