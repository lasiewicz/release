using System;
using System.Collections.Generic;
using System.Xml.Serialization;

/// <summary>
/// Contains a subset of member profile data.
/// </summary>
/// <remarks>A profile section is defined for a specific consumer. By defining profile section(s), only the 
/// allowed and relevant data gets exposed to a consumer, which reduces chatter and increases security.</remarks>
/// 
[Serializable()]
[XmlInclude(typeof(List<MemberPhoto>))]
public class ProfileSection : VersionedVO
{
   /// <summary>
   /// The ID of this instance.
   /// </summary>
   /// <remarks>The ID of a profile section is associated with a unique consumer. 
   /// If 2 consumers are interested in the same section, a definition needs to 
   /// be cloned, assigned a new ID and assigned to the new consumer.</remarks>
   public int ProfileSectionID;

   /// <summary>
   /// The member's ID
   /// </summary>
   public int MemberID;

   /// <summary>
   /// The collection of values for this section.
   /// </summary>
   public MemberAttributes Profile;

   /// <summary>
   /// Photos for defined communities
   /// </summary>
   public List<MemberPhoto> Photos;

   /// <summary>
   /// Membership info
   /// </summary>
   public MemberInfo Membership;

   /// <summary>
   /// Constructor
   /// </summary>
   public ProfileSection()
	  : base("1.0.0")
   {

   }

   /// <summary>
   /// Constructor
   /// </summary>
   /// <param name="capacity">The number of expected items in the section. 
   /// Use this constructor as much as possible.</param>
   public ProfileSection(int capacity)
	  : this()
   {
	  Profile = new MemberAttributes(capacity);
   }
}
