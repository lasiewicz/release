using System;


/// <summary>
/// Summary description for Enumerations
/// </summary>

[Flags()]
public enum ApiAccessRoll : int
{
   /// <summary>
   /// Non specific, most restricted roll
   /// </summary>
   Anyone = 0,
   /// <summary>
   /// The roll member is a Spark service or middle tier
   /// </summary>
   Internal = 1,
   /// <summary>
   /// The roll member is a Spark developer
   /// </summary>
   Developer = 2,
   /// <summary>
   /// The roll member is a Spark subsidiary or division
   /// </summary>
   Subsidiary = 4,
   /// <summary>
   /// The roll member is 3rd party to Spark
   /// </summary>
   Affiliate = 8,
   /// <summary>
   /// The roll member is a member of every other roll
   /// </summary>
   SuperUser = Int32.MaxValue - 1
}

public enum AttributeDataType : int
{
   Undefined = 0,
   Int = 1,
   Date = 2,
   String = 4,
   Boolean = 8
}
