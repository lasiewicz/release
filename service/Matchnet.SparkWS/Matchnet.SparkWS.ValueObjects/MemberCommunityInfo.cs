using System;
using System.Collections.Generic;
using System.Text;

public class MemberCommunityInfo
{

   public int CommunityID;
   public bool IsSelfSuspended;

   public MemberCommunityInfo() { }
   public MemberCommunityInfo(int communityID, bool isSelfSuspended) {
	  CommunityID = communityID;
	  IsSelfSuspended = isSelfSuspended;
   }

}