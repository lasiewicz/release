using System;
using System.Collections;
using System.Xml.Serialization;

/// <summary>
/// Represents a collection of member attribute values
/// </summary>
[Serializable()]
[  XmlInclude(typeof(MemberAttribute<int>)), 
   XmlInclude(typeof(MemberAttribute<DateTime>)), 
   XmlInclude(typeof(MemberAttribute<string>)), 
   XmlInclude(typeof(MemberAttribute<bool>)),
   XmlInclude(typeof(MemberAttribute<string[]>))
]
public class MemberAttributes
{
    private ArrayList _Values;
    public MemberAttributes() { }
    public MemberAttributes(int numberOfItems)
    {
        _Values = new ArrayList(numberOfItems);
    }

    public int Add(object memberAttribute)
    {
        return _Values.Add(memberAttribute);
    }

    public ArrayList Values
    {
        get { return _Values; }
        set { _Values = value; }
    }
}
