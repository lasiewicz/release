using System;

/// <summary>
/// Represents a value of a specific member's attribute
/// </summary>
[Serializable()]
public class MemberAttribute<T>
{
    public int AttributeID;
    public int CommunityID = 0;
    public int SiteID = 0;
    public int BrandID = 0;
    public T Value;

    public MemberAttribute(){ }

    public MemberAttribute(MemberAttributeParameters parameters, T value)
    {
        AttributeID = parameters.AttributeID;
        CommunityID = parameters.CommunityID;
        SiteID = parameters.SiteID;
        BrandID = parameters.BrandID;
        Value = value;
    }
}

