using System;


/// <summary>
/// Represents a request for profile attributes / values
/// </summary>
[Serializable()]
public class ProfileSectionRequest : VersionedVO
{
   public int ProfileSectionID = 0;
   public int MemberID = 0;
   public string MemberSessionID = string.Empty;

   internal ProfileSectionRequest()
	  : base("0.0.1")
   {
   }

   public ProfileSectionRequest(string version) : base(version) { }

   public ProfileSectionRequest(string version, int profileSectionID, int memberID)
	  : this(version)
   {
	  ProfileSectionID = profileSectionID;
	  MemberID = memberID;
   }

   public ProfileSectionRequest(string version, int profileSectionID, string memberSessionID)
	  : this(version)
   {
	  ProfileSectionID = profileSectionID;
	  MemberSessionID = memberSessionID;
   }
}
