using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for MemberPhoto
/// </summary>
[Serializable()]
public class MemberPhoto : VersionedVO
{
   public int CommunityID;

   /// <summary>
   /// Full path to photo
   /// </summary>
   public string PhotoURL;
   /// <summary>
   /// Full path to thumbnail
   /// </summary>
   public string ThumbnailURL;
   /// <summary>
   /// Photo marked as private. view by invitation only.
   /// </summary>
   public bool IsPrivate;
   /// <summary>
   /// Member allows photo to be viewed by non members (guests)
   /// </summary>
   public bool IsMembersOnly;

   /// <summary>
   /// The order of the photo, relative to other photos for this community
   /// </summary>
   public int ListOrder;

   public MemberPhoto()
	  : base("1.0.0")
   {

   }

   public MemberPhoto(int communityID, string photoURL, string thumbnailURL, bool isPrivate, bool isMembersOnly, int listOrder)
	  : this()
   {
	  PhotoURL = photoURL;
	  ThumbnailURL = thumbnailURL;
	  IsPrivate = isPrivate;
	  IsMembersOnly = isMembersOnly;
	  ListOrder = listOrder;
   }
}
