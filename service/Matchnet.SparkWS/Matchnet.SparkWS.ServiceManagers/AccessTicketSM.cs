using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.SparkWS.ValueObjects;
using Matchnet.SparkWS.ValueObjects.ServiceDefinitions;
using Matchnet.SparkWS.BusinessLogic;
using Matchnet.Exceptions;
using Matchnet.Caching;

namespace Matchnet.SparkWS.ServiceManagers
{
   public class AccessTicketSM : MarshalByRefObject, IServiceManager, IAccessTicketService
   {
	  #region IServiceManager Members

	  public void PrePopulateCache()
	  {
	  }

	  #endregion

	  #region IDisposable Members

	  public void Dispose()
	  {
	  }

	  #endregion


	  #region IAccessTicketService Members

	  public AccessTicket GetAccessTicket(string ticketKey)
	  {
		 try
		 {
			AccessTicket result = null;
			result = Matchnet.Caching.Cache.Instance.Get(ticketKey) as AccessTicket;
			if (result == null)
			{
			   result = AccessTicketBL.Instance.GetAccessTicket(ticketKey);
			   if (result != null)
			   {
				  result.AddToCache();
			   }
			   else
			   {
				  ///TODO: Protect against multi calls. If failed to get object, complain and  cache an empty one?
				  new Matchnet.Exceptions.ServiceBoundaryException("AccessTicketSM", "Can't get AccessTicket " + ticketKey);
			   }
			}
			return result;		
		 }
		 catch (ExceptionBase ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting AccessTicket " + ticketKey, ex);
		 }
		 catch (Exception ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting AccessTicket " + ticketKey, ex);
		 }
	  }

	  #endregion

	  public override object InitializeLifetimeService()
	  {
		 return null;
	  }
   }

}
