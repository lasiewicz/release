using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Matchnet.SparkWS.ServiceAdapters;
using Matchnet.SparkWS.ServiceManagers;
using Matchnet.SparkWS.ValueObjects;
using Matchnet.SparkWS.BusinessLogic;

namespace Matchnet.SparkWS.TestHarness
{
   /// <summary>
   /// Summary description for UnitTest1
   /// </summary>
   [TestClass]
   public class UnitTest1
   {

	  #region Additional test attributes
	  //
	  // You can use the following additional attributes as you write your tests:
	  //
	  // Use ClassInitialize to run code before running the first test in the class
	  // [ClassInitialize()]
	  // public static void MyClassInitialize(TestContext testContext) { }
	  //
	  // Use ClassCleanup to run code after all tests in a class have run
	  // [ClassCleanup()]
	  // public static void MyClassCleanup() { }
	  //
	  // Use TestInitialize to run code before running each test 
	  // [TestInitialize()]
	  // public void MyTestInitialize() { }
	  //
	  // Use TestCleanup to run code after each test has run
	  // [TestCleanup()]
	  // public void MyTestCleanup() { }
	  //
	  #endregion

	  [TestMethod]
	  public void TestProfileSectionSA()
	  {
		 ProfileSectionDefinition psd = ProfileSectionSA.Instance.GetProfileSectionDefinition(123);
		 Console.WriteLine(psd.ToString());
	  }

	  [TestMethod]
	  public void TestAccessTicketSA()
	  {
		 AccessTicket at;
		 at = AccessTicketSA.Instance.GetAccessTicket("ABCDEFG1234567");
		 Console.WriteLine(at.ToString());

	  }

	  [TestMethod]
	  public void TestAccessTicketBL()
	  {
		 AccessTicket at;
		 at = AccessTicketBL.Instance.GetAccessTicket("FOOBAR-990");
		 Console.WriteLine(at.ToString());
	  }


	  [TestMethod]
	  public void TestAccessTicketSM()
	  {
		 AccessTicket at;
		 AccessTicketSM sm = new AccessTicketSM();
		 
		 at = sm.GetAccessTicket("FOOBAR-990");
		 Console.WriteLine(at.ToString());
	  }

   }
}
