using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

using Matchnet.SparkWS.ServiceManagers;
using Matchnet.SparkWS.ValueObjects;

using Matchnet;
using Matchnet.Exceptions;

namespace Matchnet.SparkWS.Service
{
   public partial class SparkWS : Matchnet.RemotingServices.RemotingServiceBase
   {

	  ProfileSectionSM _ProfileSectionSM;
	  AccessTicketSM _AccessTicketSM;
	  public SparkWS()
	  {
		 InitializeComponent();
	  }


	  protected override void RegisterServiceManagers()
	  {
		 try
		 {
			_ProfileSectionSM = new ProfileSectionSM();
			base.RegisterServiceManager(_ProfileSectionSM);
			_AccessTicketSM = new AccessTicketSM();
			base.RegisterServiceManager(_AccessTicketSM);
		 }
		 catch (Exception ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
		 }
	  }

   }
}
