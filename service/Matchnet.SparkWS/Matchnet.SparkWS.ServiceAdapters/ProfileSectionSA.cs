using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.SparkWS.ValueObjects;
using Matchnet.SparkWS.ValueObjects.ServiceDefinitions;

//using Matchnet.Configuration;
//using Matchnet.Configuration.ServiceAdapters;
//using Matchnet.Configuration.ValueObjects;



using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;

namespace Matchnet.SparkWS.ServiceAdapters
{
   public class ProfileSectionSA : SABase
   {
	  private const string SERVICE_MANAGER_NAME = "ProfileSectionSM";
	  private const string SERVICE_CONSTANT = "WS_SVC";

	  #region Singleton
	  public static readonly ProfileSectionSA Instance = new ProfileSectionSA();

	  private ProfileSectionSA() { }
	  #endregion

	  public ProfileSectionDefinition GetProfileSectionDefinition(int profileSectionID)
	  {

		 ProfileSectionDefinition result = null;
		 string uri = string.Empty;

		 try
		 {
			string cacheKey = ProfileSectionDefinition.GetCacheKey(profileSectionID);
			result = Matchnet.Caching.Cache.Instance.Get(cacheKey) as ProfileSectionDefinition;

			if (result == null)
			{

			   uri = getServiceManagerUri();

			   try
			   {
				  base.Checkout(uri);
				  result = getService(uri).GetProfileSectionDefinition(profileSectionID);
			   }
			   finally
			   {
				  base.Checkin(uri);
			   }
			   
			   if (result != null)
			   {
				  result.AddToCache();
			   }
			   else {
				  ///TODO: Protect against multi calls. If failed to get object, complain and  cache an empty one?
				  new SAException("Can't get ProfileSectionDefinition " + profileSectionID.ToString());
			   }
			}

			return result;
		 }
		 catch (Exception ex)
		 {
			throw (new SAException("Cannot retrieve ProfileSectionDefinition (uri: " + uri + ")", ex));
		 }
	  }
	  
	  protected override void GetConnectionLimit()
	  {
		 base.MaxConnections = 32;
	  }

	  private IProfileSectionService getService(string uri)
	  {
		 try
		 {
			return (IProfileSectionService)Activator.GetObject(typeof(IProfileSectionService), uri);
		 }
		 catch (Exception ex)
		 {
			throw (new SAException("Cannot activate remote service manager at " + uri, ex));
		 }
	  }

	  private string getServiceManagerUri()
	  {
		 try
		 {
			string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
			string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("WS_SVC_SA_HOST_OVERRIDE");

			if (overrideHostName.Length > 0)
			{
			   UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
			   return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
			}

			return uri;
		 }
		 catch (Exception ex)
		 {
			throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
		 }
	  }

   }
}
