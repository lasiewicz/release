using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;
using Matchnet.Caching;
using Matchnet.SparkWS.ValueObjects.ServiceDefinitions;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Exceptions;
using Matchnet.Data.Hydra;


namespace Matchnet.SparkWS.BusinessLogic
{
   public class ProfileSectionBL : IProfileSectionService
   {
	  public static readonly ProfileSectionBL Instance = new ProfileSectionBL();

	  private ProfileSectionBL() { }

	  /// <summary>
	  /// Retreives the specific profile section definition
	  /// </summary>
	  /// <param name="profileSectionID"></param>
	  /// <returns></returns>
	  public ProfileSectionDefinition GetProfileSectionDefinition(int profileSectionID)
	  {
		 ProfileSectionDefinition result;
		 string key = ProfileSectionDefinition.GetCacheKey(profileSectionID);
		 result = Caching.Cache.Instance.Get(key) as ProfileSectionDefinition;
		 if (result == null)
			result = getProfileSectionDefinitionFromDB(profileSectionID);

		 if (result != null)
		 {
			Caching.Cache.Instance.Add(result);
		 }

		 return result;
	  }

	  public Dictionary<int,ProfileSectionDefinition> GetProfileSectionDefinitions(int[] profileSectionIDs)
	  {
		 Dictionary<int, ProfileSectionDefinition> result = new Dictionary<int, ProfileSectionDefinition>(profileSectionIDs.Length);

		 for (int i = 0; i < profileSectionIDs.Length; i++)
		 { 
			result[profileSectionIDs[i]] = GetProfileSectionDefinition(profileSectionIDs[i]);		 
		 }
		 return result;
	  }

	  private ProfileSectionDefinition getProfileSectionDefinitionFromDB(int profileSectionID)
	  {
		 ProfileSectionDefinition result = new ProfileSectionDefinition();
		 result.ProfileSectionID = profileSectionID;

		 Command command = null;
		 DataTable tbl = null;
		 try
		 {
			command = new Command("mnSystem", "up_ProfileSectionDefinition_Get", 0);
			command.AddParameter("@ProfileSectionDefinitionID", SqlDbType.Int, ParameterDirection.Input, profileSectionID);
			DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(command);
			if (ds != null && ds.Tables != null)
			{
			   tbl = ds.Tables[0]; // MemberAttrributeParameters
			   if (tbl != null && tbl.Rows.Count > 0)
			   {
				  result.AttributeDefinitions = new MemberAttributeParameters[tbl.Rows.Count];
				  for (int i = 0; i < tbl.Rows.Count; i++)
				  {
					 int AttributeID, CommunityID, SiteID, BrandID, LanguageID;
					 AttributeDataType DataType;
					 AttributeID = (int)tbl.Rows[i]["AttributeID"];
					 DataType = (AttributeDataType)tbl.Rows[i]["DataType"];
					 CommunityID = (int)tbl.Rows[i]["CommunityID"];
					 SiteID = (int)tbl.Rows[i]["SiteID"];
					 BrandID = (int)tbl.Rows[i]["BrandID"];
					 LanguageID = (int)tbl.Rows[i]["LanguageID"];

					 result.AttributeDefinitions[i] = new MemberAttributeParameters(AttributeID, DataType, CommunityID, SiteID, BrandID, LanguageID);
				  }
			   }

			   tbl = ds.Tables[1]; // CommunityList;
			   if (tbl != null && tbl.Rows.Count > 0)
			   {
				  result.CommunityIDList = new int [tbl.Rows.Count];
				  for (int i = 0; i < tbl.Rows.Count; i++)
				  {
					 result.CommunityIDList[i] = (int)tbl.Rows[i][0];
				  }
			   }

			   tbl = ds.Tables[2]; // SiteList;
			   if (tbl != null && tbl.Rows.Count > 0)
			   {
				  result.SiteIDList = new int[tbl.Rows.Count];
				  for (int i = 0; i < tbl.Rows.Count; i++)
				  {
					 result.SiteIDList[i] = (int)tbl.Rows[i][0];
				  }
			   }

			   tbl = ds.Tables[3]; // CommunityPhotoList;
			   if (tbl != null && tbl.Rows.Count > 0)
			   {
				  result.PhotoCommunityIDList = new int[tbl.Rows.Count];
				  for (int i = 0; i < tbl.Rows.Count; i++)
				  {
					 result.PhotoCommunityIDList[i] = (int)tbl.Rows[i][0];
				  }
			   }

			}
		 }
		 catch (Exception ex)
		 {
			new Matchnet.Exceptions.BLException("SparkWS.gatAccessTicketFromDB failed", ex);
			result.AttributeDefinitions = new MemberAttributeParameters[] { };
		 }
		 finally
		 {
			if (tbl != null) tbl.Dispose();
		 }


		 return result;
	  }


   }
}
