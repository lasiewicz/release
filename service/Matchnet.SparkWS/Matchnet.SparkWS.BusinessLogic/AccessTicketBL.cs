using System;
using System.Collections.Generic;
using System.Text;
using Matchnet.SparkWS.ValueObjects.ServiceDefinitions;
using System.Data;

using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Exceptions;
using Matchnet.Data.Hydra;
using System.Collections.Specialized;

namespace Matchnet.SparkWS.BusinessLogic
{
   public class AccessTicketBL : IAccessTicketService
   {
	  public static readonly AccessTicketBL Instance = new AccessTicketBL();

	  private AccessTicketBL() { }

	  #region IAccessTicketService Members

	  public AccessTicket GetAccessTicket(string ticketKey)
	  {
		 AccessTicket result;
		 result = getAccessTicketFromDB(ticketKey);

		 return result;

	  }

	  private AccessTicket getAccessTicketFromDB(string ticketKey)
	  {
		 AccessTicket result = null;
		 if (ticketKey == AccessTicket.DEFAULT_TICKET)
			result = new AccessTicket();
		 else
		 {
			Command command;
			DataSet ds = null;
			DataTable tbl = null;
			try
			{
			   command = new Command("mnLogon", "up_AccessTicket_Get", 0);
			   command.Parameters.Add(new Parameter("@TicketKey", SqlDbType.VarChar, ParameterDirection.Input, ticketKey));

			   ds = Matchnet.Data.Client.Instance.ExecuteDataSet(command);
			   tbl = ds.Tables[0];

			   if (tbl != null && tbl.Rows.Count == 1)
			   {
				  ApiAccessRoll roll = (ApiAccessRoll)tbl.Rows[0]["AccessRoll"];
				  bool isBlocked = (bool)tbl.Rows[0]["IsBlockedFlag"];

				  result = new AccessTicket(ticketKey, isBlocked, roll);

				  tbl = ds.Tables[1]; // profile section IDs
				  if (tbl != null && tbl.Rows.Count > 0)
				  {
					 int[] profileSectionIDs = new int[tbl.Rows.Count];

					 for (int i = 0; i < tbl.Rows.Count; i++)
					 {
						profileSectionIDs[i] = (int)tbl.Rows[i]["ProfileSectionID"];
					 }

					 result.ProfileSectionDefinitions = ProfileSectionBL.Instance.GetProfileSectionDefinitions(profileSectionIDs);
				  }


				  tbl = ds.Tables[2]; // allowed remote hosts
				  if (tbl != null && tbl.Rows.Count > 0)
				  {
					 string [] remoteHosts = new string[tbl.Rows.Count];

					 for (int i = 0; i < tbl.Rows.Count; i++)
					 {
						remoteHosts[i] = tbl.Rows[i]["RemoteHost"].ToString().Replace("*","");
					 }

					 result.RemoteHosts = remoteHosts;
				  }

			   }
			}
			catch (Exception ex)
			{
			   throw new Matchnet.Exceptions.BLException("SparkWS.gatAccessTicketFromDB failed", ex);
			}
			finally
			{
			   tbl = null;
			   if (ds != null)
				  ds.Dispose();
			}

		 }
		 return result;
	  }

	  #endregion
   }
}
