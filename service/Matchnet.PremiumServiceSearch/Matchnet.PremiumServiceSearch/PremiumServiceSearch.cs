﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.PremiumServiceSearch.ServiceManagers;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
namespace Matchnet.PremiumServiceSearch
{
    public partial class PremiumServiceSearch :  Matchnet.RemotingServices.RemotingServiceBase
    {
        private SpotlightMemberSM spotlightMemberSM;
        private QuestionAnswerSM questionAnswerSM;

        public PremiumServiceSearch()
        {
            InitializeComponent();
        }

        static void Main()
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                System.ServiceProcess.ServiceBase[] ServicesToRun;
                ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PremiumServiceSearch() };
                System.ServiceProcess.ServiceBase.Run(ServicesToRun);
            }
            catch(Exception ex)
            {throw (new ServiceBoundaryException(ex.Source, "Exception while starting service:" + ex.Message));}
        }


        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                spotlightMemberSM = new SpotlightMemberSM();
                questionAnswerSM = new QuestionAnswerSM();

                base.RegisterServiceManager(spotlightMemberSM);
                base.RegisterServiceManager(questionAnswerSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
               base.OnStart(args);
            }
            catch (Exception ex)
            { throw (new ServiceBoundaryException(ex.Source, "Exception on service start:" + ex.Message)); 
            }
        }

        protected override void OnStop()
        {
            spotlightMemberSM.Dispose();
            questionAnswerSM.Dispose();
            base.OnStop();
          
        }


    }
}
