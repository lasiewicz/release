﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace Matchnet.PremiumServiceSearch
{
    [RunInstaller(true)]
    public partial class ServiceInstaller : Installer
    {
       
        public ServiceInstaller()
        {
            InitializeComponent();
          
        }
        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            Matchnet.PremiumServiceSearch.ServiceManagers.SpotlightMemberSM.PerfCounterInstall();
            Matchnet.PremiumServiceSearch.ServiceManagers.QuestionAnswerSM.PerfCounterInstall();
        }


        private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
           Matchnet.PremiumServiceSearch.ServiceManagers.SpotlightMemberSM.PerfCounterUninstall();
           Matchnet.PremiumServiceSearch.ServiceManagers.QuestionAnswerSM.PerfCounterUninstall(); 
        }
    }
}
