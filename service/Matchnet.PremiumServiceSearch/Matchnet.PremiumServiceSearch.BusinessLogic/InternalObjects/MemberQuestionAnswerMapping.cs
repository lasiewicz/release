﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.PremiumServiceSearch.BusinessLogic.InternalObjects
{
    public class MemberQuestionAnswerMapping : ICacheable
    {
        public int MemberId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }

        public string GetCacheKey()
        {
            return GetCacheKey(MemberId, QuestionId);
        }

        public static string GetCacheKey(int memberId, int questionId)
        {
            return string.Format("MemberQuestionAnswerMapping_{0}_{1}", memberId, questionId);
        }

        public int CacheTTLSeconds { get; set; }
        public CacheItemMode CacheMode { get; private set; }
        public CacheItemPriorityLevel CachePriority { get; set; }
    }
}
