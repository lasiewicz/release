﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.Data;
using System.Data;
using Matchnet.Configuration.ServiceAdapters;
using System.Data.SqlClient;


using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects;
using DBQ = Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.PremiumServiceSearch.ValueObjects.ReplicationAction;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using System.Collections;
using System.Timers;
using System.Diagnostics;
using System.IO.Pipes;
using Matchnet.BaseClasses;
using Matchnet.Caching;
using Matchnet.Caching.CacheStrategies;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.PremiumServiceSearch.BusinessLogic.InternalObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.CachingMappings;

namespace Matchnet.PremiumServiceSearch.BusinessLogic
{
    public class QuestionAnswerBL
    {
        private Matchnet.Caching.Cache _cache;
        public readonly static QuestionAnswerBL Instance = new QuestionAnswerBL();
        const string PREMIUMSTORE_LDB = "mnPremiumServices";
        const string MODULE_NAME = "QuestionAnswerBL";
        const string PRIMARYKEY_ANSWERID = "AnswerID";
        private Dictionary<int, Brand> _brands = new Dictionary<int, Brand>();
        private object _brandsLock = new object();

        private QuestionAnswerBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
        }

        #region Replication
        /// <summary>
        /// This event handles the cache synchronization between service instances
        /// </summary>
        public delegate void ReplicationEventHandler(IReplicationAction replicationAction);
        public event ReplicationEventHandler ReplicationRequested;

        public void PlayReplicationAction(IReplicationAction replicationAction)
        {
            switch (replicationAction.GetType().Name)
            {
                case "AnswerReplicationAction":

                    AnswerReplicationAction answerReplicationAction = replicationAction as AnswerReplicationAction;
                    PlayReplicatedAnswer(answerReplicationAction);
                    break;
            }
        }	

        #endregion

        #region Perfomance counters events
        public delegate void GetQuestionRequestEventHandler();
        public event GetQuestionRequestEventHandler GetQuestionRequested;

        public delegate void GetMemberQuestionRequestEventHandler();
        public event GetMemberQuestionRequestEventHandler GetMemberQuestionsRequested;

        public delegate void GetActiveQuestionIDsRequestEventHandler();
        public event GetActiveQuestionIDsRequestEventHandler GetActiveQuestionIDsRequested;

        public delegate void AddAnswerRequestEventHandler();
        public event AddAnswerRequestEventHandler AddAnswerRequested;

        public delegate void RemoveAnswerRequestEventHandler();
        public event RemoveAnswerRequestEventHandler RemoveAnswerRequested;


        #endregion

        #region Public methods

        public QuestionList GetActiveQuestionList(int siteID)
        {
            string functionName = "GetActiveQuestionList";
            QuestionList list = new QuestionList();


            //Check Cache
            list = _cache.Get(QuestionList.GetCacheKeyString(siteID)) as QuestionList;

            if (list == null)
            {
                list = new QuestionList();
                list.SiteID = siteID;

                SqlDataReader dataReader = null;
                try
                {

                    Command comm = new Command();
                    comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.[up_GetActiveQuestions]";

                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                                 
                    dataReader = Client.Instance.ExecuteReader(comm);

                    //Question info
                    while (dataReader.Read())
                    {
                        Question q= LoadQuestionInfo(dataReader);
                        list.Questions.Add(q);
                        list.QuestionsAZ.Add(q);
                        list.SortByAlpha();
                    }

                    //update cache
                    if (list != null && list.Questions.Count > 0)
                    {
                        list.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(siteID);
                        _cache.Add(list);
                    }




                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                    }
                }
            }

            //perf
            GetActiveQuestionIDsRequested();

            return list;
        }

        public List<StockAnswer> GetStockAnswers(List<int> questionIds)
        {
            return CacheHelper.GetValuesByKeys(questionIds,
                GetStockAnswersFromDb,
                (key, value) => new Question2StockAnswerMapping(key, value),
                Question2StockAnswerMapping.GetCacheKey);
        }

        List<ItemMapping<int,StockAnswer>> GetStockAnswersFromDb(List<int> questionIds)
        {
            var command = new Command();
            command.LogicalDatabaseName = PREMIUMSTORE_LDB;
            command.StoredProcedureName = "dbo.up_GetStockAnswers_ByQuestionIDs";

            command.AddParameter("@QuestionIDs", SqlDbType.Structured, ParameterDirection.Input, GetTable(questionIds,"QuestionID"));

            var ret = new List<ItemMapping<int, StockAnswer>>();

            using (var dataReader = Client.Instance.ExecuteReader(command))
            {
                while (dataReader.Read())
                {
                    var stockAnswer = LoadStockAnswerInfo(dataReader);

                    ret.Add(new ItemMapping<int, StockAnswer>(stockAnswer.QuestionID,stockAnswer));
                }
            }
            return ret;
        }

        private DataTable GetTable<T>(List<T> items, string keyName)
        {
            var ret = new DataTable();
            ret.Columns.Add(keyName, typeof(T));
            foreach (var role in items)
            {
                ret.Rows.Add(role);
            }
            return ret;
        }



        public ActiveQuestionIDList GetActiveQuestionIDs(int siteID)
        {
            string functionName = "GetActiveQuestionIDs";
            ActiveQuestionIDList activeQuestionIDList = new ActiveQuestionIDList();
            

            //Check Cache
            activeQuestionIDList = _cache.Get(ActiveQuestionIDList.GetCacheKeyString(siteID)) as ActiveQuestionIDList;

            if (activeQuestionIDList == null)
            {
                activeQuestionIDList = new ActiveQuestionIDList();
                activeQuestionIDList.SiteID = siteID;

                SqlDataReader dataReader = null;
                try
                {

                    Command comm = new Command();
                    comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_GetActiveQuestionAnswers_BySiteID";

                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                    //TODO - Get date by siteID
                    comm.AddParameter("@nowDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                    dataReader = Client.Instance.ExecuteReader(comm);

                    //Question info
                    while (dataReader.Read())
                    {
                        activeQuestionIDList.QuestionIDs.Add(dataReader.IsDBNull(dataReader.GetOrdinal("QuestionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("QuestionID")));
                    }

                    //update cache
                    if (activeQuestionIDList != null)
                    {
                        activeQuestionIDList.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(siteID);
                        _cache.Insert(activeQuestionIDList);
                    }

                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                    }
                }
            }

            //perf
            GetActiveQuestionIDsRequested();

            return activeQuestionIDList;
        }

        public PagedQuestion GetQuestionPaged(int questionID, int pageNumber, int pageSize, bool latestFirst)
        {
            return GetQuestionWithAnswersPaged(questionID, pageNumber, pageSize, latestFirst);
        }



        public Question GetQuestion(int questionID, bool ignoreAnswers = false)
        {
            return ignoreAnswers ? GetQuestionWithoutAnswers(questionID) : GetQuestionWithAnswers(questionID);
        }


        Question GetBareQuestion(int questionID, out SqlDataReader dataReader)
        {
            Question question = null;

            Command comm = new Command();
            comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
            comm.StoredProcedureName = "dbo.up_GetQuestionAnswers_ByQuestionID";

            comm.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, questionID);
            dataReader = Client.Instance.ExecuteReader(comm);

            //Question info
            while (dataReader.Read())
            {
                question = LoadQuestionInfo(dataReader);
                break;
            }
            return question;
        }

        PagedQuestion GetBareQuestionPaged(int questionID, out SqlDataReader dataReader, int pageNumber, int pageSize, bool latestFirst)
        {
            PagedQuestion question = null;

            Command comm = new Command();
            comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
            comm.StoredProcedureName = "dbo.up_GetQuestionAnswers_ByQuestionID_Paged";

            comm.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, questionID);
            comm.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
            comm.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
            comm.AddParameter("@LatestFirst", SqlDbType.Bit, ParameterDirection.Input, latestFirst);
            dataReader = Client.Instance.ExecuteReader(comm);

            //Question info
            while (dataReader.Read())
            {
                question = LoadQuestionInfo<PagedQuestion>(dataReader);
                break;
            }
            return question;
        }

        void GetAndAttachAnswers(Question question, SqlDataReader dataReader, out ArrayList memberIDs)
        {
            memberIDs = new ArrayList();
            //Answer info
            dataReader.NextResult();

            while (dataReader.Read())
            {
                Answer answer = LoadAnswerInfo(dataReader);
                memberIDs.Add(answer.MemberID);

                //TODO - Answer Type
                question.Answers.Add(answer);
            }
        }

        void GetAndAttachTotalAnswersCount(PagedQuestion question, SqlDataReader dataReader)
        {
            dataReader.NextResult();

            if(dataReader.Read())
            {
                question.TotalAnswers = dataReader.GetInt32(dataReader.GetOrdinal("TotalAnswers"));
            }
        }


        void GetAndAttachStockAnswers(Question question)
        {
            Command comm2 = new Command();
            comm2.LogicalDatabaseName = PREMIUMSTORE_LDB;
            comm2.StoredProcedureName = "dbo.up_GetStockAnswers_ByQuestionID";

            comm2.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, question.QuestionID);

            using (var dataReader = Client.Instance.ExecuteReader(comm2))
            {
                while (dataReader.Read())
                {
                    StockAnswer stockAnswer = LoadStockAnswerInfo(dataReader);
                    question.StockAnswers.Add(stockAnswer);
                }
            }


        }
        PagedQuestion GetQuestionWithAnswersPaged(int questionID, int pageNumber, int pageSize, bool latestFirst)
        {
            string functionName = "GetQuestion";
            PagedQuestion question = null;
            question = _cache.Get(PagedQuestion.GetCacheKeyString(questionID, pageNumber, pageSize, latestFirst)) as PagedQuestion;

            if (question == null)
            {
                SqlDataReader dataReader = null;
                try
                {
                    System.Diagnostics.Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();
                    question = GetBareQuestionPaged(questionID, out dataReader, pageNumber,pageSize,latestFirst);
                    if (question != null && question.QuestionID > 0)
                    {
                        ArrayList memberIDs = null;

                        GetAndAttachAnswers(question, dataReader, out memberIDs);
                        GetAndAttachTotalAnswersCount(question, dataReader);
                        GetAndAttachStockAnswers(question);

                        //Prepopulate answers with member info for sort/filter
                        Brand brand = GetBrand(question.SiteID);
                        PrePopulateAnswersWithMemberInfoBatchProcess(brand, memberIDs, question.Answers);

                        //update cache
                        if (question != null)
                        {
                            question.PageNumber = pageNumber;
                            question.PageSize = pageSize;
                            question.LatestFirst = latestFirst;
                            question.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(question.SiteID);
                            _cache.Insert(question);

                            stopWatch.Stop();
                            EventLog.WriteEntry(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "GetQuestionWithAnswers(" + questionID.ToString() + ") - SiteID: " + question.SiteID.ToString() + ", Answers: " + question.Answers.Count.ToString() + ", IsPrePopulateAnswersForSortFilterEnabled: " + IsPrePopulateAnswersForSortFilterEnabled(brand).ToString() + ", SizePerBatch: " + GetMemberBatchSize(brand).ToString() + ", Elapsed Time: " + stopWatch.ElapsedMilliseconds.ToString(), EventLogEntryType.Information);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                    }
                }
            }

            //perf
            GetQuestionRequested();

            return question;
        }

        Question GetQuestionWithAnswers(int questionID)
        {

            string functionName = "GetQuestion";
            Question question = null;
            question = _cache.Get(Question.GetCacheKeyString(questionID)) as Question;

            if (question == null)
            {
                SqlDataReader dataReader = null;
                try
                {
                    System.Diagnostics.Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();
                    question = GetBareQuestion(questionID, out dataReader);    
                    if (question != null && question.QuestionID > 0)
                    {
                        ArrayList memberIDs = null;

                        GetAndAttachAnswers(question, dataReader, out memberIDs);
                        GetAndAttachStockAnswers(question);

                        //Prepopulate answers with member info for sort/filter
                        Brand brand = GetBrand(question.SiteID);
                        PrePopulateAnswersWithMemberInfoBatchProcess(brand, memberIDs, question.Answers);

                        //update cache
                        if (question != null)
                        {
                            question.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(question.SiteID);
                            _cache.Insert(question);

                            stopWatch.Stop();
                            EventLog.WriteEntry(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "GetQuestionWithAnswers(" + questionID.ToString() + ") - SiteID: " + question.SiteID.ToString() + ", Answers: " + question.Answers.Count.ToString() + ", IsPrePopulateAnswersForSortFilterEnabled: " + IsPrePopulateAnswersForSortFilterEnabled(brand).ToString() + ", SizePerBatch: " + GetMemberBatchSize(brand).ToString() + ", Elapsed Time: " + stopWatch.ElapsedMilliseconds.ToString(), EventLogEntryType.Information);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                    }
                }
            }

            //perf
            GetQuestionRequested();

            return question;
        }
        Question GetQuestionWithoutAnswers(int questionID)
        {

            string functionName = "GetQuestion";
            Question question = null;
            string questionOnlyKey = "QuestionOnly_" + questionID;
            question = _cache.Get(questionOnlyKey) as Question;

            if (question == null)
            {
                try
                {
                    System.Diagnostics.Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();

                    Command comm = new Command();
                    comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_GetQuestion_ByQuestionID";

                    comm.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, questionID);
                    using (var dataReader = Client.Instance.ExecuteReader(comm))
                    {
                        //Question info
                        while (dataReader.Read())
                        {
                            question = LoadQuestionInfo(dataReader);
                            break;
                        }
                    }

                    if (question != null && question.QuestionID > 0)
                    {
                        Command comm2 = new Command();
                        comm2.LogicalDatabaseName = PREMIUMSTORE_LDB;
                        comm2.StoredProcedureName = "dbo.up_GetStockAnswers_ByQuestionID";

                        comm2.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, questionID);

                        using (var dataReader = Client.Instance.ExecuteReader(comm2))
                        {
                            while (dataReader.Read())
                            {
                                StockAnswer stockAnswer = LoadStockAnswerInfo(dataReader);
                                question.StockAnswers.Add(stockAnswer);
                            }
                        }
                        //update cache
                        if (question != null)
                        {
                            Brand brand = GetBrand(question.SiteID);
                            question.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(question.SiteID);
                            question.SetAlternativeKey(questionOnlyKey);
                            _cache.Insert(question);

                            stopWatch.Stop();
                            EventLog.WriteEntry(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "GetQuestionWithoutAnswers(" + questionID.ToString() + ") - SiteID: " + question.SiteID.ToString() + ", Answers: " + question.Answers.Count.ToString() + ", IsPrePopulateAnswersForSortFilterEnabled: " + IsPrePopulateAnswersForSortFilterEnabled(brand).ToString() + ", SizePerBatch: " + GetMemberBatchSize(brand).ToString() + ", Elapsed Time: " + stopWatch.ElapsedMilliseconds.ToString(), EventLogEntryType.Information);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
            }

            //perf
            GetQuestionRequested();

            return question;
        }

        public void AddAnswer(int memberID, int siteID, int questionID, string answerValue)
        {
            string functionName = "AddAnswer";
            try
            {
                QuestionAnswerEnums.QuestionType questionType = QuestionAnswerEnums.QuestionType.FreeText;
                try
                {
                    Question theQuestion = GetQuestion(questionID);
                    questionType = (QuestionAnswerEnums.QuestionType)Enum.Parse(typeof(QuestionAnswerEnums.QuestionType), theQuestion.QuestionTypeID.ToString());
                }
                catch (Exception e)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, e, false);
                }

                if (questionType != QuestionAnswerEnums.QuestionType.MultipleChoice)
                {
                    //approval process
                    MemberQuestionList memberQuestionList = GetMemberQuestions(memberID, siteID);

                    bool enqueue = true;

                    foreach (Question question in memberQuestionList.Questions)
                    {
                        foreach (Answer questionAnswer in question.Answers)
                        {
                            if (questionAnswer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending)
                            {
                                enqueue = false;
                            }
                        }
                    }

                    if (enqueue)
                    {
                        ApprovalEnqueue(siteID, memberID);
                    }
                }

                int answerID = KeySA.Instance.GetKey(PRIMARYKEY_ANSWERID);
                //TODO - Check question/answer type to determine initial answer status
                QuestionAnswerEnums.AnswerStatusType answerStatus = QuestionAnswerEnums.AnswerStatusType.Pending;

                Command comm = new Command();
                comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                comm.StoredProcedureName = "dbo.up_InsertAnswer";

                comm.AddParameter("@AnswerID", SqlDbType.Int, ParameterDirection.Input, answerID);
                comm.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, questionID);
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                comm.AddParameter("@AnswerValue", SqlDbType.NVarChar, ParameterDirection.Input, answerValue);

                if (questionType == QuestionAnswerEnums.QuestionType.MultipleChoice)
                {
                    answerStatus = QuestionAnswerEnums.AnswerStatusType.Approved;
                }
                else
                {
                comm.AddParameter("@AnswerValuePending", SqlDbType.NVarChar, ParameterDirection.Input, answerValue);
                }
                
                comm.AddParameter("@AnswerStatusID", SqlDbType.Int, ParameterDirection.Input, (int)answerStatus);
                comm.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, Constants.NULL_INT);

                Client.Instance.ExecuteAsyncWrite(comm);

                //Create answer for updating cache and replication
                Answer answer = LoadAnswerInfo(answerID, questionID, memberID, Constants.NULL_INT, answerValue, answerValue, answerStatus, DateTime.Now, DateTime.Now);

                Brand brand = GetBrand(siteID);
                if (IsPrePopulateAnswersForSortFilterEnabled(brand) || IsUpdateAnswerMemberInfoEnabled())
                {
                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                    PopulateAnswersWithMemberInfo(brand, answer, member);

                    //update answermemberinfo in db
                    InsertAnswerMemberInfo(member, siteID);
                }

                //update cache
                AddAnswerToCache(answer, siteID);

                //replicate action
                ReplicationAnswer(answer, siteID, AnswerActionType.Add);


            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

            try
            {
                //activity recording
                ActivityRecordingSA.Instance.RecordActivity(
                    memberID,
                    Constants.NULL_INT,
                    siteID,
                    ActivityRecording.ValueObjects.Types.ActionType.AnswerQuestion,
                    ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                    string.Format("<QuestionID>{0}</QuestionID>", questionID));
            }
            catch (Exception logEx)
            {
                string message = "ActivityRecording threw an exception: " + logEx.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
            }

            //perf
            AddAnswerRequested();
        }


        public void RemoveMemberAnswer(int questionID, int siteID, int memberID)
        {
            var answerId = GetMemberAnswerId(memberID, questionID);
            if(answerId.HasValue)
                RemoveAnswer(answerId.Value,questionID,siteID,memberID);
        }

        private int? GetMemberAnswerId(int memberID, int questionID)
        {
            string functionName = "GetMemberAnswerId";
            try
            {
                var mapping = _cache.Get(MemberQuestionAnswerMapping.GetCacheKey(memberID, questionID)) as MemberQuestionAnswerMapping;

                if (mapping != null)
                    return mapping.AnswerId;

                Command comm = new Command();
                comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                comm.StoredProcedureName = "dbo.up_GetMemberAnswer_ByQuestionID";
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                comm.AddParameter("@QuestionID", SqlDbType.Int, ParameterDirection.Input, questionID);
                using (var dataReader = Client.Instance.ExecuteReader(comm))
                {
                    while (dataReader.Read())
                    {
                        var ret = dataReader.GetInt32(0);
                        _cache.Add(new MemberQuestionAnswerMapping()
                        {
                            MemberId = memberID,
                            AnswerId = ret,
                            QuestionId = questionID
                        });
                        return ret;
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
            return null;
        }

        public void RemoveAnswer(int answerID, int questionID, int siteID, int memberID)
        {
            string functionName = "RemoveAnswer";
            try
            {
                Command comm = new Command();
                comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                comm.StoredProcedureName = "dbo.up_RemoveAnswer";

                comm.AddParameter("@AnswerID", SqlDbType.Int, ParameterDirection.Input, answerID);

                Client.Instance.ExecuteAsyncWrite(comm);

                //Create answer for updating cache and replication
                Answer answer = LoadAnswerInfo(answerID, questionID, memberID, Constants.NULL_INT, "", "", QuestionAnswerEnums.AnswerStatusType.None, DateTime.MinValue, DateTime.MinValue);

                //update cache
                RemoveAnswerFromCache(answer, siteID);

                //replicate action
                ReplicationAnswer(answer, siteID, AnswerActionType.Remove);

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

            try
            {
                //activity recording
                ActivityRecordingSA.Instance.RecordActivity(
                    memberID,
                    Constants.NULL_INT,
                    siteID,
                    ActivityRecording.ValueObjects.Types.ActionType.RemoveAnswer,
                    ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                    string.Format("<QuestionID>{0}</QuestionID>", questionID));
            }
            catch (Exception logEx)
            {
                string message = "ActivityRecording threw an exception: " + logEx.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
            }

            //perf
            RemoveAnswerRequested();
        }

        public MemberQuestionList GetMemberQuestions(int memberID, int siteID)
        {
            string functionName = "GetMemberQuestions";

            MemberQuestionList memberQuestionList = _cache.Get(MemberQuestionList.GetCacheKeyString(siteID, memberID)) as MemberQuestionList;

            if (memberQuestionList == null)
            {
                memberQuestionList = new MemberQuestionList();
                memberQuestionList.SiteID = siteID;
                memberQuestionList.MemberID = memberID;

                Brand brand = GetBrand(siteID);
                int genderMask = 0;
                int globalStatusMask = 0;
                int selfSuspendedFlag = 0;
                bool hasPhotos = false;

                if (IsPrePopulateAnswersForSortFilterEnabled(brand))
                {
                    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                    genderMask = member.GetAttributeInt(brand, "GenderMask");
                    globalStatusMask = member.GetAttributeInt(brand, "GlobalStatusMask", 0);
                    selfSuspendedFlag = member.GetAttributeInt(brand, "SelfSuspendedFlag", 0);
                    hasPhotos = (member.GetAttributeInt(brand, "HasPhotoFlag", 0) == 1);
                }

                Dictionary<int, Question> questions = new Dictionary<int, Question>();
                SqlDataReader dataReader = null;
                try
                {
                    Command comm = new Command();
                    comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_GetQuestionAnswers_ByMemberID";

                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                    dataReader = Client.Instance.ExecuteReader(comm);

                    //Question info
                    while (dataReader.Read())
                    {
                        Question question = LoadQuestionInfo(dataReader);
                        questions.Add(question.QuestionID, question);
                    }

                    //Answer info
                    dataReader.NextResult();
                    while (dataReader.Read())
                    {
                        Answer answer = LoadAnswerInfo(dataReader);
                        answer.GenderMask = genderMask;
                        answer.GlobalStatusMask = globalStatusMask;
                        answer.SelfSuspendedFlag = selfSuspendedFlag;
                        answer.HasApprovedPhotos = hasPhotos;

                        //TODO - Answer Type
                        questions[answer.QuestionID].Answers.Add(answer);
                    }

                    foreach (Question q in questions.Values)
                    {
                        memberQuestionList.Questions.Add(q);
                    }

                    //add to cache
                    if (memberQuestionList != null)
                    {

                        var stockAnswers = GetStockAnswers(memberQuestionList.Questions
                            .Select(x => x.QuestionID)
                            .ToList());

                        var stockAnswersMap = new Dictionary<int, List<StockAnswer>>();

                        foreach (var stockAnswer in stockAnswers)
                        {
                            if(!stockAnswersMap.ContainsKey(stockAnswer.QuestionID))
                                stockAnswersMap[stockAnswer.QuestionID] = new List<StockAnswer>();
                            stockAnswersMap[stockAnswer.QuestionID].Add(stockAnswer);
                        }


                        foreach (var question in memberQuestionList.Questions)
                        {
                            if (stockAnswersMap.ContainsKey(question.QuestionID))
                                question.StockAnswers = stockAnswersMap[question.QuestionID];
                        }

                        memberQuestionList.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(siteID);
                        _cache.Insert(memberQuestionList);
                    }
                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                    }
                }
            }

            //perf
            GetMemberQuestionsRequested();

            return memberQuestionList;
        }

        public void UpdateAnswer(Answer answer, int siteID)
        {
            var functionName = "UpdateAnswer";

            QuestionAnswerEnums.QuestionType questionType = QuestionAnswerEnums.QuestionType.FreeText;
            try
            {
                Question theQuestion = GetQuestion(answer.QuestionID);
                questionType = (QuestionAnswerEnums.QuestionType)Enum.Parse(typeof(QuestionAnswerEnums.QuestionType), theQuestion.QuestionTypeID.ToString());
            }
            catch (Exception e)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, e, false);
            }

            if (questionType == QuestionAnswerEnums.QuestionType.MultipleChoice)
            {
                answer.AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Approved;
            }
            else
            {
                answer.AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Pending;
                answer.AnswerValuePending = answer.AnswerValue;
            }
            // Expects existing answer
            answer.AnswerID = GetMemberAnswerId(answer.MemberID, answer.QuestionID).Value;
            AdminUpdateAnswer(answer,siteID);
            UpdateAnswerActivity(answer, siteID);
        }

        void UpdateAnswerActivity(Answer answer, int siteID)
        {
            try
            {
                //activity recording
                ActivityRecordingSA.Instance.RecordActivity(
                    answer.MemberID,
                    Constants.NULL_INT,
                    siteID,
                    ActivityRecording.ValueObjects.Types.ActionType.EditAnswer,
                    ActivityRecording.ValueObjects.Types.CallingSystem.QuestionAnswer,
                    string.Format("<QuestionID>{0}</QuestionID>", answer.QuestionID));
            }
            catch (Exception logEx)
            {
                string message = "ActivityRecording threw an exception: " + logEx.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        public void AdminUpdateAnswer(Answer answer, int siteID)
        {
            string functionName = "AdminUpdateAnswer";

            try
            {
                if (answer.AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Rejected)
                {
                    RemoveAnswer(answer.AnswerID, answer.QuestionID, siteID, answer.MemberID);
                }
                else
                {
                    Command comm = new Command();

                    comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.up_UpdateAnswer";
                    
                    comm.AddParameter("@AnswerID", SqlDbType.Int, ParameterDirection.Input, answer.AnswerID);
                    comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, answer.MemberID);
                    comm.AddParameter("@AnswerValue", SqlDbType.NVarChar, ParameterDirection.Input, answer.AnswerValue);
                    comm.AddParameter("@AnswerValuePending", SqlDbType.NVarChar, ParameterDirection.Input, answer.AnswerValuePending ?? string.Empty);
                    comm.AddParameter("@AnswerStatusID", SqlDbType.Int, ParameterDirection.Input, (int)answer.AnswerStatus);
                    comm.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, answer.AdminMemberID);

                    Client.Instance.ExecuteAsyncWrite(comm);

                    answer.UpdateDate = System.DateTime.Now;
                    Brand brand = GetBrand(siteID);
                    if (answer.GenderMask <= 0 && IsPrePopulateAnswersForSortFilterEnabled(brand))
                    {
                        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(answer.MemberID, MemberLoadFlags.None);
                        PopulateAnswersWithMemberInfo(brand, answer, member);
                    }

                    //update cache
                    UpdateAnswerToCache(answer, siteID);

                    //replicate action
                    ReplicationAnswer(answer, siteID, AnswerActionType.Update);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        public AnswerList GetAnswerList(int siteID, int daysback)
        {
            string functionName = "GetAnswerList";
            //Answerli list = new AnswerList();

            //Check Cache
            AnswerList list = _cache.Get(AnswerList.GetCacheKeyString("",siteID)) as AnswerList;

            if (list == null)
            {
                list = new AnswerList();
                list.SiteID = siteID;

                SqlDataReader dataReader = null;
                try
                {
                    Stopwatch stopWatch = new Stopwatch();
                    stopWatch.Start();

                    Command comm = new Command();
                    comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                    comm.StoredProcedureName = "dbo.[up_GetAnswers_BySite]";

                    comm.AddParameter("@siteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                    comm.AddParameter("@daysback", SqlDbType.Int, ParameterDirection.Input, daysback);
                    dataReader = Client.Instance.ExecuteReader(comm);

                    //Answer info
                    ArrayList memberIDs = new ArrayList();
                    while (dataReader.Read())
                    {
                        Answer a = LoadAnswerInfo(dataReader);
                        memberIDs.Add(a.MemberID);
                        list.Answers.Add(a);
                    }

                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                        dataReader = null;
                    }

                    //Prepopulate answers with member info for sort/filter
                    Brand brand = GetBrand(siteID);
                    PrePopulateAnswersWithMemberInfoBatchProcess(brand, memberIDs, list.Answers);

                    //update cache
                    if (list != null && list.Answers.Count > 0)
                    {
                        list.CacheTTLSeconds = GetQuestionAnswerMTCacheTTL(siteID);
                        _cache.Insert(list);

                        stopWatch.Stop();
                        EventLog.WriteEntry(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "GetAnswerList(" + siteID.ToString() + ", " + daysback.ToString() + ") - Answers: " + list.Answers.Count.ToString() + ", IsPrePopulateAnswersForSortFilterEnabled: " + IsPrePopulateAnswersForSortFilterEnabled(brand).ToString() + ", SizePerBatch: " + GetMemberBatchSize(brand).ToString() + ", Elapsed Time: " + stopWatch.ElapsedMilliseconds.ToString(), EventLogEntryType.Information);
                    }

                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Dispose();
                    }
                }
            }

          
            return list;
        }

        public PagedResult<Question> GetQuestionListPaged(int siteID, int daysback, int pageNumber, int pageSize)
        {
            var cachePrefix = "RecentQuestionsAnswers";
            var key =  PagedQuestionListWrapper.GetCacheKey(cachePrefix, siteID, daysback, pageNumber, pageSize);
            var cachedQuestions = _cache.Get(key) as CacheableWrapper<PagedResult<Question>>;
            if (cachedQuestions != null)
            {
                return cachedQuestions.Data;
            }
            var answers = GetAnswerListPagedFromDb(siteID, daysback, pageNumber, pageSize);
            var questions = answers.Items.Select(x => { var q = this.GetQuestion(x.QuestionID, true).Copy(); q.Answers.Add(x); return q; }).ToList();
            var ret = new PagedResult<Question>(questions, answers.Total);
            _cache.Add(new PagedQuestionListWrapper(ret, cachePrefix, siteID, daysback, pageNumber, pageSize));
            return ret;
        }

        public PagedResult<QuestionResponse> GetActiveQuestionListPaged(int siteID, int pageNumber, int pageSize, QuestionListSort sort)
        {
            var suffix = (sort == QuestionListSort.recent) ? "recent" : "az";
            var cachePrefix = "ActiveQuestions_" + suffix;
            var key = PagedQuestionResponseListWrapper.GetCacheKey(cachePrefix, siteID, pageNumber, pageSize);
            var cachedQuestions = _cache.Get(key) as CacheableWrapper<PagedResult<QuestionResponse>>;
            if (cachedQuestions != null)
            {
                return cachedQuestions.Data;
            }
            var ret = GetActiveQuestionsFromDb(siteID, pageNumber, pageSize,sort);
            var counts = GetQuestionCountersFromDb(ret.Items.Select(x => x.QuestionID).ToList());
            ret.Items.ForEach(x => x.TotalAnswers = counts.ContainsKey(x.QuestionID)?counts[x.QuestionID]:0); // 0 can be if there are no approved answers
            _cache.Add(new PagedQuestionResponseListWrapper(ret, cachePrefix, siteID, pageNumber, pageSize));
            GetActiveQuestionIDsRequested();
            return ret;
        }

        PagedResult<QuestionResponse> GetActiveQuestionsFromDb(int siteID, int pageNumber, int pageSize, QuestionListSort sort)
        {
            var command = new Command();
            command.LogicalDatabaseName = PREMIUMSTORE_LDB;
            command.StoredProcedureName = sort == QuestionListSort.recent ? "dbo.up_GetActiveQuestions_Paged" : "dbo.up_GetActiveQuestionsAlphabetically_Paged";
            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@PageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
            command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
            var ret = new PagedResult<QuestionResponse> {Items = new List<QuestionResponse>()};
            using (var dataReader = Client.Instance.ExecuteReader(command))
            {
                while (dataReader.Read())
                {
                    ret.Items.Add(LoadQuestionInfoToQuestionResponse(dataReader));
                }
                if (dataReader.NextResult())
                {
                    if (dataReader.Read())
                    {
                        ret.Total = dataReader.GetInt32(dataReader.GetOrdinal("Count"));
                    }                    
                }
            }
            return ret;
        }

        Dictionary<int, int> GetQuestionCountersFromDb(List<int> questionIds)
        {
            var command = new Command();
            command.LogicalDatabaseName = PREMIUMSTORE_LDB;
            command.StoredProcedureName = "dbo.up_GetApprovedAnswersCount";
            command.AddParameter("@QuestionIDs", SqlDbType.Structured, ParameterDirection.Input, GetTable(questionIds, "QuestionID"));
            var ret = new Dictionary<int,int>();

            using (var dataReader = Client.Instance.ExecuteReader(command))
            {
                while (dataReader.Read())
                {
                    var counterRecord = LoadQuestionCountersInfo(dataReader);
                    ret.Add(counterRecord.Item1,counterRecord.Item2);
                }
            }
            return ret;
        }

        private Tuple<int,int> LoadQuestionCountersInfo(SqlDataReader dataReader)
        {
            
            var questionId = dataReader.IsDBNull(dataReader.GetOrdinal("QuestionID"))
                ? int.MinValue
                : dataReader.GetInt32(dataReader.GetOrdinal("QuestionID"));
            var count = dataReader.IsDBNull(dataReader.GetOrdinal("Count"))
                ? int.MinValue
                : dataReader.GetInt32(dataReader.GetOrdinal("Count"));
            return new Tuple<int, int>(questionId,count);
        }




        PagedResult<Answer> GetAnswerListPagedFromDb(int siteID, int daysback, int pageNumber, int pageSize)
        {
            string functionName = "GetAnswerList";
            //Answerli list = new AnswerList();

            var list = new AnswerList();
            list.SiteID = siteID;
            var total = 0;
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                Command comm = new Command();
                comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                comm.StoredProcedureName = "dbo.[up_GetAnswers_BySite_Paged]";
                comm.AddParameter("@pageNumber", SqlDbType.Int, ParameterDirection.Input, pageNumber);
                comm.AddParameter("@pageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
                comm.AddParameter("@siteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                comm.AddParameter("@daysback", SqlDbType.Int, ParameterDirection.Input, daysback);
                ArrayList memberIDs = new ArrayList();
                using (var dataReader = Client.Instance.ExecuteReader(comm))
                {
                    //Answer info
                    
                    while (dataReader.Read())
                    {
                        Answer a = LoadAnswerInfo(dataReader);
                        memberIDs.Add(a.MemberID);
                        list.Answers.Add(a);
                    }
                    if (dataReader.NextResult())
                    {
                        if (dataReader.Read())
                        {
                            total = dataReader.GetInt32(0);
                        }
                    }
                }

                //Prepopulate answers with member info for sort/filter
                Brand brand = GetBrand(siteID);
                PrePopulateAnswersWithMemberInfoBatchProcess(brand, memberIDs, list.Answers);

                //update cache
                if (list != null && list.Answers.Count > 0)
                {
                    stopWatch.Stop();
                    EventLog.WriteEntry(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME,
                        "GetAnswerListPaged(" + siteID.ToString() + ", " + daysback.ToString() + ", " + pageNumber + ", " + pageSize + ") - Answers: " +
                        list.Answers.Count.ToString() + ", IsPrePopulateAnswersForSortFilterEnabled: " +
                        IsPrePopulateAnswersForSortFilterEnabled(brand).ToString() + ", SizePerBatch: " +
                        GetMemberBatchSize(brand).ToString() + ", Elapsed Time: " +
                        stopWatch.ElapsedMilliseconds.ToString(), EventLogEntryType.Information);
                }

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
            
            return new PagedResult<Answer>(list.Answers,total);
        }

        public
            void InsertAnswerMemberInfo(Matchnet.Member.ServiceAdapters.Member member, int siteID)
        {
            string functionName = "InsertAnswerMemberInfo";

            try
            {
                Brand brand = GetBrand(siteID);

                if (brand != null)
                {
                    if (IsUpdateAnswerMemberInfoEnabled())
                    {
                        if (member != null)
                        {
                            Command comm = new Command();

                            comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                            comm.StoredProcedureName = "dbo.up_InsertAnswerMemberInfo";

                            comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
                            comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                            comm.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, member.GetAttributeInt(brand, "GenderMask"));
                            comm.AddParameter("@GlobalStatusMask", SqlDbType.Int, ParameterDirection.Input, member.GetAttributeInt(brand, "GlobalStatusMask", 0));
                            comm.AddParameter("@SelfSuspendedFlag", SqlDbType.Int, ParameterDirection.Input, member.GetAttributeInt(brand, "SelfSuspendedFlag", 0));
                            comm.AddParameter("@HasPhotoFlag", SqlDbType.Int, ParameterDirection.Input, member.GetAttributeInt(brand, "HasPhotoFlag", 0));

                            Client.Instance.ExecuteAsyncWrite(comm);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        #endregion

        #region Private methods

        private Question LoadQuestionInfo(SqlDataReader dataReader)
        {
            return LoadQuestionInfo<Question>(dataReader);
        }

        private T LoadQuestionInfo<T>(SqlDataReader dataReader) where T:Question,new()
        {
            T question = new T();
            LoadQuestionInfo(dataReader, question);
            return question;
        }
        private QuestionResponse LoadQuestionInfoToQuestionResponse(SqlDataReader dataReader)
        {
            QuestionResponse question = new QuestionResponse();
            LoadQuestionInfo(dataReader, question);
            return question;
        }

        private void LoadQuestionInfo(SqlDataReader dataReader, Question question)
        {
            question.QuestionID = dataReader.IsDBNull(dataReader.GetOrdinal("QuestionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("QuestionID"));
            question.SiteID = dataReader.IsDBNull(dataReader.GetOrdinal("SiteID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("SiteID"));
            question.Text = dataReader.IsDBNull(dataReader.GetOrdinal("QuestionValue")) ? "" : dataReader.GetString(dataReader.GetOrdinal("QuestionValue"));
            question.StartDate = dataReader.IsDBNull(dataReader.GetOrdinal("StartDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("StartDate"));
            question.EndDate = dataReader.IsDBNull(dataReader.GetOrdinal("EndDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("EndDate"));
            question.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            question.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            question.AdminMemberID = dataReader.IsDBNull(dataReader.GetOrdinal("AdminMemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("AdminMemberID"));
            question.Weight = dataReader.IsDBNull(dataReader.GetOrdinal("Weight")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("Weight"));
            question.Active = dataReader.IsDBNull(dataReader.GetOrdinal("Active")) ? true : Convert.ToBoolean(dataReader.GetValue(dataReader.GetOrdinal("Active")));
            question.QuestionTypeID = dataReader.IsDBNull(dataReader.GetOrdinal("QuestionTypeID")) ? int.MinValue : Convert.ToInt32(dataReader.GetValue(dataReader.GetOrdinal("QuestionTypeID")));
            
            //TODO - Question type
        }

        private Answer LoadAnswerInfo(SqlDataReader dataReader)
        {
            Answer answer = new Answer();
            answer.AnswerID = dataReader.IsDBNull(dataReader.GetOrdinal("AnswerID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("AnswerID"));
            answer.QuestionID = dataReader.IsDBNull(dataReader.GetOrdinal("QuestionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("QuestionID"));
            answer.MemberID = dataReader.IsDBNull(dataReader.GetOrdinal("MemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("MemberID"));
            answer.AdminMemberID = dataReader.IsDBNull(dataReader.GetOrdinal("AdminMemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("AdminMemberID"));
            answer.AnswerValue = dataReader.IsDBNull(dataReader.GetOrdinal("AnswerValue")) ? "" : dataReader.GetString(dataReader.GetOrdinal("AnswerValue"));
            answer.AnswerValuePending = dataReader.IsDBNull(dataReader.GetOrdinal("AnswerValuePending")) ? "" : dataReader.GetString(dataReader.GetOrdinal("AnswerValuePending"));
            answer.AnswerStatus = dataReader.IsDBNull(dataReader.GetOrdinal("AnswerStatusID")) ? QuestionAnswerEnums.AnswerStatusType.None : (QuestionAnswerEnums.AnswerStatusType)Enum.ToObject(typeof(QuestionAnswerEnums.AnswerStatusType), dataReader.GetInt32(dataReader.GetOrdinal("AnswerStatusID")));
            answer.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            answer.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));
            answer.GenderMask = dataReader.IsDBNull(dataReader.GetOrdinal("GenderMask")) ? answer.GenderMask : dataReader.GetInt32(dataReader.GetOrdinal("GenderMask"));
            answer.GlobalStatusMask = dataReader.IsDBNull(dataReader.GetOrdinal("GlobalStatusMask")) ? answer.GlobalStatusMask : dataReader.GetInt32(dataReader.GetOrdinal("GlobalStatusMask"));
            int hasPhoto = dataReader.IsDBNull(dataReader.GetOrdinal("HasPhotoFlag")) ? 1 : dataReader.GetInt32(dataReader.GetOrdinal("HasPhotoFlag"));
            if (hasPhoto <= 0)
            {
                answer.HasApprovedPhotos = false;
            }
            else
            {
                answer.HasApprovedPhotos = true;
            }
            answer.SelfSuspendedFlag = dataReader.IsDBNull(dataReader.GetOrdinal("SelfSuspendedFlag")) ? answer.SelfSuspendedFlag : dataReader.GetInt32(dataReader.GetOrdinal("SelfSuspendedFlag"));

            return answer;
        }

        private StockAnswer LoadStockAnswerInfo(SqlDataReader dataReader)
        {
            StockAnswer answer = new StockAnswer();
            answer.StockAnswerID= dataReader.IsDBNull(dataReader.GetOrdinal("StockAnswerID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("StockAnswerID"));
            answer.QuestionID = dataReader.IsDBNull(dataReader.GetOrdinal("QuestionID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("QuestionID"));
            answer.SortOrder = dataReader.IsDBNull(dataReader.GetOrdinal("SortOrder")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("SortOrder"));
            answer.AdminMemberID = dataReader.IsDBNull(dataReader.GetOrdinal("AdminMemberID")) ? int.MinValue : dataReader.GetInt32(dataReader.GetOrdinal("AdminMemberID"));
            answer.AnswerValue = dataReader.IsDBNull(dataReader.GetOrdinal("AnswerValue")) ? "" : dataReader.GetString(dataReader.GetOrdinal("AnswerValue"));
            answer.InsertDate = dataReader.IsDBNull(dataReader.GetOrdinal("InsertDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("InsertDate"));
            answer.UpdateDate = dataReader.IsDBNull(dataReader.GetOrdinal("UpdateDate")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("UpdateDate"));

            return answer;
        }

        private Question LoadQuestionInfo(int questionID, int siteID, string text, DateTime startDate, DateTime endDate, DateTime insertDate, DateTime updateDate, int AdminMemberID, int Weight, bool active, int questionTypeId)
        {
            Question question = new Question();
            question.QuestionID = questionID;
            question.SiteID = siteID;
            question.Text = text;
            question.StartDate = startDate;
            question.EndDate = endDate;
            question.InsertDate = insertDate;
            question.UpdateDate = updateDate;
            question.AdminMemberID = AdminMemberID;
            question.Weight = Weight;
            question.Active = active;
            question.QuestionTypeID = questionTypeId;
            return question;
        }

        private Answer LoadAnswerInfo(int answerID, int questionID, int memberID, int adminMemberID, string answerValue, string answerPendingValue, QuestionAnswerEnums.AnswerStatusType answerStatus, DateTime insertDate, DateTime updateDate)
        {
            Answer answer = new Answer();
            answer.AnswerID = answerID;
            answer.QuestionID = questionID;
            answer.MemberID = memberID;
            answer.AdminMemberID = adminMemberID;
            answer.AnswerValue = answerValue;
            answer.AnswerValuePending = answerPendingValue;
            answer.AnswerStatus = answerStatus;
            answer.InsertDate = insertDate;
            answer.UpdateDate = updateDate;

            return answer;
        }

        private void ApprovalEnqueue(int siteID, int memberID)
        {
            string functionName = "ApprovalEnqueue";

            try
            {
                Sites sites = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();

                Site site = (Site)(from Site s in sites
                                   where s.SiteID == siteID
                                   select s).ToArray()[0];

                DBQ.QueueItemText queueItem = new DBQ.QueueItemText();
                queueItem.CommunityID = site.Community.CommunityID;
                queueItem.SiteID = siteID;
                queueItem.BrandID = Constants.NULL_INT;
                queueItem.MemberID = memberID;
                queueItem.LanguageID = site.LanguageID;
                queueItem.TextType = DBQ.TextType.QA;

                DBApproveQueueSA.Instance.Enqueue(queueItem);

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void AddAnswerToCache(Answer answer, int siteID)
        {
            //Question
            Question question = _cache.Get(Question.GetCacheKeyString(answer.QuestionID)) as Question;
            if (question != null)
            {
                question.Answers.Insert(0, answer);
                _cache.Insert(question);
            }
            else
            {
                question = GetQuestion(answer.QuestionID);
            }

            //MemberQuestion
            // Invalidate cache for member questions
            _cache.Remove(MemberQuestionList.GetCacheKeyString(siteID, answer.MemberID));
        }

        private void UpdateAnswerToCache(Answer answer, int siteID)
        {
            //Question
            Question question = _cache.Get(Question.GetCacheKeyString(answer.QuestionID)) as Question;
            if (question != null)
            {
                int answerIndex = Constants.NULL_INT;
                for (int i = 0; i < question.Answers.Count; i++)
                {
                    if (question.Answers[i].AnswerID == answer.AnswerID)
                    {
                        answerIndex = i;
                        break;
                    }
                }

                if (answerIndex >= 0)
                {
                    question.Answers[answerIndex] = answer;
                }

                _cache.Insert(question);
            }
            else
            {
                question = GetQuestion(answer.QuestionID);
            }

            //MemberQuestion
            MemberQuestionList memberQuestionList = _cache.Get(MemberQuestionList.GetCacheKeyString(siteID, answer.MemberID)) as MemberQuestionList;
            if (memberQuestionList != null && question != null)
            {
                foreach (Question q in memberQuestionList.Questions)
                {
                    if (q.QuestionID == answer.QuestionID)
                    {
                        int answerIndex = Constants.NULL_INT;
                        for (int i = 0; i < q.Answers.Count; i++)
                        {
                            if (q.Answers[i].AnswerID == answer.AnswerID)
                            {
                                answerIndex = i;
                                break;
                            }
                        }

                        if (answerIndex >= 0)
                        {
                            q.Answers[answerIndex] = answer;
                        }
                        break;
                    }
                }

                _cache.Insert(memberQuestionList);
            }
        }

        private void RemoveAnswerFromCache(Answer answer, int siteID)
        {
            //Question
            Question question = _cache.Get(Question.GetCacheKeyString(answer.QuestionID)) as Question;
            if (question != null)
            {
                int answerIndex = Constants.NULL_INT;
                for (int i = 0; i < question.Answers.Count; i++)
                {
                    if (question.Answers[i].AnswerID == answer.AnswerID)
                    {
                        answerIndex = i;
                        break;
                    }
                }

                if (answerIndex >= 0)
                {
                    question.Answers.RemoveAt(answerIndex);
                }

                _cache.Insert(question);
            }

            //MemberQuestion
            MemberQuestionList memberQuestionList = _cache.Get(MemberQuestionList.GetCacheKeyString(siteID, answer.MemberID)) as MemberQuestionList;
            if (memberQuestionList != null)
            {
                int questionIndex = Constants.NULL_INT;
                for (int i = 0; i < memberQuestionList.Questions.Count; i++)
                {
                    if (memberQuestionList.Questions[i].QuestionID == answer.QuestionID)
                    {
                        questionIndex = i;
                        break;
                    }
                }

                if (questionIndex >= 0)
                {
                    memberQuestionList.Questions.RemoveAt(questionIndex);
                }

                _cache.Insert(memberQuestionList);
            }
        }

        private void ReplicationAnswer(Answer answer, int siteID, AnswerActionType answerActionType)
        {
            AnswerReplicationAction answerReplicationAction = new AnswerReplicationAction();
            answerReplicationAction.AnswerActionType = answerActionType;
            answerReplicationAction.Answer = answer;
            answerReplicationAction.SiteID = siteID;
            ReplicationRequested(answerReplicationAction);
        }

        private void PlayReplicatedAnswer(AnswerReplicationAction answerReplicationAction)
        {
            if (answerReplicationAction != null && answerReplicationAction.Answer != null)
            {
                switch (answerReplicationAction.AnswerActionType)
                {
                    case AnswerActionType.Add:
                        AddAnswerToCache(answerReplicationAction.Answer, answerReplicationAction.SiteID);
                        break;
                    case AnswerActionType.Remove:
                        RemoveAnswerFromCache(answerReplicationAction.Answer, answerReplicationAction.SiteID);
                        break;
                    case AnswerActionType.Update:
                        UpdateAnswerToCache(answerReplicationAction.Answer, answerReplicationAction.SiteID);
                        break;
                }
            }
        }

        private Brand GetBrand(int siteID)
        {
            Brand brand = null;
            if (!_brands.ContainsKey(siteID))
            {
                lock (_brandsLock)
                {
                    if (!_brands.ContainsKey(siteID))
                    {
                        _brands.Remove(siteID);
                        Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);

                        //default brand should have the smallest brandID
                        Brand defaultBrand = null;
                        foreach (Brand b in brands)
                        {
                            if (defaultBrand == null)
                            {
                                defaultBrand = b;
                            }
                            else if (b.BrandID < defaultBrand.BrandID)
                            {
                                defaultBrand = b;
                            }
                        }
                        _brands.Add(siteID, defaultBrand);
                    }
                }
            }

            brand = _brands[siteID];

            return brand;
        }

        private Matchnet.Member.ServiceAdapters.Member GetMemberFromList(int memberID, ArrayList memberList, bool checkMT)
        {
            Matchnet.Member.ServiceAdapters.Member member = null;
            if (memberList != null && memberList.Count > 0)
            {
                foreach (Matchnet.Member.ServiceAdapters.Member m in memberList)
                {
                    if (m.MemberID == memberID)
                    {
                        member = m;
                        break;
                    }
                }
            }

            if (member == null && checkMT)
            {
                member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            }

            return member;
        }

        private bool IsMemberSuspended(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            bool isSuspended = false;
            if (member != null)
            {
                //self-suspended flag
                if (member.GetAttributeInt(brand, "SelfSuspendedFlag", 0) == 1)
                {
                    isSuspended = true;
                }
                //admin suspend
                else if ((member.GetAttributeInt(brand, "GlobalStatusMask", 0) & 1) == 1)
                {
                    isSuspended = true;
                }
            }

            return isSuspended;
        }

        private int GetQuestionAnswerMTCacheTTL(int siteID)
        {
            int cacheSeconds = Constants.NULL_INT;
            try
            {
                cacheSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QUESTION_ANSWER_MT_CACHE_TTL", Constants.NULL_INT, siteID, Constants.NULL_INT));
            }
            catch (Exception ex)
            {
                //setting missing
                cacheSeconds = Constants.NULL_INT;
            }

            if (cacheSeconds <= 0)
            {
                Question q = new Question();
                cacheSeconds = q.CacheTTLSeconds;
            }
            return cacheSeconds;
        }

        private bool IsPrePopulateAnswersForSortFilterEnabled(Brand brand)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QA_PREPROPULATE_FOR_SORT_FILTER_MT", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        private bool IsUpdateAnswerMemberInfoEnabled()
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_QA_UPDATE_ANSWERMEMBERINFO");
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;
        }

        private int GetMemberBatchSize(Brand brand)
        {
            int batchSize = 10;
            try
            {
                batchSize = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QA_PREPOPULATE_MEMBER_BATCH_SIZE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                //setting missing
                batchSize = 10;
            }

            return batchSize;

        }

        private void PrePopulateAnswersWithMemberInfoBatchProcess(Brand brand, ArrayList memberIDs, List<Answer> answers)
        {
            try
            {
                int sizePerBatch = GetMemberBatchSize(brand);
                if (memberIDs.Count > 0 && IsPrePopulateAnswersForSortFilterEnabled(brand))
                {
                    //get genderMask for each answer and filter answers from suspended members
                    int totalBatches = memberIDs.Count / sizePerBatch;
                    if ((memberIDs.Count % sizePerBatch) > 0)
                    {
                        totalBatches += 1;
                    }

                    for (int b = 1; b <= totalBatches; b++)
                    {
                        //get batch of members
                        int batchStartIndex = (b - 1) * sizePerBatch;
                        int batchEndIndex = batchStartIndex + sizePerBatch;
                        if ((batchEndIndex + 1) > answers.Count)
                        {
                            batchEndIndex = answers.Count - 1;
                        }

                        PopulateAnswersWithMemberInfoBatch(brand, memberIDs, answers, batchStartIndex, batchEndIndex);
                    }
                }
            }
            catch (Exception ex)
            {
                string message = "PrePopulateAnswersWithMemberInfoBatchProcess error: " + ex.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        private void PopulateAnswersWithMemberInfoBatch(Brand brand, ArrayList memberIDs, List<Answer> answers, int batchStartIndex, int batchEndIndex)
        {
            try
            {
                ArrayList batchMemberIDs = memberIDs.GetRange(batchStartIndex, (batchEndIndex - batchStartIndex) + 1);
                ArrayList batchMembers = MemberSA.Instance.GetMembers(batchMemberIDs, MemberLoadFlags.None);

                for (int a = batchStartIndex; a <= batchEndIndex; a++)
                {
                    Answer currentAnswer = answers[a];
                    Matchnet.Member.ServiceAdapters.Member currentMember = GetMemberFromList(currentAnswer.MemberID, batchMembers, true);
                    PopulateAnswersWithMemberInfo(brand, currentAnswer, currentMember);
                }
            }
            catch (Exception ex)
            {
                string message = "PrePopulateAnswersWithMemberInfoBatch error: " + ex.Message;
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, message, System.Diagnostics.EventLogEntryType.Error);
            }
        }

        private void PopulateAnswersWithMemberInfo(Brand brand, Answer currentAnswer, Matchnet.Member.ServiceAdapters.Member currentMember)
        {
            if (currentMember != null)
            {
                currentAnswer.GenderMask = currentMember.GetAttributeInt(brand, "GenderMask");
                currentAnswer.GlobalStatusMask = currentMember.GetAttributeInt(brand, "GlobalStatusMask", 0);
                currentAnswer.SelfSuspendedFlag = currentMember.GetAttributeInt(brand, "SelfSuspendedFlag", 0);
                currentAnswer.HasApprovedPhotos = (currentMember.GetAttributeInt(brand, "HasPhotoFlag", 0) == 1);
            }
        }

        #endregion

    }
}
