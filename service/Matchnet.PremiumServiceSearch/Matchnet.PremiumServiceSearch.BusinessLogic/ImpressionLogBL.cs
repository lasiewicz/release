﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using Matchnet.Queuing;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.Data;
namespace Matchnet.PremiumServiceSearch.BusinessLogic
{
    public class ImpressionLogBL:IProcessorWorker
    {
        const string MODULE_NAME = "ImpressionLogBL";
        const string PREMIUMSTORE_LDB = "mnPremiumServices";
        public const string LOG_QUEUE_PATH = @".\private$\premiumservicelog";
        public const int QUEUE_THREADS = 1;


        private Matchnet.Queuing.Processor _queueProcessor;
        
        public void ProcessMessage(object messageBody)
        {
            string functionName = "ProcessMessage";
           
            try
            {
                saveLog(messageBody as ValueObjects.QueueMessage.ImpressionLogMessage);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, messageBody as ValueObjects.QueueMessage.ImpressionLogMessage);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, messageBody as ValueObjects.QueueMessage.ImpressionLogMessage, null, "", false);
                throw;
            }


        }

       public static void Enqueue(int siteid, int pageid, ValueObjects.QueueMessage.ImpressionType viewtype, int viewedmemberid, int viewingmemberid, DateTime impressiontime)
        {
            string functionName = "Enqueue";
            PremiumServiceSearch.ValueObjects.QueueMessage.ImpressionLogMessage message = null;

           try
           {
               message = new ValueObjects.QueueMessage.ImpressionLogMessage(siteid, pageid, viewtype, viewingmemberid, viewedmemberid, impressiontime);

               System.Messaging.MessageQueue queue = Matchnet.Queuing.Util.GetQueue(LOG_QUEUE_PATH, true, true);
               queue.Formatter = new BinaryMessageFormatter();
               MessageQueueTransaction trans = new MessageQueueTransaction();

               try
               {
                   trans.Begin();
                   queue.Send(message, trans);

                   trans.Commit();
               }
               finally
               {
                   trans.Dispose();
               }
           }
           catch (Exception ex)
           {
               ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, message);

               ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, message, null, "", false);
               throw;
           }

       }

       private void saveLog(ValueObjects.QueueMessage.ImpressionLogMessage message)
       {

           string functionName = "saveLog";
           Command comm = null;
             try
           {
               if(message==null)
                  return;
               comm = new Command();
               comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
               comm.StoredProcedureName = "up_InsertImpressionslog";

               int logid= Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("ImpressionsLogID");
               comm.AddParameter("@LogID", SqlDbType.Int, ParameterDirection.Input, logid);
               comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, message.SiteID);
               comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, message.ViewingMemberID);
               comm.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, message.ViewedMemberID);
               comm.AddParameter("@ViewTypeID", SqlDbType.Int, ParameterDirection.Input, (int)message.ImpressionLogType);
               comm.AddParameter("@PageID", SqlDbType.Int, ParameterDirection.Input, message.PageID);
               comm.AddParameter("@ViewTime", SqlDbType.DateTime, ParameterDirection.Input, message.ImpressionDateTime);

               Client.Instance.ExecuteAsyncWrite(comm);


           }
           catch (Exception ex)
           {
               ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, message);

               ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, message, comm, "", false);
               throw;
           }

       }
    }
}
