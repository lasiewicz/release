﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Geo;

namespace Matchnet.PremiumServiceSearch.BusinessLogic.SpotlightMember
{
    public class SpotlightSearchBL
    {
        const string MODULE_NAME = "SpotlightSearchBL";
        const string PREMIUMSTORE_LDB = "mnPremiumServices";
        const string PREMIUMSTORE_SP_SEARCH = "up_SearchSpotlightMember";
        const string PREMIUMSTORE_PROMOTION_SEARCH = "up_GetPromotionalMembers";
        const string PROMOTIONAL_MEMBERS = "PromotionalMembers";
        private Matchnet.Caching.Cache _cache;

        #region perfomance counters events
        public delegate void SpotlightSearchRequestEventHandler(bool cacheHit, bool promotionflag, int count);
        public event SpotlightSearchRequestEventHandler SpotlightSearchRequested;


        #endregion
      
        #region singleton implementation
        public readonly static SpotlightSearchBL Instance = new SpotlightSearchBL();

        private SpotlightSearchBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
            PopulatePromotionalMembers();

        }
        #endregion

        public ResultList Search(Query query)
        {
            const string functionName = "Search";
            ResultList results = null;
            PremiumMemberCollection members = null;
            PremiumMember member=null;

            try
            {
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
                if (query == null)
                    return null;

                if (!query.Parameters.Get<bool>(SpotlightConstants.SearchParameters.promotionflag))
                {
                    results = getFromCache(query);
                    if (results == null)
                    {
                        members = new PremiumMemberCollection();
                        Command comm = new Command();
                        comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                        comm.StoredProcedureName = PREMIUMSTORE_SP_SEARCH;

                        comm.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.brandid));
                        comm.AddParameter("@PromotionalMemberFlag", SqlDbType.Bit, ParameterDirection.Input, query.Parameters.Get<bool>(SpotlightConstants.SearchParameters.promotionflag));
                        comm.AddParameter("@Age", SqlDbType.SmallInt, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.age));
                        comm.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.gendermask));
                        comm.AddParameter("@BoxX", SqlDbType.Int, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.boxx));
                        comm.AddParameter("@BoxY", SqlDbType.Int, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.boxy));
                        comm.AddParameter("@depth1regionid", SqlDbType.Int, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.depth1regionid));
                        comm.AddParameter("@depth2regionid", SqlDbType.Int, ParameterDirection.Input, query.Parameters.Get<int>(SpotlightConstants.SearchParameters.depth2regionid));
                       
                        using(SqlDataReader rs = Client.Instance.ExecuteReader(comm))
                        {
                            results = new ResultList(query.GetCacheKeyString('_'));
                            while (rs.Read())
                            {
                                member = SpotlightCommon.populateMember(rs);
                                if (member != null)
                                {
                                    if (validate(query, member))
                                        results.Results.Add(member.MemberID);
                                }

                            }
                         }
                        int count = 0;
                        if (results.Results.Count > 0)
                        {
                            insertCache(results);
                            count = results.Results.Count;

                        }

                        SpotlightSearchRequested(false, false, count);

                    }
                }
                else
                {
                    results = SearchPromotions(query);



                }
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Exit method");

                return results;

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, member);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, query, null, "", false);
                throw;
            }
            finally
            { 
            
            }
        }

        public ResultList getFromCache(Query query)
        {
            const string functionName = "getFromCache";
            ResultList results = null;
            
            try
            {
                string queryKey = query.GetCacheKeyString('_');

                results = (ResultList)_cache.Get(queryKey);


                return results;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, query);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, query, null, "", false);
                throw;
            }

        }

        public PremiumMemberCollection getPromotionalMembersFromCache(string key)
        {
            const string functionName = "getPromotionalMembersFromCache";
            PremiumMemberCollection members = null;

            try
            {

                members = (PremiumMemberCollection)_cache.Get(key);

                return members;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, members, null, "key:" + key, false);
                throw;
            }

        }

        public void insertCache(Query query, PremiumMemberCollection members)
        {
            const string functionName = "insertCache";
      

            try
            {
                string key = query.GetCacheKeyString('_');
               
                if (members != null)
                    members.setCacheKey(key);

                _cache.Insert(members);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, query);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, query, null, members.ToString(), true);
            }

        }


        public void insertCache(string key, PremiumMemberCollection members)
        {
            const string functionName = "insertCache";


            try
            {
                
                if (members != null)
                    members.setCacheKey(key);

                _cache.Insert(members);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, null, null, "key:" + key, true);
            }

        }

        
        public void insertCache( ResultList results)
        {
            const string functionName = "insertCache";


            try
            {

                if (results != null && !String.IsNullOrEmpty(results.CacheKey))
                {
                   
                    _cache.Insert(results);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, results, null, "", true);
            }

        }


        public bool validate(Query query, PremiumMember member)
        {
            const string functionName = "validate";
            bool valid=false;
            try
            {
                //member coord
                double lat = member.ServiceInstanceMember.Get<double>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Latitude);
                double longit = member.ServiceInstanceMember.Get<double>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Longitude);
                int distance = member.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance);
                
                if(lat==0.0 || longit ==0.0 || distance==0)
                    return true;
                //query coord
               
                double lat1 = (double)query.Parameters.Get<double>(SpotlightConstants.SearchParameters.latitude);
                double longit1 = (double)query.Parameters.Get<double>(SpotlightConstants.SearchParameters.longitude);
                Matchnet.Geo.Coordinates memberCoord = new Coordinates(lat,longit);
                Matchnet.Geo.Coordinates queryCoord = new Coordinates(lat1, longit1);

                double dist= Matchnet.Geo.GeoUtil.CalculateDistance(memberCoord, queryCoord, true);
                if (dist <= distance)
                    valid = true;

                return valid;

                   
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, query);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, query, null, member.ToString(), false);
                throw;
            }

        }

        public PremiumMemberCollection PopulatePromotionalMembers()
        {
            const string functionName = "PopulatePromotionalMembers";
            PremiumMemberCollection members;
            PremiumMember member = null;
            try
            {
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
                members = getPromotionalMembersFromCache(PROMOTIONAL_MEMBERS);
                if (members == null)
                {
                    members = new PremiumMemberCollection();
                    var comm = new Command
                                   {
                                       LogicalDatabaseName = PREMIUMSTORE_LDB,
                                       StoredProcedureName = PREMIUMSTORE_PROMOTION_SEARCH
                                   };

                    using (var rs = Client.Instance.ExecuteReader(comm))
                    {
                        while (rs.Read())
                        {
                            member = SpotlightCommon.populateMember(rs);
                            members.Add(member);
                        }
                    }
                    insertCache(PROMOTIONAL_MEMBERS, members);
                    var count = members.Count;
                    ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Exit method: members count:" + count.ToString());
                }
                return members;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, member);
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, null, null, "", false);

                throw;
            }
        }


        public ResultList SearchPromotions(Query query)
        {

            const string functionName = "SearchCollection";
            IEnumerable<PremiumMember> q = null;
            PremiumMemberCollection cachedMembers=null;
            PremiumMemberCollection members = null;
            ResultList results = null;
            int count = 0;
            try
            {
                string key="";
                if (query == null)
                    return results;

                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName,  query.ToString());

                key = PremiumMemberCollection.GetCacheKeyString(PROMOTIONAL_MEMBERS);
               
                cachedMembers = PopulatePromotionalMembers();

                members = new PremiumMemberCollection();
                if (cachedMembers != null)
                {
                    q = from m in cachedMembers
                               where m.BrandID == query.Parameters.Get<int>(SpotlightConstants.SearchParameters.brandid) &&
                               m.ServiceInstanceMember.Get<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag)==true &&
                               m.ServiceInstanceMember.Get<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate)>= DateTime.Now &&                     
                               ((m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMin) <= query.Parameters.Get<int>(SpotlightConstants.SearchParameters.boxx) &&
                               m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMax) >= query.Parameters.Get<int>(SpotlightConstants.SearchParameters.boxx)) &&
                               (m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMin) <= query.Parameters.Get<int>(SpotlightConstants.SearchParameters.boxy) &&
                               m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMax) >= query.Parameters.Get<int>(SpotlightConstants.SearchParameters.boxy))) &&
                               (m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID) == query.Parameters.Get<int>(SpotlightConstants.SearchParameters.depth1regionid )||  m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID)==0) &&
                               (m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID) == query.Parameters.Get<int>(SpotlightConstants.SearchParameters.depth2regionid )||  m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID)==0) &&
                               (m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID) == query.Parameters.Get<int>(SpotlightConstants.SearchParameters.depth3regionid) ||  m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID)==0) &&
                               (m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask) == query.Parameters.Get<int>(SpotlightConstants.SearchParameters.gendermask) || m.ServiceInstanceMember.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask) == 0)
                               select m;

                }
                if (q != null)
                {
                    results=new ResultList(query.GetCacheKeyString('_'));
                    List<PremiumMember> list = q.ToList<PremiumMember>();
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (validate(query, list[i]))
                            results.Results.Add(list[i].MemberID);
                    }
                    count = results.Results.Count;
                }

                SpotlightSearchRequested(true, true, count);
                return results;


            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, query);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, query, null, "", false);
                throw;
            }


        }


    }
}
