﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.PremiumServiceSearch.BusinessLogic.SpotlightMember
{
    public class QueueProcessor
    {
        const string MODULE_NAME = "PremiumServiceSearch.QueueProcessor";
        public const string QUEUE_PATH = @".\private$\SpotlightMember";
        bool _runnable;

        private MessageQueue premiumMemberQueue;
        private Thread[] threads;

        public void Start()
        {
            const string functionName = "Start";
            try
            {
                startThreads();
            }
            catch (Exception ex)
            {

                PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);

            }

        }


        public void Stop()
        {
            const string functionName = "Stop";
            try
            {
                stopThreads();
            }
            catch (Exception ex)
            {

                PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);

            }

        }

        private void startThreads()
        {
            const string functionName = "startThreads";
            try
            {
                premiumMemberQueue = new MessageQueue(QUEUE_PATH);
                premiumMemberQueue.Formatter = new BinaryMessageFormatter();

                Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSEARCHSVC_THREAD_COUNT"));
                threads = new Thread[threadCount];

                for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
                {
                    Thread t = new Thread(new ThreadStart(processQueue));
                    t.Name = "ProcessMemberThread" + threadNum.ToString();
                    t.Start();
                    threads[threadNum] = t;
                    _runnable = true;
                }
            }
            catch (Exception ex)
            {
                PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

        }


        private void stopThreads()
        {
            const string functionName = "stopThreads";
            _runnable = false;
            try
            {
               Int16 threadCount = (Int16)threads.Length;

			for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
			{
				threads[threadNum].Join(10000);
			}

            PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Stopped all SpotlightMember threads");

    		}
            catch (Exception ex)
            {
                PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                
            }

        }

        public static void Enqueue(PremiumServiceSearch.ValueObjects.QueueMessage.IQueueMessage message)
        {
            const string functionName = "Enqueue";
            try
            {
                if (!MessageQueue.Exists(QUEUE_PATH))
                {
                    MessageQueue.Create(QUEUE_PATH, true);
                }

                MessageQueue queue = new MessageQueue(QUEUE_PATH);
                queue.Formatter = new BinaryMessageFormatter();
                MessageQueueTransaction trans = new MessageQueueTransaction();

                try
                {
                    trans.Begin();
                    queue.Send(message, trans);

                    trans.Commit();
                }
                finally
                {
                    trans.Dispose();
                }

            }
            catch (Exception ex)
            {
                PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }
        
        private void processQueue()
        {
            const string functionName = "processQueue";
            try
            {
                while (_runnable)
                {
                    processQueueTransaction();
                }

            }
           
            catch (Exception ex)
            {
           
                    PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
           
            }
            

        }

        private void processQueueTransaction()
        {
            MessageQueueTransaction tran = null;

            try
            {
                tran = new MessageQueueTransaction();
                
                tran.Begin();
                
                var queueItem = premiumMemberQueue.Receive(tran);

                if (queueItem == null)
                {
                    Thread.Sleep(1000);
                }
                else
                {
                    var message = (ValueObjects.QueueMessage.IQueueMessage)queueItem.Body;

                    processMessage(message, tran);
                }

                tran.Commit();
            }
            catch (Exception ex)
            {
                new BLException("Error processing a Spotlight queue item.", ex);

                attemptRollback(tran);

                Thread.Sleep(5000);
            }
            finally
            {
                if (tran != null)
                {
                    tran.Dispose();
                }

            }
        }

        private void processMessage(PremiumServiceSearch.ValueObjects.QueueMessage.IQueueMessage msg, MessageQueueTransaction tran)
        {

            try
            {
                PremiumServiceSearch.ValueObjects.QueueMessage.SaveMemberMessage savemsg = (PremiumServiceSearch.ValueObjects.QueueMessage.SaveMemberMessage)msg;

                if (savemsg.Member == null)
                {
                    SpotlightMemberBL.Instance.SaveMember(savemsg.BrandID, savemsg.MemberID, savemsg.ExpirationDate, savemsg.EnableFlag);

                }
                else
                {
                    SpotlightMemberBL.Instance.SaveMember(savemsg.Member);
                }
            
            }
            catch (Exception ex)
            {
                attemptResend(tran, msg, ex);
            }
        }

        private void attemptResend(MessageQueueTransaction tran, PremiumServiceSearch.ValueObjects.QueueMessage.IQueueMessage msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();

                    msg.Errors.Add(ex.Source + ", " + ex.Message);
                    msg.Attempts += 1;
                    msg.LastAttemptDateTime = DateTime.Now;

                    Enqueue(msg);
                }
            }
            catch (Exception e)
            {
                var message = "Exception while trying to resend msg: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                throw (new BLException(message));
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                }
            }
            catch (Exception ex)
            {
                var message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                throw (new BLException(message));
            }
        }

   
    }
}
