﻿using System;
using System.Data.SqlTypes;
using System.Web.Caching;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Geo;
using Matchnet.PremiumServiceSearch.ValueObjects.QueueMessage;

using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.CacheSynchronization.Tracking;
namespace Matchnet.PremiumServiceSearch.BusinessLogic.SpotlightMember
{
    public class SpotlightMemberBL
    {
        const string MODULE_NAME = "SpotlightMemberBL";
        const string PREMIUMSTORE_LDB = "mnPremiumServices";
        const string PREMIUMSTORE_SP_SAVE_MEMBER = "up_SaveSpotlightMember";
        const string PREMIUMSTORE_SP_GET_MEMBER = "up_GetSpotlightMember";

        const string DATA_FORMAT = "memberid:{0}, brandid:{1}";
        private Matchnet.Caching.Cache _cache;

        #region perfomance counters events
        public delegate void SpotlightMemberRequestEventHandler(bool cacheHit);
        public event SpotlightMemberRequestEventHandler SpotlightMemberRequested;
       
        public delegate void SpotlightMemberUpdateEventHandler();
        public event SpotlightMemberUpdateEventHandler SpotlightMemberUpdated;



        #endregion

        #region synchronization
        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        public event SynchronizationEventHandler SynchronizationRequested;


        private CacheItemRemovedCallback expireMember;

        private void ExpireMemberCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            //MemberRemoved();

            PremiumMember member = value as PremiumMember;
            if (member != null)
            {
                ReplicationRequested(member);
                SynchronizationRequested(member.GetCacheKey(),
                    member.ReferenceTracker.Purge(Constants.NULL_STRING));
                // IncrementExpirationCounter();
            }
        }


        #endregion

        #region singleton implementation
        public readonly static SpotlightMemberBL Instance = new SpotlightMemberBL();

        private SpotlightMemberBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
            expireMember = new CacheItemRemovedCallback(this.ExpireMemberCallback);

        }
        #endregion

        #region async calls
        public void EnqueueMember(int brandid, int memberid, DateTime expirationDate, bool enableFlag)
        {
            const string functionName = "EnqueueMember";
            string trace = String.Format(DATA_FORMAT, memberid, brandid);
            ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
            try
            {
                SaveMemberMessage msg = new SaveMemberMessage(QueueMessageType.update, brandid, memberid, expirationDate, enableFlag);
                QueueProcessor.Enqueue(msg);
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Exit method");

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());
               // throw(new ServiceBoundaryException(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME,"Error enqueue member:" + trace + "\r\n" + ex.ToString()));
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, null, null, trace, true);
            }
        }


        public void EnqueueMember(PremiumMember member)
        {
            const string functionName = "EnqueueMember";
            string trace="";
            ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
            try
            {
                if (member != null && member.ServiceInstanceMember != null)
                {
                    trace=String.Format(DATA_FORMAT, member.MemberID, member.BrandID);
                    
                    SaveMemberMessage msg = new SaveMemberMessage(member);
                    QueueProcessor.Enqueue(msg);
                }
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Exit method");

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, member, null, trace, true);
            }
        }

        #endregion

        #region sync calls

        public PremiumMember GetServiceMember(int brandid, int memberid, int serviceinstanceid)
        {
            const string functionName = "GetServiceMember";
            string trace = String.Format(DATA_FORMAT, memberid, brandid);
            PremiumMember premiummember = null;
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand=Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                if(brand==null)
                    return null;

                premiummember = getCachedMember(brand.BrandID, memberid, serviceinstanceid);
                if (premiummember == null)
                {
                    premiummember = getMember(brand.BrandID, memberid, serviceinstanceid);
                    if (premiummember != null)
                    {
                        InsertCacheMember(premiummember, true, false);
                    }

                }
               

                return premiummember;

            }
            catch (Exception ex)
            { ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());
            ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, premiummember, null, trace, false);
            throw;
            } 

        }
        public void SaveMember(PremiumMember member)
        {
            const string functionName = "SaveMember";
            string trace = "";
            try
            {
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
                if (member == null)
                    throw (new Exception("Received null premium member"));

                if (member.ServiceInstanceMember.ServiceInstanceMemberID == default(int))
                {

                    Int32 spotlightMemberID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("SpotlightMemberID");
                }
                trace = String.Format(DATA_FORMAT, member.MemberID, member.BrandID);
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand=Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(member.BrandID);
                int regionid=member.ServiceInstanceMember.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID);
                int distance=member.ServiceInstanceMember.Attributes.Get<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance);
                populateLocation(brand, regionid, distance, member.ServiceInstanceMember);

                saveMember(member);

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, member, null, trace, true);
            }
         }


        public void SaveMember(int brandid, int memberid, DateTime expirationDate, bool enableFlag)
        {
            const string functionName = "SaveMember";
            PremiumMember premiumMember = null;
            InstanceMember instanceMember = new InstanceMember();
            string trace = String.Format(DATA_FORMAT, memberid, brandid);
            
            ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                trace += ",Expiration date " + expirationDate.ToLongDateString();
                
                int siteid = brand.Site.SiteID;
                int servicesiteid = PremiumServices.ValueObjects.ServiceSite.GetServiceSiteID(siteid, (int)PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember);
                int serviceinstanceid = PremiumServices.ValueObjects.ServiceInstance.GetServiceSiteInstanceID(servicesiteid, 1);
               

                premiumMember = getCachedMember(brandid, memberid, serviceinstanceid);
                if (premiumMember == null)
                {
                    premiumMember = getMember(brand.BrandID, memberid, serviceinstanceid);

                    if (premiumMember == null)
                    {
                        premiumMember = getFromSearchPreferences(brand, memberid, serviceinstanceid);

                        if(premiumMember == null || ! SpotlightCommon.IsValidPremiumMember(premiumMember))
                            premiumMember = getDefault(brand, memberid, serviceinstanceid);

                        if (premiumMember != null && SpotlightCommon.IsValidPremiumMember(premiumMember))
                        {
                            
                            int spotlightMemberID = Configuration.ServiceAdapters.KeySA.Instance.GetKey("SpotlightMemberID");
                            premiumMember.ServiceInstanceMember.ServiceInstanceMemberID = spotlightMemberID;
                            premiumMember.ServiceInstanceMember.Attributes.Add<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.StartDate, DateTime.Now);
                            premiumMember.ServiceInstanceMember.Attributes.Add<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.PromotionalFlag, false);
                        }
                    
                    }
                }
                
                if (premiumMember != null)
                {
                  
                    premiumMember.ServiceInstanceMember.Attributes.Add<DateTime>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate, expirationDate);
                    premiumMember.ServiceInstanceMember.Attributes.Add<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag, enableFlag);
                    saveMember(premiumMember);
                    InsertCacheMember(premiumMember, true,true);

                }
                
              
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Exit method");

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex.ToString());
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, premiumMember, null, trace, true);
            }
        }

        #endregion

        public void InsertCacheMember(PremiumMember member, bool replicate, bool synchronize)
        {
            string functionName = "InsertCacheMember";
            string trace = "";
            try
            {
                if (member == null)
                    return;

                trace = String.Format(DATA_FORMAT, member.MemberID, member.BrandID);
                _cache.Insert(member);
                if (replicate)
                {
                    ReplicationRequested(member);
                }
                if (synchronize)
                {
                    SynchronizationRequested(member.GetCacheKey(),
                    member.ReferenceTracker.Purge(Constants.NULL_STRING));
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, member, null, trace, true);
            }
        }

        private void saveMember(PremiumMember member)
        {
            const string functionName = "saveMember";
            string trace = "";
            Command comm = null;
            try
            {
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");

                if (member == null)
                    return;

                if(!SpotlightCommon.IsValidPremiumMember(member))
                    return;

                trace = String.Format(DATA_FORMAT, member.MemberID, member.BrandID);
                //Int32 spotlightmemberID = KeySA.Instance.GetKey("SpotlightMemberID");
                comm = new Command();
                comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                comm.StoredProcedureName = PREMIUMSTORE_SP_SAVE_MEMBER;

                comm.AddParameter("@SpotlightMemberID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.ServiceInstanceMemberID);
                comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
                comm.AddParameter("@brandid", SqlDbType.Int, ParameterDirection.Input, member.BrandID);
                comm.AddParameter("@ServiceInstanceID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.ServiceInstanceID);
                comm.AddParameter("@PromotionalMemberFlag", SqlDbType.Bit, ParameterDirection.Input, member.ServiceInstanceMember.Get<bool>(PremiumServices.ValueObjects.ServiceConstants.Attributes.PromotionalFlag));
                comm.AddParameter("@AgeMin", SqlDbType.SmallInt, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, true));
                comm.AddParameter("@AgeMax", SqlDbType.SmallInt, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, false));
                comm.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask));
                comm.AddParameter("@BoxXMin", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMin));
                comm.AddParameter("@BoxXMax", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMax));
                comm.AddParameter("@BoxYMin", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMin));
                comm.AddParameter("@BoxYMax", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMax));
                comm.AddParameter("@Distance", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance));
                comm.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID));
                comm.AddParameter("@Depth1RegionID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID));
                comm.AddParameter("@Depth2RegionID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID));
                comm.AddParameter("@Depth3RegionID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID));
                comm.AddParameter("@Depth4RegionID", SqlDbType.Int, ParameterDirection.Input, member.ServiceInstanceMember.Get<int>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth4RegionID));
                comm.AddParameter("@Longitude", SqlDbType.Decimal, ParameterDirection.Input, member.ServiceInstanceMember.Get<double>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Longitude));
                comm.AddParameter("@Latitude", SqlDbType.Decimal, ParameterDirection.Input, member.ServiceInstanceMember.Get<double>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Latitude));
                comm.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, member.ServiceInstanceMember.Get<DateTime>(PremiumServices.ValueObjects.ServiceConstants.Attributes.StartDate));
                DateTime expirationDate = member.ServiceInstanceMember.Get<DateTime>(PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate);
                if (expirationDate == DateTime.MinValue)
                {
                    comm.AddParameter("@ExpirarionDate", SqlDbType.DateTime, ParameterDirection.Input, SqlDateTime.MinValue);
                }
                else
                {
                    comm.AddParameter("@ExpirarionDate", SqlDbType.DateTime, ParameterDirection.Input, expirationDate);
                }
                comm.AddParameter("@EnableFlag", SqlDbType.Bit, ParameterDirection.Input, member.ServiceInstanceMember.Get<bool>(PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag));
                //comm.AddParameter("@Description", SqlDbType.VarChar, ParameterDirection.Input, member.ServiceInstanceMember.Get<string>(PremiumServices.ValueObjects.ServiceConstants.Attributes.Description));


                Client.Instance.ExecuteAsyncWrite(comm);

                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Exit method");
                
                SpotlightMemberUpdated();
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, member);

                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, member, comm, trace, true);
            }
        }
        
        private PremiumMember getMember(int brandid, int memberid, int serviceinstanceid)
        {
            const string functionName = "getMember";
            PremiumMember member = null ;
            Command comm = null;
            string trace = String.Format(DATA_FORMAT, memberid, brandid);
            try
            {
                ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Enter method");
                 comm = new Command();
                comm.LogicalDatabaseName = PREMIUMSTORE_LDB;
                comm.StoredProcedureName = PREMIUMSTORE_SP_GET_MEMBER;

                comm.AddParameter("@brandid", SqlDbType.Int, ParameterDirection.Input, brandid);
                comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, memberid);
                comm.AddParameter("@serviceinstanceid", SqlDbType.Int, ParameterDirection.Input, serviceinstanceid);
                using (SqlDataReader rs = Client.Instance.ExecuteReader(comm))
                {
                    if (rs.Read())
                    { member = SpotlightCommon.populateMember(rs); }
                }
                return member;

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex,member);
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, member, comm, trace, false);
                throw;
            }

        }

        private PremiumMember getCachedMember(int brandid, int memberid, int serviceinstanceid)
        {
            const string functionName = "getCachedMember";
            PremiumMember m = null;
            try
            {
                string key = PremiumMember.GetCacheKeyString(brandid, memberid, serviceinstanceid);
                return (PremiumMember)_cache.Get(key);


            }
            catch (Exception ex)
            {
                throw new Exception("Error getting cached member.", ex);
            }

        }

        private PremiumMember getFromSearchPreferences(Content.ValueObjects.BrandConfig.Brand brand, int memberid, int serviceinstanceid)
        {
            PremiumMember premiumMember = null;
            InstanceMember instanceMember = null;
            try
            {
                    SearchPreferenceCollection preferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberid, brand.Site.Community.CommunityID);

                    if(preferences==null)
                        return null;
                    
                    instanceMember = new InstanceMember();
                    instanceMember.ServiceInstanceID=serviceinstanceid;
                    instanceMember.Attributes = new Matchnet.PremiumServices.ValueObjects.AttributeCollection();
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, Conversion.CInt(preferences["MinAge"]), Conversion.CInt(preferences["MaxAge"]));
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.MemberID, memberid);
                    instanceMember.Attributes.Add<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag, true);
                    int genderMask=Conversion.CInt(preferences["GenderMask"]);
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask, genderMask);
                    


                    int distance = Conversion.CInt(preferences["Distance"]);
                    int regionID = Conversion.CInt(preferences["RegionID"]);
                   
                    populateLocation(brand, regionID, distance, instanceMember);
                    premiumMember=new PremiumMember(brand.BrandID,memberid,instanceMember);
                    return premiumMember;                    
                }

            catch (Exception ex)
            { throw; }

        }

        private PremiumMember getDefault(Content.ValueObjects.BrandConfig.Brand brand, int memberid, int serviceinstanceid)
        {
            PremiumMember premiumMember = null;
            InstanceMember instanceMember = null;
            try
            {
                Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                if (member == null)
                    return null;
                int siteid = brand.Site.SiteID;
                instanceMember = new InstanceMember();
               
               
                instanceMember.ServiceInstanceID = serviceinstanceid;
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, brand.DefaultAgeMin,brand.DefaultAgeMax);
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.MemberID, memberid);
                instanceMember.Attributes.Add<bool>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag, true);

                int genderMask = member.GetAttributeInt(brand, "GenderMask");
                //add flip mask code
              
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask, SpotlightCommon.ReverseGenderMask(genderMask));
                    
              //  instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask, seekinggender);

                int distance = brand.DefaultSearchRadius;
                int regionID = member.GetAttributeInt(brand,"RegionID");
                if (regionID <= 0)
                {
                    regionID = brand.Site.DefaultRegionID;
                }
                populateLocation(brand, regionID, distance, instanceMember);
                premiumMember = new PremiumMember(siteid, memberid, instanceMember);
                return premiumMember;
            }

            catch (Exception ex)
            {
                throw new Exception("Error getting default.", ex);
            }

        }

        private void populateLocation(Content.ValueObjects.BrandConfig.Brand brand, int regionID, int distance, InstanceMember instanceMember)
        {
            Matchnet.Content.ValueObjects.Region.RegionLanguage region = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);

            //if (region == null)
            //{
            //    int regionid = brand.Site.DefaultRegionID;
            //    region = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(regionID, brand.Site.LanguageID);
            //}

                if (region != null && region.PostalCodeRegionID > 0)
                {
                    region = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(region.CityRegionID, brand.Site.LanguageID);

                }
           
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID, region.CityRegionID);

                Geo.Coordinates coordinates = new Coordinates((double)region.Latitude, (double)region.Longitude);
                if ((double)region.Latitude == 0.0 || (double)region.Longitude == 0.0)
                {
                    //we are not searching by coordinates
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMin, -99999);
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMax, 99999);

                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMin, -99999);
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMax, 99999);
                    distance = 0;
                }
                else
                {

                    GeoBox box = new GeoBox(coordinates, 0.1, distance, false);

                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMin, box.SearchBox.Area.BoxXMin);
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMax, box.SearchBox.Area.BoxXMax);

                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMin, box.SearchBox.Area.BoxYMin);
                    instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMax, box.SearchBox.Area.BoxYMax);
                }
                instanceMember.Attributes.Add<double>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Latitude, coordinates.Latitude);
                instanceMember.Attributes.Add<double>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Longitude, coordinates.Longitude);

                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID, region.CountryRegionID);
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID, region.CityRegionID);
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID, region.StateRegionID);
                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID, regionID);


                instanceMember.Attributes.Add<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance, distance);
            }

    }
}
