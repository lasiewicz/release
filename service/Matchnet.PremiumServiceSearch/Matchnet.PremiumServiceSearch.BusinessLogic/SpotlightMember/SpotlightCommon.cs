﻿using System;
using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.SharedLib.Utils;
using Matchnet.SharedLib.Constants;

namespace Matchnet.PremiumServiceSearch.BusinessLogic.SpotlightMember
{
    public class SpotlightCommon
    {
        const string MODULE_NAME = "Spot";
        public static PremiumMember populateMember(SqlDataReader rs)
        {
            const string functionName = "populateMember";
            PremiumMember member = new PremiumMember();
            member.ServiceInstanceMember = new InstanceMember();
            try
            {
                if (member != null && member.ServiceInstanceMember != null)
                {
                  
                    member.BrandID = Conversion.CInt(rs["brandid"]);
                    member.MemberID = Conversion.CInt(rs["memberid"]);
                    member.ServiceInstanceMember.ServiceInstanceMemberID = Conversion.CInt(rs["SpotlightMemberID"]);
                    member.ServiceInstanceMember.ServiceInstanceID = Conversion.CInt(rs["ServiceInstanceID"]);
                    member.ServiceInstanceMember.Attributes = new PremiumServices.ValueObjects.AttributeCollection();
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.PromotionalFlag, Conversion.CBool(rs["PromotionalMemberFlag"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange, Conversion.CInt(rs["AgeMin"]), Conversion.CInt(rs["AgeMax"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask, Conversion.CInt(rs["GenderMask"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMin, Conversion.CInt(rs["BoxXMin"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxXMax, Conversion.CInt(rs["BoxXMax"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMin, Conversion.CInt(rs["BoxYMin"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.BoxYMax, Conversion.CInt(rs["BoxYMax"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Distance, Conversion.CInt(rs["Distance"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.RegionID, Conversion.CInt(rs["RegionID"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth1RegionID, Conversion.CInt(rs["Depth1RegionID"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth2RegionID, Conversion.CInt(rs["Depth2RegionID"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth3RegionID, Conversion.CInt(rs["Depth3RegionID"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Depth4RegionID, Conversion.CInt(rs["Depth4RegionID"]));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Longitude, Conversion.CDouble(rs["Longitude"].ToString()));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Latitude, Conversion.CDouble(rs["Latitude"].ToString()));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.Longitude, Conversion.CDouble(rs["Longitude"].ToString()));
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.StartDate, Conversion.CDateTime(rs["StartDate"]));
                    if (Conversion.CDateTime(rs["ExpirationDate"]) == SqlDateTime.MinValue)
                    {
                        member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate, DateTime.MinValue);
                    }
                    else
                    {
                        member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate, Conversion.CDateTime(rs["ExpirationDate"]));
                    }
                    member.ServiceInstanceMember.Attributes.Add((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.EnableFlag, Conversion.CBool(rs["EnableFlag"]));
                    
                }

                return member;
            }
            catch (Exception ex)
            { ServiceTrace.Instance.Trace(MODULE_NAME + functionName, ex, member);
            throw;
            }


        }

        public static int ReverseGenderMask(int genderMask)
        {   return GenderUtils.GetGenderMaskSeeking(genderMask);

        }

        public static bool IsValidPremiumMember(PremiumMember member)
        {
            string functionName = "IsValidPremiumMember";
            bool invalid = false;
            try
            {
                if(member == null || member.ServiceInstanceMember == null)
                    return invalid;

                if(member.MemberID < 0)
                    return invalid;

                if (member.ServiceInstanceMember.Get<int>(Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.GenderMask) < 0)
                {
                    return invalid;
                }


                int agemin =member.ServiceInstanceMember.Get<int>(Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange,true);
                int agemax =member.ServiceInstanceMember.Get<int>(Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.AgeRange,false);

                                
                if(agemax < 0 || agemin < 0 || agemax < agemin)
                {       return invalid;
                 }
                
                
                return true;
            }
            catch (Exception ex)
            { 
                ServiceTrace.Instance.LogServiceException(MODULE_NAME , functionName,ex,false);
                return invalid; 
            }


        }


    }
}
