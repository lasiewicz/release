﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
namespace TestClient
{
    public partial class Form1 : Form
    {

        ResultList members = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int brandid = Int32.Parse(txtBrandID.Text);
                int memberid = Int32.Parse(txtMemberID.Text);

                SpotlightMemberSA.Instance.TestURI = txtURI.Text;
                ResultList results = SpotlightMemberSA.Instance.Search(brandid, memberid, false);
                Query q = SpotlightMemberSA.Instance.GetQuery(brandid, memberid, false);
                showQuery(q);
                dataGridView1.Columns.Clear();
                dataGridView1.Rows.Clear();
                DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
                dataGridView1.Columns.Add(newCol);
                for (int i = 0; i < results.Results.Count; i++)
                {

                    DataGridViewRow row = new DataGridViewRow();
                    DataGridViewTextBoxCell newCell = new DataGridViewTextBoxCell();
                   
                    newCell.Value = results.Results[i];

                    row.Cells.Add(newCell);
                    dataGridView1.Rows.Add(row);
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
            try
            {
                int brandid = Int32.Parse(txtBrandID.Text);
                int memberid = Int32.Parse(txtMemberID.Text);
                SpotlightMemberSA.Instance.TestURI = txtURI.Text;
                Query q = SpotlightMemberSA.Instance.GetQuery(brandid, memberid,false);
                showQuery(q);
                members= Matchnet.PremiumServiceSearch.BusinessLogic.SpotlightMember.SpotlightSearchBL.Instance.Search(q);

                dataGridView1.DataSource = members;
              //  members = null;
                
                }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void showQuery(Query q)
        {
            dataGridView3.Columns.Clear();
            dataGridView3.Rows.Clear();
            DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn newCol1 = new DataGridViewTextBoxColumn();
         
            dataGridView3.Columns.Add(newCol);
            dataGridView3.Columns.Add(newCol1);
            

            foreach (IParameter p in q.Parameters)
            {
                DataGridViewRow row = new DataGridViewRow();
                DataGridViewTextBoxCell newCell = new DataGridViewTextBoxCell();
                newCell.Value = p.ID.ToString();

                DataGridViewTextBoxCell newCell1 = new DataGridViewTextBoxCell();
                newCell1.Value = q.Parameters[p.ID].ToString();

                row.Cells.Add(newCell);
               
                row.Cells.Add(newCell1);
                dataGridView3.Rows.Add(row);

            }
           
        }

        private void showAttributes(InstanceMember m)
        {
            
          

           
            DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn newCol1 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn newCol2 = new DataGridViewTextBoxColumn();
            dataGridView2.Columns.Add(newCol);
            dataGridView2.Columns.Add(newCol1);
            dataGridView2.Columns.Add(newCol2);
           
            // Then add to the columns collection:


            DataGridViewRow rowHeader = new DataGridViewRow();
            DataGridViewTextBoxCell newCellHeader = new DataGridViewTextBoxCell();
            newCellHeader.Value = m.ServiceInstanceID;
            rowHeader.Cells.Add(newCellHeader); dataGridView2.Rows.Add(rowHeader);

             foreach(Matchnet.PremiumServices.ValueObjects.IAttribute a in m.Attributes)
             {
                 DataGridViewRow row = new DataGridViewRow();
                 DataGridViewTextBoxCell newCell = new DataGridViewTextBoxCell();
                 newCell.Value = a.ID.ToString();

                 DataGridViewTextBoxCell attrCell = new DataGridViewTextBoxCell();
                 attrCell.Value = (Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes)a.ID;

                 DataGridViewTextBoxCell newCell1 = new DataGridViewTextBoxCell();
                 newCell1.Value = m.Attributes[a.ID].ToString();
             
                 row.Cells.Add(newCell);
                 row.Cells.Add(attrCell);
                 row.Cells.Add(newCell1);
                 dataGridView2.Rows.Add(row);
                 
             }

           
        }

        private void button2_Click(object sender, EventArgs e)
        {
           // PremiumMember member = null; 
            try
            {
                int brandid = Int32.Parse(txtBrandID.Text);
                int memberid = Int32.Parse(txtMemberID.Text);
               
                InstanceMemberCollection m = Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.GetMemberServices(brandid, memberid);
                dataGridView1.DataSource = m;
                dataGridView2.Rows.Clear();
                foreach (InstanceMember member in m)
                {
                    showAttributes(member);
                }
          }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtURI.Text = "tcp://bhd-skener:49500/SpotlightMemberSM.rem";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ThreadedTest t = new ThreadedTest();
            t.Show();
        }

        private void questionAnswerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuestionAnswer form = new QuestionAnswer();

            form.Show();
        }
    }
}
