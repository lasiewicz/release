﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using System.Threading;
using System.Diagnostics;

namespace TestClient
{
    public partial class QuestionAnswer : Form
    {
        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetTextCallback(string text, TextBox textbox, bool clearFirst, bool newline);

        public QuestionAnswer()
        {
            InitializeComponent();
        }

        private void txtButton1_Click(object sender, EventArgs e)
        {
            try
            {
                AppendText(String.Format("Starting GetMemberQuestions({0}, {1})", Convert.ToInt32(txtMemberID.Text), Convert.ToInt32(txtSiteID.Text)), textBox1, false, true);
                MemberQuestionList questions = QuestionAnswerSA.Instance.GetMemberQuestions(Convert.ToInt32(txtMemberID.Text), Convert.ToInt32(txtSiteID.Text));
                
                if (questions != null)
                {
                    AppendText(String.Format("- Questions: {0}", questions.Questions.Count), textBox1, false, false);

                }
                else
                {
                    AppendText("- null", textBox1, false, false);
                }

                AppendText("Finished GetMemberQuestions()", textBox1, false, true);
            }
            catch (Exception ex)
            {
                AppendText("Error: " + ex.Message, textBox1, false, true);
            }
        }

        private void AppendText(string text, TextBox textbox, bool clearFirst, bool newline)
        {
            try
            {
                // InvokeRequired required compares the thread ID of the
                // calling thread to the thread ID of the creating thread.
                // If these threads are different, it returns true.
                if (textbox.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(AppendText);
                    this.Invoke(d, new object[] { text, textbox, clearFirst, newline });
                }
                else
                {
                    if (clearFirst)
                        textbox.Clear();

                    if (newline)
                    {
                        textbox.AppendText(System.Environment.NewLine + text);
                    }
                    else
                    {
                        textbox.AppendText(text);
                    }
                }
            }
            catch (Exception ex)
            {
                textbox.AppendText(string.Format("Error in AppendText({0}, {1}, {2}, {3}): {4}", text, textbox, clearFirst, newline, ex.Message));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int threads = Convert.ToInt32(txtThreads.Text);
                for (int i = 0; i < threads; i++)
                {
                    string[] questionIDList = txtQuestionID.Text.Split(new char[] { ',' });
                    foreach (string s in questionIDList)
                    {
                        Thread thread = new Thread(new ParameterizedThreadStart(GetQuestion));
                        thread.Start(Convert.ToInt32(s));
                    }
                }
            }
            catch (Exception ex)
            {
                AppendText("Error: " + ex.Message, textBox1, false, true);
            }
        }

        private void GetQuestion(object questionID)
        {
            Stopwatch stopWatch = new Stopwatch();
            try
            {
                stopWatch.Start();
                AppendText(String.Format("Starting GetQuestions({0})", Convert.ToInt32(questionID.ToString())), textBox1, false, true);
                Question q = QuestionAnswerSA.Instance.GetQuestion(Convert.ToInt32(questionID.ToString()));

                if (q != null)
                {
                    stopWatch.Stop();
                    AppendText(String.Format("- QuestionID: {0}, Answer count: {1}, Time Elapsed: {2}", questionID.ToString(), q.Answers.Count, stopWatch.ElapsedMilliseconds), textBox1, false, false);
                }
                else
                {
                    AppendText("- null", textBox1, false, false);
                }

                AppendText("Finished GetQuestion()", textBox1, false, true);
            }
            catch (Exception ex)
            {
                AppendText("Error: " + ex.Message, textBox1, false, true);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int threads = Convert.ToInt32(txtThreads.Text);
                for (int i = 0; i < threads; i++)
                {
                    Thread thread = new Thread(new ThreadStart(GetRecentAnswerList));
                    thread.Start();
                }
            }
            catch (Exception ex)
            {
                AppendText("Error: " + ex.Message, textBox1, false, true);
            }
        }

        private void GetRecentAnswerList()
        {
            Stopwatch stopWatch = new Stopwatch();
            try
            {
                stopWatch.Start();
                AppendText(String.Format("Starting GetRecentAnswerList({0}, 10)", Convert.ToInt32(txtSiteID.Text)), textBox1, false, true);
                List<Answer> list = QuestionAnswerSA.Instance.GetRecentAnswerList(Convert.ToInt32(txtSiteID.Text), 10);

                if (list != null)
                {
                    stopWatch.Stop();
                    AppendText(String.Format("- Answer count: {0}, Time Elapsed: {1}", list.Count, stopWatch.ElapsedMilliseconds), textBox1, false, false);
                }
                else
                {
                    AppendText("- null", textBox1, false, false);
                }

                AppendText("Finished GetRecentAnswerList()", textBox1, false, true);
            }
            catch (Exception ex)
            {
                AppendText("Error: " + ex.Message, textBox1, false, true);
            }
        }
    }
}
