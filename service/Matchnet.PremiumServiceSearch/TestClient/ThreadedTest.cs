using System;
using System.Drawing;
using System.Collections;
using System.Threading;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace TestClient
{
	/// <summary>
	/// Summary description for ThreadedTest.
	/// </summary>
	public class ThreadedTest : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox txtReqFile;
		private System.Windows.Forms.Button btnReqFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtLogFile;
		private System.Windows.Forms.Button btnLogFile;
		private System.Windows.Forms.Button btnRun;
		private System.Windows.Forms.TextBox txtThreadsNum;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox chkUseSA;
		private System.Windows.Forms.ComboBox cboRequestType;
		private System.Windows.Forms.TextBox txtURI;
		private System.Windows.Forms.TextBox txtIter;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnGetURLFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button button1;
        private Label label6;
        private TextBox txtBrandID;
        private CheckBox chkPromo;
 public ArrayList urls = new ArrayList();
		public ThreadedTest()
		{
			
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtReqFile = new System.Windows.Forms.TextBox();
            this.btnReqFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLogFile = new System.Windows.Forms.TextBox();
            this.btnLogFile = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.txtThreadsNum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtURI = new System.Windows.Forms.TextBox();
            this.chkUseSA = new System.Windows.Forms.CheckBox();
            this.cboRequestType = new System.Windows.Forms.ComboBox();
            this.txtIter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnGetURLFile = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBrandID = new System.Windows.Forms.TextBox();
            this.chkPromo = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtReqFile
            // 
            this.txtReqFile.Location = new System.Drawing.Point(120, 112);
            this.txtReqFile.Name = "txtReqFile";
            this.txtReqFile.Size = new System.Drawing.Size(616, 20);
            this.txtReqFile.TabIndex = 0;
            // 
            // btnReqFile
            // 
            this.btnReqFile.Location = new System.Drawing.Point(776, 112);
            this.btnReqFile.Name = "btnReqFile";
            this.btnReqFile.Size = new System.Drawing.Size(80, 24);
            this.btnReqFile.TabIndex = 1;
            this.btnReqFile.Text = "Get Req. File";
            this.btnReqFile.Click += new System.EventHandler(this.btnReqFile_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Req. File";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Log. File";
            // 
            // txtLogFile
            // 
            this.txtLogFile.Location = new System.Drawing.Point(120, 144);
            this.txtLogFile.Name = "txtLogFile";
            this.txtLogFile.Size = new System.Drawing.Size(616, 20);
            this.txtLogFile.TabIndex = 3;
            // 
            // btnLogFile
            // 
            this.btnLogFile.Location = new System.Drawing.Point(776, 144);
            this.btnLogFile.Name = "btnLogFile";
            this.btnLogFile.Size = new System.Drawing.Size(80, 24);
            this.btnLogFile.TabIndex = 5;
            this.btnLogFile.Text = "Log. File";
            this.btnLogFile.Click += new System.EventHandler(this.btnLogFile_Click);
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(264, 192);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(72, 40);
            this.btnRun.TabIndex = 6;
            this.btnRun.Text = "Run";
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // txtThreadsNum
            // 
            this.txtThreadsNum.Location = new System.Drawing.Point(120, 200);
            this.txtThreadsNum.Name = "txtThreadsNum";
            this.txtThreadsNum.Size = new System.Drawing.Size(108, 20);
            this.txtThreadsNum.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Threads Num";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "URI";
            // 
            // txtURI
            // 
            this.txtURI.Location = new System.Drawing.Point(120, 24);
            this.txtURI.Name = "txtURI";
            this.txtURI.Size = new System.Drawing.Size(616, 20);
            this.txtURI.TabIndex = 9;
            // 
            // chkUseSA
            // 
            this.chkUseSA.Location = new System.Drawing.Point(784, 16);
            this.chkUseSA.Name = "chkUseSA";
            this.chkUseSA.Size = new System.Drawing.Size(120, 32);
            this.chkUseSA.TabIndex = 12;
            this.chkUseSA.Text = "use SA";
            // 
            // cboRequestType
            // 
            this.cboRequestType.Items.AddRange(new object[] {
            "Search",
            "MemberUpdate",
            "PhotoUpdate",
            "PhotoUpdate[]"});
            this.cboRequestType.Location = new System.Drawing.Point(738, 64);
            this.cboRequestType.Name = "cboRequestType";
            this.cboRequestType.Size = new System.Drawing.Size(312, 21);
            this.cboRequestType.TabIndex = 13;
            // 
            // txtIter
            // 
            this.txtIter.Location = new System.Drawing.Point(120, 232);
            this.txtIter.Name = "txtIter";
            this.txtIter.Size = new System.Drawing.Size(108, 20);
            this.txtIter.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Iterations";
            // 
            // btnGetURLFile
            // 
            this.btnGetURLFile.Location = new System.Drawing.Point(880, 112);
            this.btnGetURLFile.Name = "btnGetURLFile";
            this.btnGetURLFile.Size = new System.Drawing.Size(136, 24);
            this.btnGetURLFile.TabIndex = 16;
            this.btnGetURLFile.Text = "Get URL  Req. File";
            this.btnGetURLFile.Click += new System.EventHandler(this.btnGetURLFile_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(392, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 40);
            this.button1.TabIndex = 17;
            this.button1.Text = "Run HTTP test";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 19;
            this.label6.Text = "BrandID";
            // 
            // txtBrandID
            // 
            this.txtBrandID.Location = new System.Drawing.Point(120, 65);
            this.txtBrandID.Name = "txtBrandID";
            this.txtBrandID.Size = new System.Drawing.Size(132, 20);
            this.txtBrandID.TabIndex = 18;
            // 
            // chkPromo
            // 
            this.chkPromo.AutoSize = true;
            this.chkPromo.Location = new System.Drawing.Point(295, 65);
            this.chkPromo.Name = "chkPromo";
            this.chkPromo.Size = new System.Drawing.Size(78, 17);
            this.chkPromo.TabIndex = 20;
            this.chkPromo.Text = "Promotions";
            this.chkPromo.UseVisualStyleBackColor = true;
            // 
            // ThreadedTest
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1072, 677);
            this.Controls.Add(this.chkPromo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBrandID);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnGetURLFile);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtIter);
            this.Controls.Add(this.cboRequestType);
            this.Controls.Add(this.chkUseSA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtURI);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtThreadsNum);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.btnLogFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLogFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnReqFile);
            this.Controls.Add(this.txtReqFile);
            this.Name = "ThreadedTest";
            this.Text = "ThreadedTest";
            this.Load += new System.EventHandler(this.ThreadedTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void btnRun_Click(object sender, System.EventArgs e)
		{
			try
			{
				int threadNum = Int32.Parse(txtThreadsNum.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
				for (int i=0;i< threadNum;i++)
				{
					string logfile = txtLogFile.Text + "_thread" + i.ToString() + ".log";

					ArrayList requests=GetRequests(txtReqFile.Text);
					string uri="";
					if( !chkUseSA.Checked)
						uri=txtURI.Text;

					RequestThread test = new RequestThread(requests, brandid,chkPromo.Checked, uri,int.Parse(txtIter.Text),logfile, false );
					ThreadStart threadDelegate = new ThreadStart(test.workCycleSearch);
					Thread t = new Thread(threadDelegate);
					t.Start();
				}
			}
			catch(Exception ex)
			{MessageBox.Show(ex.Source + "," + ex.Message);}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			try
			{
				int threadNum = Int32.Parse(txtThreadsNum.Text);
				for (int i=0;i< threadNum;i++)
				{
					string logfile = txtLogFile.Text + "_thread" + i.ToString() + ".log";
					

					//RequestThread test = new RequestThread(urls,int.Parse(txtIter.Text),logfile);
					//ThreadStart threadDelegate = new ThreadStart(test.workCycleHTTPSearch);
					//Thread t = new Thread(threadDelegate);
					//t.Start();
				}
			}
			catch(Exception ex)
			{MessageBox.Show(ex.Source + "," + ex.Message);}
		}

		private ArrayList GetRequests(string file)
		{
            ArrayList req = new ArrayList();
            using (TextReader reader = new StreamReader(file))
            {
                
                string res = reader.ReadLine();
                while (res != null)
                {
                    if (res != null && res != "")
                    { req.Add(res); }
                    res = reader.ReadLine();
                }
            }
            return req;
		}

		private System.Type RequestObjectType(TestRequestType reqtype)
		{
			System.Type type=null;
			switch (reqtype)
			{
                //case TestRequestType.search:
                //{
                //    SearchRequest s=new SearchRequest();
                //    type=s.GetType();
                //    break;
                //}
                //case TestRequestType.memberupdate:
                //{
					
                //    MemberUpdateRequest s=new MemberUpdateRequest();
                //    type=s.GetType();
                //    break;
                //}
                //case TestRequestType.photoupdate:
                //{
                //    PhotoUpdateRequest s=new PhotoUpdateRequest();
                //    type=s.GetType();
                //    break;
                //}

				default:
				{break;}
			}
				return type;

			
		}

		private void btnReqFile_Click(object sender, System.EventArgs e)
		{
			openFileDialog1 = new OpenFileDialog();
			openFileDialog1.ShowDialog();
			txtReqFile.Text = openFileDialog1.FileName;
			
		}

		private void ThreadedTest_Load(object sender, System.EventArgs e)
		{
			txtURI.Text="tcp://devapp01:49500/SpotlightMemberSM.rem";
		}

		private void btnGetURLFile_Click(object sender, System.EventArgs e)
		{
			openFileDialog1 = new OpenFileDialog();
			openFileDialog1.ShowDialog();
			txtReqFile.Text = openFileDialog1.FileName;
			
			using(TextReader reader=new StreamReader(txtReqFile.Text))
			{
				urls.Clear();
				string res=reader.ReadLine();
				while(res !=null)
				{
					if (res!=null && res != "")
					{ urls.Add(res); }
					res = reader.ReadLine();
				}
			}
		}

        private void btnLogFile_Click(object sender, EventArgs e)
        {

        }

		

//
//		private void StartSearchRequestThread()
//		
//			ThreadStart threadDelegate = new ThreadStart(test.workCycleSearch);
//			Thread t = new Thread(threadDelegate);
//			t.Start();
		
	}
}
