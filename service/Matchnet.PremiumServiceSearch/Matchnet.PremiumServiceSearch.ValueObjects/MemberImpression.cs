﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.PremiumServiceSearch.ValueObjects
{
    [Serializable]
    public class MemberImpression : IComparable, IValueObject
    {
        int memberID;
        DateTime impressionTime;

        public MemberImpression(int memberid, DateTime impressiontime)
        {
            memberID = memberid;
            impressionTime = impressiontime;

        }

        public MemberImpression(int memberid)
        {
            memberID = memberid;
            impressionTime = DateTime.Now;

        }

        public int MemberID
        {
            get { return memberID; }
            set { memberID = value; }

        }

        public DateTime ImpressionTime
        {
            get { return impressionTime; }
            set { impressionTime = value; }

        }

        public int CompareTo(object obj)
        {
            if (obj is MemberImpression)
            {
                MemberImpression m2 = (MemberImpression)obj;
                return impressionTime.CompareTo(m2.ImpressionTime) * -1;
            }
            else
                throw new ArgumentException("Object is not a MemberImpression.");
        }



    }
}
