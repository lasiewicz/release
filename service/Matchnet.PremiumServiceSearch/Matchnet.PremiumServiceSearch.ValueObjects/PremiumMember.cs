﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Tracking;
using Matchnet.PremiumServices.ValueObjects;
namespace Matchnet.PremiumServiceSearch.ValueObjects
{
    [Serializable]
    public class PremiumMember : IValueObject, ICacheable,IReplicable
    {
  
        int memberID;
        int brandID;
       
        InstanceMember instanceMember;


        public PremiumMember()
        {}

        public PremiumMember(int brandid, int memberid)
        {
            memberID = memberid;
            brandID = brandid;
          
            instanceMember = new InstanceMember();
        }

        public PremiumMember(int brandid, int memberid, InstanceMember member)
        {
            memberID = memberid;
            brandID = brandid;
          
         
            instanceMember = member;
        }

        public int MemberID
        {
            get { return memberID; }
            set { memberID = value; }

        }

     
     

        public int BrandID
        {
            get { return brandID; }
            set { brandID = value; }

        }


        public InstanceMember ServiceInstanceMember
        { get { return instanceMember; } set { instanceMember = value; } }




        #region IReplicable and ReferenceTracking Members
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 180;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "PremiumMember_{0}_{1}_{2}";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }


        public string GetCacheKey()
        {
            int instid=0;
            if (instanceMember != null)
            { instid = instanceMember.ServiceInstanceID; }
            
            return GetCacheKeyString( brandID,memberID,instid);
        }

        public static string GetCacheKeyString(int brandid,int memberid, int instid)
        {
            return String.Format(CACHEKEYFORMAT, brandid, memberid, instid);
        }


        #endregion

        public override string ToString()
        {
            string parm = "";
            string member = "";
            try
            {

                if (instanceMember != null)
                    member = instanceMember.ToString();

                parm = string.Format("BrandID={0}, MemberID={1}\r\n ServiceInstanceMember:{2}", brandID,memberID,member);
                return parm;

            }
            catch (Exception ex)
            { return ""; }

        }
    }


    [Serializable]
    public class PremiumMemberCollection : KeyedCollection<int,PremiumMember>, IValueObject, ICacheable
    {
        protected override int GetKeyForItem(PremiumMember item)
        {
            return item.ServiceInstanceMember.ServiceInstanceMemberID;
        }


        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "PremiumMemberCollection_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public void setCacheKey(string key)
        {
            cachekey = String.Format(CACHEKEYFORMAT, key);
        }

       
        public string GetCacheKey()
        {
            return cachekey;
        }

        public static string GetCacheKeyString(string key)
        {
            return String.Format(CACHEKEYFORMAT, key);
        }

      
        #endregion

    }
}
