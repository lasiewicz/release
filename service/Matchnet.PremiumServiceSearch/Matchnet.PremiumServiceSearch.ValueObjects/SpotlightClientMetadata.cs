﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.PremiumServiceSearch.ValueObjects
{
    [Serializable]
    public class SpotlightClientMetadata : IValueObject, ICacheable
    {
        public int MemberID { get; set; }
        public int BrandID { get; set; }
        public int LastSpotlightMember { get; set; }
        public bool SelfSpotlightedCheck { get; set; }
        public List<MemberImpression> SpotlightImpressions { get; set; }
        public List<MemberImpression> PromoImpressions { get; set; }

        #region ICacheable Members

        private int _cacheTTLSeconds = 60 * 60;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "SpotlightClientMetadata_{0}_{1}";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }


        public string GetCacheKey()
        {
            return GetCacheKeyString(BrandID, MemberID);
        }

        public static string GetCacheKeyString(int brandid, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, brandid, memberid);
        }


        #endregion

    }
}
