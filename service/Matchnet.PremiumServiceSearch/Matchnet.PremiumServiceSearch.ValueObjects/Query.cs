﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;


namespace Matchnet.PremiumServiceSearch.ValueObjects
{
    public enum PremiumSearchType
    {
        SpotlightMember

    }
    [Serializable]
    public class Query :IValueObject
   {
        ParameterCollection parameters;
        const string QUERY_FORMAT = "SearchType:{0}\r\nParameters:{1}";
        PremiumSearchType type;

        #region get cache key for search results

        public Query(PremiumSearchType t)
        {
            type = t;
            
        }

        
        public PremiumSearchType SearchType
        {
            get { return type; }
            set { type = value; }
        }
        public ParameterCollection Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public  string GetCacheKeyString(string keyprefix, char separator)
        {
            string key = keyprefix;

            foreach (IParameter p in parameters)
            {
                key += p.ToString() + separator.ToString();
            }
            return key;
        }

        public string GetCacheKeyString(char separator)
        {
            string key = type.ToString();
            foreach (IParameter p in parameters)
            {
                key += p.ToString() + separator.ToString();
            }
            return key;
        }


        #endregion


        public static int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }


        public override string ToString()
        {
            string parm = "";
            string query = "";
            try
            {
                if (parameters != null)
                {
                    parm = parameters.ToString();
                }
                query=string.Format(QUERY_FORMAT,type,parm);
                return query;

            }
            catch (Exception ex)
            { return ""; }

        }

    }
}
