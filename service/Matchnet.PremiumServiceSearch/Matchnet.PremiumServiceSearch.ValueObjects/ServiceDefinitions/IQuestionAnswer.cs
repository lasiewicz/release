﻿using System;
using System.Collections.Generic;
using Matchnet.BaseClasses;
using System.Linq;
using System.Text;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions
{
    public interface IQuestionAnswer
    {
        ActiveQuestionIDList GetActiveQuestionIDs(int siteID);
        Question GetQuestion(int questionID);
        MemberQuestionList GetMemberQuestions(int memberID, int siteID);
        void AddAnswer(int memberID, int siteID, int questionID, string answerValue);
        void RemoveAnswer(int answerID, int questionID, int siteID, int memberID);
        void RemoveMemberAnswer(int questionID, int siteID, int memberID);
        void AdminUpdateAnswer(Answer answer, int siteID);
        void UpdateAnswer(Answer answer, int siteID);
        QuestionList GetActiveQuestionList(int siteID);
        List<StockAnswer> GetStockAnswers(List<int> questionIds);
        AnswerList GetRecentAnswerList(int siteID, int daysback);
        PagedResult<Question> GetRecentQuestionListPaged(int siteID, int daysback, int pageNumber, int pageSize);
        PagedResult<QuestionResponse> GetActiveQuestionListPaged(int siteID, int pageNumber, int pageSize, QuestionListSort sort);
        PagedQuestion GetQuestionPaged(int questionID, int pageNumber, int pageSize, bool latestFirst);
    }
}
