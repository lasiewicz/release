﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions
{
    public interface IQuestionAnswerAdapter:IQuestionAnswer
    {
        List<Question> GetActiveQuestionList(int siteID, int pagesize, int startrow, QuestionListSort sort, out int total);
    }
}
