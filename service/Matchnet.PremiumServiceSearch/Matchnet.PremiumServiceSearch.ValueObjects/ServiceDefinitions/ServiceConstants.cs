﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions
{
    public class ServiceConstants
    {
        public enum SpotlightProfileType:int
        {
            home=0,
            yourmatches=1
        }
        public const string SERVICE_CONSTANT = "PREMIUMSERVICESEARCH_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.PremiumServiceSearch.Service";
        public const string SERVICE_NAME_QUESTION = "Matchnet.PremiumServiceSearch.Service.Question";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_SPOTLIGHT_MANAGER_NAME = "SpotlightMemberSM";
        public const string SERVICE_QUESTIONANSWER_MANAGER_NAME = "QuestionAnswerSM";

        public const string SERVICE_SA_HOST_OVERRIDE = "PREMIUMSERVICESEARCHSVC_SA_HOST_OVERRIDE";
    }
}
