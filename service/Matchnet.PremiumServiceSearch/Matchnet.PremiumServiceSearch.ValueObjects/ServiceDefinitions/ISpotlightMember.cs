﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;
namespace Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions
{
    public interface ISpotlightMember
    {
        void SaveMember( PremiumMember member, bool async);
        void SaveMember(int brandid, int memberid, DateTime expirationDate, bool enableFlag, bool async);
        ResultList Search(Query query);
        PremiumMember GetServiceMember(int brandid, int memberid, int serviceinstanceid);
        void LogImpression(int siteid, int pageid, QueueMessage.ImpressionType viewtype, int viewedmemberid, int viewingmemberid, DateTime impressiontime);
        

    }
}
