﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Matchnet;

namespace Matchnet.PremiumServiceSearch.ValueObjects
{
    [Serializable]
    public class ResultList:IValueObject, ICacheable
    {
        private int _cacheTTLSeconds = 180;
        private const string CACHE_KEY_PREFIX = "PremiumServiceSearchResultList";
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;


        string cacheKey;
		
        List<int> results;

        public ResultList(string querycachekey)
		{cacheKey=querycachekey;
        results = new List<int>();
        }


        public  List<int> Results
        {
            set { results = value; }
            get { return results; }
        }

        

        public string CacheKey
        {
            set { cacheKey = value; }
            get { return cacheKey; }
        }

        #region ICacheable Members
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }


        public string GetCacheKey()
        {
            return cacheKey;
        }
        #endregion

    }
}
