﻿using Matchnet.BaseClasses;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    public class PagedQuestionResponseListWrapper : CacheableWrapper<PagedResult<QuestionResponse>>
    {
        private string _key;
        public PagedQuestionResponseListWrapper(PagedResult<QuestionResponse> data, string prefix, int siteID, int pageNumber, int pageSize) 
            : base(data, CacheItemMode.Absolute, CacheItemPriorityLevel.Default, 300)
        {
            _key = GetCacheKey(prefix, siteID, pageNumber, pageSize);
        }

        public override string GetCacheKey()
        {
            return _key;
        }

        public static string GetCacheKey(string prefix, int siteID, int pageNumber, int pageSize)
        {
            return string.Format("{0}_{1}_{2}_{3}", prefix, siteID, pageNumber, pageSize);
        }
    }
}
