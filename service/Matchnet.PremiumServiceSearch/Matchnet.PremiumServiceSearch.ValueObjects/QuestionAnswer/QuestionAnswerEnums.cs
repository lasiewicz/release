﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    [Serializable]
    public class QuestionAnswerEnums
    {
        /// <summary>
        /// Question types: FreeText, multiple choice, scale, etc
        /// </summary>
        public enum QuestionType : int
        {
            None = 0,
            FreeText = 1,
            MultipleChoice = 2
        }

        /// <summary>
        /// Answer types: FreeText, Single Options, Multiple Options (mask value), etc
        /// </summary>
        public enum AnswerType : int
        {
            None = 0,
            FreeText = 1
        }

        public enum AnswerStatusType : int
        {
            None = 0,
            Pending = 1,
            Approved = 2,
            Rejected = 3
        }
    }
}
