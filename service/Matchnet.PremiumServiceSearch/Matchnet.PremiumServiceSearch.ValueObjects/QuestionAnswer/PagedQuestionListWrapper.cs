﻿using Matchnet.BaseClasses;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    public class PagedQuestionListWrapper : CacheableWrapper<PagedResult<Question>>
    {
        private string _key;
        public PagedQuestionListWrapper(PagedResult<Question> data, string prefix, int siteID, int daysback, int pageNumber, int pageSize) 
            : base(data, CacheItemMode.Absolute, CacheItemPriorityLevel.Default, 300)
        {
            _key = GetCacheKey(prefix, siteID, daysback, pageNumber, pageSize);
        }

        public override string GetCacheKey()
        {
            return _key;
        }

        public static string GetCacheKey(string prefix, int siteID, int daysback, int pageNumber, int pageSize)
        {
            return string.Format("{0}_{1}_{2}_{3}_{4}", prefix, siteID, daysback, pageNumber, pageSize);
        }
    }
}
