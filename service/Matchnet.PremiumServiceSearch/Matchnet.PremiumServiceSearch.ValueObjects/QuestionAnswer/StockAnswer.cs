﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    /// <summary>
    /// Stock Answers for Multi-Choice Question
    /// </summary>
    [Serializable]
    public class StockAnswer : IValueObject, ICacheable, IReplicable
    {
        private int _StockAnswerID = Constants.NULL_INT;
        private int _QuestionID = Constants.NULL_INT;
        private int _AdminMemberID = Constants.NULL_INT;
        private int _SortOrder = Constants.NULL_INT;
        private string _AnswerValue = string.Empty;
        private DateTime _InsertDate = DateTime.MinValue;
        private DateTime _UpdateDate = DateTime.MinValue;

        #region Properties
        public int StockAnswerID
        {
            get { return _StockAnswerID; }
            set { _StockAnswerID = value; }
        }

        public int QuestionID
        {
            get { return _QuestionID; }
            set { _QuestionID = value; }
        }

        public int AdminMemberID
        {
            get { return _AdminMemberID; }
            set { _AdminMemberID = value; }
        }

        public int SortOrder
        {
            get { return _SortOrder; }
            set { _SortOrder = value; }
        }

        public string AnswerValue
        {
            get { return _AnswerValue; }
            set { _AnswerValue = value; }
        }

        public DateTime InsertDate
        {
            get { return _InsertDate; }
            set { _InsertDate = value; }
        }

        public DateTime UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }
        #endregion

        #region ICacheable Members
        public CacheItemMode CacheMode
        {
            get { throw new NotImplementedException(); }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string GetCacheKey()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
