﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    /// <summary>
    /// Basic Question value object
    /// </summary>
    [Serializable]
    public class Question : IValueObject, ICacheable, IReplicable,IComparable<Question>
    {
        private int _QuestionID = Constants.NULL_INT;
        private int _SiteID = Constants.NULL_INT;
        private string _Text = String.Empty;
        private DateTime _StartDate = DateTime.MinValue;
        private DateTime _EndDate = DateTime.MinValue;
        private DateTime _InsertDate = DateTime.MinValue;
        private DateTime _UpdateDate = DateTime.MinValue;
        private int _AdminMemberID = Constants.NULL_INT;
        private int _Weight = Constants.NULL_INT;
        private bool _Active = false;
        private QuestionAnswerEnums.QuestionType _QuestionType = QuestionAnswerEnums.QuestionType.FreeText;
        private List<Answer> _Answers = new List<Answer>();
        private List<StockAnswer> _StockAnswers = new List<StockAnswer>();
        private int _QuestionTypeID = 1; //default type is free text

        #region Properties
        public int QuestionID
        {
            get { return _QuestionID; }
            set { _QuestionID = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        public DateTime InsertDate
        {
            get { return _InsertDate; }
            set { _InsertDate = value; }
        }

        public DateTime UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        public int AdminMemberID
        {
            get { return _AdminMemberID; }
            set { _AdminMemberID = value; }
        }

        public int Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public QuestionAnswerEnums.QuestionType QuestionType
        {
            get { return _QuestionType; }
            set { _QuestionType = value; }
        }

        public int QuestionTypeID
        {
            get { return _QuestionTypeID; }
            set { _QuestionTypeID = value; }
        }

        public List<Answer> Answers
        {
            get { return _Answers; }
            set { _Answers = value; }
        }

        public List<StockAnswer> StockAnswers
        {
            get { return _StockAnswers; }
            set { _StockAnswers = value; }
        }
        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.AboveNormal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "Question_{0}";
        protected string alternativeKey;
        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public virtual string GetCacheKey()
        {
            if(alternativeKey != null)
                return alternativeKey;
            return GetCacheKeyString(_QuestionID);
        }

        public void SetAlternativeKey(string key)
        {
            alternativeKey = key;
        }

        public static string GetCacheKeyString(int questionID)
        {
            return String.Format(CACHEKEYFORMAT, questionID);
        }
        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            throw new NotImplementedException();
        }

        #endregion
        #region IComparable
        public int CompareTo(Question q)
        {
            return StartDate.CompareTo(q.StartDate);
        }

        public static Comparison<Question> AlphabeticComparison = delegate(Question q1, Question q2) { return q1.Text.CompareTo(q2.Text); };

        #endregion


        public Question Copy()
        {
            return Copy<Question>();
        }

        public T Copy<T>() where T:Question,new()
        {
            var question = this;
            return new T()
            {
                QuestionID = question.QuestionID,
                Answers = question.Answers.ToList(),
                StockAnswers = question.StockAnswers.ToList(),
                Active = question.Active,
                AdminMemberID = question.AdminMemberID,
                CacheMode = question.CacheMode,
                CachePriority = question.CachePriority,
                CacheTTLSeconds = question.CacheTTLSeconds,
                EndDate = question.EndDate,
                InsertDate = question.InsertDate,
                QuestionType = question.QuestionType,
                QuestionTypeID = question.QuestionTypeID,
                SiteID = question.SiteID,
                StartDate = question.StartDate,
                Text = question.Text,
                UpdateDate = question.UpdateDate,
                Weight = question.Weight
            };
        }
    }

}
