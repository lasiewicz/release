﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    [Serializable]
    public class PagedQuestion:Question
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool LatestFirst { get; set; }
        public int TotalAnswers { get; set; }

        private const string CACHEKEYFORMAT_PAGED = "Question_{0}_{1}_{2}_{3}";

        public static string GetCacheKeyString(int questionID, int pageNumber, int pageSize, bool latestFirst)
        {
            return String.Format(CACHEKEYFORMAT_PAGED, questionID, pageNumber, pageSize, latestFirst);
        }

        public override string GetCacheKey()
        {
            if (alternativeKey != null)
                return alternativeKey;
            return GetCacheKeyString(QuestionID,PageNumber,PageSize, LatestFirst);
        }
    }
}
