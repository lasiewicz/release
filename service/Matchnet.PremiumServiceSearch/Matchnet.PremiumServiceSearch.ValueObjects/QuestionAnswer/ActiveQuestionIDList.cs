﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    /// <summary>
    /// Cacheable container for a list of active QuestionIDs for a site
    /// </summary>
    [Serializable]
    public class ActiveQuestionIDList : IValueObject, ICacheable
    {
        List<int> _QuestionIDs = new List<int>();
        int _SiteID = Constants.NULL_INT;

        #region Properties
        public List<int> QuestionIDs
        {
            get { return _QuestionIDs; }
            set { _QuestionIDs = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }
        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 3600;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "ActionQuestionIDList_{0}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_SiteID);
        }

        public static string GetCacheKeyString(int siteID)
        {
            return String.Format(CACHEKEYFORMAT, siteID);
        }

        #endregion
    }
}
