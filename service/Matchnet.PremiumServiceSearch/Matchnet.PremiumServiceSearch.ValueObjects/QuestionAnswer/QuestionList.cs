﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    /// <summary>
    /// Basic Question value object
    /// </summary>
    public enum QuestionListSort:int
    {
        recent=0,
        az=1
    }

    [Serializable]
    public class QuestionList : IValueObject, ICacheable, IReplicable
    {
        List<Question> _questions = new List<Question>();
        List<Question> _questionsAZ = new List<Question>(); 

        public List<Question> Questions { get { return _questions; } set { _questions = value; } }
        public List<Question> QuestionsAZ { get { return _questionsAZ; } set { _questionsAZ = value; } }
        public int SiteID { get; set; }
        public void SortByAlpha()
        {
            try
            {
                _questionsAZ.Sort(QuestionAnswer.Question.AlphabeticComparison);
            }
            catch (Exception ex)
            {
                throw new Exception("Error sorting question list alphabetically.", ex);
            }

        }
        
        
        #region ICacheable Members

        private int _cacheTTLSeconds = 600;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "QuestionList_{0}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(SiteID);
        }

        public static string GetCacheKeyString(int questionID)
        {
            return String.Format(CACHEKEYFORMAT, questionID);
        }

        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
