﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    /// <summary>
    /// Cacheable Container class of Question objects for a member
    /// </summary>
    [Serializable]
    public class MemberQuestionList : IValueObject, ICacheable
    {
        List<Question> _Questions = new List<Question>();
        int _MemberID = Constants.NULL_INT;
        int _SiteID = Constants.NULL_INT;

        #region Properties
        public List<Question> Questions
        {
            get { return _Questions; }
            set { _Questions = value; }
        }

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }
        #endregion

        #region ICacheable Members
        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "MemberQuestionList_{0}_{1}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(_SiteID, _MemberID);
        }

        public static string GetCacheKeyString(int siteID, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, siteID, memberid);
        }

        #endregion
    }
}
