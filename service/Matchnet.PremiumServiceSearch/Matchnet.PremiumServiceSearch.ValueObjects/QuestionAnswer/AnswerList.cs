﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
 [Serializable]
    public class AnswerList : IValueObject, ICacheable, IReplicable
    {
        List<Answer> _answers = new List<Answer>();

        public List<Answer> Answers { get { return _answers; } set { _answers = value; } }
        public int SiteID { get; set; }
       

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.AboveNormal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "AnswersList_{0}";
        string _CACHEKEYFORMAT = "AnswersList_{0}";

        public string CacheKeyFormat{get{return _CACHEKEYFORMAT;} set{_CACHEKEYFORMAT=value;}}
        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString(CacheKeyFormat, SiteID);
        }

        public static string GetCacheKeyString(string keyformat,int siteid)
        {
            if (String.IsNullOrEmpty(keyformat))
                keyformat = CACHEKEYFORMAT;

            return String.Format(keyformat, siteid);
        }

        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
