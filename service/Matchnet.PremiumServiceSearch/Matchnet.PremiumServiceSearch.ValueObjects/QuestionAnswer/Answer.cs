﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    /// <summary>
    /// Basic Answer value object
    /// </summary>
    [Serializable]
    public class Answer : IValueObject, ICacheable, IReplicable
    {
        private int _AnswerID = Constants.NULL_INT;
        private int _QuestionID = Constants.NULL_INT;
        private int _MemberID = Constants.NULL_INT;
        private string _AnswerValue = string.Empty;
        private string _AnswerValuePending = string.Empty;
        private int _AdminMemberID = Constants.NULL_INT;
        private QuestionAnswerEnums.AnswerStatusType _AnswerStatus = QuestionAnswerEnums.AnswerStatusType.None;
        private QuestionAnswerEnums.AnswerType _AnswerType = QuestionAnswerEnums.AnswerType.FreeText;
        private DateTime _InsertDate = DateTime.MinValue;
        private DateTime _UpdateDate = DateTime.MaxValue;
        private bool _IsDirty = false;

        //dynamic
        private int _GenderMask = Constants.NULL_INT;
        private int _GlobalStatusMask = 0;
        private int _SelfSuspendedFlag = 0;
        private bool _HasApprovedPhotos = true;

        #region Properties
        public int AnswerID
        {
            get { return _AnswerID; }
            set { _AnswerID = value; }
        }

        public int QuestionID
        {
            get { return _QuestionID; }
            set { _QuestionID = value; }
        }

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public string AnswerValue
        {
            get { return _AnswerValue; }
            set { _AnswerValue = value; }
        }

        public string AnswerValuePending
        {
            get { return _AnswerValuePending; }
            set { _AnswerValuePending = value; }
        }

        public int AdminMemberID
        {
            get { return _AdminMemberID; }
            set { _AdminMemberID = value; }
        }

        public QuestionAnswerEnums.AnswerType AnswerType
        {
            get { return _AnswerType; }
            set { _AnswerType = value; }
        }

        public QuestionAnswerEnums.AnswerStatusType AnswerStatus
        {
            get { return _AnswerStatus; }
            set { _AnswerStatus = value; }
        }

        public DateTime InsertDate
        {
            get { return _InsertDate; }
            set { _InsertDate = value; }
        }

        public DateTime UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }

        public bool IsDirty
        {
            get { return _IsDirty; }
            set { _IsDirty = value; }
        }

        public int GenderMask
        {
            get { return _GenderMask; }
            set { _GenderMask = value; }
        }

        public int GlobalStatusMask
        {
            get { return _GlobalStatusMask; }
            set { _GlobalStatusMask = value; }
        }

        public int SelfSuspendedFlag
        {
            get { return _SelfSuspendedFlag; }
            set { _SelfSuspendedFlag = value; }
        }

        public bool HasApprovedPhotos
        {
            get { return _HasApprovedPhotos; }
            set { _HasApprovedPhotos = value; }
        }

        #endregion

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { throw new NotImplementedException(); }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string GetCacheKey()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
