﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer
{
    [Serializable]
    public class QuestionResponse : Question
    {
        public int TotalAnswers { get; set; }
    }
}
