﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects
{
    public class SpotlightConstants
    {

        public enum SearchParameters
        {
            brandid,
            regionid,
            distance,
            age,
            gendermask,
            promotionflag,
            geobox,
            boxx,
            boxy,
            latitude,
            longitude,
            depth1regionid,
            depth2regionid,
            depth3regionid
            
        }

       // public string[] AttributeMap = new string[] { };
    }
}
