﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.PremiumServiceSearch.ValueObjects.ReplicationAction
{
    /// <summary>
    /// Replication action for Answer
    /// </summary>
    [Serializable]
    public class AnswerReplicationAction : IReplicationAction
    {
        private Answer _Answer;
        private int _SiteID;
        AnswerActionType _AnswerActionType = AnswerActionType.Add;

        public Answer Answer
        {
            get { return _Answer; }
            set { _Answer = value; }
        }

        public AnswerActionType AnswerActionType
        {
            get { return _AnswerActionType; }
            set { _AnswerActionType = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }
    }

    /// <summary>
    /// Available answer action types for replication
    /// </summary>
    [Serializable]
    public enum AnswerActionType
    {
        Add = 1,
        Remove = 2,
        Update = 3
    }
}
