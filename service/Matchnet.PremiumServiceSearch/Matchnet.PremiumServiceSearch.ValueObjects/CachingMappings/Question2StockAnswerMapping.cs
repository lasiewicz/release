﻿using System.Collections.Generic;
using Matchnet.BaseClasses;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace Matchnet.PremiumServiceSearch.ValueObjects.CachingMappings
{
    public class Question2StockAnswerMapping : CacheableListMapping<int, StockAnswer>
    {
        public Question2StockAnswerMapping(int key, List<StockAnswer> value)
            : base(key, value)
        {
        }

        protected override string GetCacheKeyFromId(int id)
        {
            return GetCacheKey(id);
        }

        public static string GetCacheKey(int id)
        {
            return "~Question2StockAnswerMapping^" + id;
        }

    }
}
