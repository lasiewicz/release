﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QueueMessage
{
    public enum QueueMessageType
    {
        update,
        delete
    }

    [Serializable]
    public class SaveMemberMessage:IQueueMessage
    {
        int memberID;
        int brandID;
        DateTime expirationDate;
        bool enableFlag;
        PremiumMember member = null;

        QueueMessageType saveType;

        public SaveMemberMessage()
        { }

        public SaveMemberMessage(PremiumMember m)
        {
            member = m;
        }
        public SaveMemberMessage(QueueMessageType savetype, int brandid, int memberid, DateTime expdate, bool enableflag)
        {
            memberID = memberid;
            brandID = brandid;
            expirationDate = expdate;
            saveType = savetype;
            enableFlag = enableflag;
        }

        public int MemberID
        {
            get { return (memberID); }
        }

        public int BrandID
        {
            get { return (brandID); }
        }


        public bool EnableFlag
        {
            get { return (enableFlag); }
        }



        public DateTime ExpirationDate
        {
            get { return (expirationDate); }
        }


        public PremiumMember Member
        {
            get { return (member); }
        }


        #region IQueueMember

        int attempts;
        DateTime lastAttemptDateTime;
        List<string> errors;
        QueueMessageType messageType;
        public int Attempts
        {
            get { return (attempts); }
            set { attempts=value; }
        }

        public DateTime LastAttemptDateTime
        {
            get { return (lastAttemptDateTime); }
            set { lastAttemptDateTime = value; }
        }

        public List<string> Errors
        {
            get { return (errors); }
            set { errors = value; }
        }

        public QueueMessageType MessageType
        {
            get { return (messageType); }
            set { messageType = value; }
        }
        #endregion
    }
}
