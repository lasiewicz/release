﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects.QueueMessage
{
    public enum ImpressionType:int
    {
         spotlight=1
    }

    [Serializable]
    public class ImpressionLogMessage:IValueObject
    {
        int siteID = 0;
        ImpressionType viewType;
        int pageID = 0;
        int viewedMemberID = 0;
        int viewingMemberID = 0;
        DateTime impressionDateTime;


        public ImpressionLogMessage(int siteid, int pageid, ImpressionType viewtype, int viewedmemberid, int viewingmemberid, DateTime impressiontime)
        {
            siteID = siteid;
            pageID = pageid;
            viewType = viewtype;
            viewingMemberID = viewingmemberid;
            viewedMemberID = viewedmemberid;
            impressionDateTime = impressiontime;

        }

        public int SiteID
        {
            get { return siteID; }
            set { siteID = value; }
        }

        public int PageID
        {
            get { return pageID; }
            set { pageID = value; }
        }

        public int ViewedMemberID
        {
            get { return viewedMemberID; }
            set { viewedMemberID = value; }
        }


        public int ViewingMemberID
        {
            get { return viewingMemberID; }
            set { viewingMemberID = value; }
        }

        public DateTime ImpressionDateTime
        {
            get { return impressionDateTime; }
            set { impressionDateTime = value; }

        }

        public ImpressionType ImpressionLogType
        {
            get { return viewType; }
            set { viewType = value; }

        }


        public override string ToString()
        {  
            StringBuilder trace= new StringBuilder();
            try
            {
                trace.Append("\r\nSiteID=" + siteID.ToString());
                trace.Append("\r\nPageID=" + pageID.ToString());
                trace.Append("\r\nViewedMemberID=" + viewedMemberID.ToString());
                trace.Append("\r\nViewingMemberID=" + viewingMemberID.ToString());
                trace.Append("\r\nImpressionLogType=" + viewType.ToString());
                trace.Append("\r\nImpressionDateTime=" + impressionDateTime.ToString());
                return trace.ToString();

            }
            catch (Exception ex)
            { return ""; }

        }
    }
}
