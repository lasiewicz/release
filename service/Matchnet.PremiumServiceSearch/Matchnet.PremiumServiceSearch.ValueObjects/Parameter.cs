﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServiceSearch.ValueObjects
{

    public interface IParameter
    {
        Enum ID
        {
            get;
            set;
        }
    }
    [Serializable]
    public class Parameter<T>:IParameter
    {
        private Enum id;
        private T val;
        string format = "{0}={1}";
        public Parameter(Enum id1, T val1)
        {
            id = id1;
            val = val1;
        }

        public Enum ID
        {
            get { return id; }
            set { id = value; }
        }

        public T Value
        {
            get { return val; }
            set { val=value; }

        }

        public override string ToString()
        {
            try
            {
                return String.Format(format, id.ToString(), val.ToString());
            }
            catch (Exception ex)
            { return ""; }

        }

    }
    [Serializable]
    public class ParameterCollection : KeyedCollection<Enum, IParameter>
    {
        protected override Enum GetKeyForItem(IParameter item)
        {
            return item.ID;
        }


        public T Get<T>(Enum id)
        {
            Parameter<T> param = null;
            if (this.Contains(id))
            {
                param = (Parameter<T>)this[id];
            }
            else
            {
                return default(T);
            }
            return param.Value;
        }

        public override string ToString()
        {
            string parm = "";
            try
            {
                foreach (IParameter p in this.Items)
                {
                    parm += String.Format("{0}\r\n", p.ToString());
                }
                return parm;
            }
            catch (Exception ex)
            { return ""; }

        }
      
    }
}
