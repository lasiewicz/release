﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServiceSearch.BusinessLogic;
using Matchnet.Replication;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using System.Diagnostics;
using System.Threading;
using Matchnet.BaseClasses;

namespace Matchnet.PremiumServiceSearch.ServiceManagers
{
    public class QuestionAnswerSM : MarshalByRefObject, IQuestionAnswer, IServiceManager, IReplicationActionRecipient, IDisposable
    {
        private const string MODULE_NAME = "QuestionAnswerSM";
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;

        #region perf counters
        private PerformanceCounter _perfGetQuestionRequestsPerSec;
        private PerformanceCounter _perfGetMemberQuestionsRequestsPerSec;
        private PerformanceCounter _perfGetActiveQuestionIDsRequestsPerSec;
        private PerformanceCounter _perfAddAnswerRequestsPerSec;
        private PerformanceCounter _perfRemoveAnswerRequestsPerSec;
        #endregion

        #region Constructor
        public QuestionAnswerSM()
        {
            string functionName = "QuestionAnswerSM";
            try
            {
                string machineName = "";

                //start hydra writer
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnQuestionAnswer" });
                _hydraWriter.Start();

                //replication
                QuestionAnswerBL.Instance.ReplicationRequested += new QuestionAnswerBL.ReplicationEventHandler(QuestionAnswerBL_ReplicationRequested);

                string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QUESTIONANSWERSVC_REPLICATION_OVERRIDE");
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_QUESTIONANSWER_MANAGER_NAME, machineName);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                {
                    _replicator = new Replicator("QUESTIONANSWERSVC"); //this uses specific name for QuestionAnswer instead of PremiumService service constant, so it gets it's own replication queue
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                }

                //perf initialize
                QuestionAnswerBL.Instance.AddAnswerRequested += new QuestionAnswerBL.AddAnswerRequestEventHandler(QuestionAnswerBL_AddAnswerRequested);
                QuestionAnswerBL.Instance.RemoveAnswerRequested += new QuestionAnswerBL.RemoveAnswerRequestEventHandler(QuestionAnswerBL_RemoveAnswerRequested);
                QuestionAnswerBL.Instance.GetActiveQuestionIDsRequested += new QuestionAnswerBL.GetActiveQuestionIDsRequestEventHandler(QuestionAnswerBL_GetActiveQuestionIDsRequested);
                QuestionAnswerBL.Instance.GetMemberQuestionsRequested += new QuestionAnswerBL.GetMemberQuestionRequestEventHandler(QuestionAnswerBL_GetMemberQuestionsRequested);
                QuestionAnswerBL.Instance.GetQuestionRequested += new QuestionAnswerBL.GetQuestionRequestEventHandler(QuestionAnswerBL_GetQuestionRequested);
                initPerfCounters();

                //warmup routine for caching active questions
                //only for core sites with a lot of answers
                try
                {
                    string warmupSites = GetWarmupCacheSiteList();
                    string[] warmupSitesList = warmupSites.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    int defaultMostRecentActiveQuestions = 10;
                    foreach (string s in warmupSitesList)
                    {
                        int siteID = 0;
                        int.TryParse(s, out siteID);
                        if (siteID > 0)
                        {
                            //get active question list
                            ActiveQuestionIDList activeQuestionIDList = GetActiveQuestionIDs(siteID);
                            if (activeQuestionIDList != null && activeQuestionIDList.QuestionIDs.Count > 0)
                            {
                                int totalActiveQuestions = activeQuestionIDList.QuestionIDs.Count;
                                int recentActiveQuestions = totalActiveQuestions > defaultMostRecentActiveQuestions ? defaultMostRecentActiveQuestions : totalActiveQuestions;

                                EventLog.WriteEntry(ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Warmup Cache for siteID: " + siteID.ToString() + ", number of most recent active questions to cache: " + recentActiveQuestions.ToString(), EventLogEntryType.Information);
                                for (int i = 0; i < recentActiveQuestions; i++)
                                {
                                    Thread workerGetQuestion = new Thread(new ParameterizedThreadStart(WarmupGetQuestion));
                                    workerGetQuestion.Start(activeQuestionIDList.QuestionIDs[totalActiveQuestions - i - 1]);
                                }
                            }

                            Thread workerGetRecentAnswersList = new Thread(new ParameterizedThreadStart(WarmupGetRecentAnswersList));
                            workerGetRecentAnswersList.Start(siteID);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, false);
                }

            }
            catch (Exception ex)
            { 
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); 
            }

        }

        
        #endregion

        #region IQuestionAnswer Members

        public ActiveQuestionIDList GetActiveQuestionIDs(int siteID)
        {
            string functionName = "GetActiveQuestionIDs";
            
            try
            {
                return QuestionAnswerBL.Instance.GetActiveQuestionIDs(siteID);
            }
            catch (Exception ex)
            { 
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

            return null;
            
        }

        public PagedResult<QuestionResponse> GetActiveQuestionListPaged(int siteID, int pageNumber, int pageSize, QuestionListSort sort)
        {
            return QuestionAnswerBL.Instance.GetActiveQuestionListPaged(siteID, pageNumber, pageSize, sort);
        }

        public Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer.Question GetQuestion(int questionID)
        {
            string functionName = "GetQuestion";

            try
            {
                return QuestionAnswerBL.Instance.GetQuestion(questionID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

            return null;
        }

        public PagedQuestion GetQuestionPaged(int questionID, int pageNumber, int pageSize, bool latestFirst)
        {
            string functionName = "GetQuestionPaged";

            try
            {
                return QuestionAnswerBL.Instance.GetQuestionPaged(questionID, pageNumber, pageSize, latestFirst);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
            return null;
        }

        public void AddAnswer(int memberID, int siteID, int questionID, string answerValue)
        {
            string functionName = "AddAnswer";

            try
            {
                QuestionAnswerBL.Instance.AddAnswer(memberID, siteID, questionID, answerValue);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        public void RemoveMemberAnswer(int questionID, int siteID, int memberID)
        {
            string functionName = "RemoveMemberAnswer";

            try
            {
                QuestionAnswerBL.Instance.RemoveMemberAnswer(questionID, siteID, memberID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
            
        }

        public void RemoveAnswer(int answerID, int questionID, int siteID, int memberID)
        {
            string functionName = "RemoveAnswer";

            try
            {
                QuestionAnswerBL.Instance.RemoveAnswer(answerID, questionID, siteID, memberID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        public MemberQuestionList GetMemberQuestions(int memberID, int siteID)
        {
            string functionName = "GetMemberQuestions";

            try
            {
                return QuestionAnswerBL.Instance.GetMemberQuestions(memberID, siteID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

            return null;
        }

        public void UpdateAnswer(Answer answer, int siteID)
        {
            string functionName = "UpdateAnswer";

            try
            {
                QuestionAnswerBL.Instance.UpdateAnswer(answer, siteID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        public void AdminUpdateAnswer(Answer answer, int siteID)
        {
            string functionName = "AdminUpdateAnswer";

            try
            {
                QuestionAnswerBL.Instance.AdminUpdateAnswer(answer, siteID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        public QuestionList GetActiveQuestionList(int siteID)
        {
            string functionName = "GetActiveQuestionList";

            try
            {
               return QuestionAnswerBL.Instance.GetActiveQuestionList( siteID);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                return null;
            }

        }


        public List<StockAnswer> GetStockAnswers(List<int> questionIds)
        {
            string functionName = "GetStockAnswers";

            try
            {
                return QuestionAnswerBL.Instance.GetStockAnswers(questionIds);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                return null;
            }
        }


        public AnswerList GetRecentAnswerList(int siteID, int daysback)
        {
            string functionName = "GetRecentAnswersList";

            try
            {
                return QuestionAnswerBL.Instance.GetAnswerList(siteID,daysback);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                return null;
            }

        }

        public PagedResult<Question> GetRecentQuestionListPaged(int siteID, int daysback, int pageNumber, int pageSize)
        {
            string functionName = "GetQuestionListPaged";

            try
            {
                return QuestionAnswerBL.Instance.GetQuestionListPaged(siteID, daysback, pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                return null;
            }
        }
        #endregion

        #region Private Methods
        private void WarmupGetQuestion(object questionID)
        {
            string functionName = "WarmupGetQuestion";

            try
            {
                int qID = 0;
                int.TryParse(questionID.ToString(), out qID);
                if (qID > 0)
                {
                    QuestionAnswerBL.Instance.GetQuestion(qID);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, false);
            }
        }

        private void WarmupGetRecentAnswersList(object siteID)
        {
            string functionName = "WarmupGetQuestion";

            try
            {
                int sID = 0;
                int.TryParse(siteID.ToString(), out sID);
                if (sID > 0)
                {
                    QuestionAnswerBL.Instance.GetAnswerList(sID, GetAnswerListDaysBack(sID));
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, false);
            }
        }

        private int GetAnswerListDaysBack(int siteID)
        {
            int days = 10;
            try
            {
                days = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QUESTION_ANSWER_RECENT_DAYS", Constants.NULL_INT, siteID, Constants.NULL_INT));
            }
            catch (Exception ex)
            {
                //setting missing
                days = 10;
            }

            return days;

        }

        private string GetWarmupCacheSiteList()
        {
            string warmupList = "";
            try
            {
                warmupList = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QUESTION_ANSWER_CACHE_WARMUP_SITES");
            }
            catch (Exception ex)
            {
                //setting missing
                warmupList = "103, 4, 101, 9041, 9051";
            }

            return warmupList;

        }
        #endregion

        #region Replication Handler
        private void QuestionAnswerBL_ReplicationRequested(IReplicationAction replicationAction)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicationAction);
        }
        #endregion

        #region IServiceManager Members
        public void PrePopulateCache()
        {
            //no prepopulate on service start
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

        }

        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IReplicationActionRecipient Members

        public void Receive(IReplicationAction replicationAction)
        {   
            string functionName = "IReplicationActionRecipient.Receive";

            try
            {
                QuestionAnswerBL.Instance.PlayReplicationAction(replicationAction);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        #endregion

        #region perfomance event handlers
        public static void PerfCounterInstall()
        {
            try
            {

                ServiceTrace.Instance.DebugTrace("QuestionAnswerSM", "Start PerfCounterInstall");
                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(new CounterCreationData[] {	
															 new CounterCreationData("Get Question Requests/second", "Get Question Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Get Member Questions Requests/second", "Get Member Questions Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Get Active Question IDs Requests/second","Get Active Question IDs Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Add Answer Requests/second", "Add Answer Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Remove Answer Requests/second", "Remove Answer Requests/second", PerformanceCounterType.RateOfCountsPerSecond32)														 
														 });

                PerformanceCounterCategory.Create(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, PerformanceCounterCategoryType.SingleInstance, ccdc);
                ServiceTrace.Instance.DebugTrace("QuestionAnswerSM", "Finish PerfCounterInstall");
            }
            catch (Exception ex)
            { ServiceTrace.Instance.Trace("QuestionAnswerSM", ex, null); }
        }


        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION);
        }

        private void initPerfCounters()
        {
            _perfGetQuestionRequestsPerSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, "Get Question Requests/second", false);
            _perfGetMemberQuestionsRequestsPerSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, "Get Member Questions Requests/second", false);
            _perfGetActiveQuestionIDsRequestsPerSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, "Get Active Question IDs Requests/second", false);
            _perfAddAnswerRequestsPerSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, "Add Answer Requests/second", false);
            _perfRemoveAnswerRequestsPerSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME_QUESTION, "Remove Answer Requests/second", false);

        }


        private void resetPerfCounters()
        {
            if (_perfGetQuestionRequestsPerSec != null)
            {
                _perfGetQuestionRequestsPerSec.RawValue = 0;
                _perfGetMemberQuestionsRequestsPerSec.RawValue = 0;
                _perfGetActiveQuestionIDsRequestsPerSec.RawValue = 0;
                _perfAddAnswerRequestsPerSec.RawValue = 0;
                _perfRemoveAnswerRequestsPerSec.RawValue = 0;
            }

        }

        private void QuestionAnswerBL_GetQuestionRequested()
        {
            string functionName = "QuestionAnswerBL_GetQuestionRequested";
            try
            {
                if (_perfGetQuestionRequestsPerSec != null)
                    _perfGetQuestionRequestsPerSec.Increment();
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void QuestionAnswerBL_GetMemberQuestionsRequested()
        {
            string functionName = "QuestionAnswerBL_GetMemberQuestionsRequested";
            try
            {
                if (_perfGetMemberQuestionsRequestsPerSec != null)
                    _perfGetMemberQuestionsRequestsPerSec.Increment();
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void QuestionAnswerBL_GetActiveQuestionIDsRequested()
        {
            string functionName = "QuestionAnswerBL_GetActiveQuestionIDsRequested";
            try
            {
                if (_perfGetActiveQuestionIDsRequestsPerSec != null)
                    _perfGetActiveQuestionIDsRequestsPerSec.Increment();
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void QuestionAnswerBL_RemoveAnswerRequested()
        {
            string functionName = "QuestionAnswerBL_RemoveAnswerRequested";
            try
            {
                if (_perfRemoveAnswerRequestsPerSec != null)
                    _perfRemoveAnswerRequestsPerSec.Increment();
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void QuestionAnswerBL_AddAnswerRequested()
        {
            string functionName = "QuestionAnswerBL_AddAnswerRequested";
            try
            {
                if (_perfAddAnswerRequestsPerSec != null)
                    _perfAddAnswerRequestsPerSec.Increment();
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }
        #endregion
    }
}
