﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using System.Collections;

using System.IO;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
using Matchnet.PremiumServiceSearch.BusinessLogic.SpotlightMember;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.Queuing;
using System.Messaging;
namespace Matchnet.PremiumServiceSearch.ServiceManagers
{
    public class SpotlightMemberSM : MarshalByRefObject, ISpotlightMember, ISpotlightMember11, IServiceManager, IReplicationRecipient, IDisposable
    {
        private const string MODULE_NAME = "SpotlightMemberSM";
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;
        private Synchronizer _synchronizer = null;
        QueueProcessor _queueProcessor = null;

        #region perf counters
        private PerformanceCounter _perfSpotlightUpdateRequestsSec;
        private PerformanceCounter _perfSpotlightMemberRequestsSec;
        private PerformanceCounter _perfSpotlightCachedMemberRequestsSec;
        private PerformanceCounter _perfSpotlightSearchRequestsSec;
        private PerformanceCounter _perfPromotionSearchRequestsSec;
        private PerformanceCounter _perfSpotlightResultItemsSec;

        #endregion

        #region log queue processor
        private Matchnet.Queuing.Processor _queueLogProcessor;
        #endregion

        public SpotlightMemberSM()
        {
            string functionName = "SpotlightMemberSM";
            try
            {
                ServiceTrace.Instance.Trace(functionName, "Begin constructor");
                string machineName = "";

                ServiceTrace.Instance.Trace(functionName, "Register synchronization/replication events");
                //synchronization
                SpotlightMemberBL.Instance.SynchronizationRequested += new SpotlightMemberBL.SynchronizationEventHandler(SpotlightMemberBL_SynchronizationRequested);
                //replication
                SpotlightMemberBL.Instance.ReplicationRequested += new SpotlightMemberBL.ReplicationEventHandler(SpotlightMemberBL_ReplicationRequested);

                ServiceTrace.Instance.Trace(functionName, "Starting queue processor");
                _queueProcessor = new QueueProcessor();
                _queueProcessor.Start();

                ServiceTrace.Instance.Trace(functionName, "Starting HydraWriter");
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnPremiumServices" });
                _hydraWriter.Start();

                ServiceTrace.Instance.Trace(functionName, "Getting replication URI");
                string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESEARCHSVC_REPLICATION_OVERRIDE");
              
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_CONSTANT, Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_SPOTLIGHT_MANAGER_NAME, machineName);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                { 
                    ServiceTrace.Instance.Trace(functionName, "Got replication URI:" + replicationURI);

                    _replicator = new Replicator(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                    ServiceTrace.Instance.Trace(functionName, "Started replicator");
                }
                else
                {
                    ServiceTrace.Instance.Trace(functionName, "Got empty replication URI" ); 
                }

               

                _synchronizer = new Synchronizer(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                _synchronizer.Start();
                ServiceTrace.Instance.Trace(functionName, "Started synchronizer");

                ServiceTrace.Instance.Trace(functionName, "Registering perf counters events");
                SpotlightMemberBL.Instance.SpotlightMemberRequested += new SpotlightMemberBL.SpotlightMemberRequestEventHandler(SpotlightMemberBL_SpotlightMemberRequested);
                SpotlightMemberBL.Instance.SpotlightMemberUpdated += new SpotlightMemberBL.SpotlightMemberUpdateEventHandler(SpotlightMemberBL_SpotlightMemberUpdated);
                SpotlightSearchBL.Instance.SpotlightSearchRequested += new SpotlightSearchBL.SpotlightSearchRequestEventHandler(SpotlightSearchBL_SpotlightSearchRequested);
                initPerfCounters();

                ServiceTrace.Instance.Trace(functionName, "Creating Log Queue Processor, path " + BusinessLogic.ImpressionLogBL.LOG_QUEUE_PATH);
                Matchnet.Queuing.Util.GetQueue(BusinessLogic.ImpressionLogBL.LOG_QUEUE_PATH, true, true);
                _queueLogProcessor = new Matchnet.Queuing.Processor(BusinessLogic.ImpressionLogBL.LOG_QUEUE_PATH, BusinessLogic.ImpressionLogBL.QUEUE_THREADS, true, new BusinessLogic.ImpressionLogBL(), ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                _queueLogProcessor.Start();
                ServiceTrace.Instance.Trace(functionName, "End constructor");


            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }
        }

        public void LogImpression(int siteid, int pageid, ValueObjects.QueueMessage.ImpressionType viewtype, int viewedmemberid, int viewingmemberid, DateTime impressiontime)
        {
            string functionName = "LogImpression";
            try
            {
                BusinessLogic.ImpressionLogBL.Enqueue(siteid, pageid, viewtype, viewedmemberid, viewingmemberid, impressiontime);

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }



        }
        #region ISpotlightMember

        public void SaveMember( PremiumMember member, bool async)
        {
            string functionName = "SaveMember1";
            try
            {
                if (async)
                {
                    SpotlightMemberBL.Instance.EnqueueMember(member);
                }
                else
                {
                    SpotlightMemberBL.Instance.SaveMember(member);
                }
            }
            catch (Exception ex)
            { PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }

        }

        public void SaveMember11(int brandid, int memberid, DateTime expirationDate, bool enableFlag, bool async)
        {
            string functionName = "SaveMember11";
            try
            {
                SaveMember(brandid, memberid, expirationDate, enableFlag, async);
            }
            catch (Exception ex)
            { PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }

        }
        public ResultList Search(Query query)
        {
            string functionName = "Search";
            try
            {
                return SpotlightSearchBL.Instance.Search(query);

            }
            catch (Exception ex)
            {
                ServiceBoundaryException e = PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.GetServiceException(MODULE_NAME, functionName, ex);
                throw e;
            }

        }
        public void SaveMember(int brandid, int memberid, DateTime expirationDate, bool enableFlag, bool async)
        {
            string functionName = "SaveMember";
            try
            {
                if (async)
                {
                    SpotlightMemberBL.Instance.EnqueueMember(brandid, memberid, expirationDate, enableFlag);
                }
                else
                {
                    SpotlightMemberBL.Instance.SaveMember(brandid, memberid, expirationDate, enableFlag);
                }

            }
            catch (Exception ex)
            { PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }

        }


        public PremiumMember GetServiceMember(int brandid, int memberid,int serviceinstanceid)
        {
            string functionName = "GetServiceMember";
            try
            {

              return   SpotlightMemberBL.Instance.GetServiceMember(brandid, memberid, serviceinstanceid);
               

            }
            catch (Exception ex)
            { PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex,false);
            throw ex;
            }

        }
        
        #endregion
        
        #region IServiceManager
        //avoid GC
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {
        }
        #endregion
                
        #region IDisposable Members
        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

            if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }

            if(_queueProcessor != null)
            _queueProcessor.Stop();

            if (_queueLogProcessor != null)
                _queueLogProcessor.Stop();
            //resetPerfCounters();
        }
        #endregion
        
        #region IReplicationRecipient

        void IReplicationRecipient.Receive(IReplicable replicableObject)
        {
            PremiumServiceSearch.BusinessLogic.SpotlightMember.SpotlightMemberBL.Instance.InsertCacheMember((PremiumMember)replicableObject, false,false);
        }


        #endregion

        #region synchronization/replication events

        private void SpotlightMemberBL_ReplicationRequested(IReplicable replicableObject)
        {
            if(_replicator != null)
                _replicator.Enqueue(replicableObject);
        }


        private void SpotlightMemberBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if(_synchronizer != null)
            _synchronizer.Enqueue(key, cacheReferences);
        }
        #endregion

        #region perfomance event handlers
        public static void PerfCounterInstall()
        {
            try
            {
                ServiceTrace.Instance.DebugTrace("SpotlightMemberSM", "Start PerfCounterInstall");
                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(new CounterCreationData[] {	
															 new CounterCreationData("Spotlight Member Requests/second", "Spotlight Member Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Spotlight Cached Member Requests/second", "Spotlight Cached Member Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Spotlight Search Requests/second","Spotlight Search Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Spotlight Search Promotion Requests/second", "Spotlight Search Promotion Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Spotlight Member Update Requests/second", "Spotlight Member Update Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Spotlight Search Results/second", "Spotlight Search Results/second", PerformanceCounterType.RateOfCountsPerSecond32)
														 
														 });
               
                PerformanceCounterCategory.Create(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, ccdc);
                ServiceTrace.Instance.DebugTrace("SpotlightMemberSM", "Finish PerfCounterInstall");
            }
            catch (Exception ex)
            { ServiceTrace.Instance.Trace("SpotlightMemberSM", ex, null); }
        }


        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
        }

        private void initPerfCounters()
        {
            _perfSpotlightMemberRequestsSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Spotlight Member Requests/second", false);
            _perfSpotlightCachedMemberRequestsSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Spotlight Cached Member Requests/second", false);
            _perfSpotlightSearchRequestsSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Spotlight Search Requests/second", false);
            _perfPromotionSearchRequestsSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Spotlight Search Promotion Requests/second", false);
            _perfSpotlightUpdateRequestsSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Spotlight Member Update Requests/second", false);
            _perfSpotlightResultItemsSec = new PerformanceCounter(Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, "Spotlight Search Results/second", false);
        }


        private void resetPerfCounters()
        {
            _perfSpotlightMemberRequestsSec.RawValue = 0;
            _perfSpotlightCachedMemberRequestsSec.RawValue = 0;
            _perfSpotlightSearchRequestsSec.RawValue = 0;
            _perfPromotionSearchRequestsSec.RawValue = 0;
            _perfSpotlightUpdateRequestsSec.RawValue = 0;
            _perfSpotlightResultItemsSec.RawValue = 0;


        }

        private void SpotlightMemberBL_SpotlightMemberRequested(bool cache)
        {
            try
            {

                _perfSpotlightMemberRequestsSec.Increment();
                if (cache)
                    _perfSpotlightCachedMemberRequestsSec.Increment();
            }
            catch (Exception ex)
            { }


        }


        private void SpotlightMemberBL_SpotlightMemberUpdated()
        {
            try
            {

                _perfSpotlightUpdateRequestsSec.Increment();
            }
            catch (Exception ex)
            { }

        }


        private void SpotlightSearchBL_SpotlightSearchRequested(bool cacheHit, bool promotionflag, int count)
        {
            try
            {

                _perfSpotlightSearchRequestsSec.Increment();
                if(cacheHit)
                    _perfPromotionSearchRequestsSec.Increment();
                _perfSpotlightResultItemsSec.IncrementBy((long)count);
            }
            catch (Exception ex)
            { }

        }
        #endregion
    }
}
