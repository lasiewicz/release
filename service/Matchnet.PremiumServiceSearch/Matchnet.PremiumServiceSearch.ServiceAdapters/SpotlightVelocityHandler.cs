﻿using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Spark.Common.AccessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.PremiumServiceSearch.ServiceAdapters
{
    public class SpotlightVelocityHandler
    {
        const int IMPRESSION_CAP = 50;
        SpotlightClientMetadata _spotlightClientMetadata;
        List<MemberImpression> _spotlightImpressions;
        List<MemberImpression> _promotionImpressions;
        bool promotionsEnabled;
        int _velocityCap = 0;
        int _velocityQuota = 0;
        int _velocityPeriod = 0; //hours
        int _promotionRatio = 0;
        string _lastSpotlightMember;
        bool _ignoreQuota = false;
        DateTime _requestTime = DateTime.MinValue;
        MemberImpression _lastImpression = new MemberImpression(0, DateTime.MinValue);
        Matchnet.Content.ValueObjects.BrandConfig.Brand _brand;
        Matchnet.Member.ServiceAdapters.Member _member;
        Matchnet.ICaching _CacheWebBucket;

        public bool PromotionsEnabled
        {
            get { return promotionsEnabled; }
            set { promotionsEnabled = value; }

        }

        public int VelocityCap
        {
            get { return _velocityCap; }
            set { _velocityCap = value; }

        }

        public int VelocityQuota
        {
            get { return _velocityQuota; }
            set { _velocityQuota = value; }

        }

        public int VelocityPeriod
        {
            get { return _velocityPeriod; }
            set { _velocityPeriod = value; }

        }

        public int PromotionRatio
        {
            get { return _promotionRatio; }
            set { _promotionRatio = value; }

        }

        public bool IgnoreQuota
        {
            set { _ignoreQuota = value; }
        }

        public SpotlightVelocityHandler(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member, int velocitycap, int velocityperiod, Matchnet.ICaching cache, bool initializeImpressions, bool resetSpotlightClientMetadataCache)
        {
            _CacheWebBucket = cache;

            MemberImpression promoLastImpression = new MemberImpression(0, DateTime.MinValue);
            MemberImpression memberLastImpression = new MemberImpression(0, DateTime.MinValue);
            _requestTime = DateTime.Now;
            _brand = brand;
            _member = member;
            _velocityCap = velocitycap;
            _velocityPeriod = velocityperiod;

            //get impressions data
            if (!resetSpotlightClientMetadataCache)
            {
                _spotlightClientMetadata = _CacheWebBucket.Get(SpotlightClientMetadata.GetCacheKeyString(brand.BrandID, member.MemberID)) as SpotlightClientMetadata;
            }

            if (_spotlightClientMetadata == null)
            {
                _spotlightClientMetadata = new SpotlightClientMetadata();
                _spotlightClientMetadata.MemberID = member.MemberID;
                _spotlightClientMetadata.BrandID = brand.BrandID;
            }
            if (_velocityPeriod > 0)
            {
                _spotlightClientMetadata.CacheTTLSeconds = _velocityPeriod * 60 * 60; //velocity period is in hours, convert to seconds
            }

            if (initializeImpressions)
            {
                //get spotlight impressions data
                if (_spotlightClientMetadata.SpotlightImpressions != null && _spotlightClientMetadata.SpotlightImpressions.Count > 0)
                {
                    _spotlightImpressions = RemoveExpiredImpressions(_spotlightClientMetadata.SpotlightImpressions);
                    if (_spotlightImpressions != null && _spotlightImpressions.Count > 0)
                    {
                        memberLastImpression = _spotlightImpressions[0];
                    }
                }
                else
                {
                    _spotlightImpressions = new List<MemberImpression>();
                }

                //get promotions impressions data
                if (_spotlightClientMetadata.PromoImpressions != null && _spotlightClientMetadata.PromoImpressions.Count > 0)
                {
                    _promotionImpressions = RemoveExpiredImpressions(_spotlightClientMetadata.PromoImpressions);
                    if (_promotionImpressions != null && _promotionImpressions.Count > 0)
                    {
                        promoLastImpression = (MemberImpression)_promotionImpressions[0];
                    }
                }
                else
                {
                    _promotionImpressions = new List<MemberImpression>();
                }
            }
        }

        private List<MemberImpression> RemoveExpiredImpressions(List<MemberImpression> list)
        {
            try
            {
                if (list == null || list.Count == 0)
                    return list;

                IEnumerable<MemberImpression> query = from l in list
                                                        where l.ImpressionTime.AddHours(_velocityPeriod) >= DateTime.Now
                                                        select l;

                return query.ToList<MemberImpression>();

            }
            catch (Exception ex)
            { return null; }
        }


        private List<int> GetNotImpressedMembers(List<MemberImpression> list, List<int> results)
        {
            try
            {
                if (list == null || list.Count == 0)
                    return results;

                IEnumerable<int> query = from r in results
                                         where (
                                         from l in list
                                         select l.MemberID).Contains<int>(r) == false
                                         select r;
                return query.ToList<int>();

            }
            catch (Exception ex)
            { return null; }

        }

        private List<int> GetImpressedMembers(List<MemberImpression> list, List<int> results, int quota, bool removeLastImpressedMember, bool ignoreQuota)
        {
            List<int> members = new List<int>();
            int lastMemberID = 0;
            try
            {
                if (removeLastImpressedMember)
                {
                    lastMemberID = getLastListMemberID(list);
                }

                var query = from l in list
                            join r in results
                            on l.MemberID equals r
                            group l by l.MemberID
                                into g
                                select new { MemberID = g.Key, impressionsCount = g.Count() };
                foreach (var x in query)
                {

                    if (x.MemberID != lastMemberID)
                    {
                        if (ignoreQuota)
                            members.Add(x.MemberID);
                        else
                        {
                            if (x.impressionsCount < quota)
                                members.Add(x.MemberID);
                        }
                    }

                }

                return members;

            }
            catch (Exception ex)
            { return null; }

        }

        private int GetImpressionsCount(List<MemberImpression> list, int memberid)
        {
            try
            {
                if (list == null)
                    return 0;

                int count = 0;
                foreach (MemberImpression mi in list)
                {
                    if (mi.MemberID == memberid)
                    {
                        count++;
                    }
                }

                return count;

            }
            catch (Exception ex)
            { return 0; }

        }

        public List<int> FilterOutDuplicates(List<int> searchResults, List<int> spotResults)
        {
            List<int> outList = null;
            try
            {
                if (searchResults == null)
                {
                    if (spotResults == null)
                    {
                        return null;
                    }
                    else
                    {
                        return spotResults.ToList<int>();
                    }
                }

                IEnumerable<int> query = from r in spotResults
                                         where (
                                         from l in searchResults
                                         select l).Contains<int>(r) == false
                                         select r;

                if (query != null)
                {
                    outList = query.ToList<int>();
                }

                return outList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int GetNextMemberID(List<int> results, List<int> promotionResults, out  bool promoMember)
        {
            promoMember = false;
            try
            {
                int id = 0;
                bool usePromo = false;

                if (promotionResults != null && promotionResults.Count > 0)
                {
                    usePromo = UsePromotionMember();
                }

                if (usePromo)
                {
                    id = scanList(promotionResults, _promotionImpressions, false);
                    if (id > 0)
                    {
                        _promotionImpressions.Add(new MemberImpression(id));
                        _promotionImpressions.Sort();
                        promoMember = usePromo;
                        _spotlightClientMetadata.PromoImpressions = _promotionImpressions;
                        _spotlightClientMetadata.LastSpotlightMember = id;
                        _CacheWebBucket.Insert(_spotlightClientMetadata);
                        return id;

                    }
                }

                //didn't get promoid
                usePromo = false;
                id = scanList(results, _spotlightImpressions, _ignoreQuota);
                if (id > 0)
                {
                    _spotlightImpressions.Add(new MemberImpression(id));
                    _spotlightImpressions.Sort();
                    _spotlightClientMetadata.SpotlightImpressions = _spotlightImpressions;
                    _spotlightClientMetadata.LastSpotlightMember = id;
                    _CacheWebBucket.Insert(_spotlightClientMetadata);
                }

                promoMember = usePromo;
                return id;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public int GetLastDisplayedSpotlightMember()
        {
            int memberid = 0;
            try
            {
                memberid = _spotlightClientMetadata.LastSpotlightMember;
                return memberid;
            }
            catch (Exception ex)
            {
                return memberid;
            }
        }

        public int GetSelfProfileImpression(bool useCachedSpotlightCheck)
        {
            int memberID = 0;
            bool showSelfProfile = false;

            if (RuntimeSettings.SettingExists("SPOTLIGHT_SELF_PROFILE", _brand.Site.Community.CommunityID, _brand.Site.SiteID))
            {
                showSelfProfile = Conversion.CBool(RuntimeSettings.GetSetting("SPOTLIGHT_SELF_PROFILE", _brand.Site.Community.CommunityID, _brand.Site.SiteID));
            }
            if (showSelfProfile && useCachedSpotlightCheck)
            {
                showSelfProfile = !_spotlightClientMetadata.SelfSpotlightedCheck;
            }
            if (showSelfProfile)
            {
                AccessPrivilege apSpotlight = _member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.SpotlightMember, _brand.BrandID, _brand.Site.SiteID, _brand.Site.Community.CommunityID);
                if (apSpotlight != null && apSpotlight.EndDatePST > DateTime.Now)
                {
                    _spotlightClientMetadata.LastSpotlightMember = _member.MemberID;
                    _spotlightClientMetadata.SelfSpotlightedCheck = true;
                    memberID = _member.MemberID;
                    _CacheWebBucket.Insert(_spotlightClientMetadata);
                }
            }
            
            return memberID;
        }

        public bool UsePromotionMember()
        {
            bool usePromo = false;
            int impressionsCount = 0;
            int impressionsPromoCount = 0;

            if (_spotlightImpressions != null)
                impressionsCount = _spotlightImpressions.Count;

            if (_promotionImpressions != null)
                impressionsPromoCount = _promotionImpressions.Count;

            int sumImpressions = impressionsCount + impressionsPromoCount;
            if (sumImpressions > 0)
            {
                int ratioMember = (impressionsCount * 100 / sumImpressions);
                if (ratioMember > _promotionRatio)
                    usePromo = true;
            }

            return usePromo;

        }

        private int scanList(List<int> results, List<MemberImpression> impressions, bool ignoreQuota)
        {
            int id = 0;
            bool valid = false;
            int count = 0;
            while (!valid && results.Count > 0)
            {
                id = getNextMemberID(results, impressions, ignoreQuota);
                count += 1;
                if (id <= 0)
                    break;
                valid = validateSpotlightMember(id);
                if (!valid)
                {
                    results.Remove(id);
                    id = 0;
                }

            }
            return id;
        }

        private int GetRandomMemberID(Matchnet.PremiumServiceSearch.ValueObjects.ResultList results)
        {
            try
            {
                int resCount = 0;

                if (results == null)
                    return 0;

                resCount = results.Results.Count;

                int seed = DateTime.Now.Minute * 100 + DateTime.Now.Second * 10;
                Random r = new Random(seed);

                int idx = r.Next(resCount - 1);
                int id = Conversion.CInt(results.Results[idx]);

                return id;
            }
            catch (Exception ex)
            { return 0; }

        }

        private int GetRandomMemberID(List<int> results)
        {
            try
            {
                int resCount = 0;

                if (results == null)
                    return 0;

                resCount = results.Count;

                int seed = DateTime.Now.Minute * 100 + DateTime.Now.Second * 10;
                Random r = new Random(seed);

                int idx = r.Next(resCount - 1);
                int id = Conversion.CInt(results[idx]);

                return id;
            }
            catch (Exception ex)
            { return 0; }

        }

        private int getNextMemberID(List<int> resultList, List<MemberImpression> impressionList, bool ignoreQuota)
        {
            int id = 0;
            try
            {
                List<int> members = null;
                members = GetNotImpressedMembers(impressionList, resultList);

                if (members != null && members.Count > 0)
                {
                    id = GetRandomMemberID(members);
                }
                else
                {
                    members = GetImpressedMembers(impressionList, resultList, _velocityQuota, true, ignoreQuota);
                    if (members == null || members.Count <= 0)
                    {
                        members = GetImpressedMembers(impressionList, resultList, _velocityQuota, false, ignoreQuota);
                    }

                    if (members != null && members.Count > 0)
                    {
                        id = GetRandomMemberID(members);
                    }
                }

                return id;
            }
            catch (Exception ex)
            { return id; }
        }

        private int getLastListMemberID(List<MemberImpression> list)
        {
            int id = 0;
            if (list != null && list.Count > 0)
            {
                id = list[0].MemberID;

            }
            return id;

        }

        private bool validateSpotlightMember(int memberid)
        {
            bool valid = false;
            Member.ServiceAdapters.Member member = null;
            try
            {
                List.ServiceAdapters.List list = List.ServiceAdapters.ListSA.Instance.GetList(_member.MemberID);
                member = MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                if (member == null)
                {
                    return valid;
                }
                if (!member.IsCommunityMember(_brand.Site.Community.CommunityID))
                {
                    return valid;
                }
                if (list.IsHotListed(List.ValueObjects.HotListCategory.IgnoreList, _brand.Site.Community.CommunityID, memberid))
                {
                    return valid;
                }
                int status = member.GetAttributeInt(_brand, "GlobalStatusMask");
                int selfSuspendedMember = member.GetAttributeInt(_brand, "SelfSuspendedFlag", 0);

                if (selfSuspendedMember > 0 || ((status & (int)GlobalStatusMask.AdminSuspended) == (int)GlobalStatusMask.AdminSuspended))
                {
                    return valid;
                }

                return true;
            }
            catch (Exception ex)
            { return valid; }
        }

    }
}
