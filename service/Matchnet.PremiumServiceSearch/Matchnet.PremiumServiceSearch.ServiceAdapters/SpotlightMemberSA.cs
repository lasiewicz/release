﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.Geo;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.QueueMessage;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.PremiumServiceSearch.ServiceAdapters
{
    public class SpotlightMemberSA:SABase
    {
        private Matchnet.ICaching _cache;
        private Matchnet.ICaching _cacheWebBucket;
        const string MODULE_NAME = "SpotlightMemberSA";
        const string DATA_FORMAT = "memberid:{0}, brandid:{1}";
        string testURI = "";
        public static readonly SpotlightMemberSA Instance = new SpotlightMemberSA();

        private SpotlightMemberSA()
        {
            _cache = Matchnet.Caching.Cache.Instance;   
            _cacheWebBucket = Spark.Caching.MembaseCaching.GetSingleton(RuntimeSettings.Instance.GetMembaseConfigByBucketFromSingleton("webcache"));
        }

        public ResultList Search(int brandid, int memberid, bool promotionFlag)
        {
            string functionName = "Search";
            string trace = String.Format(DATA_FORMAT, memberid, brandid);
            ResultList results = null;

            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                if(member != null)
                    results = Search(brand, member, promotionFlag);
                return results;

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, null, trace, true); return null; }
        }

        public List<int> Search(int brandid, int memberid, bool promotionFlag, int cap, int lastpage, int pagesize, out int page)
        {
            string functionName = "Search";
            string trace = "";
            List<int> listResults=null;
            ResultList results = null;
            page = 0;
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                if (member != null)
                    results = Search(brand, member, promotionFlag);

                if (results != null && results.Results != null )
                {
                    if (results.Results.Count >= cap)
                    {
                        int rescount = results.Results.Count;
                        int pagecount=0;
                        if (pagesize != 0)
                            pagecount = rescount / pagesize + (rescount % pagesize > 0 ? 1 : 0);

                        if (pagecount > lastpage + 1)
                        {
                            page = lastpage + 1;
                        }
                        else
                        {
                            page = 1;
                        }
                        int idx = (page - 1) * pagesize;
                        listResults = results.Results.GetRange(idx, pagesize);
                    }
                    else
                    {
                        listResults = results.Results;
                    }
                }
              
                return listResults;

            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, null, trace, true);
                return null;
            }
        }
        
        public ResultList Search(int brandid, Matchnet.Member.ServiceAdapters.Member member, bool promotionFlag)
        {
            ResultList results = null;
            string functionName = "Search";
            string trace = "";
            try
            {
                if (member != null)
                { 
                    trace = String.Format(DATA_FORMAT, member.MemberID, brandid);
                    trace += ",PromotionFlaf=" + promotionFlag.ToString();
                }
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                
                results = Search(brand, member, promotionFlag);
                return results;

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, null, trace, true);
            return null;
            }
        }

        public ResultList Search(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member, bool promotionFlag)
        {
            ResultList results = null;
            string functionName = "Search";
            string trace = "";
            Query query = null;
            try
            {
                if (member != null)
                {
                    trace = String.Format(DATA_FORMAT, member.MemberID, brand.BrandID);
                    trace += ",PromotionFlaf=" + promotionFlag.ToString();
                }
                query = GetQuery(brand, member, promotionFlag);
                if(query != null)
                    results = Search(query);
                return results;
            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, query, trace, true);
            return null;
            }
        }

        public ResultList SearchPromotions(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member)
        {
            ResultList results = null;
            string functionName = "Search";
            Query query = null;
            string trace = "";
            try
            {
                if (member != null)
                {
                    trace = String.Format(DATA_FORMAT, member.MemberID, brand.BrandID);
                    
                }
            query = GetQuery(brand, member,true);
            if (query != null)
            {
                query.Parameters.Add(new Parameter<bool>(SpotlightConstants.SearchParameters.promotionflag, true));
                results = Search(query);
            }
            return results;
            }
             catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, query, trace, true); return null; }

        }

        public ResultList Search(Query query)
        {
            if (query == null)
                return null;
            string cacheKey = query.GetCacheKeyString('_');
           // ArrayList results = null;
            ResultList members = (ResultList)_cache.Get(cacheKey);
            if (members == null)
            {
                string uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    members = getService(uri).Search(query);
                }
                finally
                {
                    base.Checkin(uri);
                }

                if (members != null && members.Results.Count > 0)
                {
                    _cache.Insert(members);
                }
            }
          
            return members;
        }


        public void LogImpression(int siteid, int pageid, ValueObjects.QueueMessage.ImpressionType viewtype, int viewedmemberid, int viewingmemberid, DateTime impressiontime)
        {string functionName = "LogImpression";
        try
        {
            string uri = getServiceManagerUri();
            base.Checkout(uri);
            try
            {
                getService(uri).LogImpression(siteid, pageid, viewtype, viewedmemberid, viewingmemberid, impressiontime);
            }
            finally
            {
                base.Checkin(uri);
            }
        }
        catch (Exception ex)
        { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, null, "", true); }
        }

        public void SaveMember(int brandid, int memberid, InstanceMember member, bool async)
        {  string functionName="SaveMember";
            string trace="";
            try
            {
                if (member == null)
                { throw (new Exception("Received null member object")); }
                trace=String.Format(DATA_FORMAT,memberid,brandid);
                
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                PremiumMember premiumMember = new PremiumMember(brandid, memberid, member);

                //no need to save expiration date since it now comes from UPS Access
                //Matchnet.Member.ServiceAdapters.Member m = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid,MemberLoadFlags.None);
                //m.SetAttributeDate(brand,"SpotlightExpirationDate", member.Get<DateTime>(Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate));
                //Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(m);

                string uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                     getService(uri).SaveMember(premiumMember, async);
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, member, trace, true); }
        }

        public InstanceMember GetMember(int brandid, int memberid,int serviceinstanceid)
        {
            PremiumMember member = null;
            InstanceMember instancemember = null;
            string functionName = "GetMember";
            string trace = "";
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                if (brand == null)
                    return null;
                trace = String.Format(DATA_FORMAT, memberid, brandid);
                member = (PremiumMember)_cache.Get(PremiumMember.GetCacheKeyString(brand.BrandID, memberid, serviceinstanceid));
                if (member == null)
                {
                    string uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        member=getService(uri).GetServiceMember(brandid,memberid,serviceinstanceid);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                
                }

                if (member != null)
                {
                    Matchnet.Member.ServiceAdapters.Member m = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid,MemberLoadFlags.None);
                    DateTime expDate = DateTime.MinValue;// Conversion.CDateTime(m.GetAttributeDate(brand, "SpotlightExpirationDate"));
                    Spark.Common.AccessService.AccessPrivilege privilege = m.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.SpotlightMember, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    if (privilege != null)
                    {
                        expDate = privilege.EndDatePST;
                    }

                    member.ServiceInstanceMember.Attributes.Add<DateTime>((int)PremiumServices.ValueObjects.ServiceConstants.Attributes.ExpirationDate, expDate);
                    _cache.Insert(member);
                    instancemember = member.ServiceInstanceMember;
                }

                return instancemember;
             
            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, member, trace, true); return null; }
        }

        public Query GetQuery(int brandid, int memberid, bool promotionFlag)
        {
            Query query = null;
            string functionName = "GetMember";
            string trace = "";
            try
            {
                trace = String.Format(DATA_FORMAT, memberid, brandid);
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                if (member != null && brand != null)
                    query = GetQuery(brand, member, promotionFlag);

                return query;

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, query, trace, true); return null; }
        }

        public Query GetQuery(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member, bool promotionFlag)
        {
            string functionName = "GetQuery";
            string trace = "";
            Query query = new Query(PremiumSearchType.SpotlightMember);
            try
            {
                ParameterCollection parameters=new ParameterCollection();

                
                Parameter<int> param1 = new Parameter<int>(SpotlightConstants.SearchParameters.regionid,member.GetAttributeInt(brand,"RegionID"));
                if (param1.Value < 0)
                    return null;

                
               parameters.Add(param1);
                

                Matchnet.Content.ValueObjects.Region.RegionLanguage region= Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(param1.Value, brand.Site.LanguageID);

                parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.depth1regionid,region.CountryRegionID));
                if (region.StateRegionID > 0)
                {
                    parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.depth2regionid, region.StateRegionID));
                    parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.depth3regionid, region.CityRegionID));
                   
                
                }
                else
                {
                    parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.depth2regionid, region.CityRegionID));
                   
                }

                parameters.Add(new Parameter<double>(SpotlightConstants.SearchParameters.longitude, (double)region.Longitude));
                parameters.Add(new Parameter<double>(SpotlightConstants.SearchParameters.latitude, (double)region.Latitude));

                BoxKey key =GeoUtil.GetBoxKey( (double)region.Longitude,(double)region.Latitude,0.1);

                parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.boxx,key.X));
                parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.boxy,key.Y));
                parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.brandid, brand.BrandID));
                parameters.Add(new Parameter<bool>(SpotlightConstants.SearchParameters.promotionflag, promotionFlag));
                DateTime bdate = member.GetAttributeDate(brand, "Birthdate");
                if (bdate == DateTime.MinValue)
                    return null;

                int age = Query.GetAge(bdate);
                
                parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.age,age));

                int gendermask=member.GetAttributeInt(brand,"GenderMask");
                if (gendermask < 0)
                    return null;
                parameters.Add(new Parameter<int>(SpotlightConstants.SearchParameters.gendermask, gendermask));

                query.Parameters = parameters;
                return query;

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogAdapterException(MODULE_NAME, functionName, ex, query, trace, true); return null; }

        }

        public int GetNextSpotlight(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member, System.Collections.Generic.List<int> searchIDs, Matchnet.PremiumServices.ValueObjects.PremiumServiceCollections services, int pageID, bool resetSpotlightClientMetadataCache, out bool promoMemberFlag)
        {
            int spotlightProfileMemberID = 0;
            promoMemberFlag = false;

            Matchnet.PremiumServices.ValueObjects.ServiceSite servicesite = services.ServiceSiteCollection.GetSiteService(brand.Site.SiteID, (int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember);
            if (servicesite == null)
            {
                return spotlightProfileMemberID;
            }

            int cap = servicesite.GetAttribute<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.VelocityListCap);
            int time = servicesite.GetAttribute<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.VelocityTime);

            SpotlightVelocityHandler velocityhandler = new SpotlightVelocityHandler(brand, member, cap, time, _cacheWebBucket, true, resetSpotlightClientMetadataCache);
            velocityhandler.PromotionRatio = servicesite.GetAttribute<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.PromotionMemberDisplayRatio);
            velocityhandler.VelocityQuota = servicesite.GetAttribute<int>((int)Matchnet.PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.VelocityQuota);

            //get next spotlight or promo
            Matchnet.PremiumServiceSearch.ValueObjects.ResultList spotlightMembers = this.Search(brand.BrandID, member, false);
            Matchnet.PremiumServiceSearch.ValueObjects.ResultList promotionMembers = this.Search(brand.BrandID, member, true);

            List<int> spotlightResults = null;
            if (spotlightMembers != null && spotlightMembers.Results.Count > 0)
            {
                spotlightResults = velocityhandler.FilterOutDuplicates(searchIDs, spotlightMembers.Results);
            }
            List<int> promotionResults = null;
            if (promotionMembers != null && promotionMembers.Results != null && promotionMembers.Results.Count > 0)
            {
                promotionResults = velocityhandler.FilterOutDuplicates(searchIDs, promotionMembers.Results);
            }

            spotlightProfileMemberID = velocityhandler.GetNextMemberID(spotlightResults, promotionResults, out promoMemberFlag);
            if (spotlightProfileMemberID > 0)
            {
                this.SpotlightLogImpression(brand, pageID, member.MemberID, spotlightProfileMemberID);
            }

            return spotlightProfileMemberID;
        }

        public int GetLastDisplayedSpotlight(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member, int pageID)
        {
            int spotlightProfileMemberID = 0;

            SpotlightVelocityHandler velocityhandler = new SpotlightVelocityHandler(brand, member, 0, 0, _cacheWebBucket, false, false);
            spotlightProfileMemberID = velocityhandler.GetLastDisplayedSpotlightMember();
            if (spotlightProfileMemberID > 0)
            {
                this.SpotlightLogImpression(brand, pageID, member.MemberID, spotlightProfileMemberID);
            }

            return spotlightProfileMemberID;
        }

        public int GetSelfProfileSpotlight(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Member.ServiceAdapters.Member member, bool useCachedSpotlightCheck)
        {
            int spotlightProfileMemberID = 0;

            SpotlightVelocityHandler velocityhandler = new SpotlightVelocityHandler(brand, member, 0, 0, _cacheWebBucket, false, false);
            spotlightProfileMemberID = velocityhandler.GetSelfProfileImpression(useCachedSpotlightCheck);

            return spotlightProfileMemberID;
        }

        public void SpotlightLogImpression(Matchnet.Content.ValueObjects.BrandConfig.Brand brand, int pageId, int viewerMemberId, int viewedMemberId)
        {
            bool log = false;
            try
            {

                log = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_LOG_FLAG"), false);
                if (!log || viewerMemberId <= 0 || viewedMemberId <= 0)
                    return;

                int siteid = brand.Site.SiteID;
                ImpressionType impressiontype = ImpressionType.spotlight;
                this.LogImpression(siteid, pageId, impressiontype, viewedMemberId, viewerMemberId, DateTime.Now);

            }
            catch (Exception ex)
            { }
        }
        
        #region SABase implementation
        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESEARCHSVC_SA_CONNECTION_LIMIT"));
        }
        #endregion SABase implementation


       #region service proxy

		private ISpotlightMember getService(string uri)
        {
            string useURI = "";
			try
			{
				return (ISpotlightMember)Activator.GetObject(typeof(ISpotlightMember), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
        {
            string uri = "";
			try
			{
                if (!String.IsNullOrEmpty(testURI))
                    uri = testURI;
                else
                {
                    uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_SPOTLIGHT_MANAGER_NAME);
                }
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}


        public String TestURI
        {
            get { return testURI; }
            set { testURI = value; }
        }
		#endregion
	}

       
    }
