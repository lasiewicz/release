﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.BaseClasses;
using Matchnet.RemotingClient;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.PremiumServiceSearch.ValueObjects.ServiceDefinitions;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Matchnet.PremiumServiceSearch.ValueObjects;
using Matchnet.PremiumServiceSearch.ValueObjects.CachingMappings;

#endregion

namespace Matchnet.PremiumServiceSearch.ServiceAdapters
{
    public class QuestionAnswerSA : SABase, IQuestionAnswerAdapter
    {
        private readonly Cache _cache;
        const string ModuleName = "QuestionAnswerSA";
        /// <summary>
        /// 
        /// </summary>
        public static readonly QuestionAnswerSA Instance = new QuestionAnswerSA();

        private QuestionAnswerSA()
        {
            _cache = Cache.Instance;
        }

        #region Public Methods
        public ActiveQuestionIDList GetActiveQuestionIDs(int siteID)
        {
            string functionName = "GetActiveQuestionIDs";
            try
            {
                //Check Cache
                //Note: 20120319 TL - Tried adding to cache but the way this object is used in web causes memory issues, so will not try to cache which is how it was before
                ActiveQuestionIDList activeQuestionIDList = null;// _cache.Get(ActiveQuestionIDList.GetCacheKeyString(siteID)) as ActiveQuestionIDList;

                if (activeQuestionIDList == null)
                {
                    string uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        activeQuestionIDList = getService(uri).GetActiveQuestionIDs(siteID);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return activeQuestionIDList;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }

            return null;
        }


        public List<StockAnswer> GetStockAnswers(List<int> questionIds)
        {
            return CacheHelper.GetValuesByKeys(questionIds,
                keys =>
                {
                    var stockAnswers = GetStockAnswersFromRemoteService(keys);
                    var ret = new List <ItemMapping<int,StockAnswer>>();
                    foreach (var stockAnswer in stockAnswers)
                        ret.Add(new ItemMapping<int, StockAnswer>(stockAnswer.QuestionID, stockAnswer));
                    return ret;
                },
            (key, value) => new Question2StockAnswerMapping(key, value),
            Question2StockAnswerMapping.GetCacheKey);

        }

        List<StockAnswer> GetStockAnswersFromRemoteService(List<int> questionIds)
        {
            string functionName = "GetStockAnswers";
            try
            {
                List<StockAnswer> stockAnswersList = null;

        
                if (stockAnswersList == null)
                {
                    string uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        stockAnswersList = getService(uri).GetStockAnswers(questionIds);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return stockAnswersList;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }

            return null;
        }

        public Question GetQuestion(int questionID)
        {
            string functionName = "GetQuestion";
            try
            {
                if (questionID <= 0)
                    return null;

                //check cache
                Question question = _cache.Get(Question.GetCacheKeyString(questionID)) as Question;

                if (question == null)
                {
                    lock (GetServiceVOLock(Question.GetCacheKeyString(questionID)))
                    {
                        question = _cache.Get(Question.GetCacheKeyString(questionID)) as Question;

                        if (question == null)
                        {
                            string uri = getServiceManagerUri();
                            base.Checkout(uri);
                            try
                            {
                                question = getService(uri).GetQuestion(questionID);

                                //update cache
                                if (question != null)
                                {
                                    question.CacheTTLSeconds = GetQuestionAnswerWebCacheTTL(question.SiteID);
                                    _cache.Insert(question);
                                }
                            }
                            finally
                            {
                                base.Checkin(uri);
                            }
                        }
                    }
                }

                return question;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }

            return null;
        }

        public PagedQuestion GetQuestionPaged(int questionID, int pageNumber, int pageSize, bool latestFirst)
        {
            string functionName = "GetQuestionPaged";
            try
            {
                if (questionID <= 0)
                    return null;

                //check cache
                PagedQuestion question = _cache.Get(PagedQuestion.GetCacheKeyString(questionID,pageNumber,pageSize,latestFirst)) as PagedQuestion;

                if (question == null)
                {
                    lock (GetServiceVOLock(PagedQuestion.GetCacheKeyString(questionID, pageNumber, pageSize, latestFirst)))
                    {
                        question = _cache.Get(PagedQuestion.GetCacheKeyString(questionID, pageNumber, pageSize, latestFirst)) as PagedQuestion;

                        if (question == null)
                        {
                            string uri = getServiceManagerUri();
                            base.Checkout(uri);
                            try
                            {
                                question = getService(uri).GetQuestionPaged(questionID, pageNumber, pageSize, latestFirst);

                                //update cache
                                if (question != null)
                                {
                                    question.CacheTTLSeconds = GetQuestionAnswerWebCacheTTL(question.SiteID);
                                    _cache.Insert(question);
                                }
                            }
                            finally
                            {
                                base.Checkin(uri);
                            }
                        }
                    }
                }

                return question;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }

            return null;            
        }


        public MemberQuestionList GetMemberQuestions(int memberID, int siteID)
        {
            string functionName = "GetMemberQuestions";
            try
            {
                //check cache
                MemberQuestionList memberQuestionList = _cache.Get(MemberQuestionList.GetCacheKeyString(siteID, memberID)) as MemberQuestionList;

                if (memberQuestionList == null)
                {
                    string uri = getServiceManagerUri(new PartitionParams(Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID));
                    base.Checkout(uri);
                    try
                    {
                        memberQuestionList = getService(uri).GetMemberQuestions(memberID, siteID);

                        //update cache
                        if (memberQuestionList != null)
                        {
                            memberQuestionList.CacheTTLSeconds = GetQuestionAnswerWebCacheTTL(siteID);
                            _cache.Insert(memberQuestionList);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return memberQuestionList;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }

            return null;
        }

        public void AddAnswer(int memberID, int siteID, int questionID, string answerValue)
        {
            string functionName = "AddAnswer";
            try
            {
                string uri = getServiceManagerUri(new PartitionParams(Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID));
                base.Checkout(uri);
                try
                {
                    getService(uri).AddAnswer(memberID, siteID, questionID, answerValue);

                    //remove local cache so it'll be refreshed from mt
                    _cache.Remove(Question.GetCacheKeyString(questionID));
                    _cache.Remove(MemberQuestionList.GetCacheKeyString(siteID, memberID));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }
        }

        public void RemoveMemberAnswer(int questionID, int siteID, int memberID)
        {
            string functionName = "RemoveMemberAnswer";
            try
            {
                string uri = getServiceManagerUri(new PartitionParams(Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID));
                base.Checkout(uri);
                try
                {
                    getService(uri).RemoveMemberAnswer(questionID, siteID, memberID);

                    //remove local cache so it'll be refreshed from mt
                    _cache.Remove(Question.GetCacheKeyString(questionID));
                    _cache.Remove(MemberQuestionList.GetCacheKeyString(siteID, memberID));

                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }
            
        }

        public void RemoveAnswer(int answerID, int questionID, int siteID, int memberID)
        {
            string functionName = "RemoveAnswer";
            try
            {
                string uri = getServiceManagerUri(new PartitionParams(Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID));
                base.Checkout(uri);
                try
                {
                    getService(uri).RemoveAnswer(answerID, questionID, siteID, memberID);

                    //remove local cache so it'll be refreshed from mt
                    _cache.Remove(Question.GetCacheKeyString(questionID));
                    _cache.Remove(MemberQuestionList.GetCacheKeyString(siteID, memberID));

                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }
        }

        public void UpdateAnswer(Answer answer, int siteID)
        {
            string functionName = "UpdateAnswer";
            try
            {
                string uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).UpdateAnswer(answer, siteID);

                    //remove local cache so it'll be refreshed from mt
                    _cache.Remove(Question.GetCacheKeyString(answer.QuestionID));
                    _cache.Remove(MemberQuestionList.GetCacheKeyString(siteID, answer.MemberID));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }
        }

        public void AdminUpdateAnswer(Answer answer, int siteID)
        {
            string functionName = "AdminUpdateAnswer";
            try
            {
                string uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).AdminUpdateAnswer(answer, siteID);

                    //remove local cache so it'll be refreshed from mt
                    _cache.Remove(Question.GetCacheKeyString(answer.QuestionID));
                    _cache.Remove(MemberQuestionList.GetCacheKeyString(siteID, answer.MemberID));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }
        }
        public QuestionList GetActiveQuestionList(int siteID)
        {
            string functionName = "GetActiveQuestionList";
            try
            {
                //TODO - Caching
                //check cache
                QuestionList questionList = _cache.Get(QuestionList.GetCacheKeyString(siteID)) as QuestionList;

                if (questionList == null)
                {
                    string uri = getServiceManagerUri();
                    base.Checkout(uri);
                    try
                    {
                        questionList = getService(uri).GetActiveQuestionList(siteID);
                        if (questionList != null)
                        {
                            questionList.CacheTTLSeconds = GetQuestionAnswerWebCacheTTL(siteID);
                            _cache.Insert(questionList);
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                return questionList;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
                return null;
            }


        }

        public PagedResult<QuestionResponse> GetActiveQuestionListPaged(int siteID, int pageNumber, int pageSize, QuestionListSort sort)
        {
            var uri = getServiceManagerUri();
            base.Checkout(uri);
            try
            {
                var suffix = (sort == QuestionListSort.recent) ? "recent" : "az";
                var cachePrefix = "ActiveQuestions_" + suffix;
                var key = PagedQuestionResponseListWrapper.GetCacheKey(cachePrefix, siteID, pageNumber, pageSize);
                var cachedQuestions = _cache.Get(key) as CacheableWrapper<PagedResult<QuestionResponse>>;
                if (cachedQuestions != null)
                {
                    return cachedQuestions.Data;
                }
                var ret = getService(uri).GetActiveQuestionListPaged(siteID, pageNumber, pageSize, sort);
                _cache.Add(new PagedQuestionResponseListWrapper(ret, cachePrefix, siteID, pageNumber, pageSize));
                return ret;
            }
            finally
            {
                base.Checkin(uri);
            }
        }

        public List<Question> GetActiveQuestionList(int siteID, QuestionListSort sort)
        {
            string functionName = "GetActiveQuestionList";
            try
            {
                QuestionList questionList = GetActiveQuestionList(siteID);
                if (sort == QuestionListSort.az)
                    return questionList.QuestionsAZ;
                else
                    return questionList.Questions;
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
            }

            return null;
        }

        public List<Question> GetActiveQuestionList(int siteID, int pagesize, int startrow, QuestionListSort sort, out int total)
        {
            string functionName = "GetActiveQuestionList";
            total = 0;
            try
            {
                List<Question> list = GetActiveQuestionList(siteID, sort);

                if (list == null)
                    return null;


                int beginindx = startrow > list.Count - 1 ? list.Count - 1 : startrow;
                int count = startrow + pagesize > list.Count - 1 ? list.Count - startrow - 1 : pagesize;
                total = list.Count;
                return list.GetRange(beginindx, count);



            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogAdapterException(ModuleName, functionName, ex, null, "", true);
                return null;
            }


        }

        /// <summary>
        ///     This is the interface method but was never implemented.
        ///     Instead the other GetRecentAnswerList method was implemented and is used.
        ///     This method will obviously break when Unit Testing!
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="daysback"></param>
        /// <returns></returns>
        public AnswerList GetRecentAnswerList(int siteID, int daysback)
        {
            return GetRecentAnswerListImpl(siteID, daysback, false);
        }

        public PagedResult<Question> GetRecentQuestionListPaged(int siteID, int daysback, int pageNumber, int pageSize)
        {
            var cachePrefix = "RecentQuestionsAnswers";
            var key = PagedQuestionListWrapper.GetCacheKey(cachePrefix, siteID, daysback, pageNumber, pageSize);
            var cachedQuestions = _cache.Get(key) as CacheableWrapper<PagedResult<Question>>;
            if (cachedQuestions != null)
            {
                return cachedQuestions.Data;
            }
            var ret = GetRecentQuestionListPagedFromService(siteID, daysback, pageNumber, pageSize);
            _cache.Add(new PagedQuestionListWrapper(ret, cachePrefix, siteID, daysback, pageNumber, pageSize));
            return ret;
        }

        public PagedResult<Question> GetRecentQuestionListPagedFromService(int siteID, int daysback, int pageNumber, int pageSize)
        {
            var cachePrefix = "RecentQuestionsAnswers";
            var key = PagedQuestionListWrapper.GetCacheKey(cachePrefix, siteID, daysback, pageNumber, pageSize);
            lock (GetServiceVOLock(key))
            {
                string uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetRecentQuestionListPaged(siteID, daysback, pageNumber, pageSize);

                }
                finally
                {
                    base.Checkin(uri);
                }
            }
        }

        /// <summary>
        ///     Why was this used instead of the interface method..?
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="daysback"></param>
        /// <param name="notUsed"></param>
        /// <returns></returns>
        public List<Answer> GetRecentAnswerList(int siteID, int daysback, bool notUsed = false)
        {
            var answerList = GetRecentAnswerListImpl(siteID, daysback, notUsed);

            if (answerList != null)
                return answerList.Answers;
            else
                return null;
        }
        
        AnswerList GetRecentAnswerListImpl(int siteID, int daysback, bool notUsed = false)
        {
            string functionName = "GetRecentAnswerList";
            //check cache
            //i added extra  cache key var becuase what if we decide to cache by days back also
            AnswerList answerList = _cache.Get(AnswerList.GetCacheKeyString("", siteID)) as AnswerList;

            if (answerList == null)
            {
                lock (GetServiceVOLock(AnswerList.GetCacheKeyString("", siteID)))
                {
                    answerList = _cache.Get(AnswerList.GetCacheKeyString("", siteID)) as AnswerList;
                    if (answerList == null)
                    {
                        string uri = getServiceManagerUri();
                        base.Checkout(uri);
                        try
                        {
                            answerList = getService(uri).GetRecentAnswerList(siteID, daysback);
                            if (answerList != null)
                            {
                                answerList.CacheTTLSeconds = GetQuestionAnswerWebCacheTTL(siteID);
                                _cache.Insert(answerList);
                            }
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }
                    }
                }
            }
            return answerList;
        }




        #endregion

        #region Private Methods
        private int GetQuestionAnswerWebCacheTTL(int siteID)
        {
            int cacheSeconds = Constants.NULL_INT;
            try
            {
                cacheSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("QUESTION_ANSWER_WEB_CACHE_TTL", Constants.NULL_INT, siteID, Constants.NULL_INT));
            }
            catch (Exception ex)
            {
                //setting missing
                cacheSeconds = Constants.NULL_INT;
            }

            if (cacheSeconds <= 0)
            {
                Question q = new Question();
                cacheSeconds = q.CacheTTLSeconds;
            }
            return cacheSeconds;

        }

        #endregion

        #region SABase implementation
        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESEARCHSVC_SA_CONNECTION_LIMIT"));
        }
        #endregion SABase implementation

        #region service proxy

        private IQuestionAnswer getService(string uri)
        {
            try
            {
                return (IQuestionAnswer)Activator.GetObject(typeof(IQuestionAnswer), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri()
        {
            return getServiceManagerUri(new PartitionParams(Matchnet.Configuration.ServiceAdapters.PartitionMode.Random, 0));
        }

        private string getServiceManagerUri(PartitionParams partitionParams)
        {
            string uri = "";
            try
            {
                if (partitionParams.Guid > 0)
                {
                    uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_CONSTANT, partitionParams.Mode, partitionParams.Guid).ToUri(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_QUESTIONANSWER_MANAGER_NAME);
                }
                else
                {
                    uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_CONSTANT, partitionParams.Mode).ToUri(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_QUESTIONANSWER_MANAGER_NAME);
                }

                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(PremiumServiceSearch.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        #endregion
    }

    class PartitionParams
    {
        public Matchnet.Configuration.ServiceAdapters.PartitionMode Mode { get; set; }
        public int Guid { get; set; }

        public PartitionParams(Matchnet.Configuration.ServiceAdapters.PartitionMode mode, int guid)
        {
            this.Mode = mode;
            this.Guid = guid;
        }
    }
}
