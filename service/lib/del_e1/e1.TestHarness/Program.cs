using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.e1.Engine.TestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            EvalPrint ep = new EvalPrint();
            //ep.DoCommand(@"load file=C:\Matchnet\Bedrock\Service\lib\e1\e1.Engine\Data\jd75k.csv");
            //ep.DoSearch();

            ep.DoCommand("time");

            foreach (string arg in args)
            {
                ep.DoCommandFile(arg);
            }

            ep.DoCommand("time mode=1");

            Int32 result = 0;

            while (result >= 0)
            {
                string command = Console.ReadLine();
                result = ep.DoCommand(command);
            }
        }
    }
}
