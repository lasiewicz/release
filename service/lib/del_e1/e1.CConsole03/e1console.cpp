// This is the main project file for VC++ application project 
// generated using an Application Wizard.


#ifdef GCC

extern "C" { 
#include "papi.h"
extern main(int argc, char **argv);
}
int _tmain(int argc, char **argv)
{
	return main(argc, argv);
}

#else

#include "stdafx.h"
#include "e1cpp.h"
#using <mscorlib.dll>
using namespace e1;

int _tmain(int argc, char **argv)
{
	E1Gsv e1("Console");
	int err = e1.main(argc, argv);
	return err;
}
#endif
