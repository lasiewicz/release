#include "stdafx.h"
#include "Result.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

Result::Result(S1Result result)
{
	_result = result;
}

Result::~Result()
{
	S1ResultFree(_result);
}