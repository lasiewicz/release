#ifndef INCLUDE_PAPI
	#include "papi.h"
#endif

using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				//Wrapper for unmanaged c string
				[Serializable]
				public ref class StringWrapper
				{
				private:
					IntPtr ip;
				public:
					StringWrapper(String^ string);
					~StringWrapper();
					property STR NativeSTR
					{
						STR get();
					}
				};

				//Creates a managed string from an unmanaged string
				public ref class StringConverter
				{
				public:
					static String^ STRToString(STR str)
					{
						return System::Runtime::InteropServices::Marshal::PtrToStringAnsi(static_cast<IntPtr>(str));
					}
					static StringWrapper^ GetE1DateString(DateTime dateTime)
					{
						return gcnew StringWrapper(dateTime.ToString("yyyy-MM-dd"));
					}
				};
			};
		};
	};
};