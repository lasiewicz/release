#include "Location.h"
#include "XML.h"
#include "stdio.h"

using namespace System::Collections;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class Load
				{
				public:
					static void LoadFile(String^ fileName, String^ header, int mode, XML^ xml);
				};
			};
		};
	};
};