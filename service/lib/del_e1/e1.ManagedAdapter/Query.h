#include "Location.h"
#include "Scores.h"
#include "NameValue.h"

using namespace System;
using namespace Matchnet::Search::Interfaces;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				[Serializable]
				public ref class Query
				{
				private:
					S1Query _query;
					Int32 _parameterCount;
					Location^ _searchLocation;
					Scores^ _scores;
				public:
					Query();
					~Query();
					property QuerySorting SortKey
					{
						QuerySorting get();
						void set(QuerySorting);
					}
					property Int32 MaxResults
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 CommunityID
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 GenderMask
					{
						Int32 get();
						void set(Int32);
					}
					property Location^ SearchLocation
					{
						Location^ get();
						void set(Location^);
					}
					property Int16 Radius
					{
						short get();
						void set(Int16);
					}
					property QueryHasPhoto HasPhoto
					{
						QueryHasPhoto get();
						void set(QueryHasPhoto);
					}
					property QueryIsOnline IsOnline
					{
						QueryIsOnline get();
						void set(QueryIsOnline);
					}
					property Int16 AgeMinDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 AgeMaxDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 HeightMinDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 HeightMaxDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 WeightMinDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Int16 WeightMaxDesired
					{
						Int16 get();
						void set(Int16);
					}
					property Scores^ Blend
					{
						Scores^ get();
						void set(Scores^);
					}
					void AddParameter(NameValue^);
					property S1Query NativeQuery
					{
						S1Query get();
					}
				};
			};
		};
	};
};