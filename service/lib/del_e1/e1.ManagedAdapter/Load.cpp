#include "StdAfx.h"
#include "Load.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

void Load::LoadFile(String^ fileName, String^ header, int mode, XML^ xml)
{
	StringWrapper^ swFileName = gcnew StringWrapper(fileName);
	StringWrapper^ swHeader = gcnew StringWrapper(header);

	FILE* file = NULL;//fopen(swFileName->NativeSTR, "r");
	
	LoadFileAux(swFileName->NativeSTR, file, xml->NativeE1Xml->pxObj, mode, swHeader->NativeSTR);

	//fclose(file);
}
