#include "stdafx.h"

using namespace System;
using namespace Matchnet::e1::Engine::ManagedAdapter;

StringWrapper::StringWrapper(String^ string)
{
	ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(string);
}

StringWrapper::~StringWrapper()
{
	System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);
}

STR StringWrapper::NativeSTR::get()
{
	return static_cast<char*>(ip.ToPointer());
}

