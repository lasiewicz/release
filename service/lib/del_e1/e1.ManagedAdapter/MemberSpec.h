#include "Location.h"
#include "NameValue.h"

using namespace System::Collections;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class MemberSpec
				{
				private:
					MemSpec _memSpec;
					Int32 _tagCount;
					Location^ _location;
					StringWrapper^ _birthDate;
					StringWrapper^ _registerDate;
					StringWrapper^ _activeDate;
					ArrayList^ _tags;
				public:
					MemberSpec();
					~MemberSpec();
					property Int32 MemberID
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 CommunityID
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 GenderMask
					{
						Int32 get();
						void set(Int32);
					}
					property Location^ MemberLocation
					{
						Location^ get();
						//void set(Location^);
					}
					property DateTime BirthDate
					{
						void set(DateTime);
					}
					property DateTime RegisterDate
					{
						void set(DateTime);
					}
					property DateTime ActiveDate
					{
						void set(DateTime);
					}
					property Int32 Height
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 Weight
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 ChildrenCount
					{
						Int32 get();
						void set(Int32);
					}
					property bool HasPhoto
					{
						bool get();
						void set(bool);
					}
					property bool IsOnline 
					{
						bool get();
						void set(bool);
					}
					property Int32 RelocateFlag
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 Popularity
					{
						Int32 get();
						void set(Int32);
					}
					property Int32 Activity
					{
						Int32 get();
						void set(Int32);
					}
					//void AddTag(NameValue^ nameValue);
					void AddTag(String^ name, String^ value);
					void Load();
				};
			};
		};
	};
};