#include "e1cpp.h"

using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class XML
				{
				private:
					E1Xml* _e1Xml;
				public:
					XML();
					XML(S4 dataMax);
					Int32 CommandEval(String^ command);
					Int32 DoCommandFile(String^ fileName);
					property String^ CommandResult
					{
						String^ get();
					}
					property E1Xml* NativeE1Xml
					{
						E1Xml* get();
					}
					void Reset();
				};

				public ref class GSV
				{
				private:
					E1Gsv *e1Gsv;
				public:
					GSV();
				};
			}
		}
	}
}