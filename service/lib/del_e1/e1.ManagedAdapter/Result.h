using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				public ref class Result
				{
				private:
					S1Result _result;
				public:
					Result(S1Result result);
					~Result();

					property Int16 Count
					{
						Int16 get()
						{
							return _result->memNum;
						}
					}

					Int32 GetResult(Int32 index)
					{
						return _result->memids[index];
					}
				};
			};
		};
	};
};