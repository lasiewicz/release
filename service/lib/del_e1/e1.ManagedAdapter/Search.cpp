#include "stdafx.h"
#include "Search.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

Result^ Search::DoSearch(Query ^query)
{
	return gcnew Result(S1ResultNew(query->NativeQuery));
}