#include "StdAfx.h"
#include "MemberSpec.h"

using namespace System::Collections;

using namespace Matchnet::e1::Engine::ManagedAdapter;

MemberSpec::MemberSpec()
{
	_memSpec = new MemSpecRec();
	_tags = gcnew ArrayList();
	_tagCount = 0;
}

MemberSpec::~MemberSpec()
{
	delete _memSpec;
	delete _location;
	delete _birthDate;
	delete _registerDate;
	delete _activeDate;

	for (Int32 i = 0; i < _tags->Count; i++)
	{
		delete _tags[i];
	}
	delete _tags;
}

Int32 MemberSpec::MemberID::get()
{
	return _memSpec->memId;
}
void MemberSpec::MemberID::set(Int32 value)
{
	_memSpec->memId = value;
}

Int32 MemberSpec::CommunityID::get()
{
	return _memSpec->domId;
}
void MemberSpec::CommunityID::set(Int32 value)
{
	_memSpec->domId = value;
}

Int32 MemberSpec::GenderMask::get()
{
	return _memSpec->genderMask;
}
void MemberSpec::GenderMask::set(Int32 value)
{
	_memSpec->genderMask = value;
}

Location^ MemberSpec::MemberLocation::get()
{
	if (!_location)
	{
		_location = gcnew Location(&(_memSpec->memLoc));
	}

	return _location;
}
/*void MemberSpec::MemberLocation::set(Location^ value)
{
	_location = value;
	_location->NativeLoc = &(_memSpec->memLoc);
}*/

void MemberSpec::BirthDate::set(DateTime value)
{
	_birthDate = StringConverter::GetE1DateString(value);
	_memSpec->birthDate = _birthDate->NativeSTR;
}

void MemberSpec::RegisterDate::set(DateTime value)
{
	_registerDate = StringConverter::GetE1DateString(value);
	_memSpec->registerDate = _registerDate->NativeSTR;
}

void MemberSpec::ActiveDate::set(DateTime value)
{
	_activeDate = StringConverter::GetE1DateString(value);
	_memSpec->activeDate = _activeDate->NativeSTR;
}

Int32 MemberSpec::Height::get()
{
	return _memSpec->height;
}
void MemberSpec::Height::set(Int32 value)
{
	_memSpec->height = value;
}

Int32 MemberSpec::Weight::get()
{
	return _memSpec->weight;
}
void MemberSpec::Weight::set(Int32 value)
{
	_memSpec->weight = value;
}

Int32 MemberSpec::ChildrenCount::get()
{
	return _memSpec->childrenCount;
}
void MemberSpec::ChildrenCount::set(Int32 value)
{
	_memSpec->childrenCount = value;
}

bool MemberSpec::HasPhoto::get()
{
	return (_memSpec->hasPhoto == 1);
}
void MemberSpec::HasPhoto::set(bool value)
{
	_memSpec->hasPhoto = value ? 1 : 0;
}

bool MemberSpec::IsOnline::get()
{
	return (_memSpec->isOnline == 1);
}
void MemberSpec::IsOnline::set(bool value)
{
	_memSpec->isOnline = value ? 1 : 0;
}

Int32 MemberSpec::RelocateFlag::get()
{
	return _memSpec->relocateFlag;
}
void MemberSpec::RelocateFlag::set(Int32 value)
{
	_memSpec->relocateFlag = value;
}

Int32 MemberSpec::Popularity::get()
{
	return _memSpec->saPop;
}
void MemberSpec::Popularity::set(Int32 value)
{
	_memSpec->saPop = value;
}

Int32 MemberSpec::Activity::get()
{
	return _memSpec->saAct;
}
void MemberSpec::Activity::set(Int32 value)
{
	_memSpec->saAct = value;
}

/*void MemberSpec::AddTag(NameValue ^nameValue)
{
	_memSpec->tags[_tagCount] = *(nameValue->NativeNameValue);
	_tagCount++;
}*/

void MemberSpec::AddTag(String^ name, String^ value)
{
	NameValue^ nv = gcnew NameValue(name, value);
	_memSpec->tags[_tagCount] = *(nv->NativeNameValue);
	_tagCount++;
	_tags->Add(nv);
}

void MemberSpec::Load()
{
	MemCreateOrUpdate(_memSpec);
}