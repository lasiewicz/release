using namespace System;

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			namespace ManagedAdapter
			{
				[Serializable]
				public ref class Location
				{
				private:
					MemLoc _memLoc;
					StringWrapper^ _country;
					StringWrapper^ _state;
					StringWrapper^ _city;
					StringWrapper^ _zip;
					StringWrapper^ _areaCode; 
				public:
					Location();
					Location(MemLoc memLoc);
					~Location();

					property float Latitude
					{
						float get();
						void set(float);
					}
					property float Longitude
					{
						float get();
						void set(float);
					}
					property String^ Country
					{
						String^ get();
						void set(String^);
					}
					property String^ State
					{
						String^ get();
						void set(String^);
					}
					property String^ City
					{
						String^ get();
						void set(String^);
					}
					property String^ Zip
					{
						String^ get();
						void set(String^);
					}
					property String^ AreaCode
					{
						String^ get();
						void set(String^);
					}
					property MemLoc NativeLoc
					{
						MemLoc get();
					}
				};
			};
		};
	};
};