// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#if !defined ( INCLUDE_PAPI )
extern "C"
{
	#include "papi.h"
}
#endif

#include "StringConverter.h"