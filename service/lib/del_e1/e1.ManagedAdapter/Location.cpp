#include "stdafx.h"
#include "Location.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;

Location::Location()
{
	_memLoc = new MemLocRec();
}

Location::Location(MemLoc memLoc)
{
	_memLoc = memLoc;
}

Location::~Location()
{
	delete _memLoc;
}

float Location::Latitude::get()
{
	return _memLoc->regLat;
}

void Location::Latitude::set(float value)
{
	_memLoc->regLat = value;
}

float Location::Longitude::get()
{
	return _memLoc->regLng;
}

void Location::Longitude::set(float value)
{
	_memLoc->regLng = value;
}

String^ Location::Country::get()
{
	return StringConverter::STRToString(_memLoc->regCountry);
}

void Location::Country::set(String^ value)
{
	_country = gcnew StringWrapper(value);
	_memLoc->regCountry = _country->NativeSTR;
}

String^ Location::State::get()
{
	return StringConverter::STRToString(_memLoc->regState);
}

void Location::State::set(String^ value)
{
	_state = gcnew StringWrapper(value);
	_memLoc->regState = _state->NativeSTR;
}

String^ Location::City::get()
{
	return StringConverter::STRToString(_memLoc->regCity);
}

void Location::City::set(String^ value)
{
	_city = gcnew StringWrapper(value);
	_memLoc->regCity = _city->NativeSTR;
}

String^ Location::Zip::get()
{
	return StringConverter::STRToString(_memLoc->regZip);
}

void Location::Zip::set(String^ value)
{
	_zip = gcnew StringWrapper(value);
	_memLoc->regZip = _zip->NativeSTR;
}

String^ Location::AreaCode::get()
{
	return StringConverter::STRToString(_memLoc->regAreaCode);
}

void Location::AreaCode::set(String^ value)
{
	_areaCode = gcnew StringWrapper(value);
	_memLoc->regAreaCode = _areaCode->NativeSTR;
}

MemLoc Location::NativeLoc::get()
{
	return _memLoc;
}