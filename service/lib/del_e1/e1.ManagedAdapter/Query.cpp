#include "stdafx.h"
#include "Query.h"

using namespace Matchnet::e1::Engine::ManagedAdapter;
using namespace System;
using namespace System::Runtime::InteropServices;

Query::Query()
{
	_query = new S1QueryRec();
	_parameterCount = 0;
}

Query::~Query()
{
	delete _query;
}

QuerySorting Query::SortKey::get()
{
	return (QuerySorting) _query->sortKey;
}
void Query::SortKey::set(QuerySorting value)
{
	_query->sortKey = (S1SortKey) value;
}

Int32 Query::MaxResults::get()
{
	return _query->domId;
}
void Query::MaxResults::set(Int32 value)
{
	_query->domId = value;
}

Int32 Query::CommunityID::get()
{
	return _query->domId;
}
void Query::CommunityID::set(Int32 value)
{
	_query->domId = value;
}

Int32 Query::GenderMask::get()
{
	return _query->genderMask;
}
void Query::GenderMask::set(Int32 value)
{
	_query->genderMask = value;
}

Location^ Query::SearchLocation::get()
{
	return _searchLocation;
}
void Query::SearchLocation::set(Location^ location)
{
	_searchLocation = location;
	_query->locKey = *_searchLocation->NativeLoc;
}

Int16 Query::Radius::get()
{
	return _query->locDst;
}
void Query::Radius::set(Int16 value)
{
	_query->locDst = value;
}

QueryHasPhoto Query::HasPhoto::get()
{
	return (QueryHasPhoto) _query->hasPhoto;
}
void Query::HasPhoto::set(QueryHasPhoto value)
{
	_query->hasPhoto = (S1HasPhoto) value;
}

QueryIsOnline Query::IsOnline::get()
{
	return (QueryIsOnline) _query->isOnline;
}
void Query::IsOnline::set(QueryIsOnline value)
{
	_query->isOnline = (S1IsOnline) value;
}

Int16 Query::AgeMinDesired::get()
{
	return _query->ageMinDesired;
}
void Query::AgeMinDesired::set(Int16 value)
{
	_query->ageMinDesired = value;
}

Int16 Query::AgeMaxDesired::get()
{
	return _query->ageMaxDesired;
}
void Query::AgeMaxDesired::set(Int16 value)
{
	_query->ageMaxDesired = value;
}

Int16 Query::HeightMinDesired::get()
{
	return _query->hgtMinDesired;
}
void Query::HeightMinDesired::set(Int16 value)
{
	_query->hgtMinDesired = value;
}

Int16 Query::HeightMaxDesired::get()
{
	return _query->hgtMaxDesired;
}
void Query::HeightMaxDesired::set(Int16 value)
{
	_query->hgtMaxDesired = value;
}

Int16 Query::WeightMinDesired::get()
{
	return _query->wgtMinDesired;
}
void Query::WeightMinDesired::set(Int16 value)
{
	_query->wgtMinDesired = value;
}

Int16 Query::WeightMaxDesired::get()
{
	return _query->wgtMaxDesired;
}
void Query::WeightMaxDesired::set(Int16 value)
{
	_query->wgtMaxDesired = value;
}

Scores^ Query::Blend::get()
{
	return _scores;
}

void Query::Blend::set(Scores^ value)
{
	_scores = value;
	_query->blend = *(_scores->NativeScores);
}

void Query::AddParameter(NameValue^ parameter)
{
	//_query->tags[_parameterCount].name = parameter->
}

S1Query Query::NativeQuery::get()
{
	return _query;
}