// mobxml.c
// pnelson 11/25/2005
// Export results in XML

#include "string.h"
#include "peng.h"

#define PKGNAME PkgName_Mob

static int MobResultXmlMem(MobResult xres, PXml px, int mode)
{
  int err, i, j, msgIdx;
  char msgBuf[512];

  snprintf(msgBuf, sizeof(msgBuf), "Mems count='%d' about='%ld'", xres->numMem, xres->aboutMem);
  err = PXmlPush(px, "Mems", 0);
  for (i = 0; !err && i < xres->numMem; i++) {
	MobMem mm = &(xres->mems[i]);
	snprintf(msgBuf, sizeof(msgBuf), 
			"Mem memId='%ld' order='%d' match='%d' near='%d' age='%d' hgt='%d' saAct='%d' saPop='%d'",
			mm->memId, mm->priOrder, mm->s.val.match,
			mm->s.val.near, mm->s.val.age, mm->s.val.hgt,
			mm->s.val.saAct, mm->s.val.saPop);

	if (!mm->bestTags[0]) {
	  err |= PXmlPush0(px, msgBuf, 0);
	} else {
	  err |= PXmlPush(px, msgBuf, 0);
	  strcpy(msgBuf, "BestTags"); msgIdx = strlen(msgBuf);
	  for (j=0; mm->bestTags[j] && j < ArrayCount(mm->bestTags); j++) {
		snprintf(msgBuf+msgIdx, sizeof(msgBuf)-msgIdx, 
				 " tag%d='%d'", j, mm->bestTags[j]); 
		msgIdx += strlen(msgBuf+msgIdx);
	  }
	  err |= PXmlPush0(px, msgBuf, 0);
	  err |= PXmlPop(px);
	}
  }
  err |= PXmlPop(px);
  return err;
}

static int MobResultXmlTag(MobResult xres, PXml px, int mode)
{
  int i, err;
  char msgBuf[128];

  err = PXmlPush(px, "Tags", xres->numTag);
  for (i = 0; !err && i < xres->numTag; i++) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "Tag name='%s' value='%s' count='%d' unique='%d'",
			 xres->tags[i].name, xres->tags[i].value, 
			 xres->tags[i].count, xres->tags[i].uniqueness);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  err |= PXmlPop(px);
  return err;
}

int MobResultXml(MobResult xres, PXml px, int mode)
{
	int err;

	if ((err = PXmlPush(px, "Result", xres->sequence)) < 0)
		return err;

	if (xres->retCode)   err |= PXmlSetNum(px, "retCode", xres->retCode);
	if (xres->retMsg[0]) err |= PXmlSetStr(px, "retMsg", xres->retMsg);

	if (!err && xres->numTag) 
		err |= MobResultXmlTag(xres, px, mode);

	if (!err)
		err |= MobResultXmlMem(xres, px, mode);

	err |= PXmlPop(px);

	return err;
}
	
