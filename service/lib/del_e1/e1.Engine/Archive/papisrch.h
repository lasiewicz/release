// papisrch.h
// pnelson, 11/13/2005
// Defintions needed by callers to the engine

#define INCLUDE_PAPISRCH

/***********/
/** Query **/
/***********/

#define kMobSpec_TagsMax     100
#define kMobResults_MemsMax  1000
#define kMobResults_TagsMax  100

// Blend Preference == how much to blend this score, from -100 to 100
//
typedef S1 MobBlendKey;
#define MobBlend_Require  101
#define MobBlendMax       100
#define MobBlendMin      -100
#define MobBlend_Reject  -101

typedef S1 MobScore;

/*********************/
/** Specify a query **/
/*********************/

typedef struct _MobSelectSpec *MobSelectSpec;
typedef struct _MobOrderSpec  *MobOrderSpec;

typedef struct _MobSelectSpec {
	DomId        domId;
	US1          gender;
	US1          scoreMin;  // minum score to be returned in the results
	MobOrderSpec order;     // Primary order spec used to truncate list
	MemLocRec    locKey;    // Full location as focus of search (maybe support more later)
	S2           locDst;    // Preferred radius from location
	MobScore     locDstNeg; // How much to discount score if distance outside band 
	US1          photo;     // -1 has none, 0 don't care, 1 require
	US1          online;    // -1 not online, 0 don't care, 1 require
	US2          ageMinDesired;
	US2          ageMaxDesired;
	US2          hgtMinDesired;
	US2          hgtMaxDesired;
	US2          wgtMinDesired;
	US2          wgtMaxDesired;
	MemNVRec     tags[kMobSpec_TagsMax];
  // Debug params
  US2 debugLocLat, debugLocLng;
} MobSelectSpecRec;

// How important is a good match on each of these attributes or tags (0 is avg importance)
//
typedef struct _MobOrderSpec {
	MobBlendKey match;     // Overall match of attributes
	MobBlendKey near;
	MobBlendKey photo;     
	MobBlendKey online;
	MobBlendKey age;
	MobBlendKey hgt;
	MobBlendKey saAct;
	MobBlendKey saPop;
	MobBlendKey tags[kMobSpec_TagsMax];
} MobOrderSpecRec;

/************************************/
/** Results, w/ ability to reorder **/
/************************************/

typedef struct _MobOrder  *MobOrder;
typedef struct _MobResult *MobResult;

// What engine tells us about each tag
//
typedef struct _MobTag {
	STR   name;
	STR   value;
	US2   count;
	MobScore  uniqueness;
	void      *handle;
} MobTagRec, *MobTag;

#define kMobMem_match    0  // score for oveall attr match
#define kMobMem_saAct    1  // normalized score based on activity
#define kMobMem_saPop    2  // normalized score based on popularity 
#define kMobMem_near     3  // score for nearness (both lat/lng and c/s/c/z
#define kMobMem_age      4  // score for age match
#define kMobMem_hgt      5  // score for height match
#define kMobMem_MaxPri   6  // total number of primary attributes scores
#define kMobMem_MaxTag   (15-kMobMem_MaxPri)

// What engine tells us about each member
// 
typedef union _MobMemScores {
	MobScore arr[kMobMem_MaxPri]; // Somtimes want them all as array
	struct {
		MobScore match;  // score for oveall attr match
		MobScore saAct;  // normalized score based on activity
		MobScore saPop;  // normalized score based on popularity 
		MobScore near;   // score for nearness (both lat/lng and c/s/c/z
		MobScore age;    // score for age match
		MobScore hgt;    // score for height match
	} val;
} MobMemScoresRec;

typedef struct _MobMem {
	MemId           memId;
	MobScore        priOrder;  // score in the primary order used to filter list
	MobMemScoresRec s;         // aray of six scores
	US1             bestTags[kMobMem_MaxTag];   // index into tags list for this mem
} MobMemRec, *MobMem;

// Results includes stats, and list of tags and members
//
typedef struct _MobResult {
	S4       sequence;
	
	S2       retCode;
	char     retMsg[126];

	MobOrder  primaryOrder;

	MobTagRec tags[kMobResults_TagsMax];
	US2       numTag;

	US2       numMem;
	S4        aboutMem;
	MobMemRec mems[kMobResults_MemsMax];

	// Engine will also recommend other tags of interest
	MobTagRec *newTags;
	US2        newTagsNum;

} MobResultRec;

extern MobResult MobSelectExec(MobSelectSpec, MobOrderSpec);
extern void      MobSelectFree(MobResult);
extern int       MobResultXml(MobResult xres, PXml px, int mode);

extern MobOrder MobOrderExec(MobResult, MobOrderSpec);
extern int      MobOrderPage(MobOrder, US2 base, US2 num, US2 *list, US2 *count);
extern int      MobOrderFree(MobOrder);
