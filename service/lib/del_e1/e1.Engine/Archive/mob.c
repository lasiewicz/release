// mob.c
// pnelson 11/25/2005
// Select a mob of members

#define MEM_FRIEND
#include "peng.h"

#define PKGNAME PkgName_Mob

// Internal data that drives the scanner

typedef struct _MobScan {
  RankTune     rankTune;  // Rank tuning parameters
  DomId        domId;     // Must be this domain
  US1          gender;    // Must be this gender
  US1          gay;
  MobScore     scoreMin;  // Min score to even be considered in the Mob
  LocRec       loc;       // Lng Lat Box at focus of search
  S2           locDst;    // Preferred radius from location
  MobScore     locDstNeg; // How much to discount score if distance outside band 
  MemCSCZMapRec cscz;     // Packed bitfield of Country/State/City names & Zipcode
  
  MemDateRec dateMin;     // Min and max birthdate
  MemDateRec dateMax;
  int        hgtMin, hgtMax;
  int        wgtMin, wgtMax;
  int        photo;     // -1 has none, 0 don't care, 1 require
  int        online;    // -1 not online, 0 don't care, 1 require

  // Blends for core attributes based on primary order
  MobMemScoresRec priFilter;
  MobMemScoresRec priBlend;

  // Ptr to actual tags if they exist
  S2           numTags;
  Tag          tags[kMobSpec_TagsMax];
} MobScanRec, *MobScan;


// Convert search input requests to formatted info for our scanner
//
static int MobScanInit(MobScan xscan, MobSelectSpec xspec)
{
	int i;
	memset(xscan, 0, sizeof(*xscan));
	xscan->rankTune  = &gsvRank;
	xscan->domId     = (xspec->domId ? xspec->domId : 1);
	xscan->gender    = MemPackGender(xspec->gender, &(xscan->gay));
	xscan->scoreMin  = xspec->scoreMin;
	if (xspec->debugLocLat || xspec->debugLocLng) {
	  xscan->loc.lat = xspec->debugLocLat;
	  xscan->loc.lng = xspec->debugLocLng;
	} else {
	  MemPackLoc(&(xscan->loc), &(xspec->locKey));
	}
	xscan->locDst    = xspec->locDst;
	xscan->locDstNeg = xspec->locDstNeg;
	MemPackCSCZMap(&(xscan->cscz), &(xspec->locKey));
	// The oldest target member has the smaller DOB
	xscan->dateMin.pack = RankPackAge(xspec->ageMaxDesired);
	xscan->dateMax.pack = RankPackAge(xspec->ageMinDesired);
	xscan->hgtMin = RankPackHgt(xspec->hgtMinDesired);
	xscan->hgtMax = RankPackHgt(xspec->hgtMaxDesired);
	xscan->wgtMin = RankPackWgt(xspec->wgtMinDesired);
	xscan->wgtMax = RankPackWgt(xspec->wgtMaxDesired);
	xscan->photo   = xspec->photo;
	xscan->online  = xspec->online;

	//xscan->priFilter = gsvRank.priFilter;
	//xscan->priBlend  = gsvRank.priBlend;

	for (i=0; xspec->tags[i].name; i++) {
		TagKeyRec tk; StructNull(tk);
		str0ncpy(tk.name,  xspec->tags[i].name,  sizeof(tk.name));
		str0ncpy(tk.value, xspec->tags[i].value, sizeof(tk.value));
		xscan->tags[i] = TagFind(&tk);
	}
	xscan->numTags = i;
	return 0;
}


// MobMem  is the result object we're populating
// MobScan is the rep of our query
// Mem     is the member we're scanning
// ldgScr  is score representing distance from this LDG chain to focus of the search
//
static MobScore MobScanExec(MobMem res, MobScan xscan, Mem xmem, MobScore ldgScr)
{
	int ok = 1;

	res->memId = xmem->memId;

	res->s.val.age = 0; // ??? Use the lookup array method when this code is restored
	if (res->s.val.age == 0)
		if (xscan->priFilter.val.age == MobBlend_Require) 
			ok = 0;

	res->s.val.saAct  = (MobScore)xmem->vitals.saAct * 6;  // normalize 0-15
	res->s.val.saPop  = (MobScore)xmem->vitals.saPop * 6;  // normalize 0-15

	res->s.val.near = RankNearExec(&(xmem->cscz), &(xscan->cscz), xscan->rankTune);
	if (res->s.val.near == 0)
		if (xscan->priFilter.val.near == MobBlend_Require) 
		ok = 0;

	res->s.val.hgt = RankHgtExec(xmem->vitals.height, xscan->hgtMin, xscan->hgtMax, xscan->rankTune);
	if (res->s.val.hgt == 0)
		if (xscan->priFilter.val.hgt == MobBlend_Require) 
			ok = 0;

	res->s.val.match = 100; // ???
	if (res->s.val.match == 0)
		if (xscan->priFilter.val.match == MobBlend_Require) ok = 0;
	
	// ??? res->bestTags[] ;

	// ??? Punt this user if not above threshhold on required parameters
	if (!ok) {
		res->priOrder = 0;
	} else {
		res->priOrder = RankAccrue(kMobMem_MaxPri, res->s.arr, xscan->priBlend.arr);
	}
	return res->priOrder;
}

// Do the Search
// ??? This needs to get much smarter
//
MobResult MobSelectExec(MobSelectSpec xspec, MobOrderSpec oSpec)
{
	Ldg xldg;
	LdgIterRec iter;
	MobScanRec xscan;
	MobResult  xres = PEngAlloc(PKGNAME, sizeof(MobResultRec));
	S4 resPtr, resMax;
	int i;

	// Map the query into our internal rep optimized for scanning
	MobScanInit(&xscan, xspec);

	// Put the requested tags into the result
	// Engine will provide count and uniqueness
	for (i=0; i<xscan.numTags; i++) {
		xres->tags[i].name  = xspec->tags[i].name;
		xres->tags[i].value = xspec->tags[i].value;
		xres->tags[i].handle = (void*)xscan.tags[i];
	}
	xres->numTag = i;

	// Prepare to iterate
	StructNull(iter);
	LdgIterInit(&iter, &(xscan.loc), xscan.domId, xscan.gender, 0);
	resPtr = xres->numMem;
	resMax = ArrayCount(xres->mems);

	// Iterate -- this looks at way too many
	while (resPtr < resMax && (xldg = LdgIterNext(&iter))) {
		S4       ldgDst = RankLdgDstCalc(&(xscan.loc), &(iter.ldgKey.loc));
		MobScore ldgScr = RankNearLdgDst(ldgDst, xscan.rankTune);
		Mem *root = LdgChainMemRoot(xldg), xmem;
		for (xmem = *root; xmem && resPtr < resMax; xmem = xmem->ldgChain) {
			MobScore score = MobScanExec(&(xres->mems[resPtr]), &xscan, xmem, ldgScr);
			if (score > xscan.scoreMin) {
				xres->numMem = (US2)++resPtr;
			}
		}
	}
	return xres;
}

void MobSelectFree(MobResult xres)
{
	if (xres)
		PEngFree(PKGNAME, xres, sizeof(*xres));
}

/****************/
