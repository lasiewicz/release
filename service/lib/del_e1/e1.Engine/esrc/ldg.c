// ldg.c
// pnelson 11/15/2005
// Organize members in chains of same Location/Domain/Gender

#include "stdlib.h"
#include "peng.h"

#define PKGNAME PkgName_Ldg

// Generate a distributed index from a location/domain/gender key
static PHashIdx xLdgKeyToIdx(LdgKey lk)
{
  US4 idx = (lk->loc.lng*997 + lk->loc.lat*7) ^ (5*(US4)lk->domId) ^ lk->gender;
  idx %= kLdg_HashSize;
  return (PHashIdx)idx;
}

// When ordering Ldgs, favor lower lng, then lat, then gender, then domain
static int xLdgKeyCmp(LdgKey lk, Ldg xldg)
{
	LdgKey xk = &(xldg->ldgKey);
	if (lk->loc.lng != xk->loc.lng)
		return (lk->loc.lng < xk->loc.lng ? -1 : 1);
	if (lk->loc.lat != xk->loc.lat)
		return (lk->loc.lat < xk->loc.lat ? -1 : 1);
	if (lk->gender != xk->gender)
		return (lk->gender < xk->gender ? -1 : 1);
	if (lk->domId != xk->domId)
		return (lk->domId < xk->domId ? -1 : 1);
	return 0;
}
	
/****************/

static void xLdgInitialize()
{
	if (!gsv->ldgs) {
		gsv->ldgs = PHashNew(PKGNAME, kLdg_HashSize, 
							         (PHashKeyToIdxFnc*)xLdgKeyToIdx, 
							         (PHashKeyCmpFnc  *)xLdgKeyCmp);
		gsv->ldgObjMgr = ObjMgrCreate(PKGNAME, sizeof(LdgRec), kLdg_MaxLdgs);
		if (!gsv->ldgs) 
			PEngError(PKGNAME, NULL, "Init");
	}
}

/****************/

S4 LdgSequence(Ldg xldg)
{
	return ObjMgrPtrToSeq(gsv->ldgObjMgr, (ObjPtr)xldg);
}

Ldg LdgBySequence(S4 seq)
{
	return (Ldg)ObjMgrSeqToPtr(gsv->ldgObjMgr, seq);
}

Ldg LdgFind(LdgKey lk)
{
	if (!gsv->ldgs) xLdgInitialize();
	if (!lk) return NULL;
	return (Ldg)PHashFind(gsv->ldgs, (PHashKey)lk);
}

Ldg LdgFindOrNew(LdgKey lk)
{
	Ldg xldg;
	if (!gsv->ldgs) xLdgInitialize();
	if (!lk) return NULL;
	if ((xldg = (Ldg)PHashFind(gsv->ldgs, (PHashKey)lk)))
		return xldg;
	xldg = (Ldg)ObjMgrNew(gsv->ldgObjMgr, NULL);
	xldg->ldgKey = *lk;
	return (Ldg)PHashPush(gsv->ldgs, (PHashKey)&(xldg->ldgKey), (PHashObj)xldg);
}

LdgKey LdgKeyGet(Ldg xldg)
{
	if (!xldg) return 0;
	return &(xldg->ldgKey);
}

Mem *LdgChainMemRoot(Ldg xldg)
{
	if (!xldg) return 0;
	return &(xldg->memChain);
}

// ****************************************************************

// Iter -- new object pre-allocated on the stack
// domId, gender -- what to look for
// loc  -- starting location for iteration
// maxBand -- how many boxes out to look
//
void LdgIterInit(LdgIter iter, Loc loc, DomId domId, int gender, int maxBand)
{
	memset(iter, 0, sizeof(LdgIterRec));
	// Set up our center
	iter->ldgKey.loc    = *loc;
	iter->ldgKey.domId  = domId;
	iter->ldgKey.gender = gender;

	iter->locBase = *loc;

	iter->band = 0;
	iter->side = 4;  // Will trigger IterNext to goto next band
	if (maxBand <= 0 || maxBand > kLdg_IterBandMax)
		maxBand = kLdg_IterBandMax;
	iter->maxBand = maxBand;

	// See if we have one at the center
	iter->cache = LdgFind(&(iter->ldgKey));
}

// Walks in squares around the center until we find a loc
// Returns NULL when there are really none left
//
Ldg LdgIterNext(LdgIter iter)
{
  Ldg tst;

  if ((tst = iter->cache))
	{ iter->cache = NULL;	return tst;	}

  while (1) {
	// Maybe it's time to go to the next side
	if (iter->sidx == iter->band*2) {
	  iter->sidx  = 0;
	  iter->side += 1;
	  
	  // Maybe it's time to go to the next band
	  if (iter->side > 3) {
		iter->band += 1;
		iter->scanLng = (S4)iter->locBase.lng - iter->band;
		iter->scanLat = (S4)iter->locBase.lat - iter->band;
		iter->side = iter->sidx = 0;
		
		// Maybe it's time to leave for good
		if (iter->band >= iter->maxBand)
		  break;
	  }
	}
	
	// Adjust for wrap around, check current point
	iter->ldgKey.loc.lng = (US2)(iter->scanLng + kLoc_EWBoxes) % kLoc_EWBoxes;
	iter->ldgKey.loc.lat = (US2)(iter->scanLat + kLoc_NSBoxes) % kLoc_NSBoxes;
	tst = LdgFind(&(iter->ldgKey));
	
	// Setup advance to next 
	switch (iter->side) {
	case 0: iter->scanLng++; break;
	case 1: iter->scanLat++; break;
	case 2: iter->scanLng--; break;
	case 3: iter->scanLat--; break;
	}
	iter->sidx += 1;
	
	if (tst)
	  return tst;
  }
  return NULL;
}

// ****************************************************************

int LdgXml(PXml px, Ldg xldg, int mode)
{
  char msgBuf[128];
  int err;
  S4 seq = LdgSequence(xldg);
  LdgKey lk;

  lk = &(xldg->ldgKey);

  snprintf(msgBuf, sizeof(msgBuf), "Ldg seq='%ld' domid='%d' gender='%d' lng='%d' lat='%d' memcount='%ld'",
		   seq, lk->domId, lk->gender, lk->loc.lng, lk->loc.lat, xldg->memCount);
  if (mode == 0) { 
	err = PXmlPush0(px, msgBuf, 0);
  } else {
	err = PXmlPush(px, msgBuf, 0);
	err |= MemXmlLdgChain(px, xldg->memChain, 0);
	err |= PXmlPop(px);
  }
  return err;
}
