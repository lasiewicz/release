// pmap.c
// pnelson 11/14/2005
// Place a bit wherever we have members, and iterate on just those places
// !!! It might be faster to iterate by checking for each neighboring location

#include "peng.h"

#define PKGNAME PkgName_PMap

#define kPMap_CellBoxes (1<<5)  // 32 bits per populated box
#define kPMap_BandMax   (1<<9)  // Max distance we'll ever look

typedef struct _PMapCell { 
	US1 bits[(kPMap_CellBoxes*kPMap_CellBoxes)/8]; 
} PMapCellRec, *PMapCell;

typedef struct _PMapNS {
	PMapCell lineNS[kLoc_NSBoxes/kPMap_CellBoxes];
} PMapNSRec, *PMapNS;

typedef struct _PMapEW {
	PMapNS lineEW[kLoc_EWBoxes/kPMap_CellBoxes];
} PMapEWRec, *PMapEW;


/****************/

typedef struct _PMapLoc {
	US2 idxEW, idxNS;      // Location of cell
	US2 bytCell, bitCell;  // Where in the cell
} PMapLocRec, *PMapLoc;

// Map a loc to a cell in the PMap, and the byte/bit in the cell
//
static void xPMapDecode(Loc loc, PMapLoc decode)
{
  US2 bitEW, bitNS, idx;
  decode->idxEW = loc->lng / kPMap_CellBoxes;
  decode->idxNS = loc->lat / kPMap_CellBoxes;
  bitEW = loc->lng % kPMap_CellBoxes;
  bitNS = loc->lat % kPMap_CellBoxes;
  idx = (bitNS * kPMap_CellBoxes + bitEW);
  decode->bytCell = idx / 8;
  decode->bitCell = idx % 8;
}

/****************/


void PMapSet(Loc loc)
{
	PMapLocRec pmloc;
	PMapEW ew;
	PMapNS ns;
	PMapCell cell;

	xPMapDecode(loc, &pmloc);

	// Maybe auti create the map
	if (!(ew = gsv->pmap))
		ew = gsv->pmap = PEngAlloc(PKGNAME, sizeof(PMapEWRec));

	// Find or Create the vertical line
	if (!(ns = ew->lineEW[pmloc.idxEW]))
		ns = ew->lineEW[pmloc.idxEW] = PEngAlloc(PKGNAME, sizeof(PMapNSRec));

	// Find or Create the cell 
	if (!(cell = ns->lineNS[pmloc.idxNS]))
		cell = ns->lineNS[pmloc.idxNS] = PEngAlloc(PKGNAME, sizeof(PMapCellRec));

	// Flip the bit in the cell
	cell->bits[pmloc.bytCell] |= (1 << pmloc.bitCell);
}

int PMapGet(Loc loc)
{
	PMapLocRec pmloc;
	PMapEW ew;
	PMapNS ns;
	PMapCell cell;

	xPMapDecode(loc, &pmloc);

	if (!(ew = gsv->pmap))
		return 0;

	if (!(ns = ew->lineEW[pmloc.idxEW]))
		return 0;

	if (!(cell = ns->lineNS[pmloc.idxNS]))
		return 0;

	if (cell->bits[pmloc.bytCell] & (1 << pmloc.bitCell))
		return 1;
	else
		return 0;
}

/**************/
/** Iterator **/
/**************/

Loc PMapIterInit(PMapIter iter, Loc loc, US2 maxBand)
{
	memset(iter, 0, sizeof(PMapIterRec));
	iter->loc  = *loc;
	iter->next = *loc;
	iter->band = 0;
	iter->side = 4;  // Will trigger IterNext to goto next band

	if (maxBand == 0 || maxBand > kPMap_BandMax)
		maxBand = kPMap_BandMax;
	iter->maxBand = maxBand;

	// Invalidate cache
	iter->cellEW = iter->cellNS = 0xFFFF;
	return &(iter->next);
}

Loc PMapIterTest(PMapIter iter)
{
	PMapLocRec pmloc;
	PMapEW ew;
	PMapNS ns;
	PMapCell cell;

	// Adjust for wrap around
	iter->next.lng = (US2)(iter->scanLng + kLoc_EWBoxes) % kLoc_EWBoxes;
	iter->next.lat = (US2)(iter->scanLat + kLoc_NSBoxes) % kLoc_NSBoxes;
	// Decode
	xPMapDecode(&(iter->next), &pmloc);
	// Get Cell
	if (pmloc.idxEW != iter->cellEW || pmloc.idxNS != iter->cellNS) {
		iter->cellEW = pmloc.idxEW;
		iter->cellNS = pmloc.idxNS;
	ew = (PMapEW)gsv->pmap;
		ns = ew->lineEW[pmloc.idxEW];
		iter->cell = (!ns ? NULL : ns->lineNS[pmloc.idxNS]);
	}
	if (!(cell = iter->cell))
		return NULL;
	if (cell->bits[pmloc.bytCell] & (1 << pmloc.bitCell))
		return &(iter->next);
	return NULL;
}

// Walks in squares around the center until it finds and returns a loc
// Returns NULL when there are really none left
//
Loc PMapIterNext(PMapIter iter)
{
	Loc tst = NULL;
	while (1) {
		// Maybe it's time to go to the next side
		if (iter->sidx == iter->band*2) {
			iter->sidx  = 0;
			iter->side += 1;

			// Maybe it's time to go to the next band
			if (iter->side > 3) {
				iter->band += 1;
				iter->scanLng = (S4)iter->loc.lng - iter->band;
				iter->scanLat = (S4)iter->loc.lat - iter->band;
				iter->side = iter->sidx = 0;

				// Maybe it's time to leave for good
				if (iter->band >= iter->maxBand)
					break;
			}
		}

		// Check the bit under the current point, then advance
		switch (iter->side) {
		case 0: tst = PMapIterTest(iter); iter->scanLng++; break;
		case 1: tst = PMapIterTest(iter); iter->scanLat++; break;
		case 2: tst = PMapIterTest(iter); iter->scanLng--; break;
		case 3: tst = PMapIterTest(iter); iter->scanLat--; break;
		}
		iter->sidx += 1;
		if (tst)
			return tst;
	}
	return NULL;
}
