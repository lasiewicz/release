// pxml.c
// pnelson 11/20/2005
// Utility to help serialize objects into string of XML

#include "string.h"
#include "peng.h"

#define PKGNAME PkgName_PXml

#define kPXml_NodeNameMax   32         // 32 bytes is max node name
#define kPXml_StackMax  64         // 64 is max stack depth

typedef char PXmlNodeName[kPXml_NodeNameMax];

static char *xPXmlHeaderFmt = 
  "<?xml version='1.0' encoding='UTF-8'?><?xml-stylesheet href='%s' type='text/xsl'?>\n";

typedef struct _PXml {
	S4   allocSize;   // How much did we alloc
	S2   enumMax;     // How deep to walk lists

	S4   dataMax;     // Max data in the XML string
	S4   dataAbort;   // When gets this big, shut it down
	S4   dataIdx;     // Current size
	char *dataXml;    // The actual data

	PXmlNodeName nodeStack[kPXml_StackMax];
	S4   nodeSequence[kPXml_StackMax];
	S2   nodeIdx;

    char *xslName;    // Name of XSL transform to insert
} PXmlRec;

// Convenience macro for current write point
#define XPT(px)       ((px)->dataXml + ((px)->dataIdx))
#define XPTExtend(px) (px)->dataIdx += strlen(XPT(px))

// Allocate a XML serialization collector
//
PXml PXmlNew(S4 dataMax, int enumMax, STR xslName)
{
	S4   aSize; 
	US1  *data;
	PXml  px;
	int   xslLen;
    char  xslBuf[1024];

	if (xslName && xslName[0]) {
	  snprintf(xslBuf, sizeof(xslBuf), xPXmlHeaderFmt, xslName);
	} else {
	  xslBuf[0] = 0;
	}
	xslName = xslBuf;
	xslLen  = (strlen(xslName) + 4) & (~3); // round up for alignment

	dataMax   = minmax(dataMax, (1 << 13), (1 << 20));
	enumMax   = minmax(enumMax, (1<<2), (1<<8));

	aSize  = StructSizeAlign(PXmlRec) + xslLen + dataMax;
	data = PEngAlloc(PKGNAME, aSize);
	px = (PXml)data;
	px->xslName   = (data + StructSizeAlign(PXmlRec));
	px->dataXml   = (data + StructSizeAlign(PXmlRec) + xslLen);

	px->allocSize = aSize;
	px->enumMax   = enumMax;
	px->dataMax   = dataMax;
	px->dataAbort = dataMax - 1024;

	strcpy(px->xslName, xslName);

	PXmlReset(px);

	return px;
}

// Free an XML serialization collector
//
void PXmlFree(PXml px)
{
	if (px)
	  PEngFree(PKGNAME, px, px->allocSize);
}

/****************/

void PXmlShow(PXml px, void *file, int reset)
{
	FILE *ff = (FILE*)file;
	if (!ff) ff = gsv->dbgOutput;
	if (!ff) ff = stdout;

	if (px) {
		S4 xslLen = strlen(px->xslName);
		if (px->dataIdx > xslLen) {
			if (ff) fputs(px->dataXml, ff);
			if (reset) PXmlReset(px);
		}
	}
	fflush(ff);
}

void PXmlReset(PXml px)
{
	if (px) {
	  strcpy(px->dataXml, px->xslName);
	  px->dataIdx = strlen(px->dataXml);
	  px->nodeIdx = 0;
	}
}

/****************/

static void peelAttr(STR tag)
{
	// Peel any attributes off the name
	int i;
	for (i=0; tag[i]; i++) 
		if (tag[i] == ' ') { 
			tag[i] = 0; 
			break;
		}
}


// Push an object tag with no decendants
int PXmlPush0(PXml px, cSTR tag, S4 seq)
{
  int idx = px->nodeIdx;
  if (seq) {
	sprintf(XPT(px), "%*s<%s seq='%ld'/>\n", idx*2, " ", tag, seq);
  } else {
	sprintf(XPT(px), "%*s<%s />\n", idx*2, " ", tag);
  }
  XPTExtend(px);
  return (px->dataIdx >= px->dataAbort) ? 1 : 0;
}

// Push an object tag with descendants (must be Popped)
int PXmlPush(PXml px, cSTR tag, S4 seq)
{
	int idx; char seqBuf[24];

	if (px->nodeIdx >= kPXml_StackMax)
		return -1;

	// Push this level
	idx = px->nodeIdx++;
	str0ncpy(px->nodeStack[idx], tag, sizeof(PXmlNodeName));
	peelAttr(px->nodeStack[idx]);
	px->nodeSequence[idx] = seq;

	// Only show sequence number if it has one
	if (seq == 0) seqBuf[0] = 0; 
	else snprintf(seqBuf, sizeof(seqBuf), " seq='%ld'", seq);

	sprintf(XPT(px), "%*s<%s%s>\n", idx*2, " ", tag, seqBuf);
	XPTExtend(px);
	return (px->dataIdx >= px->dataAbort) ? 1 : 0;
}

// Pop a corresponding Push
int PXmlPop(PXml px)
{
	int idx = --px->nodeIdx;
	sprintf(XPT(px), "%*s</%s>\n", idx*2, " ", px->nodeStack[idx]);
	XPTExtend(px);
	return (px->dataIdx >= px->dataAbort) ? 1 : 0;
}

// Insert a name with string value
int PXmlSetStr(PXml px, cSTR name, cSTR value)
{
	char tagBuf[32];
	int idx = px->nodeIdx;
	str0ncpy(tagBuf, name, sizeof(tagBuf));
	peelAttr(tagBuf);
	sprintf(XPT(px), "%*s<%s>%s</%s>\n", idx*2, " ", name, value, tagBuf);
	XPTExtend(px);
	return (px->dataIdx >= px->dataAbort) ? 1 : 0;
}

// Insert a string -- assume we are between a push and a pop
int PXmlInsertStr(PXml px, cSTR str)
{
  strcpy(XPT(px), str);
  XPTExtend(px);
  return (px->dataIdx >= px->dataAbort) ? 1 : 0;
}

// Insert a name with numeric value
int PXmlSetNum(PXml px, cSTR name, S4 value)
{
	int idx = px->nodeIdx;
	sprintf(XPT(px), "%*s<%s num='%ld'/>\n", idx*2, " ", name, value);
	XPTExtend(px);
	return (px->dataIdx >= px->dataAbort) ? 1 : 0;
}

STR PXmlData(PXml px, S4 *lenp)
{
	if (!px) return NULL;
	if (lenp) *lenp = px->dataIdx;
	return px->dataXml;
}

int PXmlEnumMax(PXml px)
{
	return (px ? px->enumMax : 0);
}
