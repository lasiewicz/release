// seqmgr.c
// pnelson 11/20/2005
// Assign objects a sequence number, then look up by that number

#include "peng.h"
#define PKGNAME PkgName_SeqMgr

#define kSeqMgr_PagesMax 64
 
typedef SeqObj *SeqPage;

typedef struct _SeqMgr {
	PkgName pkg;
	S4  pageSize;   // How many per page   
	S4  nextPtr;    // Current page
	S4  nextIdx;    // Current item on page
	S4  nextFree;   // Free list
	S4  aSize;      // Sizeof initial alloc

	SeqPage pages[kSeqMgr_PagesMax];

} SeqMgrRec;

SeqMgr SeqMgrCreate(PkgName pkg, S4 maxObjs)
{
	S4 pageBytes, aSize;
	US1 *data;
	SeqMgr xom;
	
	// Round up if needed
	int pageSize = maxObjs/kSeqMgr_PagesMax;
	if (maxObjs % kSeqMgr_PagesMax) pageSize++;

	pageBytes = pageSize * sizeof(SeqObj);

	// Alloc manager and first page
	aSize  = StructSizeAlign(SeqMgrRec);
	data  = PEngAlloc(pkg, aSize + pageBytes);

	xom    = (SeqMgr)data;
	xom->pages[0] = (SeqPage)(data + aSize);
	xom->pkg      = pkg;
	xom->pageSize = pageSize;
	xom->nextPtr  = 0;
	xom->nextIdx  = 1;  // Don't give out 0,0
	xom->nextFree = 0;
	xom->aSize    = aSize + pageBytes;

	return xom;
}

S4 SeqMgrSequence(SeqMgr xom)
{
	if (xom) {
		S4 seq = xom->nextPtr * xom->pageSize + xom->nextIdx;
		return seq;
	}
	return 0;
}

void SeqMgrDestroy(SeqMgr xom)
{
	if (xom) {
		int i;
		for (i = 1; i < kSeqMgr_PagesMax; i++) 
			if (xom->pages[i])
				PEngFree(xom->pkg, xom->pages[i], xom->pageSize*sizeof(SeqObj));
		PEngFree(xom->pkg, xom, xom->aSize);		
	}
}	

// Give an object a sequence number that it can be fetched by later
//
S4 SeqMgrPush(SeqMgr xom, SeqObj obj)
{
  S4 seq = 0, ptr, idx;

  if (xom->nextFree) {
	seq = xom->nextFree;
	xom->nextFree = (S4)SeqMgrLookup(xom, seq);
	ptr = seq / xom->pageSize;
	idx = seq % xom->pageSize;
	(xom->pages[ptr])[idx] = obj;
	return seq;
  }
  
  // Maybe need a new page
  if (xom->nextIdx == xom->pageSize) {
	if (xom->nextPtr++ == kSeqMgr_PagesMax)
	  goto error;
	if (!(xom->pages[xom->nextPtr] = PEngAlloc(xom->pkg, sizeof(SeqObj)*xom->pageSize)))
	  goto error;
	xom->nextIdx = 0;
  }
  
  // Place it on the page
  (xom->pages[xom->nextPtr])[xom->nextIdx] = obj;
  seq = xom->nextPtr * xom->pageSize + xom->nextIdx;
  xom->nextIdx += 1;
  
  return seq;
  
 error:
  { 
	char buf[128];
	snprintf(buf, sizeof(buf), "SeqMgr assigning %s after %ld pages, %ld on page", 
			 GsvPkgNameAsStr(xom->pkg), xom->nextPtr, xom->nextIdx);
	PEngError(xom->pkg, NULL, buf);
  }
  return 0;
}

// Lookup an object by it's sequence number
//
SeqObj SeqMgrLookup(SeqMgr xom, S4 seq)
{
	if (xom && seq > 0) {
		S4 ptr = seq / xom->pageSize;
		S4 idx = seq % xom->pageSize;

		if (ptr < xom->nextPtr ||
				(ptr == xom->nextPtr && idx < xom->nextIdx)) {
			return (xom->pages[ptr])[idx];
		}
	}
	return NULL;
}

// Remove an object and free up its sequence for later use
//
int SeqMgrPop(SeqMgr xom, S4 seq)
{
	S4 ptr, idx;
	if (!xom || seq <= 0)
		return -1;

	ptr = seq / xom->pageSize;
	idx = seq % xom->pageSize;

	if (ptr < xom->nextPtr ||
			(ptr == xom->nextPtr && idx < xom->nextIdx)) {
		(xom->pages[ptr])[idx] = (SeqObj)xom->nextFree;
		xom->nextFree = seq;
		return 0;
	} else {
		return -1;
	}
}

int SeqMgrXml(PXml px, SeqMgr xom, int mode)
{
  int errCode = 0;
  if (xom && px) {
	char msgBuf[256]; 
	S4 seq = SeqMgrSequence(xom);
	snprintf(msgBuf, sizeof(msgBuf), "SeqMgr name='%s'", GsvPkgNameAsStr(xom->pkg));

	if ((errCode = PXmlPush(px, msgBuf, seq)) < 0)
	  return errCode;
	else {
	  S4 cnt; S4 scan;
	  for (cnt=0, scan = xom->nextFree; scan; cnt++, scan=(S4)SeqMgrLookup(xom, scan)) ;
	  snprintf(msgBuf, sizeof(msgBuf), 
			   "Stats pageMax='%d' pageSize='%d' nextPtr='%d' nextIdx='%d' nextFree='%d' freelist='%d'",
			   kSeqMgr_PagesMax, xom->pageSize, 
			   xom->nextPtr, xom->nextIdx, xom->nextFree, cnt);
	  errCode |= PXmlPush0(px, msgBuf, 0);
	}
	errCode |= PXmlPop(px);
  }
  return errCode;
}
