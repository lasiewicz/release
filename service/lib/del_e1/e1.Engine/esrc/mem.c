// mem.c
// pnelson 11/15/2005
// In Memory rep of members

#include "stdlib.h"
#define MEM_FRIEND
#include "peng.h"

#define PKGNAME PkgName_Mem

/**********************/
/** Hashable methods **/
/**********************/

// Hash index is XOR of member id and domain
//
static PHashIdx xMemKeyToIdx(MemKey mk)
{
  US4 idx = (mk->memId ^ mk->domId) % kMem_HashSize;
  return (PHashIdx)idx;
}

// Compare by memId, then domId 
//
static int xMemKeyCmp(MemKey mk, Mem xmem)
{
  MemId xmemId;
  DomId xdomId;
  
  xmemId = xMemMemId(xmem);
  if (mk->memId != xmemId)
	return (mk->memId < xmemId) ? -1 : 1;
  
  xdomId = xMemDomId(xmem);
  if (mk->domId != xdomId)
	return (mk->domId < xdomId) ? -1 : 1;
  
  return 0;
}

/***************/
/** Utilities **/
/***************/

void MemInitialize()
{
  if (!gsv->mems) {
	gsv->mems = PHashNew(PKGNAME, kMem_HashSize, 
						 (PHashKeyToIdxFnc*)xMemKeyToIdx, 
						 (PHashKeyCmpFnc  *)xMemKeyCmp);
	gsv->memObjMgr = ObjMgrCreate(PKGNAME, sizeof(MemRec), kMem_MaxMems);
	// Build mapping tables that give strings a number
	gsv->pkCountry = StxDbNew(kStx_CountryBits, "Country");
	gsv->pkState   = StxDbNew(kStx_StateBits,   "State");  
	gsv->pkCity    = StxDbNew(kStx_CityBits,    "City");   
	gsv->pkZip     = StxDbNew(kStx_ZipBits,     "Zip");   
	if (!gsv->mems) 
	  PEngError(PKGNAME, NULL, "Init");
  }
}

// Cut a member from all other links
//
static char *xMemLinkOut(Mem xmem)
{
  MemKeyRec mk;
  Mem *memRoot, memScan;
  char *err;
  
  if (!xmem) return NULL;
  
  StructNull(mk);
  mk.memId = xMemMemId(xmem);
  mk.domId = xMemDomId(xmem);
  
  // Cut it from the ldg chain
  if (xmem->ldg) {
	xmem->ldg->memCount -= 1;
	for (memRoot = LdgChainMemRoot(xmem->ldg); 
		 (memScan = *memRoot); 
		 memRoot = &(memScan->ldgChain)) {
	  if (memScan == xmem)
		{ *memRoot = xmem->ldgChain; break; }
	}
	if (!memScan)  // Never found it
	  { err = "Find in LdgChain"; goto error; }
  }
  
  // Cut it from the parent list of the tag grp
  if (xmem->grp) {
  	GrpCountUpdate(xmem->grp, -1);
	for (memRoot = GrpChainMemRoot(xmem->grp);
		 (memScan = *memRoot); 
		 memRoot = &(memScan->grpChain)) {
	  if (memScan == xmem)
		{ *memRoot = memScan->grpChain; break; }
	}
	if (!memScan) // Never found it
	  { err = "Find in GrpChain"; goto error; }
  }
  
  // Pop it from the hash table
  if (!(PHashPop(gsv->mems, (PHashKey)&mk, (PHashObj)xmem)))
	{ err = "Find in Hash"; goto error; }
  
  return NULL;
  
 error:
  return err;
}
	
// Add a member to all other links
//
static Mem xMemLinkIn(Mem xmem, MemKey mk)
{
  Mem *memRoot;
  
  // Add it to the head of the ldg chain ??? insert in some order ???
  memRoot = LdgChainMemRoot(xmem->ldg);
  xmem->ldgChain = *memRoot;
  *memRoot = xmem;
  xmem->ldg->memCount += 1;
  
  // Add it to parent list of the tag grp
  if (xmem->grp) {
	memRoot = GrpChainMemRoot(xmem->grp);
	xmem->grpChain = *memRoot;
	*memRoot = xmem;
	GrpCountUpdate(xmem->grp, 1);
  }
  
  // Put it into the hash table
  return (Mem)PHashPush(gsv->mems, (PHashKey)mk, (PHashObj)xmem);
}

/**********************/
/** External Methods **/
/**********************/

S4 MemSequence(Mem xmem)
{
  return ObjMgrPtrToSeq(gsv->memObjMgr, (ObjPtr)xmem);
}

Mem MemBySequence(S4 seq)
{
  return (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, seq);
}

Mem MemFind(MemKey mk)
{
  if (!mk) return NULL;
  return (Mem)PHashFind(gsv->mems, (PHashKey)mk);
}

Mem MemCreateOrUpdate(MemSpec memSpec)
{
  return MemUpsert(memSpec);
}

// Create a new member, or update an existing one
// *Need a write permit
//
Mem MemUpsert(MemSpec memSpec)
{
  MemKeyRec mk;
  LdgKeyRec lk;
  Ldg xldg = NULL;
  Grp xgrp = NULL;
  Mem xmem = NULL;
  Mem memOld = NULL;
  char *err = NULL;
  int errCode = 0;
  PermitWrite myPermit = (PermitWrite)0;
  int gender, reuseOld;
  S4  seq;
  char memBuf[20];

  // Must have memId and domId
  if (!memSpec || (!memSpec->debugSeq && (!memSpec->memId || !memSpec->domId)))
	return NULL;
  // Must have gender
  if (!(gender = MemPackGender(memSpec->genderMask, NULL)))
	return NULL;

  if (memSpec->permit) {
	// If permit passed in, make sure it's good
	if ((errCode = PermitWriteConfirm((PermitWrite)memSpec->permit)))
	  { err = "Invalid Permit"; goto error; }
  } else {
	// Get the write permit
	if ((errCode = PermitWriteTake("MemUpsert", &myPermit)))
	  { err = "Couldn't Take Permit"; goto error; }
  }
	  
  // See if this person already exists
  StructNull(mk);
  if (memSpec->debugSeq) {
	// If caller asked for it by sequence, sequence must already exist
	if (!(memOld = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, memSpec->debugSeq)))
	  { err = "By Seq"; mk.memId = memSpec->debugSeq; goto error; }
  } else {
	mk.memId = memSpec->memId;
	mk.domId = memSpec->domId;
	if ((memOld = (Mem)PHashFind(gsv->mems, (PHashKey)&mk)))
	  memBuf[0] = 0; // Just a place holder to stop in debugger		
  }

  // Find or create this location
  StructNull(lk);
  if (memSpec->debugLocLat || memSpec->debugLocLng) {
	lk.loc.lat = memSpec->debugLocLat;
	lk.loc.lng = memSpec->debugLocLng;
  } else {
	MemPackLoc(&(lk.loc), &(memSpec->memLoc));
  }
  lk.domId  = memSpec->domId;
  lk.gender = gender;
  if (!(xldg = LdgFindOrNew(&lk)))
	{ err = "Ldg Find"; goto error; }

  {
	// Process any remaining NV pairs into tags
	int i;
	int memTagIdx = memSpec->tobjCnt;
	int memTagMax = ArrayCount(memSpec->tobj) - 32;
	Tag *memTagDat = (Tag*)memSpec->tobj;
	for (i = 0; memSpec->tags[i].name && i < ArrayCount(memSpec->tags) && memTagIdx < memTagMax; i++) {
	  STR tnam = memSpec->tags[i].name;
	  STR tval = memSpec->tags[i].value;
	  TagDbId tdx = TagDbFind(tnam);
	  if (tdx) {
		S4 code = atol(tval);
		memTagIdx += TagDbByCode(tdx, code, memTagDat+memTagIdx);
	  } else if (tnam && tnam[0]) {
		Tag xtag = TagFindOrNewStr(tnam, tval);
		if (xtag)
		  memTagDat[memTagIdx++] = xtag;
	  }
	  memSpec->tobjCnt = memTagIdx;
	}
	// Find or create this attribute group
	if (memTagIdx > 0) {
	  if (!(xgrp = GrpFindOrNewFromTags(memTagDat, memTagIdx)))
		{ err = "Grp Find"; goto error; }
	}
  }

  // Check to see if nothing linked has changed, and only writeable fields are specified
  reuseOld = (memOld && (xldg == memOld->ldg) && (!xgrp || xgrp == memOld->grp));
				 
  if (reuseOld) {
	xmem = memOld; 
  } else {
	// Create a new one -- can't write over objects in use !!!
	xmem = (Mem)ObjMgrNew(gsv->memObjMgr, &seq);
#ifdef PENGDEBUG
	if ((seq & 0xFFFF) == 0)
	  memBuf[0] = 0; 
#endif
	// Link it to supporting objects 
	xmem->memId    = memSpec->memId;
	xmem->ldg      = xldg;
	xmem->grp      = xgrp;
  }

  // Load with accurate rep of it's location
  MemPackCSCZMap(&(xmem->cscz), &(memSpec->memLoc));

  // Load dates from passed in strings or pre-packed values
  if (memSpec->birthDate)      xmem->birthPack.pack = MemPackDate(memSpec->birthDate);
  else if (memSpec->birthPack) xmem->birthPack.pack = memSpec->birthPack;

  if (memSpec->registerDate)      xmem->registerPack.pack = MemPackDate(memSpec->registerDate);
  else if (memSpec->registerPack) xmem->registerPack.pack = memSpec->registerPack;

  if (memSpec->activeDate)      xmem->activePack.pack = MemPackDate(memSpec->activeDate);
  else if (memSpec->activePack) xmem->activePack.pack = memSpec->activePack;

  // Load in the rest of the vitals
  MemPackVitals(&(xmem->vitals), memSpec);

  if (!reuseOld) {
	// Disconnect old member instance from other chains
	if (memOld)
	  if ((err = xMemLinkOut(memOld)))
		goto error;
	// Now link me in
	if (!(xMemLinkIn(xmem, &mk)))
	  { err = "LinkIn"; goto error; }
	// Set old memory to expire when all active threads have passed
	if (memOld)
	  PermitExpireObj(gsv->memObjMgr, (ObjPtr)memOld);
  }
  
  if (myPermit)
    PermitWriteGive(myPermit);
  return xmem;

 error:
  if (myPermit)
    PermitWriteGive(myPermit);
  snprintf(memBuf, sizeof(memBuf), "%ld", mk.memId);
  PEngError(PKGNAME, memBuf, err);
  return NULL;
}

// Update dynamic attributes of a member that don't impact linkage
// *Need a search permit
//
Mem MemUpdate(MemUpdateSpec xspec)
{
  Mem xmem = NULL;
  PermitSearch myPermit = (PermitSearch)NULL;

  if (!gsv->mems || !xspec) 
	return NULL;

  // Take a search permit so no one else messes with our Mem
  if (PermitSearchTake(&myPermit))
	return NULL;

  if (xspec->debugSeq) { 
	// lookup by sequence
	xmem = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, xspec->debugSeq);
  } else { 
	// lookup by domid/memid
	MemKeyRec mk;
	StructNull(mk);
	mk.memId = xspec->memId;
	mk.domId = xspec->domId;
	xmem = (Mem)PHashFind(gsv->mems, (PHashKey)&mk);
  }

  if (xmem) {
	cSTR sval; US2 ival; US1 tmpAct;
	// Set the last active date, maybe updating synthetic saAct
	if (sval = xspec->activeDate) {
	  xmem->activePack.pack = (sval == (cSTR)-1) ? 0 : MemPackDate(sval);
	  tmpAct = RankLastLoginToSAAct(xmem->activePack.pack, NULL);
	} else if (ival = xspec->activePack) {
	  xmem->activePack.pack = (ival == (US2)-1) ? 0 : ival;
	  tmpAct = RankLastLoginToSAAct(xmem->activePack.pack, NULL);
	} else {
	  tmpAct = 0;
	}
	if (xspec->saAct == 0)
	  xspec->saAct = tmpAct;
	
	MemPackVitalsUpdate(&(xmem->vitals), xspec);
  }
  // Give back our permit
  PermitSearchGive(myPermit);
  return xmem;
}

// Delete a member
// *Need a WritePermit
//
int MemDelete(MemDeleteSpec xspec)
{
  Mem xmem;
  cSTR err = NULL;
  char memBuf[64];
  PermitWrite myPermit = (PermitWrite)0;

  if (!gsv->mems || !xspec) 
	{ err = "NULL"; goto error; }

  if (PermitWriteTake("MemDelete", &myPermit))
	{ err = "Couldn't Take Permit"; goto error; }

  if (xspec->debugSeq) { 
	// lookup by sequence
	xmem = (Mem)ObjMgrSeqToPtr(gsv->memObjMgr, xspec->debugSeq);
  } else { 
	// lookup by domid/memid
	MemKeyRec mk;
	StructNull(mk);
	mk.memId = xspec->memId;
	mk.domId = xspec->domId;
	xmem = (Mem)PHashFind(gsv->mems, (PHashKey)&mk);
  }
  
  if (!xmem)
	{ err = "NotFound"; goto error; }
  
  if ((err = xMemLinkOut(xmem)))
	goto error;
  
  if (xmem)
	PermitExpireObj(gsv->memObjMgr, (ObjPtr)xmem);
  
  PermitWriteGive(myPermit);
  return 0;

 error: 
  if (myPermit)
	PermitWriteGive(myPermit);
  snprintf(memBuf, sizeof(memBuf), "%s %ld", (err ? err : ""),
		   (xspec->debugSeq ? xspec->debugSeq : xspec->memId));
  PEngError(PKGNAME, memBuf, err);
  return -1;
}
