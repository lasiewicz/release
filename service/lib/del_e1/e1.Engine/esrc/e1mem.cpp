// C++ interface to loading new members
//

#include "e1cpp.h"

using namespace Matchnet::e1::Engine;

int E1Mem::load()
{
  if (!xspec.memId || !xspec.domId || !(xspec.genderMask & 3))
	return -1;
  else if (!(xmem = MemUpsert(&xspec)))		
	return -2;
  else
	return 0;
}

void E1Mem::setMDG(MemId memid, DomId domid, int genderMask)
{
  xspec.memId = memid;
  xspec.domId = domid;
  xspec.genderMask = genderMask;
}
void E1Mem::setMemId(MemId memid) { xspec.memId = memid; }
void E1Mem::setDomId(DomId domid) { xspec.domId = domid; }
void E1Mem::setGenderMask(int genderMask) { xspec.genderMask = genderMask; }

void E1Mem::setHeight(int height) { xspec.height = height; } // In inches
void E1Mem::setWeight(int weight) { xspec.weight = weight; } // In pounds
void E1Mem::setChildrenCount(int ccnt){ xspec.childrenCount = ccnt; }
void E1Mem::setHasPhoto(int hasPhoto) { xspec.hasPhoto = hasPhoto; }
void E1Mem::setIsOnline(int isOnline) { xspec.isOnline = isOnline; }
void E1Mem::setRelocateFlag(int flag) { xspec.relocateFlag = flag; }
void E1Mem::setSaPop(int saPop)       { xspec.saPop = saPop; }
void E1Mem::setSaAct(int saAct)       { xspec.saAct = saAct; }

void E1Mem::setLoc(float regLat, float regLng)
{ 
  xspec.memLoc.regLat = regLat; 
  xspec.memLoc.regLng = regLng; 
}
void E1Mem::setLoc(STR regCountry, STR regState, STR regCity, STR regZip)
{ 
  xspec.memLoc.regCountry  = regCountry;
  xspec.memLoc.regState    = regState;
  xspec.memLoc.regCity     = regCity;
  xspec.memLoc.regZip      = regZip;
}
void E1Mem::setLoc(STR regCountry, STR regState, STR regCity, STR regZip, STR regAreaCode)
{
  xspec.memLoc.regAreaCode = regAreaCode;
  setLoc(regCountry, regState, regCity, regZip);
}

void E1Mem::setDates(STR birthDate, STR registerDate, STR activeDate)
{
  if (birthDate)    xspec.birthDate = birthDate;
  if (registerDate) xspec.registerDate = registerDate;
  if (activeDate)   xspec.activeDate = activeDate;
}

int E1Mem::setTag(STR name, STR value)
{
  if (xcnt < (sizeof(xspec.tags)/sizeof(xspec.tags[0]))) {
	int ix = xcnt++;
	xspec.tags[ix].name  = name;
	xspec.tags[ix].value = value;
	return ix;
  } else {
	return -1;
  }
}
