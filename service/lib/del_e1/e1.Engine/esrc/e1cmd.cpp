// C++ interface to running commands and fetching XML
//

#include <stdio.h>
#include "e1cpp.h"

using namespace Matchnet::e1::Engine;

E1Xml::E1Xml()            { pxObj = PXmlNew((1<<18), 0, "e1.xsl"); } //TOFIX: wrap this
E1Xml::E1Xml(S4 dataMax)  { pxObj = PXmlNew(dataMax, 0, "e1.xsl"); }
E1Xml::~E1Xml()           { PXmlFree(pxObj); }

int E1Xml::cmd(STR cmdBuf)
{
  int errCode = CmdEval(cmdBuf, pxObj, 0, 0);
  return errCode;
}

int E1Xml::cmd(STR cmdBuf, int readOnly, S4 threadId)
{
  int errCode = CmdEval(cmdBuf, pxObj, readOnly, threadId);
  return errCode;
}

S4 E1Xml::show(void *file)
{
  S4 len;
  char *data = PXmlData(pxObj, &len);
  FILE *ff = (file ? (FILE*)file : stdout);
  fputs(data, ff);
  PXmlReset(pxObj);
  return len;
}

char * E1Xml::dataBuf()
{
  S4 len;
  return PXmlData(pxObj, &len);
}

S4 E1Xml::dataLen()
{
  S4 len;
  char *data = PXmlData(pxObj, &len);
  return len;
}

void E1Xml::reset()
{
  PXmlReset(pxObj);
}

