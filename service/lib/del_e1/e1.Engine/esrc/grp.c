// grp.c
// pnelson 11/15/2005
// Manage groups of tags as a single object

#define GRP_FRIEND
#define MEM_FRIEND // for XML routines
#define TAG_FRIEND // for XML routines
#include "peng.h"

#define PKGNAME PkgName_Grp

/****************/

/* GrpKey must be "fixed" before calling these methods
 * Hash index is munge of all Tag->seq numbers
 */
static US4 xGrpKeyToIdx(GrpKey tg)
{
  int i; US4 idx;

  idx = 0;
  for (i=0; i < tg->count; i++) {
	Tag  xtag = tg->tagKeys[i].tag;
	US4  xseq = TagSequence(xtag);
	idx ^= ((xseq*53 + i*997) * 17);
#ifdef PENGDEBUG
	if (idx == 0)
	  idx = 0;
#endif
  }
  idx %= kGrp_HashSize;
  return idx;
}

/* Shorter lists go first, then favor higher Tag->seq in corresponding position 
 */
static int xGrpKeyCmp(const GrpKey tg, const Grp xgrp)
{
  int i;
  if (tg->count != xgrp->tagCount)
	return (tg->count < xgrp->tagCount) ? -1 : 1;
  
  for (i = 0; i < tg->count; i++) {
	Tag ktag = tg->tagKeys[i].tag;
	US4 kseq = TagSequence(ktag);
	
	Tag xtag = xgrp->tagList[i];
	US4 xseq = TagSequence(xtag);
	
	if (kseq < xseq) return -1;
	if (kseq > xseq) return 1;
  }
  return 0;
}

/****************/


static void xGrpInitialize()
{
  if (!gsv->grps) {
	gsv->grps = PHashNew(PKGNAME, kGrp_HashSize, 
						 (PHashKeyToIdxFnc*)xGrpKeyToIdx, 
						 (PHashKeyCmpFnc  *)xGrpKeyCmp);
	gsv->grpSeqMgr = SeqMgrCreate(PKGNAME, kGrp_MaxGrps);
	if (!gsv->grps || !gsv->grpSeqMgr)
	  PEngError(PKGNAME, NULL, "Init");
  }
}

/* qsort helper function to organize list of tags
 */
static int xGrpSortCmp(const TagKey t1, const TagKey t2)
{
  US4 seq1 = TagSequence(t1->tag);
  US4 seq2 = TagSequence(t2->tag);
  if (seq1 < seq2) return -1;
  if (seq1 > seq2) return 1;
  return 0;
}

/* Fix up a GrpKey by auto creating tags as needed, then sorting into cannonical order
 */
static void xGrpFix(GrpKey tg)
{
  int i;
  if (tg->fixed == 0) {
	// Must create the tags if they don't exist
	for (i=0; i<tg->count; i++) {
	  TagKey xkey = &(tg->tagKeys[i]);
	  if (!xkey->tag)
		xkey->tag = TagFindOrNew(xkey);
	}
	// Now sort them
	qsort((void*)tg->tagKeys, tg->count, sizeof(tg->tagKeys[0]), 
		  (QSortCmpFnc)xGrpSortCmp);
	tg->fixed = 1;
  }
}

/****************/

GrpKey GrpKeyNewFromMemNV(MemNV nv, int num)
{
  // ??? Only alloc proper size
  GrpKey gk = PEngAlloc(PKGNAME, sizeof(GrpKeyRec));
  if (nv && num > 0) {
	int i;
	for (i = 0; i < num && i < kTag_TextMaxs; i++) {
	  STR src; char *dst; int cnt = 0;
	  
	  src = nv[i].name; dst = gk->tagKeys[i].name;
	  if (src && src[0]) { cnt++; str0ncpy(dst, src, kTag_TextMaxName); }
	  
	  src = nv[i].value; dst = gk->tagKeys[i].value;
	  if (src && src[0]) { cnt++; str0ncpy(dst, src, kTag_TextMaxValue); }
	  
	  if (cnt == 0) break;  // No values
	}
	gk->count = i;
  }
  return gk;
}

void GrpKeyFree(GrpKey gk)
{
  if (gk) PEngFree(PKGNAME, gk, sizeof(GrpKeyRec));
}

/****************/

S4 GrpSequence(Grp xgrp)
{
  if (xgrp)
	return xgrp->sequence;
  else
	return 0;
}

Grp GrpBySequence(S4 seq)
{
  return (Grp)SeqMgrLookup(gsv->grpSeqMgr, seq);
}

Grp GrpFindOrNewFromMemNV(MemNV nv, int num)
{
  GrpKey tg = GrpKeyNewFromMemNV(nv, num);
  Grp    xgrp = GrpFindOrNew(tg);
  GrpKeyFree(tg);
  return xgrp;
}

/* qsort helper function to organize list of tags
 */
static int xTagCmp(const Tag *t1, const Tag *t2)
{
  US4 seq1 = TagSequence(*t1);
  US4 seq2 = TagSequence(*t2);
  if (seq1 < seq2) return -1;
  if (seq1 > seq2) return 1;
  return 0;
}

Grp GrpFindOrNewFromTags(Tag *tags, int num)
{
  int i;
  GrpKeyRec gk;

  if (num > ArrayCount(gk.tagKeys)) {
	PEngError(PKGNAME, "TooManyTags", NULL);
	num = ArrayCount(gk.tagKeys);
  }
  if (num > 1)
	qsort((void*)tags, num, sizeof(Tag), (QSortCmpFnc)xTagCmp);

  // Put them in already sorted
  StructNull(gk);
  for (i=0; i<num; i++)
	gk.tagKeys[i].tag = tags[i];
  gk.count = i;
  gk.fixed = 1;

  return GrpFindOrNew(&gk);
}

Grp GrpFindOrNew(GrpKey tg)
{
  Grp xgrp;
  int i;
  
  if (!gsv->grps) xGrpInitialize();
  if (!tg)
	return NULL;
  
  // Cannonicalize the key
  xGrpFix(tg);
  
  // See if we have it already
  if ((xgrp = (Grp)PHashFind(gsv->grps, (PHashKey)tg)))
	return xgrp;
  
  // Allocate
  { 
	US4 sSize = StructSizeAlign(GrpRec);
	US4 tSize = tg->count * sizeof(Tag);
	US4 gSize = tg->count * sizeof(Grp);
	
	xgrp = (Grp)PEngAlloc(PKGNAME, (sSize + tSize + gSize));
	xgrp->tagList = (Tag*)((US1*)xgrp + (sSize));
	xgrp->grpNext = (Grp*)((US1*)xgrp + (sSize + tSize));
	
	xgrp->sequence = SeqMgrPush(gsv->grpSeqMgr, (SeqObj)xgrp);
  }
  
  // Load it
  xgrp->tagCount = tg->count;
  for (i=0; i<tg->count; i++)
	xgrp->tagList[i] = tg->tagKeys[i].tag;
  
  // Now that object internally consistent, add it the primary hash
  if (!PHashPush(gsv->grps, (PHashKey)tg, (PHashObj)xgrp))
	goto error;
  
  // Now that it really exists, link it in as parent of every contained tag
  // !!! Order dependent assignment to preserve consistency
  for (i=0; i<tg->count; i++) {
	Tag xtag = xgrp->tagList[i];
	Grp *root = TagChainGrpRoot(xtag);
	xgrp->grpNext[i] = *root;
	*root = xgrp;
  }
  return xgrp;
 error:
  PEngError(PKGNAME, NULL, "FindOrNew");
  return NULL;
}

Mem *GrpChainMemRoot(Grp xgrp)
{
  if (!xgrp) return NULL;
  return &(xgrp->memChain);
}

US4 GrpCountUpdate(Grp xgrp, S4 amt)
{
	if (xgrp) {
		int i;
		for (i=0; i < xgrp->tagCount; i++) {
			Tag xtag = xgrp->tagList[i];
			xtag->memCount += amt;
		}
		return (xgrp->memCount += amt);
	} else {
		return 0;
	}
}
// ****************************************************************

int GrpXml(PXml px, Grp xgrp, int mode)
{
  int err, i;
  char msgBuf[64];

  snprintf(msgBuf, sizeof(msgBuf), "Grp memcount='%ld'", xgrp->memCount);
  if ((err = PXmlPush(px, msgBuf, xgrp->sequence)) < 0)
	return err;
  
  if (!err && mode >= 1) {
	snprintf(msgBuf, sizeof(msgBuf), "Tags count='%d'", xgrp->tagCount);
	err |= PXmlPush(px, msgBuf, 0);
	for (i=0; i<xgrp->tagCount; i++) {
	  Tag xtag = xgrp->tagList[i];
	  err |= TagXml(px, xtag, 0);
	}
	err |= PXmlPop(px);
  }
  
  if (!err && mode >= 2)
	err |= MemXmlGrpChain(px, xgrp->memChain, 0);
  
  err |= PXmlPop(px);
  return err;
}

int GrpXmlTagChain(PXml px, Grp xgrp, Tag xtag)
{
  int err = 0;
  
  while (xgrp && !err) {
	err |= PXmlPush0(px, "Grp", xgrp->sequence);
	xgrp = GrpFollow(xgrp, xtag, 0);
  }
  return err;
}

#define MAXHISTO 256

static int xGrpXmlHisto(PXml px, S4 *histo, cSTR name, S4 longest)
{
  char msgBuf[128]; 
  int i, err = 0;
  
  // Show histo of our tagCounts
  snprintf(msgBuf, sizeof(msgBuf), "Histo%s", name);
  err = PXmlPush(px, msgBuf, 0);
  for (i = 0; i < MAXHISTO; i++) {
	if (histo[i]) {
	  snprintf(msgBuf, sizeof(msgBuf), "Chain len='%ld' cnt='%ld'", i, histo[i]);
	  err |= PXmlPush0(px, msgBuf, 0);
	}
  }
  if (histo[MAXHISTO]) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "LongChains longest='%ld' cnt='%ld'", longest, histo[MAXHISTO]);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  err |= PXmlPop(px);
  return err;
}


// XML dump of stats for the whole package
//
int GrpPkgXml(PXml px, int mode)
{
  int err;
  S4 seq, tagCountLongest, memCount, memCountLongest;
  S4 tagCountHisto[MAXHISTO+1], memCountHisto[MAXHISTO+1];
  Grp xgrp; Mem xmem;

  StructNull(tagCountHisto);
  StructNull(memCountHisto);
  tagCountLongest = memCountLongest = 0;

  for (seq=1; (xgrp = GrpBySequence(seq)); seq++) {
	
	if (xgrp->tagCount < MAXHISTO)
	  tagCountHisto[xgrp->tagCount] += 1;
	else {
	  tagCountHisto[MAXHISTO] += 1;
	  if (tagCountLongest < xgrp->tagCount)
		tagCountLongest = xgrp->tagCount;
	}

	for (memCount=0, xmem=xgrp->memChain; xmem; xmem = xmem->grpChain) memCount++;

	if (memCount >= 0 && memCount < MAXHISTO)
	  memCountHisto[memCount] += 1;
	else {
	  memCountHisto[MAXHISTO] += 1;
	  if (memCountLongest < memCount)
		memCountLongest = memCount;
	}
  }

  // Show histo of our tagCounts

  err = PXmlPush(px, "GrpPkgAnalysis", seq-1);
  err |= xGrpXmlHisto(px, tagCountHisto, "TagCount", tagCountLongest);
  err |= xGrpXmlHisto(px, memCountHisto, "MemCount", memCountLongest);
  err |= PXmlPop(px);

  return err;
}

Grp GrpFollow(Grp xgrp, Tag xtag, int verbose)
{
  int j;
  if (!xgrp || !xtag)
	return NULL;

  for (j=0; j<xgrp->tagCount; j++)
	if (xgrp->tagList[j] == xtag)
	  return xgrp->grpNext[j];

  if (verbose) {
	cSTR name  = StxDbById(gsv->pkTags, xtag->nxId);
	cSTR value = StxDbById(gsv->pkTags, xtag->vxId);
	char msgBuf[128];
	snprintf(msgBuf, sizeof(msgBuf), "TagGrpInconsistent (%s:%s) Grp=%ld", 
			 name, value, xgrp->sequence);
	PEngLog(PKGNAME, msgBuf, NULL, 0);
  }
  return NULL;
}
