// peng.h
// pnelson, 11/13/2005
// Internal Engine definitions

#define PENGDEBUG 1 // For now

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

#define snprintf _snprintf
#define strlwr   _strlwr

#define INCLUDE_PENG

#ifndef INCLUDE_PAPI
#include "papi.h"
#endif

typedef struct _Gsv      *Gsv;
typedef struct _PHash    *PHash;
typedef struct _PHashObj *PHashObj;
typedef struct _Tag      *Tag;
typedef struct _Grp      *Grp;
typedef struct _Ldg      *Ldg;

typedef S4                Stx;
typedef struct _StxDb    *StxDb;

typedef struct _Loc {
  US2 lng;
  US2 lat;
} LocRec, *Loc;

/***************/
/** Key Sizes **/
/***************/

#define kPI (float)(3.14159265)
#define kLoc_EWBoxes (1L << 12)
#define kLoc_fLngMin (-kPI)      // ??? Longitude spans 2pi around globe, wraps around
#define kLoc_fLngMax ( kPI)      // 

#define kLoc_NSBoxes (1L << 11)
#define kLoc_fLatMin (-(kPI/3))  // ??? north pole is Pi/2, artic circle is pi/3 
#define kLoc_fLatMax ( (kPI/3))  // 

#define kTag_HashPower   12
#define kTag_HashSize   (1L << kTag_HashPower)
#define kTag_HashShift  (kTag_HashPower - 4)
#define kTag_MaxTags    (1L << 20)
#define kStx_TagBits    20       // up to 1 million unique tag string

#define kGrp_HashPower   18
#define kGrp_HashSize   (1L << kGrp_HashPower)
#define kGrp_MaxGrps    (1L << 23)

#define kLdg_HashPower   16
#define kLdg_HashSize   (1L << kLdg_HashPower)
#define kLdg_MaxLdgs    (1L << 20)
#define kLdg_IterBandMax (kLoc_EWBoxes/50)  // Max boxes we'll look (2% of globe in each dir)

#define kMem_HashPower   18
#define kMem_HashSize   (1L << kMem_HashPower)
#define kMem_MaxMems    (1L << 24)


/****************/

#ifndef NULL
#define NULL 0
#endif

/**************/
/** Packages **/
/**************/

typedef enum {
	PkgName_Unknown,
	PkgName_Eng,
	PkgName_Cmd,
	PkgName_PHash,
	PkgName_ObjMgr,
	PkgName_SeqMgr,
	PkgName_Stx,
	PkgName_PMap,
	PkgName_Tag,
	PkgName_TagDb,
	PkgName_Grp,
	PkgName_Ldg,
	PkgName_Mem,
	PkgName_PXml,
	PkgName_S1,
	PkgName_PRes,
	PkgName_Mob,
	PkgName_Order,
	PkgName_Load
} PkgName;

#define kPkg_NameMax ((int)PkgName_Load+1)

/************/
/** Macros **/
/************/

#define StructSize(ss)       sizeof(ss)
#define StructSizeAlign(ss)  ((sizeof(ss) + 7) & ~7)
#define StructNull(ss)       memset((void*)&(ss), 0, sizeof(ss))
#define ArrayCount(sar)      (sizeof(sar)/sizeof((sar)[0]))

#define minmax(v,n,x) ((v) < (n) ? (n) : ((v) > (x) ? (x) : (v)))

typedef int (*QSortCmpFnc)(const void *, const void *);

extern char *str0ncpy(char *dst, const char *src, int dstSize);

/*********************/
/** Utility methods **/
/*********************/

extern void  PEngError (PkgName pkg, cSTR obj, cSTR err);
extern void  PEngLog   (PkgName pkg, cSTR fmt, cSTR strarg, S4 intarg);
extern void  PEngLoadError(PkgName pkg, cSTR file, S4 lineNum, MemId memid, cSTR errName, cSTR data);
extern void *PEngAlloc (PkgName pkg, US4 size);
extern void  PEngFree  (PkgName pkg, void *ptr, US4 size);

// External PXml entry points in papi.h
extern int  PXmlPush0  (PXml px, cSTR tag, S4 seq);
extern int  PXmlPush   (PXml px, cSTR tag, S4 seq);
extern int  PXmlPop    (PXml px);
extern int  PXmlSetStr (PXml px, cSTR name, cSTR value);
extern int  PXmlSetNum (PXml px, cSTR name, S4 value);
extern int  PXmlInsertStr(PXml px, cSTR str);
extern int  PXmlEnumMax(PXml px);


/**************************************/
/** Fixed object size memory manager **/
/**************************************/

typedef struct _ObjMgr *ObjMgr;
typedef struct _ObjPtr *ObjPtr;

extern ObjMgr ObjMgrCreate  (PkgName pkg, int objSize, S4 maxObjs);
extern S4     ObjMgrSequence(ObjMgr xom);
extern void   ObjMgrDestroy (ObjMgr xom);
extern ObjPtr ObjMgrNew     (ObjMgr xom, S4*);
extern void   ObjMgrFree    (ObjMgr xom, ObjPtr obj);
extern S4     ObjMgrPtrToSeq(ObjMgr xom, ObjPtr obj);
extern ObjPtr ObjMgrSeqToPtr(ObjMgr xom, S4 seq);
extern int    ObjMgrXml(PXml px, ObjMgr xom, int mode);

typedef struct _SeqMgr *SeqMgr;
typedef void   *SeqObj;
extern SeqMgr SeqMgrCreate(PkgName pkg, S4 maxObjs);
extern S4     SeqMgrSequence(SeqMgr xom);
extern void   SeqMgrDestroy(SeqMgr xom);
extern S4     SeqMgrPush(SeqMgr xom, SeqObj obj);
extern SeqObj SeqMgrLookup(SeqMgr xom, S4 seq);
extern int    SeqMgrPop(SeqMgr xom, S4 seq);
extern int    SeqMgrXml(PXml px, SeqMgr xom, int mode);

/********************/
/** Protected Hash **/
/********************/

typedef US4   PHashIdx;     /* The index into the hash table */ 
typedef void *PHashKey;     /* The value that was hashed     */

/* Base class of hashable objects 
 */ 
typedef struct _PHashObj {
	PHashObj     hashNext;
} PHashObjRec;

/* Virtual method to generate hash index from the key 
 */ 
typedef PHashIdx PHashKeyToIdxFnc(PHashKey);

/* Virtual method to compare a key to an already hashed object 
 */
typedef int      PHashKeyCmpFnc  (PHashKey, PHashObj);  

/* Hash table methods
 */
extern PHash    PHashNew(PkgName pkg, US4 maxHashIdx, 
							           PHashKeyToIdxFnc *keyToIdxFnc, 
							           PHashKeyCmpFnc   *keyCmpFnc);
extern void     PHashFree (PHash phash);
extern PHashObj PHashFind (PHash phash, PHashKey xkey);
extern PHashObj PHashPush (PHash phash, PHashKey xkey, PHashObj xobj);
extern PHashObj PHashPop  (PHash phash, PHashKey xkey, PHashObj xobj);

/*********************/
/** Strings to Keys **/
/*********************/

extern StxDb  StxDbNew(US1 maxSizeBits, cSTR name);
extern void   StxDbFree(StxDb xpkc);
extern Stx    StxDbFindOrNew(StxDb xpkc, cSTR value);
extern Stx    StxDbFind(StxDb xpkc, cSTR value);
extern cSTR   StxDbById(StxDb xpkc, Stx xid);
extern S4     StxDbCount(StxDb xpkc);

/*********************/
/** Bulk CSV loader **/
/*********************/

extern void        LoadInitialize(void);

// Opaque handle
typedef struct _LoadPattern *LoadPattern;

// Codes for load status
typedef enum {
  LoadStatus_Ok = 0,
  LoadStatus_MemIdMissing = 1,
  LoadStatus_DomIdMissing = 2,
  LoadStatus_GenMissing   = 3,
  LoadStatus_TagsTooMany  = 4
} LoadStatusCode;
  
// Status of loading this member
typedef struct _LoadStatusInst {
  S4 lineNum; 
  S4 memId; 
  US1 domId;
  US1 gender;
  S2 status;
} LoadStatusInstRec, *LoadStatusInst;

// All the status as list of lists
typedef struct _LoadStatus *LoadStatus;
typedef struct _LoadStatus {
  LoadStatus  nextPage;
  S4 numInst;
  S4 maxInst;
  LoadStatusInstRec *datInst;
} LoadStatusRec;

#define ERR_Load_Pattern         -(((int)PkgName_Load*1000)+1)
#define ERR_Load_Permit          -(((int)PkgName_Load*1000)+2)

extern LoadPattern LoadPatternNew(cSTR fname, char *header);
extern int         LoadPatternLoad(LoadPattern xlp, MemSpec memSpec, char *data, S4 lineNum);
extern LoadStatus  LoadPatternStatus(LoadPattern xlp);
extern void        LoadPatternFree(LoadPattern xlp);

/*************************/
/** Global state holder **/
/*************************/

typedef struct _Gsv {
  S4 initialized;
  
  // All our tags
  PHash  tags;
  ObjMgr tagObjMgr;
  void  *tagDb;
  StxDb  pkTags;
  
  // All our tag groups
  PHash  grps;
  SeqMgr grpSeqMgr;
  
  // All our Location/Domain/Gender locators
  PHash  ldgs;
  ObjMgr ldgObjMgr;
  // All our Member structures
  PHash  mems;
  ObjMgr memObjMgr;
  // The String<>ID mapping tables
  StxDb  pkCountry;
  StxDb  pkState;
  StxDb  pkCity;
  StxDb  pkZip;
  // Loader state
  int    loaderInit;
  
  // Bitmap of the world to optimize "near by" scans
  void *pmap;
  
  STR pkgNames[kPkg_NameMax];
  US4 pkgMemory[kPkg_NameMax];
  US4 pkgAllocs[kPkg_NameMax];
  
  // Debugging fields
  void *dbgOutput, *logOutput, *loadOutput; 
  char lastErr[256];
  
} GsvRec;

extern Gsv gsv;
extern STR GsvPkgNameAsStr(PkgName pkg);


extern int  E1CfgInitialize(cSTR source);
extern void E1CfgDestroy();
extern cSTR E1CfgGetStr(cSTR name);
extern S4   E1CfgGetInt(cSTR name);

/****************************************/
/** Permits for thread synchronization **/
/****************************************/

#define ERR_Permit_Null         -101
#define ERR_Permit_Inited       -102
#define ERR_Permit_MutexCreate  -103

#define ERR_PermitWrite_Timeout  -120
#define ERR_PermitWrite_Abandon  -121
#define ERR_PermitWrite_Unknown  -122
#define ERR_PermitWrite_NotOwner -123

#define ERR_PermitSearch_Null     -110
#define ERR_PermitSearch_NoneLeft -111
#define ERR_PermitSearch_InUse    -112
#define ERR_PermitSearch_NotInUse -113

#define WARN_PermitSearch_InUse    112

typedef struct _PermitSearch *PermitSearch;
typedef long                  PermitWrite;

extern int  PermitInitialize(int searchCount);
extern void PermitDestroy();

extern int PermitSearchTake(PermitSearch *permit);
extern int PermitSearchGive(PermitSearch permit);
extern int PermitSearchBeg (PermitSearch permit);
extern int PermitSearchEnd (PermitSearch permit);

extern int PermitWriteTake(cSTR name, PermitWrite *seqp);
extern int PermitWriteGive(PermitWrite seq);
extern int PermitWriteConfirm(PermitWrite seq);

extern void PermitExpirePtr(PkgName pkg, void *ptr, US4 size);
extern void PermitExpireObj(ObjMgr xom, ObjPtr obj);
extern int  PermitExpireExec();

/************************************/
/** Location/Domain/Gender Manager **/
/************************************/

typedef struct _LdgKey {
	LocRec   loc;
	DomId    domId;
	US1      gender;
} LdgKeyRec, *LdgKey;

typedef struct _Ldg {
	Ldg       hashNext;   // Next LDG in hash
	Mem       memChain;   // Head of chain of members at this loc/dom/gen
	LdgKeyRec ldgKey;     // The loc/dom/gen of this chain
	US4       memCount;     // How many members
} LdgRec;

typedef struct _LdgIter {
	LdgKeyRec ldgKey;    // Where we're looking
	LocRec    locBase;   // The origin
	S4  scanLng;    // Last point we looked at
	S4  scanLat;    //   must be S4 to handle wraparound below zero
	US2 band;       // How far from center are we
	US2 maxBand;    // How far will we go
	US2 side;       // Which side of the box are we scanning 
	US2 sidx;       // where are we on that side
	Ldg cache;      // cache the next to return if known
} LdgIterRec, *LdgIter;

extern S4   LdgSequence(Ldg xldg);
extern Ldg  LdgBySequence(S4 seq);
extern Ldg  LdgFind(LdgKey lk);
extern Ldg  LdgFindOrNew(LdgKey lk);
extern Mem *LdgChainMemRoot(Ldg xldg);
extern void LdgIterInit(LdgIter iter, Loc loc, DomId domid, int gender, int maxBand);
extern Ldg  LdgIterNext(LdgIter iter);

/*****************************************************/
/** Map of all occupied regions to accelerate scans **/
/*****************************************************/

typedef struct _PMapIter {
	LocRec loc;   // The origin
	LocRec next;  // Point we are returning
	S4  scanLng;  // Last point we looked at
	S4  scanLat;  //   must be S4 to handle wraparound below zero
	US2 band;       // How far from center are we
	US2 maxBand;    // How far will we go
	US2 side;       // Which side of the box are we scanning 
	US2 sidx;       // where are we on that side
	// Cached Cell to minimize lookups
	void *cell;
	US2   cellEW;
	US2   cellNS;
} PMapIterRec, *PMapIter;

/*****************************/
/** Internal Object Methods **/
/*****************************/

#define kTag_TextMaxName  64
#define kTag_TextMaxValue 64
#define kTag_TextMax      (kTag_TextMaxName + kTag_TextMaxValue)
#define kTag_TextMaxs     128

typedef struct _TagKey {
	char   name [kTag_TextMaxName];
	char   value[kTag_TextMaxValue];
	Stx   nId, vId; // Ptrs to IDs of strings when available
	Tag    tag;      // Ptr to actual tag when available
} TagKeyRec, *TagKey;

#ifdef TAG_FRIEND
typedef struct _Tag {
  Tag   hashNext;    // Next Tag in hash
  S4    sequence;    // Store explicitly since seq used when sorting tags
  Grp   grpChain;    // Head of chain of Grps that use this tag
  Stx   nxId, vxId;
  US4   memCount;     // How many members
} TagRec;
#endif

extern int TagInitialize();
extern Tag  TagFind(TagKey nv);
extern Tag  TagBySequence(S4 seq);
extern S4   TagSequence(Tag xtag);
extern void TagNameValue(Tag xtag, cSTR *namep, cSTR *valuep);
extern Tag  TagFindOrNew(TagKey nv);
extern Tag  TagFindOrNewStr(cSTR name, cSTR value);
extern Grp *TagChainGrpRoot(Tag xtag);
extern S4   TagCount(Tag xtag, S4 *grpCntp);

typedef enum {
  TagDb_Unknown,
  TagDb_Mask,    // codes are (multiple) bits in a mask, each representing a value
  TagDb_Id,      // codes are integers representing a value
  TagDb_Value    // codes are the actual values
} TagDbCoding;

typedef S4 TagDbId;

extern TagDbId TagDbFind(cSTR name);
extern TagDbId TagDbFindOrNew(cSTR name, TagDbCoding coding, int noAutoCreate);
extern Tag     TagDbTagAssign(TagDbId tdx, cSTR strVal, US4 code);
extern int     TagDbByCode(TagDbId tdx, US4 code, Tag *tagList);
extern S4      TagDbInternal(TagDbId tdx, S4 newData);
extern int     TagDbXml(PXml px, TagDbId tdx, int mode);

#ifdef GRP_FRIEND
typedef struct _Grp {
  Grp     hashNext;   // Next Grp in hash
  S4      sequence;   // Sequence number of this Grp
  Tag     *tagList;   // List of actual tags in this Grp, ordered by Tag->seq
  Mem     memChain;   // Head of chain of members that use this Grp
  Grp     *grpNext;   // For each tag, ptr to next group that also uses this tag
  US4     memCount;     // How many members
  US2     tagCount;   // Count of tags in the group
} GrpRec;
#endif

typedef struct _GrpKey {
	Grp tagGrp; /* Ptr to actual taggrp when available */
	US2 count; US1 fixed; US1 xalign;
	TagKeyRec tagKeys[kTag_TextMaxs];
} GrpKeyRec, *GrpKey;

extern Grp  GrpBySequence(S4 seq);
extern S4   GrpSequence(Grp xgrp);
extern GrpKey  GrpKeyNewFromMemNV(MemNV nv, int num);
extern void    GrpKeyFree(GrpKey gk);
extern Grp  GrpFindOrNewFromMemNV(MemNV nv, int num);
extern Grp  GrpFindOrNewFromTags(Tag *tags, int num);
extern Grp  GrpFindOrNew(GrpKey tg);
extern Mem *GrpChainMemRoot(Grp xgrp);
extern US4  GrpCountUpdate(Grp xgrp, S4 amt);
extern Grp  GrpFollow(Grp xgrp, Tag xtag, int verbose);


/***********************/
/** Member defintions **/ 
/***********************/

// Lookup a member by her ID in the master hash
//
typedef struct _MemKey {
	MemId memId;
	DomId domId;
} MemKeyRec, *MemKey;

extern void MemInitialize();
extern S4  MemSequence(Mem xmem);
extern Mem MemBySequence(S4 seq);
extern S1  MemGender(S1 inp);
extern Mem MemFind(MemKey mk);

// Max Sizes for LocationString <> ID mapping tables
#define kStx_CountryBits   9 
#define kStx_StateBits    15
#define kStx_CityBits     22
#define kStx_ZipBits      22
#define kStx_AreaCodeBits 10

// Bit packed struct to store IDs of location strings in minimal (8 bytes) space
typedef struct _MemCSCZMap {
  unsigned pkCountry : kStx_CountryBits;
  unsigned pkState   : kStx_StateBits;
  unsigned pkCity    : kStx_CityBits;
  unsigned pkZip     : kStx_ZipBits;
  unsigned pkArea    : kStx_AreaCodeBits;
} MemCSCZMapRec, *MemCSCZMap;

typedef union _MemDate {
  US2 pack;
  struct {
	unsigned day   : 5;    // 1 based day of month
	unsigned month : 4;    // 1 based month
	unsigned year  : 7;    // 1900 + 128 years
  } v;
} MemDateRec, *MemDate;

#define kMemPack_YearMin  1900  // Min year to fit in 7 bits
#define kMemPack_YearRng   127  // Min year to fit in 7 bits
#define kMemPack_HeightMin  122  // Min height in cm to fit in 7 bits
#define kMemPack_HeightRng  120  // Range of cm that fit in 7 bits
#define kMemPack_WeightMin  75  // Min weight in pounds to fit in 8 bits
#define kMemPack_WeightRng 255  // Min weight in pounds to fit in 8 bits

// Bit packed struct to store all the other vitals
//
typedef struct _MemVitals {
  unsigned saPop    : 4;    // synthetic popularity
  unsigned saAct    : 4;    // synthetic activity

  unsigned height   : 7;    // ins above 3 feet
  unsigned hasPhoto : 1;    // has a photo

  unsigned weight   : 8;    // pounds over 75

  unsigned childCnt : 2;
  unsigned isOnline : 1;    // is online now
  unsigned gayMask  : 1;
  unsigned relocate : 2;

} MemVitalsRec, *MemVitals;

#ifdef MEM_FRIEND
typedef struct _Mem {
  Mem    hashNext;   // Next member in hash by memberId
  
  MemId  memId;      // My MemId
  Ldg    ldg;        // Head of LDG chain, with loc/dom/gen
  Mem    ldgChain;   // Next member in chain at this loc/dom/gen
  
  Grp    grp;        // Tag group with all my attributes
  Mem    grpChain;   // Next member in chain w/ this Grp

  MemDateRec birthPack;
  MemDateRec registerPack;
  MemDateRec activePack;

  MemCSCZMapRec cscz;   // Packed bitfield of IDs of Country/State/City names & Zipcode
  MemVitalsRec  vitals; // Packed bitfield of Member's key info

} MemRec;

// Macros to pull common items out of embedded Mem structures
#define xMemMemId(mm)   ((mm)->memId)
#define xMemLoc(mm)     ((mm)->ldg->ldgKey.loc)
#define xMemDomId(mm)   ((mm)->ldg->ldgKey.domId)
#define xMemGender(mm)  ((mm)->ldg->ldgKey.gender)

#endif

extern int  MemPackGender (int intGmask, US1 *gayp);
extern US2  MemPackDate   (cSTR dateStr);
extern STR  MemPackDateStr(cSTR name, US2 pack, char *buf);
extern S4   MemPackDateAsDays(US2 pack);
extern int  MemPackLocMilesAsBands(int miles);
extern void MemPackLoc    (Loc, MemLoc);
extern void MemPackVitals (MemVitals, MemSpec);
extern void MemPackVitalsUpdate(MemVitals, MemUpdateSpec);
extern void MemPackCSCZMap(MemCSCZMap, MemLoc);

/*********************/
/** XML serializers **/
/*********************/

extern int TagXml(PXml px, Tag xtag, int mode);
extern int GrpXml(PXml px, Grp xgrp, int mode);
extern int LdgXml(PXml px, Ldg xldg, int mode);
extern int MemXml(PXml px, Mem xmem, int mode);
extern int GrpXmlTagChain(PXml px, Grp xgrp, Tag xtag);
extern int MemXmlGrpChain(PXml px, Mem xmem, int mode);
extern int MemXmlLdgChain(PXml px, Mem xmem, int mode);
extern int GsvXml  (PXml px, int mode);
extern int StxDbXml(PXml px, StxDb xpkc, int mode);
extern int PHashXml(PXml px, PHash phash, US4 sequence, int mode);

extern int GrpPkgXml(PXml px, int mode);

/********************/
/** Rank functions **/
/********************/

extern PRes PResNew(int target);
extern int  PResInsert(PRes xres, MemId memid, US2 score);
extern int  PResLoad(PRes pres, S1Result sres);
extern void PResFree(PRes xres);
extern int  PResXml (PXml px, PRes xres, int mode);

#define kRank_NearBandsMax 16
#define kRank_AgoBandsMax  16

typedef struct _RankTuneNear {
	S1Score inCountry, inState, inCity, inZip;  // Score if direct hit
	S1BlendRec      blendCscz;    // How much to blend direct hit 
	S1BlendRec      blendLdg;     // How much to blend box distance
	struct { 	            // First and last band must have dst=0, start scan from second
		S4       outerDst;  // (box distance)^2 of farthest point of band
		S1Score outerScr;  // score at farthest point of band, assume 100 at center 
	} bands[kRank_NearBandsMax];
} RankTuneNearRec, *RankTuneNear;

#define kRank_AgeVctMax 100
#define kRank_AgeMin     18
#define kRank_AgeMax     99

typedef struct _RankTuneAge {   // score is 100 for center of age band
  S1Score  inBandLow;               // Lowest score (at inner edge) of requested age band
  S1Score  outBandHi;               // Highest Score (at ouer edge) of requested age band
  struct { US1 pctDif, perYear; } ynger[6];  // penalty for each pct range 
  struct { US1 pctDif, perYear; } older[6];
} RankTuneAgeRec, *RankTuneAge;

typedef struct _RankTuneHgt {
	S1Score  inBand;             // score at edge of prefered hgt band, else all 100
	S1Score  outBandMax;         // Best score for someone out of band
	S1Score  perInch;            // deduction per inch of height difference from band
} RankTuneHgtRec, *RankTuneHgt;

typedef struct _RankTuneAgo {
  struct {         // First and last band must have days=0, start scan from second
	US2      days;   // how many days at this resolution
	S1Score score;  // score at end of those days
  } bands[kRank_AgoBandsMax];
} RankTuneAgoRec, *RankTuneAgo;

typedef struct _RankTuneAct {
  US2 days[16];  // score for # of days ago last logged in
} RankTuneActRec, *RankTuneAct;

typedef struct _RankTune {
    MemDateRec   today;      // Todays date
    S4           todayDays;  // Number days from beginning to today
    S1Scores     s1blend;
    RankTuneAgo  ago;
	RankTuneNear near;
	RankTuneAge  age;
	RankTuneHgt  hgt;
	RankTuneAct act;
} RankTuneRec, *RankTune;

extern RankTuneRec gsvRank;

extern US2 RankPackAge(int age);
extern int RankPackHgt(int hgt);
extern int RankPackWgt(int wgt);

extern S4      RankLdgDstCalc(Loc xscan, Loc ldg);
extern S1Score RankNearLdgDst(S4 ldgDst, RankTune rt);
extern S1Score RankNearExec(MemCSCZMap xmem, MemCSCZMap scan, RankTune rt);
extern void    RankAgeInit(US1 *byAge, US1 amin, US1 amax, RankTuneAge ra);
extern S1Score RankByTimeExec(US2 thenPack, RankTune rt);
extern S1Score RankHgtExec (int xmem, int vmin, int vmax, RankTune rt);
extern int     RankLastLoginToSAAct(US2 actPack, RankTune rt);
