// e1cpp.h

#pragma once

#if !defined ( INCLUDE_PAPI )
	extern "C"
	{
		#include "papi.h"
	};
#endif

namespace Matchnet
{
	namespace e1
	{
		namespace Engine
		{
			class E1Gsv  // Placehold class for all global things
			{
			public:
				E1Gsv(char *name);
				~E1Gsv(void);
				int main(int argc, char **argv);  // Console main
			};


			class E1Mem // Describe then load a member
			{
			private:
				MemSpecRec xspec;  // MemSpec record to pass to load
				int        xcnt;   // How many tags we've loaded so far
				Mem        xmem;   // Ptr to mem eventually returned from load
			public:
				int load(void);  // Load the member once it's been described
				// Lots of calls to describe a member before loading  memid/domid/gendermask required
				void setMDG(MemId memid, DomId domid, int genderMask);
				void setMemId(MemId memid);
				void setDomId(DomId domid);
				void setGenderMask(int genderMask);

				void setLoc(float regLat, float regLng);
				void setLoc(STR regCountry, STR regState, STR regCity, STR regZip);
				void setLoc(STR regCountry, STR regState, STR regCity, STR regZip, STR regAreaCode);

				void setDates(STR birthDate, STR registerDate, STR activeDate);

				void setHeight(int height);         // In inches
				void setWeight(int weight);         // In pounds

				void setChildrenCount(int childrenCount);
				void setHasPhoto(int hasPhoto);
				void setIsOnline(int isOnline);
				void setRelocateFlag(int relocateFlag);
				void setSaPop(int saPop);
				void setSaAct(int saAct);
				int  setTag(STR name, STR value);  // Set Name:Value tags
			};

			class E1Xml // Process a command in URL syntax, and get answer as XML
			{
			private:
				//PXml pxObj;
			public:
				PXml pxObj; //This is public as a temporary hack -- eventually this layer should be removed and the managed C++ XML object should wrap PXml directly
				E1Xml ();
				E1Xml (S4 dataMax);
				~E1Xml ();

				// Execute an URL/command
				int   cmd(STR cmdBuf);
				int   cmd(STR cmdBuf, int readOnly, S4 threadId);
				S4    show(void *file);  // Show in file, then reset XML

				// Get the data that contains the XML after the URL is processed
				char *dataBuf(void);
				S4    dataLen(void);

				// Reset the XML so it can be used to run another command
				void  reset(void);
			};

			/*public ref class QueryParameter
			{
			private:
				MemNV _memNV;

			public:
				property String^ Name
				{
					String^ get();
					void set(String^ s);
				}
				property String^ Value
				{
					String^ get();
					void set(String^ s);
				}
			};

			public class StringConverter
			{
			public:
				static String^ STRToString(STR str)
				{
					return System::Runtime::InteropServices::Marshal::PtrToStringAnsi(static_cast<IntPtr>(str));
				}

				static STR StringToSTR(String^ string)
				{
					IntPtr ip;
					STR result;

					try
					{
						ip = System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(string);
						result = static_cast<char*>(ip.ToPointer());
					}
					finally
					{
						System::Runtime::InteropServices::Marshal::FreeHGlobal(ip);
					}

					return result;
				}
			};*/
		};
	};
};
