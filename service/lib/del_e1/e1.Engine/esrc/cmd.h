// cmd.h
// pnelson 11/22/2005
// Helpers for the URL/CMD processor
//


#include "kw.h"

#define kCmd_ArgsMax  32    // max number of unique arg names in an Url
#define kCmd_PairsMax 500   // max number of arg name/values in an Url
#define kCmd_BufMax (1<<15) // Maximum size of a Cmd input buffer

/******************************************/
/** The base class for all Command Specs **/
/******************************************/

// The types of arguments in a command specification
//
typedef enum { 
	Arg_Unknown, 
	Arg_S4, 
	Arg_S2, 
	Arg_S1, 
	Arg_CH, 
	Arg_Str,
	Arg_Buf16,
	Arg_Buf32,
	Arg_Float
 } CmdArgType;

// The dynamic list of values for this command
//
typedef struct _CmdSpecX { 
	KWEnum tag;             // name of this value
	CmdArgType argType; // typ of the value of this arg
	int soff;           // offset in structure where this value stored
	int howMany;        // 0,1 means 1 value, otherwise length of array
	int curIdx;         // if an array, will be used in inst to point to current
} CmdSpecXRec, *CmdSpecX;

// The base stucture for all Cmd Specs
//
typedef struct _CmdSpec {
	cSTR      help;
	KWEnum    cmd;        // The Command
	KWEnum    typ;        // The optional secondary command
	int       specSize;   // The actual size of the spec structure
	CmdSpecXRec args[kCmd_ArgsMax];   // The list of arguments to this command
    // Shared options across all command instances
    S2  mode;
	// Invisble bookkeeping handles
	void      *pairs;
	S4        pairsSize;
} CmdSpecRec, *CmdSpec;

// from cmdaux.c
extern KWEnum  CmdKeyFromStr(STR str);
extern STR     CmdStrFromKey(KWEnum key);
extern CmdSpec CmdSpecNew(STR cmdLine, const CmdSpec *grammar);
extern void    CmdSpecFree(CmdSpec spec);
extern int     CmdGrammarShowHelp(const CmdSpec *grammar, KWEnum justOne, PXml px);

// from cmd.c
extern int CmdEval(STR cmdLine, PXml px, int readOnly, S4 threadId);
