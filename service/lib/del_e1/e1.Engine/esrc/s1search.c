// s1search.c
// pnelson 12/11/2005
// Basic searching to emulate legacy

#define MEM_FRIEND
#define GRP_FRIEND
#define TAG_FRIEND
#include "string.h"
#include "peng.h"

#define PKGNAME PkgName_S1

#define kS1_scanMax (200000)  // Most people we'll ever look at in a search

// Internal data to support fast query execution
//
typedef struct _S1Scan {

  S4           allocSize; // How big is this struct (for debugging)
  RankTune     rankTune;  // Rank tuning parameters

  DomId        domId;     // Must be this domain
  US1          gender;    // Must be this gender
  LocRec       loc;       // Lng Lat Box at focus of search
  S2           locDst;    // Preferred radius from location

  MemCSCZMapRec cscz;     // Packed bitfield of Country/State/City names & Zipcode

  int        ageMin, ageMax;
  US1  byAge[kRank_AgeVctMax];
  int        hgtMin,  hgtMax;
  int        wgtMin,  wgtMax;

  int        hasPhoto;     // -1 has none, 0 don't care, 1 require
  int        isOnline;    // -1 not online, 0 don't care, 1 require

  // Ptr to actual tags if they exist -- masks may expand into multiple tags
  S2     tagCnt;
  Tag    tagDat[kS1Query_TagMax*4];

  // How to blend scores
  S1SortKey    sortKey;
  S1ScoresRec  blend;

  // Some stats
  S4 scanCnt;    // How many we looked at
  S4 liveCnt;    // How many showed some sign of life
  S4 dropCnt;    // How many rejected by PRes for score too low

  // The mask and the bit val per tag
  US4     orTarget;    // Mask with all used bits turned on

  TagDbId orBits[33];  // Which tagdb controls which bit
  US2     orMaskMax;   // largest bit in use
  US1     orMask[1];   // Which bit to flip for this tag
  // Structure will be alloced bigger to allow space for orMask to cover all tags
} S1ScanRec;


// Convert search input requests to formatted info for our scanner
//
static S1Scan S1ScanNew(S1Query xqry)
{
	S4  asize;
	S1Scan xscan;
	int b, i, tagNew, tagMax; 
	
	// Alloc struct big enough to include an orMask byte per tag
	tagMax = ObjMgrSequence(gsv->tagObjMgr);
	asize  = sizeof(S1ScanRec) + tagMax;
	xscan  = PEngAlloc(PKGNAME, asize);
	xscan->allocSize = asize;
	xscan->orMaskMax = tagMax;

	if (xqry->blend.bias != 0) {
	  xscan->blend = xqry->blend;
	} else {
	  xscan->blend = *(gsvRank.s1blend);
	}

	// Setup processed versions of all our other builtin search params
	xscan->rankTune  = &gsvRank;
	xscan->sortKey   = xqry->sortKey;
	xscan->domId     = (xqry->domId ? xqry->domId : 1);
	xscan->gender    = MemPackGender(xqry->genderMask, NULL);
	if (xqry->debugLocLat || xqry->debugLocLng) {
	  xscan->loc.lat = xqry->debugLocLat;
	  xscan->loc.lng = xqry->debugLocLng;
	} else {
	  MemPackLoc(&(xscan->loc), &(xqry->locKey));
	}
	xscan->locDst    = xqry->locDst;
	MemPackCSCZMap(&(xscan->cscz), &(xqry->locKey));
	
	xscan->hasPhoto = xqry->hasPhoto;
	xscan->isOnline = xqry->isOnline;
	
	// Setup lookup vector for every age
	xscan->ageMin = xqry->ageMinDesired;
	xscan->ageMax = xqry->ageMaxDesired;
	RankAgeInit(xscan->byAge, xscan->ageMin, xscan->ageMax, gsvRank.age);

	xscan->hgtMin = RankPackHgt(xqry->hgtMinDesired);
	xscan->hgtMax = RankPackHgt(xqry->hgtMaxDesired);
	xscan->wgtMin = RankPackWgt(xqry->wgtMinDesired);
	xscan->wgtMax = RankPackWgt(xqry->wgtMaxDesired);

	// Now look through all the requested tags in the query and
	//   set the proper masks on all tag instances we care about
	for (i=0; xqry->tags[i].name; i++) {
	  STR      name = xqry->tags[i].name;
	  STR     value = xqry->tags[i].value;
	  TagDbId  tdx  = TagDbFind(name);
	  S4       code = atol(value);
	  
	  if (!tdx)
		continue; // ??? we don't know about this tag

	  // Find the bit for this tag's OR group
	  for (b=0; xscan->orBits[b] != tdx && xscan->orBits[b] != 0; b++) ;

	  if (b >= 32)
		continue; // ??? too many unique tag types in this search

	  // Remember that this tag group is tied to this bit
	  xscan->orBits[b] = tdx;

	  // Find all the tags the user requested
	  tagNew = TagDbByCode(tdx, code, &(xscan->tagDat[xscan->tagCnt]));

	  // Mark those tags with the and/or bit to flip when present
	  tagMax = xscan->tagCnt + tagNew;
	  while (xscan->tagCnt < tagMax) {
		Tag tag = xscan->tagDat[xscan->tagCnt++];
		S4  seq = tag->sequence;
		if (seq >= xscan->orMaskMax)
		  continue; // ??? This tag is not in range of all tags
		else
		  xscan->orMask[seq] = b+1; // Do it one based so that zero means don't care
	  }
	}

	// This is what all our bits being on looks like
	xscan->orTarget = 0;
	for (b=0; xscan->orBits[b] != 0 && b < 32; b++) 
	  xscan->orTarget |= (1L << b);

	return xscan;
}

static void S1ScanFree(S1Scan xscan)
{
    if (xscan)
	  PEngFree(PKGNAME, xscan, xscan->allocSize);
}

/****************/

#define S1Scale          ((US2)655)   // Fit 0-100 in 16 bits
#define S1Identity       ((US2)(100 * S1Scale))
#define S1Inverse(s,b)   (US2)(S1Identity - (US2)((US4)(s) * (US4)(b) * S1Scale / 100))
#define S1MultScale(a,b) (US2)(((US4)(a) * (US4)(b)) / S1Identity)

static S1Score nearMix(S1Score ldg, S1Score near)
{
  S4 sl = S1Inverse(90, ldg);
  S4 sn = S1Inverse(80, near);
  S4 ps = S1Identity - S1MultScale(sl, sn);
  return (S1Score)ps/S1Scale;
}
  
// Returns a score between 0 and S1Scale*100
// xscan  is the compiled rep of the query
// xmem   is the member we're scanning
// xss    is score we/re building, should already include ldg distance
//
static US2 S1ScanExec(S1Scan xscan, Mem xmem, S1Scores xss)
{
  US4 orMask;
  Grp xgrp; int i;
  S4  probSum;
  MemDateRec ageRec; US1 age;
  S1Scores blend;

  blend = &(xscan->blend);

  // First check whether has photo
  if (xscan->hasPhoto == S1HasPhoto_Require && !xmem->vitals.hasPhoto)
	  goto reject;

  // First check the age --  most likely cause of reject
  ageRec.pack = gsvRank.today.pack - xmem->birthPack.pack;
  age = ageRec.v.year;
  if (!(xss->age = (age < kRank_AgeMin || age > kRank_AgeMax ? 0 : xscan->byAge[age])))
	goto reject;
  probSum = S1Inverse(xss->age, blend->age);

  // Now check all tag groups
  orMask = 0;
  if (xscan->orTarget && (xgrp = xmem->grp)) {
	for (i=0; i<xgrp->tagCount; i++) {
	  Tag xtag = xgrp->tagList[i];
	  S4  seq  = xtag->sequence;
	  int b = xscan->orMask[seq];  // one based
	  if (b) {
		orMask |= (1L << (b-1));
		if (orMask == xscan->orTarget)
		  break;
	  }
	}
  }
  if (orMask == xscan->orTarget) {
	// No tags (both zero) or complete match
	xss->match = 100;
	// If not a pure filter, blend it in
	if (blend->match != 0xFF)
	  probSum = S1MultScale(probSum, S1Inverse(100, blend->match));

  } else if (blend->match == 0xFF) {
	// This is filter so reject the member
	goto reject;

  } else if (orMask == 0) {
	// Got nothing
	xss->match = 0;

  } else {
	// ??? For now, charge 20 pts for every missing attr
	S4 refMask = xscan->orTarget;
	xss->match = 100;
	while (refMask) {
	  if ((orMask & 1) == 0)
		if (xss->match >= 20) 
		  xss->match -= 20;
	  refMask = (refMask >> 1);
	  orMask  = (orMask  >> 1);
	}
	if (xss->match)
	  probSum = S1MultScale(probSum, S1Inverse(xss->match, blend->match));
  }

  // Tag match is ok, now factor in euclidean distance based on LDG chain in caller
  probSum = S1MultScale(probSum, S1Inverse(xss->ldg, blend->ldg));

  // Generate and prob sum each individual score
  if ((xss->near = RankNearExec(&(xmem->cscz), &(xscan->cscz), xscan->rankTune)))
	probSum = S1MultScale(probSum, S1Inverse(xss->near, blend->near));

  if ((xss->act = (S1Score)xmem->vitals.saAct * 6))  // normalize 0-15
	probSum = S1MultScale(probSum, S1Inverse(xss->act, blend->act));

  if ((xss->pop = (S1Score)xmem->vitals.saPop * 6))  // normalize 0-15
	probSum = S1MultScale(probSum, S1Inverse(xss->pop, blend->pop));

  if (xscan->hgtMin) {
	xss->hgt = RankHgtExec(xmem->vitals.height, xscan->hgtMin, xscan->hgtMax, 
						   xscan->rankTune);
	if (!xss->hgt)
	  goto reject;
	probSum = S1MultScale(probSum, S1Inverse(xss->hgt, blend->hgt));
  }

#if 0
  if (xscan->wgtMin) {
	xss->wgt = RankWgtExec(xmem->vitals.weight, xscan->wgtMin, xscan->wgtMax, 
							xscan->rankTune);
	if (!xss->wgt)
	  goto reject;
	probSum = S1MultScale(probSum, S1Inverse(xss->wgt, blend->wgt));
  }
#endif

  // Now add a final score to strongly bias results to Ordering request
  switch (xscan->sortKey) {
  case S1Sort_Newest:  
	xss->bias = RankByTimeExec(xmem->registerPack.pack, xscan->rankTune);
	break;
  case S1Sort_Active:  xss->bias = xss->act; break;
  case S1Sort_Popular: xss->bias = xss->pop; break;
  case S1Sort_Near:    xss->bias = nearMix(xss->ldg, xss->near); break;
  case S1Sort_Online:  xss->bias = (xmem->vitals.isOnline ? 100 : 0); break;
  default: xss->bias = 0;
  }
  if (!xss->bias)
	goto reject;
  probSum = S1MultScale(probSum, S1Inverse(xss->bias, blend->bias));

  // Now 
  probSum = S1Identity - probSum;
  xss->total = (S1Score)((probSum + S1Scale/2) / S1Scale);
  return (US2)probSum;
  
 reject:
  xss->total = 0;
  return 0;
}

// Do the Search
// ??? This needs to get much smarter
//
S1Result S1ResultNew(S1Query xqry)
{
	S1Result     xres;
	S1Scan       xscan;
	PRes         pres;
	S1ScoresRec  xss;
	Ldg          xldg;
	LdgIterRec   iter;
	Mem         *root, xmem;
	S4  resMax, ldgDst, ldgScr, locDst;
	US2 scaled; int drop; 
	PermitSearch myPermit = (PermitSearch)0;

	if (PermitSearchTake(&myPermit))
		return NULL;

	if (!xqry->sortKey)
	  xqry->sortKey = S1Sort_Popular;

	if (!xqry->resMax)
	  xqry->resMax = kS1Result_MemMax/2;

	// Allocate space for results, including an S1Scores per result
	xres = PEngAlloc(PKGNAME, StructSize(S1ResultRec));

	// Map the query into our internal rep optimized for scanning
	xres->xscan = S1ScanNew(xqry);
	xscan   = xres->xscan;

	// Now create our result gatherer
	resMax = (xqry->resMax ? xqry->resMax : kS1Result_MemMax/2);
	if (resMax > kS1Result_MemMax)
	  resMax = kS1Result_MemMax-2;
	xres->pres = pres = PResNew(resMax);
	xres->memMax = (US2)resMax;

	// Start defending memory in the network
	PermitSearchBeg(myPermit);

	// Prepare to iterate
	StructNull(iter);
	locDst = MemPackLocMilesAsBands(xqry->locDst);
	LdgIterInit(&iter, &(xscan->loc), xscan->domId, xscan->gender, locDst);

	// Iterate -- this still looks at way too many since it iters on square, not circle
	while ((xldg = LdgIterNext(&iter))) {
	  ldgDst = RankLdgDstCalc(&(xscan->loc), &(iter.ldgKey.loc));
	  ldgScr = RankNearLdgDst(ldgDst, xscan->rankTune);
	  root = LdgChainMemRoot(xldg);
	  for (xmem = *root; xmem; xmem = xmem->ldgChain) {
		StructNull(xss);
		xss.ldg = (S1Score)ldgScr;
		if (xscan->scanCnt++ > kS1_scanMax)
		  goto done;
		if ((scaled = S1ScanExec(xscan, xmem, &xss)) > 0) {
		  xscan->liveCnt += 1;
		  if ((drop = PResInsert(pres, xmem->memId, scaled)))
			xscan->dropCnt += 1;
		}
	  }
	}

 done:
	// Stop defending memory
	PermitSearchEnd(myPermit);

	// Now sort and load the results
	PResLoad(pres, xres);

	// Give up the permit
	PermitSearchGive(myPermit);
	return xres;
}

void S1ResultFree(S1Result xres)
{
  if (xres) {
	if (xres->xscan) 
	  S1ScanFree(xres->xscan);
	if (xres->pres)
	  PResFree(xres->pres);
	PEngFree(PKGNAME, xres, sizeof(*xres));
  }
}

/****************************************************************/

static int S1ResultXmlMem(S1Result xres, PXml px, int mode)
{
  int err, i; S4 ldgDst;
  char msgBuf[512];
  MemKeyRec   mk;
  S1ScoresRec xss;
  S1Scan xscan;
  Mem    xmem;

  xscan = xres->xscan;
  StructNull(mk);
  mk.domId = xscan->domId;

  err = PXmlPush(px, "Mems", xres->memNum);
  
  for (i = 0; !err && i < xres->memNum; i++) {
	US2 scaled, score = xres->scores[i];
	mk.memId = xres->memids[i];
	if ((mode & 4) && (xmem = MemFind(&mk))) {
	  // Recalculate full score vector for this guy
	  StructNull(xss);
	  ldgDst  = RankLdgDstCalc(&(xscan->loc), &(xmem->ldg->ldgKey.loc));
	  xss.ldg = RankNearLdgDst(ldgDst, xscan->rankTune);
	  scaled  = S1ScanExec(xscan, xmem, &xss);
	  sprintf(msgBuf, "Mem memId='%ld' score='%d' total='%d' bias='%ld' ldg='%d' near='%d' match='%d'",
			  mk.memId, scaled, xss.total, xss.bias, xss.ldg, xss.near, xss.match);
	  sprintf(msgBuf+strlen(msgBuf), " age='%d' hgt='%d' wgt='%d' saAct='%d' saPop='%d'",
			  xss.age, xss.hgt, xss.wgt, xss.act, xss.pop);
	} else {
	  sprintf(msgBuf, "Mem memId='%ld' score='%d'",	mk.memId, score);
	}
	err |= PXmlPush0(px, msgBuf, 0);
  }
  err |= PXmlPop(px);
  return err;
}

static int S1ResultXmlTag(S1Result xres, PXml px, int mode)
{
  int i, err;
  S1Scan xscan = xres->xscan;

  err = PXmlPush(px, "Tags", xscan->tagCnt);
  for (i = 0; !err && i < xscan->tagCnt; i++) {
	Tag xtag = xscan->tagDat[i];
	err |= TagXml(px, xtag, mode);
  }
  err |= PXmlPop(px);
  return err;
}


static int S1ResultXmlQry(S1Result xres, PXml px, int mode)
{
  int err;
  char msgBuf[512], msgLen = 0;
  S1Scan xscan = xres->xscan;
  MemDateRec today = gsvRank.today;

  snprintf(msgBuf, sizeof(msgBuf), 
		   "Query domid='%d' gender='%d' blng='%ld' blat='%ld' locdst='%d' sortkey='%d' agemin='%d' agemax='%d'",
		   xscan->domId, xscan->gender, xscan->loc.lng, xscan->loc.lat, xscan->locDst, xscan->sortKey,
		   xscan->ageMin, xscan->ageMax);
  if ((err = PXmlPush(px, msgBuf, 0)) < 0)
	return err;

  if (1) {
	S1Scores b = &(xscan->blend);
	snprintf(msgBuf, sizeof(msgBuf), 
			 "Blend bias='%ld' match='%d' age='%d' ldg='%d' near='%d' act='%d' pop='%d' hgt='%d' wgt='%d'",
			 b->bias, b->match, b->age, b->ldg, b->near, b->act, b->pop, b->hgt, b->wgt);
	err |= PXmlPush0(px, msgBuf, 0);
  }

  if (1) {
	int amin, amax;
	for (amin = kRank_AgeMin; amin < kRank_AgeMax; amin++)
	  if (xscan->byAge[amin] > 0) break;
	for (amax = kRank_AgeMax; amax > kRank_AgeMin; amax--)
	  if (xscan->byAge[amax] > 0) break;
	snprintf(msgBuf, sizeof(msgBuf), 
			 "Dates today='%d-%d-%d' ageminok='%d' agemaxok='%d'",
			 1900+today.v.year, today.v.month, today.v.day, amin, amax);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  if (1) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "Flags hasPhoto='%d' isOnline='%d'", xscan->hasPhoto, xscan->isOnline);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  if (xscan->hgtMin) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "Hgt min='%d' max='%d''", xscan->hgtMin, xscan->hgtMax);
	err |= PXmlPush0(px, msgBuf, 0);
  }
  if (xscan->wgtMin) {
	snprintf(msgBuf, sizeof(msgBuf), 
			 "Wgt min='%d' max='%d''", xscan->wgtMin, xscan->wgtMax);
	err |= PXmlPush0(px, msgBuf, 0);
  }

  err |= PXmlPop(px);
  return err;
}

int S1ResultXml(S1Result xres, PXml px, int mode)
{
	char msgBuf[128];
	int err;
	S1Scan xscan;

	if (!xres) 
		return -1;
	xscan = xres->xscan;

	snprintf(msgBuf, sizeof(msgBuf), "Result count='%d' scanCnt='%ld' liveCnt='%ld' dropCnt='%ld' orTgt='%ld'",
			   xres->memNum, xscan->scanCnt, xscan->liveCnt, xscan->dropCnt, xscan->orTarget);

	if (mode == 64)
		return PXmlPush0(px, msgBuf, 0);

	if ((err = PXmlPush(px, msgBuf, xres->sequence)) < 0)
		return err;

  if (!err && mode) {
	if ((mode & 2))
	  err |= S1ResultXmlQry(xres, px, mode);
	if ((mode & 1) && xres->xscan->tagCnt) 
	  err |= S1ResultXmlTag(xres, px, 0);
	if ((mode & 4) && xres->pres)
	  err |= PResXml(px, (PRes)xres->pres, mode);
  }

  if (!err)
	err |= S1ResultXmlMem(xres, px, mode);

  err |= PXmlPop(px);
  
  return err;
}
