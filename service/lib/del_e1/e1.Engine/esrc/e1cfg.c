// e1cfg.c
// pnelson 1/15/2006
// Configuration parameters

#include "peng.h"

#ifdef GCC
#define _stricmp stricmp
#endif

#define PKGNAME PkgName_Eng

typedef struct _E1CfgParam {
  char *name;
  char *strValue;
  long intValue;
} E1CfgParamRec, *E1CfgParam;

typedef struct _E1CfgDb {
  E1CfgParamRec *params;
} E1CfgDbRec, *E1CfgDb;

static E1CfgParamRec gsvCfgParams[] = {
  { "cfg/permit/searchCount", NULL, 16 },
  { NULL }
};

static E1CfgDbRec gsvCfgDb = {
  gsvCfgParams
};

/****************/

int  E1CfgInitialize(cSTR source)
{
  return 0;
}

void E1CfgDestroy()
{
  return;
}

/****************/

cSTR E1CfgGetStr(cSTR name)
{
  E1CfgParam params = gsvCfgDb.params;
  int i;
  for (i=0; params[i].name; i++) {
	if (_stricmp(name, params[i].name) == 0)
	  return params[i].strValue;
  }
  return NULL;
}

S4 E1CfgGetInt(cSTR name)
{
  E1CfgParam params = gsvCfgDb.params;
  int i;
  for (i=0; params[i].name; i++) {
	if (_stricmp(name, params[i].name) == 0)
	  return params[i].intValue;
  }
  return 0;
}
