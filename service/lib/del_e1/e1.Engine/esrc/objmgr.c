// pobj.c
// pnelson 11/20/2005
// Object memory manager

#include "peng.h"
#define PKGNAME PkgName_ObjMgr

#define kObjMgr_PagesMax 128
 
#define XOMSEQ(xom) ((xom)->nextPtr * (xom)->pageSize + xom->nextIdx)

typedef struct _ObjPtr { 
	ObjPtr next; 
} ObjPtrRec;

typedef struct _ObjMgr {
	PkgName pkg;
	S4  objSize;    // Size of each object
	S4  pageSize;   // How many per page   
	S4  pageBytes;  // How many bytes per page   
	S4  nextPtr;    // Current page
	S4  nextIdx;    // Current item on page
	ObjPtr nextFree; // Free list
	S4  aSize;      // Sizeof initial alloc

	US1  *pages[kObjMgr_PagesMax];

} ObjMgrRec;

ObjMgr ObjMgrCreate(PkgName pkg, int objSize, S4 maxObjs)
{
	S4 pageSize, pageBytes, aSize;
	US1 *data = NULL;
	ObjMgr xom = NULL;

	// establish minimums
	if (objSize < sizeof(ObjPtr))
		objSize = sizeof(ObjPtr);
	if (maxObjs < kObjMgr_PagesMax*2)
		maxObjs = kObjMgr_PagesMax*2;

	// Round up if needed
	if (objSize % 4)
		objSize = ((objSize/4)+1)*4;
	pageSize = maxObjs/kObjMgr_PagesMax;
	if (maxObjs % kObjMgr_PagesMax) pageSize++;

	pageBytes = pageSize * objSize;

	// Alloc manager and first page
	aSize  = StructSizeAlign(ObjMgrRec);
	data = PEngAlloc(pkg, aSize + pageBytes);
	xom    = (ObjMgr)data;
	xom->pages[0] = (US1*)(data + aSize);
	xom->pkg      = pkg;
	xom->objSize  = objSize;
	xom->pageSize = pageSize;
	xom->pageBytes= pageBytes;
	xom->nextPtr  = 0;
	xom->nextIdx  = 1;  // Don't give out 0,0
	xom->nextFree = NULL;
	xom->aSize    = aSize + pageBytes;

	return xom;
}

S4 ObjMgrSequence(ObjMgr xom)
{
	if (xom)
		return XOMSEQ(xom);
	return 0;
}

void ObjMgrDestroy(ObjMgr xom)
{
	if (xom) {
		int i;
		for (i = 1; i < kObjMgr_PagesMax; i++) 
			if (xom->pages[i])
				PEngFree(xom->pkg, xom->pages[i], xom->pageBytes);
		PEngFree(xom->pkg, xom, xom->aSize);		
	}
}	
	

ObjPtr ObjMgrNew(ObjMgr xom, S4 *seqp)
{
  ObjPtr obj; S4 seq;
  // Get it from the free list
  if ((obj = xom->nextFree)) {
	xom->nextFree = obj->next;
	memset(obj, 0, xom->objSize);
	if (seqp)
	  *seqp = ObjMgrPtrToSeq(xom, obj);
	return obj;
  }
  // Maybe need a new page
  if (xom->nextIdx == xom->pageSize) {
	if (xom->nextPtr++ == kObjMgr_PagesMax)
	  goto error;
	if (!(xom->pages[xom->nextPtr] = PEngAlloc(xom->pkg, xom->pageBytes)))
	  goto error;
	xom->nextIdx = 0;
  }
  // Popit from this page (memory should already be NULL)
  obj = (ObjPtr)(xom->pages[xom->nextPtr] + (xom->nextIdx * xom->objSize));
  seq = XOMSEQ(xom);
  xom->nextIdx += 1;
  
  // Return object and sequence number
  if (seqp)	*seqp = seq;
  return obj;
  
 error:
  { 
	char buf[128];
	snprintf(buf, sizeof(buf), "Allocating %s after %ld pages, %ld on page", 
			 GsvPkgNameAsStr(xom->pkg), xom->nextPtr, xom->nextIdx);
	PEngError(xom->pkg, NULL, buf);
  }
  if (seqp)
	*seqp = 0;
  return NULL;
}

void ObjMgrFree(ObjMgr xom, ObjPtr obj)
{
	if (obj) {
		obj->next = xom->nextFree;
		xom->nextFree = obj;
	}
}

S4 ObjMgrPtrToSeq(ObjMgr xom, ObjPtr obj)
{
	if (xom && obj) {
		US4 ob = (US4)obj;
		int i;
		for (i = 0; i <= xom->nextPtr; i++) {
			US4 pg = (US4)xom->pages[i];
			if (ob >= pg && ob < pg+xom->pageBytes) {
				ob -= pg;
				if (ob % xom->objSize)
					goto align;
				return (i*xom->pageSize + ob/xom->objSize);
			}
		}
	}
	// This is bad -- means object handle can't be valid
	return 0;
 align:
	// This is bad -- means object handle is corrupt
	return 0;
}

ObjPtr ObjMgrSeqToPtr(ObjMgr xom, S4 seq)
{
	if (xom && seq) {
		S4 ptr = seq / xom->pageSize;
		S4 idx = seq % xom->pageSize;
		if (ptr <= xom->nextPtr ||
				(ptr == xom->nextPtr && idx < xom->nextIdx)) {
			return (ObjPtr)(xom->pages[ptr] + idx * xom->objSize);
		}
	}
	return NULL;
}

int ObjMgrXml(PXml px, ObjMgr xom, int mode)
{
  int errCode = 0;
  if (xom && px) {
	char msgBuf[256]; 
	S4 seq = ObjMgrSequence(xom);
	snprintf(msgBuf, sizeof(msgBuf), "ObjMgr name='%s'", GsvPkgNameAsStr(xom->pkg));

	if ((errCode = PXmlPush(px, msgBuf, seq)) < 0)
	  return errCode;
	else {
	  S4 cnt; ObjPtr scan;
	  for (cnt=0, scan = xom->nextFree; scan; cnt++, scan=scan->next) ;
	  snprintf(msgBuf, sizeof(msgBuf), 
			   "Stats objSize='%d' pageMax='%d' pageSize='%d' pageBytes='%d' nextPtr='%d' nextIdx='%d' freelist='%d'",
			   xom->objSize, kObjMgr_PagesMax, xom->pageSize, 
			   xom->pageBytes, xom->nextPtr, xom->nextIdx, cnt);
	  errCode |= PXmlPush0(px, msgBuf, 0);
	}
	errCode |= PXmlPop(px);
  }
  return errCode;
}
