// C++ interface to overall engine state
//

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "e1cpp.h"

using namespace Matchnet::e1::Engine;

static void showError(int fatal, STR e1, STR e2)
{
	fprintf(stderr, "Args: inputFile.csv\n");
	if (e1)
		fprintf(stderr, " Error: %s: %s\n", e1, (e2 ? e2 : ""));
	if (fatal)
		exit(fatal);
}

int E1Gsv::main(int argc, char **argv)
{
	char buf[16*1024];
	int err = 0, i;
	void *ff = (void*)stdout;

	E1Xml px((1<<17));
  
	for (i = 1; i < argc; i++) {
		// Argument must be a command file
		char *inpName = argv[i];
		FILE *inpFile = NULL;
		if (!(inpFile = fopen(inpName, "r"))) {
			showError(0, "Args: cmds.txt", NULL);
		} else {
			fprintf(stdout, "## Processing command file: %s", inpName, 0);
			while (!err && fgets(buf, sizeof(buf), inpFile))
			if (buf[0] && buf[0] != '#' && buf[0] != '\n') {
				err = px.cmd(buf);
				px.show(ff);
			}
			fclose(inpFile);
		}
	}
	if (argc == 1) {
		// First show help
		strcpy(buf, "help type=help");
		err = px.cmd(buf);
		px.show(ff);
	}
	fflush(stdout);

	// Now loop
	while (fgets(buf, sizeof(buf), stdin)) {
		// Show the command line
		fprintf(stdout, "## %s\n", buf);
		// Now eval the command
		if ((err = px.cmd(buf)) < 0) 
			break;
		// Show the output
		px.show(ff);
		fflush(stdout);
	}
	return 0;
}
