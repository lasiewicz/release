// rank.c
// pnelson 11/25/2005
// Ranking algorithms
//

#include "peng.h"

#define PKGNAME PkgName_Rank

static RankTuneAgoRec rtAgo = {
  { { 0, 100 }, 
	{ 10, 90 }, 
	{ 30, 80 }, 
	{ 70, 70 },
	{ 120, 60 }, 
	{ 180, 50 },
	{ 370, 30 }, 
	{ 500, 10 }, 
	{ 0, 0 } }
};

static S1ScoresRec rtS1Blend = 	{ 90, 0xFF, 30, 30, 30, 30, 30, 30, 30 };

static RankTuneNearRec rtNear = {
  5, 10, 75, 95,
  { 100 }, { 75 },
  { {   0, 100 }, 
	{   9,  95 }, 
	{  25,  90 },
	{  49,  85 },
	{  81,  80 },
	{ 121,  75 },
	{ 400,  70 },
	{ 900,  65 },
	{2500,  55 },
	{8100,  40 },
	{  0,   0 } }
};

static RankTuneAgeRec rtAge = { 
  90, 10,   // 90 is score at edge of requested band, 70 is score just outside band
  { {4,1}, {8, 2}, {12,3}, {16, 4} }, // How much to penalize being younger
  { {4,1}, {8, 2}, {12,3}, {16, 4} }  // How much to penalize being older
};

static RankTuneHgtRec rtHgt = { 90, 60, 2 };

//Nobody gets a score of 0 (1 is used for dates > 180 days old)
static RankTuneActRec rtAct = { {0, 0, 180, 150, 120, 90, 75, 60, 45, 35, 28, 21, 14, 7, 3, 1 } };

RankTuneRec gsvRank = {
    // Todays date packed into vitals (init'd in GsvInitialize()
    { 0 }, 0,
	&rtS1Blend, // S1 score blend
	&rtAgo,     // Ago
	&rtNear,    // Near
	&rtAge,     // Age
	&rtHgt,      // Height
	&rtAct
};

/****************/

US2 RankPackAge(int age)
{
  MemDateRec tt = gsvRank.today;
  tt.v.year -= age;
  return tt.pack;
}

int RankPackHgt(int hgt)
{
  if (hgt == 0) return 0;
  hgt -= kMemPack_HeightMin;
  hgt = minmax(hgt, 1, kMemPack_HeightRng);
  return hgt;
}

int RankPackWgt(int wgt)
{
  if (wgt == 0) return 0;
  wgt -= kMemPack_WeightMin;
  wgt = minmax(wgt, 1, kMemPack_WeightRng);
  return wgt;
}

S4 RankLdgDstCalc(Loc xscan, Loc ldg)
{
	S4 latDst, lngDst;

	if (xscan->lat > ldg->lat)  latDst = xscan->lat - ldg->lat;
	else                        latDst = ldg->lat - xscan->lat;

	// Longitude wraps
	if (xscan->lng > ldg->lng)  lngDst = xscan->lng - ldg->lng;
	else                        lngDst = ldg->lng - xscan->lng;
	if (lngDst > kLoc_EWBoxes/2) lngDst = kLoc_EWBoxes - lngDst; 
	
	return (latDst*latDst) + (lngDst*lngDst);
}


S1Score RankNearLdgDst(S4 ldgDst, RankTune rt)
{
	RankTuneNear rn = rt->near;
	int i;
	// Figure out what band we're in, and score based on position in the band
	for (i = 1; rn->bands[i].outerDst; i++) {
		if (ldgDst <= rn->bands[i].outerDst) {
			S4       innerDst = rn->bands[i-1].outerDst;
			S4       outerDst = rn->bands[i  ].outerDst;
			S1Score  innerScr = rn->bands[i-1].outerScr;
			S1Score  outerScr = rn->bands[i  ].outerScr;
			S4 pctDst = (ldgDst - innerDst) * 1000 / (outerDst - innerDst);
			S4 actScr = (pctDst * (innerScr - outerScr)) / 1000;
			return (S1Score)(innerScr - actScr);
		}
	}
	return 0;
}

// Generate a score based on overlap of country, stte, city, zip
//
S1Score RankNearExec(MemCSCZMap xmem, MemCSCZMap scan, RankTune rt)
{
	RankTuneNear rn = rt->near;
	S1Score     scr = 0;

	if (scan->pkCountry && (xmem->pkCountry == scan->pkCountry)) {
	  scr = rn->inCountry;
	  if (scan->pkState && (xmem->pkState == scan->pkState)) {
		scr = rn->inState;
		if (scan->pkCity && (xmem->pkCity == scan->pkCity)) {
		  scr = rn->inCity;
		  if (scan->pkZip && (xmem->pkZip == scan->pkZip)) {
			scr = rn->inZip;
		  }
		}
	  }
	}
	return scr;
}

/****************/

S1Score RankHgtExec(int xmem, int vmin, int vmax, RankTune rt)
{
	RankTuneHgt rh = rt->hgt;
	S4 score, midPt, gap;
	if (vmin == 0) {
		score = 0;
	} else if (xmem < vmin) {
		score = rh->outBandMax - rh->perInch * (vmin - xmem);
	} else if (xmem > vmax) {
		score = rh->outBandMax - rh->perInch * (xmem - vmax);
	} else {
		score = 100;
		if (rh->inBand < 100) {
			midPt = (vmin + vmax)/2;
			gap   = vmax - midPt;
			if (gap > 0) {
				S4 delta = (xmem < midPt) ? (midPt - xmem) : (xmem - midPt);
				score -= ((100 - rh->inBand) * delta) / gap;
			}
		}
	}
	return (S1Score)(score < 0 ? 0 : score);
}

/****************/

// 
void RankAgeInit(US1 *byAge, US1 amin, US1 amax, RankTuneAge ra)
{
  int ageMin, ageMax, bestScr, deltaAge, deltaPct, thisScr, midp, gap, age, i, penalty;

  // amin and amax are the edges of the requested band, outside that, penalty extracted
  // ageMin and ageMax are absolute limits for this query -- outside that score is zero

  // If both 0, we don't care about age
  if (!amin && !amax) {
	memset(byAge, 0, kRank_AgeVctMax);
	return;
  }

  ageMin = ageMax = 0;
  if (!amin && amax) {
	// If just specified max, assume min is yng possible, max is hard limit
	amin   = kRank_AgeMin;
	ageMax = amax;
  } else if (amin && !amax) {
	// If just specified min, assume max is old possible, min is hard limit
	ageMin = amin;
	amax   = kRank_AgeMax;
  } else if (amin > amax) {
	// if yng > old, switch them, and assume each is a hard boundary
	US1 tmp = amax;
	ageMax = amax = amin;
	ageMin = amin = tmp;
  }

  if (ageMin == 0 || ageMin < kRank_AgeMin) 
	ageMin = kRank_AgeMin;
  if (ageMax == 0 || ageMax > kRank_AgeMax) 
	ageMax = kRank_AgeMax;

  // Now calculate score per every age
  for (age = kRank_AgeMin; age < kRank_AgeMax; age++) {
	if (age < ageMin || age > ageMax) 
	  thisScr = 0;
	else if (age < amin) {
	  // Out of band younger
	  bestScr  = ra->outBandHi;
	  deltaAge = amin - age;
	  deltaPct = (deltaAge * 100) / amin;
	  thisScr  = 0;
	  for (i=0; i < ArrayCount(ra->ynger) && ra->ynger[i].pctDif; i++)
		if (deltaPct <= ra->ynger[i].pctDif) {
			penalty = (deltaAge * ra->ynger[i].perYear);
			thisScr = (penalty < bestScr ? bestScr - penalty : 0);
			break;
		}
	} else if (age > amax) {
	  // Out of band older
	  bestScr  = ra->outBandHi;
	  deltaAge = age - amax;
	  deltaPct = (deltaAge * 100) / amax;
	  thisScr  = 0;
	  for (i=0; i < ArrayCount(ra->older) && ra->older[i].pctDif; i++)
		if (deltaPct <= ra->older[i].pctDif) {
			penalty = (deltaAge * ra->older[i].perYear);
			thisScr = (penalty < bestScr ? bestScr - penalty : 0);
			break;
		}
	} else {
	  midp = (amax + amin) / 2;
	  gap  = amax - midp;
	  if (gap < 1 || age == midp)
		thisScr = 100;
	  else {
		deltaAge = (age < midp ? midp - age : age - midp);
		deltaPct = (deltaAge * 100) / gap;
		thisScr = 100 - (deltaPct * (100 - ra->inBandLow)) / 100;
	  }
	}
	byAge[age] = thisScr;
  }
}

/****************/

S1Score RankByTimeExec(US2 thenPack, RankTune rt)
{
  RankTuneAgo ra = rt->ago;
  int i;
  S4 nowDays  = gsvRank.todayDays;
  S4 thenDays = MemPackDateAsDays(thenPack);
  S4 agoDays  = nowDays - thenDays;

  // Figure out what band we're in, and score based on position in the band
  for (i = 1; ra->bands[i].days; i++) {
	if (agoDays <= ra->bands[i].days) {
	  S2       newerDay = ra->bands[i-1].days;
	  S4       olderDay = ra->bands[i  ].days;
	  S1Score  newerScr = ra->bands[i-1].score;
	  S1Score  olderScr = ra->bands[i  ].score;
	  S4 pctDst = (agoDays - newerDay) * 1000 / (olderDay - newerDay);
	  S4 actScr = (pctDst * (newerScr - olderScr)) / 1000;
	  return (S1Score)(newerScr - actScr);
	}
  }
  return 0;
}

//Convert packed last login date into number from 1-15 (15 = more recent)
int RankLastLoginToSAAct(US2 actPack, RankTune rt)
{
	if (actPack == 0)
		return 0;
	else {
		RankTuneAct ra = (rt ? rt->act : gsvRank.act);
		int i;
		S4 nowDays  = gsvRank.todayDays;
		S4 actDays = MemPackDateAsDays(actPack);
		S4 agoDays  = nowDays - actDays;

		// Figure out what band we're in, and score based on position in the band
		for (i = 15; i > 0; i--) {
			if (agoDays <= ra->days[i])
				return i;
		}
	}
	return 1;
}
