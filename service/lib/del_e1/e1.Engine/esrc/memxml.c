// memxml.c
// pnelson 12/4/2005
// Export results in XML

#define MEM_FRIEND
#include "peng.h"

#define PKGNAME PkgName_Mem

int MemXml(PXml px, Mem xmem, int mode)
{
  char msgBuf[128];
  int err;
  S4  nval, seq = MemSequence(xmem);
  cSTR sval;
  
  snprintf(msgBuf, sizeof(msgBuf), "Mem seq='%ld' memId='%ld' domId='%d' gender='%d' lng='%d' lat='%d'",
		   seq, xMemMemId(xmem), xMemDomId(xmem), xMemGender(xmem),
		   xMemLoc(xmem).lng, xMemLoc(xmem).lat);

  if ((err = PXmlPush(px, msgBuf, 0)) < 0)
	return err;
  
  if ((sval = StxDbById(gsv->pkCountry, xmem->cscz.pkCountry)))
	err |= PXmlSetStr(px, "country",  sval);
  
  if ((sval = StxDbById(gsv->pkState,   xmem->cscz.pkState)))
	err |= PXmlSetStr(px, "state", sval);
  
  if ((sval = StxDbById(gsv->pkCity,    xmem->cscz.pkCity)))
	err |= PXmlSetStr(px, "city", sval);
  
  if ((sval = StxDbById(gsv->pkZip,     xmem->cscz.pkZip)))
	err |= PXmlSetStr(px, "zip", sval);
  
  if (xmem->cscz.pkArea) {
	snprintf(msgBuf, sizeof(msgBuf), "%0.3d", xmem->cscz.pkArea);
	err |= PXmlSetStr(px, "areacode", msgBuf);
  }

  sval = MemPackDateStr("birthDate", xmem->birthPack.pack, msgBuf);
  err |= PXmlPush0(px, msgBuf, 0);
  sval = MemPackDateStr("registerDate", xmem->registerPack.pack, msgBuf);
  err |= PXmlPush0(px, msgBuf, 0);
  sval = MemPackDateStr("activeDate", xmem->activePack.pack, msgBuf);
  err |= PXmlPush0(px, msgBuf, 0);

  nval = xmem->vitals.height;    err |= PXmlSetNum(px, "height", nval + kMemPack_HeightMin);
  nval = xmem->vitals.weight;    err |= PXmlSetNum(px, "weight", nval + kMemPack_WeightMin);
  nval = xmem->vitals.hasPhoto;  err |= PXmlSetNum(px, "hasPhoto", nval);
  nval = xmem->vitals.isOnline;  err |= PXmlSetNum(px, "isOnline", nval);
  nval = xmem->vitals.saPop;     err |= PXmlSetNum(px, "saPop", nval);
  nval = xmem->vitals.saAct;     err |= PXmlSetNum(px, "saAct", nval);
  nval = xmem->vitals.childCnt;	 err |= PXmlSetNum(px, "childCnt", nval);
  nval = xmem->vitals.gayMask;	 err |= PXmlSetNum(px, "gayMask", nval);
  nval = xmem->vitals.relocate;	 err |= PXmlSetNum(px, "relocate", nval);

  if (!err && mode >= 1 && xmem->grp)
	err |= GrpXml(px, xmem->grp, mode-1);
  if (!err && mode >= 2 && xmem->ldg)
	err |= LdgXml(px, xmem->ldg, mode-2);
  
  err |= PXmlPop(px);
  return err;
}

int MemXmlGrpChain(PXml px, Mem xmem, int mode)
{
  int i, err = 0;
  int maxChain = PXmlEnumMax(px);
  Mem scan; char buf[32];

  for (i=0, scan=xmem; scan; i++, scan=scan->grpChain) ;

  snprintf(buf, sizeof(buf), "GrpChain count='%d'", i);
  err |= PXmlPush(px, buf, 0);
  for (i=0; !err && xmem && i<maxChain; i++, xmem=xmem->grpChain) {
	S4 seq = MemSequence(xmem);
	err |= PXmlPush0(px, "Mem", seq);
  }
  err |= PXmlPop(px);
  return err;
}

int MemXmlLdgChain(PXml px, Mem xmem, int mode)
{
  int i, err = 0;
  int maxChain = PXmlEnumMax(px);
  Mem scan; char buf[32];
  
  for (i=0, scan=xmem; scan; i++, scan=scan->ldgChain) ;
  
  snprintf(buf, sizeof(buf), "LdgChain count='%d'", i);
  err |= PXmlPush(px, buf, 0);
  for (i=0; !err && xmem && i<maxChain; i++, xmem=xmem->ldgChain) {
	S4 seq = MemSequence(xmem);
	err |= PXmlPush0(px, "Mem", seq);
  }
  err |= PXmlPop(px);
  
  return err;
}
