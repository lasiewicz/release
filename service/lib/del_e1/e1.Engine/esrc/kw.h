// kw.h
// pnelson 12/15/2005 == Consolidated efintions of all known keywpords
//

#ifndef INCLUDE_KW
#define INCLUDE_KW

typedef enum {
  KW_null = 0,
  KW_memid,
  KW_domid,
  KW_gender,
  KW_gendermask,
  // Location and distance,
  KW_boxlat,
  KW_boxlng,
  KW_fltlat,
  KW_fltlng,
  KW_distance,
  KW_country,
  KW_state,
  KW_city,
  KW_zip,
  KW_areacode,
  // Dates,
  KW_birthdate,
  KW_activedate,
  KW_registerdate,
  KW_updatedate,
  // Names, Values, Tags,
  KW_name,
  KW_value,
  KW_code,
  KW_tag,
  KW_tagdb,
  KW_coding,
  KW_noauto,
  // URL commands,
  KW_cmd,
  KW_help,
  KW_quit,
  KW_time,
  KW_show,
  KW_type,
  KW_load,
  KW_file,
  KW_gsv,
  KW_mem,
  KW_grp,
  KW_ldg,
  KW_seq,
  KW_mode,
  KW_update,
  KW_delete,
  KW_search,
  KW_resmax,
  KW_sortkey,
  // Flags, dynamic attributes,
  KW_hasphoto,
  KW_isonline,
  KW_saact,
  KW_sapop,
  KW_emailcount,
  // Age, Height, Weight,
  KW_age,
  KW_agemax,
  KW_agemin,
  KW_hgt,
  KW_hgtmax,
  KW_hgtmin,
  KW_wgt,
  KW_wgtmax,
  KW_wgtmin,
  //  Other attributes,
  KW_appearanceimportance,
  KW_bodyart,
  KW_bodytype,
  KW_childcnt,
  KW_cuisine,
  KW_custody,
  KW_desireddrinkinghabits,
  KW_desirededucationlevel,
  KW_desiredjdatereligion,
  KW_desiredmaritalstatus,
  KW_desiredsmokinghabits,
  KW_drinkinghabits,
  KW_educationlevel,
  KW_entertainmentlocation,
  KW_ethnicity,
  KW_eyecolor,
  KW_favoritetimeofday,
  KW_habitattype,
  KW_haircolor,
  KW_havechildren,
  KW_incomelevel,
  KW_industrytype,
  KW_intelligenceimportance,
  KW_israelareacode,
  KW_israelimmigrationdate,
  KW_israelipersonalitytrait,
  KW_isrealimmigrationyear,
  KW_jdateethnicity,
  KW_jdatereligion,
  KW_keepkosher,
  KW_languagemask,
  KW_leisureactivity,
  KW_majortype,
  KW_maritalstatus,
  KW_militaryservicetype,
  KW_morechildrenflag,
  KW_moviegenremask,
  KW_music,
  KW_personalitytrait,
  KW_pets,
  KW_physicalactivity,
  KW_planets,
  KW_politicalorientation,
  KW_reading,
  KW_relationshipmask,
  KW_relationshipstatus,
  KW_religion,
  KW_religionactivitylevel,
  KW_relocate,
  KW_synagogueattendance,
  KW_smokinghabits,
  KW_zodiac,

  KW_last
} KWEnum;

typedef struct _e1Kw {
  const char *name;
  KWEnum kw;
} E1KwRec, *E1Kw;

const E1KwRec *KWLookupAux (const char *str, unsigned int len);
const char    *KWLookupStr (KWEnum key);
KWEnum   KWLookupKey (const char *str);

#endif
