// papi.h
// pnelson, 11/13/2005
// Defintions needed by callers to the engine

#define INCLUDE_PAPI

/*******************/
/** Generic Types **/
/*******************/

typedef   signed char   S1;
typedef unsigned char  US1;

typedef   signed short  S2;
typedef unsigned short US2;

typedef   signed long   S4;
typedef unsigned long  US4;

typedef       char  *STR;
typedef const char  *cSTR;


/*********************/
/** URL Interpreter **/
/*********************/

extern void GsvInitialize();
extern void GsvTodaySet();
extern void GsvDestroy();

typedef struct _PXml *PXml;
extern PXml PXmlNew  (S4 dataMax, int enumMax, STR xslName);
extern STR  PXmlData (PXml px, S4 *lenp);
extern void PXmlShow (PXml px, void *file, int reset);
extern void PXmlReset(PXml px);
extern void PXmlFree (PXml px);

extern int CmdEval(STR cmdLine, PXml px, int readOnly, S4 notUsed);

extern int         LoadFile(cSTR fname, void *file, PXml px, int mode);
extern int         LoadFileAux(cSTR fname, void *file, PXml px, int mode, cSTR header);

/****************/
/** Base Types **/
/****************/

typedef US4 MemId;
typedef US1 DomId;
#define GenderMale   0x1
#define GenderFemale 0x2

/********************/
/** Adding Members **/
/********************/

#define kMemSpec_TagsMax  128

// Opaque handle to a Member
//
typedef struct _Mem   *Mem;

// Full spec of a location
//
typedef struct _MemLoc {
  float  regLat;
  float  regLng;
  STR    regCountry;
  STR    regState;
  STR    regCity;
  STR    regZip;
  STR    regAreaCode;
} MemLocRec, *MemLoc;

typedef struct _MemNV { 
  STR name;
  STR value; 
} MemNVRec, *MemNV;

// Describe a member to load
//
typedef struct _MemSpec {
  // memId, domId, gender
  MemId   memId;
  DomId   domId;
  US1     genderMask;

  /** Remaining fields can also be passed as NV pairs, vals in struct take precendence **/

  // Location
  MemLocRec  memLoc;

  // Key vitals stored in Mem structure (everything else is a tag) 
  STR    birthDate;      // YYYY-MM-DD
  STR    registerDate;   // YYYY-MM-DD
  STR    activeDate;     // YYYY-MM-DD

  US2    height;         // In inches
  US2    weight;         // In pounds
  US1    childrenCount;
  US1    hasPhoto;
  US1    isOnline;  
  US1    relocateFlag;
  US1    saPop;
  US1    saAct;
  
  // Other attributes as name-value pairs
  MemNVRec tags[kMemSpec_TagsMax];

  // Internal fields -- don't change
  void *tobj[kMemSpec_TagsMax*2];
  S2    tobjCnt;
  S4    debugSeq;
  US2   debugLocLat, debugLocLng;
  US2   birthPack;      // YYYY-MM-DD
  US2   registerPack;   // YYYY-MM-DD
  US2   activePack;     // YYYY-MM-DD

  S4    permit;         // Permit to write
} MemSpecRec, *MemSpec;

// Create or update this member, may require a new object to replace old
//
extern Mem MemCreateOrUpdate(MemSpec memSpec);
extern Mem MemUpsert(MemSpec memSpec);

// Describe fields that can be easily updated
//
typedef struct _MemUpdateSpec {
  // Locate the member with memId, domId
  MemId   memId;
  DomId   domId;

  STR    activeDate;     // YYYY-MM-DD
  US1    hasPhoto;
  US1    isOnline;  
  US1    saPop;
  US1    saAct;

  // Internal fields -- don't change
  S4    debugSeq;
  US2   activePack;     // YYYY-MM-DD

} MemUpdateSpecRec, *MemUpdateSpec;

// Update the rapidly changing fields on an existing member 
//
extern Mem MemUpdate(MemUpdateSpec memSpec);

// Describe member to delete
//
typedef struct _MemDeleteSpec {
  MemId   memId;
  DomId   domId;
  S4    debugSeq;
} MemDeleteSpecRec, *MemDeleteSpec;

// Delete a member 
//
extern int MemDelete(MemDeleteSpec memSpec);


/****************************************************************/

/************/
/** Search **/
/************/

#define kS1Query_TagMax     100
#define kS1Result_MemMax  500

typedef US1 S1Score;

// Blend Preference == how much to blend a piece of evidence into the final score
//
typedef struct _S1Blend {
	unsigned blend    : 7;  // How much to blend this evidence (100 is best, 0 not at all)
	unsigned require  : 1;  // If evidence <= minScore, reject the whole thing
	unsigned negev    : 1;  // *NotSupported* Is evidence treated as good or bad
	unsigned reject   : 1;  // *NotSupported* If evidence >= maxScore, reject the whole thing
	unsigned minScore : 7;  // *NotSupported* min score before considering evidence (or rejecting outright)
	unsigned maxScore : 7;  // *NotSupported* max score before considering evidence (or rejecting outright)
} S1BlendRec, *S1Blend;

//NB: The following enums are defined separately in managed C# code.  If any of the underlying values are changed here,
//they must be changed in the corresponding enum in Matchnet.Search.Interfaces.
typedef enum {
  S1Sort_Unknown = 0,
  S1Sort_Newest = 1,
  S1Sort_Active = 2,
  S1Sort_Near = 3,
  S1Sort_Popular = 4,
  S1Sort_Online = 5
} S1SortKey;

typedef enum {
  S1HasPhoto_Either = 0,
  S1HasPhoto_Require = 1,
  S1HasPhoto_None = 2
} S1HasPhoto;

typedef enum {
  S1IsOnline_Either = 0,
  S1IsOnline_Require = 1,
  S1IsOnline_None = 2
} S1IsOnline;


// All the scores for a member in the basic search engine
//
typedef struct _S1Scores {
  S1Score bias;  // score to bias based on primary sort key
  S1Score match; // score for oveall match of other tags
  S1Score age;   // score for age match
  S1Score ldg;   // score for LDG distance
  S1Score near;  // score for nearness in c/s/c/z
  S1Score act;   // normalized score based on activity
  S1Score pop;   // normalized score based on popularity 
  S1Score hgt;   // score for height match
  S1Score wgt;   // score for height match
  S1Score total; // Overall score
} S1ScoresRec, *S1Scores;


typedef struct _S1Query {

  S2        sortKey;
  S4        resMax;
  DomId     domId;
  US1       genderMask;
  MemLocRec locKey;    // Full location as focus of search (maybe support more later)
  S2     locDst;       // Preferred radius from location in miles
  S1HasPhoto hasPhoto;     // -1 has none, 0 don't care, 1 require
  S1IsOnline isOnline;     // -1 not online, 0 don't care, 1 require
  US2    ageMinDesired;
  US2    ageMaxDesired;
  US2    hgtMinDesired;
  US2    hgtMaxDesired;
  US2    wgtMinDesired;
  US2    wgtMaxDesired;

  MemNVRec tags[kS1Query_TagMax];
  S1ScoresRec  blend;  // ??? This should be graduated to full blend structure

  // Debug params
  US2 debugLocLat, debugLocLng;

} S1QueryRec, *S1Query;

// Opaque handle to compiled query
typedef struct _S1Scan *S1Scan;
typedef struct _PRes   *PRes;

// Results includes stats, and list of tags and members
//
typedef struct _S1Result {
  S4        sequence;
  S1SortKey sortKey;
  US2       memMax;
  US2       memNum;
  S4        memAbout;
  MemId     memids[kS1Result_MemMax];
  US2       scores[kS1Result_MemMax];

  // Internal handles, don't touch
  S4      allocSize;
  S1Scan  xscan;
  void    *pres;
} S1ResultRec, *S1Result;

extern S1Result S1ResultNew(S1Query);
extern int      S1ResultXml(S1Result xres, PXml px, int mode);
extern void     S1ResultFree(S1Result);
