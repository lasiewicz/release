// Cmd.c
// pnelson 11/21/2005
// URL/CmdLine dispatcher

#include <time.h>
#include "peng.h"
#include "cmd.h"

#define PKGNAME PkgName_Cmd

// A macro that shows where in structure a named slot will be
// 
#define SO(rec, fld)	(int)(&(((rec)0)->fld))

/**********************************/
/** Each individual Command Spec **/
/**********************************/

static const CmdSpecRec cmdSpecQuit = {
	NULL,
	KW_quit, KW_null, sizeof(CmdSpecRec), { { 0 } }
};

static const CmdSpecRec cmdSpecHelp = {
	"'help type=Command' for detailed help on command",
	KW_help, KW_null, sizeof(CmdSpecRec), { { 0 } }
};

// The spec for showing the global state
typedef struct _CmdSpecTime {
	CmdSpecRec spec;
	S2 reset;
} CmdSpecTimeRec, *CmdSpecTime;

static const CmdSpecRec cmdSpecTime = {
	"Reset restarts the saved clock",
	KW_time, KW_null, sizeof(CmdSpecTimeRec), { 
		{ KW_mode, Arg_S2, SO(CmdSpecTime, reset) },
		{ 0 }
	}
};

// The spec for showing the global state
typedef struct _CmdSpecShowGsv {
	CmdSpecRec spec;
} CmdSpecShowGsvRec, *CmdSpecShowGsv;

static const CmdSpecRec cmdSpecShowGsv = {
	"Mode values are 0x111",
	KW_show, KW_gsv, sizeof(CmdSpecShowGsvRec), { 
		{ KW_mode, Arg_S2, SO(CmdSpecShowGsv, spec.mode) },
		{ 0 }
	}
};

// The spec for showing the global state
#define kCmd_LoadFileMax 128
typedef struct _CmdSpecLoad {
  CmdSpecRec spec;
  STR	fname[kCmd_LoadFileMax];
} CmdSpecLoadRec, *CmdSpecLoad;

static const CmdSpecRec cmdSpecLoad = {
  "Mode values are 0x1",
  KW_load, KW_null, sizeof(CmdSpecLoadRec), { 
	{ KW_mode, Arg_S2,  SO(CmdSpecLoad, spec.mode) },
	{ KW_file, Arg_Str, SO(CmdSpecLoad, fname), kCmd_LoadFileMax },
	{ 0 }
  }
};

// The spec for showing a Ldg
typedef struct _CmdSpecTagDb {
  CmdSpecRec spec;
  STR   name;
  STR   coding;
  S1    noAutoCreate;
  S4    codes [kCmd_PairsMax];
  char *values[kCmd_PairsMax];
} CmdSpecTagDbRec, *CmdSpecTagDb;

static const CmdSpecRec cmdSpecTagDb = {
  "coding=[Id|Mask|Value] n=NameOfAttr  {c=code,v=value}...",
  KW_tagdb, KW_null, sizeof(CmdSpecTagDbRec), { 
	{ KW_coding, Arg_Str, SO(CmdSpecTagDb, coding) },
	{ KW_name,   Arg_Str, SO(CmdSpecTagDb, name) },
	{ KW_noauto, Arg_S1, SO(CmdSpecTagDb, noAutoCreate) },
	// Now the tags
	{ KW_code,	 Arg_S4,  SO(CmdSpecTagDb, codes), kCmd_PairsMax },
	{ KW_value, Arg_Str, SO(CmdSpecTagDb, values), kCmd_PairsMax },
	{ 0 }
  }
};

typedef struct _CmdSpecShowTagDb {
  CmdSpecRec spec;
  STR name;
} CmdSpecShowTagDbRec, *CmdSpecShowTagDb;


static const CmdSpecRec cmdSpecShowTagDb = {
  "Mode values are 0x1",
  KW_show, KW_tagdb, sizeof(CmdSpecShowTagDbRec), { 
	{ KW_mode, Arg_S2,  SO(CmdSpecShowTagDb, spec.mode) },
	{ KW_name, Arg_Str, SO(CmdSpecShowTagDb, name) },
	{ 0 }
  }
};

// The spec for showing a member
typedef struct _CmdSpecShowMem {
	CmdSpecRec spec;
	S4	seq;
	S4	memId;
	S1	domId;
} CmdSpecShowMemRec, *CmdSpecShowMem;

static const CmdSpecRec cmdSpecShowMem = {
	"Specify seq or memid/domid, mode values are 0x11",
	KW_show, KW_mem, sizeof(CmdSpecShowMemRec), {
		{ KW_mode,  Arg_S2, SO(CmdSpecShowMem, spec.mode) },
		{ KW_seq,   Arg_S4, SO(CmdSpecShowMem, seq) },
		{ KW_memid, Arg_S4, SO(CmdSpecShowMem, memId) },
		{ KW_domid, Arg_S1, SO(CmdSpecShowMem, domId) },
		{ 0 }
	}
};

// The spec for showing a tag
typedef struct _CmdSpecShowTag {
	CmdSpecRec spec;
	S4	seq;
	STR	name;
	STR	value;
} CmdSpecShowTagRec, *CmdSpecShowTag;

static const CmdSpecRec cmdSpecShowTag = {
	"Specify seq or name/value, mode values are 0x11",
	KW_show, KW_tag, sizeof(CmdSpecShowTagRec), {
		{ KW_mode,  Arg_S2,  SO(CmdSpecShowTag, spec.mode) },
		{ KW_seq,   Arg_S4,  SO(CmdSpecShowTag, seq) },
		{ KW_name,  Arg_Str, SO(CmdSpecShowTag, name) },
		{ KW_value, Arg_Str, SO(CmdSpecShowTag, value) },
		{ 0 }
	}
};

// The spec for showing a tag
typedef struct _CmdSpecShowGrp {
	CmdSpecRec spec;
	S4	seq;
} CmdSpecShowGrpRec, *CmdSpecShowGrp;

static const CmdSpecRec cmdSpecShowGrp = {
	"Can only lookup by seq, mode values are 0x11",
	KW_show, KW_grp,	sizeof(CmdSpecShowGrpRec), {
		{ KW_mode, Arg_S2, SO(CmdSpecShowGrp, spec.mode) },
		{ KW_seq,  Arg_S4, SO(CmdSpecShowGrp, seq) },
		{ 0 }
	}
};

// The spec for showing a Ldg
typedef struct _CmdSpecShowLdg {
	CmdSpecRec spec;
	S4	seq;
	S1	domId;
	S1	gender;
	S2	bLat, bLng;
	float	fLat, fLng;
} CmdSpecShowLdgRec, *CmdSpecShowLdg;

static const CmdSpecRec cmdSpecShowLdg = {
	"Specify seq or domid/gender/loc where loc is bLat/bLng (box) or fLat/fLng (radians), mode is 0x11",
	KW_show, KW_ldg,	sizeof(CmdSpecShowLdgRec), {
		{ KW_mode,   Arg_S2,  SO(CmdSpecShowLdg, spec.mode) },
		{ KW_seq,    Arg_S4, SO(CmdSpecShowLdg, seq) },
		{ KW_domid,  Arg_S1, SO(CmdSpecShowLdg, domId) },
		{ KW_gender, Arg_S1, SO(CmdSpecShowLdg, gender) },
		{ KW_boxlat, Arg_S2, SO(CmdSpecShowLdg, bLat) },
		{ KW_boxlng, Arg_S2, SO(CmdSpecShowLdg, bLng) },
		{ KW_fltlat, Arg_Float, SO(CmdSpecShowLdg, fLat) },
		{ KW_fltlng, Arg_Float, SO(CmdSpecShowLdg, fLng) },
		{ 0 }
	}
};

// The spec for showing a Ldg
typedef struct _CmdSpecSearch {
	CmdSpecRec   spec;
    S1QueryRec   s1qry;
	char *tagNames[kMemSpec_TagsMax];
	char *tagValues[kMemSpec_TagsMax];
} CmdSpecSearchRec, *CmdSpecSearch;

static const CmdSpecRec cmdSpecSearch = {
	"Search domid=dd&gender=[m|f] required, bLat/bLng overrides fLat/fLng",
	KW_search, KW_null,	 sizeof(CmdSpecSearchRec), {
		{ KW_mode,	Arg_S2,	SO(CmdSpecSearch, spec.mode) },

		{ KW_sortkey,	Arg_S2,	SO(CmdSpecSearch, s1qry.sortKey) },
		{ KW_resmax,	Arg_S4,	SO(CmdSpecSearch, s1qry.resMax) },

		{ KW_domid,	Arg_S1,	SO(CmdSpecSearch, s1qry.domId) },
		{ KW_gender,	Arg_S1,	SO(CmdSpecSearch, s1qry.genderMask) },
		{ KW_distance,	Arg_S2,	 SO(CmdSpecSearch, s1qry.locDst) },
		{ KW_boxlat,	Arg_S2,	SO(CmdSpecSearch, s1qry.debugLocLat) },
		{ KW_boxlng,	Arg_S2,	SO(CmdSpecSearch, s1qry.debugLocLng) },
		{ KW_fltlat,	Arg_Float, SO(CmdSpecSearch, s1qry.locKey.regLat) },
		{ KW_fltlng,	Arg_Float, SO(CmdSpecSearch, s1qry.locKey.regLng) },
		{ KW_country,	Arg_Str, SO(CmdSpecSearch, s1qry.locKey.regCountry) },
		{ KW_state,	Arg_Str, SO(CmdSpecSearch, s1qry.locKey.regState) },
		{ KW_city,	Arg_Str, SO(CmdSpecSearch, s1qry.locKey.regCity) },
		{ KW_zip,	Arg_Str, SO(CmdSpecSearch, s1qry.locKey.regZip) },
		{ KW_hasphoto,	Arg_S1,	 SO(CmdSpecSearch, s1qry.hasPhoto) },
		{ KW_isonline,	Arg_S1,	 SO(CmdSpecSearch, s1qry.isOnline) },
		{ KW_agemin,	Arg_S2,	 SO(CmdSpecSearch, s1qry.ageMinDesired) },
		{ KW_agemax,	Arg_S2,	 SO(CmdSpecSearch, s1qry.ageMaxDesired) },
		{ KW_hgtmin,	Arg_S2,	 SO(CmdSpecSearch, s1qry.hgtMinDesired) },
		{ KW_hgtmax,	Arg_S2,	 SO(CmdSpecSearch, s1qry.hgtMaxDesired) },

		// ??? Add the blends

		// Now the tags
		{ KW_name,	 Arg_Str, SO(CmdSpecSearch, tagNames),	kMemSpec_TagsMax },
		{ KW_value, Arg_Str, SO(CmdSpecSearch, tagValues), kMemSpec_TagsMax },
		{ 0 }
	}
};

// The spec for creating a member
typedef struct _CmdSpecMemUpsert {
	CmdSpecRec	spec;
	MemSpecRec	mspec;
	char	*tagNames[kMemSpec_TagsMax];
	char	*tagValues[kMemSpec_TagsMax];
} CmdSpecMemUpsertRec, *CmdSpecMemUpsert;

static const CmdSpecRec cmdSpecMemUpsert = {
  "Mem memId/domId/gender and a location are required, bLat/bLng overrides fLat/fLng",
  KW_mem, KW_null,  sizeof(CmdSpecMemUpsertRec), {
	{ KW_mode, Arg_S2,  SO(CmdSpecMemUpsert, spec.mode) },
	{ KW_memid,    Arg_S4,  SO(CmdSpecMemUpsert, mspec.memId) },
	{ KW_domid,    Arg_S1,  SO(CmdSpecMemUpsert, mspec.domId) },
	{ KW_gender,   Arg_S1,  SO(CmdSpecMemUpsert, mspec.genderMask) },

	{ KW_fltlat, Arg_Float, SO(CmdSpecMemUpsert, mspec.memLoc.regLat) },
	{ KW_fltlng, Arg_Float, SO(CmdSpecMemUpsert, mspec.memLoc.regLng) },
	{ KW_country,  Arg_Str, SO(CmdSpecMemUpsert, mspec.memLoc.regCountry) },
	{ KW_state,    Arg_Str, SO(CmdSpecMemUpsert, mspec.memLoc.regState) },
	{ KW_city,     Arg_Str, SO(CmdSpecMemUpsert, mspec.memLoc.regCity) },
	{ KW_zip,      Arg_Str, SO(CmdSpecMemUpsert, mspec.memLoc.regZip) },
	{ KW_areacode, Arg_Str, SO(CmdSpecMemUpsert, mspec.memLoc.regAreaCode) },

	{ KW_birthdate,    Arg_Str, SO(CmdSpecMemUpsert, mspec.birthDate) },
	{ KW_registerdate, Arg_Str, SO(CmdSpecMemUpsert, mspec.registerDate) },
	{ KW_activedate,   Arg_Str, SO(CmdSpecMemUpsert, mspec.activeDate) },

	{ KW_hgt,    Arg_S2,  SO(CmdSpecMemUpsert, mspec.height) },
	{ KW_wgt,    Arg_S2,  SO(CmdSpecMemUpsert, mspec.weight) },

	{ KW_childcnt,  Arg_S1,  SO(CmdSpecMemUpsert, mspec.childrenCount) },
	{ KW_relocate,  Arg_S1,  SO(CmdSpecMemUpsert, mspec.relocateFlag) },
	{ KW_hasphoto,  Arg_S1,  SO(CmdSpecMemUpsert, mspec.hasPhoto) },
	{ KW_isonline,  Arg_S1,  SO(CmdSpecMemUpsert, mspec.isOnline) },
	{ KW_saact,     Arg_S1,  SO(CmdSpecMemUpsert, mspec.saAct) },
	{ KW_sapop,     Arg_S1,  SO(CmdSpecMemUpsert, mspec.saPop) },

	{ KW_seq,  Arg_S4,  SO(CmdSpecMemUpsert, mspec.debugSeq) },
	{ KW_boxlat, Arg_S2,  SO(CmdSpecMemUpsert, mspec.debugLocLat) },
	{ KW_boxlng, Arg_S2,  SO(CmdSpecMemUpsert, mspec.debugLocLng) },
	
	// Now the tags
	{ KW_name,   Arg_Str, SO(CmdSpecMemUpsert, tagNames),  kMemSpec_TagsMax },
	{ KW_value,  Arg_Str, SO(CmdSpecMemUpsert, tagValues), kMemSpec_TagsMax },
	{ 0 }
  }
};

// The spec for updating a member
typedef struct _CmdSpecMemUpdate {
	CmdSpecRec	spec;
	MemUpdateSpecRec mspec;
} CmdSpecMemUpdateRec, *CmdSpecMemUpdate;

static const CmdSpecRec cmdSpecMemUpdate = {
  "Update memId/domId or seq",
  KW_update, KW_null,  sizeof(CmdSpecMemUpdateRec), {
	{ KW_mode, Arg_S2,  SO(CmdSpecMemUpdate, spec.mode) },
	{ KW_memid,    Arg_S4,  SO(CmdSpecMemUpdate, mspec.memId) },
	{ KW_domid,    Arg_S1,  SO(CmdSpecMemUpdate, mspec.domId) },
	{ KW_seq,      Arg_S4,  SO(CmdSpecMemUpdate, mspec.debugSeq) },
	{ KW_activedate, Arg_Str, SO(CmdSpecMemUpdate, mspec.activeDate) },
	{ KW_hasphoto,   Arg_S1,  SO(CmdSpecMemUpdate, mspec.hasPhoto) },
	{ KW_isonline,   Arg_S1,  SO(CmdSpecMemUpdate, mspec.isOnline) },
	{ KW_saact,      Arg_S1,  SO(CmdSpecMemUpdate, mspec.saAct) },
	{ KW_sapop,      Arg_S1,  SO(CmdSpecMemUpdate, mspec.saPop) },
	{ 0 }
  }
};

// The spec for updating a member
typedef struct _CmdSpecMemDelete {
	CmdSpecRec	spec;
	MemDeleteSpecRec mspec;
} CmdSpecMemDeleteRec, *CmdSpecMemDelete;

static const CmdSpecRec cmdSpecMemDelete = {
  "Delete memId/domId or seq",
  KW_delete, KW_null,  sizeof(CmdSpecMemDeleteRec), {
	{ KW_mode, Arg_S2,  SO(CmdSpecMemDelete, spec.mode) },
	{ KW_memid,    Arg_S4,  SO(CmdSpecMemDelete, mspec.memId) },
	{ KW_domid,    Arg_S1,  SO(CmdSpecMemDelete, mspec.domId) },
	{ KW_seq,      Arg_S4,  SO(CmdSpecMemDelete, mspec.debugSeq) },
	{ 0 }
  }
};

// All the specs in our grammar

static const CmdSpec cmdGrammar[] = {
	(CmdSpec)&cmdSpecQuit,
	(CmdSpec)&cmdSpecHelp,
	(CmdSpec)&cmdSpecTime,
	(CmdSpec)&cmdSpecLoad,
	(CmdSpec)&cmdSpecTagDb,
	(CmdSpec)&cmdSpecShowGsv,
	(CmdSpec)&cmdSpecShowMem,
	(CmdSpec)&cmdSpecShowTag,
	(CmdSpec)&cmdSpecShowTagDb,
	(CmdSpec)&cmdSpecShowGrp,
	(CmdSpec)&cmdSpecShowLdg,
	(CmdSpec)&cmdSpecSearch,
	(CmdSpec)&cmdSpecMemUpsert,
	(CmdSpec)&cmdSpecMemUpdate,
	(CmdSpec)&cmdSpecMemDelete,
	0 
};


/****************************************************************/

static int cmdExecHelp(KWEnum typ, PXml px)
{
  return CmdGrammarShowHelp(cmdGrammar, typ, px);
}

static int cmdExecTime(CmdSpecTime cs, PXml px)
{ 
  static time_t timeStart = 0;
  static time_t timeBase = 0;
  char buf[128];
  time_t timeNow;
  int err = 0;

  timeNow = time(&timeNow);

  if (!timeStart)
	timeStart = timeNow;

  if (!timeBase) 
	timeBase = timeNow;

  sprintf(buf, "Time now='%ld' toBase='%ld' toStart='%ld'",
		  timeNow, timeNow-timeBase, timeNow-timeStart);

  PEngLog(PKGNAME, buf, NULL, 0);
  err = PXmlPush0(px, buf, 0);

  if (cs->reset)
	timeBase = 0;

  return err;
}

static int cmdExecLoad(CmdSpecLoad cs, PXml px)
{
  int fcnt, errCode = 0, err;
  char msgBuf[256];
  FILE *ff;

  for (fcnt = 0; cs->fname[fcnt]; fcnt++) {
	char *fname = cs->fname[fcnt];
	snprintf(msgBuf, sizeof(msgBuf), "Load file='%s'", fname);
	errCode = PXmlPush(px, msgBuf, 0);
	ff = fopen(fname, "r");
	
	if (!ff) {
	  snprintf(msgBuf, sizeof(msgBuf), "Load error='CantOpen'");
	  errCode = PXmlPush0(px, msgBuf, 0);
	} else {
	  if ((err = LoadFile(fname, ff, px, cs->spec.mode))) {
		snprintf(msgBuf, sizeof(msgBuf), "Load error='%d'", err);
		errCode = PXmlPush0(px, msgBuf, 0);
	  }
	  fclose(ff);
	}
	errCode |= PXmlPop(px);
  }
  return errCode;
}

static int cmdExecTagDb(CmdSpecTagDb cs, PXml px)
{
  char ch, msgBuf[128];
  TagDbCoding coding;
  TagDbId tdx;
  STR name; int i;

  coding = TagDb_Id;
  if (cs->coding && (ch = cs->coding[0])) {
	if (ch == 'M' || ch == 'm') 
	  coding = TagDb_Mask;
	else if (ch == 'V' || ch == 'v') 
	  coding = TagDb_Value;
  }

  name = (cs->name && cs->name[0] ? cs->name : NULL);

  if (name) {
	if ((tdx = TagDbFindOrNew(name, coding, cs->noAutoCreate))) {
	  for (i=0; cs->values[i] || cs->codes[i]; i++)
		if (!TagDbTagAssign(tdx, cs->values[i], cs->codes[i]))
		  break;
	}
  }
  snprintf(msgBuf, sizeof(msgBuf), "TagDb name='%s' coding='%s' noauto='%d' count='%d'",
		  name, (coding == TagDb_Mask) ? "Mask" : "Num", cs->noAutoCreate, i);
  return PXmlPush0(px, msgBuf, 0);
}

static int cmdExecSearch(CmdSpecSearch cs, PXml px)
{
	S1Query s1qry = &(cs->s1qry);
	S1Result xres = NULL;
	int i;
	
	// Load the tags
	for (i = 0; cs->tagNames[i]; i++) {
		s1qry->tags[i].name	  = cs->tagNames[i];
		s1qry->tags[i].value = cs->tagValues[i];
	}

	if ((xres = S1ResultNew(s1qry))) {
	  S1ResultXml(xres, px, cs->spec.mode);
	  S1ResultFree(xres);
	}

	return 0;
}

static int cmdExecMemUpsert(CmdSpecMemUpsert cs, PXml px)
{
	MemSpec mspec = &(cs->mspec);
	Mem xmem = NULL;
	int i;

	if (!mspec->memId || !mspec->domId || !mspec->genderMask) {
		PXmlSetStr(px, "Mem error='Args'", "Must specify memId, domId, gender");
		return 1;
	}
	// Load the tags
	for (i = 0; cs->tagNames[i]; i++) {
		mspec->tags[i].name	 = cs->tagNames[i];
		mspec->tags[i].value = cs->tagValues[i];
	}
	xmem = MemUpsert(mspec);
	if (!xmem) {
		PXmlSetStr(px, "Mem error='NoneCreated'", "???");
		return 1;
	}
	MemXml(px, xmem, cs->spec.mode);
	return 0;
}

static int cmdExecMemUpdate(CmdSpecMemUpdate cs, PXml px)
{
	MemUpdateSpec mspec = &(cs->mspec);
	Mem xmem = NULL;

	if (!mspec->debugSeq && (!mspec->memId || !mspec->domId)) {
		PXmlSetStr(px, "Update error='Args'", "Must specify memId,domId");
		return 1;
	}
	xmem = MemUpdate(mspec);
	if (!xmem) {
		PXmlSetStr(px, "Update error='NoneFound'", "???");
		return 1;
	}
	MemXml(px, xmem, cs->spec.mode);
	return 0;
}

static int cmdExecMemDelete(CmdSpecMemDelete cs, PXml px)
{
	MemDeleteSpec mspec = &(cs->mspec);
	int err;

	if (!mspec->debugSeq && (!mspec->memId || !mspec->domId)) {
		PXmlSetStr(px, "Delete error='Args'", "Must specify memId,domId");
		return 1;
	}
	err = MemDelete(mspec);
	if (err) {
		char msgBuf[128];
		snprintf(msgBuf, sizeof(msgBuf), "Delete err='%d' memId='%ld' domId='%d' seq='%d'",
				 err, mspec->memId, mspec->domId, mspec->debugSeq);
		PXmlSetStr(px, msgBuf, "???");
		return 1;
	}
	return 0;
}

static int cmdExecShowGsv(CmdSpecShowGsv sgx, PXml px)
{ 
	return GsvXml(px, (int)sgx->spec.mode);
}

static int cmdExecShowMem(CmdSpecShowMem smx, PXml px)
{
	int errCode = 0;
	Mem xmem = NULL;
	if (smx->seq) {
		xmem = MemBySequence(smx->seq);
	} else if (smx->memId) {
		MemKeyRec mk;
		StructNull(mk);
		mk.memId = smx->memId;
		mk.domId = smx->domId;
		xmem = MemFind(&mk);
	}
	if (xmem) {
		errCode = MemXml(px, xmem, smx->spec.mode);
	} else {
		char msgBuf[128];
		snprintf(msgBuf, sizeof(msgBuf), "Mem err='NotFound' memId='%ld' domId='%d' mode='%d'",
			smx->memId, smx->domId, smx->spec.mode);
		errCode = PXmlPush0(px, msgBuf, smx->seq);
	}
	return errCode;
}

static int cmdExecShowTag(CmdSpecShowTag stx, PXml px)
{
	int errCode = 0;
	Tag xtag = NULL;
	if (stx->seq) {
		xtag = TagBySequence(stx->seq);
	} else if (stx->name) {
		TagKeyRec tk;
		StructNull(tk);
		str0ncpy(tk.name, stx->name, sizeof(tk.name));
		str0ncpy(tk.value, stx->value, sizeof(tk.value));
		xtag = TagFind(&tk);
	}
	if (xtag) {
		errCode = TagXml(px, xtag, stx->spec.mode);
	} else {
		char msgBuf[128];
		snprintf(msgBuf, sizeof(msgBuf), "Tag err='NotFound' name='%s' value='%s' mode='%d'",
						stx->name, stx->value, stx->spec.mode);
		errCode = PXmlPush0(px, msgBuf, stx->seq);
	}
	return errCode;
}

static int cmdExecShowTagDb(CmdSpecShowTagDb cs, PXml px)
{
  char msgBuf[128];
  TagDbId tdx = 0;
  if (cs->name && cs->name[0]) {
	if (!(tdx = TagDbFind(cs->name))) {
	  snprintf(msgBuf, sizeof(msgBuf), "TagDb err='NotFound' name='%s'", cs->name);
	  return PXmlPush0(px, msgBuf, 0);
	}
  }
  return TagDbXml(px, tdx, (int)cs->spec.mode);
}

static int cmdExecShowGrp(CmdSpecShowGrp sgx, PXml px)
{
	int errCode = 0;
	Grp xgrp = NULL;
	if (sgx->seq) {
		xgrp = GrpBySequence(sgx->seq);
		errCode = GrpXml(px, xgrp, sgx->spec.mode);
	} else {
		char msgBuf[128];
		snprintf(msgBuf, sizeof(msgBuf), "Grp err='NotFound'");
		errCode = PXmlPush0(px, msgBuf, sgx->seq);
	}
	return errCode;
}

static int cmdExecShowLdg(CmdSpecShowLdg slx, PXml px)
{
	int errCode = 0;
	Ldg xldg = NULL;
	if (slx->seq) {
		xldg = LdgBySequence(slx->seq);
	} else if (slx->gender) {
		LdgKeyRec lk;
		StructNull(lk);
		if (slx->fLat || slx->fLng) {
			MemLocRec ml;
			ml.regLat = slx->fLat;
			ml.regLng = slx->fLng;
			MemPackLoc(&lk.loc, &ml);
		} else {
			lk.loc.lat = slx->bLat;
			lk.loc.lng = slx->bLng;
		}
		lk.domId	= slx->domId;
		lk.gender = slx->gender;
		xldg = LdgFind(&lk);
	}
	if (xldg) {
		errCode = LdgXml(px, xldg, slx->spec.mode);
	} else {
		char msgBuf[128];
		snprintf(msgBuf, sizeof(msgBuf), "Ldg err='NotFound'");
		errCode = PXmlPush0(px, msgBuf, slx->seq);
	}
	return errCode;
}

// STR cmdLine   :: command to process in text buffer, will be modified
// PXml px       :: XML structure to populate
// int readOnly  :: flag to indicate that only ops should be readonly
// S4 notused    :: not used
//
int CmdEval(STR cmdLine, PXml px, int readOnly, S4 notused)
{
  CmdSpec spec;
  int errCode = 0;

  PEngLog(PkgName_Cmd, "## %s", cmdLine, 0);

  spec = CmdSpecNew(cmdLine, cmdGrammar);

  if (spec) {
	switch (spec->cmd) {
	case KW_quit:		errCode = -1; break;
	case KW_help:		errCode = cmdExecHelp(spec->typ, px); break;
	case KW_time:		errCode = cmdExecTime((CmdSpecTime)spec, px); break;
	case KW_tagdb:		errCode = cmdExecTagDb((CmdSpecTagDb)spec, px); break;
	case KW_search:	errCode = cmdExecSearch((CmdSpecSearch)spec, px); break;
	case KW_show: 
	  {
		switch (spec->typ) {
		case KW_gsv:
		  errCode = cmdExecShowGsv((CmdSpecShowGsv)spec, px);
		  break;
		case KW_mem:
		  errCode = cmdExecShowMem((CmdSpecShowMem)spec, px);
		  break;
		case KW_tag:
		  errCode = cmdExecShowTag((CmdSpecShowTag)spec, px);
		  break;
		case KW_tagdb:
		  errCode = cmdExecShowTagDb((CmdSpecShowTagDb)spec, px);
		  break;
		case KW_grp:
		  errCode = cmdExecShowGrp((CmdSpecShowGrp)spec, px);
		  break;
		case KW_ldg:
		  errCode = cmdExecShowLdg((CmdSpecShowLdg)spec, px);
		  break;
		default:
		  break;
		}
	  } break;
	case KW_update:
	  // Can be done w/o locking
	  errCode = cmdExecMemUpdate((CmdSpecMemUpdate)spec, px); 
	  break;
	case KW_delete:
	  // Must be locked
	  if (readOnly) errCode = 1;
	  else errCode = cmdExecMemDelete((CmdSpecMemDelete)spec, px); 
	  break;
	case KW_load:
	  // Must be locked
	  if (readOnly) errCode = 1;
	  else errCode = cmdExecLoad((CmdSpecLoad)spec, px); 
	  break;
	case KW_mem:
	  // Must be locked
	  if (readOnly) errCode = 1;
	  else errCode = cmdExecMemUpsert((CmdSpecMemUpsert)spec, px); 

	default:
	  errCode = 1;
	}
  }
  CmdSpecFree(spec);
  return errCode;
}
