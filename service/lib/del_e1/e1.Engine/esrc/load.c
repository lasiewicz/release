// load.c
// pnelson 11/20/2005
// Utility class to help load members from list of strings

#include "time.h"
#include "peng.h"
#include "kw.h"

#define PKGNAME PkgName_Load

#define kLoad_FieldsMax      256
#define kLoad_StatusPageSize (16*1024)

// The opaque pattern structure
//
typedef struct _LoadPattern {
  char       fname[128];              // Filename for error reporting
  LoadStatus status;                  // Status of our loads
  S2        num;                      // How many columns in this csv
  KWEnum fields[kLoad_FieldsMax];  // Per column, is it builtin
  S4        xdx[kLoad_FieldsMax];     // Per column, maybe TagDbId or Stx of col name
  
  LoadStatus curPage;  // Where we are recording status for current line 

  PermitWrite permit;  // Grab the write permit for the whole load

} LoadPatternRec;

// Builtin fields will have tagDb created for them
//
static const struct { KWEnum field; TagDbCoding coding; } xxLoadTab[] = {
  { KW_memid },
  { KW_domid },
  { KW_gender },
  { KW_gendermask },
  { KW_fltlat },
  { KW_fltlng },
  { KW_country },
  { KW_state },
  { KW_city },
  { KW_zip },
  { KW_areacode },
  { KW_birthdate },
  { KW_registerdate },
  { KW_activedate },
  { KW_updatedate },
  { KW_hgt },
  { KW_wgt },
  { KW_hasphoto },
  { KW_isonline },
  { KW_saact },
  { KW_sapop },
  { KW_emailcount },
  { KW_childcnt },
  { KW_relocate },
  { KW_name  },
  { KW_value },
  { KW_tag },
};

#define xxLoadTabNum (sizeof(xxLoadTab)/sizeof(xxLoadTab[0]))

//  Sentinels for the loader, indicates that columnheader is name of tag, contents per row are value
#define KW_tagbyname  (KWEnum)((int)KW_last+1)
#define KW_tagbydb    (KWEnum)((int)KW_last+2)

static char *xLoadStatusNames[] = {
  "Ok",            // LoadStatus_Ok = 0,
  "MemIdMissing",  // LoadStatus_MemIdMissing = 1,
  "DomIdMissing",  // LoadStatus_DomIdMissing = 2,
  "GenMissing",    // LoadStatus_GenMissing   = 3,
  "TagsTooMany"    // LoadStatus_TagsTooMany  = 4
};

// Seed the tagDb table with all the builtins so they can be fetched for help
//
void LoadInitialize()
{
  if (!gsv->loaderInit) {
	int i;
	for (i=0; i< xxLoadTabNum; i++) {
	  KWEnum     field   = xxLoadTab[i].field;
	  cSTR        name   = KWLookupStr(field);
	  TagDbCoding coding = xxLoadTab[i].coding ? xxLoadTab[i].coding : TagDb_Value;
	  TagDbId     tdx    = TagDbFindOrNew(name, coding, 1);
	  TagDbInternal(tdx, (S4)field);
	}
	gsv->loaderInit = 1;
  }
}

/****************/

static KWEnum byName(cSTR name, S4 *xdxp)
{
  TagDbId tdx;
  Stx     nid;
  KWEnum  lf;

  // KW may have multiple possible names, so first try to map string to key, then key to primary name
  if ((lf = KWLookupKey(name)))
	name = KWLookupStr(lf);

  // See if we know it in our tagDb (this will catch internals too)
  if ((tdx = TagDbFind(name))) {
	*xdxp = tdx;
	if ((lf = (KWEnum)TagDbInternal(tdx, 0)))
	  return lf;
	else
	  return KW_tagbydb;
  }
  // Otherwise, assume column name is name of tag, contents will be value
  if ((name[0] != '_') && (nid = StxDbFindOrNew(gsv->pkTags, name))) {
	*xdxp = nid;
	return KW_tagbyname;
  }

  *xdxp = 0;
  return KW_null;
}

static char *nvSplit(char *val)
{
  while (*val && *val != ':') 
	val++;
  if (*val == ':') 
	*val++ = 0;
  return val;
}

#define isBreak(ch)    ((ch)=='\t'||(ch)==',')
#define isSpace(ch)    ((ch)==' '||(ch)=='\r'||(ch)=='\n')

static char *popString(char **strp)
{
	char *str, *head; int i;

	str = *strp;
	// Trim leading space
	while (*str && (*str == ' ')) ++str;
	head = str;
	// Walk to the break
	while (*str && !isBreak(*str)) str++;
	// Replace break with NULL if this isn't the end
	if (*str) *str++ = 0;
	// Reset our parents ptr to start of next
	*strp = str;
	// Trim trailing space from selected string
	i = strlen(head); 
	while (i-->0) {
		if (isSpace(head[i])) head[i] = 0; 
		else break;
	}
	return head;
}

/****************/

LoadPattern LoadPatternNew(cSTR fname, char *header)
{
  LoadPattern xlp; int i;

  xlp = PEngAlloc(PKGNAME, sizeof(LoadPatternRec));
  if (fname) str0ncpy(xlp->fname, fname, sizeof(xlp->fname));
  for (i = 0; i < kLoad_FieldsMax && header && *header; i++) {
	STR val = popString(&header);
	xlp->fields[i] = byName(val, &(xlp->xdx[i]));
	if (xlp->fields[i] == KW_null)
	  PEngLog(PKGNAME, "Unknown Field: %s", val, 0);
	if (xlp->fields[i] == KW_tagbyname)
	  PEngLog(PKGNAME, "TagByName Field: %s", val, 0);
  }
  xlp->num = i;
  return xlp;
}

LoadStatus LoadPatternStatus(LoadPattern xlp)
{
  if (xlp)
	return xlp->status;
  else
	return NULL;
}

#define loadStatusAllocSize(page) (sizeof(LoadStatusRec) + ((page)*sizeof(LoadStatusInstRec)))

void LoadPatternFree(LoadPattern xlp)
{
  if (xlp) {
	LoadStatus status;
	while ((status = xlp->status)) {
	  xlp->status = status->nextPage;
	  PEngFree(PKGNAME, status, loadStatusAllocSize(status->maxInst));
	}
	PEngFree(PKGNAME, xlp, sizeof(LoadPatternRec));
  }
}

static LoadStatus xLoadStatusSetup(LoadPattern xlp)
{
  LoadStatus page;
  if (!(page = xlp->curPage) || (page->numInst >= page->maxInst)) {
	S4 maxInst = kLoad_StatusPageSize;
	if ((page = PEngAlloc(PKGNAME, loadStatusAllocSize(maxInst)))) {
	  page->datInst = (LoadStatusInstRec*)((US1*)page + sizeof(LoadStatusRec));
	  page->nextPage = NULL;
	  page->numInst = 0;   // Will be incremented to start at zero
	  page->maxInst = maxInst;
	}
	if (!xlp->curPage) { xlp->status = page; } // start it
	else               { xlp->curPage->nextPage = page; } // append it
	xlp->curPage = page;
  }
  if (xlp->curPage)
	xlp->curPage->numInst++;
  return xlp->curPage;
}

// pass fname and lineNum for reporting
//
int LoadPatternLoad(LoadPattern xlp, MemSpec memSpec, char *data, S4 lineNum)
{
  int i;
  S4     code;
  Tag    xtag;
  cSTR    dynamicName;  // Holds the name of the current tag
  TagDbId dynamicTdx;   // Holds the TagDb of the current tag (if present)
  Tag *memTagDat = (Tag*)memSpec->tobj;  // Holds the actual tag structures
  S2  memTagIdx = memSpec->tobjCnt;  // How many are there
  S2  memTagMax = ArrayCount(memSpec->tobj);
  LoadStatus statPage;
  LoadStatusInstRec statInstRec;
  LoadStatusInst statInst;

  char errBuf[64];

  // Grab a copy of the line in case we have an error
  str0ncpy(errBuf, data, sizeof(errBuf));

  // Setup space for status
  if ((statPage = xLoadStatusSetup(xlp))) {
	statInst = &(statPage->datInst[statPage->numInst-1]);
	statInst->status = 0;
	statInst->lineNum = lineNum;
  } else {
	StructNull(statInstRec);
	statInst = &statInstRec;
  }
  
  for (i = 0; i < xlp->num && data && *data; i++) {
	STR val = popString(&data);
	if (val) {
	  switch (xlp->fields[i]) {
	  case KW_null:  
		break;
	  case KW_memid:        memSpec->memId       = (MemId)atol(val); break;
	  case KW_domid:        memSpec->domId       = (DomId)atol(val); break;
	  case KW_gender:       memSpec->genderMask  = (US1)atol(val); break;
	  case KW_gendermask:   memSpec->genderMask  = (US1)atol(val); break;
	  case KW_fltlat:       memSpec->memLoc.regLat      = (float)atof(val); break;
	  case KW_fltlng:       memSpec->memLoc.regLng      = (float)atof(val); break;
	  case KW_country:      memSpec->memLoc.regCountry  = val; break;
	  case KW_state:        memSpec->memLoc.regState    = val; break;
	  case KW_city:         memSpec->memLoc.regCity     = val; break;
	  case KW_zip:          memSpec->memLoc.regZip      = val; break;
	  case KW_areacode:     memSpec->memLoc.regAreaCode = val; break;
	  case KW_birthdate:    memSpec->birthPack    = MemPackDate(val); break;
	  case KW_registerdate: memSpec->registerPack = MemPackDate(val); break;
	  case KW_activedate:   memSpec->activePack   = MemPackDate(val); break;
	  case KW_hgt:          memSpec->height       = (US2)atol(val); break;
	  case KW_wgt:          memSpec->weight       = (US2)atol(val); break;
	  case KW_hasphoto: memSpec->hasPhoto   = (US1)atol(val); break;
	  case KW_isonline: memSpec->isOnline   = (US1)atol(val); break;
	  case KW_saact:    memSpec->saAct      = (US1)atol(val); break;
	  case KW_sapop:    memSpec->saPop      = (US1)atol(val); break;
	  case KW_emailcount: memSpec->saPop    = (US1)atol(val); break; //TOFIX:???
	  case KW_childcnt: memSpec->childrenCount      = (US1)atol(val); break;
	  case KW_relocate: memSpec->relocateFlag = (US1)atol(val); break;
	  case KW_updatedate: break; //for internal DB use only -- ignore
		
		// Load tags too, Name/Value comes in ordered pairs
	  case KW_name:  
		if (!val[0])
		  { dynamicName = NULL; dynamicTdx  = 0; } // No tag name
		else
		  { dynamicName = val;  dynamicTdx  = TagDbFind(val); }
		break;
	  case KW_tag:
		if (val[0]) {
		  dynamicName = val;
		  // If we find a ':' split this string into its two parts
		  val = nvSplit(val);
		  dynamicTdx  = TagDbFind(dynamicName);
		}
		// Drop thru and process val
	  case KW_value: 
		if (val[0]) {  // No value means ignore
		  if (dynamicTdx) {
			if ((code = atol(val)) != -1) // val=-1 for a known field means ignore
			  memTagIdx += TagDbByCode(dynamicTdx, code, &(memTagDat[memTagIdx]));
		  } else if (dynamicName) {
			if ((xtag = TagFindOrNewStr(dynamicName, val)))
			  memTagDat[memTagIdx++] = xtag;
		  }
		}
		dynamicName = NULL;
		dynamicTdx  = 0;
		break;

	  case KW_tagbydb:
		if (val[0]) {
		  // This column contains codes for a known tag name
		  TagDbId xx = xlp->xdx[i];
		  code = atol(val);
		  memTagIdx += TagDbByCode(xx, code, memTagDat+memTagIdx);
		}
		break;

	  case KW_tagbyname:
		if (val[0]) {
		  // This column contains string values for tags with name of column
		  TagKeyRec tk; 
		  StructNull(tk);
		  tk.nId = xlp->xdx[i]; 
		  str0ncpy(tk.value, val, sizeof(tk.value));
		  if ((xtag = TagFindOrNew(&tk)))
			memTagDat[memTagIdx++] = xtag;
		}
		break;

	  default: 
		break;
	  }
	  // See if we have too many tags
	  if (memTagIdx + 32 >= memTagMax) {
		char xBuf[64]; 
		snprintf(xBuf, sizeof(xBuf), "line='%ld' memid='%ld' domid='%d'", 
				 lineNum, memSpec->memId, memSpec->domId);
		statInst->status = (int)LoadStatus_TagsTooMany;
		PEngError(PKGNAME, xBuf, "TooManyTags");
		break;
	  }
	}
  }
  // Any remaining fields are loaded as tags
  while (memTagIdx < memTagMax && data && *data) {
	STR nx = popString(&data);
	STR vx = nvSplit(nx);
	if ((xtag = TagFindOrNewStr(nx, vx)))
	  memTagDat[memTagIdx++] = xtag;
  }
  // Cap the list
  memTagDat[memTagIdx] = NULL;
  memSpec->tobjCnt = memTagIdx;
  
  // Remember the status
  statInst->memId  = memSpec->memId;
  statInst->domId  = memSpec->domId;
  statInst->gender = memSpec->genderMask;

  if (!statInst->memId)
	statInst->status = LoadStatus_MemIdMissing;
  else if (!statInst->domId)
	statInst->status = LoadStatus_DomIdMissing;
  else if (!(statInst->gender & 3))
	statInst->status = LoadStatus_GenMissing;
  else 
	statInst->status = LoadStatus_Ok;

  // Can do better error checking here
  if (statInst->status) {
	char *msg = (statInst->status >= 0 && statInst->status < ArrayCount(xLoadStatusNames)
				 ? xLoadStatusNames[(int)statInst->status] : "Invalid");
	PEngLoadError(PKGNAME, xlp->fname, lineNum, memSpec->memId, msg, errBuf);
	return -1;
  } else {
	return 0;
  }
}

int LoadFile(cSTR fname, void *inpX, PXml px, int verbose)
{
	return LoadFileAux(fname, inpX, px, verbose, NULL);
}

int LoadFileAux(cSTR fname, void *inpX, PXml px, int verbose, cSTR header)
{
  FILE     *inpFile = (FILE*)inpX;
  LoadPattern   xlp = NULL;
  LoadStatus  xstat = NULL;
  S4  lineCnt = 0;
  S4   errCnt = 0;
  int errCode = 0;
  Mem xmem;
  MemSpecRec memSpec;
  clock_t clockBase, clockLast;
  char buf[4096], msgBuf[128], *str;

  //Open file if not already 
  if (!inpX)
    inpFile = fopen(fname, "r");
  
  // Grab the time
  clockBase = clock();

  // Reset our loader error log
  if (gsv->loadOutput)
	{ fclose(gsv->loadOutput); gsv->loadOutput = NULL; }

  // First line must be header, establish pattern for other lines
  if (!header) {
	  fgets(buf, sizeof(buf), inpFile); 
	  buf[sizeof(buf)-1] = 0;
  } else {
	  str0ncpy(buf, header, sizeof(buf));
  }
  xlp = LoadPatternNew(fname, buf);
  if (!xlp)
	return ERR_Load_Pattern;

  // Grab the write permit to pass in to each load
  if ((errCode = PermitWriteTake("LoadFile", &(xlp->permit))))
	return ERR_Load_Permit;

  // ??? Be sure to return the permit we've taken

  // Now load them
  clockLast = clock();
  lineCnt = 1;
  while ((str = fgets(buf, sizeof(buf), inpFile))) {
	
	// Parse the csv line into a memspec
	StructNull(memSpec);
	if ((errCode = LoadPatternLoad(xlp, &memSpec, buf, lineCnt)) == 0) {
	  // Add the permit to the MemUpsert params 
	  memSpec.permit = xlp->permit;
	  // Now load it
	  xmem = MemUpsert(&memSpec);
	}

	// Make a status callback every so often
	if ((++lineCnt % 25000) == 0 && verbose) {
	  clock_t clockNow = clock();
	  snprintf(msgBuf, sizeof(msgBuf),
			   "Status fnc='LoadPattern' line='%ld' memid='%ld' time='%ld' delta='%ld'", 
			   lineCnt, memSpec.memId, clockNow-clockBase, clockNow-clockLast);
	  clockLast = clockNow;
	  if (px) PXmlPush0(px, msgBuf, 0); 
	  PEngLog(PKGNAME, msgBuf, NULL, 0);
	}
  }

  // LoadPatternStatus has accumulated the resolution of each line in the csv
  // Errors will have been exported with calls to PEngLoadError
  // For now, summarize the load run
  if ((xstat = LoadPatternStatus(xlp))) {
	S4 histo[5]; S4 i;
	StructNull(histo);
	for ( ; xstat; xstat = xstat->nextPage) {
	  for (i=0; i<xstat->numInst; i++) {
		LoadStatusInst si = &(xstat->datInst[i]);
		switch (si->status) {
		case LoadStatus_Ok:           histo[0] += 1; break;
		case LoadStatus_MemIdMissing: histo[1] += 1; break;
		case LoadStatus_DomIdMissing: histo[2] += 1; break;
		case LoadStatus_GenMissing:   histo[3] += 1; break;
		case LoadStatus_TagsTooMany:  histo[4] += 1; break;
		}
	  }
	}
	snprintf(msgBuf, sizeof(msgBuf),
			 "Status ok='%ld' nomemid='%ld' nodomid='%ld' nogen='%ld' toomanytag='%ld'",
			 histo[0], histo[1], histo[2], histo[3], histo[4]);
	if (px) PXmlPush0(px, msgBuf, 0); 
	PEngLog(PKGNAME, msgBuf, NULL, 0);
  }

//done:
  // Clean up
  if (xlp) {
	PermitWriteGive(xlp->permit);
	LoadPatternFree(xlp);
  }

  // Reset our loader error log
  if (gsv->loadOutput)
	{ fclose(gsv->loadOutput); gsv->loadOutput = NULL; }

  if (!inpX)
	fclose(inpFile);

  return 0;
}
