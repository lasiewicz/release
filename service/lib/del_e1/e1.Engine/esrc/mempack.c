// mempack.c
// pnelson 12/3/2005
// Pack member data into concise forms

#define MEM_FRIEND
#include "peng.h"

#define PKGNAME PkgName_Mem


/* Gender mask bits
 * 1	Male
 * 2	Female
 * 4	Seeking Male
 * 8	Seeking Female
 * 16	Male turned female
 * 32	Female turned male
 * 64	Seeking male turned female
 * 128	Seeking female turned male
 */
// Return 0/GenderMale/GenderFemale
//
int MemPackGender(int gm, US1 *gayp)
{
  int ima, gay; 

  if      (gm & 1)  ima = GenderMale;
  else if (gm & 2)  ima = GenderFemale;
  else if (gm & 16) ima = GenderFemale;
  else if (gm & 32) ima = GenderMale;
  else goto error;

  if      ((gm&1) && (gm&4)) gay = 1;
  else if ((gm&2) && (gm&8)) gay = 1;
  else if (gm & (16|32|64|128)) gay = 1;
  else gay = 0;

  if (gayp) *gayp = gay;
  return ima;

 error:
  if (gayp) *gayp = 0;
  return 0;
}

STR MemPackDateStr(cSTR name, US2 pack, char *buf)
{
  MemDateRec dd;
  dd.pack = pack;
  if (pack == 0) {
	sprintf(buf, "%s code='0' ymd='0000-00-00'", name);
  } else {
	sprintf(buf, "%s code='%d' ymd='%d-%0.2d-%0.2d'", name, pack,
			dd.v.year + kMemPack_YearMin, dd.v.month, dd.v.day);
  }
  return buf;
}

US2 MemPackDate(cSTR dateStr)
{
  int year, month, day;
  US2 pack;
  year = month = day = 0;
  sscanf(dateStr, "%d-%d-%d", &year, &month, &day);
  if (year > kMemPack_YearMin)
	year -= kMemPack_YearMin;
  if (year == 0 && month == 0 && day == 0)
	return 0;
  else {
	year  = minmax(year,  1, kMemPack_YearRng);
	month = minmax(month, 1, 12);
	day   = minmax(day,   1, 31);
	pack = (year << 9) | (month << 5) | day;
	return pack;
  }
}

S4 MemPackDateAsDays(US2 pack)
{
  MemDateRec dd;
  dd.pack = pack;
  return ((dd.v.year)*12 + (dd.v.month-1)) * 31 + dd.v.day;
}

int MemPackLocMilesAsBands(int miles)
{
  // Assume 24k miles around the earth
  if (miles <= 0) 
	miles = 50; // ??? put this in RankTune
  return 1+ (((S4)miles*kLoc_EWBoxes) / 24000);
}  

void MemPackLoc(Loc loc, MemLoc memLoc)
{
	float fltLng = memLoc->regLng, pctLng;
	float fltLat = memLoc->regLat, pctLat;

	fltLng = minmax(fltLng, kLoc_fLngMin, kLoc_fLngMax);
	fltLat = minmax(fltLat, kLoc_fLatMin, kLoc_fLatMax);

	pctLng = (fltLng - kLoc_fLngMin) / (kLoc_fLngMax - kLoc_fLngMin);
	if      (pctLng <= 0.0) loc->lng = 0;
	else if (pctLng >= 1.0) loc->lng = kLoc_EWBoxes - 1;
	else    loc->lng = (US2)(pctLng * (float)kLoc_EWBoxes);

	pctLat = (fltLat - kLoc_fLatMin) / (kLoc_fLatMax - kLoc_fLatMin);
	if      (pctLat <= 0.0) loc->lat = 0;
	else if (pctLat >= 1.0) loc->lat = kLoc_NSBoxes - 1;
	else    loc->lat = (US2)(pctLat * (float)kLoc_NSBoxes);
}

void MemPackVitals(MemVitals vitals, MemSpec xspec)
{
  US1 gay;

#define ph kMemPack_HeightMin
  if (xspec->height)
	vitals->height   = minmax(xspec->height, ph+1, ph+kMemPack_HeightRng) - ph;
#undef ph
#define pw kMemPack_WeightMin
  if (xspec->weight)
	vitals->weight   = minmax(xspec->weight, pw+1, pw+kMemPack_WeightRng) - pw;
#undef pw

  vitals->saPop    = minmax(xspec->saPop, 1, 15);
  vitals->saAct    = (xspec->saAct ? minmax(xspec->saAct, 1, 15)
						: RankLastLoginToSAAct(xspec->activePack, NULL));
  
  vitals->hasPhoto = (xspec->hasPhoto ? 1 : 0);
  vitals->isOnline = (xspec->isOnline ? 1 : 0);

  vitals->childCnt  = (xspec->childrenCount > 3 ? 3 : xspec->childrenCount);
  vitals->relocate  = (xspec->relocateFlag  > 2 ? 0 : xspec->relocateFlag);

  (void)MemPackGender(xspec->genderMask, &gay);
  vitals->gayMask   = gay;
}

void MemPackVitalsUpdate(MemVitals vitals, MemUpdateSpec xspec)
{
  if (vitals->saPop)
	vitals->saPop = (xspec->saPop == (US1)-1 ? 0 : minmax(xspec->saPop, 1, 15));
  if (vitals->saAct)
	vitals->saAct = (xspec->saAct == (US1)-1 ? 0 : minmax(xspec->saAct, 1, 15));
  if (vitals->hasPhoto)
	vitals->hasPhoto = (xspec->hasPhoto == (US1)-1 ? 0 : 1);
  if (vitals->isOnline)
	vitals->isOnline = (xspec->isOnline == (US1)-1 ? 0 : 1);
}

static S4 csczLookup(cSTR typeName, StxDb db, cSTR name, int bitsMax, int mod) 
{
  char msgBuf[128];
  Stx sx;

  // Lookup or create a numeric Stx for each string, Stx fnc guarantees size in range
  if (!name) 
	return 0;
  else if (!(sx = StxDbFind(db, name))) {
	sx = StxDbFindOrNew(db, name);
	if (sx >= (1 << bitsMax)) {
	  snprintf(msgBuf, sizeof(msgBuf), "TooMany %ld=%s", sx, name);
	  PEngError(PKGNAME, typeName, msgBuf);
	  sx = 0;
	} else if ((sx % mod) == 0) {
	  snprintf(msgBuf, sizeof(msgBuf), "Auto %s: %ld=%s", typeName, sx, name);
	  PEngLog(PKGNAME, msgBuf, NULL, 0);
	}
  }
  return sx;
}

void MemPackCSCZMap(MemCSCZMap map, MemLoc ml)
{
  STR zip     = ml->regZip;
  STR area    = ml->regAreaCode;

  // Lookup or create a numeric Stx for each string, Stx fnc guarantees size in range
  map->pkCountry = csczLookup("Country", gsv->pkCountry, ml->regCountry, kStx_CountryBits, 10);
  map->pkState   = csczLookup("State",   gsv->pkState,   ml->regState,   kStx_StateBits, 50);
  map->pkCity    = csczLookup("City",    gsv->pkCity,    ml->regCity,    kStx_CityBits, 1000);
  map->pkZip     = csczLookup("Zip",     gsv->pkZip,     ml->regZip,     kStx_ZipBits,  1000);
  // Store area code as integer, make sure it fits in its bit range
  if (area && area[0]) {
	S4 iarea = atol(area);
	map->pkArea = minmax(iarea, 1, 999);
  }
}


