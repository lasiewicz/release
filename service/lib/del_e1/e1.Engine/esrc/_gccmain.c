// C command line console
//

#include "peng.h"

// Only use this when compiling a command console with GCC
#ifdef GCC

static void showError(int fatal, STR e1, STR e2)
{
	fprintf(stderr, "Args: inputFile.csv\n");
	if (e1)
		fprintf(stderr, " Error: %s: %s\n", e1, (e2 ? e2 : ""));
	if (fatal)
		exit(fatal);
}

#define isw(c) ((c) == ' '  || (c) == '\t' || (c) == '\n' || (c) == '\r')

static char *trimBuf(char *buf)
{
  int len = strlen(buf);
  while (len-- > 0 && isw(buf[len]))
	buf[len] = 0;
  return buf;
}

static int doCmd(PXml px, char *command, FILE *outFile)
{
  int err;
  fprintf(outFile, "## %.120s\n", command);
  err = CmdEval(command, px, 0, 0);
  PXmlShow(px, outFile, 1);
  return err;
}

static int doCmdFile(PXml px, char *inpName, FILE *outFile, 
					 char *bufDat, int bufSize)
{
  int err = 0;
  FILE *inpFile = NULL;
  if (!(inpFile = fopen(inpName, "r"))) {
	fprintf(outFile, "## Error opening command file: %s\n", inpName, 0);
	showError(0, "Args: cmds.txt", NULL);
	err = 1;
  } else {
	fprintf(outFile, "## Processing command file: %s\n", inpName, 0);
	while (!err && fgets(bufDat, bufSize, inpFile)) {
	  trimBuf(bufDat);
	  if (bufDat[0] && bufDat[0] != '#' && bufDat[0] != '\n') {
		if (bufDat[0] == '@') {
		  err = doCmdFile(px, bufDat+1, outFile, bufDat, bufSize);
		} else {
		  err = doCmd(px, bufDat, outFile);
		}
	  }
	}
	fclose(inpFile);
  }
  fflush(outFile);
  return err;
}


int main(int argc, char **argv)
{
	char bufDat[16*1024];
	int err = 0, i;
	PXml px;
	FILE *ff = stdout;

	GsvInitialize();
	px = PXmlNew((1<<17), 0, NULL);
  
	// Arguments must be command files
	if (argc > 1) {
	  // Time stamp
	  doCmd(px, "time", ff);
	  for (i = 1; i < argc; i++)
		err = doCmdFile(px, argv[i], ff, bufDat, sizeof(bufDat));
	}
	doCmd(px, "time", ff);

	// Now loop
	while (fgets(bufDat, sizeof(bufDat), stdin)) {
		// Show the command line
	    trimBuf(bufDat);
		if (bufDat[0] == '@') {
		  err = doCmdFile(px, bufDat+1, (FILE*)ff, bufDat, sizeof(bufDat));
		} else {
		  err = doCmd(px, bufDat, ff);
		}
		if (err < 0) 
		  break;
	}
	
	PXmlFree(px);
	GsvDestroy();
	return 0;
}

#endif
