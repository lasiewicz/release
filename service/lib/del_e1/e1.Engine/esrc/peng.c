// peng.c
// pnelson 11/15/2005
// Utilities for the engine

#include <malloc.h>
#include <time.h>
#include "peng.h"

#define PKGNAME PkgName_Eng

// Define the global state holder
static GsvRec gsvRec;
Gsv gsv = &gsvRec;

static void gsvPkgNameInit()
{
  gsv->pkgNames[0] = "PkgUnknown";
  gsv->pkgNames[PkgName_Eng]   = "PkgEng";
  gsv->pkgNames[PkgName_Cmd]   = "PkgCmd";
  gsv->pkgNames[PkgName_PHash] = "PkgPHash";
  gsv->pkgNames[PkgName_Stx]   = "PkgStx";
  gsv->pkgNames[PkgName_PMap]  = "PkgPMap";
  gsv->pkgNames[PkgName_Tag]   = "PkgTag";
  gsv->pkgNames[PkgName_TagDb] = "PkgTagDb";
  gsv->pkgNames[PkgName_Grp]   = "PkgGrp";
  gsv->pkgNames[PkgName_Ldg]   = "PkgLdg";
  gsv->pkgNames[PkgName_Mem]   = "PkgMem";
  gsv->pkgNames[PkgName_PXml]  = "PkgPXml";
  gsv->pkgNames[PkgName_Mob]   = "PkgMob";
  gsv->pkgNames[PkgName_S1]    = "PkgS1Search";
  gsv->pkgNames[PkgName_PRes]  = "PkgPRes";
  gsv->pkgNames[PkgName_Order] = "PkgOrder";
  gsv->pkgNames[PkgName_Load]  = "PkgLoad";
}

void GsvTodaySet()
{
  struct tm *xtm;
  time_t timer = 0;
  timer = time(&timer);
  xtm = localtime(&timer);
  gsvRank.today.v.year  = xtm->tm_year;  // also based on 1900
  gsvRank.today.v.month = xtm->tm_mon+1;
  gsvRank.today.v.day   = xtm->tm_mday;
  gsvRank.todayDays = MemPackDateAsDays(gsvRank.today.pack);
}

void GsvInitialize()
{
  int fatal = 0;
  
  if (gsv->initialized)
	return;
  gsv->initialized = clock();
  gsv->dbgOutput   = stdout;
  
  GsvTodaySet();

  // Get our config data
  E1CfgInitialize(NULL);

  // Initialize our packages
  gsvPkgNameInit();
  MemInitialize();
  LoadInitialize();

  if (PermitInitialize(0)) {
	PEngError(PKGNAME, "Permit Initialize", "");
	fatal = 1;
  }

  // Make sure compiler is packing DOB properly
  { 
	MemDate tt = &(gsvRank.today); 
	US2 pack;

	pack = ((US2)tt->v.year << 9) | ((US2)tt->v.month << 5) | ((US2)tt->v.day);
	if (pack != tt->pack) {
	  PEngError(PKGNAME, "COMPILER ALIGNMENT", "Packing DOB");
	  fatal = 1;
	}
  }
  
  if (fatal)
	exit(fatal);
  
  PEngLog(PkgName_Eng, "\nStartup", NULL, 0);
}

STR GsvPkgNameAsStr(PkgName pkg)
{
  int ipkg = minmax((int)pkg, 0, kPkg_NameMax);
  return gsv->pkgNames[ipkg];
}

void GsvDestroy()
{
  PEngLog(PkgName_Eng, "Shutdown", NULL, 0);

  PHashFree(gsv->mems);
  ObjMgrDestroy(gsv->memObjMgr);
  
  PHashFree(gsv->ldgs);
  ObjMgrDestroy(gsv->ldgObjMgr);
  
  PHashFree(gsv->grps);
  // Free all the grps ???
  
  PHashFree(gsv->tags);
  ObjMgrDestroy(gsv->tagObjMgr);
  StxDbFree(gsv->pkTags);
  
  StxDbFree(gsv->pkCountry);
  StxDbFree(gsv->pkState);
  StxDbFree(gsv->pkCity);
  StxDbFree(gsv->pkZip);
  
  // PMapDestroy(gsv->pmap)
}

char *str0ncpy(char *dst, const char *src, int dstSize)
{
	if (!dst || dstSize <= 0) return dst;
	strncpy(dst, src, dstSize-1);
	dst[dstSize-1] = 0;
	return dst;
}

/****************/

static char *timeText(time_t timer, char *buf, int bufSize)
{
  int len;
  char *asc = ctime(&timer); // timeBuf ??? NOT THREAD SAFE
  str0ncpy(buf, asc, bufSize);
  if ((len = strlen(buf)) > 0)
	if (buf[len-1] == '\n') buf[len-1] = 0;
  return buf+4;  // Skip day of the week in time output
}

static char *timeFile(time_t timer, cSTR prefix, char *buf, int bufSize)
{
  struct tm *xtm;
  xtm = localtime(&timer); // timeBuf ??? NOT THREAD SAFE
  snprintf(buf, bufSize, "%s-%0.2d%0.2d%0.2d-%0.2d.log", 
		   prefix, xtm->tm_year-100, xtm->tm_mon+1, xtm->tm_mday, xtm->tm_hour);
  return buf;
}

/****/

void PEngError(PkgName pkg, cSTR obj, cSTR err)
{
  STR pstr = GsvPkgNameAsStr(pkg);
  if (!obj) obj = "~";
  if (!err) err = "!";
  snprintf(gsv->lastErr, sizeof(gsv->lastErr), "Error (%s) [%s]: %s", pstr, obj, err);
  fputs(gsv->lastErr, gsv->dbgOutput);
  PEngLog(pkg, gsv->lastErr, NULL, 0);
  // exit(1);
}

void PEngLog(PkgName pkg, cSTR fmt, cSTR strarg, S4 intarg)
{
  char msgBuf[2048], timeBuf[32], *timeTxt;
  int msgLen;
  FILE *out; 
  time_t timer = 0;

  static time_t lastReopen = 0;
  static time_t lastFlush = 0;

  timer = time(&timer);

  // reopen every ten minutes
  if (!gsv->logOutput || timer > (lastReopen+600)) {
	char *fileName = timeFile(timer, "_peng", msgBuf, sizeof(msgBuf));
	if (gsv->logOutput) 
	  { fclose(gsv->logOutput); gsv->logOutput = NULL; }
 	gsv->logOutput = fopen(fileName, "a");
	lastReopen = timer;
  }

  if (!fmt)	fmt = (strarg ? "%s %d" : "%d");

  if (strarg) snprintf(msgBuf, sizeof(msgBuf), fmt, strarg, intarg);
  else        snprintf(msgBuf, sizeof(msgBuf), fmt, intarg);
  if ((msgLen = strlen(msgBuf)) > 0)
	if (msgBuf[msgLen-1] == '\n') msgBuf[msgLen-1] = 0;
 
  out = (gsv->logOutput ? gsv->logOutput : stderr);

  timeTxt = timeText(timer, timeBuf, sizeof(timeBuf));
  fprintf(out, "%s %s: %s\n", timeTxt, GsvPkgNameAsStr(pkg), msgBuf);
  if (timer >= lastFlush+10)
	{ fflush(out); lastFlush = timer; }
}

void PEngLoadError(PkgName pkg, cSTR file, S4 lineNum, MemId memid, cSTR errName, cSTR data)
{
  time_t timer = 0;
  static time_t lastReopen = 0;

  timer = time(&timer);

  // reopen every ten minues
  if (!gsv->loadOutput || timer > (lastReopen+600)) {
	char fileBuf[128];
	char *fileName = timeFile(timer, "_load", fileBuf, sizeof(fileBuf));
	if (gsv->loadOutput) 
	  { fclose(gsv->loadOutput); gsv->loadOutput = NULL; }
 	gsv->loadOutput = fopen(fileName, "a");
	lastReopen = timer;
  }
  if (gsv->loadOutput) {
	cSTR fname = (file ? file : "");
	int  flen  = strlen(fname);
	if (flen > 24) fname += (flen-24);
	fprintf(gsv->loadOutput, "%s:%ld err:%s memid:%ld csv:%s\n", 
			fname, lineNum, (errName ? errName : "Invalid"), memid, data);
  }
}

/****************/

#ifdef PENGDEBUG
static PkgName debugPkg = 0;
#endif

void *PEngAlloc(PkgName pkg, US4 size)
{
	int ipkg = minmax((int)pkg, 0, kPkg_NameMax);
	void *ptr = calloc(size, 1);
#ifdef PENGDEBUG
	if (debugPkg == pkg)
		debugPkg = pkg;  // Pt to stop in debugger
#endif
	if (!ptr) {
		char msgBuf[64];
		snprintf(msgBuf, sizeof(msgBuf), "Allocating %ld bytes", size);
		PEngError(pkg, NULL, msgBuf);
	}
	gsv->pkgAllocs[ipkg] += 1;
	gsv->pkgMemory[ipkg] += size;
	return ptr;
}

void PEngFree(PkgName pkg, void *ptr, US4 size)
{
#ifdef PENGDEBUG
	if (debugPkg == pkg)
		debugPkg = pkg;  // Pt to stop in debugger
#endif
	if (size > 0) {
		int ipkg = minmax((int)pkg, 0, kPkg_NameMax);
		gsv->pkgAllocs[ipkg] -= 1;
		gsv->pkgMemory[ipkg] -= size;
	}
	if (ptr)
		free(ptr);
}

/****************/

int GsvXml(PXml px, int mode)
{
  int i, err;
  S4 tagSeq = ObjMgrSequence(gsv->tagObjMgr);
  S4 ldgSeq = ObjMgrSequence(gsv->ldgObjMgr);
  S4 grpSeq = SeqMgrSequence(gsv->grpSeqMgr);
  S4 memSeq = ObjMgrSequence(gsv->memObjMgr);
  
  if ((err = PXmlPush(px, "Gsv", 0)) < 0)
	return err;
  
  err |= PXmlPush(px, "Sequences", 0);
  {
	err |= PXmlSetNum(px, "tagSequence", tagSeq);
	err |= PXmlSetNum(px, "grpSequence", grpSeq);
	err |= PXmlSetNum(px, "ldgSequence", ldgSeq);
	err |= PXmlSetNum(px, "memSequence", memSeq);
  }
  err |= PXmlPop(px);
  
  err |= PXmlPush(px, "Stxs", 0);
  {
	err |= PXmlSetNum(px, "Stx name='tags'",    StxDbCount(gsv->pkTags));
	err |= PXmlSetNum(px, "Stx name='country'", StxDbCount(gsv->pkCountry));
	err |= PXmlSetNum(px, "Stx name='state'",   StxDbCount(gsv->pkState));
	err |= PXmlSetNum(px, "Stx name='city'",    StxDbCount(gsv->pkCity));
	err |= PXmlSetNum(px, "Stx name='zip'",     StxDbCount(gsv->pkZip));
  }
  err |= PXmlPop(px);
  
  err |= PXmlPush(px, "MemAlloced", 0);
  for (i=0; i<kPkg_NameMax; i++)
	if (gsv->pkgAllocs[i]) {
	  char msgBuf[64]; 
	  snprintf(msgBuf, sizeof(msgBuf), "Memory name='%s' num='%ld' size='%ld'", 
			   gsv->pkgNames[i], gsv->pkgAllocs[i], gsv->pkgMemory[i]);
	  err |= PXmlPush0(px, msgBuf, 0);
	}
  err |= PXmlPop(px);
  
  if ((mode & 1) && !err) {
	// Show stats on our STR<>ID tables
	err = PXmlPush(px, "StxDbStats", 0);
	if (!err) err |= StxDbXml(px, gsv->pkTags,    mode);
	if (!err) err |= StxDbXml(px, gsv->pkCountry, mode);
	if (!err) err |= StxDbXml(px, gsv->pkState,   mode);
	if (!err) err |= StxDbXml(px, gsv->pkCity,    mode);
	if (!err) err |= StxDbXml(px, gsv->pkZip,     mode);
	PXmlPop(px);
  }
  
  if ((mode & 2) && !err) {
	// Show stats on our Hash tables
	err = PXmlPush(px, "PHashStats", 0);
	if (!err) err = PHashXml(px, gsv->tags, tagSeq, 0);
	if (!err) err = PHashXml(px, gsv->grps, grpSeq, 0);
	if (!err) err = PHashXml(px, gsv->ldgs, ldgSeq, 0);
	if (!err) err = PHashXml(px, gsv->mems, memSeq, 0);
	if (!err) err = GrpPkgXml(px, mode);
	PXmlPop(px);
  }
  
  if ((mode & 4) && !err) {
	// Show stats on our Object Managers
	err = PXmlPush(px, "ObjManagers", 0);
	if (!err) err |= ObjMgrXml(px, gsv->tagObjMgr, 0);
	if (!err) err |= SeqMgrXml(px, gsv->grpSeqMgr, 0);
	if (!err) err |= ObjMgrXml(px, gsv->ldgObjMgr, 0);
	if (!err) err |= ObjMgrXml(px, gsv->memObjMgr, 0);
	PXmlPop(px);
  }
  
  err |= PXmlPop(px);
  return err;
}
