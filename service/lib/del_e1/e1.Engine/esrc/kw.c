/* C code produced by gperf version 2.7.2 */
/* Command-line: gperf -a -C -G -t -T -N KWLookupAux -r -p --switch=1 -k '*' -D  */
// kw.src & kw.c == use gperf to quickly match E1 keywords
// Build kw.c from kw.src with the following:
//    cat kw.src | tr -d '\015' | gperf -a -C -G -t -T -N KWLookupAux -r -p --switch=1 -k '*' -D > kw.c
// Use _kwunit.c to test 

#include "string.h"
#ifndef strlwr
#define strlwr   _strlwr
#endif
#include "kw.h"


#define TOTAL_KEYWORDS 152
#define MIN_WORD_LENGTH 1
#define MAX_WORD_LENGTH 23
#define MIN_HASH_VALUE 176
#define MAX_HASH_VALUE 3115
/* maximum key range = 2940, duplicates = 10 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (str, len)
     register const char *str;
     register unsigned int len;
{
  static const unsigned short asso_values[] =
    {
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,  193,
       241,  134,  188, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116,  120,   77,  183,
       151,  196,  140,  116,   55,  120,  106,   84,   55,  132,
       251,   75,   89,   87,  172,   15,    8,  209,  175,   86,
       102,  140,  130, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116, 3116,
      3116, 3116, 3116, 3116, 3116, 3116
    };
  register int hval = len;

  switch (hval)
    {
      default:
      case 23:
        hval += asso_values[(unsigned char)str[22]];
      case 22:
        hval += asso_values[(unsigned char)str[21]];
      case 21:
        hval += asso_values[(unsigned char)str[20]];
      case 20:
        hval += asso_values[(unsigned char)str[19]];
      case 19:
        hval += asso_values[(unsigned char)str[18]];
      case 18:
        hval += asso_values[(unsigned char)str[17]];
      case 17:
        hval += asso_values[(unsigned char)str[16]];
      case 16:
        hval += asso_values[(unsigned char)str[15]];
      case 15:
        hval += asso_values[(unsigned char)str[14]];
      case 14:
        hval += asso_values[(unsigned char)str[13]];
      case 13:
        hval += asso_values[(unsigned char)str[12]];
      case 12:
        hval += asso_values[(unsigned char)str[11]];
      case 11:
        hval += asso_values[(unsigned char)str[10]];
      case 10:
        hval += asso_values[(unsigned char)str[9]];
      case 9:
        hval += asso_values[(unsigned char)str[8]];
      case 8:
        hval += asso_values[(unsigned char)str[7]];
      case 7:
        hval += asso_values[(unsigned char)str[6]];
      case 6:
        hval += asso_values[(unsigned char)str[5]];
      case 5:
        hval += asso_values[(unsigned char)str[4]];
      case 4:
        hval += asso_values[(unsigned char)str[3]];
      case 3:
        hval += asso_values[(unsigned char)str[2]];
      case 2:
        hval += asso_values[(unsigned char)str[1]];
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval;
}

static const E1KwRec wordlist[] =
  {
    {"v",                              KW_value},
    {"hgt",                            KW_hgt},
    {"c",                              KW_code},
    {"wgt",                            KW_wgt},
    {"show",                           KW_show},
    {"tag",                            KW_tag},
    {"n",                              KW_name},
    {"blat",                           KW_boxlat},
    {"seq",                            KW_seq},
    {"gsv",                            KW_gsv},
    {"pets",                           KW_pets},
    {"ldg",                            KW_ldg},
    {"flat",                           KW_fltlat},
    {"zip",                            KW_zip},
    {"state",                          KW_state},
    {"grp",                            KW_grp},
    {"fltlat",                         KW_fltlat},
    {"sapop",                          KW_sapop},
    {"help",                           KW_help},
    {"load",                           KW_load},
    {"quit",                           KW_quit},
    {"age",                            KW_age},
    {"type",                           KW_type},
    {"boxlat",                         KW_boxlat},
    {"saact",                          KW_saact},
    {"city",                           KW_city},
    {"time",                           KW_time},
    {"mem",                            KW_mem},
    {"cmd",                            KW_cmd},
    {"tagdb",                          KW_tagdb},
    {"hasphoto",                       KW_hasphoto},
    {"blng",                           KW_boxlng},
    {"file",                           KW_file},
    {"hgtmax",                         KW_hgtmax},
    {"maxhgt",                         KW_hgtmax},
    {"height",                         KW_hgt},
    {"mode",                           KW_mode},
    {"flng",                           KW_fltlng},
    {"wgtmax",                         KW_wgtmax},
    {"maxwgt",                         KW_wgtmax},
    {"null",                           KW_null},
    {"weight",                         KW_wgt},
    {"code",                           KW_code},
    {"fltlng",                         KW_fltlng},
    {"domid",                          KW_domid},
    {"music",                          KW_music},
    {"reglat",                         KW_fltlat},
    {"boxlng",                         KW_boxlng},
    {"hgtmin",                         KW_hgtmin},
    {"minhgt",                         KW_hgtmin},
    {"sortkey",                        KW_sortkey},
    {"name",                           KW_name},
    {"wgtmin",                         KW_wgtmin},
    {"minwgt",                         KW_wgtmin},
    {"memid",                          KW_memid},
    {"planets",                        KW_planets},
    {"resmax",                         KW_resmax},
    {"noauto",                         KW_noauto},
    {"search",                         KW_search},
    {"bodyart",                        KW_bodyart},
    {"value",                          KW_value},
    {"update",                         KW_update},
    {"relmask",                        KW_relationshipmask},
    {"zodiac",                         KW_zodiac},
    {"custody",                        KW_custody},
    {"agemax",                         KW_agemax},
    {"maxage",                         KW_agemax},
    {"relstatus",                      KW_relationshipstatus},
    {"delete",                         KW_delete},
    {"latitude",                       KW_fltlat},
    {"bodytype",                       KW_bodytype},
    {"coding",                         KW_coding},
    {"reglng",                         KW_fltlng},
    {"heightmax",                      KW_hgtmax},
    {"maxheight",                      KW_hgtmax},
    {"birthdate",                      KW_birthdate},
    {"hasphotoflag",                   KW_hasphoto},
    {"agemin",                         KW_agemin},
    {"minage",                         KW_agemin},
    {"weightmax",                      KW_wgtmax},
    {"maxweight",                      KW_wgtmax},
    {"habitattype",                    KW_habitattype},
    {"relocate",                       KW_relocate},
    {"childcnt",                       KW_childcnt},
    {"haircolor",                      KW_haircolor},
    {"country",                        KW_country},
    {"majortype",                      KW_majortype},
    {"distance",                       KW_distance},
    {"heightmin",                      KW_hgtmin},
    {"minheight",                      KW_hgtmin},
    {"gender",                         KW_gender},
    {"ethnicity",                      KW_ethnicity},
    {"isonline",                       KW_isonline},
    {"weightmin",                      KW_wgtmin},
    {"minweight",                      KW_wgtmin},
    {"eyecolor",                       KW_eyecolor},
    {"cuisine",                        KW_cuisine},
    {"religion",                       KW_religion},
    {"maritalstatus",                  KW_maritalstatus},
    {"domainid",                       KW_domid},
    {"reading",                        KW_reading},
    {"keepkosher",                     KW_keepkosher},
    {"memberid",                       KW_memid},
    {"longitude",                      KW_fltlng},
    {"smokinghabits",                  KW_smokinghabits},
    {"areacode",                       KW_areacode},
    {"updatedate",                     KW_updatedate},
    {"activedate",                     KW_activedate},
    {"emailcount",                     KW_emailcount},
    {"gendermask",                     KW_gendermask},
    {"relocateflag",                   KW_relocate},
    {"registerdate",                   KW_registerdate},
    {"lastactivedate",                 KW_activedate},
    {"industrytype",                   KW_industrytype},
    {"communityid",                    KW_domid},
    {"languagemask",                   KW_languagemask},
    {"relationshipmask",               KW_relationshipmask},
    {"incomelevel",                    KW_incomelevel},
    {"physicalactivity",               KW_physicalactivity},
    {"relationshipstatus",             KW_relationshipstatus},
    {"drinkinghabits",                 KW_drinkinghabits},
    {"jdateethnicity",                 KW_jdateethnicity},
    {"personalitytrait",               KW_personalitytrait},
    {"jdatereligion",                  KW_jdatereligion},
    {"havechildren",                   KW_havechildren},
    {"depth3regionid",                 KW_city},
    {"leisureactivity",                KW_leisureactivity},
    {"depth4regionid",                 KW_zip},
    {"israelareacode",                 KW_israelareacode},
    {"depth1regionid",                 KW_country},
    {"childrencount",                  KW_childcnt},
    {"depth2regionid",                 KW_state},
    {"moviegenremask",                 KW_moviegenremask},
    {"educationlevel",                 KW_educationlevel},
    {"favoritetimeofday",              KW_favoritetimeofday},
    {"desiredmaritalstatus",           KW_desiredmaritalstatus},
    {"morechildrenflag",               KW_morechildrenflag},
    {"desiredsmokinghabits",           KW_desiredsmokinghabits},
    {"politicalorientation",           KW_politicalorientation},
    {"militaryservicetype",            KW_militaryservicetype},
    {"israelipersonalitytrait",        KW_israelipersonalitytrait},
    {"communityinsertdate",            KW_registerdate},
    {"israelimmigrationdate",          KW_israelimmigrationdate},
    {"religionactivitylevel",          KW_religionactivitylevel},
    {"desireddrinkinghabits",          KW_desireddrinkinghabits},
    {"isrealimmigrationyear",          KW_isrealimmigrationyear},
    {"desiredjdatereligion",           KW_desiredjdatereligion},
    {"synagogueattendance",            KW_synagogueattendance},
    {"entertainmentlocation",          KW_entertainmentlocation},
    {"appearanceimportance",           KW_appearanceimportance},
    {"desirededucationlevel",          KW_desirededucationlevel},
    {"intelligenceimportance",         KW_intelligenceimportance}
  };

#ifdef __GNUC__
__inline
#endif
const E1KwRec *
KWLookupAux (str, len)
     register const char *str;
     register unsigned int len;
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= MIN_HASH_VALUE)
        {
          register const E1KwRec *wordptr;
          register const E1KwRec *wordendptr;
          register const E1KwRec *resword;

          switch (key - 176)
            {
              case 0:
                resword = &wordlist[0];
                goto compare;
              case 6:
                resword = &wordlist[1];
                goto compare;
              case 8:
                resword = &wordlist[2];
                goto compare;
              case 37:
                resword = &wordlist[3];
                goto compare;
              case 59:
                resword = &wordlist[4];
                goto compare;
              case 71:
                resword = &wordlist[5];
                goto compare;
              case 76:
                resword = &wordlist[6];
                goto compare;
              case 88:
                resword = &wordlist[7];
                goto compare;
              case 125:
                resword = &wordlist[8];
                goto compare;
              case 133:
                resword = &wordlist[9];
                goto compare;
              case 136:
                resword = &wordlist[10];
                goto compare;
              case 149:
                resword = &wordlist[11];
                goto compare;
              case 151:
                resword = &wordlist[12];
                goto compare;
              case 166:
                resword = &wordlist[13];
                goto compare;
              case 176:
                resword = &wordlist[14];
                goto compare;
              case 204:
                resword = &wordlist[15];
                goto compare;
              case 216:
                resword = &wordlist[16];
                goto compare;
              case 217:
                resword = &wordlist[17];
                goto compare;
              case 223:
                resword = &wordlist[18];
                goto compare;
              case 229:
                resword = &wordlist[19];
                goto compare;
              case 252:
                resword = &wordlist[20];
                goto compare;
              case 259:
                resword = &wordlist[21];
                goto compare;
              case 261:
                resword = &wordlist[22];
                goto compare;
              case 267:
                resword = &wordlist[23];
                goto compare;
              case 275:
                resword = &wordlist[24];
                goto compare;
              case 279:
                resword = &wordlist[25];
                goto compare;
              case 284:
                resword = &wordlist[26];
                goto compare;
              case 287:
                resword = &wordlist[27];
                goto compare;
              case 293:
                resword = &wordlist[28];
                goto compare;
              case 301:
                resword = &wordlist[29];
                goto compare;
              case 324:
                resword = &wordlist[30];
                goto compare;
              case 327:
                resword = &wordlist[31];
                goto compare;
              case 339:
                resword = &wordlist[32];
                goto compare;
              case 363:
                wordptr = &wordlist[33];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 380:
                resword = &wordlist[35];
                goto compare;
              case 382:
                resword = &wordlist[36];
                goto compare;
              case 390:
                resword = &wordlist[37];
                goto compare;
              case 394:
                wordptr = &wordlist[38];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 398:
                resword = &wordlist[40];
                goto compare;
              case 411:
                resword = &wordlist[41];
                goto compare;
              case 433:
                resword = &wordlist[42];
                goto compare;
              case 455:
                resword = &wordlist[43];
                goto compare;
              case 458:
                resword = &wordlist[44];
                goto compare;
              case 488:
                resword = &wordlist[45];
                goto compare;
              case 497:
                resword = &wordlist[46];
                goto compare;
              case 506:
                resword = &wordlist[47];
                goto compare;
              case 512:
                wordptr = &wordlist[48];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 521:
                resword = &wordlist[50];
                goto compare;
              case 527:
                resword = &wordlist[51];
                goto compare;
              case 543:
                wordptr = &wordlist[52];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 560:
                resword = &wordlist[54];
                goto compare;
              case 565:
                resword = &wordlist[55];
                goto compare;
              case 567:
                resword = &wordlist[56];
                goto compare;
              case 568:
                resword = &wordlist[57];
                goto compare;
              case 571:
                resword = &wordlist[58];
                goto compare;
              case 574:
                resword = &wordlist[59];
                goto compare;
              case 584:
                resword = &wordlist[60];
                goto compare;
              case 603:
                resword = &wordlist[61];
                goto compare;
              case 605:
                resword = &wordlist[62];
                goto compare;
              case 609:
                resword = &wordlist[63];
                goto compare;
              case 612:
                resword = &wordlist[64];
                goto compare;
              case 616:
                wordptr = &wordlist[65];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 631:
                resword = &wordlist[67];
                goto compare;
              case 632:
                resword = &wordlist[68];
                goto compare;
              case 699:
                resword = &wordlist[69];
                goto compare;
              case 708:
                resword = &wordlist[70];
                goto compare;
              case 726:
                resword = &wordlist[71];
                goto compare;
              case 736:
                resword = &wordlist[72];
                goto compare;
              case 737:
                wordptr = &wordlist[73];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 740:
                resword = &wordlist[75];
                goto compare;
              case 759:
                resword = &wordlist[76];
                goto compare;
              case 765:
                wordptr = &wordlist[77];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 768:
                wordptr = &wordlist[79];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 776:
                resword = &wordlist[81];
                goto compare;
              case 837:
                resword = &wordlist[82];
                goto compare;
              case 838:
                resword = &wordlist[83];
                goto compare;
              case 860:
                resword = &wordlist[84];
                goto compare;
              case 869:
                resword = &wordlist[85];
                goto compare;
              case 871:
                resword = &wordlist[86];
                goto compare;
              case 876:
                resword = &wordlist[87];
                goto compare;
              case 886:
                wordptr = &wordlist[88];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 912:
                resword = &wordlist[90];
                goto compare;
              case 914:
                resword = &wordlist[91];
                goto compare;
              case 915:
                resword = &wordlist[92];
                goto compare;
              case 917:
                wordptr = &wordlist[93];
                wordendptr = wordptr + 2;
                goto multicompare;
              case 924:
                resword = &wordlist[95];
                goto compare;
              case 925:
                resword = &wordlist[96];
                goto compare;
              case 937:
                resword = &wordlist[97];
                goto compare;
              case 939:
                resword = &wordlist[98];
                goto compare;
              case 952:
                resword = &wordlist[99];
                goto compare;
              case 957:
                resword = &wordlist[100];
                goto compare;
              case 996:
                resword = &wordlist[101];
                goto compare;
              case 1008:
                resword = &wordlist[102];
                goto compare;
              case 1014:
                resword = &wordlist[103];
                goto compare;
              case 1025:
                resword = &wordlist[104];
                goto compare;
              case 1045:
                resword = &wordlist[105];
                goto compare;
              case 1082:
                resword = &wordlist[106];
                goto compare;
              case 1111:
                resword = &wordlist[107];
                goto compare;
              case 1183:
                resword = &wordlist[108];
                goto compare;
              case 1267:
                resword = &wordlist[109];
                goto compare;
              case 1272:
                resword = &wordlist[110];
                goto compare;
              case 1306:
                resword = &wordlist[111];
                goto compare;
              case 1313:
                resword = &wordlist[112];
                goto compare;
              case 1335:
                resword = &wordlist[113];
                goto compare;
              case 1356:
                resword = &wordlist[114];
                goto compare;
              case 1370:
                resword = &wordlist[115];
                goto compare;
              case 1467:
                resword = &wordlist[116];
                goto compare;
              case 1469:
                resword = &wordlist[117];
                goto compare;
              case 1491:
                resword = &wordlist[118];
                goto compare;
              case 1493:
                resword = &wordlist[119];
                goto compare;
              case 1498:
                resword = &wordlist[120];
                goto compare;
              case 1500:
                resword = &wordlist[121];
                goto compare;
              case 1509:
                resword = &wordlist[122];
                goto compare;
              case 1523:
                resword = &wordlist[123];
                goto compare;
              case 1565:
                resword = &wordlist[124];
                goto compare;
              case 1672:
                resword = &wordlist[125];
                goto compare;
              case 1676:
                resword = &wordlist[126];
                goto compare;
              case 1726:
                resword = &wordlist[127];
                goto compare;
              case 1729:
                resword = &wordlist[128];
                goto compare;
              case 1731:
                resword = &wordlist[129];
                goto compare;
              case 1746:
                resword = &wordlist[130];
                goto compare;
              case 1779:
                resword = &wordlist[131];
                goto compare;
              case 1818:
                resword = &wordlist[132];
                goto compare;
              case 1828:
                resword = &wordlist[133];
                goto compare;
              case 1929:
                resword = &wordlist[134];
                goto compare;
              case 1947:
                resword = &wordlist[135];
                goto compare;
              case 2029:
                resword = &wordlist[136];
                goto compare;
              case 2033:
                resword = &wordlist[137];
                goto compare;
              case 2065:
                resword = &wordlist[138];
                goto compare;
              case 2200:
                resword = &wordlist[139];
                goto compare;
              case 2314:
                resword = &wordlist[140];
                goto compare;
              case 2330:
                resword = &wordlist[141];
                goto compare;
              case 2364:
                resword = &wordlist[142];
                goto compare;
              case 2501:
                resword = &wordlist[143];
                goto compare;
              case 2506:
                resword = &wordlist[144];
                goto compare;
              case 2517:
                resword = &wordlist[145];
                goto compare;
              case 2531:
                resword = &wordlist[146];
                goto compare;
              case 2565:
                resword = &wordlist[147];
                goto compare;
              case 2641:
                resword = &wordlist[148];
                goto compare;
              case 2726:
                resword = &wordlist[149];
                goto compare;
              case 2836:
                resword = &wordlist[150];
                goto compare;
              case 2939:
                resword = &wordlist[151];
                goto compare;
            }
          return 0;
        multicompare:
          while (wordptr < wordendptr)
            {
              register const char *s = wordptr->name;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return wordptr;
              wordptr++;
            }
          return 0;
        compare:
          {
            register const char *s = resword->name;

            if (*str == *s && !strcmp (str + 1, s + 1))
              return resword;
          }
        }
    }
  return 0;
}
const char *KWLookupStr(KWEnum key)
{
	int i, cnt = sizeof(wordlist)/sizeof(wordlist[0]);
	for (i=0; i < cnt; i++)
		if (wordlist[i].kw == key)
			return wordlist[i].name;
	return NULL;
}

#define kKWBufSiz 128

KWEnum KWLookupKey(const char *str)
{
    const E1KwRec * kw;
	char buf[kKWBufSiz+2]; 
	int len;

	if (!str || !*str)
		return KW_null;

	len = strlen(str);
	if (len >= kKWBufSiz)
	  len = kKWBufSiz;
	memcpy(buf, str, len);
	buf[len] = 0;
	strlwr(buf);

	kw = KWLookupAux(buf, len);
	if (kw)
	  return kw->kw;
	else
	  return KW_null;
}
