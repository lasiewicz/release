// pres.c
// pnelson 12/12/2005
// Core structure to manage collecting results

#include "peng.h"

#define PKGNAME PkgName_PRes

#define kPRes_NodesMax  64
#define kPRes_LinesMax  32
#define kPRes_LineSize 256

typedef enum {
  PResType_Unknown = 0,
  PResType_Node = 1,
  PResType_Line = 2,
  PResType_Dead = 3
} PResType;

// The holder of a specific member and 2byte score
typedef struct _PResMem {
  MemId memId;
  US2   score;
  US2   xpad;    // Padding for alignment in array
} PResMemRec, *PResMem;

// A line of members at a leaf level in the tree
typedef struct _PResLine {
  PResMemRec mems[kPRes_LineSize];
} PResLineRec, *PResLine;

// The head of branch -- either a node, line or dead
typedef struct _PResHead {
  S1 type;
  S1 id;
  S2 cnt;
} PResHeadRec, *PResHead;

// A node in the tree with a lowbit/highbit branch
typedef struct _PResNode {
  PResHeadRec h0;
  PResHeadRec h1;
} PResNodeRec, *PResNode;

// The actual results tree
typedef struct _PRes {
  S4  target;       // How many we looking to keep
  US2 minPossible;  // What is lowest score we're still accepting
  US1 nodeFree;     // Id of next free node (-1 means done)
  US1 lineFree;     // Id of next free line (-1 means done)
  PResNodeRec nodes[kPRes_NodesMax];  // The nodes, id is index in this array
  PResLineRec lines[kPRes_LinesMax];  // The lines, id is index in this array
} PResRec;

/*****************************/
/** Internal Object manager **/
/*****************************/

#define ROOTNODE 0
#define DEADID 0xFF

// Pop a line off the free list
// returns -1 if none left
static int lineNew(PRes xres)
{
  int lix;
  if ((lix = xres->lineFree) < 0)
	return DEADID;
  else {
	xres->lineFree = (US1)xres->lines[lix].mems[0].score;
	return lix;
  }
}  

// Put a line back on the free list
static void lineFree(PRes xres, int lix)
{
  xres->lines[lix].mems[0].score = xres->lineFree;
  xres->lineFree = lix;
}

// Free the line pointed at by this head, and mark it dead
static void headFreeLine(PRes xres, PResHead head)
{
  lineFree(xres, head->id);
  head->type = PResType_Dead;
  head->cnt  = 0;
  head->id   = DEADID;
}

// Pop a node off the free list, and give each branch a line
// return DEADID if none left
static int nodeNew(PRes xres)
{
  int nix; PResNode node;

  if ((nix = xres->nodeFree) < 0)
	goto abort;

  node = &(xres->nodes[nix]);
  xres->nodeFree = node->h0.id;

  node->h0.type = PResType_Line;
  if ((node->h0.id = lineNew(xres)) < 0)
	goto abort;
  node->h0.cnt  = 0;

  node->h1.type = PResType_Line;
  if ((node->h1.id = lineNew(xres)) < 0)
	goto abort;
  node->h1.cnt  = 0;

  return nix;
 abort:
  return DEADID;
}

// Put a node back on the free list, assume embedded objects already free
static void nodeFree(PRes xres, int nix)
{
  PResNode node = &(xres->nodes[nix]);
  node->h0.id = xres->nodeFree;
  xres->nodeFree = nix;
}

/*****************/
/** Constructor **/
/*****************/

PRes PResNew(int target)
{
  PRes xres = PEngAlloc(PKGNAME, sizeof(PResRec));
  int rix, i;

  xres->target      = target;
  xres->minPossible = 0;

  // Build our free lists in reverse order so zero comes out first
  xres->lineFree = DEADID;
  i = kPRes_LinesMax;
  while (i-- > 0) lineFree(xres, i);

  xres->nodeFree = DEADID;
  i = kPRes_NodesMax; 
  while (i-- > 0) nodeFree(xres, i);

  // zero will be first off the free list for the root
  rix = nodeNew(xres);
  if (rix != ROOTNODE)
	PEngError(PKGNAME, "PResFreeList", NULL);

  return xres;
}

void PResFree(PRes xres)
{
  if (xres)
	PEngFree(PKGNAME, xres, sizeof(PResRec));
}

/**************/
/** Inserter **/
/**************/

static US2 rbCnt(PRes xres, PResHead head, int depth, US2 minScore)
{
  US2 mask = (1 << (16-depth));
  S4 total;

  switch (head->type) {
  case PResType_Dead: 
	minScore += mask;
	if (xres->minPossible < minScore)
	  xres->minPossible = minScore;
	return 0;
  case PResType_Line:
	return head->cnt;
  case PResType_Node: {
	int nix = head->id;
	PResNode node = &(xres->nodes[nix]);
	node->h0.cnt = rbCnt(xres, &(node->h0), depth+1, minScore);
	node->h1.cnt = rbCnt(xres, &(node->h1), depth+1, minScore+mask/2);
	total = node->h0.cnt + node->h1.cnt;
	return (total < 0x7FFF ? (US2)total : 0x7FFF);
  }
  default:
	return 0;
  }
}

static void lineToNode(PRes xres, PResNode node, PResHead head, US2 mask)
{
  int i, lptr;
  PResLine lineX = &(xres->lines[head->id]);
  PResLine line0 = &(xres->lines[node->h0.id]);
  PResLine line1 = &(xres->lines[node->h1.id]);
  for (i = 0; i < head->cnt; i++) {
	if (mask & lineX->mems[i].score) {
	  lptr = node->h1.cnt++;
	  line1->mems[lptr] = lineX->mems[i];
	} else {
	  lptr = node->h0.cnt++;
	  line0->mems[lptr] = lineX->mems[i];
	}
  }
}

static void rbPrune(PRes xres, PResHead head, int target, int depth)
{
  US2 mask = (1 << (16-depth));

  if (target < 0) {
	// If this is dead, prune the tree at the smallest depth
	if (mask > xres->minPossible)
	  xres->minPossible = mask;
  }

  // If it's a node, prune each branch discounting for high side cnt
  switch (head->type) {

  case PResType_Dead:
	break;

  case PResType_Node: {
	PResNode node = &(xres->nodes[head->id]);
	rbPrune(xres, &(node->h0), target - node->h1.cnt, depth+1);
	rbPrune(xres, &(node->h1), target, depth+1);
	if (target < 0) {
	  // Kill this node and replace with "Dead"
	  if (node->h0.type != PResType_Dead)
		PEngError(PKGNAME, "Prune-h0", NULL);
	  if (node->h1.type != PResType_Dead)
		PEngError(PKGNAME, "Prune-h1", NULL);
	  nodeFree(xres, head->id);
	  head->type = PResType_Dead;
	  head->id   = DEADID;
	  head->cnt  = 0;
	} else if (node->h0.type == PResType_Dead &&
			   node->h1.type == PResType_Line) {
	  // If my low end is dead, and my high end is a line, change myself to that line
	  int nix = head->id;
	  *head = node->h1;
	  nodeFree(xres, nix);
	}
	break;
  }

  // If it's a line, kill it, split it, leave it
  case PResType_Line:

	if (target <= 0) { 
	  // Kill it
	  headFreeLine(xres, head); 

	} else if (depth < 16 && head->cnt > kPRes_LineSize/2) {

	  // Split it
	  int       nix  = nodeNew(xres);
	  PResNode node  = &(xres->nodes[nix]);
	  lineToNode(xres, node, head, mask/2);
	  lineFree(xres, head->id);
	  head->type = PResType_Node;
	  head->id   = nix;
	  if (node->h1.cnt >= target) {
		headFreeLine(xres, &(node->h0));
		head->cnt  = node->h1.cnt;
	  } else {
		head->cnt  = node->h1.cnt + node->h0.cnt;
	  }
	}
  }
}

// Rebalance a tree by splitting long lines, pruning low scorers
//
static void rebalance(PRes xres)
{
  PResNode node = &(xres->nodes[ROOTNODE]);

  // Count the halves of the tree
  rbCnt(xres, &(node->h0), 1, 0);
  rbCnt(xres, &(node->h1), 1, (1L<<15));

  // Prune what we don't need
  rbPrune(xres, &(node->h0), xres->target - node->h1.cnt, 1);
  rbPrune(xres, &(node->h1), xres->target, 1);
}

int PResInsert(PRes xres, MemId memid, US2 score)
{
  PResNode node;
  PResHead head;
  PResMem  xmem;

  int rix   = ROOTNODE;  // At the root node
  int depth = 1;         // depth is one based
  US2 mask;

  if (score < xres->minPossible)
	goto dropit;

  while (1) {

	// Grab proper branch of current node
	mask = (1 << (16-depth));
	node = &(xres->nodes[rix]);
	if (score & mask)
	  head = &(node->h1);
	else
	  head = &(node->h0);
	
	switch (head->type) {
	case PResType_Dead:
	  goto dropit;

	case PResType_Node:
	  // If already a node, go down the tree
	  rix = head->id;
	  depth += 1;
	  break;  // Continue looping at next level

	case PResType_Line:
	  // Room in this line to hold it
	  if (head->cnt < kPRes_LineSize) {
		int lptr = head->cnt++;
		xmem = &(xres->lines[head->id].mems[lptr]);
		xmem->memId = memid;
		xmem->score = score;
		return 0;
	  }

	  // At the bottom line, and its full, so reject this guy
	  if (depth == 16)
		goto dropit;

	  // This line is full, time to rebalance and try again
	  rebalance(xres);
	  rix = ROOTNODE; 
	  depth = 1;
	  break; // Continue looping back from the top
	}
  }
 dropit:
  return 1;
}

/************/
/** Loader **/
/************/

// qsort callback to sort members in a line
// Favor higher score, higher memid
static int xresSortCmp(const PResMem m1, const PResMem m2)
{
  if (m1->score == m2->score) {
	return ((m1->memId > m2->memId) ? -1 :
			(m1->memId < m2->memId) ? 1 : 0);
  } else {
	return (m1->score > m2->score ? -1 : 1);
  }
}

// Recursive function to load from best to worst
// Sort each line as needed, loading just the best
static void loadHead(PRes xres, S1Result sres, PResHead head) 
{
  // We're done
  if (sres->memNum >= xres->target)
	return;

  // If it's a node, load high side, then low if needed
  if (head->type == PResType_Node) {
	PResNode node = &(xres->nodes[head->id]);
	loadHead(xres, sres, &(node->h1));
	loadHead(xres, sres, &(node->h0));
	return ;
  }

  // If it's a line, sort it and load from the best
  if (head->type == PResType_Line) {
	PResLine line = &(xres->lines[head->id]);
	S4 i, loadNum = xres->target - sres->memNum;
	if (head->cnt >= 2)
	  qsort((void*)line->mems, head->cnt, sizeof(PResMemRec), (QSortCmpFnc)xresSortCmp);
	if (loadNum > head->cnt) 
	  loadNum = head->cnt;
	for (i=0; i < loadNum; i++) {
	  int sptr = sres->memNum++;
	  sres->memids[sptr] = line->mems[i].memId;
	  sres->scores[sptr] = line->mems[i].score;
	}
  }
}
  
int PResLoad(PRes xres, S1Result sres)
{
  PResNode node = &(xres->nodes[ROOTNODE]);
  loadHead(xres, sres, &(node->h1));
  loadHead(xres, sres, &(node->h0));
  return sres->memNum;
}


/****************/

static int presXmlHead(PXml px, char *msgBuf, PRes xres, PResHead head, int hilo, int depth)
{
  int err; PResNode node;
  switch (head->type) {
  case PResType_Node:
	node = &(xres->nodes[head->id]);
	sprintf(msgBuf, "Node depth='%d' hilo='%d' id='%d' cnt='%d'", depth, hilo, head->id, head->cnt);
	if ((err = PXmlPush(px, msgBuf, 0)) < 0)
	  return err;
	err |= presXmlHead(px, msgBuf, xres, &(node->h0), 0, depth+1);
	err |= presXmlHead(px, msgBuf, xres, &(node->h1), 1, depth+1);
	err |= PXmlPop(px);
	return err;
  case PResType_Line:
	sprintf(msgBuf, "Line depth='%d' hilo='%d' id='%d' cnt='%d'", depth, hilo, head->id, head->cnt);
	return PXmlPush0(px, msgBuf, 0);
  case PResType_Dead:
	sprintf(msgBuf, "Dead depth='%d' hilo='%d'", depth, hilo);
	return PXmlPush0(px, msgBuf, 0);
  default:
	sprintf(msgBuf, "Error type='%d' depth='%d' hilo='%d'", head->type, depth, hilo);
	return PXmlPush0(px, msgBuf, 0);
  }
}


int PResXml(PXml px, PRes xres, int mode)
{
  int err, nodeNum, lineNum;
  US1 nodePtr, linePtr;
  char msgBuf[512]; 
  PResNode node;
  
  if (!xres) 
	return -1;

  // Update counts
  node = &(xres->nodes[ROOTNODE]);
  rbCnt(xres, &(node->h0), 1, 0);
  rbCnt(xres, &(node->h1), 1, (1L<<15));

  // Count Lines, nodes in use is max - num on freelist
  nodePtr = xres->nodeFree;
  for (nodeNum = 0; nodePtr != DEADID; nodeNum++)
	nodePtr = xres->nodes[nodePtr].h0.id;
  nodeNum = ArrayCount(xres->nodes) - nodeNum;

  linePtr = xres->lineFree;
  for (lineNum = 0; linePtr != DEADID; lineNum++)
	linePtr = (US1)xres->lines[linePtr].mems[0].score;
  lineNum = ArrayCount(xres->lines) - lineNum;

  snprintf(msgBuf, sizeof(msgBuf),
		   "PRes target='%d' floor='%d' nodes='%d' lines='%d'",
		   xres->target, xres->minPossible, nodeNum, lineNum);
  if ((err = PXmlPush(px, msgBuf, 0)) < 0)
	return err;
  err |= presXmlHead(px, msgBuf, xres, &(node->h0), 0, 1);
  err |= presXmlHead(px, msgBuf, xres, &(node->h1), 1, 1);
  err |= PXmlPop(px);
  return err;
}
