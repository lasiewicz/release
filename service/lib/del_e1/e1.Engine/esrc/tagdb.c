// tagdb.c
// pnelson 11/15/2005
// Manage multiple values for a tag

#define TAG_FRIEND
#include "peng.h"

#define PKGNAME PkgName_TagDb

#define kTagDb_IdShift   100000  // TagDbId == this + index in all->dataTag
#define kTagDb_MaxTags   128
#define kTagDb_InitInsts  8

/****************************************************************/

// A specific value and corresponding code (e.g. Language:English)
//
typedef struct _TagDbInst {
  Tag    tag;    // Tag structure of this name:value pair
  US4    code;   // Numeric code equivalent for this value 
} TagDbInstRec, *TagDbInst;

// A specific tag, with references to all it's values (e.g. Language: )
// If code is a mask, alloc a value per bit
// Otherwise, keep a lits of values
//
typedef struct _TagDbTag {
  Stx         nid;    // Id of name of this task
  TagDbCoding coding; // whether values are bit fields (may be many)
  S4          idata;  // If coding is internal, some storage space
  int         noAutoCreate; // Whether to autocreate unknown tags
  union {
	// if coding == Mask, then 0-31 (which bit is on), 32 for zero
	struct {
	  Tag *tagMask;   // If coding is mask, array of 33 tag ptrs 
	} b;
	// if coding == Num, expandable array of TagDbInstRec
	struct {
	  S2 numInst;     // How many insts have we recorded
	  S2 maxInst;     // How many insts alloced in the dataInst vector
	  TagDbInstRec *datInst;   // The actual instances, can be realloc'd bigger
	} n;
  } v;
} TagDbTagRec, *TagDbTag;

typedef struct _TagDbAll {
  S2          numTag;     // How many tags have we recorded
  S2          maxTag;     // How many tags alloced in the dataTag vector
  TagDbTagRec dataTag[kTagDb_MaxTags];    // The actual tag recs
} TagDbAllRec, *TagDbAll;

static int TagDbInitialize()
{
  if (!gsv->tagDb) {
	TagDbAll tdb = PEngAlloc(PKGNAME, sizeof(TagDbAllRec));
	gsv->tagDb = tdb;
	tdb->numTag = 0;
	tdb->maxTag = ArrayCount(tdb->dataTag);

	TagInitialize();
  }
  return 0;
}

// Find or create a DB for this tag name
//
TagDbId TagDbFindOrNew(cSTR name, TagDbCoding coding, int noAutoCreate)
{
  TagDbAll tdb;
  TagDbTag ttt;
  int tdx;
  Stx nid;

  if (!name || !name[0])
	return 0;

  if (!(tdb = gsv->tagDb))
	{ TagDbInitialize(); tdb = gsv->tagDb; }

  if (!(nid = StxDbFindOrNew(gsv->pkTags, name)))
	return 0;

  // See if we have it
  for (tdx = 0; tdx < tdb->numTag; tdx++)
	if (tdb->dataTag[tdx].nid == nid)
	  break;

  // Give out a new (zero based) ID
  if (tdx == tdb->numTag) {
	if (tdb->numTag == tdb->maxTag)
	  return 0;
	tdx = tdb->numTag++;
	ttt = &(tdb->dataTag[tdx]);
	ttt->nid    = nid;
	ttt->coding = coding;
	ttt->noAutoCreate = noAutoCreate;
	switch (coding) {
	case TagDb_Mask:
	  ttt->v.b.tagMask = PEngAlloc(PKGNAME, 33*sizeof(Tag)); // 32=0, else which bit
	  break;
	case TagDb_Id:
	  ttt->v.n.numInst = 0;
	  ttt->v.n.maxInst = kTagDb_InitInsts;
	  ttt->v.n.datInst = PEngAlloc(PKGNAME, ttt->v.n.maxInst * sizeof(TagDbInstRec));
	  break;
	case TagDb_Value:
	  ttt->v.n.maxInst = 0;
	  ttt->v.n.datInst = NULL;
	}
  }
  return kTagDb_IdShift + tdx;
}

// See if we have a DB for this tag name
//
TagDbId TagDbFind(cSTR name)
{
  TagDbAll tdb;
  int tdx;
  Stx nid;

  if (!name || !name[0])
	return 0;

  if (!(tdb = gsv->tagDb))
	{ TagDbInitialize(); tdb = gsv->tagDb; }

  if (!(nid = StxDbFindOrNew(gsv->pkTags, name)))
	return 0;

  // See if we have it
  for (tdx = 0; tdx < tdb->numTag; tdx++)
	if (tdb->dataTag[tdx].nid == nid)
	  return kTagDb_IdShift + tdx;

  return 0;
}

// Assign a value to a code for this Tag name
//
Tag TagDbTagAssign(TagDbId tdx, cSTR strVal, US4 code)
{
  int tix, vid;
  TagDbAll tdb;
  TagDbTag ttt;
  TagKeyRec tk;
  char strBuf[16];

  tdx -= kTagDb_IdShift;
  if (!(tdb = gsv->tagDb) || tdx < 0 || tdx >= tdb->numTag)
	return NULL;
  ttt = &(tdb->dataTag[tdx]);

  if (!strVal) {
	char msgBuf[128];
	snprintf(strBuf, sizeof(strBuf), "T%ld", code); 
	strVal = strBuf;
	snprintf(msgBuf, sizeof(msgBuf), "AutoTag code=%d %s:%s", 
			 code, StxDbById(gsv->pkTags, ttt->nid), strBuf);
	PEngLog(PKGNAME, msgBuf, NULL, 0);
  }


  // Find the ID of our value string
  vid = (strVal && strVal[0] ? StxDbFindOrNew(gsv->pkTags, strVal) : 0);

  switch (ttt->coding) {
  case TagDb_Mask:
	// Assign this to the first on bit
	if (code == 0) tix=32;
	else {
	  for (tix=0; tix<32; tix++)
		if (code & (1<<tix)) break;
	}
	StructNull(tk);
	tk.nId = ttt->nid; tk.vId = vid;
	ttt->v.b.tagMask[tix] = TagFindOrNew(&tk);
	return ttt->v.b.tagMask[tix];

  case TagDb_Id:
	// See if we have this code 
	for (tix=0; tix < ttt->v.n.numInst; tix++)
	  if (ttt->v.n.datInst[tix].code == code)
		break;

	if (tix < ttt->v.n.numInst) {
	  ; // have seen this code before, just override
	} else {
	  if (ttt->v.n.numInst == ttt->v.n.maxInst) { // Need more space
		int newMax  = ttt->v.n.maxInst * 2;
		int oldSize = ttt->v.n.maxInst * sizeof(TagDbInstRec);
		int newSize = newMax * sizeof(TagDbInstRec);
		TagDbInstRec *oldDat = ttt->v.n.datInst;
		TagDbInstRec *newDat = PEngAlloc(PKGNAME, newSize);
		memcpy(newDat, oldDat, oldSize);
		ttt->v.n.datInst = newDat;
		PermitExpirePtr(PKGNAME, oldDat, oldSize);
		ttt->v.n.maxInst = newMax;
	  }
	  // Build tag and fill in our code
	  tix = ttt->v.n.numInst++;
	} 
	ttt->v.n.datInst[tix].code = code;
	StructNull(tk);
	tk.nId = ttt->nid; tk.vId = vid;
	ttt->v.n.datInst[tix].tag  = TagFindOrNew(&tk);
	return ttt->v.n.datInst[tix].tag;
 
  case TagDb_Value:
	return NULL;
  default:
	PEngError(PKGNAME, StxDbById(gsv->pkTags, ttt->nid), "InvalidCoding");
	return NULL;
  }
}


// Returns number of tags that match, up to 32
// Fills tagList w/ pointers to the tags
// Auto creates tags for previously unknown tags
//
int TagDbByCode(TagDbId tdx, US4 code, Tag *tagList)
{
  TagDbAll tdb;
  TagDbTag ttt;
  Tag tag = NULL;
  int tcnt = 0, b, i; 
  int tdxx = (tdx - kTagDb_IdShift);

  if (!(tdb = gsv->tagDb) || tdxx < 0 || tdxx >= tdb->numTag)
	return 0;
  ttt = &(tdb->dataTag[tdxx]);

  switch (ttt->coding) {
  case TagDb_Mask:
	if (code == 0) {
	  tag = ttt->v.b.tagMask[32];
	  if (!tag && !ttt->noAutoCreate) // Maybe create it
		tag = TagDbTagAssign(tdx, NULL, 0);
	  if (tag)
		tagList[tcnt++] = tag;
	} else {
	  for (b=0; code && b<32; b++)
		if (code & (1<<b)) {
		  code ^= (1<<b); // shut the bit off
		  tag = ttt->v.b.tagMask[b];
		  if (!tag && !ttt->noAutoCreate) // Maybe create it
			tag = TagDbTagAssign(tdx, NULL, (1<<b));
		  if (tag)
			tagList[tcnt++] = tag;
		}
	}
	break;
  case TagDb_Id:
	for (i=0; i<ttt->v.n.numInst; i++)
	  if (ttt->v.n.datInst[i].code == code)
		{ tag = ttt->v.n.datInst[i].tag; break; }
	if (!tag && !ttt->noAutoCreate) // Maybe create it
	  tag = TagDbTagAssign(tdx, NULL, code);
	if (tag)
	  tagList[tcnt++] = tag;
	break;
  case TagDb_Value:
	break;
  }
  return tcnt;
}

S4 TagDbInternal(TagDbId tdx, S4 newData)
{
  TagDbAll tdb;
  TagDbTag ttt;
  S4 oldData;

  tdx -= kTagDb_IdShift;
  if (!(tdb = gsv->tagDb) || tdx < 0 || tdx >= tdb->numTag)
	return 0;
  ttt = &(tdb->dataTag[tdx]);

  oldData = ttt->idata;
  if (newData) ttt->idata = newData;
  return oldData;
}
  
// ****************************************************************

static int dbXmlAux(PXml px, int mode, Tag xtag, S4 code, int bit)
{
  char msgBuf[256]; int len;
  S4 seq;
  cSTR name, value;

  seq = TagSequence(xtag);
  TagNameValue(xtag, &name, &value);

  snprintf(msgBuf, sizeof(msgBuf), "Tag name='%s' seq='%ld' code='%ld' memcount='%ld'", 
		name, seq, code, xtag->memCount);
  if (bit) {
	len = strlen(msgBuf);
	snprintf(msgBuf+len, sizeof(msgBuf)-len, " bit='%d'", bit);
  }
  if (mode) {
	S4 grpCnt, memCnt;
	memCnt = TagCount(xtag, &grpCnt);
	if (memCnt || grpCnt) {
	  len = strlen(msgBuf);
	  snprintf(msgBuf+len, sizeof(msgBuf)-len, " Xmemcnt='%ld' Xgrpcnt='%ld'", memCnt, grpCnt);
	}
  }
  return PXmlSetStr(px, msgBuf, value);
}

int TagDbXml(PXml px, TagDbId tdx, int mode)
{
  TagDbAll tdb;
  TagDbTag ttt;
  Tag tag;
  char msgBuf[128];
  int b, err = 0;

  if (!(tdb = gsv->tagDb))
	return 0;

  if (tdx == 0) {
	// Do them all
	err = PXmlPush(px, "TagDbs", tdb->numTag);
	for (tdx = 0; tdx < tdb->numTag; tdx++)
	  err |= TagDbXml(px, kTagDb_IdShift+tdx, mode);
	err |= PXmlPop(px);
	return err;
  }

  tdx -= kTagDb_IdShift;
  if (tdx < 0 || tdx >= tdb->numTag)
	return 0;
  ttt = &(tdb->dataTag[tdx]);

  switch (ttt->coding) {
  case TagDb_Mask:
	snprintf(msgBuf, sizeof(msgBuf), "TagDb name='%s' coding='mask' noauto='%d'", 
			 StxDbById(gsv->pkTags, ttt->nid), ttt->noAutoCreate);
	if (mode == 0) {
	  err = PXmlPush0(px, msgBuf, 0);
	} else {
	  err = PXmlPush(px, msgBuf, 0);
	  if ((tag = ttt->v.b.tagMask[32]))
		err |= dbXmlAux(px, mode, tag, 0, 0);
	  for (b=0; b<32; b++)
		if ((tag = ttt->v.b.tagMask[b]))
		  err |= dbXmlAux(px, mode, tag, (1<<b), b+1);
	  err |= PXmlPop(px);
	}
	break;
  case TagDb_Id:
	snprintf(msgBuf, sizeof(msgBuf), "TagDb name='%s' coding='num' count='%d' noauto='%d'", 
			 StxDbById(gsv->pkTags, ttt->nid), ttt->v.n.numInst, ttt->noAutoCreate);
	if (mode == 0) {
	  err = PXmlPush0(px, msgBuf, 0);
	} else {
	  err = PXmlPush(px, msgBuf, 0);
	  for (b=0; b<ttt->v.n.numInst; b++)
		if ((tag = ttt->v.n.datInst[b].tag))
		  err |= dbXmlAux(px, mode, tag, ttt->v.n.datInst[b].code, 0);
	  err |= PXmlPop(px);
	}
	break;
  case TagDb_Value:
	snprintf(msgBuf, sizeof(msgBuf), "TagDb name='%s' coding='value'",
			 StxDbById(gsv->pkTags, ttt->nid));
	err = PXmlPush0(px, msgBuf, 0);
	break;
  default:
	break;
  }
  return err;
}
