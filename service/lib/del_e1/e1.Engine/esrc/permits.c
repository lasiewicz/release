// permits.c
// pnelson 1/10/2005
// Inter thread coordination primitives
//    The one writer must have possesion of the one Mutex
//    Searchers grab one of several search permits from an InterlockedList
//    Memory free's can be queued up waiting for all possible searchers to finish

#include "peng.h"

#define PKGNAME PkgName_Eng
#define PermitWrite_MutexTimeout 5000L

/****************************************************************/

#ifdef GCC

// In GCC (PCN's debug mode), stub out all the low level primitives
#include "time.h"
#define HANDLE void*
#define DWORD  US4
#ifndef FALSE
#define FALSE  0
#endif
#define WAIT_OBJECT_0    0
#define WAIT_TIMEOUT     1
#define WAIT_ABANDONED   2

typedef struct { void *next; } SLIST_ENTRY;
typedef struct { void *next; } SLIST_HEADER;
static void InitializeSListHead(SLIST_HEADER *r) { r->next = NULL; } 
static void InterlockedPushEntrySList(SLIST_HEADER *r, SLIST_ENTRY *p)
  { p->next = r->next; r->next = p; }
static void *InterlockedPopEntrySList(SLIST_HEADER *r)
  { SLIST_ENTRY *p = r->next; if (p) r->next = p->next; return p; } 
static void InterlockedFlushSList(SLIST_HEADER *r) { r->next = NULL; }

static DWORD WaitForSingleObject(HANDLE m, long timeout) { return WAIT_OBJECT_0; }
static HANDLE CreateMutex(HANDLE m, int b, HANDLE n) { return "MyMutex"; }
static void  ReleaseMutex(HANDLE m) { ; }

typedef long E1Ticks;
static void E1TicksNow(E1Ticks *t)
{
  clock_t now = clock();
  *t = now;
}

#else

#include <windows.h>

// SuperFast time measure using Pentium-specific RDTSC instruction -- from www.sysinternals.com

typedef __int64 E1Ticks;

static void __declspec(naked) E1TicksNow(E1Ticks* t)
{
	_asm push ebp
	_asm push eax
	_asm push edx
	_asm _emit 0Fh
	_asm _emit 31h
	_asm mov ebp, dword ptr [esp + 0x10]
	_asm mov DWORD PTR [ebp], eax
	_asm mov DWORD PTR [ebp + 4], edx
	_asm pop edx
	_asm pop eax
	_asm pop ebp
	_asm ret
}
#endif

/****************************************************************/

// The one write permit is implemented with a Mutex
//

// Search Permits are implemented with an interlocked list
//   max num of simultaneous searches set on init() and that many permits created and pushed to list
//   Everyone trying to search must get one of those permits
//   While the search is running, the start time (in ticks) is stored in the permit
//   No memory will actually be free() until all searches that started before that memory expired are done
//   http://msdn.microsoft.com/library/en-us/dllproc/base/interlocked_singly_linked_lists.asp
//
typedef struct _PermitSearch {
  SLIST_ENTRY    sList; // Internal interlocked list structure
  int            seq;   // Sequence number of this permit
  E1Ticks        now;   // Time that search holding this permit started (or 0)
  void *forceAlign;     // Make sure structure is aligned (SLIST requirement) 
} PermitSearchRec;

// Holder for expired memory that will be freed at next opportunity
//
#define IsObjectFlag  (PkgName)((int)PkgName_ObjMgr * -1)

typedef struct _PermitExpire *PermitExpire;
typedef struct _PermitExpire {
  PermitExpire next;  // Next in either expire list, or in freelist when waiting for reuse
  E1Ticks   now;   // Time when memory was expired
  PkgName   pkg;   // Pkg that allocated ptr, or IsObjectFlag if its an object
  union {
	struct { 
	  void    *ptr;   // Memory to free
	  long     size;  // Size of that memory (for tracking) 
	} p;
	struct {
	  ObjMgr   xom;   // Object manager
	  ObjPtr   obj;   // Object to expire
	} o;
  } u;
} PermitExpireRec;

// The holders are allocated in big blocks for efficiency
// 
#define PermitExpireBlockCount 256
typedef struct _PermitExpireBlock *PermitExpireBlock;
typedef struct _PermitExpireBlock {
  PermitExpireBlock next;
  PermitExpireRec   mems[PermitExpireBlockCount];
} PermitExpireBlockRec;

// State of all inter thread synchronization mechanisms
//
typedef struct _GsvPermits {

  E1Ticks now;   // The last time that the clock was checked

  // Write implemented as a Mutex
  HANDLE      writeMutex;   // The OS mutex handle
  cSTR        writeOwner;   // The name of the current owner of the Mutex
  PermitWrite writeRand;    // A random number we give to owner, to make sure return is correct

  // Search implemented as fixed number of permits 
  int              searchCount;    // How many permits/slots are allocated
  SLIST_HEADER     searchHead;     // The internal head of the interlocked list
  PermitSearchRec *searchPermits;  // The actual permits that include start time on active searches
  
  // List of expired memory waiting to be freed
  PermitExpire      expList;   // Head of list of ptrs to be freed when possible
  PermitExpireBlock expBlocks;   // List of all allocated Mem blocks
  PermitExpire      expFree;    // Freelist of PermitExpire objects for reuse
  int            memBlockCnt;    // How many blocks have we allocated

} GsvPermitsRec, *GsvPermits;

// Our global state
static GsvPermitsRec gsvPermits;

/****************************************************************/

// Initialize state for Write,Search,MemExpire handling
//
int PermitInitialize(int count)
{
  int i;

  if (gsvPermits.searchPermits || gsvPermits.writeMutex) {
	fprintf(stderr, "**Already Initialized permits\n");
	return ERR_Permit_Inited;
  }

  // Initialize the write Mutex
  if ((gsvPermits.writeMutex = CreateMutex(NULL, FALSE, NULL)) == NULL) {
	fprintf(stderr, "**PermitInitizlize - Create Mutex error\n");
	return ERR_Permit_MutexCreate;
  }

  // Allocate the read permits
  if (count <= 0)
	count = E1CfgGetInt("cfg/permit/searchCount");
  if (count <= 0)
	count = 16;

  gsvPermits.searchCount   = count;
  gsvPermits.searchPermits = PEngAlloc(PKGNAME, count*sizeof(PermitSearchRec));

  // Initialize the interlock list
  InitializeSListHead(&(gsvPermits.searchHead));

  // Insert all permits on the list in reverse order
  for (i=0; i < count; i++) {
	PermitSearch permit = &(gsvPermits.searchPermits[i]);
	permit->now = 0;
	permit->seq = i;
	(void)InterlockedPushEntrySList(&(gsvPermits.searchHead), &(permit->sList));
  }
  return 0;
}

// Destroy the Permits package. 
// Engine is not useable after this
//
void PermitDestroy()
{
  PermitSearch permit;
  PermitExpire mem;
  PermitExpireBlock block;

  if (!gsvPermits.searchPermits) 
	return;

  // ??? What do do with the write Mutex?

  // Free the search permits
  InterlockedFlushSList(&(gsvPermits.searchHead));
  permit = (PermitSearch)InterlockedPopEntrySList(&(gsvPermits.searchHead));

  if (!permit) {
	free(gsvPermits.searchPermits);
	gsvPermits.searchPermits = NULL;
	gsvPermits.searchCount   = 0;
	memset(&(gsvPermits.searchHead), 0, sizeof(gsvPermits.searchHead));
  }

  // Free any pending expired memory
  while ((mem = gsvPermits.expList)) {
	gsvPermits.expList = mem->next;
	PEngFree(mem->pkg, mem->u.p.ptr, mem->u.p.size);
  }
  // Free all the blocks of holders
  while ((block = gsvPermits.expBlocks)) {
	gsvPermits.expBlocks = block->next;
	PEngFree(PKGNAME, block, sizeof(*block));
  }
}

/****************/

// Allocate a new block of holders for freed memory
//
static int xPermitExpireBlockNew()
{
  PermitExpireBlock block = (PermitExpireBlock)PEngAlloc(PKGNAME, sizeof(PermitExpireBlockRec));
  if (block) {
	int i;
	for (i=0; i < ArrayCount(block->mems); i++) {
	  PermitExpire mem = &(block->mems[i]);
	  mem->next = gsvPermits.expFree;
	  gsvPermits.expFree = mem;
	}
	gsvPermits.memBlockCnt += 1;
	return 0;
  } else {
	return -1;
  }
}

// Fetch a holder for this mem we're expiring
//
static PermitExpire xPermitExpirePop()
{
  PermitExpire mem;

  // Find holder for this ptr
  if (!gsvPermits.expFree)
	if (xPermitExpireBlockNew())
	  return NULL; // ???

  // Pop a holder off the free list - ok cause only 1 writer
  mem = gsvPermits.expFree;
  gsvPermits.expFree = mem->next;

  // Set up expiration time
  E1TicksNow(&(mem->now));
  return mem;
}

// Push the holder onto the "to be expired" list
//
static void xPermitExpirePush(PermitExpire mem)
{
  mem->next = gsvPermits.expList;
  gsvPermits.expList = mem;
}

// Dispose of the memory based on its type when the time has come
//
static void xPermitExpireDispose(PermitExpire mem)
{
  if (mem->pkg == IsObjectFlag) {
	if (mem->u.o.obj) {
	  ObjMgrFree(mem->u.o.xom, mem->u.o.obj);
	  mem->u.o.obj = NULL;
	}
  } else {
	if (mem->u.p.ptr) {
	  PEngFree(mem->pkg, mem->u.p.ptr, mem->u.p.size);
	  mem->u.p.ptr = NULL;
	}
  }
  mem->pkg = 0; 
}

// Can only be called by the one permit holding writer 
// Set up this ptr be freed as soon as all possible readers have finished
//
void PermitExpirePtr(PkgName pkg, void *ptr, US4 size)
{
  PermitExpire mem;
  if (ptr) {
	if ((mem = xPermitExpirePop())) {
	  mem->pkg  = pkg;
	  mem->u.p.ptr  = ptr;
	  mem->u.p.size = size;
	  xPermitExpirePush(mem);
	}
  }
}

// Can only be called by the one permit holding writer 
// Set up this obj be recycled as soon as all possible readers have finished
//
void PermitExpireObj(ObjMgr xom, ObjPtr obj)
{
  PermitExpire mem;
  if (xom && obj) {
	if ((mem = xPermitExpirePop())) {
	  mem->pkg   = IsObjectFlag;
	  mem->u.o.xom = xom;
	  mem->u.o.obj = obj;
	  xPermitExpirePush(mem);
	}
  }
}

// !!! Optimize -- one writer and push guarantee list in order, so free from the back

// Can only be called by the one permit holding writer 
// Free any expired memory that is not possibly in use
// Returns number of ptrs that were actually freed
//
int PermitExpireExec()
{
  int count = 0, i;
  if (gsvPermits.expList) {

	PermitExpire *root, mem;
	E1Ticks ticks, lowMark;

	// Find the time now
	E1TicksNow(&ticks);
	gsvPermits.now = ticks;

	// Find oldest search
	lowMark = ticks;
	for (i = 0; i < gsvPermits.searchCount; i++)
	  if (ticks = gsvPermits.searchPermits[i].now)
		if (ticks < lowMark)
		  lowMark = ticks;
	
	// Free any memory which expired before oldest search began
	root = &(gsvPermits.expList); 
	while ((mem = *root)) {
	  if (mem->now < lowMark) {
		// Goodbye to the memory
		++count;
		xPermitExpireDispose(mem);
		// Cut this holder out of the expire list, and set up root to look at next
		*root = mem->next;   
		// Add this holder to the free list
		mem->next = gsvPermits.expFree;
		gsvPermits.expFree = mem;
	  } else {
		// See if we can free the next one
		root = &(mem->next);
	  }
	}
  }
  return count;
}
  

/****************/

// Grab the Write Mutex, seqp will be set to rand number which must be returned by owner
// 
int PermitWriteTake(cSTR name, PermitWrite *seqp)
{
  // Request ownership of mutex.
  DWORD dwWaitResult = WaitForSingleObject(gsvPermits.writeMutex, PermitWrite_MutexTimeout);

  switch (dwWaitResult) {
  case WAIT_OBJECT_0: 
	// Got it!
	E1TicksNow(&(gsvPermits.now));
	gsvPermits.writeOwner = name;
	gsvPermits.writeRand  = *seqp = (long)(gsvPermits.now & 0x00FFFFFF);
	return 0;

  case WAIT_TIMEOUT:  // Time-out.
	return ERR_PermitWrite_Timeout;

  case WAIT_ABANDONED:	// Earlier owner abandoned ???
	return ERR_PermitWrite_Abandon;

  default:
	return ERR_PermitWrite_Unknown;
  }
}

// Confirm that the write permit is held by this thread
//
int PermitWriteConfirm(PermitWrite seq)
{
  // Make sure this is the real owner of the Mutex
	if (!seq)
		return ERR_Permit_Null;
	if (seq != gsvPermits.writeRand)
		return ERR_PermitWrite_NotOwner;
	return 0;
}

// Return the write Mutex, freeing any expired memory while we're here
//
int PermitWriteGive(PermitWrite seq)
{
  // Make sure this is the real owner of the Mutex
  if (seq != gsvPermits.writeRand)
	return ERR_PermitWrite_NotOwner;

  // Free up any memory that might be waiting
  if (gsvPermits.expList)
	PermitExpireExec();
  
  // Give back the Mutex
  gsvPermits.writeRand = 0;
  gsvPermits.writeOwner = NULL;
  ReleaseMutex(gsvPermits.writeMutex);
  return 0;
}

/****************/

// Take a search permit so you can run a search
//
int PermitSearchTake(PermitSearch *permitp)
{
  PermitSearch permit = (PermitSearch)InterlockedPopEntrySList(&(gsvPermits.searchHead));
  *permitp = NULL;

  if (!permit)
	return ERR_PermitSearch_NoneLeft;

  if (permit->now != 0) {
	fprintf(stderr, "**Permit(%d) already in use (0x%I64x) on Take()\n", permit->seq, permit->now);
	InterlockedPushEntrySList(&(gsvPermits.searchHead), &(permit->sList));
	return ERR_PermitSearch_InUse;
  }

  // "->now" will be set when search begins 
  *permitp = permit;
  return 0;
}

// Give back search permit, wanr if it still seems to be in use
//
int PermitSearchGive(PermitSearch permit)
{
  int err = 0;

  if (!permit)
	return ERR_PermitSearch_Null;

  if (permit->now)
	err = WARN_PermitSearch_InUse;

  InterlockedPushEntrySList(&(gsvPermits.searchHead), &(permit->sList));
  return err;
}

// Begin a search -- flags the start time
//
int PermitSearchBeg(PermitSearch permit)
{
  if (!permit)
	return ERR_PermitSearch_Null;
  if (permit->now)
	return ERR_PermitSearch_InUse;

  // Remember the time
  E1TicksNow(&(permit->now));
  gsvPermits.now = permit->now;
  return 0;
}

// End a search -- null the start time, permit can be re-used w/ Give/Take
//
int PermitSearchEnd(PermitSearch permit)
{
  if (!permit)
	return ERR_PermitSearch_Null;
  if (permit->now == 0)
	return ERR_PermitSearch_NotInUse;

  // No search running anymore
  permit->now = 0;
  return 0;
}

