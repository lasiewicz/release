// tag.c
// pnelson 11/15/2005
// Manage each tag associated with a user

#define TAG_FRIEND
#define GRP_FRIEND   // For XML walker
#define MEM_FRIEND   // For XML walker
#include "peng.h"

#define PKGNAME PkgName_Tag

// Generate hash for tag -- by XOR'ing IDs of strings
//
static PHashIdx xTagKeyToIdx(TagKey nv)
{
	US4 idx = (nv->nId * nv->vId) % kTag_HashSize;
	return (PHashIdx)idx;
}

// Compare by doing string compate on name/value
//
static int xTagKeyCmp(TagKey nv, Tag tag)
{
#ifdef PENGDEBUG
 // Place to stop in debugger
	if (nv->nId == 0)
		nv->nId = 0;
	if (nv->vId == 0)
		nv->vId = 0;
#endif
	if (nv->nId != tag->nxId)
		return (nv->nId < tag->nxId) ? -1 : 1;
	if (nv->vId != tag->vxId)
		return (nv->vId < tag->vxId) ? -1 : 1;
	return 0;
}

/****************/

int TagInitialize()
{
	if (!gsv->tags) {
		gsv->tags = PHashNew(PKGNAME, kTag_HashSize, 
							         (PHashKeyToIdxFnc*)xTagKeyToIdx, 
							         (PHashKeyCmpFnc  *)xTagKeyCmp);
		gsv->tagObjMgr = ObjMgrCreate(PKGNAME, sizeof(TagRec), kTag_MaxTags);
		gsv->pkTags = StxDbNew(kStx_TagBits, "tags");
		if (!gsv->tags || !gsv->tagObjMgr || !gsv->pkTags) 
			PEngError(PKGNAME, NULL, "Init");
	}
	return 0;
}

/****************/

S4 TagSequence(Tag xtag)
{
	if (xtag)
		return xtag->sequence;
	else
		return 0;
}

Tag TagBySequence(S4 seq)
{
	return (Tag)ObjMgrSeqToPtr(gsv->tagObjMgr, seq);
}

Tag TagFind(TagKey nv)
{
	if (!gsv->tags) 
	  TagInitialize();
	if (!nv) 
	  return NULL;
	if (nv->nId == 0) {
	  strlwr(nv->name);
	  nv->nId = StxDbFind(gsv->pkTags, nv->name);
	}
	if (nv->vId == 0) {
	  strlwr(nv->value);
	  nv->vId = StxDbFind(gsv->pkTags, nv->value);
	}
	nv->tag = (Tag)PHashFind(gsv->tags, (PHashKey)nv);
	return nv->tag;
}

Tag TagFindOrNewStr(cSTR name, cSTR value)
{
  TagKeyRec nv;
  StructNull(nv);
  str0ncpy(nv.name,  name,  sizeof(nv.name));
  str0ncpy(nv.value, value, sizeof(nv.value));
  return TagFindOrNew(&nv);
}

Tag TagFindOrNew(TagKey nv)
{
	Tag xtag; S4 seq;
	// has side effect of lower casing string, and looking up ids of name/value strs
	if ((xtag = TagFind(nv)))
		return xtag;
	// Create them if neded
	if (nv->nId == 0)
		nv->nId = StxDbFindOrNew(gsv->pkTags, nv->name);
	if (nv->vId == 0)
		nv->vId = StxDbFindOrNew(gsv->pkTags, nv->value);

	xtag = (Tag)ObjMgrNew(gsv->tagObjMgr, &seq);
	xtag->hashNext = NULL;
	xtag->sequence = seq;
	xtag->grpChain = NULL;
	xtag->nxId = nv->nId;
	xtag->vxId = nv->vId;
	// Back pointer in key
	nv->tag = xtag;

	return (Tag)PHashPush(gsv->tags, (PHashKey)nv, (PHashObj)xtag);
}

void TagNameValue(Tag xtag, cSTR *namep, cSTR *valuep)
{
  if (xtag) {
	*namep  = StxDbById(gsv->pkTags, xtag->nxId);
	*valuep = StxDbById(gsv->pkTags, xtag->vxId);
  } else {
	*namep = *valuep = NULL;
  }
}

Grp *TagChainGrpRoot(Tag xtag)
{
	if (!xtag) return NULL;
	return &(xtag->grpChain);
}

// ****************************************************************

int TagXml(PXml px, Tag xtag, int mode)
{
  int err;
  char msgBuf[128];
  S4 seq = TagSequence(xtag);
  cSTR name  = StxDbById(gsv->pkTags, xtag->nxId);
  cSTR value = StxDbById(gsv->pkTags, xtag->vxId);
  
  snprintf(msgBuf, sizeof(msgBuf), "Tag name='%s' seq='%ld' memcount='%ld'", 
		   name, xtag->sequence, xtag->memCount);
  if (mode != 0 && xtag->grpChain) {
	err  = PXmlPush(px, msgBuf, 0);
	err |= PXmlSetStr(px, "value", value);
	if (!err) {
	  err |= PXmlPush(px, "GrpsWithTag", 0);
	  err |= GrpXmlTagChain(px, xtag->grpChain, xtag);
	  err |= PXmlPop(px);
	}
	err |= PXmlPop(px);
  } else {
	err = PXmlSetStr(px, msgBuf, value);
  }
  return err;
}

// Returns # members with this tag, and optionally # grps w/ this tag
//
S4 TagCount(Tag xtag, S4 *grpCntp)
{
	Mem xmem, *root;
	Grp xgrp = xtag->grpChain;
	S4  grpCnt = 0, memCnt = 0;
  
	while (xgrp) {
		grpCnt += 1;

		// First count members who pt at this group
		root = GrpChainMemRoot(xgrp);
		for (xmem = *root; xmem; xmem = xmem->grpChain) 
			++memCnt;

		// Now move to the next group
		xgrp = GrpFollow(xgrp, xtag, 1);
	}
	if (grpCntp)
		*grpCntp = grpCnt;
	return memCnt;
}

