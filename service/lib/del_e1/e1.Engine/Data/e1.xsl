<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<!-- XSL Transformation for e1 output
 *
 * History:
 *   v1.0 |  12/13/05 | Philip Nelson
 *
-->

<xsl:stylesheet version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" >
  
  <xsl:output method="html" indent="no" encoding="utf-8" />
  <xsl:preserve-space elements="*"/>

  <!-- The outer structure of the page -->

<xsl:template match="/">
  <html>
    <head>
      <title>Hi</title>
    </head>
    <body>
      <h1> Hi </h1>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
