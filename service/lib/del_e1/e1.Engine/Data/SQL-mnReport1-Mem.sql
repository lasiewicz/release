select 
	mc.MemberID as memId, 
	mc.GroupID   as domId, 
	GenderMask, 
	BirthDate,
	CommunityInsertDate    as registerDate,
	CommunityLastLogonDate as activeDate,
	HasPhotoFlag as hasPhoto, 
	EducationLevel, 
	Religion, 
	LanguageMask, 
	Ethnicity, 
	SmokingHabits, 
	DrinkingHabits, 
	Height,
	MaritalStatus, 
	JDateReligion, 
	JDateEthnicity, 
	SynagogueAttendance, 
	KeepKosher, 
	SexualIdentityType, 
	RelationshipMask, 
	RelationshipStatus, 
	BodyType, 
	Zodiac, 
	MajorType, 
	Longitude as fltLng,
	Latitude  as fltLat,
	Depth1RegionID as Country,
	Depth2RegionID as State,
	Depth3RegionID as City,
	Depth4RegionID as Zip,
	12345 as SchoolID, -- ****************fix me! ******************
	AreaCode, 
	CommunityEmailCount as EmailCount	
from MemberCommunity mc (nolock)
	join MemberPersonals mp (nolock) 
		on mc.MemberID = mp.MemberID
	join MemberCompositeAttribute mca (nolock) 
		on mc.memberid = mca.memberid 
		and mc.GroupID = mca.GroupID
	join mnRegion.dbo.RegionFlat rf (nolock)
		on mp.RegionID = rf.Depth4RegionID
 	left join mnRegion.dbo.AreaCode ac (nolock) 
		on rf.Depth3RegionID = ac.RegionID
where

	    (GlobalStatusMask is null or GlobalStatusMask & 3 = 0)
	and mc.GroupID	in (1,10,12) -- all but jd
	and mca.GroupID in (1,10,12) -- all but jd
	and GenderMask in (5,6,9,10)
	and (SelfSuspendedFlag is null or SelfSuspendedFlag = 0)
--	and (mc.GroupID != 1 or CommunityEssayCount > 0 or HasPhotoFlag = 1)
	and (mc.GroupID != 1 or NextRegistrationActionPageID > 2 or HasPhotoFlag = 1)
 	and mp.RegionID is not null
 	and mp.BirthDate is not null
  	and Username is not null
  	and EmailAddress is not null
 	and (HideMask is null or HideMask = 0)


/*
create index  ix_MemberPersonals_RegionID on MemberPersonals(RegionID)
create index  ix_MemberPersonals_GlobalStatusMask on MemberPersonals(GlobalStatusMask)
create index  ix_MemberCommunity_GenderMask on MemberCommunity(GenderMask)
*/
-- select SelfSuspendedFlag, Count(*) from membercommunity (nolock) group by SelfSuspendedFlag
--select GenderMask, count(*) from MemberCommunity (nolock) where GroupID = 3 group by GenderMask
-- select 