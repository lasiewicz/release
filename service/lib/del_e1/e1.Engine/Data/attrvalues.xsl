<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#160;"> ]>

<!-- XSL Transformation for to build tagdb
 *
 * History:
 *   v1.0 |  12/5/05 | Philip Nelson
 *
-->

<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" >

	<xsl:output method="text" indent="no" encoding="utf-8" />

<!-- The outer structure of the page -->
<xsl:template match="/">
 <xsl:apply-templates select="attrmap/DescribeAttrs" />
</xsl:template>

<xsl:template match="DescribeAttrs">
  <xsl:apply-templates mode='mask' select="attr[@coding='mask']" />
  <xsl:apply-templates mode='num'  select="attr[@coding='num']" />
</xsl:template>

<xsl:template match="attr" mode="mask">
	<xsl:text>tagdb?coding=mask&amp;n=</xsl:text>
	<xsl:value-of select="@name"/>
	<xsl:apply-templates select="value" />
	<xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="attr" mode="num">
	<xsl:text>tagdb?coding=num&amp;n=</xsl:text>
	<xsl:value-of select="@name"/>
	<xsl:apply-templates select="value" />
	<xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="value">
	<xsl:text>&amp;c=</xsl:text>
	<xsl:value-of select="@code"/>
	<xsl:text>&amp;v=</xsl:text>
	<xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>
