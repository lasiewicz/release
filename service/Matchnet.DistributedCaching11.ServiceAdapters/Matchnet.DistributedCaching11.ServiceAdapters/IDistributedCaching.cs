using System;
using System.Collections;

namespace Matchnet.DistributedCaching.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IDistributedCaching.
	/// </summary>
	public interface IDistributedCaching
	{
		void Put(string cacheKey,  ArrayList cacheItem, string namedCacheName);

		ArrayList GetArrayList(string cacheKey, string namedCacheName);

		void Put(string cacheKey, string value, string namedCacheName);

		string GetString(string cacheKey, string namedCacheName);
	}
}
