using System;
using System.Collections;
using System.Diagnostics;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.DistributedCaching.ValueObjects.ServiceDefinitions;

namespace Matchnet.DistributedCaching11.ServiceAdapters
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class DistributedCachingSA : SABase
	
	{
		private const string SOURCE = "Matchnet.DistributedCaching";

		#region Singleton implementation
		public static readonly DistributedCachingSA Instance = new DistributedCachingSA();

		private DistributedCachingSA()
		{ }
		#endregion

		public void Put(string cacheKey, string cacheItem, string namedCacheName)
		{
			string uri = string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).Put(cacheKey, cacheItem, namedCacheName);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				SAException saException = new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex);
				EventLog.WriteEntry(SOURCE, saException.Dump, EventLogEntryType.Error);
			}
		}

		public void Put(string cacheKey, ArrayList cacheItem, string namedCacheName)
		{
			string uri = string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).Put(cacheKey, cacheItem, namedCacheName);
				}
				finally
				{
					base.Checkin(uri);
				}                               
			}
			catch (Exception ex)
			{
				SAException saException = new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex);
				EventLog.WriteEntry(SOURCE, saException.Dump, EventLogEntryType.Error);
			}   
		}

		public string GetString(string cacheKey, string namedCacheName)
		{
			string uri = string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).GetString(cacheKey, namedCacheName);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				SAException saException = new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex);
				EventLog.WriteEntry(SOURCE, saException.Dump, EventLogEntryType.Error);
			}

			return Constants.NULL_STRING;
		}

		public ArrayList GetArrayList(string cacheKey, string namedCacheName)
		{
			string uri = string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).GetArrayList(cacheKey, namedCacheName);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				SAException saException = new SAException(string.Format("Put() error (uri: {0}).  CacheKey: {1}", uri, cacheKey), ex);
				EventLog.WriteEntry(SOURCE, saException.Dump, EventLogEntryType.Error);
			}

			return null;
		}

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISTRIBUTEDCACHINGSVC_SA_CONNECTION_LIMIT"));
		}

		private IDistributedCaching getService(string uri)
		{
			try
			{
				return (IDistributedCaching)Activator.GetObject(typeof(IDistributedCaching), uri);
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}

		private string GetServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition("DISTRIBUTEDCACHING_SVC", Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DISTRIBUTEDCACHINGSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw new Exception("Cannot get configuration settings for remote service manager.", ex);
			}
		}
	}
}
