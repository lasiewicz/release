using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;


#region Matchnet references
	using Matchnet.PromoEngine.ServiceManagers;
	using Matchnet.PromoEngine.ValueObjects;
	using Matchnet.RemotingServices;
#endregion

namespace Matchnet.PromoEngine.Service
{
	/// <summary>
	///  PromoEngine service class 
	/// </summary>
	public class PromoEngine : RemotingServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private PromoEngineSM _PromoEngineSM;
		private ResourceTemplateSM _ResourceTemplateSM;

		/// <summary>
		/// PromoEngine service constructor
		/// </summary>
		public PromoEngine()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

		}

        private static void OnUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;

            if (ex != null)
            {
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_NAME,
                    "Unhandled exception:" + ex.ToString(),
                    System.Diagnostics.EventLogEntryType.Error);
            }
        }

		// The main entry point for the process
		static void Main()
		{
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);

			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PromoEngine() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}
		/// <summary>
		/// 
		/// </summary>
		protected override void RegisterServiceManagers()
		{
			System.Diagnostics.Trace.WriteLine("PromoEngine, Start RegisterServiceManagers");

			_PromoEngineSM = new PromoEngineSM();
			base.RegisterServiceManager(_PromoEngineSM);

			_ResourceTemplateSM = new ResourceTemplateSM();
			base.RegisterServiceManager(_ResourceTemplateSM);

			base.RegisterServiceManagers ();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			if (_PromoEngineSM!=null)
			{
				_PromoEngineSM.Dispose();
			}
            if (_ResourceTemplateSM != null)
            {
                _ResourceTemplateSM.Dispose();
            }
			base.Dispose( disposing );
		}
	}
}
