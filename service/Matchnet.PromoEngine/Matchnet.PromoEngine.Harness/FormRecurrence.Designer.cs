﻿namespace Matchnet.PromoEngine.Harness
{
    partial class FormRecurrence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonListJobs = new System.Windows.Forms.Button();
            this.listBoxJobs = new System.Windows.Forms.ListBox();
            this.buttonSavePromoApproval = new System.Windows.Forms.Button();
            this.listBoxTriggers = new System.Windows.Forms.ListBox();
            this.buttonListTriggers = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonListJobs
            // 
            this.buttonListJobs.Location = new System.Drawing.Point(73, 210);
            this.buttonListJobs.Name = "buttonListJobs";
            this.buttonListJobs.Size = new System.Drawing.Size(75, 23);
            this.buttonListJobs.TabIndex = 0;
            this.buttonListJobs.Text = "List Jobs";
            this.buttonListJobs.UseVisualStyleBackColor = true;
            this.buttonListJobs.Click += new System.EventHandler(this.buttonListJobs_Click);
            // 
            // listBoxJobs
            // 
            this.listBoxJobs.FormattingEnabled = true;
            this.listBoxJobs.Location = new System.Drawing.Point(33, 31);
            this.listBoxJobs.Name = "listBoxJobs";
            this.listBoxJobs.Size = new System.Drawing.Size(158, 160);
            this.listBoxJobs.TabIndex = 1;
            // 
            // buttonSavePromoApproval
            // 
            this.buttonSavePromoApproval.Location = new System.Drawing.Point(299, 106);
            this.buttonSavePromoApproval.Name = "buttonSavePromoApproval";
            this.buttonSavePromoApproval.Size = new System.Drawing.Size(75, 58);
            this.buttonSavePromoApproval.TabIndex = 2;
            this.buttonSavePromoApproval.Text = "Save PromoApproval";
            this.buttonSavePromoApproval.UseVisualStyleBackColor = true;
            this.buttonSavePromoApproval.Click += new System.EventHandler(this.buttonSavePromoApproval_Click);
            // 
            // listBoxTriggers
            // 
            this.listBoxTriggers.FormattingEnabled = true;
            this.listBoxTriggers.Location = new System.Drawing.Point(33, 254);
            this.listBoxTriggers.Name = "listBoxTriggers";
            this.listBoxTriggers.Size = new System.Drawing.Size(158, 160);
            this.listBoxTriggers.TabIndex = 3;
            // 
            // buttonListTriggers
            // 
            this.buttonListTriggers.Location = new System.Drawing.Point(73, 449);
            this.buttonListTriggers.Name = "buttonListTriggers";
            this.buttonListTriggers.Size = new System.Drawing.Size(75, 23);
            this.buttonListTriggers.TabIndex = 4;
            this.buttonListTriggers.Text = "List Triggers";
            this.buttonListTriggers.UseVisualStyleBackColor = true;
            this.buttonListTriggers.Click += new System.EventHandler(this.buttonListTriggers_Click);
            // 
            // FormRecurrence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 545);
            this.Controls.Add(this.buttonListTriggers);
            this.Controls.Add(this.listBoxTriggers);
            this.Controls.Add(this.buttonSavePromoApproval);
            this.Controls.Add(this.listBoxJobs);
            this.Controls.Add(this.buttonListJobs);
            this.Name = "FormRecurrence";
            this.Text = "FormRecurrence";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonListJobs;
        private System.Windows.Forms.ListBox listBoxJobs;
        private System.Windows.Forms.Button buttonSavePromoApproval;
        private System.Windows.Forms.ListBox listBoxTriggers;
        private System.Windows.Forms.Button buttonListTriggers;
    }
}