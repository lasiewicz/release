using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.PromoEngine.Harness
{
	/// <summary>
	/// Summary description for ResourceTemplate.
	/// </summary>
	public class FormResourceTemplate : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button ButtonGetResourceTemplateCollection;
		private System.Windows.Forms.DataGrid DataGridResourceTemplateCollection;
		private System.Windows.Forms.Button ButtonSaveResourceTemplate;
		private System.Windows.Forms.Button ButtonGetPromo;
		private System.Windows.Forms.TextBox TextBoxBranID;
		private System.Windows.Forms.DataGrid DataGridPromo;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormResourceTemplate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ButtonGetResourceTemplateCollection = new System.Windows.Forms.Button();
			this.DataGridResourceTemplateCollection = new System.Windows.Forms.DataGrid();
			this.ButtonSaveResourceTemplate = new System.Windows.Forms.Button();
			this.ButtonGetPromo = new System.Windows.Forms.Button();
			this.DataGridPromo = new System.Windows.Forms.DataGrid();
			this.TextBoxBranID = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.DataGridResourceTemplateCollection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DataGridPromo)).BeginInit();
			this.SuspendLayout();
			// 
			// ButtonGetResourceTemplateCollection
			// 
			this.ButtonGetResourceTemplateCollection.Location = new System.Drawing.Point(8, 8);
			this.ButtonGetResourceTemplateCollection.Name = "ButtonGetResourceTemplateCollection";
			this.ButtonGetResourceTemplateCollection.Size = new System.Drawing.Size(192, 23);
			this.ButtonGetResourceTemplateCollection.TabIndex = 0;
			this.ButtonGetResourceTemplateCollection.Text = "Get ResourceTemplateCollection";
			this.ButtonGetResourceTemplateCollection.Click += new System.EventHandler(this.ButtonGetResourceTemplateCollection_Click);
			// 
			// DataGridResourceTemplateCollection
			// 
			this.DataGridResourceTemplateCollection.DataMember = "";
			this.DataGridResourceTemplateCollection.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.DataGridResourceTemplateCollection.Location = new System.Drawing.Point(8, 40);
			this.DataGridResourceTemplateCollection.Name = "DataGridResourceTemplateCollection";
			this.DataGridResourceTemplateCollection.Size = new System.Drawing.Size(472, 200);
			this.DataGridResourceTemplateCollection.TabIndex = 1;
			// 
			// ButtonSaveResourceTemplate
			// 
			this.ButtonSaveResourceTemplate.Location = new System.Drawing.Point(224, 8);
			this.ButtonSaveResourceTemplate.Name = "ButtonSaveResourceTemplate";
			this.ButtonSaveResourceTemplate.Size = new System.Drawing.Size(200, 23);
			this.ButtonSaveResourceTemplate.TabIndex = 2;
			this.ButtonSaveResourceTemplate.Text = "Create New ResourceTemplate";
			this.ButtonSaveResourceTemplate.Click += new System.EventHandler(this.ButtonSaveResourceTemplate_Click);
			// 
			// ButtonGetPromo
			// 
			this.ButtonGetPromo.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
			this.ButtonGetPromo.Location = new System.Drawing.Point(8, 248);
			this.ButtonGetPromo.Name = "ButtonGetPromo";
			this.ButtonGetPromo.Size = new System.Drawing.Size(160, 23);
			this.ButtonGetPromo.TabIndex = 3;
			this.ButtonGetPromo.Text = "Get Promo";
			this.ButtonGetPromo.Click += new System.EventHandler(this.ButtonGetPromo_Click);
			// 
			// DataGridPromo
			// 
			this.DataGridPromo.DataMember = "";
			this.DataGridPromo.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.DataGridPromo.Location = new System.Drawing.Point(8, 280);
			this.DataGridPromo.Name = "DataGridPromo";
			this.DataGridPromo.Size = new System.Drawing.Size(472, 280);
			this.DataGridPromo.TabIndex = 4;
			// 
			// TextBoxBranID
			// 
			this.TextBoxBranID.Location = new System.Drawing.Point(176, 248);
			this.TextBoxBranID.Name = "TextBoxBranID";
			this.TextBoxBranID.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
			this.TextBoxBranID.TabIndex = 5;
			this.TextBoxBranID.Text = "1003";
			// 
			// FormResourceTemplate
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(488, 573);
			this.Controls.Add(this.TextBoxBranID);
			this.Controls.Add(this.DataGridPromo);
			this.Controls.Add(this.ButtonGetPromo);
			this.Controls.Add(this.ButtonSaveResourceTemplate);
			this.Controls.Add(this.DataGridResourceTemplateCollection);
			this.Controls.Add(this.ButtonGetResourceTemplateCollection);
			this.Name = "FormResourceTemplate";
			this.Text = "ResourceTemplate";
			((System.ComponentModel.ISupportInitialize)(this.DataGridResourceTemplateCollection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DataGridPromo)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void ButtonGetResourceTemplateCollection_Click(object sender, System.EventArgs e)
		{
			Matchnet.PromoEngine.ValueObjects.ResourceTemplate.ResourceTemplateCollection collection = 
				Matchnet.PromoEngine.ServiceAdapters.ResourceTemplateSA.Instance.GetResourceTemplateCollection();

			DataGridResourceTemplateCollection.DataSource = collection;
		}

		private void ButtonSaveResourceTemplate_Click(object sender, System.EventArgs e)
		{
			FormResourceTemplateSave save = new FormResourceTemplateSave();

			save.Show();

		}

		private void ButtonGetPromo_Click(object sender, System.EventArgs e)
		{
			Matchnet.PromoEngine.ValueObjects.PromoCollection promoCollection = 
			Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromos(Convert.ToInt32(TextBoxBranID.Text));

			DataGridPromo.DataSource = promoCollection;
		}
	}
}
