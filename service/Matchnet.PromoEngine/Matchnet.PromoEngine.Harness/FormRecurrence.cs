﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Quartz;

namespace Matchnet.PromoEngine.Harness
{
    public partial class FormRecurrence : Form
    {
        public FormRecurrence()
        {
            InitializeComponent();
        }

        private void buttonListJobs_Click(object sender, EventArgs e)
        {
            string[] jobs = Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetRecurrenceJobNames();

            listBoxJobs.DataSource = jobs;

        }

        private void buttonSavePromoApproval_Click(object sender, EventArgs e)
        {
            Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.SavePromoApproval(4322, 1003, 2);
        }

        void listBoxJobs_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            int index = this.listBoxJobs.IndexFromPoint(e.Location);

            if (index != System.Windows.Forms.ListBox.NoMatches)
            {

                MessageBox.Show(index.ToString());

                JobDetail jobDetail = (JobDetail)listBoxJobs.Items[index];

                MessageBox.Show("Name:" + jobDetail.Name + jobDetail.JobDataMap["CronExpression"]);
            }

        }

        private void buttonListTriggers_Click(object sender, EventArgs e)
        {
            string[] triggers = Matchnet.PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetRecurrenceTriggerNames();

            listBoxJobs.DataSource = triggers;
        }


    }
}
