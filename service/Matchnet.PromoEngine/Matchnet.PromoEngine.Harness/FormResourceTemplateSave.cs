using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.PromoEngine.Harness
{
	/// <summary>
	/// Summary description for FormResourceTemplateSave.
	/// </summary>
	public class FormResourceTemplateSave : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox TextBoxResourceTemplateID;
		private System.Windows.Forms.TextBox TextBoxContent;
		private System.Windows.Forms.TextBox TextBoxDescription;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button ButtonSave;
		private System.Windows.Forms.Label label4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormResourceTemplateSave()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.TextBoxResourceTemplateID = new System.Windows.Forms.TextBox();
			this.TextBoxContent = new System.Windows.Forms.TextBox();
			this.TextBoxDescription = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.ButtonSave = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// TextBoxResourceTemplateID
			// 
			this.TextBoxResourceTemplateID.Location = new System.Drawing.Point(144, 16);
			this.TextBoxResourceTemplateID.Name = "TextBoxResourceTemplateID";
			this.TextBoxResourceTemplateID.TabIndex = 0;
			this.TextBoxResourceTemplateID.Text = "";
			// 
			// TextBoxContent
			// 
			this.TextBoxContent.Location = new System.Drawing.Point(144, 48);
			this.TextBoxContent.Multiline = true;
			this.TextBoxContent.Name = "TextBoxContent";
			this.TextBoxContent.Size = new System.Drawing.Size(392, 200);
			this.TextBoxContent.TabIndex = 1;
			this.TextBoxContent.Text = "";
			// 
			// TextBoxDescription
			// 
			this.TextBoxDescription.Location = new System.Drawing.Point(144, 264);
			this.TextBoxDescription.Multiline = true;
			this.TextBoxDescription.Name = "TextBoxDescription";
			this.TextBoxDescription.Size = new System.Drawing.Size(392, 120);
			this.TextBoxDescription.TabIndex = 2;
			this.TextBoxDescription.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 23);
			this.label1.TabIndex = 3;
			this.label1.Text = "ResourceTemplateID:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 56);
			this.label2.Name = "label2";
			this.label2.TabIndex = 4;
			this.label2.Text = "Content:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 264);
			this.label3.Name = "label3";
			this.label3.TabIndex = 5;
			this.label3.Text = "Description:";
			// 
			// ButtonSave
			// 
			this.ButtonSave.Location = new System.Drawing.Point(176, 408);
			this.ButtonSave.Name = "ButtonSave";
			this.ButtonSave.Size = new System.Drawing.Size(152, 23);
			this.ButtonSave.TabIndex = 6;
			this.ButtonSave.Text = "Save ResourceTemplate";
			this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(256, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(240, 23);
			this.label4.TabIndex = 7;
			this.label4.Text = "Leave Empty to create a new one.";
			// 
			// FormResourceTemplateSave
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(576, 453);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.ButtonSave);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.TextBoxDescription);
			this.Controls.Add(this.TextBoxContent);
			this.Controls.Add(this.TextBoxResourceTemplateID);
			this.Name = "FormResourceTemplateSave";
			this.Text = "FormResourceTemplateSave";
			this.ResumeLayout(false);

		}
		#endregion

		private void ButtonSave_Click(object sender, System.EventArgs e)
		{
			// Create New
			if (TextBoxResourceTemplateID.Text == String.Empty)
			{
				Matchnet.PromoEngine.ServiceAdapters.ResourceTemplateSA.Instance.SaveResourceTemplate(
					new Matchnet.PromoEngine.ValueObjects.ResourceTemplate.ResourceTemplate(
					TextBoxContent.Text,
					TextBoxDescription.Text));

				MessageBox.Show("Saved");
			}
			else
			{
				// Update
				Matchnet.PromoEngine.ServiceAdapters.ResourceTemplateSA.Instance.SaveResourceTemplate(
					new Matchnet.PromoEngine.ValueObjects.ResourceTemplate.ResourceTemplate(
					Convert.ToInt32(TextBoxResourceTemplateID.Text),
					TextBoxContent.Text,
					TextBoxDescription.Text,
					Constants.NULL_INT,
					Matchnet.PromoEngine.ValueObjects.ResourceTemplate.ResourceTemplateType.Plan,
					false));

				MessageBox.Show("Updated");
			}
		}
	}
}
