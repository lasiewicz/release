using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Xml;
using  System.Text;
using System.Xml.Serialization;

using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;

namespace Matchnet.PromoEngine.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmPromoEngineHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnGetPromo;
		private System.Windows.Forms.ListBox lstPromos;
		private System.Windows.Forms.Label lblBrandID;
		private System.Windows.Forms.TextBox txtBrandID;
		private System.Windows.Forms.Button btnActivePromos;
		private System.Windows.Forms.Button btnUpdate;
		private System.Windows.Forms.Button btnMemberPromo;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.Button btnInactivate;
		private System.Windows.Forms.TextBox txtPromoID;
		private System.Windows.Forms.Button btnGetPromoByID;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtActivateDate;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtPageTitle;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtStartDate;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtPromoDesc;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.TextBox txtPromoURL;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox txtListOrd;
		private System.Windows.Forms.Label lblListOrder;
		private System.Windows.Forms.Button wso586;
		private System.Windows.Forms.Button rnpp53;
		private System.Windows.Forms.Button sprhp17uk;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button button14;
		private System.Windows.Forms.Button button15;
		private System.Windows.Forms.Button button16;
		private System.Windows.Forms.Button button17;
		private System.Windows.Forms.Button button18;
		private System.Windows.Forms.Button button19;
		private System.Windows.Forms.Button button20;
		private System.Windows.Forms.Button button21;
		private System.Windows.Forms.Button button22;
		private System.Windows.Forms.Button rnpp73;
		private System.Windows.Forms.Button rnpp74;
		private System.Windows.Forms.Button rnpp75;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button23;
		private System.Windows.Forms.Button button24;
		private System.Windows.Forms.Button button25;
		private System.Windows.Forms.Button button26;
		private System.Windows.Forms.Button button27;
		private System.Windows.Forms.Button button28;
		private System.Windows.Forms.Button button29;
		private System.Windows.Forms.Button button30;
		private System.Windows.Forms.Button button31;
		private System.Windows.Forms.Button button32;
		private System.Windows.Forms.Button button33;
		private System.Windows.Forms.Button button34;
		private System.Windows.Forms.CheckBox cbTestOnly;
		private System.Windows.Forms.Button button35;
		private System.Windows.Forms.Button button36;
		private System.Windows.Forms.Button button37;
		private System.Windows.Forms.Button rnpp102;
		private System.Windows.Forms.Button rnpp103;
		private System.Windows.Forms.Button button38;
		private System.Windows.Forms.Button button39;
		private System.Windows.Forms.Button RNPP96b;
		private System.Windows.Forms.Button button40;
		private System.Windows.Forms.Button button41;
		private System.Windows.Forms.Button button42;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Button ButtonHeaderResourceTemplateManager;
		private System.Windows.Forms.Button button43;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.ComboBox ComboBoxPromoApprovalStatus;
        private System.Windows.Forms.Label label11;
        private MenuItem menuItem4;
        private IContainer components;

		public frmPromoEngineHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.btnGetPromo = new System.Windows.Forms.Button();
            this.lstPromos = new System.Windows.Forms.ListBox();
            this.lblBrandID = new System.Windows.Forms.Label();
            this.txtBrandID = new System.Windows.Forms.TextBox();
            this.btnActivePromos = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnMemberPromo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.btnInactivate = new System.Windows.Forms.Button();
            this.txtPromoID = new System.Windows.Forms.TextBox();
            this.btnGetPromoByID = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtActivateDate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPageTitle = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStartDate = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPromoDesc = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ComboBoxPromoApprovalStatus = new System.Windows.Forms.ComboBox();
            this.ButtonHeaderResourceTemplateManager = new System.Windows.Forms.Button();
            this.txtListOrd = new System.Windows.Forms.TextBox();
            this.lblListOrder = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.rnpp53 = new System.Windows.Forms.Button();
            this.wso586 = new System.Windows.Forms.Button();
            this.sprhp17uk = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.rnpp75 = new System.Windows.Forms.Button();
            this.rnpp74 = new System.Windows.Forms.Button();
            this.rnpp73 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.txtPromoURL = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.RNPP96b = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.rnpp103 = new System.Windows.Forms.Button();
            this.rnpp102 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.cbTestOnly = new System.Windows.Forms.CheckBox();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGetPromo
            // 
            this.btnGetPromo.Location = new System.Drawing.Point(64, 56);
            this.btnGetPromo.Name = "btnGetPromo";
            this.btnGetPromo.Size = new System.Drawing.Size(128, 23);
            this.btnGetPromo.TabIndex = 1;
            this.btnGetPromo.Text = "Get Promos";
            this.btnGetPromo.Click += new System.EventHandler(this.btnGetPromo_Click);
            // 
            // lstPromos
            // 
            this.lstPromos.Location = new System.Drawing.Point(448, 24);
            this.lstPromos.Name = "lstPromos";
            this.lstPromos.Size = new System.Drawing.Size(608, 95);
            this.lstPromos.TabIndex = 3;
            this.lstPromos.SelectedIndexChanged += new System.EventHandler(this.lstPromos_SelectedIndexChanged);
            // 
            // lblBrandID
            // 
            this.lblBrandID.Location = new System.Drawing.Point(16, 24);
            this.lblBrandID.Name = "lblBrandID";
            this.lblBrandID.Size = new System.Drawing.Size(64, 23);
            this.lblBrandID.TabIndex = 5;
            this.lblBrandID.Text = "BrandID:";
            // 
            // txtBrandID
            // 
            this.txtBrandID.Location = new System.Drawing.Point(64, 24);
            this.txtBrandID.Name = "txtBrandID";
            this.txtBrandID.Size = new System.Drawing.Size(128, 20);
            this.txtBrandID.TabIndex = 0;
            // 
            // btnActivePromos
            // 
            this.btnActivePromos.Location = new System.Drawing.Point(64, 88);
            this.btnActivePromos.Name = "btnActivePromos";
            this.btnActivePromos.Size = new System.Drawing.Size(128, 23);
            this.btnActivePromos.TabIndex = 2;
            this.btnActivePromos.Text = "Get Active Promos";
            this.btnActivePromos.Click += new System.EventHandler(this.btnActivePromos_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(180, 55);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(64, 23);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnMemberPromo
            // 
            this.btnMemberPromo.Location = new System.Drawing.Point(280, 56);
            this.btnMemberPromo.Name = "btnMemberPromo";
            this.btnMemberPromo.Size = new System.Drawing.Size(128, 23);
            this.btnMemberPromo.TabIndex = 7;
            this.btnMemberPromo.Text = "Get Member Promo";
            this.btnMemberPromo.Click += new System.EventHandler(this.btnMemberPromo_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(216, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 23);
            this.label1.TabIndex = 9;
            this.label1.Text = "MemberID:";
            // 
            // txtMemberID
            // 
            this.txtMemberID.Location = new System.Drawing.Point(280, 24);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(128, 20);
            this.txtMemberID.TabIndex = 8;
            // 
            // btnInactivate
            // 
            this.btnInactivate.Location = new System.Drawing.Point(24, 104);
            this.btnInactivate.Name = "btnInactivate";
            this.btnInactivate.Size = new System.Drawing.Size(200, 23);
            this.btnInactivate.TabIndex = 10;
            this.btnInactivate.Text = "Inactivate selected promo";
            this.btnInactivate.Click += new System.EventHandler(this.btnInactivate_Click);
            // 
            // txtPromoID
            // 
            this.txtPromoID.Location = new System.Drawing.Point(280, 89);
            this.txtPromoID.Name = "txtPromoID";
            this.txtPromoID.Size = new System.Drawing.Size(128, 20);
            this.txtPromoID.TabIndex = 12;
            // 
            // btnGetPromoByID
            // 
            this.btnGetPromoByID.Location = new System.Drawing.Point(280, 120);
            this.btnGetPromoByID.Name = "btnGetPromoByID";
            this.btnGetPromoByID.Size = new System.Drawing.Size(128, 23);
            this.btnGetPromoByID.TabIndex = 11;
            this.btnGetPromoByID.Text = "Get Promo By ID";
            this.btnGetPromoByID.Click += new System.EventHandler(this.btnGetPromoByID_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(216, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "PromoID:";
            // 
            // txtActivateDate
            // 
            this.txtActivateDate.Location = new System.Drawing.Point(88, 24);
            this.txtActivateDate.Name = "txtActivateDate";
            this.txtActivateDate.Size = new System.Drawing.Size(80, 20);
            this.txtActivateDate.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 23);
            this.label3.TabIndex = 18;
            this.label3.Text = "End Date";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 23);
            this.label5.TabIndex = 24;
            this.label5.Text = "Page Title";
            // 
            // txtPageTitle
            // 
            this.txtPageTitle.Location = new System.Drawing.Point(24, 159);
            this.txtPageTitle.Name = "txtPageTitle";
            this.txtPageTitle.Size = new System.Drawing.Size(200, 20);
            this.txtPageTitle.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 23);
            this.label6.TabIndex = 27;
            this.label6.Text = "Start Date";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(88, 48);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(80, 20);
            this.txtStartDate.TabIndex = 28;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(16, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 16);
            this.label7.TabIndex = 30;
            this.label7.Text = "Promo Description";
            // 
            // txtPromoDesc
            // 
            this.txtPromoDesc.Location = new System.Drawing.Point(24, 215);
            this.txtPromoDesc.Name = "txtPromoDesc";
            this.txtPromoDesc.Size = new System.Drawing.Size(200, 20);
            this.txtPromoDesc.TabIndex = 31;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.ComboBoxPromoApprovalStatus);
            this.groupBox1.Controls.Add(this.ButtonHeaderResourceTemplateManager);
            this.groupBox1.Controls.Add(this.txtListOrd);
            this.groupBox1.Controls.Add(this.lblListOrder);
            this.groupBox1.Controls.Add(this.txtActivateDate);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtPageTitle);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btnUpdate);
            this.groupBox1.Controls.Add(this.btnInactivate);
            this.groupBox1.Controls.Add(this.txtStartDate);
            this.groupBox1.Controls.Add(this.txtPromoDesc);
            this.groupBox1.Location = new System.Drawing.Point(664, 168);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 345);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Update Promo";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(187, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 22);
            this.label11.TabIndex = 41;
            this.label11.Text = "Status";
            // 
            // ComboBoxPromoApprovalStatus
            // 
            this.ComboBoxPromoApprovalStatus.Items.AddRange(new object[] {
            "Pending",
            "Rejected",
            "Approved"});
            this.ComboBoxPromoApprovalStatus.Location = new System.Drawing.Point(253, 21);
            this.ComboBoxPromoApprovalStatus.Name = "ComboBoxPromoApprovalStatus";
            this.ComboBoxPromoApprovalStatus.Size = new System.Drawing.Size(101, 21);
            this.ComboBoxPromoApprovalStatus.TabIndex = 38;
            // 
            // ButtonHeaderResourceTemplateManager
            // 
            this.ButtonHeaderResourceTemplateManager.Location = new System.Drawing.Point(13, 263);
            this.ButtonHeaderResourceTemplateManager.Name = "ButtonHeaderResourceTemplateManager";
            this.ButtonHeaderResourceTemplateManager.Size = new System.Drawing.Size(180, 20);
            this.ButtonHeaderResourceTemplateManager.TabIndex = 34;
            this.ButtonHeaderResourceTemplateManager.Text = "HeaderResourceTemplate Manager";
            this.ButtonHeaderResourceTemplateManager.Click += new System.EventHandler(this.ButtonHeaderResourceTemplateManager_Click);
            // 
            // txtListOrd
            // 
            this.txtListOrd.Location = new System.Drawing.Point(240, 159);
            this.txtListOrd.Name = "txtListOrd";
            this.txtListOrd.Size = new System.Drawing.Size(100, 20);
            this.txtListOrd.TabIndex = 33;
            // 
            // lblListOrder
            // 
            this.lblListOrder.Location = new System.Drawing.Point(240, 132);
            this.lblListOrder.Name = "lblListOrder";
            this.lblListOrder.Size = new System.Drawing.Size(100, 16);
            this.lblListOrder.TabIndex = 32;
            this.lblListOrder.Text = "List Order";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.rnpp53);
            this.groupBox3.Controls.Add(this.wso586);
            this.groupBox3.Controls.Add(this.sprhp17uk);
            this.groupBox3.Controls.Add(this.button19);
            this.groupBox3.Controls.Add(this.button18);
            this.groupBox3.Controls.Add(this.button17);
            this.groupBox3.Controls.Add(this.button16);
            this.groupBox3.Controls.Add(this.button22);
            this.groupBox3.Controls.Add(this.button20);
            this.groupBox3.Controls.Add(this.button11);
            this.groupBox3.Controls.Add(this.button15);
            this.groupBox3.Controls.Add(this.button10);
            this.groupBox3.Controls.Add(this.button14);
            this.groupBox3.Controls.Add(this.button21);
            this.groupBox3.Controls.Add(this.button13);
            this.groupBox3.Controls.Add(this.button12);
            this.groupBox3.Controls.Add(this.rnpp75);
            this.groupBox3.Controls.Add(this.rnpp74);
            this.groupBox3.Controls.Add(this.rnpp73);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button24);
            this.groupBox3.Controls.Add(this.button23);
            this.groupBox3.Controls.Add(this.button9);
            this.groupBox3.Controls.Add(this.button8);
            this.groupBox3.Controls.Add(this.button35);
            this.groupBox3.Controls.Add(this.button32);
            this.groupBox3.Controls.Add(this.button31);
            this.groupBox3.Controls.Add(this.button33);
            this.groupBox3.Controls.Add(this.button30);
            this.groupBox3.Controls.Add(this.button27);
            this.groupBox3.Controls.Add(this.button26);
            this.groupBox3.Controls.Add(this.button25);
            this.groupBox3.Controls.Add(this.button36);
            this.groupBox3.Controls.Add(this.button34);
            this.groupBox3.Controls.Add(this.button29);
            this.groupBox3.Controls.Add(this.button28);
            this.groupBox3.Location = new System.Drawing.Point(40, 168);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(373, 432);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "OLD Promos";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Save JD Promos";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 48);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Save AS Promos";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(16, 80);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(80, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Save CUPID Promos";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // rnpp53
            // 
            this.rnpp53.Location = new System.Drawing.Point(112, 16);
            this.rnpp53.Name = "rnpp53";
            this.rnpp53.Size = new System.Drawing.Size(75, 23);
            this.rnpp53.TabIndex = 58;
            this.rnpp53.Text = "RNPP-53";
            this.rnpp53.Click += new System.EventHandler(this.rnpp53_Click);
            // 
            // wso586
            // 
            this.wso586.Location = new System.Drawing.Point(112, 48);
            this.wso586.Name = "wso586";
            this.wso586.Size = new System.Drawing.Size(75, 23);
            this.wso586.TabIndex = 53;
            this.wso586.Text = "WSO-536";
            this.wso586.Click += new System.EventHandler(this.wso586_Click);
            // 
            // sprhp17uk
            // 
            this.sprhp17uk.Location = new System.Drawing.Point(112, 80);
            this.sprhp17uk.Name = "sprhp17uk";
            this.sprhp17uk.Size = new System.Drawing.Size(75, 23);
            this.sprhp17uk.TabIndex = 61;
            this.sprhp17uk.Text = "SPRHP17-uk";
            this.sprhp17uk.Click += new System.EventHandler(this.sprhp17uk_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(16, 112);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 9;
            this.button19.Text = "RNPP-60";
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(16, 144);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 8;
            this.button18.Text = "RNPP-65";
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(16, 176);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 7;
            this.button17.Text = "RNPP-63";
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(16, 208);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 6;
            this.button16.Text = "RNPP-64";
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(112, 112);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 12;
            this.button22.Text = "Matt Test";
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(16, 304);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 10;
            this.button20.Text = "RNPP-68";
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(16, 272);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 1;
            this.button11.Text = "RNPP-67";
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(16, 336);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 5;
            this.button15.Text = "RNPP-69";
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(16, 368);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 0;
            this.button10.Text = "RNPP-70";
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(16, 240);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 4;
            this.button14.Text = "RNPP-66";
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(16, 400);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 11;
            this.button21.Text = "RNPP-71";
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(112, 176);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 3;
            this.button13.Text = "RNPP-62";
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(112, 144);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 2;
            this.button12.Text = "RNPP-61";
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // rnpp75
            // 
            this.rnpp75.Location = new System.Drawing.Point(112, 272);
            this.rnpp75.Name = "rnpp75";
            this.rnpp75.Size = new System.Drawing.Size(75, 23);
            this.rnpp75.TabIndex = 2;
            this.rnpp75.Text = "RNPP-75";
            this.rnpp75.Click += new System.EventHandler(this.rnpp75_Click);
            // 
            // rnpp74
            // 
            this.rnpp74.Location = new System.Drawing.Point(112, 240);
            this.rnpp74.Name = "rnpp74";
            this.rnpp74.Size = new System.Drawing.Size(75, 23);
            this.rnpp74.TabIndex = 1;
            this.rnpp74.Text = "RNPP-74";
            this.rnpp74.Click += new System.EventHandler(this.rnpp74_Click);
            // 
            // rnpp73
            // 
            this.rnpp73.Location = new System.Drawing.Point(112, 208);
            this.rnpp73.Name = "rnpp73";
            this.rnpp73.Size = new System.Drawing.Size(75, 23);
            this.rnpp73.TabIndex = 0;
            this.rnpp73.Text = "RNPP-73";
            this.rnpp73.Click += new System.EventHandler(this.rnpp73_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(112, 304);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 38;
            this.button6.Text = "RNPP-80";
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(112, 336);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "RNPP81";
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(200, 112);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(75, 23);
            this.button24.TabIndex = 4;
            this.button24.Text = "RNPP-86";
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(200, 80);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 3;
            this.button23.Text = "RNPP-85";
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(200, 48);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 2;
            this.button9.Text = "RNPP-84";
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(200, 16);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 1;
            this.button8.Text = "RNPP-83";
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(200, 272);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(75, 23);
            this.button35.TabIndex = 15;
            this.button35.Text = "RNPP-97";
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(200, 208);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(75, 23);
            this.button32.TabIndex = 12;
            this.button32.Text = "RNPP-95";
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(200, 144);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 11;
            this.button31.Text = "RNPP-93";
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(200, 240);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(75, 23);
            this.button33.TabIndex = 13;
            this.button33.Text = "RNPP-96";
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(200, 176);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(75, 23);
            this.button30.TabIndex = 10;
            this.button30.Text = "RNPP-94";
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(288, 80);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(75, 23);
            this.button27.TabIndex = 7;
            this.button27.Text = "RNPP-90";
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(288, 48);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(75, 23);
            this.button26.TabIndex = 6;
            this.button26.Text = "RNPP-88";
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(288, 16);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(75, 23);
            this.button25.TabIndex = 5;
            this.button25.Text = "RNPP-87";
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(288, 208);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(75, 23);
            this.button36.TabIndex = 16;
            this.button36.Text = "RNPP-99";
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(288, 176);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(75, 23);
            this.button34.TabIndex = 14;
            this.button34.Text = "RNPP-98";
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(288, 144);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(75, 23);
            this.button29.TabIndex = 9;
            this.button29.Text = "RNPP-89";
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(288, 112);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(75, 23);
            this.button28.TabIndex = 8;
            this.button28.Text = "RNPP-91";
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.txtPromoURL);
            this.groupBox4.Controls.Add(this.button4);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(420, 423);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(233, 173);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Create URL";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(112, 40);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 39;
            this.button5.Text = "Decrypt";
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtPromoURL
            // 
            this.txtPromoURL.Location = new System.Drawing.Point(8, 72);
            this.txtPromoURL.Name = "txtPromoURL";
            this.txtPromoURL.Size = new System.Drawing.Size(192, 20);
            this.txtPromoURL.TabIndex = 38;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(24, 40);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 37;
            this.button4.Text = "Encrypt";
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(7, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 23);
            this.label4.TabIndex = 37;
            this.label4.Text = "Put promo ID to encrypt or decrypt in the textbox";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 96);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(224, 32);
            this.label8.TabIndex = 37;
            this.label8.Text = "/Applications/Subscription/Subscribe.aspx?PromoPlan=";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button43);
            this.groupBox5.Controls.Add(this.button42);
            this.groupBox5.Controls.Add(this.button41);
            this.groupBox5.Controls.Add(this.button40);
            this.groupBox5.Controls.Add(this.RNPP96b);
            this.groupBox5.Controls.Add(this.button39);
            this.groupBox5.Controls.Add(this.button38);
            this.groupBox5.Controls.Add(this.rnpp103);
            this.groupBox5.Controls.Add(this.rnpp102);
            this.groupBox5.Controls.Add(this.button37);
            this.groupBox5.Location = new System.Drawing.Point(420, 173);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(192, 241);
            this.groupBox5.TabIndex = 37;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "NEW Promos";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(13, 194);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(94, 24);
            this.button43.TabIndex = 26;
            this.button43.Text = "JD Default Credit";
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(96, 112);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(75, 23);
            this.button42.TabIndex = 25;
            this.button42.Text = "RNPP-93b";
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(96, 80);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(75, 23);
            this.button41.TabIndex = 24;
            this.button41.Text = "RNPP-109";
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(8, 144);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(75, 23);
            this.button40.TabIndex = 23;
            this.button40.Text = "RNPP-94b";
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // RNPP96b
            // 
            this.RNPP96b.Location = new System.Drawing.Point(8, 112);
            this.RNPP96b.Name = "RNPP96b";
            this.RNPP96b.Size = new System.Drawing.Size(75, 23);
            this.RNPP96b.TabIndex = 22;
            this.RNPP96b.Text = "RNPP-96b";
            this.RNPP96b.Click += new System.EventHandler(this.RNPP96b_Click);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(8, 80);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(75, 23);
            this.button39.TabIndex = 21;
            this.button39.Text = "RNPP-104";
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(8, 48);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(75, 23);
            this.button38.TabIndex = 20;
            this.button38.Text = "RNPP-95b";
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // rnpp103
            // 
            this.rnpp103.Location = new System.Drawing.Point(96, 48);
            this.rnpp103.Name = "rnpp103";
            this.rnpp103.Size = new System.Drawing.Size(75, 23);
            this.rnpp103.TabIndex = 19;
            this.rnpp103.Text = "RNPP-103";
            this.rnpp103.Click += new System.EventHandler(this.rnpp103_Click);
            // 
            // rnpp102
            // 
            this.rnpp102.Location = new System.Drawing.Point(96, 16);
            this.rnpp102.Name = "rnpp102";
            this.rnpp102.Size = new System.Drawing.Size(75, 23);
            this.rnpp102.TabIndex = 18;
            this.rnpp102.Text = "RNPP-102";
            this.rnpp102.Click += new System.EventHandler(this.rnpp102_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(8, 16);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(75, 23);
            this.button37.TabIndex = 17;
            this.button37.Text = "RNPP-100";
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // cbTestOnly
            // 
            this.cbTestOnly.Location = new System.Drawing.Point(456, 144);
            this.cbTestOnly.Name = "cbTestOnly";
            this.cbTestOnly.Size = new System.Drawing.Size(104, 24);
            this.cbTestOnly.TabIndex = 15;
            this.cbTestOnly.Text = "Test Only";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem3,
            this.menuItem4});
            this.menuItem1.Text = "Other Harnesses";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "ResourceTemplate";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "Approval";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "Recurrences";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // frmPromoEngineHarness
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1286, 698);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPromoID);
            this.Controls.Add(this.txtMemberID);
            this.Controls.Add(this.txtBrandID);
            this.Controls.Add(this.btnGetPromoByID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMemberPromo);
            this.Controls.Add(this.btnActivePromos);
            this.Controls.Add(this.lblBrandID);
            this.Controls.Add(this.lstPromos);
            this.Controls.Add(this.btnGetPromo);
            this.Controls.Add(this.cbTestOnly);
            this.Menu = this.mainMenu1;
            this.Name = "frmPromoEngineHarness";
            this.Text = "Promo Engine Harness";
            this.Load += new System.EventHandler(this.frmPromoEngineHarness_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{

        Application.Run(new frmPromoEngineHarness());
		}

		private void btnGetPromo_Click(object sender, System.EventArgs e)
		{
			
			string strBrand=txtBrandID.Text.Trim();
			
			if (strBrand.Length>0)
			{
				try
				{
					PromoCollection colpromos=	PromoEngineSA.Instance.GetPromos(Int32.Parse(strBrand));
				
					
					if(colpromos.Count>0)
					{
						lstPromos.DataSource=colpromos;
						lstPromos.ValueMember="PromoID";
						lstPromos.DisplayMember="PromoID";
					}
					else
					{
						lstPromos.DataSource=null;
					}

					MessageBox.Show("Get Done");
				}
				catch(Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message);
					MessageBox.Show(ex.Message);
				}
				
			}
			else
			{
				MessageBox.Show("Please enter a BrandID.");
			}
		}

		private void btnActivePromos_Click(object sender, System.EventArgs e)
		{
			string strBrand=txtBrandID.Text.Trim();
			if (strBrand.Length>0)
			{
				try
				{
					PromoCollection colpromos=	PromoEngineSA.Instance.GetActivePromos(Int32.Parse(strBrand));
				
					
					if(colpromos.Count>0)
					{
						lstPromos.DataSource=colpromos;
						lstPromos.ValueMember="PromoID";
						lstPromos.DisplayMember="PromoID";
					}
					else
					{
						lstPromos.DataSource=null;
					}

					MessageBox.Show("Get Done");
				}
				catch(Exception ex)
				{
					MessageBox.Show(ex.Message);
				}


				
			}
			else
			{
				MessageBox.Show("Please enter a BrandID.");
			}

		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
					
			int brandID=1001;
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			ExpressionCollection criteria = new ExpressionCollection();
			
			
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(731,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(10,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(58,brandID));

			// Adding Age criteria
			
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThan,new ArrayList());
			expAge1.Values.Add(20);			
			criteria.Add(expAge1);

			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.LessThan,new ArrayList());
			expAge2.Values.Add(50);
			criteria.Add(expAge2);

			// Adding Education level criteria
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(32);	
			criteria.Add(expEducation);
			
			// Marital status
			Expression expMarital= new Expression(attributes.FindByID(32),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expMarital.Values.Add(2);	
			criteria.Add(expMarital);

			// Industry type
			Expression expIndustry= new Expression(attributes.FindByID(392),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expIndustry.Values.Add(134217728);	
			criteria.Add(expIndustry);

			// Gender Mask
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);
			//DaysToBirthDate
			Expression expBirthDateLower = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expBirthDateLower.Values.Add(-14);
			criteria.Add(expBirthDateLower);
			
			Expression expBirthDateHigher = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expBirthDateHigher.Values.Add(14);
			criteria.Add(expBirthDateHigher);
			
			//	Country = USA
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry .Values.Add(223);
			criteria.Add(expCountry );
			//	state = calif
			Expression expState= new Expression(attributes.FindByID(1005),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expState.Values.Add(1538);
			criteria.Add(expState );
			//	City= Brentwood
			Expression expCity= new Expression(attributes.FindByID(1006),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCity.Values.Add(9801921);
			criteria.Add(expCity );


			Promo promo= new Promo(Constants.NULL_INT,"Test. saved from code. for AS. Age between 20-50",new DateTime(2008,3,13),new DateTime(2008,4,21),10,criteria,plans,72,72,"Title on the fly");
			
			string st=SerializePromo(promo);
			System.Console.WriteLine(st);

			//PromoEngineSA.Instance.SavePromotion(promo,brandID);
			//Promo deserializedObj= (Promo ) DeserializePromo(st);
			Promo deserializedObj=DeserializePromofromFile(@"C:\PromoObj.xml");

		}
		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			try
			{
				Promo p =(Promo) lstPromos.SelectedItem;
				p.EndDate = Convert.ToDateTime(this.txtActivateDate.Text);//p.EndDate.AddDays(2);	// updating the end date
				p.StartDate = Convert.ToDateTime(this.txtStartDate.Text);
				p.ListOrder = Convert.ToInt32(this.txtListOrd.Text);
				p.PageTitle = this.txtPageTitle.Text;
				p.Description = this.txtPromoDesc.Text;
				p.PromoApprovalStatus = (PromoApprovalStatus)Enum.Parse(typeof(PromoApprovalStatus),ComboBoxPromoApprovalStatus.SelectedItem.ToString());
				PromoEngineSA.Instance.SavePromotion(p,Int32.Parse(txtBrandID.Text.Trim()));
				MessageBox.Show("Promo updated");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
		private void btnFaulty_Click(object sender, System.EventArgs e)
		{
			/*	Test for Transaction writer warning
			Promo promo24= new Promo(Constants.NULL_INT,"Test. saved from code. for JD, Faulty-No plans!",new DateTime(2008,3,13),new DateTime(2008,4,21),15,null,null,72,72,"Title on the fly");
			PromoEngineSA.Instance.SavePromotion(promo24,1001);	
			*/	
			// JD test
			int brandID=1003;
			
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			ExpressionCollection criteria = new ExpressionCollection();
			
			
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(707,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(72,brandID));


			ArrayList values = new ArrayList();
			values.Add(20);
			values.Add(40);
			
			
			criteria.Add(new Expression(attributes.FindByID(1005), PromoEngine.ValueObjects.OperatorType.Equal,values));
			
			try
			{
				// 1- no criteria
				Promo promo= new Promo(Constants.NULL_INT,"Test. saved from code. for JD, Faulty-No Criteria!",new DateTime(2008,3,13),new DateTime(2008,4,21),15,null,plans,72,72,"Title on the fly");
				PromoEngineSA.Instance.SavePromotion(promo,brandID);
				
			}
			catch
			{
				MessageBox.Show("JD promo Save failed.");

			}
			try
			{
				// 1- no plans
				Promo promo2= new Promo(Constants.NULL_INT,"Test. saved from code. for JD, Faulty-No plans!",new DateTime(2008,3,13),new DateTime(2008,4,21),15,criteria,null,72,72,"Title on the fly");
				PromoEngineSA.Instance.SavePromotion(promo2,brandID);	
			}
			catch
			{
				MessageBox.Show("JD promo Save failed.");

			}
		}

		private void btnMemberPromo_Click(object sender, System.EventArgs e)
		{
													
			try
			{
				
				if (txtMemberID.Text.Length>0 && txtBrandID.Text.Length>0)
				{
					int brandId=Int32.Parse(txtBrandID.Text.Trim());
					int intMemberID=Int32.Parse( txtMemberID.Text.Trim());
					Promo p = PromoEngineSA.Instance.GetMemberPromo(intMemberID, Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandId));
					if (p !=null)
					{
						PromoCollection colp= new PromoCollection(brandId);
						colp.Add(p);
						lstPromos.DataSource=colp;
						lstPromos.ValueMember="PromoID";
						lstPromos.DisplayMember="PromoID";
					}
					else
					{
						lstPromos.DataSource=null;
					}

					MessageBox.Show("Get Done");

				}
				else
				{
					MessageBox.Show("Please enter a BrandID and MemberID.");

				}
				
			}
			catch (Exception ex)
			{
				MessageBox.Show("No matching Promo found for memberID." + ex.Message);
			}
		
		}

		private void btnInactivate_Click(object sender, System.EventArgs e)
		{
			Promo p =(Promo) lstPromos.SelectedItem;
			p.EndDate=DateTime.Now;	// updating the end date
			PromoEngineSA.Instance.SavePromotion(p,Int32.Parse(txtBrandID.Text.Trim()));
			MessageBox.Show("Promo deactivated.");
		
		}

		private void btnGetPromoByID_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (txtPromoID.Text.Length>0 && txtBrandID.Text.Length>0)
				{
					Promo p = PromoEngineSA.Instance.GetPromoByID(Int32.Parse( txtPromoID.Text.Trim()), 
						Int32.Parse( txtBrandID.Text.Trim()));
					if (p !=null)
					{
						PromoCollection colp= new PromoCollection(Int32.Parse( txtBrandID.Text.Trim()));
						colp.Add(p);
						lstPromos.DataSource=colp;
						lstPromos.ValueMember="PromoID";
						lstPromos.DisplayMember="PromoID";
					}
					else
					{
						lstPromos.DataSource=null;
					}

					MessageBox.Show("Get Done");

				}
				
			}
			catch (Exception ex)
			{
				MessageBox.Show("No matching Promo found for PromoID. Make sure you have entered the brandID.");
			}
		
		
		}
		

		private Promo DeserializePromofromFile(string filename)
		{   

			// Create an instance of the XmlSerializer specifying type and namespace.
			XmlSerializer serializer = new 
				XmlSerializer(typeof(Promo));

			// A FileStream is needed to read the XML document.
			FileStream fs = new FileStream(filename, FileMode.Open);
			XmlReader reader = new XmlTextReader(fs);
          
			// Declare an object variable of the type to be deserialized.
			Promo p=null;

			// Use the Deserialize method to restore the object's state.
			p = (Promo) serializer.Deserialize(reader);
			return p;
			
		}

		/// <summary>

		/// Method to reconstruct an Object from XML string

		/// </summary>

		/// <param name="pXmlizedString"></param>

		/// <returns></returns>		
		
		private Object DeserializePromo ( String pXmlizedString )

		{

			XmlSerializer xs = new XmlSerializer ( typeof ( Promo ) );

			XmlTextReader textReader = new XmlTextReader(@"C:\PromoObj.xml");
			//textReader.t
			MemoryStream memoryStream = new MemoryStream ( StringToUTF8ByteArray ( pXmlizedString ) );

			XmlTextWriter xmlTextWriter = new XmlTextWriter ( memoryStream, Encoding.UTF8 );

 

			return xs.Deserialize ( memoryStream );

		} 


		private String SerializePromo ( Object pObject )
		{

			try

			{
				String XmlizedString = null;
				MemoryStream memoryStream = new MemoryStream ( );
				XmlSerializer xs = new XmlSerializer ( typeof ( Promo ) );				

				XmlTextWriter xmlTextWriter = new XmlTextWriter ( memoryStream, Encoding.UTF8 );
				XmlTextWriter xmlTextFileWriter = new XmlTextWriter ( @"C:\PromoObj.xml", Encoding.UTF8 );
			
				xs.Serialize ( xmlTextWriter, pObject );
				xs.Serialize ( xmlTextFileWriter, pObject );
				 
				xmlTextFileWriter.Close();

				memoryStream = ( MemoryStream ) xmlTextWriter.BaseStream;

				XmlizedString = UTF8ByteArrayToString( memoryStream.ToArray ( ) );								
				return XmlizedString;

			}

			catch ( Exception e )

			{

				System.Console.WriteLine ( e );

				return null;

			}

		} 

		private void CreateASPromo()
		{
			int brandID=1001;
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			ExpressionCollection criteria = new ExpressionCollection();
			
			
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(728,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(10,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));

			// Age(30,50),Education=Masters,MaritalStatus=Divorced,Job=Student,Gender=MF,-14<DaystoBD<14,country=US,State=CA,City=Brentwood 90049
			// Adding Age criteria
			
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThan,new ArrayList());
			expAge1.Values.Add(30);			
			criteria.Add(expAge1);

			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.LessThan,new ArrayList());
			expAge2.Values.Add(50);
			criteria.Add(expAge2);

			// Adding Education level criteria
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(32);	
			criteria.Add(expEducation);
			
			// Marital status 8= Divored
			Expression expMarital= new Expression(attributes.FindByID(32),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expMarital.Values.Add(8);	
			criteria.Add(expMarital);

			// Industry type 536870912=Student
			Expression expIndustry= new Expression(attributes.FindByID(392),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expIndustry.Values.Add(536870912);	
			criteria.Add(expIndustry);

			// Gender Mask
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);
			//DaysToBirthDate
			Expression expBirthDateLower = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expBirthDateLower.Values.Add(-14);
			criteria.Add(expBirthDateLower);
			
			Expression expBirthDateHigher = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expBirthDateHigher.Values.Add(14);
			criteria.Add(expBirthDateHigher);
			
			//	Country = USA
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry.Values.Add(223);
			criteria.Add(expCountry );
			//	state = calif
			Expression expState= new Expression(attributes.FindByID(1005),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expState.Values.Add(1538);
			criteria.Add(expState );
			//	City= Brentwood
			Expression expCity= new Expression(attributes.FindByID(1006),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCity.Values.Add(9801921);
			criteria.Add(expCity );


			Promo promo= new Promo(Constants.NULL_INT,"Age(30,50),Education=Masters,MaritalStatus=Divorced,Job=Student,Gender=MF,-14<DaystoBD<14,country=US,State=CA,City=Brentwood 90049",new DateTime(2008,4,1),new DateTime(2011,12,31),13,criteria,plans,728,728,"Age(30,50),Education=Masters,MaritalStatus=Divorced,Job=Student,Gender=MF,-14<DaystoBD<14,country=US,State=CA,City=Brentwood 90049");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);
			MessageBox.Show("All attributes Promo Saved.");
		}
		private void CreateJDPromos()
		{
			int brandID=1003;
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			ExpressionCollection criteria = new ExpressionCollection();
			
			// Birthday promo
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(725,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(726,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//DaysToBirthDate
			Expression expBirthDateLower = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expBirthDateLower.Values.Add(-7);
			criteria.Add(expBirthDateLower);
			
			Expression expBirthDateHigher = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expBirthDateHigher.Values.Add(7);
			criteria.Add(expBirthDateHigher);
			
			// subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			
			criteria.Add(expSubStatus);
			Promo promo= new Promo(Constants.NULL_INT,"JDate Birth Date Promo",new DateTime(2008,4,1),new DateTime(2011,12,31),10,criteria,plans,725,725,"It's your birthday - join JDate and find your life's companion!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("Birthday Promo Saved.");

			// Registered for 30 days
			plans.Clear();
			criteria.Clear();
			promo=null;

			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(726,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(74,brandID));


			Expression expRegisterDays = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThan,new ArrayList());
			expRegisterDays.Values.Add(30);
			
			Expression expSubStatusReg= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatusReg.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);			
			criteria.Add(expSubStatusReg);

			Promo promoReg= new Promo(Constants.NULL_INT,"Registered for 30 days.",new DateTime(2008,4,1),new DateTime(2011,12,31),11,criteria,plans,726,726,"You've registered, now subscribe and find your soul mate!");

			PromoEngineSA.Instance.SavePromotion(promoReg,brandID);

			MessageBox.Show("Registered for 30 days Promo Saved.");
			

			//	All Attributes promo
			//  a) Age between (not equal) 20 AND 50 b) Education Level=Masters degree c) Marital Status= Single d)What Do you do=Retired 
			//  e) Gender Mask=Man looking for woman f)Country = USA g)state=CA

			plans.Clear();
			criteria.Clear();
			promo=null;

			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(707,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(722,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			

			// Adding Age criteria
			
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThan,new ArrayList());
			expAge1.Values.Add(20);			
			criteria.Add(expAge1);

			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.LessThan,new ArrayList());
			expAge2.Values.Add(50);
			criteria.Add(expAge2);

			// Adding Education level criteria
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(32);	
			criteria.Add(expEducation);
			
			// Marital status
			Expression expMarital= new Expression(attributes.FindByID(32),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expMarital.Values.Add(2);	
			criteria.Add(expMarital);

			// Industry type
			Expression expIndustry= new Expression(attributes.FindByID(392),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expIndustry.Values.Add(134217728);	
			criteria.Add(expIndustry);

			// Gender Mask
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);		
			
			//	Country = USA
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry .Values.Add(223);
			criteria.Add(expCountry );
			//	state = calif
			Expression expState= new Expression(attributes.FindByID(1005),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expState.Values.Add(1538);
			criteria.Add(expState );
			
			
			Promo promoAll= new Promo(Constants.NULL_INT,"Age between 20 AND 50,Education=Masters,MaritalStatus=Single,Job=Retired,Gender=MW,Country=US,state=CA",new DateTime(2008,4,1),new DateTime(2011,12,31),12,criteria,plans,418,418,"Age between 20 AND 50,Education=Masters,MaritalStatus=Single,Job=Retired,GenderMask=MW,Country=US,state=CA");

			PromoEngineSA.Instance.SavePromotion(promoAll,brandID);

			MessageBox.Show(" All attributes Promo Saved.");
		}


		private void CreateCUPIDPromos()
		{
			int brandID=1015;
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			ExpressionCollection criteria = new ExpressionCollection();
			
			// Promo 1
			
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(288,brandID));			
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(228,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(190,brandID));			
			
			Expression expSubStatusReg= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatusReg.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);			
			criteria.Add(expSubStatusReg);

			// Adding Age criteria
			
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge1.Values.Add(18);			
			criteria.Add(expAge1);

			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expAge2.Values.Add(24);
			criteria.Add(expAge2);

			//	Country = Israel
			Expression expCountry1 = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry1.Values.Add(105);
			criteria.Add(expCountry1);

			// Industry type 536870912=Student
			Expression expIndustry= new Expression(attributes.FindByID(392),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expIndustry.Values.Add(536870912);	
			criteria.Add(expIndustry);

			Promo promoReg= new Promo(Constants.NULL_INT,"CUPID- nonSub, Age18-24,student, live in Israel",new DateTime(2008,4,1),new DateTime(2011,12,31),11,criteria,plans,288,288,"CUPID- nonSub, Age18-24,student, live in Israel");

			PromoEngineSA.Instance.SavePromotion(promoReg,brandID);

			MessageBox.Show("Promo 1 Saved.");

			// Cupid promo 2
			plans.Clear();
			criteria.Clear();			
			
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(174,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(316,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(318,brandID));
			
			// subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			
			criteria.Add(expSubStatus1);

			//	Country = Israel
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry.Values.Add(105);
			criteria.Add(expCountry );

			Promo promo= new Promo(Constants.NULL_INT,"CUPID Promo- live in Israel and  neverSubed",new DateTime(2008,4,1),new DateTime(2011,12,31),12,criteria,plans,316,316,"CUPID Promo- live in Israel and  neverSubed");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("CUPID promo 2 Saved.");			

		}

		/// <summary>

		/// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.

		/// </summary>

		/// <param name="characters">Unicode Byte Array to be converted to String</param>

		/// <returns>String converted from Unicode Byte Array</returns>

		private String UTF8ByteArrayToString ( Byte[ ] characters )

		{

			UTF8Encoding encoding = new UTF8Encoding ( );

			String constructedString = encoding.GetString ( characters );

			return ( constructedString );

		}

		/// <summary>

		/// Converts the String to UTF8 Byte array and is used in De serialization

		/// </summary>

		/// <param name="pXmlString"></param>

		/// <returns></returns>

		private Byte[ ] StringToUTF8ByteArray ( String pXmlString )

		{

			UTF8Encoding encoding = new UTF8Encoding ( );

			Byte[ ] byteArray = encoding.GetBytes ( pXmlString );

			return byteArray;

		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			CreateJDPromos();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			CreateASPromo();
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			CreateCUPIDPromos();
		}

		private void frmPromoEngineHarness_Load(object sender, System.EventArgs e)
		{
			this.txtActivateDate.Text = "";
		}

		private void btnRNPP35_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(725,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(80,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - los angeles
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			//7 to 21 days after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);

//			Expression expRegisterDaysEnd = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
//			expRegisterDaysEnd.Values.Add(21);
//			criteria.Add(expRegisterDaysEnd);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP35 - JD LA Non-Sub; Never Sub Promo",new DateTime(2008, 5, 5),new DateTime(2008,10,5),5,criteria,plans,725,725,"Two great summer deals! Act now, this offer expires in 14 days.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP35 promos saved");

		}

		private void RNPP36_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(725,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(80,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - los angeles
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			//30 to 44 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

//			Expression expDaysAfterSubscriptionExpiredEnd= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
//			expDaysAfterSubscriptionExpiredEnd.Values.Add(-44);
//			criteria.Add(expDaysAfterSubscriptionExpiredEnd);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP36 - JD LA Non-Sub; X-Subscriber Promo",new DateTime(2008, 5, 5),new DateTime(2008,10,5),6,criteria,plans,725,725,"Two great summer deals! Act now, this offer expires in 14 days.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP36 promos saved");
		}

		private void btnRNPP37_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(725,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(80,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//7 to 21 days after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);

//			Expression expRegisterDaysEnd = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
//			expRegisterDaysEnd.Values.Add(21);
//			criteria.Add(expRegisterDaysEnd);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP37 - JD Miami Non-Sub; Never Sub Promo",new DateTime(2008, 5, 5),new DateTime(2008,10,5),7,criteria,plans,725,725,"Two great summer deals! Act now, this offer expires in 14 days. ");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP37 promos saved");
		}

		private void btnRNPP38_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(725,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(80,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//30 to 44 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

//			Expression expDaysAfterSubscriptionExpiredEnd= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
//			expDaysAfterSubscriptionExpiredEnd.Values.Add(-44);
//			criteria.Add(expDaysAfterSubscriptionExpiredEnd);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP38 - JD Miami Non-Sub; X-Subscriber Promo",new DateTime(2008, 5, 5),new DateTime(2008,10,5),8,criteria,plans,725,725,"Two great summer deals! Act now, this offer expires in 14 days.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP38 promos saved");
		}

		private void lstPromos_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				Promo p =(Promo) lstPromos.SelectedItem;
				this.txtActivateDate.Text = p.EndDate.ToShortDateString();
				this.txtStartDate.Text = p.StartDate.ToShortDateString();
				this.txtPageTitle.Text = p.PageTitle;
				this.txtPromoDesc.Text = p.Description;
				this.txtListOrd.Text = p.ListOrder.ToString();
				this.ComboBoxPromoApprovalStatus.SelectedItem = p.PromoApprovalStatus.ToString();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			try
			{
				Promo p =(Promo) lstPromos.SelectedItem;
				p.PageTitle = this.txtPageTitle.Text; //update page title
				PromoEngineSA.Instance.SavePromotion(p,Int32.Parse(txtBrandID.Text.Trim()));
				MessageBox.Show("Page Title updated");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void label6_Click(object sender, System.EventArgs e)
		{
		
		}

		private void btnUpdateStartDate_Click(object sender, System.EventArgs e)
		{
			try
			{
				Promo p =(Promo) lstPromos.SelectedItem;
				p.EndDate = Convert.ToDateTime(this.txtStartDate.Text); //update start date
				PromoEngineSA.Instance.SavePromotion(p,Int32.Parse(txtBrandID.Text.Trim()));
				MessageBox.Show("Date updated");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btnPromoDesc_Click(object sender, System.EventArgs e)
		{
			try
			{
				Promo p =(Promo) lstPromos.SelectedItem;
				p.Description = this.txtPromoDesc.Text; //update promo desc
				PromoEngineSA.Instance.SavePromotion(p,Int32.Parse(txtBrandID.Text.Trim()));
				MessageBox.Show("Promo Desc updated");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void button4_Click_1(object sender, System.EventArgs e)
		{
			try
			{
				string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies"; //this is the key used in subscribecommon.cs
				Matchnet.Lib.Encryption.SymmetricalEncryption encryptor  = new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
				this.txtPromoURL.Text = System.Web.HttpUtility.UrlEncode(encryptor.Encrypt(this.txtPromoURL.Text, SUBSCRIPTION_ENCRYPT_KEY));
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			try
			{
				string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies"; //this is the key used in subscribecommon.cs
				Matchnet.Lib.Encryption.SymmetricalEncryption decryptor  = new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
				this.txtPromoURL.Text = decryptor.Decrypt(System.Web.HttpUtility.UrlDecode(this.txtPromoURL.Text), SUBSCRIPTION_ENCRYPT_KEY);
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void RNPP29_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(725,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(726,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//7 days before birthday til 6 days after
			Expression expBirthDateLower = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expBirthDateLower.Values.Add(7);
			criteria.Add(expBirthDateLower);
			
			Expression expBirthDateHigher = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expBirthDateHigher.Values.Add(-6);
			criteria.Add(expBirthDateHigher);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP29 Birthday Promotion",new DateTime(2008, 5, 15),new DateTime(2008,12,31),8,criteria,plans,725,725,"Your birthday special expires soon - Subscribe today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP29 promos saved");
		}

		private void SPRHP17_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
//			brandID=1001; //as.com			
//			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
//			
//			//PLANS
//			plans= new PromoPlanCollection(brandID);
//			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(751,brandID));
//			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID));
//			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));
//			
//			//CRITERIAS
//			criteria = new ExpressionCollection();
//			//subscription status
//			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
//			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
//			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
//			criteria.Add(expSubStatus1_1);
//
//			promo= new Promo(Constants.NULL_INT,"SPRHP17 1001 Test Premium Services on STAGE",new DateTime(2008, 5, 12),new DateTime(2008,6,30),4,criteria,plans,751,751,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
//			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 2
			brandID=1013; //date.ca
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(752,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus2_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus2_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1013 Test Premium Services on STAGE",new DateTime(2008, 5, 12),new DateTime(2008,6,30),4,criteria,plans,752,752,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 3
			brandID=1107; //jdate.co.uk
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(750,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus3_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus3_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1107 Test Premium Services on STAGE",new DateTime(2008, 5, 12),new DateTime(2008,6,30),4,criteria,plans,750,750,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 4
			brandID=1003; //jdate.com
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus4_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus4_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus4_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus4_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1003 Test Premium Services on STAGE",new DateTime(2008, 5, 12),new DateTime(2008,6,30),4,criteria,plans,747,747,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("SPRHP-17 promos saved");
		}

		private void RNPP40_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - Philadelphia
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504);
			criteria.Add(expDMA);

			//7 after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP40 JD Philadelphia Non-Subscriber; Never Subscribed Promo",new DateTime(2008, 5, 13),new DateTime(2008,5,29),9,criteria,plans,400,400,"Ready to meet someone special? Become a Subscriber today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP40 promos saved");
		}

		private void RNPP41_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - Philadelphia
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504);
			criteria.Add(expDMA);

			//30+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP41 JD Philadelphia Non-Subscriber; Ex Sub Promo",new DateTime(2008, 5, 13),new DateTime(2008,5,29),9,criteria,plans,400,400,"Ready to meet someone special? Become a Subscriber today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP41 promos saved");
		}

		private void RNPP42_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - NY
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(501);
			criteria.Add(expDMA);

			//30+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP42 JD Philadelphia Non-Subscriber; Ex Sub Promo",new DateTime(2008, 5, 13),new DateTime(2008,5,29),9,criteria,plans,400,400,"Ready to meet someone special? Become a Subscriber today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP42 promos saved");
		}

		private void RNPP43_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - NY
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(501);
			criteria.Add(expDMA);

			//7 after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP43 JD NY Non-Subscriber; Never Subscribed Promo",new DateTime(2008, 5, 13),new DateTime(2008,5,29),9,criteria,plans,400,400,"Ready to meet someone special? Become a Subscriber today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP43 promos saved");
		}

		private void RNPP44_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - Chicago
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(602);
			criteria.Add(expDMA);

			//7 after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP44 JD Chicago Non-Subscriber; Never Subscribed Promo",new DateTime(2008, 5, 13),new DateTime(2008,5,29),9,criteria,plans,400,400,"Ready to meet someone special? Become a Subscriber today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP44 promos saved");
		}

		private void RNPP45_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - Chicago
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(602);
			criteria.Add(expDMA);

			//30+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP45 JD Chicago Non-Subscriber; Ex Sub Promo",new DateTime(2008, 5, 13),new DateTime(2008,5,29),9,criteria,plans,400,400,"Ready to meet someone special? Become a Subscriber today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP45 promos saved");
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			//RNPP46
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);

			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(786,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - los angeles
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			//7+ days after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP46 - JD LA Non-Sub; Never Sub Promo",new DateTime(2008, 5, 20),new DateTime(2008,6,3),8,criteria,plans,786,786,"Act now, limited time offer.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP46 promos saved");
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			//RNPP-47
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(786,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - los angeles
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			//30+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP47 - JD LA Non-Sub; X-Subscriber Promo",new DateTime(2008, 5, 20),new DateTime(2008,6,3),8,criteria,plans,786,786,"Act now, limited time offer.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP47 promos saved");
		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			//RNPP-48
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(786,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//7+ days after registration date
			Expression expRegisterDaysBegin = new Expression(attributes.FindByID(1002), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expRegisterDaysBegin.Values.Add(7);
			criteria.Add(expRegisterDaysBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP48 - JD Miami Non-Sub; Never Sub Promo",new DateTime(2008, 5, 20),new DateTime(2008,6,3),8,criteria,plans,786,786,"Act now, limited time offer.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP48 promos saved");
		}

		private void button9_Click(object sender, System.EventArgs e)
		{
			//RNPP-49
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(786,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus1);

			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//30+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP49 - JD Miami Non-Sub; X-Subscriber Promo",new DateTime(2008, 5, 20),new DateTime(2008,6,3),8,criteria,plans,786,786,"Act now, limited time offer.");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP49 promos saved");
		}

		private void SPRHP17b_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1001; //as.com			
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(804,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1001 Test Premium Services on STAGE",new DateTime(2008, 5, 12),new DateTime(2008,6,30),4,criteria,plans,804,804,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("SPRHP17b promos saved");
		}

		private void RNPP50_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//1 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-1);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//dma - Miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP50 1003 Miami Promo ExSub",new DateTime(2008, 6, 10),new DateTime(2008,6,24),9,criteria,plans,747,747,"Act now! Limited-time offer!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP50 promos saved");
		}

		private void sprhp30_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1004; //jdate.co.il	
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(748,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(749,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP30 1004 Test Premium Services on STAGE",new DateTime(2008, 5, 23),new DateTime(2008,6,30),4,criteria,plans,748,748,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("SPRHP30 promos saved");
		}

		private void groupBox5_Enter(object sender, System.EventArgs e)
		{
		
		}

		private void rnpp51_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1015; //cupid.co.il
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(404,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(174,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(369,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			// Gender Mask - male
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(1);	
			criteria.Add(expGender);

			promo= new Promo(Constants.NULL_INT,"RNPP51 New CUPID promotion for men - 3 months in 195 ILS, Non Subscriber",new DateTime(2008, 6, 10),new DateTime(2008,9,10),20,criteria,plans,174,174,"");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP51 promos saved");
		}

		private void wso586_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(818,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(819,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//dma - los angeles
			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(803);
			criteria.Add(expDMA2);

			promo= new Promo(Constants.NULL_INT,"WSO536 - Test Promo for Jay Moon",new DateTime(2008, 5, 20),new DateTime(2008,6,10),50,criteria,plans,818,818,"WSO-586 Test Promo for Jay Moon");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("WSO-536 promos saved");
		}

		private void rnpp52_Click(object sender, System.EventArgs e)
		{
			int brandID=1003; //jdate.com
			PromoAttributeCollection attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			PromoPlanCollection plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(786,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			#region criterias
			ExpressionCollection criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1);

			//7 days before birthday til 6 days after
			Expression expBirthDateLower = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expBirthDateLower.Values.Add(7);
			criteria.Add(expBirthDateLower);
			
			Expression expBirthDateHigher = new Expression(attributes.FindByID(1008),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expBirthDateHigher.Values.Add(-6);
			criteria.Add(expBirthDateHigher);
			
			#endregion

			Promo promo= new Promo(Constants.NULL_INT,"RNPP52 Birthday Promotion",new DateTime(2008, 6, 11),new DateTime(2008,12,31),8,criteria,plans,747,747,"Your birthday special expires soon - Subscribe today!");

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			MessageBox.Show("RNPP52 promos saved");
		}

		private void rnpp55_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(818,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//dma - Miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP55 1003 Miami Promo NeverSub",new DateTime(2008, 6, 10),new DateTime(2008,6,24),9,criteria,plans,747,747,"Act now! Limited-time offer! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP55 promos saved");
		}

		private void PremiumSvcJdate_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(813);
			criteria.Add(expDMA);

			//Gender Mask - Male seeks Female
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);

			//Age > 95
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge1.Values.Add(95);			
			criteria.Add(expAge1);

			//Education - Elementary
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(2);	
			criteria.Add(expEducation);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - NonSub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,747,747,"Premium Services Preprod Test for Matthew ROUND2!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo2
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus2_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalSubscribed);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedFTS);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscriberFreeTrial);
			criteria.Add(expSubStatus2_1);

			//dma
			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(813);
			criteria.Add(expDMA2);

			//Gender Mask - Male seeks Female
			Expression expGender2= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender2.Values.Add(9);	
			criteria.Add(expGender2);

			//Age > 95
			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge2.Values.Add(95);			
			criteria.Add(expAge2);

			//Education - Elementary
			Expression expEducation2= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation2.Values.Add(2);	
			criteria.Add(expEducation2);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - Sub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,400,400,"Premium Services Preprod Test for Matthew ROUND2!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);
			#endregion

			MessageBox.Show("Premium Services Jdate promos saved");
		}

		private void PremiumSvcJdateUK_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1107; //jdate UK
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(750,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(813);
			criteria.Add(expDMA);

			//Gender Mask - Male seeks Female
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);

			//Age > 95
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge1.Values.Add(95);			
			criteria.Add(expAge1);

			//Education - Elementary
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(2);	
			criteria.Add(expEducation);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - NonSub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,750,750,"Premium Services Preprod Test for Matthew ROUND2! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo2
			brandID=1107; //jdate UK
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(740,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus2_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalSubscribed);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedFTS);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscriberFreeTrial);
			criteria.Add(expSubStatus2_1);

			//dma
			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(813);
			criteria.Add(expDMA2);

			//Gender Mask - Male seeks Female
			Expression expGender2= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender2.Values.Add(9);	
			criteria.Add(expGender2);

			//Age > 95
			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge2.Values.Add(95);			
			criteria.Add(expAge2);

			//Education - Elementary
			Expression expEducation2= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation2.Values.Add(2);	
			criteria.Add(expEducation2);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - Sub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,740,740,"Premium Services Preprod Test for Matthew ROUND2! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("Premium Services JdateUK promos saved");
		}

		private void PremiumSvcAS_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1001; //AS
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(804,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(813);
			criteria.Add(expDMA);

			//Gender Mask - Male seeks Female
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);

			//Age > 95
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge1.Values.Add(95);			
			criteria.Add(expAge1);

			//Education - Elementary
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(2);	
			criteria.Add(expEducation);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - NonSub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,804,804,"Premium Services Preprod Test for Matthew ROUND2!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo2
			brandID=1001; //AS
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(6,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus2_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalSubscribed);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedFTS);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscriberFreeTrial);
			criteria.Add(expSubStatus2_1);

			//dma
			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(813);
			criteria.Add(expDMA2);

			//Gender Mask - Male seeks Female
			Expression expGender2= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender2.Values.Add(9);	
			criteria.Add(expGender2);

			//Age > 95
			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge2.Values.Add(95);			
			criteria.Add(expAge2);

			//Education - Elementary
			Expression expEducation2= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation2.Values.Add(2);	
			criteria.Add(expEducation2);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - Sub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,6,6,"Premium Services Preprod Test for Matthew ROUND2!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);
			#endregion

			MessageBox.Show("Premium Services AS promos saved");
		}

		private void PremiumSvcCA_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1013; //date.ca
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(752,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(813);
			criteria.Add(expDMA);

			//Gender Mask - Male seeks Female
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(9);	
			criteria.Add(expGender);

			//Age > 95
			Expression expAge1= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge1.Values.Add(95);			
			criteria.Add(expAge1);

			//Education - Elementary
			Expression expEducation= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation.Values.Add(2);	
			criteria.Add(expEducation);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - NonSub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,752,752,"Premium Services Preprod Test for Matthew ROUND2!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo2
			brandID=1013; //date.ca
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(724,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus2_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalSubscribed);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.RenewalWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedFTS);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscribedWinBack);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.SubscriberFreeTrial);
			criteria.Add(expSubStatus2_1);

			//dma
			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(813);
			criteria.Add(expDMA2);

			//Gender Mask - Male seeks Female
			Expression expGender2= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender2.Values.Add(9);	
			criteria.Add(expGender2);

			//Age > 95
			Expression expAge2= new Expression(attributes.FindByID(1003), PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expAge2.Values.Add(95);			
			criteria.Add(expAge2);

			//Education - Elementary
			Expression expEducation2= new Expression(attributes.FindByID(89),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expEducation2.Values.Add(2);	
			criteria.Add(expEducation2);

			promo= new Promo(Constants.NULL_INT,"Premium Services Preprod test for Matthew - Sub",new DateTime(2008, 6, 5),new DateTime(2008,6,9),4,criteria,plans,724,724,"Premium Services Preprod Test for Matthew ROUND2!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);
			#endregion

			MessageBox.Show("Premium Services Date.ca promos saved");
		}

		private void sprhp17prod_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1001; //as.com			
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(804,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1001 Premium Services",new DateTime(2008, 6, 9),new DateTime(2008,12,31),10,criteria,plans,804,804,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 2
			brandID=1013; //date.ca
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(752,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus2_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus2_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus2_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1013 Premium Services",new DateTime(2008, 6, 9),new DateTime(2008,12,31),10,criteria,plans,752,752,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 3
			brandID=1107; //jdate.co.uk
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(750,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus3_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus3_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1107 Premium Services",new DateTime(2008, 6, 9),new DateTime(2008,12,31),10,criteria,plans,750,750,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 4
			brandID=1003; //jdate.com
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus4_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus4_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus4_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus4_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1003 Premium Services",new DateTime(2008, 6, 9),new DateTime(2008,12,31),10,criteria,plans,747,747,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("SPRHP-17prod promos saved");
		}

		private void rnpp56_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//20 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-20);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP56 1003 Los Angeles Promo ExSub",new DateTime(2008, 6, 10),new DateTime(2008,6,24),9,criteria,plans,747,747,"Act now! Limited-time offer! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP56 promos saved");
		}

		private void rnpp54_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(818,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//5 days after registration
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(5);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP54 1003 Los Angeles Promo NeverSub",new DateTime(2008, 6, 10),new DateTime(2008,6,24),9,criteria,plans,747,747,"Act now! Limited-time offer! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP54 promos saved");
		}

		private void rnpp53_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1003; //jdate
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//20 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-20);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//dma - philadelphia
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504);
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP53 1003 Philadelphia Promo ExSub",new DateTime(2008, 6, 10),new DateTime(2008,6,26),9,criteria,plans,747,747,"Act now! Limited-time offer! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP53 promos saved");
		}

		private void rnpp57_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1001; //AS
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(10,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//180 days or less after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-180);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//country
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry.Values.Add(223); //usa
			expCountry.Values.Add(38); //canada
			criteria.Add(expCountry);

			promo= new Promo(Constants.NULL_INT,"RNPP57 1001 Father's day promo for ex-sub",new DateTime(2008, 6, 11),new DateTime(2008,6,18),50,criteria,plans,727,727,"Save this Father's Day at AmericanSingles.com");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP57 promos saved");
		}

		private void rnpp58_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 1
			brandID=1013; //date.ca
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(746,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(745,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus1_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			//expSubStatus1_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus1_1);

			//180 days or less after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-180);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//country
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry.Values.Add(223); //usa
			expCountry.Values.Add(38); //canada
			criteria.Add(expCountry);

			promo= new Promo(Constants.NULL_INT,"RNPP58 1013 Father's day promo for ex-sub",new DateTime(2008, 6, 11),new DateTime(2008,6,18),50,criteria,plans,746,746,"Save this Father's Day at Date.ca");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP58 promos saved");
		}

		private void sprhp17uk_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			#region Promo 3
			brandID=1107; //jdate.co.uk
			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(750,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID));
			plans.Add((PromoPlan)Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID));
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus3_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus3_1);

			promo= new Promo(Constants.NULL_INT,"SPRHP17 1107 Premium Services",new DateTime(2008, 6, 9),new DateTime(2008,12,31),10,criteria,plans,750,750,"Stand out in the crowd! Purchase 6 months and get your highlighted profile free.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion



			MessageBox.Show("SPRHP-17prod promos saved");
		}

		private void rnpp59_Click(object sender, System.EventArgs e)
		{
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1003; //jdate
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(820,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus3_1= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus3_1.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus3_1);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(501); //ny
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP-59 1003, 6/19-6/26 New York promo for higher priced 6 month plan",new DateTime(2008, 6, 19),new DateTime(2008,6,26),9,criteria,plans,820,820,"Act now! Limited time offer!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-59 promos saved");
		}

		private void button19_Click(object sender, System.EventArgs e)
		{
			//rnpp60
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(825,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(824,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(823,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-60 1003, 6/26-12/31/08 Premium Services",new DateTime(2008, 6, 26),new DateTime(2008,12,31),10,criteria,plans,825,825,"Stand out in the crowd with a highlighted profile! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-60 promos saved");
		}

		private void button18_Click(object sender, System.EventArgs e)
		{
			//rnpp65
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1107; //jdate.co.uk
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(832,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(831,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(830,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(740,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-65 1107, 6/26-12/31/08 Premium Services",new DateTime(2008, 6, 26),new DateTime(2008,12,31),10,criteria,plans,832,832,"Stand out in the crowd with a highlighted profile! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-65 promos saved");
		}

		private void button17_Click(object sender, System.EventArgs e)
		{
			//rnpp63
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(838,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(837,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(836,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-63 1001, 6/26-12/31/08 Premium Services",new DateTime(2008, 6, 26),new DateTime(2008,12,31),10,criteria,plans,838,838,"Stand out in the crowd with a highlighted profile! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-63 promos saved");
		}

		private void button16_Click(object sender, System.EventArgs e)
		{
			//rnpp64
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1013; //date.ca
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(845,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(844,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(843,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(724,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-64 1013, 6/26-12/31/08 Premium Services",new DateTime(2008, 6, 26),new DateTime(2008,12,31),10,criteria,plans,845,845,"Stand out in the crowd with a highlighted profile! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-64 promos saved");
		}

		private void button12_Click(object sender, System.EventArgs e)
		{
			//rnpp-61
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1001; //american singles
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//Country = USA
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry.Values.Add(223);
			criteria.Add(expCountry );

			promo= new Promo(Constants.NULL_INT,"RNPP-61 1001, 6/30-7/7/08 Fourth of July",new DateTime(2008, 6, 26),new DateTime(2008,7,7),20,criteria,plans,727,727,"Create your own fireworks this 4th of July!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-61 promos saved");
		}

		private void button13_Click(object sender, System.EventArgs e)
		{
			//rnpp-62
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1013; //date.ca
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(746,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(752,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//Country = Canada
			Expression expCountry = new Expression(attributes.FindByID(1004),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expCountry.Values.Add(38);
			criteria.Add(expCountry );

			promo= new Promo(Constants.NULL_INT,"RNPP-62 1013, 6/30-7/7/08 Special Canada Day",new DateTime(2008, 6, 26),new DateTime(2008,7,7),20,criteria,plans,746,746,"Limited Time Canada Day Offer ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-62 promos saved");
		}

		private void button14_Click(object sender, System.EventArgs e)
		{
			//rnpp-66
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803); //los angeles
			criteria.Add(expDMA);

			//20+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-20);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			promo= new Promo(Constants.NULL_INT,"RNPP-66 1003, 6/30-7/10/08 LA x-sub promo",new DateTime(2008, 6, 30),new DateTime(2008,7,10),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-66 promos saved");
		}

		private void button11_Click(object sender, System.EventArgs e)
		{
			//rnpp-67
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803); //los angeles
			criteria.Add(expDMA);

			//5+ days after registration
			Expression expDaysAfterRegister= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterRegister.Values.Add(5);
			criteria.Add(expDaysAfterRegister);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-67 1003, 6/30-7/10/08 LA never-sub promo",new DateTime(2008, 6, 30),new DateTime(2008,7,10),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-67 promos saved");
		}

		private void button20_Click(object sender, System.EventArgs e)
		{
			//rnpp-68
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528); //miami
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP-68 1003, 6/30-7/10/08 Miami x-sub promo",new DateTime(2008, 6, 30),new DateTime(2008,7,10),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-68 promos saved");
		}

		private void button15_Click(object sender, System.EventArgs e)
		{
			//rnpp-69
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528); //miami
			criteria.Add(expDMA);

			promo= new Promo(Constants.NULL_INT,"RNPP-69 1003, 6/30-7/10/08 Miami never-sub promo",new DateTime(2008, 6, 30),new DateTime(2008,7,10),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-69 promos saved");
		}

		private void button10_Click(object sender, System.EventArgs e)
		{
			//rnpp-70
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(823,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504); //philadelphia
			criteria.Add(expDMA);

			//20+ days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-20);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			promo= new Promo(Constants.NULL_INT,"RNPP-70 1003, 6/30-7/10/08 Philadelphia x-sub promo",new DateTime(2008, 6, 30),new DateTime(2008,7,10),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-70 promos saved");
		}

		private void button21_Click(object sender, System.EventArgs e)
		{
			//rnpp-71
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(823,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504); //philadelphia
			criteria.Add(expDMA);

			//5+ days after registration
			Expression expDaysAfterRegister= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterRegister.Values.Add(5);
			criteria.Add(expDaysAfterRegister);

			promo= new Promo(Constants.NULL_INT,"RNPP-71 1003, 6/30-7/10/08 Philadelphia never-sub promo",new DateTime(2008, 6, 30),new DateTime(2008,7,10),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-71 promos saved");
		}

		private void button22_Click(object sender, System.EventArgs e)
		{
			//Matt dummy promo for test
			//rnpp-71
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(747,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(820,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(72,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504); //philadelphia
			criteria.Add(expDMA);

			Expression expDMA1= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA1.Values.Add(528); //miami
			criteria.Add(expDMA1);

			promo= new Promo(Constants.NULL_INT,"Matthew's Test Promo",new DateTime(2008, 6, 23),new DateTime(2008,7,10),99,criteria,plans,400,400,"Matthew's Test Promo");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 2
			brandID=1001; //as
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(804,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(723,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(424,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//dma
			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(504); //philadelphia
			criteria.Add(expDMA2);

			Expression expDMA22= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA22.Values.Add(528); //miami
			criteria.Add(expDMA22);

			promo= new Promo(Constants.NULL_INT,"Matthew's Test Promo",new DateTime(2008, 6, 23),new DateTime(2008,7,10),99,criteria,plans,727,727,"Matthew's Test Promo ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 3
			brandID=1013; //date.ca
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(724,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(752,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(743,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(746,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//dma
			Expression expDMA23= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA23.Values.Add(504); //philadelphia
			criteria.Add(expDMA23);

			Expression expDMA233= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA233.Values.Add(528); //miami
			criteria.Add(expDMA233);

			promo= new Promo(Constants.NULL_INT,"Matthew's Test Promo",new DateTime(2008, 6, 23),new DateTime(2008,7,10),99,criteria,plans,724,724,"Matthew's Test Promo ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 4
			brandID=1107; //jdate.co.uk
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(740,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(750,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID);


			plans.Add((PromoPlan)plan4);
//
//			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID);


//			plans.Add((PromoPlan)plan5);
//
//			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID);


//			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//dma
			Expression expDMA234= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA234.Values.Add(504); //philadelphia
			criteria.Add(expDMA23);

			Expression expDMA2334= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2334.Values.Add(528); //miami
			criteria.Add(expDMA2334);

			promo= new Promo(Constants.NULL_INT,"Matthew's Test Promo",new DateTime(2008, 6, 23),new DateTime(2008,7,10),99,criteria,plans,740,740,"Matthew's Test Promo ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("Matthew's Test Promo promos saved");
		}


		private void AddInaccessibleCriteria(ExpressionCollection criteria, PromoAttributeCollection attributes)
		{
			//dma
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(504); //philadelphia
			criteria.Add(expDMA);

			Expression expDMA2= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA2.Values.Add(528); //miami
			criteria.Add(expDMA2);
			
		}

		private void rnpp73_Click(object sender, System.EventArgs e)
		{
			//rnpp-73
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1015; //cupid.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(420,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(369,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(174,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			// Gender Mask - male
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(1);	
			criteria.Add(expGender);

			promo= new Promo(Constants.NULL_INT,"RNPP-73 1015, 7/15-7/23/08 Women Free promo for ex-sub",new DateTime(2008, 7, 15),new DateTime(2008,7,23),8,criteria,plans,420,420,"No Title.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID); 

			#endregion

			MessageBox.Show("RNPP-73 promos saved");
		}

		private void rnpp74_Click(object sender, System.EventArgs e)
		{
			//rnpp-74
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1015; //cupid.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(190,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(174,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(216,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			// Gender Mask - male
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(1);	
			criteria.Add(expGender);

			promo= new Promo(Constants.NULL_INT,"RNPP-74 1015, 7/15-7/23/08 Women Free promo for never-sub",new DateTime(2008, 7, 15),new DateTime(2008,7,23),8,criteria,plans,190,190,"No Title.");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-74 promos saved");
		}

		private void rnpp75_Click(object sender, System.EventArgs e)
		{
			//rnpp63
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(838,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(837,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(836,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(723,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-75 1001, 7/9-12/31/08 Premium Services",new DateTime(2008, 7, 9),new DateTime(2008,12,31),10,criteria,plans,838,838,"Stand out in the crowd with a highlighted profile! ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-75 promos saved");
		}

		private void button6_Click_1(object sender, System.EventArgs e)
		{
			//rnpp-80
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(859,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(838,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(837,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(836,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(6,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(723,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID);


			plans.Add((PromoPlan)plan6);

			plan8 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID);


			plans.Add((PromoPlan)plan8);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			promo= new Promo(Constants.NULL_INT,"RNPP-80 1001, 7/30-12/31/08 Premium Phase 3",new DateTime(2008, 7, 30),new DateTime(2008,12,31),10,criteria,plans,859,859,"Stand out in the crowd with a highlighted profile!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-80 promos saved");
		}

		private void button7_Click_1(object sender, System.EventArgs e)
		{
			//rnpp-81
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1004; //jdate.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(254,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(368,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(350,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//60 days after registration
			Expression expDaysAfterRegister= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterRegister.Values.Add(60);
			criteria.Add(expDaysAfterRegister);

			promo= new Promo(Constants.NULL_INT,"RNPP-81a 1004, 8/07-9/07/08 JDIL promos for non subs",new DateTime(2008, 8, 7),new DateTime(2008,9,7),9,criteria,plans,368,368,"בשביל למצוא אהבה צריך ליזום! עכשיו לזמן מוגבל מנוי במחיר אטרקטיבי במיוחד!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			#region Promo 2
			brandID=1004; //jdate.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(254,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(368,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(350,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//60 days after sub end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-60);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			promo= new Promo(Constants.NULL_INT,"RNPP-81b 1004, 8/07-9/07/08 JDIL promos for non subs",new DateTime(2008, 8, 7),new DateTime(2008,9,7),9,criteria,plans,368,368,"בשביל למצוא אהבה צריך ליזום! עכשיו לזמן מוגבל מנוי במחיר אטרקטיבי במיוחד!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-81 promos saved");
		}

		private void button8_Click_1(object sender, System.EventArgs e)
		{
			//rnpp83
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(867,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(869,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(823,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(868,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(870,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//30 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-30);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-83 1003, 8/8-8/18/08 Premium Services",new DateTime(2008, 8, 8),new DateTime(2008,8,18),8,criteria,plans,867,867,"Take advantage of this limited-time pre-New Years special offer while it lasts!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-83 promos saved");
		}

		private void button9_Click_1(object sender, System.EventArgs e)
		{
			//rnpp-84
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1004; //jdate.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(792,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(798,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(350,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//60 days after registration
			Expression expDaysAfterRegister= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterRegister.Values.Add(60);
			criteria.Add(expDaysAfterRegister);

			if (this.cbTestOnly.Checked)
				this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-84 1004, 8/21-9/21/08 JDIL promos for non subs",new DateTime(2008, 8, 21),new DateTime(2008,9,21),9,criteria,plans,798,798,"!בשביל למצוא אהבה צריך ליזום! עכשיו לזמן מוגבל מנוי במחיר אטרקטיבי במיוחד ");
			if (this.cbTestOnly.Checked)
			{
				promo.Description = "Test ONLY" + promo.Description;
				promo.PageTitle = "TEST ONLY - " + promo.PageTitle;
				promo.ListOrder = 50;
			}

			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-84 promos saved");
		}

		private void button23_Click(object sender, System.EventArgs e)
		{
			//rnpp-85
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1004; //jdate.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(792,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(798,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(350,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//60 days after sub end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-60);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			if (this.cbTestOnly.Checked)
				this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-85 1004, 8/21-9/21/08 JDIL promos for non subs",new DateTime(2008, 8, 21),new DateTime(2008,9,21),9,criteria,plans,798,798,"!בשביל למצוא אהבה צריך ליזום! עכשיו לזמן מוגבל מנוי במחיר אטרקטיבי במיוחד ");
			if (this.cbTestOnly.Checked)
			{
				promo.Description = "Test ONLY" + promo.Description;
				promo.PageTitle = "TEST ONLY - " + promo.PageTitle;
				promo.ListOrder = 50;
			}
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-85 promos saved");
		}

		private void JDateDefaultPromoCredit(int brandID)
		{
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Plan plan1, plan2, plan3, plan4, plan5, plan6;
			Promo promo;
			PromoPlan promoPlan;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();

			//PLANS
			plans= new PromoPlanCollection(brandID);
			
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(903,brandID);
			promoPlan = new PromoPlan(plan1);
			promoPlan.ListOrder = 1;
			promoPlan.ColumnGroup = 1;
			promoPlan.ResourceTemplateID = 901;
			plans.Add(promoPlan);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(883,brandID);
			promoPlan = new PromoPlan(plan2);
			promoPlan.ListOrder = 2;
			promoPlan.ColumnGroup = 1;
			promoPlan.ResourceTemplateID = 901;
			plans.Add(promoPlan);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(880,brandID);
			promoPlan = new PromoPlan(plan3);
			promoPlan.ListOrder = 3;
			promoPlan.ColumnGroup = 1;
			promoPlan.ResourceTemplateID = 901;
			plans.Add(promoPlan);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID);
			promoPlan = new PromoPlan(plan4);
			promoPlan.ListOrder = 1;
			promoPlan.ColumnGroup = 2;
			promoPlan.ResourceTemplateID = 901;
			plans.Add(promoPlan);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);
			promoPlan = new PromoPlan(plan5);
			promoPlan.ListOrder = 2;
			promoPlan.ColumnGroup = 2;
			promoPlan.ResourceTemplateID = 901;
			plans.Add(promoPlan);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);
			promoPlan = new PromoPlan(plan6);
			promoPlan.ListOrder = 3;
			promoPlan.ColumnGroup = 2;
			promoPlan.ResourceTemplateID = 901;
			plans.Add(promoPlan);

			//CRITERIAS
			criteria = new ExpressionCollection();

			//0 days after registration
			Expression expDaysAfterRegister= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterRegister.Values.Add(0);
			criteria.Add(expDaysAfterRegister);

			promo= new Promo(Constants.NULL_INT, Matchnet.Purchase.ValueObjects.PaymentType.CreditCard,
				"Default Promo for Jdate - Credit",new DateTime(2008, 8, 21),new DateTime(2100,11,21),9,criteria,plans
				,798,798,"JDate Promo - Default Plans", PromoApprovalStatus.Pending, 16000206, PromoType.Member, DateTime.MinValue);

			promo.ColHeaderResourceTemplateCollection.Add(new ColHeaderResourceTemplate(promo.PromoID, 900,1, PlanType.Regular, PremiumType.None));
			promo.ColHeaderResourceTemplateCollection.Add(new ColHeaderResourceTemplate(promo.PromoID, 900,2, PlanType.Regular, PremiumType.None));

			promo.RowHeaderResourceTemplateCollection.Add(new RowHeaderResourceTemplate(promo.PromoID, 900, 1, 1, DurationType.Month ));
			promo.RowHeaderResourceTemplateCollection.Add(new RowHeaderResourceTemplate(promo.PromoID, 900, 2,1, DurationType.Month));
			promo.RowHeaderResourceTemplateCollection.Add(new RowHeaderResourceTemplate(promo.PromoID, 900, 3,1, DurationType.Month));

			PromoEngineSA.Instance.SavePromotion(promo,brandID);
		}

		private void button24_Click(object sender, System.EventArgs e)
		{
			//rnpp-86
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1015; //cupid.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(192,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(402,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(821,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			// Gender Mask - female
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(2);	
			criteria.Add(expGender);

			//add inaccessible criterias - email campaign only
			this.AddInaccessibleCriteria(criteria, attributes);
			
			promo= new Promo(Constants.NULL_INT,"RNPP-86 1015, 8/21-8/26/08 JDIL promo for email campaign women free ex",new DateTime(2008, 8, 21),new DateTime(2008,8,26),50,criteria,plans,192,192,"!גם החודש תהני מכל יתרונות המנוי בקיופיד ");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-86 promos saved");
		}

		private void button25_Click(object sender, System.EventArgs e)
		{
			//TEST PROMO for QA

			//rnpp-87
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3, plan4;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(871,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(872,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(873,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(874,brandID);


			plans.Add((PromoPlan)plan4);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//add inaccessible criterias
			this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-87 1001, Test promo for QA",new DateTime(2008, 8, 11),new DateTime(2008,8,20),50,criteria,plans,871,871,"AS Test Promo for QA TEAM");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-87 promos saved");
		}

		private void button26_Click(object sender, System.EventArgs e)
		{
			//rnpp88
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-88 1003, 8/13-8/31/08 CLONE -Miami non-subs",new DateTime(2008, 8, 12),new DateTime(2008,8,31),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-88 promos saved");
		}

		private void button27_Click(object sender, System.EventArgs e)
		{
			//rnpp90
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//dma - los angeles
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			//5 days after registration
			Expression expDaysAfterRegister= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
			expDaysAfterRegister.Values.Add(5);
			criteria.Add(expDaysAfterRegister);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-90 1003, 8/13-8/31/08 CLONE - LA never subscribed",new DateTime(2008, 8, 12),new DateTime(2008,8,31),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-90 promos saved");
		}

		private void button28_Click(object sender, System.EventArgs e)
		{
			//rnpp91
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//dma - los angeles
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(803);
			criteria.Add(expDMA);

			//20 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-20);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-91 1003, 8/13-8/31/08 New promo for Member Spotlight project",new DateTime(2008, 8, 12),new DateTime(2008,8,31),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-91 promos saved");
		}

		private void button29_Click(object sender, System.EventArgs e)
		{
			//rnpp89
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(850,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(849,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(851,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(852,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(785,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(376,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//dma - miami
			Expression expDMA= new Expression(attributes.FindByID(1007),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expDMA.Values.Add(528);
			criteria.Add(expDMA);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-89 1003, 8/13-8/31/08 CLONE -Miami ex-sub",new DateTime(2008, 8, 12),new DateTime(2008,8,31),9,criteria,plans,850,850,"Ready to meet someone special? Become a Subscriber today!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-89 promos saved");
		}

		private void button31_Click(object sender, System.EventArgs e)
		{
			//rnpp93
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(829,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(828,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(827,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(826,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			if (this.cbTestOnly.Checked)
				this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-93 1003, 8/12-8/12/2012 CLONE -Miami ex-sub",new DateTime(2008, 8, 12),new DateTime(2012,8,12),10,criteria,plans,829,829,"Stand out from the Crowd! Purchase your Spotlight now!");
			if (this.cbTestOnly.Checked)
			{
				promo.Description = "Test ONLY" + promo.Description;
				promo.PageTitle = "TEST ONLY - " + promo.PageTitle;
				promo.ListOrder = 50;
			}
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-93 promos saved");
		}

		private void button30_Click(object sender, System.EventArgs e)
		{
			//rnpp94
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(842,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(841,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(840,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(839,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(6,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(723,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(388,brandID);


			plans.Add((PromoPlan)plan6);

			plan8 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(360,brandID);


			plans.Add((PromoPlan)plan8);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-94 1001, 8/12-8/12/2012 New promo for Member Spotlight project",new DateTime(2008, 8, 12),new DateTime(2012,8,12),10,criteria,plans,842,842,"Stand out from the Crowd! Purchase your Spotlight now!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-94 promos saved");
		}

		private void button32_Click(object sender, System.EventArgs e)
		{
			//rnpp95
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1107; //jdate.co.uk
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(835,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(834,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(833,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(740,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID);


			plans.Add((PromoPlan)plan6);

			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-95 1107, 8/12-8/12/2012 New promo for Member Spotlight project",new DateTime(2008, 8, 12),new DateTime(2012,8,12),10,criteria,plans,835,835,"Stand out from the Crowd! Purchase your Spotlight now!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-95 promos saved");
		}

		private void button33_Click(object sender, System.EventArgs e)
		{
			//rnpp96
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1013; //date.ca
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(848,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(847,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(846,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(724,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID);


			plans.Add((PromoPlan)plan6);

			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-96 1013, 8/12-8/12/2012 New promo for Member Spotlight project",new DateTime(2008, 8, 12),new DateTime(2012,8,12),10,criteria,plans,848,848,"Stand out from the Crowd! Purchase your Spotlight now!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-96 promos saved");
		}

		private void button34_Click(object sender, System.EventArgs e)
		{
			//TEST PROMO for QA

			//rnpp-98
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3, plan4;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(875,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(876,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(877,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(874,brandID);


			plans.Add((PromoPlan)plan4);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//add inaccessible criterias
			this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-98 1001, Test promo for QA",new DateTime(2008, 8, 14),new DateTime(2008,8,25),50,criteria,plans,875,875,"AS Test Promo for QA TEAM");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-98 promos saved");
		}

		private void button35_Click(object sender, System.EventArgs e)
		{
			//rnpp97
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(867,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(869,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(823,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(868,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(870,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//email campaign only
			this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-97 1003, 8/22-8/31/08 CLONE Promotion to send to Jdate x-subs",new DateTime(2008, 8, 22),new DateTime(2008,8,31),50,criteria,plans,867,867,"Exclusive offer to former JDate subscribers! Act now before this offer is gone!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);			

			#endregion

			MessageBox.Show("RNPP-97 promos saved");
		}

		private void button36_Click(object sender, System.EventArgs e)
		{
			//rnpp99
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(874,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(873,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(872,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(871,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(875,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(876,brandID);


			plans.Add((PromoPlan)plan6);

			plan8 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(877,brandID);


			plans.Add((PromoPlan)plan8);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			if (this.cbTestOnly.Checked)
				this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-99 1001, 8/19-8/12/2012 Promo for all AS",new DateTime(2008, 8, 17),new DateTime(2012,8,12),10,criteria,plans,875,875,"Subscribe today to send and receive messages, initiate IM conversations and more! And save big compared to Match.com or eHarmony.");
			if (this.cbTestOnly.Checked)
			{
				promo.Description = "Test ONLY" + promo.Description;
				promo.PageTitle = "TEST ONLY - " + promo.PageTitle;
				promo.ListOrder = 50;
			}
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-99 promos saved");
		}

		private void button37_Click(object sender, System.EventArgs e)
		{
			//rnpp100
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(867,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(869,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(823,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(868,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(870,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			//15 days after subscription end date
			Expression expDaysAfterSubscriptionExpiredBegin= new Expression(attributes.FindByID(1001),PromoEngine.ValueObjects.OperatorType.LessThanEqual,new ArrayList());
			expDaysAfterSubscriptionExpiredBegin.Values.Add(-15);
			criteria.Add(expDaysAfterSubscriptionExpiredBegin);


			promo= new Promo(Constants.NULL_INT,"RNPP-100 1003, 8/22-8/31/08 CLONE Promotion for Jdate x-subs website",new DateTime(2008, 8, 19),new DateTime(2008,8,31),8,criteria,plans,867,867,"Exclusive offer to former JDate subscribers! Act now before this offer is gone!");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);			

			#endregion

			MessageBox.Show("RNPP-100 promos saved");
		}

		private void rnpp102_Click(object sender, System.EventArgs e)
		{
			//rnpp102
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(874,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(873,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(872,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(879,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(875,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(876,brandID);


			plans.Add((PromoPlan)plan6);

			plan8 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(878,brandID);


			plans.Add((PromoPlan)plan8);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

//			Expression expReg= new Expression(attributes.FindByID(1002),PromoEngine.ValueObjects.OperatorType.GreaterThanEqual,new ArrayList());
//			expReg.Values.Add(0);
//			criteria.Add(expReg);

			if (this.cbTestOnly.Checked)
				this.AddInaccessibleCriteria(criteria, attributes);

			promo= new Promo(Constants.NULL_INT,"RNPP-102 1001, 8/28-8/12/2012 Promo for all AS",new DateTime(2008, 8, 25),new DateTime(2012,8,12),10,criteria,plans,874,874,"Subscribe today to send and receive messages, initiate IM conversations and more! And save big compared to Match.com or eHarmony.");
			if (this.cbTestOnly.Checked)
			{
				promo.Description = "Test ONLY" + promo.Description;
				promo.PageTitle = "TEST ONLY - " + promo.PageTitle;
				promo.ListOrder = 50;
			}
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-102 promos saved");
		}

		private void rnpp103_Click(object sender, System.EventArgs e)
		{
			//rnpp-103
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1015; //cupid.co.il
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(192,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(402,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(821,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			criteria.Add(expSubStatus);

			// Gender Mask - female
			Expression expGender= new Expression(attributes.FindByID(69),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expGender.Values.Add(2);	
			criteria.Add(expGender);

			//add inaccessible criterias - email campaign only
			this.AddInaccessibleCriteria(criteria, attributes);
			
			promo= new Promo(Constants.NULL_INT,"RNPP-103 1015, 8/28-9/02/08 CupidIL promo for email campaign women free ex",new DateTime(2008, 8, 28),new DateTime(2008,9,2),50,criteria,plans,192,192,"1 month for 65 ILS in Cupid");
			PromoEngineSA.Instance.SavePromotion(promo,brandID);

			#endregion

			MessageBox.Show("RNPP-103 promos saved");
		}

		private void button38_Click(object sender, System.EventArgs e)
		{
			//rnpp95b
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1107; //jdate.co.uk
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(888,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(887,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(886,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(740,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(741,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(742,brandID);


			plans.Add((PromoPlan)plan6);

			
			//CRITERIAS
			criteria = new ExpressionCollection();
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			CreateSavePromo(brandID, attributes, Constants.NULL_INT,"RNPP-95b 1107, 8/12-8/12/2012 New promo for Member Spotlight project",new DateTime(2008, 9, 7),new DateTime(2012,8,12),10,criteria,plans,Constants.NULL_INT,Constants.NULL_INT,"Stand out from the Crowd! Purchase your Spotlight now!");

			#endregion

			MessageBox.Show("RNPP-95b promos saved");
		}

		#region Helper Functions
		private void CreateSavePromo(int brandID, PromoAttributeCollection attributes,
			int promoID,string description,DateTime startDate, DateTime endDate, int listOrder, ExpressionCollection criteria,  PromoPlanCollection plans,int bestValuePlanID, int defaultPlanID, string pagetitle)
		{
			Promo promo;

			//this will create a test only promo by adding inaccessible criterias (only accessed via promocode in url), not meant for actual usage.
			if (this.cbTestOnly.Checked)
				this.AddInaccessibleCriteria(criteria, attributes);

			//create promo
			promo= new Promo(promoID,description,startDate,endDate,listOrder,criteria,plans,bestValuePlanID,defaultPlanID,pagetitle);
			
			//if test only promo, explicitly display test only indicator
			if (this.cbTestOnly.Checked)
			{
				promo.Description = "Test ONLY" + promo.Description;
				promo.PageTitle = "TEST ONLY - " + promo.PageTitle;
				promo.ListOrder = 50;
				promo.EndDate = promo.StartDate.AddDays(10);
			}

			//save promo
			PromoEngineSA.Instance.SavePromotion(promo,brandID);
		}
		#endregion

		private void button39_Click(object sender, System.EventArgs e)
		{
			//rnpp-104
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3;

			#region Promo 1
			brandID=1015; //cupid
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(192,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(402,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(821,brandID);


			plans.Add((PromoPlan)plan3);
			
			//CRITERIAS
			criteria = new ExpressionCollection();
			
			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			//email campaign only
			this.AddInaccessibleCriteria(criteria, attributes);

			CreateSavePromo(brandID, attributes, Constants.NULL_INT,"RNPP-104 1015, 9/11-11/05/2008 Cupid 1 month for 65 ILS promo plan",new DateTime(2008, 9, 7),new DateTime(2008,11,05),50,criteria,plans,Constants.NULL_INT,Constants.NULL_INT,"Cupid 1 month for 65 ILS");

			#endregion

			MessageBox.Show("RNPP-104 promos saved");
		}

		private void RNPP96b_Click(object sender, System.EventArgs e)
		{
			//rnpp96b
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6;

			#region Promo 1
			brandID=1013; //date.ca
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(898,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(896,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(895,brandID);


			plans.Add((PromoPlan)plan3);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(724,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(394,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(398,brandID);


			plans.Add((PromoPlan)plan6);

			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			CreateSavePromo(brandID, attributes, Constants.NULL_INT,"RNPP-96b 1013, 9/07-9/07/2012 New promo for Member Spotlight project",new DateTime(2008, 9, 7),new DateTime(2012,9,7),10,criteria,plans,Constants.NULL_INT,Constants.NULL_INT,"Stand out from the Crowd! Purchase your Spotlight now!");


			#endregion

			MessageBox.Show("RNPP-96b promos saved");
		}

		private void button40_Click(object sender, System.EventArgs e)
		{
			//rnpp94b
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1001; //as.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(893,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(892,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(891,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(889,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(875,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(727,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(876,brandID);


			plans.Add((PromoPlan)plan6);

			plan8 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(878,brandID);


			plans.Add((PromoPlan)plan8);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			CreateSavePromo(brandID, attributes, Constants.NULL_INT,"RNPP-94b 1001, 9/07-9/07/2012 New promo for Member Spotlight project",new DateTime(2008, 9, 7),new DateTime(2012,9,7),10,criteria,plans,Constants.NULL_INT,Constants.NULL_INT,"Stand out from the Crowd! Purchase your Spotlight now!");


			#endregion

			MessageBox.Show("RNPP-94b promos saved");
		}

		private void button41_Click(object sender, System.EventArgs e)
		{
			//rnpp109
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1003; //jdate
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(829,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(828,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(827,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(826,brandID);


			plans.Add((PromoPlan)plan7);

//			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(875,brandID);


//			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);


			plans.Add((PromoPlan)plan6);

			plan8 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan8);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			CreateSavePromo(brandID, attributes, Constants.NULL_INT,"RNPP-109 1001, 9/07-9/07/2012 New promo for Member Spotlight project",new DateTime(2008, 9, 7),new DateTime(2012,9,7),10,criteria,plans,Constants.NULL_INT,Constants.NULL_INT,"Stand out from the Crowd! Purchase your Spotlight now!");


			#endregion

			MessageBox.Show("RNPP-109 promos saved");
		}

		private void button42_Click(object sender, System.EventArgs e)
		{
			//rnpp93
			int brandID;
			PromoPlanCollection plans;
			PromoAttributeCollection attributes;
			ExpressionCollection criteria;
			Promo promo;

			attributes = PromoEngine.ServiceAdapters.PromoEngineSA.Instance.GetPromoAttributes();
			Plan plan1,plan2,plan3,plan4,plan5,plan6,plan7,plan8;

			#region Promo 1
			brandID=1003; //jdate.com
			
			//PLANS
			plans= new PromoPlanCollection(brandID);
			plan1 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(829,brandID);


			plans.Add((PromoPlan)plan1);

			plan2 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(828,brandID);


			plans.Add((PromoPlan)plan2);

			plan3 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(827,brandID);


			plans.Add((PromoPlan)plan3);

			plan7 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(826,brandID);


			plans.Add((PromoPlan)plan7);

			plan4 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(400,brandID);


			plans.Add((PromoPlan)plan4);

			plan5 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(378,brandID);


			plans.Add((PromoPlan)plan5);

			plan6 = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(418,brandID);


			plans.Add((PromoPlan)plan6);
			
			//CRITERIAS
			criteria = new ExpressionCollection();

			//subscription status
			Expression expSubStatus= new Expression(attributes.FindByID(1000),PromoEngine.ValueObjects.OperatorType.Equal,new ArrayList());
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber);
			expSubStatus.Values.Add(Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed);
			criteria.Add(expSubStatus);

			CreateSavePromo(brandID, attributes, Constants.NULL_INT,"RNPP-93 1003, 9/07-9/07/2012 New promo for Member Spotlight project",new DateTime(2008, 9, 7),new DateTime(2012,9,7),10,criteria,plans,Constants.NULL_INT,Constants.NULL_INT,"Stand out from the Crowd! Purchase your Spotlight now!");


			#endregion

			MessageBox.Show("RNPP-93b promos saved");
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			FormResourceTemplate formResourceTemplate = new FormResourceTemplate();

			formResourceTemplate.Show();
		}

		private void ButtonHeaderResourceTemplateManager_Click(object sender, System.EventArgs e)
		{
			if ((txtPromoID.Text == string.Empty) || (txtBrandID.Text == string.Empty))
			{
				MessageBox.Show("PromoID and BrandID cannot be empty.");
				return;
			}
			FormPromoHeaderResourceTemplateManager manager = 
				new FormPromoHeaderResourceTemplateManager(Convert.ToInt32(txtPromoID.Text), 
				Convert.ToInt32(txtBrandID.Text));

			manager.Show();
		}

		private void button43_Click(object sender, System.EventArgs e)
		{
			JDateDefaultPromoCredit(1003);
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			//FormApproval form = new FormApproval();

			//form.Show();
		}

		private void button44_Click(object sender, System.EventArgs e)
		{

		}

        private void menuItem4_Click(object sender, EventArgs e)
        {
            FormRecurrence form = new FormRecurrence();

            form.Show();
        }
	}
}
