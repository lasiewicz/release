using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;

namespace Matchnet.PromoEngine.Harness
{
	/// <summary>
	/// Summary description for FormPromoHeaderResourceTemplateManager.
	/// </summary>
	public class FormPromoHeaderResourceTemplateManager : System.Windows.Forms.Form
	{
		private int promoID = Constants.NULL_INT;
		private int brandID = Constants.NULL_INT;

		private ResourceTemplateCollection resourceTemplateCollection = null;
		private System.Windows.Forms.ComboBox ComboBoxHeaderType;
		private System.Windows.Forms.ComboBox ComboBoxHeaderResourceTemplate;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox ComboBoxHeaderNumber;
		private System.Windows.Forms.Button ButtonSave;
		private System.Windows.Forms.ListView ListViewCols;
		private System.Windows.Forms.ListView ListViewRows;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ListBox ListBoxPromoPlans;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button ButtonDelete;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormPromoHeaderResourceTemplateManager(int promoID, int brandID)
		{
			this.promoID = promoID;
			this.brandID = brandID;

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ComboBoxHeaderType = new System.Windows.Forms.ComboBox();
			this.ComboBoxHeaderResourceTemplate = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.ButtonSave = new System.Windows.Forms.Button();
			this.ComboBoxHeaderNumber = new System.Windows.Forms.ComboBox();
			this.ListViewCols = new System.Windows.Forms.ListView();
			this.ListViewRows = new System.Windows.Forms.ListView();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.ListBoxPromoPlans = new System.Windows.Forms.ListBox();
			this.label6 = new System.Windows.Forms.Label();
			this.ButtonDelete = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ComboBoxHeaderType
			// 
			this.ComboBoxHeaderType.Items.AddRange(new object[] {
																	"Row",
																	"Column"});
			this.ComboBoxHeaderType.Location = new System.Drawing.Point(176, 344);
			this.ComboBoxHeaderType.Name = "ComboBoxHeaderType";
			this.ComboBoxHeaderType.Size = new System.Drawing.Size(121, 21);
			this.ComboBoxHeaderType.TabIndex = 3;
			// 
			// ComboBoxHeaderResourceTemplate
			// 
			this.ComboBoxHeaderResourceTemplate.Location = new System.Drawing.Point(176, 408);
			this.ComboBoxHeaderResourceTemplate.Name = "ComboBoxHeaderResourceTemplate";
			this.ComboBoxHeaderResourceTemplate.Size = new System.Drawing.Size(121, 21);
			this.ComboBoxHeaderResourceTemplate.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(72, 344);
			this.label1.Name = "label1";
			this.label1.TabIndex = 5;
			this.label1.Text = "Header Type:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(72, 376);
			this.label2.Name = "label2";
			this.label2.TabIndex = 6;
			this.label2.Text = "Header Number:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 408);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(152, 23);
			this.label3.TabIndex = 7;
			this.label3.Text = "Header Resource Template:";
			// 
			// ButtonSave
			// 
			this.ButtonSave.Location = new System.Drawing.Point(40, 448);
			this.ButtonSave.Name = "ButtonSave";
			this.ButtonSave.Size = new System.Drawing.Size(120, 23);
			this.ButtonSave.TabIndex = 8;
			this.ButtonSave.Text = "Save";
			this.ButtonSave.Click += new System.EventHandler(this.button1_Click);
			// 
			// ComboBoxHeaderNumber
			// 
			this.ComboBoxHeaderNumber.Items.AddRange(new object[] {
																	  "1",
																	  "2",
																	  "3",
																	  "4",
																	  "5",
																	  "6",
																	  "7",
																	  "8",
																	  "9",
																	  "10"});
			this.ComboBoxHeaderNumber.Location = new System.Drawing.Point(176, 376);
			this.ComboBoxHeaderNumber.Name = "ComboBoxHeaderNumber";
			this.ComboBoxHeaderNumber.Size = new System.Drawing.Size(121, 21);
			this.ComboBoxHeaderNumber.TabIndex = 9;
			// 
			// ListViewCols
			// 
			this.ListViewCols.FullRowSelect = true;
			this.ListViewCols.GridLines = true;
			this.ListViewCols.Location = new System.Drawing.Point(64, 40);
			this.ListViewCols.Name = "ListViewCols";
			this.ListViewCols.Size = new System.Drawing.Size(240, 104);
			this.ListViewCols.TabIndex = 13;
			this.ListViewCols.View = System.Windows.Forms.View.List;
			// 
			// ListViewRows
			// 
			this.ListViewRows.FullRowSelect = true;
			this.ListViewRows.GridLines = true;
			this.ListViewRows.Location = new System.Drawing.Point(64, 192);
			this.ListViewRows.Name = "ListViewRows";
			this.ListViewRows.Size = new System.Drawing.Size(240, 128);
			this.ListViewRows.TabIndex = 14;
			this.ListViewRows.View = System.Windows.Forms.View.List;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(64, 8);
			this.label4.Name = "label4";
			this.label4.TabIndex = 15;
			this.label4.Text = "Columns:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(64, 152);
			this.label5.Name = "label5";
			this.label5.TabIndex = 16;
			this.label5.Text = "Rows:";
			// 
			// ListBoxPromoPlans
			// 
			this.ListBoxPromoPlans.Location = new System.Drawing.Point(376, 40);
			this.ListBoxPromoPlans.Name = "ListBoxPromoPlans";
			this.ListBoxPromoPlans.Size = new System.Drawing.Size(312, 277);
			this.ListBoxPromoPlans.TabIndex = 17;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(376, 8);
			this.label6.Name = "label6";
			this.label6.TabIndex = 18;
			this.label6.Text = "Promo Plans:";
			// 
			// ButtonDelete
			// 
			this.ButtonDelete.Location = new System.Drawing.Point(176, 448);
			this.ButtonDelete.Name = "ButtonDelete";
			this.ButtonDelete.Size = new System.Drawing.Size(120, 23);
			this.ButtonDelete.TabIndex = 19;
			this.ButtonDelete.Text = "Delete";
			this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
			// 
			// FormPromoHeaderResourceTemplateManager
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(752, 533);
			this.Controls.Add(this.ButtonDelete);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.ListBoxPromoPlans);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.ListViewRows);
			this.Controls.Add(this.ListViewCols);
			this.Controls.Add(this.ComboBoxHeaderNumber);
			this.Controls.Add(this.ButtonSave);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.ComboBoxHeaderResourceTemplate);
			this.Controls.Add(this.ComboBoxHeaderType);
			this.Name = "FormPromoHeaderResourceTemplateManager";
			this.Text = "FormPromoHeaderResourceTemplateManager";
			this.Load += new System.EventHandler(this.FormPromoHeaderResourceTemplateManager_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void FormPromoHeaderResourceTemplateManager_Load(object sender, System.EventArgs e)
		{
			resourceTemplateCollection = ResourceTemplateSA.Instance.GetResourceTemplateCollection();

			ComboBoxHeaderResourceTemplate.DisplayMember = "Description";
			ComboBoxHeaderResourceTemplate.ValueMember = "ResourceTemplateID";
			ComboBoxHeaderResourceTemplate.DataSource = resourceTemplateCollection;

			Promo promo = PromoEngineSA.Instance.GetPromoByID(promoID, brandID);

			DataGridTableStyle tableStyle = new DataGridTableStyle();
			tableStyle.MappingName = "Promo";

			foreach(HeaderResourceTemplate colHeaderResourceTemplate in promo.ColHeaderResourceTemplateCollection)
			{
				ListViewCols.Items.Add(new ListViewItem(new string[]{
							colHeaderResourceTemplate.HeaderNumber.ToString() + ":" + 
							colHeaderResourceTemplate.ResourceTemplateID.ToString()
							}));
			}

			foreach(HeaderResourceTemplate rowHeaderResourceTemplate in promo.RowHeaderResourceTemplateCollection)
			{
				ListViewRows.Items.Add(new ListViewItem(new string[]{
						rowHeaderResourceTemplate.HeaderNumber.ToString() + ":" + 
						rowHeaderResourceTemplate.ResourceTemplateID.ToString()}));
			}

			foreach(PromoPlan promoPlan in promo.PromoPlans)
			{
				ListBoxPromoPlans.Items.Add("PlanID:" + promoPlan.PlanID + ", Column:" + promoPlan.ColumnGroup + ", Row:" + promoPlan.ListOrder);
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Promo promo = PromoEngineSA.Instance.GetPromoByID(promoID, brandID);

			promo.ColHeaderResourceTemplateCollection[0].PlanType = Purchase.ValueObjects.PlanType.Installment;
			promo.RowHeaderResourceTemplateCollection.FindByPosition(1).Duration = 6;
			PromoEngineSA.Instance.SavePromotion(promo, 1003);
			return;
			
			
			HeaderResourceTemplateType type;

			if (ComboBoxHeaderType.SelectedItem.ToString() == "Col")
			{
				type = HeaderResourceTemplateType.Col;
				promo.ColHeaderResourceTemplateCollection.Add( 
					new ColHeaderResourceTemplate(promo.PromoID, 
					Convert.ToInt32(ComboBoxHeaderResourceTemplate.SelectedValue.ToString()),
					Convert.ToInt16(ComboBoxHeaderNumber.SelectedItem.ToString()), 
					Matchnet.Purchase.ValueObjects.PlanType.Regular,
					Matchnet.Purchase.ValueObjects.PremiumType.HighlightedProfile)
																	   );
			}
			else
			{
				type = HeaderResourceTemplateType.Row;
				promo.RowHeaderResourceTemplateCollection.Add(
					new RowHeaderResourceTemplate(promo.PromoID, 
					Convert.ToInt32(ComboBoxHeaderResourceTemplate.SelectedValue.ToString()),
					Convert.ToInt16(ComboBoxHeaderNumber.SelectedItem.ToString()), 1, Matchnet.DurationType.Month));
			}

			PromoEngineSA.Instance.SavePromotion(promo, brandID);
		}

		private void ButtonLoad_Click(object sender, System.EventArgs e)
		{
			
		}

		private void ButtonDelete_Click(object sender, System.EventArgs e)
		{
			Promo promo = PromoEngineSA.Instance.GetPromoByID(promoID, brandID);

			HeaderResourceTemplateType type;

			if (ComboBoxHeaderType.SelectedItem.ToString() == "Col")
			{
				type = HeaderResourceTemplateType.Col;
				promo.ColHeaderResourceTemplateCollection.FindByPosition((int)ComboBoxHeaderNumber.SelectedItem).MarkForDelete = true;
			}
			else
			{
				type = HeaderResourceTemplateType.Row;
				promo.RowHeaderResourceTemplateCollection.FindByPosition((int)ComboBoxHeaderNumber.SelectedItem).MarkForDelete = true;
			}


			PromoEngineSA.Instance.SavePromotion(promo, brandID);
		}
	}
}
