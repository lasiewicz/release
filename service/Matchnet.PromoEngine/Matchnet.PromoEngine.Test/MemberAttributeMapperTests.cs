﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Matchnet.Member.ServiceAdapters.Mocks;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.PromoEngine.Test
{
    [TestFixture]
    public class MemberAttributeMapperTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
        }

        [Test]
        public void GetMemberAttribute_DeviceOS_IPhone()
        {
            int deviceOS = int.MinValue;
            MockMember member = new MockMember();
            member.MemberID = 1000;

            PromoAttribute promoAttribute = new PromoAttribute();
            promoAttribute.AttrubiteType = PromoAttributeType.Calculated;
            promoAttribute.ID = 1012;

            Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper mam = new BusinessLogic.MemberAttributeMapper();
            mam.refreshedSubStatus = false;
            mam.NUnit = true;

            //iphone (mobile)
            member.SetAttributeText("TrackingRegOS", "iPhone OS 5");
            member.SetAttributeText("TrackingRegFormFactor", "<DeviceProperties><IsMobileDevice>true</IsMobileDevice><IsTablet>false</IsTablet><Manufacturer>Apple</Manufacturer><Model>iPhone</Model><ScreenPixelsHeight>480</ScreenPixelsHeight><ScreenPixelsWidth>320</ScreenPixelsWidth></DeviceProperties>");
            deviceOS = Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper.GetMemberAttributeValueInt(member, promoAttribute, GetJdateBrand(), mam);
            Assert.IsTrue(deviceOS == (int)Matchnet.Member.ValueObjects.Enumerations.DeviceOS.IPhone);

        }

        [Test]
        public void GetMemberAttribute_DeviceOS_IPad()
        {
            int deviceOS = int.MinValue;
            MockMember member = new MockMember();
            member.MemberID = 1000;

            PromoAttribute promoAttribute = new PromoAttribute();
            promoAttribute.AttrubiteType = PromoAttributeType.Calculated;
            promoAttribute.ID = 1012;

            Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper mam = new BusinessLogic.MemberAttributeMapper();
            mam.refreshedSubStatus = false;
            mam.NUnit = true;

            //ipad (tablet)
            member.SetAttributeText("TrackingRegOS", "iPhone OS 5");
            member.SetAttributeText("TrackingRegFormFactor", "<DeviceProperties><IsMobileDevice>true</IsMobileDevice><IsTablet>true</IsTablet><Manufacturer>Apple</Manufacturer><Model>iPhone</Model><ScreenPixelsHeight>480</ScreenPixelsHeight><ScreenPixelsWidth>320</ScreenPixelsWidth></DeviceProperties>");
            deviceOS = Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper.GetMemberAttributeValueInt(member, promoAttribute, GetJdateBrand(), mam);
            Assert.IsTrue(deviceOS == (int)Matchnet.Member.ValueObjects.Enumerations.DeviceOS.IPad);
        }

        [Test]
        public void GetMemberAttribute_DeviceOS_Android_Mobile()
        {
            int deviceOS = int.MinValue;
            MockMember member = new MockMember();
            member.MemberID = 1000;

            PromoAttribute promoAttribute = new PromoAttribute();
            promoAttribute.AttrubiteType = PromoAttributeType.Calculated;
            promoAttribute.ID = 1012;

            Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper mam = new BusinessLogic.MemberAttributeMapper();
            mam.refreshedSubStatus = false;
            mam.NUnit = true;

            //android (mobile)
            member.SetAttributeText("TrackingRegOS", "Android 4");
            member.SetAttributeText("TrackingRegFormFactor", "<DeviceProperties><IsMobileDevice>true</IsMobileDevice><IsTablet>false</IsTablet><Manufacturer>Samsung</Manufacturer><Model>Galaxy Gio</Model><ScreenPixelsHeight>480</ScreenPixelsHeight><ScreenPixelsWidth>320</ScreenPixelsWidth></DeviceProperties>");
            deviceOS = Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper.GetMemberAttributeValueInt(member, promoAttribute, GetJdateBrand(), mam);
            Assert.IsTrue(deviceOS == (int)Matchnet.Member.ValueObjects.Enumerations.DeviceOS.Android_Phone);

        }

        [Test]
        public void GetMemberAttribute_DeviceOS_Android_Tablet()
        {
            int deviceOS = int.MinValue;
            MockMember member = new MockMember();
            member.MemberID = 1000;

            PromoAttribute promoAttribute = new PromoAttribute();
            promoAttribute.AttrubiteType = PromoAttributeType.Calculated;
            promoAttribute.ID = 1012;

            Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper mam = new BusinessLogic.MemberAttributeMapper();
            mam.refreshedSubStatus = false;
            mam.NUnit = true;

            //android (mobile)
            member.SetAttributeText("TrackingRegOS", "Android 4");
            member.SetAttributeText("TrackingRegFormFactor", "<DeviceProperties><IsMobileDevice>true</IsMobileDevice><IsTablet>true</IsTablet><Manufacturer>Samsung</Manufacturer><Model>Galaxy Gio</Model><ScreenPixelsHeight>480</ScreenPixelsHeight><ScreenPixelsWidth>320</ScreenPixelsWidth></DeviceProperties>");
            deviceOS = Matchnet.PromoEngine.BusinessLogic.MemberAttributeMapper.GetMemberAttributeValueInt(member, promoAttribute, GetJdateBrand(), mam);
            Assert.IsTrue(deviceOS == (int)Matchnet.Member.ValueObjects.Enumerations.DeviceOS.Android_Tablet);

        }

        public Brand GetJdateBrand()
        {
            Community community = new Community(3, "JDate");
            Site site = new Site(103, community, "JDate.com", 1, 2, "en-US", 223, 7, 1, "/lib/css/jd_jc.css", 1255,
                                 DirectionType.ltr, -8, "www", "https://www.jdate.com", 3);
            return new Brand(1003, site, "Jdate.com", 9, "(877) 453-3861", 25, 40, 50);
        }
    }
}
