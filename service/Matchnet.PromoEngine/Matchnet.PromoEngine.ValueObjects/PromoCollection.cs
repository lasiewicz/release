using System;
using System.Collections;



namespace Matchnet.PromoEngine.ValueObjects
{	
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PromoCollection : CollectionBase, IValueObject, ICacheable
	{
		#region Private Members
		
		private int cacheTTLSeconds = 2000;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Absolute;
		private int BrandID= Matchnet.Constants.NULL_INT;
		private const string PROMO_CACHE_KEY="~PROMOCOLLECTION^";
	
		
		#endregion
		
		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		public PromoCollection(int brandID)
		{
			this.BrandID = brandID;			
		}

		#endregion

		#region CollectionBase Implementation
		/// <summary>
		/// 
		/// </summary>
		public Promo this[int index]
		{
			get{return (Promo)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promo"></param>
		/// <returns></returns>
		public int Add(Promo promo)
		{
			return base.InnerList.Add(promo);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <returns></returns>
		public Promo FindByID(int promoID)
		{
			foreach(Promo promo in this)
			{
				if(promo.PromoID== promoID)
				{
					return promo;
				}
			}
			return null;
		}
		/// <summary>
		/// Returs the index of the found item in the collection
		/// </summary>
		/// <param name="promoID"></param>
		/// <returns>Returns -1 if not found otherwise indesx of the item in the collection.</returns>
		public int IndexOf(int promoID)
		{
			for (int i =0; i< this.Count;i++)
			{
				if(this[i].PromoID== promoID)
				{
					return i;
				}
			}
			return -1;
		}
		#endregion

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int brandID)
		{
			return  string.Format("{0}{1}",PROMO_CACHE_KEY, brandID.ToString());
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return  string.Format("{0}{1}",PROMO_CACHE_KEY, BrandID.ToString());
		}

		#endregion
		
	}
}