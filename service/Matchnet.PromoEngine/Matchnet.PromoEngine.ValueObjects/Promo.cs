using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

#region Matchnet references
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
#endregion

namespace Matchnet.PromoEngine.ValueObjects
{
	public enum PromoApprovalStatus
	{
		/// <summary>
		/// Default value, this creates a new promo approval queue item. Should not be set externally.
		/// </summary>
		Pending = 1,
		/// <summary>
		/// 
		/// </summary>
		Approved = 2,
		/// <summary>
		/// 
		/// </summary>
		Rejected = 3 
	}

	public enum PromoType
	{
		/// <summary>
		/// Default and members would see this type of promo.
		/// </summary>
		Member = 0,
		/// <summary>
		/// Promos that admins can only see to perhaps purchase for a customer.
		/// </summary>
		AdminOnly = 1,
		/// <summary>
		/// Promos used in banners and email campaigns and targeting conditions do not apply.
		/// </summary>
		EmailLink = 2,
		/// <summary>
		/// Promos used in banners and email campaigns and still respect the targeting  conditions.
		/// </summary>
		EmailLinkWithTargeting=3,
        /// <summary>
        /// Promos used in the mobile optimized web site like Spark.Web.Mobile.Jdate
        /// </summary>
        Mobile = 4,
        /// <summary>
        /// For supervisors only promos.
        /// </summary>
        SupervisorOnly = 5,
        /// <summary>
        /// Apple In App Purchase
        /// </summary>
        AppleIAP = 6

	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Promo : IValueObject, ICloneable
	{
		#region Private Members

		private int _promoID;

		private string _description;
        
		private DateTime _startDate;

		private DateTime _endDate;

		private DateTime _insertDate;

		private int _listOrder;

		//private int _promoTypeID;
		private PromoType _promoType;

		private ExpressionCollection _criteria;

		private PromoPlanCollection _promoPlans;

		private int _bestValuePlanID;

		private int _defaultPlanID;
		
		private string _pageTitle;

        private string _legalNote;

		private PromoApprovalStatus _promoApprovalStatus;

		private int _adminMemberID;

		private Matchnet.Purchase.ValueObjects.PaymentType _paymentType;

		private ColHeaderResourceTemplateCollection colHeaderResourceTemplateCollection = new ColHeaderResourceTemplateCollection();
		private RowHeaderResourceTemplateCollection rowHeaderResourceTemplateCollection = new RowHeaderResourceTemplateCollection();

		private int upsTemplateID = Constants.NULL_INT;

        private bool recurring = false;

        private DateTime recurrenceActiveUntilDateTime = DateTime.MinValue;

        private string recurrenceCronExpression = Constants.NULL_STRING;

        private int recurrenceDurationHours = Constants.NULL_INT;

	    #endregion
		#region Constructors

		/// <summary>
		/// 
		/// </summary>
		public Promo()
		{
			_promoID=Constants.NULL_INT;
			_description=Constants.NULL_STRING;
			_startDate=DateTime.MinValue;
			_endDate=DateTime.MinValue;
			_insertDate=DateTime.MinValue;
			_listOrder=Constants.NULL_INT;
			_criteria=null;
			_promoPlans = null;//new PlanCollection();
			_bestValuePlanID=Constants.NULL_INT;
			_defaultPlanID=Constants.NULL_INT;
			_pageTitle=Constants.NULL_STRING;
            _legalNote = Constants.NULL_STRING;
            GiftTypeID = Constants.NULL_INT;
		    DisableOneClick = false;
		}

		/// <summary>
		/// TODO:deprecated constructor, left in because the harness has code referernce.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="description"></param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <param name="listOrder"></param>
		/// <param name="criteria"></param>
		/// <param name="promoPlans"></param>
		/// <param name="bestValuePlanID"></param>
		/// <param name="defaultPlanID"></param>
		/// <param name="pagetitle"></param>
		public Promo(int promoID, 
			string description,DateTime startDate, DateTime endDate, int listOrder, ExpressionCollection criteria,  PromoPlanCollection promoPlans,int bestValuePlanID, int defaultPlanID, string pagetitle)
		{
			this._promoID=promoID;
			this._description=description;
			this._startDate=startDate;
			this._endDate=endDate;
			this._insertDate=DateTime.MinValue;
			this._listOrder=listOrder;
			this._criteria=criteria;
			this._promoPlans=promoPlans;
			this._bestValuePlanID=bestValuePlanID;
			this._defaultPlanID=defaultPlanID;
			this._pageTitle=pagetitle;
            GiftTypeID = Constants.NULL_INT;
            DisableOneClick = false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="paymentType"></param>
		/// <param name="description"></param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <param name="listOrder"></param>
		/// <param name="criteria"></param>
		/// <param name="promoPlans"></param>
		/// <param name="bestValuePlanID"></param>
		/// <param name="defaultPlanID"></param>
		/// <param name="pagetitle"></param>
		/// <param name="promoApprovalStatus"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="promoType">PromoType added to support admin only & email promos</param>
		public Promo(int promoID, Matchnet.Purchase.ValueObjects.PaymentType paymentType,
			string description,DateTime startDate, DateTime endDate, int listOrder, ExpressionCollection criteria, 
			PromoPlanCollection promoPlans,int bestValuePlanID, int defaultPlanID, string pagetitle,
			PromoApprovalStatus promoApprovalStatus, int adminMemberID, PromoType promoType, DateTime insertDate, string legalNote = null)
		{
			this._promoID=promoID;
			this._paymentType = paymentType;
			this._description=description;
			this._startDate=startDate;
			this._endDate=endDate;
			this._insertDate=insertDate;
			this._listOrder=listOrder;
			this._criteria=criteria;
			this._promoPlans=promoPlans;
			this._bestValuePlanID=bestValuePlanID;
			this._defaultPlanID=defaultPlanID;
			this._pageTitle=pagetitle;
			this._promoApprovalStatus = promoApprovalStatus;
			this._adminMemberID = adminMemberID;
			this._promoType = promoType;
            GiftTypeID = Constants.NULL_INT;
            DisableOneClick = false;
            this._legalNote = legalNote;
		}
		#endregion
		#region Property methods
		/// <summary>
		/// 
		/// </summary>
		public int PromoID 
		{
			get{return _promoID;}
			set{_promoID=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Description 
		{
			get{return _description;}
			set{_description=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime StartDate
		{
			get{return _startDate;}
			set{_startDate=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime EndDate
		{
			get{return _endDate;}
			set{_endDate=value;}
		}
		/// <summary>
		/// Used to determine the active admin only promo if there are more than 1 admin only promo active.
		/// </summary>
		public DateTime InsertDate
		{
			get { return _insertDate; }
			set { _insertDate = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public int ListOrder
		{
			get{return _listOrder;}
			set{_listOrder=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public ExpressionCollection  Criteria
		{
			get{return _criteria;}
			set{_criteria=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public PromoPlanCollection PromoPlans
		{
			get{return _promoPlans;}
			set{_promoPlans=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int BestValuePlanID
		{
			get{return _bestValuePlanID;}
			set{_bestValuePlanID=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int DefaultPlanID
		{
			get{return _defaultPlanID;}
			set{_defaultPlanID=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PageTitle
		{
			get {return _pageTitle;}
			set {_pageTitle=value;}
		}

        public string LegalNote
        {
            get { return _legalNote; }
            set { _legalNote = value; }
        }
	
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.Purchase.ValueObjects.PaymentType PaymentType
		{
			get{ return _paymentType;}
			set{ _paymentType = value;}
		}

		/// <summary>
		/// MemberID of the admin who created this promo.
		/// </summary>
		public int AdminMemberID
		{
			get
			{
				return _adminMemberID;
			}
			set
			{
				_adminMemberID = value;
			}
		}

		/// <summary>
		/// Set to pending when creating a new promo.
		/// </summary>
		public PromoApprovalStatus PromoApprovalStatus
		{
			get
			{
				return _promoApprovalStatus;
			}
			set
			{
				_promoApprovalStatus = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ColHeaderResourceTemplateCollection ColHeaderResourceTemplateCollection
		{
			get
			{
				return colHeaderResourceTemplateCollection;
			}
			set
			{
				colHeaderResourceTemplateCollection = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public RowHeaderResourceTemplateCollection RowHeaderResourceTemplateCollection
		{
			get
			{
				return rowHeaderResourceTemplateCollection;
			}
			set
			{
				rowHeaderResourceTemplateCollection = value;
			}
		}

		/// <summary>
		/// PromoType was added to support admin only & email promos on top of normal member promos
		/// </summary>
		public PromoType PromoType
		{
			get { return _promoType; }
			set { _promoType = value; }
		}

        public int UPSTemplateID
        {
            get
            {
                return upsTemplateID;
            }
            set
            {
                upsTemplateID = value;
            }
        }


        public bool Recurring
        {
            get
            {
                return recurring;
            }
            set
            {
                recurring = value;
            }
        }

        public DateTime RecurrenceActiveUntilDateTime
        {
            get
            {
                return recurrenceActiveUntilDateTime;
            }
            set
            {
                recurrenceActiveUntilDateTime = value;
            }

        }

        public string RecurrenceCronExpression
        {
            get
            {
                return recurrenceCronExpression;
            }
            set
            {
                recurrenceCronExpression = value;
            }
        }

        public int RecurrenceDurationHours
        {
            get
            {
                return recurrenceDurationHours;
            }
            set
            {
                recurrenceDurationHours = value;
            }
        }

        public int GiftTypeID { get; set; }

        public bool DisableOneClick { get; set; }
		#endregion

		#region ICloneable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Object Clone()
		{
			using (Stream objStream = new MemoryStream())
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(objStream, this);
				objStream.Seek(0, SeekOrigin.Begin);
				return formatter.Deserialize(objStream);
			}
		}

		#endregion

	}
}
