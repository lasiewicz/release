using System;

namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// Summary description for ServiceConstants.
	/// </summary>
	public class ServiceConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "PROMOENGINE_SVC";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.PromoEngine.Service";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_MANAGER_NAME = "PromoEngineSM";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_MANAGER_RESOURCETEMPLATE_NAME = "ResourceTemplateSM";
		/// <summary>
		/// Performance counter name for total hits.
		/// </summary>
		public  const string PERF_TOTALCOUNT_NAME="Total hit count";
		/// <summary>
		/// Performance counter name for matched count.
		/// </summary>
		public const string PERF_TOTAL_MATCH_COUNT_NAME="Total Matched count";
	
		private ServiceConstants()
		{
		}

		#region Private members
		#endregion
		
		#region Constructors
		#endregion

		#region Property methods
		#endregion
	}
}
