using System;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// There are properties that we don't store in the DB but need it for UI purposes.
	/// This class was created for this purpose only.
	/// </summary>
	[Serializable]
	public class WebPlan
	{
		#region Private Members
		private Plan _plan = null;
		private decimal _costPer = Constants.NULL_DECIMAL;
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public Plan ThisPlan
		{ 
			get { return _plan; }
			set { _plan = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal CostPer
		{
			get { return _costPer; }
			set { _costPer = value; }
		}
		#endregion		

		/// <summary>
		/// 
		/// </summary>
		public WebPlan()
		{
			ThisPlan = new Plan();
		}
	}
}
