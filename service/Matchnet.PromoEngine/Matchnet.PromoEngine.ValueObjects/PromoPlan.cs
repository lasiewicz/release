using System;

using Matchnet.Purchase.ValueObjects;

namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// Inherited from Plan in Purchase VO. The web tier should only be using this VO from now on in replacement of Plan vo.
	/// </summary>
	[Serializable]
	public class PromoPlan : Plan
	{
		private Int32 resourceTemplateID;
		private int listOrder = Constants.NULL_INT;
		private int columnGroup = Constants.NULL_INT;

		// Ported Variables from PlanDisplayMetadata from Purchase which has been deprecated.
		private bool enabledForMember = true;

		/// <summary>
		/// Use this value to populate from ResourceTemplateCollection available through ResourceTemplateSM
		/// </summary>
		public Int32 ResourceTemplateID
		{
			get
			{
				return resourceTemplateID;
			}
			set
			{
				resourceTemplateID = value;
			}
		}

		/// <summary>
		/// Set whether this promo plan should be enabled for the given member.
		/// </summary>
		public bool EnabledForMember
		{
			get
			{
				return enabledForMember;
			}
			set
			{
				enabledForMember = value;
			}
		}

		/// <summary>
		/// Initial cost per duration (i.e. per month).
		/// </summary>
		public decimal InitialCostPerDuration
		{
			get
			{
				decimal retVal = 0;

				if(base.InitialCost > 0 && base.InitialDuration > 0)
					retVal = base.InitialCost / base.InitialDuration;

				return retVal;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrder
		{
			get 
			{ 
				return listOrder;
			}
			set 
			{ 
				listOrder = value; 
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ColumnGroup
		{
			get
			{
				return columnGroup;
			}
			set
			{
				columnGroup = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public PromoPlan()
		{
			base.PlanID = Constants.NULL_INT;
		}

		/// <summary>
		/// Map the plan object into this instance.
		/// </summary>
		/// <param name="plan"></param>
		public PromoPlan(Plan plan)
		{
			base.PlanID = plan.PlanID;
			base.CurrencyType = plan.CurrencyType;
			base.InitialCost = plan.InitialCost;
			base.InitialDuration = plan.InitialDuration;
			base.InitialDurationType = plan.InitialDurationType;
			base.RenewCost = plan.RenewCost;
			base.RenewDuration = plan.RenewDuration;
			base.RenewDurationType = plan.RenewDurationType;
			base.PlanTypeMask = plan.PlanTypeMask;
			base.PremiumTypeMask = plan.PremiumTypeMask;
			base.RenewAttempts = plan.RenewAttempts;
			base.UpdateDate = plan.UpdateDate;
			base.BestValueFlag = plan.BestValueFlag;
			base.PaymentTypeMask = plan.PaymentTypeMask;
			base.PlanResourcePaymentTypeID = plan.PlanResourcePaymentTypeID;
			base.StartDate = plan.StartDate;
			base.EndDate = plan.EndDate;
			base.CreditAmount = plan.CreditAmount;
			base.PurchaseMode = plan.PurchaseMode;
		}

	}
}
