using System;

using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.CacheSynchronization.ValueObjects;

namespace Matchnet.PromoEngine.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IResourceTemplateService.
	/// </summary>
	public interface IResourceTemplateService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		ResourceTemplateCollection GetResourceTemplateCollection(string clientHostName, CacheReference cacheReferece);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="resourceTemplate"></param>
		ResourceTemplate.ResourceTemplate SaveResourceTemplate(ResourceTemplate.ResourceTemplate resourceTemplate);
	}
}
