using System;
using System.Collections.Generic;

using Quartz;

#region Matchnet references	
using Matchnet.PromoEngine.ValueObjects;
#endregion

namespace Matchnet.PromoEngine.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IPromoEngineService
	{
		/// <summary>
		/// Returns an admin only promo for a given brand
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
		Promo GetAdminPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType);
        /// <summary>
        /// Returns a supervisor only promo
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        Promo GetSupervisorPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType);
		/// <summary>
		/// Checks to see if a member qualifies for a specific promo.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="promo"></param>
		/// <returns></returns>
		bool QualifyPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Promo promo);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
		Promo GetMemberPromo(int memberID,Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="paymentType"></param>
        /// <param name="promoType"></param>
        /// <returns></returns>
        Promo GetMemberPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType, PromoType promoType);
		/// <summary>
		/// 
		/// </summary>		
		/// <param name="brandID"></param>
		/// <returns></returns>
		PromoCollection GetPromos(int brandID);
		/// <summary>
		/// Returns an active promo with matching PromoID  
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		Promo GetPromoByID(int promoID,int brandID);
		/// <summary>
		/// Returns a promo by ID.  Have the option to search through active ones only.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		Promo GetPromoByID(int promoID,int brandID, bool activeOnly);
		/// <summary>
		/// Returns an active promo with matching PromoID, along with member specific UI metadata
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		Promo GetPromoByID(int promoID,int brandID, int memberID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		Promo GetPromoByID(int promoID,int brandID, int memberID, bool activeOnly);
		/// <summary>
		/// This method takes in substring of the Promo's description and returns all promos that match that.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		PromoCollection GetPromosByDescription(string description, int brandID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		PromoCollection GetActivePromos(int brandID);
		/// <summary>
		/// 
		/// </summary>
		int SavePromo(Promo promo, int brandID);
		/// <summary>
		/// Persists the PromoCollection to the following fields of each promo; EndDate, Description, StartDate, ListOrder, PageTitle.
		/// </summary>
		/// <param name="promos"></param>
		/// <param name="brandID"></param>
		void SavePromos(PromoCollection promos, int brandID);
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		PromoAttributeCollection GetPromoAttributes();

        string[] GetRecurrenceTriggerNames();

        string[] GetRecurrenceJobNames();

        List<JobDetail> ListJobDetails();
	}
}
