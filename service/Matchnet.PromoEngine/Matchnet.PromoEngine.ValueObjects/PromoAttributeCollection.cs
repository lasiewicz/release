using System;
using System.Collections;


namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// Summary description for PromoAttributeCollection.
	/// </summary>
	[Serializable]
	public class PromoAttributeCollection : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// Cache Key constant
		/// </summary>
		public static string ATTRIBUTES_CACHE_KEY="~PROMOATTRIBUTES^";
		#region Private Members
		
		private int cacheTTLSeconds = 6000;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Absolute;
				
		#endregion
		/// <summary>
		/// 
		/// </summary>
		public PromoAttributeCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region CollectionBase Implementation
		/// <summary>
		/// 
		/// </summary>
		public PromoAttribute this[int index]
		{
			get{return (PromoAttribute)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attribute"></param>
		/// <returns></returns>
		public int Add(PromoAttribute attribute)
		{
			return base.InnerList.Add(attribute);
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public PromoAttribute FindByID(int attributeID)
		{
			foreach(PromoAttribute attr in this)
			{
				if(attr.ID== attributeID)
				{
					return attr;
				}
			}
			return null;
		}

		#endregion

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string  GetCacheKey()
		{
			return  ATTRIBUTES_CACHE_KEY;
		}
		
		#endregion
	}
}
