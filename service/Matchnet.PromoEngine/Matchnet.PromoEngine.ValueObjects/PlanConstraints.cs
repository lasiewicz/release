using System;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.PromoEngine.ValueObjects
{
	[Serializable]	
	public class PlanConstraints
	{
		#region Private Members
		private int _initialDuration = Constants.NULL_INT;
		private DurationType _initialDurationType = DurationType.Month;
        private PlanType _planType = PlanType.Regular;
		private PremiumType _premiumType = PremiumType.None;
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public int InitialDuration
		{ 
			get { return _initialDuration; }
			set { _initialDuration = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public DurationType InitialDurationType
		{
			get { return _initialDurationType; }
			set { _initialDurationType = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public PlanType PlanType
		{
			get { return _planType; }
			set { _planType = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public PremiumType PremiumType
		{ 
			get { return _premiumType; }
			set { _premiumType = value; }
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="initialDuration"></param>
		/// <param name="initialDurationType"></param>
		/// <param name="planType"></param>
		/// <param name="premiumType"></param>
		public PlanConstraints(int initialDuration, DurationType initialDurationType, PlanType planType, PremiumType premiumType)
		{
			InitialDuration = initialDuration;
			InitialDurationType = initialDurationType;
			PlanType = planType;
			PremiumType = premiumType;
		}
	}
}
