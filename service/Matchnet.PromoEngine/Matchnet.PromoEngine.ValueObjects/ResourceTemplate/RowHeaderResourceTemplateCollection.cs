using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	/// <summary>
	/// Summary description for RowHeaderResourceTemplateCollection.
	/// </summary>
	[Serializable]
    public class RowHeaderResourceTemplateCollection : CollectionBase, IValueObject, ICloneable
	{
		/// <summary>
		/// 
		/// </summary>
		public RowHeaderResourceTemplateCollection()
		{
		}

		#region Collectionbase Implementation

		/// <summary>
		/// 
		/// </summary>
		public RowHeaderResourceTemplate this[int index]
		{
			get{return (RowHeaderResourceTemplate)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="RowHeaderResourceTemplate"></param>
		/// <returns></returns>
		public int Add(RowHeaderResourceTemplate RowHeaderResourceTemplate)
		{
			return base.InnerList.Add(RowHeaderResourceTemplate);
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerNumber"></param>
		/// <returns></returns>
		public RowHeaderResourceTemplate FindByPosition(int headerNumber)
		{
			foreach(RowHeaderResourceTemplate RowHeaderResourceTemplate in this)
			{
				if (RowHeaderResourceTemplate.HeaderNumber == headerNumber)
				{
					return RowHeaderResourceTemplate;
				}													 
			}

			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerNumber"></param>
		public void RemoveByPosition(int headerNumber)
		{
			foreach(RowHeaderResourceTemplate RowHeaderResourceTemplate in this)
			{
				if (RowHeaderResourceTemplate.HeaderNumber == headerNumber)
				{
					base.InnerList.Remove(RowHeaderResourceTemplate);
				}													 
			}
		}

        #region ICloneable Members

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Object Clone()
        {
            using (Stream objStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objStream, this);
                objStream.Seek(0, SeekOrigin.Begin);
                return formatter.Deserialize(objStream);
            }
        }

        #endregion
    }
}
