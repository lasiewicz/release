using System;

using Matchnet;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{

	/// <summary>
	/// 
	/// </summary>
	public enum HeaderResourceTemplateType : int
	{
		/// <summary>
		/// 
		/// </summary>
		Row = 0,
		/// <summary>
		/// 
		/// </summary>
		Col = 1
	}

	/// <summary>
	/// Header resourcetemplate is used to populate either a column header or a row header in a subscription promo plan display.
	/// Header type is determined in Promo List call and is stored either as a ColHeaderResourceTemplate or RowHeaderResourceTemplate.
	/// </summary>
	[Serializable]
	public class HeaderResourceTemplate : IValueObject	
	{
		private Int32 headerResourceTemplateID = Constants.NULL_INT;
		private Int32 promoID = Constants.NULL_INT;
		private Int32 resourceTemplateID = Constants.NULL_INT;
		private Int32 headerNumber = Constants.NULL_INT;
		private HeaderResourceTemplateType headerType;
		private DateTime updateDateTime = DateTime.MinValue;
		private DateTime insertDateTime = DateTime.MinValue;
		private bool markForDelete = false;

		/// <summary>
		/// 
		/// </summary>
		public Int32 HeaderResourceTemplateID
		{
			get
			{
				return headerResourceTemplateID;
			}
			set
			{
				headerResourceTemplateID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 PromoID
		{
			get
			{
				return promoID;
			}
			set
			{
				promoID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 ResourceTemplateID
		{
			get
			{
				return resourceTemplateID;
			}
			set
			{
				resourceTemplateID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 HeaderNumber
		{
			get
			{
				return headerNumber;
			}
			set
			{
				headerNumber = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public HeaderResourceTemplateType HeaderType
		{
			get
			{
				return headerType;
			}
			set
			{
				headerType = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime UpdateDateTime
		{
			get
			{
				return updateDateTime;
			}
			set
			{
				updateDateTime = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDateTime
		{
			get
			{
				return insertDateTime;
			}
			set
			{
				insertDateTime = value;
			}
		}

		/// <summary>
		/// This will result in deleting this item in the database when called through SavePromotion
		/// </summary>
		public bool MarkForDelete
		{
			get
			{
				return markForDelete;
			}
			set
			{
				markForDelete = value;
			}
		}

        public HeaderResourceTemplate()
        {
        }
		/// <summary>
		/// Use for populating from DB
		/// </summary>
		/// <param name="headerResourceTemplateID">Unique identifier for header resource template</param>
		/// <param name="promoID"></param>
		/// <param name="resourceTemplateID"></param>
		/// <param name="headerNumber"></param>
		/// <param name="headerType"></param>
		/// <param name="updateDateTime"></param>
		/// <param name="insertDateTime"></param>
		public HeaderResourceTemplate(Int32 headerResourceTemplateID, Int32 promoID, Int32 resourceTemplateID, Int16 headerNumber, HeaderResourceTemplateType headerType, DateTime updateDateTime, DateTime insertDateTime)
		{
			this.headerResourceTemplateID = headerResourceTemplateID;
			this.promoID = promoID;
			this.resourceTemplateID = resourceTemplateID;
			this.headerNumber = headerNumber;
			this.headerType = headerType;
			this.updateDateTime = updateDateTime;
			this.insertDateTime = insertDateTime;
		}

		/// <summary>
		/// Use for creating new
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="resourceTemplateID"></param>
		/// <param name="headerNumber"></param>
		/// <param name="headerType"></param>
		public HeaderResourceTemplate(Int32 promoID, Int32 resourceTemplateID, Int16 headerNumber, HeaderResourceTemplateType headerType)
		{
			this.promoID = promoID;
			this.resourceTemplateID = resourceTemplateID;
			this.headerNumber = headerNumber;
			this.headerType = headerType;
		}
	}
}
