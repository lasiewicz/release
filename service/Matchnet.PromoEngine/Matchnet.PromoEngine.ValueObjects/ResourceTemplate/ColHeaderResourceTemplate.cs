using System;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	/// <summary>
	/// Summary description for ColHeaderResourceTemplate.
	/// </summary>
	[Serializable]
	public class ColHeaderResourceTemplate : HeaderResourceTemplate
	{
		private PlanType planType;
		private PremiumType premiumType;

		/// <summary>
		/// 
		/// </summary>
		public PlanType PlanType
		{
			get
			{
				return planType;
			}
			set
			{
				planType = value;
			}
		}

		public PremiumType PremiumType
		{
			get
			{
				return premiumType;
			}
			set
			{
				premiumType = value;
			}
		}

        public ColHeaderResourceTemplate()
        {
        }

		/// <summary>
		/// Used for populationg from the DB
		/// </summary>
		/// <param name="headerResourceTemplateID"></param>
		/// <param name="promoID"></param>
		/// <param name="resourceTemplateID"></param>
		/// <param name="headerNumber"></param>
		/// <param name="planType"></param>
		/// <param name="updateDateTime"></param>
		/// <param name="insertDateTime"></param>
		public ColHeaderResourceTemplate(Int32 headerResourceTemplateID, Int32 promoID, Int32 resourceTemplateID, Int16 headerNumber, PlanType planType, PremiumType premiumType, DateTime updateDateTime, DateTime insertDateTime)
			: base (headerResourceTemplateID, promoID, resourceTemplateID, headerNumber, HeaderResourceTemplateType.Col, updateDateTime, insertDateTime)
		{
			this.planType = planType;
			this.premiumType = premiumType;
		}

		/// <summary>
		/// Use for creating new or updating
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="resourceTemplateID"></param>
		/// <param name="headerNumber"></param>
		/// <param name="planType"></param>
		/// <param name="premiumType"></param>
		public ColHeaderResourceTemplate(Int32 promoID, Int32 resourceTemplateID, Int16 headerNumber, PlanType planType, PremiumType premiumType)
			: base (promoID, resourceTemplateID, headerNumber, HeaderResourceTemplateType.Col)
		{
			this.planType = planType;
			this.premiumType = premiumType;
		}
	}
}
