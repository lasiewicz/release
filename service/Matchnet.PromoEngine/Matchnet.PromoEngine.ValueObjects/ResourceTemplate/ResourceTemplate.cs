using System;

using Matchnet;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	public enum ResourceTemplateType
	{
		Row = 1, 
		Column = 2,
		Plan = 3
	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ResourceTemplate : IValueObject
	{
		private int resourceTemplateID = Constants.NULL_INT;
		private string content = Constants.NULL_STRING;
		private string description = Constants.NULL_STRING;
		private int groupID = Constants.NULL_INT;
		private bool groupDefault = false;
		private ResourceTemplateType resourceTemplateType;
		private DateTime insertDateTime = DateTime.MinValue;
		private DateTime updateDateTime = DateTime.MinValue;

		/// <summary>
		/// 
		/// </summary>
		public int ResourceTemplateID
		{
			get
			{
				return resourceTemplateID;
			}
			set
			{
				resourceTemplateID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Content
		{
			get
			{
				return content;
			}
			set
			{
				content = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}
		public int GroupID
		{
			get
			{
				return groupID;
			}
			set
			{
				groupID = value;
			}
		}
		public bool GroupDefault
		{
			get
			{
				return groupDefault;
			}
			set
			{
				groupDefault = value;
			}
		}
		public ResourceTemplateType ResourceTemplateType
		{
			get
			{
				return resourceTemplateType;
			}
			set
			{
				resourceTemplateType = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDateTime
		{
			get
			{
				return insertDateTime;
			}
			set
			{
				insertDateTime = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime UpdateDateTime
		{
			get
			{
				return updateDateTime;
			}
			set
			{
				updateDateTime = value;
			}
		}
        public ResourceTemplate()
        {
        }

		/// <summary>
		/// Use this constructor for populating from DB.
		/// </summary>
		/// <param name="resourceTemplateID"></param>
		/// <param name="content"></param>
		/// <param name="description"></param>
		/// <param name="groupDefault"></param>
		/// <param name="insertDateTime"></param>
		/// <param name="updateDateTime"></param>
		public ResourceTemplate(int resourceTemplateID, string content, string description, int groupID, bool groupDefault, ResourceTemplateType resourceTemplateType, DateTime insertDateTime, DateTime updateDateTime)
		{
			this.resourceTemplateID = resourceTemplateID;
			this.content = content;
			this.description = description;
			this.groupID = groupID;
			this.groupDefault = groupDefault;
			this.resourceTemplateType = resourceTemplateType;
			this.insertDateTime = insertDateTime;
			this.updateDateTime = updateDateTime;
		}

		/// <summary>
		/// Use this constructor for creating new.
		/// Save method will pass in Constants.NULL_INT as the ID to generate a new resource template.
		/// </summary>
		/// <param name="content"></param>
		/// <param name="description"></param>
		public ResourceTemplate(string content, string description)
		{
			this.content = content;
			this.description = description;
		}

		/// <summary>
		/// Use this constructor for updating
		/// </summary>
		/// <param name="resourceTemplateID"></param>
		/// <param name="content"></param>
		/// <param name="description"></param>
		/// <param name="groupID"></param>
		/// <param name="groupDefault"></param>
		/// <param name="resourceTemplateType"></param>
		public ResourceTemplate(int resourceTemplateID, string content, string description, int groupID, ResourceTemplateType resourceTemplateType, bool groupDefault)
		{
			this.resourceTemplateID = resourceTemplateID;
			this.content = content;
			this.description = description;
			this.groupID = groupID;
			this.groupDefault = groupDefault;
			this.resourceTemplateType = resourceTemplateType;
		}
	}
}
