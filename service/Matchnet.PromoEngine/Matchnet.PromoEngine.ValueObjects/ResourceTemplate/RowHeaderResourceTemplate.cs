using System;
using Matchnet.Purchase.ValueObjects;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	/// <summary>
	/// Summary description for RowHeaderResourceTemplate.
	/// </summary>
	[Serializable]
	public class RowHeaderResourceTemplate : HeaderResourceTemplate
	{
		private int duration;
		private DurationType durationType;

		/// <summary>
		/// 
		/// </summary>
		public int Duration
		{
			get
			{
				return duration;
			}
			set
			{
				duration = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public DurationType DurationType
		{
			get
			{
				return durationType;
			}
			set
			{
				durationType = value;
			}
		}
        /// <summary>
        /// 
        /// </summary>
        public RowHeaderResourceTemplate()
        {
        }
		/// <summary>
		/// Used for populationg from the DB
		/// </summary>
		/// <param name="headerResourceTemplateID"></param>
		/// <param name="promoID"></param>
		/// <param name="resourceTemplateID"></param>
		/// <param name="headerNumber"></param>
		/// <param name="duration"></param>
		/// <param name="durationType"></param>
		/// <param name="updateDateTime"></param>
		/// <param name="insertDateTime"></param>
		public RowHeaderResourceTemplate(Int32 headerResourceTemplateID, Int32 promoID, Int32 resourceTemplateID, Int16 headerNumber, int duration, DurationType durationType, DateTime updateDateTime, DateTime insertDateTime)
			: base (headerResourceTemplateID, promoID, resourceTemplateID, headerNumber, HeaderResourceTemplateType.Row, updateDateTime, insertDateTime)
		{
			this.duration = duration;
			this.durationType = durationType;
		}

		/// <summary>
		/// Use for creating new or updating
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="resourceTemplateID"></param>
		/// <param name="headerNumber"></param>
		/// <param name="duration"></param>
		/// <param name="durationType"></param>
		public RowHeaderResourceTemplate(Int32 promoID, Int32 resourceTemplateID, Int16 headerNumber, int duration, DurationType durationType)
			: base (promoID, resourceTemplateID, headerNumber, HeaderResourceTemplateType.Row)
		{
			this.duration = duration;
			this.durationType = durationType;
		}
	}
}
