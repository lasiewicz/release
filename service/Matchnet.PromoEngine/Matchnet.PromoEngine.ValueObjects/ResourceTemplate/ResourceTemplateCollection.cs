using System;
using System.Collections;

using Matchnet;
using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ResourceTemplateCollection : CollectionBase, IValueObject, ICacheable, IReplicable
	{
		// Cache for one hour
		private int cacheTTLSeconds = 3600;
		/// <summary>
		/// 
		/// </summary>
		public const string RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY ="~RESOURCETEMPLATECOLLECTION";
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		// Ensure that it's always reloaded every hour.
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Absolute;
		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		
		/// <summary>
		/// 
		/// </summary>
		public ResourceTemplateCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		#region Collectionbase Implementation

		/// <summary>
		/// 
		/// </summary>
		public ResourceTemplate this[int index]
		{
			get{return (ResourceTemplate)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="resourceTemplate"></param>
		/// <returns></returns>
		public int Add(ResourceTemplate resourceTemplate)
		{
			return base.InnerList.Add(resourceTemplate);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="resourceTemplateID"></param>
		/// <returns></returns>
		public ResourceTemplate FindByResourceTemplateID(int resourceTemplateID)
		{
			foreach(ResourceTemplate resourceTemplate in this)
			{
				if (resourceTemplate.ResourceTemplateID == resourceTemplateID)
					return resourceTemplate;
			}

			return null;
		}

		#endregion

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return  RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY;
		}

		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
