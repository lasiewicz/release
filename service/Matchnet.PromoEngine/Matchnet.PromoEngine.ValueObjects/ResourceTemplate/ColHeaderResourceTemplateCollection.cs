using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	/// <summary>
	/// Summary description for ColHeaderResourceTemplateCollection.
	/// </summary>
	[Serializable]
	public class ColHeaderResourceTemplateCollection : CollectionBase, IValueObject, ICloneable
	{
		/// <summary>
		/// 
		/// </summary>
		public ColHeaderResourceTemplateCollection()
		{
		}

		#region Collectionbase Implementation

		/// <summary>
		/// 
		/// </summary>
		public ColHeaderResourceTemplate this[int index]
		{
			get{return (ColHeaderResourceTemplate)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ColHeaderResourceTemplate"></param>
		/// <returns></returns>
		public int Add(ColHeaderResourceTemplate ColHeaderResourceTemplate)
		{
			return base.InnerList.Add(ColHeaderResourceTemplate);
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerNumber"></param>
		/// <returns></returns>
		public ColHeaderResourceTemplate FindByPosition(int headerNumber)
		{
			foreach(ColHeaderResourceTemplate ColHeaderResourceTemplate in this)
			{
				if (ColHeaderResourceTemplate.HeaderNumber == headerNumber)
				{
					return ColHeaderResourceTemplate;
				}													 
			}

			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerNumber"></param>
		public void RemoveByPosition(int headerNumber)
		{
			foreach(ColHeaderResourceTemplate ColHeaderResourceTemplate in this)
			{
				if (ColHeaderResourceTemplate.HeaderNumber == headerNumber)
				{
					base.InnerList.Remove(ColHeaderResourceTemplate);
				}													 
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Object Clone()
        {
            using (Stream objStream = new MemoryStream())
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(objStream, this);
                objStream.Seek(0, SeekOrigin.Begin);
                return formatter.Deserialize(objStream);
            }
        }

	}
}
