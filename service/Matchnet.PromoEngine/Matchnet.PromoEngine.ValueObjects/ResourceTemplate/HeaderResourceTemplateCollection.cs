using System;
using System.Collections;

namespace Matchnet.PromoEngine.ValueObjects.ResourceTemplate
{
	/// <summary>
	/// Summary description for HeaderResourceTemplateCollection.
	/// </summary>
	[Serializable]
	public class HeaderResourceTemplateCollection : CollectionBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public HeaderResourceTemplateCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		#region Collectionbase Implementation

		/// <summary>
		/// 
		/// </summary>
		public HeaderResourceTemplate this[int index]
		{
			get{return (HeaderResourceTemplate)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerResourceTemplate"></param>
		/// <returns></returns>
		public int Add(HeaderResourceTemplate headerResourceTemplate)
		{
			return base.InnerList.Add(headerResourceTemplate);
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerType"></param>
		/// <param name="headerNumber"></param>
		/// <returns></returns>
		public HeaderResourceTemplate FindByPosition(HeaderResourceTemplateType headerType, int headerNumber)
		{
			foreach(HeaderResourceTemplate headerResourceTemplate in this)
			{
				if ((headerResourceTemplate.HeaderType == headerType) && (headerResourceTemplate.HeaderNumber == headerNumber))
				{
					return headerResourceTemplate;
				}													 
			}

			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerType"></param>
		/// <param name="headerNumber"></param>
		public void RemoveByPosition(HeaderResourceTemplateType headerType, int headerNumber)
		{
			foreach(HeaderResourceTemplate headerResourceTemplate in this)
			{
				if ((headerResourceTemplate.HeaderType == headerType) && (headerResourceTemplate.HeaderNumber == headerNumber))
				{
					base.InnerList.Remove(headerResourceTemplate);
				}													 
			}
		}

	}
}
