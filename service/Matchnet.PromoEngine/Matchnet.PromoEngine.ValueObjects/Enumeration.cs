﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PromoEngine.ValueObjects
{
    [Serializable]
    public class Enumeration
    {
        /* Note: This enum has been moved to Matchnet.Member.ValueObjects.Enumerations
        public enum DeviceOS
        {
            IPhone = 1,
            IPad = 2,
            Android_Phone = 3,
            Android_Tablet = 4,
            Blackberry = 5,
            Blackberry_Tablet = 6,
            Linux_Phone = 7,
            Palm_Phone = 8,
            Palm_Tablet = 9,
            Symbian_Phone = 10,
            Windows_Phone = 11,
            Windows_Tablet = 12
        }
         * */
    }
}
