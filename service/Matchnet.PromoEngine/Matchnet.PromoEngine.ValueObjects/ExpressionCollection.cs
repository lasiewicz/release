using System;
using System.Collections;


namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ExpressionCollection	: CollectionBase, IValueObject
	{
		#region Private members
		#endregion
		
		#region Constructors
		#endregion

		#region Property methods
		#endregion

		#region CollectionBase Implementation
		/// <summary>
		/// 
		/// </summary>
		public Expression this[int index]
		{
			get{return (Expression)base.InnerList[index];}
			
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="expression"></param>
		/// <returns></returns>
		public int Add(Expression expression)
		{
			return base.InnerList.Add(expression);
		}

		#endregion
	}
}
