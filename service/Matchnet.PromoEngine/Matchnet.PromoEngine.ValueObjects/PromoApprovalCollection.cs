using System;
using System.Collections;
using Matchnet;

namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// Summary description for PromoApprovalCollection.
	/// </summary>
	[Serializable]
	public class PromoApprovalCollection: CollectionBase, IEnumerable, IValueObject, ICacheable
	{
		private int cacheTTLSeconds = 6000;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Absolute;
		/// <summary>
		/// 
		/// </summary>
		public const string PROMOAPPROVAL_CACHE_KEY="~PROMOAPPROVAL^";

		/// <summary>
		/// 
		/// </summary>
		public PromoApprovalCollection()
		{
		}

		#region CollectionBase Implementation

		/// <summary>
		/// 
		/// </summary>
		public PromoApproval this[int index]
		{
			get{return (PromoApproval)base.InnerList[index];}
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoApproval"></param>
		/// <returns></returns>
		public int Add(PromoApproval promoApproval)
		{
			return base.InnerList.Add(promoApproval);
		}

		#endregion

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}
		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return  PROMOAPPROVAL_CACHE_KEY;
		}

		#endregion
	}
}
