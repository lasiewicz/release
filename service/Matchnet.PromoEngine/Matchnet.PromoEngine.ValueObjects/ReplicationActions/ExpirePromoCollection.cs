using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.PromoEngine.ValueObjects.ReplicationActions
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ExpirePromoCollection: IReplicationAction, ISerializable
	{
		private string _cacheKey;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="_cacheKey"></param>
		public ExpirePromoCollection(string _cacheKey)
		{
			this._cacheKey = _cacheKey;
		}

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public string CacheKey
		{
			get{return this._cacheKey;}
			set{this._cacheKey=value;}
		}
		
		#endregion

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExpirePromoCollection(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new BinaryReader(ms);
			
			this._cacheKey= br.ReadString();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(this._cacheKey);

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}