using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Matchnet;


namespace Matchnet.PromoEngine.ValueObjects
{
	#region PromoPlan Display Validation

	/// <summary>
	/// Returned type from server side validation
	/// </summary>
	public enum PlanDisplayValidation : int
	{
		/// <summary>
		/// 
		/// </summary>
		None = 0,
		/// <summary>
		/// 
		/// </summary>
		PurchaseModeChanged = 1,
		/// <summary>
		/// 
		/// </summary>
		RemainingCreditChanged = 2,
		/// <summary>
		/// 
		/// </summary>
		PurchaseAllowedWithRemainingCreditChanged = 3
	};
	
	#endregion

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public sealed class PromoPlanCollection : CollectionBase, IValueObject, ICacheable, ICloneable
	{
		#region Private Members
		
		//private int cacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.SerivceAdapters.RuntimeSettings.GetSetting("SUBSCRIPTIONSVC_CACHE_TTL_PLANS"));
		private int cacheTTLSeconds = 6000;
		private Matchnet.CacheItemPriorityLevel cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode cacheItemMode = Matchnet.CacheItemMode.Absolute;
		private int brandID = Matchnet.Constants.NULL_INT;

		#endregion


		#region Constructors
        public PromoPlanCollection()
        {
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		public PromoPlanCollection(int brandID)
		{
			this.brandID = brandID;
		}

		#endregion

		#region CollectionBase Implementation

		/// <summary>
		/// 
		/// </summary>
		public PromoPlan this[int index]
		{
			get{return (PromoPlan)base.InnerList[index];}
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoPlan"></param>
		/// <returns></returns>
		public int Add(PromoPlan promoPlan)
		{
			return base.InnerList.Add(promoPlan);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="planID"></param>
		/// <returns></returns>
		public PromoPlan FindByID(int planID)
		{
			foreach(PromoPlan promoPlan in this)
			{
				if(promoPlan.PlanID == planID)
				{
					return promoPlan;
				}
			}
			return null;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PromoPlan FindByPosition(int listOrder, int columnGroup)
		{
			foreach(PromoPlan promoPlan in this)
			{
				if(promoPlan.ListOrder == listOrder && promoPlan.ColumnGroup == columnGroup)
				{
					return promoPlan;
				}
			}
			return null;
		}


		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get { return cacheTTLSeconds; }
			set { cacheTTLSeconds = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get { return cacheItemMode; }
			set { cacheItemMode = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get { return cachePriority; }
			set { cachePriority = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int brandID)
		{
			return "~PromoPlanCollection^" + brandID.ToString();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return "~PromoPlanCollection^" + brandID.ToString();
		}

		#endregion

		#region

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Object Clone()
		{
			using (Stream objStream = new MemoryStream())
			{
				IFormatter formatter = new BinaryFormatter();
				formatter.Serialize(objStream, this);
				objStream.Seek(0, SeekOrigin.Begin);
				return formatter.Deserialize(objStream);
			}			
		}

		#endregion

	}
}
